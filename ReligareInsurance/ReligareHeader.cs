﻿using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReligareInsurance
{
    public class ReligareQuotation
    {
        #region private prop
        int locationId;
        int productType;
        string productId;
        DateTime? startDate;
        DateTime? endDate;
        int term;
        string ped;
        int travellers;
        List<TravellerAge> travellerAges;
        string sumInsuredId;
        string tripType;
        string maxTripPeriod;
        string rangeId;
        decimal premium;
        string productName;
        int agentId;
        decimal markupValue;
        string markupType;
        decimal markup;
        decimal handlingFeeValue;
        string handlingFeeType;
        decimal handlingFee;
        decimal discountValue;
        string discountType;
        decimal discount;
        #endregion
        #region public properties
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public decimal MarkupValue
        {
            get { return markupValue; }
            set { markupValue = value; }
        }
        public decimal HandlingFeeValue
        {
            get { return handlingFeeValue; }
            set { handlingFeeValue = value; }
        }
        public decimal Markup
        {
            get { return markup; }
            set { markup = value; }
        }
        public decimal HandlingFee
        {
            get { return handlingFee; }
            set { handlingFee = value; }
        }
        public string MarkupType
        {
            get { return markupType; }
            set { markupType = value; }
        }
        public string HandlingFeeType
        {
            get { return handlingFeeType; }
            set { handlingFeeType = value; }
        }
        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }
        public int LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        public decimal Premium
        {
            get { return premium; }
            set { premium = value; }
        }
        public List<TravellerAge> TravellerAges
        {
            get { return travellerAges; }
            set { travellerAges = value; }
        }
        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }
        public string MaxTripPeriod
        {
            get { return maxTripPeriod; }
            set { maxTripPeriod = value; }
        }
        public string TripType
        {
            get { return tripType; }
            set { tripType = value; }
        }
        public string RangeId
        {
            get { return rangeId; }
            set { rangeId = value; }
        }
        public int ProductType
        {
            get { return productType; }
            set { productType = value; }
        }
        public int Term
        {
            get { return term; }
            set { term = value; }
        }
        public string ProductId
        {
            get { return productId; }
            set { productId = value; }
        }
        public string Ped
        {
            get { return ped; }
            set { ped = value; }
        }
        public int Travellers
        {
            get { return travellers; }
            set { travellers = value; }
        }
        public string SumInsuredId
        {
            get { return sumInsuredId; }
            set { sumInsuredId = value; }
        }
        public DateTime? StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime? EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        #endregion
        #region methods
        public static DataTable GetInsuranceTypes()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                return DBGateway.FillDataTableSP(ReligareSPNames.GetReligareProductTypes, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare insurance types:" + ex.ToString(), "");
                throw ex;
            }
        }
        public static DataTable GetProductDetails(ref string productId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@productId", productId);
                return DBGateway.FillDataTableSP(ReligareSPNames.GetReligareProductDetails, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the product Details:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable GetTravellerAgeDetails(ref string insuredId, ref int ped, ref int term,string sumInsuredId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[4];
                parameters[0] = new SqlParameter("@SUM_INSURED_ID", insuredId);
                parameters[1] = new SqlParameter("@ped", ped);
                parameters[2] = new SqlParameter("@term", term);
                parameters[3] = new SqlParameter("@Insured_id", sumInsuredId);
                return DBGateway.FillDataTableSP(ReligareSPNames.GetReligareTravellerDetails, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the Traveller Age Details:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable GetSumInsuredDetails(ref string insuredId, ref int ped, ref int term)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@PRODUCT_ID", insuredId);
                parameters[1] = new SqlParameter("@ped", ped);
                parameters[2] = new SqlParameter("@term", term); 
                return DBGateway.FillDataTableSP(ReligareSPNames.GetReligareSumInsuredDetails, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the Traveller Age Details:" + ex.ToString(), "");
                throw ex;
            }
        }
        public static string GetSumInsuredRangeID(ref string insuredId, ref int ped, ref int term, ref int detailsID)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[4];
                parameters[0] = new SqlParameter("@SUM_INSURED_DETAIL_ID", detailsID);
                parameters[1] = new SqlParameter("@SUM_INSURED_ID", insuredId);
                parameters[2] = new SqlParameter("@ped", ped);
                parameters[3] = new SqlParameter("@term", term);
                return Utility.ToString(DBGateway.FillDataTableSP(ReligareSPNames.GetReligareSumInsuredRangeId, parameters).Rows[0]["RANGE_ID"]);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the sum insured Range ID:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable GetQuestions(ref string productId, ref string typeId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PRODUCT_ID", productId);
                parameters[1] = new SqlParameter("@typeId", typeId);
                return DBGateway.FillDataTableSP(ReligareSPNames.Get_ReligareInsQuestions, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare PED Questionary:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable GetEducationQuestions(ref string productId, ref string typeId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PRODUCT_ID", productId);
                parameters[1] = new SqlParameter("@typeId", typeId);
                return DBGateway.FillDataTableSP(ReligareSPNames.GetEducationReligareInsQuestions, parameters);                 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare Educational Questionary:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable GetOptionalCoverQuestions(ref string productId, ref string typeId)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PRODUCT_ID", productId);
                parameters[1] = new SqlParameter("@typeId", typeId);
                return DBGateway.FillDataTableSP(ReligareSPNames.GetOPCoverReligareInsQuestions, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare optional Questionary:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable GetVisaNationalities()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                return DBGateway.FillDataTableSP(ReligareSPNames.GetVisaNationalities, parameters);
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the country Nationalities:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static DataTable Get_Relations(string status)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@status", status);
                return DBGateway.FillDataTableSP(ReligareSPNames.GetRelationOfProposer, parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare Relations:" + ex.ToString(), "");
                throw ex;
            }
         }
        public static string GetDiscountDetails(ref int travellerCount)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@travellerCount", travellerCount);
                return Utility.ToString( DBGateway.FillDataTableSP(ReligareSPNames.GETReligareDiscountMaster, parameters).Rows[0]["DISCOUNT_PERCENTAGE"]);
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare Discount Details:" + ex.ToString(), "");
                throw ex;
            }
         }
        #endregion
    }
    public class TravellerAge
    {
        public string travellerAge { get; set; }
    }
    public class ReligareHeader
    {
        #region Properties
        string addons;
        int header_id;
        string produ_type;
        string produ_id;
        string productName;
        DateTime? startdate;
        DateTime? endDate;
        int days;
        bool isPED;
        string policy_no;
        string cover_type;
        string business_type;
        bool trip_from_india;
        string voucher_status;
        int noofPax;
        string sumInsured_optioncode;
        string sumInsured;
        decimal? premium_Amount;
        decimal? addFee;
        decimal? markup;
        decimal? markupValue;
        string markupType;
        decimal? discount;
        decimal? discountValue;
        string discountType;
        string sourceCurrency;
        decimal? sourceAmount;
        decimal? source_discount;
        decimal? rOE;
        decimal? b2CMarkup;
        decimal? b2CMarkupValue;
        string b2CMarkupType;
        bool isAccounted;
        string currency;
        string trans_type;
        string agency_ref;
        int agent_id;
        int location_id;
        int status;
        DateTime? cancel_datetime;
        int created_by;
        DateTime? created_on;
        int modified_by;
        DateTime? modified_on;
        string remarks;
        string visitPurpose;
        string tripType;
        string sponsorName;
        string universityName;
        string universityAddress;
        string courseDetails;
        string relationshipToStudent;
        DateTime? sponsorDOB;
        decimal handlingFeeValue;
        string handlingFeeType;
        decimal handlingFee;
        decimal gstValue;
        string maxTripPeriod;
        #endregion
        #region public properties 
        public string MaxTripPeriod
        {
            get { return maxTripPeriod; }
            set { maxTripPeriod = value; }
        }
        public decimal GstValue
        {
            get { return gstValue; }
            set { gstValue = value; }
        }
        public decimal HandlingFee
        {
            get { return handlingFee; }
            set { handlingFee = value; }
        }
        public decimal HandlingFeeValue
        {
            get { return handlingFeeValue; }
            set { handlingFeeValue = value; }
        }       
        public string HandlingFeeType
        {
            get { return handlingFeeType; }
            set { handlingFeeType = value; }
        }
        public string CourseDetails
        {
            get { return courseDetails; }
            set { courseDetails = value; }
        }
        public string RelationshipToStudent
        {
            get { return relationshipToStudent; }
            set { relationshipToStudent = value; }
        }
        public DateTime? SponsorDOB
        {
            get { return sponsorDOB; }
            set { sponsorDOB = value; }
        }
        public string UniversityAddress
        {
            get { return universityAddress; }
            set { universityAddress = value; }
        }
        public string UniversityName
        {
            get { return universityName; }
            set { universityName = value; }
        }
        public string SponsorName
        {
            get { return sponsorName; }
            set { sponsorName = value; }
        }

        public string Addons
        {
            get { return addons; }
            set { addons = value; }
        }
        public string TripType
        {
            get { return tripType; }
            set { tripType = value; }
        }
        public string VisitPurpose
        {
            get { return visitPurpose; }
            set { visitPurpose = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public DateTime? Cancel_datetime
        {
            get { return cancel_datetime; }
            set { cancel_datetime = value; }
        }
        public DateTime? Created_on
        {
            get { return created_on; }
            set { created_on = value; }
        }
        public DateTime? Modified_on
        {
            get { return modified_on; }
            set { modified_on = value; }
        }
        public int Created_by
        {
            get { return created_by; }
            set { created_by = value; }
        }
        public int Modified_by
        {
            get { return modified_by; }
            set { modified_by = value; }
        }
        public int Status
        {
            get { return status; }
            set { status = value; }
        }
        public bool IsAccounted
        {
            get { return isAccounted; }
            set { isAccounted = value; }
        }
        public int Agent_id
        {
            get { return agent_id; }
            set { agent_id = value; }
        }
        public int Location_id
        {
            get { return location_id; }
            set { location_id = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public string Agency_ref
        {
            get { return agency_ref; }
            set { agency_ref = value; }
        }
        public string Trans_type
        {
            get { return trans_type; }
            set { trans_type = value; }
        }
        public string SumInsured
        {
            get { return sumInsured; }
            set { sumInsured = value; }
        }
        public decimal? Premium_Amount
        {
            get { return premium_Amount; }
            set { premium_Amount = value; }
        }
        public decimal? AddFee
        {
            get { return addFee; }
            set { addFee = value; }
        }
        public decimal? Markup
        {
            get { return markup; }
            set { markup = value; }
        }
        public decimal? MarkupValue
        {
            get { return markupValue; }
            set { markupValue = value; }
        }
        public decimal? Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        public decimal? DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public decimal? SourceAmount
        {
            get { return sourceAmount; }
            set { sourceAmount = value; }
        }
        public decimal? Source_discount
        {
            get { return source_discount; }
            set { source_discount = value; }
        }
        public decimal? ROE
        {
            get { return rOE; }
            set { rOE = value; }
        }
        public decimal? B2CMarkup
        {
            get { return b2CMarkup; }
            set { b2CMarkup = value; }
        }
        public decimal? B2CMarkupValue
        {
            get { return b2CMarkupValue; }
            set { b2CMarkupValue = value; }
        }
        public string SourceCurrency
        {
            get { return sourceCurrency; }
            set { sourceCurrency = value; }
        }
        public string SumInsured_optioncode
        {
            get { return sumInsured_optioncode; }
            set { sumInsured_optioncode = value; }
        }
        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public string B2CMarkupType
        {
            get { return b2CMarkupType; }
            set { b2CMarkupType = value; }
        }
        public string MarkupType
        {
            get { return markupType; }
            set { markupType = value; }
        }
        public DateTime? Startdate
        {
            get { return startdate; }
            set { startdate = value; }
        }
        public DateTime? EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public bool IsPED
        {
            get { return isPED; }
            set { isPED = value; }
        }
        public bool Trip_from_india
        {
            get { return trip_from_india; }
            set { trip_from_india = value; }
        }
        public int Header_id
        {
            get { return header_id; }
            set { header_id = value; }
        }
        public int Days
        {
            get { return days; }
            set { days = value; }
        }
        public int NoofPax
        {
            get { return noofPax; }
            set { noofPax = value; }
        }
        public string Produ_type
        {
            get { return produ_type; }
            set { produ_type = value; }
        }
        public string Produ_id
        {
            get { return produ_id; }
            set { produ_id = value; }
        }
        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }
        public string Policy_no
        {
            get { return policy_no; }
            set { policy_no = value; }
        }
        public string Cover_type
        {
            get { return cover_type; }
            set { cover_type = value; }
        }
        public string Business_type
        {
            get { return business_type; }
            set { business_type = value; }
        }
        public string Voucher_status
        {
            get { return voucher_status; }
            set { voucher_status = value; }
        }

        private List<ReligarePassengers> religarePassengers;
        public List<ReligarePassengers> ReligarePassengers
        {
            get { return religarePassengers; }
            set { religarePassengers = value; }
        }

        private List<ReligarePaxQuestions> religarePaxQuestions;
        public List<ReligarePaxQuestions> ReligarePaxQuestions
        {
            get { return religarePaxQuestions; }
            set { religarePaxQuestions = value; }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Getting the city Codes using pincode.
        /// </summary>
        /// <param name="pincode"></param>
        /// <returns></returns>
        public static DataTable GetCityCodes(string pincode)
        {
            DataTable dt = new DataTable();
            try
            {                
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@pincode", pincode);
                dt = DBGateway.FillDataTableSP(ReligareSPNames.GetReligareCityStateCodes, paramList);               
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare city codes:" + ex.ToString(), "");
                throw ex;
            }
            return dt;
        }
        public void Save()
        {
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;                 

                SqlParameter[] paramList = new SqlParameter[48];
                paramList[0] = new SqlParameter("@header_id", Header_id);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@Produ_type", Produ_type);
                paramList[2] = new SqlParameter("@Produ_id", Produ_id);
                paramList[3] = new SqlParameter("@ProductName", productName);
                paramList[4] = new SqlParameter("@Startdate", Startdate);
                paramList[5] = new SqlParameter("@endDate", EndDate);
                paramList[6] = new SqlParameter("@Days", Days);
                paramList[7] = new SqlParameter("@isPED", IsPED);  
                paramList[8] = new SqlParameter("@policy_no", Policy_no);
                paramList[9] = new SqlParameter("@cover_type", Cover_type);
                paramList[10] = new SqlParameter("@business_type", business_type);
                paramList[11] = new SqlParameter("@trip_from_india", Trip_from_india);
                paramList[12] = new SqlParameter("@voucher_status", Voucher_status);
                paramList[13] = new SqlParameter("@NoofPax", NoofPax);
                paramList[14] = new SqlParameter("@SumInsured_optioncode", SumInsured_optioncode);
                paramList[15] = new SqlParameter("@SumInsured", SumInsured);
                paramList[16] = new SqlParameter("@premium_Amount", Premium_Amount);
                paramList[17] = new SqlParameter("@AddFee", AddFee);
                paramList[18] = new SqlParameter("@Markup", Markup);
                paramList[19] = new SqlParameter("@MarkupValue", MarkupValue);
                paramList[20] = new SqlParameter("@MarkupType", MarkupType);
                paramList[21] = new SqlParameter("@Discount", Discount);
                paramList[22] = new SqlParameter("@DiscountValue", DiscountValue);
                paramList[23] = new SqlParameter("@DiscountType", DiscountType);
                paramList[24] = new SqlParameter("@SourceCurrency", SourceCurrency);
                paramList[25] = new SqlParameter("@SourceAmount", sourceAmount);
                paramList[26] = new SqlParameter("@source_discount", Source_discount);
                paramList[27] = new SqlParameter("@ROE", ROE);  
                paramList[28] = new SqlParameter("@B2CMarkup", B2CMarkup);
                paramList[29] = new SqlParameter("@B2CMarkupValue", B2CMarkupValue);
                paramList[30] = new SqlParameter("@B2CMarkupType", B2CMarkupType);
                paramList[31] = new SqlParameter("@isAccounted", IsAccounted);
                paramList[32] = new SqlParameter("@currency", Currency);
                paramList[33] = new SqlParameter("@trans_type", Trans_type);
                paramList[34] = new SqlParameter("@agency_ref", Agency_ref);
                paramList[35] = new SqlParameter("@agent_id", Agent_id);
                paramList[36] = new SqlParameter("@location_id", Location_id);
                paramList[37] = new SqlParameter("@status", Status); 
                paramList[38] = new SqlParameter("@cancel_datetime", Cancel_datetime);
                paramList[39] = new SqlParameter("@created_by", Created_by);
                paramList[40] = new SqlParameter("@created_on", Created_on);
                paramList[41] = new SqlParameter("@modified_by", Modified_by);
                paramList[42] = new SqlParameter("@modified_on", Modified_on);
                paramList[43] = new SqlParameter("@remarks", Remarks);
                //Added by Hari Malla 07-02-2019 for Religare Handling Fee.
                paramList[44] = new SqlParameter("@HandlingFee", HandlingFee);
                paramList[45] = new SqlParameter("@HandlingFeeAmount", HandlingFeeValue);
                paramList[46] = new SqlParameter("@HandlingFeeType", HandlingFeeType);
                paramList[47] = new SqlParameter("@GstTotal", GstValue);               

                //Saving the passenger information into bke_religare_Header
                int res = DBGateway.ExecuteNonQuery(ReligareSPNames.InsertReligarHeader, paramList);
                Header_id = Convert.ToInt32(Utility.ToString(paramList[0].Value));                
                int counter = 0;
                //Checking passenger relation SELF Existing or not.
                bool self_status = false;
                List<ReligarePassengers> self_record = (from s in religarePassengers
                                                        where s.Relation == "SELF"
                                                        select s).ToList();
                self_status = self_record.Count > 0 ? true : false;
                //Saving the passenger information into bke_religare_pax_details
                for (int i = noofPax; i >= 0; i--)
                {
                    ReligarePassengers passenger = (ReligarePassengers)ReligarePassengers[i];
                    passenger.Relation = string.IsNullOrEmpty(passenger.Relation) ? "Proposer" : passenger.Relation;
                    int paxId = passenger.save(cmd, Header_id);
                    List<ReligarePaxQuestions> questions = (from pax in ReligarePaxQuestions
                                                            where pax.Pax_id == passenger.Pax_id || pax.Question_category == "yesno"
                                                            select pax).ToList();
                    if (counter == noofPax)
                    {
                        var selfQues = (from pax in ReligarePassengers where pax.Relation == "SELF" select pax).ToList();
                        int id = 0;
                        foreach (ReligarePassengers p in selfQues)
                            id = p.Pax_id;
                        questions = (from pax in ReligarePaxQuestions
                                     where pax.Pax_id == id || pax.Question_category == "yesno"
                                     select pax).ToList();
                    }
                    bool status = false;
                    int c = 0;
                    foreach (ReligarePaxQuestions paxques in questions)
                    {
                        if ((paxques.Question_other_remarks == "CB" || paxques.Question_set_code == "PEDotherDetailsTravel") && paxques.Question_status == true)
                            status = true;
                        else if (paxques.Question_code.StartsWith("PR"))
                        {
                            if (string.IsNullOrEmpty(paxques.Question_other_remarks))
                            {
                                questions[c - 1].Question_status = false;
                                paxques.Question_status = false;
                            }
                            else
                            {
                                questions[c - 1].Question_status = true;
                                paxques.Question_status = true;
                            }
                        }
                        c++;
                    }
                    questions[0].Question_status = status ? true : false;
                    foreach (ReligarePaxQuestions paxques in questions)
                    {
                        long qid = paxques.save(cmd, Header_id, paxId, Produ_type);
                        paxques.Pax_id = paxId;
                    }
                    passenger.Pax_id = paxId;
                    counter++;
                    if (self_status && i == 1)
                        break;
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Audit.Add(EventType.Exception, Severity.High, 0, "Religare Header failed to . Error:" + ex.ToString(), "");
                throw ex;
            }
        }
        /// <summary>
        /// getting the header id using invoice number.
        /// </summary>
        /// <param name="InvoiceNo"></param>
        /// <returns></returns>
        //public static int GetHeaderId(int InvoiceNo)
        //{
        //    try
        //    {
        //        if (InvoiceNo <= 0)
        //        {
        //            throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + InvoiceNo);
        //        }
        //        SqlCommand cmd = new SqlCommand(ReligareSPNames.GetHeaderId, DBGateway.CreateConnection());
        //        SqlParameter[] paramList = new SqlParameter[1];
        //        paramList[0] = new SqlParameter("@invoiceNo", InvoiceNo);
        //        cmd.Parameters.AddRange(paramList);
        //        int invoice = Convert.ToInt32(DBGateway.FillDataTableSP(ReligareSPNames.GetHeaderId, paramList).Rows[0]["invoiceNumber"]);
        //            //Convert.ToInt32(cmd.ExecuteScalar());
        //        return invoice;
        //    }
        //    catch  (Exception ex)
        //    {
        //        Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the invoice NO:" + ex.ToString(), "");
        //        throw ex;
        //    }            
        //}
        /// <summary>
        /// getting the header details using header id.
        /// </summary>
        /// <param name="headerId"></param>
        /// <returns></returns>
        public DataSet GetHeaderDetails(int headerId)
        {
            try
            {
                if (headerId <= 0)
                {
                    throw new ArgumentException("header id should be positive integer headerId=" + headerId);
                }               
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@header_id", headerId);
                DataSet dsHeader = DBGateway.FillSP(ReligareSPNames.GetReligareHeaderDetails, paramList);
                return dsHeader;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the Details:" + ex.ToString(), "");
                throw ex;
            }            
        }

        public void Load(int headerId)
        {
            try
            {
                if (headerId <= 0)
                {
                    throw new ArgumentException("Header Id should be positive integer HeaderID=" + headerId);
                }                 
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@header_id", headerId);
                using (DataTable dtHeader = DBGateway.FillDataTableSP(ReligareSPNames.GetReligareHeaderDetails, paramList))
                {
                    if (dtHeader != null && dtHeader.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtHeader.Rows)
                        {
                            header_id = Convert.ToInt32(dr["header_id"]);
                            produ_type = Convert.ToString(dr["Produ_type"]);
                            Produ_id = Convert.ToString(dr["Produ_id"]);
                            ProductName = Convert.ToString(dr["ProductName"]);
                            Startdate = Convert.ToDateTime(dr["Startdate"]);
                            endDate = Convert.ToDateTime(dr["endDate"]);
                            policy_no = Convert.ToString(dr["policy_no"]);
                            premium_Amount = dr["premium_Amount"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["premium_Amount"]);
                            currency = Convert.ToString(dr["currency"]);
                            agency_ref = Convert.ToString(dr["agency_ref"]);
                            agent_id = Convert.ToInt32(dr["agent_id"]);
                            created_on = Convert.ToDateTime(dr["created_on"]);
                            status = Convert.ToInt32(dr["status"]);
                            Markup = dr["Markup"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["Markup"]);
                            MarkupValue = dr["MarkupValue"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["MarkupValue"]);
                            MarkupType = Convert.ToString(dr["MarkupType"]);
                            Discount = dr["Discount"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["Discount"]);
                            DiscountValue = dr["DiscountValue"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["DiscountValue"]);
                            DiscountType = Convert.ToString(dr["DiscountType"]);
                            B2CMarkup = dr["B2CMarkup"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["B2CMarkup"]);
                            B2CMarkupValue = dr["B2CMarkupValue"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["B2CMarkupValue"]);
                            B2CMarkupType = Convert.ToString(dr["B2CMarkupType"]);
                            trans_type = Convert.ToString(dr["trans_type"]);
                            created_by = Convert.ToInt32(dr["created_by"]);
                            HandlingFee = dr["HandlingFee"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["HandlingFee"]);
                            GstValue  = dr["GstTotal"] ==DBNull.Value ? 0 : Convert.ToDecimal(dr["GstTotal"]);
                                 
                            //SourceCurrency = Convert.ToString(dr["SourceCurrency"]);
                            //SourceAmount = Convert.ToDecimal(dr["SourceAmount"]);
                            //source_discount = Convert.ToDecimal(dr["source_discount"]);
                            //ROE = Convert.ToDecimal(dr["ROE"]);
                            //isAccounted = Convert.ToBoolean(dr["isAccounted"]);
                            //location_id = Convert.ToInt32(dr["location_id"]);                        
                            //cancel_datetime = Convert.ToDateTime(dr["cancel_datetime"]);
                            //modified_by = Convert.ToInt32(dr["modified_by"]);
                            //modified_on = Convert.ToDateTime(dr["modified_on"]);
                            //remarks = Convert.ToString(dr["remarks"]);
                            // Days = Convert.ToInt32(dr["Days"]);
                            //  isPED = Convert.ToBoolean(dr["isPED"]);
                            //cover_type = Convert.ToString(dr["cover_type"]);
                            //business_type = Convert.ToString(dr["business_type"]);
                            //trip_from_india = Convert.ToBoolean(dr["trip_from_india"]);
                            //voucher_status = Convert.ToString(dr["voucher_status"]);
                            //NoofPax = Convert.ToInt32(dr["NoofPax"]);
                            //SumInsured_optioncode = Convert.ToString(dr["SumInsured_optioncode"]);
                            //SumInsured = Convert.ToString(dr["SumInsured"]);
                            //AddFee = Convert.ToDecimal(dr["AddFee"]);
                        }
                        ReligarePassengers passengers = new ReligarePassengers();
                        religarePassengers = passengers.Load(dtHeader);
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to loading the header Details:" + ex.ToString(), "");
                throw ex;
            }
        }
        //in queue plans deisplaying
        public static DataTable GetInsurancePlans(int insId)
        {
            DataTable dt = null;
            try
            {                
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_insId", insId);
                dt = DBGateway.FillDataTableSP(ReligareSPNames.GetInsurancePlanList, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare product(plans) list:" + ex.ToString(), "");
            }
            return dt;
            
         }
        //  public static DataTable GetHeaderQueue(DateTime Startdate, DateTime EndDate, string TransType, string USER_TYPE, long LOCATION_ID, string AGENT_TYPE, long USER_ID, string PolicyNo,int agencyId)
        public static DataTable GetHeaderQueue(DateTime departureDate, DateTime returnDate, DateTime purchaseDateFrom, DateTime purchaseDateTo, string policyNo, int agencyId, string pnrNo, string origin, string destination, string memberType, decimal locationId, long loginUserId, string agentType, string transType)
        {
            try
            {                 
                SqlParameter[] paramList = new SqlParameter[14];
                if (departureDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[0] = new SqlParameter("@P_DepartureDate", departureDate);
                if (returnDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[1] = new SqlParameter("@P_ReturnDate", returnDate);
                if (purchaseDateFrom != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[2] = new SqlParameter("@P_PurchaseDate_From", purchaseDateFrom);
                if (purchaseDateTo != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[3] = new SqlParameter("@P_PurchaseDate_To", purchaseDateTo);
                paramList[4] = new SqlParameter("@P_PolicyNo", policyNo);
                if (agencyId > 0) paramList[5] = new SqlParameter("@P_AgencyId", agencyId);
                if (!string.IsNullOrEmpty(pnrNo)) paramList[6] = new SqlParameter("@P_PNRno", pnrNo);
                if (!string.IsNullOrEmpty(origin)) paramList[7] = new SqlParameter("@P_DepartureStationCode", origin);
                if (!string.IsNullOrEmpty(destination)) paramList[8] = new SqlParameter("@P_ArrivalStationCode", destination);
                paramList[9] = new SqlParameter("@P_USER_TYPE", memberType);
                paramList[10] = new SqlParameter("@P_LOCATION_ID", locationId);
                paramList[11] = new SqlParameter("@P_USER_ID", loginUserId);
                paramList[12] = new SqlParameter("@P_AGENT_TYPE", agentType);
                //Added Param 19/08/2015 by Chandan Sharma
                paramList[13] = new SqlParameter("@P_TransType", transType);
                DataTable dt_Header = DBGateway.FillDataTableSP(ReligareSPNames.GetReligareHeaderQueue, paramList);
                return dt_Header;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare queue list:" + ex.ToString(), "");
                throw ex;
            }
        }
        public static DataTable GetInsuranceChangeRequestQueue(DateTime departureDate, DateTime returnDate, DateTime purchaseDateFrom, DateTime purchaseDateTo, string policyNo, int agencyId, string pnrNo, string origin, string destination, string memberType, decimal locationId, long loginUserId)
        {
            try
            {               
                SqlParameter[] paramList = new SqlParameter[12];
                if (departureDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[0] = new SqlParameter("@P_DepartureDate", departureDate);
                if (returnDate != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[1] = new SqlParameter("@P_ReturnDate", returnDate);
                if (purchaseDateFrom != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[2] = new SqlParameter("@P_PurchaseDate_From", purchaseDateFrom);
                if (purchaseDateTo != Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"))) paramList[3] = new SqlParameter("@P_PurchaseDate_To", purchaseDateTo);
                if (!string.IsNullOrEmpty(policyNo)) paramList[4] = new SqlParameter("@P_PolicyNo", policyNo);
                if (agencyId > 0) paramList[5] = new SqlParameter("@P_AgencyId", agencyId);
                if (!string.IsNullOrEmpty(pnrNo)) paramList[6] = new SqlParameter("@P_PNRno", pnrNo);
                if (!string.IsNullOrEmpty(origin)) paramList[7] = new SqlParameter("@P_Origin", origin);
                if (!string.IsNullOrEmpty(destination)) paramList[8] = new SqlParameter("@P_Destination", destination);
                paramList[9] = new SqlParameter("@P_USER_TYPE", memberType);
                paramList[10] = new SqlParameter("@P_LOCATION_ID", locationId);
                paramList[11] = new SqlParameter("@P_USER_ID", loginUserId);
                DataTable dt_Header = DBGateway.FillDataTableSP(ReligareSPNames.GetReligareInsuranceChangeRequestList, paramList);
                return dt_Header;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting the religare change requests:" + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// updating policy No and vocher status.
        /// </summary>
        public void updateStatus()
        {
            try
            {               
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@header_id", Header_id);
                paramList[1] = new SqlParameter("@policy_no", Policy_no);
                paramList[2] = new SqlParameter("@status", Status);
                paramList[3] = new SqlParameter("@agency_ref", agency_ref); // Added by Hari Malla 07-02-2019  for Saving The proposal No
                int status = DBGateway.ExecuteNonQuerySP(ReligareSPNames.UpdateReligareHeaderStatus, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to Updating the header status" + ex.ToString(), "");
                throw ex;
            }
        }
        /// <summary>
        /// getting the headerid using policy No. 
        /// </summary>
        /// <param name="policyNo"></param>
        /// <returns></returns>
        public static string Get_Header(string policyNo)
        {
            try
            {             
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@policy_no", policyNo);                
                string HeaderId = Convert.ToString(DBGateway.FillDataTableSP(ReligareSPNames.GetReligareHeaderId, paramList).Rows[0]["header_id"]);
                return HeaderId; 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get The headerId" + ex.ToString(), "");
                throw ex;
            }
        }
        public DataTable GetInvoiceDetails(int headerId)
        {
            try
            {                
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@transxHeaderId", headerId);
                DataTable dt_Header = DBGateway.FillDataTableSP(ReligareSPNames.GetInvoiceNoBytranxHeaderId, paramList);
                return dt_Header;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to getting The Invoice No" + ex.ToString(), "");
                throw ex;
            }
        }
        #endregion         
    }
    public enum  ReligareProducts
    {
        Explore = 1,
        StudentExplore = 2
    }
    public class ReligarePassengers
    {
        #region private Properties
        int pax_id;
        int header_id;
        string title;
        string firstName;
        string lastName;
        string gender;
        string nationality;
        string passportNo;
        string address1;
        string address2;
        string email;
        string mobileno;
        string pinCode;
        string city;
        string state;
        string nominee_name;
        string relation;
        string policy_url_link;
        string residenceProof;
        string proofDetails;
        string purposeVisit;
        DateTime? dob;
        int travellingAgeId;
        string instituteName;
        string courseName;
        string instituteAddress;
        string instituteCountry;
        string sponserName;
        DateTime sponserDob;
        string nomineeRelation;
        string sponserRelation;

        #endregion
        #region public Properties
        public string NomineeRelation
        {
            get { return nomineeRelation; }
            set { nomineeRelation = value; }
        }
        public string InstituteName
        {
            get { return instituteName; }
            set { instituteName = value; }
        }
        public string CourseName
        {
            get { return courseName; }
            set { courseName = value; }
        }
        public string InstituteAddress
        {
            get { return instituteAddress; }
            set { instituteAddress = value; }
        }
        public string InstituteCountry
        {
            get { return instituteCountry; }
            set { instituteCountry = value; }
        }
        public string SponserName
        {
            get { return sponserName; }
            set { sponserName = value; }
        }
        public string SponserRelation
        {
            get { return sponserRelation; }
            set { sponserRelation = value; }
        }


        public DateTime SponserDob
        {
            get { return sponserDob; }
            set { sponserDob = value; }
        }
        public DateTime? Dob
        {
            get { return dob; }
            set { dob = value; }
        }
        public string Policy_url_link
        {
            get { return policy_url_link; }
            set { policy_url_link = value; }
        }
        public string Relation
        {
            get { return relation; }
            set { relation = value; }
        }
        public string Nominee_name
        {
            get { return nominee_name; }
            set { nominee_name = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string PinCode
        {
            get { return pinCode; }
            set { pinCode = value; }
        }
        public string Mobileno
        {
            get { return mobileno; }
            set { mobileno = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Address1
        {
            get { return address1; }
            set { address1 = value; }
        }
        public string Address2
        {
            get { return address2; }
            set { address2 = value; }
        }
        public string PassportNo
        {
            get { return passportNo; }
            set { passportNo = value; }
        }
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string ResidenceProof
        {
            get { return residenceProof; }
            set { residenceProof = value; }
        }
        public string ProofDetails
        {
            get { return proofDetails; }
            set { proofDetails = value; }
        }
        public string PurposeVisit
        {
            get { return purposeVisit; }
            set { purposeVisit = value; }
        }
        public int Header_id
        {
            get { return header_id; }
            set { header_id = value; }
        }
        public int Pax_id
        {
            get { return pax_id; }
            set { pax_id = value; }
        }
        public int TravellingAgeId
        {
            get { return travellingAgeId; }
            set { travellingAgeId = value; }
        }
        #endregion
        #region Methods
        public int save(SqlCommand cmd, int HeaderId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[22];
                paramList[0] = new SqlParameter("@header_id", HeaderId);
                paramList[1] = new SqlParameter("@title", Title);
                paramList[2] = new SqlParameter("@firstName", FirstName);
                paramList[3] = new SqlParameter("@lastName", LastName);
                paramList[4] = new SqlParameter("@DOB", Dob);
                paramList[5] = new SqlParameter("@gender", Gender);
                paramList[6] = new SqlParameter("@nationality", Nationality);
                paramList[7] = new SqlParameter("@passportNo", PassportNo);
                paramList[8] = new SqlParameter("@Address1", Address1);
                paramList[9] = new SqlParameter("@Address2", Address2);
                paramList[10] = new SqlParameter("@email", Email);
                paramList[11] = new SqlParameter("@mobileno", Mobileno);
                paramList[12] = new SqlParameter("@pinCode", PinCode);
                paramList[13] = new SqlParameter("@city", City);
                paramList[14] = new SqlParameter("@state", State);
                paramList[15] = new SqlParameter("@nominee_name", Nominee_name);
                paramList[16] = new SqlParameter("@Relation", Relation);
                paramList[17] = new SqlParameter("@policy_url_link", Policy_url_link);
                paramList[18] = new SqlParameter("@puposeOfVisit", PurposeVisit);
                paramList[19] = new SqlParameter("@residenceProof", ResidenceProof);
                paramList[20] = new SqlParameter("@proofDetails", ProofDetails);
                paramList[21] = new SqlParameter("@pax_id", Pax_id);
                paramList[21].Direction = ParameterDirection.Output;
                int res = DBGateway.ExecuteNonQuery(ReligareSPNames.InsertPassengerDetails, paramList);
                int paxId = Convert.ToInt32(Utility.ToString(paramList[21].Value));
                return paxId;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to inserting The insured Details" + ex.ToString(), "");
                throw ex;
            }            
        }
        public List<ReligarePassengers> Load(DataTable dtHeader)
        {
            List<ReligarePassengers> passengers = new List<ReligarePassengers>();
            try
            {
                foreach (DataRow dr in dtHeader.Rows)
                {
                    ReligarePassengers pax = new ReligarePassengers();
                    pax.firstName =Utility.ToString(dr["firstName"]);
                    pax.lastName = Utility.ToString(dr["lastName"]);
                    pax.passportNo = Utility.ToString(dr["passportNo"]);
                    pax.mobileno = Utility.ToString(dr["mobileno"]);
                    pax.city = Utility.ToString(dr["city"]);
                    pax.State = Utility.ToString(dr["state"]);
                    pax.email = Utility.ToString(dr["email"]);
                    pax.dob = Convert.ToDateTime(dr["DOB"]);
                    pax.relation = Utility.ToString(dr["Relation"]);
                    pax.residenceProof = Convert.ToString(dr["residenceProof"]);
                    pax.proofDetails = Convert.ToString(dr["proofDetails"]);
                    pax.gender = Utility.ToString(dr["gender"]);
                    passengers.Add(pax);
                }
                return passengers;
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to binding The Insured Details" + ex.ToString(), "");
                throw ex;
            }           
        }
        #endregion
    }
    public class ReligarePaxQuestions
    {
        #region private Properties
        string question_id;
        int header_id;
        int pax_id;
        string question_set_code;
        string question_code;
        string question_description;
        string question_category;
        bool question_status;
        string question_other_remarks;
        string productTypeId;
        #endregion
        #region public properies
        public string ProductTypeId
        {
            get { return productTypeId; }
            set { productTypeId = value; }
        }
        public string Question_id
        {
            get { return question_id; }
            set { question_id = value; }
        }
        public int Header_id
        {
            get { return header_id; }
            set { header_id = value; }
        }
        public int Pax_id
        {
            get { return pax_id; }
            set { pax_id = value; }
        }
        public string Question_set_code
        {
            get { return question_set_code; }
            set { question_set_code = value; }
        }
        public string Question_description
        {
            get { return question_description; }
            set { question_description = value; }
        }
        public string Question_category
        {
            get { return question_category; }
            set { question_category = value; }
        }
        public string Question_code
        {
            get { return question_code; }
            set { question_code = value; }
        }
        public bool Question_status
        {
            get { return question_status; }
            set { question_status = value; }
        }
        public string Question_other_remarks
        {
            get { return question_other_remarks; }
            set { question_other_remarks = value; }
        }
        #endregion
        #region Methods
        public long save(SqlCommand cmd, int HeaderId, int pId, string Produ_type)
        {
            try
            {
                int status = 0;
                if (Question_status == false)
                    status = 1;
                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@header_id", HeaderId);
                paramList[1] = new SqlParameter("@pax_id", pId);
                paramList[2] = new SqlParameter("@question_set_code", Question_set_code);
                paramList[3] = new SqlParameter("@question_code", Question_code);
                paramList[4] = new SqlParameter("@question_description", Question_description);
                paramList[5] = new SqlParameter("@question_category", Question_category);
                paramList[6] = new SqlParameter("@question_status", status);
                paramList[7] = new SqlParameter("@question_other_remarks", Question_other_remarks);
                paramList[8] = new SqlParameter("@questionID", Convert.ToInt64(Question_id));
                paramList[9] = new SqlParameter("@Produ_type", Convert.ToInt32(Produ_type));
                paramList[8].Direction = ParameterDirection.Output;
                int res = DBGateway.ExecuteNonQuery(ReligareSPNames.InsertQuestionaryDetails, paramList);
                long QId = Convert.ToInt64(paramList[8].Value);
                return QId;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to inserting The questionary" + ex.ToString(),"");
                throw ex;
            }           
        }
        #endregion
    }
}

