﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;


namespace ReligareInsurance
{
   public class SumInsured
    {
        #region private members
        private const long NEW_RECORD = -1;
        int _sum_insured_id;
        int _product_type_id;
        int _product_id;
        string _range_id;
        string _range_description;
        string _cover_tyep;
        long _created_by;
        long _id;
        #endregion
        #region public properties
        public int SUM_INSURED_ID
        {
            get { return _sum_insured_id; }
            set { _sum_insured_id = value; }
        }
        public int PRODUCT_TYPE_ID
        {
            get { return _product_type_id; }
            set { _product_type_id = value; }
        }
        public int PRODUCT_ID
        {
            get { return _product_id; }
            set { _product_id = value; }
        }
        public string RANGE_ID
        {
            get { return _range_id; }
            set { _range_id = value; }
        }
        public string RANGE_DESCRIPTION
        {
            get { return _range_description; }
            set { _range_description = value; }
        }
        public string COVER_TYPE
        {
            get { return _cover_tyep; }
            set { _cover_tyep = value; }
        }
        public long ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        #endregion

        #region Constructors
        public SumInsured()
        {
            _id = NEW_RECORD;
        }
        public SumInsured(long id)
        {


            _id = id;
            GetDetails(id);
        }

        #endregion
        #region Methods
        void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
       private  DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@suminsuredid", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_ReligareSumInsuredMaster_getdata", paramList);
                return dsResult;
            }
            catch(Exception ex)
            {
                throw ex;
                //Audit.Add(EventType.InsuranceBooking, Severity.High, 0, ex.ToString(), "master");
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["SUM_INSURED_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch
            { throw; }

        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["SUM_INSURED_ID"]);
                _product_type_id = Utility.ToInteger(dr["PRODUCT_TYPE_ID"]);
                _product_id= Utility.ToInteger(dr["PRODUCT_ID"]);
                _range_id = Utility.ToString(dr["RANGE_ID"]);
                _range_description = Utility.ToString(dr["RANGE_DESCRIPTION"]);
                _cover_tyep = Utility.ToString(dr["COVER_TYPE"]);
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetList(int productid)
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[1];
                paramlist[0] = new SqlParameter("@product_type_id", productid);
                return DBGateway.ExecuteQuery("usp_ReligareProductIdDetails_Getlist",paramlist).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public void Save()
        {
            try
            {
                SqlParameter[] productlist = new SqlParameter[9];
                productlist[0] = new SqlParameter("@suninsuredid", _id);
                productlist[1] = new SqlParameter("@product_type_id", _product_type_id);
                productlist[2] = new SqlParameter("@productid", _product_id);
                productlist[3] = new SqlParameter("@rangeid", _range_id);
                productlist[4] = new SqlParameter("@rangedescription", _range_description);
                productlist[5] = new SqlParameter("@cover_type", _cover_tyep);
                productlist[6] = new SqlParameter("@created_by", _created_by);
                productlist[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                productlist[7].Direction = ParameterDirection.Output;
                productlist[8] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                productlist[8].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_ReligareSumInsuredMaster_add_update", productlist);
                string messageType = Utility.ToString(productlist[7].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(productlist[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_ReligareSumInsuredMaster_Details").Tables[0];
            }
            catch
            {
                throw;
            }

        }
        #endregion
    }
}
