﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
namespace ReligareInsurance
{
    public class ReligareDiscount
    {
        #region private properties
        private const long NEW_RECORD = -1;
        int _discountid;
        int _discount_member_id;
        decimal _discount_percentage;
        string _discount_remarks;
        long _created_by;
        
        long _id;
        #endregion
        #region Public properties
        public int DISCOUNTID
        { get { return _discountid;}
        set { _discountid = value; } }
        public int DISCOUNT_MEMBER_ID
        {
            get { return _discount_member_id; }
            set { _discount_member_id = value; }
        }
        public decimal DISCOUNT_PERCENTAGE
        {
            get { return _discount_percentage; }
            set { _discount_percentage = value; }
        }
        public string DISCOUNT_REMARKS
        {
            get { return _discount_remarks; }
            set { _discount_remarks = value; }
        }
        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        public long ID
        {
            get { return _id; }
            set { _id = value; }
        }
        

        #endregion

        #region Constructors
        public ReligareDiscount()
        {
            _id = NEW_RECORD;
        }
        public ReligareDiscount(long id)
        {
            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_ReligareDiscountMaster_Details").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void Save()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@discountid", _id);
                parameters[1] = new SqlParameter("@discount_memberid", _discount_member_id);
                parameters[2] = new SqlParameter("@discount_percentage", _discount_percentage);
                parameters[3] = new SqlParameter("@discount_remarks", _discount_remarks);
                parameters[4] = new SqlParameter("@created_by", _created_by);
                parameters[5] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[5].Direction = ParameterDirection.Output;
                parameters[6] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                parameters[6].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_ReligareDiscountMaster_add_update", parameters);
                string messageType = Utility.ToString(parameters[5].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[6].Value);
                    if (message != string.Empty) throw new Exception(message);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
         DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@discountid", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_ReligareDiscountMaster_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["DISCOUNT_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["DISCOUNT_ID"]);
               _discount_member_id= Utility.ToInteger(dr["DISCOUNT_MEMBER_ID"]);
              _discount_percentage= Utility.ToDecimal(dr["DISCOUNT_PERCENTAGE"]);
              _discount_remarks= Utility.ToString(dr["DICOUNT_REMARKS"]);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
