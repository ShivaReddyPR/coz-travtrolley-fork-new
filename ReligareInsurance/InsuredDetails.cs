﻿using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReligareInsurance
{
    public class InsuredDetails
    {
        #region Private properties
        private const long NEW_RECORD = -1;
        int _sum_insured_details_id;
        int _product_Type_id;
        int _product_id;
        int _sum_insured_id;
        
        String _age_band;
        string _plan;
        int _ped;
        int _sum_insured;
        string _trip_type;
        int _members_in_age_band;
        int _term;
        decimal _premium;
        decimal _premium_roundoff;
        decimal _premium_with_gst;
        decimal _premium_roundoff_with_gst;
        long _id;
        long _created_by;

        #endregion
        #region Public properties
        public int SUM_INSURED_DETAILS_ID
        {
            get { return _sum_insured_details_id; }
            set { _sum_insured_details_id = value; }
        }
        public int PRODUCT_TYPE_ID
        {
            get { return _product_Type_id; }
            set { _product_Type_id = value; }
        }
        public int PRODUCT__ID
        {
            get { return _product_id; }
            set { _product_id = value; }
        }
        public int SUM_INSURED_ID
        {
            get { return _sum_insured_id; }
            set { _sum_insured_id = value; }
        }
       
        public string AGE_BAND
        {
            get { return _age_band; }
            set { _age_band = value; }
        }
        public string PLAN
        {
            get { return _plan; }
            set { _plan = value; }
        }
        public int PED
        {
            get { return _ped; }
            set { _ped = value; }
        }
        public int SUM_INSURED
        {
            get { return _sum_insured; }
            set { _sum_insured = value; }
        }
        public string TRIP_TYPE
        {
            get { return _trip_type; }
            set { _trip_type = value; }
        }
        public int MEMBER_IN_AGE_BAND
        {
            get { return _members_in_age_band; }
            set { _members_in_age_band = value; }
        }
        public int TERM
        {
            get { return _term; }
            set { _term = value; }
        }
        public decimal PREMIUM
        {
            get { return _premium; }
            set { _premium = value; }
        }
        public decimal PREMIUN_ROUNDOFF
        {
            get { return _premium_roundoff; }
            set { _premium_roundoff = value; }
        }
        public decimal PREMIUM_WITH_GST
        {
            get { return _premium_with_gst; }
            set { _premium_with_gst = value; }
        }
        public decimal PREMIUM_ROUNDOFF_WITH_GST
        {
            get { return _premium_roundoff_with_gst; }
            set { _premium_roundoff_with_gst = value; }
        }
        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        public long ID
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion
        #region Constructors
        public InsuredDetails()
        {
            _id = NEW_RECORD;
        }
        public InsuredDetails(long id)
        {


            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods
        void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
         DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@sum_insured_detail_id", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_ReligareSumInsuredDetails_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["SUM_INSURED_DETAIL_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["SUM_INSURED_DETAIL_ID"]);
                _product_Type_id =Utility.ToInteger(dr["PRODUCT_TYPE_ID"]);
                _product_id=Utility.ToInteger(dr["PRODUCT_ID"]);
                _sum_insured_id = Utility.ToInteger(dr["SUM_INSURED_ID"]);
                _age_band = Utility.ToString(dr["AGE_BAND"]);
                _plan = Utility.ToString(dr["PLANS"]);
                _ped = Utility.ToInteger(dr["PED"]);
                _sum_insured = Utility.ToInteger(dr["SUM_INSURED"]);
                _trip_type = Utility.ToString(dr["TRIP_TYPE"]);
                _members_in_age_band = Utility.ToInteger(dr["MEMBERS_IN_AGE_BAND"]);
                _term = Utility.ToInteger(dr["TERM"]);
                _premium = Utility.ToDecimal(dr["PREMIUM"]);
                _premium_roundoff = Utility.ToDecimal(dr["PREMIUM_ROUNDOFF"]);
                _premium_with_gst = Utility.ToDecimal(dr["PREMIUM_WITH_GST"]);
                _premium_roundoff_with_gst = Utility.ToDecimal(dr["PREMIUM_ROUNDOFF_WITH_GST"]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetList()
        {
            try
            {
            
                return DBGateway.ExecuteQuery("usp_ReligareSumInsuredDetails_Getlist").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetListProductTypeDetailsId(int productTypeid,int productid)
        {
            try
            {
                SqlParameter[] paramlist = new SqlParameter[2];
                paramlist[0] = new SqlParameter("@product_type_id", productTypeid);
                paramlist[1] = new SqlParameter("@product_id", productid);
                return DBGateway.ExecuteQuery("usp_ReligareSumInsuredProductIdDetails_Getlist", paramlist).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_ReligareSumInsuredDetails_Details").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void Save()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[18];
                parameters[0] = new SqlParameter("@sum_insured_detail_id", _id);
                parameters[1] = new SqlParameter("@product_Type_Id", _product_Type_id);
                parameters[2] = new SqlParameter("@prodcut_id", _product_id);
                parameters[3] = new SqlParameter("@sum_insured_id", _sum_insured_id);
                parameters[4] = new SqlParameter("@age_band", _age_band);
                parameters[5] = new SqlParameter("@plan", _plan);
                parameters[6] = new SqlParameter("@ped", _ped);
                parameters[7] = new SqlParameter("@sum_insured", _sum_insured);
                parameters[8] = new SqlParameter("@trip_type", _trip_type);
                parameters[9] = new SqlParameter("@member_in_age_band", _members_in_age_band);
                parameters[10] = new SqlParameter("@term", _term);
                parameters[11] = new SqlParameter("@premium", _premium);
                parameters[12] = new SqlParameter("@premium_roundoff", _premium_roundoff);
                parameters[13] = new SqlParameter("@primium_with_gst", _premium_with_gst);
                parameters[14] = new SqlParameter("@premium_roundoff_with_gst", _premium_roundoff_with_gst);
                parameters[15] = new SqlParameter("@created_by", _created_by);
                parameters[16] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[16].Direction = ParameterDirection.Output;
                parameters[17] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                parameters[17].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_ReligareSumInsuredDetails_add_update", parameters);
                string messageType = Utility.ToString(parameters[16].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[17].Value);
                    if (message != string.Empty) throw new Exception(message);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
