﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

namespace ReligareInsurance
{
   public class QuestionMaster
    {
        #region private members
        private const long NEW_RECORD = -1;

        int _questionid;
        string _question_set_code;
        int _productId;
        string _question_code;
        string _question_description;
        long _created_by;
        long _id;
        string _question_type;
        string _status;
        #endregion

        #region Public Properties
        public int QUESTION_ID
        { get { return _questionid; }
          set { _questionid = value; }
        }
        public string QUESTION_SET_CODE
        {
            get { return _question_set_code; }
            set { _question_set_code = value; }
        }
        public string QUESTIONCODE
        { get { return _question_code; }
            set { _question_code = value; }
        }
        public string QUESTION_DESCRIPTION
        { get { return _question_description; }
            set { _question_description = value; }
        }
       
        public long CREATED_BY
        {
            get { return _created_by; }
            set { _created_by = value; }
        }
        public int PRODUCTID
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public long ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public string QUESTION_TYPE
        {
            get { return _question_type; }
            set { _question_type = value; }
        }
        public string STATUS
        {
            get { return _status; }
            set { _status = value; }
        }

        #endregion
        #region Constructors
        public QuestionMaster()
        {
            _id = NEW_RECORD;
        }
        public QuestionMaster(long id)
        {


            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods
        void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);

        }
         DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@quesion_Id", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_ReligareQuestionMaster_getdata", paramList);
                return dsResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["QUESTION_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
         void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["QUESTION_ID"]);
                _productId = Utility.ToInteger(dr["PRODUCT_ID"]);
                _question_set_code = Utility.ToString(dr["QUESTION_SET_CODE"]);
                _question_code = Utility.ToString(dr["QUESTION_CODE"]);
                _question_description = Utility.ToString(dr["QUESION_DESCRIPTION"]);
                _status = Utility.ToString(dr["STATUS"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save()
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[9];
                parameters[0] = new SqlParameter("@questionid", _id);
                parameters[1] = new SqlParameter("@productid", _productId);
                parameters[2] = new SqlParameter("@question_Set_Code", _question_set_code);
                parameters[3] = new SqlParameter("@question_code", _question_code);
                parameters[4] = new SqlParameter("@question_Description", _question_description);
                parameters[5] = new SqlParameter("@created_by", _created_by);
                parameters[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                parameters[6].Direction = ParameterDirection.Output;
                parameters[7] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                parameters[7].Direction = ParameterDirection.Output;
                parameters[8] = new SqlParameter("@question_type", _question_type);
                DBGateway.ExecuteNonQuerySP("usp_ReligareQuestionMaster_add_update", parameters);
                string messageType = Utility.ToString(parameters[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameters[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetAdditionalServiceMasterDetails()
        {
            try
            {
                return DBGateway.ExecuteQuery("usp_ReligareQuestionMaster_Details").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}
