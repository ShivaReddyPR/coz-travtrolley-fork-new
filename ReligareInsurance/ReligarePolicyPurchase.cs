﻿using System;
using System.Collections.Generic;
using ReligareInsurance.RelWebService;
using CT.Configuration;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Linq;
using System.Xml;
using Newtonsoft.Json;
using CT.Core;

namespace ReligareInsurance
{
    public class ReligarePolicyPurchase
    {
        string _xmlPath = string.Empty;
        string _purchaseURL = string.Empty;
        string _policyPdfUrl = string.Empty;
        string _userName = string.Empty;
        string _password = string.Empty;
        string appUserId;
        string baseAgentId;
        string baseAgentName;

        public string AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }        
        public string BaseAgentId
        {
            get { return baseAgentId; }
            set { baseAgentId = value; }
        }
        public string BaseAgentName
        {
            get { return baseAgentName; }
            set { baseAgentName = value; }
        }
       
        public ReligarePolicyPurchase()
        {
            _xmlPath = ConfigurationSystem.ReligareConfig["XmlLogPath"];
            _xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            _purchaseURL= ConfigurationSystem.ReligareConfig["purchaseURL"];
            _policyPdfUrl = ConfigurationSystem.ReligareConfig["policyURL"];
            _userName = ConfigurationSystem.ReligareConfig["username"];
            _password = ConfigurationSystem.ReligareConfig["password"];
            try
            {
                if (!System.IO.Directory.Exists(_xmlPath))
                {
                    System.IO.Directory.CreateDirectory(_xmlPath);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public object ConfirmPolicyPurchaseorg(ref ReligareHeader religareHeader)
        {
            try
            { 
                RelSymbiosysServices rs = new RelSymbiosysServices();
                IntPolicyDataIO poldataio = new IntPolicyDataIO();
                IntPolicyDO objpolicy = new IntPolicyDO();
                IntPartyDO objpartydo = new IntPartyDO();
                IntPolicyAdditionalFieldsDO objpolicyadditionalfields = new IntPolicyAdditionalFieldsDO();
                // Policy Details
                objpolicy.businessTypeCd = "NEWBUSINESS";
                objpolicy.baseProductId = religareHeader.Produ_id;
                objpolicy.baseAgentId = baseAgentId;
                objpolicy.coverType = "INDIVIDUAL";
                objpolicy.policyCommencementDt = Convert.ToDateTime(religareHeader.Startdate).ToString("dd/MM/yyyy");
                objpolicy.policyMaturityDt = Convert.ToDateTime(religareHeader.EndDate).ToString("dd/MM/yyyy");
                objpolicy.travelGeographyCd = religareHeader.Produ_id.ToString();
                objpolicy.quotationReferenceNum = DateTime.Now.ToString("yyyyMMddhhmmss");
                objpolicy.visitPurposeCd = religareHeader.VisitPurpose;
                objpolicy.isPremiumCalculation = "YES";
                objpolicy.sumInsured = religareHeader.SumInsured.ToString();
                objpolicy.premium = Convert.ToDouble(religareHeader.Premium_Amount);
                objpolicy.nomineeName = religareHeader.ReligarePassengers[0].Nominee_name;
                objpolicy.nomineeRelationship = religareHeader.ReligarePassengers[0].NomineeRelation;
                objpolicy.term = Convert.ToInt16(religareHeader.Days);
                objpolicy.maxTripPeriod = 60;
                objpolicy.tripTypeCd = religareHeader.TripType;
                objpolicy.anyPED = religareHeader.IsPED ? "YES" : "NO";
                int prodId = Convert.ToInt32(religareHeader.Produ_id);

                //Additional Information
                objpolicy.policyAdditionalFieldsDOList = new IntPolicyAdditionalFieldsDO[1];
                objpolicyadditionalfields = new IntPolicyAdditionalFieldsDO();
                objpolicyadditionalfields.fieldAgree = "YES";
                objpolicyadditionalfields.fieldAlerts = "YES";
                objpolicyadditionalfields.fieldTc = "YES";
                objpolicyadditionalfields.field20 = (religareHeader.NoofPax + 1).ToString();
                objpolicyadditionalfields.field10 = religareHeader.ReligarePassengers[0].Nominee_name;
                objpolicyadditionalfields.field12 = religareHeader.ReligarePassengers[0].NomineeRelation;
                if (Convert.ToInt32(religareHeader.Produ_type) == (int)ReligareProducts.StudentExplore)
                {
                    objpolicy.visitPurposeCd = "ST";
                    objpolicy.tripTypeCd = "SINGLE";
                    objpolicyadditionalfields.field8 = religareHeader.UniversityName;
                    objpolicyadditionalfields.sponsorName = religareHeader.SponsorName;
                    objpolicyadditionalfields.sponsorDOB = Convert.ToDateTime(religareHeader.SponsorDOB).ToString("dd/MM/yyyy");
                    objpolicyadditionalfields.relationshipToStudent = religareHeader.RelationshipToStudent;
                    objpolicyadditionalfields.courseDetails = religareHeader.CourseDetails;
                    objpolicyadditionalfields.universityAddress = religareHeader.UniversityAddress;
                }
                objpolicy.policyAdditionalFieldsDOList[0] = objpolicyadditionalfields;
                objpolicyadditionalfields.tripStart = religareHeader.Trip_from_india ? "YES" : "NO";

                string guid = ""; string passportNo = "";
                int counter = 0;
                List<IntPartyDO> intPartyDOList = new List<IntPartyDO>();
                foreach (ReligarePassengers religarePassengers in religareHeader.ReligarePassengers)
                {
                    objpartydo = new IntPartyDO();
                    IntPartyAddressDO objpartyaddr = new IntPartyAddressDO();
                    IntPartyContactDO objpartycontact = new IntPartyContactDO();
                    IntPartyEmailDO objpartyemail = new IntPartyEmailDO();
                    IntPartyIdentityDO objpartyidentity = new IntPartyIdentityDO();

                    //Personal Details
                    objpartydo.birthDt = Convert.ToDateTime(religarePassengers.Dob).ToString("dd/MM/yyyy");//"12/12/1989";
                    objpartydo.firstName = religarePassengers.FirstName.ToUpper();
                    objpartydo.genderCd = religarePassengers.Gender.ToUpper();
                    objpartydo.guid = Guid.NewGuid().ToString();
                    objpartydo.lastName = religarePassengers.LastName.ToUpper();
                    objpartydo.titleCd = religarePassengers.Title.ToUpper();

                    if (counter == 0)//religarePassengers.Pax_id == 9)
                    {
                        objpartydo.relationCd = "SELF";
                        objpartydo.roleCd = "PROPOSER";
                        guid = objpartydo.guid;
                        passportNo = religarePassengers.PassportNo;
                        counter = 1;
                    }
                    else
                    {
                        if (passportNo == religarePassengers.PassportNo)
                            objpartydo.guid = guid;

                        objpartydo.relationCd = religarePassengers.Relation;
                        objpartydo.roleCd = "PRIMARY";
                    }

                    // Contact Details
                    objpartydo.partyContactDOList = new IntPartyContactDO[1];
                    objpartycontact = new IntPartyContactDO();
                    objpartycontact.contactNum = Convert.ToInt64(religarePassengers.Mobileno);
                    objpartycontact.contactNumSpecified = true;
                    objpartycontact.contactTypeCd = "MOBILE";
                    objpartycontact.stdCode = "+91";
                    objpartydo.partyContactDOList[0] = objpartycontact;

                    //PERMANENT Address
                    objpartydo.partyAddressDOList = new IntPartyAddressDO[2];
                    objpartyaddr = new IntPartyAddressDO();
                    objpartyaddr.addressLine1Lang1 = religarePassengers.Address1.ToUpper();
                    objpartyaddr.addressLine2Lang1 = religarePassengers.Address2.ToUpper();
                    objpartyaddr.addressTypeCd = "PERMANENT";
                    objpartyaddr.areaCd = religarePassengers.City;
                    objpartyaddr.cityCd = religarePassengers.City;
                    objpartyaddr.pinCode = religarePassengers.PinCode;
                    objpartyaddr.stateCd = religarePassengers.State;
                    objpartyaddr.countryCd = "IND";
                    objpartydo.partyAddressDOList[0] = objpartyaddr;

                    //COMMUNICATION Address
                    objpartyaddr = new IntPartyAddressDO();
                    objpartyaddr.addressLine1Lang1 = religarePassengers.Address1.ToUpper();
                    objpartyaddr.addressLine2Lang1 = religarePassengers.Address2.ToUpper();
                    objpartyaddr.addressTypeCd = "COMMUNICATION";
                    objpartyaddr.areaCd = religarePassengers.City;
                    objpartyaddr.cityCd = religarePassengers.City;
                    objpartyaddr.pinCode = religarePassengers.PinCode;
                    objpartyaddr.stateCd = religarePassengers.State;
                    objpartyaddr.countryCd = "IND";
                    objpartydo.partyAddressDOList[1] = objpartyaddr;

                    // Email
                    objpartydo.partyEmailDOList = new IntPartyEmailDO[1];
                    objpartyemail.emailAddress = religarePassengers.Email;
                    objpartyemail.emailTypeCd = "PERSONAL";
                    objpartydo.partyEmailDOList[0] = objpartyemail;

                    // Identity Details
                    objpartydo.partyIdentityDOList = new IntPartyIdentityDO[1];
                    objpartyidentity = new IntPartyIdentityDO();
                    objpartyidentity.identityNum = religarePassengers.PassportNo;
                    objpartyidentity.identityTypeCd = "PASSPORT";
                    objpartydo.partyIdentityDOList[0] = objpartyidentity;

                    List<ReligarePaxQuestions> liQuestions = (from pax in religareHeader.ReligarePaxQuestions
                                                              where pax.Pax_id == religarePassengers.Pax_id || pax.Question_category == "yesno"
                                                              select pax).ToList();
                    List<IntPartyQuestionDO> liQuestionary = new List<IntPartyQuestionDO>();
                    bool status = false;
                    int count = 0;
                    foreach (ReligarePaxQuestions religarePaxQuestions in liQuestions)
                    {
                        IntPartyQuestionDO intPartyQuestionDO = new IntPartyQuestionDO();
                        intPartyQuestionDO.questionCd = religarePaxQuestions.Question_code;
                        intPartyQuestionDO.questionSetCd = religarePaxQuestions.Question_set_code;
                        if ((religarePaxQuestions.Question_other_remarks == "CB" || religarePaxQuestions.Question_set_code == "PEDotherDetailsTravel") && religarePaxQuestions.Question_status == true)
                            status = true;
                        intPartyQuestionDO.response = religarePaxQuestions.Question_status ? "YES" : "NO";
                        if (religarePaxQuestions.Question_category == "student")
                        {
                            intPartyQuestionDO.response = religarePaxQuestions.Question_other_remarks;
                        }
                        if (religarePaxQuestions.Question_set_code == "PEDotherDetailsTravel")
                        {
                            intPartyQuestionDO.response = string.IsNullOrEmpty(religarePaxQuestions.Question_other_remarks)?"NO": "YES";
                            if (intPartyQuestionDO.response == "YES")
                            {
                                liQuestionary.Add(intPartyQuestionDO);
                                intPartyQuestionDO = new IntPartyQuestionDO();
                                intPartyQuestionDO.questionCd = "otherDiseasesDescription";
                                intPartyQuestionDO.questionSetCd = religarePaxQuestions.Question_set_code;
                                intPartyQuestionDO.response = religarePaxQuestions.Question_other_remarks;
                                count++;
                            }
                        }
                        if (religarePaxQuestions.Question_code.StartsWith("PR"))
                        {
                            if (string.IsNullOrEmpty(religarePaxQuestions.Question_other_remarks))
                            {
                                liQuestionary[count - 1].response = "NO";
                                religarePaxQuestions.Question_status = false;
                            }
                            else
                            {
                                liQuestionary[count - 1].response = "YES";
                                intPartyQuestionDO.response = religarePaxQuestions.Question_other_remarks;
                                religarePaxQuestions.Question_status = true;
                            }
                        }
                        else if (religarePaxQuestions.Question_code.StartsWith("dateofBirth"))
                        {
                            intPartyQuestionDO.response = Convert.ToDateTime(religareHeader.SponsorDOB).ToString("dd/MM/yyyy");
                        }
                        else if (religarePaxQuestions.Question_code.StartsWith("relationwithInsured"))
                        {
                            intPartyQuestionDO.response = religareHeader.RelationshipToStudent;
                        }
                        liQuestionary.Add(intPartyQuestionDO);
                        count++;
                    }
                    liQuestionary[0].response = status == false ? "NO" : "YES";
                    objpartydo.partyQuestionDOList = liQuestionary.ToArray();
                    intPartyDOList.Add(objpartydo);
                    if (religarePassengers.Relation == "SELF")
                        intPartyDOList[0].partyQuestionDOList = liQuestionary.ToArray();
                }

                //Inward Details
                objpolicy.inwardDOList = new IntInwardDO[1];
                IntInwardDO intInwardDO = new IntInwardDO();

                objpolicy.partyDOList = intPartyDOList.ToArray();
                IntIO objio = new IntIO();
                objio = poldataio;
                poldataio.policy = objpolicy;
                string filePath = "";
                //Serialization Request.
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(IntPolicyDataIO));
                    filePath = _xmlPath + "PolicyPurchaseRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, poldataio);
                    sw.Close();
                }
                catch (System.Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in policy purchase request" + ex.ToString(), "");
                     
                }
                rs.Url = _purchaseURL;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);
                XmlNode node = xmlDoc.SelectSingleNode("IntPolicyDataIO");

                XmlNodeList list = node.ChildNodes;
                XmlNode tempnode = xmlDoc.CreateNode(XmlNodeType.Element, "policy", null);

                for (int j = 0; j < list.Count; j++)
                {
                    XmlNode n = (XmlNode)list[j];
                    if (n.Name == "errorLists" || n.Name == "listErrorListList" || n.Name == "headerUserID" || n.Name == "classDescription" || n.Name == "occupationClass" || n.Name == "occupationCode")
                    {
                        list[j].ParentNode.RemoveChild(list[j]);
                    }
                    if (n.Name == "policy")
                        tempnode.AppendChild(list[j].ParentNode.RemoveChild(list[j]));
                }

                string tempData = tempnode.InnerXml;//.Replace("</policy>", "<maxTripPeriod>60</maxTripPeriod></policy>");
                if (objpolicy.tripTypeCd == "MULTI")
                    tempData = tempnode.InnerXml.Replace("</policy>", "<maxTripPeriod>"+religareHeader.MaxTripPeriod+"</maxTripPeriod></policy>");
                if (Convert.ToInt32(religareHeader.Produ_type) == (int)ReligareProducts.StudentExplore)
                {
                    string addOns = "<addOns>" + religareHeader.Addons + "</addOns></policy>";
                    tempData = tempnode.InnerXml.Replace("</policy>", "<addOns>" + religareHeader.Addons + "</addOns></policy>");
                }

                StreamReader reader = new StreamReader(filePath);
                string request = reader.ReadToEnd();
                reader.Close();
                //Serialization Request.
                string Response = GetResponse(tempData, _purchaseURL, "createPolicyTravel");
                //Serialization Response.  
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.InnerXml = Response;
                    doc.Save(_xmlPath + "PolicyPurchaseResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                }
                catch (System.Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in policy purchase response" + ex.ToString(), "");
                     
                }
                return Response;
            }
            catch (System.Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in policy purchase web response" + ex.ToString(), "");
                throw ex;
            }
        }
        public object confirmDDCheque(string proposalNo)
        {
            try
            {
                 RelSymbiosysServices rs = new RelSymbiosysServices();
                ChequeDDReqResIO chequeDDReqResIO = new ChequeDDReqResIO();
                rs.Url = _purchaseURL;
                string nodes = "bankCd,insurerBankCd,bankBranch,inwardAmount,paymentMethodCd,paymentSubTypeCd,instrumentDt,micrNum,inwardNum,bankName,depositDt,paymentMode,proposalNum";
                string requestData = "";
                foreach (string str in nodes.Split(','))
                {
                    if (str == "proposalNum")
                        requestData += "<proposalNum>" + proposalNo + "</proposalNum>";
                    else if (str == "paymentMode")
                        requestData += "<paymentMode>AGENT_SUSPENSE</paymentMode>";
                    else if (str == "depositDt")
                        requestData += "<depositDt>" + DateTime.Now.ToString("dd/MM/yyyy") + "</depositDt>";
                    else
                        requestData += "<" + str + ">00</" + str + ">";
                }
                string Response = GetResponse(requestData, _purchaseURL, "doChequeDDService");
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.InnerXml = Response;
                    doc.Save(_xmlPath + "chequeDDReqResIOResponse_" + Convert.ToString(appUserId) + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                }
                catch (System.Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in policy payment details response" + ex.ToString(), "");
                    
                }
                return Response;
            }
            catch (System.Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in policy payment details response" + ex.ToString(), "");
                throw ex;
            }            
        }
        private string GetResponse(string requestData, string url, string methodName)
        {
            string responseFromServer = string.Empty;
            string responseXML = string.Empty;
            try
            {
                string xmlRequest = @"<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:rel='http://relinterface.insurance.symbiosys.c2lbiz.com' xmlns:xsd='http://intf.insurance.symbiosys.c2lbiz.com/xsd'><soap:Header /><soap:Body> <rel:" + methodName + "><rel:intIO >" + requestData + "</rel:intIO> </rel:" + methodName + "></soap:Body></soap:Envelope> ";
                string xmlData = string.Format(xmlRequest);
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                string contentType = "application/soap+xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST"; //Using POST method       
                string postData = xmlData;// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;
                Stream requestWriter = (request.GetRequestStream());
                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.InnerXml = xmlRequest;
                    doc.Save(_xmlPath + "soap" + methodName + "request" + Convert.ToString(appUserId) + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                }
                catch (System.Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured inGetting the policy Response :" + ex.ToString(), "");
                    
                }
                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "web exception occured getting the response:" + webEx.ToString(), "");
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
                throw webEx;
            }
            return responseFromServer;
        }
        /// <summary>
        /// Method for decrypting response compression i.e GZIP, Deflate, default
        /// </summary>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        public object GetPolicyPdf(long policyNo)
        {
            try
            {
                string responseFromServer = string.Empty;
                string responseXML = string.Empty;
                string xmlRequest = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://schemas.cordys.com/default'><soapenv:Header /><soapenv:Body><def:GET_PDFBpmWS> <def:policyNo>" + policyNo + "</def:policyNo><def:ltype>POLSCHD</def:ltype></def:GET_PDFBpmWS></soapenv:Body></soapenv:Envelope>";
                string xmlData = string.Format(xmlRequest);
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                string contentType = "application/soap+xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_policyPdfUrl);
                request.Method = "POST"; //Using POST method       
                string postData = xmlData;// GETTING XML STRING...
                request.ContentType = contentType;
                string _auth = string.Format("{0}:{1}", _userName, _password);
                request.Headers.Add(HttpRequestHeader.Authorization, _auth);
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                //request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;
                Stream requestWriter = (request.GetRequestStream());
                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();
                // soap Request
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.InnerXml = xmlRequest;
                    doc.Save(_xmlPath + "soapGET_PDFrequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                }
                catch (System.Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Getting the pdf request :" + ex.ToString(), "");
                   
                }
                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;             
                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                //soap Response
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.InnerXml = responseFromServer.ToString();
                    doc.Save(_xmlPath + "soapGET_PDFresponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                }
                catch (System.Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Getting the pdf Response :" + ex.ToString(), "");
                    
                }
                return responseFromServer;
            }
            catch (System.Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured inGetting the policy PDF Response :" + ex.ToString(), "");
                throw ex;
            }
         }        
    }
}










































