﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TransactionBE.master"CodeBehind="TransferConfirmation.aspx.cs" Inherits="CozmoB2BWebApp.TransferConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link href="css/menuDefault.css" rel="stylesheet" type="text/css">
    <link href="css/accordion_ui.css" type="text/css" rel="stylesheet" />
    <div style="" class="ns-h3 margin-top-10">
            TransferReservation Confirmation
            <label class="hidden-xs" style="float: right; font-size: 11px; padding-right: 10px; padding-top: 0px;">                
                Confirmation No:
                <span id="ConfirmationNo"></span>                
        </label>
    </div>
    <div class="bg_white pad_10 bor_gray"> 
        <%--<div class="col-md-2">
            <img id="ctl00_cphTransaction_imgHeaderLogo" src="https://ctb2cstage.cozmotravel.com/images/logo.jpg" style="height:64px;width:87px;border-width:0px;">

        </div>--%>
          
        <div class="col-md-6"> 
            <div> 
                <b id="TransferVehicle">
                    
                </b>
            </div>     
            <div>   
                Message To Driver:
                <b id="TransferRemark">
                   
                </b>
            </div>
                        <div>   
                Pick-up Sign:
                <b id="pickUpSign">
                   
                </b>
            </div>
            <div class="flightDetails">   
                Flight Number :
                <b id="flightNumber">
                   
                </b>
            </div>
             <div class="flightDetails">   
                Departure City :
                <b id="departureCity">
                   
                </b>
            </div>
            <div> 
                Tel :- 
                <b id="MobileNo"></b>
            </div>       
        </div>
        <div class="col-md-6">  
            <div>              
                <b class="spnred">No. of People </b>: 
                <span id="NoOfPassengers">

                </span> 
                
            </div>
            <div>
                <b>
                    <span class="spnred"> Booked on:</span>   </b>  
                <span id="bookedOn">
                </span>        
            </div>
            <div>   
                <b>
                    <span class="spnred"> Pick up date:</span>   </b>  
                <span id="pickUpDate">
                </span>
            </div>
        </div>
     
      
      <div class="clearfix"> </div>
      </div>
    <br />
    <br />
    <h4>  Payment Details</h4>
    <div class="table-responsive bg_white"> 
        <table width="100%" class="table" id="tblVehicleDetails">        
            <tbody>
                <tr>
                    <td>
                        <b>Vehicle</b>
                    </td>

                   
                    <td>
                        <b>No. of Passengers</b>
                    </td>
                    <td>
                        <b>Total Price </b>
                    </td>                                
                  
                 </tr>                             
                 <tr>
                    
                 </tr>  
                </tbody>
            </table>
            <table width="100%" class="table" id="tblPaymentDetails">        
                <tbody>
                    <tr>
                        <td>
                            <b>Total </b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                       
                        <td>
                            &nbsp;
                        </td>
                                
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="totalAmt"></span></b>
                        </td>
                    </tr>    
                    <%--<tr>
                        <td>
                            <b>Markup </b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                                
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="actMarkup"></span>
                            </b>
                        </td>
                    </tr> --%>
                    <tr class="discountRow">
                        <td>
                            <b>Discount </b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                                
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="actDiscountAmt"></span>
                            </b>
                        </td>
                    </tr> 
                    <tr style="display:none;">
                        <td>
                            <b>Total Markup </b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                                
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="totalMarkUp"></span>
                            </b>
                        </td>
                    </tr>    
                    <tr class="vatRow">
                        <td>
                                    
                                <b>Vat </b>
                                    
                        </td>
                        <td>
                            &nbsp;
                        </td>                                           
                         <td>
                            &nbsp;
                        </td>       
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="vatAmt"></span>
                            </b>
                        </td>
                     </tr>    
                    <tr class="gstRow">
                        <td>
                                    
                                <b>GST </b>
                                    
                        </td>
                        <td>
                            &nbsp;
                        </td>                                           
                         <td>
                            &nbsp;
                        </td>       
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="cgstAmount"></span>
                            </b>
                        </td>
                     </tr>
                    
                    <tr >
                        <td>
                            <b>Grand Total </b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                       <td>
                            &nbsp;
                        </td>
                        <td>
                            <b class="spnred">
                                <span class="agentCurrency"></span>
                                <span id="grandAmt"></span>
                            </b>
                        </td>
                     </tr>    
              </tbody>
        </table>
    </div>
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnBehalfLocation" runat="server" />
    <input type="hidden" id="hdnUserId" runat="server" />
    <div class="col-md-12" id="divViewVowcher">
        <span>Last Cancellatin Date : </span><span id="CancelDate"></span>
    </div>
    <script>
        var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
        var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["TransferWebApiUrl"]%>';

    </script>
    <script src="scripts/Transfers/TransferConfirmation.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
