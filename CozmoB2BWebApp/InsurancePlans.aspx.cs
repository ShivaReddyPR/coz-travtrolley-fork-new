﻿using System;
using CT.BookingEngine.Insurance;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;
using CT.MetaSearchEngine;

public partial class InsurancePlans :CT.Core.ParentPage// System.Web.UI.Page
{
    protected InsuranceResponse InsPlansResponse;
    protected InsuranceRequest request = new InsuranceRequest();
    protected int decimalPoint = 0;
    protected AgentMaster agency;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    if (Session["InsRequest"] != null)
                    {
                        request = (InsuranceRequest)Session["InsRequest"];
                        
                   
                        MetaSearchEngine mse = new MetaSearchEngine();
                        mse.SettingsLoginInfo = Settings.LoginInfo;

                        //Loading all plans
                        InsPlansResponse = mse.GetAvailablePlans(request);
                        //storing plans response object into session
                        Session["InsPlansResponse"] = InsPlansResponse;
                        int duration = Convert.ToDateTime(request.ReturnDateTime).Subtract(Convert.ToDateTime(request.DepartureDateTime)).Days + 1;
                        lblDuration.Text = duration.ToString();
                    }
                    else
                    {
                        Response.Redirect("Insurance.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to load Insurance: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }


    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["InsPlansResponse"] != null)
            {
                Session["SelectedPlans"] = null;
                InsPlansResponse = Session["InsPlansResponse"] as CT.BookingEngine.Insurance.InsuranceResponse;
                InsuranceResponse selectedPlans = new InsuranceResponse();
                //selected OTA plans
                if (InsPlansResponse.OTAPlans != null)
                {
                    if (!string.IsNullOrEmpty(Request["hdnOTPPlan"])) //checking selected plans null or empty
                    {
                        int otaPlans = Convert.ToInt32(Request["hdnOTPPlan"]);
                        selectedPlans.OTAPlans = new List<InsurancePlanDetails>();
                        selectedPlans.OTAPlans.Add(InsPlansResponse.OTAPlans[otaPlans]);
                    }
                }
                //selected Upsell plans
                if (InsPlansResponse.UpsellPlans != null)
                {
                    if (!string.IsNullOrEmpty(Request["hdnUPSellPlan"]))//checking selected Upplans null or empty
                    {
                        selectedPlans.UpsellPlans = new List<InsurancePlanDetails>();
                        string[] upsellPlans = Request["hdnUPSellPlan"].Split(',');
                        for (int k = 0; k < upsellPlans.Length; k++)
                        {
                            selectedPlans.UpsellPlans.Add(InsPlansResponse.UpsellPlans[Convert.ToInt32(upsellPlans[k])]);
                        }
                    }
                }
                selectedPlans.SessionId = InsPlansResponse.SessionId;
                //selected plans storing SelectedPlans session
                Session["SelectedPlans"] = selectedPlans;
                Response.Redirect("InsurancePax.aspx",false);
            }
            else
            {
                Response.Redirect("Insurance.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to load selected Insurance plans : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
