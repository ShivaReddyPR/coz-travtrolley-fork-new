﻿using System;
using System.Data;
using System.Web.UI;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
public partial class CorporateViewExpenseDetailsUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    IntialiseControls();

                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateViewExpenseDetailsUI page Load Error: " + ex.ToString(), "0");
        }

    }
    private void IntialiseControls()
    {
        try
        {
            DataTable dtDetails = CorporateProfileExpenseDetails.GetExpenseDetails(Convert.ToInt32(Request.QueryString["ExpId"]));
            dlExpenseDetails.DataSource = dtDetails;
            dlExpenseDetails.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
