﻿using System;
using System.Web.Services;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class hotelStatusUpdate : ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
        }

        [WebMethod]
        public static string GetStatus(string Confno)
        {
            string Status = "";
            try
            {
                 Status = HotelItinerary.GetStatusbyConfirmationNo(Convert.ToString(Confno));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High,Convert.ToInt32(Settings.LoginInfo.UserID), "Error at GetStatus():" + ex.ToString(), "");   
            }
            return Status;
        }

        [WebMethod]
        public static int UpdateStatus(string Confno, string status, string remarks)
        {
            int rowseffected = 0;
            try
            {
                int sts = Convert.ToInt32((HotelBookingStatus)Enum.Parse(typeof(HotelBookingStatus), status));
                 rowseffected = HotelItinerary.UpdateStatusbyConfirmationNo(Convert.ToString(Confno), sts, Convert.ToInt32(Settings.LoginInfo.UserID), Convert.ToString(remarks));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Error at UpdateStatus():" + ex.ToString(), "");
            }
            return rowseffected;
        }
    }
}
