﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareQuestionMaster.aspx.cs" Inherits="CozmoB2BWebApp.ReligareQuestionMaster" %>


<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
 <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div><h3>Religare Question  Master</h3></div>
         <div class="body_container" style="height:200px">
        <div class="col-md-12 padding-0 marbot_10">

            <div class="col-md-2">
                <asp:Label ID="lblProductId" runat="server" Text="Product Type Name:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="ddlProductId" runat="server"  AutoPostBack="true"></asp:DropDownList>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblQuestionSetCode" runat="server" Text="Question Set Code:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtQuestionSetcode" runat="server"></asp:TextBox>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblQuestionCode" runat="server" Text="Question Code:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtQuestionCode" runat="server"></asp:TextBox>
            </div>
             <div class="clearfix"></div>
             </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                <asp:Label ID="lblQuestionDescription" runat="server" Text="Question Description:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtQuestionDescription" runat="server"></asp:TextBox>
            </div>
             <div class="col-md-2">
                Question Type
                    </div>
                <div class="col-md-2">
                <asp:DropDownList ID="ddlQuestionType" CssClass="form-control" Style="width: 200px;"
                runat="server">
                <asp:ListItem Value="0">-- Select QuestionType --</asp:ListItem>
                <asp:ListItem Value="sponser">SPONSER</asp:ListItem>
                <asp:ListItem Value="Optional">OPTIONAL</asp:ListItem>
                <asp:ListItem Value="PED">PED</asp:ListItem>
            </asp:DropDownList>
                    </div>
            <div class="clearfix"></div>
           </div>
           
            

        <div class="col-md-12">
            <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
            </div>

       
        <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <script type="text/javascript">
        function Save() {
            if (getElement('ddlProductId').value == -1) addMessage('Product Id cannot be blank!', '');
            if (getElement('txtQuestionSetcode').value == '') addMessage('Question Set Code cannot be blank!', '');
            if (getElement('txtQuestionCode').value == '') addMessage('Question Code cannot be blank!', '');
            if (getElement('txtQuestionDescription').value == '') addMessage('QuestionDescription cannot be blank!', '');
            if (getElement('ddlQuestionType').value == 0) addMessage('QuestionType cannot be blank!', '');

        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
    </script>
    <asp:HiddenField runat="server" ID="hdfMeid" Value="0" />
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 

     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="QUESTION_ID"
      emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">

    

     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblQuestionSetcode" runat="server" Text='<%# Eval("QUESTION_SET_CODE") %>' CssClass="label grdof" ToolTip='<%# Eval("QUESTION_SET_CODE") %>'></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Question Set Code
             </HeaderTemplate>
         </asp:TemplateField>

         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblQuestioncode" runat="server" Text='<%# Eval("QUESTION_CODE") %>' CssClass="label grdof" ToolTip='<%# Eval("QUESTION_CODE") %>'></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Question Code
             </HeaderTemplate>
         </asp:TemplateField>
          <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblQuestionType" runat="server" Text='<%# Eval("STATUS") %>' CssClass="label grdof" ToolTip='<%# Eval("STATUS") %>'></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Question Type
             </HeaderTemplate>
         </asp:TemplateField>

         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlbltxtQuestionDescription" runat="server" Text='<%# Eval("QUESION_DESCRIPTION") %>' CssClass="label grdof" ToolTip='<%# Eval("QUESION_DESCRIPTION") %>' ></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Question Description
             </HeaderTemplate>
         </asp:TemplateField>
          <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblddlQuestionType" runat="server" style="display:none" Text='<%# Eval("STATUS") %>' CssClass="label grdof" ToolTip='<%# Eval("STATUS") %>'></asp:Label>
   
    </ItemTemplate>   
         </asp:TemplateField>

     </Columns>

     </asp:GridView>
     

    </asp:Content>


