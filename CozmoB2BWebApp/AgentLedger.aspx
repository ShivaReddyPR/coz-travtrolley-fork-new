﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AgentLedger" Title="Ledger Transaction" CodeBehind="AgentLedger.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.AccountingEngine" %>
<%@ Import Namespace="CT.GlobalVisa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script src="scripts/LINQ_JS_MIN.js" type="text/javascript"></script>
    <script type="text/javascript">
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            //cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
           // cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            init();
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            document.getElementById('container1').style.display = "block";
         }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render(); 
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }

    </script>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 10%; display: none; z-index: 9999;" >
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 43%; z-index: 9999; display: none;">
        </div>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
        <asp:HiddenField ID ="hdnReferenceTypes" runat="server" />
        <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width: 700px" align="left">
                    <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                        onclick="return ShowHide('divParam');">Hide Parameter</a>
                </td>
            </tr>
        </table>
        <div class="paramcon" title="Param" id="divParam">
            <asp:Panel runat="server" ID="pnlParam" Visible="true">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">From Date:</div>
                    <div class="col-md-2">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="CheckIn" runat="server" Width="110px" CssClass="form-control" onchange="GetLedgerDetails();" ></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">To Date:</div>
                    <div class="col-md-2">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="CheckOut" runat="server" CssClass="form-control" Width="110px" onchange="GetLedgerDetails();"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">Agent:</div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlAgents" CssClass="form-control" runat="server" onchange="BindAgents('B2B',this.value);">
                            <%-- AutoPostBack="true"  OnSelectedIndexChanged="ddlAgents_SelectionChanged" >--%>
                        </asp:DropDownList>
                        <%----%>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        <asp:Label ID="lblB2BAgent" runat="server" Visible="true" Text="B2BAgent:"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="form-control" onchange="BindAgents('B2B2B',this.value);">
                            <%--  OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"> --%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblB2B2BAgent" runat="server" Visible="true" Text="B2B2BAgent:"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" onchange="GetLedgerDetails();">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlTransType" runat="server" CssClass="form-control" Visible="false" onchange="GetLedgerDetails();">
                            <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                            <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                            <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 padding-0 marbot_10"  >
                    <div class="col-md-2" style="display:none;">
                        <asp:Label ID="lblPaymentType" runat="server" Text="Account Type:"></asp:Label>
                    </div>
                    <div class="col-md-2" style="display:none;">
                        <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="form-control" onchange="GetLedgerDetails();">
                            <asp:ListItem Value="OnAccount" Text="OnAccount" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Card" Text="Card"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <input type="button" ID="btnSubmit"  class="btn but_b pull-right" value="Search" onclick="GetLedgerDetails()" />
<%--                    <asp:Button CssClass="btn but_b pull-right" Visible="false" ID="btnSubmit" runat="server" Text="Search" OnClientClick="GetLedgerDetails();" />--%>
                    <%--OnClientClick="GetLedgerDetails(); return false;"--%>
                    <div class="clearfix"></div>
                </div>
            </asp:Panel>
        </div>
        
        <div id="ExportDiv" runat="server">
            <div class="table-responsive col-md-12 padding-0 margin-top-10">
                <table id="tblLedger" class="table bg_white" border="1">
                    <thead>
                        <tr class="themecol1" style="text-align: center">
                            <td class="padding-left-10 bold">
                                <span style="color: White;">Agent Name/Date </span></td>
                            <td class="bold  padding-left-10 ">
                                <span style="color: White;">Ref #</span></td>
                            <td class="bold  padding-left-10 ">
                                <span style="color: White;">Particulars</span></td>
                            <td class="bold  padding-left-10">
                                <span style="color: White;">Debit</span></td>
                            <td class="bold  padding-left-10">
                                <span style="color: White;">Credit</span></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
               
                <div class="clearfix"></div>
            </div>
        </div>
        <div>
            <input type="button" ID="ExportToExcelButton" value="Export To Excel" class="btn but_b pull-right" onclick="ExportExcel()" />
<%--        <asp:Button ID="ExportToExcelButton" Visible="false" runat="server" Text="Export To Excel" CssClass="btn but_b pull-right" OnClientClick="ExportExcel()" /> --%>
            </div>

    </div>

    <script>
        function GetToday() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            dd = dd < 10 ? '0' + dd : dd;
            mm = mm < 10 ? '0' + mm : mm;
            today = dd + '/' + mm + '/' + yyyy;
            return today;
        }
        function GetLedgerDetails() {
            try {
                $('#btnSubmit').attr('disabled',true);
                var agentId = 0, startDate = '', endDate = '', agentType = '', transType = '', paymentType = $('#<%=ddlPaymentType.ClientID%>').val();
                 if ($('#<%=CheckIn.ClientID%>').val() != '') {
                    startDate = $('#<%=CheckIn.ClientID%>').val();
                }
                else {
                    startDate = GetToday();
                }

                if ($('#<%=CheckOut.ClientID%>').val() != '') {
                    endDate = $('#<%=CheckOut.ClientID%>').val() + " 23:59:59";
                }
                else {
                    endDate = GetToday() + " 23:59:59";
                }
              
                agentId = $('#<%=ddlAgents.ClientID%>').val();
                if (parseInt($('#<%=ddlAgents.ClientID%>').val()) > 0) {
                    agentId = $('#<%=ddlAgents.ClientID%>').val();
                    agentType = "AGENT";
                }
                else {
                    agentType = "ALL";
                }

                if (parseInt($('#<%=ddlB2BAgent.ClientID%>').val()) > 0) {
                    agentId = $('#<%=ddlB2BAgent.ClientID%>').val();
                    agentType = "AGENT";
                }
                else if (parseInt($('#<%=ddlB2BAgent.ClientID%>').val()) == 0) {
                    agentType = 'ALL';
                }
                var isDisabled = $("#ctl00_cphTransaction_ddlB2B2BAgent").is(":disabled");
                if (!isDisabled) {
                    if (parseInt($('#<%=ddlB2B2BAgent.ClientID%>').val()) > 0) {
                        agentId = $('#<%=ddlB2B2BAgent.ClientID%>').val();
                        agentType = "AGENT";
                    }
                    else if (parseInt($('#<%=ddlB2B2BAgent.ClientID%>').val()) == 0) {
                        agentId = parseInt($('#<%=ddlB2BAgent.ClientID%>').val()) > 0 ? $('#<%=ddlB2BAgent.ClientID%>').val() : $('#<%=ddlAgents.ClientID%>').val();
                        agentType = 'ALL';
                    }
                }
                if (agentId == 0) agentType = '';             

                if ("<%=Settings.LoginInfo.TransType%>" == "B2B") {
                    transType = "B2B";
                }
                else if ("<%=Settings.LoginInfo.TransType%>" == "B2C") {
                    transType = "B2C";
                }
                else {
                    if ($('#<%=ddlTransType.ClientID%>').val() == "-1") {
                        transType = '';
                    }
                    else {
                        transType = $('#<%=ddlTransType.ClientID%>').val()
                    }
                }                 
                $("#tblLedger > tbody").html('<tr><td colspan=5><b>Loading.Please Wait ...</b> </td></tr>');
                $('#ExportToExcelButton').hide()
                $.ajax({
                    type: "POST",
                    url: "AgentLedger.aspx/GetLedgerDetails",
                    contentType: "application/json; charset=utf-8",
                    data: "{'agentId':'" + agentId + "','startDate':'" + startDate + "','endDate':'" + endDate + "','agentType':'" + agentType + "','transType':'" + transType + "','paymentType':'" + paymentType + "'}",
                    dataType: "json",
                    async: true,
                    success: function (data) {
                        if (data.d != undefined && data.d != '[]') {
                            var ledgerAdmin = JSON.parse(data.d);
                           // console.log(ledgerAdmin.length);
                           // console.log(new Date());
                            BindLedger(ledgerAdmin);
                           // console.log(new Date());
                            var rowcount = $("#tblLedger tbody tr").length;
                            rowcount > 0 ? $('#ExportToExcelButton').show() : $('#ExportToExcelButton').hide();
                            rowcount > 0 ? $('#tblLedger').show() : $('#tblLedger').hide();
                        }
                        else {
                           $("#tblLedger > tbody").html('<tr><td colspan=5><b>Ledger Transactions not Found.</b></td></tr>');
                           var rowcount = $("#tblLedger tbody tr").length;
                           rowcount > 1 ? $('#ExportToExcelButton').show() : $('#ExportToExcelButton').hide();
                        }
                        $('#btnSubmit').attr('disabled',false);
                    },
                    error: (error) => {
                        console.log(JSON.stringify(error));
                        $("#tblLedger > tbody").html('<tr><td colspan=5><b>Ledger Transactions not Found.</b></td></tr>');
                        var rowcount = $("#tblLedger tbody tr").length;
                        rowcount > 1 ? $('#ExportToExcelButton').show() : $('#ExportToExcelButton').hide();
                        $('#btnSubmit').attr('disabled',false);
                    }
                });  
            }
            catch (e) {
                console.log(e);
            }
        }

        function BindLedger(ledgerAdmin) {
            try {
                if (ledgerAdmin.length > 0) {
                    var agencyId = ledgerAdmin[0].AgencyId;
                    var totalDebit = 0;
                    var totalCredit = 0;
                    var balanceTillDate = 0;
                    var invoiceNumber = '';
                    var ledgerTotal = 0;
                    var ticketCounter = 1;
                    var counterString = '';
                    var decimalValue = 2;
                    var tbl = '';
                    var mxFr1 = new Intl.NumberFormat("en-IN", { minimumFractionDigits:decimalValue, maximumFractionDigits: decimalValue});
                    var refTypes = JSON.parse($('#ctl00_cphTransaction_hdnReferenceTypes').val());
                    $("#tblLedger > tbody").html("");
                    for (var i = 0; i < ledgerAdmin.length; i++) {
                        var debit = '';
                        var credit = '';
                        ticketCounter = ledgerAdmin[i].PaxCount;
                        ledgerTotal = ledgerAdmin[i].Amount;
                        decimalValue = ledgerAdmin[i].AgentDecimal;
                        counterString = '';
                        invoiceNumber = '';
                        invoiceNumber = ledgerAdmin[i].InvoiceNumber;
                        mxFr1 = new Intl.NumberFormat("en-IN", { minimumFractionDigits:decimalValue, maximumFractionDigits: decimalValue});
                        //Flight Tickets combined
                        if (refTypes[ledgerAdmin[i].ReferenceType] != undefined && refTypes[ledgerAdmin[i].ReferenceType] != 'VisaReceipt') {
                            ticketCounter = ledgerAdmin.filter(function (p) { return (p.InvoiceNumber == invoiceNumber && p.ReferenceType == ledgerAdmin[i].ReferenceType) }).length;
                            ledgerTotal = ledgerAdmin.sum(function (p) {
                                return (p.InvoiceNumber == invoiceNumber && p.ReferenceType == ledgerAdmin[i].ReferenceType ? p.Amount : 0)
                            });
                        }
                        if (refTypes[ledgerAdmin[i].ReferenceType] != undefined && refTypes[ledgerAdmin[i].ReferenceType] != 'VisaReceipt') {
                            counterString = " X " + ticketCounter;
                        }
                        credit = ledgerTotal > 0 ? ledgerTotal.toFixed(decimalValue) : '-';
                        debit = ledgerTotal > 0 ? '-' : -ledgerTotal.toFixed(decimalValue);
                        if (agencyId != ledgerAdmin[i].AgencyId || i == 0) {
                            agencyId = ledgerAdmin[i].AgencyId;
                            if (i != 0) {
                                tbl += '<tr class="height-25 width-100 light-gray-back">';
                                tbl += '<td colspan="3" class="bold padding-right-10  ">Total</td>';
                                tbl += '<td class="bold padding-right-10">' + totalDebit.toFixed(decimalValue) + '</td>';
                                tbl += '<td class="bold padding-right-10">' + totalCredit.toFixed(decimalValue) + '</td>';
                                tbl += '</tr>';

                                tbl += '<tr class="height-25 wid th-100 light-gray-back">';
                                tbl += '<td colspan="4" class="bold " style="border-right: 0px">Closing Balance: &nbsp;</td>';
                                // If agnet payment mode is 9 then showing the totaldebit -totalcredit amount.                                
                                if (ledgerAdmin[i].Agent_Payment_Mode == 9) {                                   
                                    var closingBalance = Math.abs(totalCredit) - Math.abs(totalDebit);
                                    if (closingBalance < 0)
                                        closingBalance = -(closingBalance);
                                    console.log(closingBalance);
                                    tbl += '<td class="bold padding-right-10  "style="border-left: 0px;">' +
                                        mxFr1.format(closingBalance);
                                    + '</td>';
                                } else {
                                    tbl += '<td class="bold padding-right-10  " style="border-left: 0px">' +
                                        mxFr1.format(parseFloat(-(balanceTillDate + totalDebit - totalCredit)));
                                    + '</td>';
                                }
                                tbl += '</tr>';
                            }
                            if (ledgerAdmin[i].Agent_Payment_Mode == 9) {
                                tbl += '<tr>';
                                tbl += '<td><b>' + ((ledgerAdmin[i].Agent_Payment_Mode == 9) ? '' : ledgerAdmin[i].AgentName) + '</b></td>';
                                tbl += '<td colspan="2" class="bold padding-left-10 " style="border-right: 0px;">Available Current Balance (All values are in ' + ledgerAdmin[i].AgentCurrency + ')';
                                tbl += '</td>';
                                balanceTillDate = ledgerAdmin[i].BalanceTillDate;

                                if (ledgerAdmin[i].PaymentMode == "4" || ledgerAdmin[i].PaymentMode == "2" || ledgerAdmin[i].PaymentMode == "0") {
                                    balanceTillDate += -ledgerAdmin[i].AgentCurrentBalance;
                                }

                                if (balanceTillDate < 0) {
                                    tbl += '<td class="bold padding-right-10" style="border-left: 0px;">' +
                                        mxFr1.format(parseFloat(ledgerAdmin[i].AgentCurrentBalance)); + '</td>';
                                }
                                else {
                                    tbl += '<td class="bold padding-right-10" style="border-left: 0px;"><img alt="Spacer" src="Images/spacer.gif" /></td>';
                                }
                                if (balanceTillDate >= 0) {
                                    tbl += '<td class="bold padding-right-10 ">' +
                                        mxFr1.format(parseFloat(ledgerAdmin[i].AgentCurrentBalance)); + '</td>';
                                }
                                else {
                                    tbl += '<td class="bold padding-right-10 "><img alt="Spacer" src="Images/spacer.gif" /></td>';
                                }
                                tbl += '</tr>';


                                tbl += '<tr><td><b>' + ledgerAdmin[i].AgentName + '</b></td> <td colspan="2">';
                                tbl += '<b>Credit Limit :&nbsp&nbsp;' + ledgerAdmin[i].Agent_Credit_Limit + '&nbsp' + ledgerAdmin[i].AgentCurrency + '</b>&nbsp;&nbsp&nbsp;';
                                tbl += '<b>Temp Credit :&nbsp&nbsp;' + ledgerAdmin[i].Agent_Credit_Buffer_Amount + '&nbsp' + ledgerAdmin[i].AgentCurrency + '</b>&nbsp;&nbsp&nbsp;';
                                tbl += '<b> Credit Days :&nbsp&nbsp;' + ledgerAdmin[i].Agent_Credit_Days + '</b>&nbsp;&nbsp&nbsp;';
                                tbl += '</td><td> </td><td> </td></tr>';
                            }
                            totalCredit = 0;
                            totalDebit = 0;
                            tbl += '<tr>';
                            tbl += '<td><b>' + ((ledgerAdmin[i].Agent_Payment_Mode == 9) ? '' : ledgerAdmin[i].AgentName) + '</b></td>';
                            tbl += '<td colspan="2" class="bold padding-left-10 " style="border-right: 0px;">Opening Current Balance (All values are in ' + ledgerAdmin[i].AgentCurrency + ')';
                            tbl += '</td>';
                            balanceTillDate = ledgerAdmin[i].BalanceTillDate;
                            
                            if (ledgerAdmin[i].PaymentMode == "4" || ledgerAdmin[i].PaymentMode == "2" || ledgerAdmin[i].PaymentMode == "0") {
                                balanceTillDate += -ledgerAdmin[i].AgentCurrentBalance;
                            }

                            
                            if (balanceTillDate < 0) {
                                //balanceTillDate = -(balanceTillDate );
                               // balanceTillDate = (balanceTillDate * -1);
                                 
                                tbl += '<td class="bold padding-right-10 ">' +                                    
                                   mxFr1.format(parseFloat(-(balanceTillDate))); + '</td>';
                                
                            }
                            else if (balanceTillDate >= 0 && ledgerAdmin[i].Agent_Payment_Mode == 9) {
                                    tbl += '<td class="bold padding-right-10 ">' +
                                        mxFr1.format(parseFloat(ledgerAdmin[i].AgentCurrentBalance)); + '</td>';
                                }
                            else {
                                tbl += '<td class="bold padding-right-10 "><img alt="Spacer" src="Images/spacer.gif" /></td>';
                            }
                            tbl += '</tr>';
                        }

                        if (ledgerAdmin[i].TransType == "B2B") {
                            if (ledgerTotal > 0) {
                                totalCredit += ledgerTotal;
                            }
                            else {
                                totalDebit += -ledgerTotal;
                            }
                        }
                        tbl += '<tr class="height-25 width-100">';
                        var createdOn = new Date(ledgerAdmin[i].CreatedOn);
                        tbl += '<td class="padding-left-10 bold locked-pnr-booking-width  ">' + createdOn.getDate() + '-' + (createdOn.getMonth() + 1) + '-' + createdOn.getFullYear() + '</td>';

                        if (refTypes[ledgerAdmin[i].ReferenceType] != undefined && refTypes[ledgerAdmin[i].ReferenceType] != 'VisaReceipt') {
                            tbl += '<td class="date-of-birth width-140  padding-left-10 ">' + invoiceNumber + '</td>';
                        }
                        else {
                            if (ledgerAdmin[i].Narration.DocNo != "") {
                                tbl += '<td class="date-of-birth width-140  padding-left-10 ">' + ledgerAdmin[i].Narration.DocNo + '</td>';
                            }
                            else
                                if (ledgerAdmin[i].CheckNo != "0") {
                                    tbl += '<td class="date-of-birth width-140  padding-left-10 ">' + ledgerAdmin[i].CheckNo + '</td>';
                                }
                                else {
                                    tbl += '<td class="date-of-birth width-140  padding-left-10 ">-</td>';
                                }
                        }

                        if (refTypes[ledgerAdmin[i].ReferenceType] == "PaymentReversed") {
                            tbl += '<td class="width-800" style="padding-left: 10px;"><b>' + ledgerAdmin[i].Narration.Remarks + ': </b>' + ledgerAdmin[i].Notes+' '+ledgerAdmin[i].Narration.PaxName + '</td>';
                        }
                        else {
                            tbl += "<td class='width-800' style='padding-left: 10px;'><b>" + ledgerAdmin[i].Narration.Remarks.replace('@', '| ') + ": </b>" + ledgerAdmin[i].Narration.PaxName + ' ' + counterString + ' ' + ledgerAdmin[i].Notes + "</td>";
                        }

                        tbl += '<td class="padding-right-10">' + (debit != '-' ? mxFr1.format(parseFloat(debit)) : debit) + '</td>';
                        tbl += '<td class="padding-right-10">' + (credit != '-' ? mxFr1.format(parseFloat(credit)) : credit) + '</td>';

                        tbl += '</tr>';
                        if (ledgerAdmin[i].Narration.Sector != "") {
                            tbl += '<tr class="height-25">';
                            tbl += '<td class="padding-left-10 bold"></td>';
                            tbl += '<td class="bold  padding-left-10"></td>';
                            tbl += '<td class=" padding-left-10"><b>' + ledgerAdmin[i].Narration.Sector + ' </b>TICKET NO.: ' + ledgerAdmin[i].TicketNo + '</td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '</tr>';
                        }
                        else if (ledgerAdmin[i].Narration.HotelConfirmationNo != "") {
                            tbl += '<tr class="height-25">';
                            tbl += '<td class="padding-left-10 bold"></td>';
                            tbl += '<td class="bold  padding-left-10"></td>';
                            tbl += "<td class='padding-left-10'><b>" + ledgerAdmin[i].Narration.DestinationCity + " </b>CONFIRMATION NO.: " + ledgerAdmin[i].Narration.HotelConfirmationNo.replace('@', '| ') + " </td>";
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '</tr>';
                        }
                        else if (ledgerAdmin[i].Narration.PolicyNo != "") {
                            tbl += '<tr class="height-25">';
                            tbl += '<td class="padding-left-10 bold"></td>';
                            tbl += '<td class="bold  padding-left-10"></td>';
                            tbl += '<td class=" padding-left-10"><b>POLICY NO.: ' + ledgerAdmin[i].Narration.PolicyNo + '</b></td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '</tr>';
                        }
                        if (ledgerAdmin[i].Narration.TravelDate != "") {
                            if (ledgerAdmin[i].Narration.TravelDate.indexOf("/") > 0) { var travelDate = ledgerAdmin[i].Narration.TravelDate.split('/') } else { var travelDate = ledgerAdmin[i].Narration.TravelDate.split('-'); }
                            tbl += '<tr class="height-25" colspan="5">';
                            tbl += '<td class="padding-left-10 bold" ></td>';
                            tbl += '<td class="bold  padding-left-10"></td>';
                            if (ledgerAdmin[i].Narration.FlightNo != "") {
                                tbl += '<td class=" padding-left-10">TRAVEL DATE :' + travelDate[1] + "/" + travelDate[0] + "/" + travelDate[2] + ' BY : ' + ledgerAdmin[i].Narration.FlightNo + '</td>';
                            }
                            else {
                                if (refTypes[ledgerAdmin[i].ReferenceType] == "MobileRecharge") {
                                    tbl += '<td class=" padding-left-10">RECHARGE DATE : ' + travelDate[1] + "/" + travelDate[0] + "/" + travelDate[2] + '</td>';
                                }
                                else {
                                    tbl += '<td class="bold left  text-right padding-right-10"></td>';
                                }
                            }
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '</tr>';
                        }
                        if (ledgerAdmin[i].Narration.ChequeNo != "") {
                            tbl += '<tr class="height-25">';
                            tbl += '<td class="padding-left-10 bold"></td>';
                            tbl += '<td class="bold  padding-left-10"></td>';
                            tbl += '<td class=" padding-left-10"><b>' + ledgerAdmin[i].Narration.PaymentMode + ' No.: </b>' + ledgerAdmin[i].Narration.ChequeNo + '</td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '<td class="bold left  text-right padding-right-10"></td>';
                            tbl += '</tr>';
                        }
                        tbl += '<tr class="height-25">';
                        tbl += '<td class="padding-left-10 bold">&nbsp;</td>';
                        tbl += '<td class="bold  padding-left-10">&nbsp;</td>';
                        tbl += '<td class="padding-left-10">&nbsp;</td>';
                        tbl += '<td class="bold left  text-right padding-right-10">&nbsp;</td>';
                        tbl += '<td class="bold left  text-right padding-right-10">&nbsp;</td>';
                        tbl += '</tr>';
                        i = refTypes[ledgerAdmin[i].ReferenceType] == "HotelBooked" ? i++ : i + ticketCounter - 1;
                    }
                    tbl += '<tr class="height-25 wid th-100 light-gray-back">';
                    tbl += '<td colspan="3" class="bold " style="border-right: 0px">Total &nbsp; </td>';
                    tbl += '<td class="bold padding-right-10  " style="border-left: 0px">' + mxFr1.format(totalDebit); + '</td>';
                    tbl += '<td class="bold padding-right-10  ">' +
                        mxFr1.format(totalCredit);
                    + '</td>';
                    tbl += '</tr>';
                    tbl += '<tr class="height-25 wid th-100 light-gray-back">';
                    tbl += '<td colspan="4" class="bold " style="border-right: 0px">Closing Balance: &nbsp;</td>';
                    // If agnet payment mode is 9 then showing the totaldebit -totalcredit amount.
                    if (ledgerAdmin[ledgerAdmin.length - 1].Agent_Payment_Mode == 9) {                              
                        //var closingBalance = Math.abs(totalCredit) - Math.abs(totalDebit);                        
                        var closingBalance = ledgerAdmin[ledgerAdmin.length - 1].AgentCurrentBalance;
                            //closingBalance = (closingBalance * -1);
                        tbl += '<td class="bold padding-right-10  " style="border-left: 0px">' +
                            mxFr1.format(closingBalance);
                        + '</td>';
                    } else {
                        var closingBalance = -(balanceTillDate + totalDebit - totalCredit);
                        //if (closingBalance < 0)
                        //    closingBalance = -(closingBalance);
                            //closingBalance = (closingBalance * -1);
                        tbl += '<td class="bold padding-right-10  " style="border-left: 0px">' +
                            mxFr1.format(closingBalance);
                        + '</td>';
                    } 
                    tbl += '</tr>';
                    $("#tblLedger > tbody").html(tbl);
                }
            }
            catch (e) { console.log(e); }
        }
       
        function ExportExcel() {
            var bgcolor = $('.themecol1').css('background-color');
            var tab_text = "<table  border='1'>";
            tab_text += '<tr style="font-weight:bold;text-align:center">'
                + '<td style="width:150px;background-color:' + bgcolor + '"><span style="color: White;">Agent Name/Date </span></td>'
                + '<td style="width:150px;background-color:' + bgcolor + '"><span style="color: White;">Ref #</span></td>'
                + '<td style="width:500px;background-color:' + bgcolor + '"><span style="color: White;">Particulars</span></td>'
                + '<td style="width:150px;background-color:' + bgcolor + '"><span style="color: White;">Debit</span></td>'
                + '<td style="width:150px;background-color:' + bgcolor + '"><span style="color: White;">Credit</span></td></tr>';
            var rowcount = $("#tblLedger tbody tr").length;
            //$("#tblLedger tbody tr").each(function () {
            //    tab_text += '<tr>' + $(this).html() + "</tr>";
            //});
            tab_text += $("#tblLedger").prop("innerHTML");
            tab_text = tab_text + "</table>";
            // Javascript file downloading allows only less than 1 MB.1048576
            if (rowcount > 1) {
                var date = new Date();//    1048576 
                var link = document.createElement("A");
                link.href = 'data:text/application/vnd.ms-excel,' + encodeURIComponent(tab_text);
                link.download = "LedgerDetails_" + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '-' + date.getTime() + ".xls";
                link.click();
            }
            else if (rowcount < 1) {
                alert('Max File Size Exceeded..');
            }
        }
        function pageLoad() {
            GetLedgerDetails();
            var rowcount = $("#tblLedger tbody tr").length;
            rowcount > 0 ? $('#ExportToExcelButton').show() : $('#ExportToExcelButton').hide();
            rowcount > 0 ? $('#tblLedger').show() : $('#tblLedger').hide();
        }
        function BindAgents(agentType, val) { 
            var agentId = val;
            var options = ''; 
            if (parseInt(agentId) > 0) { 
                $.ajax({
                    type: "POST",
                    url: "AgentLedger.aspx/GetAgents",
                    contentType: "application/json; charset=utf-8",
                    data: "{'agentId':'" + agentId + "','agentType':'" + agentType + "'}",
                    dataType: "json",
                    async: true,
                    success: function (data) { 
                        var tempoptions = '<option value="-1">--Select ' + agentType + 'Agent--</option>';
                        tempoptions += '<option value="0">--ALL--</option>';
                        $.each(data.d, function (index, item) {
                            options += '<option value="' + index + '">' + item + '</option>'
                        });
                        if (agentType == 'B2B') {
                            $('#ctl00_cphTransaction_ddlB2BAgent').html(tempoptions + options);
                            $("#ctl00_cphTransaction_ddlB2B2BAgent").prop("disabled", true);
                            $('#' + getElement('ddlB2B2BAgent').id).select2('val', '-1');
                            $('#' + getElement('ddlB2BAgent').id).select2('val', '-1');
                        }
                        else {
                            $('#ctl00_cphTransaction_ddlB2B2BAgent').html(tempoptions + options);
                            $("#ctl00_cphTransaction_ddlB2B2BAgent").prop("disabled", false);
                            $('#' + getElement('ddlB2B2BAgent').id).select2('val', '-1');
                        }
                        GetLedgerDetails();
                    },
                    error: (error) => {
                        console.log(JSON.stringify(error));
                    }
                });
            }
            else {
                options = '<option value="-1">--Select ' + agentType + 'Agent--</option>';
                options += '<option value="0">--ALL--</option>';
                if (agentType == 'B2B') {
                    $('#ctl00_cphTransaction_ddlB2BAgent').html(options);
                    $("#ctl00_cphTransaction_ddlB2B2BAgent").prop("disabled", false);
                }
                else {
                    $('#ctl00_cphTransaction_ddlB2B2BAgent').html(options);
                    $("#ctl00_cphTransaction_ddlB2B2BAgent").prop("disabled", false);
                    $('#' + getElement('ddlB2B2BAgent').id).select2('val', '-1');
                }
                GetLedgerDetails();
            }
        }
    </script>
    <style>
        .overlay {
    position: fixed;
    top: 0;
    left: 0;
    background-color: #fff;
    width: 100%;
    height: 100%;
    display: none;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>


