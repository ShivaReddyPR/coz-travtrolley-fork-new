﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.WhiteLabel;
using CT.TicketReceipt.Web.UI.Controls;

public partial class CorpFeedBackList : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Master.PageRole = true;

        if (Settings.LoginInfo != null)
        {
            if (!IsPostBack)
            {
                CheckIn.Text = DateTime.Now.ToString("dd/MM/yyyy");
                CheckOut.Text = DateTime.Now.ToString("dd/MM/yyyy");
                BindList();
            }
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

    void BindList()
    {
        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
        DataTable dt = Feedback.GetFeedbackDetails(Convert.ToDateTime(CheckIn.Text, dateFormat), Convert.ToDateTime(CheckOut.Text, dateFormat));

        foreach (DataControlField column in gvCorpFBList.Columns)
        {
            //column.ItemStyle.Width = new Unit(120, UnitType.Pixel);
            //column.ItemStyle.Wrap = true;
            //column.ItemStyle.VerticalAlign = VerticalAlign.Top;
            //column.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
        }
        
        CommonGrid cg = new CommonGrid();
        cg.BindGrid(gvCorpFBList, dt);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindList();
    }

    protected void gvCorpList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCorpFBList.PageIndex = e.NewPageIndex;
        BindList();
    }

    protected void gvCorpList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                foreach(Control ctrl in cell.Controls)
                {
                    Label lbl = ctrl as Label;
                    //if (lbl != null && lbl.Text != null && lbl.Text.Trim().Length > 0)
                    //{
                    //    lbl.Text = lbl.Text.Substring(0, 29) + "<a href='#' onclick=\"popupopen('comments" + e.Row.DataItemIndex + "')\";>view more</a> <div class='firstPopupDivOuter' id='comments" + e.Row.DataItemIndex + "'>" + gvCorpFBList.Columns[e.Row.DataItemIndex].HeaderText + "</br>" + lbl.Text + "<br /><div id='firstPopupDivInner'><a class='f_R button' onclick=\"popupclose('comments" + e.Row.DataItemIndex + "')\";>close popup</a></div></div>";
                    //}
                }
            }
        }
    }

    protected void gvCorpFBList_DataBound(object sender, EventArgs e)
    {

    }
}
