
<%@ Page Language="C#" AutoEventWireup="true" Codebehind="HotelSearch.aspx.cs" Inherits="HotelSearch" 
    MasterPageFile="~/TransactionBE.master" EnableEventValidation="false" ValidateRequest="false" Trace="false" TraceMode="SortByTime" %>
    <%@ MasterType VirtualPath="~/TransactionBE.master"   %>

<%@ PreviousPageType VirtualPath="~/HotelResults.aspx" %> 

<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">
     <style type="text/css">
     .body_container {
         min-height: calc(100vh - 166px);
     }

	 .title-style{
        display:none !important;
    }
	.latestupdate {
		position: absolute;
		z-index: 10;
		right: 0;
		left: 0;
		padding: 3px 5px 5px 5px;
		top: 0;
   }
	.latestupdate .alert.alert-notice {
		background-color: #fdd75b;
		border-color: #fdd75b;
		color: #20204a;
		padding:7px 15px;
	}

	.latestupdate .alert-icon {
		color: #fa3535;
		float: left;
		display: block;
		margin-top: -4px;
		width: auto;
		height: 100%;
		text-align: center;
		font-size:18px;
        font-weight:bolder  ;
	}
	.alert.alert-notice .alert-icon {
		background-color: #fed750;
	}	
	.latestupdate .alert-message {
		padding-left: 20px;
		color: #000;
		display: block;
		padding-right: 40px;
		margin-left: 18px;
        font-weight:bolder;
	}
	.latestupdate .latest-update-title {
		color: #e40a5a;
		    line-height: .5;

	}
    
.disabled {
    color: lightgray;
    pointer-events: none;
}
    </style>
    <link href="build/css/owl-carousel/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href="build/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
<%--    <link href="css/select2.css" rel="stylesheet" />--%>

    <%--<link href="css/main-style.css" rel="stylesheet" type="text/css" /> for theme--%>
    <input id="hdfCalendarSource" type="hidden" value="<% = calendarSource %>" />
    <input id="SessionId" type="hidden" value="<% = sessionIdValue %>" />
    <input id="UTCTime" type="hidden" value="<% = DateTime.Now.ToUniversalTime().ToString("dd/MM/yyyy") %>" />
    <input id="ISTTime" type="hidden" value="<% = BookingUtility.GetIST().ToString() %>" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/fontawesome/css/font-awesome.min.css" rel="stylesheet" />

<script src="Scripts/MultiCity.js" type="text/javascript" defer="defer"></script>
    <script src="Scripts/PrefAirline.js" type="text/javascript" defer="defer"></script>

    <script src="build/js/owl.carousel.min.js" type="text/javascript"></script>
    
    <%--<link rel="stylesheet" href="css/style.css"> for Theme --%>


    <%--<script type="text/javascript" src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>

    <script type="text/javascript" src="js/organictabs.jquery.js"></script>--%>
    
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#divVisaEnable').hide();
//            $('.swap-btn').click(function () {
//                $(this).toggleClass('toggled');
//            })
            $("#Additional-Link").click(function() {
            $("#Additional-Div").toggle();
            });
        });
        //To Validate Duplicate Preferred Airline
        function ValidateAirline(id) {
            var isValid = true;
            var prefAir1 = document.getElementById('txtPreferredAirline' + id).value;
            if (document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value.trim().length > 0 && prefAir1.trim().length > 0) {
                if (document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value == prefAir1) {
                    errMess.style.display = 'block';
                    errorMessage.innerHTML = 'Duplicate Preferred Airline not allowed';
                    document.getElementById('txtPreferredAirline' + id).focus();
                    document.getElementById('txtPreferredAirline' + id).value = "";
                    isValid = false;
                }
                else if (document.getElementById('txtPreferredAirline' + eval(id - 1)) != null && document.getElementById('txtPreferredAirline' + eval(id - 1)).value == prefAir1) {
                    errMess.style.display = 'block';
                    errorMessage.innerHTML = 'Duplicate Preferred Airline not allowed';
                    document.getElementById('txtPreferredAirline' + id).focus();
                    document.getElementById('txtPreferredAirline' + id).value = "";
                    isValid = false;
                } 
            }
            return isValid;
        }
        //To Remove input preferred airline code from airlineCode hidden field
        //when preferred airline cleared from textbox
        function RecheckAirline(index) {
            
            if (index == 0 && document.getElementById('<%=txtPreferredAirline.ClientID %>').value != '')
                return;

            if (index > 0 && document.getElementById('txtPreferredAirline' + (index - 1)) != null && document.getElementById('txtPreferredAirline' + (index - 1)).value != '')
                return;

            var airlineCodes = document.getElementById('<%=airlineCode.ClientID %>').value.split(',');
            airlineCodes.splice((index), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
            document.getElementById('<%=airlineCode.ClientID %>').value = arCodes;
        }
        function SetModifyValues() {
            var rooms = eval('<%=reqObj.NoOfRooms %>');
            var adults = '<%=adults %>';
            var childs = '<%=childs %>';

            var roomAdults = adults.split(',');
            var roomChilds = childs.split(',');
            var childAges = '<%=childAges %>';
            childAges = childAges.split(',');
            //document.getElementById('NoOfRooms').value = rooms;
            document.getElementById('roomCount').value = rooms;            
                        $('#roomCount').val(rooms);//using jQuery
            ShowRoomDetails();
            
                
            
        //document.getElementById('rating').value = '<%=((int)reqObj.Rating).ToString() %>';
            var age = eval(0);
            for (i = 1; i <= rooms; i++) {
                for (j = 0; j < roomAdults.length; j++) {
                    if (roomAdults[j].split('-')[0] == i) {
                        document.getElementById('adtRoom-' + i).value = roomAdults[j].split('-')[1];
                    }
                }
                for (k = 0; k < roomChilds.length; k++) {
                    if (roomChilds[k].split('-')[0] == i) {
                        document.getElementById('chdRoom-' + i).value = roomChilds[k].split('-')[1];
                        ShowChildAge(k + 1);
                        var chd = eval(roomChilds[k].split('-')[1]);
                        for (l = 0; l < chd; l++) {
                            document.getElementById('ChildBlock-' + (k + 1) + '-ChildAge-' + (l + 1)).value = childAges[age];
                            age++;
                        }
                    }
                }
            }
            hotelPaxCount();

        }
    </script>
    <script language="javascript">
        var cntr = 0;
        var rid = 3;

        //add a new row to the table
        function addRow() {
            //add a row to the rows collection and get a reference to the newly added row
            //            var newRow = document.all("tblGrid").insertRow(document.all("tblGrid").rows.length - 1);
            //            /*alert(cntr);*/
            //            //add 3 cells (<th>) to the new row and set the innerHTML to contain text boxes
            //            
            //            var oCell = newRow.insertCell(0);
            //            oCell.innerHTML = "<input style='width:110px' class='auto-list' type='text' name='city" + tid + "' id='city" + tid + "' ><div style='width:250px;display:none' id='citycontainer" + tid + "'></div>";

            //            oCell = newRow.insertCell(1);
            //            oCell.innerHTML = "<input type='text' style='width:90px' class='auto-list' value='DD MM YYYY' name='t" + cid + "' id='t" + cid + "'> <a href='javascript:void(null)' onclick='showMultiCityCal" + cid + "()'><img id='dateLink1' src='images/call-cozmo.png' alt='Pick Date' style='vertical-align: middle;margin: 1px' /></a>";
            //            tid = tid + 1;
            //            cid = cid + 1;
            //            oCell = newRow.insertCell(2);
            //            oCell.innerHTML = "<input style='width:110px' class='auto-list' type='text' name='city" + tid + "' id='city" + tid + "' ><div style='width:250px;display:none' id='citycontainer" + tid + "'></div>";

            //            oCell = newRow.insertCell(3);
            //            oCell.innerHTML = "<input type='image'  name='imageField' id='imageField' src='images/minus.gif' onClick='removeRow(this);'>";
            document.getElementById('tblRow' + rid).style.display = 'block';
            if (rid < 6) {
                rid = rid + 1;
            }

            cntr = cntr + 1;
            //alert(cntr);
            if (cntr == 4) {
                document.getElementById('btnAddRow').style.display = 'none';
            }

        }

        //deletes the specified row from the table
        function removeRow(src) {

            //alert(cntr);
            //            var oRow = src.parentElement.parentElement;


            //            document.all("tblGrid").deleteRow(oRow.rowIndex);
            document.getElementById('tblRow' + src).style.display = 'none';
            if (src == 3) {
                document.getElementById('ctl00_cphTransaction_Time3').value = "";
                document.getElementById('ctl00_cphTransaction_City5').value = "";
                document.getElementById('ctl00_cphTransaction_City6').value = "";
            }
            else if (src == 4) {
                document.getElementById('ctl00_cphTransaction_Time4').value = "";
                document.getElementById('ctl00_cphTransaction_City7').value = "";
                document.getElementById('ctl00_cphTransaction_City8').value = "";
            }
            else if (src == 5) {
                document.getElementById('ctl00_cphTransaction_Time5').value = "";
                document.getElementById('ctl00_cphTransaction_City9').value = "";
                document.getElementById('ctl00_cphTransaction_City10').value = "";
            }
            else {
                document.getElementById('ctl00_cphTransaction_Time6').value = "";
                document.getElementById('ctl00_cphTransaction_City11').value = "";
                document.getElementById('ctl00_cphTransaction_City12').value = "";
            }
            rid = src;
            cntr = cntr - 1;
            if (cntr < 4)
                document.getElementById('btnAddRow').style.display = 'block';



        }

        //Adding prefered Airline Added by brahmam 14.10.2016
        function AddPrefAirline() {

            //show upto 3 preferred airline div's
            if (document.getElementById('divPrefAirline0').style.display == "none") {
                document.getElementById('divPreferredAirline0').style.display = 'block';
                document.getElementById('divPrefAirline0').style.display = 'block';
                document.getElementById('removeRowLink0').style.display = 'block';
                document.getElementById('txtPreferredAirline0').value = '';
            } else if (document.getElementById('divPrefAirline1').style.display == "none") {
                document.getElementById('divPreferredAirline1').style.display = 'block';
                document.getElementById('divPrefAirline1').style.display = 'block';
                document.getElementById('removeRowLink1').style.display = 'block';
                document.getElementById('txtPreferredAirline1').value = '';
            }
            else if (document.getElementById('divPrefAirline2').style.display == "none") {
                document.getElementById('divPreferredAirline2').style.display = 'block';
                document.getElementById('divPrefAirline2').style.display = 'block';
                document.getElementById('removeRowLink2').style.display = 'block';
                document.getElementById('txtPreferredAirline2').value = '';
            }
            if (document.getElementById('divPrefAirline0').style.display == "block" && document.getElementById('divPrefAirline1').style.display == "block" && document.getElementById('divPrefAirline2').style.display == "block") {
                document.getElementById('addRowLink').style.display = 'none';
            }
        }
        //removing prefered Airline Added by brahmam 14.10.2016
        function RemovePrefAirline(index) {
            //Just clear the previously entered values.
            if (document.getElementById('txtPreferredAirline' + index) != null) {

                document.getElementById('txtPreferredAirline' + index).value = "";
            }
            document.getElementById('divPreferredAirline' + index).style.display = 'none';
            document.getElementById('divPrefAirline' + index).style.display = 'none';
            document.getElementById('removeRowLink' + index).style.display = 'none';
            document.getElementById('addRowLink').style.display = 'block';
            var airlineCodes = document.getElementById('<%=airlineCode.ClientID %>').value.split(',');
            airlineCodes.splice((index + 1), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
            document.getElementById('<%=airlineCode.ClientID %>').value = arCodes;
        }
		
		</script>

    <script type="text/javascript">
        function selectHotelSources() {
            var active = eval(document.getElementById('<%=hdnActiveSources.ClientID %>').value);
            var selected;
            if (document.getElementById('ctl00_cphTransaction_chkSource0') != null) {
                if (document.getElementById('ctl00_cphTransaction_chkSource0').checked) {
                    for (i = 1; i < active; i++) {
                        document.getElementById('ctl00_cphTransaction_chkSource' + i).checked = true;
                    }
                }
                else {
                    for (i = 1; i < active; i++) {
                        document.getElementById('ctl00_cphTransaction_chkSource' + i).checked = false;
                    }
                }
            }


        }


        function hotelSourceValidate() {
            //============   Added by somasekhar 
            var supplier = null;
            var rooms = document.getElementById('roomCount');   
            var adltCount = 0;
            var chldCount = 0; 
            var oyoAdltCount = 0;
            var oyoChldCount = 0;
            var roomCount = eval(rooms.options[rooms.selectedIndex].value);
            if (rooms != null) {
                for (a = 1; a <= roomCount; a++) {
                    if(document.getElementById('adtRoom-' + a).value!=null){
                        adltCount += parseInt(document.getElementById('adtRoom-' + a).value);
                        oyoAdltCount += parseInt(document.getElementById('adtRoom-' + a).value);
                    }
                    if(document.getElementById('chdRoom-' + a).value!=null){
                        chldCount += parseInt(document.getElementById('chdRoom-' + a).value);
                        var childcount = document.getElementById('chdRoom-' + a).value;// For  OYO  
                        for (var c = 1; c <= childcount; c++) {
                            if (document.getElementById('ChildBlock-' + a + '-ChildAge-' + c).value != null && parseInt(document.getElementById('ChildBlock-' + a + '-ChildAge-' + c).value) > 5) {
                                oyoChldCount += 1; 
                            }
                         }
                    }
                }
            }             
            //========= End =======
            var active = eval(document.getElementById('<%=hdnActiveSources.ClientID %>').value);
            var counter = 0;
            for (i = 1; i < active; i++) {
                if (document.getElementById('ctl00_cphTransaction_chkSource' + i) != null && document.getElementById('ctl00_cphTransaction_chkSource' + i).checked) {
                    counter++;
                    supplier = document.getElementById('ctl00_cphTransaction_chkSource' + i).nextSibling.innerHTML;                                     
                }
            }
            if (('<%=Settings.LoginInfo.AgentType %>') == '<%=AgentType.B2B%>' || ('<%=Settings.LoginInfo.AgentType %>') == '<%=AgentType.B2B2B%>') {
                counter = 1;
            }



            if (counter == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
                return false;
            }
              //============   Added by somasekhar  
           // debugger;
            if (counter==1 && supplier!=null) {
                // In our Hotel Booking system we are restrincting to search max rooms upto 4  and max pax upto 9
                     //for RezLive --This suppliers are  allowing to book a hotel when  max rooms upto 3 only
                    if ( ( roomCount> 3 || adltCount > 9 ) && supplier == "RezLive" ) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Won't allow to book when MaxRoom > 3 or MaxPax > 9";
                        return false;
                    }
                    //for LOH --This suppliers are  allowing to book a hotel when  max rooms upto 3 only
                   else if ( ( roomCount> 3 || adltCount > 9 ) &&   supplier=="LOH") {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Won't allow to book when MaxRoom > 3 or MaxPax > 9";
                        return false;
                    }
                     //for DOTW  -- This supplier is allowing to book a hotel when max pax count upto 14 only (including childs count)
                   else if ( ( adltCount + chldCount ) > 14 &&  supplier == "DOTW" ) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Won't allow to book when  MaxPax > 14";
                        return false;
                    }
                     //for OYO  -- This supplier is allowing to book a hotel when max count less than (3 * No.of Rooms). Note : Chiled Age upto 5 Yrs only (if age above 5 then treated as pax)  
                    else if ( ( roomCount > (oyoAdltCount + oyoChldCount) || (oyoAdltCount + oyoChldCount)  > (3 *roomCount) ) &&  supplier == "OYO" ) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Won't allow to book when  Max Pax > 9, Max Pax > (3 * No of Rooms) and Chiled Age upto 5 Yrs only";
                        return false;
                    }
                    
                     }
              // In our Hotel Booking system we are restrincting to search  max pax upto 
         if ((oyoAdltCount + oyoChldCount) > 9 || adltCount > 9) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Won't allow to book when  MaxPax > 9";
                         
                        return false;
                    }
            //========= End =======
            return true;
        }

        function selectFlightSources() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                if (document.getElementById('<%=chkFlightAll.ClientID %>') != null) {
                    if (document.getElementById('<%=chkFlightAll.ClientID %>').checked) {
                        for (i = 0; i < checkbox.length; i++) {
                            document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).checked = true;
                        }
                    }
                    else {
                        for (i = 0; i < checkbox.length; i++) {
                            document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).checked = false;
                        }
                    }
                }
            }
        }


        function flightSourceValidate() {
            var chcksup = false;
            var pkFares = false;
            var sourceCount =0;
            var sourceId = '';
            $('#divchkSuppliers input:checked').each(function () {
                if (this.style.display != 'none')
                    chcksup = true;
            });

            if (!chcksup) {
               document.getElementById('errMess').style.display = "block";
               document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
            }
            //Go Air: Allows only 6 Passengers to book[Adult+Child+Infant]
            //Go Air : Restrict the search if there are more than 6 passengers.
            if (chcksup) {
                var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
                var passerngerCount = eval(document.getElementById('<%=ddlAdults.ClientID %>').value) + eval(document.getElementById('<%=ddlChilds.ClientID %>').value) + eval(document.getElementById('<%=ddlInfants.ClientID %>').value);
                if (chkSupp != null) {
                    var checkbox = chkSupp.getElementsByTagName("input");
                    for (var i = 0; i < checkbox.length; i++) {
                        if (checkbox[i].checked) {
                            if (document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).nextSibling.innerHTML == "G8") {
                                if (passerngerCount > 6) {
                                    document.getElementById('errMess').style.display = "block";
                                    document.getElementById('errorMessage').innerHTML = "GoAir Doesn't allow morethan 6 passengers";
                                    chcksup = false;
                                }
                            }
                            else if (checkbox[i] != undefined && checkbox[i].nextElementSibling.innerText.toUpperCase()=="PK" && parseInt($('#ctl00_cphTransaction_ddlInfants').val())>0)
                            {
                                pkFares = true;    
                                chcksup = false;    
                                sourceId =checkbox[i].id;                     
                            } 
                            sourceCount++;
                        }
                    }
                }
            }
            if (pkFares && sourceCount ==1)
            {
                document.getElementById('errMess').style.display = "block";
                $('#errorMessage').text('Infants are not allowed currently in Search for PK. Please modify your search.');                       
                chcksup = false;
            }
            else if (pkFares && sourceCount >1)
            {
                  $('#'+sourceId).prop('checked', false); 
            }



            return chcksup;
        }

        function setCheck(ctrl) {
            if (document.getElementById(ctrl).checked == false) {
                document.getElementById('ctl00_cphTransaction_chkSource0').checked = false;
            }
        }
        function setUncheck() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                for (var i = 0; i < checkbox.length; i++) {
                    if (!checkbox[i].checked) {
                        document.getElementById("<%=chkFlightAll.ClientID%>").checked = false;
                    }
                }
            }

        }


        function HotelSearch() {
           
            document.getElementById('container2').style.display = "none";
           
            if (('<%=Settings.LoginInfo.MemberType %>') == '<%=MemberType.ADMIN%>') {
                if (document.getElementById('<%=rbtnAgent.ClientID %>') !=null && document.getElementById('<%=rbtnAgent.ClientID %>').checked) {
                    if (document.getElementById('<%=ddlAgents.ClientID %>').selectedIndex <= 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Client from the list";
                        return false;
                    }
                }
            }
            if (CheckCity() && checkDates() && checkRooms() && hotelSourceValidate()) {

                if (document.getElementById('<%=rbtnAgent.ClientID %>') !=null && document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAgents.ClientID %>').value != "-1"
                    && document.getElementById('<%=ddlHotelAgentLocations.ClientID %>').value != "-1") {
                    document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlHotelAgentLocations.ClientID %>').value;

                }
                var active = eval(document.getElementById('<%=hdnActiveSources.ClientID %>').value);
                var selected = "";
                for (i = 1; i <= active - 1; i++) {
                    if (document.getElementById('ctl00_cphTransaction_chkSource' + i) != null && document.getElementById('ctl00_cphTransaction_chkSource' + i).checked) {
                        if (selected.length > 0) {
                            selected += "," + document.getElementById('ctl00_cphTransaction_chkSource' + i).nextSibling.innerHTML;
                        }
                        else {
                            selected = document.getElementById('ctl00_cphTransaction_chkSource' + i).nextSibling.innerHTML;
                        }
                    }
                }
                document.getElementById('<%=hdnSelectedSources.ClientID %>').value = selected;

                document.getElementById('PreLoader').style.display = "block";
                document.getElementById('MainDiv').style.display = "none";

                //Detecting only IE Browser..
                if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                    document.getElementById('imgHotelLoading').innerHTML = document.getElementById('imgHotelLoading').innerHTML;
                }
                document.getElementById('<%=hdnSubmit.ClientID %>').value = "search"
                 return true;
            }
            else {
                return false;
            }
        }

        function CheckCity() {
            var submit = false;
            var city = document.getElementById('city');
            var countryArr = city.value.split(',');
            if (city.value != "Enter city name") {
                submit = true;
                document.getElementById('searchCity').innerHTML = city.value;
                if (countryArr[1].toUpperCase() == "UNITED ARAB EMIRATES") {
                    document.getElementById('isUAE').innerHTML = "Tourism Dirham Charge to be paid directly by the client to hotel before check out (AED 10- 20 per room per night). Applicable for Check in from 31st March onward..";
                }
            }
            else {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Enter a City";
                submit = false;
            }
            return submit;
        }

        function checkDates() {
            document.getElementById('errMess').style.display = "none";


            var date1 = document.getElementById('<%= CheckIn.ClientID %>').value;
            var date2 = document.getElementById('<%= CheckOut.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
                return false;
            }
            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check In Date";
                return false;
            }
            var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            if (todaydate.getTime() > cInDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check In Date should be greater than equal to todays date";
                return false;
            }

            if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check Out Date";
                return false;
            }
            var retDateArray = date2.split('/');

            // checking if date2 is valid	
            if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check Out Date";
                return false;
            }
            var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            if (todaydate.getTime() > cOutDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check Out Date should be greater than equal to todays date";
                return false;
            }

            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckOut date should be greater than  or equal to CheckIn date";
                return false;
            }

            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckIn date and CheckOut date could not be same";
                return false;
            }

            var nationality = document.getElementById('<%=ddlNationality.ClientID %>');
            var country = document.getElementById('<%=ddlResidence.ClientID %>');


            if (nationality.options[nationality.selectedIndex].value == "Select Nationality") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select your Nationality";
                return false;
            }

            if (country.options[country.selectedIndex].value == "Select Country") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select your residing Country";
                return false;
            }
            document.getElementById('fromDate').innerHTML = date1;
            document.getElementById('toDate').innerHTML = date2;

            return true;
        }

        function checkRooms() {
            var submit = true;
            var rooms = document.getElementById('roomCount');
            var room = parseInt(rooms.options[rooms.selectedIndex].value);

            if (room > 0) {
                for (var k = 1; k <= room; k++) {
                    if (submit == false) {
                        break;
                    }
                    if (document.getElementById("adtRoom-" + k).value == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Enter Room " + k + "Details!!";
                        submit = false;
                    }

                    if (document.getElementById("chdRoom-" + k).value > 0) {
                        for (var j = 1; j <= document.getElementById("chdRoom-" + k).value; j++) {
                            var str = "ChildBlock-" + k + "-ChildAge-" + j;
                            //                            str += parseInt(k);
                            //                            str += parseInt(j);
                            if (document.getElementById(str).value < 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter Child-" + (parseInt(j)) + "  Age Details in Room " + (parseInt(k)) + "!!";
                                submit = false;
                                break;
                            }

                        } //for inner for
                    } //end if
                }  //end for
            }
            else {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 room";
                submit = false;
            }

            return submit;
        }


        function FlightSearch() {

            if (CheckCorporateEntries() && CheckFlightCities() && checkFlightDates()) {

                if (eval(document.getElementById('<%=ddlAdults.ClientID %>').value) < eval(document.getElementById('<%=ddlInfants.ClientID %>').value)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Infant count should not be greater than adult count";
                    return false;
                } else if (document.getElementById('divPrefAirline0').style.display == 'block' && document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your first preferred airline";
                    document.getElementById('ctl00_cphTransaction_txtPreferredAirline').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline0').style.display == 'block' && document.getElementById('txtPreferredAirline0').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your second preferred airline";
                    document.getElementById('txtPreferredAirline0').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline1').style.display == 'block' && document.getElementById('txtPreferredAirline1').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your third preferred airline";
                    document.getElementById('txtPreferredAirline1').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline2').style.display == 'block' && document.getElementById('txtPreferredAirline2').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your fourth preferred airline";
                    document.getElementById('txtPreferredAirline2').focus();
                    return false;
                }
                else if ((eval(document.getElementById('<%=ddlAdults.ClientID %>').value) + eval(document.getElementById('<%=ddlInfants.ClientID %>').value) + eval(document.getElementById('<%=ddlChilds.ClientID %>').value)) > 9) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Passengers count should not be greater than 9 ";
                    return false;
                }
                else if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null) {// For policy Entilment checking
                    var travelReason = document.getElementById('<%=ddlFlightTravelReasons.ClientID %>');

                    if (travelReason.options[travelReason.selectedIndex].text == "Entitlement" && document.getElementById('ctl00_cphTransaction_DepDate').value.length > 0) {
                        var depVal = document.getElementById('ctl00_cphTransaction_DepDate').value;
                        var departure = depVal.split('/');
                        var depDate = new Date(departure[2], eval(departure[1]) - 1, departure[0]);
                        var today = new Date();
                        var validDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 30);
                        if (validDate > depDate) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please select Travel Date after 30 days from current date";
                            document.getElementById('ctl00_cphTransaction_DepDate').focus();
                            return false;
                        }
                    }
                }
                

                else if (!ValidateAirline(0)) 
                    return false;                
                else if (!ValidateAirline(1)) 
                    return false;                
                else if (!ValidateAirline(2)) 
                    return false;   
                
                if (flightSourceValidate()) {
                    document.getElementById('FlightPreLoader').style.display = "block";
                    document.getElementById('MainDiv').style.display = "none";
                    document.getElementById('divPrg').style.display = "none";

                    //detecting all IE vesions of browsers
                    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                        document.getElementById('imgFlightLoading').innerHTML = document.getElementById('imgFlightLoading').innerHTML;
                    }
                    $('SearchResultLoad').context.styleSheets[0].display = "block";
                    document.getElementById('<%=hdnSubmit.ClientID %>').value = "searchFlight";

                    //Added by lokesh.Based on this location agent id, Corresponding GST number is auto populated for SG and 6E.
                    if (document.getElementById('<%=radioAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAirAgents.ClientID %>').value != "-1"
                        && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value != "-1")
                    {
                       document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value;

                    }

                    return true;
                }
                else {
                    return false;
                }
            }   
            else {
                return false;
            }
        }

        function CheckCorporateEntries() {
            document.getElementById('errMulti').style.display = "none";
            document.getElementById('errMess').style.display = "none"
            if ($('#CorpDiv').is(':visible')) {
                if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').value == "-1") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Reason for Travel";
                    document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=ddlFlightEmployee.ClientID %>') != null && document.getElementById('<%=ddlFlightEmployee.ClientID %>').value == "Select Traveller") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Traveller / Employee";
                    document.getElementById('<%=ddlFlightEmployee.ClientID %>').focus();
                    return false;
                }

                else {

                    document.getElementById('errorMessage').innerHTML = "";
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter first City";
                    return true;

                }
            }
            else
                return true;
        }

        function CheckFlightCities() {
            document.getElementById('errMulti').style.display = "none";
            document.getElementById('errMess').style.display = "none"
            if (document.getElementById('<%=hdnWayType.ClientID %>').value != "multiway") {
                var origin = document.getElementById('<%=Origin.ClientID %>').value;
                var dest = document.getElementById('<%=Destination.ClientID %>').value;

                if (origin.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a Origin City";
                    return false;
                }
                else if (dest.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a Destination City";
                    return false;
                }
                else if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAirAgents.ClientID %>').value == "-1")
                {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Client !";
                    return false;
                }
                else if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "-1")
                {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Location !";
                    return false;
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                    var arrOrigin = origin.split(',')[0].split(')')[1];
                    var arrDest = dest.split(',')[0].split(')')[1];
                    document.getElementById('flightOnwards').innerHTML = origin.substring(1, 4) + ' ' + '(' + arrOrigin + ')' + ' - ' + dest.substring(1, 4) + ' ' + '(' + arrDest + ')';
                    document.getElementById('flightReturn').innerHTML = dest.substring(1, 4) + ' ' + '(' + arrDest + ')' + ' - ' + origin.substring(1, 4) + ' ' + '(' + arrOrigin + ')';
                    if (document.getElementById('<%=hdnWayType.ClientID %>').value == "oneway") {
                        document.getElementById('twowaytable').style.display = 'none';
                    }
                    //                    if (window.navigator.appName == "Netscape") {
                    //                        setTimeout('document.images.imgPreloader.src = "images/preloader11.gif"', 200); 
                    //                    }
                    return true;
                }
            }
            else {
                var city1 = document.getElementById('<%=City1.ClientID%>').value;
                var city2 = document.getElementById('<%=City2.ClientID%>').value;
                var city3 = document.getElementById('<%=City3.ClientID%>').value;
                var city4 = document.getElementById('<%=City4.ClientID%>').value;
                var city5 = document.getElementById('<%=City5.ClientID%>').value;
                var city6 = document.getElementById('<%=City6.ClientID%>').value;
                var city7 = document.getElementById('<%=City7.ClientID%>').value;
                var city8 = document.getElementById('<%=City8.ClientID%>').value;
                var city9 = document.getElementById('<%=City9.ClientID%>').value;
                var city10 = document.getElementById('<%=City10.ClientID%>').value;
                var city11 = document.getElementById('<%=City11.ClientID%>').value;
                var city12 = document.getElementById('<%=City12.ClientID%>').value;

                if (city1.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter first City";
                    document.getElementById('<%=City1.ClientID%>').focus();
                    return false;
                } else if (city2.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter second City";
                    document.getElementById('<%=City2.ClientID%>').focus();
                    return false;
                } else if (city3.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter third City";
                    document.getElementById('<%=City3.ClientID%>').focus();
                    return false;
                } else if (city4.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter fourth City";
                    document.getElementById('<%=City4.ClientID%>').focus();
                    return false;
                } else if (tblRow3.style.display == 'block' && city5.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 5th City";

                    document.getElementById('<%=City5.ClientID%>').focus();
                    return false;
                } else if (tblRow3.style.display == 'block' && city6.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 6th City";

                    document.getElementById('<%=City6.ClientID%>').focus();
                    return false;
                } else if (tblRow4.style.display == 'block' && city7.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 7th City";

                    document.getElementById('<%=City7.ClientID%>').focus();
                    return false;
                } else if (tblRow4.style.display == 'block' && city8.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 8th City";

                    document.getElementById('<%=City8.ClientID%>').focus();
                    return false;
                } else if (tblRow5.style.display == 'block' && city9.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 9th City";

                    document.getElementById('<%=City9.ClientID%>').focus();
                    return false;
                } else if (tblRow5.style.display == 'block' && city10.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 10th City";

                    document.getElementById('<%=City10.ClientID%>').focus();
                    return false;
                } else if (tblRow6.style.display == 'block' && city11.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 11th City";

                    document.getElementById('<%=City11.ClientID%>').focus();
                    return false;
                } else if (tblRow6.style.display == 'block' && city12.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 12th City";

                    document.getElementById('<%=City12.ClientID%>').focus();
                    return false;
                }
                else {
                    document.getElementById('errMulti').style.display = "none";

                    var success = true;
                    document.getElementById('onetwoDiv').style.display = 'none';
                    document.getElementById('multiDiv').style.display = 'block';
                    var arrCity1 = city1.split(',')[0].split(')')[1];
                    var arrCity2 = city2.split(',')[0].split(')')[1];
                    var arrCity3 = city3.split(',')[0].split(')')[1];
                    var arrCity4 = city4.split(',')[0].split(')')[1];
                    document.getElementById('multiway1').innerHTML = city1.substring(1, 4) + ' ' + '(' + arrCity1 + ')' + ' - ' + city2.substring(1, 4) + ' ' + '(' + arrCity2 + ')';
                    document.getElementById('multiway2').innerHTML = city3.substring(1, 4) + ' ' + '(' + arrCity3 + ')' + ' - ' + city4.substring(1, 4) + ' ' + '(' + arrCity4 + ')';
                    var tdIndex = 3;
                    for (var i = 5; i < 13; i++) {
                        if (document.getElementById('tblRow' + tdIndex).style.display == "block") {
                            var addlCity1 = document.getElementById('ctl00_cphTransaction_City' + i).value;
                            var addlCity2 = document.getElementById('ctl00_cphTransaction_City' + (i + 1)).value;
                            var arraddlCity1 = addlCity1.split(',')[0].split(')')[1];
                            var arraddlCity2 = addlCity2.split(',')[0].split(')')[1];

                            if (addlCity1.length <= 0 || addlCity2.length <= 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter all Cities";
                                success = false;
                                break;
                            }
                            document.getElementById('multiwayable' + tdIndex).style.display = "block";
                            document.getElementById('multiway' + tdIndex).innerHTML = addlCity1.substring(1, 4) + ' ' + '(' + arraddlCity1 + ')' + ' - ' + addlCity2.substring(1, 4) + ' ' + '(' + arraddlCity2 + ')';

                            tdIndex++;
                        }
                        i++;
                    }
                    //                    if (window.navigator.appName == "Netscape") {
                    //                        setTimeout('document.images.imgPreloader.src = "images/preloader11.gif"', 200);
                    //                    }

                    document.getElementById('imgPreloader').src = "images/preloaderFlight.gif";                  
                    return true;
                    

                }
            }
        }
        function checkFlightDates() {
            document.getElementById('errMulti').style.display = "none";
            document.getElementById('errMess').style.display = "none"
            if (document.getElementById('<%=hdnWayType.ClientID %>').value != "multiway") {
                var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
                var date2 = document.getElementById('<%=ReturnDateTxt.ClientID %>').value;
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                document.getElementById('deptDate').innerHTML = date1;
                document.getElementById('arrivalDate').innerHTML = date2;
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Departure Date";
                    return false;
                }
                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                // Removing same day validation
//                if (todaydate.getTime() > cInDate.getTime()) {
//                    document.getElementById('errMess').style.display = "block";
//                    document.getElementById('errorMessage').innerHTML = " Departure Date should be greater than equal to todays date";
//                    return false;
//                }
                if (document.getElementById('<%=roundtrip.ClientID%>').checked == true) {
                    if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Return Date";
                        return false;
                    }

                    var retDateArray = date2.split('/');

                    // checking if date2 is valid	
                    if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = " Invalid Return Date";
                        return false;
                    }
                    var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    if (todaydate.getTime() > cOutDate.getTime()) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Return Date should be greater than equal to todays date";
                        return false;
                    }

                    var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    var difference = returndate.getTime() - depdate.getTime();

                    if (difference < 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Departure date should be greater than or equal to Return date";
                        return false;
                    }
                }

                return true;
            }
            else if (document.getElementById('<%=hdnWayType.ClientID %>').value == "multiway") {

                var date1 = document.getElementById('ctl00_cphTransaction_Time1').value;
                var date2 = document.getElementById('ctl00_cphTransaction_Time2').value;
                var date3 = document.getElementById('ctl00_cphTransaction_Time3').value;
                var date4 = document.getElementById('ctl00_cphTransaction_Time4').value;
                var date5 = document.getElementById('ctl00_cphTransaction_Time5').value;
                var date6 = document.getElementById('ctl00_cphTransaction_Time6').value;

                if (date1.length == 0 || date2.length == 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                    if (date1.length == 0) {
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                    }
                    else {
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                    }
                    return false;
                }
                else if (date1.length > 0 && date2.length > 0) {
                    this.today = new Date();
                    var thisMonth = this.today.getMonth();
                    var thisDay = this.today.getDate();
                    var thisYear = this.today.getFullYear();

                    var todaydate = new Date(thisYear, thisMonth, thisDay);
                    if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                        return false;
                    }
                    var depDateArray = date1.split('/');

                    // checking if date1 is valid		    
                    if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                        return false;
                    }
                    var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    if (todaydate.getTime() > cInDate.getTime()) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Departure Date should be greater than equal to todays date";
                        return false;
                    }

                    if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Return Date";
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                        return false;
                    }

                    var retDateArray = date2.split('/');

                    // checking if date2 is valid	
                    if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                        return false;
                    }
                    var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    if (todaydate.getTime() > cOutDate.getTime()) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Return Date should be greater than equal to todays date";
                        return false;
                    }

                    var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    var difference = returndate.getTime() - depdate.getTime();

                    if (difference < 0) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Departure date should be greater than or equal to Return date";
                        return false;
                    }

                    if ((tblRow3.style.display == 'block' && date3.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time3').focus();
                        return false;
                    }
                    else if ((tblRow4.style.display == 'block' && date4.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time4').focus();
                        return false;
                    }
                    else if ((tblRow5.style.display == 'block' && date5.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time5').focus();
                        return false;
                    }
                    else if ((tblRow6.style.display == 'block' && date6.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time6').focus();
                        return false;
                    }
                    else if ((tblRow3.style.display == 'block' && date3.length > 0)) {
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        if (date3 != null && (date3 == "DD/MM/YYYY" || date3 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time3').focus();
                            return false;
                        }
                        var depDateArray = date3.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time3').focus();
                            return false;
                        }
                        //                       
                    }

                    else if ((tblRow4.style.display == 'block' && date4.length > 0)) {
                        if (date4 != null && (date4 == "DD/MM/YYYY" || date4 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time4').focus();
                            return false;
                        }
                        var retDateArray = date4.split('/');

                        // checking if date2 is valid	
                        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time4').focus();
                            return false;
                        }

                    }

                    else if ((tblRow5.style.display == 'block' && date5.length > 0)) {
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        if (date5 != null && (date5 == "DD/MM/YYYY" || date5 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time5').focus();
                            return false;
                        }
                        var depDateArray = date5.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time5').focus();
                            return false;
                        }

                    }

                    else if ((tblRow6.style.display == 'block' && date6.length > 0)) {
                        if (date6 != null && (date6 == "DD/MM/YYYY" || date6 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time6').focus();
                            return false;
                        }
                        var retDateArray = date6.split('/');

                        // checking if date2 is valid	
                        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time6').focus();
                            return false;
                        }

                    }

                    document.getElementById('multiDate1').innerHTML = date1;
                    document.getElementById('multiDate2').innerHTML = date2;
                    for (var k = 3; k <= 6; k++) {
                        if (document.getElementById('tblRow' + k).style.display == "block") {
                            document.getElementById('multiDate' + k).innerHTML = document.getElementById('ctl00_cphTransaction_Time' + k).value;
                        }
                    }
                }
                return true;

            }
        }


        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar
            cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
            //cal1 = new YAHOO.widget.Calendar("cal1", "Outcontainer1");
            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
            //            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCalendar1() {
            //cal2.hide();
            if (cal2 != null) cal2.hide();
            document.getElementById('container1').style.display = "block";
            document.getElementById('Outcontainer1').style.display = "block";
            document.getElementById('Outcontainer2').style.display = "none";
            //document.getElementById('Outcontainer2').style.display = "none";
        }
        var departureDate = new Date();
        function showCalendar2() {
            $('container1').context.styleSheets[0].display = "none";
            document.getElementById('Outcontainer1').style.display = "none";
            document.getElementById('Outcontainer2').style.display = "block";
            if (cal1 != null) cal1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();
            document.getElementById('Outcontainer1').style.display = "none";
//when we select date on Deperturedate,returndate calender shold be opened default
            showCalendar2();
        }
        function setDate2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 1) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                return false;
            }
            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
            document.getElementById('Outcontainer2').style.display = "none";
        }
        YAHOO.util.Event.addListener(window, "load", init);

        var call1;
        var call2;
        //-Flight Calender control
        function init1() {
            var today = new Date();
            // Rendering Cal1
            call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
            call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            call1.cfg.setProperty("title", "Select your desired departure date:");
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setFlightDate1);
            call1.render();
            // Rendering Cal2
            call2 = new YAHOO.widget.CalendarGroup("call2", "fcontainer2");
            //            call2.cfg.setProperty("title", "Select your desired return date:");
            call2.selectEvent.subscribe(setFlightDate2);
            call2.cfg.setProperty("close", true);
            call2.render();
        }

        function showFlightCalendar1() {
            call2.hide();
            document.getElementById('fcontainer1').style.display = "block";
            document.getElementById('fcontainer2').style.display = "none";

            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            if (date1.length > 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                cal1.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                cal1.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
            }
        }

        function showFlightCalendar2() {
            call1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            var date2 = document.getElementById('<%=ReturnDateTxt.ClientID %>').value;
            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                var retDateArray = date2.split('/');
                var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
                if (document.getElementById('<%=ReturnDateTxt.ClientID %>').value.length <= 0) {
                    call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate()) + "/" + depDateArray[2]);
                    call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                } else {
                    cal2.cfg.setProperty("selected", retDateArray[1] + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                    cal2.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate()) + "/" + depDateArray[2]);
                }
                call2.render();
            }
            document.getElementById('fcontainer2').style.display = "block";
        }

        function setFlightDate1() {
            var date1 = call1.getSelectedDates()[0];
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=DepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            call1.hide();
            //when we select date on Deperturedate,returndate calender shold be opened default
            if (document.getElementById('<%=roundtrip.ClientID %>') != null && document.getElementById('<%=roundtrip.ClientID %>').checked == true){
               showFlightCalendar2();
            }
        }

        function setFlightDate2() {
            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select departure date.";
                return false;
            }
            var date2 = call2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date1 + ")";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=ReturnDateTxt.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            call2.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init1);


        function getStates_Hotel(sQuery) {

            var paramList = 'searchKey=' + sQuery;

            if (document.getElementById('domesticHotel').checked == true) {

                paramList += '&requestSource=' + "HotelSearchDomestic";
            }
            else {
                paramList += '&requestSource=' + "HotelSearchInternational";
            }
            var url = "CityAjax";
            var arrayStates = "";
            var faltoo = getFile(url, paramList);
            arrayStates = faltoo.split('/');
            if (arrayStates[0] != "") {
                for (var i = 0; i < arrayStates.length; i++) {
                    if (arrayStates[i].split(',')[2].length > 0) {
                        arrayStates[i] = [arrayStates[i].split(',')[1] + ', ' + arrayStates[i].split(',')[2] + ', ' + arrayStates[i].split(',')[3], arrayStates[i]];
                    }
                    else {
                        arrayStates[i] = [arrayStates[i].split(',')[1] + ', ' + arrayStates[i].split(',')[3], arrayStates[i]];
                    }
                }

                return arrayStates;
            }
            else return (false);
        }

        //for added helper in source destination lookup

        function autoCompInitHotelCity() {
            // Instantiate third data source        
            oACDSHC = new YAHOO.widget.DS_JSFunction(getStates_Hotel); // temporarily autosearch deactivated
            // Instantiate third auto complete        
            oAutoCompHC = new YAHOO.widget.AutoComplete('city', 'statescontainer3', oACDSHC);
            oAutoCompHC.prehighlightClassName = "yui-ac-prehighlight";
            oAutoCompHC.queryDelay = 0;
            oAutoCompHC.minQueryLength = 3;
            oAutoCompHC.useIFrame = true;
            oAutoCompHC.useShadow = true;

            oAutoCompHC.formatResult = function(oResultItem, sQuery) {
                document.getElementById('statescontainer3').style.display = "block";
                var toShow = oResultItem[1].split(',');
                var sMarkup;
                if (toShow[2].length > 0) {
                    sMarkup = toShow[1] + ',' + toShow[2] + ',' + toShow[3];
                }
                else {
                    sMarkup = toShow[1] + ',' + toShow[3];
                }
                //var aMarkup = ["<li>", sMarkup, "</li>"]; 
                var aMarkup = [sMarkup];
                return (aMarkup.join(""));
            };
            oAutoCompHC.itemSelectEvent.subscribe(itemSelectHandlerHC);
        }
        var itemSelectHandlerHC = function(sType2, aArgs2) {

            YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
            var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
            var city = oMyAcInstance2[1].split(',');
            document.getElementById('CityCode').value = city[0];
            document.getElementById('CountryName').value = city[3];
            document.getElementById('city').value = city[1] + ',' + city[3];
            document.getElementById('statescontainer3').style.display = "none";
            var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
            var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
        };
        YAHOO.util.Event.addListener(this, 'load', autoCompInitHotelCity); //temporarily commented
        
    </script>

    <script type="text/javascript">


        // on checking self from Booking for section
        //        function SelectBookingFor() {
        //            if (document.getElementById('<%= rbtnSelf.ClientID %>').checked == true) {
        //                $('BookingAgencyID').value = $('hdfAdminId').value;

        //            }
        // }
        function setRadioButton(radiobutton) {
            document.getElementById(radiobutton).checked = true;
            return;
        }
        function ShowRoomDetails() {

            //var f=document.getElementById;

            var rooms = document.getElementById('roomCount');
            if (rooms != null) {
                var count = eval(rooms.options[rooms.selectedIndex].value);
                if (document.getElementById('PrevNoOfRooms') != null) {
                    var prevCount = eval(document.getElementById('PrevNoOfRooms').value);

                    if (count > prevCount) {
                        for (var i = (prevCount + 1); i <= count; i++) {

                            document.getElementById('room-' + i).style.display = 'flex';
                            document.getElementById('adtRoom-' + i).value = '1';
                            document.getElementById('chdRoom-' + i).value = '0';
                            document.getElementById('PrevChildCount-' + i).value = '0';

                        }

                    }
                    else if (count < prevCount) {
                        //alert('count' +count+',prev '+prevCount);
                        //alert(document.getElementById('PrevNoOfRooms').value);

                        for (var i = prevCount; i > count; i--) {
                            document.getElementById('room-' + i).style.display = 'none';
                            document.getElementById('adtRoom-' + i).value = '1';
                            
                            var childcount = document.getElementById('chdRoom-' + i).value
                            document.getElementById('ChildBlock-' + i).style.display = 'none';
                            for (var j = 1; j <= childcount; j++) {
                                document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).value = '-1';
                                document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).style.display = 'none';
                                document.getElementById('ChildBlock-' + i + '-Child-' + j).style.display = 'none';
                            }
                            document.getElementById('chdRoom-' + i).value = '0';
                        }
                    }
                }
                document.getElementById('PrevNoOfRooms').value = count;
            }
        }
        /*function to show number of childrens    */



        function ShowChildAge(number) {
            var childCount = eval(document.getElementById('chdRoom-' + number).value);
            var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);
            if (eval(document.getElementById('chdRoom-1').value) > 0 || eval(document.getElementById('chdRoom-2').value) > 0 || eval(document.getElementById('chdRoom-3').value) > 0 || eval(document.getElementById('chdRoom-4').value) > 0) {
                document.getElementById('childDetails').style.display = 'block';
            }
            else {
                document.getElementById('childDetails').style.display = 'none';
            }
            if (childCount > PrevChildCount) {
                document.getElementById('ChildBlock-' + number).style.display = 'block';
                for (var i = (PrevChildCount + 1); i <= childCount; i++) {
                    document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).style.display = 'block';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                      $('#ChildBlock-' + number + '-ChildAge-' + i).select2('destroy');
                }
            }
            else if (childCount < PrevChildCount) {
                if (childCount == 0) {
                    document.getElementById('ChildBlock-' + number).style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-1').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-2').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-3').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-4').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-5').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-6').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-3').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-4').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-5').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-6').style.display = 'none';

                }







                else {
                    for (var i = PrevChildCount; i > childCount; i--) {
                        if (i != 0) {
                            document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                            document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                        }
                    }
                }
            }
            document.getElementById('PrevChildCount-' + number).value = childCount;
        }
        function childInfo(id) {
            var index = parseInt(id);
            var se = $("selectChild-" + index);
            if (parseInt(se.value) > 0) {


                var str = "";

                str += "  <div style='float:left; width:200px;' id='roomChild" + index + "'>";
                for (var i = 0; i < se.value; i++) {

                    str += "<span style='float:left; width:95px; padding-bottom:3px;'>";
                    str += "  <label style='float:left; width:40px; padding-top:4px; font-size:11px;'> Child " + (parseInt(i) + 1) + "</label>";
                    str += "         <em style='float:left;'>";
                    str += "            <select style='width:50px;font-size:10px;font-family:Arial' class='room_guests' name='child" + index + (parseInt(i) + 1) + "' id='child" + index + (parseInt(i) + 1) + "'>";
                    str += "                    <option selected='selected' value='-1'>Age?</option>";

                    str += "                    <option value='0'><1</option>";
                    str += "                      <option value='1' >1</option>";
                    str += "                      <option value='2'>2</option>";
                    str += "                    <option value='3'>3</option>";
                    str += "                        <option value='4'>4</option>";
                    str += "                        <option value='5'>5</option>";
                    str += "                        <option value='6'>6</option>";
                    str += "                     <option value='7'>7</option> ";
                    str += "                     <option value='8'>8</option>";
                    str += "                      <option value='9'>9</option>"
                    str += "                      <option value='10'>10</option>";
                    str += "                      <option value='11'>11</option>";
                    str += "                   <option value='12'>12</option>";
                    str += "                    <option value='13'>13</option> ";
                    str += "                      <option value='14'>14</option>";
                    str += "                      <option value='15'>15</option>";
                    str += "                     <option value='16'>16</option> ";
                    str += "                    <option value='17'>17</option>";
                    str += "                      <option value='18'>18</option>";

                    str += "               </select>";
                    str += "       </em>";
                    str += "        </span>";
                }
                str += "  </div>";

                $("childInfo-" + index).innerHTML = str;
                $("childInfo-" + index).style.display = "block";
            }
            else {
                $("roomChild" + index).style.display = "none";

            }


        }
        function roomInfo(id) {

            if (parseInt(id) == 0) {
                var se = $('roomCount');
            }
            else {
                var se = $('norooms');
            }
            if (parseInt(se.value) > 1) {
                $('guestsInfo').style.display = "none";
            }
            else {
                $('guestsInfo').style.display = "none";
                return;

            }
            var str = "";
            for (var i = 2; i <= se.value; i++) {
                str += "   <div style='float:left; width:220px;' id='noOfPax-" + i + "'>";
                str += "<b style='float:left; padding:8px 5px 0 0;'>Room " + parseInt(i) + "</b>";
                str += "      <div class='no_of_guests'>";
                str += "     <select class='guests'  name='adultCount-" + parseInt(i) + "' id='adultCount-" + parseInt(i) + "'>";
                str += "               <option selected='selected' value='1' >1</option>";
                str += "               <option value='2' >2</option>";
                str += "               <option value='3'>3</option>";
                str += "               <option value='4'>4</option>";
                str += "        </select>";
                str += "       <span>Adults</span>";
                str += "       <b>(18+ yrs)</b>";
                str += "   </div>";
                str += "   <div class='no_of_guests' >";
                str += "       <select class='guests' id='selectChild-" + parseInt(i) + "' name='childCount-" + parseInt(i) + "' onchange='javascript:childInfo(" + parseInt(i) + ")'>";
                str += "               <option selected='selected' value='0'>none</option>";
                str += "               <option value='1'>1</option>";
                str += "               <option value='2'>2</option>";
                str += "           </select>";
                str += "       <span>Children</span>";
                str += "       <b>(Till 18 yrs)</b>";
                str += "   </div>";
                str += "   </div>";
                str += "<div id='childInfo-" + i + "' style='display:none'></div>"
            }
            $('guestsInfo').innerHTML = str;
            $('guestsInfo').style.display = "block";

        }
    </script>

    <script type="text/javascript" language="javascript">
        var routingEnabledForOnBehalfAgent = false;
        function disableradiobutton() {
            disablefield();
            $("label[for='ctl00_cphTransaction_radioAgent']").removeClass("tgl-btn");
            $("label[for='ctl00_cphTransaction_radioAgent']").addClass("tgl-btn2");
             $('#ctl00_cphTransaction_radioAgent').attr('disabled', true);
            $("label[for='ctl00_cphTransaction_rbtnAgent']").removeClass("tgl-btn");
            $("label[for='ctl00_cphTransaction_rbtnAgent']").addClass("tgl-btn2");
             $('#ctl00_cphTransaction_rbtnAgent').attr('disabled', true);
        }
        function disablefield() {           
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if ('<%=userType %>' == 'ADMIN' || '<%=userType %>' == 'SUPER') {
                if (document.getElementById('<%=rbtnAgent.ClientID %>') != null) {
                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                        
                        document.getElementById('wrapper').style.display = "block";
                        document.getElementById('divHotelLocations').style.display = "block";
                    }
                    else {
                        if (document.getElementById('wrapper') != null) {
                            document.getElementById('wrapper').style.display = "none";
                        }
                        document.getElementById('divHotelLocations').style.display = "none";
                    }

                    if (document.getElementById('<%=radioAgent.ClientID %>').checked == true) {
                        document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "Checked";
                        document.getElementById('wrapper1').style.display = "block";
                        document.getElementById('wrapper2').style.display = "block";
                        //document.getElementById('wrapper3').style.display = "block";

                        
                    }
                    else {
                         document.getElementById('wrapper1').style.display = "none";
                        document.getElementById('wrapper2').style.display = "none";
                        //document.getElementById('wrapper3').style.display = "none";
                        document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "UnChecked";
                        $("#ctl00_cphTransaction_ddlAirAgents").select2("val", "-1");
                        $("#ctl00_cphTransaction_ddlAirAgentsLocations").empty();    
                        $("#ctl00_cphTransaction_ddlAirAgentsLocations").select2("val", "-1");
                        $('#CorpDiv').hide();
                        $('#travellerDropdown').parent(':first-child').removeClass('form-control-holder form-control-element with-custom-dropdown disabled');
                        LoadCorpDropDownForOBAgent(<%=Settings.LoginInfo.AgentId%>, '');
                    }
                }

                if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled).ToString().ToLower()%>'=='true') {
                    ShowAllSources();//Show all sources for oneway
                    $('#divSearchOptions').show();                    
                }
                else {
                    $('#divSearchOptions').hide();
                    ShowAllSources();//Show all sources for oneway
                    routingEnabledForOnBehalfAgent = false;
                }
                
                if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsReturnFareEnabled).ToString().ToLower()%>'=='true') {
                    ShowAllSources();//Show all sources for oneway
                    $('#divhidebyagentid').show();                    
                }
                else {
                    $('#divhidebyagentid').hide();
                    ShowRoutingSources();//Show all sources for oneway
                    routingEnabledForOnBehalfAgent = false;
                }
            }

            if (document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                document.getElementById('tblNormal').style.display = 'block';
                document.getElementById('tblNormal1').style.display = 'block';
                document.getElementById('tblNormal2').style.display = 'block';
                document.getElementById('tblNormal3').style.display = 'block';
                document.getElementById('tblNormal4').style.display = 'block';

                document.getElementById('tblMultiCity').style.display = 'none';
                document.getElementById('textbox_A3').style.visibility = "visible";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "return";

                // document.getElementById('tblGrid').style.display='none';
                document.getElementById('tblMultiCity').style.display = 'none';
                if (document.getElementById('<%=hdnIsCrossPostBack.ClientID%>').value == 'false') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            checkbox[i].checked = true;
                            checkbox[i].disabled = false;
                            document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                            document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                        }
                    }
                }
                EnableDisableRouting();
            }
            else if (document.getElementById('<%=oneway.ClientID %>').checked == true) {
                document.getElementById('tblNormal').style.display = 'block';
                document.getElementById('tblNormal1').style.display = 'block';
                document.getElementById('tblNormal2').style.display = 'block';
                document.getElementById('tblNormal3').style.display = 'block';
                document.getElementById('tblNormal4').style.display = 'block';
                document.getElementById('fcontainer2').style.display = 'none';

                document.getElementById('tblMultiCity').style.display = 'none';
                document.getElementById('textbox_A3').style.visibility = "hidden";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "oneway";
                // document.getElementById('tblGrid').style.display='none';
                document.getElementById('tblMultiCity').style.display = 'none';
                if (document.getElementById('<%=hdnIsCrossPostBack.ClientID%>').value == 'false') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            checkbox[i].checked = true;
                            checkbox[i].disabled = false;
                            document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                            document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                        }
                    }
                }
                if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsReturnFareEnabled).ToString().ToLower()%>' == 'true') {
                    $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option

                    ShowAllSources();//Show all sources for oneway
                }
                else{
                    $('#ctl00_cphTransaction_rbtnSearchBySegment').prop('checked', true);
                    ShowRoutingSources();
                }
            }
            else if (document.getElementById('<%= multicity.ClientID%>').checked == true) {
                document.getElementById('tblNormal1').style.display = 'none';
                document.getElementById('tblNormal2').style.display = 'none';
                document.getElementById('tblNormal3').style.display = 'block';
                document.getElementById('tblNormal4').style.display = 'block';
                document.getElementById('tblMultiCity').style.display = 'block';
                document.getElementById('fcontainer2').style.display = 'none';



                // document.getElementById('tblGrid').style.display='block';
                document.getElementById('<%=hdnWayType.ClientID %>').value = "multiway";
                var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");

                var reqType = document.getElementById("<%=hdnWayType.ClientID %>");
                if (reqType.value == 'multiway') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            var source = checkbox[i].nextSibling.innerHTML;
                            if (source == "G9" || source == "FZ" || source == "SG" || source == "6E" || source == "IX") {
                                checkbox[i].checked = false;
                                checkbox[i].disabled = true;
                                document.getElementById("<%=chkFlightAll.ClientID %>").checked = false;
                                document.getElementById("<%=chkFlightAll.ClientID %>").disabled = true;
                            }
                            else{ // for gds multicity added by bangar

                                checkbox[i].checked = true;
                                checkbox[i].disabled = false;
                                document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                                document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                            }
                        }
                    }
                }                
              if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsReturnFareEnabled).ToString().ToLower()%>' == 'true') {
                    $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option

                    ShowAllSources();//Show all sources for oneway
                }
                else{
                    $('#ctl00_cphTransaction_rbtnSearchBySegment').prop('checked', true);
                    ShowRoutingSources();
                }

               // $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
               
            }
            document.getElementById('<%=hdnIsCrossPostBack.ClientID%>').value = 'false';
            var agentId = '';
            if ($('#<%=ddlAirAgents.ClientID%>').val() != "-1")
                agentId = $('#<%=ddlAirAgents.ClientID%>').val();
            
            if (IsRoutingEnabledForOnBehalfAgent(agentId))
                $('#divSearchOptions').show();
            else if ($('#<%=ddlAirAgents.ClientID%>').val() == "-1" && IsRoutingEnabledForOnBehalfAgent(<%=Settings.LoginInfo.AgentId%>)){                
                $('#divSearchOptions').show();
            }
            else {
                $('#divSearchOptions').hide();
            }

            EnableVisaChange();  
        }

        function EnableDisableRouting() {
            if (routingEnabledForOnBehalfAgent || '<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled).ToString().ToLower()%>' == 'true') {
                if ($('#ctl00_cphTransaction_rbtnSearchBySegment').prop("checked")) {
                    ShowRoutingSources();
                }
                else {
                    ShowAllSources();
                }
                $('#divSearchOptions').show();
            }
            else {
                $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divSearchOptions').hide();
                ShowAllSources();//Show all sources for oneway
            }
             if (routingEnabledForOnBehalfAgent || '<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsReturnFareEnabled).ToString().ToLower()%>' == 'true') {
                if ($('#ctl00_cphTransaction_rbtnSearchByPrice').prop("checked")) {
                   
                    ShowAllSources();
                }
                else {
                    ShowRoutingSources();
                }
                $('#divhidebyagentid').show();
            }
            else {
                $('#ctl00_cphTransaction_rbtnSearchBySegment').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divhidebyagentid').hide();
                ShowRoutingSources();//Show all sources for oneway
            }
        }
        
        //Function to hide non routing sources for the Agent -  this will be called when you select Search by Availability checkbox
        function ShowRoutingSources() {
            var routingSources = '<%=routingSources%>'
            $('#ctl00_cphTransaction_ddlBookingClass').prop('selectedIndex', '0').change();//Always select Any for Combination Fares            
            var checked_checkboxes = $("[id*=ctl00_cphTransaction_chkSuppliers] input");
            
            if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled).ToString().ToLower()%>'=='true') {
                checked_checkboxes.each(function () {
                    var value = $(this).val();
                    var text = $(this).closest("td").find("label").html();
                    if (routingSources.indexOf(text) < 0) {
                        $(this).closest("td").find("label").parent().hide();
                        $(this).prop("checked", false);
                        this.style.display = 'none';
                    }
                });
            }
            else if (routingEnabledForOnBehalfAgent) {                
                if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled).ToString().ToLower()%>' == 'false') {

                    LoadRoutingSources();

                }
                else {
                    checked_checkboxes.each(function () {
                        var value = $(this).val();
                        var text = $(this).closest("td").find("label").html();
                        if (routingSources.indexOf(text) < 0) {
                            $(this).closest("td").find("label").parent().hide();
                            $(this).prop("checked", false);
                        }
                    });
                }
            }

        }

        //Function to show all sources for the Agent - this will be called when you select Search by Price checkbox
        function ShowAllSources() {
            var routingSources = '<%=routingSources%>'

            var checked_checkboxes = $("[id*=ctl00_cphTransaction_chkSuppliers] input");

            checked_checkboxes.each(function () {
                $(this).closest("td").find("label").parent().show();
                $(this).prop("checked", true);
                this.style.display = 'inline';
            });

            $('#ctl00_cphTransaction_ddlBookingClass').select2('val', '2');//Always select Economy for Return Fares
        EnableVisaChange();
}

        function LoadRoutingSources() {
            var paramList = 'requestSource=getRoutingSources&agentid=' + $('#ctl00_cphTransaction_ddlAirAgents').val();
            var url = "CityAjax";
            if (window.XMLHttpRequest) {
                Ajax = new XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }
            Ajax.onreadystatechange = BindRoutingSources;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }
        function BindRoutingSources() {
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
                routingSources = Ajax.responseText;

                var hidden_rows = $("[id*=ctl00_cphTransaction_chkSuppliers] td");

                hidden_rows.each(function () {
                    var text = $(this).find("label").html();
                    if (routingSources.indexOf(text) < 0) {
                        $(this).show();
                    }
                });

                var checked_checkboxes = $("[id*=ctl00_cphTransaction_chkSuppliers] input");
                checked_checkboxes.each(function () {
                    var value = $(this).val();
                    var text = $(this).closest("td").find("label").html();
                    if (routingSources.indexOf(text) < 0) {
                        $(this).closest("td").find("label").parent().hide();
                        $(this).prop("checked", false);
                    }
                });
            }
        }
    </script>

    <script type="text/javascript">
        $(function() {

            $("#example-one").organicTabs();

            $("#tabbed-menu").organicTabs({
                "speed": 200
            });

        });
    </script>
<script type="text/javascript">
    var arrayStates = new Array();
    function invokePage(url, passData) {
        if (window.XMLHttpRequest) {
            AJAX = new XMLHttpRequest();
        }
        else {
            AJAX = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if (AJAX) {
            AJAX.open("POST", url, false);
            AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            AJAX.send(passData);
            return AJAX.responseText;
        }
        else {
            return false;
        }
    }

    function getAirlines(sQuery) {

        var paramList = 'searchKey=' + sQuery;
        paramList += '&requestSource=' + "PreferredAirline";
        var url = "CityAjax";
        var arrayStates = "";
        var faltoo = invokePage(url, paramList);
        arrayStates = faltoo.split('/');
        if (arrayStates[0] != "") {
            for (var i = 0; i < arrayStates.length; i++) {
                arrayStates[i] = [arrayStates[i].split(',')[1], arrayStates[i]];
            }

            return arrayStates;
        }
        else return (false);
    }

    //    function ShowCount(product) {
    //       
    //        if (product == "Flights") {
    //            document.getElementById('tdHotelsBkgCount1').style.visibility = 'hidden';
    //            document.getElementById('tdHotelsBkgCount2').style.visibility = 'hidden';
    //            document.getElementById('tdHotelsBkgCount3').style.visibility = 'hidden';
    //            document.getElementById('tdFligtsBkgCount1').style.visibility = 'visible';
    //            document.getElementById('tdFligtsBkgCount1').style.width = '20%';
    //            document.getElementById('tdFligtsBkgCount2').style.visibility = 'visible';
    //            document.getElementById('tdFligtsBkgCount2').style.width = '20%';
    //            document.getElementById('tdFligtsBkgCount3').style.visibility = 'visible';
    //            document.getElementById('tdFligtsBkgCount3').style.width = '20%';
    //        }
    //        if (product == "Hotels") {
    //            document.getElementById('tdFligtsBkgCount1').style.width = '0%';
    //            document.getElementById('tdFligtsBkgCount1').style.visibility = 'hidden';
    //            document.getElementById('tdFligtsBkgCount2').style.width = '0%';
    //            document.getElementById('tdFligtsBkgCount2').style.visibility = 'hidden';
    //            document.getElementById('tdFligtsBkgCount3').style.width = '0%';
    //            document.getElementById('tdFligtsBkgCount3').style.visibility = 'hidden';
    //            document.getElementById('tdHotelsBkgCount1').style.visibility = 'visible';
    //            document.getElementById('tdHotelsBkgCount1').style.width = '100%';
    //            document.getElementById('tdHotelsBkgCount2').style.visibility = 'visible';
    //            document.getElementById('tdHotelsBkgCount2').style.width = '100%';
    //            document.getElementById('tdHotelsBkgCount3').style.visibility = 'visible';
    //            document.getElementById('tdHotelsBkgCount3').style.width = '100%';
    //           
    //        }

    //    }


    function autoCompInitPrefAirline() {
        oACDSPA = new YAHOO.widget.DS_JSFunction(getAirlines);
        // Instantiate third auto complete
        oAutoCompPA = new YAHOO.widget.AutoComplete('<%=txtPreferredAirline.ClientID %>', 'statescontainer4', oACDSPA);
        oAutoCompPA.prehighlightClassName = "yui-ac-prehighlight";
        oAutoCompPA.queryDelay = 0;
        oAutoCompPA.minQueryLength = 2;
        oAutoCompPA.useIFrame = true;
        oAutoCompPA.useShadow = true;

        oAutoCompPA.formatResult = function(oResultItem, sQuery) {
            document.getElementById('statescontainer4').style.display = "block";
            var toShow = oResultItem[1].split(',');
            var sMarkup = toShow[0] + ',' + toShow[1]; ;
            //var aMarkup = ["<li>", sMarkup, "</li>"]; 
            var aMarkup = [sMarkup];
            return (aMarkup.join(""));
        };
        oAutoCompPA.itemSelectEvent.subscribe(itemSelectHandlerPA);
    }
    var itemSelectHandlerPA = function(sType2, aArgs2) {

        YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
        var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
        var city = oMyAcInstance2[1].split(',');
        document.getElementById('<%=airlineCode.ClientID %>').value = city[0];
        document.getElementById('<%=airlineName.ClientID %>').value = city[1];
        document.getElementById('statescontainer4').style.display = "none";
        var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
        var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
    };
    YAHOO.util.Event.addListener(this, 'load', autoCompInitPrefAirline); //temporarily commented

    var Ajax;
    var loc;
    var product = 1;
    function LoadAgentLocations(id, type) { // id = selected agent id of flight/hotel, type = F(Flight)/H(Hotel)
       
       // if ($('#<%=ddlAirAgents.ClientID %>').val() != "-1") { 
        if ($('#'+id).val() != "-1") { // added by somasekhar - for binding Locations based on selected Agent id  of Flight or Hotel
            var location = 'ctl00_cphTransaction_ddlAirAgentsLocations';
            if (type == 'H') {
                location = 'ctl00_cphTransaction_ddlHotelAgentLocations';
                product = 2;
            }
            var agentId = $('#<%=ddlAirAgents.ClientID %>').val();
            $('#'+location).select2('val', '-1');//Clear selected value
            $('#<%=ddlFlightTravelReasons.ClientID%>').select2('val', '-1');
            $('#<%=ddlFlightEmployee.ClientID%>').select2('val', 'Select Traveller');

            if ($('#<%=ddlAirAgents.ClientID %> option:selected').text().indexOf('(') == 0 && $('#<%=ddlAirAgents.ClientID %> option:selected').text().lastIndexOf('(') == 0) {
                $('#CorpDiv').show();
                $('#SelectedTravelerInfo').html('1 Adult');
                $('#<%=ddlAdults.ClientID %>').val('1');
                $('#<%=ddlChilds.ClientID %>').val('0');
                $('#<%=ddlInfants.ClientID %>').val('0');
                $('#travellerDropdown').parent(':first-child').addClass('form-control-holder form-control-element with-custom-dropdown disabled');
                          
                LoadCorpDropDownForOBAgent(agentId, 'E');
                LoadCorpDropDownForOBAgent(agentId, 'T');
            }
            else {
                $('#CorpDiv').hide();
                $('#travellerDropdown').parent(':first-child').removeClass('form-control-holder form-control-element with-custom-dropdown disabled');
                
                LoadCorpDropDownForOBAgent(agentId, '');//To Load Time Intervals
            }
            

            loc = location;
            var sel = document.getElementById('<%=ddlAirAgents.ClientID %>').value;
            if (type == 'H') {
                sel = document.getElementById('<%=ddlAgents.ClientID %>').value;
            }
            var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + sel + '&id=' + location;;
            var url = "CityAjax";

            if (window.XMLHttpRequest) {
                Ajax = new XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }
            Ajax.onreadystatechange = BindLocationsList;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }
        else {
            $("#ctl00_cphTransaction_ddlAirAgentsLocations").empty();
            $("#ctl00_cphTransaction_ddlAirAgentsLocations").select2("val", "");
             $('#CorpDiv').hide();
                $('#travellerDropdown').parent(':first-child').removeClass('form-control-holder form-control-element with-custom-dropdown disabled');                
                LoadCorpDropDownForOBAgent(agentId, '');//To Load Time Intervals
            routingEnabledForOnBehalfAgent = false;
            if ('<%=(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled).ToString().ToLower()%>' == 'true') {
                //If one way or multicity checked and on behalf location selected then if agent is routing also we need to hide the option
                if (document.getElementById('ctl00_cphTransaction_oneway').checked || (document.getElementById('ctl00_cphTransaction_multicity') != null && document.getElementById('ctl00_cphTransaction_multicity').checked)) {
                    $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                    $('#divSearchOptions').hide();
                    ShowAllSources();//Show all sources for oneway
                    console.log('Routing disabled');
                } else {//if round trip radio selected
                   $('#divSearchOptions').show();
                    console.log('Routing enabled');
                    if ($('#ctl00_cphTransaction_rbtnSearchBySegment').prop("checked")) {
                        ShowRoutingSources();
                    }
                    else {
                        ShowAllSources();
                    }
                }
            }
            else {//If login agent doesnt have routing access
                $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divSearchOptions').hide();
                ShowAllSources();//Show all sources for oneway
                console.log('Routing disabled');
            }
             var agentId = '';
            if ($('#<%=ddlAirAgents.ClientID%>').val() != "-1")
                agentId = $('#<%=ddlAirAgents.ClientID%>').val();
            
            if (IsRoutingEnabledForOnBehalfAgent(agentId))
                $('#divSearchOptions').show();
            else if ($('#<%=ddlAirAgents.ClientID%>').val() == "-1" && IsRoutingEnabledForOnBehalfAgent(<%=Settings.LoginInfo.AgentId%>)){                
                $('#divSearchOptions').show();
            }
            else {
                $('#divSearchOptions').hide();
            }
        }       
    }
    function BindLocationsList() {
        var ddl = document.getElementById(loc);
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    if (ddl != null) {
                        ddl.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Location";
                        el.value = "-1";
                        ddl.add(el, 0);
                        //routing agent Id|location name#location id
                        var values = Ajax.responseText.split('#')[1].split(',');
                        for (var i = 0; i < values.length; i++) {
                            var opt = values[i];
                            if (opt.length > 0 && opt.indexOf('|') > 0) {
                                var el = document.createElement("option");
                                el.textContent = opt.split('|')[0];
                                el.value = opt.split('|')[1];
                                ddl.appendChild(el);
                            }
                        }
                <% if (Page.PreviousPage != null && Page.PreviousPage.Title == "Hotel Search Results")
    { %>                
                        $('#' + ddl.id).select2('val', '<%=Settings.LoginInfo.OnBehalfAgentLocation.ToString()%>');
                <%}%>
                    }

                    routingEnabledForOnBehalfAgent = eval(Ajax.responseText.split('#')[0]);
                    if (routingEnabledForOnBehalfAgent && product == 1) {
                        //If one way or multicity checked and on behalf location selected then if agent is routing also we need to hide the option
                        if (document.getElementById('ctl00_cphTransaction_oneway').checked || (document.getElementById('ctl00_cphTransaction_multicity') != null && document.getElementById('ctl00_cphTransaction_multicity').checked)) {
                            $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                            $('#divSearchOptions').hide();
                            ShowAllSources();//Show all sources for oneway
                            console.log('Routing disabled');
                        } else {
                            $('#divSearchOptions').show();
                            console.log('Routing enabled');
                            if ($('#ctl00_cphTransaction_rbtnSearchBySegment').prop("checked")) {
                                ShowRoutingSources();
                            }
                            else {
                                ShowAllSources();
                            }
                        }
                    }
                    else {
                        $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                        $('#divSearchOptions').hide();
                        ShowAllSources();//Show all sources for oneway
                        console.log('Routing disabled');
                    }
                }
                else {//Clear previous agent locations if no locations found for the current agent
                    if (ddl != null) {
                        ddl.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Location";
                        el.value = "-1";
                        ddl.add(el, 0);
                        $('#' + ddl.id).select2('val', '-1');//Select Location
                    }                   
                }
            }
        } 
    }

    function IsRoutingEnabledForOnBehalfAgent(agentId) {
        return AjaxCall("CommonWebMethods.aspx/IsRoutingEnabledForAgent", "{'agentId':" + agentId + "}");
    }

    function LoadCorpDropDownForOBAgent(agentId, type) {
        $.ajax({
            type: "POST",
            url: "CommonWebMethods.aspx/GetCorporateDropDownValues",
            contentType: "application/json; charset=utf-8",
            data: "{'agentId':'" + agentId+ "','valueType':'" + type + "'}",
            dataType: "json",     
            async: true,
            success: function (data) {
                if (data.d != undefined) {
                    var ddlFTReasons = document.getElementById('<%=ddlFlightTravelReasons.ClientID%>');
                    var ddlFEmployees = document.getElementById('<%=ddlFlightEmployee.ClientID%>');

                    var cvalues = data.d.split(',');

                    if (type == 'E') {
                        ddlFEmployees.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Traveller / Employee";
                        el.value = "Select Traveller";
                        ddlFEmployees.add(el, 0);

                        for (var i = 0; i < cvalues.length; i++) {
                            var opt = cvalues[i];
                            if (opt.length > 0 && opt.indexOf('#') > 0) {
                                var el = document.createElement("option");
                                el.textContent = opt.split('#')[1];
                                el.value = opt.split('#')[0];
                                ddlFEmployees.appendChild(el);
                            }
                        }
                    }
                    else {
                        ddlFTReasons.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Reason For Travel";
                        el.value = "-1";
                        ddlFTReasons.add(el, 0);

                        for (var i = 0; i < cvalues.length; i++) {
                            var opt = cvalues[i];
                            if (opt.length > 0 && opt.indexOf('#') > 0) {
                                var el = document.createElement("option");
                                el.textContent = opt.split('#')[1];
                                el.value = opt.split('#')[0];
                                ddlFTReasons.appendChild(el);
                            }
                        }
                    }
                }
            },
            error: (error) => {
                console.log(JSON.stringify(error));
            }
        });

        //Binding the Time Intervals for Corporate Agent

        $.ajax({
            type: "POST",
            url: "CommonWebMethods.aspx/GetTimeIntervals",
            contentType: "application/json; charset=utf-8",
            data: "{'agentId':'" +agentId + "'}",
            dataType: "json",     
            async: true,
            success: function (data) {
                if (data.d != undefined) {
                    var ddlDepTime = document.getElementById('<%=ddlDepTime.ClientID%>');
                    var ddlRetTime = document.getElementById('<%=ddlReturnTime.ClientID%>');

                    var cvalues = data.d.split(',');

                    ddlDepTime.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Any Time";
                    el.value = "Any";
                    ddlDepTime.add(el, 0);

                    ddlRetTime.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Any Time";
                    el.value = "Any";
                    ddlRetTime.add(el, 0);

                    for (var i = 0; i < cvalues.length; i++) {
                        var opt = cvalues[i];
                        if (opt.length > 0) {
                            var options = opt.split('|');

                            var el = document.createElement("option");
                            el.textContent = options[0];
                            el.value = options[1];
                            ddlDepTime.appendChild(el);

                            el = document.createElement("option");
                            el.textContent = options[0];
                            el.value = options[1];
                            ddlRetTime.appendChild(el);

                        }
                    }
                }
            },
            error: (error) => {
                console.log(JSON.stringify(error));
            }
        });
    }


    //Added by somasekhar on 28/09/2018
        // Get Agent County Code Based on AgentId Location Id
        //Used in HotelSearch.aspx page. To set Default Nationality and Residence India for IN county code
    function LoadAgentCountryCode() {
        var location = document.getElementById('ctl00_cphTransaction_ddlHotelAgentLocations').value;// $("#ddlHotelAgentLocations").val();// 'ctl00_cphTransaction_ddlHotelAgentLocations';        
        //alert('location' + location);
        if (location != "") {
        var paramList = 'requestSource=getAgentsCountryByLocId' + '&LocationId=' + location;
        var url = "CityAjax";
        if (window.XMLHttpRequest) {
            Ajax = new XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        Ajax.onreadystatechange = BindLocationCountry;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);    
        }           
    }
    function BindLocationCountry(response) {
         $('#' + getElement('ddlNationality').id).select2('val',"Select Nationality");
         $('#' + getElement('ddlResidence').id).select2('val', "Select Country");
         if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
             var conuntryCode = Ajax.responseText;
            if (conuntryCode != "") {
                if (conuntryCode.split('|')[0] == "IN") {
                    $('#' + getElement('ddlNationality').id).select2('val', conuntryCode.split('|')[1]);
                    $('#' + getElement('ddlResidence').id).select2('val', conuntryCode.split('|')[1]);

                    //$('#' + getElement('ddlNationality').id).select2('val', '20');
                    //$('#' + getElement('ddlResidence').id).select2('val', '20');
                } 
            }
                      
        }
    }
    //===================

</script>
    <!-- For slider javascript END -->
    <%  string setDisplay = string.Empty;
        if (Request["sessionId"] != null && Basket.FlightBookingSession.ContainsKey(Request["sessionId"]) && Basket.FlightBookingSession[sessionIdValue].XmlMessage.Count != 0 && Request["show"] == "anotherfare")
        {
            setDisplay = "style=\"display:none;\"";
        } %>
    <%  
        if (Request["calendarSearch"] != null && Request["calendarSearch"] == "CalendarSearch")
        {
            setDisplay = "style=\"display:none;\"";
        }
        if (dateChangeSearch)
        {
            setDisplay = "style=\"display:none;\"";
        }%>
    <!-- search result javascript  END -->
    <span style="display:none;">
                 <dfn>
                 <input type="radio" id="domesticHotel" name="hotelSearchArea" onclick="javascript:IntDomChecked()" checked="checked" value="1" />
                 </dfn>
                 <em>Domestic Search</em> 
                 </span>
                 <span style="display:none;">
                 <dfn>
                 <input type="radio" id="intlHotel" name="hotelSearchArea" onclick="javascript:IntDomChecked()" value="0" />
                 </dfn> 
                 <em>International</em>
                </span>
    <asp:HiddenField ID="hdnSubmit" runat="server" />
    <asp:HiddenField ID="hdnWayType" runat="server" Value="return" />
    <asp:HiddenField ID="hdnActiveSources" runat="server"  />
    <asp:HiddenField ID="hdnSelectedSources" runat="server"  />
    <asp:HiddenField ID="hdnMultiRows" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIsCrossPostBack" runat="server" Value="false" />
   <asp:HiddenField ID="hdnAllowFlightSearch" runat="server" Value="true" />
   <asp:HiddenField ID="hdnUAPIAllowed" runat="server" Value="true" />
   <asp:HiddenField ID="hdnTBOAllowed" runat="server" Value="true" />
   <asp:HiddenField ID="hdnAgentLocation" runat="server" />
    <asp:HiddenField ID="hdnFlightEmployee" runat="server" />
    <asp:HiddenField ID="hdnTravelReason" runat="server" />
    
    
    <div id="CitySearchPop" class="light-grey-bg border-y padding-bottom-10 search-popup-parent"
        style="display: none; width: 410px; position: absolute; left: 500px; top: 110px;
        z-index: 100">
        <div id="LoadingCountryList" style="display: none; font-weight: bolder; margin-left: 105px;
            padding-top: 10px">
            Loading
           <%-- <img style="width: 80px" src="../images/ajax-loader2.gif" />--%></div>
        <div id="SelectCountryDropDown" class="fleft padding-5 search-popup-child" style="width: 400px;
            display: none">
        </div>
        <div id="AirportList" class="fleft padding-5" style="height: 15px; display: none">
        </div>
    </div>
    <input type="hidden" id="OfferedFareHidden" value="" />
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div id="SearchResultLoad" style="display: none; margin-left: 5px; margin-top: -20px;">
        <a id="positionForValidation"></a>
        <img id="Img2" src="../images/indicator.gif" alt="up-arrow" />
        Loading Search Result....
    </div>
    <div id="SearchResultBlock">
    </div>
    <div id="MainDiv" onload="SetModifyValues()">
    <div>
     <div class="error_module">
     
     </div>

      <%--  <div class="clear" style="margin-left: 25px">
            <div id="container1" style="position: absolute; top:257px; left: 167px; display: none;">
            </div>
        </div>--%>
                    <!-- End Visa Div-->        


<asp:HiddenField ID="hdnAdults" runat="server"  />
            <asp:HiddenField ID="hdnChilds" runat="server"  />
            <asp:HiddenField ID="hdnChildAges" runat="server"  />
            <asp:HiddenField ID="hdnRating" runat="server"  />
            <input type="hidden" id="hRooms" name="hRooms" value="<%=reqObj.NoOfRooms %>" />

        
        
    
        
        
      
  
        <div class="clear" style="margin-left: 25px">
            <div id="mcontainer1" style="position: absolute;    top: 179px;  right: 0px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="mcontainer2" style="position: absolute; top: 199px;  right: 0px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="mcontainer3" style="position: absolute; top: 219px; right: 0px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="mcontainer4" style="position: absolute; top: 239px; right: 0px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="mcontainer5" style="position: absolute; top: 259px; right: 0px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="mcontainer6" style="position: absolute; top: 279px; right: 0px; display: none;">
            </div>
        </div>
      
    
      
      
      
     <%-- left panel startes here--%>
      
   
         
           
    <div class="banner-container" style="background-image:url(build/img/main-bg.jpg)">
        <div class="row no-gutter">
          <div class="col-lg-7 col-xl-6">
            <div id="tabbed-menu" class="search-container">            
                <ul class="tabbed-menu nav">  
                    <li>
                        <a  id="lnkFlights"  href="#core2" <%=Settings.roleFunctionList.Exists(x => x.ToLower().Contains("hotelsearch?source=flight")) ? flightEnable : "style='display:none'" %>>                   
                          Flight
                        </a> 
                    </li>                           
                    <li>
                        <a id="lnkHotels" onclick="Javascript:location='<%= Settings.LoginInfo.Currency == "INR" && Settings.LoginInfo.IsCorporate=="N" ? "HotelSearch?source=Hotel" : "findHotels" %>'"  href="#featured2"  <%=Settings.roleFunctionList.Exists(x => x.ToLower().Contains("hotelsearch?source=hotel") ) || ( Settings.roleFunctionList.Exists(x => x.ToLower().Contains("findhotels"))) ? hotelEnable : "style='display:none'" %>>
                        
                         Hotel
                        </a>
                    </li>      
                    <li>
                        <a  id="lnkACtivity" onclick="Javascript:location='ActivityResults'" href="#classics2"  <%=Settings.roleFunctionList.Exists(x => x.ToLower().Contains("activityresults")) ? activityEnable : "style='display:none'"%>>
                         Activity
                        </a>
                    </li>    
                    <li><a id="lnkPackages" onclick="Javascript:location='packages'"  href="#featured2"  <%=Settings.roleFunctionList.Exists(x => x.ToLower().Contains("packages")) ? packageEnable : "style='display:none'" %>>
                        Packages
                        </a>
                    </li>
                     <li>
                         <a onclick="Javascript:location='Insurance'" id="lnkInsurance" href="Insurance" <%=Settings.roleFunctionList.Exists(x => x.ToLower().Contains("insurance")) ? insEnable : "style='display:none'"%>>
                            Insurance
                         </a>
                   </li>                                   
                                        <%-- <li><a id="A1" onclick="Javascript:location='FixedDepartureResults.aspx'"  href="#fixed2">
                            <span class="fixeddep_ico">Group Tours</span></a></li>
                            --%>                                                            
                </ul>
                 
               <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" class="alert-messages alert-caution">
                    </div>
                </div>
                       
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>

 <%--Hotel SEARCH--%>

                <%--    <div class="search-matrix-wrap p-4 search-page-form hide hotels"  id="featured2" <%=hotelDiv %>>--%>
                  <div class="search-matrix-wrap p-4 search-page-form hotels" data-tab-content="source=Hotel" style="display:none"  id="featured2" >
                        <%if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
                            { %>
                              
                           <div class="custom-radio-table row mb-3"  id="tblAgent">

                               <div class="col-md-3 d-none">
                                   <div class="form-group">
                                        <asp:RadioButton ID="rbtnSelf" Visible="false" runat="server" GroupName="book" onclick="disablefield();"
                                        Checked="true" Font-Bold="True" Text="For Self" />
                                       
                                   </div>
                               </div>
                               <div class="col-md-2 d-none">
                                   <div class="form-group">
                                       <%--<asp:RadioButton ID="rbtnAgent" runat="server" GroupName="book" onclick="disablefield();"
                                        Font-Bold="True" Text="Client" />--%>
                                      
                                       
                                        <input type="hidden" name="BookingAgencyID" id="BookingAgencyID" value="<%= AdminId %>" />
                                        <input type="hidden" name="hdfAdminId" id="hdfAdminId" value="<%= AdminId %>" />
                                   </div>
                               </div>
                               <div class="col-12">
                                   <div class="row custom-gutter">
                                         <div class="col-md-12"> 
                                             <div class="agent-checkbox-wrap float-left">      
                                                     <asp:CheckBox ID="rbtnAgent"  Text="" runat="server" onclick="disablefield();"/>    
                                                     <label class="tgl-btn" for="ctl00_cphTransaction_rbtnAgent"><em></em></label>
                                              </div> 
                                          </div>
                                          <div class="col-md-6" id="wrapper" style="display:none">
                                               <asp:DropDownList CssClass="form-control" ID="ddlAgents" runat="server" onchange="LoadAgentLocations(this.id,'H')">
                                                        </asp:DropDownList>
                                           </div>
                               
               <div id="divHotelLocations" class="col-md-6" style="display:none;">
                               

                 


                                <asp:DropDownList ID="ddlHotelAgentLocations" CssClass="form-control" runat="server" onchange="LoadAgentCountryCode()">
                                </asp:DropDownList>
                           
                         </div>
                                   </div>
                               </div>
                            
                               
                           </div>        
                       
                    

                       
                               
                               
                                                                 
                        <%} %>

                       <div class="row custom-gutter">
                            <div class="col-md-6">
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-location" aria-label="Origin Airport"></span>
                                    </div>                                       
                                    <asp:DropDownList ID="ddlCity" CssClass="form-control" runat="server" AppendDataBoundItems="True" 
                                        Visible="false">
                                        <asp:ListItem Selected="True">Select City</asp:ListItem>
                                    </asp:DropDownList>
                                    <input class="form-control" type="text" id="city" name="city" value="<%=cityName%>" onblur="markout(this, '<%=cityName%>')" onfocus="markin(this, '<%=cityName%>')" onclick="IntDom('statescontainer3' , 'city')" />
                                    <input type="hidden" id="CityCode" name="CityCode" value="<%=reqObj.CityId %>" />
                                    <input type="hidden" id="CountryName" name="CountryName" value="" />                    
                                    <input type="hidden" name="destinationCity" id="destinationCity" />
                                    <input type="hidden" name="isDomesticHotel" id="isDomesticHotel" />                     
                                    <div id="statescontainer3" style="width:95%; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;    top: 38px;" ></div>    
                               </div>
                            </div>
                            <div class="col-md-3">
                                        <a class="form-control-holder"  href="javascript:void(null)" onclick="showCalendar1()">
                                            <div class="icon-holder">
                                                <span class="icon-calendar" aria-label=""></span>
                                            </div>    
                                             <asp:TextBox Class="form-control" placeholder="Check In" ID="CheckIn" autocomplete="off" data-calendar-contentID="#container1" runat="server"></asp:TextBox>                                                  
                                        </a>
                                         <div id="Outcontainer1" style="display: none" >
                                            <div id="container1"> </div>
                                         </div>
                            </div>
                            <div class="col-md-3">
                                  <a class="form-control-holder" href="javascript:void(null)" onclick="showCalendar2()">
                                        <div class="icon-holder">
                                            <span class="icon-calendar" aria-label=""></span>
                                        </div>   
                                       <asp:TextBox Class="form-control" placeholder="Check Out" ID="CheckOut" autocomplete="off" data-calendar-contentID="#container2" runat="server"></asp:TextBox>  
                                    </a>
                                    <div id="Outcontainer2" style="display: none;" >
                                        <div id="container2"> </div>
                                    </div>        
                            </div>
                       </div>
                        <div class="row custom-gutter">
                            <div class="col-md-4">       
                                  <div class="form-control-holder">
                                        <div class="icon-holder">
                                            <span class="icon-passport" aria-label="Select Nationality"></span>
                                        </div>     
                                       <asp:DropDownList CssClass="form-control"   ID="ddlNationality" runat="server"
                                            DataTextField="CountryName" DataValueField="CountryCode"  
                                            AppendDataBoundItems="True">
                                            <asp:ListItem Selected="True">Select Nationality</asp:ListItem>
                                        </asp:DropDownList>                                                 
                                   </div>
                            </div>
                            <div class="col-md-4">        
                                  <div class="form-control-holder">
                                        <div class="icon-holder">
                                            <span class="icon-language" aria-label="Select Country"></span>
                                        </div>   
                                      <asp:DropDownList CssClass="form-control" ID="ddlResidence" runat="server" DataTextField="CountryName"
                                                                        DataValueField="CountryCode"  AppendDataBoundItems="True">
                                                                        <asp:ListItem Selected="True">Select Country</asp:ListItem>
                                                                    </asp:DropDownList>                                                   
                                   </div> 
                            </div>
                            <div class="col-md-4">       
                                  <div class="form-control-holder">
                                        <div class="icon-holder">
                                            <span class="icon-star" aria-label=""></span>
                                        </div>             
                                        <asp:DropDownList runat="server"  ID="rating"  CssClass="form-control">
                                            <asp:ListItem selected="True" Text="Show All" value="0"></asp:ListItem>
                                            <asp:ListItem Text="1 rating" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2 rating" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3 rating" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4 rating" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5 rating" Value="5"></asp:ListItem>
                                        </asp:DropDownList>                                         
                                   </div>  
                            </div>
                        </div>

              
                     <div class="row custom-gutter">
                         <div class="col-md-6">
                             <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#hotelRoomSelectDropDown">
                                        <div class="icon-holder">
 	                                         <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text" id="htlTotalPax">1 Room, 1 Adult</span>               
                                    </a>
                                    <div class="dropdown-content d-none p-4 hotel-pax-dropdown" id="hotelRoomSelectDropDown">     
                                        <div class="row custom-gutter">
                                            <div class="col-4">
                                                ROOM 
                                            </div>
                                            <div class="col-6">
                                                  <input type="hidden" value="1" name="PrevNoOfRooms" id="PrevNoOfRooms" />
                                                  <div class="form-control-holder">
                                                     <select class="form-control small no-select2 htl-room-select" id="roomCount" name="roomCount" onchange="ShowRoomDetails();">
                                                        <option selected="selected">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <%--<option value="5">5</option>
                                                        <option value="6">6</option>--%>
                                                    </select>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-1">                                           
                                            <div class="col-12"> <label class="room-label">Room 1</label></div>
                                            <div class="col-4">
                                                 <div class="form-group">  
                                                       <label>Adults</label> 
                                                    <select class="form-control no-select2 adult-pax" name="adtRoom-1" id="adtRoom-1">
                                                                        <option selected="selected" value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                    </select>
                                                                    <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1" />  
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                 <div class="form-group">                                                     
                                                     <label> Child(2-12 yrs)</label> 
                                                     <select onchange="javascript:ShowChildAge('1')" name="chdRoom-1" 
                                                            id="chdRoom-1" class="form-control no-select2  child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                 </div>
                                                
                                            </div>
                                            <div class="col-12" id="ChildBlock-1" name="ChildBlock-1" style="display: none; width: 100%;">
                                                 <div class="row no-gutters child-age-wrapper">                                                     
                                                    <div class="col-4" id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display: none;">
                                                        <div class="form-group">                                                     
                                                             <label>Child 1</label> 
                                                             <select class="form-control" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option> 
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="col-4" id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display: none;">
                                                        <div class="form-group">                                                     
                                                             <label>Child 2</label> 
                                                             <select class="form-control" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 3</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 4</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display: none;" >
                                                        <div class="form-group">
                                                            <label>Child 5</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-6" name="ChildBlock-1-Child-6" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 6</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-6" name="ChildBlock-1-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="col-12" id="childDetails" style="display: none;">
                                                         <!--<p>Child Details</p>-->
                                                     </div>



                                                 </div>
                                            </div>


                                        </div>     
                                        
                                        <div class="row custom-gutter room-wrapper" id="room-2" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 2</label></div>
                                            <div class="col-4">
                                                 <div class="form-group">  
                                                       <label>Adults</label> 
                                                         <select name="adtRoom-2" id="adtRoom-2" class="form-control no-select2 adult-pax">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                        </select>
                                                       <input type="hidden" value="0" id="PrevChildCount-2" name="PrevChildCount-2" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                      <label> Child (2-12 yrs)</label> 
                                                      <select onchange="javascript:ShowChildAge('2')" name="chdRoom-2" id="chdRoom-2" class="form-control no-select2  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                     </select>     
                                                </div>
                                            </div>
                                            <div class="col-12"  id="ChildBlock-2" name="ChildBlock-2" style="display: none;">
                                              <div class="row no-gutters child-age-wrapper">  
                                                <div class="col-4"  id="ChildBlock-2-Child-1" name="ChildBlock-2-Child-1">
                                                   <div class="form-group">  
                                                    <label>Child 1</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-1" name="ChildBlock-2-ChildAge-1">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-2" name="ChildBlock-2-Child-2" style="display: none;">
                                                   <div class="form-group">  
                                                    <label>Child 2</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-2" name="ChildBlock-2-ChildAge-2">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-3" name="ChildBlock-2-Child-3" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 3</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-3" name="ChildBlock-2-ChildAge-3">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-4" name="ChildBlock-2-Child-4" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 4</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-4" name="ChildBlock-2-ChildAge-4">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-5" name="ChildBlock-2-Child-5" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 5</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-5" name="ChildBlock-2-ChildAge-5">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-6" name="ChildBlock-2-Child-6" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 6</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-6" name="ChildBlock-2-ChildAge-6">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-3" style="display: none;">  
                                                <div class="col-12"> <label class="room-label">Room 3</label></div>
                                                    <div class="col-4" >
                                                        <div class="form-group">
                                                            <label>Adults</label> 
                                                            <select name="adtRoom-3" id="adtRoom-3" class="form-control no-select2 adult-pax">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                             </select>
                                                             <input type="hidden" value="0" id="PrevChildCount-3" name="PrevChildCount-3" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4" >
                                                        <div class="form-group">
                                                            <label> Children (2-12 yrs)</label> 
                                                            <select onchange="javascript:ShowChildAge('3')" name="chdRoom-3" id="chdRoom-3" class="form-control  no-select2  child-pax">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12"  id="ChildBlock-3" name="ChildBlock-3" style="display: none;">
                                                        <div class="row no-gutters child-age-wrapper">     
                                                                <div class="col-4"  id="ChildBlock-3-Child-1" name="ChildBlock-3-Child-1" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 1</label>
                                                                    <select class="form-control" id="ChildBlock-3-ChildAge-1" name="ChildBlock-3-ChildAge-1">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-2" name="ChildBlock-3-Child-2" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 2</label>
                                                                    <select class="form-control" id="ChildBlock-3-ChildAge-2" name="ChildBlock-3-ChildAge-2"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-3" name="ChildBlock-3-Child-3" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 3</label>
                                                                    <select class="form-control" id="ChildBlock-3-ChildAge-3" name="ChildBlock-3-ChildAge-3"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-4" name="ChildBlock-3-Child-4" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 4</label>
                                                                    <select class="form-control" id="ChildBlock-3-ChildAge-4" name="ChildBlock-3-ChildAge-4"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-5" name="ChildBlock-3-Child-5" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 5</label>
                                                                    <select class="form-control" id="ChildBlock-3-ChildAge-5" name="ChildBlock-3-ChildAge-5"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-6" name="ChildBlock-3-Child-6" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 6</label>
                                                                    <select class="form-control" id="ChildBlock-3-ChildAge-6" name="ChildBlock-3-ChildAge-6"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>      
                                                        </div>
                                                    </div>

                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-4" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 4</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                          <select name="adtRoom-4" id="adtRoom-4" class="form-control no-select2 adult-pax">
                                                            <option selected="selected" value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                        <input type="hidden" value="0" id="PrevChildCount-4" name="PrevChildCount-4" /> 
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>  Child (2-12 yrs)</label> 
                                                    <select onchange="javascript:ShowChildAge('4')" name="chdRoom-4" id="chdRoom-4" class="form-control no-select2 child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>  

                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-4" name="ChildBlock-4" style="display: none;">
                                                <div class="row no-gutters child-age-wrapper">   
                                                    <div class="col-4" id="ChildBlock-4-Child-1" name="ChildBlock-4-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-1" name="ChildBlock-4-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-2" name="ChildBlock-4-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-2" name="ChildBlock-4-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-3" name="ChildBlock-4-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-3" name="ChildBlock-4-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-4" name="ChildBlock-4-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-4" name="ChildBlock-4-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-5" name="ChildBlock-4-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-5" name="ChildBlock-4-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-6" name="ChildBlock-4-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-6" name="ChildBlock-4-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-5" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 5</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                       <select name="adtRoom-5" id="adtRoom-5" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-5" name="PrevChildCount-5" />    
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                     <select onchange="javascript:ShowChildAge('5')" name="chdRoom-5" id="chdRoom-5" class="form-control  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-5" name="ChildBlock-5" style="display: none;">                                             
                                                    <div class="row no-gutters child-age-wrapper">
                                                        <div class="col-4" id="ChildBlock-5-Child-1" name="ChildBlock-5-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-1" name="ChildBlock-5-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-2" name="ChildBlock-5-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-2" name="ChildBlock-5-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-3" name="ChildBlock-5-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-3" name="ChildBlock-5-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-4" name="ChildBlock-5-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-4" name="ChildBlock-5-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-5" name="ChildBlock-5-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-5" name="ChildBlock-5-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-6" name="ChildBlock-5-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                          <label>Child 6</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-6" name="ChildBlock-5-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>  
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-6" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 6</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                     <select name="adtRoom-6" id="adtRoom-6" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-6" name="PrevChildCount-6" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                          <select onchange="javascript:ShowChildAge('6')" name="chdRoom-6" id="chdRoom-6" class="form-control child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>  
                                                </div>
                                            </div>                                            
                                            <div class="col-12" id="ChildBlock-6" name="ChildBlock-6" style="display: none;">
                                                 <div class="row no-gutters child-age-wrapper">
                                                    <div class="col-4" id="ChildBlock-6-Child-1" name="ChildBlock-6-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 16</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-1" name="ChildBlock-6-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-2" name="ChildBlock-6-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 26</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-2" name="ChildBlock-6-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-3" name="ChildBlock-6-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 36</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-3" name="ChildBlock-6-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-4" name="ChildBlock-6-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 46</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-4" name="ChildBlock-6-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-5" name="ChildBlock-6-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 56</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-5" name="ChildBlock-6-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-6" name="ChildBlock-6-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-6" name="ChildBlock-6-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                                                                                             
                              
  
                                  
                                    </div>
                              </div>
                         </div>
                   
                                   
                            <% if (Settings.LoginInfo.AgentType == AgentType.Agent || Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                                { %>
                              <div class="col-md-6">
                                  <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#HtlSupplierDropdown">
                                        <div class="form-control-holder">
                                            <div class="icon-holder">                                       
                                                 <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text" id="htlSelectedSuppliers">Search Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="HtlSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Search Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12">                               
                                                <table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" runat="server" border="0" cellspacing="0" cellpadding="0"></table>  
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                                 </div>
                              </div>
                            <%} %>
                        
                    </div>
                            <%-- Hidden Contents --%>
                                <div class="col-xs-12 col-md-6" style="display:none">
                                    <div class="form-group">
                                        <!--<label>Select Travel Reason</label> -->
                                        <asp:DropDownList ID="ddlHotelTravelReasons" CssClass="form-control select-element"  runat="server">
                                        <asp:ListItem  Selected="True" Text="Business" Value="Business"></asp:ListItem>
                                        <asp:ListItem Text="Entitlement" Value="Entitlement"></asp:ListItem>
                                        <asp:ListItem Text="Personal Trips" Value="Personal Trips"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>                                  
                                </div>
                                <div class="col-xs-12 col-md-6"  style="display:none">
                                    <div class="form-group">
                                        <!--<label>Specify Traveller / Employee Number</label>-->
                                        <asp:DropDownList ID="ddlHotelEmployee" CssClass="form-control select-element"  runat="server">
                                        </asp:DropDownList>
                                    </div>                                  
                                </div>
                            <%-- END:Hidden Contents --%>

                       <div class="row custom-gutter">   
                             <div class="col-md-12">
                                 <div class="form-control-holder">
                                      <div class="icon-holder">
                                         <span class="icon-bed" aria-label="Origin Airport"></span>
                                      </div>                             
                                     <asp:TextBox placeholder="Search By Hotel Name" CssClass="form-control" ID="txtHotelName" runat="server"></asp:TextBox>                           
                                 </div>             
                              </div>    
                           </div>      
                          
                          <div class="col-xs-12 py-3">
                              <asp:Button CssClass="but but_b pull-right block_xs"   ID="btnHotelSearch" Text="Search Hotels"
                                                                OnClientClick="return HotelSearch();" runat="server" OnClick="btnHotelSearch_Click" />      
                          </div>      
                                
                               
           
                            <div class="clear"></div>    
                               
                                
                    </div>

<%--FLights SEARCH--%>

              <%--      <div class="search-matrix-wrap p-4 search-page-form flights" id="core2" <%=flightDiv %>>   --%>  
                
                                     
                <div class="search-matrix-wrap p-4 search-page-form flights"  data-tab-content="source=Flight" style="display:none" id="core2">         
                       <ul class="tabbed-menu-outline text-uppercase pb-2 d-block float-left" style="display:none">
                           <%--<li class="active"><a href="javascript:void(0);">FLIGHT TICKETS</a></li>--%>
                           <%--<li><a href="Offlinebooking.aspx">OFFLINE BOOKING</a></li>--%>
                           <%
                               //if (Settings.roleFunctionList.Contains("DISPLAYPNR.ASPX")) {
                               //if (Settings.roleFunctionList.Any(p=> p=="DISPLAYPNR.ASPX")) {
                               if (Settings.LoginInfo.IsCorporate != "Y" && Settings.roleFunctionList.Exists(x => x.ToLower().Contains("displaypnr")))
                               {
                                   %>
                           <li><a href="DisplayPNR">IMPORT PNR</a></li><%} %>
                       </ul>


                              
                                 <%if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
                                     { %>

                                 <div class="agent-checkbox-wrap">      
                                         <asp:CheckBox ID="radioAgent"  Text="" runat="server" onclick="disablefield();"/>    
                                         <label class="tgl-btn" for="ctl00_cphTransaction_radioAgent"><em></em></label>
                                </div>

                          
                          <div class="row"> 

                          
                                            
                                        <div class="col-12">        
                                               
                                                  <div id="trAgent" class="custom-radio-table"> 
                                                               
                                                                <div class="col-md-3 mt-2 pl-0">               
                                                                       <asp:RadioButton Visible="false" ID="radioSelf" GroupName="Flight" Checked="true" Text="For Self" onclick="disablefield();" runat="server" />
                                                           </div>   


                                                           <div class="col-md-3 mt-2 pl-0">             
                                                           
                                                           <%--<asp:RadioButton ID="radioAgent" Text="Client" GroupName="Flight" runat="server" onclick="disablefield();"/> </div>    --%>
                                                             </div>
                                                      </div>




                                       </div>     



                                                
                       <div class="col-12">
                           <div class="row custom-gutter">
                               
                                        <div class="col-md-6 col-xs-6" id="wrapper1" style="display:none">
                                                   <asp:DropDownList ID="ddlAirAgents"  AppendDataBoundItems="true"  CssClass="form-control" runat="server"  onchange="LoadAgentLocations(this.id,'F')">
                                                            </asp:DropDownList>
                                                </div>        
                                       
                                 

                                   <div class="col-md-6 col-xs-6" id="wrapper2" style="display: none" >

                                                     <asp:DropDownList ID="ddlAirAgentsLocations" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                           </div>

                       </div>
        
                                                                          
 
                                      
                      
                              </div>
                      
                             <%} %>




                    <div class="custom-radio-table row my-2">                   
                        <div id="CorpDiv" class="col-md-12" style="display:none">                        
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <!--<label>Select Travel Reason</label>-->
                                <asp:DropDownList ID="ddlFlightTravelReasons" AppendDataBoundItems="true" CssClass="form-control select-element"  runat="server">
                                <asp:ListItem Selected="True" Value="-1" Text="Select Reason for Travel"></asp:ListItem>
                                </asp:DropDownList>
                            </div>                                  
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <!-- <label>Specify Traveller / Employee Number</label>-->
                                <asp:DropDownList ID="ddlFlightEmployee" CssClass="form-control select-element"  runat="server">
                                <%--<asp:ListItem Selected="True" Value="-1" Text="Select Traveller / Employee"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>                                  
                        </div>
                            <div class="clearfix"> </div>
                        </div>                       
                        
                        <div class="col-md-3 col-4 mt-2"><asp:RadioButton ID="oneway" GroupName="radio" onclick="disablefield();" runat="server" Text="One Way" Font-Bold="true" />  </div>
                        <div class="col-md-3 col-4 mt-2"> <asp:RadioButton ID="roundtrip" GroupName="radio" onclick="disablefield();" runat="server" Text="Round Trip" Font-Bold="true" /> </div>
                        <div class="col-md-3 col-4 mt-2 hidden-xs"><label style="display:block">
                            <%if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate=="N"){ %>                                
                                                        <asp:RadioButton ID="multicity" GroupName="radio" onclick="disablefield();" runat="server"  Text="Multi City" Font-Bold="true"/>
                            <%} %>
                                                                   </label> </div>

                        <div class="col-md-12">
                                          
                          
                    <div class="row">
                            <div class="col-md-6 mt-2" id="divhidebyagentid" >
                            <asp:RadioButton ID="rbtnSearchByPrice" runat="server" Text="Return Fares" onchange="ShowAllSources()" GroupName="Search" Checked="true"  />
                        </div>
                        <div class="col-md-6 mt-2" id="divSearchOptions">
                            <asp:RadioButton ID="rbtnSearchBySegment" Text="Combination Fares" onchange="ShowRoutingSources()" runat="server" GroupName="Search"  />
                        </div>
                    </div>
                            
                               </div>  
                    <asp:HiddenField ID="hdnBookingAgent" runat="server" />
                             
                    
                              
                           
                            <div class="clearfix"> </div>
                           </div>
                           
                    <div id="tblNormal">

                    <div id="tblNormal1">
                    <div class="row no-gutters"> 
                        
                         <div class="col-12 col-sm">  

                         <label style=" position:absolute; top:0px; right:3px; z-index:999; width:70px; 
                             background-color:#fff; border-left: solid 1px #ccc; height:38px;"> 
                         
                         
                          <div> <span style="font-size:9px; color:Black; position:absolute; top:4px; right:30px; display:block;"> Nearby <br />airports </span> </div>
                         
                         <div> <asp:CheckBox ID="chkNearByPort" CssClass="custom-checkbox-style dark float-right" runat="server" Text=" " /> </div>

                          </label>

                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Origin Airport"></span>
                                 </div>
                                 <asp:TextBox ID="Origin" autocomplete="off" placeholder="From Airport" CssClass="form-control origin-airport" runat="server" onblur="EnableVisaChange();" ></asp:TextBox> 
                                 <div id="multipleCity1"> </div>
                                 <div style="width:100%; line-height:30px; color:#000;top: 38px;" id="statescontainer"></div>
                             </div>   
                         </div>
                         <div class="col swap-airports-wrap">
                              <a href="javascript:void(0);" class="swap-airports" tabindex="-1" title="Switch Airports"> <span class="icon-loop-alt4"></span></a>
                         </div>
                         <div class="col-12 col-sm">  
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Destination Airport"></span>
                                 </div>
                                   <asp:TextBox ID="Destination" autocomplete="off" placeholder="To Airport" CssClass="form-control dest-airport" runat="server" onblur="EnableVisaChange();"></asp:TextBox>
                                <div id="multipleCity2"> </div>
                            <%--       <div id="Div1">--%>
                                    <div style="width:100%; line-height:30px; color:#000;top: 38px;" id="statescontainer2">
                                    </div>
                            <%--    </div>--%>
                             </div> 
                                                     
                         </div>

                         </div>
                        
                    </div>
                    <div class="row custom-gutter" id="tblNormal2">
                         <div class="col-md-6">
                             <div id="fcontainer1" style="display: none;"> </div>
                             <div class="form-control-holder" >
                                  <div class="icon-holder" onclick="showFlightCalendar1()">
                                     <span class="icon-calendar"></span>
                                  </div>
                                   <a href="javascript:void(0);" onclick="showFlightCalendar1()">
                                     <asp:TextBox ID="DepDate" CssClass="form-control" placeholder="Departure Date" autocomplete="off" data-calendar-contentID="#fcontainer1" runat="server"></asp:TextBox> </a>                        
                                  <div class="select-dropdown-holder ml-2 mt-2">
                                     <asp:DropDownList ID="ddlDepTime" CssClass="form-control small"  runat="server">
                                        <asp:ListItem Selected="True" Text="Any Time" Value="Any"></asp:ListItem>
                                        <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                        <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                        <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                        <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                    </asp:DropDownList> 
                                   </div>
                             </div>                             
                         </div>  
                         <div class="col-md-6"  id="textbox_A3">  
                               <div id="fcontainer2" style="display: none;"> </div>     
                              <div class="form-control-holder">
                                    <div class="icon-holder" onclick="showFlightCalendar2()">
                                        <span class="icon-calendar"></span>
                                    </div>
                                    <a href="javascript:void(0);"  onclick="showFlightCalendar2()">                                    
                                      <asp:TextBox ID="ReturnDateTxt"  CssClass="form-control" placeholder="Return Date" autocomplete="off" data-calendar-contentID="#fcontainer2" runat="server"></asp:TextBox> 
                                    </a>      
                                    <div class="select-dropdown-holder ml-2 mt-2">
                                         <asp:DropDownList ID="ddlReturnTime" CssClass="form-control small" runat="server">
                                        <asp:ListItem Selected="True" Text="Any Time" Value="Any"></asp:ListItem>
                                                    <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                    <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                    <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                    <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                    </asp:DropDownList>
                                    </div>                                   
                                </div>          
                         </div>
                    </div>

                    
                   


 
                           
                           
                    </div>
            
            
            
           
              <%-- MultiCity Grid --%>           
                  <div id="tblMultiCity" style="display: none;">
                  <%--    <div class="row no-gutters">
                          <div class="col-12 col-md">
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Origin Airport"></span>
                                 </div>
                              </div>
                          </div>
                          <div class="col swap-airports-wrap">
                              <a href="javascript:void(0);" class="swap-airports" title="Switch Airports"> <span class="icon-loop-alt4"></span></a>
                         </div>
                          <div class="col-12 col-md">  
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Destination Airport"></span>
                                 </div>
                             </div>                                                      
                         </div>
                      </div>--%>
                   
                    <table  width: 100%">
                                <tr>
                                    <td colspan="2">
                                        <table id="tblGrid" runat="server">
                                            <tr>
                                                <td>
                                                    Departure
                                                </td>
                                                <td>
                                                    Arrival
                                                </td>
                                                <td>
                                                    Date
                                                </td>                                             
                                            </tr>
                                            <tr>
                                                <td>
                                                     <div class="form-control-holder">
                                                         <div class="icon-holder">
                                                             <span class="icon-location" aria-label="Origin Airport"></span>
                                                         </div>
                                                           <asp:TextBox ID="City1" runat="server" CssClass="form-control"></asp:TextBox>
                                                          <div style="margin-top: 38px; width:300px; line-height:30px; color:#000; position: absolute;display: none" id="citycontainer1"></div>
                                                      </div>
                                                   
                                                </td>
                                                <td class="px-2">
                                                  <div class="form-control-holder">
                                                     <div class="icon-holder">
                                                         <span class="icon-location" aria-label="Origin Airport"></span>
                                                     </div>
                                                       <asp:TextBox ID="City2" runat="server"  CssClass="form-control"></asp:TextBox>
                                                         <div style=" margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer2">
                                                    </div>
                                                  </div>  
                                                </td>
                                                <td class="w-25">
                                                    <a class="form-control-holder" onclick="showMultiCityCal1()">
                                                        <div class="icon-holder">
                                                            <span class="icon-calendar" aria-label="Destination Airport"></span>
                                                        </div>
                                                        <asp:TextBox ID="Time1" data-calendar-contentID="#mcontainer1" runat="server" CssClass="form-control time_t" ></asp:TextBox>
                                                    </a>                                                              
                                                </td>                                             
                                            </tr> 
                                            <tr><td colspan="3"><asp:CheckBox ID="chkNearByPort1" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                            <tr>
                                                    
                                            <td colspan="3">
                                                    
                                            <table class="w-100"> 
                                                    
                                                    
                                             <tr>
                                                    <td>
                                                         <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                               <asp:TextBox ID="City3" runat="server" CssClass="form-control"></asp:TextBox>
                                                              <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none"" id="citycontainer3">
                                                    </div>
                                                          </div>

                                                  
                                                   
                                                </td>
                                                <td class="px-2">
                                                       <div class="form-control-holder">
                                                         <div class="icon-holder">
                                                             <span class="icon-location" aria-label="Origin Airport"></span>
                                                         </div>
                                                             <asp:TextBox ID="City4" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div id="statesshadow2" style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; ">
                                                                <div style="display: none"" id="citycontainer4">
                                                                </div>
                                                            </div>
                                                      </div>                                                     
                                                </td>
                                              <td class="w-25">
                                                   <a class="form-control-holder" onclick="showMultiCityCal2()">
                                                         <div class="icon-holder">
                                                             <span class="icon-calendar" aria-label="Destination Airport"></span>
                                                         </div>
                                                          <asp:TextBox ID="Time2" data-calendar-contentID="#mcontainer2" runat="server" CssClass="form-control time_t"></asp:TextBox> 
                                                     </a>                                                             
                                                                                                            
                                                </td>                                         
                                            </tr>
                                                    
                                            </table>
                                                    
                                                    
                                                    
                                                </td>
                                            </tr>                                            
                                            <tr><td colspan="3"><asp:CheckBox ID="chkNearByPort2" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>      
                                                  
                                                  
                                                  
                                            <tr>
                                                <td colspan="3">
                                                        
                                                        
                                            <table id="tblRow3" style="display: none;"> 
                                                        
                                                        <tr>
                                                <td>
                                                      <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                            <asp:TextBox ID="City5" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer5"></div>
                                                          </div>
                                                  
                                                </td>
                                                   <td class="px-2">
                                                      <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                          <asp:TextBox ID="City6" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer6"></div>
                                                      </div>                                                        
                                                </td>
                                             <td class="w-25 position-relative">
                                                   <a class="form-control-holder"  onclick="showMultiCityCal3()">
                                                             <div class="icon-holder">
                                                                 <span class="icon-calendar" aria-label="Origin Airport"></span>
                                                             </div>
                                                        <asp:TextBox  ID="Time3" data-calendar-contentID="#mcontainer3" runat="server" CssClass="form-control time_t" ></asp:TextBox>
                                                        
                                                     </a>
                                                     <a class="remove-button" id="" style="cursor: pointer; display: block;" onclick="removeRow(3)">
                                                           <span class="icon-close"></span>
                                                        </a>
                                                <%--  <input type='image' name='imageField' id='image1' src='images/minus.gif' onclick='removeRow(3);return false;'>--%>
                                                </td>
                                                      
                                           
                                            
                                            </tr>
                                            <tr><td colspan="3"><asp:CheckBox ID="chkNearByPort3" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>          
                                            </table>  
                                                        
                                                            
                                                              
                                                        </td>
                                                    </tr>
                                                   
                                                   
                                            <tr>
                                                <td colspan="3">
                                                    <table id="tblRow4" style="display: none;">
                                                       <tr>
                                                        <td>
                                                          <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                            <asp:TextBox ID="City7" runat="server" CssClass="form-control"></asp:TextBox>
                                                             <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer7">
                                                            </div>
                                                          </div>
                                                                    
                                                            
                                                        </td>
                                                  <td class="px-2">
                                                          <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                            <asp:TextBox ID="City8" runat="server" CssClass="form-control"></asp:TextBox>
                                                             <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer8">
                                                            </div>
                                                          </div>
                                                                   
                                                          
                                                        </td>
                                                              
                                                          <td class="w-25 position-relative">
                                                           <a class="form-control-holder" onclick="showMultiCityCal4()">
                                                             <div class="icon-holder">
                                                                 <span class="icon-calendar" aria-label="Dest Airport"></span>
                                                             </div>
                                                       <asp:TextBox ID="Time4" data-calendar-contentID="#mcontainer4" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                               
                                                          </a>  
                                                             <a class="remove-button" id="" style="cursor: pointer; display: block;" onclick="removeRow(4)">
                                                               <span class="icon-close"></span>
                                                            </a>    
                                                                
                                                        </td>
                                                                
                                                     </tr>
                                                                
                                                     <tr><td colspan="3"><asp:CheckBox ID="chkNearByPort4" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>          
                                                    </table>
                                                </td>
                                            </tr>
                                                    
                                                    
                                                    
                                                    
                                            <tr>
                                                <td colspan="3">
                                                    <table id="tblRow5" style="display: none;">
                                                        <tr>
                                                        <td>
                                                           <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                       <asp:TextBox ID="City9" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer9">
                                                            </div>
                                                          </div>       
                                                           
                                                        </td>
                                                           <td class="px-2">
                                                                <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                                <asp:TextBox ID="City10" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer10">
                                                            </div>
                                                          </div>     
                                                           
                                                        </td>
                                                       <td class="w-25 position-relative">
                                                                 <a class="form-control-holder" onclick="showMultiCityCal5()">
                                                             <div class="icon-holder">
                                                                 <span class="icon-calendar" aria-label="Origin Airport"></span>
                                                             </div>
                                                      <asp:TextBox ID="Time5" data-calendar-contentID="#mcontainer5" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                          </a>   
                                                              <a class="remove-button" id="" style="cursor: pointer; display: block;" onclick="removeRow(5)">
                                                               <span class="icon-close"></span>
                                                            </a> 
                                                                    
                                                        </td>
                                                      
                                                      </tr>
                                                    <tr><td colspan="3"><asp:CheckBox ID="chkNearByPort5" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>          
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table id="tblRow6" style="display: none;">
                                                       <tr>
                                                       <td>
                                                           <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                      <asp:TextBox ID="City11" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div class="yui-autocomplete" style="margin-top: 38px;width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer11">
                                                            </div>
                                                          </div>          
                                                            
                                                        </td>
                                                          <td class="px-2">
                                                           <div class="form-control-holder">
                                                             <div class="icon-holder">
                                                                 <span class="icon-location" aria-label="Origin Airport"></span>
                                                             </div>
                                                        <asp:TextBox ID="City12" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div class="yui-autocomplete" style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer12">
                                                            </div>
                                                          </div>         
                                                          
                                                        </td>
                                                         <td class="w-25 position-relative">
                                                          <a class="form-control-holder" onclick="showMultiCityCal6()">
                                                             <div class="icon-holder">
                                                                 <span class="icon-calendar" aria-label="Origin Airport"></span>
                                                             </div>
                                                       <asp:TextBox ID="Time6" data-calendar-contentID="#mcontainer6" runat="server" CssClass="form-control time_t"></asp:TextBox>

                                                          </a>           
                                                            <a class="remove-button" id="" style="cursor: pointer; display: block;" onclick="removeRow(6)">
                                                               <span class="icon-close"></span>
                                                            </a> 
                                                                    
                                                        </td> 
                                                     
                                                       </tr>
                                                    <tr><td colspan="3"><asp:CheckBox ID="chkNearByPort6" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>          
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                               
                                               
                                                <div style=" height:36px">
                                                 
                                                    
                                                   <u> <a style=" color:#fff; cursor:pointer" id="btnAddRow" onclick="addRow();">+ Add More Cities  </a></u> 
                                                    
                                                    </div>
                                                   
                                                   
                                            <div id="errMulti" style="display:none">
                                            <div id="errorMulti" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
    </div>
                                            </div>
                                    </td>
                                </tr>
                            </table>
                            
                    
                 </div>  


                 <div class="row custom-gutter" id="tblNormal3">
                            <div class="col-md-6">
                                <div class="custom-dropdown-wrapper">
                                   
                                    <a id="paxLink" href="javascript:void(0);" <%=Settings.LoginInfo.IsCorporate!="Y" ? "class='form-control-holder form-control-element with-custom-dropdown'" : "class='form-control-holder form-control-element with-custom-dropdown disabled'" %>  data-dropdown="#travellerDropdown">
                                        <div class="icon-holder">
 	                                         <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text" id="SelectedTravelerInfo">1 Adult, 0 Child, 0 Infant</span>               
                                    </a>
                                    <div class="dropdown-content d-none right-aligned p-4" id="travellerDropdown" style="max-width: 200px;">                                       
                                        <div class="row no-gutters">
                                              <div class="col-4">
                                                 <span class="pt-2 d-block"> Adult</span>
                                              </div>
                                              <div class="col-8">
                                                  <asp:DropDownList ID="ddlAdults" CssClass="form-control no-select2 flight-pax" runat="server">
                                                    <asp:ListItem Selected="True" Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                </asp:DropDownList>
                                              </div>
                                          </div> 
                                        <div class="row no-gutters">
                                              <div class="col-4">
                                                 <span class="pt-2 d-block"> Child</span>
                                              </div>
                                              <div class="col-8">
                                                  <asp:DropDownList ID="ddlChilds" CssClass="form-control no-select2 flight-pax" runat="server">
                                                    <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                </asp:DropDownList>
                                              </div>
                                          </div> 
                                        <div class="row no-gutters">
                                              <div class="col-4">
                                                 <span class="pt-2 d-block"> Infant</span>
                                              </div>
                                              <div class="col-8">
                                                  <asp:DropDownList ID="ddlInfants" CssClass="form-control no-select2 flight-pax" runat="server">
                                                    <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                </asp:DropDownList>
                                              </div>
                                          </div>   
                                          <a href="javascript:void(0);" class="mt-2 btn btn-primary btn-sm float-right dd-done-btn">DONE</a>       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">  
                                <div class="form-control-holder">
                                     
                                    <div class="icon-holder">
 	                                     <span class="icon-class"></span>
                                    </div>
                                    <asp:DropDownList ID="ddlBookingClass" runat="server" CssClass="form-control">
                                   <%-- <asp:ListItem  Text="Any" Value="1"></asp:ListItem>
                                    <asp:ListItem Selected="True" Text="Economy" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Premium Economy" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Business" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="First" Value="6"></asp:ListItem>--%>
                                    </asp:DropDownList>  
                                 
                                </div>                
                           </div>
                    </div>


                     <div class="row custom-gutter" id="tblNormal4">
                        <%AgentAppConfig appConfig = new AgentAppConfig();
                            appConfig.AgentID = Settings.LoginInfo.AgentId;
                            string showPA = "display: block;";
                            AgentAppConfig showPAForAgent = appConfig.GetConfigData().Find(x => x.AppKey.ToLower() == "showpreferredairline");
                            if (showPAForAgent != null && showPAForAgent.AppValue.ToLower() == "false")
                                showPA = "display:none;";%>
                         
                         <div class="col-md-6 preferred-airline" style="<%=showPA%>">
                           <div class="form-control-holder">
                                   <div class="icon-holder">                                        
                                        <span class="icon-aircraft-take-off"></span>                     
                                   </div>  
                                     <asp:TextBox ID="txtPreferredAirline" CssClass="form-control" onchange="RecheckAirline(0)" 
                                                                runat="server" onclick="IntDom('statescontainer4' , 'ctl00_cphTransaction_txtPreferredAirline')" placeholder="Type Preferred Airline" ></asp:TextBox>
                               <div id="statescontainer4"  style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;    top: 38px;" ></div>
                                                                        
                                                            <asp:HiddenField ID="airlineCode" runat="server" />
                                                            <asp:HiddenField ID="airlineName" runat="server" />
                                       
                                
                              </div>
                             
                               <div id="divPreferredAirline0"  class="row no-gutters mb-2"> 
                                             
                                        <div class="col-md"> 
                                        <div id="divPrefAirline0" style="display: none">
                                                    <input type="text" id="txtPreferredAirline0" name="txtPreferredAirline0" class="form-control" onclick="IntDom('statescontainer5' , 'txtPreferredAirline0')"
                                                        placeholder="Type Preferred Airline" onchange="RecheckAirline(1)" />
                                                    <div id="statescontainer5" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                              <a class="remove-button" id="removeRowLink0" 
                                                    style="cursor: pointer; display: none" onclick="RemovePrefAirline(0)">
                                                   <span class="icon-close"></span>
                                                </a>
                                            </div>
                                    
                                </div>
                                        <div  class="row no-gutters  mb-2" id="divPreferredAirline1">
                                            <div class="col-md">
                                                <div id="divPrefAirline1" style="display: none">
                                                    <input type="text" id="txtPreferredAirline1" name="txtPreferredAirline1" class="form-control"
                                                        onclick="IntDom('statescontainer6' , 'txtPreferredAirline1')" placeholder="Type Preferred Airline" onchange="RecheckAirline(2)"/>
                                                    <div id="statescontainer6" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink1" 
                                                    style="cursor: pointer; display: none" onclick="RemovePrefAirline(1)">
                                                     <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                                        <div  class="row no-gutters  mb-2" id="divPreferredAirline2">
                                            <div class="col-md">
                                                <div id="divPrefAirline2" style="display: none">
                                                    <input type="text" id="txtPreferredAirline2" name="txtPreferredAirline2" class="form-control"
                                                        onclick="IntDom('statescontainer7' , 'txtPreferredAirline2')" placeholder="Type Preferred Airline" onchange="RecheckAirline(3)"/>
                                                    <div id="statescontainer7" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink2" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(2)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                                        <a class="fcol_fff cursor_point" id="addRowLink" onclick="AddPrefAirline()">+ Add New Preferred Airline</a>

                                     
                        </div>
                        <div class="col-md-6">
                                                         
                  <div class="custom-dropdown-wrapper">    
                      <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                          { %>
               
                                  <a href="jaavscript:void(0);" class="form-control-element with-custom-dropdown" data-dropdown="#SupplierDropdown">
                                    <div class="form-control-holder">
                                         <div class="icon-holder">                                       
                                             <span class="icon-global-settings"></span>                                     
                                        </div>   
                                        <span class="form-control-text" id="selectedSuppliers">Select OR View Suppliers</span>                                  
                                    </div>  
                                 </a>
                                  <%} %>
                                <div class="dropdown-content d-none" id="SupplierDropdown"> 
                                 <div style="" id="Additional-Div" >
                                               
                                                   



    
  
  
  
  
  
   <asp:Label ID="lblSearchSupp" runat="server" Text="Search Suppliers :" Visible="false" Font-Bold="true" ></asp:Label>
     
     
    <div>
    
    <label class="pull-left px-3 pt-3">
    <asp:CheckBox CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" ID="chkFlightAll"  runat="server" Text="Select All"  Checked="true" Visible="true" onclick="selectFlightSources();" /> </label>    

    <div class="clearfix"></div>

    <div class="px-3 pb-3" id="divchkSuppliers">  
        
    <asp:CheckBoxList CssClass="chk-Suppliers-table custom-checkbox-table " ID="chkSuppliers" runat="server" RepeatDirection="Horizontal" Visible="true" onclick="setUncheck();">
    </asp:CheckBoxList>

             <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>               
        <div class="clearfix"></div>

    </div>
     
    </div>
     
     

                                                
                                                    
                                               
                                    </div>
                            </div>

                            </div>
                             <div id="divVisaEnable" style="float: right; margin-top: 26px; margin-right: 30px">
                                 <span class="custom-checkbox-table">
                                     <input type="checkbox" id="chkVisaEnable" onchange="UnSelectNonVisaSources();" /><label for="chkVisaEnable">Need visa Change</label>
                                 </span>
                             </div>

                         </div>
                    </div>

                  <div class="row mt-3">
                     <div class="col-md-7">
                          <asp:RadioButtonList Width="100%" ID= "rbtnlMaxStops" CssClass="custom-radio-table" runat="server" RepeatDirection="Horizontal" >
                                        <asp:ListItem Value="-1" Text="All Flights" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="Direct" ></asp:ListItem>
                                        <asp:ListItem Value="1" Text="1 Stop"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="2 Stops"></asp:ListItem>
                                        </asp:RadioButtonList>
                     </div>
                      <div class="col-md-5 text-right">
                             <asp:CheckBox ID="chkRefundFare" CssClass="custom-checkbox-table" runat="server" Text="Refundable Fares Only" />
                      </div>
                      
                    </div>

          <div class="col-xs-12 py-3">
                          <asp:Button CssClass="but but_b pull-right btn-xs-block" runat="server" ID="btnSearchFlight" Text="Search Flights"  
                                    OnClientClick="return FlightSearch();" OnClick="btnSearchFlight_Click" />
                      </div>
                           

                                
                    <div class="clearfix"></div>
                                                  
                                    
              
                                
                                
                                
                        </div>
                   
                    <ul id="classics2" class="hide">
                                
                    </ul>
                </div>
                <!-- END List Wrap -->
            </div>
          <div class="col-lg-5 col-xl-6">  
                <div class="home-slider-wrapper d-flex flex-column h-100">                    
                    <!-- Tab panes -->
                    <div class="tab-content flex-fill">
                        <div role="tabpanel" class="tab-pane active" id="home">

                             <!--Only for Petrofac-->
                             <% if (Settings.LoginInfo.AgentId ==680 || Settings.LoginInfo.AgentId ==2512 
                                          || Settings.LoginInfo.AgentId ==2785 || Settings.LoginInfo.AgentId ==2786 || Settings.LoginInfo.AgentId ==2787 || Settings.LoginInfo.AgentId ==2788 || Settings.LoginInfo.AgentId ==2826
                                           || Settings.LoginInfo.AgentId ==2827 || Settings.LoginInfo.AgentId ==2828 || Settings.LoginInfo.AgentId ==2931 || Settings.LoginInfo.AgentId ==3551 ) // for Petrofac (All),3551=FTI consultant
                                     { %>
                             
                            <div class="latestupdate" style="bottom:11px;top:auto">	
						        <div class="alert alert-warning p-2 small">
                                    <%if (Settings.LoginInfo.AgentId == 3551)
                                        { %>

                                    <strong>Please <b><a target="_blank" style="cursor:pointer" href="https://travtrolley.com/FITnotes.html">Click Here</a></b> for travel guidlines.</strong>
                                    <%} else{ %>
									<strong>Prior to commencing any international travel please ensure you are aware of quarantine restrictions at all destinations (including where you will be returning to) and that you comply with any Covid19 testing required. For more information see International SOS and IATA Quarantine guidelines available&nbsp;on the <u>Petrofac Hub</u> Travel pages.</strong>
                                    <%} %>
							    </div>
					        </div>

                            <% } %>


                            <%if (clsCMSDet != null && clsCMSDet.sliders != null && clsCMSDet.sliders.Count() > 0)
                            { %>
                            <div class="owl-carousel owl-theme promo-carousel">
                                <%foreach (var slide in clsCMSDet.sliders)
                                { %>
                                <div class="item active"><img src="<%=slide.CMS_IMAGE_FILE_PATH %>" /></div>
                                <%} %>
                            </div>
                            <%} %>
                        </div>   
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_BANK)
                        { %>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="table-responsive" style="padding:10px"><%=clsCMSDet.CMS_BANK_DETAILS %></div>
                        </div>  
                        <%} %>
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_SUPPORT)
                        { %>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="table-responsive" style="padding:10px; line-height:21px"><%=clsCMSDet.CMS_SUPPORT_DETAILS %></div>
                        </div>
                        <%} %>
                    </div>
                <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Promotions</a></li>
                        <!-- Not Required for Corporate   -->
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_BANK)
                        { %>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Bank A/c Details</a></li>
                        <%} %>
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_SUPPORT)
                        { %>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Support</a></li>
                        <%} %>
                    </ul>
                </div>  
              
                <%--<div class="home-slider-wrapper d-flex flex-column h-100">
                    <!-- For Temp  -->
                    
                     <!--<div id="latestupdate" class="latestupdate">		
						<div class="alert alert-notice" id="latestupdate-template">	
								<div class="alert-icon">
									<i class="icon fa fa-exclamation-circle"></i>
								</div>
								<div class="alert-message">
									<div class="latest-update-title">
										<marquee style="line-height: normal;"><strong>NOTE:</strong> REFRESH YOUR BROWSER OR CLEAR HISTORY FOR AN ENHANCED PORTAL EXPERIENCE</marquee>
									</div>
								</div>
							</div>
					</div> -->
                    <!-- For Temp  -->

                     <% string Currency = Settings.LoginInfo.Currency; %>
                    <!-- Tab panes -->
                  <div class="tab-content flex-fill">
                        <div role="tabpanel" class="tab-pane active" id="home">

                            <div class="owl-carousel owl-theme promo-carousel">
                                <%if (Settings.LoginInfo.AgentId == 2976)
                                    {  %>
                                <div class="item">
                                     <img src="images/banner2976.jpg" />
                                </div>
                                <%} else if (Settings.LoginInfo.AgentId ==680 || Settings.LoginInfo.AgentId ==2512 
                                          || Settings.LoginInfo.AgentId ==2785 || Settings.LoginInfo.AgentId ==2786 || Settings.LoginInfo.AgentId ==2787 || Settings.LoginInfo.AgentId ==2788 || Settings.LoginInfo.AgentId ==2826
                                           || Settings.LoginInfo.AgentId ==2827 || Settings.LoginInfo.AgentId ==2828 || Settings.LoginInfo.AgentId ==2931 ) // for Petrofac (All)
                                     { %>
                                <div class="item">
                                     <img src="images/banner_petrofac.jpg" />
                                </div>
                              
                                  <%} else
                                 if (Settings.LoginInfo.AgentId ==2706) // for euro monitor (corp demo)
                                     { %>
                                <div class="item">
                                     <img src="images/Euro_Mon.jpg" />
                                </div>
                              
                                  <%} else if (Currency == "SAR")
                                      { %>
                                 <div class="item">
                                    <img src="images/KSA_B2B_Banner.jpg" />
                                </div>
                               <%}else if (Currency != "INR")
                                     { %>
                                 <div class="item">
                                    <img src="images/banner1.jpg" />
                                </div>
                               
                                 <div class="item">
                                    <img src="images/banner3.jpg" />
                                </div>
                                 <div class="item">
                                    <img src="images/banner4.jpg" />
                                </div>
                                 <div class="item">
                                    <img src="images/banner2.jpg" />
                                </div> 
                                  <%} else   {%>
          <div class="item active">
        <img src="images/GPS_Backdrop_GlobalVisa_70x48in.jpg" />
      </div>

        <div class="item">
        <img src="images/GPS_Backdrop_Holidays_70x48in.jpg" />
      </div>
         <div class="item">
        <img src="images/GPS_Backdrop_Services_70X40in.jpg" />
      </div>
        <%} %>
                            </div>
                        </div>
                      <%if (Settings.LoginInfo.IsCorporate != "Y")
                              { %>
                        <div role="tabpanel" class="tab-pane" id="profile" >
<div class="table-responsive" style="padding:10px"> 
   <%if (Currency == "KWD")
       {%>
<h4 class="bggray">United Arab Emirates </h4>
<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Ahli United Bank</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">1 2 2 9 3 0 1 1 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">KW 18 BKME 0000 0000 0000 0012293011</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">AED</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>Branch</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Fahed Al Salem Street</td>
  </tr>
 <%-- <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EBILAEADSUK</td>
  </tr>-->
</table><br />
    
<%} else if (Currency == "INR")
    { %>
    <h4 class="bggray">INDIA </h4>
<table class="acdetails table table-structure" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">ICICI BANK LIMITED</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Cozmo Travel World Private Limited</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">0 0 1 1 0 5 0 2 6 1 3 9 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IFSC CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">ICIC0000011</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">INR</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>Branch</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAGAR AVENUE, GROUND FLOOR, OPP. SHOPPERS STOP, S.V. ROAD ANDHERI WEST 400058</td>
  </tr>
 <%-- <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EBILAEADSUK</td>
  </tr>-->
</table><br />
    <%}

        else if (Currency != "SAR")
        {%>
<h4 class="bggray p-3">United Arab Emirates </h4>
<table class="acdetails table table-structure" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EMIRATES NBD</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL LLC</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">1 0 1 4 0 4 0 9 2 6 9 0 8 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">AE580260001014040926908</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">AED</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>Branch</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Al Souk</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EBILAEADSUK</td>
  </tr>
</table><br />

<%} else { %>
<h4 class="bggray"> Saudi Arabia </h4> 


<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAMBA</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL LIMITED CO</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">2 1 0 0 2 0 6 0 1 0 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SA69 4000 0000 0021 0020 6010 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAR</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAMBSARI</td>
  </tr>
</table>



<%--<div style=" border-bottom: solid 2px #ccc"> </div>


<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">NCB</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Accoun Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL LTD.CO</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">2 7 0 5 6 2 8 1 0 0 0 1 0 5 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SA53 1000 0027 0562 8100 0105</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAR</td>
  </tr>


</table>
-->

<%} %>

</div>
    
                           
                            
                        </div>
                      <%} %>
                        <div role="tabpanel" class="tab-pane" id="messages" style="display:none">
<div style=" padding:10px">


<div class="paramcon"> 

<div class="marbot_10"> 
<div class="col-md-3"> Amount :  <i class="fcol_red">* </i> </div> 
<div class="col-md-4"> <input type="text" class="form-control">   </div> 
<div class="clearfix"> </div> 
</div>



<div> 
<div class="col-md-3"> Booking Ref Details :  <i class="fcol_red">* </i> </div> 
<div class="col-md-4"> <textarea class="form-control" name="" cols="" rows=""></textarea>  </div> 
<div class="clearfix"> </div> 
</div>



<div class="col-md-12"> 
<h4 class="mt-4">Payment Option </h4> 

 We accept only GCC countries Bank issued Visa / Master Card / American Express credit cards.<br>

<div> <img src="images/payment-logos.jpg">   </div> 


 Payment is processed through an online payment gateway. Your credit card transaction will be processed directly with bank which authorizes your credit card. <br>

<div class="mt-4"> <a class="btn but_b"> Make Payment </a>   </div> 


</div> 



<div class="clearfix"> </div> 

 </div>

            
  
    

 </div>
        <div style="display:none">
            <%--<iframe id="TopUpFrame"  src="AgentPaymentDetails.aspx?ref=Topup" style="height:400px; width:100%;" ></iframe>-->
                
        </div>                          

                        </div>
                        <div role="tabpanel" class="tab-pane" id="settings">
                           <div class="table-responsive" style="padding:10px; line-height:21px">
   
   
<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">

<tr class="hidden-xs"> 

<td> <center> <img height="136" src="images/support-center.png" /></center></td>
</tr>

</table>   
<%if (Currency != "INR")
    { %>
<table  border="0" cellspacing="0" cellpadding="0">

<tr> 

<td colspan="3">  <strong>Contact No.</strong><br /> </td>

</tr>
<%--<tr> 
<td> Landline</td> <td width="60">: </td> <td> +971 60052444</td>


</tr>-->





    <%if (Currency == "SAR")
        { %>

    <tr> <td> Landline</td> <td>: </td> <td>+966 11 5200854 </td></tr>
    <tr> <td> Mobile</td> <td>: </td> <td>+966 55 3008039 </td></tr>
    <%} else { %>
    
   <tr> <td> Landline</td> <td>: </td> <td>+971 6 5074592 </td></tr>
    <tr> <td> Mobile</td> <td>: </td> <td>+971 56  4479659 </td></tr>

    <%} %>





<tr>

<td colspan="3">  <strong>Email:</strong><br /> </td>

</tr>


<tr>
<td>  For any Support Contact</td> <td>: </td> <td> <a href="mailto:B2bsupport@cozmotravel.com">b2bsupport@cozmotravel.com</a></td>

</tr>

<tr>

<td> For cancellations, refunds, reissuance</td> <td>: </td> <td><a href="mailto:B2bsupport@cozmotravel.com">b2bsupport@cozmotravel.com</a> </td>

</tr>
    <%if (Currency != "SAR")
        { %>
<tr> <td> For Hotels, Packages, Tours, Insurance</td> <td>: </td> <td><a href="mailto:B2bholidays@cozmotravel.com">b2bholidays@cozmotravel.com</a> </td>
</tr>
    <%} %>



<%if (Currency != "SAR")
        {%>
    <tr> <td> For Sales</td> <td>: </td> <td><a href="mailto:B2bsales@cozmotravel.com">b2bsales@cozmotravel.com</a></td></tr>
<tr> <td>  For Topup:</td> <td>: </td> <td><a href="mailto:b2btopupcok@cozmotravel.com">b2btopupcok@cozmotravel.com</a> </td></tr>
     <%if (Currency == "AED" && Settings.LoginInfo.IsCorporate != "Y")
        {%>
    <tr><td>&nbsp;</td></tr>
    <tr> <td> Click  <a  style="font-size: large;color:Blue!important" href="<%=Request.Url.Scheme%>://www.travtrolley.com/doc/VMS MealVooucherDoc.docx"> Here </a>To Download <font style="color:#e80808">Meal Voucher Process  </font></td></tr>
     
    <%} %>
<%}
  else
  { %>
<tr> <td>  For Topup:</td> <td>: </td> <td><a href="mailto:Ksatopup@cozmotravel.com">Ksatopup@cozmotravel.com</a> </td></tr>
   

<%} %>


</table>

  <%}
    else
    { %>
       <table  border="0" cellspacing="0" cellpadding="0">

<tr> 

<td colspan="3">  <strong>Contact No.</strong><br /> </td>

</tr>
<%--<tr> 
<td> Landline</td> <td width="60">: </td> <td> +971 60052444</td>


</tr>-->


<tr> <td> Landline</td> <td>: </td> <td>022 7100 4644</td>
</tr>

<tr> <td> Mobile</td> <td>: </td> <td>9967444999</td></tr>

<tr> 

<td colspan="3">  <strong>Email:</strong><br /> </td>

</tr>


<tr> 
<td>  For any Support Email Id</td> <td>: </td> <td> <a href="supportb2b@cozmotravelworld.com">supportb2b@cozmotravelworld.com</a></td>

</tr>

<tr> 

<td> For INFO Email Id</td> <td>: </td> <td><a href="infob2b@cozmotravelworld.com">infob2b@cozmotravelworld.com</a> </td>

</tr>

<tr> <td> For TOP UP Email id</td> <td>: </td> <td><a href="topupb2b@cozmotravelworld.com">topupb2b@cozmotravelworld.com</a> </td>
</tr>


<tr> 
<td> For SALES Email Id</td> <td>: </td> <td><a href=" salesb2b@cozmotravelworld.com"> salesb2b@cozmotravelworld.com</a></td>

</tr>
</table>

       <%} %>

   


  

 
 
 </div>
                        </div>
                    </div>
              <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Promotions</a></li>
                      <%if (Settings.LoginInfo.IsCorporate != "Y" && !Settings.LoginInfo.AgentName.Contains("IBYTA"))
                          { %>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Bank A/c Details</a></li>
                     
                    <%--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Agent Topup</a></li>--
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Support</a></li>
                       <%} %>
                  </ul>
                </div>--%>    
          </div>
        </div>
      </div>

  </div>
          
     
     
      <%-- right panel startes here--%>
      
      
      
      <div class="row">

            <div  data-backdrop="static" id="ShowMsgDiv" tabindex="-1" role="dialog" aria-labelledby="ShowMsgDiv" style="display:none">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return window.close('ShowMsgDiv');" style="color:#fff;opacity:1;"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Message Details</h4>
                        </div>
                        <div class="modal-body">                                          
                              <asp:Label ID="lblFare" runat="server" Text=""></asp:Label>
                               <table class="table table-striped table-condensed" >       
                                        <tr>
                                            <td height="30px" align="left">
                                                From:
                                            </td>
                                            <td align="left">
                                                <label id="From" name="From">
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30px" align="left">
                                                Subject:
                                            </td>
                                            <td align="left">
                                                <label id="Subject" name="Subject">
                                                </label>
                                            </td>
                                        </tr>            
                                        <tr>
                                            <td height="30px" align="left">
                                                Message:
                                            </td>
                                            <td align="left">
                                                <label id="Message" name="Message">
                                                </label>
                                                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>

                        </div>
                </div>
                </div>
            </div>











     <div class="col-md-6 ">  
     
     <%--<div class="row"> --%>
     
     <div class="col-8"> <h3> Important Messages</h3> </div>
     <%--<div class="col-4 mt-4 text-right"> <a style="color:Blue!important" href="http://www.travtrolley.com/doc/VMS MealVooucherDoc.docx"> Meal Voucher Process </a> </div>
     
     </div>--%>

          <div style="overflow-y:scroll; height:180px; border: solid 1px #ccc">
               
                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:GridView CssClass="grdTable" ID="gvMessages" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="true" PageSize="8" DataKeyNames="msg_id" CellPadding="0" BorderWidth="0" 
                                CellSpacing="0" GridLines="Horizontal" BorderColor="White" 
                                OnPageIndexChanging="gvMessages_PageIndexChanging" >
                                
                                <HeaderStyle CssClass="showMsgHeading">
 </HeaderStyle>
                                <Columns>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                           <label>
                                                <b>From</b></label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMsgFrom" runat="server" Text='<%# Eval("MsgFrom") %>' ToolTip='<%# Eval("MsgFrom") %>' Width="120px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                   <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <label>
                                                <b>Subject</b></label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                                                <a  style="width:190px;cursor:pointer" onclick="GetSelectedRow(this,'ShowMsgDiv')" ><%# Eval("subject") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <label >
                                                <b>ReceivedOn</b></label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblReceivedOn" runat="server" Text='<%# Eval("ReceivedOn") %>' ToolTip='<%# Eval("ReceivedOn") %>' Width="80px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                       <ItemTemplate>
                                       <asp:HiddenField ID="IThdfMessage" runat="server" Value='<%# Eval("message") %>' />
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
               </div>
     </div>
     
     
      <div class="col-md-6">  
      
      <h3>  Booking History</h3>
      
      <div class="">
                <div class="nA-h3">
                    Booking Made Today</div>
                <div class="nA-h4 ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td id="tdFligtsBkgCount1" <%=flightsCountEnable %>>
                                <table>
                                    <tr >
                                        <td width="5%">
                                            <img class="row-image" src="images/flight_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Flights : 
                                            <%--<asp:Label ID="lblFlightsBookingCountToday" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblFlightsBookingCountToday" runat="server" NavigateUrl="~/AgentBookingQueue?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                           <td id="tdHotelsBkgCount1" <%=hotelsCountEnable %>>
                                <table>
                                    <tr >
                                        <td width="5%">
                                            <img class="row-image" src="images/hotel_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Hotels : 
                                            <%--<asp:Label ID="lblHotelsBookingCountToday" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblHotelsBookingCountToday" runat="server" NavigateUrl="~/HotelBookingQueue?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%--<td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/car_sml.png" width="23" height="16" />
                                        </td>
                                        <td width="95%">
                                            Transfers : (0)
                                        </td>
                                    </tr>
                                </table>
                            </td>--%>
                            <%--<td width="30%">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/packages_sml.png" width="19" height="19" />
                                        </td>
                                        <td width="95%">
                                            Packages :(0)
                                        </td>
                                    </tr>
                                </table>
                            </td>--%>
                        </tr>
                    </table>
                </div>
                <div class="nA-h3">
                    Booking Made in Last 7 Days
                </div>
                <div class="nA-h4 ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                               <td id="tdFligtsBkgCount2" <%=flightsCountEnable %> >
                                <table width="100%" border="0"  cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/flight_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Flights : 
                                            <%--<asp:Label ID="lblFlightsBkgWeeklyCount" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblFlightsBkgWeeklyCount" runat="server" NavigateUrl="~/AgentBookingQueue?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                           <td id="tdHotelsBkgCount2" <%=hotelsCountEnable %>>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/hotel_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Hotels : 
                                            <%--<asp:Label ID="lblHotelsBkgWeeklyCount" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblHotelsBkgWeeklyCount" runat="server" NavigateUrl="~/HotelBookingQueue?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%--<td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/car_sml.png" width="23" height="16" />
                                        </td>
                                        <td width="95%">
                                            Transfers : (0)
                                        </td>
                                    </tr>
                                </table>
                            </td>--%>
                            <%--<td width="30%">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/packages_sml.png" width="19" height="19" />
                                        </td>
                                        <td width="95%">
                                            Packages :(0)
                                        </td>
                                    </tr>
                                </table>
                            </td>--%>
                        </tr>
                    </table>
                </div>
                <div class="nA-h3">
                    Booking Made in Last 30 Days</div>
                <div class="nA-h4 ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td id="tdFligtsBkgCount3" <%=flightsCountEnable %>>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/flight_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Flights : 
                                            <%--<asp:Label ID="lblFlightsBkgMonthlyCount" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblFlightsBkgMonthlyCount" runat="server" NavigateUrl="~/AgentBookingQueue?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                              <td id="tdHotelsBkgCount3" <%=hotelsCountEnable %>>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/hotel_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Hotels : 
                                            <%--<asp:Label ID="lblHotelsBkgMonthlyCount" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblHotelsBkgMonthlyCount" runat="server" NavigateUrl="~/HotelBookingQueue?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%--<td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/car_sml.png" width="23" height="16" />
                                        </td>
                                        <td width="95%">
                                            Transfers : (0)
                                        </td>
                                    </tr>
                                </table>
                            </td>--%>
                            <%--<td width="30%">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/packages_sml.png" width="19" height="19" />
                                        </td>
                                        <td width="95%">
                                            Packages :(0)
                                        </td>
                                    </tr>
                                </table>
                            </td>--%>
                        </tr>
                    </table>
                </div>
                <br />
                
               
            </div>
 
     
     </div>
     
      
      <div class="clearfix"></div>
      
      </div>
      
      
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <%--   <% if (Request["sessionId"] != null && Basket.FlightBookingSession.ContainsKey(Request["sessionId"]) && Basket.FlightBookingSession[Request["sessionId"]].Result.Length > 0 && Request["show"] == "anotherfare")
      { %>
    <script type="text/javascript">
        AjaxSearchResultBlockWhenBack();
    </script>
  <% }%>--%>
    <% if (Request["calendarSearch"] != null && Request["calendarSearch"] == "CalendarSearch")
       { %>

    <script type="text/javascript">
        AjaxSearchResultForCalendar();
    </script>

    <% } %>
    <%if (dateChangeSearch)
      { %>

    <script type="text/javascript">
        AjaxSearchResultForChangedDate();
    </script>

    <%}%>
    <%if ((Session["agencyId"] != null && Convert.ToInt32(Session["agencyId"]) > 0) && (!Convert.ToBoolean(Session["isB2B2BAgent"])))
      { %>
    <label style="width: 180px; color: Red; font-weight: bold;display:none">
        Check the new <a href="calendarbookingview" target="_blank">Air Passenger 
        Calendar</a></label>
    <%} %>
    
    
   




<div id="PreLoader" style="display:none;position:relative;top:0;">
        <div class="loadingDiv">
        <div style="font-size: 14px; color:#FE642E">
        <strong>
                <span id="isUAE"></span>
                    </strong>
        </div>
            <div id="imgHotelLoading">
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Loading results for</strong></div>
            <div style="font-size: 21px;" class="primary-color">
                <strong>
                <span id="searchCity"></span>
                    </strong></div>
        </div>
        <div class="parameterDiv" style="top:435px">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%">
                        <label class="primary-color">
                            <b class="primary-color">Check In:</b></label>
                        
                        <strong> <span id="fromDate"></span></strong>
                    </td>
                    <td width="50%">
                        <label class="primary-color">
                            <b class="primary-color">Check out:</b></label>
                        <strong> <span id="toDate"></span></strong>
                    </td>
                </tr>
            </table>
        </div>
    </div>
      <!-- loading for flight -->
      
      <div id="FlightPreLoader" style=" width:100%;  z-index:999;  display:none;position: relative;top: 0;">
    <div class="loadingDiv">
<div id="imgFlightLoading"> <img id="imgPreloader" height="200px" src="<%=Request.Url.Scheme%>://gocozmo.com/images/preloaderFlight.gif" /></div>

<div style="font-size:16px; color:#999999 "><strong> Please wait... </strong></div>
<div style="font-size:21px; ">We are searching for the best available flights</div>
<div class="" id="onetwoDiv">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
      
      <table id="onewaytable" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="flightOnwards" class="primary-color"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="deptDate"></span></td>
        </tr>
      </table>
      
      </td>
      
      
      <td>
      
      <table id="twowaytable" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/arrival_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="flightReturn" class="primary-color"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="arrivalDate"></span></td>
        </tr>
      </table>
      
      
      </td>
    </tr>
  </table>
</div>

<div class="" id="multiDiv" style="display:none;">

<center>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
      
      <table  id="multiwayable1" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left">
          <label class="primary-color"><strong><span id="multiway1"></span></strong></label>
          
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="multiDate1"></span></td>
        </tr>
      </table>
      
      </td>
      
      <td>
      
   <table  id="multiwayable2" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="multiway2"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="multiDate2"></span></td>
        </tr>
      </table>
      
      
      </td>  
     
      </tr>
      
    

      
    
      
       <tr>
       
        <td>

      
      <table id="multiwayable3" style="display:none;" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="multiway3"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="multiDate3"></span></td>
        </tr>
      </table>
      
       <td>   <table id="multiwayable4" style="display:none;" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="multiway4"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="multiDate4"></span></td>
        </tr>
      </table> </td>
      
    
      </td>
      
       
      </tr>
    
      <tr> 
      
      <td> 
      
       <table id="multiwayable5" style="display:none;" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="multiway5"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="multiDate5"></span></td>
        </tr>
      </table>
      
      
      </td> 
      
      
       <td>
      
  
   
      
      <table  id="multiwayable6" style="display:none;" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%"><img src="images/departure_flight.png" width="16" height="15" /></td>
          <td width="98%" align="left"><label class="primary-color"><strong><span id="multiway6"></span></strong></label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left"><span style=" font-size:21px" id="multiDate6"></span></td>
        </tr>
      </table>
      
      </td>
      
       </tr>
       
       
    
 
    
    
  </table>
  </center>
  
</div>

</div>




</div>
    
<%--<script src="Scripts/select2.min.js" type="text/javascript"></script>--%>
<script type="text/javascript">
    $('.select-element').select2();
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        items: 1,
        nav: true,
        autoplay: true,
        autoplayTimeout:3000,// 3 Seconds
        autoplayHoverPause:true
    })
    //FlightPax Count
    function flightPaxCount() {
        var adt = $('#ctl00_cphTransaction_ddlAdults').val(),
                   chd = $('#ctl00_cphTransaction_ddlChilds').val(),
                   inf = $('#ctl00_cphTransaction_ddlInfants').val();

                   var FlightTotalPax = '<strong>';
                   FlightTotalPax += adt + '</strong> ';
                   FlightTotalPax += 'Adult';
                   if (chd > 0) {
                       FlightTotalPax += ' , <strong>' + chd + '</strong> Child';
                   }                  
                   if (inf > 0) {
                       FlightTotalPax += ' , <strong>' + inf + '</strong> infant';
                   }                  
                   $('#SelectedTravelerInfo').html(FlightTotalPax);

        //$('#SelectedTravelerInfo').text(adt + ' Adult ,' + chd + ' Child ,' + inf + ' Infant');
    }
    $('.flight-pax').change(function(){
        flightPaxCount()
    })
    flightPaxCount()

    //Hotel Pax Count
    function hotelPaxCount() {
        var roomCount = $('#PrevNoOfRooms').val();
        var htlAdultCount = 0,
            htlChildCount = 0;
        $('#hotelRoomSelectDropDown select.adult-pax').each(function (index) {
            count = index + 1;
            if (count > roomCount) {
                return false;
            }
            htlAdultCount += parseInt($(this).val());
        })
        $('#hotelRoomSelectDropDown select.child-pax').each(function (index) {
            count = index + 1;
            if (count > roomCount) {
                return false;
            }
            htlChildCount += parseInt($(this).val());
        });
        var HotelTotalPax = '<strong>';
            HotelTotalPax += roomCount;
            HotelTotalPax += '</strong> Room , <strong>';       
            HotelTotalPax += htlAdultCount;
            HotelTotalPax += '</strong> Adult ';        
        if (htlChildCount > 0) {
            HotelTotalPax += ', <strong>' + htlChildCount + '</strong> Child';        
        }
        $('#htlTotalPax').html(HotelTotalPax);
    }
    hotelPaxCount()    
    $('.hotel-pax-dropdown select').change(function(){      
        hotelPaxCount()
    })


    //Activete Hotel/Flight Search Tab based on URL query string
    var URLLocation = location.href;
    var URLparam = URLLocation.substring(URLLocation.indexOf("?") + 1);
    //$('[data-tab-content]').hide();
    $('[data-tab-content="'+URLparam.replace('&domain=yes','')+'"]').show();


</script>

<!-- loading for flight end-->
 <script type="text/javascript">
     function GetSelectedRow(lnk, id) {
         var row = lnk.parentNode.parentNode;
         var gv = document.getElementById("<%=gvMessages.ClientID %>");
         var rwIndex = row.rowIndex;

         document.getElementById('From').innerHTML = gv.rows[rwIndex].cells[0].childNodes[1].innerHTML;
         document.getElementById('Subject').innerHTML = gv.rows[rwIndex].cells[1].childNodes[1].innerHTML;
         document.getElementById('Message').innerHTML = gv.rows[rwIndex].cells[3].childNodes[1].value;

         //var div = document.getElementById(id);
         //div.style.visibility = 'visible';
         $('#' + id).show();
         return false;
     }

     function close(id) {
         //var div = document.getElementById(id);
         //div.style.visibility = 'hidden';
         $('#' + id).hide();
         return false;
     }
     $(function () {
     <%if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled)
                              { %>
     $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divSearchOptions').show();       
                ShowAllSources();//Show all sources for oneway
     <%}else{%>
     $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divSearchOptions').hide();       
                ShowAllSources();//Show all sources for oneway
     <%}%>
          <%if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsReturnFareEnabled)
                              { %>
     $('#ctl00_cphTransaction_rbtnSearchByPrice').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divhidebyagentid').show();       
                ShowAllSources();//Show all sources for oneway
     <%}else{%>
     $('#ctl00_cphTransaction_rbtnSearchBySegment').prop('checked', true);//Set Search by Price checked by default before hiding the option
                $('#divhidebyagentid').hide();       
               ShowRoutingSources();//Show all sources for oneway
     <%}%>         
     });
    </script>     
    <%if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack)
      {
          if (Page.PreviousPage.Title == "Hotel Search Results")
          { %>
          <script>
              
              <%if(Settings.LoginInfo.OnBehalfAgentLocation > 0) {%> 
              LoadAgentLocations('ct100_cphTransaction_ddlAgents', 'H');
              <%}%>
              SetModifyValues();
                  disablefield();    
          </script>
          <%}
              else if (searchRequest.Segments != null)
              {
                %>
                <script>
                    document.getElementById('<%=hdnIsCrossPostBack.ClientID %>').value = "true";
                    disablefield();  
                </script>
                <%
    if (searchRequest.Type == CT.BookingEngine.SearchType.MultiWay)
    {%>

    <script>
        var rows = eval('<%=(searchRequest.Segments != null && searchRequest.Segments.Length > 2 ? (searchRequest.Segments.Length - 2) : 0) %>');
        if (rows > 0) {
            for (var i = 0; i < rows; i++) {
                addRow();
            }
        }      
       
    </script> 
    
    <%}

            }

        }
        if (Settings.LoginInfo.IsCorporate == "Y")
        { %>
    <script>$('#CorpDiv').show();</script>
    <%}%>
    <script type="text/javascript">
        $("#<%=ddlFlightEmployee.ClientID%>").change(function () {
            $("#<%=ddlFlightEmployee.ClientID%> option:selected").each(function () {
                $('#<%=hdnFlightEmployee.ClientID%>').val($(this).val());
            });
        });
        $("#<%=ddlFlightTravelReasons.ClientID%>").change(function () {
            $("#<%=ddlFlightTravelReasons.ClientID%> option:selected").each(function () {
                $('#<%=hdnTravelReason.ClientID%>').val($(this).val());
            });
        });
    </script>
<script type="text/javascript">
    function EnableVisaChange() {

        var airportCodes = ['SHJ'];
        var isOnewayChecked = $("#ctl00_cphTransaction_oneway").is(":checked");
        var isRoundtripChecked = $("#ctl00_cphTransaction_roundtrip").is(":checked");
        var isReturnFaresChecked = $("#ctl00_cphTransaction_rbtnSearchByPrice").is(":checked");
        var origin = $('#ctl00_cphTransaction_Origin').val().toUpperCase().trim();
        var destination = $('#ctl00_cphTransaction_Destination').val().toUpperCase().trim();
        if ((isOnewayChecked || isRoundtripChecked) && isReturnFaresChecked && <%=(Settings.LoginInfo.IsCorporate == "N").ToString().ToLower()%> && <%=(Settings.LoginInfo.Currency == "AED").ToString().ToLower()%>) {
            if ((origin != undefined && origin.length >= 3)) {
                var code = origin.split(')')[0].replace('(', '');
                var dest = destination.split(')')[0].replace('(', '');
                if (airportCodes.includes(code)) {//Validating only Origin for Visa Change
                    $('#chkVisaEnable').prop("checked", false);
                    $('#divVisaEnable').show();
                }
                else {
                    $('#divVisaEnable').hide();
                    $('#chkVisaEnable').prop('checked', false);
                    EnableAllSources();
                }
            }
            else {
                $('#divVisaEnable').hide();
                $('#chkVisaEnable').prop("checked", false);
                EnableAllSources();
            }
        } else {
            $('#divVisaEnable').hide();
            $('#chkVisaEnable').prop("checked", false);
            EnableAllSources();
        }
    }
    function UnSelectNonVisaSources() {
        var sources = ['FI']
        var isChecked = $("#chkVisaEnable").is(":checked");
        $('#Additional-Div input[type=checkbox]').each(function () {
            var id = $(this).attr('id');
            var src = $('#' + id).next('label').text().toUpperCase().trim();
            if (sources.includes(src) && isChecked) {
                $('#' + id).prop("checked", true);
                $('#' + id).prop("disabled", true);
            }
            else if (isChecked) {
                $('#' + id).prop("checked", false);
                $('#' + id).prop("disabled", true);
            }
            else {
                $('#' + id).prop("checked", true);
                $('#' + id).prop("disabled", false);
            }
        });
    }

    function EnableAllSources() {
        $('#Additional-Div input[type=checkbox]').each(function () {
            var id = $(this).attr('id');
            
            $('#' + id).prop("checked", true);
            $('#' + id).prop("disabled", false);
        });
    }
   
    </script>
</asp:Content>
