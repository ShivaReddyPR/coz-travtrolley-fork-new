﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Collections;
using System.Configuration;
using CT.Core;
public partial class FixedDepartureChangeRequestQueue : System.Web.UI.Page
{
    PagedDataSource pagedData = new PagedDataSource();
    bool isShowAll = false;
    protected AgentMaster agency;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                isShowAll = true;
                InitializeControls();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception,Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "pageLoad", "0");
        }
    }
    private void InitializeControls()
    {
        try
        {
            BindAgency();
            Clear();
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindAgency()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--Select Agency--", "0"));

            ddlAgency.SelectedIndex = -1;
            if (Settings.LoginInfo.AgentId > 0)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            if (Settings.LoginInfo.AgentId > 1)
            {
                ddlAgency.Enabled = false;
            }
            else
            {
                ddlAgency.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetFixedDepartureList();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "searchEvent", "0");
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "ClearEvent", "0");
        }
    }
    private void Clear()
    {
        txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        txtTripId.Text = string.Empty;
        txtPaxName.Text = string.Empty;
        txtPrice.Text = "0.00";
        isShowAll = true;
        Session["FixedDepartureQueue"] = null;
        GetFixedDepartureList();
    }


    void GetFixedDepartureList()
    {
        try
        {
            try
            {
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                string StartFromDate = string.Empty;
                if (!isShowAll)
                    StartFromDate = (txtFrom.Text);
                else
                    StartFromDate = DateTime.MinValue.ToString();
                DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
                string StartToDate = (txtTo.Text);
                DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
                toDate = Convert.ToDateTime(toDate.ToString("MM/dd/yyyy 23:59:59"));
                string tripId = txtTripId.Text;
                string paxName = txtPaxName.Text;
                decimal price = Convert.ToDecimal(txtPrice.Text);
                
                int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                DataTable fdQueue = Activity.GetFDChangeRequestQueue(fromDate, toDate, tripId, paxName, price, agencyId, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID, "Y");
                Session["FixedDepartureQueue"] = fdQueue;
                dlFDQueue.DataSource = fdQueue;
                dlFDQueue.DataBind();
                CurrentPage = 0;
                doPaging();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #region Paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        DataTable dt = (DataTable)Session["FixedDepartureQueue"];
        pagedData.DataSource = dt.DefaultView;
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlFDQueue.DataSource = pagedData;
        dlFDQueue.DataBind();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage--;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "", "0");
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage++;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "pageLoad", "0");
        }
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "pageLoad", "0");
        }
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage = (int)Session["count"];
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "pageLoad", "0");
        }
    }
    #endregion

    protected void dlFDQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Refund")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    int fixId = Convert.ToInt32(e.CommandArgument);
                    DataSet ds = Activity.GetActivityQueueDetails(fixId);
                    Activity.UpdateStatusForTransactionHeader(fixId, "Z"); //Here Z means Cancel
                    //Cancellation changes.
                    CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                    cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                    //cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                    cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                    cancellationCharge.ReferenceId = fixId;
                    cancellationCharge.ProductType = ProductType.FixedDeparture;
                    cancellationCharge.CancelPenalty = 0;
                    cancellationCharge.CreatedBy = (int)Settings.LoginInfo.UserID;
                    cancellationCharge.Save();

                    AgentMaster agent = new AgentMaster(Convert.ToInt32(ds.Tables[0].Rows[0]["AgencyId"]));
                    Hashtable table = new Hashtable(3);
                    table.Add("agentName", agent.Name);
                    table.Add("FixedDepartureName", ds.Tables[0].Rows[0]["activityName"].ToString());
                    table.Add("TripId", ds.Tables[0].Rows[0]["TripId"].ToString());

                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    UserMaster bookedUser = new UserMaster(Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"].ToString()));
                    toArray.Add(bookedUser.Email);
                    toArray.Add(agent.Email1);
                    string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["FIXED_CANCEL_MAIL"]).Split(';');
                    foreach (string cnMail in cancelMails)
                    {
                        toArray.Add(cnMail);
                    }

                    string message = ConfigurationManager.AppSettings["FIXED_DEPARTURE_REFUND"];
                    try
                    {
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(FixedDeparture) Request for cancellation. TripId:(" + ds.Tables[0].Rows[0]["TripId"].ToString() + ")", message, table);
                    }
                    catch
                    {

                    }
                    Response.Redirect("FixedDepartureChangeRequestQueue.aspx",false);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "itemCommand", "0");
        }
    }
    protected void dlFDQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                HiddenField hdnQuoted = e.Item.FindControl("hdnQuoted") as HiddenField;
                HtmlTable tblRefund = e.Item.FindControl("tblRefund") as HtmlTable;
                Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                int agentId = Convert.ToInt32(((DataRowView)e.Item.DataItem).Row.ItemArray[16]);
                AgentMaster agent = new AgentMaster(agentId);
                if (hdnQuoted.Value.Trim() == "Y")
                {
                    tblRefund.Visible = true;
                    lblStatus.Visible = true;
                    lblStatus.Text = "REQUESTED FOR CANCEL";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else if(hdnQuoted.Value.Trim() == "Z")
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "CANCELLED";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "itemDataBound", "0");
        }
    }
    protected string CTCurrencyFormat(object currency, object AgentId)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency + " " + Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
        }
        else
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency + " " + Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
        }
    }

}
