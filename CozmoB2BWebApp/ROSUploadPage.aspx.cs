using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;

public partial class ROSUploadPageUI : CT.Core.ParentPage
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport);
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text=this.Title;
               // BindVisaType();
                //Mode = PageMode.Add;
                //InitializePageControls();
                //Clear();
                //lblSuccessMsg.Text = string.Empty;
              
            }
        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtROSDocNo.Text.Trim()))
                if (txtROSDocNo.Text.Trim() != hdfDocNo.Value)
                    hdfDocNo.Value = txtROSDocNo.Text.Trim();

            BindGrid();
            //txtROSDocNo.Text= hdfDocNo.Value;            
            //string source = CT.TicketReceipt.Common.Utility.ToString(Request.QueryString["source"]);
            //if(source=="VisaReco")
            //    visaReconcile();
            //else
            //    handlingReconcile();

        }
        catch { throw; }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("ROSUploadPage.aspx");

        }
        catch { throw; }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
           // selectedItem();
            FilterControls();
            // loadExcelData(((DataTable)gvRosterList.DataSource).Copy());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvROSUploadMismatchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {          
            
            gvROSUploadMismatchList.PageIndex = e.NewPageIndex;
            gvROSUploadMismatchList.EditIndex = -1;
            //BindGrid(false);
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    private void BindGrid()
    {

        try
        {
            DataTable dt = CT.Roster.RosterUpload.GetROSReconcileDetails(hdfDocNo.Value, "N");
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvROSUploadMismatchList, dt);
            dvPaxGrid.Visible = true;
        }
        catch { throw; }


    }
    protected void FilterControls()
    {

        try
        {

            string[,] textboxesNColumns ={{ "HTtxtDocNumber", "UPLD_RM_DOC_NUMBER" },{"HTtxtFileName", "UPLD_FILE_NAME" }, { "HTtxtStaffId", "UPLD_STAFF_ID" }, { "HTtxtFlightNo", "UPLD_FLIGHT_NO" },
            { "HTRMDate", "UPLD_RM_DATE" },{ "HTtxtFromSector", "UPLD_FROM_SECTOR" },{ "HTtxtToSector", "UPLD_TO_SECTOR" }};

            DataTable dt = CT.Roster.RosterUpload.GetROSReconcileDetails(hdfDocNo.Value, "N");
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvROSUploadMismatchList, dt , textboxesNColumns);

        }
        catch { throw; }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            FilterControls();
            DataTable dt = ((DataTable)gvROSUploadMismatchList.DataSource).Copy();
            string attachment = "attachment; filename=ROSMisMatchList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgROSMisMatchList.AllowPaging = false;
            dgROSMisMatchList.DataSource = dt;
            dgROSMisMatchList.DataBind();
            dgROSMisMatchList.RenderControl(htw);
            Response.Write(sw.ToString());
            //string script = "window.open('ExportExcelVisaList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
        finally
        {
            Response.End();
        }
    }
    //private void BindVisaType()
    //{
    //    try
    //    {
    //        ddlVisaType.DataSource = VisaTypeMaster.GetList(ListStatus.Short, RecordStatus.Activated, 0, Settings.LoginInfo.AgentId);
    //        ddlVisaType.DataValueField = "VISA_TYPE_ID";
    //        ddlVisaType.DataTextField = "VISA_TYPE_NAME";
    //        ddlVisaType.DataBind();
    //        ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
    //    }
    //    catch { throw; }
    //}





    #region Date Format
    protected string CZDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }

    protected string CZTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("hh:MM");
        }
    }
    #endregion

}
