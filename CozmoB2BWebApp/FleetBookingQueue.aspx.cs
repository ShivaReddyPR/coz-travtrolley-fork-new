﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.Core;
using CT.Configuration;
using CT.MetaSearchEngine;
public partial class FleetBookingQueue : CT.Core.ParentPage
{

    protected List<int> sourcesList = new List<int>();
    protected List<int> selectedSources = new List<int>();
    protected int pageNo;
    protected int noOfPages;
    protected string show = string.Empty;
    protected UserMaster loggedMember;
    private List<int> shownStatuses = new List<int>();
    protected bool filtered = false;
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
    protected int locationId;
    protected string Cancelled = string.Empty; // For filtering with paging 
    protected string Confirmed = string.Empty; // For filtering with paging 
    protected string reqType = "both"; // For filtering with paging
    protected int agentFilter;// For restricted filtering
    protected string FleetFilter = string.Empty; // For restricted filtering
    protected string paxFilter = string.Empty;// For restricted filtering
    protected string pnrFilter = string.Empty;// For restricted filtering
    protected string lastCanDate = string.Empty;
    protected List<int> bookingModes = new List<int>();
    protected List<fleetAgentBookingBO> filteredResults;
    protected int maxResult, minResult;
    protected string pagingScript = string.Empty;
    int currentPageNo = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (Settings.LoginInfo != null)
            {

                Page.Title = "Fleet Booking Queue";
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    hdfParam.Value = "0";
                    if (Request["FromDate"] != null)
                    {
                        CheckIn.Text = Request["FromDate"];
                    }
                    else
                    {
                        CheckIn.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    }
                    CheckOut.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    sourcesList = FleetItinerary.LoadSourceId();
                    selectedSources = sourcesList;
                    InitializeControls(); 
                    Array Sources = Enum.GetValues(typeof(CarBookingSource));
                    foreach (CarBookingSource source in Sources)
                    {
                        if (source == CarBookingSource.Sayara)
                        {
                            ListItem item = new ListItem(Enum.GetName(typeof(CarBookingSource), source), ((int)source).ToString());
                            ddlSource.Items.Add(item);
                        }
                    }
                    Array Statuses = Enum.GetValues(typeof(CarBookingStatus));
                    foreach (CarBookingStatus status in Statuses)
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(CarBookingStatus), status), ((int)status).ToString());
                        if (item.Text == "Confirmed")
                        {
                            ddlBookingStatus.Items.Insert(2, new ListItem("Vouchered", "1"));
                        }
                        else
                        {
                            ddlBookingStatus.Items.Add(item);
                        }
                    }
                    ddlLocations.SelectedIndex = -1;
                    ddlLocations.Items.FindByText(Settings.LoginInfo.LocationName).Selected = true;
                    pageNo = 1;
                    LoadBookings(); //Loading all bookings
                }
                if (Settings.LoginInfo.AgentId != 1)
                {
                    ddlTransType.Enabled = false;
                }
                else
                {
                    ddlTransType.Enabled = true;
                }
                if (ddlTransType.SelectedItem.Value == "B2B") //B2B paging
                {
                    loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
                    if (Request["PageNoString"] != null)
                    {
                        pageNo = Convert.ToInt16(Request["PageNoString"]);
                    }
                    else
                    {
                        pageNo = 1;
                    }
                }
                else   //B2C paging
                {
                    if (PageB2CNoString.Value.Length > 0 && Session["bookingQueueRes"] != null && hdnTransTypeSelected.Value=="0")
                    {
                        currentPageNo = Convert.ToInt16(PageB2CNoString.Value);
                        List<fleetAgentBookingBO> bookingQueueRes = Session["bookingQueueRes"] as List<fleetAgentBookingBO>;
                        filteredResults = DoPaging(bookingQueueRes);
                        if (filteredResults != null && filteredResults.Count > 0)
                        {
                            dlSCRBookingQueue.DataSource = filteredResults;
                            dlSCRBookingQueue.DataBind();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetBookingQueue, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "0");
        }
    }
    private void InitializeControls()
    {
        BindAgent();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            ddlLocations.Enabled = true;
        }
        else
        {
            ddlSource.Visible = false;
            lblSource.Visible = false;
        }
        ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();
        int b2bAgentId;
        int b2b2bAgentId;
        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
        {
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        }
        else if (Settings.LoginInfo.AgentType == AgentType.Agent)
        {
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlAgents.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B)
        {
            ddlAgents.Enabled = false;
            b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2BAgent.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
        {
            ddlAgents.Enabled = false;
            ddlB2BAgent.Enabled = false;
            b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
            ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
            ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2B2BAgent.Enabled = false;
        }
        BindB2BAgent(Convert.ToInt32(ddlAgents.SelectedItem.Value));
        BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "Agent_Name";
            ddlAgents.DataValueField = "agent_id";
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }

            BindLocation(agentId, type);
        }
        catch (Exception ex)
        { }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindLocation(Utility.ToInteger(ddlAgents.SelectedItem.Value), type);
    }
    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
        hdfParam.Value = "0";
    }
    /// <summary>
    /// Loads all the bookings
    /// </summary>
    /// 

    private void LoadBookings()
    {
        try
        {
            FleetItinerary[] tempItinerary = new FleetItinerary[0];
            string orderByString = string.Empty;
            string whereString = string.Empty;
            whereString = GetWhereString();
            orderByString = GetOrderbyString();

            int queueCount = 0;
            ArrayList listOfItemId = new ArrayList();
            queueCount = CT.Core.Queue.GetTotalFilteredRecordsCount(whereString, orderByString, "Car");
            string url = "FleetBookingQueue.aspx";
            if ((queueCount % recordsPerPage) > 0)
            {
                noOfPages = (queueCount / recordsPerPage) + 1;
            }
            else
            {
                noOfPages = (queueCount / recordsPerPage);
            }


            if (noOfPages > 1)
            {
                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                }
            }
            else
            {
                pageNo = 1;
            }
            if (queueCount > 0)
            {
                show = MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
            }

            DataTable dtQueue = CT.Core.Queue.GetTotalFilteredRecords(pageNo, recordsPerPage, queueCount, whereString, orderByString, "Car");


            dlBookings.DataSource = dtQueue;
            dlBookings.DataBind();
        }
        catch (Exception exp)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.Normal, 0, "Exception from Booking Queue:" + exp.ToString(), "");
        }
    }
    /// <summary>
    /// Method Gets Where String from the filters(Html Controls)
    /// </summary>
    /// <returns></returns>
    private string GetWhereString()
    {
        bool isOrderByLCD = false;
        string source = string.Empty;
        string voucherd = string.Empty;
        shownStatuses.Clear();
        if (Request["filter"] != null && Request["filter"] == "true")
        {
            filtered = true;
        }
        if (filtered == false)
        {
            //voucherStatus = "Both";
            voucherd = "";
            source = "";
            reqType = "both";
            Cancelled = "True";
            Confirmed = "True";
            shownStatuses.Clear();
            shownStatuses.Add((int)CarBookingStatus.Cancelled);
            shownStatuses.Add((int)CarBookingStatus.Confirmed);
            shownStatuses.Add((int)CarBookingStatus.Pending);

        }
        if (ddlBookingStatus.SelectedValue == "-1")
        {
            shownStatuses.Add((int)CarBookingStatus.Cancelled);
            shownStatuses.Add((int)CarBookingStatus.Confirmed);
            shownStatuses.Add((int)CarBookingStatus.Error);
            shownStatuses.Add((int)CarBookingStatus.Failed);
            shownStatuses.Add((int)CarBookingStatus.Pending);
            shownStatuses.Add(17); //17 means Modified
        }
        else
        {
            shownStatuses.Add(Convert.ToInt32(ddlBookingStatus.SelectedValue));
        }

        if (ddlSource.SelectedItem.Text == "All")
        {
            foreach (ListItem item in ddlSource.Items)
            {
                if (item.Value != "-1")
                {
                    selectedSources.Add(Convert.ToInt32(item.Value));
                }
            }
        }
        else
        {
            selectedSources.Add(Convert.ToInt32(ddlSource.SelectedValue));
        }



        isOrderByLCD = true;
        if (txtFleetName.Text != string.Empty)
        {
            FleetFilter = txtFleetName.Text.Trim();
        }
        if (txtPaxName.Text != string.Empty)
        {
            paxFilter = txtPaxName.Text.Trim();
        }
        if (txtConfirmNo.Text != string.Empty)
        {
            pnrFilter = txtConfirmNo.Text;
        }
        string agentType = string.Empty;
        if (!IsPostBack)
        {
            agentFilter = Utility.ToInteger(Settings.LoginInfo.AgentId);
        }
        else
        {
            agentFilter = Utility.ToInteger(ddlAgents.SelectedItem.Value);
            if (agentFilter == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            //if (ddlAgents.SelectedIndex > 0)
            //{


            if (agentFilter > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agentFilter > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agentFilter = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agentFilter = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            //}
        }

        DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
        if (CheckIn.Text != string.Empty)
        {
            try
            {
                startDate = Convert.ToDateTime(CheckIn.Text, provider);
            }
            catch { }
        }
        else
        {
            startDate = DateTime.Now;
        }

        if (CheckOut.Text != string.Empty)
        {
            try
            {
                endDate = Convert.ToDateTime(Convert.ToDateTime(CheckOut.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
            }
            catch { }
        }
        else
        {
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }

        if (!IsPostBack)
        {
            startDate = Convert.ToDateTime(CheckIn.Text, provider);
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }
        if (ddlLocations.SelectedIndex > 0)
        {
            locationId = Convert.ToInt32(ddlLocations.SelectedValue);
        }
        #region B2C purpose
        string transType = string.Empty;
        if (Settings.LoginInfo.TransType == "B2B")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2B";
        }
        else if (Settings.LoginInfo.TransType == "B2C")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2C";
        }
        else
        {
            ddlTransType.Visible = true;
            lblTransType.Visible = true;
            if (ddlTransType.SelectedItem.Value == "-1")
            {
                transType = null;
            }
            else
            {
                transType = ddlTransType.SelectedItem.Value;
            }
        }
        #endregion

        //string whereString = string.Empty;
        string whereString = CT.Core.Queue.GetWhereStringForFleetBookingQueue(shownStatuses, FleetFilter, paxFilter, pnrFilter, agentFilter, bookingModes, source, string.Empty, isOrderByLCD, selectedSources, locationId, startDate, endDate, agentType, transType);

        return whereString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string GetOrderbyString()
    {
        string orderString = string.Empty;
        if (Request["lcd"] != null && Request["lcd"] != string.Empty)
        {
            lastCanDate = Request["lcd"];
            orderString = " order by lastCancellationDate asc ";
        }
        return orderString;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlTransType.SelectedItem.Value == "B2B") 
        {
            tblB2B.Visible = true;
            tblB2C.Visible = false;
            LoadBookings();
        }
        else if(ddlTransType.SelectedItem.Value=="B2C") //Here B2C means Sayara Bookings
        {
            tblB2B.Visible = false;
            tblB2C.Visible = true;
            LoadQueue();
        }
    }
    protected void dlBookings_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            //Check if the bound item is DataItem only
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Get the bound item
                DataRowView fleetItinerary = e.Item.DataItem as DataRowView;
                Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                Label lblGuest = e.Item.FindControl("lblGuests") as Label;
                Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                Label lblSupplier = e.Item.FindControl("lblSupplier") as Label;
                //Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                Label lblBooking = e.Item.FindControl("lblBooking") as Label;
                Button btnBooking = e.Item.FindControl("btnBooking") as Button;
                HtmlTable tblBooking = e.Item.FindControl("tblBooking") as HtmlTable;
                Label lblAgentName = e.Item.FindControl("lblAgentName") as Label;
                Label lblBookedBy = e.Item.FindControl("lblBookedBy") as Label;
                Label lblLocation = e.Item.FindControl("lblLocation") as Label;


                Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;

                Button btnInvoice = e.Item.FindControl("btnInvoice") as Button;
                Button btnOpen = e.Item.FindControl("btnOpen") as Button;
                LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;
                HtmlGenericControl div = e.Item.FindControl("divLicense") as HtmlGenericControl;
                LinkButton lnkDocuments = e.Item.FindControl("lnkDocuments") as LinkButton;

                if (fleetItinerary != null)
                {
                    int bkgId = 0;
                    btnBooking.OnClientClick = "return CancelAmendBooking('" + e.Item.ItemIndex + "');";
                    FleetItinerary itinerary = new FleetItinerary();
                    FleetPassenger fPax = new FleetPassenger();
                    BookingDetail bkgDetail = null;
                    AgentMaster agent = null;
                    try
                    {
                        //---------------------------Retrieve Agent-------------------------------------------
                        bkgId = BookingDetail.GetBookingIdByProductId(Convert.ToInt32(fleetItinerary["fleetId"]), ProductType.Car);
                        bkgDetail = new BookingDetail(bkgId);
                        Product[] products = BookingDetail.GetProductsLine(bkgId);

                        itinerary.Load(products[0].ProductId);
                        itinerary.PassengerInfo = fPax.Load(products[0].ProductId);
                        btnInvoice.OnClientClick = "return ViewInvoice('" + itinerary.FleetId + "','" + bkgDetail.AgencyId + "','" + itinerary.BookingRefNo + "');";
                        btnOpen.OnClientClick = "return ViewVoucher('" + itinerary.BookingRefNo + "');";


                        agent = new AgentMaster(bkgDetail.AgencyId);
                        lblAgentName.Text = agent.Name;
                        lblAgencyBalance.Text = agent.CurrentBalance.ToString("0.000");
                        lblAgencyEmail.Text = agent.Email1;
                        lblAgencyName.Text = agent.Name;
                        lblAgencyPhone1.Text = agent.Phone1;
                        lblAgencyPhone2.Text = agent.Phone2;
                        LocationMaster location = new LocationMaster(itinerary.LocationId);
                        lblLocation.Text = location.Name;


                        //------------------------------------------------------------------------------------
                    }
                    catch { }
                    try
                    {
                        UserMaster bookedBy = new UserMaster(Convert.ToInt64(fleetItinerary["CreatedBy"]));
                        lblBookedBy.Text = bookedBy.FirstName + " " + bookedBy.LastName;
                    }
                    catch { }
                    // int adults = 0, childs = 0;
                    decimal totalPrice = 0;

                    totalPrice = Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.Markup - itinerary.Price.Discount + itinerary.Price.Tax); //Added B2c Markup only by chandann on 13062016
                    //Bind the Fleet Booking price
                    lblPrice.Text = (itinerary.Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Price.Currency) : "AED ") + " " + Math.Ceiling(totalPrice).ToString("N" + agent.DecimalValue); //Use ceiling instead of Round, Changed on 05082016
                    lblPaxName.Text = itinerary.PassengerInfo[0].FirstName == null ? " " : itinerary.PassengerInfo[0].FirstName + " " + itinerary.PassengerInfo[0].LastName;
                    lblGuest.Text = "1";
                    //download documents links
                    try
                    {
                        if (!string.IsNullOrEmpty(itinerary.PassengerInfo[0].FileNames))
                        {
                            lnkDocuments.Visible = true;
                            string html = string.Empty;
                            string[] files = itinerary.PassengerInfo[0].FileNames.Split('|');
                            //Binding all uploading documets
                            for (int q = 0; q < files.Length; q++)
                            {
                                html += "<div id ='divFiles" + files[q] + "'>";
                                html += "<table>";
                                html += "<tr>";
                                html += "<td>";
                                html += "<a href='#' onclick=\"javascript:Download('" + (itinerary.PassengerInfo[0].DocumentsPath + @"\" + files[q]).Replace("\\", "//") + "','" + files[q] + "')\">" + files[q] + "</a>";
                                html += "</td>";
                                html += "</tr>";
                                html += "</table>";
                                html += "</div>";
                            }
                            div.InnerHtml = html;
                        }
                    }
                    catch { }

                    
                    lblSupplier.Text = (itinerary.Source.ToString());
                    DateTime checkInDate = itinerary.FromDate;

                    if (Settings.LoginInfo.MemberType == MemberType.SUPER || Convert.ToInt32(Math.Ceiling(checkInDate.Subtract(DateTime.Now).TotalDays)) > 1)
                    {

                        tblBooking.Visible = true;
                        btnBooking.Visible = true;
                    }

                    if (itinerary.VoucherStatus && itinerary.BookingStatus == CarBookingStatus.Confirmed)
                    {
                        lblStatus.Text = "Vouchered";

                    }
                    else
                    {
                        lblStatus.Text = itinerary.BookingStatus.ToString();

                        lblStatus.Style["color"] = "Red";

                        btnInvoice.Visible = false;
                    }

                    if (bkgDetail != null && bkgDetail.Status == BookingStatus.InProgress)
                    {
                        lblBooking.Text = "Request For Cancellation";
                        btnBooking.Visible = false;
                        tblBooking.Visible = false;
                    }
                    else if (bkgDetail.Status == BookingStatus.ModificationInProgress)
                    {
                        lblBooking.Text = "Request For Modification";
                        btnBooking.Visible = false;
                        tblBooking.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetBookingQueue, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), "0");
        }
    }

    
    protected void dlBookings_ItemCommand(object sender, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Booking")
        {
            if (e.CommandArgument.ToString().Length > 0)
            {
                DropDownList cmbBooking = e.Item.FindControl("ddlBooking") as DropDownList;
                TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;

                string bkg = cmbBooking.SelectedValue;

                string confirmationNo = e.CommandArgument.ToString();


                if (bkg.ToLower() == "cancel booking" || bkg.ToLower() == "modified booking")
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(FleetItinerary.GetFleetId(confirmationNo), ProductType.Car));
                    int bookingId = bookingDetail.BookingId;
                    string data = txtRemarks.Text;
                    int loggedMemberId = Convert.ToInt32(Settings.LoginInfo.UserID);
                    int requestTypeId;
                    if (bkg.ToLower() == "cancel booking")
                    {
                        requestTypeId = 8;
                    }
                    else
                    {
                        requestTypeId = 9;
                    }
                    AgentMaster agency = null;
                    try
                    {

                        ServiceRequest serviceRequest = new ServiceRequest();
                        BookingDetail booking = new BookingDetail();
                        booking = new BookingDetail(bookingId);
                        FleetItinerary itineary = new FleetItinerary();
                        FleetPassenger pax = new FleetPassenger();
                        Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                        for (int i = 0; i < products.Length; i++)
                        {
                            if (products[i].ProductTypeId == (int)ProductType.Car)
                            {
                                itineary.Load(products[i].ProductId);
                                itineary.PassengerInfo = pax.Load(products[i].ProductId);
                                break;
                            }
                        }
                        agency = new AgentMaster(booking.AgencyId);
                        serviceRequest.BookingId = bookingId;
                        serviceRequest.ReferenceId = itineary.FleetId;
                        serviceRequest.ProductType = ProductType.Car;

                        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                        serviceRequest.Data = data;
                        //serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

                        serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        serviceRequest.IsDomestic = true;

                        serviceRequest.PaxName = itineary.PassengerInfo[0].FirstName + " " + itineary.PassengerInfo[0].LastName;
                        serviceRequest.Pnr = itineary.BookingRefNo;
                        serviceRequest.Source = (int)itineary.Source;
                        serviceRequest.StartDate = itineary.FromDate;
                        serviceRequest.SupplierName = itineary.FleetName;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                        serviceRequest.ReferenceNumber = itineary.BookingRefNo;
                        serviceRequest.PriceId = itineary.Price.PriceId;
                        serviceRequest.AgencyId = Settings.LoginInfo.AgentId; //Every request must fall in Admin Queue, so assign Admin ID
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.CarBooking;
                        serviceRequest.DocName = "";
                        serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                        serviceRequest.LastModifiedOn = DateTime.Now;
                        serviceRequest.CreatedOn = DateTime.Now;
                        serviceRequest.IsDomestic = true;
                        serviceRequest.AgencyTypeId = Agencytype.Cash;
                        serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                        serviceRequest.Save();
                        // Setting booking details status 

                        if (requestTypeId == 8)
                        {
                            booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
                        }
                        else
                        {
                            booking.SetBookingStatus(BookingStatus.ModificationInProgress, loggedMemberId);
                        }


                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the Fleet booking.";


                        bh.EventCategory = EventCategory.CarCancel;

                        bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                        bh.Save();
                        //Sending email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agency.Name);
                        table.Add("itemName", itineary.FleetName);
                        table.Add("confirmationNo", itineary.BookingRefNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itineary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["FLEET_CANCEL_MAIL"]).Split(';');
                        foreach (string cnMail in cancelMails)
                        {
                            toArray.Add(cnMail);
                        }

                        string message = ConfigurationManager.AppSettings["FLEET_CANCEL_REQUEST"];
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Car)Request for cancellation. Confirmation No:(" + itineary.BookingRefNo + ")", message, table);

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.ChangeRequest, Severity.High, 1, "Exception in the Fleet Change Request. Message:" + ex.Message, "");

                    }
                }

            }
            Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
            Label lblBooking = e.Item.FindControl("lblBooking") as Label;

            BookingDetail bkgDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(FleetItinerary.GetFleetId(e.CommandArgument.ToString()), ProductType.Car));
            if (bkgDetail.Status == BookingStatus.InProgress)
            {
                lblBooking.Text = "Request For Cancellation";
            }

            LoadBookings();
        }
    }
    protected void ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
    {
        tblB2B.Visible = false;
        tblB2C.Visible = false;
        if (ddlTransType.SelectedItem.Value == "B2B")
        {
            VisableControls(true);
            lblPaymentStatus.Visible = false;
            ddlPaymentStatus.Visible = false;
            lblB2CBookingStatus.Visible = false;
            ddlB2CBookingStatus.Visible = false;
            lblB2CConfirmNo.Visible = false;
            txtB2CConfirmNo.Visible = false;
        }
        else if (ddlTransType.SelectedItem.Value == "B2C")
        {
            VisableControls(false);
            lblPaymentStatus.Visible = true;
            ddlPaymentStatus.Visible = true;
            lblB2CBookingStatus.Visible = true;
            ddlB2CBookingStatus.Visible = true;
            lblB2CConfirmNo.Visible = true;
            txtB2CConfirmNo.Visible = true;
        }
        hdfParam.Value = "0";
    }
    private void VisableControls(bool value)
    {
        lblSource.Visible = value;
        ddlSource.Visible = value;
        ddlAgents.Visible = value;
        lblB2BAgent.Visible = value;
        lblB2B2BAgent.Visible = value;
        ddlB2B2BAgent.Visible = value;
        ddlB2BAgent.Visible = value;
        lblAgent.Visible = value;
        lblLocation.Visible = value;
        ddlLocations.Visible = value;
        lblFleetName.Visible = value;
        txtFleetName.Visible = value;
        lblPaxName.Visible = value;
        txtPaxName.Visible = value;
        lblConfirNo.Visible = value;
        txtConfirmNo.Visible = value;
        lblBookingStatus.Visible = value;
        ddlBookingStatus.Visible = value;
    }


    private void LoadQueue()
    {
        try
        {

            DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
            string bkRefNum = string.Empty;
            string bkStatus = string.Empty;
            string paymentStatus = string.Empty;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            if (CheckIn.Text != string.Empty)
            {
                try
                {
                    startDate = Convert.ToDateTime(CheckIn.Text, provider);
                }
                catch { }
            }
            else
            {
                startDate = DateTime.Now;
            }

            if (CheckOut.Text != string.Empty)
            {
                try
                {
                    endDate = Convert.ToDateTime(Convert.ToDateTime(CheckOut.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                catch { }
            }
            else
            {
                endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
            }
            if (txtConfirmNo.Text != string.Empty)
            {
                bkRefNum = txtConfirmNo.Text;
            }
            if (ddlBookingStatus.SelectedIndex > 0)
            {
                bkStatus = ddlBookingStatus.SelectedItem.Value;
            }
            if (ddlPaymentStatus.SelectedIndex > 0)
            {
                paymentStatus = ddlPaymentStatus.SelectedItem.Value;
            }
            Sayara.SayaraApi sayaraApi = new Sayara.SayaraApi();
            List<fleetAgentBookingBO>  bookingQueueRes = sayaraApi.GetSayaraBookingDetails(startDate, endDate, bkRefNum, bkStatus, paymentStatus);
            Session["bookingQueueRes"] = bookingQueueRes;
            filteredResults = DoPaging(bookingQueueRes);
            if (filteredResults != null && filteredResults.Count > 0)
            {
                dlSCRBookingQueue.DataSource = filteredResults;
                dlSCRBookingQueue.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void dlSCRBookingQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                fleetAgentBookingBO queeuDetail = e.Item.DataItem as fleetAgentBookingBO;

                if (queeuDetail != null)
                {

                    Label lblBookingStatus = e.Item.FindControl("lblBookingStatus") as Label;

                    Label lblBookingDate = e.Item.FindControl("lblBookingDate") as Label;
                    Label lblFromLoc = e.Item.FindControl("lblFromLoc") as Label;
                    Label lblToLoc = e.Item.FindControl("lblToLoc") as Label;
                    Label lblToLocation = e.Item.FindControl("lblToLocation") as Label;
                    Label lblBookingReference = e.Item.FindControl("lblBookingReference") as Label;
                    Label lblBookingNetValue = e.Item.FindControl("lblBookingNetValue") as Label;
                    Label lblPaymentStatus = e.Item.FindControl("lblPaymentStatus") as Label;


                    Label lblBookedBy = e.Item.FindControl("lblBookedBy") as Label;
                    Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                    LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;

                    Label lblPayBookingID = e.Item.FindControl("lblPayBookingID") as Label;
                    Label lblPaymentID = e.Item.FindControl("lblPaymentID") as Label;
                    Label lblPayAmount = e.Item.FindControl("lblPayAmount") as Label;
                    Label lblPaySource = e.Item.FindControl("lblPaySource") as Label;

                    lblBookingStatus.Text = queeuDetail.BookingStatus;
                    lblBookingDate.Text = queeuDetail.BookingDate.ToString();
                    lblFromLoc.Text = queeuDetail.FromLocation;
                    if (!string.IsNullOrEmpty(queeuDetail.ToLocation))
                    {
                        lblToLoc.Text = queeuDetail.ToLocation;
                        lblToLoc.Visible = true;
                        lblToLocation.Visible = true;
                    }
                    else
                    {
                        lblToLoc.Visible = false;
                        lblToLocation.Visible = false;
                    }
                    lblBookingReference.Text = queeuDetail.BookingRef;
                    lblBookingNetValue.Text = Convert.ToString(queeuDetail.BookingNetAmount);
                    lblPaymentStatus.Text = queeuDetail.PayStatus;
                    lblBookedBy.Text = queeuDetail.FirstName + " " + queeuDetail.LastName;
                    lblPaxName.Text = queeuDetail.FirstName + " " + queeuDetail.LastName;

                    if (queeuDetail.PayStatus == "Confirmed" && !string.IsNullOrEmpty(queeuDetail.PayRefId) && !string.IsNullOrEmpty(queeuDetail.Source))
                    {
                        lnkPayment.Visible = true;
                        lnkPayment.OnClientClick = "return ViewPaymentInfo('" + e.Item.ItemIndex + "');";
                        lblPayBookingID.Text = Convert.ToString(queeuDetail.BookingId);
                        lblPaymentID.Text = Convert.ToString(queeuDetail.PayRefId);
                        lblPayAmount.Text = Convert.ToString(queeuDetail.Amount);
                        lblPaySource.Text = Convert.ToString(queeuDetail.Source);

                    }
                }

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="resultDisplay"></param>
    /// <returns></returns>
    private List<fleetAgentBookingBO> DoPaging(List<fleetAgentBookingBO> resultDisplay)
    {
        List<fleetAgentBookingBO> results = new List<fleetAgentBookingBO>();
        try
        {
            if (resultDisplay != null && resultDisplay.Count > 0)
            {

                int numberOfPages = 0;
                if (resultDisplay.Count % recordsPerPage > 0)
                {
                    numberOfPages = (resultDisplay.Count / recordsPerPage) + 1;
                }
                else
                {
                    numberOfPages = resultDisplay.Count / recordsPerPage;
                }

                string urlForPaging = string.Empty;
                pagingScript = PagingJavascript(numberOfPages, urlForPaging, currentPageNo);

                if (resultDisplay.Count > 0)
                {
                    if ((currentPageNo * recordsPerPage) <= resultDisplay.Count)
                    {
                        for (int i = (currentPageNo * recordsPerPage) - recordsPerPage; i < (currentPageNo * recordsPerPage); i++)
                        {
                            if (i < resultDisplay.Count)
                            {
                                results.Add(resultDisplay[i]);
                            }
                        }
                    }
                    else
                    {
                        for (int i = (currentPageNo * recordsPerPage) - recordsPerPage; i < (currentPageNo * recordsPerPage); i++)
                        {
                            if (i < resultDisplay.Count)
                            {
                                results.Add(resultDisplay[i]);
                            }
                        }
                    }
                }

                maxResult = results.Count * currentPageNo;
                if (currentPageNo * recordsPerPage < maxResult)
                {
                    maxResult = currentPageNo * recordsPerPage;//filteredResults.Length;
                }

                minResult = ((currentPageNo * recordsPerPage) - recordsPerPage);
            }
            return results;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    /// </summary>
    /// <param name="noOfPages"></param>
    /// <param name="url"></param>
    /// <param name="pageNo"></param>
    /// <returns></returns>
    public static string PagingJavascript(int noOfPages, string url, int pageNo)
    {
        string show = string.Empty;
        string previous = string.Empty;
        string first = string.Empty;
        string last = string.Empty;
        string next = string.Empty;
        int showperPage = 2;

        // records per page from configuration       
        showperPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["showPerPage"]);

        showperPage = showperPage - 1;
        int showPageNos;
        if ((pageNo + showperPage) <= noOfPages)
        {
            showPageNos = pageNo + showperPage;
        }
        else
        {
            showPageNos = noOfPages;
        }

        if (pageNo != 1)
        {
            previous = "<a href=\"" + "javascript:ShowB2CPage(" + (pageNo - 1) + ")\"> Previous </a>| ";
            first = "<a href=\"" + "javascript:ShowB2CPage(1)\"> First </a>| ";
        }

        if (pageNo != noOfPages)
        {
            next = "<a href=\"" + "javascript:ShowB2CPage(" + (pageNo + 1) + ")\"> Next </a>";
            last = "| <a href=\"" + "javascript:ShowB2CPage(" + noOfPages + ")\"> Last </a>";
        }
        show = first + previous;
        for (int k = pageNo; k <= showPageNos; k++)
        {
            if (k == pageNo)
            {
                show += "<b><a href=\"" + "javascript:ShowB2CPage(" + k + ")\">" + k + "</a></b> | ";
            }
            else
            {
                show += "<a href=\"" + "javascript:ShowB2CPage(" + k + ")\">" + k + "</a> | ";
            }
        }
        show = show + next + last;

        return show;
    }
}
