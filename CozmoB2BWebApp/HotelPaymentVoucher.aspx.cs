﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

public partial class HotelPaymentVoucherUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected BookingResponse bookingResponse;
    protected HotelItinerary itinerary;
    protected PriceType priceType;
    protected decimal rateOfExchange = 1;
    HotelPassenger hPax = null;
    HotelRoom hRoom;
    PriceAccounts priceInfo = new PriceAccounts();
    bool isMultiRoom;
    protected HotelDetails hotelDetails = new HotelDetails();
    protected int nights;
    protected AgentMaster agent;
    protected BookingDetail booking;
    protected LocationMaster location = new LocationMaster();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            if (Request.QueryString["ConfNo"] != null)
            {

                int agentId = 0;

                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agentId = Settings.LoginInfo.OnBehalfAgentID;
                    agent = new AgentMaster(agentId);
                    location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                }
                else
                {
                    agentId = Settings.LoginInfo.AgentId;
                    agent = new AgentMaster(agentId);
                }
                
                
                string logoPath = string.Empty;
                if (agentId > 1)
                {
                    logoPath = ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                    imgHeaderLogo.ImageUrl = logoPath;
                    //logoPath = Server.MapPath(new AgentMaster(agentId).ImgFileName);

                    //logoPath = new AgentMaster(agentId).ImgFileName;
                    //imgHeaderLogo.ImageUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + ConfigurationManager.AppSettings["AgentImage"] + logoPath).AbsoluteUri;
                }
                else
                {
                    imgHeaderLogo.ImageUrl = "images/logocozmo.jpg";
                } 
                itinerary = new HotelItinerary();
                hPax = new HotelPassenger();
                hRoom = new HotelRoom();
                string confirmationNo = Request.QueryString["ConfNo"].ToString();

                if (Session["hItinerary"] != null)
                {
                    itinerary = Session["hItinerary"] as HotelItinerary;
                }
                if (Session["BookingResponse"] != null)
                {
                    bookingResponse = (BookingResponse)Session["BookingResponse"];
                }
                else if (Request.QueryString["ConfNo"] == null)
                {
                    Response.Redirect("AbandonSession.aspx");
                }

                //if (itinerary.Source != HotelBookingSource.HotelConnect)
                {
                    booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(confirmationNo), ProductType.Hotel));
                    try
                    {
                        agent = new AgentMaster(booking.AgencyId);
                        Product[] products = BookingDetail.GetProductsLine(booking.BookingId);

                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Hotel)
                        {
                            itinerary.Load(products[i].ProductId);

                            // Getting Passenger details
                            hPax.Load(products[i].ProductId);
                            itinerary.HotelPassenger = hPax;

                            //Getting HotelRoom Details                

                            itinerary.Roomtype = hRoom.Load(products[i].ProductId);
                            hRoom = itinerary.Roomtype[0];
                            priceInfo.Load(hRoom.PriceId);
                            hRoom.Price = priceInfo;
                            Session["hItinerary"] = itinerary;

                                if (itinerary.Source == HotelBookingSource.DOTW)
                                {
                                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                                    //hotelDetails = mse.GetHotelDetails(itinerary.CityCode, "Hotel", itinerary.HotelCode, itinerary.Source);
                                }
                                else if(itinerary.Source == HotelBookingSource.RezLive)
                                {
                                    RezLive.XmlHub rezAPI = new RezLive.XmlHub(Session["cSessionId"].ToString());
                                    //hotelDetails = rezAPI.GetHotelDetails(itinerary.HotelCode);
                                }
                                else if (itinerary.Source == HotelBookingSource.LOH)
                                {
                                    try
                                    {
                                        LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(Session["cSessionId"].ToString());
                                        //hotelDetails = jxe.GetHotelDetails(itinerary.HotelCode);
                                        hotelDetails = jxe.GetHotelDetails(itinerary.HotelCode,itinerary.CityCode);
                                    }
                                    catch { }
                                }
                                else if (itinerary.Source == HotelBookingSource.HotelBeds)   //Added by brahmam 26.09.2014
                                {
                                    CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                                    hotelDetails = hBeds.GetItemInformation(itinerary.CityCode, itinerary.HotelName, itinerary.HotelCode);
                                }
                                else if (itinerary.Source == HotelBookingSource.GTA)
                                {
                                    CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                                    hotelDetails = gtaApi.GetItemInformation(itinerary.CityCode, itinerary.HotelName, itinerary.HotelCode);
                                }
                                #region GRN
                                else if (itinerary.Source == HotelBookingSource.GRN)
                                {
                                    CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                                    hotelDetails = grnApi.GetHotelDetailsInformation(itinerary.CityCode, itinerary.HotelName, itinerary.HotelCode);
                                }
                                #endregion

                                if (itinerary.Source != HotelBookingSource.Desiya && itinerary.Source != HotelBookingSource.IAN && itinerary.NoOfRooms > 1)
                                {
                                    isMultiRoom = true;
                                }
                                break;
                            }
                        }
                        
                        System.TimeSpan DiffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
                        nights = DiffResult.Days;
                        
                      //  SendHotelVoucherMail();
                        //if (itinerary.HotelName == null)
                        //{
                        //    //errorMessage = "Invalid Booking ID";
                        //}
                        //else if (!((int)Session["agencyId"] == 0 || booking.AgencyId == Settings.LoginInfo.AgentId || Settings.LoginInfo.UserID == 1))
                        //{
                        //    //errorMessage = "Invalid Booking ID";
                        //}
                        Session["hCode"] = null;
                        Session["rCode"] = null;
                        Session["rpCode"] = null;
                        //Session["req"] = null;
                        Session["Markup"] = null;
                        Session["city"] = null;
                        Session["locations"] = null;
                        Session["hFilterCriteria"] = null;
                        Session["isMultiRoom"] = null;
                        Session["RoomAvailability"] = null;
                        Session["SearchResults"] = null;
                        Session["cSessionId"] = null;
                        Session["Result"] = null;
                        Session[itinerary.HotelCode] = null;
                        Session["hItinerary"] = null;
                    }
                    catch (Exception exp)
                    {
                        //errorMessage = "Invalid Booking";
                        Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Exception Loading itinerary. Message: " + exp.Message, Request["REMOTE_ADDR"]);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get Voucher. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    //protected void SendHotelVoucherMail()
    //{
    //    try
    //    {
    //        if (itinerary.Roomtype.Length > 0 && itinerary.Roomtype[0].PassenegerInfo.Count > 0)
    //        {
    //            string logoPath = "";
    //            int agentId = 0;

    //            if (Session["BookingAgencyID"] != null)
    //            {
    //                agentId = Convert.ToInt32(Session["BookingAgencyID"]);
    //            }
    //            else
    //            {
    //                agentId = Settings.LoginInfo.AgentId;
    //            }

    //            string serverPath = "";

    //            if (Request.Url.Port > 0)
    //            {
    //                serverPath = Request.Url.Scheme+"://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
    //            }
    //            else
    //            {
    //                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
    //            }

    //            if (agentId> 1)
    //            {
    //                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
    //                imgLogo.ImageUrl =  logoPath;
    //            }
    //            else
    //            {
    //                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
    //            } 

    //            //if (agentId > 1)
    //            //{
    //            //    logoPath = Server.MapPath(new AgentMaster(agentId).ImgFileName);

    //            //    logoPath = new AgentMaster(agentId).ImgFileName;
    //            //    imgLogo.ImageUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + ConfigurationManager.AppSettings["AgentImage"] + logoPath).AbsoluteUri;
    //            //}
    //            //else
    //            //{
    //            //    imgLogo.ImageUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + "images/logocozmo.jpg").AbsoluteUri;
    //            //} 

    //            string myPageHTML = "";
    //            System.IO.StringWriter sw = new System.IO.StringWriter();
    //            HtmlTextWriter htw = new HtmlTextWriter(sw);
    //            PrintDiv.RenderControl(htw);
    //            myPageHTML = sw.ToString();
    //            myPageHTML = myPageHTML.Replace("\"", "/\"");

    //            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
    //            toArray.Add(itinerary.Roomtype[0].PassenegerInfo[0].Email);
    //            /*int agentId = 0;

    //            if (Settings.LoginInfo.IsOnBehalfOfAgent)
    //            {
    //                agentId = Settings.LoginInfo.OnBehalfAgentID;
    //            }
    //            else
    //            {
    //                agentId = Settings.LoginInfo.AgentId;
    //            }*/
                
    //            AgentMaster agency = new AgentMaster(agentId);
    //            string bccEmails = string.Empty;
    //            if (!string.IsNullOrEmpty(agency.Email1))
    //            {
    //                bccEmails = agency.Email1;
    //            }
    //            if (!string.IsNullOrEmpty(agency.Email2))
    //            {
    //                bccEmails = bccEmails + "," + agency.Email2;
    //            }

    //            //toArray.Add(agency.Email1);
    //           // if (ViewState["MailSent"] == null)
    //           if(Session["req"] != null)// TO sent mail only  on first time
    //            {
    //                ViewState["MailSent"] = true;
    //                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Reservation Confirmation - " + itinerary.ConfirmationNo , myPageHTML, new Hashtable(), bccEmails);
    //                Session["req"] = null;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Hotel Voucher Email: reason - "+ex.ToString(), Request["REMOTE_ADDR"]);
    //    }
    //}
}
