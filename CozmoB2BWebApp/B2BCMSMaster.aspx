﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="B2BCMSMaster.aspx.cs" Inherits="CozmoB2BWebApp.B2BCMSMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>            
            <script src="css/toastr.min.js"></script>
            <link href="css/toastr.css" rel="stylesheet" />
            <div class="container" style="margin-top: 20px;">
                <div class="row">
                    <asp:HiddenField ID="hdnCmsId" runat="server" />
                    <asp:HiddenField ID="hdnImageData" runat="server" />
                    <asp:HiddenField ID="hdnImageFileNames" runat="server" />
                    <div class="col-md-3">
                        <span>Select the files</span>
                        <div class="form-group">
                            <asp:Panel class="pnldrop" ID="pnlDragandDrop" runat="server" Style="display: block; width: 100%; height: 100px; background-color: White; border-color: blue">
                                <div id="dZUploadpir" class="dropzone" runat="server" style="width: 250px; height: 200px; overflow-y: auto" accept="image/*">
                                    <div class="dz-default dz-message">
                                        Upload or Drag Drop(Image formatted files only) here.
                                        File size must be lessthan 500KB. <%--and 160px X 42px.--%>
										and Dimensions 580 X 412.
                                    </div>
                                </div>
                            </asp:Panel>
                            <input type="text" id="dzBoardPassFiles" style="display: none" runat="server" />
                            <span id="dzBoardPassFileserror" style="color: red;"></span>
                           
                        </div>
                        
                    </div>
                    <div class="col-md-3">
                        <span>Select Agent </span>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlAgents" runat="server" CssClass="form-control">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorddlAgents" style="color: red" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <a id="aBank" data-toggle="modal" data-target="#myModal1" title="Click to Save/Edit Bank/support Details." style="color: #1c498a; cursor: pointer;"><b>Bank/Support Details</b>
                            <asp:LinkButton ID="btnbank" runat="server" Visible="false" data-toggle="modal" data-target="#myModal1" OnClientClick="return false;"> <b>Bank/Support Details</b></asp:LinkButton>
                            <div class="form-group">
                               
                        
                                <div id="divBankDetails" runat="server" style="height: 100px; overflow-x: auto; overflow-y: auto;">
                                </div>
                                <br />
                                <div id="divSupportDetails" runat="server" style="height: 100px; overflow-x: auto; overflow-y: auto;">
                                </div>
                                <span id="errorbank" style="color: red" />
                                <span id="errorsupplier" style="color: red" />
                            </div>
                    </div>
                    <div class="col-md-1" style="display: none">
                       
                        <asp:LinkButton ID="lbtnsupport" runat="server" Visible="false" data-toggle="modal" data-target="#myModal1" OnClientClick="return false;"> <b>Support Details</b></asp:LinkButton>
                        <a id="asupport" style="color: #1c498a; display: none"><b>support Details</b></a>
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                 <span id="errorfiles" style="color: red" />
            </div>
            <div class="container" style="margin-top: 100px;">
                <div class="row" style="float: right">
                    <asp:Button ID="btnClear" runat="server" class="btn btn-info btn-primary" Text="Clear"  OnClick="btnClear_Click"   />
                    &nbsp;&nbsp;
                    <button type="button" class="btn btn-info btn-primary" data-toggle="modal" data-target="#modalAgents" style="border-radius: 5px">Search</button>
                    &nbsp;&nbsp;
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" OnClientClick="javascript:return Save();" class="btn btn-info btn-primary" Style="border-radius: 5px" />
                </div>
                <asp:Label ID="lblmsg" runat="server" />
            </div>
            <div class="container">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="DLCMSMaster" runat="server" AllowPaging="True" Style="margin-top: 10px;" AutoGenerateColumns="false" class="table" PageSize="5" OnPageIndexChanging="DLCMSMaster_PageIndexChanging">
                            <AlternatingRowStyle BackColor="White" />                            
                            <HeaderStyle CssClass="ns-h3" Font-Bold="True" ForeColor="White" />   
                             <PagerStyle CssClass="pagination-ys" HorizontalAlign="Left" />
                             <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" /> 
                            <Columns>
                                <asp:TemplateField HeaderText="SL.No">
                                    <ItemTemplate>&nbsp;&nbsp;<%#Container.DataItemIndex+1 %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CMS ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCMS_ID" runat="server" Text='<%# Eval("CMS_ID")%>' />
                                       
                                        <asp:Label ID="lblSLIDER_ID" Visible="false" runat="server" Text='<%# Eval("SLIDER_ID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent Name" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblagent" runat="server" Text='<%# Eval("agent_name")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Uploaded Image">
                                    <ItemTemplate>
                                        <asp:Image ID="imgupload" src='<%# Eval("CMS_IMAGE_FILE_PATH")%>' runat="server" Width="200px" Height="100px" ImageAlign="Middle" />
                                        <asp:HiddenField ID="hdnFilepath" runat="server" Value='<%# Eval("CMS_IMAGE_FILE_PATH")%>' />
                                        <input id="isUploaded" type="hidden" runat="server" data-id="isUploaded" value="0" />
                                        </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlider_Order" CssClass="lblOrder" runat="server" Text='<%# Eval("Slider_Order")%>' />
                                        <asp:HiddenField ID="hdnSliderOrder" runat="server" Value='<%# Eval("Slider_Order")%>' />
                                        </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblfname" runat="server" Text='<%# Eval("CMS_IMAGE_LABEL")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnstatus" runat="server" Style="border-radius: 5px" CssClass='<%# ( Eval("SLIDER_STATUS").ToString() =="1") ? "btn btn-info btn-primary" :  "btn btn-danger" %>' OnClick="lbtnstatus_Click">
                            <%# ( Eval("SLIDER_STATUS").ToString() =="1") ? "Active" :  "In Active" %>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbtnDelete" runat="server" Style="border-radius: 5px" CssClass="btn btn-info btn-danger"  OnClick="lbtnDelete_Click" Text="Delete" OnClientClick="return deleteSlider();" >
                            
                                        </asp:LinkButton>
                                         <button class="btn btn-info editImage">Edit</button>
                                        <asp:Button runat="server" CssClass="btn btn-success btnNonVisibile updImg" Text='Update' ID="btnUpdateImage" OnClick="btnUpdateImage_Click"/>
                                        <asp:Label runat="server" ID="lblstatus" Text='<%# Eval("SLIDER_STATUS")%>' Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="modalAgents" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="color: white">
                            <h5 class="modal-title" id="H1">Find B2BCMS Master Details
                            <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                                <span aria-hidden="true" style="color: white;"><b>X</b></span>
                            </button>
                            </h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <span>Select Agent</span>
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlFindAgent" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                        </asp:DropDownList>
                                        <span id="errorddlFindAgents" style="color: red" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-info btn-primary" style="border-radius: 5px">Close</button>
                            <asp:Button ID="btnFind" runat="server" Text="Find" OnClientClick="javascript:return FindAgents();" CssClass="btn btn-info btn-primary" Style="border-radius: 5px" OnClick="btnFind_Click" />
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal1" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="color: white">
                            <asp:Label ID="lbltitle" Style="color: white; text-align: center; margin-top: 15px" runat="server" Text="Bank/Support Details"></asp:Label>
                            <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                        <div class="modal-body" id="modalBank">
                            <div class="row" style="margin-left:10px;font-weight:bold"><span>Bank Details</span>&nbsp;<asp:CheckBox ID="chkBank" runat="server" checked="true"  />
                           </div> 
                            <CKEditor:CKEditorControl ID="txtBankDesc" runat="server" FilebrowserImageUploadUrl="/TicketReceipt/Upload.ashx" FilebrowserUploadUrl="/TicketReceipt/Upload.ashx">
                            </CKEditor:CKEditorControl>
                        </div>
                        <div class="modal-body" id="modalSupport">
                           <div class="row" style="margin-left:10px;font-weight:bold"><span>Support Details</span> &nbsp;<asp:CheckBox ID="chkSupport" runat="server" checked="true"  /></div>
                            <CKEditor:CKEditorControl ID="txtSupportDesc" runat="server" FilebrowserImageUploadUrl="/TicketReceipt/Upload.ashx" FilebrowserUploadUrl="/TicketReceipt/Upload.ashx">
                            </CKEditor:CKEditorControl>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnclose" runat="server" data-dismiss="modal" CssClass="btn btn-info btn-primary" Text="Close" Style="border-radius: 5px" />
                            <asp:Button ID="btnSavechanges" runat="server" OnClientClick="javascript:return SetComments();" CssClass="btn btn-info btn-primary" Text="Set Details" Style="border-radius: 5px" data-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <script src="DropzoneJs_scripts/dropzone.js"></script> 

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
            var data = $('#ctl00_cphTransaction_hdnimg').html();
            $('#ctl00_cphTransaction_dZUploadpir').val(data);
        }

        var dropZonePIR = null;
        var dropZoneBP;
        var dropZonePIRObj;
        var dropZoneBoardPassObj;
        var pirFiles;
        var boardpassFiles;

        function InitDropZone() { 
            $(".closepnldocs").on('click', function () {
                $(".pnldocs").hide();
                $(".pnldrop").hide();
                if (document.getElementById('lnkRemove') != null) {
                    document.getElementById('lnkRemove').click();
                }
            }); 
            // Dropzone.autoDiscover = false;
            Dropzone.prototype.defaultOptions.acceptedFiles = "image/*";
            Dropzone.prototype.defaultOptions.maxFiles = "1"; 
			var filesize = 1024 * 500;
			var maxHeight = 412;
			var maxWidth = 580;
            Dropzone.maxFilesize = 5;
            // for PIR files Upload  
            var files = [];
            dropZonePIRObj = {
                url: "hn_B2BCMSMaster.ashx",
                maxFiles: 10,
                addRemoveLinks: true,
				init: function () {
					var msg = '';
                    var drop = this;
                    var duplicate = 'N';					
                    this.on("addedfile", function (file) {
                        if (this.files.length) {
                            var _i, _len;
                            //Reoving the File when exceed the File Limit.
                            if (file.size > filesize) {
                                this.removeFile(file);
                                if (!files.includes(file.name))
                                    files.push(file.name);
                                var msg = '';
                                for (var i = 0; i < files.length; i++)
                                    msg += files[i] + (i != files.length - 1 ? ',' : '');
                                $('#errorfiles').html("<b>" + msg + "</b> Failed to Upload the files exceeded file Limits.");
                            }
                            //Removing the duplicate file having same size ,name and modification date.
                            for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                            {
                                if (this.files[_i].name === file.name && this.files[_i].size === file.size) { // && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                                    this.removeFile(file);
                                    //duplicate = 'Y';
                                }
                             }

                             var fr;
							fr = new FileReader;
							fr.onload = function() {
							var img;
                                img = new Image;
                                
                                img.onload = function () {
                                         msg = file.name;
                                        if (img.height > maxHeight || img.width > maxWidth) {                                                                                   
                                            drop.removeFile(file);
                                            //img.removeFile;
                                            $('#errorfiles').html("<b>" + msg + "</b> Failed to Upload the file exceeded dimensions Limits.");
                                        }
                                        else
                                            $('#errorfiles').html("<b>" + msg + "</b> Uploaded.");

                                        return; //alert(img.height);
                                    };
                                    return img.src = fr.result;
                                
							}; return fr.readAsDataURL(file);
                        }
                    });     
                },
                success: function (file, response) {
                    pirFiles = $('#ctl00_cphTransaction_dzPIRFiles').val() + file.name + "|";
                    $('#ctl00_cphTransaction_dzPIRFiles').val(pirFiles);
                    file.previewElement.classList.add("dz-success");
                    var data = $('#ctl00_cphTransaction_dZUploadpir').html();
                    $('#ctl00_cphTransaction_hdnimg').val(data);
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }    
            $('.dropzone').each(function () {
                let dropzoneControl = $(this)[0].dropzone;
                if (dropzoneControl) {
                    dropzoneControl.destroy();
                }
            });
            dropZonePIR = new Dropzone('#ctl00_cphTransaction_dZUploadpir', dropZonePIRObj); 
        }
        $(document).ready(function () {          
            InitDropZone();
            var oldorderId = 0;
            $('body').on('click', '.editImage', function (e) {
                e.preventDefault();                                
                var existinGeditRow = $("#ctl00_cphTransaction_DLCMSMaster").find('.updImg');
                if (existinGeditRow.length > 0) {
                    $(".updImg").closest('tr').find('td:eq(1)').find('.uploadFile').remove();
                    $(".updImg").closest('tr').find('td:eq(2)').find('.txtSlideOrder').remove();
                    $(".updImg").closest('td').find('a').show();
                    $(".updImg").closest('td').find('button').show();
                    if (oldorderId != 0) {
                        $(".updImg").closest('tr').find('td:eq(2) .lblOrder').text(oldorderId)
                    }                    
                    $(".updImg").closest('tr').find('td:eq(2) .lblOrder').show();
                    $(".cancelUpdatebtn").remove();
                    $(".updImg").addClass('btnNonVisibile');
                    
                }                  
                oldorderId=$(this).closest('tr').find('td:eq(2) input[type="hidden"]').val();
                $(this).closest('tr').find('td:eq(1)').append('<input type="file" class="form-control uploadFile" data-isload=false />');
                $(this).closest('tr').find('td:eq(2)').append('<input type="text" class="form-control txtSlideOrder" value="'+oldorderId+'" />');
                $(this).closest('td').find('a').hide();
                $(this).closest('tr').find('td:eq(2) .lblOrder').hide();
                $(this).hide();                
                $(this).closest('td').append('<button class="btn btn-danger cancelUpdatebtn">Cancel</button>');
                $(this).closest('td').find('.updImg').removeClass('btnNonVisibile');               

            });
            $('body').on('click', '.cancelUpdatebtn', function (e) {
                e.preventDefault();
                $(this).closest('td').find('a').show();
                $(this).closest('td').find('button').show();
                $(this).closest('td').find(".updImg").addClass('btnNonVisibile');               
                $(this).closest('tr').find('td:eq(1)').find('.uploadFile').remove();
                $(this).closest('tr').find('td:eq(2) .lblOrder').text(oldorderId)                                      
                $(this).closest('tr').find('td:eq(2) .lblOrder').show();
                $(this).closest('tr').find('td:eq(2)').find('.txtSlideOrder').remove();
                $(this).remove();               
                
            });
            $('body').on('click', '.updImg', function (e) {                
                var uploadInput = $(this).closest('tr').find('td:eq(1)').find('.uploadFile');  
                var orderId = $(this).closest('tr').find('td:eq(2)').find('.txtSlideOrder').val();
                $(this).closest('tr').find('td:eq(2) input[type="hidden"]').val(orderId);
                $(this).closest('td').find('a').show();
                $(this).closest('td').find('button').show();
                $(this).closest('td').find(".cancelUpdatebtn").remove();
                $(this).closest('tr').find('td:eq(1)').find('.uploadFile').remove();
                $(this).addClass('btnNonVisibile');  
            });
            var _URL = window.URL || window.webkitURL;
            $('body').on('change', '.uploadFile', function () {
                var inpElm = $(this);
                var filesize = 1024 * 500;
                var maxHeight = 412;
			    var maxWidth = 580;
                var file = $(this).get(0).files[0];
                if (file.size > filesize) {
                    toastr.error(file.name+' Failed to Upload the files exceeded file Limits.');
                    $('#errorfiles').html("<b>" + file.name + "</b> Failed to Upload the files exceeded file Limits.");
                    $(inpElm).attr('data-isload', false);
                    $(inpElm).closest('tr').find('input[data-id="isUploaded"]').val(0)
                }
                else {
                    var img = new Image();
                    img.onload = function () {
                        var msg = file.name;
                        if (img.height > maxHeight || img.width > maxWidth) {
                            toastr.error(file.name+' Failed to Upload the file exceeded dimensions Limits.');
                            $('#errorfiles').html("<b>" + msg + "</b> Failed to Upload the file exceeded dimensions Limits.");
                            $(inpElm).val('');
                            (inpElm).attr('data-isload', false);
                            $(inpElm).closest('tr').find('input[data-id="isUploaded"]').val(0)
                        }
                        else {
                            var formData = new FormData();
                            formData.append('file', file);
                            $.ajax({
                                url: "hn_B2BCMSMaster.ashx",
                                type: "post",
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function () {
                                    $('#errorfiles').html("<b>" + msg + "</b> Uploaded.");
                                    $(inpElm).closest('tr').find('input[data-id="isUploaded"]').val(1);
                                    $(inpElm).attr('data-isload', true);
                                },
                                error: function () {
                                    $(inpElm).closest('tr').find('input[data-id="isUploaded"]').val(0)
                                    alert("File uploading failed");
                                }
                            });
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }  
            });

            $('body').on('keydown', '.txtSlideOrder', function (e) {               
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $('body').on('change', '.txtSlideOrder', function (e) {
                if ($(this).val() == "") {
                    $(this).val(oldorderId)
                }
                else if (parseInt($(this).val()) == 0) {
                    $(this).val(oldorderId)
                }
            });
        });
        function SetComments() {
            var iframeid1 = document.getElementById('cke_contents_ctl00_cphTransaction_txtBankDesc');
            var iframe1 = iframeid1.querySelector('iframe');
            document.getElementById('ctl00_cphTransaction_divBankDetails').innerHTML = iframe1.contentWindow.document.body.innerHTML

            var iframeid2 = document.getElementById('cke_contents_ctl00_cphTransaction_txtSupportDesc');
            var iframe2 = iframeid2.querySelector('iframe');
            document.getElementById('ctl00_cphTransaction_divSupportDetails').innerHTML = iframe2.contentWindow.document.body.innerHTML
            return false;
        }
        function Save() {
            var isvalid = true;
            var ImageData = new Array();
            var ImageNames = new Array();
            var i = 0;
            $('#ctl00_cphTransaction_dZUploadpir img').each(function () {
                ImageData[i] = $(this).attr('src');
                ImageNames[i] = $(this).attr('alt');
                i++;
            });
           // document.getElementById('<%=hdnImageData.ClientID%>').value = JSON.stringify(ImageData);
            document.getElementById('<%=hdnImageFileNames.ClientID%>').value = JSON.stringify(ImageNames);
            var filesnames = document.getElementById('<%=hdnImageFileNames.ClientID%>').value; 
            var buttontext = $('#ctl00_cphTransaction_btnSave').val();
            if (buttontext == "Save") {
                var ddlAgents = $("#<%=ddlAgents.ClientID%>").val();
                if (ddlAgents == "-1" || $("#<%=ddlAgents.ClientID%>").text() == "-- Select --") {
                    $("#s2id_ctl00_cphTransaction_ddlAgents").focus();
                    $("#s2id_ctl00_cphTransaction_ddlAgents").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlAgents").innerText = "Please select Agent.";
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlAgents").css("border-color", "");
                    $('#errorddlAgents').val('');
                }

                if (ImageNames.length == 0) {
                    document.getElementById("errorfiles").innerText = "Please select the file.";
                    isvalid = false;
                }
                else
                    $('#errorfiles').val('');
            }

            var bankdesc = $('#ctl00_cphTransaction_divBankDetails').html().trim();
            var supplierdesc = $('#ctl00_cphTransaction_divSupportDetails').html().trim();
            if (bankdesc == "" && supplierdesc == "") {
                isvalid = false;
                document.getElementById("errorbank").innerText = "Please enter the Bank/Supplier Details.";
                $('#errorbank').val('');
                $('#errorsupplier').val('');
            } else {
                $('#errorbank').val('');
                $('#errorsupplier').val('');
            }
            return isvalid;
        }
        function FindAgents() {
            var isvalid = true;
            var ddlFindAgent = $("#<%=ddlFindAgent.ClientID%>").val();
            if (ddlFindAgent == "-1" || $("#<%=ddlFindAgent.ClientID%>").text() == "-- Select --") {
                $("#s2id_ctl00_cphTransaction_ddlFindAgent").focus();
                $("#s2id_ctl00_cphTransaction_ddlFindAgent").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorddlFindAgents").innerText = "Please select Agent.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlFindAgent").css("border-color", "");
                $('#errorddlFindAgents').val('');
            }
            if (isvalid) {
                $("[class*='modal-backdrop fade in']").remove();
                $("body").removeClass('modal-open');
            }
            return isvalid;
        }
        function message(msg) {
            alert(msg);
           // window.location.href = "B2BCMSMaster.aspx"; 
        }
        function deleteSlider() {
           return confirm("Are You Sure Delete the Slider."); 
        }
    </script>
 
     <style type="text/css">
        .pagination-ys {
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
            background-color: #6d6b6c;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    font-weight: bold;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }

        #DLCMSMaster thead tr th {
            text-align: center !important;
            width: auto;
        }

        #DLCMSMaster tbody tr td {
            width: 100%;
        }
 
        .modal-content {
            width: 150%;
            overflow-y: auto;
        }

        .modal-body {
            height: auto;
            overflow: auto;
        }
         .modalBackground {
            height: 100%;
            width: 100%;
            background-color: #000000;
            filter: alpha(opacity=80);
            opacity: 0.7;
        } 
        .table tbody tr th {
            text-align: center !important;
        }
        .btnNonVisibile{
            visibility:hidden;
        }
    </style>
</asp:Content>

