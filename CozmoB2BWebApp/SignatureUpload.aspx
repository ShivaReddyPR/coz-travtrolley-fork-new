﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="SignatureUploadGUI" Title="Signature Upload" MasterPageFile="~/TransactionBE.master" Codebehind="SignatureUpload.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
     <div><center>  <h4> Signature Upload</h4></center> </div>
     <div class=" bg_white bor_gray pad_10 paramcon" style="width: 50%; float: right; margin-right: 25%;"> 
         <center>
    <div >      
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:FileUpload ID="fuMultiple" runat="server" AllowMultiple="true"  accept=".jpg,.png,.gif,.html,.js,.css" />
                 <label style=" padding-right:260px" class="f_R"> <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
    runat="server" Display="Dynamic" ErrorMessage="Please Select one image" 
    ControlToValidate="fuMultiple"></asp:RequiredFieldValidator></label>
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
            </Triggers>
        </asp:UpdatePanel>           
           <label style=" padding-right:300px" class="f_R"> <asp:Button  ID="btnUpload" Width="100px"  CssClass="btn but_b"  runat="server" OnClick="btnUpload_Click" Text="UPLOAD" /></label>         
        </div>
             </center>
         </div>
    </asp:content>

