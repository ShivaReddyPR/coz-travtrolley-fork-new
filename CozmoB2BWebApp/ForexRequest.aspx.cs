﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Collections;
using System.Data;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.BookingEngine;
using System.Configuration;

namespace CozmoB2BWebApp
{
    public partial class ForexRequest : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                BindDetails();
                hdnAgentType.Value =Settings.LoginInfo.AgentType.ToString();
            }
        }
        /// <summary>
        /// Saving the forex request details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            try
            {
                ForexRequestDetails forexRequest = new ForexRequestDetails();
                forexRequest.FRID = !string.IsNullOrEmpty(hdnFRID.Value) ? Convert.ToInt64(hdnFRID.Value) : 0;
                forexRequest.FRRefNo ="FRX"+( !string.IsNullOrEmpty(hdnFRRefNo.Value) ? Convert.ToString(hdnFRRefNo.Value) : DateTime.Now.ToString("yyMMddHHmm"));
                forexRequest.AgentId = rbb2b.Checked ? Convert.ToInt32(ddlagents.SelectedValue) : Settings.LoginInfo.AgentId;
                forexRequest.LocationId = rbb2b.Checked ? Convert.ToInt32(ddlAgentLoc.SelectedValue) : Settings.LoginInfo.LocationID;
                forexRequest.IsOnbehalfAgent = rbb2b.Checked ? true : false;
                forexRequest.ProductType = ddlProduct.SelectedValue;
                forexRequest.Currency = ddlCurrency.SelectedValue;
                forexRequest.ForexRequired = Convert.ToString(txtForex.Text);
                forexRequest.AmountInINR = Convert.ToString(txtAmount.Text);
                forexRequest.Name = txtName.Text;
                forexRequest.MobileNumber = txtMobileNo.Text;
                forexRequest.Email = txtEmail.Text;
                forexRequest.TravellingTo = txtTravelling.Text;
                forexRequest.CreatedBy = (int)Settings.LoginInfo.UserID;
                forexRequest.CreatedOn = DateTime.Now;

                List<ForexRequestRemarks> requestRemarks = new List<ForexRequestRemarks>();
                ForexRequestRemarks requestRemark = new ForexRequestRemarks();
                requestRemark.FRRID = !string.IsNullOrEmpty(hdnFRRID.Value) ? Convert.ToInt64(hdnFRRID.Value) : 0;
                requestRemark.FRID = !string.IsNullOrEmpty(hdnFRID.Value) ? Convert.ToInt64(hdnFRID.Value) : 0;
                requestRemark.RemarksStatus =   ForexStatus.Pending.ToString()  ;
                requestRemark.Remarks =   "Pending";
                requestRemark.CreatedBy = (int)Settings.LoginInfo.UserID;
                requestRemark.CreatedOn = DateTime.Now;
                requestRemarks.Add(requestRemark);
                forexRequest.forexRequestRemarks = requestRemarks;
                int result = forexRequest.save();
                if (result > 0)
                {
                    msg = "Successfully" + (string.IsNullOrEmpty(hdnFRID.Value) ? " Saved " : " Updated ") + "Forex Request Details";
                    AgentAppConfig clsCnfig = new AgentAppConfig();
                    clsCnfig.AgentID = 1;
                    var liconfigs = clsCnfig.GetConfigData();
                    var config = liconfigs.Where(x => x.ProductID== (int)ProductType.Forex && x.AppKey.ToUpper() == "FOREXAGENTEMAIL").FirstOrDefault();
                    if (config != null && !string.IsNullOrWhiteSpace(config.AppValue))
                    { 
                        Hashtable ht = new Hashtable(); 
                        ht["AgentName"] = Settings.LoginInfo.AgentId == forexRequest.AgentId? Settings.LoginInfo.AgentName : new AgentMaster(forexRequest.AgentId).Name;
                        ht["FRRefNo"] = forexRequest.FRRefNo;
                        ht["ProductType"] = forexRequest.ProductType;
                        ht["Currency"] = forexRequest.Currency;
                        ht["ForexRequired"] = forexRequest.ForexRequired;
                        ht["AmountInINR"] = forexRequest.AmountInINR;
                        ht["Name"] = forexRequest.Name;
                        ht["MobileNumber"] = forexRequest.MobileNumber;
                        ht["Email"] = forexRequest.Email;
                        ht["TravellingTo"] = forexRequest.TravellingTo;
                        ht["CreatedOn"] = forexRequest.CreatedOn;
                        ht["RemarksStatus"] = requestRemark.RemarksStatus;
                        ht["Remarks"] = requestRemark.Remarks;

                        List<string> toArray = new List<string>();
                        toArray.Add(config.AppValue);
                        toArray.Add(forexRequest.Email);
                        string EmailBody = ConfigurationManager.AppSettings["FOREXREQUEST"];
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Forex Request Details(Pending)", EmailBody,ht);
                    }
                }
                else
                {
                    msg = "Failed" + (string.IsNullOrEmpty(hdnFRID.Value) ? " Saved " : " Updated ") + "Forex Request Details";
                }
                Clear();
                Utility.StartupScript(this.Page, "message('" + msg + "');", "message");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Save/Update the Forex Request Details" + ex.ToString(), Request["REMOTE_ADDR"]);
                msg = string.IsNullOrEmpty(hdnFRID.Value) ? "Failed to Saving Forex Request." : "Failed to Updating Forex Request.";
                Utility.StartupScript(this.Page, "message('" + msg + "');", "message");
            }
        }
        /// <summary>
        /// loading the agent details.
        /// </summary>
        private void BindDetails()
        {
            try
            {
                DataTable dtlist = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                DataView dv = dtlist.DefaultView;
                dv.RowFilter = "agent_Id NOT IN('" + Settings.LoginInfo.AgentId + "')";
                ddlagents.AppendDataBoundItems = true;
                ddlagents.DataSource = dtlist;
                ddlagents.DataValueField = "agent_id";
                ddlagents.DataTextField = "agent_name";
                ddlagents.DataBind(); 
                ddlagents.Items.Insert(0, new ListItem("-- Select Agent --", "-1"));
                ddlAgentLoc.Items.Insert(0, new ListItem("-- Select Location --", "-1"));
                DataTable dtLocations = LocationMaster.GetList(Utility.ToInteger(ddlagents.SelectedItem.Value), ListStatus.Short, RecordStatus.Activated, string.Empty);
               
            }
            catch { throw; }
        }
        private void Clear()
        {
            rbself.Checked = true;
            rbb2b.Checked = false;
            ddlProduct.SelectedValue = "-1";
            ddlCurrency.SelectedValue = "-1";
            txtAmount.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtForex.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtName.Text = string.Empty;
            txtTravelling.Text = string.Empty;
            btnsave.Text = "Save";
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
        /// <summary>
        /// Getting the agent location details based on agent id.
        /// </summary>
        /// <param name="agentid"></param>
        /// <returns></returns>
        [WebMethod]
        public static Hashtable BindLocations(string agentid)
        {
            Hashtable hashtable = new Hashtable();
            try
            {
                DataTable dtLocations = LocationMaster.GetList(Utility.ToInteger(agentid), ListStatus.Short, RecordStatus.Activated, string.Empty);
                if (dtLocations != null && dtLocations.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtLocations.Rows)
                    {
                        if (!hashtable.ContainsKey(dr["LOCATION_ID"].ToString()))
                            hashtable[dr["LOCATION_ID"].ToString()] = dr["LOCATION_NAME"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "failed to Binding the agent Location Details" + ex.ToString(), "");
            }
            return hashtable;
        }
    }
}