﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using System.IO;
using CT.Core;

public partial class FlightAcctReportGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string FLIGHTACCTREPORT = "_FlightAcctReport";
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnExport);
                //this.Master.PageRole = true;
                lblSuccessMsg.Text = string.Empty;
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
                if (!IsPostBack)
                {
                    //((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    InitializePageControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            Array Sources = Enum.GetValues(typeof(BookingSource));
            foreach (BookingSource source in Sources)
            {
                if (source == BookingSource.AirArabia || source == BookingSource.UAPI || source == BookingSource.Indigo || source == BookingSource.AirIndiaExpressIntl || source == BookingSource.FlyDubai || source == BookingSource.TBOAir || source == BookingSource.FlightInventory || source == BookingSource.PKFares || source == BookingSource.OffLine || source == BookingSource.OffLine)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(BookingSource), source), ((int)source).ToString());
                    ddlSource.Items.Add(item);
                }
            }
            BindAgent();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindGrid();

        }
        catch
        { throw; }

    }
    #endregion

    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));
            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch { throw; }
    }
    #region session
    private DataTable FlightAcctReport
    {
        get
        {
            return (DataTable)Session[FLIGHTACCTREPORT];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(FLIGHTACCTREPORT);
            }
            else
            {
                //value.PrimaryKey = new DataColumn[] { value.Columns["ticketId"] };
                Session[FLIGHTACCTREPORT] = value;
            }
        }
    }
    #endregion


    private void BindGrid()
    {
        try
        {
            DateTime fromDate = Utility.ToDate(dcFromDate.Value);
            DateTime toDate = Utility.ToDate(dcToDate.Value);
            int agent = Utility.ToInteger(ddlAgent.SelectedValue);
            int source = Utility.ToInteger(ddlSource.SelectedValue);
            int acctStatus = Utility.ToInteger(ddlAcctStatus.SelectedValue);
            string agentType = string.Empty;
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            #region B2C purpose
            string transType = string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }
            #endregion
            FlightAcctReport = ProductReport.FlightReportGetList(fromDate, toDate, agent, source, acctStatus,agentType,transType);
            Session["FlightAcctReport"] = FlightAcctReport;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvFlightAcctReport, FlightAcctReport);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from Flight Report:" + ex.ToString(), "");
        }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={ { "HTtxtAgentCode", "agent_code" }, { "HTtxtAgentName", "agent_name" }, { "HTtxtBookingDate", "createdOn" },
                                            { "HTtxtPnr", "pnr" }, {"HTtxtTicketNo","ticketNumber"},{"HTtxtSource","Supplier"} ,
                                            {"HTtxtTravelDate","travelDate"},{"HTtxtAirline","airlineCode"},{"HTtxtPassengerName","paxfullName"},{"HTtxtRouting","routing"},
                                            {"HTtxtPayableAmount","Payableamount"},{"HTtxtInvoiceAmount","TotalAmount"},{"HTtxtProfit","profit"},{"HTtxtDiscount","Discount"},{"HTtxtInputVat","InVatAmount"},{"HTtxtOutputVat","OutVatAmount"},
                                            {"HTtxtAgentSF","AgentSF"},{"HTtxtIsAccounted","AccountedStatus"},{"HTtxtClass","class"},{"HTtxtStatus","status"},
                                             {"HTtxtFare","Fare"},{"HTtxtTax","tax"},{"HTtxtInvoiceNo","InvoiceNo"},{"HTtxtCurrrency","agent_currency"},{"HTtxtTravelDate","TicketedDate"},{"HTtxtLocation","LocationName"},
                                         {"HTtxtUser","UserName"}};

            CommonGrid g = new CommonGrid();
            FlightAcctReport=(DataTable) Session["FlightAcctReport"] ;
            g.FilterGridView(gvFlightAcctReport, FlightAcctReport.Copy(), textboxesNColumns);
            //Session["FlightAcctReport"] = ((DataTable)gvFlightAcctReport.DataSource).Copy();
            FlightAcctReport = ((DataTable)gvFlightAcctReport.DataSource).Copy();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvFlightAcctReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            selectedItem();
            gvFlightAcctReport.PageIndex = e.NewPageIndex;
            gvFlightAcctReport.EditIndex = -1;
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        try
        {

            selectedItem();
            isTrackingEmpty();

           // int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            foreach (DataRow dr in FlightAcctReport.Rows)
            {
                if (dr != null)
                {
                    bool selected = Utility.ToString(dr["isAccounted"]) == "Y" ? true : false;
                    long ticketId = Utility.ToLong(dr["ticketId"]);
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false;
                    if (selected && !isUpdated)
                    {
                        ProductReport.UpdateFlightAcctStatus(ticketId, Utility.ToInteger(Settings.LoginInfo.UserID));
                    }
                }
            }
            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = "Updated Successfully";
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcelFlightAcctList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");
            string attachment = "attachment; filename=FlightAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgFlightAcctReportList.AllowPaging = false;
            dgFlightAcctReportList.DataSource = FlightAcctReport;
            dgFlightAcctReportList.DataBind();
            dgFlightAcctReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
        finally
        {
            Response.End();
        }
    }

    private void selectedItem()
    {
        try
        {
            foreach (System.Data.DataColumn col in FlightAcctReport.Columns) col.ReadOnly = false;
            foreach (GridViewRow gvRow in gvFlightAcctReport.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfTicketId = (HiddenField)gvRow.FindControl("IThdfTicketId");

                foreach (DataRow dr in FlightAcctReport.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["ticketId"]) == hdfTicketId.Value)
                        {
                            if (Utility.ToString(dr["isAccounted"]) != "U") dr["isAccounted"] = chkSelect.Checked ? "Y" : "N";
                        }
                        dr.EndEdit();
                    }

                }

            }

        }
        catch { throw; }
    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in FlightAcctReport.Rows)
            {
                if (dr != null)
                {
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false; ;
                    if (!isUpdated && Utility.ToString(dr["isAccounted"]) == "Y")
                    {
                        _selected = true;
                        return;
                    }
                }
            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected) 
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }              

    //Binding B2B Agents
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Binding B2B2B Agents
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        { }
    }
    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            hdfParam.Value = "0";

        }
        catch (Exception ex)
        { }
    }

    #region Date Format
    protected string CTDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CTDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }

    protected string CTCurrencyFormat(object currency, object decimalPoint)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + decimalPoint);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + decimalPoint);
        }
    }
    #endregion
}
