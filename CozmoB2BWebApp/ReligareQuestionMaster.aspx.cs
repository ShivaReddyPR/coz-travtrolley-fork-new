﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareQuestionMaster : CT.Core.ParentPage
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
               
                IntialiseControls();


            }

        }

        #region Binding Productid in CT_T_Religare_ProductType
         void BindProductId()
        {
            try
            {
                ddlProductId.DataSource = ReligareProductDetails.GetList();
                ddlProductId.DataValueField = "PRODUCTID";
                ddlProductId.DataTextField = "PRODUCTNAME";
                ddlProductId.DataBind();
                ddlProductId.Items.Insert(0, new ListItem("Select Product Type", "-1"));

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
        #region Methods
        void IntialiseControls()
        {
            BindProductId();
        }

         void Save()
        {
            try
            {
                QuestionMaster Qmaster=new QuestionMaster();
                if (Utility.ToInteger(hdfMeid.Value) > 0)
                {
                    Qmaster.ID = Utility.ToLong(hdfMeid.Value);
                }
                else { Qmaster.ID = -1; }
                string questionsetcode = string.Empty;
                Qmaster.PRODUCTID= Utility.ToInteger(ddlProductId.SelectedItem.Value);
                Qmaster.QUESTION_SET_CODE = txtQuestionSetcode.Text;
                Qmaster.QUESTIONCODE = txtQuestionCode.Text;
                Qmaster.QUESTION_DESCRIPTION = txtQuestionDescription.Text;
                Qmaster.QUESTION_TYPE = ddlQuestionType.SelectedItem.Value;
                Qmaster.CREATED_BY = Settings.LoginInfo.UserID;
                Qmaster.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfMeid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

          void Clear()
        {
           
            txtQuestionCode.Text = string.Empty;
            txtQuestionDescription.Text = string.Empty;
            txtQuestionSetcode.Text = string.Empty;
            ddlProductId.SelectedIndex = -1;
            ddlQuestionType.SelectedIndex = 0;
        }

         void bindSearch()
        {

            try
            {

                DataTable dt = QuestionMaster.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }


         void Edit(long id)
        {
            try
            {
                QuestionMaster Qmaster = new QuestionMaster(id);
                hdfMeid.Value = Utility.ToString(Qmaster.ID);
                ddlProductId.SelectedValue =Convert.ToString(Qmaster.PRODUCTID);
                txtQuestionSetcode.Text = Qmaster.QUESTION_SET_CODE;
                txtQuestionCode.Text = Qmaster.QUESTIONCODE;
                txtQuestionDescription.Text = Qmaster.QUESTION_DESCRIPTION;
                ddlQuestionType.SelectedItem.Value = Qmaster.STATUS;
                if (Qmaster.STATUS == "S")
                {
                    ddlQuestionType.SelectedValue = "SPONSER";
                }
                else if (Qmaster.STATUS == "O")
                {
                    ddlQuestionType.SelectedValue = "OPTIONALCOVERS";
                }
                else if (Qmaster.STATUS == "A")
                {
                    ddlQuestionType.SelectedValue = "PED";
                }
                else
                { ddlQuestionType.SelectedItem.Value = ""; }
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareQuestionMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareQuestionMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
               Clear();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareQuestionMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
        #region GridView Events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long questionid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(questionid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareQuestionMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();


            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareQuestionMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion


    }
}
