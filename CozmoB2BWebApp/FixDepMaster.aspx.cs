﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections.Generic;
using CT.ActivityDeals;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class FixDepMasterGUI : CT.Core.ParentPage
{
    private string AFC_DETAILS_SESSION = "_FixDepDeatilList";
    private string PRICE_DETAILS_SESSION = "_PriceDetails";
    private string ACF_SESSION = "ACFdetails";
    private string FD_ITINERARY_DETAILS = "FDItineraryDetails";

    protected Dictionary<string, string> activityVariables = new Dictionary<string, string>();
    protected Dictionary<string, string[]> activityVariablesArray = new Dictionary<string, string[]>();
    protected string dateBlockStyle = String.Empty;
    protected bool isAdmin = false;
    protected string[] splitter = { "#&#" };
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblError.Visible = false;
            lblMessage.Text = string.Empty;
            if (Convert.ToBoolean(Request["savePage"]))
            {
                Save();
            }
            else
            {
                string[] emptyStringArr = new string[1];
                emptyStringArr[0] = string.Empty;
                if (!IsPostBack)
                {
                    InitializePageControls();
                    long activityId = Convert.ToInt32(Request.QueryString["activity"]);
                    //activityId = 28;
                    if (activityId > 0)
                        LoadActivity(activityId);

                    //txtDuration.Attributes.Add("readonly", "true");
                    if (activityId <= 0)
                    {
                        
                        int inclusionsCount = 0;
                        if (inclusionsCount < 2)
                        {
                            inclusionsCount = 2;
                            activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                        }
                        int exclusionsCount = 0;
                        if (exclusionsCount < 2)
                        {
                            exclusionsCount = 2;
                            activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                        }

                        int CancellPolicyCount = 0;
                        if (CancellPolicyCount < 2)
                        {
                            CancellPolicyCount = 2;
                            activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                        }

                        int ThingsToBringCount = 0;
                        if (ThingsToBringCount < 2)
                        {
                            ThingsToBringCount = 2;
                            activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                        }

                        int UnAvailDatesCount = 0;
                        if (UnAvailDatesCount < 2)
                        {
                            UnAvailDatesCount = 2;
                            activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                        }
                        int RegionCountries = 0;
                        if (RegionCountries < 2)
                        {
                            RegionCountries = 2;
                            activityVariablesArray.Add("RegionCountriesValue", emptyStringArr);
                        }
                        
                    }
                    else
                    {


                        //Inclusion

                        int inclusionsCount = Convert.ToInt16(hdfinclCounter.Value);
                        emptyStringArr[0] = string.Empty;
                        string inclusions = hdfinclusion.Value;
                        if (inclusionsCount <= 2 && inclusions == "#&#")
                        {
                            inclusionsCount = 2;
                            activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                        }
                        else
                        {
                            activityVariablesArray.Add("inclusionsValue", inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                        }


                        //Exclusion

                        int exclusionsCount = Convert.ToInt16(hdfExclCounter.Value);
                        emptyStringArr[0] = string.Empty;
                        string exclusions = hdfExclusions.Value;
                        if (exclusionsCount <= 2 && exclusions == "#&#")
                        {
                            exclusionsCount = 2;
                            activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                        }
                        else
                        {
                            activityVariablesArray.Add("exclusionsValue", exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                        }


                        //UnavailDate
                        int UnAvailDatesCount = Convert.ToInt16(hdfUnavailDateCounter.Value);
                        emptyStringArr[0] = string.Empty;
                        string unavailDates = hdfUnavailDates.Value;
                        if (UnAvailDatesCount <= 2 && unavailDates == "#&#")
                        {
                            UnAvailDatesCount = 2;
                            activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                        }
                        else
                        {
                            activityVariablesArray.Add("UnAvailDatesValue", unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                        }


                        //CancelPolicy
                        int CancelPolicyCount = Convert.ToInt16(hdfCanPolCounter.Value);
                        emptyStringArr[0] = string.Empty;
                        string cancelPolicy = hdfCanPolicy.Value;
                        if (CancelPolicyCount <= 2 && cancelPolicy == "#&#")
                        {
                            CancelPolicyCount = 2;
                            activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                        }
                        else
                        {
                            activityVariablesArray.Add("CancellPolicyValue", cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                        }



                        //Things to Bring
                        int thingsCount = Convert.ToInt16(hdfThingsCounter.Value);
                        emptyStringArr[0] = string.Empty;
                        string things = hdfThings.Value;
                        if (thingsCount <= 2 && things == "#&#")
                        {
                            thingsCount = 2;
                            activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                        }
                        else
                        {
                            activityVariablesArray.Add("ThingsToBringValue", things.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                        }

                        int regionsCount = Convert.ToInt16(hdfRegionsCounter.Value);
                        emptyStringArr[0] = string.Empty;
                        string regions = hdfRegions.Value;

                        if (regionsCount <= 2)
                        {
                            regionsCount = 2;
                            activityVariablesArray.Add("RegionCountriesValue", emptyStringArr);
                            hdfRegionsCounter.Value = "2";
                        }
                        else
                        {
                            activityVariablesArray.Add("RegionCountriesValue", regions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                        }
                    }

                }
                else
                {
                    BindPostBackControls();
                }
                hdfinclusion.Value = "";
                hdfExclusions.Value = "";
                hdfCanPolicy.Value = "";
                hdfThings.Value = "";
                hdfUnavailDates.Value = "";
                
                
                activityVariables.Add("exclusionsCount", hdfExclCounter.Value);
                activityVariables.Add("inclusionsCount", hdfinclCounter.Value);
                activityVariables.Add("CancellPolicyCount", hdfCanPolCounter.Value);
                activityVariables.Add("ThingsToBringCount", hdfThingsCounter.Value);
                activityVariables.Add("UnAvailDatesCount", hdfUnavailDateCounter.Value);
                activityVariables.Add("RegionsCount", hdfRegionsCounter.Value);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    //Binding Agent ... @@@ 14/10/2015
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindPostBackControls()
    {
        try
        {
          

            string[] emptyStringArr = new string[1];
            emptyStringArr[0] = string.Empty;
            int inclusionsCount = Convert.ToInt16(hdfinclCounter.Value);
            emptyStringArr[0] = string.Empty;

            string inclusions = string.Empty;
            for (int i = 1; i < inclusionsCount; i++)
            {
                inclusions += Request["InclusionsText-" + i];
                inclusions += "#&#";
            }
            if (inclusions == "#&#")
            {
                activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                hdfinclCounter.Value = "2";
            }
            else
            {
                if (inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                    hdfinclCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("inclusionsValue", inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfinclCounter.Value = Convert.ToString(inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }

            //Exclusion

            int exclusionsCount = Convert.ToInt16(hdfExclCounter.Value);
            emptyStringArr[0] = string.Empty;
            string exclusions = string.Empty;
            for (int i = 1; i < exclusionsCount; i++)
            {
                exclusions += Request["ExclusionsText-" + i];
                exclusions += "#&#";
            }
            if (exclusions == "#&#")
            {
                activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                hdfExclCounter.Value = "2";
            }
            else
            {
                if (exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                    hdfExclCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("exclusionsValue", exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfExclCounter.Value = Convert.ToString(exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }

            //UnavailDate
            int UnAvailDatesCount = Convert.ToInt16(hdfUnavailDateCounter.Value);
            emptyStringArr[0] = string.Empty;
            string unavailDates = string.Empty;
            for (int i = 1; i < UnAvailDatesCount; i++)
            {
                unavailDates += Request["txtUnAvailDatesText-" + i];
                unavailDates += "#&#";
            }
            if (unavailDates == "#&#")
            {
                activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                hdfUnavailDateCounter.Value = "2";
            }
            else
            {
                if (unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                    hdfUnavailDateCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("UnAvailDatesValue", unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfUnavailDateCounter.Value = Convert.ToString(unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }


            //CancelPolicy
            int CancelPolicyCount = Convert.ToInt16(hdfCanPolCounter.Value);
            emptyStringArr[0] = string.Empty;
            string cancelPolicy = string.Empty;
            for (int i = 1; i < CancelPolicyCount; i++)
            {
                cancelPolicy += Request["CancellPolicyText-" + i];
                cancelPolicy += "#&#";
            }
            if (cancelPolicy == "#&#")
            {
                activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                hdfCanPolCounter.Value = "2";
            }
            else
            {
                if (cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                    hdfCanPolCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("CancellPolicyValue", cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfCanPolCounter.Value = Convert.ToString(cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }



            //Things to Bring
            int thingsCount = Convert.ToInt16(hdfThingsCounter.Value);
            emptyStringArr[0] = string.Empty;

            string things = string.Empty;
            for (int i = 1; i < thingsCount; i++)
            {
                things += Request["ThingsToBringText-" + i];
                things += "#&#";
            }
            if (things == "#&#")
            {
                activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                hdfThingsCounter.Value = "2";
            }
            else
            {
                if (things.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                    hdfThingsCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("ThingsToBringValue", things.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfThingsCounter.Value = Convert.ToString(things.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }

            }
            string region = "", country = "", city = "";
            //To Retain the values between the post backs
            if (Convert.ToInt16(Request["RegionsCurrent"]) > 2)
            {

                region = RegionsText1.SelectedValue;
                if (Convert.ToInt16(Request["RegionsCurrent"]) == 3)
                {
                    //country = hdfCountries.Value;
                    country = hdfSelCountry.Value;
                    //city = hdfCities.Value;
                    city = hdfSelCity.Value;
                }
                else
                {
                    country = hdfSelCountry.Value;
                    city = hdfSelCity.Value;
                }

                for (int i = 2; i < Convert.ToInt16(Request["RegionsCurrent"]); i++)
                {
                    if (region.Length > 0)
                    {
                        region += "," + Request["RegionsText-" + i];
                    }
                    else
                    {
                        region = Request["RegionsText-" + i];
                    }

                    if (country.Length > 0)
                    {
                        country += "," + Request["CountryText-" + i];
                    }
                    else
                    {
                        country = Request["CountryText-" + i];
                    }

                    if (city.Length > 0)
                    {
                        city += "," + Request["CityText-" + i];
                    }
                    else
                    {
                        city = Request["CityText-" + i];
                    }
                }

                string[] regions = region.Split(',');
                string[] countries = country.Split(',');
                string[] cities = city.Split(',');

                hdfRegionsCounter.Value = Convert.ToString(region.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length + 1);

                if (!string.IsNullOrEmpty(regions[0]))
                {
                    try
                    {
                        RegionsText1.Items.FindByValue(regions[0]).Selected = true;
                        GetCountry();

                        if (!string.IsNullOrEmpty(countries[0]))
                            CountryText1.Items.FindByValue(countries[0]).Selected = true;

                        GetCity("");
                        if (!string.IsNullOrEmpty(cities[0]))
                            CityText1.Items.FindByValue(cities[0]).Selected = true;
                    }
                    catch (Exception)
                    {
                    }
                }
                hdfCountries.Value = country;
                hdfCities.Value = city;
                hdfRegions.Value = region;
            }
            else
            {
                region = RegionsText1.SelectedValue;
                country = hdfSelCountry.Value;
                city = hdfSelCity.Value;
                hdfCountries.Value = country;
                hdfCities.Value = city;
                hdfRegions.Value = region;

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /*protected void Page_PreRender(object sender, EventArgs e)
    {
        try
        {
            string rrr = txtDuration.Text;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }*/


    private void InitializePageControls()
    {
        try
        {
            clear();
            //GetCountry();
            //GetCity(ddlCountry.SelectedItem.Value);
            Getthemes();
            BindDays();
            BindRegions();
            BindAgent();
        }
        catch
        {
            throw;
        }
    }

    private void GetCountry()
    {
        CountryText1.DataSource = Country.GetCountryListByRegion(RegionsText1.SelectedValue);
        CountryText1.DataValueField = "Value";
        CountryText1.DataTextField = "Key";
        CountryText1.DataBind();
    }

    private void GetCity(string countryid)
    {
        CityText1.DataSource = Activity.GetCity(CountryText1.SelectedValue);
        CityText1.DataTextField = "cityName";
        CityText1.DataValueField = "cityCode";
        CityText1.DataBind();
    }

    private void Getthemes()
    {
        try
        {
            chkTheme.DataSource = Activity.GetTheme("F");
            chkTheme.DataValueField = "themeId";
            chkTheme.DataTextField = "themeName";
            chkTheme.DataBind();
        }
        catch
        {
            throw;
        }
    }

    private void BindDays()
    {
        ddlDays.Items.Clear();

        for (int i = 1; i <= 31; i++)
        {
            ddlDays.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    private void BindRegions()
    {
       // Commented by chandan for notfound GetDistinctRegions();on 11/10/2015 @@@
        RegionsText1.DataSource = Region.GetDistinctRegions();
        RegionsText1.DataTextField = "RegionName";
        RegionsText1.DataValueField = "RegionCode";
        RegionsText1.DataBind();
    }
    protected void btnUpload1_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage1.HasFile == true)
            {
                if (fuImage1.PostedFile.ContentLength < 614400)// 600 kb
                {
                    string fileExtension = Path.GetExtension(fuImage1.FileName);
                    string filename = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");

                    string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
                    //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;
                    string imagePath = folderName + filename + fileExtension;
                    fuImage1.SaveAs(imagePath);
                    //Session["flname"] = flname;
                    lblUpload1.Text = filename + fileExtension;
                    //lblUploadMsg.Text = filename + fileExtension + " Image 1 uploaded.";
                    lbUploadMsg.Text = " Image 1 uploaded.";
                    hdfImage1.Value = imagePath;
                    hdfImage1Extn.Value = fileExtension;
                    hdfUploadYNImage1.Value = "1";
                    btnUpload1.Focus();
                }
                else
                {
                    lblError.Text = "File size should not be more than 600 KB";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }

        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }

    }

    protected void lbUploadMsg_Click(object sender, EventArgs e)
    {
        try
        {
            string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
            string imgPath = folderName+""+hdfImage1.Value;
            //string script="window.open("+imgpath1+");";            
            //Response.Redirect(hdfImage1.Value);
            // lbUploadMsg.OnClientClick = "return showimag1('dUpload1Msg','imgupload1','" + hdfImage1.ClientID + "')";

            //img1PreView.ImageUrl="~\\ActivityImages\\pack4.jpg";
            //img1PreView.ImageUrl = @imgPath1;
            img1PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','1');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
            btnUpload1.Focus();
        }
        catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0"); }

    }
    protected void btnUpload2_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage2.HasFile == true)
            {
                if (fuImage2.PostedFile.ContentLength < 614400)// 600 kb
                {
                    string fileExtension = Path.GetExtension(fuImage2.FileName);
                    string filename = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");

                    string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
                    string imagePath = folderName + filename + fileExtension;
                    //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;
                    //string imagePath = Server.MapPath("~/ActivityImages/") + filename + fileExtension;
                    fuImage2.SaveAs(imagePath);
                    lblUplaod2.Text = filename + fileExtension;
                    //lblUpload2Msg.Text = filename + fileExtension + " image 2 uploaded.";
                    //lblUpload2Msg.Text =  " Image 2 uploaded.";
                    lbUpload2Msg.Text = " Image 2 uploaded.";
                    hdfImage2.Value = imagePath;
                    hdfImage2Extn.Value = fileExtension;
                    hdfUploadYNImage2.Value = "1";
                    btnUpload2.Focus();
                }
                else
                {
                    lblError.Text = "File size should not be more than 600 KB";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }


        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }

    protected void lbUpload2Msg_Click(object sender, EventArgs e)
    {
        try
        {
            string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
            string imgPath =folderName+""+ hdfImage2.Value;
            //string script="window.open("+imgpath1+");";            
            //Response.Redirect(hdfImage1.Value);
            // lbUploadMsg.OnClientClick = "return showimag1('dUpload1Msg','imgupload1','" + hdfImage1.ClientID + "')";

            //img1PreView.ImageUrl="~\\ActivityImages\\pack4.jpg";
            //img1PreView.ImageUrl = @imgPath1;
            img2PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','2');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
            btnUpload2.Focus();

        }
        catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0"); }

    }
    protected void btnUpload3_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage3.HasFile == true)
            {
                if (fuImage3.PostedFile.ContentLength < 614400)// 600 kb
                {
                    string fileExtension = Path.GetExtension(fuImage3.FileName);
                    string filename = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");

                    string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
                    string imagePath = folderName + filename + fileExtension;
                    //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;
                    fuImage3.SaveAs(imagePath);
                    lblUpload3.Text = filename + fileExtension;
                    //lblUpload3Msg.Text = filename + fileExtension + " image 3 uploaded.";
                    lbUpload3Msg.Text = " Image 3 uploaded.";
                    hdfImage3.Value = imagePath;
                    hdfImage3Extn.Value = fileExtension;
                    hdfUploadYNImage3.Value = "1";
                    btnUpload3.Focus();
                }
                else
                {
                    lblError.Text = "File size should not be more than 600 KB";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }

        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }

    protected void lbUpload3Msg_Click(object sender, EventArgs e)
    {
        try
        {
            string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
            string imgPath = folderName+""+hdfImage3.Value;
            //string script="window.open("+imgpath1+");";            
            //Response.Redirect(hdfImage1.Value);
            // lbUploadMsg.OnClientClick = "return showimag1('dUpload1Msg','imgupload1','" + hdfImage1.ClientID + "')";

            //img1PreView.ImageUrl="~\\ActivityImages\\pack4.jpg";
            //img1PreView.ImageUrl = @imgPath1;
            img3PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','3');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
            btnUpload3.Focus();
        }
        catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0"); }

    }
    private DataTable ActivityDetails
    {
        get
        {
            DataTable dtActivity = null;
            dtActivity = Session[AFC_DETAILS_SESSION] as DataTable;
            return dtActivity;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["flexId"] };
            Session[AFC_DETAILS_SESSION] = value;
        }
    }

    private DataTable PriceDetails
    {
        get
        {
            DataTable dtPrice = null;

            dtPrice = Session[PRICE_DETAILS_SESSION] as DataTable;
            return dtPrice;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["priceId"] };
            Session[PRICE_DETAILS_SESSION] = value;
        }
    }

    private DataTable FDItineraryDetails
    {
        get
        {
            DataTable dtFDItineraryDetails = Session[FD_ITINERARY_DETAILS] as DataTable;
            return dtFDItineraryDetails;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ID"] };
            Session[FD_ITINERARY_DETAILS] = value;
        }
    }
    private void BindGrid()
    {
        try
        {
            DataTable dtTemp;
            if (ActivityDetails.Rows.Count > 0)
            {
                DataRow[] dr = ActivityDetails.Select("flexid= 0");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                }
                dtTemp = ActivityDetails.Copy();
                dtTemp.AcceptChanges();
                if (dtTemp.Rows.Count == 0)
                {
                    AddNewGridRowForAFD();

                }
                else
                {
                    gvAFCDetails.DataSource = ActivityDetails;
                    gvAFCDetails.DataBind();
                }

               // gvAFCDetails.DataSource = ActivityDetails;
                //gvAFCDetails.DataBind();

            }
            else
            {
                AddNewGridRowForAFD();
                dtTemp = ActivityDetails.Copy();
                dtTemp.AcceptChanges();
            }

           // setRowId();
            try
            {
                if (gvAFCDetails.Rows.Count >= 1 && Convert.ToInt32(dtTemp.Rows[0]["flexid"]) > 0)
                {
                   
                    hdfAFCCount.Value = Convert.ToString(gvAFCDetails.Rows.Count);
                }
            }
            catch
            {
                hdfAFCCount.Value = "0";
            }
            //else
            //{
                
            //    DataRow dr = ActivityDetails.NewRow();
            //    foreach (DataColumn dc in ActivityDetails.Columns)
            //    {
            //        if (dc.ColumnName == "flexControlDESC") dc.MaxLength = 20;
            //        if (dc.ColumnName == "flexDataTypeDESC") dc.MaxLength = 20;
            //        if (dc.ColumnName == "flexStatusDESC") dc.MaxLength = 20;

            //        if (!dc.AllowDBNull)
            //        {
            //            switch (dc.DataType.Name)
            //            {
            //                case "DateTime":
            //                    dr[dc] = default(DateTime);
            //                    break;
            //                default:
            //                    dr[dc] = 0;
            //                    break;
            //            }
            //        }
            //    }
            //    dr["flexid"] = 0;
            //    ActivityDetails.Rows.Add(dr);
            //    gvAFCDetails.DataSource = ActivityDetails;
            //    gvAFCDetails.DataBind();
            //    gvAFCDetails.Rows[0].Visible = false;
            //}

        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message);
            throw;
        }


    }

    private void AddNewGridRowForAFD()
    {


        DataRow dr = ActivityDetails.NewRow();
        foreach (DataColumn dc in ActivityDetails.Columns)
        {
            if (dc.ColumnName == "flexControlDESC") dc.MaxLength = 20;
            if (dc.ColumnName == "flexDataTypeDESC") dc.MaxLength = 20;
            if (dc.ColumnName == "flexStatusDESC") dc.MaxLength = 20;

            if (!dc.AllowDBNull)
            {
                switch (dc.DataType.Name)
                {
                    case "DateTime":
                        dr[dc] = default(DateTime);
                        break;
                    default:
                        dr[dc] = 0;
                        break;
                }
            }
        }

        dr["flexid"] = 0;
        ActivityDetails.Rows.Add(dr);
        gvAFCDetails.DataSource = ActivityDetails;
        gvAFCDetails.DataBind();
        gvAFCDetails.Rows[0].Visible = false;
    }

    private void BindGridPrice()
    {
        try
        {

            DataTable dtTemp;
            if (PriceDetails.Rows.Count > 0)
            {
                DataRow[] dr = PriceDetails.Select("priceId= 0");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                }
                dtTemp = PriceDetails.Copy();
                dtTemp.AcceptChanges();
                if (dtTemp.Rows.Count == 0)
                {
                    AddNewGridRow();

                }
                else
                {
                    GvPrice.DataSource = PriceDetails;
                    GvPrice.DataBind();
                }
            }
            else
            {
                AddNewGridRow();
                dtTemp = PriceDetails.Copy();
                dtTemp.AcceptChanges();
            }
           
            setRowId();
            try
            {
                if (GvPrice.Rows.Count >= 1 && Convert.ToInt32(dtTemp.Rows[0]["priceId"]) > 0)
                {
                    hdfPriceCount.Value = Convert.ToString(GvPrice.Rows.Count);
                }
            }
            catch 
            {
                    hdfPriceCount.Value = "0";            
            }

            
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message);
            throw;
        }


    }
    private void AddNewGridRow()
    {
       

        DataRow dr = PriceDetails.NewRow();
        foreach (DataColumn dc in PriceDetails.Columns)
        {
            if (!dc.AllowDBNull)
            {
                switch (dc.DataType.Name)
                {
                    case "DateTime":
                        dr[dc] = default(DateTime);
                        break;
                    default:
                        dr[dc] = 0;
                        break;
                }
            }
        }

        dr["priceId"] = 0;
        PriceDetails.Rows.Add(dr);        
        GvPrice.DataSource = PriceDetails ;
        GvPrice.DataBind();
        GvPrice.Rows[0].Visible = false;
    }
    private void clear()
    {
        gvAFCDetails.EditIndex = -1;
        Activity tempAFC = new Activity(-1);
        ActivityDetails = tempAFC.DTAFCDetails;
        PriceDetails = tempAFC.DTPriceDetails;
        FDItineraryDetails = tempAFC.DTFDItineraryDetails;
        txtActivityName.Text = string.Empty;
        txtStartingFrom.Text = string.Empty;
        fuImage1.Dispose();
        fuImage2.Dispose();
        fuImage3.Dispose();
        chkTheme.Dispose();
        lbUploadMsg.Text = string.Empty;
        lbUpload2Msg.Text = string.Empty;
        lbUpload3Msg.Text = string.Empty;
        //ddlCity.SelectedIndex = 0;
        //ddlCountry.SelectedIndex = 0;
        ddlDays.SelectedIndex = 0;
        //txtStartFromHr.Text = string.Empty;
        //txtStartTimeMin.Text = string.Empty;
        //txtEndTimeMin.Text = string.Empty;
        //txtEndToHr.Text = string.Empty;
        txtFrom.Text = string.Empty;
        txtFromTime.Text = string.Empty;
        txtTo.Text = string.Empty;
        txtToTime.Text = string.Empty;
        Date.Text = string.Empty;
        //txtPickUpHr.Text = string.Empty;
        //txtPickUpMin.Text = string.Empty;
        //txtDropUpHr.Text = string.Empty;
        //txtDropUpMin.Text = string.Empty;
        txtUnavailableDays.Text = string.Empty;
        // txtDuration.Text = string.Empty;
        txtIntroduction.Text = string.Empty;
        txtOverView.Text = string.Empty;
        txtItinerary1.Text = string.Empty;
        txtItinerary2.Text = string.Empty;
        txtDetails.Text = string.Empty;
        txtSupplierName.Text = string.Empty;
        txtSupplierEmail.Text = string.Empty;
        txtMealsInclude.Text = string.Empty;
        txtTrasferInclude.Text = string.Empty;
        txtPickLocation.Text = string.Empty;
        txtDropOffLocation.Text = string.Empty;
        txtUnavailableDays.Text = string.Empty;
        txtBookingCutOff.Text = string.Empty;
        lblUpload3.Text = string.Empty;
        lblUplaod2.Text = string.Empty;
        lblUpload1.Text = string.Empty;
        txtStockInHand.Text = string.Empty;
        //lstThings.Items.Clear();
        //      lbExclusion.Items.Clear();
        //lbInclusion.Items.Clear();
        chkTheme.SelectedIndex = -1;
        //lbCancellationPolicy.Items.Clear();
        lblMessage.Text = string.Empty;
        hdfAFCDetails.Value = "0";
        hdfPrice.Value = "0";
        BindGrid();
        BindGridPrice();
        CurrentObject = null;

        //hdfExclCounter.Value = "2";
        hdfinclCounter.Value = "2";
        hdfCanPolCounter.Value = "2";
        hdfThingsCounter.Value = "2";
        hdfUnavailDateCounter.Value = "2";
        hdfRegionsCounter.Value = "2";

        txtHotel.Text = string.Empty;
        txtAir.Text = string.Empty;
        ddlAgent.Enabled = true;

        //    Page.ClientScript.RegisterStartupScript(type"
        //GetType(), 
        //"MyKey", 
        //"Myfunction();", 
        //true);
        //for (int i = 1; i < Convert.ToInt32( hdfExclCounter.Value ); i++)
        //{
        //Page.ClientScript.RegisterStartupScript(GetType(), "remove", "RemoveAll(Exclusions-," + Convert.ToInt32(hdfExclCounter.Value) + ");");
        //Page.ClientScript.RegisterStartupScript(GetType(), "remove", "alert('hi');");
        //}
    }    
    private void LoadActivity(long id)
    {
        gvAFCDetails.EditIndex = -1;
        Activity activity = new Activity(id);
        CurrentObject = activity;
        ActivityDetails = activity.DTAFCDetails;
        PriceDetails = activity.DTPriceDetails;
        FDItineraryDetails = activity.DTFDItineraryDetails;
        txtActivityName.Text = activity.ActivityName;
        txtStartingFrom.Text = Convert.ToString(activity.StartingFrom);
        //fuImage1.Dispose();
        //fuImage2.Dispose();
        //fuImage3.Dispose();
        //chkTheme.Dispose();
        //lblUploadMsg.Text = string.Empty;
        //lblUpload2Msg.Text = string.Empty;
        //lblUpload3Msg.Text = string.Empty;  
        Getthemes();
        string[] themes = activity.Theme.Split(',');
        foreach (string theme in themes)
        {
            if (chkTheme.Items.FindByValue(theme) != null)
            {
                chkTheme.Items.FindByValue(theme).Selected = true;
            }
        }
        if (activity.FDInclusions != null)
        {
            chkInclusions.SelectedIndex = -1;
            string[] FDInclusions = activity.FDInclusions.Split(',');
            foreach (string incl in FDInclusions)
            {
                if(!string.IsNullOrEmpty(incl))chkInclusions.Items.FindByValue(incl).Selected = true;
            }
        }

        BindAgent();
        ddlAgent.SelectedValue = Convert.ToString(activity.AgencyId);
        ddlAgent.Enabled = false;
        //string[] themes = activity.Theme.Split(',');
        //int themCnt=themes.Length;

        //string s1 = string.Empty;
        //for (int i = 0; i < chkTheme.Items.Count; i++)
        //{            
        //    for(int j =0; j<themCnt ;j++)
        //    {
        //        if (chkTheme.Items[i].Value==themes[j])
        //        {
        //            chkTheme.Items[i].Selected = true;
        //        }
        //    }
        //}
        //GetCountry();
        //ddlCountry.SelectedValue = activity.Country;
        //GetCity(activity.Country);
        //ddlCity.SelectedValue = activity.City;
        
        string[] regions = activity.Region.Split(',');
        string[] countries = activity.Country.Split(',');
        string[] cities = activity.City.Split(',');

        hdfRegionsCounter.Value = Convert.ToString(activity.Region.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length + 1);

        if (!string.IsNullOrEmpty(regions[0]))
        {
            RegionsText1.Items.FindByValue(regions[0]).Selected = true;
            GetCountry();
            try
            {
                if (!string.IsNullOrEmpty(countries[0]))
                {

                    CountryText1.Items.FindByValue(countries[0]).Selected = true;
                    hdfSelCountry.Value = countries[0];
                }

                GetCity("");
            if (!string.IsNullOrEmpty(cities[0]))
            {
                CityText1.Items.FindByValue(cities[0]).Selected = true;
                hdfSelCity.Value = cities[0];
            }
            }
            catch(Exception)
            {
            }
        }
        hdfCountries.Value = activity.Country;
        hdfCities.Value = activity.City;
        hdfRegions.Value = activity.Region;

        ddlDays.SelectedValue = Convert.ToString(activity.ActivityDays);

        //Set Itineraries
        if (FDItineraryDetails.Rows.Count > 0)
        {
            DataRow row = FDItineraryDetails.Rows[0];
            txtItineraryName1.Text = row["ItineraryName"].ToString();
            txtMeals1.Text = row["Meals"].ToString();
            txtItinerary1.Text = row["Itinerary"].ToString();
        }
        if (FDItineraryDetails.Rows.Count > 1)
        {
            for (int i = 1; i < FDItineraryDetails.Rows.Count; i++)
            {
                DataRow row = FDItineraryDetails.Rows[i];
                TextBox txtName = this.FindControl("ctl00$cphTransaction$txtItineraryName" + (i + 1)) as TextBox;
                TextBox txtMeals = this.FindControl("ctl00$cphTransaction$txtMeals" + (i + 1)) as TextBox;
                TextBox txtItinerary = this.FindControl("ctl00$cphTransaction$txtItinerary" + (i + 1)) as TextBox;

                //txtName.Text = row[i].ToString();
                //txtMeals.Text = row[i].ToString();
                //txtItinerary.Text = row[i].ToString();
                if (txtName != null)
                {
                    txtName.Text = row["ItineraryName"].ToString();
                }

                if (txtMeals != null)
                {
                    txtMeals.Text = row["Meals"].ToString();
                }

                if (txtItinerary != null)
                {
                    txtItinerary.Text = row["Itinerary"].ToString();
                }
            }
        }

        string[] startFrom = activity.StartFrom.ToString("HH:mm").Split(':');
        //string[] start=startFrom.sp
        //txtStartFromHr.Text = startFrom[0];
        //txtStartTimeMin.Text = startFrom[1];

        string[] endTo = activity.EndTo.ToString("HH:mm").Split(':');
        //txtEndToHr.Text = endTo[0];
        //txtEndTimeMin.Text = endTo[1];
        //txtDuration.Text = activity.ActivityDuration;
        string FromDate = activity.AvailableDateStart.ToString("dd/MM/yyyy");
        //FromDate=FromDate.Replace(
        txtFrom.Text = activity.AvailableDateStart.ToString("dd/MM/yyyy").Replace("-", "/");
        txtFromTime.Text = activity.AvailableDateStart.ToString("hh:mm tt");
        txtTo.Text = activity.AvailableDateEnd.ToString("dd/MM/yyyy").Replace("-", "/");
        txtToTime.Text = activity.AvailableDateEnd.ToString("hh:mm tt"); ;
        Date.Text = string.Empty;
        //if (activity.PickUpDateTime != DateTime.MinValue)
        //{
        //    string[] pickTime = activity.PickUpDateTime.ToString("HH:mm").Split(':');
        //    txtPickUpHr.Text = pickTime[0];
        //    txtPickUpMin.Text = pickTime[1];
        //}
        //else
        //{
        //    txtPickUpHr.Text = string.Empty;
        //    txtPickUpMin.Text = string.Empty;
        //}

        //if (activity.DropOffDateTime != DateTime.MinValue)
        //{
        //    string[] dropTime = activity.DropOffDateTime.ToString("HH:mm").Split(':');
        //    txtDropUpHr.Text = dropTime[0];
        //    txtDropUpMin.Text = dropTime[1];
        //}
        //else
        //{
        //    txtDropUpHr.Text = string.Empty;
        //    txtDropUpMin.Text = string.Empty;
        //}
        txtUnavailableDays.Text = string.Empty;
        // txtDuration.Text = string.Empty;
        txtIntroduction.Text = activity.Introduction;
        txtOverView.Text = activity.Overview;
        txtAir.Text = activity.Itinerary1;
        txtHotel.Text = activity.Itinerary2;
        

        //txtItinerary1.Text = activity.Itinerary1;
        //if (activity.ActivityDays == 2)
        //{
        //    txtItinerary2.Visible = true;
        //    lblItinerary2.Visible = true;
        //    txtItinerary2.Text = activity.Itinerary2;
        //}
        txtDetails.Text = activity.Details;
        txtSupplierName.Text = activity.SupplierName;
        txtSupplierEmail.Text = activity.SupplierEmail;
        txtMealsInclude.Text = activity.MealsIncluded;
        txtTrasferInclude.Text = activity.TransferInclude;
        txtPickLocation.Text = activity.PickLocation;
        txtDropOffLocation.Text = activity.DropOffLocation;
        txtUnavailableDays.Text = activity.UnAvailableDays;
        txtBookingCutOff.Text = Convert.ToString(activity.BookingCutOff);

        //string
        string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityEditImagesFolder"];
        //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;

        lblUpload1.Text = activity.Image1;
        hdfImage1.Value = folderName+ lblUpload1.Text;
        hdfImage1Extn.Value = Path.GetExtension(activity.Image1);
        if (!string.IsNullOrEmpty(activity.Image1) && !string.IsNullOrEmpty(hdfImage1Extn.Value))
        {
            lbUploadMsg.Text = " Image 1 uploaded.";
            //Response.Redirect(hdfImage1.Value);
            //lbUploadMsg.OnClientClick = "return showimag1('dUpload1Msg','imgupload1','" + hdfImage1.ClientID + "')";
        }
        hdfUploadYNImage1.Value = "1";


        lblUplaod2.Text = activity.Image2;
        hdfImage2.Value = folderName + lblUplaod2.Text;

        //hdfImage1.Value = folderName + "\\" + lblUpload1.Text;
        hdfImage2Extn.Value = Path.GetExtension(activity.Image2);
        if (!string.IsNullOrEmpty(activity.Image2) && !string.IsNullOrEmpty(hdfImage2Extn.Value))
        {
            lbUpload2Msg.Text = " Image 2 uploaded.";
           // lbUpload2Msg.OnClientClick = "return showimag1('dUpload2Msg','imgupload2','" + hdfImage2.ClientID + "')";
        }
        hdfUploadYNImage2.Value = "1";


        lblUpload3.Text = activity.Image3;
        //hdfImage3.Value = "./ActivityImages/" + lblUpload3.Text;
        hdfImage3.Value = folderName + lblUpload3.Text;
        hdfImage3Extn.Value = Path.GetExtension(activity.Image3);
        if (!string.IsNullOrEmpty(activity.Image3) && !string.IsNullOrEmpty(hdfImage3Extn.Value))
        {
            lbUpload3Msg.Text = " Image 3 uploaded.";
         //   lbUpload3Msg.OnClientClick = "return showimag1('dUpload3Msg','imgupload3','" + hdfImage3.ClientID + "')";
        }
        hdfUploadYNImage3.Value = "1";


        txtStockInHand.Text = Convert.ToString(activity.StockInHand);
        if (activity.Inclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfinclCounter.Value = Convert.ToString(activity.Inclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfinclCounter.Value = "2";
        }
        if (activity.Exclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfExclCounter.Value = Convert.ToString(activity.Exclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfExclCounter.Value = "2";
        }
        if (activity.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfCanPolCounter.Value = Convert.ToString(activity.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfCanPolCounter.Value = "2";
        }

        if (activity.ThingsToBring.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfThingsCounter.Value = Convert.ToString(activity.ThingsToBring.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfThingsCounter.Value = "2";
        }
        if (activity.UnvailableDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfUnavailDateCounter.Value = Convert.ToString(activity.UnvailableDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfUnavailDateCounter.Value = "2";
        }
        hdfinclusion.Value = activity.Inclusion;
        hdfExclusions.Value = activity.Exclusion;
        hdfCanPolicy.Value = activity.CancellationPolicy;
        hdfThings.Value = activity.ThingsToBring;
        hdfUnavailDates.Value = activity.UnvailableDates;
        hdfRegions.Value = activity.Region;

        //lstThings.Items.Clear();
        //      lbExclusion.Items.Clear();
        //lbInclusion.Items.Clear();
        // chkTheme.SelectedIndex = -1;
        //lbCancellationPolicy.Items.Clear();
        lblMessage.Text = string.Empty;
        hdfAFCDetails.Value = "0";
        hdfPrice.Value = "0";
        BindGrid();
        BindGridPrice();

        //int inclusionsCount = activity.Inclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1;
        //string[] emptyStringArr = new string[1];
        //emptyStringArr[0] = string.Empty;
        //if (inclusionsCount < 2)
        //{
        //    inclusionsCount = 2;
        //    activityVariablesArray.Add("inclusionsValue", emptyStringArr);
        //}
        //else
        //{
        //    activityVariablesArray.Add("inclusionsValue", activity.Inclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
        //}
        //activityVariables.Add("inclusionsCount", Convert.ToString(inclusionsCount));



        //    Page.ClientScript.RegisterStartupScript(type"
        //GetType(), 
        //"MyKey", 
        //"Myfunction();", 
        //true);
        //for (int i = 1; i < Convert.ToInt32( hdfExclCounter.Value ); i++)
        //{
        //Page.ClientScript.RegisterStartupScript(GetType(), "remove", "RemoveAll(Exclusions-," + Convert.ToInt32(hdfExclCounter.Value) + ");");
        //Page.ClientScript.RegisterStartupScript(GetType(), "remove", "alert('hi');");
        //}
    }

   

    protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
       

    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           // GetCity(ddlCountry.SelectedItem.Value);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private void setRowId()
    {
        try
        {
            if (GvPrice.EditIndex > -1)
            {
            }
            else
            {
                ((TextBox)GvPrice.FooterRow.FindControl("FTtxtAmount")).Text = Convert.ToDecimal(0).ToString();
            }
            txtTotal.Text = Convert.ToString(PriceDetails.Compute("SUM(amount)", ""));


        }
        catch { throw; }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Save();

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
            lblMessage.Text = ex.Message;
        }
    }

    protected void RenameImage(string activityId, string imagePath, string slNo)
    {
        try
        {

            //string imagePath = hdfImage1.Value;
            string source = imagePath.Substring(0, imagePath.LastIndexOf("\\") + 1);
            if (!string.IsNullOrEmpty(source))
            {
                // string fName = imagePath;

                string oldFile = Path.GetFileName(imagePath);
                string fileExtension = Path.GetExtension(imagePath);
                string dFilePath = string.Empty;
                //dFilePath = source + dFile;
                //FileInfo fi = new FileInfo(dFilePath);
                string newfile = activityId + "_" + slNo + fileExtension;
                if (System.IO.File.Exists(source + newfile)) System.IO.File.Delete(source + newfile);
                System.IO.File.Move(source + oldFile, source + newfile);
                System.IO.File.Delete(source + oldFile);
            }

        }
        catch { }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            clear();
            Response.Redirect("FixDepMaster.aspx");
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = ex.Message;

        }
    }
    private void Save()
    {
        try
        {
            Activity tmpActivity = new Activity(-1);

            tmpActivity.IsFixedDeparture = "Y";
            int themeCount = 0;
            string s1 = string.Empty;
            for (int i = 0; i < chkTheme.Items.Count; i++)
            {
                if (chkTheme.Items[i].Selected)
                {
                    s1 += chkTheme.Items[i].Value + ",";
                    themeCount = themeCount + 1;
                }
            }

            if (themeCount == 0)
            {
                throw new Exception("Atleast one theme should be selected !");
            }

            string FDInclusions = "";

            foreach (ListItem item in chkInclusions.Items)
            {
                if (item.Selected)
                {
                    if (FDInclusions.Length > 0)
                    {
                        FDInclusions += "," + item.Value;
                    }
                    else
                    {
                        FDInclusions = item.Value;
                    }
                }
            }



            if (txtIntroduction.Text == string.Empty) throw new Exception("Introduction cannot be blank !");
            if (txtOverView.Text == string.Empty) throw new Exception("OverView cannot be blank !");
            if (txtAir.Text == string.Empty) throw new Exception("Air cannot be blank !");
            if (txtHotel.Text == string.Empty) throw new Exception("Hotel cannot be blank !");

            int excCounter = Convert.ToInt32(Request["ExclusionsCurrent"]);
            string exclusions = string.Empty;
            for (int i = 1; i < excCounter; i++)
            {
                exclusions += Request["ExclusionsText-" + i];
                exclusions += "#&#";
            }
            if (exclusions.Length > 3900) throw new Exception("Exclusion details cannot be exceeded 3900 chracters !");

            int incCounter = Convert.ToInt32(Request["InclusionsCurrent"]);
            string inclusions = string.Empty;
            for (int i = 1; i < incCounter; i++)
            {
                inclusions += Request["InclusionsText-" + i];
                inclusions += "#&#";
            }
            if (inclusions.Length > 3900) throw new Exception("Inclusion details cannot be exceeded 3900 chracters !");

            int canPolicyCounter = Convert.ToInt32(Request["CancellPolicyCurrent"]);
            string cancelPolicy = string.Empty;
            for (int i = 1; i < canPolicyCounter; i++)
            {
                cancelPolicy += Request["CancellPolicyText-" + i];
                cancelPolicy += "#&#";
            }
            if (cancelPolicy.Length > 3900) throw new Exception("Cancel Policy details cannot be exceeded 3900 chracters !");


            int ThingsToBringCounter = Convert.ToInt32(Request["ThingsToBringCurrent"]); //payment policy
            string thingsToBring = string.Empty;
            for (int i = 1; i < ThingsToBringCounter; i++)
            {
                thingsToBring += Request["ThingsToBringText-" + i];
                thingsToBring += "#&#";
            }
            if (thingsToBring.Length > 3999) throw new Exception("Payment Policy details cannot be exceeded 3900 chracters !");
            //if (txtItinerary1.Text == "") throw new Exception("Itinerary 1 cannot be blank !");

            //if (ActivityDetails.Rows.Count <= 1)
            //{
            //    DataRow[] drFlex = ActivityDetails.Select("flexid= 0");
            //    if (drFlex.Length > 0)
            //    {
            //        throw new Exception("Activity File details cannot be blank !");
            //    }

            //}

            if (PriceDetails.Rows.Count <= 1)
            {
                DataRow[] drPrice = PriceDetails.Select("priceId= 0");
                if (drPrice.Length > 0)
                {
                    throw new Exception("Price details cannot be blank !");
                }
            }

            if (CurrentObject == null)
            {
                FDItineraryDetails.Rows.Clear();
                DataRow row = FDItineraryDetails.NewRow();
                row["Day"] = 1;
                row["ItineraryName"] = txtItineraryName1.Text.Trim();
                row["Meals"] = txtMeals1.Text.Trim();
                row["Itinerary"] = txtItinerary1.Text.Trim();
                row["Status"] = "A";

                FDItineraryDetails.Rows.Add(row);

                for (int i = 2; i <= Convert.ToInt16(ddlDays.SelectedValue); i++)
                {
                    row = FDItineraryDetails.NewRow();
                    row["Day"] = i;
                    row["ItineraryName"] = Request["ctl00$cphTransaction$txtItineraryName" + i].Trim();
                    row["Meals"] = Request["ctl00$cphTransaction$txtMeals" + i].Trim();
                    row["Itinerary"] = Request["ctl00$cphTransaction$txtItinerary" + i].Trim();
                    row["Status"] = "A";

                    FDItineraryDetails.Rows.Add(row);
                }
            }
            else
            {
                if (FDItineraryDetails.Rows.Count > 0)
                {
                    DataRow[] details = FDItineraryDetails.Select("Day=1");

                    if (details != null && details.Length > 0)
                    {
                        DataRow row = details[0];
                        row["ItineraryName"] = txtItineraryName1.Text.Trim();
                        row["Meals"] = txtMeals1.Text.Trim();
                        row["Itinerary"] = txtItinerary1.Text.Trim();
                        row["Status"] = "A";
                    }

                    for (int i = 2; i <= Convert.ToInt16(ddlDays.SelectedValue); i++)
                    {
                        details = FDItineraryDetails.Select("Day=" + i);

                        if (details != null && details.Length > 0)
                        {
                            DataRow row = details[0];
                            row["ItineraryName"] = Request["ctl00$cphTransaction$txtItineraryName" + i].Trim();
                            row["Meals"] = Request["ctl00$cphTransaction$txtMeals" + i].Trim();
                            row["Itinerary"] = Request["ctl00$cphTransaction$txtItinerary" + i].Trim();
                            row["Status"] = "A";
                        }
                        else
                        {
                            DataRow row = FDItineraryDetails.NewRow();
                            row["Day"] = i;
                            row["ItineraryName"] = Request["ctl00$cphTransaction$txtItineraryName" + i].Trim();
                            row["Meals"] = Request["ctl00$cphTransaction$txtMeals" + i].Trim();
                            row["Itinerary"] = Request["ctl00$cphTransaction$txtItinerary" + i].Trim();
                            row["Status"] = "A";
                            row["Id"] = -i;
                            FDItineraryDetails.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    FDItineraryDetails.Rows.Clear();
                    DataRow row = FDItineraryDetails.NewRow();
                    row["Day"] = 1;
                    row["ItineraryName"] = txtItineraryName1.Text.Trim();
                    row["Meals"] = txtMeals1.Text.Trim();
                    row["Itinerary"] = txtItinerary1.Text.Trim();
                    row["Status"] = "A";
                    row["Id"] = -1;
                    FDItineraryDetails.Rows.Add(row);

                    for (int i = 2; i <= Convert.ToInt16(ddlDays.SelectedValue); i++)
                    {
                        row = FDItineraryDetails.NewRow();
                        row["Day"] = i;
                        row["ItineraryName"] = Request["ctl00$cphTransaction$txtItineraryName" + i].Trim();
                        row["Meals"] = Request["ctl00$cphTransaction$txtMeals" + i].Trim();
                        row["Itinerary"] = Request["ctl00$cphTransaction$txtItinerary" + i].Trim();
                        row["Status"] = "A";
                        row["Id"] = -i;
                        FDItineraryDetails.Rows.Add(row);
                    }
                }

            }

            Activity objActivity;

            if (CurrentObject == null)
            {
                objActivity = new Activity();
                objActivity.Id = -1;
            }
            else
            {
                objActivity = CurrentObject;
            }

            objActivity.ActivityName = txtActivityName.Text;
            if (txtStockInHand.Text.Trim() != "")
            {
                objActivity.StockInHand = Convert.ToInt32(txtStockInHand.Text); ;
            }
            objActivity.ActivityDays = Convert.ToInt32(ddlDays.SelectedItem.Value);
            if (txtStartingFrom.Text.Trim() != "")
            {
                objActivity.StartingFrom = Convert.ToDecimal(txtStartingFrom.Text);
            }
            objActivity.FDInclusions = FDInclusions;
            //objActivity.City = ddlCity.SelectedItem.Value;
            //objActivity.Country = ddlCountry.SelectedItem.Value;
            string region = "", country = "", city = "";

            if (region.Length > 0)
            {
                region += "," + RegionsText1.SelectedValue;
            }
            else
            {
                region = RegionsText1.SelectedValue;
            }
            country = hdfSelCountry.Value;
            //if (objActivity.Id > 0)
            // {
            //country =  CountryText1.SelectedValue;

            // }
            // else
            // {
            //    country = hdfCountries.Value;//CountryText1.SelectedValue;
            //  }

            //if (objActivity.Id > 0)
            //{
            // city = CityText1.SelectedValue;

            // }
            //else
            //{
            city = hdfCities.Value;//CityText1.SelectedValue;
            //} 
            city = hdfSelCity.Value;

            for (int i = 2; i < Convert.ToInt16(Request["RegionsCurrent"]); i++)
            {
                if (region.Length > 0)
                {
                    region += "," + Request["RegionsText-" + i];
                }
                else
                {
                    region = Request["RegionsText-" + i];
                }

                if (country.Length > 0)
                {
                    country += "," + Request["CountryText-" + i];
                }
                else
                {
                    country = Request["CountryText-" + i];
                }

                if (city.Length > 0)
                {
                    city += "," + Request["CityText-" + i];
                }
                else
                {
                    city = Request["CityText-" + i];
                }
            }

            objActivity.Region = region;
            objActivity.Country = country;
            objActivity.City = city;

            objActivity.Image1 = lblUpload1.Text;
            objActivity.Image2 = lblUplaod2.Text;
            objActivity.Image3 = lblUpload3.Text;
            objActivity.Image1Extn = hdfImage1Extn.Value;
            objActivity.Image2Extn = hdfImage2Extn.Value;
            objActivity.Image3Extn = hdfImage3Extn.Value;
            //string s1 = string.Empty;
            //for (int i = 0; i < chkTheme.Items.Count; i++)
            //{
            //    if (chkTheme.Items[i].Selected)
            //    {
            //        s1 += chkTheme.Items[i].Value + ",";
            //    }
            //}
            objActivity.Theme = s1.ToString().TrimEnd(',');
            objActivity.Introduction = txtIntroduction.Text;
            objActivity.Overview = txtOverView.Text;
            objActivity.Itinerary1 = txtAir.Text;
            objActivity.Itinerary2 = txtHotel.Text;

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            /* if (txtStartFromHr.Text != "" & txtStartTimeMin.Text != "")
             {
                 string StartFromDate = (txtStartFromHr.Text + ":" + txtStartTimeMin.Text);
                 string tmpStartFromDate = Convert.ToString(DateTime.Now.ToShortDateString());
                 StartFromDate = tmpStartFromDate + " " + StartFromDate;
                 //objActivity.StartFrom = Convert.ToDateTime(StartFromDate, dateFormat);
                 objActivity.StartFrom = Convert.ToDateTime(StartFromDate);
             }
             if (txtEndToHr.Text != "" && txtEndTimeMin.Text != "")
             {
                 string EndToDate = (txtEndToHr.Text + ":" + txtEndTimeMin.Text);
                 string tmpEndToDate = Convert.ToString(DateTime.Now.ToShortDateString());
                 EndToDate = tmpEndToDate + " " + EndToDate;
                 //objActivity.EndTo = Convert.ToDateTime(EndToDate,dateFormat);
                 objActivity.EndTo = Convert.ToDateTime(EndToDate);
             }
             objActivity.ActivityDuration = txtDuration.Text;*/
            objActivity.ActivityDuration = "0";

            objActivity.StartFrom = DateTime.Now;
            objActivity.EndTo = DateTime.Now;
            if (txtFrom.Text != "" && txtFromTime.Text != "")
            {
                string AFrom = (txtFrom.Text + " " + txtFromTime.Text);
                objActivity.AvailableDateStart = Convert.ToDateTime(AFrom, dateFormat);
                //objActivity.AvailableDateStart = Convert.ToDateTime(AFrom);
            }
            else
            {
                objActivity.AvailableDateStart = DateTime.Now;
            }
            if (txtTo.Text != "" && txtToTime.Text != "")
            {
                string ATo = (txtTo.Text + " " + txtToTime.Text);
                objActivity.AvailableDateEnd = Convert.ToDateTime(ATo, dateFormat);
                //objActivity.AvailableDateEnd = Convert.ToDateTime(ATo);
            }
            else
            {
                objActivity.AvailableDateEnd = DateTime.Now;
            }

            int unAvailCounter = Convert.ToInt32(Request["UnAvailDatesCurrent"]);
            string unAvailDates = string.Empty;
            for (int i = 1; i < unAvailCounter; i++)
            {
                unAvailDates += Request["txtUnAvailDatesText-" + i];
                unAvailDates += "#&#";
            }
            objActivity.UnvailableDates = unAvailDates;

            //objActivity.Itinerary1 = txtItinerary1.Text;
            //if (objActivity.ActivityDays == 2)
            //{
            //    objActivity.Itinerary2 = txtItinerary2.Text;
            //}
            //else
            //{
            //    objActivity.Itinerary2 = string.Empty;
            //}
            objActivity.Details = txtDetails.Text;
            //objActivity.UnAvailableDays = txtUnavailableDays.Text;

            objActivity.SupplierName = txtSupplierName.Text.Trim();
            objActivity.SupplierEmail = txtSupplierEmail.Text;
            objActivity.MealsIncluded = txtMealsInclude.Text;
            objActivity.TransferInclude = txtTrasferInclude.Text;
            objActivity.PickLocation = txtPickLocation.Text;
            //if (txtPickUpHr.Text != "" && txtPickUpMin.Text != "")
            //{
            //    string PickUpDate = (txtPickUpHr.Text + ":" + txtPickUpMin.Text);
            //    string tmpPickUpDate = Convert.ToString(DateTime.Now.ToShortDateString());
            //    PickUpDate = tmpPickUpDate + " " + PickUpDate;
            //    //objActivity.PickUpDateTime = Convert.ToDateTime(PickUpDate, dateFormat);
            //    objActivity.PickUpDateTime = Convert.ToDateTime(PickUpDate);
            //}
            objActivity.DropOffLocation = txtDropOffLocation.Text;
            //if (txtDropUpHr.Text != "" && txtDropUpMin.Text != "")
            //{
            //    //string DropUpDate = (txtDropUpHr.Text + " " + txtDropOffLocation.Text);
            //    string DropUpDate = (txtDropUpHr.Text + ":" + txtDropUpMin.Text);
            //    string tmpDropUpDate = Convert.ToString(DateTime.Now.ToShortDateString());
            //    DropUpDate = tmpDropUpDate + " " + DropUpDate;
            //    //objActivity.DropOffDateTime = Convert.ToDateTime(DropUpDate, dateFormat);
            //    objActivity.DropOffDateTime = Convert.ToDateTime(DropUpDate);
            //}
            objActivity.PickUpDateTime = DateTime.Now;
            objActivity.DropOffDateTime = DateTime.Now;
            //int excCounter = Convert.ToInt32(Request["ExclusionsCurrent"]);
            //string exclusions = string.Empty;
            //for (int i = 1; i < excCounter; i++)
            //{
            //    exclusions += Request["ExclusionsText-" + i];
            //    exclusions += "#&#";
            //}
            objActivity.Exclusion = exclusions;


            //int incCounter = Convert.ToInt32(Request["InclusionsCurrent"]);
            //string inclusions = string.Empty;
            //for (int i = 1; i < incCounter; i++)
            //{
            //    inclusions += Request["InclusionsText-" + i];
            //    inclusions += "#&#";
            //}
            objActivity.Inclusion = inclusions;


            //int canPolicyCounter = Convert.ToInt32(Request["CancellPolicyCurrent"]);
            //string cancelPolicy = string.Empty;
            //for (int i = 1; i < canPolicyCounter; i++)
            //{
            //    cancelPolicy += Request["CancellPolicyText-" + i];
            //    cancelPolicy += "#&#";
            //}
            objActivity.CancellationPolicy = cancelPolicy;


            //int ThingsToBringCounter = Convert.ToInt32(Request["ThingsToBringCurrent"]);
            //string thingsToBring = string.Empty;
            //for (int i = 1; i < ThingsToBringCounter; i++)
            //{
            //    thingsToBring += Request["ThingsToBringText-" + i];
            //    thingsToBring += "#&#";
            //}
            objActivity.ThingsToBring = thingsToBring;

            if (txtBookingCutOff.Text.Trim() != "")
            {
                objActivity.BookingCutOff = Convert.ToInt32(txtBookingCutOff.Text);
            }
            objActivity.DTAFCDetails = ActivityDetails;
            objActivity.DTPriceDetails = PriceDetails;
            objActivity.DTFDItineraryDetails = FDItineraryDetails;
            objActivity.CreatedBy = (int)Settings.LoginInfo.UserID;
            objActivity.IsFixedDeparture = "Y";
            // objActivity.AgencyId = Settings.LoginInfo.AgentId;
            objActivity.AgencyId = Convert.ToInt16(ddlAgent.SelectedItem.Value);
            try
            {
                objActivity.SaveActivity();
                string actId = Convert.ToString(objActivity.Id);
                RenameImage(actId, hdfImage1.Value, "1");
                RenameImage(actId, hdfImage2.Value, "2");
                RenameImage(actId, hdfImage3.Value, "3");
                //hdfMode.Value = "New";

                Response.Redirect("FixDepMaster.aspx");
                clear();
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Text = "SucessFully Inserted";
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
                LoadPageControls();
            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
            LoadPageControls();
        }

    }
    private void LoadPageControls()
    {
        try
        {
            activityVariables = new Dictionary<string, string>();
            activityVariablesArray = new Dictionary<string, string[]>();
            BindPostBackControls();
            hdfinclusion.Value = "";
            hdfExclusions.Value = "";
            hdfCanPolicy.Value = "";
            hdfThings.Value = "";
            hdfUnavailDates.Value = "";
            activityVariables.Add("exclusionsCount", hdfExclCounter.Value);
            activityVariables.Add("inclusionsCount", hdfinclCounter.Value);
            activityVariables.Add("CancellPolicyCount", hdfCanPolCounter.Value);
            activityVariables.Add("ThingsToBringCount", hdfThingsCounter.Value);
            activityVariables.Add("UnAvailDatesCount", hdfUnavailDateCounter.Value);
            activityVariables.Add("RegionsCount", hdfRegionsCounter.Value);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected Activity CurrentObject
    {
        get
        {
            return (Activity)Session[ACF_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ACF_SESSION);
            }
            else
            {
                Session[ACF_SESSION] = value;
            }

        }

    }


    protected void gvAFCDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvAFCDetails.FooterRow;
                DataRow dr = ActivityDetails.NewRow();
                DataTable dtDeleted = ActivityDetails.GetChanges(DataRowState.Deleted);
                long deletedSerial = 0;
                if (dtDeleted != null)
                {
                    dtDeleted.RejectChanges();
                    deletedSerial = Convert.ToInt32(dtDeleted.Compute("MAX(flexId)", ""));
                }
                long serial = Convert.ToInt32(ActivityDetails.Compute("MAX(flexId)", ""));
                serial = (serial >= deletedSerial) ? serial + 1 : deletedSerial + 1;
                dr["flexId"] = serial;
                SetActivityDetails(dr, gvRow, "FT");

                ActivityDetails.Rows.Add(dr);

                BindGrid();

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    private void SetActivityDetails(DataRow dr, GridViewRow gvRow, string mode)
    {
        try
        {

            dr["flexControl"] = ((DropDownList)gvRow.FindControl(mode + "ddlControl")).SelectedItem.Value;
            dr["flexControlDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlControl")).SelectedItem.Text;
            dr["flexLabel"] = ((TextBox)gvRow.FindControl(mode + "txtLabel")).Text;

            dr["flexSqlQuery"] = ((TextBox)gvRow.FindControl(mode + "txtSqlQuery")).Text;
            dr["flexDataType"] = ((DropDownList)gvRow.FindControl(mode + "ddlDatatype")).SelectedItem.Value;
            dr["flexDataTypeDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlDatatype")).SelectedItem.Text;
            dr["flexMandatoryStatus"] = ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Value;
            dr["flexStatusDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Text;
            dr["flexOrder"] = Convert.ToInt32(((TextBox)gvRow.FindControl(mode + "txtOrder")).Text);
            dr["flexCreatedBy"] = (int)Settings.LoginInfo.UserID;// Added By chandan on 15/10/2015
        }
        catch { throw; }
    }

    private void SetPriceDetails(DataRow dr, GridViewRow gvRow, string mode)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            dr["label"] = ((TextBox)gvRow.FindControl(mode + "txtLabel")).Text;
            dr["SupplierCost"] = ((TextBox)gvRow.FindControl(mode + "txtSupplierCost")).Text;
            dr["tax"] = ((TextBox)gvRow.FindControl(mode + "txtTax")).Text;
            dr["markup"] = ((TextBox)gvRow.FindControl(mode + "txtMarkup")).Text;
            dr["PriceDate"] = Convert.ToDateTime(((TextBox)gvRow.FindControl(mode + "txtDate")).Text, dateFormat);
            //dr["amount"] = ((TextBox)gvRow.FindControl(mode + "txtAmount")).Text;
            dr["amount"] = Convert.ToDecimal(dr["SupplierCost"]) + Convert.ToDecimal(dr["tax"]) + Convert.ToDecimal(dr["markup"]);
            dr["PaxType"] = ((DropDownList)gvRow.FindControl(mode + "ddlPaxType")).SelectedItem.Value;
            dr["StockInHand"] = ((TextBox)gvRow.FindControl(mode + "txtStockInHand")).Text;
            dr["createdBy"] = (int)Settings.LoginInfo.UserID; //Added by chandan on 15/10/2015
            //  dr["flexTypeDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlType")).SelectedItem.Text;
        }
        catch { throw; }
    }



    protected void gvAFCDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvAFCDetails.EditIndex = e.NewEditIndex;
            BindGrid();
            long serial = Convert.ToInt32(gvAFCDetails.DataKeys[e.NewEditIndex].Value);
            GridViewRow gvRow = gvAFCDetails.Rows[e.NewEditIndex];
            ((DropDownList)gvRow.FindControl("EITddlControl")).Text = ((HiddenField)gvRow.FindControl("EIThdfControl")).Value;
            ((DropDownList)gvRow.FindControl("EITddlDataType")).Text = ((HiddenField)gvRow.FindControl("EIThdfDataType")).Value;
            ((DropDownList)gvRow.FindControl("EITddlMandatory")).Text = ((HiddenField)gvRow.FindControl("EIThdfStatus")).Value;
            gvAFCDetails.FooterRow.Visible = false;
            hdfAFCDetails.Value = "2";

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void gvAFCDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvAFCDetails.Rows[e.RowIndex];
            long serial = Convert.ToInt32(gvAFCDetails.DataKeys[e.RowIndex].Value);
            DataRow dr = ActivityDetails.Rows.Find(serial);
            dr.BeginEdit();
            SetActivityDetails(dr, gvRow, "EIT");
            dr.EndEdit();
            gvAFCDetails.EditIndex = -1;
            BindGrid();
            hdfAFCDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void gvAFCDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvAFCDetails.Rows[e.RowIndex];
            long serial = Convert.ToInt32(gvAFCDetails.DataKeys[e.RowIndex].Value);
            ActivityDetails.Rows.Find(serial).Delete();
            BindGrid();
            gvAFCDetails.EditIndex = -1;
            hdfAFCDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }

    }
    protected void GvPrice_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = GvPrice.FooterRow;
                DataRow dr = PriceDetails.NewRow();
                DataTable dtDeleted = PriceDetails.GetChanges(DataRowState.Deleted);
                long deletedSerial = 0;
                if (dtDeleted != null)
                {
                    dtDeleted.RejectChanges();
                    deletedSerial = Convert.ToInt32(dtDeleted.Compute("MAX(priceId)", ""));
                }
                long serial = Convert.ToInt32(PriceDetails.Compute("MAX(priceId)", ""));
                serial = (serial >= deletedSerial) ? serial + 1 : deletedSerial + 1;
                dr["priceId"] = serial;
                SetPriceDetails(dr, gvRow, "FT");
                PriceDetails.Rows.Add(dr);
                BindGridPrice();
                btnClear.Focus();

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void GvPrice_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            GvPrice.EditIndex = e.NewEditIndex;

            BindGridPrice();
            long serial = Convert.ToInt32(GvPrice.DataKeys[e.NewEditIndex].Value);
            GridViewRow gvRow = GvPrice.Rows[e.NewEditIndex];
            ((DropDownList)gvRow.FindControl("EITddlPaxType")).Text = ((HiddenField)gvRow.FindControl("EIThdfPaxType")).Value;
            GvPrice.FooterRow.Visible = false;
            hdfPrice.Value = "2";
            //GvPrice.Focus();
            
            int nRowIndex = GvPrice.Rows.Count - 1;

            //GvPrice.Rows[nRowIndex].Focus();
            //GvPrice.Rows[nRowIndex].Cells[0].Focus();
            btnClear.Focus();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void GvPrice_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = GvPrice.Rows[e.RowIndex];
            long serial = Convert.ToInt32(GvPrice.DataKeys[e.RowIndex].Value);
            DataRow dr = PriceDetails.Rows.Find(serial);
            dr.BeginEdit();
            SetPriceDetails(dr, gvRow, "EIT");
            dr.EndEdit();
            GvPrice.EditIndex = -1;
            BindGridPrice();
            hdfPrice.Value = "0";
            btnClear.Focus();

            //int rwCnt = GvPrice.Rows.Count - 1;
            //GvPrice.Rows[rwCnt].Focus();
            //GvPrice.Rows[rwCnt].Cells[0].Focus();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void GvPrice_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow gvRow = GvPrice.Rows[e.RowIndex];
            long serial = Convert.ToInt32(GvPrice.DataKeys[e.RowIndex].Value);
            PriceDetails.Rows.Find(serial).Delete();
            BindGridPrice();
            GvPrice.EditIndex = -1;
            hdfPrice.Value = "0";
            btnClear.Focus();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");  
        }
    }
    protected void gvAFCDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvAFCDetails.EditIndex = -1;
            BindGrid();
            hdfAFCDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void gvAFCDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAFCDetails.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void GvPrice_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            GvPrice.EditIndex = -1;
            BindGridPrice();
            hdfPrice.Value = "0";
            btnClear.Focus();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    protected void GvPrice_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GvPrice.PageIndex = e.NewPageIndex;
            BindGridPrice();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "FixDepMaster page " + ex.Message, "0");
        }
    }
    
    protected void ddlDays_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDays.SelectedValue == "1")
        {
            lblItinerary2.Visible = false;
            txtItinerary2.Visible = false;
        }
        else
        {
            lblItinerary2.Visible = true;
            txtItinerary2.Visible = true;
        }

    }

    private void AuthorizationCheck()
    {
        //if (Settings.LoginInfo == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    Response.Redirect("Login.aspx" + values, true);
        //}

    }
}
