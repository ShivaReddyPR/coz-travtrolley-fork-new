<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"
    Inherits="CriteriaList" Title="Cozmo Travels" MaintainScrollPositionOnPostback="true" Codebehind="VisaCriteriaList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    
    
    <div>  <h4> Visa Criteria List</h4> </div>
    
    
    <div class=" bg_white bor_gray pad_10 paramcon"> 
    
    <div class="col-md-2"> <label class="pull-right fl_xs"> Search by country:</label> </div>
    
    <div class="col-md-2"><asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                    </asp:DropDownList> </div>
    <div class="col-md-2"><label class="pull-right fl_xs">Agent: </label> </div>
    
    <div class="col-md-2"><asp:DropDownList ID="ddlAgent" CssClass="form-control"  runat="server">
                </asp:DropDownList> </div>
                
    <div class="col-md-2 xspadtop10"><asp:Button ID="btnSearch" CssClass="but but_b"  runat="server" Text="Search" OnClick="btnSearch_Click" /> </div>
    
    <div class="col-md-2"><asp:HyperLink ID="HyperLink1" CssClass="fcol_blue" runat="server" NavigateUrl="~/AddVisaCriteria.aspx">Add New Criteria </asp:HyperLink > </div>
       
    
    <div class="clearfix"> </div> 
    </div>
    
    

        
             
            
                <div>
                    <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
                </div>
               
               
               
               
               <div class="table-responsive margin-top-10"> 
               
                <asp:GridView ID="GridView1" CssClass="datagrid" runat="server" AutoGenerateColumns="False" CellPadding="2"
                    Width="100%" DataKeyNames="criteriaId"  GridLines="None" 
                    OnRowDataBound="GridView1_RowDataBound"
                    OnRowCommand="GridView1_RowCommand" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging">
                    <FooterStyle Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="criteriaId" HeaderText="ID" ReadOnly="True" >
                            <HeaderStyle HorizontalAlign="Left"  />
                        </asp:BoundField>
                         <asp:BoundField DataField="agency" HeaderText="Agent" ReadOnly="True">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="criteriaName" HeaderText="Name">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="countryCode" HeaderText="Country Code">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                       
                        <asp:BoundField DataField="isActive" HeaderText="Is Active" ReadOnly="True">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="criteriaId" HeaderText="Edit"
                            DataNavigateUrlFormatString="AddVisaCriteria.aspx?id={0}" >
                            <ControlStyle  />
                        </asp:HyperLinkField>
                        <asp:TemplateField HeaderText="Activate/Deactivate">
                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:LinkButton ID="linkButtonStatus" runat="server" Text="Activate"
                                    CommandName="ChangeStatus">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle HorizontalAlign="Left" />
                    <EditRowStyle />
                    <SelectedRowStyle  Font-Bold="True" />
                    <PagerStyle  HorizontalAlign="Left" Font-Bold="True" />
                    <HeaderStyle  Font-Bold="True"  HorizontalAlign="Left"  />
                       <AlternatingRowStyle CssClass="altrow" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                        NextPageText="Next" PreviousPageText="Previous" />
                </asp:GridView>
               
               </div>
            
            <br />
       
</asp:Content>
