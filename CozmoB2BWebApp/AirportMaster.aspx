﻿<%@ Page Title="Airport Master" EnableEventValidation="false" MasterPageFile="~/TransactionBE.master" Language="C#" AutoEventWireup="true" Inherits="AirportMasterUI" Codebehind="AirportMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript">
        function deleteAlert() {
            alert('Changed Successfully');
        }
        function saveNotification() {
            alert('Saved Successfully');
        }
        function updateNotification() {
            alert('Updated Successfully');
        }
    </script>

    <div class="body_container" style="border: 1px solid black">

        <div class="Col-md-3" style="text-align: center">
            <h3>Airport Master 
            </h3>
        </div>


        <div role="tabpanel" class="tab-pane" id="pnlAirportMaster">
            <div class="row">


                <div class="col-md-12">
                    <asp:UpdatePanel ID="pnlAM" runat="server">
                        <ContentTemplate>
                            <asp:GridView CssClass="table b2b-corp-table" ID="gridViewAM" ShowFooter="true" DataKeyNames="Id"
                                runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" OnRowCancelingEdit="gridViewAM_RowCancelingEdit"
                                OnRowDeleting="gridViewAM_RowDeleting" OnRowEditing="gridViewAM_RowEditing" OnRowUpdating="gridViewAM_RowUpdating"
                                OnRowCommand="gridViewAM_RowCommand" OnRowDataBound="gridViewAM_RowDataBound">
                                <Columns>

                                    <asp:TemplateField HeaderText="Airport Index">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnStatus" Value='<%# Eval("Status")%>' />
                                            <asp:Label ID="lblAirportIndex" runat="server" Text='<%# Eval("AirportIndex")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="5" CssClass="form-control" ID="txtEditAirportIndex" runat="server"
                                                Text='<%# Eval("AirportIndex")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditAirportIndex" runat="server" ControlToValidate="txtEditAirportIndex"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);"  CssClass="form-control" ID="txtAirportIndex" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvAirportIndex" runat="server" ControlToValidate="txtAirportIndex"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Airport Code">
                                        <ItemTemplate>
                                           
                                            <asp:Label ID="lblAirportCode" runat="server" Text='<%# Eval("AirportCode")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="3" CssClass="form-control" ID="txtEditAirportCode" runat="server"
                                                Text='<%# Eval("AirportCode")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditAirportCode" runat="server" ControlToValidate="txtEditAirportCode"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="3"  CssClass="form-control" ID="txtAirportCode" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvAirportCode" runat="server" ControlToValidate="txtAirportCode"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Airport Name">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblAirportName" runat="server" Text='<%# Eval("AirportName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="100" CssClass="form-control" ID="txtEditAirportName" runat="server"
                                                Text='<%# Eval("AirportName")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditAirportName" runat="server" ControlToValidate="txtEditAirportName"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="100"  CssClass="form-control" ID="txtAirportName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvAirportName" runat="server" ControlToValidate="txtAirportName"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Airport Name(Arabic)">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblAirportNameAR" runat="server" Text='<%# Eval("AirportNameAR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="100" CssClass="form-control" ID="txtEditAirportNameAR" runat="server"
                                                Text='<%# Eval("AirportNameAR")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditAirportNameAR" runat="server" ControlToValidate="txtEditAirportNameAR"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="100"  CssClass="form-control" ID="txtAirportNameAR" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvAirportNameAR" runat="server" ControlToValidate="txtAirportNameAR"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="City Code">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblCityCode" runat="server" Text='<%# Eval("CityCode")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="3" CssClass="form-control" ID="txtEditCityCode" runat="server"
                                                Text='<%# Eval("CityCode")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditCityCode" runat="server" ControlToValidate="txtEditCityCode"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="3"  CssClass="form-control" ID="txtCityCode" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCityCode" runat="server" ControlToValidate="txtCityCode"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                     <asp:TemplateField HeaderText="City Name">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblCityName" runat="server" Text='<%# Eval("CityName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="100" CssClass="form-control" ID="txtEditCityName" runat="server"
                                                Text='<%# Eval("CityName")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditCityName" runat="server" ControlToValidate="txtEditCityName"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="100"  CssClass="form-control" ID="txtCityName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCityName" runat="server" ControlToValidate="txtCityName"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="City Name (Arabic)">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblCityNameAR" runat="server" Text='<%# Eval("CityNameAR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="100" CssClass="form-control" ID="txtEditCityNameAR" runat="server"
                                                Text='<%# Eval("CityNameAR")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditCityNameAR" runat="server" ControlToValidate="txtEditCityNameAR"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="100"  CssClass="form-control" ID="txtCityNameAR" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCityNameAR" runat="server" ControlToValidate="txtCityNameAR"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Country Code">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblCountryCode" runat="server" Text='<%# Eval("CountryCode")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="2" CssClass="form-control" ID="txtEditCountryCode" runat="server"
                                                Text='<%# Eval("CountryCode")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditCountryCode" runat="server" ControlToValidate="txtEditCountryCode"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="2"  CssClass="form-control" ID="txtCountryCode" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtCountryCode"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Country Name">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblCountryName" runat="server" Text='<%# Eval("CountryName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="100" CssClass="form-control" ID="txtEditCountryName" runat="server"
                                                Text='<%# Eval("CountryName")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditCountryName" runat="server" ControlToValidate="txtEditCountryName"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="100"  CssClass="form-control" ID="txtCountryName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCountryName" runat="server" ControlToValidate="txtCountryName"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Country Name(Arabic)">
                                        <ItemTemplate>
                   
                                            <asp:Label ID="lblCountryNameAR" runat="server" Text='<%# Eval("CountryNameAR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox MaxLength="100" CssClass="form-control" ID="txtEditCountryNameAR" runat="server"
                                                Text='<%# Eval("CountryNameAR")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditCountryNameAR" runat="server" ControlToValidate="txtEditCountryNameAR"
                                                Text="Required" ValidationGroup="ValidationEditAM" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="100"  CssClass="form-control" ID="txtCountryNameAR" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCountryNameAR" runat="server" ControlToValidate="txtCountryNameAR"
                                                Text="Required" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Actions">
                                        <EditItemTemplate>
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnUpdateAM" runat="server" CommandName="Update" Text="Update"
                                                ValidationGroup="ValidationEditAM" />
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnCancelAM" runat="server" CommandName="Cancel" Text="Cancel" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnEditAM" runat="server" CommandName="Edit" Text="Edit" />
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnDeleteAM" Text="Delete" runat="server" CommandName="Delete" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnAddRM" runat="server"
                                                CommandName="AddNew" Text="Add" ValidationGroup="ValidationAM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gridViewAM" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>





    </div>


</asp:Content>
