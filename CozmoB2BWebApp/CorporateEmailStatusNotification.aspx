<%@ Page Language="C#" AutoEventWireup="true"
    Title="Corporate Email Status Notification" Inherits="CorporateEmailStatusNotification" Codebehind="CorporateEmailStatusNotification.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Corporate Email Status Notification</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <%if (Request.QueryString["status"] != null && Request.QueryString["status"] == "A")  %>
    <%{ %>
    
    <div style="background: #f3f3f3!important; color: #0a0a0a; margin: 10%">
        <h1 style='color: Green; text-align: center'>
            Successfully
            <% =status%></h1>
            
            </div>
            <%} %>
            
         <%if (Request.QueryString["status"] != null && Request.QueryString["status"] != "A")  %>
    <%{ %>   
        <div style="background: #f3f3f3!important; color: #0a0a0a; margin: 10%">
            <table>
                <tr>
                    <td>
                        Reason:
                    </td>
                    <td>
                        <asp:TextBox runat="server" Rows="10" Columns="30"  TextMode="MultiLine" ID="txtReason"></asp:TextBox>
            <asp:RequiredFieldValidator ErrorMessage="Please enter the reason" ValidationGroup="grpReason" runat="server" ControlToValidate="txtReason" ID="rfvReason"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button OnClick="btnReject_Click" ValidationGroup="grpReason" runat="server" ID="btnReject" Text="Reject" />
                    </td>
                </tr>
            </table>
            
            <asp:Label runat="server" ID="lblSuccess" Visible="false"></asp:Label>
            
            
        </div>
     <%} %>
    </form>
</body>
</html>
