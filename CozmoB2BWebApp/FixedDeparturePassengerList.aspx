﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDeparturePassengerListGUI"
    Title="FixedDeparture PassengerList" Codebehind="FixedDeparturePassengerList.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script type="text/javascript">
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right
    function IsAlphaNumeric(e) {
        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
        return ret;
    }

    function validateName(oSrc, args) {
        args.IsValid = (args.Value.length >= 2);
    }


    function Validate() {
        if (getElement('hdnTotalPaxCount').value != getElement('hdnPaxCount').value) {
            document.getElementById('errMessHotel').style.display = "block";
            document.getElementById('errMessHotel').innerHTML = "Pax Count mismatch. Please Add pax chosen earlier.";
            return false;
        }
        return true;
    }
    
    </script>


    <asp:HiddenField ID="hdnAdults" runat="server" Value="" />
    <asp:HiddenField ID="hdnChilds" runat="server" Value="" />
    <asp:HiddenField ID="hdnInfants" runat="server" Value="" />
      <asp:HiddenField ID="hdnTotalPaxCount" runat="server" Value="0" />
        <asp:HiddenField ID="hdnPaxCount" runat="server" Value="0" />
    <div style="position: relative; padding-top: 10px">
        <div class="ns-h3">
            <asp:Label ID="lblName" runat="server" Text=""></asp:Label></div>
            <div class="error_msg" style="display:none;width:100%;text-align:center;" id="errMessHotel"> </div>
            <table id="tblPassenger" runat="server" width="100%" border="0" cellpadding="0" cellspacing="0"></table>
            <div class="ns-h3">Contact Details</div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 190px;">
                        <p>
                            <strong>Name<span style='color: red;'>*</span>:</strong>
                        </p>
                    </td>
                    <td style="width: 230px;">
                        <asp:TextBox ID="txtName" runat="server" Text="" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfName" runat="server"  ControlToValidate="txtName" ErrorMessage="Enter Name" ValidationGroup="pax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 210px;">
                        <p>
                            <strong>Contact number in UAE<span style='color: red;'>*</span>:</strong>
                        </p>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContact" runat="server" Text="" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfContact" runat="server"  ControlToValidate="txtContact" ErrorMessage="Enter Contact number UAE" ValidationGroup="pax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                
                
       <tr>
       <td>
       <p>
                            <strong>Contact number while travelling</span>:</strong>
                        </p>
       </td>
       <td>
       <asp:TextBox ID="txtTravel" runat="server" Text="" Width="150px"></asp:TextBox>
       </td>
         <td>
       <p>
                            <strong>Emergency Number while travelling:</strong>
                        </p>
       </td>
         <td>
       <asp:TextBox ID="txtEmTravel" runat="server" Text="" Width="150px"></asp:TextBox>
       </td>
       </tr>
       <tr>
       <td>
       <p>
                            <strong>How did you get to about us <span style='color: red;'>*</span>:</strong>
                        </p>
       </td>
         <td>
       <asp:TextBox ID="txtAbouts" runat="server" Text="" Width="150px"></asp:TextBox>
       <asp:RequiredFieldValidator ID="rfAbouts" runat="server" SetFocusOnError="true" ValidationGroup="pax" ControlToValidate="txtAbouts" ErrorMessage="Enter Remarks"></asp:RequiredFieldValidator>
       </td>
           <td>
       <p>
                            <strong>Additional info:</strong>
                        </p>
       </td>
         <td>
       <asp:TextBox ID="txtAddinfo" runat="server" Text="" Width="150px"></asp:TextBox>
       </td>
       </tr>
            </table>
        <div class="clear">
        </div>
    </div>
   
  <div class="f_R">
        <asp:LinkButton ID="imgSubmit" Text="Submit" CssClass="button-new" runat="server"
            CausesValidation="true" ValidationGroup="pax" OnClick="imgSubmit_Click" OnClientClick="return Validate();" />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
