﻿using System;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class PackagesUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected HolidayPackage packageList;
    protected List<string> packageCityList = new List<string>();
    protected List<PackageTheme> hotelThemes = new List<PackageTheme>();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (!IsPostBack)
            {
                InitialitionControls();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

    private void InitialitionControls()
    {
        try
        {
            BindPackageCity();
            BindPackageTheme();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindPackageCity()
    {
        try
        {
            HolidayPackage.AgentCurrency = Settings.LoginInfo.Currency;
            HolidayPackage.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            packageCityList = HolidayPackage.LoadPackageCity(Settings.LoginInfo.AgentId);
            //if (packageCityList.Count > 0)
            //{
                ddlPackageCity.DataSource = packageCityList;
                ddlPackageCity.DataBind();
                ddlPackageCity.Items.Insert(0, new ListItem("Select", "0"));
                ddlPackageCity.SelectedIndex = 0;
                //ddlPackageCity.Items.Add("Others");
                //ddlPackageCity.Items.FindByText("Others").Value = "Others";
            //}
        }
        catch(Exception ex)
        {
            throw  ex;
        }
    }

    private void BindPackageTheme()
    {
        try
        {
            hotelThemes = PackageTheme.Load();
            //if (hotelThemes.Count > 0)
            //{
                ddlPackageTheme.DataSource = hotelThemes;
                ddlPackageTheme.DataValueField = "ThemeId";
                ddlPackageTheme.DataTextField = "ThemeName";
                ddlPackageTheme.DataBind();
                ddlPackageTheme.Items.Insert(0, new ListItem("Select", "0"));
                ddlPackageTheme.SelectedIndex = 0;
            //}
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSearchPackage_Click(object sender, EventArgs e)
    {
        try
        {
            packageList = new HolidayPackage();
            if(ddlPackageCity.SelectedItem.Value == "0")
            {
                packageList.City = string.Empty;
            }
            //else if(ddlPackageCity.SelectedItem.Value == "Others")
            //{
            //    packageList.PackageSearchCity = txtSearchCity.Text.Trim();
            //    packageList.City = ddlPackageCity.SelectedItem.Value;
            //}
            else
            {
                packageList.City = ddlPackageCity.SelectedItem.Value;
            }

            packageList.ThemeName = ddlPackageTheme.SelectedItem.Value;
            if (rbSelect.SelectedItem.Value == "A")
            {
                packageList.Classification = string.Empty;
            }
            else
            {
                packageList.Classification = rbSelect.SelectedItem.Value;
            }
            packageList.StartRate=Utility.ToDecimal(txtFrom.Text.Trim());
            packageList.EndRate = Utility.ToDecimal(txtTo.Text.Trim());
            Session["packageList"] = packageList;
            Response.Redirect("StaticPackages.aspx", false);
          
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

}
