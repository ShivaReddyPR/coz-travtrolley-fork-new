﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.AccountingEngine;
using CT.TicketReceipt.Common;

public partial class AdminLeadger :CT.Core.ParentPage// System.Web.UI.Page
{
    protected System.Text.StringBuilder csvLedger = new System.Text.StringBuilder();
    protected List<LedgerTransaction> ledgerAdmin = new List<LedgerTransaction>();
    protected DateTime startDate, endDate;
    //protected int AgentId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.ExportToExcelButton);
                if (!IsPostBack)
                {

                    InitializePageControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void InitializePageControls()
    {
        try
        {
            CheckIn.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            CheckOut.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            BindAgent();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgents.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgents.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgents.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgents.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindLeadger();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindAgent()
    {
        try
        {
            ddlAgents.DataSource = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgents.DataValueField = "agent_id";
            ddlAgents.DataTextField = "agent_name";
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("-- All --", "0"));
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }

    //Binding B2B Agents
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Binding B2B2B Agents
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlAgents_SelectionChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        { }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            hdfParam.Value = "0";

        }
        catch (Exception ex)
        { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            BindLeadger();

        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, " Admin Ledger Exception: " + ex.ToString(), "0");
        }
    }

    private void BindLeadger()
    {
        try
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            if (CheckIn.Text != string.Empty)
            {
                try
                {
                    startDate = Convert.ToDateTime(CheckIn.Text, provider);
                }
                catch { }
            }
            else
            {
                startDate = DateTime.Now;
            }

            if (CheckOut.Text != string.Empty)
            {
                try
                {
                    endDate = Convert.ToDateTime(Convert.ToDateTime(CheckOut.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                catch { }
            }
            else
            {
                endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
            }

            int agent = Utility.ToInteger(ddlAgents.SelectedValue);
            string agentType = string.Empty;
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            #region B2C purpose
            string transType = string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }
            #endregion
            ledgerAdmin = LedgerTransaction.GetAllTransactions(agent, startDate, endDate,agentType,transType,ddlPaymentType.SelectedItem.Value);
        }
        catch
        {
            Exception ex;
        }
    }
    public void ExportToExcelButton_Click(object sender, EventArgs e)
    {
        string csvledger = Session["csvLedger"].ToString();
        string text = string.Empty;
        if (csvledger != null && csvledger != "")
        {
            text = csvledger;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Ledger.csv");
        }

        if (text != null && text.Length > 0)
        {
            Response.AddHeader("Content-Length", text.Length.ToString());
            Response.Write(text);
            Response.End();
        }
    }

    public static DateTime UTCtoISTtimeZoneConverter(string dateTime)
    {
        TimeSpan absoluteOffset = new TimeSpan(00, 00, 00) - new TimeSpan(04, 00, 00);
        absoluteOffset = absoluteOffset.Duration();
        DateTime ISTtime = DateTime.Parse(dateTime) + absoluteOffset;
        return ISTtime;
    }
}
