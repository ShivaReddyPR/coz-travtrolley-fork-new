﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections.Generic;
//Added UD Namespace here
using CT.Core;
using CT.BookingEngine;
using System.Data;
using CT.TicketReceipt.BusinessLayer;

public partial class SightseeingGUI : CT.Core.ParentPage //System.Web.UI.Page
{
    protected SightSeeingReguest requestObj = new SightSeeingReguest();
    protected string cityName = "Enter city name";
    protected string adults = "", childs = "0";
    protected string childAges = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                CheckIn.Text = "DD/MM/YYYY";
                bindCountryList();
                BindAgents();
            }
            if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack)
            {
                if (Page.PreviousPage.Title == "Sightseeing Search Results")
                {
                    requestObj = (SightSeeingReguest)Session["ssReq"];
                    cityName = requestObj.CityName + "," + requestObj.CountryName;
                    CheckIn.Text = requestObj.TourDate.ToString("dd/MM/yyyy");
                    txtNameKey.Text = requestObj.ItemName;
                    if (requestObj.CategoryCodeList.Count > 0)
                    {
                        ddlCategory.SelectedValue = requestObj.CategoryCodeList[0];
                    }
                    if (requestObj.TypeCodeList.Count >= 1)
                    {
                        for (int i = 0; i < requestObj.TypeCodeList.Count; i++)
                        {
                            foreach (ListItem item in chkList.Items)
                            {
                                if (item.Value == requestObj.TypeCodeList[i])
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                    }
                    if (requestObj.NoOfAdults > 0)
                    {
                        adults = requestObj.NoOfAdults.ToString();
                    }
                    if (requestObj.ChildCount > 0)
                    {
                        childs = requestObj.ChildCount.ToString();
                        if (requestObj.ChildrenList.Count > 0)
                        {
                            foreach (int age in requestObj.ChildrenList)
                            {
                                if (childAges.Length > 0)
                                {
                                    childAges += "," + age.ToString();
                                }
                                else
                                {
                                    childAges = age.ToString();
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(requestObj.Nationality))
                    {
                        ddlNationality.SelectedValue = requestObj.Nationality;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Creating req object
        try
        {
            Session["searchResult"] = null;
            Session["SItinerary"] = null;
            Session["totalResultList"] = null;
            Session["filteredResultList"] = null;
            Session["cSessionId"] = null;
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                if(hdnBookingAgent.Value == "Checked" && Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    Settings.LoginInfo.IsOnBehalfOfAgent = true;
                    Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnLocation.Value);
                    AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);
                    Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                }
                else
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                }

            }
            SightSeeingReguest reqObj = new SightSeeingReguest();
            IFormatProvider format = new CultureInfo("en-GB", true);
            string sDate = CheckIn.Text.Trim();
            reqObj.TourDate = DateTime.Parse(sDate, format);
            reqObj.NoOfAdults = Convert.ToInt16(Request["adtRoom-1"]);
            reqObj.ChildCount = Convert.ToInt16(Request["chdRoom-1"]);
            List<int> childInfo = new List<int>();
            if (reqObj.ChildCount > 0)
            {
                string numChild = string.Empty;
                for (int i = 0; i < reqObj.ChildCount; i++)
                {
                    numChild = "ChildBlock-1-ChildAge-" + (i + 1);
                    childInfo.Add(Convert.ToInt16(Request[numChild]));
                }
            }
            reqObj.ChildrenList = childInfo;
            List<string> category = new List<string>();
            if (ddlCategory.SelectedIndex > 0)
            {
                category.Add(ddlCategory.SelectedItem.Value);
            }
            reqObj.CategoryCodeList = category;

            List<string> typeCodeList = new List<string>();
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    typeCodeList.Add(chkList.Items[i].Value);
                }
            }
            reqObj.TypeCodeList = typeCodeList;
            reqObj.CountryName = Request["CountryName"].ToString();
            reqObj.CityName = Request["city"].ToString().Split(',')[0];
            reqObj.DestinationCode = Request["CityCode"].ToString();
            reqObj.Language = "English";
            reqObj.ItemName = txtNameKey.Text.Trim();
            reqObj.NoDays = Convert.ToInt32(ddlPlus.SelectedItem.Value);
            reqObj.Nationality = ddlNationality.SelectedValue.ToString() ;
            Session["ssReq"] = reqObj;
            Response.Redirect("SightseeingResult.aspx", false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingSearch, Severity.High, 0, "SightSearch page " + ex.ToString(), "");
        }

    }

    // binding Param(nationality,residence)
    private void bindCountryList()
    {
        DOTWCountry dotwC = new DOTWCountry();
        Dictionary<string, string> countryList = dotwC.GetAllCountries();

        ddlNationality.DataSource = countryList;
        ddlNationality.DataValueField = "key";
        ddlNationality.DataTextField = "value";
        ddlNationality.DataBind(); 
    }
    //Bind AgentList
    private void BindAgents()
    {
        try
        {
            int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            if (dtAgents != null)
            {
                ddlAgents.DataSource = dtAgents.Select("AGENT_ID NOT ='" + agentId + "'").CopyToDataTable();
                ddlAgents.DataTextField = "AGENT_NAME";
                ddlAgents.DataValueField = "AGENT_ID";
                ddlAgents.DataBind();
                ddlAgents.Items.Insert(0, new ListItem("--Select Agent--", "0"));
                ddlAgents.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
