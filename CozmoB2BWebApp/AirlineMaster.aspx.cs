﻿using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;

public partial class AirlineMaster : CT.Core.ParentPage
{
    private string AIRLINEMASTER_SEARCH_LIST;
    private string AIRLINE_SESSION = "_AirlineMaster";
    private string AIRLINE_UPDATEALL = "_AirlineUpdate";


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    InitializePageControls();
                    lblSuccessMsg.Text = string.Empty;
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }           

        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    private void InitializePageControls()
    {
        try
        {
            string rootFolder = Utility.ToString(Airline.logoDirectory);
            dmAirlineLogo.SavePath = rootFolder + "/";

            string AirlineImg = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");
            AirlineImg = AirlineImg.Replace("-", "");
            dmAirlineLogo.FileName = AirlineImg;
            string AirlineImgPath = Server.MapPath("~/" + rootFolder + "/") + AirlineImg;
            hdfAirlineImg.Value = AirlineImgPath;
            BindGrid();
            Clear();
        }
        catch
        {
            throw;
        }
    }
    private void Clear()
    {
        try
        {
            txtAirlineCode.Text = string.Empty;
            txtAirlineName.Text = string.Empty;
            txtNumericCode.Text = string.Empty;
            chkIsLCC.Checked = false;
            chkTBOAllowed.Checked = false;
            chkUAPIAllowed.Checked = false;
            imgPreview.Style.Add("display", "none");
            lnkView.Text = "";
            hdfCount.Value = "0";
            dmAirlineLogo.Clear();
            btnSave.Text = "Save";
            CurrentObject = null;
        }
        catch { throw; }
    }

    private void selectedItem()
    {
        try
        {
            foreach (System.Data.DataColumn col in UpdateList.Columns) col.ReadOnly = false;
            
            foreach (GridViewRow gvRow in gvAirline.Rows)
            {
                
                CheckBox chkUA = (CheckBox)gvRow.FindControl("ITchkUA");
                CheckBox chkTA = (CheckBox)gvRow.FindControl("ITchkTA");
                HiddenField hdfAirlineCode = (HiddenField)gvRow.FindControl("IThdfAirlineCode");
                
                foreach (DataRow dr in UpdateList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["airlineCode"]) == hdfAirlineCode.Value)
                        {
                            if (chkUA.Checked)
                            {
                                dr["UA"] = "A";
                            }
                            else
                            {
                                dr["UA"] = "R";
                            }
                        }
                        if (Utility.ToString(dr["airlineCode"]) == hdfAirlineCode.Value)
                        {
                            if (chkUA.Checked)
                            {
                                dr["TA"] = "A";
                            }
                            else
                            {
                                dr["TA"] = "R";
                            }
                        }
                        dr.EndEdit();
                    }
                }
            }
        }
        catch { throw; }
    }
    protected void gvAirline_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            selectedItem();
            gvAirline.PageIndex = e.NewPageIndex;
            gvAirline.EditIndex = -1;
            FilterSearchAirline_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvAirline_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void gvAirline_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string strAirlineCode = Utility.ToString(gvAirline.SelectedValue);
            Clear();
            Edit(strAirlineCode);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void BindGrid()
    {
        UpdateList = Airline.GetAllAirlines();
        //Session["AirlineUpdate"] = UpdateList;
        CommonGrid g = new CommonGrid();
        g.BindGrid(gvAirline, UpdateList);
    }
    
    private void EnableControls(bool bflag)
    {
        txtAirlineCode.Enabled = bflag;
        txtAirlineName.Enabled = bflag;
        txtNumericCode.Enabled = bflag;
        chkIsLCC.Enabled = bflag;
        
    }

    private Airline CurrentObject
    {
        get
        {
            return (Airline)Session[AIRLINE_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(AIRLINE_SESSION);
            }
            else
            {
                Session[AIRLINE_SESSION] = value;
            }

        }
    }

    private void Save()
    {
        try
        {
            Airline objAirline;
            if (CurrentObject == null)
            {
                objAirline = new Airline();
            }
            else
            {
                objAirline = CurrentObject;
            }
            string airlinecode = string.Empty;
            objAirline.AirlineName = txtAirlineName.Text;
            objAirline.AirlineCode = txtAirlineCode.Text;
            airlinecode = txtAirlineCode.Text.Trim();
            objAirline.NumericCode = txtNumericCode.Text;

            if (!string.IsNullOrEmpty(dmAirlineLogo.FileExtension))
            {
                string[] Image1 = hdfAirlineImg.Value.Split('.');
                hdfAirlineImg.Value = Image1[0];
                hdfAirlineImg.Value = hdfAirlineImg.Value + dmAirlineLogo.FileExtension;
                RenameImage(airlinecode, hdfAirlineImg.Value);

                dmAirlineLogo.FileName = txtAirlineCode.Text;
                objAirline.LogoFile = dmAirlineLogo.FileName + "" + dmAirlineLogo.FileExtension;
            }
            
            objAirline.CreatedBy = (Int32)Settings.LoginInfo.UserID;
            objAirline.CreatedOn = DateTime.Now;
            objAirline.Commission = (Int32)0;
            objAirline.AgentCommission = (Int32)0;
            objAirline.PLB = (Int32)0;
            objAirline.AgentPLB = (Int32)0;
            objAirline.IsDomestic = (bool)false;
            if(chkIsLCC.Checked)
                objAirline.IsLCC = (bool)true;
            else
                objAirline.IsLCC = (bool)false;

            objAirline.ETicketEligible = (bool)true;
            objAirline.ETicketForManualPrice = (bool)false;
            objAirline.Surcharge = (Int32)0;
            objAirline.ETicketForImportPNR = (bool)true;
            objAirline.TransactionFee = (Int32)0;
            objAirline.IsServiceTaxOnBaseFarePlusYQ = (bool)false;
            objAirline.Remarks = Utility.ToString("");
            objAirline.CommissionType = "RB";
            objAirline.FareRule = Utility.ToString("");
            objAirline.TBOAirAllowed = chkTBOAllowed.Checked;
            objAirline.UapiAllowed = chkUAPIAllowed.Checked;
            objAirline.Save();            
            
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Airline", airlinecode,(CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
        }
        catch
        {
            throw;
        }
    }
    protected void RenameImage(string strAirlinecode, string imagePath)
    {
        try
        {
            string source = imagePath.Substring(0, imagePath.LastIndexOf("\\") + 1);
            string oldFile = Path.GetFileName(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string dFilePath = string.Empty;
            string newfile = strAirlinecode + "" + fileExtension;
            if (System.IO.File.Exists(source + newfile)) System.IO.File.Delete(source + newfile);
            System.IO.File.Copy(source + oldFile, source + newfile);
            if (System.IO.File.Exists(source + oldFile)) System.IO.File.Delete(source + oldFile);

        }
        catch { }
    }


    private void Edit(string strAirLineCode)
    {
       
        try
        {
            Airline objAirline = new Airline();
            objAirline.Load(strAirLineCode);
            CurrentObject = objAirline;
            txtAirlineCode.Text = objAirline.AirlineCode;
            txtAirlineName.Text = objAirline.AirlineName;
            txtNumericCode.Text = objAirline.NumericCode;
            chkIsLCC.Checked = objAirline.IsLCC;
            chkUAPIAllowed.Checked = objAirline.UapiAllowed;
            chkTBOAllowed.Checked = objAirline.TBOAirAllowed;

            if (objAirline.LogoFile  != string.Empty)
            {
                dmAirlineLogo.DefaultImageUrl = "~/images/common/Preview.png";
                lnkView.Visible = true;
                lnkView.Text = "View Image";
                string rootFolder = Airline.logoDirectoryPath;
                hdfImgPath.Value = rootFolder + "\\" + objAirline.LogoFile;
                dmAirlineLogo.FileExtension = null;
            }
        
            btnSave.Text = "Update";
            lblSuccessMsg.Text = "";
            btnClear.Text = "Cancel";

        }
        catch
        {
            throw;
        }
        
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[AIRLINEMASTER_SEARCH_LIST];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["airlineCode"] };
            Session[AIRLINEMASTER_SEARCH_LIST] = value;
        }
    }

    private DataTable UpdateList
    {
        get 
        {
            return (DataTable)Session[AIRLINE_UPDATEALL];
        }
        set
        {
            //if (value == null)
            //{
            //    Session.Remove(AIRLINE_UPDATEALL);
            //}
            value.PrimaryKey = new DataColumn[] { value.Columns["airlineCode"] };
            Session[AIRLINE_UPDATEALL] = value;
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            lblError.Text = string.Empty;
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);

        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            lblSuccessMsg.Text = string.Empty;
            lblError.Text = string.Empty;
            Clear();
            BindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    
    protected void btnUpdateAll_Click(object sender, EventArgs e)
    {
        try
        {
            selectedItem();
            Airline objAirline = new Airline();
            
            foreach (GridViewRow gvRow in gvAirline.Rows)
            {                
                Label lblAirlineCode = (Label)gvRow.FindControl("ITlblAirlineCode");
               
                CheckBox chkUA = (CheckBox)gvRow.FindControl("ITchkUA");
                CheckBox chkTA = (CheckBox)gvRow.FindControl("ITchkTA");
                
                objAirline.Load(lblAirlineCode.Text);
                
                objAirline.UapiAllowed = chkUA.Checked;
                objAirline.TBOAirAllowed = chkTA.Checked;
                objAirline.LastModifiedBy = (int)Settings.LoginInfo.UserID;
               
                objAirline.Save();                
            }

            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Airline", objAirline.AirlineName, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void BindSearch()
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            Airline objAirline = new Airline();
            SearchList = Airline.GetAllAirlinesList();
            grid.BindGrid(gvSearch, SearchList);
        }
        catch
        {
            throw;
        }
    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={{ "HTtxtAirlineCode", "airlineCode" }
                                             ,{"HTtxtAirlineName", "airlineName" } 
                                             ,{"HTtxtIsLCC", "IsLCC" }};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void FilterSearchAirline_Click(object sender,EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={{ "HTtxtAirLineCode", "airlineCode" }
                                             ,{"HTtxtAirLineName","airlineName"}
                                             //,{"HTtxtNumericCode","numericCode"}
                                             ,{"HTtxtIsLCC","isLCC"}};
                                            
            CommonGrid g = new CommonGrid();
            //UpdateList = (DataTable)Session["AirlineUpdate"];
            g.FilterGridView(gvAirline, UpdateList.Copy(), textboxesNColumns);
            //UpdateList = ((DataTable)gvAirline.DataSource).Copy();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string strAirlineCode = Utility.ToString(gvSearch.SelectedValue);
            Clear();
            Edit(strAirlineCode);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }

    protected void gvAirline_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            Image img = e.Row.FindControl("ITlblLogoFile") as Image;
            CheckBox chkUA = e.Row.FindControl("ITchkUA") as CheckBox;
            CheckBox chkTA = e.Row.FindControl("ITchkTA") as CheckBox;

            DataRowView view = e.Row.DataItem as DataRowView;

            if (img != null)
            {
                img.ImageUrl = "images/AirlineLogo/" + view["LogoFile"].ToString();
            }

            chkUA.Checked = (view["UA"].ToString() == "A" ? true : false);
            chkTA.Checked = (view["TA"].ToString() == "A" ? true : false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Bind Airlines. "+ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
            
