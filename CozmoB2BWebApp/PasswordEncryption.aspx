﻿<%@ Page Title="Update Encrypted Passwords" Language="C#" AutoEventWireup="true" CodeBehind="PasswordEncryption.aspx.cs" Inherits="CozmoB2BWebApp.PasswordEncryption" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.5.2/flatly/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery-progress-lgh.js"></script>
    <!-- layui -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/layui/2.5.7/css/layui.min.css" integrity="sha512-+V5QIHvfXMjtomyr134OIvhZfJ0KHWwQc2D0wdVWcYqoe0KMTS0g4UpKN40Kuow4v8ihhehXAo+Hnz4OnIBpbw==" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/layui/2.5.7/layui.min.js" integrity="sha512-FhTbYRtdVlF8dnJwX88FNOfMkl/YYSN3nLcZajWU0ctexrfLh4Ll/pDwY1Qsc2JvDXOHC2a58ae+NvTxudZjAA==" crossorigin="anonymous"></script>

    <style>
        body {
            background-color: #fafafa;
        }

        .container {
            margin: 150px auto;
        }
    </style>
</head>
<body>
    <div class="container">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
            <legend>Password Update Progress</legend>
            <div class="layui-field-box">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="demo"></div>
                </div>
            </div>
        </fieldset>
    </div>
    <script type="text/javascript">

        var percent = 0;

        layui.use(['layer', 'element'], function () {
            const layer = layui.layer;
            const element = layui.element;

            $.ajax({
                type: "POST",
                url: "PasswordEncryption.aspx/GetAllUsers",
                contentType: "application/json; charset=utf-8",
                data: "",
                dataType: "json",
                async: true,
                success: function (data) {
                    if (data.d != undefined) {
                        var users = JSON.parse(data.d);
                        for (var i = 0; i < users.length; i++) {
                            $.ajax({
                                type: "POST",
                                url: "PasswordEncryption.aspx/UpdatePasswords",
                                contentType: "application/json; charset=utf-8",
                                data: "{'userId':'" + users[i].user_id + "','password':'" + users[i].user_password + "'}",
                                dataType: "json",
                                async: true,
                                success: function (data) {
                                    if (data.d != undefined) {
                                        percent += eval(data.d);
                                        console.log(percent);
                                        startProgress('bootstrap', $("#demo"));
                                    }
                                }
                            });
                        }
                    }
                }
            });

            // bootstrap-progress
            



            function startProgress(type, filter, index) {
                // step-1
                let percentage = 0;
                const progress = new Progress({
                    get: function () {
                       
                        percentage = percent + Math.random() * 10 | 0;
                        if (percentage > 100) {
                            percentage = 100;
                        }
                        else {
                            
                        }
                        progress.update(percentage);
                    },
                    set: function (percentage) {
                        switch (type) {
                            case 'bootstrap':
                                filter.css('width', percentage + '%').text(percentage + '%');
                                break;
                            case 'layui':
                                element.progress(filter, percentage + '%');
                                if (index && percentage === 100) {
                                    layer.close(index);
                                }
                                break;
                            default:

                        }
                    }
                });
                // step2 start
                progress.start();
            }
        });

    </script>

</body>
</html>
