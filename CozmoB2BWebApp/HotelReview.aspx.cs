﻿using System;
using System.Collections;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.AccountingEngine;
using CT.Configuration;
using System.Linq;

public partial class HotelReview : CT.Core.ParentPage// System.Web.UI.Page
{
    protected HotelItinerary itinerary = new HotelItinerary();
    protected HotelSearchResult result = new HotelSearchResult();
    protected string errorMessage = string.Empty;
    protected AgentMaster agency;
    protected bool onBehalf = false;
    protected bool isAgent = true;
    protected UserMaster loggedMember;
    /// <summary>
    /// Credit remaining with the agency for which the booking is being made.
    /// </summary>
    protected decimal amountToCompare = 0;
    protected bool isEligible = true;
    protected string lessCredit = string.Empty;
    protected bool otherFare = false;
    protected string searchPage = string.Empty;
    protected decimal rateofExchange = 1;
    protected Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
    protected string symbol = "" + ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
    protected bool isMultiRoom = false;
    protected SortedList countryList;
    protected PriceType priceType;
    protected bool ccError;
    protected bool payerAuth;
    protected decimal agencyBalance = 0;
    protected CT.BookingEngine.HotelSource hSource = new CT.BookingEngine.HotelSource();
    protected bool ccReturned = false;
    protected string mailMessage = string.Empty;
    protected string bookingMessage = string.Empty;
    string sessionId = string.Empty;
    protected int index = 0;
    protected int agencyId = 0;
    protected HotelRequest request = new HotelRequest();
    protected HotelSearchResult[] hotelInfo = new HotelSearchResult[0];
    protected string warningMsg = "";
    protected string cancelData = "", remarks = "";
    protected string discountType = "";
    protected int decimalValue = 2;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    if (Session["cSessionId"] == null || Session["req"] == null)
                    {
                        Response.Redirect("AbandonSession.aspx");
                    }
                    bool IAN3Dverified = Convert.ToBoolean(ConfigurationSystem.IANConfig["IAN3DVerified"]);
                    int memberId;

                    sessionId = Request["sessionId"];

                    memberId = Convert.ToInt32(Settings.LoginInfo.UserID);
                    loggedMember = new UserMaster(memberId);




                    if (Request.QueryString["hCode"] != null)
                    {
                        index = Convert.ToInt32(Request.QueryString["hCode"]);
                        Session["hCode"] = index;
                    }
                    else
                    {
                        index = Convert.ToInt32(Session["hCode"]);
                    }
                    int agencyId = 0;
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        rateOfExList = Settings.LoginInfo.AgentExchangeRates;
                        decimalValue = Settings.LoginInfo.DecimalValue;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                        rateOfExList = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        decimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }

                    //agency = new AgentMaster(agencyId);
                    //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
                    //staticInfo.BaseCurrency = agency.AgentCurrency;
                    //rateOfExList = staticInfo.CurrencyROE;
                    //if (Settings.LoginInfo != null && Settings.LoginInfo.AgentId > 0)
                    //{
                    //    agencyId = Convert.ToInt32(Settings.LoginInfo.AgentId);
                    //}                


                    if (agencyId == 0)
                    {
                        errorMessage = "Your Session is expired !! Please" + "<a href=\"HotelSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                        return;
                    }
                    //agency = new AgentMaster(agencyId);
                    sessionId = Session["cSessionId"].ToString();
                    //Dictionary<string, string> filterCriteria = (Dictionary<string, string>)Session["hFilterCriteria"];
                    //HotelSearchResult hData = new HotelSearchResult();
                    //int noOfPages = 1;
                    //hotelInfo = hData.GetFilteredResult(sessionId, filterCriteria, ref noOfPages);
                    if (Session["cSessionId"] == null)
                    {
                        errorMessage = "Your Session is expired !! Please" + "<a href=\"HotelSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                        Response.Redirect("HotelSearch.aspx?source=Hotel");
                    }
                    else
                    {
                        request = (HotelRequest)Session["req"];
                        //in case of IAN There is no way to select different room types for multi room, its like Desiya
                        isMultiRoom = (result.BookingSource == HotelBookingSource.IAN) ? false : request.IsMultiRoom;
                        Session["isMultiRoom"] = isMultiRoom;

                        LoadHotelItinerary();
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Display Hotel Review Details. Error: " + ex.Message, "0");
        }
    }

    /// <summary>
    /// Code for downloading image from DOTW xml
    /// </summary>
    /// <param name="imageUrl"></param>
    /// <returns></returns>
    public System.Drawing.Image DownloadImageFromUrl(string imageUrl)
    {
        System.Drawing.Image image = null;

        try
        {
            System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
            webRequest.AllowWriteStreamBuffering = true;
            webRequest.Timeout = 30000;

            System.Net.WebResponse webResponse = webRequest.GetResponse();

            System.IO.Stream stream = webResponse.GetResponseStream();

            image = System.Drawing.Image.FromStream(stream);

            webResponse.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return image;
    }

    /// <summary>
    /// 
    /// </summary>
    void LoadHotelItinerary()
    {

        if (Session["cSessionId"] == null || Session["req"] == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        else
        {

            itinerary.PassengerCountryOfResidence = request.PassengerCountryOfResidence;
            itinerary.PassengerNationality = request.PassengerNationality;
            result = Session["SelectedHotel"] as HotelSearchResult;
            if (result.BookingSource == HotelBookingSource.IAN)
            {
                result.RoomDetails = (HotelRoomsDetails[])Session["RoomAvailability"];
            }
            itinerary.AgencyId = agencyId;
            itinerary.EndDate = result.EndDate;
            itinerary.StartDate = result.StartDate;
            itinerary.HotelCode = result.HotelCode;
            itinerary.HotelAddress1 = result.HotelAddress;
            itinerary.HotelAddress2 = result.HotelContactNo;
            itinerary.HotelName = result.HotelName;
            itinerary.NoOfRooms = request.NoOfRooms;
            itinerary.Rating = result.Rating;
            itinerary.CityRef = request.CityName;
            itinerary.Map = result.HotelMap;
            itinerary.CityCode = result.CityCode;
            itinerary.HotelPolicyDetails = string.Empty;
            itinerary.VoucherStatus = true; //always set to true, otherwise View Invoice button on booking queue will not be displayed
            /////////////////// PropertyType Used For TBO Hotel to send while booking ahotel
            //added on 02032016
            //Agoda Also We are Using This properity 
            itinerary.PropertyType = result.PropertyType; //PropertyType store value of Hotel alert message for miki
            //update request city code for the selected booking source
            request.CityCode = result.CityCode;
            itinerary.Source = result.BookingSource;
            //////////////////////////////////////////////////////////////////
            //    Used for LOH only to send while booking a Hotel.
            itinerary.SequenceNumber = result.SequenceNumber;
            //////////////////////////////////////////////////////////////////
            itinerary.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
            //if (Settings.LoginInfo.AgentId == (int)Session["BookingAgencyID"])
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                itinerary.AgencyId = Settings.LoginInfo.AgentId;
            }
            else
            {
                itinerary.AgencyId = Settings.LoginInfo.OnBehalfAgentID;  // on behalf of booking passing location id as -2
                itinerary.LocationId = Settings.LoginInfo.OnBehalfAgentLocation;
            }

            itinerary.IsDomestic = false;
            //if (result.Currency == "" + ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            //{
            //    itinerary.IsDomestic = true;
            //}
            //else
            //{
            //    itinerary.IsDomestic = false;
            //}
            //Get Hotel source commission type                
            hSource.Load(result.BookingSource.ToString());

            //since there are two enums of similar type where one is in BookingEngine(PriceAccounts) & other in CoreLogic(Enumerator).
            //if (hSource.FareTypeId == CT.Core.FareType.Net)
            {
                priceType = PriceType.NetFare;// always net fare
            }
            //else
            //{
            //    priceType = PriceType.PublishedFare;
            //}
            /****************************************************************************
             * LotsOfHotels require to sent the price in the same currency returned in  *
             * the XML initially in the search response, otherwise booking will fail.   *
             * **************************************************************************/

            //setting room info into itinerary
            HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
            if (isMultiRoom && itinerary.Source != HotelBookingSource.Desiya)
            {
                #region block for multiple room booking
                discountType = Request.QueryString["dt"];
                roomInfo = LoadMultiRoomDetails();
                #endregion

            }
            else
            {
                #region block for single room booking
                discountType = Session["DiscType"].ToString();
                roomInfo = LoadRoomDetails();
                #endregion
            }

            itinerary.Roomtype = roomInfo;

            if (result.BookingSource == HotelBookingSource.LOH || result.BookingSource == HotelBookingSource.EET || result.BookingSource == HotelBookingSource.HotelConnect)
            {
                itinerary.Roomtype[0].Price.SupplierCurrency = result.Price.SupplierCurrency;
                //if (hotelInfo[index].Price.SupplierPrice == 0)
                {
                    foreach (HotelRoom room in itinerary.Roomtype)
                    {
                        itinerary.TotalPrice += room.Price.SupplierPrice;
                    }
                }
                //else
                //{
                //    itinerary.TotalPrice = result.Price.SupplierPrice;
                //}
            }
            else
            {
                itinerary.TotalPrice = Math.Round(result.SellingFare, 2, MidpointRounding.AwayFromZero);
            }
            if (result.BookingSource == HotelBookingSource.GRN)
            {
                itinerary.SupplierType = result.SupplierType;//For checking either bundeled rates/Non Bundeled rate Rooms
            }
            foreach (HotelRoom room in itinerary.Roomtype)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    room.Price.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    room.Price.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                room.Price.Discount = 0;//After discussing with vinay and Ziyad ,Added by harish for disable the discount in Normal flow
            }
            itinerary.Currency = result.Currency;
            //CALL MSE METHODS TO GET CANCELLATION DETAILS & HOTEL DETAILS

            try
            {
                LoadCancellationDetails(ref itinerary);

                if (itinerary.Source != HotelBookingSource.HotelConnect)
                {
                    foreach (HotelRoom room in itinerary.Roomtype)
                    {
                        if (room.NonRefundable || room.PaymentMode == "CC" || room.RatePlanCode == "")
                        {
                            warningMsg = "This Hotel cannot be booked. Please search again to book another Hotel.";
                            hdnWarning.Value = "This Hotel cannot be booked. Please search again to book another Hotel.";
                            imgContinue.Text = "Search";
                        }
                    }
                }

                LoadHotelDetails(ref itinerary);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            // for hotel check-in check-out information - End
            Session["hItinerary"] = itinerary;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hotelInfo"></param>
    /// <returns></returns>
    protected HotelRoom[] LoadMultiRoomDetails()
    {
        HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
        int z = 0;//index for each hotel room
        try
        {
            string[] rCodes = null;//Request["rCode"].Split(',');

            if (Request.QueryString["rCode"] != null)
            {
                if (itinerary.Source != HotelBookingSource.HotelBeds)
                {
                    rCodes = Request.QueryString["rCode"].Split(',');
                    Session["rCode"] = Request.QueryString["rCode"];
                }
                else
                {
                    rCodes = Request.QueryString["rCode"].Replace(" ", "+").Split(',');
                    Session["rCode"] = Request.QueryString["rCode"].Replace(" ", "+");
                }
            }
            else
            {
                rCodes = Session["rCode"].ToString().Split(',');
            }

            for (int iR = 0; iR < request.NoOfRooms; iR++)
            {
                for (int j = 0; j < result.RoomDetails.Length; j++)
                {
                    //foreach (string rCode in rCodes)
                    string rCode = "";

                    if (request.NoOfRooms > rCodes.Length)
                    {
                        rCode = rCodes[0];
                    }
                    else
                    {
                        rCode = rCodes[iR];
                    }

                    if (result.BookingSource == HotelBookingSource.DOTW || result.BookingSource == HotelBookingSource.HotelBeds)   //Added by brahmam 26.09.2014
                    {
                        if (result.RoomDetails[j].RoomTypeCode.Equals(rCode))
                        {
                            HotelRoom roomdata = new HotelRoom();
                            if (itinerary.Source == HotelBookingSource.GTA)
                            {
                                string[] roomList = result.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomTypeCode = roomList[0];
                                roomdata.NoOfCots = Convert.ToInt16(roomList[1]);
                                roomdata.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                            }
                            else if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                string[] roomList = result.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomId = Convert.ToInt32(roomList[0]);
                                roomdata.RoomTypeCode = roomList[1];
                            }
                            else
                            {
                                roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            roomdata.ExtraGuestCharge = result.RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = result.RoomDetails[j].ChildCharges;
                            if (itinerary.Source == HotelBookingSource.DOTW)
                            {
                                roomdata.CancelRestricted = result.RoomDetails[j].CancelRestricted;
                                roomdata.EssentialInformation = result.RoomDetails[j].EssentialInformation;
                            }
                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[j].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[j].Rates[k].Days;
                                if (itinerary.Source != HotelBookingSource.RezLive)
                                {
                                    fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No Need to call GetPrice method again

                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                {
                                    grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                    totalExtraGuestCharge += result.RoomDetails[m].SellExtraGuestCharges;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    if (result.RoomDetails[m].supplierPrice > 0)
                                    {
                                        roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    }
                                    else
                                    {
                                        roomdata.Price.SupplierPrice = result.RoomDetails[m].TotalPrice / result.Price.RateOfExchange;
                                    }
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                }

                                totalChildCharge = result.RoomDetails[m].ChildCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;

                            }
                            roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                            roomdata.Price.Tax = totalTax;

                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;

                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                    else if (itinerary.Source == HotelBookingSource.GTA)
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                            price = result.RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            {
                                roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            }
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            roomdata.EssentialInformation = result.RoomDetails[j].EssentialInformation;
                            roomdata.NoOfCots = result.RoomDetails[j].NumberOfCots;
                            roomdata.NoOfExtraBed = result.RoomDetails[j].MaxExtraBeds;
                            roomdata.ExtraBed = result.RoomDetails[j].IsExtraBed;
                            roomdata.SharingBed = result.RoomDetails[j].SharingBedding;
                            itinerary.HotelCancelPolicy = result.RoomDetails[j].CancellationPolicy;
                            roomdata.ExtraGuestCharge = result.RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = result.RoomDetails[j].ChildCharges;
                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;
                                    if (result.BookingSource == HotelBookingSource.RezLive)
                                    {
                                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                                        {
                                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                                            fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / names.Length;
                                        }
                                        else
                                        {
                                            fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                        }
                                    }
                                    {
                                        fare.RoomPrice = fare.RoomPrice;
                                    }
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    grossFare += result.RoomDetails[m].SellingFare;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                totalChildCharge = result.RoomDetails[m].ChildCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            {
                                roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                            }
                            roomdata.Price.Tax = totalTax;
                            //Not required
                            //decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                            //roomdata.Price.PublishedFare += agentComm;
                            //roomdata.Price.AgentCommission = agentComm;

                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                    // itinerary.Source == HotelBookingSource.Yatra
                    //Above condition Added by somasekhar on 03/09/2018   
                    // itinerary.Source == HotelBookingSource.OYO
                    //below condition Added by somasekhar on 14/12/2018
                    else if (itinerary.Source == HotelBookingSource.Miki || itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.OYO)
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                            price = result.RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            {
                                roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            }
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                            {
                                itinerary.HotelCancelPolicy = result.RoomDetails[j].CancellationPolicy;
                            }
                            else
                            {
                                itinerary.HotelCancelPolicy += "|" + result.RoomDetails[j].CancellationPolicy;
                            }
                            roomdata.RoomIndex = result.RoomDetails[j].RoomIndex;
                            roomdata.SmokingPreference = result.RoomDetails[j].SmokingPreference;
                            roomdata.Supplements = result.RoomDetails[j].Supplements;
                            if (itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.OYO)
                            {
                                roomdata.EssentialInformation = result.RoomDetails[j].EssentialInformation;
                                roomdata.SurChargeList = result.RoomDetails[j].SurChargeList;
                            }
                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;

                                    {
                                        fare.RoomPrice = fare.RoomPrice;
                                    }
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    grossFare += result.RoomDetails[m].SellingFare;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    // itinerary.Source == HotelBookingSource.Yatra
                                    //Above condition Added by somasekhar on 03/09/2018   
                                    if (itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra)
                                    {
                                        roomdata.TBOPrice = result.RoomDetails[m].TBOPrice;
                                    }
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                //totalChildCharge = result.RoomDetails[m].ChildCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            {
                                roomdata.Price.NetFare = grossFare + totalExtraGuestCharge;
                            }
                            roomdata.Price.Tax = totalTax;
                            //Not required
                            //decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                            //roomdata.Price.PublishedFare += agentComm;
                            //roomdata.Price.AgentCommission = agentComm;

                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;

                            break;
                        }
                    }

                    else if (itinerary.Source == HotelBookingSource.RezLive)  //Added by brahmam   19/09/2015
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        //if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        //{
                        //    string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                        //    price = result.RoomDetails[j].TotalPrice / names.Length;
                        //}
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;

                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            itinerary.HotelCancelPolicy = result.RoomDetails[j].CancellationPolicy;
                            roomdata.ExtraGuestCharge = result.RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = result.RoomDetails[j].ChildCharges;
                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;
                                    fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                    //if (result.BookingSource == HotelBookingSource.RezLive)
                                    //{
                                    //    if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                                    //    {
                                    //        string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                                    //        fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / names.Length;
                                    //    }
                                    //    else
                                    //    {
                                    //        fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                    //    }
                                    //}
                                    fare.RoomPrice = fare.RoomPrice;
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;
                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    grossFare += result.RoomDetails[m].SellingFare;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                totalChildCharge = result.RoomDetails[m].ChildCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            {
                                roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                            }
                            roomdata.Price.Tax = totalTax;
                            //Not required
                            //decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                            //roomdata.Price.PublishedFare += agentComm;
                            //roomdata.Price.AgentCommission = agentComm;

                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                    else if (itinerary.Source == HotelBookingSource.HotelConnect)
                    {
                        decimal price = 0;
                        //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
                        //staticInfo.BaseCurrency = agency.AgentCurrency;
                        //Dictionary<string, decimal> rateOfExchange = staticInfo.CurrencyROE;
                        //if (result.Currency == "USD")
                        //{
                        //    price = result.RoomDetails[j].SellingFare * (decimal)Settings.LoginInfo.AgentExchangeRates[result.Currency];
                        //}
                        //else
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                            price = result.RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);


                        if (result.RoomDetails[j].RoomTypeName != null && result.RoomDetails[j].RoomTypeCode == rCode)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;

                            //if (result.RoomDetails[j].RoomTypeName.Split('|').Length > 0)
                            //{
                            //    roomdata.RoomName = result.RoomDetails[j].RoomTypeName.Split('|')[iR];
                            //}
                            //else

                            roomdata.RoomName = result.RoomDetails[j].RoomTypeName;


                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            roomdata.ExtraGuestCharge = result.RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = result.RoomDetails[j].ChildCharges;

                            if (result.RoomDetails[j].Occupancy != null && result.RoomDetails[j].Occupancy.ContainsKey("ExtraBed") && result.RoomDetails[j].Occupancy["ExtraBed"] == 1)
                            {
                                roomdata.RoomName = roomdata.RoomName + " With Extra Bed";
                                roomdata.ExtraBed = true;
                            }
                            HotelRoomFareBreakDown[] fareInfo;

                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;

                                    fare.RoomPrice = result.RoomDetails[j].TotalPrice;
                                    //if (result.Currency != "AED")
                                    //{
                                    //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                    //}
                                    //else
                                    {
                                        fare.RoomPrice = fare.RoomPrice;
                                    }
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {

                                    grossFare += result.RoomDetails[m].TotalPrice - result.RoomDetails[m].Markup;

                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    roomdata.Price.SupplierCurrency = (result.Price != null && result.Price.SupplierCurrency != null ? result.Price.SupplierCurrency : Settings.LoginInfo.Currency);
                                    roomdata.Price.RateOfExchange = (result.Price != null ? result.Price.RateOfExchange : 1);
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                totalChildCharge = result.RoomDetails[m].ChildCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            //if (result.Currency != "AED")
                            //{
                            //    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * rateOfExList[result.Currency];
                            //}
                            //else
                            {
                                roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge + totalTax;
                                //roomdata.Price.Markup = result.Price.Markup;
                            }
                            roomdata.Price.Tax = totalTax;
                            if (roomdata.Price.SupplierPrice == 0)
                            {
                                roomdata.Price.SupplierPrice = (roomdata.Price.NetFare) / roomdata.Price.RateOfExchange;
                            }
                            //roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            //Not Required to save
                            //decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * 1 / 100;
                            //roomdata.Price.PublishedFare += agentComm;
                            //roomdata.Price.AgentCommission = agentComm;

                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            //if (rateOfExList.ContainsKey(result.Currency))
                            //{
                            //    roomdata.Price.RateOfExchange = rateOfExList[result.Currency];
                            //}
                            //else
                            //{
                            //    roomdata.Price.RateOfExchange = 1;
                            //}
                            //roomdata.Price.Markup = result.Price.Markup;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                    //Loading itinerary added by brahmam
                    else if (itinerary.Source == HotelBookingSource.WST || itinerary.Source == HotelBookingSource.JAC)
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            {
                                roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            }
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;

                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;
                                    {
                                        fare.RoomPrice = fare.RoomPrice;
                                    }
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    grossFare += result.RoomDetails[m].SellingFare;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    if (itinerary.Source == HotelBookingSource.JAC) //Added by brahmam 20.09.2016
                                    {
                                        roomdata.Price.PublishedFare = result.RoomDetails[m].TBOPrice.PublishedFare;  // Jac publishfare storing in TBOPrice.PublishedFare Properity
                                        roomdata.EssentialInformation = result.RoomDetails[m].EssentialInformation;
                                    }
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            {
                                roomdata.Price.NetFare = grossFare + totalExtraGuestCharge;
                            }
                            roomdata.Price.Tax = totalTax;
                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;

                            break;
                        }
                    }
                    else if (itinerary.Source == HotelBookingSource.TBOHotel)
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                            price = result.RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                            {
                                itinerary.HotelCancelPolicy = result.RoomDetails[j].CancellationPolicy;
                            }
                            else
                            {
                                string[] splitter = { "@#" };
                                itinerary.HotelCancelPolicy = itinerary.HotelCancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0] + "|" + result.RoomDetails[j].CancellationPolicy;
                            }
                            roomdata.Supplements = result.RoomDetails[j].Supplements;
                            roomdata.SmokingPreference = result.RoomDetails[j].SmokingPreference;
                            roomdata.RoomIndex = result.RoomDetails[j].RoomIndex;
                            roomdata.TBOPrice = result.RoomDetails[j].TBOPrice;
                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;
                                    fare.RoomPrice = fare.RoomPrice;
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;//totalTax = 0,
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    grossFare += result.RoomDetails[m].SellingFare;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.PublishedFare = ((result.RoomDetails[m].TBOPrice.PublishedFare) * result.Price.RateOfExchange);
                                    roomdata.Price.Tax = ((result.RoomDetails[m].TBOPrice.Tax) * result.Price.RateOfExchange);
                                    roomdata.Price.SeviceTax = ((result.RoomDetails[m].TBOPrice.SeviceTax) * result.Price.RateOfExchange);
                                    roomdata.Price.AgentCommission = result.RoomDetails[m].TBOPrice.AgentCommission;
                                    roomdata.Price.TdsCommission = result.RoomDetails[m].TBOPrice.TdsCommission;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                totalChildCharge = result.RoomDetails[m].ChildCharges;
                                //totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }

                    else if (itinerary.Source == HotelBookingSource.GRN)
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                            price = result.RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            {
                                roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            }
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                            {
                                itinerary.HotelCancelPolicy = result.RoomDetails[j].CancellationPolicy;
                            }
                            else
                            {
                                itinerary.HotelCancelPolicy += "|" + result.RoomDetails[j].CancellationPolicy;
                            }
                            roomdata.RoomIndex = result.RoomDetails[j].RoomIndex;
                            roomdata.SmokingPreference = result.RoomDetails[j].SmokingPreference;
                            roomdata.Supplements = result.RoomDetails[j].Supplements;
                            roomdata.EssentialInformation = result.RoomDetails[j].EssentialInformation;
                            roomdata.BedTypeCode = result.RoomDetails[j].BedTypeCode;
                            HotelRoomFareBreakDown[] fareInfo;
                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;

                                    {
                                        fare.RoomPrice = fare.RoomPrice;
                                    }
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    grossFare += result.RoomDetails[m].SellingFare;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;

                                }
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            {
                                roomdata.Price.NetFare = grossFare;
                            }
                            roomdata.Price.Tax = totalTax;


                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;

                            break;
                        }
                    }

                    else
                    {
                        if (itinerary.Source != HotelBookingSource.HotelConnect)
                        {
                            j = Convert.ToInt32(rCode);
                        }
                        decimal price = 0;
                        //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
                        //staticInfo.BaseCurrency = agency.AgentCurrency;
                        //Dictionary<string, decimal> rateOfExchange = staticInfo.CurrencyROE;
                        //if (result.Currency == "USD")
                        // {
                        //     price = result.RoomDetails[j].SellingFare * (decimal)Settings.LoginInfo.AgentExchangeRates[result.Currency];
                        // }
                        // else
                        {
                            price = result.RoomDetails[j].SellingFare;
                        }
                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                            price = result.RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);
                        if (itinerary.Source != HotelBookingSource.HotelConnect)
                        {
                            rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        }
                        //For RezLive update based on room index


                        if (result.RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();


                            roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;


                            //if (result.RoomDetails[j].RoomTypeName.Split('|').Length > 0)
                            //{
                            //    roomdata.RoomName = result.RoomDetails[j].RoomTypeName.Split('|')[iR];
                            //}
                            //else
                            {
                                roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            }


                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            roomdata.ExtraGuestCharge = result.RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = result.RoomDetails[j].ChildCharges;
                            //roomdata.ChildAge = request.RoomGuest[j].childAge;
                            //roomdata.ChildCount = request.RoomGuest[j].noOfChild;
                            HotelRoomFareBreakDown[] fareInfo;


                            fareInfo = new HotelRoomFareBreakDown[1];
                            for (int k = 0; k < 1; k++)
                            {
                                if (k == 0)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = result.RoomDetails[j].Rates[k].Days;
                                    if (result.BookingSource == HotelBookingSource.RezLive)
                                    {
                                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                                        {
                                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                                            fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / names.Length;
                                        }
                                        else
                                        {
                                            fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                        }
                                    }
                                    else if (result.BookingSource == HotelBookingSource.LOH || result.BookingSource == HotelBookingSource.EET)
                                    {
                                        fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare;
                                    }
                                    else
                                    {
                                        fare.RoomPrice = result.RoomDetails[j].TotalPrice;
                                    }
                                    //if (result.Currency != "AED")
                                    //{
                                    //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                    //}
                                    //else
                                    {
                                        fare.RoomPrice = fare.RoomPrice;
                                    }
                                    fareInfo[k] = fare;
                                }
                            }
                            roomdata.RoomFareBreakDown = fareInfo;

                            //room guest for each room
                            roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                            roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                            roomdata.ChildAge = request.RoomGuest[iR].childAge;

                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = j; m < result.RoomDetails.Length; m++)
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    if (result.BookingSource == HotelBookingSource.RezLive)
                                    {
                                        if (result.RoomDetails[j].RoomTypeName.Contains("|"))
                                        {
                                            string[] names = result.RoomDetails[j].RoomTypeName.Split('|');
                                            grossFare += result.RoomDetails[m].Rates[k].SellingFare / names.Length;
                                        }
                                        else
                                        {
                                            grossFare += result.RoomDetails[m].Rates[k].SellingFare / request.NoOfRooms;
                                        }
                                    }
                                    else if (result.BookingSource == HotelBookingSource.LOH || result.BookingSource == HotelBookingSource.EET)
                                    {
                                        grossFare += result.RoomDetails[m].SellingFare;
                                        roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                        roomdata.Price.PublishedFare = result.RoomDetails[m].supplierPrice * rateOfExList["USD"];
                                    }
                                    else if (result.BookingSource == HotelBookingSource.HotelConnect)
                                    {
                                        grossFare += result.RoomDetails[m].SellingFare;
                                    }
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = (result.Price != null && result.Price.SupplierCurrency != null ? result.Price.SupplierCurrency : Settings.LoginInfo.Currency);
                                    roomdata.Price.RateOfExchange = (result.Price != null ? result.Price.RateOfExchange : 1);
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                }
                                totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                totalChildCharge = result.RoomDetails[m].ChildCharges;
                                totalTax = result.RoomDetails[m].TotalTax;
                                break;
                            }
                            //if (result.Currency != "AED")
                            //{
                            //    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * rateOfExList[result.Currency];
                            //}
                            //else
                            {
                                roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                                //roomdata.Price.Markup = result.Price.Markup;
                            }
                            roomdata.Price.Tax = totalTax;
                            //Not required
                            //if (itinerary.Source != HotelBookingSource.HotelConnect)
                            //{
                            //    //roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            //    decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                            //    roomdata.Price.PublishedFare += agentComm;
                            //    roomdata.Price.AgentCommission = agentComm;
                            //}
                            roomdata.Price.AccPriceType = priceType;

                            roomdata.Price.CurrencyCode = result.Currency;
                            //if (rateOfExList.ContainsKey(result.Currency))
                            //{
                            //    roomdata.Price.RateOfExchange = rateOfExList[result.Currency];
                            //}
                            //else
                            //{
                            //    roomdata.Price.RateOfExchange = 1;
                            //}
                            //roomdata.Price.Markup = result.Price.Markup;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get MultiRoom Details. Error: " + ex.Message, "0");
        }
        if (itinerary.Source == HotelBookingSource.GRN)
        {
            roomInfo = roomInfo.OrderBy(o => o.RoomTypeCode.Split('|')[0]).ToArray();
        }
        return roomInfo;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hotelInfo"></param>
    /// <returns></returns>
    protected HotelRoom[] LoadRoomDetails()
    {
        HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
        string roomCode = string.Empty;


        try
        {

            if (itinerary.Source == HotelBookingSource.IAN)
            {
                roomCode = Request["rCode" + index].ToString();
                roomCode = roomCode.Replace("\r\n", string.Empty).Trim();
                itinerary.PropertyType = result.PropertyType;
                itinerary.SupplierType = result.SupplierType;
            }
            else
            {
                roomCode = Session["rCode"].ToString();//Request["rCode"].ToString();
            }
            bool rpValidate = true;
            for (int j = 0; j < result.RoomDetails.Length; j++)
            {
                rpValidate = true;
                if (result.BookingSource == HotelBookingSource.Desiya && !result.RoomDetails[j].RatePlanCode.Equals(Session["rpCode"].ToString()))
                {
                    rpValidate = false;
                }
                if (result.BookingSource == HotelBookingSource.RezLive)//Added by brahmam   19/09/2015
                {
                    int rc = Convert.ToInt32(roomCode);
                    if (result.RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            roomdata.RoomTypeCode = result.RoomDetails[rc].RoomTypeCode;
                            roomdata.RoomName = result.RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[rc].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[rc].mealPlanDesc;
                            itinerary.HotelCancelPolicy = result.RoomDetails[rc].CancellationPolicy;
                            roomdata.ExtraGuestCharge = result.RoomDetails[rc].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = result.RoomDetails[rc].ChildCharges / request.NoOfRooms;
                            roomdata.Supplements = result.RoomDetails[j].Supplements;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[rc].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[rc].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare * request.NoOfRooms;
                                }
                                else
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare / request.NoOfRooms;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = 0; m < result.RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = result.RoomDetails[m].ChildCharges;
                                    totalTax = result.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    break;
                                }
                            }
                            roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else if (result.BookingSource == HotelBookingSource.LOH || result.BookingSource == HotelBookingSource.EET)
                {
                    int rc = Convert.ToInt32(roomCode);
                    if (result.RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {

                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();

                            {
                                roomdata.RoomTypeCode = result.RoomDetails[rc].RoomTypeCode;
                            }
                            roomdata.RoomName = result.RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[rc].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[rc].mealPlanDesc;
                            roomdata.ExtraGuestCharge = result.RoomDetails[rc].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = result.RoomDetails[rc].ChildCharges / request.NoOfRooms;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[rc].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[rc].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare * request.NoOfRooms;
                                    //if (result.Currency != "AED")
                                    //{
                                    //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                    //}
                                }
                                else
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare / request.NoOfRooms;
                                    //if (result.Currency != "AED")
                                    //{
                                    //    fare.RoomPrice = fare.RoomPrice * rateOfExList[result.Currency];
                                    //}
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = 0; m < result.RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                    grossFare = result.RoomDetails[m].SellingFare;
                                    //for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                    //{
                                    //   // grossFare = result.RoomDetails[m].SellingFare;
                                    //}
                                    totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = result.RoomDetails[m].ChildCharges;
                                    totalTax = result.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.PublishedFare = result.RoomDetails[m].supplierPrice * rateOfExList["USD"];
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    break;
                                }
                            }
                            //if (result.Currency != "AED")
                            //{
                            //    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms * rateOfExList[result.Currency];
                            //}
                            //else
                            {
                                roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                                //roomdata.Price.Markup = result.Price.Markup;
                            }


                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                                roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            }

                            //incase of fixed type commission it is giving mark up for all room at once in accounting engine to do change.
                            if (hSource.CommissionTypeId == CT.Core.CommissionType.Fixed)
                            {
                                //roomdata.Price.Markup = roomdata.Price.Markup * request.NoOfRooms;// commented by shiva
                            }
                            roomdata.Price.AccPriceType = priceType;
                            //if (itinerary.Source == HotelBookingSource.Desiya)
                            //{
                            //    roomdata.Price.RateOfExchange = 1;
                            //    roomdata.Price.CurrencyCode = "" + ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
                            //}
                            //else
                            //{
                            //    roomdata.Price.CurrencyCode = result.Currency;
                            //    if (rateOfExList.ContainsKey(result.Currency))
                            //    {
                            //        roomdata.Price.RateOfExchange = rateOfExList[result.Currency];
                            //        roomdata.Price.RateOfExchange = (roomdata.Price.RateOfExchange <= 0 ? 1 : roomdata.Price.RateOfExchange);
                            //    }
                            //    else
                            //    {
                            //        roomdata.Price.RateOfExchange = 1;
                            //    }
                            //}
                            //roomdata.Price.Markup = result.Price.Markup;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else if (itinerary.Source == HotelBookingSource.GTA)
                {
                    int rc = Convert.ToInt32(roomCode);
                    if (result.RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            {
                                roomdata.RoomTypeCode = result.RoomDetails[rc].RoomTypeCode;
                            }
                            roomdata.RoomName = result.RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[rc].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[rc].mealPlanDesc;
                            roomdata.EssentialInformation = result.RoomDetails[rc].EssentialInformation;
                            roomdata.NoOfCots = result.RoomDetails[rc].NumberOfCots;
                            roomdata.NoOfExtraBed = result.RoomDetails[rc].MaxExtraBeds;
                            roomdata.ExtraBed = result.RoomDetails[rc].IsExtraBed;
                            roomdata.SharingBed = result.RoomDetails[rc].SharingBedding;
                            itinerary.HotelCancelPolicy = result.RoomDetails[rc].CancellationPolicy;
                            roomdata.ExtraGuestCharge = result.RoomDetails[rc].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = result.RoomDetails[rc].ChildCharges / request.NoOfRooms;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[rc].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[rc].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare * request.NoOfRooms;
                                }
                                else
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare / request.NoOfRooms;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = 0; m < result.RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = result.RoomDetails[m].ChildCharges;
                                    totalTax = result.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    break;
                                }
                            }
                            roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                                roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            }

                            //incase of fixed type commission it is giving mark up for all room at once in accounting engine to do change.
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else if (itinerary.Source == HotelBookingSource.Miki || itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.GRN || itinerary.Source == HotelBookingSource.OYO)// itinerary.Source == HotelBookingSource.Yatra -- Added by somasekhar for Yatra(03/09/2018) and OYO
                {
                    int rc = Convert.ToInt32(roomCode);

                    if (result.RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            {
                                roomdata.RoomTypeCode = result.RoomDetails[rc].RoomTypeCode;
                            }
                            roomdata.RoomName = result.RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[rc].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[rc].mealPlanDesc;
                            roomdata.Supplements = result.RoomDetails[rc].Supplements;
                            roomdata.RoomIndex = result.RoomDetails[rc].RoomIndex;
                            roomdata.SmokingPreference = result.RoomDetails[rc].SmokingPreference;
                            if (itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.GRN || itinerary.Source == HotelBookingSource.OYO)
                            {
                                roomdata.EssentialInformation = result.RoomDetails[rc].EssentialInformation;
                                roomdata.SurChargeList = result.RoomDetails[rc].SurChargeList;
                            }
                            if (itinerary.Source == HotelBookingSource.GRN)
                            {
                                roomdata.BedTypeCode = result.RoomDetails[j].BedTypeCode;
                            }

                            itinerary.HotelCancelPolicy = result.RoomDetails[rc].CancellationPolicy;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[rc].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[rc].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare * request.NoOfRooms;
                                }
                                else
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare / request.NoOfRooms;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0;
                            for (int m = 0; m < result.RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                    totalTax = result.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    if (itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra) // Added by Somasekhar --  itinerary.Source == HotelBookingSource.Yatra
                                    {
                                        roomdata.TBOPrice = result.RoomDetails[m].TBOPrice;
                                    }
                                    break;
                                }
                            }
                            roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge) * request.NoOfRooms;
                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else if (itinerary.Source == HotelBookingSource.TBOHotel)  //Added by brahmam
                {
                    int rc = Convert.ToInt32(roomCode);
                    if (result.RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();

                            roomdata.RoomTypeCode = result.RoomDetails[rc].RoomTypeCode;
                            roomdata.RoomName = result.RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[rc].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[rc].mealPlanDesc;
                            roomdata.Supplements = result.RoomDetails[rc].Supplements;
                            roomdata.SmokingPreference = result.RoomDetails[rc].SmokingPreference;
                            roomdata.RoomIndex = result.RoomDetails[rc].RoomIndex;
                            roomdata.TBOPrice = result.RoomDetails[rc].TBOPrice;
                            itinerary.HotelCancelPolicy = result.RoomDetails[rc].CancellationPolicy;

                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[j].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare * request.NoOfRooms;

                                }
                                else
                                {
                                    fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalExtraGuestCharge = 0, totalChildCharge = 0; //totalTax = 0,
                            for (int m = 0; m < result.RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.PublishedFare = ((result.RoomDetails[m].TBOPrice.PublishedFare) * result.Price.RateOfExchange);
                                    roomdata.Price.Tax = ((result.RoomDetails[m].TBOPrice.Tax) * result.Price.RateOfExchange);
                                    roomdata.Price.SeviceTax = ((result.RoomDetails[m].TBOPrice.SeviceTax) * result.Price.RateOfExchange);
                                    roomdata.Price.AgentCommission = result.RoomDetails[m].TBOPrice.AgentCommission;
                                    roomdata.Price.TdsCommission = result.RoomDetails[m].TBOPrice.TdsCommission;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    break;
                                }
                            }

                            roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }
                        break;
                    }
                }
                else if (result.BookingSource == HotelBookingSource.WST || result.BookingSource == HotelBookingSource.JAC) //Added by brahmam
                {
                    int rc = Convert.ToInt32(roomCode);
                    if (result.RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();

                            roomdata.RoomTypeCode = result.RoomDetails[rc].RoomTypeCode;
                            roomdata.RoomName = result.RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[rc].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[rc].mealPlanDesc;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[rc].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[rc].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare * request.NoOfRooms;
                                }
                                else
                                {
                                    fare.RoomPrice = result.RoomDetails[rc].Rates[k].SellingFare / request.NoOfRooms;
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0;
                            for (int m = 0; m < result.RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    totalExtraGuestCharge = result.RoomDetails[m].SellExtraGuestCharges;
                                    totalTax = result.RoomDetails[m].TotalTax;
                                    roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                    roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                    roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                    roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                    roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                    roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                    roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                    roomdata.Price.SupplierCurrency = result.Price.SupplierCurrency;
                                    roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                    roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                    roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                    if (itinerary.Source == HotelBookingSource.JAC)
                                    {
                                        roomdata.Price.PublishedFare = result.RoomDetails[m].TBOPrice.PublishedFare;  // Jac publishfare storing in TBOPrice.PublishedFare Properity
                                        roomdata.EssentialInformation = result.RoomDetails[m].EssentialInformation;
                                    }
                                    break;
                                }
                            }
                            roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge) * request.NoOfRooms;
                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else
                {
                    if (result.RoomDetails[j].RoomTypeCode.Replace("\r\n", string.Empty).Trim().Equals(roomCode) && rpValidate)
                    {
                        if (result.Price == null)
                        {
                            result.Price = new PriceAccounts();
                        }
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            if (itinerary.Source == HotelBookingSource.GTA)
                            {
                                string[] roomList = result.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomTypeCode = roomList[0];
                                roomdata.NoOfCots = Convert.ToInt16(roomList[1]);
                                roomdata.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                            }
                            else if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                string[] roomList = result.RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomId = Convert.ToInt32(roomList[0]);
                                roomdata.RoomTypeCode = roomList[1];
                            }
                            else
                            {
                                roomdata.RoomTypeCode = result.RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = result.RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = result.RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = result.RoomDetails[j].Amenities;
                            roomdata.MealPlanDesc = result.RoomDetails[j].mealPlanDesc;
                            roomdata.ExtraGuestCharge = result.RoomDetails[j].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = result.RoomDetails[j].ChildCharges / request.NoOfRooms;
                            if (itinerary.Source == HotelBookingSource.HotelConnect)
                            {
                                if (result.RoomDetails[j].Occupancy != null && result.RoomDetails[j].Occupancy.ContainsKey("ExtraBed") && result.RoomDetails[j].Occupancy["ExtraBed"] == 1)
                                {
                                    roomdata.RoomName = roomdata.RoomName + " With Extra Bed";
                                    roomdata.ExtraBed = true;
                                }
                            }
                            if (itinerary.Source == HotelBookingSource.DOTW)
                            {
                                roomdata.CancelRestricted = result.RoomDetails[j].CancelRestricted;
                                roomdata.EssentialInformation = result.RoomDetails[j].EssentialInformation;
                            }

                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[j].Rates.Length];
                            for (int k = 0; k < result.RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = result.RoomDetails[j].Rates[k].Days;
                                if (request.NoOfRooms == result.RoomDetails.Length)
                                {
                                    if (itinerary.Source == HotelBookingSource.IAN)
                                    {
                                        fare.RoomPrice = result.RoomDetails[j].Rates[k].Totalfare;
                                    }
                                    else
                                    {
                                        fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare * request.NoOfRooms;
                                    }
                                }
                                else
                                {
                                    if (itinerary.Source == HotelBookingSource.IAN)
                                    {
                                        fare.RoomPrice = result.RoomDetails[j].Rates[k].Totalfare / request.NoOfRooms;
                                    }
                                    else
                                    {
                                        if (itinerary.Source != HotelBookingSource.RezLive)
                                        {
                                            fare.RoomPrice = result.RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                        }
                                        else
                                        {
                                            fare.RoomPrice = result.RoomDetails[j].Rates[k].Totalfare / request.NoOfRooms;
                                        }
                                    }
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            roomdata.RateType = RoomRateType.DOTW;// for failed serialization
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price (updated with Markup) as it is already set for HotelResult                           
                            {
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = 0; m < result.RoomDetails.Length; m++)
                                {
                                    if (result.RoomDetails[m].RoomTypeCode.Replace("\r\n", string.Empty).Trim().Equals(roomCode))
                                    {
                                        fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[m].Rates.Length];
                                        for (int k = 0; k < result.RoomDetails[m].Rates.Length; k++)
                                        {
                                            if (itinerary.Source != HotelBookingSource.RezLive)
                                            {
                                                grossFare += result.RoomDetails[m].Rates[k].SellingFare;
                                            }
                                            else
                                            {
                                                grossFare += (result.RoomDetails[m].Rates[k].Totalfare / result.RoomDetails[m].Rates.Length);
                                            }
                                            roomdata.Price.Discount = result.RoomDetails[m].Discount;
                                            //for some Extra Guest Charges will come and we need add it to the total - DOTW only
                                            totalExtraGuestCharge += result.RoomDetails[m].SellExtraGuestCharges;
                                            roomdata.Price.Markup = result.RoomDetails[m].Markup;
                                            roomdata.Price.MarkupValue = result.RoomDetails[m].MarkupValue;
                                            roomdata.Price.MarkupType = result.RoomDetails[m].MarkupType;
                                            roomdata.Price.DiscountValue = result.RoomDetails[m].DiscountValue;
                                            roomdata.Price.DiscountType = result.RoomDetails[m].DiscountType;
                                            roomdata.Price.SupplierCurrency = (result.Price != null && result.Price.SupplierCurrency != null ? result.Price.SupplierCurrency : Settings.LoginInfo.Currency);
                                            roomdata.Price.InputVATAmount = result.RoomDetails[m].InputVATAmount;
                                            roomdata.Price.TaxDetails = result.RoomDetails[m].TaxDetail;
                                            if (result.RoomDetails[m].supplierPrice > 0)
                                            {
                                                roomdata.Price.SupplierPrice = result.RoomDetails[m].supplierPrice;
                                            }
                                            else
                                            {
                                                if (result.Price.RateOfExchange == 0)
                                                {
                                                    result.Price.RateOfExchange = 1;
                                                }
                                                roomdata.Price.SupplierPrice = itinerary.Source != HotelBookingSource.HotelConnect ? (result.RoomDetails[m].TotalPrice) / result.Price.RateOfExchange : (result.RoomDetails[m].TotalPrice - result.RoomDetails[m].Markup) / result.Price.RateOfExchange;
                                            }
                                            roomdata.Price.RateOfExchange = result.Price.RateOfExchange;
                                        }

                                        totalChildCharge = result.RoomDetails[m].ChildCharges;
                                        totalTax = result.RoomDetails[m].TotalTax;
                                        break;
                                    }
                                }
                                roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge + totalTax) * request.NoOfRooms;
                                roomdata.Price.Tax = totalTax * request.NoOfRooms;
                                if (itinerary.Source == HotelBookingSource.TBOConnect)
                                {
                                    decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * result.Price.AgentCommission / 100;
                                    roomdata.Price.PublishedFare += agentComm;
                                    roomdata.Price.AgentCommission = agentComm;
                                    roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                }
                            }
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.CurrencyCode = result.Currency;
                            roomdata.Price.Currency = result.Currency;
                            roomInfo[i] = roomdata;
                        }
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Room Details. Error: " + ex.Message, "0");
        }
        return roomInfo;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="itinerary"></param>
    void LoadCancellationDetails(ref HotelItinerary itinerary)
    {
        MetaSearchEngine mse = new MetaSearchEngine(sessionId);
        mse.SettingsLoginInfo = Settings.LoginInfo;
        //mse.AgentBaseCurrency = agency.AgentCurrency;
        //CT.Core1.StaticData sd = new CT.Core1.StaticData();
        //sd.BaseCurrency = agency.AgentCurrency;
        //mse.RateOfExchange = sd.CurrencyROE;
        //mse.DecimalPoint = agency.DecimalValue;
        Dictionary<string, string> CancellationData = new Dictionary<string, string>();
        try
        {
            //Dictionary<string, string> CancellationData = new Dictionary<string, string>();
            List<HotelPenality> penaltyInfo = itinerary.PenalityInfo;


            if (penaltyInfo == null)
                penaltyInfo = new List<HotelPenality>();

            CZInventory.SearchEngine se = new CZInventory.SearchEngine();
            CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
            if (itinerary.Source == HotelBookingSource.DOTW)
            {
                //CancellationData = dotw.GetCancellationPolicy(itinerary, ref penaltyInfo, true);
                //itinerary.PassengerCountryOfResidence=request.
                CancellationData = mse.GetCancellationDetails(itinerary, ref penaltyInfo, true, HotelBookingSource.DOTW);
                //itinerary.PenalityInfo = penaltyInfo;// commented by shiva
            }
            else if (itinerary.Source == HotelBookingSource.HotelConnect)
            {
                List<string> Rooms = new List<string>(); 

                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    Rooms.Add(room.RoomTypeCode);
                }

                CancellationData = se.GetCancellationPolicy(ref result, request, Rooms);
                //CancellationData = se.GetCancellationPolicy(ref result, request.NoOfRooms);
            }
            else if (itinerary.Source == HotelBookingSource.RezLive)   //modified by brahmam   19/09/2015
            {

                //request.Currency = result.Currency;
                //request.CityCode = result.CityCode;
                //request.CityName = request.CityName.Replace(" City", "");


                DOTWCountry dotwApi = new DOTWCountry();
                Dictionary<string, string> Countries = dotwApi.GetAllCountries();

                itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                itinerary.DestinationCountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);

                decimal supplierPrice = 0;
                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                //Session["req"] = request;
                RezLive.XmlHub rezAPI = new RezLive.XmlHub(Session["cSessionId"].ToString());
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    rezAPI.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    rezAPI.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    rezAPI.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    rezAPI.AgentCurrency = Settings.LoginInfo.Currency;
                    rezAPI.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    rezAPI.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                CancellationData = rezAPI.GetCancellationInfo(itinerary, supplierPrice);
            }
            else if (itinerary.Source == HotelBookingSource.LOH)
            {
                LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(Session["cSessionId"].ToString());
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    jxe.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    jxe.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    jxe.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    jxe.AgentCurrency = Settings.LoginInfo.Currency;
                    jxe.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    jxe.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                decimal supplierPrice = 0;

                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                DOTWCountry dotwApi = new DOTWCountry();
                Dictionary<string, string> Countries = dotwApi.GetAllCountries();

                itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                //cancelation policy is coming based on the nationality
                CancellationData = jxe.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, itinerary.SequenceNumber, supplierPrice, itinerary.PassengerNationality);
                try
                {
                    Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                }
                catch { CancellationData["lastCancellationDate"] = itinerary.StartDate.AddDays(-Convert.ToDouble(ConfigurationSystem.LOHConfig["CancellationBuffer"])).ToString(); }
            }
            else if (itinerary.Source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
            {
                CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                hBeds.sessionId = Session["cSessionId"].ToString();
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    hBeds.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    hBeds.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    hBeds.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    hBeds.AgentCurrency = Settings.LoginInfo.Currency;
                    hBeds.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    hBeds.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                CancellationData = hBeds.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
            }
            else if (itinerary.Source == HotelBookingSource.WST) //Added by brahmam 26.09.2014
            {
                // un commented by brahmam
                CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    wstApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    wstApi.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    wstApi.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    wstApi.AgentCurrency = Settings.LoginInfo.Currency;
                    wstApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    wstApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                CancellationData = wstApi.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
            }
            else if (itinerary.Source == HotelBookingSource.GTA)
            {
                string[] splitter = { "@#" };
                CancellationData.Add("lastCancellationDate", itinerary.StartDate.ToString());
                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy.Split(splitter, StringSplitOptions.None)[0]);
                CancellationData.Add("AmendmentPolicy", itinerary.HotelCancelPolicy.Split(splitter, StringSplitOptions.None)[1]);
                CancellationData.Add("HotelPolicy", "");
            }
            //Added by chandan on 26/12/2015
            else if (itinerary.Source == HotelBookingSource.Miki)
            {

                CancellationData.Add("lastCancellationDate", itinerary.StartDate.ToString());

                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy); //@@@@ to be check for cal rooms cancel policy
                CancellationData.Add("HotelPolicy", itinerary.PropertyType); //here propertyType store hotel alert message for miki
            }
            else if (itinerary.Source == HotelBookingSource.Agoda)
            {
                CancellationData.Add("lastCancellationDate", itinerary.StartDate.ToString());
                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy); //@@@@ to be check for cal rooms cancel policy
            }
            //Added by Brahmam on 16/04/2016
            else if (itinerary.Source == HotelBookingSource.TBOHotel)
            {
                string[] splitter = { "@#" };
                CancellationData.Add("lastCancellationDate", itinerary.HotelCancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1]);
                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0]);
                CancellationData.Add("HotelPolicy", "");

            }
            //Added by Brahmam on 20.09.2016 Loading cancellation policy
            else if (itinerary.Source == HotelBookingSource.JAC)
            {
                CT.BookingEngine.GDS.JAC jacApi = new CT.BookingEngine.GDS.JAC();
                jacApi.SessionId = Session["cSessionId"].ToString();
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    jacApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    jacApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    jacApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    jacApi.AgentCurrency = Settings.LoginInfo.Currency;
                    jacApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    jacApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                CancellationData = jacApi.GetHotelChargeCondition(itinerary);
            }
            else if (itinerary.Source == HotelBookingSource.EET) //Loading cancellation policy added by brahmam 26.10.2016
            {
                CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET(Session["cSessionId"].ToString());
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    eetApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    eetApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    eetApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    eetApi.AgentCurrency = Settings.LoginInfo.Currency;
                    eetApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    eetApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                decimal supplierPrice = 0;

                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                DOTWCountry dotwApi = new DOTWCountry();
                Dictionary<string, string> Countries = dotwApi.GetAllCountries();

                itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                //cancelation policy is coming based on the nationality
                CancellationData = eetApi.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, itinerary.SequenceNumber, supplierPrice, itinerary.PassengerNationality);
                try
                {
                    Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                }
                catch { CancellationData["lastCancellationDate"] = itinerary.StartDate.AddDays(-Convert.ToDouble(ConfigurationSystem.EETConfig["CancellationBuffer"])).ToString(); }
            }
            //Added by somasekhar on 03/09/2018 -- for Yatra cancelation policy
            else if (itinerary.Source == HotelBookingSource.Yatra)
            {
                CancellationData.Add("lastCancellationDate", itinerary.StartDate.ToString());
                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy); //@@@@ to be check for cal rooms cancel policy
            }
            else if (itinerary.Source == HotelBookingSource.GRN)
            {
                CancellationData.Add("lastCancellationDate", itinerary.StartDate.ToString());

                if (itinerary.HotelCancelPolicy == string.Empty)// Calling Cancellation policy once again if it is empty
                {


                    HotelSearchResult resultObj = null;
                    HotelSearchResult[] hotelSearchResult = new HotelSearchResult[0];
                    CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                    grnApi.SessionId = Session["cSessionId"].ToString();
                    grnApi.GrnUserId = Settings.LoginInfo.UserID;
                    int agencyId = 0;
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID;
                        grnApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        grnApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        grnApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        grnApi.AgentCurrency = Settings.LoginInfo.Currency;
                        grnApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        grnApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                    }
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);

                        if (activeSources != null && activeSources.Count > 0)
                        {
                            grnApi.SourceCountryCode = activeSources[HotelBookingSource.GRN.ToString()];
                        }
                        else
                        {
                            grnApi.SourceCountryCode = "AE";
                        }
                    }
                    catch
                    {
                        grnApi.SourceCountryCode = "AE";
                    }


                    decimal oldPrice = 0;
                    decimal newPrice = 0;
                    HotelSearchResult[] hotelInfo = Session["SearchResults"] as HotelSearchResult[];
                    string hotelcode = itinerary.HotelCode;
                    resultObj = hotelInfo.First<HotelSearchResult>(delegate (HotelSearchResult h) { return h.HotelCode == hotelcode; });
                    string rateTye = resultObj.SupplierType.Split('|')[0];
                    //HotelSearchResult resultGRN = new HotelSearchResult();
                    //  resultGRN = resultObj.CopyByValue();

                    HotelRoomsDetails[] roomDetails = new HotelRoomsDetails[resultObj.RoomDetails.Length];
                    for (int i = 0; i < resultObj.RoomDetails.Length; i++)
                    {
                        roomDetails[i].RoomTypeCode = resultObj.RoomDetails[i].RoomTypeCode;
                    }
                    if (itinerary.Roomtype[0].BedTypeCode == "Bundeled")
                    {
                        grnApi.GetRoomPriceValidate(ref resultObj, request.CopyByValue(), 0, null, itinerary.Roomtype[0].RoomTypeCode.Split('|')[1], itinerary.Roomtype[0].RoomTypeCode.Split('|')[4], itinerary.Roomtype[0].RoomTypeCode.Split('|')[5]);
                        resultObj.RoomDetails = resultObj.RoomDetails.OrderBy(o => o.RoomTypeCode.Split('|')[0]).ToArray();
                        if (itinerary.Roomtype.Length == resultObj.RoomDetails.Length)
                        {
                            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                            {
                                for (int j = 0; j < resultObj.RoomDetails.Length; j++)
                                {
                                    if (resultObj.RoomDetails[j].RoomTypeCode.Split('|')[6] == itinerary.Roomtype[i].RoomTypeCode.Split('|')[6] && itinerary.Roomtype[i].RoomTypeCode.Split('|')[2] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[2] && itinerary.Roomtype[i].MealPlanDesc == resultObj.RoomDetails[j].mealPlanDesc)
                                    {
                                        newPrice += resultObj.RoomDetails[j].TotalPrice;
                                        itinerary.Roomtype[i].RoomTypeCode = resultObj.RoomDetails[j].RoomTypeCode;
                                        itinerary.PropertyType = resultObj.PropertyType;
                                        if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                                        {
                                            itinerary.HotelCancelPolicy = resultObj.RoomDetails[j].CancellationPolicy;
                                        }
                                        else
                                        {
                                            itinerary.HotelCancelPolicy = itinerary.HotelCancelPolicy + "|" + resultObj.RoomDetails[j].CancellationPolicy;
                                        }

                                        break;

                                    }
                                }
                            }
                        }

                        else
                        {
                            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                            {
                                for (int j = 0; j < resultObj.RoomDetails.Length; j++)
                                {
                                    if (itinerary.Roomtype[i].RoomTypeCode.Split('|')[7] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[7] && itinerary.Roomtype[i].RoomTypeCode.Split('|')[8] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[8])
                                    {
                                        newPrice += resultObj.RoomDetails[j].TotalPrice;
                                        if (itinerary.Roomtype[i].RoomTypeCode.Split('|')[2] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[2] && itinerary.Roomtype[i].MealPlanDesc == resultObj.RoomDetails[j].mealPlanDesc)
                                        {
                                            itinerary.Roomtype[i].RoomTypeCode = resultObj.RoomDetails[j].RoomTypeCode;
                                            itinerary.PropertyType = resultObj.PropertyType;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                        {
                            oldPrice += itinerary.Roomtype[i].Price.NetFare;
                            grnApi.GetRoomPriceValidate(ref resultObj, request.CopyByValue(), 0, null, itinerary.Roomtype[i].RoomTypeCode.Split('|')[1], itinerary.Roomtype[i].RoomTypeCode.Split('|')[4], itinerary.Roomtype[i].RoomTypeCode.Split('|')[5]);
                            newPrice += resultObj.RoomDetails[0].TotalPrice;
                            if (itinerary.Roomtype[i].RoomTypeCode.Split('|')[2] == resultObj.RoomDetails[0].RoomTypeCode.Split('|')[2] && itinerary.Roomtype[i].MealPlanDesc == resultObj.RoomDetails[0].mealPlanDesc)
                            {
                                itinerary.PropertyType = resultObj.PropertyType;
                                itinerary.Roomtype[i].RoomTypeCode = resultObj.RoomDetails[0].RoomTypeCode;
                            }
                        }
                    }


                }



                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                CancellationData.Add("HotelPolicy", "");
            }
            //Added by somasekhar on 14/12/2018 -- for OYO cancelation policy
            else if (itinerary.Source == HotelBookingSource.OYO)
            {
                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy); //@@@@ to be check for cal rooms cancel policy
            }
            foreach (KeyValuePair<string, string> pair in CancellationData)
            {
                switch (pair.Key)
                {
                    case "lastCancellationDate":
                        if (itinerary.Source != HotelBookingSource.DOTW)
                        {
                            itinerary.LastCancellationDate = Convert.ToDateTime(pair.Value);
                        }
                        break;
                    case "CancelPolicy":
                        cancelData = pair.Value;
                        itinerary.HotelCancelPolicy = cancelData;
                        break;
                    case "HotelPolicy":
                        remarks = pair.Value;
                        itinerary.HotelPolicyDetails = remarks;
                        break;
                    case "QUOTE_CHANGED":
                        warningMsg = "Price/Quotation has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                    case "PRICE_CHANGED":
                        warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                    case "NOT_AVAILABILITY":
                        warningMsg = "This particular room(s) is not available for this Hotel. Please search again to book this Hotel.";
                        break;
                    case "NOT_POSSIBLE":
                        warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                }
            }
            if (itinerary.Source != HotelBookingSource.LOH && itinerary.Source != HotelBookingSource.EET)
            {
                //For Price check
                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    if (room.PreviousFare > 0 && room.PreviousFare < room.Price.NetFare)
                    {
                        warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                    }
                }
            }
            hdnWarning.Value = warningMsg;
            if (!string.IsNullOrEmpty(warningMsg))
            {
                imgContinue.Text = "Search Again";
            }
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.GetBooking, CT.Core.Severity.High, Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID), "Failed to get Cancellation details(HotelReview). Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }


        //itinerary.HotelCancelPolicy = string.Empty;
        //List<HotelPenality> penalityList = new List<HotelPenality>();
        //Dictionary<string, string> polList = mse.GetCancellationDetails(itinerary, ref penalityList, true, itinerary.Source);
        //if (itinerary.IsDomestic || itinerary.Source == HotelBookingSource.IAN)
        //{
        //    itinerary.VoucherStatus = true;
        //    if (itinerary.Source == HotelBookingSource.TBOConnect)
        //    {
        //        itinerary.PenalityInfo = penalityList;
        //    }
        //}
        //else
        //{
        //    itinerary.VoucherStatus = false;
        //    itinerary.PenalityInfo = penalityList;
        //}
        //if (polList.ContainsKey("HotelPolicy"))
        //{
        //    itinerary.HotelPolicyDetails = polList["HotelPolicy"];
        //}
        //if (polList.ContainsKey("CancelPolicy"))
        //{
        //    itinerary.HotelCancelPolicy = polList["CancelPolicy"];
        //}
        // modified by brahmam assigned lastCancellationDate 25.11.2015
        if (itinerary.Source == HotelBookingSource.TBOHotel)
        {
            if (CancellationData.ContainsKey("lastCancellationDate"))
            {
                DateTime lastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                itinerary.LastCancellationDate = lastCancellationDate;
            }
            else
            {
                itinerary.LastCancellationDate = itinerary.StartDate;
            }
        }
        else if (itinerary.Source == HotelBookingSource.DOTW)
        {
            if (CancellationData.ContainsKey("lastCancellationDate"))
            {
                itinerary.LastCancellationDate = mse.GetLastCancellationDateforHotel(Convert.ToInt32(CancellationData["lastCancellationDate"]), itinerary.StartDate, itinerary.Source);
                //if ((!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelBookingUnderCancellation)) && itinerary.LastCancellationDate <= DateTime.Now && !itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
                {
                    bookingMessage = "We are unable to book the hotel as The check-in date for the booking is within the cancellation period.";
                    mailMessage = "If you still want to book this hotel Go to: <a href=\"HotelSearch.aspx\">Search Page</a> or  <a href=\"javascript:sendMail()\">Send E-mail.</a>";
                    Session["hItinerary"] = itinerary;
                }
            }
            else
            {
                //incase of non refundable room booking, last cancellation date is null in case of IAN/Tourico/HotelBeds
                itinerary.LastCancellationDate = DateTime.Now;
            }
        }
        if (CancellationData.ContainsKey("isNonRefundable") && CancellationData["isNonRefundable"] == "true")
        {
            itinerary.LastCancellationDate = DateTime.Now;//since its a non-refundable booking, can not be cancelled
            //if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelBookingUnderCancellation) && !itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
            {
                bookingMessage = "We are unable to book the hotel as you have selected non refundable room.";
                mailMessage = "If you still want to book this hotel <a href=\"javascript:sendMail()\">Click here to E-mail.</a> <br/>or<br/> <a href=\"javascript:history.go(-1)\">Go Back</a> to Choose another hotel.";
                Session["hItinerary"] = itinerary;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="itinerary"></param>
    void LoadHotelDetails(ref HotelItinerary itinerary)
    {

        MetaSearchEngine mse = new MetaSearchEngine(sessionId);
        mse.SettingsLoginInfo = Settings.LoginInfo;

        HotelDetails hotelDetail = new HotelDetails();
        if (itinerary.Source != HotelBookingSource.TBOHotel)
        {
            hotelDetail = mse.GetHotelDetails(itinerary.CityCode, itinerary.HotelCode, itinerary.StartDate, itinerary.EndDate, request.PassengerNationality, request.PassengerCountryOfResidence, itinerary.Source);
        }
        else
        {
            try
            {
                TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                hotelDetail = tboHotel.GetHotelDetails(itinerary.CityCode, itinerary.HotelCode, Convert.ToInt32(itinerary.SequenceNumber), itinerary.PropertyType);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOHoteDetailsSerch, Severity.High, 0, "Exception returned from TBOHotel.GetHotelDetails Error Message:" + ex.Message + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
        }
        if (itinerary.Source == HotelBookingSource.Desiya || itinerary.Source == HotelBookingSource.IAN)
        {
            if (!string.IsNullOrEmpty(hotelDetail.HotelPolicy))
            {
                itinerary.HotelPolicyDetails += hotelDetail.HotelPolicy + "| ";
            }
            else
            {
                itinerary.HotelPolicyDetails = string.Empty;
            }
        }
        //HotelAddress1,HotelAddress2 empty time we need to reasigned 
        if (!string.IsNullOrEmpty(hotelDetail.Address))
        {
            itinerary.HotelAddress1 = hotelDetail.Address;
        }
        if (!string.IsNullOrEmpty(hotelDetail.PhoneNumber))
        {
            itinerary.HotelAddress2 = hotelDetail.PhoneNumber;
        }

        if (hotelDetail.Email != null && hotelDetail.Email.Length > 0 && !hotelDetail.Email.Contains("NA"))
        {
            itinerary.HotelAddress1 += "\n E-mail :" + hotelDetail.Email;
        }
        if (hotelDetail.URL != null && hotelDetail.URL.Length > 0 && !hotelDetail.URL.Contains("NA"))
        {
            itinerary.HotelAddress1 += "\n Website :" + hotelDetail.URL;
        }
        if (hotelDetail.PhoneNumber != null && hotelDetail.PhoneNumber.Length > 0 && !hotelDetail.PhoneNumber.Contains("NA"))
        {
            if (!string.IsNullOrEmpty(itinerary.HotelAddress2))
            {
                itinerary.HotelAddress2 = hotelDetail.PhoneNumber;
            }
        }
        if (hotelDetail.FaxNumber != null && hotelDetail.FaxNumber.Length > 0 && !hotelDetail.FaxNumber.Contains("NA"))
        {
            itinerary.HotelAddress2 += ", \n Fax : " + hotelDetail.FaxNumber;
        }
        if (itinerary.Source == HotelBookingSource.Agoda)
        {
            itinerary.HotelPolicyDetails = hotelDetail.HotelPolicy;
        }

    }

    /// <summary>
    /// This Method is used for clearing Session Objects
    /// </summary>
    private void clearSession()
    {
        Session.Remove("req");
        Session.Remove("hResult");
        Session.Remove("hItinerary");
    }
    protected void imgContinue_Click(object sender, EventArgs e)
    {
        if (hdnWarning.Value.Length > 0)
        {
            Response.Redirect("HotelSearch.aspx?source=Hotel");
        }
        else
        {
            Response.Redirect("GuestDetails.aspx");
        }
    }
}
