using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using System.IO;
using CT.TicketReceipt.BusinessLayer;
using CozmoB2BWebApp;

public partial class EmailItineraryPage : System.Web.UI.Page
{
    protected SearchResult[] result = new SearchResult[0];
    protected Ticket ticket;
    protected Invoice invoice;
    protected List<Ticket> ticketList;
    protected FlightItinerary flightItinerary;
    protected UserPreference preference = new UserPreference();
    protected AgentMaster agency;
    protected string logo = string.Empty;
    protected string cityName = string.Empty;
    protected int paxIndex = -1;
    protected string paxName = string.Empty;
    protected int paxTypeIndex = 0;   // for ptcDetail
    //protected InsurancePassenger passenger;
    bool isDN = false;
    protected string segmentMessage = String.Empty;
    protected string flightNumberString = String.Empty;
    protected List<string> airlineNameRemarksList = new List<string>();
    protected List<string> airlineNameFareRuleList = new List<string>();
    protected string remarks = string.Empty;
    protected string fareRule = string.Empty;
    protected string adMessage = string.Empty;
    protected Dictionary<string, DataRow[]> segment = new Dictionary<string, DataRow[]>();
    protected DataTable segmentTable;
    protected string tboConnect = string.Empty;
    protected bool IsShowHotelAd = false;  // Bug ID : 0029414- E-Ticket Ads
    protected string inBaggage = "", outBaggage = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string preferenceValue = string.Empty;
        string showLogoPreference = string.Empty;
        int memberId = Convert.ToInt32(Settings.LoginInfo.UserID);

        int agentId = 0, decimalValue=0;
        if (Session["BookingAgencyID"] != null && Convert.ToInt32(Session["BookingAgencyID"]) == 0)
        {
            agentId = Convert.ToInt32(Session["BookingAgencyID"]);
        }
        else if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo != null)
        {
            if (Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId) != 0)
            {
                agentId = Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                decimalValue = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue;
            }
            else
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.OnBehalfAgentID;
                decimalValue = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.OnBehalfAgentDecimalValue;
            }
        }


        string sourceName = Request["sourceName"].Trim().ToString();
        string serverPath = Request.Url.Scheme + "://ctb2b.cozmotravel.com/";
        #region Flight Details Mail
        if (sourceName == "Flights")
        {
             if (Request["csvId"] != null)
            {

                string addressList = Request["addressList"];
                double markup = Convert.ToDouble(Request["totalMarkup"]);
                int index = Convert.ToInt32(Request["index"]);
                string[] idArray = Request["csvId"].Split(',');
                string emailBody = "";
                try
                {
                    // SearchResult resultObj=new SearchResult();

                    //Boolean test = Convert.ToBoolean(0);
                    string logoPath = "";
                    // string serverPath = "";
                    //serverPath = "http://ctb2b.cozmotravel.com/";

                    // to show agent logo
                    if (agentId > 0)
                    {
                        logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;

                    }
                    string airlineLogo = serverPath + Airline.logoDirectory + "/";


                    SearchRequest request = new SearchRequest();
                    request = (SearchRequest)Session["FlightRequest"];


                    pageTitle.InnerHtml = "<div style='margin-top:5px;'><img src='" + logoPath + "' width='180px' height='60px'/></div>";
                    pageTitle.InnerHtml += "<div style='height:25px; width:100%; background-color:#1C498A; color:#ffffff;'><h3> Flight Itinerary Details</h3></div>";



                    // bool published = Convert.ToBoolean(Request["isPublished"]);

                    result = new SearchResult[index];

                    result = new SearchResult[idArray.Length];
                    int r = 0;
                    if (idArray[0] != "")
                    {
                        foreach (string s in idArray)
                        {
                            int resultId = Convert.ToInt32(s);
                            result[r] = Basket.FlightBookingSession[Session["sessionId"].ToString()].Result[Convert.ToInt32(idArray[r]) - 1];
                            r++;
                        }
                    }

                    for (int k = 0; k < result.Length; k++)
                    {
                        SearchResult resultObj = result[k];


                        for (int i = 0; i < resultObj.Flights.Length; i++)
                        {



                            //if (resultObj.Flights.Length <= 1)
                            //{
                            //    //imgReturn.Visible = false;
                            //    //imgReturnLogo.Visible = false;
                            //}
                            DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());
                            if (i == 0) // out bound flights (INWARDS FLIGHTS)
                            {
                                //DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());
                                //imgOnwardLogo.ImageUrl = logoInfo.Value.ToString();
                                //lblOnCarrier.Text = logoInfo.Key.ToString();
                                HtmlTableRow onwardRow = new HtmlTableRow();
                                HtmlTableCell onwardCell = new HtmlTableCell();
                                onwardCell.ColSpan = 5;
                                onwardCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:0px; line-height:25px; width:100%; margin:0px; color:#999999;'>";
                                onwardCell.InnerHtml += "<div style='font-size:15px; font-weight:bold; color:#000000;'>Onward</div>";
                                //HtmlTableCell priceCell = new HtmlTableCell();
                                //priceCell.Align = "center";
                                //priceCell.InnerHtml = "<div id='ViewFlight" + resultObj.ResultId + "div' ><span style='font-weight: bold;text-align:center;'><label style='font-size: 14px;'><span style='color: #FF0000'>AED</span><br/><span>" + (Math.Ceiling(resultObj.TotalFare) + markup).ToString("N2") + "</span></label></span>";

                                onwardRow.Cells.Add(onwardCell);
                                //seperationRow.Cells.Add(priceCell);
                                flightDetails.Rows.Add(onwardRow);

                                for (int j = 0; j < resultObj.Flights[i].Length; j++)
                                {
                                    Airline departingAirline = new Airline();
                                    departingAirline.Load(resultObj.Flights[i][j].Airline);


                                    #region Binding View Flights Details
                                    //flight details
                                    HtmlTableRow detailsRow = new HtmlTableRow();
                                    HtmlTableRow connectingRow = new HtmlTableRow();

                                    HtmlTableCell logoCell = new HtmlTableCell();
                                    HtmlTableCell originCell = new HtmlTableCell();
                                    HtmlTableCell destCell = new HtmlTableCell();
                                    HtmlTableCell duraCell = new HtmlTableCell();
                                    HtmlTableCell aircraftCell = new HtmlTableCell();
                                    HtmlTableCell connectCell = new HtmlTableCell();

                                    logoCell.Width = "40px";

                                    logoCell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:5px;'><img src='" + airlineLogo + departingAirline.LogoFile + "' width='28' height='30'/></span><br/>";
                                    logoCell.InnerHtml += departingAirline.AirlineCode + "<br/>";
                                    logoCell.Align = "Center";
                                    logoCell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                                    logoCell.InnerHtml += resultObj.Flights[i][j].FlightNumber;

                                    originCell.Attributes.Add("style", "width:23%;text-align:center;");
                                    destCell.Attributes.Add("style", "width:23%;text-align:center;");
                                    duraCell.Attributes.Add("style", "width:14%;text-align:center;");
                                    connectCell.ColSpan = 5;

                                    aircraftCell.RowSpan = 1;
                                    aircraftCell.Attributes.Add("style", "width:20%;text-align:center;");


                                    originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                    originCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, HH:mm tt");

                                    destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                    destCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, HH:mm tt");
                                    duraCell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                    if (resultObj.Flights[i][j].CabinClass != null)
                                    {
                                        duraCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                    }
                                    else
                                    {
                                        duraCell.InnerHtml += "<br/>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                    }
                                    duraCell.InnerHtml += "<br/>";
                                    aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style=' color:#CCCCCC;'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style=' color:#CCCCCC;'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                    if (j > 0)
                                    {

                                        connectCell.ColSpan = 5;
                                        connectCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins</strong> ";
                                        connectCell.InnerHtml += "<span style='color:#CCCCCC;'>|</span> Connecting flight may departs from different Terminal</div>";
                                        connectingRow.Cells.Add(connectCell);
                                        flightDetails.Rows.Add(connectingRow);
                                    }
                                    detailsRow.Cells.Add(logoCell);
                                    detailsRow.Cells.Add(originCell);
                                    detailsRow.Cells.Add(destCell);
                                    detailsRow.Cells.Add(duraCell);
                                    detailsRow.Cells.Add(aircraftCell);
                                    flightDetails.Rows.Add(detailsRow);

                                    #endregion




                                }
                            }
                            else  //inbound flight details  (RETURN FLIGHTS)
                            {
                                HtmlTableRow seperationRow = new HtmlTableRow();
                                HtmlTableCell seperationCell = new HtmlTableCell();
                                seperationCell.ColSpan = 5;
                                seperationCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right repeat; padding-right:0px; line-height:25px; margin:0px; color:#999999;'>";
                                seperationCell.InnerHtml += "<div style='font-size:15px; font-weight:bold; color:#000000;'>Return</div>";

                                seperationRow.Cells.Add(seperationCell);
                                //seperationRow.Cells.Add(priceCell);
                                flightDetails.Rows.Add(seperationRow);



                                for (int j = 0; j < resultObj.Flights[i].Length; j++)
                                {
                                    Airline departingAirline = new Airline();
                                    departingAirline.Load(resultObj.Flights[i][j].Airline);

                                    #region Binding View Flights Details
                                    //flight details
                                    HtmlTableRow detailsRow = new HtmlTableRow();
                                    HtmlTableRow connectingRow = new HtmlTableRow();

                                    HtmlTableCell logoCell = new HtmlTableCell();
                                    HtmlTableCell originCell = new HtmlTableCell();
                                    HtmlTableCell destCell = new HtmlTableCell();
                                    HtmlTableCell duraCell = new HtmlTableCell();
                                    HtmlTableCell aircraftCell = new HtmlTableCell();
                                    HtmlTableCell connectCell = new HtmlTableCell();


                                    logoCell.Width = "40px";
                                    logoCell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:5px;'><img src='" + airlineLogo + departingAirline.LogoFile + "' width='28' height='30'/></span><br/>";
                                    logoCell.InnerHtml += departingAirline.AirlineCode + "<br/>";
                                    logoCell.Align = "Center";
                                    logoCell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                                    logoCell.InnerHtml += resultObj.Flights[i][j].FlightNumber;

                                    originCell.Attributes.Add("style", "width:23%;text-align:center;");
                                    destCell.Attributes.Add("style", "width:23%;text-align:center;");
                                    duraCell.Attributes.Add("style", "width:14%;text-align:center;");
                                    connectCell.ColSpan = 5;

                                    aircraftCell.RowSpan = 1;
                                    aircraftCell.Attributes.Add("style", "width:20%;text-align:center;");


                                    originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                    originCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, HH:mm tt");

                                    destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                    destCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, HH:mm tt");



                                    duraCell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";

                                    if (resultObj.Flights[i][j].CabinClass != null)
                                    {
                                        duraCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                    }
                                    else
                                    {
                                        duraCell.InnerHtml += "<br/>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                    }
                                    aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style=' color:#CCCCCC;'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style=' color:#CCCCCC;'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                    if (j > 0)
                                    {

                                        connectCell.ColSpan = 5;
                                        connectCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins</strong> ";
                                        connectCell.InnerHtml += "<span style='color:#CCCCCC;'>|</span> Connecting flight may departs from different Terminal</div>";
                                        connectingRow.Cells.Add(connectCell);
                                        flightDetails.Rows.Add(connectingRow);
                                    }

                                    detailsRow.Cells.Add(logoCell);
                                    detailsRow.Cells.Add(originCell);
                                    detailsRow.Cells.Add(destCell);
                                    detailsRow.Cells.Add(duraCell);
                                    detailsRow.Cells.Add(aircraftCell);

                                    flightDetails.Rows.Add(detailsRow);


                                    #endregion
                                }
                            }

                        }
                        #region Fare Summary


                        HtmlTableRow paxRow = new HtmlTableRow();
                        HtmlTableCell titleCell = new HtmlTableCell();
                        HtmlTableCell totalCell = new HtmlTableCell();
                        titleCell.ColSpan = 4;

                        HtmlTableCell paxNoCell = new HtmlTableCell();
                        HtmlTableCell basicFareCell = new HtmlTableCell();
                        HtmlTableCell taxCel = new HtmlTableCell();
                        // paxNoCell.InnerHtml = "<div style='position: absolute; top: 190px; left: 350px; z-index: 1; display: none' id='fare_summary_" + resultObj.ResultId + "'><div>";
                        paxNoCell.InnerHtml = "<div style='width:100%; height:20px; background-color:#1C498A; color:#ffffff;'><h2> Fare Summary</h2></div>";
                        paxNoCell.InnerHtml += "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr style='background-color:#E8E8E8 ;'><td><b>Passengers</b></td><td><b>Base Fare</b></td><td><b>Taxes & Fees</b></td><td><b>Total Fare</b></td></tr>";
                        //paxNoCell.InnerHtml += "<td style='width:20%'>Total</td></tr>";
                        result[k] = Basket.FlightBookingSession[Session["sessionId"].ToString()].Result[Convert.ToInt32(idArray[k]) - 1];
                        int adultCount = 0;
                        int childCount = 0;
                        int infantCount = 0;
                        decimal adultBaseFare = 0;
                        decimal childBaseFare = 0;
                        decimal infantBaseFare = 0;
                        decimal adultTax = 0;
                        decimal childTax = 0;
                        decimal infantTax = 0;
                        decimal totalFare = 0;
                        decimal adultMarkup = 0;
                        decimal childMarkup = 0;
                        decimal infantMarkup = 0, otherCharges=0;
                        // int decimalPoint = 0;
                        decimal adultBreakMarkup = Convert.ToDecimal(markup);
                        // decimal childBreakMarkup = Convert.ToDecimal(markup);
                        int totalPax = 0;
                        if (request.AdultCount > 0)
                        {
                            totalPax = 1;

                        }

                        if (request.ChildCount > 0)
                        {
                            totalPax += 1;
                        }
                        if (request.InfantCount > 0)
                        {
                            totalPax += 1;

                        }
                        
                        adultBreakMarkup = Math.Round(adultBreakMarkup / totalPax, decimalValue);
                        if (resultObj.ResultBookingSource == BookingSource.TBOAir)
                        {
                            otherCharges = ((resultObj.Price.OtherCharges + resultObj.Price.TransactionFee + resultObj.Price.SServiceFee) / (request.AdultCount + request.ChildCount + request.InfantCount));
                        }
                        for (int i = 0; i < resultObj.FareBreakdown.Length; i++)
                        {
                            if (resultObj.FareBreakdown[i].PassengerType == PassengerType.Adult)
                            {
                                adultCount = resultObj.FareBreakdown[i].PassengerCount;
                                adultMarkup = (resultObj.FareBreakdown[i].AgentMarkup - resultObj.FareBreakdown[i].AgentDiscount);
                                adultBaseFare = ((decimal)resultObj.FareBreakdown[i].BaseFare + resultObj.FareBreakdown[i].HandlingFee) / adultCount;
                                adultTax = ((resultObj.FareBreakdown[i].Tax + resultObj.FareBreakdown[i].AdditionalTxnFee + resultObj.FareBreakdown[i].AirlineTransFee + adultMarkup) / adultCount);
                                adultTax += adultBreakMarkup + otherCharges;

                                // Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Tax"+resultObj.FareBreakdown[i].Tax.ToString() +",addTXNfee "+ resultObj.FareBreakdown[i].AdditionalTxnFee.ToString() +",AilineFee "+ resultObj.FareBreakdown[i].AirlineTransFee.ToString() +", adt markup "+ adultMarkup, "0");
                                totalFare += ((adultBaseFare + adultTax) * adultCount);
                                paxNoCell.InnerHtml += "<tr><td >Adults" + " X " + adultCount + "</td><td align='left'>" + (adultBaseFare * adultCount).ToString("N" + decimalValue) + "</td><td align='left'>" + ((adultTax * adultCount)).ToString("N" + decimalValue) + "</td><td align='left'> " + (((adultBaseFare + adultTax) * adultCount)).ToString("N" + decimalValue) + "</td></tr>";
                            }
                            if (resultObj.FareBreakdown[i].PassengerType == PassengerType.Child)
                            {
                                childCount = resultObj.FareBreakdown[i].PassengerCount;
                                childMarkup = (resultObj.FareBreakdown[i].AgentMarkup - resultObj.FareBreakdown[i].AgentDiscount);
                                childBaseFare = ((decimal)resultObj.FareBreakdown[i].BaseFare + resultObj.FareBreakdown[i].HandlingFee) / childCount;
                                childTax = ((resultObj.FareBreakdown[i].Tax + resultObj.FareBreakdown[i].AdditionalTxnFee + resultObj.FareBreakdown[i].AirlineTransFee + childMarkup) / childCount);
                                childTax += adultBreakMarkup + otherCharges;
                                totalFare += ((childBaseFare + childTax) * childCount);
                                paxNoCell.InnerHtml += "<tr><td >Childs" + " X " + childCount + "</td><td align='left'>" + (childBaseFare * childCount).ToString("N" + decimalValue) + "</td><td align='left'>" + ((childTax * childCount)).ToString("N" + decimalValue) + "</td><td align='left'> " + (((childBaseFare + childTax) * childCount)).ToString("N" + decimalValue) + "</td></tr>";
                            }
                            if (resultObj.FareBreakdown[i].PassengerType == PassengerType.Infant)
                            {
                                infantCount = resultObj.FareBreakdown[i].PassengerCount;
                                infantMarkup = (resultObj.FareBreakdown[i].AgentMarkup - resultObj.FareBreakdown[i].AgentDiscount);
                                infantBaseFare = ((decimal)resultObj.FareBreakdown[i].BaseFare + resultObj.FareBreakdown[i].HandlingFee) / infantCount;
                                infantTax = ((resultObj.FareBreakdown[i].Tax + resultObj.FareBreakdown[i].AdditionalTxnFee + resultObj.FareBreakdown[i].AirlineTransFee + infantMarkup) / infantCount);
                                infantTax += adultBreakMarkup + otherCharges;
                                totalFare += ((infantBaseFare + infantTax) * infantCount);
                                //paxNoCell.InnerHtml += "<tr><td>Infants</td><td align='center'>" + infantBaseFare.ToString("N" + decimalPoint) + "</td><td align='center'>" + infantTax.ToString("N" + decimalPoint) + "</td><td align='center'> " + (infantBaseFare + infantTax).ToString("N" + decimalPoint) + " X " + infantCount + "</td><td align='right' > " + ((infantBaseFare + infantTax) * infantCount).ToString("N" + decimalPoint) + "</td></tr>";
                                paxNoCell.InnerHtml += "<tr><td >Infants" + " X " + infantCount + "</td><td align='left'>" + (infantBaseFare * infantCount).ToString("N" + decimalValue) + "</td><td align='left'>" + ((infantTax * infantCount)).ToString("N" + decimalValue) + "</td><td align='left'> " + (((infantBaseFare + infantTax) * infantCount)).ToString("N" + decimalValue) + "</td></tr>";
                            }
                        }
                        paxNoCell.InnerHtml += "<br/><br/><tr><td colspan='3' style='text-align:right; font-size:16px; font-weight:bold;'></td><td align='right' style='text-align:right; font-size:20px; font-weight:bold;'>Total Net Fare: " + resultObj.Currency + " " +(resultObj.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(totalFare).ToString("N" + decimalValue) : Math.Round(totalFare).ToString("N" + decimalValue)) + "</td></tr>"; ;
                        paxNoCell.InnerHtml += "<br/><tr><td colspan='4'><div><sup>*</sup> There might a slight difference in the total price in fare summary due to rounding off.</div></td></tr></div>";
                        paxNoCell.InnerHtml += "<br/><div style='text-align:center; font-size:12px; font-weight:bold;'>Note: This is a system generated email. We request you not to reply to this email. </div></table>";



                        paxRow.Cells.Add(titleCell);
                        paxRow.Cells.Add(paxNoCell);

                        paxTable.Rows.Add(paxRow);
                        //count++;
                        // }
                        //}
                        #endregion

                        #region Create Subject
                        string origin, destination, endDate;
                        List<string> city = new List<string>();
                        List<string> date = new List<string>();

                        if (request.Type != SearchType.MultiWay)
                        {
                            FlightSegment segment = request.Segments[0];

                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                            {
                                city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                break;
                            }

                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                            {
                                city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                break;
                            }
                            //IFormatProvider format = new CultureInfo("en-GB");
                            string[] depDate = segment.PreferredDepartureTime.ToString("dd-MM-yyyy").Split('-');
                            string[] arrDate = new string[0];
                            date.Add(depDate[0] + "/" + depDate[1] + "/" + depDate[2]);
                            if (request.Type == SearchType.Return)
                            {
                                date.Add(request.Segments[1].PreferredArrivalTime.ToString("dd/MM/yyyy"));
                            }
                            else
                            {
                                //date.Add(segment.PreferredArrivalTime.ToString("dd/MM/yyyy"));
                            }
                        }
                        else
                        {

                            for (int i = 0; i < request.Segments.Length; i++)
                            {
                                FlightSegment segment = request.Segments[i];
                                switch (i + 1)
                                {
                                    case 1:
                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }

                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }
                                        date.Add(Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy"));

                                        break;
                                    case 2:
                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }

                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }
                                        date.Add(Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy"));
                                        break;
                                    case 3:
                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }

                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }
                                        date.Add(Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy"));
                                        break;
                                    case 4:
                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }

                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }
                                        date.Add(Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy"));
                                        break;
                                    case 5:
                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }

                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }
                                        date.Add(Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy"));
                                        break;
                                    case 6:
                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }

                                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                        {
                                            city.Add("(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName);
                                            break;
                                        }
                                        date.Add(Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy"));
                                        break;



                                }
                            }
                        }



                        int startIndex = city[0].IndexOf(')') + 1;
                        int endIndex = city[0].LastIndexOf(',') - 1;
                        int length = endIndex - startIndex + 1;
                        origin = city[0];
                        origin = origin.Substring(startIndex, length);

                        startIndex = city[city.Count - 1].IndexOf(')') + 1;
                        endIndex = city[city.Count - 1].LastIndexOf(',') - 1;
                        length = endIndex - startIndex + 1;
                        destination = city[city.Count - 1];
                        destination = destination.Substring(startIndex, length);
                        if (date.Count > 1)
                        {
                            endDate = "(" + date[date.Count - 1].ToString() + ") for Return ";
                        }
                        else
                        {
                            endDate = "";
                        }
                        //startIndex = date[0].IndexOf(')') + 1;
                        //endIndex = date[0].LastIndexOf(',') - 1;
                        //length = endIndex - startIndex + 1;
                        //startDate = date[0];
                        //startDate = startDate.Substring(startIndex, length);

                        #endregion

                        #region Old B2B Code For Send Mail of flight Details
                        //Old Code Start
                        //for (int i = 0; i < resultObj.Flights.Length; i++)
                        //{
                        //    HtmlTableRow firstRow = new HtmlTableRow();
                        //    HtmlTableRow secondRow = new HtmlTableRow();
                        //    HtmlTableRow thirdRow = new HtmlTableRow();

                        //    HtmlTableRow connectingRow = new HtmlTableRow();
                        //    HtmlTableRow fourthRow = new HtmlTableRow();
                        //    HtmlTableRow fifthRow = new HtmlTableRow();
                        //    HtmlTableRow sixthRow = new HtmlTableRow();
                        //    HtmlTableRow seventhRow = new HtmlTableRow();

                        //    if (i == 0)
                        //    {
                        //        DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());

                        //        for (int j = 0; j < resultObj.Flights[i].Length; j++)
                        //        {
                        //            switch (j)
                        //            {
                        //                case 0://non stop
                        //                    HtmlTableCell logoCell = new HtmlTableCell();
                        //                    HtmlTableCell originCell = new HtmlTableCell();
                        //                    HtmlTableCell destCell = new HtmlTableCell();
                        //                    HtmlTableCell duraCell = new HtmlTableCell();
                        //                    HtmlTableCell aircraftCell = new HtmlTableCell();

                        //                    //flight details code
                        //                    logoCell.Width = "40px";
                        //                    logoCell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='" + ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logoCell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logoCell.Align = "Center";
                        //                    logoCell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    //flight details code
                        //                    logoCell.InnerHtml += resultObj.Flights[i][j].FlightNumber;


                        //                    originCell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    destCell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    duraCell.Attributes.Add("style", "width:14%;text-align:center;");
                        //                    aircraftCell.RowSpan = 1;
                        //                    aircraftCell.Attributes.Add("style", "width:20%;");
                        //                    originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    originCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                        //                    destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    destCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                        //                    TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                        //                    duraCell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";
                        //                    aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                        //                    //flight details code
                        //                    firstRow.Cells.Add(logoCell);
                        //                    firstRow.Cells.Add(originCell);
                        //                    firstRow.Cells.Add(destCell);
                        //                    firstRow.Cells.Add(duraCell);
                        //                    firstRow.Cells.Add(aircraftCell);
                        //                    flightDetails.Rows.Add(firstRow);
                        //                    break;
                        //                case 1://one stops
                        //                    HtmlTableCell logo1Cell = new HtmlTableCell();
                        //                    HtmlTableCell origin1Cell = new HtmlTableCell();
                        //                    HtmlTableCell dest1Cell = new HtmlTableCell();
                        //                    HtmlTableCell dura1Cell = new HtmlTableCell();
                        //                    HtmlTableCell connectCell = new HtmlTableCell();
                        //                    HtmlTableCell aircraft1Cell = new HtmlTableCell();

                        //                    //flight details code
                        //                    logo1Cell.Width = "40px";
                        //                    logo1Cell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='"+ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logo1Cell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logo1Cell.Align = "Center";
                        //                    logo1Cell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    logo1Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber;

                        //                    origin1Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dest1Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dura1Cell.Attributes.Add("style", "width:14%;text-align:center;");

                        //                    connectCell.ColSpan = 5;

                        //                    aircraft1Cell.RowSpan = 1;
                        //                    aircraft1Cell.Attributes.Add("style", "width:20%;");


                        //                    origin1Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    origin1Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    dest1Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    dest1Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);

                        //                    dura1Cell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";
                        //                    aircraft1Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                        //                    TimeSpan connectDuration = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                        //                    connectCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + connectDuration.Hours + "hrs " + connectDuration.Minutes + "mins</strong> ";
                        //                    connectCell.InnerHtml += "<span style='color:#CCCCCC'>|</span> Connecting flight may departs from different Terminal</div>";

                        //                    //flight details code
                        //                    connectingRow.Cells.Add(connectCell);
                        //                    flightDetails.Rows.Add(connectingRow);

                        //                    secondRow.Cells.Add(logo1Cell);
                        //                    secondRow.Cells.Add(origin1Cell);
                        //                    secondRow.Cells.Add(dest1Cell);
                        //                    secondRow.Cells.Add(dura1Cell);
                        //                    flightDetails.Rows.Add(secondRow);


                        //                    break;
                        //                case 2://Two stops
                        //                    HtmlTableCell logo2Cell = new HtmlTableCell();
                        //                    HtmlTableCell origin2Cell = new HtmlTableCell();
                        //                    HtmlTableCell dest2Cell = new HtmlTableCell();
                        //                    HtmlTableCell dura2Cell = new HtmlTableCell();
                        //                    HtmlTableCell aircraft2Cell = new HtmlTableCell();
                        //                    HtmlTableCell connect2Cell = new HtmlTableCell();
                        //                    HtmlTableRow connectingRow2 = new HtmlTableRow();
                        //                    origin2Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dest2Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dura2Cell.Attributes.Add("style", "width:14%;text-align:center;");

                        //                    //flight details code
                        //                    logo2Cell.Width = "40px";
                        //                    logo2Cell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='" + ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logo2Cell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logo2Cell.Align = "Center";
                        //                    logo2Cell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    logo2Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber;

                        //                    connect2Cell.ColSpan = 5;

                        //                    aircraft2Cell.RowSpan = 1;
                        //                    aircraft2Cell.Attributes.Add("style", "width:20%;");


                        //                    origin2Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    origin2Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    dest2Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    dest2Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);

                        //                    dura2Cell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";
                        //                    aircraft2Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");

                        //                    TimeSpan connectDuration3 = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                        //                    connect2Cell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + connectDuration3.Hours + "hrs " + connectDuration3.Minutes + "mins</strong> ";
                        //                    connect2Cell.InnerHtml += "<span style='color:#CCCCCC'>|</span> Connecting flight may departs from different Terminal</div>";

                        //                    //flight details code
                        //                    connectingRow2.Cells.Add(connect2Cell);
                        //                    flightDetails.Rows.Add(connectingRow2);

                        //                    secondRow.Cells.Add(logo2Cell);
                        //                    thirdRow.Cells.Add(origin2Cell);
                        //                    thirdRow.Cells.Add(dest2Cell);
                        //                    thirdRow.Cells.Add(dura2Cell);
                        //                    flightDetails.Rows.Add(thirdRow);
                        //                    break;
                        //            }
                        //        }
                        //    }
                        //    else
                        //    {
                        //        DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());


                        //        HtmlTableRow seperationRow = new HtmlTableRow();
                        //        HtmlTableCell seperationCell = new HtmlTableCell();
                        //        seperationCell.ColSpan = 4;
                        //        seperationCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>";
                        //        HtmlTableCell priceCell = new HtmlTableCell();
                        //        priceCell.Align = "center";
                        //        priceCell.InnerHtml = "<div id='ViewFlight" + resultObj.ResultId + "div' ><span style='font-weight: bold;text-align:center;'><label style='font-size: 14px;'><span style='color: #FF0000'>AED</span><br/><span>" + resultObj.TotalFare.ToString("N3") + "</span></label></span>";

                        //        seperationRow.Cells.Add(seperationCell);
                        //        seperationRow.Cells.Add(priceCell);
                        //        flightDetails.Rows.Add(seperationRow);

                        //        for (int j = 0; j < resultObj.Flights[i].Length; j++)
                        //        {


                        //            switch (j)
                        //            {
                        //                case 0://Non Stop
                        //                    //flight details code
                        //                    HtmlTableCell logo1Cell = new HtmlTableCell();
                        //                    logo1Cell.Width = "40px";
                        //                    logo1Cell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='" + ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logo1Cell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logo1Cell.Align = "Center";
                        //                    logo1Cell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    logo1Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "<br/>";
                        //                    //logo1Cell.InnerHtml += logoInfo.Key.ToString() + " " + resultObj.Flights[i][j].FlightNumber + "</br>";

                        //                    HtmlTableCell originCell = new HtmlTableCell();
                        //                    HtmlTableCell destCell = new HtmlTableCell();
                        //                    HtmlTableCell duraCell = new HtmlTableCell();
                        //                    HtmlTableCell aircraftCell = new HtmlTableCell();

                        //                    originCell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    destCell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    duraCell.Attributes.Add("style", "width:14%;text-align:center;");
                        //                    aircraftCell.RowSpan = 1;
                        //                    aircraftCell.Attributes.Add("style", "width:20%;");


                        //                    originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    originCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    destCell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);

                        //                    duraCell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";
                        //                    aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                        //                    //flight details code
                        //                    fourthRow.Cells.Add(logo1Cell);
                        //                    fourthRow.Cells.Add(originCell);
                        //                    fourthRow.Cells.Add(destCell);
                        //                    fourthRow.Cells.Add(duraCell);
                        //                    fourthRow.Cells.Add(aircraftCell);
                        //                    flightDetails.Rows.Add(fourthRow);
                        //                    break;
                        //                case 1://Single Stop
                        //                    HtmlTableCell logo2Cell = new HtmlTableCell();
                        //                    logo2Cell.Width = "40px";
                        //                    logo2Cell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='"+ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logo2Cell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logo2Cell.Align = "Center";
                        //                    logo2Cell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    logo2Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "<br/>";

                        //                    HtmlTableCell origin1Cell = new HtmlTableCell();
                        //                    HtmlTableCell dest1Cell = new HtmlTableCell();
                        //                    HtmlTableCell dura1Cell = new HtmlTableCell();
                        //                    HtmlTableCell aircraft1Cell = new HtmlTableCell();
                        //                    HtmlTableCell connectCell = new HtmlTableCell();
                        //                    connectCell.ColSpan = 5;

                        //                    origin1Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dest1Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dura1Cell.Attributes.Add("style", "width:14%;text-align:center;");
                        //                    aircraft1Cell.RowSpan = 1;
                        //                    aircraft1Cell.Attributes.Add("style", "width:20%;");


                        //                    origin1Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    origin1Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    dest1Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    dest1Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    TimeSpan duration1 = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);

                        //                    dura1Cell.InnerHtml = duration1.Hours + "hrs " + duration1.Minutes + "mins";
                        //                    aircraft1Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");

                        //                    TimeSpan connectDuration = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                        //                    connectCell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + connectDuration.Hours + "hrs " + connectDuration.Minutes + "mins</strong> ";
                        //                    connectCell.InnerHtml += "<span style='color:#CCCCCC'>|</span> Connecting flight may departs from different Terminal</div>";

                        //                    //flight details code
                        //                    connectingRow.Cells.Add(connectCell);
                        //                    flightDetails.Rows.Add(connectingRow);
                        //                    //flight details code
                        //                    fifthRow.Cells.Add(logo2Cell);
                        //                    fifthRow.Cells.Add(origin1Cell);
                        //                    fifthRow.Cells.Add(dest1Cell);
                        //                    fifthRow.Cells.Add(dura1Cell);
                        //                    flightDetails.Rows.Add(fifthRow);
                        //                    break;
                        //                case 2://Two Stops
                        //                    //@@@@
                        //                    HtmlTableCell logo3Cell = new HtmlTableCell();
                        //                    logo3Cell.Width = "40px";
                        //                    logo3Cell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='" + ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logo3Cell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logo3Cell.Align = "Center";
                        //                    logo3Cell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    logo3Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "<br/>";

                        //                    HtmlTableCell origin2Cell = new HtmlTableCell();
                        //                    HtmlTableCell dest2Cell = new HtmlTableCell();
                        //                    HtmlTableCell dura2Cell = new HtmlTableCell();
                        //                    HtmlTableCell aircraft2Cell = new HtmlTableCell();
                        //                    HtmlTableCell connect3Cell = new HtmlTableCell();
                        //                    connect3Cell.ColSpan = 5;

                        //                    origin2Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dest2Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dura2Cell.Attributes.Add("style", "width:14%;text-align:center;");
                        //                    aircraft2Cell.RowSpan = 1;
                        //                    aircraft2Cell.Attributes.Add("style", "width:20%;");


                        //                    origin2Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    origin2Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    dest2Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    dest2Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    TimeSpan duration2 = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);

                        //                    dura2Cell.InnerHtml = duration2.Hours + "hrs " + duration2.Minutes + "mins";
                        //                    aircraft2Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");

                        //                    TimeSpan connectDuration2 = resultObj.Flights[i][j].GroundTime;
                        //                    connect3Cell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>"+connectDuration2.Days+" Days, " + connectDuration2.Hours + " hrs " + connectDuration2.Minutes + " mins</strong> ";
                        //                    connect3Cell.InnerHtml += "<span style='color:#CCCCCC'>|</span> Connecting flight may departs from different Terminal</div>";
                        //                    HtmlTableRow connectingRow3 = new HtmlTableRow();
                        //                    //flight details code
                        //                    connectingRow3.Cells.Add(connect3Cell);
                        //                    flightDetails.Rows.Add(connectingRow3);
                        //                    //flight details code
                        //                    sixthRow.Cells.Add(logo3Cell);
                        //                    sixthRow.Cells.Add(origin2Cell);
                        //                    sixthRow.Cells.Add(dest2Cell);
                        //                    sixthRow.Cells.Add(dura2Cell);
                        //                    flightDetails.Rows.Add(sixthRow);
                        //                    break;
                        //                case 3:
                        //                    HtmlTableCell logo4Cell = new HtmlTableCell();
                        //                    logo4Cell.Width = "40px";
                        //                    logo4Cell.InnerHtml = "<span style='width:19%; text-align:left; padding-left:28px;'><img src='" + ConfigurationManager.AppSettings["RootFolder"] + logoInfo.Value.ToString() + "' width='20' height='35'/></span><br/>";
                        //                    logo4Cell.InnerHtml += logoInfo.Key.ToString() + "<br/>";
                        //                    logo4Cell.Align = "Center";
                        //                    logo4Cell.Attributes.Add("style", "font-size:12px;  width:20%; text-align:center;");
                        //                    logo4Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "<br/>";

                        //                    HtmlTableCell origin3Cell = new HtmlTableCell();
                        //                    HtmlTableCell dest3Cell = new HtmlTableCell();
                        //                    HtmlTableCell dura3Cell = new HtmlTableCell();
                        //                    HtmlTableCell aircraft3Cell = new HtmlTableCell();
                        //                    HtmlTableCell connect4Cell = new HtmlTableCell();
                        //                    connect4Cell.ColSpan = 5;

                        //                    origin3Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dest3Cell.Attributes.Add("style", "width:23%;text-align:center;");
                        //                    dura3Cell.Attributes.Add("style", "width:14%;text-align:center;");
                        //                    aircraft3Cell.RowSpan = 1;
                        //                    aircraft3Cell.Attributes.Add("style", "width:20%;");


                        //                    origin3Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                        //                    origin3Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");

                        //                    dest3Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                        //                    dest3Cell.InnerHtml += "<br/>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                        //                    //dura3Cell.InnerHtml = duration3.Hours + "hrs " + duration3.Minutes + "mins";

                        //                    dura3Cell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes+"mins";
                        //                    aircraft3Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span style='color:#CCCCCC'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span style='color:#CCCCCC'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");

                        //                    TimeSpan connectDuration3 = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                        //                    connect4Cell.InnerHtml = "<div style='border-bottom: solid 1px #ccc;  border-top: solid 1px #ccc; background:url(../images/flight_2_icon.jpg) right no-repeat; padding-right:30px; line-height:25px; margin:5px; color:#999999;'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + connectDuration3.Hours + "hrs " + connectDuration3.Minutes + "mins</strong> ";
                        //                    connect4Cell.InnerHtml += "<span style='color:#CCCCCC'>|</span> Connecting flight may departs from different Terminal</div>";
                        //                    HtmlTableRow connectingRow4 = new HtmlTableRow();
                        //                    //flight details code
                        //                    connectingRow4.Cells.Add(connect4Cell);
                        //                    flightDetails.Rows.Add(connectingRow4);
                        //                    //flight details code
                        //                    seventhRow.Cells.Add(logo4Cell);
                        //                    seventhRow.Cells.Add(origin3Cell);
                        //                    seventhRow.Cells.Add(dest3Cell);
                        //                    seventhRow.Cells.Add(dura3Cell);
                        //                    flightDetails.Rows.Add(seventhRow);
                        //                    break;

                        //            }
                        //        }
                        //    }
                        //}
                        //End

                        #endregion
                        StringWriter sw = new StringWriter();
                        HtmlTextWriter htw = new HtmlTextWriter(sw);
                        ViewFlight.RenderControl(htw);
                        emailBody = sw.ToString();
                        emailBody = emailBody.Replace("none", "block");
                        string subject = origin + "(" + date[0] + ") for Departure - " + destination + endDate + ": Requested Flight Details";
                        //Email.Send(Settings.LoginInfo.AgentEmail, addressList, subject, emailBody);
                        Email.Send(ConfigurationManager.AppSettings["fromEmail"], addressList, subject, emailBody);
                        emailBody = "";
                        flightDetails.Rows.Clear();
                        paxTable.Rows.Clear();
                    }




                    Response.Write("Email Sent");
                }

                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
            }
        }
        #endregion


        #region HotelDetailsMail
        else if (sourceName == "Hotel Details")
        {
            pageTitle.InnerHtml = "";

            HotelRequest hotelrequest = new HotelRequest();

            string addressList = Request["addressList"];
            double markup = Convert.ToDouble(Request["totalMarkup"] == "" ? "0" : Request["totalMarkup"]);
            string[] roomId = Request["roomTypeNo"].ToString().Split(',');
            string emailBody = "";
            decimal TotalPrice=0;

            if (Session["SelectedHotel"] != null)
            {
                hotelrequest = (HotelRequest)Session["req"];
                HotelSearchResult hotelResultObj = Session["SelectedHotel"] as HotelSearchResult;
                HtmlTableRow rowNumber1 = new HtmlTableRow();
                HtmlTableRow rowNumber2 = new HtmlTableRow();
                HtmlTableRow rowNumber3 = new HtmlTableRow();
                HtmlTableRow rowNumber4 = new HtmlTableRow();
                HtmlTableRow rowNumber5 = new HtmlTableRow();
                HtmlTableCell hotelDetails = new HtmlTableCell(); HtmlTableCell hotelName = new HtmlTableCell();
                HtmlTableCell horizentalRule = new HtmlTableCell();
                HtmlTableCell lineBreak = new HtmlTableCell(); HtmlTableCell priceDetails = new HtmlTableCell();
                HtmlTableCell hotelDescriptions = new HtmlTableCell();

                //For Logo
                string logoPath = "";
                //string serverPath = "";
                //serverPath = "http://ctb2bstage.cozmotravel.com/";
                //to show agent logo
                if (agentId > 0)
                {
                    logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                }

                //For Title Of Page
                pageTitle.InnerHtml = "";
                pageTitle.InnerHtml += "<div style='margin-top:5px;' ><img src='" + logoPath + "' width='180px' height='65px'/></div>";
                pageTitle.InnerHtml += "<div style='height:30px;margin-top:0px; width:100%; background-color:#1C498A; color:#ffffff; font-weight:bold;'> Hotel Itinerary Details</div>";


                //End Page Title



                hotelName.Attributes.Add("style", "width:100%;font-size:20px;");
                hotelName.InnerHtml = "<strong>" + hotelResultObj.HotelName + "</strong><br/><br/>";
                hotelName.ColSpan = 2;
                hotelName.Align = "center";


                lineBreak.InnerHtml = "<br/>";

                priceDetails.Attributes.Add("style", "width:100%;text-align:right; padding-right:30px;");
                priceDetails.ColSpan = 2;
                for (int j = 0; j < roomId.Length; j++)
                {
                    TotalPrice += hotelResultObj.RoomDetails[Convert.ToInt32(roomId[j])].SellingFare + hotelResultObj.RoomDetails[Convert.ToInt32(roomId[j])].Markup;
                }
                TotalPrice +=Convert.ToDecimal(markup);
                priceDetails.InnerHtml = "<span style='font-size:16px;font-weight:bold;color:#ffffff;'>Total Price: </span> <strong><span style='color:#ffffff;font-weight:bold;font-size:18px;'>" + hotelResultObj.Currency + " " + Math.Ceiling(TotalPrice).ToString("N2") + "</span></strong>";
                priceDetails.InnerHtml += "<br/><br/><span style='font-weight:bold;font-size:14px;'>Star Type: " + hotelResultObj.Rating + "</span>";
                priceDetails.InnerHtml += "<br/><br/><span style='font-weight:bold;font-size:14px;'>" + hotelResultObj.HotelAddress + "</span>";
                if (hotelResultObj.HotelContactNo != null)
                {
                    priceDetails.InnerHtml += "<br/><span style='font-weight:bold;font-size:14px;'>" + hotelResultObj.HotelContactNo + "</span>";
                }
                hotelDescriptions.Attributes.Add("style", "width:100%;text-align:left; font-size:14px; padding:10px 10px 10px 10px;");
                hotelDescriptions.ColSpan = 2;
                hotelDescriptions.InnerHtml += "<br/><br/>" + hotelResultObj.HotelDescription;

                horizentalRule.Attributes.Add("style", "width:100%;");
                horizentalRule.ColSpan = 2;
                horizentalRule.InnerHtml += "<hr/>";
                //Hotel Details
                hotelDetails.Attributes.Add("style", "width:40%;font-size:14px; padding-left:5px;");
                hotelDetails.InnerHtml = "<div style='width:100%; height:20px; background-color:#1C498A; color:#ffffff;'><h4> Hotel Details</h4></div>";
                hotelDetails.InnerHtml += "<table>";
                hotelDetails.InnerHtml += "<tr><td style='width:10%;font-size:13px;'>City Name:</td> <td style='text-align:left; width:30%;font-weight:bold;font-size:14px;'>" + hotelrequest.CityName + "</span></td></tr>";
                hotelDetails.InnerHtml += "<tr><td style='width:10%;font-size:13px;'>Check In:</td> <td style='text-align:left; width:30%;font-weight:bold;font-size:14px;'>" + hotelResultObj.StartDate.ToString("dd/MM/yyyy") + "</span></td></tr>";
                hotelDetails.InnerHtml += "<tr><td style='width:10%;font-size:13px;'>Check Out:</td> <td style='text-align:left; width:30%;font-weight:bold;font-size:14px;'>" + hotelResultObj.EndDate.ToString("dd/MM/yyyy") + "</span></td></tr>"; ;
                hotelDetails.InnerHtml += "<tr></tr>";
                hotelDetails.InnerHtml += "<tr></tr>";
                hotelDetails.InnerHtml += "<tr><td style='width:10%;font-size:13px;'>Total Room(s):</td> <td style='text-align:left; width:30%;font-weight:bold;font-size:14px;'>" + hotelrequest.NoOfRooms + "</span></td></tr>";
                hotelDetails.InnerHtml += "</table>";


                //int roomNo=0;
                hotelDetails.InnerHtml += "<div style='width:100%; height:20px; background-color:#1C498A; color:#ffffff;'><h4> Room Details</h4></div>";
                 int noOfguests = 0;

                    
                    for (int k = 0; k < roomId.Length; k++)
                    {
                        hotelDetails.InnerHtml += "Room <strong>" + (k + 1) + "</strong>";
                        noOfguests = ((hotelrequest.RoomGuest[k].noOfAdults) + (hotelrequest.RoomGuest[k].noOfChild));
                        hotelDetails.InnerHtml += "<br/>No. Of Guest(s): <strong>" + noOfguests + "</strong>, (Adult(s) <strong>" + hotelrequest.RoomGuest[k].noOfAdults + "</strong>, Child(ren) <strong>" + hotelrequest.RoomGuest[k].noOfChild + "</strong> )";
                        int roomIndex = Convert.ToInt32(roomId[k]);
                        hotelDetails.InnerHtml += "<br/>Room Name: <span style='font-weight:bold;'>" + hotelResultObj.RoomDetails[roomIndex].RoomTypeName + "</span>";


                        hotelDetails.InnerHtml += "<br/>Meals Plan: <span style='font-weight:bold;'>" + hotelResultObj.RoomDetails[roomIndex].mealPlanDesc + "</span><hr/>";
                    }
               
                



                rowNumber1.Cells.Add(hotelName);
                //rowNumber3.Cells.Add(lineBreak);
                rowNumber2.Cells.Add(priceDetails);
                rowNumber3.Cells.Add(hotelDescriptions);
                rowNumber4.Cells.Add(horizentalRule);
                rowNumber5.Cells.Add(hotelDetails);


                flightDetails.Rows.Add(rowNumber1);
                flightDetails.Rows.Add(rowNumber2);
                flightDetails.Rows.Add(rowNumber3);
                flightDetails.Rows.Add(rowNumber4);
                flightDetails.Rows.Add(rowNumber5);


                #region Fare Summary

                //if (count == 1)
                //{
                HtmlTableRow paxRow = new HtmlTableRow();
                 HtmlTableCell paxNoCell = new HtmlTableCell();
                 paxNoCell.InnerHtml = "<div style='width:100%; height:20px; background-color:#1C498A; color:#ffffff;'><h2> Fare Summary</h2></div>";
                paxNoCell.InnerHtml += "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr style='background-color:#E8E8E8 ; font-size:12px;'><td><b>Room</b></td><td><b>RoomType</b></td><td><b>Room Price</b></td><td><b>Tax & Fees</b></td><td><b>Total Fare</b></td></tr>";
                //paxNoCell.InnerHtml += "<td style='width:20%'>Total</td></tr>";
               
              
                decimal roomBreakMarkup = Convert.ToDecimal(markup);
                roomBreakMarkup = Math.Round(roomBreakMarkup / roomId.Length,3);

               
                //adultBreakMarkup = Math.Ceiling(adultBreakMarkup);
                for (int k = 0; k < roomId.Length; k++)
                {
                      int roomIndex = Convert.ToInt32(roomId[k]);
                      paxNoCell.InnerHtml += "<tr ><td>Room " + (k + 1) + "</td><td>" + hotelResultObj.RoomDetails[roomIndex].RoomTypeName + "</td><td>" + (hotelResultObj.RoomDetails[roomIndex].SellingFare).ToString("N2")+"</td><td>" + (hotelResultObj.RoomDetails[roomIndex].TotalTax + roomBreakMarkup).ToString("N2") + "</td><td>" + (hotelResultObj.RoomDetails[roomIndex].TotalPrice + hotelResultObj.RoomDetails[roomIndex].Markup + roomBreakMarkup).ToString("N2") + "</td></tr>";
                    
                    
                }
                paxNoCell.InnerHtml += "<br/><tr><td colspan='5' style='text-align:right; margin-right:5px; font-size:20px; font-weight:bold;'><strong>Total Net Fare: " + hotelResultObj.Currency + " " + Math.Ceiling(TotalPrice).ToString("N2") + "</strong></td></tr>";
                
                paxNoCell.InnerHtml += "<br/><tr><td colspan='5'><div><sup>*</sup> There might a slight difference in the total price in fare summary due to rounding off.</div></td></tr>";
                paxNoCell.InnerHtml += "<br/><tr><td colspan='5'><div style='text-align:center;'><sup>*</sup><strong>Note: This is a system generated email. We request you not to reply to this email.</strong> </div></td></tr></table>";



               
                paxRow.Cells.Add(paxNoCell);

                paxTable.Rows.Add(paxRow);
                //count++;
                // }
                //}
                #endregion

                string subject = hotelResultObj.HotelName + " (Check In: " + hotelrequest.StartDate.ToString("dd/MM/yyyy") + ") - (Check Out: " + hotelrequest.EndDate.ToString("dd/MM/yyyy") + ") :Request For Hotel Details.";
                //Send Email
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                ViewFlight.RenderControl(htw);
                emailBody = sw.ToString();
                emailBody = emailBody.Replace("none", "block");
                Email.Send(ConfigurationManager.AppSettings["fromEmail"], addressList, subject, emailBody);
                emailBody = "";
                flightDetails.Rows.Clear();


            }

            Response.Write("Email Sent");

        }
        #endregion

        //When we email E-Ticket
        #region When we email E-Ticket
        else if (Request["isEticket"] != null && Convert.ToBoolean(Request["isEticket"]))
        {
            #region Loading all the objects required for E-Ticket
            ticket = new Ticket();
            List<Ticket> ticketList = new List<Ticket>();
            int ticketId = 0;
            int flightId = 0;
            if (Request["ticketId"] != null)
            {
                ticketId = Convert.ToInt32(Request["ticketId"]);
                ticket.Load(ticketId);
            }
            else
            {
                flightId = Convert.ToInt32(Request["flightId"]);
                ticketList = Ticket.GetTicketList(flightId);
                ticket = ticketList[0];
                ticket.PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(flightId);
            }

            flightItinerary = new FlightItinerary(ticket.FlightId);
            for (int i = 0; i < flightItinerary.Passenger.Length; i++)
            {
                if (flightItinerary.Passenger[i].PaxId == ticket.PaxId)
                {
                    paxIndex = i;
                    break;
                }
            }
            if (paxIndex >= 0)
            {
                paxName = FlightPassenger.GetPaxFullName(flightItinerary.Passenger[paxIndex].PaxId);
            }
            else
            {
                paxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
            }
            paxName = paxName.Trim();

            int agencyId = Ticket.GetAgencyIdForTicket(ticket.TicketId);
            agency = new AgentMaster(agencyId);
            int bookedBy = (int)agency.ID;
            preference = preference.GetPreference(bookedBy, preference.ETicket, preference.ETicket);
            if (preference.Value != null)
            {
                preferenceValue = preference.Value.ToUpper();
            }
            else
            {
                preferenceValue = preference.ETicketShowFare;
            }
            //if (agency.City.Length > 0)
            //{
            //    cityName = RegCity.GetCity(agency.Address.CityId).CityName;
            //}
            //else
            {
                cityName = agency.City;
            }
            //logo = AgentMaster.GetAgencyLogo(agency.ID);
            for (int i = 0; i < ticket.PtcDetail.Count; i++)
            {
                if (ticket.PaxType == FlightPassenger.GetPassengerType(ticket.PtcDetail[i].PaxType))
                {
                    paxTypeIndex = i;
                    break;
                }
            }
            //policy = PolicyDetail.GetPolicyDetailByPaxId(flightItinerary.Passenger[paxIndex].PaxId);

            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
            {
                foreach (FlightPassenger pax in flightItinerary.Passenger)
                {

                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (outBaggage.Length > 0)
                    {
                        outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                    }
                    else
                    {
                        outBaggage = pax.BaggageCode.Split(',')[1];
                    }
                }
            }

            string phoneString = string.Empty;
            if (agency.Phone1 != null && agency.Phone1.Length > 0)
            {
                phoneString = agency.Phone1;
            }
            string eTicket = "Itinerary";
            string message = "This is not an E-Ticket. Please collect your ticket from your travel agent";
            string validString = "(This document is not valid for travel)";
            if (ticket.ETicket)
            {
                eTicket = "E-Ticket";
                message = "This is an electronic ticket. Please carry a positive identification for check in. ";
                validString = "";
            }
            if (logo.Length != 0)
            {
                // logo = "<div style=\"float: left; width: 67px\"><img alt=\"Agent Logo\" height=\"60px\" src=\"C:/Root/Source/CT/Projects/CT/TekTravelWeb/UserImages/" + logo + "\" width=\"60px\" /></div>";
                // logo = "http://localhost:1507/TekTravelWeb/userimages/param.jpg";// + logo;
                // C:/Root/Source/CT/Projects/CT/TekTravelWeb/UserImages/
            }
            else
            {
                logo = "";
            }

            #endregion
            // calculating IsLcc
            bool isLCC = false;
            string code = string.Empty;
            string airLineLogo = string.Empty;
            string airLineName = string.Empty;
            if (flightItinerary.AliasAirlineCode.Equals("ITR"))
            {
                isDN = true;
            }
            foreach (FlightInfo segment in flightItinerary.Segments)
            {
                string airlineCode = segment.Airline;
                Airline airline = new Airline();
                airline.Load(airlineCode);
                if (airline.AirlineCode == flightItinerary.AirlineCode && airLineName == "")
                {
                    airLineName = airline.AirlineName;
                }
                if (airline.LogoFile.Trim().Length != 0)
                {
                    string[] fileExtension = airline.LogoFile.Split('.');
                    airLineLogo = flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                }

                if (airline.IsLCC)
                {
                    isLCC = true;
                    code = airline.AirlineCode;
                    break;
                }
                else
                {
                    if (flightItinerary.FlightBookingSource == BookingSource.WorldSpan)
                    {
                        code = "1P";
                    }
                    else if (flightItinerary.FlightBookingSource == BookingSource.Amadeus)
                    {
                        code = "1A";
                    }
                    else if (flightItinerary.FlightBookingSource == BookingSource.Galileo)
                    {
                        code = "1G";
                    }
                    else if (flightItinerary.FlightBookingSource == BookingSource.Abacus)
                    {
                        code = "1B";
                    }
                }
            }

            //This is a global hash table which we send in Email.Send which replaces 
            //hashvariables with values(code which is not in loop)
            Hashtable globalHashTable = new Hashtable();

            string agentLogo = "";
            string airlinelogo = "";
            UserPreference pref = preference.GetPreference(bookedBy, preference.ShowLogoOnEticket, preference.ETicket);
            if (pref.Value != null)
            {
                if (pref.Value.ToUpper() == "TRUE")
                {
                    agentLogo = "<div style=\"float: left; width: 100%; text-align:right\"><img alt=\"Agent Logo\" src=\"" + ConfigurationSystem.Core["virtualAgentLogoPath"] + "\\" + logo + "\"/></div>";
                    airlinelogo = "<div style=\"float: left; width: 100%\"><img alt=\"AirLine Logo\" src=\"" + ConfigurationManager.AppSettings["AirlineLogoPath"] + airLineLogo + "\"/></div>";
                }
            }
            globalHashTable.Add("logo", agentLogo);
            globalHashTable.Add("airLogo", airlinelogo);
            globalHashTable.Add("airLineName", airLineName);
            globalHashTable.Add("agencyName", agency.Name);
            globalHashTable.Add("addressLine1", agency.Address);
            globalHashTable.Add("addressLine2", "");
            globalHashTable.Add("cityName", cityName);
            globalHashTable.Add("addressPin", agency.POBox);
            if (!isDN)
            {
                globalHashTable.Add("segmentMessage", String.Empty);
            }
            else
            {
                segmentMessage = "<h4><b>" + ConfigurationSystem.AirDeccanConfig["FlightMessage"] + "</b></h4>";
                globalHashTable.Add("segmentMessage", segmentMessage);
            }
            if (agency.Phone1 != null && agency.Phone1.Length != 0)
            {
                globalHashTable.Add("addressPhone", "Phone:" + agency.Phone1);
            }
            else
            {
                globalHashTable.Add("addressPhone", "");
            }

            if (ticket.Status == "Cancelled" || ticket.Status == "Voided" || ticket.Status == "Refunded")
            {
                string temp = "<tr><td colspan=\"2\" style=\"font-weight:bold; font-size:16px;\">" +
                             "<span> Ticket Status : Cancelled	</span> </td></tr>";

                globalHashTable.Add("ticketStatus", temp);
            }
            else
            {
                globalHashTable.Add("ticketStatus", "");
            }

            string ticketDetails = string.Empty;

            globalHashTable.Add("eTicketString", eTicket);
            globalHashTable.Add("validString", validString);
            if (flightItinerary.FlightBookingSource == BookingSource.HermesAirLine)
            {
                globalHashTable.Add("PNR", ticket.ValidatingAriline + " - " + flightItinerary.Segments[0].AirlinePNR);
            }
            else
            {
                globalHashTable.Add("PNR", code + " - " + flightItinerary.PNR);
            }
            globalHashTable.Add("issueDate", ticket.IssueDate.ToString("ddd dd MMM yyyy"));
            globalHashTable.Add("paxName", paxName);
            if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.WorldSpan || flightItinerary.FlightBookingSource == BookingSource.Galileo || flightItinerary.FlightBookingSource == BookingSource.Paramount || flightItinerary.FlightBookingSource == BookingSource.Mdlr)
            {
                globalHashTable.Add("ticketNumber", "Ticket Number: " + ticket.ValidatingAriline + ticket.TicketNumber);
            }
            else
            {
                globalHashTable.Add("ticketNumber", "Reference Number: " + flightItinerary.PNR);
            }
            string ffNumber = flightItinerary.Passenger[paxIndex].FFAirline + flightItinerary.Passenger[paxIndex].FFNumber;
            if (ffNumber != null && ffNumber.Trim() != string.Empty)
            {
                globalHashTable.Add("FQTV", "FQTV : " + ffNumber);
            }
            else
            {
                globalHashTable.Add("FQTV", "");
            }
            globalHashTable.Add("message", message);
            string transactionFee = string.Empty;

            string fareString = string.Empty;

            string ticketNo = string.Empty;

            if (ticketList.Count > 0)
            {
                for (int p = 0; p < ticketList.Count; p++)
                {
                    if (!isLCC)
                    {
                        ticketNo = ticketList[p].ValidatingAriline + ticketList[p].TicketNumber;
                    }
                    else
                    {
                        ticketNo = ticketList[p].TicketNumber;
                    }
                    if (ticketList[p].Status != "Cancelled" && ticketList[p].Status != "Voided" && ticketList[p].Status != "Refunded")
                    {
                        paxName = FlightPassenger.GetPaxFullName(ticketList[p].PaxId);
                        ticketDetails += "<tr><td>" + paxName + "</td><td>" +
                        ticketNo + "</td></tr>";
                    }
                }
            }
            else
            {
                if (!isLCC)
                {
                    ticketNo = ticket.ValidatingAriline + ticket.TicketNumber;
                }
                else
                {
                    ticketNo = ticket.TicketNumber;
                }

                ticketDetails += "<tr><td>" + paxName + "</td><td>" + ticketNo + "</td></tr>";
            }

            if (preferenceValue == preference.ETicketShowFare)
            {
                decimal totalFare = 0;
                //decimal insurancePrice = 0;
                decimal serviceFee = 0;
                bool showServiceFee = false;
                if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                {
                    showServiceFee = true;
                }
                else
                {
                    showServiceFee = false;
                }
                decimal totalTax = 0;
                decimal publishFare = 0;
                decimal additionalTxnFee = 0;
                decimal baggageCharge = 0;

                decimal totalTransactionFee = 0;
                if (ticketList.Count > 0)
                {
                    for (int j = 0; j < ticketList.Count; j++)
                    {
                        if (ticketList[j].Status != "Cancelled" && ticketList[j].Status != "Voided" && ticketList[j].Status != "Refunded")
                        {
                            if (flightItinerary.BookingMode == BookingMode.WhiteLabel || flightItinerary.BookingMode == BookingMode.Itimes)
                            {
                                totalTax += ticketList[j].Price.Tax + ticketList[j].Price.OtherCharges + ticketList[j].ServiceFee + ticketList[j].Price.SeviceTax + ticketList[j].Price.WLCharge;
                            }
                            else
                            {
                                if (showServiceFee)
                                {
                                    totalTax += Convert.ToDecimal(ticketList[j].Price.Tax + ticketList[j].Price.OtherCharges);
                                }
                                else
                                {
                                    totalTax += Convert.ToDecimal(ticketList[j].Price.Tax + ticketList[j].Price.OtherCharges + ticketList[j].ServiceFee);
                                }
                            }
                            if (flightItinerary.BookingMode == BookingMode.WhiteLabel || flightItinerary.BookingMode == BookingMode.Itimes)
                            {
                                totalFare += ticketList[j].Price.PublishedFare + ticketList[j].Price.Tax + ticketList[j].Price.AdditionalTxnFee + ticketList[j].Price.OtherCharges + ticketList[j].ServiceFee + ticketList[j].Price.SeviceTax + ticketList[j].Price.WLCharge;
                            }
                            else
                            {
                                totalFare += Convert.ToDecimal(ticketList[j].Price.PublishedFare + ticketList[j].Price.Tax + ticketList[j].Price.AdditionalTxnFee + ticketList[j].Price.OtherCharges + ticketList[j].Price.TransactionFee + ticketList[j].ServiceFee);//+ insurancePrice
                            }
                            additionalTxnFee += ticketList[j].Price.AdditionalTxnFee;
                            publishFare += ticketList[j].Price.PublishedFare;
                            serviceFee += ticketList[j].ServiceFee;
                            totalTransactionFee += ticketList[j].Price.TransactionFee;
                            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                            {
                                totalFare += ticketList[j].Price.BaggageCharge;
                                baggageCharge += ticketList[j].Price.BaggageCharge;
                            }
                        }
                    }
                }
                else
                {
                    if (showServiceFee)
                    {
                        totalTax = Convert.ToDecimal(ticket.Price.Tax + ticket.Price.OtherCharges);
                    }
                    else
                    {
                        totalTax = Convert.ToDecimal(ticket.Price.Tax + ticket.Price.OtherCharges + ticket.ServiceFee);
                    }
                    if (flightItinerary.BookingMode == BookingMode.WhiteLabel || flightItinerary.BookingMode == BookingMode.Itimes)
                    {
                        totalTax = ticket.Price.Tax + ticket.Price.OtherCharges + ticket.ServiceFee + ticket.Price.SeviceTax + ticket.Price.WLCharge;
                    }
                    totalFare = Convert.ToDecimal(ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.AdditionalTxnFee + ticket.Price.OtherCharges + ticket.Price.TransactionFee + ticket.ServiceFee);//+ insurancePrice
                    if (flightItinerary.BookingMode == BookingMode.WhiteLabel || flightItinerary.BookingMode == BookingMode.Itimes)
                    {
                        totalFare = ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.AdditionalTxnFee + ticket.Price.OtherCharges + ticket.ServiceFee + ticket.Price.SeviceTax + ticket.Price.WLCharge;
                    }

                    additionalTxnFee = ticket.Price.AdditionalTxnFee;
                    publishFare = ticket.Price.PublishedFare;
                    serviceFee = ticket.ServiceFee;
                    totalTransactionFee = ticket.Price.TransactionFee;
                    if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                    {
                        totalFare += ticket.Price.BaggageCharge;
                        baggageCharge += ticket.Price.BaggageCharge;
                    }
                }

                string AmountPrefix = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
                if (flightItinerary.Passenger[0].Price.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                {
                    AmountPrefix = flightItinerary.Passenger[0].Price.Currency;
                    totalTransactionFee = totalTransactionFee / flightItinerary.Passenger[0].Price.RateOfExchange;
                    publishFare = publishFare / flightItinerary.Passenger[0].Price.RateOfExchange;
                    totalTax = totalTax / flightItinerary.Passenger[0].Price.RateOfExchange;
                    additionalTxnFee = additionalTxnFee / flightItinerary.Passenger[0].Price.RateOfExchange;
                    serviceFee = serviceFee / flightItinerary.Passenger[0].Price.RateOfExchange;
                    totalFare = totalFare / flightItinerary.Passenger[0].Price.RateOfExchange;
                }

                if (totalTransactionFee > 0)
                {
                    transactionFee = "<tr><td style=\"width:120px;\"><span style=\"margin:0px;padding:0px;float:right;font-size:14px;text-align:right;font-weight:500;\">Tra Fee:</span></td><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;float:right;font-size:14px;	text-align:right;font-weight:500;\">" + AmountPrefix + " " + totalTransactionFee.ToString("N2") + "</span></td></tr>";
                }

                fareString = "<td style=\"width:240px;\">							<table style=\"width:240px;	margin:-4px 0px 0px 0px;	padding:0px;	float:left;\"border=\"0\" cellspacing=\"0\" cellpadding=\"0\">								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											AirFare:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											 " + AmountPrefix + " " + publishFare.ToString("N2") + "										</span>									</td>								</tr>";
                fareString += "<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Fee & Surcharge:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											" + AmountPrefix + " " + totalTax.ToString("N2") + "										</span>									</td>								</tr>									";
                if (additionalTxnFee > 0)
                {
                    fareString += "<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Txn Fee:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											" + AmountPrefix + " " + additionalTxnFee.ToString("N2") + "										</span>									</td>								</tr>									";
                }
                fareString += transactionFee;
                if (showServiceFee)
                {
                    fareString += "<tr id=\"serviceFeeValue\">									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">Txn Fee:</span></td>									<td style=\"width:120px;\">	<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											 " + AmountPrefix + " " + serviceFee.ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</span></td>								</tr>";
                }
                //if (policy != null && policy.PolicyNumber != null && policy.PolicyNumber.Length > 0)
                //{
                //    insurancePrice = policy.DisplayPrice;
                //    fareString += "<tr>									<td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Insurance Charges:</span></td>									<td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + " " + policy.DisplayPrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + "</span></td>								</tr>                                ";
                //}
                if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                {
                    fareString += "<tr><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">Baggage Fare:</span></td><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">" + AmountPrefix + " " + baggageCharge.ToString("N2") + "</span></td></tr>";
                }
                fareString += "<tr><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">TotalAirFare:</span></td><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">" + AmountPrefix + " " + totalFare.ToString("N2") + "</span></td></tr></table></td>";


            }
            globalHashTable.Add("fare", fareString);
            globalHashTable.Add("ticketDetails", ticketDetails);
            if (!isLCC && (ticket.PaxType == PassengerType.Adult || ticket.PaxType == PassengerType.Senior))
            {
                //adMessage = "<a href=\"http://www.zoomtra.com/e-ticket/click.aspx?source=Ballantine&type=Cocktails\" target=\"_blank\"><img alt=\"Ballantine\" src=\"http://www.zoomtra.com/e-ticket/open.aspx?source=Ballantine&type=Cocktails\" width=\"728\" height=\"90\"/></a>";
                //adMessage = DynamicAdvertise.GetActiveAdDiv(ProductType.Flight, DyAdPage.ETicket, Request["HTTP_HOST"], Request.ServerVariables["URL"]);
                //adMessage = "<hr style=\"width: 680px; color: #000;\"/> <a href=\"http://beta.zoomtra.com/ads/redirect.aspx?source=travelboutiq&type=flights\"> <img src=\"http://beta.zoomtra.com/ads/ad.aspx?source=travelboutiq&type=flights\" alt=\"image\" width=\"680\" height=\"90\" border=\"0\" /></a> <hr style=\"width: 680px; color: #000;\"/>";
                //adMessage = DynamicAdvertise.GetActiveAdDiv(ProductType.Flight, DyAdPage.ETicket, Request["HTTP_HOST"], Request.ServerVariables["URL"],Request.ServerVariables["REMOTE_ADDR"]);
            }
            globalHashTable.Add("adMessage", adMessage);
            #region // Bug ID : 0029414- E-Ticket Ads

            UserMaster member = new UserMaster();
            agencyId = Ticket.GetAgencyIdForTicket(ticket.TicketId);
            agency = new AgentMaster(agencyId);
            member = new UserMaster((agency.ID));
            pref = new UserPreference();
            pref = pref.GetPreference((int)member.ID, pref.ShowHotelAdOnEticket, pref.ETicket);
            if (pref.Value != null)
            {
                if (pref.Value.ToUpper() == "TRUE")
                {
                    IsShowHotelAd = true;
                }
            }
            #endregion
            //if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]) && IsShowHotelAd == true)
            //{



            //    CityMaster city = new CityMaster();

            //    HotelSearchResult res = new HotelSearchResult();
            //    HotelSearchResult[] searchRes = res.LoadTBOResults(city.CityName);
            //    if (searchRes.Length > 0)
            //    {
            //        tboConnect = "<div style=\"width:728px; margin:0 auto; border-top:solid 1px rgb(228,227,223)\"><div style=\" color:rgb(104,104,104); text-align:justify; font:bold 12px Arial,Verdna,Thoma; width:97%; padding:0px 10px\">";
            //        tboConnect += "<span>" + agency.AgencyName + " can also provide you great deals in " + city.CityName;
            //        tboConnect += ".All hotel featured have been referred by customers and checked for quality of services provided.";
            //        tboConnect += "</span></div><div style=\"color:rgb(29,53,99); text-align:left; font:bold 13px Arial,Verdna,Thoma; width:100%; padding:0px 10px; margin:10px 0px\"><span> Some Offers are : </span></div><div style=\"width:100%\"> ";
            //        tboConnect += "<table style=\"border-collapse:collapse; width:79%; margin:0px auto; border:solid 1px rgb(84,100,133); font:13px Arial,Verdana,Thoma\"><tr style=\"width:100%; border-right:solid 1px rgb(84,100,133)\"><td style=\"width:25%; border-right:solid 1px rgb(84,100,133)\"><span style=\"float:left; width:97%; padding:5px 0 5px 5px;background:rgb(217,217,217);\">";
            //        tboConnect += "<b> Hotel name </b></span></td><td style=\"width:27%; border-right:solid 1px rgb(84,100,133)\"><span style=\"float:left; width:97%; padding:5px 0 5px 5px;background:rgb(217,217,217)\">";
            //        tboConnect += "<b> Address </b></span> </td><td style=\"width:20%; border-right:solid 1px rgb(84,100,133)\"><span style=\"background:rgb(217,217,217);float:left; width:97%; padding:5px 0 5px 5px\">";
            //        tboConnect += "<b> Star Catagory </b></span> </td><td style=\"width:20%;\"><span style=\"background:rgb(217,217,217);float:left; width:97%; padding:5px 0 5px 5px\">";
            //        tboConnect += "<b> Price per night </b></span> </td></tr>";
            //        string rating = string.Empty;
            //        for (int i = 0; (i < searchRes.Length && i < Convert.ToInt16(CT.Configuration.ConfigurationSystem.HotelConnectConfig["tboConnectResults"])); i++)
            //        {
            //            if (searchRes[i].Rating != HotelRating.All)
            //            {
            //                rating = searchRes[i].Rating.ToString();
            //                rating = rating.Insert(rating.Length - 4, " ");
            //            }
            //            else
            //            {
            //                rating = "All";
            //            }
            //            tboConnect += "<tr style=\"width:100%; border-right:solid 1px rgb(84,100,133)\">";
            //            tboConnect += "<td style=\"width:25%; border-right:solid 1px rgb(84,100,133)\"> <span style=\"float:left; width:97%; padding:5px 0 5px 5px\">" + searchRes[i].HotelName + "</span> </td>";
            //            tboConnect += "<td style=\"width:27%; border-right:solid 1px rgb(84,100,133)\"><span style=\"float:left; width:97%; padding:5px 0 5px 5px\">" + searchRes[i].HotelAddress + "</span> </td>";
            //            tboConnect += "<td style=\"width:20%; border-right:solid 1px rgb(84,100,133)\"> <span style=\"float:left; width:97%; padding:5px 0 5px 5px\">" + searchRes[i].Rating + " </span> </td>";
            //            tboConnect += "<td style=\"width:20%\"> <span style=\"float:left; width:97%; padding:5px 0 5px 5px\">" + searchRes[i].TotalPrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + "</span> </td></tr>";
            //        }
            //        tboConnect += "</table></div><div class=\"wid100\">";
            //        tboConnect += "<span style=\"font:14px Arial,Verdana,Thoma; color:rgb(29,53,101); text-align:center; width:100%; float:left; margin:5px auto\"><b> Call :- " + agency.AgencyName + "(" + agency.Address.CityName + "),";
            //        if (agency.Address.Mobile != null && agency.Address.Mobile.Length > 0)
            //        {
            //            tboConnect += agency.Address.Mobile;
            //        }
            //        if (agency.Address.Phone != null && agency.Address.Phone.Length > 0)
            //        {
            //            tboConnect += "Ph:-" + agency.Address.Phone + "<b style=\"color:rgb(212,170,86);\"> (for more options) </b></span></div></div>";
            //        }
            //    }
            //}
            globalHashTable.Add("tboConnect", tboConnect);
            string promotionalCode = string.Empty;

            if (ticket.CorporateCode != null && ticket.CorporateCode.Trim().Length > 0)
            {
                promotionalCode += "Promotional Code: " + ticket.CorporateCode;
            }
            if (ticket.TourCode != null && ticket.TourCode.Trim().Length > 0)
            {
                promotionalCode += " | Tour Code: " + ticket.TourCode;
            }
            if (ticket.Endorsement != null && ticket.Endorsement.Trim().Length > 0)
            {
                promotionalCode += " | Endorsement: " + ticket.Endorsement;
            }
            if (promotionalCode.Length > 0)
            {
                globalHashTable.Add("promotionalCode", promotionalCode);
            }
            else
            {
                globalHashTable.Add("promotionalCode", "");
            }
            globalHashTable.Add("ticketAdvisory", ticket.TicketAdvisory);
            if (ticket.Remarks != null || ticket.Remarks.Length != 0)
            {
                remarks = "<tr><td style=\"font-weight:bold\">Remarks</td></tr><tr><td>" + ticket.Remarks;
            }
            remarks += "</td></tr>";
            if (ticket.FareRule != null && ticket.FareRule.Length != 0)
            {
                fareRule = "<tr><td style=\"font-weight:bold\">";
                if (ticket.Remarks != null || ticket.Remarks.Length != 0)
                {
                    fareRule += "<div style=\"margin-top:3px\">";
                }
                fareRule += "Fare Rules";
                if (ticket.Remarks != null || ticket.Remarks.Length != 0)
                {
                    fareRule += "<div />";
                }
                fareRule += "</td></tr><tr><td>";
                fareRule += ticket.FareRule;
                fareRule += "</td></tr>";
            }

            globalHashTable.Add("remarks", remarks);
            globalHashTable.Add("fareRule", fareRule);
            //Html code in text file for E-ticket
            string filePath = ConfigurationManager.AppSettings["ETicketPage"].ToString();
            StreamReader sr = new StreamReader(filePath);
            //string contains the code of loop
            string loopString = string.Empty;

            bool loopStarts = false;
            //string contains the code before loop 
            string startString = string.Empty;
            //string contains the code after loop
            string endString = string.Empty;
            bool loopEnds = false;
            #region seperate out the loop code and the other code
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
                {
                    loopEnds = true;
                    loopStarts = false;
                    if (line.IndexOf("%loopEnds%") >= 0)
                    {
                        line = sr.ReadLine();
                    }
                    endString += line;
                }
                if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
                {
                    if (line.IndexOf("%loopStarts%") >= 0)
                    {
                        line = sr.ReadLine();
                    }
                    loopString += line;
                    loopStarts = true;
                }
                else
                {
                    if (!loopEnds)
                    {
                        startString += line;
                    }
                }
            }
            sr.Close();
            endString += "<br /><div>" + ticket.TicketAdvisory + "</div>";
            #endregion
            Dictionary<string, List<string>> statusList = ConfigurationSystem.StatusListConfig;
            string midString = string.Empty;
            for (int count = 0; count < flightItinerary.Segments.Length; count++)
            {
                Airline airline = new Airline();
                airline.Load(flightItinerary.Segments[count].Airline);
                //Temporary hashtable which replaces loopvariable continuously

                Hashtable tempTable = new Hashtable();
                tempTable.Add("airlineLogo", Airline.logoDirectoryPath + airline.LogoFile);
                tempTable.Add("airlineName", airline.AirlineName);
                tempTable.Add("airlineCode", airline.AirlineCode);
                tempTable.Add("flightNumber", flightItinerary.Segments[count].FlightNumber);
                if (flightItinerary.Segments[count].AirlinePNR != null && flightItinerary.Segments[count].AirlinePNR.Length != 0)
                {
                    tempTable.Add("airlinePNR", "Airline Ref: " + flightItinerary.Segments[count].AirlinePNR);
                }
                else
                {
                    tempTable.Add("airlinePNR", "");
                }
                string status = "NotConfirmed";
                foreach (KeyValuePair<string, List<string>> statuses in statusList)
                {
                    if (statuses.Value.Contains(flightItinerary.Segments[count].Status))
                    {
                        status = statuses.Key;
                        break;
                    }
                }
                //if (flightItinerary.Segments[count].FlightStatus == FlightStatus.Confirmed)
                //{
                tempTable.Add("flightStatus", "Status : " + status);
                //}
                //else
                //{
                //    tempTable.Add("flightStatus", "Status : Not Confirmed");
                //}
                tempTable.Add("depDay", flightItinerary.Segments[count].DepartureTime.DayOfWeek.ToString().Substring(0, 3));
                tempTable.Add("depDate", flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy"));
                tempTable.Add("origAirportCode", flightItinerary.Segments[count].Origin.AirportCode);
                tempTable.Add("origAirportName", flightItinerary.Segments[count].Origin.AirportName);
                if (flightItinerary.Segments[count].ArrTerminal != null && flightItinerary.Segments[count].ArrTerminal.Trim() != string.Empty)
                {
                    tempTable.Add("arrTerminal", "Terminal " + flightItinerary.Segments[count].ArrTerminal);
                }
                else
                {
                    tempTable.Add("arrTerminal", "");
                }
                tempTable.Add("depTime", flightItinerary.Segments[count].DepartureTime.ToShortTimeString());
                tempTable.Add("destAirportCode", flightItinerary.Segments[count].Destination.AirportCode);
                tempTable.Add("destAirportName", flightItinerary.Segments[count].Destination.AirportName);
                if (flightItinerary.Segments[count].DepTerminal != null && flightItinerary.Segments[count].DepTerminal.Trim() != string.Empty)
                {
                    tempTable.Add("depTerminal", "Terminal " + flightItinerary.Segments[count].DepTerminal);
                }
                else
                {
                    tempTable.Add("depTerminal", "");
                }
                tempTable.Add("arrTime", flightItinerary.Segments[count].ArrivalTime.ToShortTimeString());
                tempTable.Add("arrDay", flightItinerary.Segments[count].ArrivalTime.DayOfWeek.ToString().Substring(0, 3));
                tempTable.Add("bookingClass", flightItinerary.Segments[count].BookingClass);
                if (flightItinerary.Segments[count].Duration > new TimeSpan(0, 0, 0))
                {
                    tempTable.Add("flightDuration", ((flightItinerary.Segments[count].Duration.Days * 24 + flightItinerary.Segments[count].Duration.Hours) + ":" + flightItinerary.Segments[count].Duration.Minutes) + " Hours Flight");
                }
                else
                {
                    tempTable.Add("flightDuration", string.Empty);
                }
                string stopString = "Non stop";
                if (flightItinerary.Segments[count].Stops > 0)
                {
                    stopString = flightItinerary.Segments[count].Stops + " stops";
                }
                tempTable.Add("stopString", stopString);
                string mealCode = FlightPassenger.GetMealCode(ticket.PaxId);
                string mealDescription = string.Empty;
                if (mealCode != string.Empty)
                {
                    mealDescription = Meal.GetMeal(mealCode).Description;
                }
                if (mealDescription != null && mealDescription.Length != 0)
                {
                    tempTable.Add("mealDescription", "Meal: " + mealDescription + "<span style=\"font-size: 9px; font-weight: normal; color: #888888;\">(subject to availability)</span>");
                }
                else
                {
                    tempTable.Add("mealDescription", "");
                }
                string baggage = string.Empty;
                if (flightItinerary.FlightBookingSource != BookingSource.AirArabia)
                {
                    if (ticket.PtcDetail != null && ticket.PtcDetail.Count > 0)
                    {
                        baggage = ticket.PtcDetail[paxTypeIndex].Baggage;
                    }
                }
                else
                {
                    //Connecting Cities or Multi Stop
                    if (flightItinerary.Segments.Length > 2)
                    {
                        if ((count + 1) <= 2)
                        {
                            baggage = inBaggage;
                        }
                        else
                        {
                            baggage = outBaggage;
                        }
                    }
                    else
                    {
                        if ((count + 1) <= 1)
                        {
                            baggage = inBaggage;
                        }
                        else
                        {
                            baggage = outBaggage;
                        }
                    }
                }

                if (baggage != null && baggage.Length != 0)
                {
                    tempTable.Add("baggage", "Baggage (per person): " + baggage);
                }
                else
                {
                    tempTable.Add("baggage", "");
                }
                if (flightItinerary.Segments[count].Craft != null && flightItinerary.Segments[count].Craft.ToString().Trim() != string.Empty)
                {
                    tempTable.Add("craft", "Aircraft: " + flightItinerary.Segments[count].Craft);
                }
                else
                {
                    tempTable.Add("craft", "");
                }

                midString += Email.ReplaceHashVariable(loopString, tempTable);
            }
            //string insuranceString = string.Empty;
            //if (policy != null && policy.PolicyNumber != null && policy.PolicyNumber.Length > 0)
            //{
            //    insuranceString = "		<tr>		  <td>		    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">		      <tr style=\"padding:10px 0 0 5px; font-size:14px;\">		        <td>Insurance Issuing Company: <b>ICICI Lombard</b></td>		        <td style=\"padding-left:30px;\">Policy Number: <b>" + policy.PolicyNumber + "</b></td>		      </tr>		    </table>		  </td>		</tr>";
            //}

            //globalHashTable.Add("insuranceString", insuranceString);
            string fullString = startString + midString + endString;
            List<string> toArray = new List<string>();
            string addressList = Request["addressList"];
            string[] addressArray = addressList.Split(',');
            for (int k = 0; k < addressArray.Length; k++)
            {
                toArray.Add(addressArray[k]);
            }
            string from = new UserMaster(memberId).Email;
            try
            {
                CT.Core.Email.Send(from, from, toArray, "E-Ticket", fullString, globalHashTable);
            }
            catch (System.Net.Mail.SmtpException)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
            }
        } 
        #endregion
        //when we Email Invoice
        else if (Request["isInvoice"] != null && Convert.ToBoolean(Request["isInvoice"]))
        {
            #region Loading all the objects required for Invoice
            invoice = new Invoice();
            invoice.Load(Convert.ToInt32(Request["invoiceNumber"]));
            agency = new AgentMaster(invoice.AgencyId);
            int flightId = FlightItinerary.GetFlightIdByInvoiceNumber(Convert.ToInt32(Request["invoiceNumber"]));

            FlightInfo[] allSegments = FlightInfo.GetSegments(flightId);
            segmentTable = new DataTable();
            DataColumn column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "source"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "destination"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "flightNumber"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "status"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "bookingClass"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "AirLine"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "conjunctionNo"; segmentTable.Columns.Add(column);


            List<string> conjunctionlist = new List<string>();
            conjunctionlist.Add("C0");

            for (int t = 0; t < allSegments.Length; t++)
            {
                if (allSegments[t].ConjunctionNo != null)
                {
                    if (!conjunctionlist.Contains(allSegments[t].ConjunctionNo.ToString()))
                    {
                        conjunctionlist.Add(allSegments[t].ConjunctionNo.ToString());
                    }
                }

                DataRow row = segmentTable.NewRow();

                row["source"] = allSegments[t].Origin.AirportCode;
                row["destination"] = allSegments[t].Destination.AirportCode;
                row["flightNumber"] = allSegments[t].FlightNumber;
                row["status"] = allSegments[t].Status;
                row["bookingClass"] = allSegments[t].BookingClass;
                row["AirLine"] = allSegments[t].Airline;
                row["conjunctionNo"] = allSegments[t].ConjunctionNo;
                segmentTable.Rows.Add(row);
            }

            for (int j = 0; j < conjunctionlist.Count; j++)
            {
                if (conjunctionlist[j] == "C0")
                {
                    segment[conjunctionlist[j]] = segmentTable.Select("conjunctionNo is null");
                }
                else
                {
                    segment.Add(conjunctionlist[j], segmentTable.Select("conjunctionNo='" + conjunctionlist[j].ToString() + "'"));
                }
            }

            flightItinerary = new FlightItinerary(flightId);
            ticketList = new List<Ticket>(); //Ticket.GetTicketList(flightId);
            for (int count = 0; count < invoice.LineItem.Count; count++)
            {
                Ticket ticket = new Ticket();
                if (Request["isCreditNote"] != null && Convert.ToBoolean(Request["isCreditNote"]))
                {
                    ticket.LoadTicketForCreditNote(invoice.LineItem[count].ItemReferenceNumber);
                }
                else
                {
                    ticket.Load(invoice.LineItem[count].ItemReferenceNumber);
                }

                ticketList.Add(ticket);
            }
            DateTime travelDate = new DateTime();
            travelDate = Convert.ToDateTime(flightItinerary.TravelDate);
            //This is a global hash table which we send in Email.Send which replaces 
            //hashvariables with values(code which is not in loop)
            Hashtable globalHahTable = new Hashtable();
            globalHahTable.Add("completeInvoiceNumber", invoice.CompleteInvoiceNumber);
            globalHahTable.Add("createdOn", Util.UTCToIST(invoice.CreatedOn).ToString("dd MMMM yyyy"));
            //globalHahTable.Add("invoiceRemarks", invoice.Remarks);
            globalHahTable.Add("PNR", flightItinerary.PNR);
            globalHahTable.Add("agencyName", agency.Name);
            globalHahTable.Add("agencyAddressLine1", agency.Address);
            globalHahTable.Add("agencyAddressLine2", "");
            globalHahTable.Add("agencyCity", agency.City);
            if (agency.Phone1 != null && agency.Phone1.Length != 0)
            {
                globalHahTable.Add("agencyAddressPhone", "Phone:" + agency.Phone1);
            }
            else
            {
                globalHahTable.Add("agencyAddressPhone", "");

            }
            if (agency.Phone2 != null && agency.Phone2.Length != 0)
            {
                globalHahTable.Add("mobile", "Mobile: " + agency.Phone2);
            }
            else
            {
                globalHahTable.Add("mobile", "");

            }

            if (agency.Fax != null && agency.Fax.Length != 0)
            {
                globalHahTable.Add("fax", "Fax:" + agency.Fax);
            }
            else
            {
                globalHahTable.Add("fax", "");
            }
            globalHahTable.Add("travelDate", "Travel Date:" + travelDate.ToString("dd MMM yyyy"));
            string filePath = string.Empty;
            decimal cancellationCharge = 0;
            //Html code in text file for E-mailing Invoice
            string subject = string.Empty;
            if (Request["isCreditNote"] != null && Convert.ToBoolean(Request["isCreditNote"]))
            {
                // new object for invoice number
                int sessionTicketId = Convert.ToInt32(Request["ticketId"]);
                int invno = Invoice.GetInvoiceNumberByTicketId(sessionTicketId);
                Invoice inv = new Invoice();
                inv.Load(invno);

                globalHahTable.Add("originalInvoiceNumber", inv.CompleteInvoiceNumber);
                globalHahTable.Add("remarks", invoice.Remarks);
                filePath = ConfigurationManager.AppSettings["EmailCreditNote"].ToString();
                if (Request["cancellationCharge"] != null)
                {
                    cancellationCharge = Convert.ToDecimal(Request["cancellationCharge"]);
                }
                globalHahTable.Add("cancellationcharge", cancellationCharge);
                subject = "Credit Note";
            }
            else
            {
                filePath = ConfigurationManager.AppSettings["EmailInvoicePage"].ToString();
                subject = "Invoice";
            }


            StreamReader sr = new StreamReader(filePath);
            //string contains the code of loop
            string loopString = string.Empty;
            bool loopStarts = false;
            //string contains the code before loop 
            string startString = string.Empty;
            //string contains the code after loop
            string endString = string.Empty;
            bool loopEnds = false;
            #region seperate out the loop code and the other code
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
                {
                    loopEnds = true;
                    loopStarts = false;
                    if (line.IndexOf("%loopEnds%") >= 0)
                    {
                        line = sr.ReadLine();
                    }
                    endString += line;
                }
                if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
                {
                    if (line.IndexOf("%loopStarts%") >= 0)
                    {
                        line = sr.ReadLine();
                    }
                    loopString += line;
                    loopStarts = true;
                }
                else
                {
                    if (!loopEnds)
                    {
                        startString += line;
                    }
                }
            }
            #endregion
            string midString = string.Empty;
            int i = 0;
            float plbInPercent = 0;
            string airlineCode = string.Empty;
            string routing = string.Empty;
            foreach (Ticket ticket in ticketList)
            {
                string bookingClass = string.Empty;
                // FlightInfo[] segments = FlightInfo.GetSegments(ticket.FlightId);
                bool multiAirline = false;

                DataRow[] row;
                if (ticket.TicketType == "C" || ticket.ConjunctionNumber.StartsWith("C"))
                {
                    row = segment[ticket.ConjunctionNumber];
                }
                else
                {
                    row = segment["C0"];
                }
                string tempAirline = row[0]["AirLine"].ToString();

                if (ticket.Price.PublishedFare != 0 && ticket.Price.PublishedFare - ticket.Price.OurCommission != 0)
                {
                    plbInPercent = Convert.ToSingle((ticket.Price.AgentPLB * 100) / (ticket.Price.PublishedFare - ticket.Price.OurCommission));
                }
                for (int g = 1; g < row.Length; g++)
                {
                    if (tempAirline != row[g]["AirLine"].ToString())
                    {
                        multiAirline = true;
                        break;
                    }
                }
                if (!multiAirline)
                {
                    airlineCode = tempAirline;
                }
                else
                {
                    //Code for multi Airline
                    airlineCode = "##";
                }

                routing = string.Empty;
                int k = 0;
                for (; k < row.Length; k++)
                {
                    bookingClass = bookingClass + row[k]["bookingClass"].ToString();
                    if (k == 0)
                    {
                        routing = row[k]["source"].ToString();
                    }
                    else
                    {
                        routing = routing + "-" + row[k]["source"].ToString();
                    }
                }

                routing = routing + "-" + row[k - 1]["destination"].ToString();

                string flightNum = row[0]["AirLine"].ToString() + row[0]["flightNumber"].ToString();
                i++;



                //Temporary hashtable which replaces loopvariable continuously
                Hashtable tempTable = new Hashtable();
                tempTable.Add("serialNumber", i.ToString());
                tempTable.Add("ticketNo", ticket.ValidatingAriline + ticket.TicketNumber);
                tempTable.Add("sectors", routing);
                tempTable.Add("flight", flightNum);
                tempTable.Add("paxFullName", FlightPassenger.GetPaxFullName(ticket.PaxId));
                tempTable.Add("type", FlightPassenger.GetPTC(ticket.PaxType));
                tempTable.Add("class", bookingClass);
                tempTable.Add("fare", ticket.Price.PublishedFare.ToString("N2"));
                tempTable.Add("tax", ticket.Price.Tax.ToString("N2"));
                tempTable.Add("otherCharges", ticket.Price.OtherCharges.ToString("N2"));
                midString += Email.ReplaceHashVariable(loopString, tempTable);
            }
            string from = new UserMaster(memberId).Email;
            string fullString = startString + midString + endString;
            List<string> toArray = new List<string>();
            string addressList = Request["addressList"];
            string[] addressArray = addressList.Split(',');
            for (int k = 0; k < addressArray.Length; k++)
            {
                toArray.Add(addressArray[k]);
            }

            decimal totalPrice = 0;
            decimal handlingCharge = 0;
            decimal serviceTax = 0;
            decimal tdsCommission = 0;
            decimal tdsPLB = 0;
            decimal plb = 0;
            decimal transactionFee = 0;
            decimal discount = 0;
            decimal reverseHandlingCharge = 0;
            for (int k = 0; k < ticketList.Count; k++)
            {
                if (Request["isCreditNote"] != null && Convert.ToBoolean(Request["isCreditNote"]))
                {
                    totalPrice = totalPrice + (ticketList[k].Price.PublishedFare + ticketList[k].Price.Tax);
                }
                else
                {
                    totalPrice = totalPrice + (ticketList[k].Price.PublishedFare + ticketList[k].Price.Tax + ticketList[k].Price.OtherCharges);
                }
                //if (agency.AgencyTypeId == (int)Agencytype.Service)
                //{
                //    handlingCharge = 0;
                //    tdsCommission = 0;
                //    plb = 0;
                //    tdsPLB = 0;
                //}
                //else
                {
                    handlingCharge = handlingCharge + (ticketList[k].Price.AgentCommission);
                    tdsCommission = tdsCommission + ticketList[k].Price.TdsCommission;
                    plb = plb + ticketList[k].Price.AgentPLB;
                    tdsPLB = tdsPLB + ticketList[k].Price.TDSPLB;
                }

                serviceTax = serviceTax + (ticketList[k].Price.SeviceTax);
                transactionFee = transactionFee + ticketList[k].Price.TransactionFee;
                discount += ticketList[k].Price.Discount;
                reverseHandlingCharge += ticketList[k].Price.ReverseHandlingCharge;
            }
            globalHahTable.Add("gross", totalPrice.ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            // Changed the code to show Commission Earned  as handlingCharge + plb irrespective of whether it is agent view or admin view
            if (Request["fromAgent"] == null && invoice.AgencyId == 1)
            {
                globalHahTable.Add("handlingCharges", (handlingCharge + plb).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            }
            else
            {
                globalHahTable.Add("handlingCharges", (handlingCharge + plb).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));

            }
            globalHahTable.Add("serviceTax", serviceTax.ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            globalHahTable.Add("traFee", transactionFee.ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            globalHahTable.Add("tdsDeducted", (tdsCommission + tdsPLB).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            if (discount > 0)
            {
                globalHahTable.Add("discount", "You are eligible for a discount of " + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "" + discount + " on this invoice");
            }
            else
            {
                globalHahTable.Add("discount", "");
            }
            string reverseHC = "<div style=\"float:left; width:100%; margin-top:5px;\"><div style=\"float:left; width:50px; font-style:italic;\">Add</div><div style=\"float:left; width:140px;\">Reverse H/C</div><div style=\"float:left; width:60px; text-align:right;\">" + reverseHandlingCharge + "</div></div>";
            if (reverseHandlingCharge > 0)
            {
                globalHahTable.Add("reverseHandlingCharge", reverseHC);
            }
            else
            {
                globalHahTable.Add("reverseHandlingCharge", "");
            }
            globalHahTable.Add("netAmount", (Math.Round(totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee + reverseHandlingCharge)).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            globalHahTable.Add("netReceivable", (Math.Round(totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee + cancellationCharge + reverseHandlingCharge)).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
            UserMaster member = new UserMaster(invoice.CreatedBy);
            globalHahTable.Add("invoiceStatus", invoice.Status.ToString());
            globalHahTable.Add("ticketedBy", member.FirstName + " " + member.LastName);
            try
            {
                CT.Core.Email.Send(from, from, toArray, subject, fullString, globalHahTable);
                Response.Write("Email Sent");
            }
            catch (System.Net.Mail.SmtpException)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
            }


            #endregion
        }
        #region Old Code
        //else if (Request["isInsuranceInvoice"] != null && Convert.ToBoolean(Request["isInsuranceInvoice"]))
        //{
        //    #region Loading all the objects required for Invoice
        //    try
        //    {
        //        invoice = new Invoice();
        //        invoice.Load(Convert.ToInt32(Request["invoiceNumber"]));
        //        agency = new AgentMaster(invoice.AgencyId);
        //        Insurance insurance = new Insurance();
        //        insurance.Load(Convert.ToInt32(Request["insuranceId"]));
        //        Hashtable globalHahTable = new Hashtable();
        //        globalHahTable.Add("agencyName", agency.AgencyName);
        //        globalHahTable.Add("agencyAddressLine1", agency.Address.Line1);
        //        globalHahTable.Add("agencyAddressLine2", agency.Address.Line2);
        //        globalHahTable.Add("agencyCity", agency.Address.CityName);
        //        if (agency.Address.Phone != null && agency.Address.Phone.Length != 0)
        //        {
        //            globalHahTable.Add("addressPhone", "Phone:" + agency.Address.Phone);
        //        }
        //        else
        //        {
        //            globalHahTable.Add("addressPhone", "");
        //        }
        //        globalHahTable.Add("completeInvoiceNumber", invoice.CompleteInvoiceNumber);
        //        globalHahTable.Add("createdOn", invoice.CreatedOn.ToString("dd MMMM yyyy"));
        //        globalHahTable.Add("invoiceRemarks", invoice.Remarks);
        //        globalHahTable.Add("ServiceTaxRegNumber", ConfigurationManager.AppSettings["ServiceTaxRegNo"].ToString());
        //        string filePath = ConfigurationManager.AppSettings["EmailInsuranceInvoicePage"].ToString();
        //        StreamReader sr = new StreamReader(filePath);
        //        //string contains the code of loop
        //        string loopString = string.Empty;
        //        bool loopStarts = false;
        //        //string contains the code before loop 
        //        string startString = string.Empty;
        //        //string contains the code after loop
        //        string endString = string.Empty;
        //        bool loopEnds = false;
        //        #region seperate out the loop code and the other code
        //        while (!sr.EndOfStream)
        //        {
        //            string line = sr.ReadLine();
        //            if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
        //            {
        //                loopEnds = true;
        //                loopStarts = false;
        //                if (line.IndexOf("%loopEnds%") >= 0)
        //                {
        //                    line = sr.ReadLine();
        //                }
        //                endString += line;
        //            }
        //            if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
        //            {
        //                if (line.IndexOf("%loopStarts%") >= 0)
        //                {
        //                    line = sr.ReadLine();
        //                }
        //                loopString += line;
        //                loopStarts = true;
        //            }
        //            else
        //            {
        //                if (!loopEnds)
        //                {
        //                    startString += line;
        //                }
        //            }
        //        }
        //        #endregion
        //        string midString = string.Empty;
        //        decimal totalPrice = 0;
        //        decimal toatalCommisson = 0;
        //        foreach (InsurancePassenger passenger in insurance.Passenger)
        //        {
        //            if (passenger.IsActive || passenger.Status == InsurancePassengerStatus.Cancelled)
        //            {
        //                decimal premium = passenger.Price.PublishedFare;
        //                totalPrice += passenger.Price.PublishedFare;
        //                toatalCommisson += passenger.Price.AgentCommission;
        //                Hashtable tempTable = new Hashtable();
        //                tempTable.Add("InsuranceSource", insurance.Source.ToString());
        //                tempTable.Add("PolicyNumber", passenger.PolicyNumber);
        //                tempTable.Add("Name", passenger.Title + passenger.FirstName + passenger.LastName);
        //                tempTable.Add("Premium", premium.ToString("N2"));
        //                midString += Email.ReplaceHashVariable(loopString, tempTable);
        //            }
        //        }
        //        string fullString = startString + midString + endString;
        //        string from = new UserMaster(memberId).Email;
        //        List<string> toArray = new List<string>();
        //        string addressList = Request["addressList"];
        //        string[] addressArray = addressList.Split(',');
        //        for (int k = 0; k < addressArray.Length; k++)
        //        {
        //            toArray.Add(addressArray[k]);
        //        }
        //        string remarksMessge = string.Empty;
        //        globalHahTable.Add("Gross", totalPrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //        decimal netAmount = totalPrice - toatalCommisson;
        //        globalHahTable.Add("commission", toatalCommisson.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //        globalHahTable.Add("NetAmount", netAmount.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //        globalHahTable.Add("NetReceivable", netAmount.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //        UserMaster member = new UserMaster(invoice.CreatedBy);
        //        globalHahTable.Add("issuedBy", member.FirstName + " " + member.LastName);
        //        try
        //        {
        //            Email.Send(from, from, toArray, "Invoice", fullString, globalHahTable);
        //        }
        //        catch (System.Net.Mail.SmtpException)
        //        {
        //            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    #endregion
        //}
        //else if (Request["isOfflineCreditNote"] != null && Convert.ToBoolean(Request["isOfflineCreditNote"]))
        //{
        //    #region Loading all the objects required for Invoice
        //    invoice = new Invoice();
        //    invoice.Load(Convert.ToInt32(Request["invoiceNumber"]));
        //    agency = new AgentMaster(invoice.AgencyId);
        //    //int flightId = FlightItinerary.GetFlightIdByInvoiceNumber(Convert.ToInt32(Request["invoiceNumber"]));
        //    //flightItinerary = new FlightItinerary(flightId);
        //    //ticketList = new List<Ticket>(); //Ticket.GetTicketList(flightId);
        //    //for (int count = 0; count < invoice.LineItem.Count; count++)
        //    //{
        //    //    Ticket ticket = new Ticket();
        //    //    ticket.Load(invoice.LineItem[count].ItemReferenceNumber);
        //    //    ticketList.Add(ticket);
        //    //}
        //    //DateTime travelDate = new DateTime();
        //    //travelDate = Convert.ToDateTime(flightItinerary.TravelDate);
        //    //This is a global hash table which we send in Email.Send which replaces 
        //    //hashvariables with values(code which is not in loop)
        //    int offlineBookingId = Convert.ToInt32(Request["offlineBookingId"]);
        //    OfflineBooking offlinebooking = new OfflineBooking();
        //    offlinebooking.Load(offlineBookingId);
        //    Hashtable globalHahTable = new Hashtable();
        //    globalHahTable.Add("completeInvoiceNumber", invoice.CompleteInvoiceNumber);
        //    globalHahTable.Add("createdOn", invoice.CreatedOn.ToString("dd MMMM yyyy"));
        //    //globalHahTable.Add("invoiceRemarks", invoice.Remarks);
        //    globalHahTable.Add("PNR", offlinebooking.PNR);
        //    globalHahTable.Add("agencyName", agency.AgencyName);
        //    globalHahTable.Add("agencyAddressLine1", agency.Address.Line1);
        //    globalHahTable.Add("agencyAddressLine2", agency.Address.Line2);
        //    globalHahTable.Add("agencyCity", agency.Address.CityName);
        //    if (agency.Address.Phone != null && agency.Address.Phone.Length != 0)
        //    {
        //        globalHahTable.Add("agencyAddressPhone", "Phone:" + agency.Address.Phone);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("agencyAddressPhone", "");

        //    }
        //    if (agency.Address.Mobile != null && agency.Address.Mobile.Length != 0)
        //    {
        //        globalHahTable.Add("mobile", "Mobile: " + agency.Address.Mobile);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("mobile", "");

        //    }

        //    if (agency.Address.Fax != null && agency.Address.Fax.Length != 0)
        //    {
        //        globalHahTable.Add("fax", "Fax:" + agency.Address.Fax);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("fax", "");
        //    }
        //    globalHahTable.Add("travelDate", "");
        //    string filePath = string.Empty;
        //    decimal cancellationCharge = 0;
        //    //Html code in text file for E-mailing Invoice
        //    string subject = string.Empty;
        //    globalHahTable.Add("remarks", invoice.Remarks);
        //    filePath = ConfigurationManager.AppSettings["EmailCreditNote"].ToString();
        //    if (Request["cancellationCharge"] != null)
        //    {
        //        cancellationCharge = Convert.ToDecimal(Request["cancellationCharge"]);
        //    }
        //    globalHahTable.Add("cancellationcharge", cancellationCharge);
        //    subject = "Credit Note";

        //    StreamReader sr = new StreamReader(filePath);
        //    //string contains the code of loop
        //    string loopString = string.Empty;
        //    bool loopStarts = false;
        //    //string contains the code before loop 
        //    string startString = string.Empty;
        //    //string contains the code after loop
        //    string endString = string.Empty;
        //    bool loopEnds = false;
        //    #region seperate out the loop code and the other code
        //    while (!sr.EndOfStream)
        //    {
        //        string line = sr.ReadLine();
        //        if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
        //        {
        //            loopEnds = true;
        //            loopStarts = false;
        //            if (line.IndexOf("%loopEnds%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            endString += line;
        //        }
        //        if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
        //        {
        //            if (line.IndexOf("%loopStarts%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            loopString += line;
        //            loopStarts = true;
        //        }
        //        else
        //        {
        //            if (!loopEnds)
        //            {
        //                startString += line;
        //            }
        //        }
        //    }
        //    #endregion
        //    string midString = string.Empty;
        //    int i = 0;
        //    float plbInPercent = 0;
        //    string airlineCode = string.Empty;
        //    string routing = string.Empty;
        //    string bookingClass = string.Empty;
        //    //FlightInfo[] segments = FlightInfo.GetSegments(ticket.FlightId);
        //    bool multiAirline = false;
        //    string tempAirline = offlinebooking.ValidatingCarrier;
        //    if (offlinebooking.Price.PublishedFare != 0 && offlinebooking.Price.PublishedFare - offlinebooking.Price.OurCommission != 0)
        //    {
        //        plbInPercent = Convert.ToSingle((offlinebooking.Price.AgentPLB * 100) / (offlinebooking.Price.PublishedFare - offlinebooking.Price.OurCommission));
        //    }
        //    routing = offlinebooking.ItineararyString;
        //    string flightNum = string.Empty;
        //    i++;

        //    //Temporary hashtable which replaces loopvariable continuously
        //    Hashtable tempTable = new Hashtable();
        //    tempTable.Add("serialNumber", i.ToString());
        //    tempTable.Add("ticketNo", offlinebooking.TicketNo);
        //    tempTable.Add("sectors", routing);
        //    tempTable.Add("flight", flightNum);
        //    tempTable.Add("paxFullName", offlinebooking.PaxFullName);
        //    tempTable.Add("type", offlinebooking.PaxType);
        //    tempTable.Add("class", "");
        //    tempTable.Add("fare", offlinebooking.Price.PublishedFare.ToString("N2"));
        //    tempTable.Add("tax", offlinebooking.Price.Tax.ToString("N2"));
        //    tempTable.Add("otherCharges", offlinebooking.Price.OtherCharges.ToString("N2"));
        //    midString += Email.ReplaceHashVariable(loopString, tempTable);

        //    string from = new UserMaster(memberId).Email;
        //    string fullString = startString + midString + endString;
        //    List<string> toArray = new List<string>();
        //    string addressList = Request["addressList"];
        //    string[] addressArray = addressList.Split(',');
        //    for (int k = 0; k < addressArray.Length; k++)
        //    {
        //        toArray.Add(addressArray[k]);
        //    }

        //    decimal totalPrice = 0;
        //    decimal handlingCharge = 0;
        //    decimal serviceTax = 0;
        //    decimal tdsCommission = 0;
        //    decimal tdsPLB = 0;
        //    decimal plb = 0;
        //    decimal transactionFee = 0;

        //    totalPrice = (offlinebooking.Price.PublishedFare + offlinebooking.Price.Tax);
        //    handlingCharge = (offlinebooking.Price.AgentCommission);
        //    serviceTax = (offlinebooking.Price.SeviceTax);
        //    tdsCommission = offlinebooking.Price.TdsCommission;
        //    plb = offlinebooking.Price.AgentPLB;
        //    tdsPLB = offlinebooking.Price.TDSPLB;
        //    transactionFee = offlinebooking.Price.TransactionFee;


        //    globalHahTable.Add("gross", totalPrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    if (Request["fromAgent"] == null && invoice.AgencyId == 1)
        //    {
        //        globalHahTable.Add("handlingCharges", handlingCharge.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    }
        //    else
        //    {
        //        globalHahTable.Add("handlingCharges", (handlingCharge + plb).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));

        //    }
        //    globalHahTable.Add("serviceTax", serviceTax.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("traFee", transactionFee.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("tdsDeducted", (tdsCommission + tdsPLB).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("netAmount", (Convert.ToDouble(totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee)).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("netReceivable", (Convert.ToDouble(totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee + cancellationCharge)).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    UserMaster member = new UserMaster(invoice.CreatedBy);
        //    globalHahTable.Add("ticketedBy", member.FirstName + " " + member.LastName);
        //    try
        //    {
        //        Email.Send(from, from, toArray, subject, fullString, globalHahTable);
        //    }
        //    catch (System.Net.Mail.SmtpException)
        //    {
        //        CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
        //    }
        //    #endregion
        //}
        ////For train invoice
        //else if (Request["isTrainInvoice"] != null && Convert.ToBoolean(Request["isTrainInvoice"]))
        //{
        //    #region Email train invoice
        //    invoice = new Invoice();
        //    invoice.Load(Convert.ToInt32(Request["invoiceNumber"]));
        //    agency = new AgentMaster(invoice.AgencyId);
        //    int itineraryId = Convert.ToInt32(Request["itineraryId"]);
        //    TrainItinerary itinerary = new TrainItinerary();
        //    itinerary.Load(itineraryId);
        //    DateTime travelDate = new DateTime();
        //    travelDate = itinerary.TravelDate;
        //    //This is a global hash table which we send in Email.Send which replaces 
        //    //hashvariables with values(code which is not in loop)
        //    Hashtable globalHahTable = new Hashtable();
        //    globalHahTable.Add("completeInvoiceNumber", invoice.CompleteInvoiceNumber);
        //    globalHahTable.Add("createdOn", invoice.CreatedOn.ToString("dd MMMM yyyy"));
        //    //globalHahTable.Add("invoiceRemarks", invoice.Remarks);
        //    globalHahTable.Add("PNR", itinerary.Pnr);
        //    globalHahTable.Add("agencyName", agency.AgencyName);
        //    globalHahTable.Add("agencyAddressLine1", agency.Address.Line1);
        //    globalHahTable.Add("agencyAddressLine2", agency.Address.Line2);
        //    globalHahTable.Add("agencyCity", agency.Address.CityName);
        //    if (agency.Address.Phone != null && agency.Address.Phone.Length != 0)
        //    {
        //        globalHahTable.Add("agencyAddressPhone", "Phone:" + agency.Address.Phone);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("agencyAddressPhone", "");

        //    }
        //    if (agency.Address.Mobile != null && agency.Address.Mobile.Length != 0)
        //    {
        //        globalHahTable.Add("mobile", "Mobile: " + agency.Address.Mobile);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("mobile", "");

        //    }

        //    if (agency.Address.Fax != null && agency.Address.Fax.Length != 0)
        //    {
        //        globalHahTable.Add("fax", "Fax:" + agency.Address.Fax);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("fax", "");
        //    }
        //    globalHahTable.Add("travelDate", "Travel Date:" + travelDate.ToString("dd MMM yyyy"));
        //    globalHahTable.Add("ServiceTaxRegNumber", ConfigurationManager.AppSettings["ServiceTaxRegNo"].ToString());
        //    string filePath = string.Empty;
        //    decimal cancellationCharge = 0;
        //    //Html code in text file for E-mailing Invoice
        //    string subject = string.Empty;
        //    filePath = ConfigurationManager.AppSettings["EmailTrainInvoicePage"].ToString();

        //    subject = "Invoice";
        //    StreamReader sr = new StreamReader(filePath);
        //    //string contains the code of loop
        //    string loopString = string.Empty;
        //    bool loopStarts = false;
        //    //string contains the code before loop 
        //    string startString = string.Empty;
        //    //string contains the code after loop
        //    string endString = string.Empty;
        //    bool loopEnds = false;
        //    #region seperate out the loop code and the other code
        //    while (!sr.EndOfStream)
        //    {
        //        string line = sr.ReadLine();
        //        if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
        //        {
        //            loopEnds = true;
        //            loopStarts = false;
        //            if (line.IndexOf("%loopEnds%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            endString += line;
        //        }
        //        if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
        //        {
        //            if (line.IndexOf("%loopStarts%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            loopString += line;
        //            loopStarts = true;
        //        }
        //        else
        //        {
        //            if (!loopEnds)
        //            {
        //                startString += line;
        //            }
        //        }
        //    }
        //    #endregion
        //    string midString = string.Empty;
        //    int i = 0;
        //    decimal total = 0;
        //    decimal serviceCharge = 0;
        //    decimal irctcCharge = 0;
        //    decimal netTotal = 0;
        //    foreach (TrainPassenger passenger in itinerary.PassengerList)
        //    {
        //        i++;
        //        string ticketNo = itinerary.TicketNumber;
        //        string routing = itinerary.Origin + " - " + itinerary.Destination;
        //        string trainNumber = itinerary.TrainNumber;
        //        string trainName = itinerary.TrainName;
        //        string paxName = passenger.PaxName;
        //        string paxType = passenger.PaxType.ToString();
        //        string bookingClass = itinerary.BookingClass;
        //        PriceAccounts price = new PriceAccounts();
        //        price.Load(passenger.PriceId);
        //        decimal fare = price.PublishedFare - price.Discount;
        //        decimal otherCharge = price.GetTrainTicketPrice() - fare - price.Markup - price.TransactionFee;
        //        string baseFareString = Convert.ToString(Math.Round(fare, Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
        //        string otherChargeString = Convert.ToString(Math.Round(otherCharge, Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
        //        total += price.GetTrainTicketPrice() - price.Markup - price.TransactionFee;
        //        serviceCharge += price.Markup;
        //        irctcCharge += price.TransactionFee;
        //        netTotal += price.GetTrainTicketPrice();
        //        //Temporary hashtable which replaces loopvariable continuously
        //        Hashtable tempTable = new Hashtable();
        //        tempTable.Add("serialNumber", i.ToString());
        //        tempTable.Add("Sector", routing);
        //        tempTable.Add("Train Name", trainName);
        //        tempTable.Add("PAX Name", paxName);
        //        tempTable.Add("Pax Type", paxType);
        //        tempTable.Add("Class", bookingClass);
        //        tempTable.Add("Fare", baseFareString);
        //        tempTable.Add("O/C", otherChargeString);
        //        midString += Email.ReplaceHashVariable(loopString, tempTable);
        //    }
        //    string from = new UserMaster(memberId).Email;
        //    string fullString = startString + midString + endString;
        //    List<string> toArray = new List<string>();
        //    string addressList = Request["addressList"];
        //    string[] addressArray = addressList.Split(',');
        //    for (int k = 0; k < addressArray.Length; k++)
        //    {
        //        toArray.Add(addressArray[k]);
        //    }
        //    globalHahTable.Add("gross", total.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("serviceCharge", serviceCharge.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("irctcCharge", irctcCharge.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("netAmount", netTotal.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("netReceivable", netTotal.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    UserMaster member = new UserMaster(invoice.CreatedBy);
        //    globalHahTable.Add("ticketedBy", member.FirstName + " " + member.LastName);
        //    try
        //    {
        //        Email.Send(from, from, toArray, subject, fullString, globalHahTable);
        //    }
        //    catch (System.Net.Mail.SmtpException)
        //    {
        //        CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
        //    }


        //    #endregion
        //}
        ////For train Credit Note
        //else if (Request["isTrainCreditNote"] != null && Convert.ToBoolean(Request["isTrainCreditNote"]))
        //{
        //    #region Email train credit note
        //    List<TrainCreditNoteQueue> lst = TrainCreditNoteQueue.GetTrainCreditNote(Convert.ToString(Request["creditNoteNo"]));
        //    agency = new AgentMaster(lst[0].AgencyId);
        //    DateTime travelDate = new DateTime();
        //    travelDate = lst[0].TravelDate;
        //    //This is a global hash table which we send in Email.Send which replaces 
        //    //hashvariables with values(code which is not in loop)
        //    Hashtable globalHahTable = new Hashtable();
        //    globalHahTable.Add("completeInvoiceNumber", lst[0].CreditNoteNo);
        //    globalHahTable.Add("createdOn", lst[0].CreatedOn.ToString("dd MMMM yyyy"));
        //    //globalHahTable.Add("invoiceRemarks", invoice.Remarks);
        //    globalHahTable.Add("PNR", lst[0].Pnr);
        //    globalHahTable.Add("agencyName", agency.AgencyName);
        //    globalHahTable.Add("agencyAddressLine1", agency.Address.Line1);
        //    globalHahTable.Add("agencyAddressLine2", agency.Address.Line2);
        //    globalHahTable.Add("agencyCity", agency.Address.CityName);
        //    if (agency.Address.Phone != null && agency.Address.Phone.Length != 0)
        //    {
        //        globalHahTable.Add("agencyAddressPhone", "Phone:" + agency.Address.Phone);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("agencyAddressPhone", "");

        //    }
        //    if (agency.Address.Mobile != null && agency.Address.Mobile.Length != 0)
        //    {
        //        globalHahTable.Add("mobile", "Mobile: " + agency.Address.Mobile);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("mobile", "");

        //    }

        //    if (agency.Address.Fax != null && agency.Address.Fax.Length != 0)
        //    {
        //        globalHahTable.Add("fax", "Fax:" + agency.Address.Fax);
        //    }
        //    else
        //    {
        //        globalHahTable.Add("fax", "");
        //    }
        //    globalHahTable.Add("travelDate", "Travel Date:" + travelDate.ToString("dd MMM yyyy"));
        //    globalHahTable.Add("ServiceTaxRegNumber", ConfigurationManager.AppSettings["ServiceTaxRegNo"].ToString());
        //    string filePath = string.Empty;
        //    decimal cancellationCharge = 0;
        //    //Html code in text file for E-mailing Invoice
        //    string subject = string.Empty;
        //    filePath = ConfigurationManager.AppSettings["EmailTrainCreditNote"].ToString();

        //    subject = "Credit Note";
        //    StreamReader sr = new StreamReader(filePath);
        //    //string contains the code of loop
        //    string loopString = string.Empty;
        //    bool loopStarts = false;
        //    //string contains the code before loop 
        //    string startString = string.Empty;
        //    //string contains the code after loop
        //    string endString = string.Empty;
        //    bool loopEnds = false;
        //    #region seperate out the loop code and the other code
        //    while (!sr.EndOfStream)
        //    {
        //        string line = sr.ReadLine();
        //        if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
        //        {
        //            loopEnds = true;
        //            loopStarts = false;
        //            if (line.IndexOf("%loopEnds%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            endString += line;
        //        }
        //        if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
        //        {
        //            if (line.IndexOf("%loopStarts%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            loopString += line;
        //            loopStarts = true;
        //        }
        //        else
        //        {
        //            if (!loopEnds)
        //            {
        //                startString += line;
        //            }
        //        }
        //    }
        //    #endregion
        //    string midString = string.Empty;
        //    int refundPaxCount = 1;
        //    cancellationCharge = 0;

        //    decimal netTotal = 0;
        //    foreach (TrainCreditNoteQueue tcn in lst)
        //    {
        //        string train = tcn.Detail;
        //        string paxName = tcn.PaxName;
        //        string paxType = tcn.PaxType.ToString();
        //        string bookingClass = tcn.BookingClass;
        //        string fare = tcn.TotalFare.ToString("N2");
        //        string otherCharges = tcn.OtherCharges.ToString("N2");
        //        cancellationCharge += Convert.ToDecimal(tcn.SupplierFee);
        //        netTotal += Convert.ToDecimal(tcn.TotalFare)+Convert.ToDecimal(tcn.OtherCharges);
        //        //Temporary hashtable which replaces loopvariable continuously
        //        Hashtable tempTable = new Hashtable();
        //        tempTable.Add("serialNumber", refundPaxCount.ToString());
        //        tempTable.Add("Train", train);
        //        tempTable.Add("Pax Name", paxName);
        //        tempTable.Add("Pax Type", paxType);
        //        tempTable.Add("Class", bookingClass);
        //        tempTable.Add("Fare", fare);
        //        tempTable.Add("otherCharges", otherCharges);
        //        midString += Email.ReplaceHashVariable(loopString, tempTable);
        //        refundPaxCount++;
        //    }
        //    string from = new UserMaster(memberId).Email;
        //    string fullString = startString + midString + endString;
        //    List<string> toArray = new List<string>();
        //    string addressList = Request["addressList"];
        //    string[] addressArray = addressList.Split(',');
        //    for (int k = 0; k < addressArray.Length; k++)
        //    {
        //        toArray.Add(addressArray[k]);
        //    }
        //    globalHahTable.Add("gross", netTotal.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("cancellationCharge", cancellationCharge.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    globalHahTable.Add("netRefundable", (netTotal-cancellationCharge).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
        //    UserMaster member = new UserMaster(lst[0].CreatedBy);
        //    globalHahTable.Add("ticketedBy", member.FirstName + " " + member.LastName);
        //    try
        //    {
        //        Email.Send(from, from, toArray, subject, fullString, globalHahTable);
        //    }
        //    catch (System.Net.Mail.SmtpException)
        //    {
        //        CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Smtp is unable to send the message", "");
        //    }


        //    #endregion
        //}

        #endregion
        else
        {
            Response.Write("There is no itinerary which is selected so please select the itinerary");
        }
    }
}
