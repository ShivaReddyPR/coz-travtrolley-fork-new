using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Linq;

public partial class ETicketOLD :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Ticket ticket;
    protected Ticket routingTicket;
    protected FlightItinerary flightItinerary;
    protected FlightItinerary routingItinerary;
    protected AgentMaster agency;
    
    protected string logo = string.Empty;
    protected string cityName = string.Empty;
    protected int paxIndex = -1;
    protected string paxName = string.Empty;
    protected string routingPaxName = string.Empty;
    protected string paxType = string.Empty;
    protected int paxTypeIndex = 0;   // for ptcDetail
    protected bool isAgent = true;
    //protected UserMaster loggedMember;
    protected string preferenceValue = string.Empty;
    protected string ffNumber = string.Empty;
    protected UserPreference preference = new UserPreference();
    //protected Insurance insurance;
    //protected PolicyDetail policy;
    //protected decimal totalInsurancePrice = 0;
    protected string segmentMessage = string.Empty;
    protected string flightNumberString = string.Empty;
    protected UserPreference serviceFeePref = new UserPreference();
    protected decimal serviceFee = 0;
    protected bool showServiceFee;
    protected bool isDN = false;
    BookingMode mode;
    protected BookingDetail booking;
    protected BookingDetail routingBooking;
    protected Airline airline;
    protected Airline routingAirline;
    protected bool IsShowLogo = false;
    protected bool IsShowHotelAd = false; // Bug ID : 0029414- E-Ticket Ads
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected string AgentLogoPath = CT.Configuration.ConfigurationSystem.Core["virtualAgentLogoPath"];
    protected HotelSearchResult[] search = new HotelSearchResult[0];
    protected HotelRequest req;
    protected CityMaster city = new CityMaster();
    protected int cityId = 0;
    protected string inBaggage = "", outBaggage = "", routingInBaggage = string.Empty, routingOutBaggage = string.Empty;
    protected List<Ticket> ticketList;
    protected List<Ticket> routingTicketList;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        //loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        //if (loggedMember.AgentId <= 1)
        if(Settings.LoginInfo.AgentType==AgentType.BaseAgent || Settings.LoginInfo.AgentType==AgentType.Agent) 
        {
            isAgent = false;
        }

       
        if (Request["ticketId"] != null || Request["pendingId"] != null || Request["FlightId"] != null)
        {
            if (Request["pendingId"] != null)
            {
                int pendingId = Convert.ToInt32(Request["pendingId"]);
                flightItinerary = FailedBooking.LoadItinerary(pendingId);
                booking = new BookingDetail();
                booking.Status = BookingStatus.Ticketed;
                booking.CreatedBy = flightItinerary.CreatedBy;
                booking.AgencyId = flightItinerary.AgencyId;
                agency = new AgentMaster(flightItinerary.AgencyId);
                FailedBooking fb = FailedBooking.Load((flightItinerary.PNR));
                booking.CreatedOn = fb.CreatedOn;
                mode = BookingMode.Auto;
            }
            else if(Request["ticketId"] != null)
            {
                hdfTicketType.Value = "Individual";
                ticket = new Ticket();
                ticket.Load(Convert.ToInt32(Request["ticketId"]));
                ticketList = new List<Ticket>();
                ticketList.Add(ticket);
                flightItinerary = new FlightItinerary(ticket.FlightId);
                try
                {
                    if (!string.IsNullOrEmpty(flightItinerary.RoutingTripId))
                    {
                        routingItinerary = new FlightItinerary(FlightItinerary.GetFlightIdbyRoutingTripId(flightItinerary.RoutingTripId, flightItinerary.PNR));
                        routingAirline = new Airline();
                        routingAirline.Load(routingItinerary.ValidatingAirlineCode);
                        routingBooking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(routingItinerary.FlightId));

                        if (routingAirline.LogoFile.Trim().Length != 0)
                        {
                            string[] fileExtension = routingAirline.LogoFile.Split('.');
                            AirlineLogoPath = AirlineLogoPath + routingItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                        }

                        List<Ticket> tickets = Ticket.GetTicketList(routingItinerary.FlightId);

                        if (tickets != null && tickets.Count > 0)
                        {
                            routingTicket = tickets.Find(t => t.PaxFirstName + "" + t.PaxLastName == ticket.PaxFirstName + "" + t.PaxLastName);
                            routingTicketList = new List<Ticket>();
                            routingTicketList.Add(routingTicket);

                            if (routingItinerary.IsLCC && (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.TBOAir || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl) || routingItinerary.FlightBookingSource == BookingSource.PKFares || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                            {
                                FlightPassenger pax = new FlightPassenger();
                                pax.Load(routingTicket.PaxId);
                                //foreach (FlightPassenger pax in routingItinerary.Passenger)
                                //{
                                if (routingInBaggage.Length > 0)
                                {
                                    routingInBaggage += ", " + pax.BaggageCode.Split(',')[0];
                                }
                                else
                                {
                                    routingInBaggage = pax.BaggageCode.Split(',')[0];
                                }
                                if (pax.BaggageCode.Split(',').Length > 1)
                                {
                                    if (routingOutBaggage.Length > 0)
                                    {
                                        routingOutBaggage += ", " + pax.BaggageCode.Split(',')[1];
                                    }
                                    else
                                    {
                                        routingOutBaggage = pax.BaggageCode.Split(',')[1];
                                    }
                                }
                                else
                                {
                                    if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                    {
                                        routingOutBaggage = routingInBaggage;//In Case 15Kg show it both ways
                                    }
                                }
                                if (routingItinerary.FlightBookingSource == BookingSource.TBOAir && pax.Type != PassengerType.Infant)
                                {
                                    routingInBaggage = routingInBaggage != string.Empty ? routingInBaggage : "Airline norms";
                                    routingOutBaggage = routingOutBaggage != string.Empty ? routingOutBaggage : "Airline norms";

                                    routingInBaggage = routingInBaggage.Contains("|") ? routingInBaggage.Split('|')[routingInBaggage.Split('|').Length - 1] + " Kg" : routingInBaggage;
                                    routingOutBaggage = routingOutBaggage.Contains("|") ? routingOutBaggage.Split('|')[routingOutBaggage.Split('|').Length - 1] + " Kg" : routingOutBaggage;
                                }
                                //}
                            }
                            else if (routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                int paxIndex = 0;
                                if (Request["paxId"] != null)
                                {
                                    paxIndex = Convert.ToInt32(Request["paxId"]);

                                    switch (routingItinerary.Passenger[paxIndex].Type)
                                    {
                                        case PassengerType.Adult:
                                            paxType = "ADT";
                                            break;
                                        case PassengerType.Child:
                                            paxType = "CNN";
                                            break;
                                        case PassengerType.Infant:
                                            paxType = "INF";
                                            break;
                                    }
                                }
                            }
                            else if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                routingInBaggage = routingInBaggage != string.Empty ? routingInBaggage : "Airline norms";
                                routingOutBaggage = routingOutBaggage != string.Empty ? routingOutBaggage : "Airline norms";

                                routingInBaggage = routingInBaggage.Contains("|") ? routingInBaggage.Split('|')[routingInBaggage.Split('|').Length - 1] + " Kg" : routingInBaggage;
                                routingOutBaggage = routingOutBaggage.Contains("|") ? routingOutBaggage.Split('|')[routingOutBaggage.Split('|').Length - 1] + " Kg" : routingOutBaggage;

                            }

                        }
                    }
                }
                catch {}
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(ticket.FlightId));
                
                int productid = Product.GetProductIdByBookingId(booking.BookingId, ProductType.Flight);
                mode = Product.GetBookingMode(productid, ProductType.Flight);
                agency = new AgentMaster(flightItinerary.AgencyId);
                
               // Audit.Add(EventType.Exception, Severity.High, 1, "BK agent ID: " + booking.AgencyId.ToString()+" , BK id:  " + booking.BookingId.ToString(), Request["REMOTE_ADDR"]);

                if (flightItinerary.IsLCC && (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl) || flightItinerary.FlightBookingSource == BookingSource.PKFares || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                {
                    FlightPassenger pax = new FlightPassenger();
                    pax.Load(ticket.PaxId);
                    //foreach (FlightPassenger pax in flightItinerary.Passenger)
                    //{
                        if (inBaggage.Length > 0)
                        {
                            inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                        }
                        else
                        {
                            inBaggage = pax.BaggageCode.Split(',')[0];
                        }
                        if (pax.BaggageCode.Split(',').Length > 1)
                        {
                            if (outBaggage.Length > 0)
                            {
                                outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                            }
                            else
                            {
                                outBaggage = pax.BaggageCode.Split(',')[1];
                            }
                        }
                        else
                        {
                            if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                outBaggage = inBaggage;//In Case 15Kg show it both ways
                            }
                        }
                        if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && pax.Type != PassengerType.Infant)
                        {
                            inBaggage = inBaggage != string.Empty ? inBaggage : "Airline norms";
                            outBaggage = outBaggage != string.Empty ? outBaggage : "Airline norms";

                            inBaggage = inBaggage.Contains("|") ? inBaggage.Split('|')[inBaggage.Split('|').Length - 1] + " Kg" : inBaggage;
                            outBaggage = outBaggage.Contains("|") ? outBaggage.Split('|')[outBaggage.Split('|').Length - 1] + " Kg" : outBaggage;
                        }
                    //}
                }
                else if (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    int paxIndex = 0;
                    if (Request["paxId"] != null)
                    {
                        paxIndex = Convert.ToInt32(Request["paxId"]);

                        switch (flightItinerary.Passenger[paxIndex].Type)
                        {
                            case PassengerType.Adult:
                                paxType = "ADT";
                                break;
                            case PassengerType.Child:
                                paxType = "CNN";
                                break;
                            case PassengerType.Infant:
                                paxType = "INF";
                                break;
                        }
                    }
                }
                else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    inBaggage = inBaggage != string.Empty ? inBaggage : "Airline norms";
                    outBaggage = outBaggage != string.Empty ? outBaggage : "Airline norms";

                    inBaggage = inBaggage.Contains("|") ? inBaggage.Split('|')[inBaggage.Split('|').Length - 1] + " Kg" : inBaggage;
                    outBaggage = outBaggage.Contains("|") ? outBaggage.Split('|')[outBaggage.Split('|').Length - 1] + " Kg" : outBaggage;

                }
                try
                {
                    if (!string.IsNullOrEmpty(flightItinerary.RoutingTripId))
                    {
                        routingItinerary = new FlightItinerary(FlightItinerary.GetFlightIdbyRoutingTripId(flightItinerary.RoutingTripId, flightItinerary.PNR));
                        routingBooking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(routingItinerary.FlightId));

                        if (routingBooking.Status == BookingStatus.Ticketed)
                        {
                            routingTicketList = Ticket.GetTicketList(routingItinerary.FlightId);
                        }
                        else
                        {
                            routingTicketList = new List<Ticket>();
                        }

                        routingAirline = new Airline();
                        routingAirline.Load(routingItinerary.ValidatingAirlineCode);

                        if (routingAirline.LogoFile.Trim().Length != 0)
                        {
                            string[] fileExtension = routingAirline.LogoFile.Split('.');
                            AirlineLogoPath = AirlineLogoPath + routingItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                        }

                        if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.TBOAir || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                        {
                            foreach (FlightPassenger pax in routingItinerary.Passenger)
                            {
                                if (routingInBaggage.Length > 0)
                                {
                                    routingInBaggage += ", " + pax.BaggageCode.Split(',')[0];
                                }
                                else
                                {
                                    routingInBaggage = pax.BaggageCode.Split(',')[0];
                                }
                                if (pax.BaggageCode.Split(',').Length > 1)
                                {
                                    if (routingOutBaggage.Length > 0)
                                    {
                                        routingOutBaggage += ", " + pax.BaggageCode.Split(',')[1];
                                    }
                                    else
                                    {
                                        routingOutBaggage = pax.BaggageCode.Split(',')[1];
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }
            }
            else if (Request["flightId"] != null && Request["paxId"] != null && Request["bkg"] != null)
            {
                int flightId = Convert.ToInt32(Request["FlightId"]);
                flightItinerary = new FlightItinerary(flightId);
                hdfTicketType.Value = "Individual";
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
                
                int productid = Product.GetProductIdByBookingId(booking.BookingId, ProductType.Flight);
                mode = Product.GetBookingMode(productid, ProductType.Flight);
                // Audit.Add(EventType.Exception, Severity.High, 1, "BK agent ID: " + booking.AgencyId.ToString() + " , BK id:  " + booking.BookingId.ToString(), Request["REMOTE_ADDR"]);
                agency = new AgentMaster(flightItinerary.AgencyId);

                int paxIndex = Convert.ToInt32(Request["paxId"]);
                if (paxIndex >= 0)
                {
                    paxName = FlightPassenger.GetPaxFullName(flightItinerary.Passenger[paxIndex].PaxId);
                   
                    if (flightItinerary.FlightBookingSource == BookingSource.UAPI)
                    {
                        switch (flightItinerary.Passenger[paxIndex].Type)
                        {
                            case PassengerType.Adult:
                                paxType = "ADT";
                                break;
                            case PassengerType.Child:
                                paxType = "CNN";
                                break;
                            case PassengerType.Infant:
                                paxType = "INF";
                                break;
                        }
                    }
                    FlightPassenger pax = new FlightPassenger();
                    pax.Load(flightItinerary.Passenger[paxIndex].PaxId);
                    //foreach (FlightPassenger pax in flightItinerary.Passenger)
                    //{
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                    else
                    {
                        if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            outBaggage = inBaggage;//In Case 15Kg show it both ways
                        }
                    }
                }
                else
                {
                    paxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
                }
                paxName = paxName.Trim();
                ffNumber = flightItinerary.Passenger[paxIndex].FFAirline + flightItinerary.Passenger[paxIndex].FFNumber;

                airline = new Airline();
                airline.Load(flightItinerary.ValidatingAirlineCode);

                if (airline.LogoFile.Trim().Length != 0)
                {
                    string[] fileExtension = airline.LogoFile.Split('.');
                    AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                }
                //If the booking is hold then no need of checking Routing trip since we are generating second ticket only when onward ticket is successful

            }
            else if (Request["FlightId"] != null )
            {
                hdfTicketType.Value = "Multiple";
                int flightId = Convert.ToInt32(Request["FlightId"]);
                flightItinerary = new FlightItinerary(flightId);
               
                if (Request["bkg"] == null)
                {
                    ticketList = Ticket.GetTicketList(flightId);
                    booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(ticketList[0].FlightId));
                }
                else
                {
                    booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
                }
                int productid = Product.GetProductIdByBookingId(booking.BookingId, ProductType.Flight);
                mode = Product.GetBookingMode(productid, ProductType.Flight);
               // Audit.Add(EventType.Exception, Severity.High, 1, "BK agent ID: " + booking.AgencyId.ToString() + " , BK id:  " + booking.BookingId.ToString(), Request["REMOTE_ADDR"]);
                agency = new AgentMaster(flightItinerary.AgencyId);
                if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                {
                    foreach (FlightPassenger pax in flightItinerary.Passenger)
                    {
                        if (inBaggage.Length > 0)
                        {
                            inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                        }
                        else
                        {
                            inBaggage = pax.BaggageCode.Split(',')[0];
                        }
                        if (pax.BaggageCode.Split(',').Length > 1)
                        {
                            if (outBaggage.Length > 0)
                            {
                                outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                            }
                            else
                            {
                                outBaggage = pax.BaggageCode.Split(',')[1];
                            }
                        }
                    }
                }
                //if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                //{
                //    inBaggage = inBaggage != string.Empty ? inBaggage : "Airline norms";
                //    outBaggage = outBaggage != string.Empty ? outBaggage : "Airline norms";

                //    //if (inBaggage.Contains("KG") || inBaggage.Contains("Kg"))
                //    if(inBaggage.ToLower().Contains("kg"))
                //    {
                //        inBaggage = inBaggage.Contains("|") ? inBaggage.Split('|')[inBaggage.Split('|').Length - 1] : inBaggage;

                //    }
                //    else
                //    {
                //        inBaggage = inBaggage.Contains("|") ? inBaggage.Split('|')[inBaggage.Split('|').Length - 1] + " Kg" : inBaggage;
                //    }
                //    //if (outBaggage.Contains("KG") || outBaggage.Contains("Kg"))
                //    if(outBaggage.ToLower().Contains("kg"))
                //    {
                //        outBaggage = outBaggage.Contains("|") ? outBaggage.Split('|')[outBaggage.Split('|').Length - 1] : outBaggage;
                //    }
                //    else
                //    {
                //        outBaggage = outBaggage.Contains("|") ? outBaggage.Split('|')[outBaggage.Split('|').Length - 1] + " Kg" : outBaggage;
                //    }
                //}
                airline = new Airline();
                airline.Load(flightItinerary.ValidatingAirlineCode);

                if (airline.LogoFile.Trim().Length != 0)
                {
                    string[] fileExtension = airline.LogoFile.Split('.');
                    AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                }

                try
                {
                    if (!string.IsNullOrEmpty(flightItinerary.RoutingTripId) && Request["bkg"] == null)
                    {
                        routingItinerary = new FlightItinerary(FlightItinerary.GetFlightIdbyRoutingTripId(flightItinerary.RoutingTripId, flightItinerary.PNR));
                        routingBooking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(routingItinerary.FlightId));

                        if (routingBooking.Status == BookingStatus.Ticketed)
                        {
                            routingTicketList = Ticket.GetTicketList(routingItinerary.FlightId);
                        }
                        else
                        {
                            routingTicketList = new List<Ticket>();
                        }

                        routingAirline = new Airline();
                        routingAirline.Load(routingItinerary.ValidatingAirlineCode);

                        if (routingAirline.LogoFile.Trim().Length != 0)
                        {
                            string[] fileExtension = routingAirline.LogoFile.Split('.');
                            AirlineLogoPath = AirlineLogoPath + routingItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                        }

                        if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.TBOAir || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                        {
                            foreach (FlightPassenger pax in routingItinerary.Passenger)
                            {
                                if (routingInBaggage.Length > 0)
                                {
                                    routingInBaggage += ", " + pax.BaggageCode.Split(',')[0];
                                }
                                else
                                {
                                    routingInBaggage = pax.BaggageCode.Split(',')[0];
                                }
                                if (pax.BaggageCode.Split(',').Length > 1)
                                {
                                    if (routingOutBaggage.Length > 0)
                                    {
                                        routingOutBaggage += ", " + pax.BaggageCode.Split(',')[1];
                                    }
                                    else
                                    {
                                        routingOutBaggage = pax.BaggageCode.Split(',')[1];
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }
            }
            // Bug ID : 0029414- E-Ticket Ads
            #region Show Hotel Ad on E-Ticket
            UserMaster member = new UserMaster(Settings.LoginInfo.UserID);
            //int agencyId = 0;
            //if (Request["ticketId"] != null)
            //{
            //    agencyId = Ticket.GetAgencyIdForTicket(ticket.TicketId);
            //}
            //else if (Request["FlightId"] != null)
            //{
            //    agencyId = Ticket.GetAgencyIdForTicket(ticketList[0].TicketId);
            //}
            //agency = new  AgentMaster(agencyId);
            
            //member.Load(Member.GetPrimaryMemberId(agency.AgencyId));TODO
            UserPreference pref = new UserPreference();
            pref = pref.GetPreference((int)member.ID, pref.ShowHotelAdOnEticket, pref.ETicket);
            if (pref.Value != null)
            {
                if (pref.Value.ToUpper() == "TRUE")
                {
                    IsShowHotelAd = true;
                }
            }
            else
            {
                IsShowHotelAd = true;
            }
            #endregion

            //if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]) && IsShowHotelAd == true) // Bug ID : 0029414 - Check for show Ad
           // {
                //if (flightItinerary.IsDomestic)
                //{
                //    city = new CityMaster();
                //    //city.Load(flightItinerary.Destination);TODO
                //    HotelSearchResult res = new HotelSearchResult();
                //    search = res.LoadTBOResults(city.Name);
                //}
          //  }
            if (mode == BookingMode.Manual || mode == BookingMode.ManualImport)
            {
                string firstName = flightItinerary.Passenger[0].FirstName;
                string lastName = flightItinerary.Passenger[0].LastName;
                string pnr = flightItinerary.PNR;
                string origin = flightItinerary.Origin;
                string destination = flightItinerary.Destination;
                if (flightItinerary.FlightBookingSource == BookingSource.WorldSpan)
                {
                    Response.Redirect(Request.Url.Scheme+"://mytripandmore.com/Frameset.aspx?pageName=Itinerary.aspx&PNR=" + pnr + "&clockFormat=12&lastName=" + ticket.PaxFirstName + "");
                }
                else
                {
                    Response.Redirect("EticketInformation.aspx?bookingSource=" + flightItinerary.FlightBookingSource + "&pnr=" + pnr + "&firstName=" + firstName + "&lastName=" + lastName + "&origin=" + origin + "&destination=" + destination + "");
                }
            }


            if (  isAgent && booking.AgencyId != Settings.LoginInfo.AgentId )
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "Is agent: " + isAgent.ToString(), Request["REMOTE_ADDR"]);
                //Audit.Add(EventType.Exception, Severity.High, 1, "Booking agent: " + booking.AgencyId.ToString()+" ,logged agentid: "+loggedMember.AgentId.ToString(), Request["REMOTE_ADDR"]);
                ErrorMessage.Text = "Access Denied.";
                MultiViewETicket.ActiveViewIndex = 1;
            }
            else
            {
                if (Request["serviceFee"] != null)
                {
                if (flightItinerary.Passenger[0].Price.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                    {
                        ticket.ServiceFee = (flightItinerary.Passenger[0].Price.RateOfExchange* Convert.ToDecimal(Request["serviceFee"]));
                    }
                    else
                    {
                        ticket.ServiceFee = Convert.ToDecimal(Request["serviceFee"]);
                    }
                 

                    ticket.ShowServiceFee = (ServiceFeeDisplay)Enum.Parse(typeof(ServiceFeeDisplay), Request["serviceFeeDisplay"].ToString());
                    ticket.Save();
                }
                MultiViewETicket.ActiveViewIndex = 0;
                if (Request["ticketId"] != null)
                {
                    for (int i = 0; i < flightItinerary.Passenger.Length; i++)
                    {
                        if (flightItinerary.Passenger[i].PaxId == ticket.PaxId)
                        {
                            paxIndex = i;
                            break;
                        }
                    }
                    if (paxIndex >= 0)
                    {
                        paxName = FlightPassenger.GetPaxFullName(flightItinerary.Passenger[paxIndex].PaxId);
                    }
                    else
                    {
                        paxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
                    }
                    paxName = paxName.Trim();
                    ffNumber = flightItinerary.Passenger[paxIndex].FFAirline + flightItinerary.Passenger[paxIndex].FFNumber;
                    pref = pref.GetPreference((int)member.ID, pref.ShowLogoOnEticket, pref.ETicket);
                    if (pref.Value != null)
                    {
                        if (pref.Value.ToUpper() == "TRUE")
                        {
                            IsShowLogo = true;
                        }
                    }

                    airline = new Airline();
                    airline.Load(flightItinerary.ValidatingAirlineCode);

                    //if (member.LogoFile != null && member.LogoFile.Trim().Length > 0)TODO
                    //{
                    //    string[] fileExtension = member.LogoFile.Split('.');
                    //    AgentLogoPath = AgentLogoPath + "\\" + member.LoginName + "." + fileExtension[fileExtension.Length - 1];
                    //}
                    if (airline.LogoFile.Trim().Length != 0)
                    {
                        string[] fileExtension = airline.LogoFile.Split('.');
                        AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                    }

                    for (int i = 0; i < ticket.PtcDetail.Count; i++)
                    {
                        if (ticket.PaxType == FlightPassenger.GetPassengerType(ticket.PtcDetail[i].PaxType))
                        {
                            paxTypeIndex = i;
                            break;
                        }
                    }

                    //policy = PolicyDetail.GetPolicyDetailByPaxId(flightItinerary.Passenger[paxIndex].PaxId);
                    serviceFee = ticket.ServiceFee;
                    if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                    {
                        showServiceFee = true;
                    }
                    else
                    {
                        showServiceFee = false;
                    }
                }
                else if (Request["pendingId"] != null)
                {
                    paxIndex = Convert.ToInt32(Request["PaxId"]);
                    paxName = flightItinerary.Passenger[paxIndex].FirstName + flightItinerary.Passenger[paxIndex].LastName;
                    ffNumber = flightItinerary.Passenger[paxIndex].FFAirline + flightItinerary.Passenger[paxIndex].FFNumber;
                    agency = new AgentMaster(booking.AgencyId);
                }
                
                if (agency.City.Length > 0)
                {
                    cityName = agency.City;//RegCity.GetCity(cityId).CityName;
                }
                else
                {
                    cityName = Settings.LoginInfo.LocationName;
                }
                //logo = Agency.GetAgencyLogo(agency.AgencyId);TODO
                //for (int i = 0; i < ticket.PtcDetail.Count; i++)
                //{
                //    if (ticket.PaxType == FlightPassenger.GetPassengerType(ticket.PtcDetail[i].PaxType))
                //    {
                //        paxTypeIndex = i;
                //        break;
                //    }
                //}
                preference = preference.GetPreference((int)Settings.LoginInfo.UserID, preference.ETicket, preference.ETicket);
                if (preference.Value != null)
                {
                    preferenceValue = preference.Value.ToUpper();
                }
                else
                {
                    preferenceValue = preference.ETicketShowFare;
                }
                //policy = PolicyDetail.GetPolicyDetailByPaxId(flightItinerary.Passenger[paxIndex].PaxId);
                //serviceFee = ticket.ServiceFee;
                //if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                //{
                //    showServiceFee = true;
                //}
                //else
                //{
                //    showServiceFee = false;
                //}
            }
        }
        string serverPath = "";
        string logoPath = "";
        if (Request.Url.Port > 80)
        {
            serverPath = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
        }
        else
        {
            serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
        }
        //to show agent logo
        if (agency.ID> 1)
        {

            logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + agency.ImgFileName;
            imgLogo.ImageUrl = logoPath;
            Image1.ImageUrl = logoPath;
        }
        else
        {
            imgLogo.ImageUrl = serverPath + "images/logo.jpg";
            Image1.ImageUrl = serverPath + "images/logo.jpg";
        } 
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }

    protected void btnEmailVoucher_Click(object sender, EventArgs e)
    {
        try
        {



            #region Sending Email
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]))
            {                

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

                toArray.Add(txtEmailId.Text.Trim());
                btnEmail.Visible = false;
                btnPrint.Visible = false;
                imgEmail.Visible = false;
                imgPrint.Visible = false;
                ancFare.Visible = false;
                ancFareRouting.Visible = false;                

                if (hdfFare.Value == "0")
                {
                    divFare.Visible = false;
                    divFareRouting.Visible = false;
                }
                string message = "";

                System.IO.StringWriter sWriter = new System.IO.StringWriter();
                HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
                PrintDiv.RenderControl(htWriter);
                message += sWriter.ToString();
                message = message.Replace("white", "black");//to display header in manual email, change text color from white to black  
                
                try
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "From Mail:" +ConfigurationManager.AppSettings["fromEmail"], Request["REMOTE_ADDR"]);
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Air Reservation", message, null);
                    Audit.Add(EventType.Email, Severity.Normal, 0, "After Mail", Request["REMOTE_ADDR"]);
                }

                catch (System.Net.Mail.SmtpException ex)
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "Error Reason :" + ex.ToString(), Request["REMOTE_ADDR"]);
                    throw ex;
                }
                finally
                {
                    btnEmail.Visible = true;
                    btnPrint.Visible = true;
                    imgEmail.Visible = true;
                    imgPrint.Visible = true;
                    ancFare.Visible = true;
                    divFare.Visible = true;
                    divFareRouting.Visible = true;
                    ancFareRouting.Visible = true;
                }
            }
            #endregion

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.Normal, 0, "Failed to send Ticket Voucher to " + txtEmailId.Text + " Reason " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    //Added by lokesh on 4-July-2018
    /// <summary>
    /// This method returns the baggage for GDS suppliers from table BKE_Segment_PTC_Detail
    /// </summary>
    /// <param name="flightId"></param>
    /// <returns></returns>
    protected string GetBaggageForGDS(int flightId, PassengerType paxType)
    {
        string gdsBaggage = string.Empty;
        try
        {
            List<SegmentPTCDetail> segmentPTCDetails = new List<SegmentPTCDetail>();
            List<SegmentPTCDetail> paxTypeSegmentPTCDetails = new List<SegmentPTCDetail>();
            string paxCode = string.Empty;
            switch (paxType)
            {
                case PassengerType.Adult:
                    paxCode = "ADT";
                    break;
                case PassengerType.Child:
                       paxCode = "CNN";                    
                    break;
                case PassengerType.Infant:
                    paxCode = "INF";
                    break;
            }

            segmentPTCDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightId);
            paxTypeSegmentPTCDetails = segmentPTCDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.PaxType.ToLower().Equals(paxCode.ToLower()); });
            if (paxTypeSegmentPTCDetails != null && paxTypeSegmentPTCDetails.Count > 0)
            {
                for (int i = 0; i < paxTypeSegmentPTCDetails.Count; i++)
                {
                    if (!string.IsNullOrEmpty(paxTypeSegmentPTCDetails[i].Baggage))
                    {
                        if (string.IsNullOrEmpty(gdsBaggage))
                        {
                            gdsBaggage = paxTypeSegmentPTCDetails[i].Baggage;
                        }
                        else
                        {
                            gdsBaggage += "," + paxTypeSegmentPTCDetails[i].Baggage;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to read baggage" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        if (string.IsNullOrEmpty(gdsBaggage) && paxType != PassengerType.Infant)
        {
            gdsBaggage = "Airline Norms";
        }
        return gdsBaggage;
    }
    public void GetConfigData(string appkey)
    {
        AgentAppConfig agentAppConfig = new AgentAppConfig();
        agentAppConfig.AgentID = 1;
        List<AgentAppConfig> agentAppConfigs = agentAppConfig.GetConfigData();
        //lblConfigData.Text = agentAppConfigs.Where(x => Convert.ToInt32(x.Source) == (int)BookingSource.FlightInventory && x.AppKey.ToUpper() == appkey.ToUpper() && x.AppValue.ToUpper() == "TRUE").FirstOrDefault().Description;
        lblConfigData.Text = agentAppConfigs.Where(x =>  x.AppKey.ToUpper() == appkey.ToUpper() && x.AppValue.ToUpper() == "TRUE").FirstOrDefault().Description;
    }

}

