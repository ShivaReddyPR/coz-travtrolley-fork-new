﻿using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using System.Configuration;
using System.Net.Http;

namespace CozmoB2BWebApp
{
    public partial class TransferQueue : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            else
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            hdnUserId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.UserID);
            TokenGeneration();
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object LoadAgents()
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            return JsonConvert.SerializeObject(dtAgents);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object LoadB2BAgents(int agentId)
        {
            DataTable dtAgents = AgentMaster.GetList(1, string.Empty, agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            return JsonConvert.SerializeObject(dtAgents);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object LoadB2B2BAgents(int agentId)
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            return JsonConvert.SerializeObject(dtAgents);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object TransferSources()
        {
            Array Sources = Enum.GetValues(typeof(TransferBookingSource));
            List<Dictionary<string, string>> SourceList = new List<Dictionary<string, string>>();
            foreach (TransferBookingSource source in Sources)
            {
                Dictionary<string, string> SourceItem = new Dictionary<string, string>();
                SourceItem["ItemName"] = Enum.GetName(typeof(TransferBookingSource), source);
                SourceItem["ItemValue"]= ((int)source).ToString();
                SourceList.Add(SourceItem);
            }
            return JsonConvert.SerializeObject(SourceList);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object TransferStatus()
        {
            Array Statuses = Enum.GetValues(typeof(TransferBookingStatus));
            List<Dictionary<string, string>> StatusList = new List<Dictionary<string, string>>();
            foreach (TransferBookingStatus status in Statuses)
            {
                Dictionary<string, string> SourceItem = new Dictionary<string, string>();
                SourceItem["ItemName"] = Enum.GetName(typeof(TransferBookingStatus), status);
                SourceItem["ItemValue"] = ((int)status).ToString();
                StatusList.Add(SourceItem);
            }
            return JsonConvert.SerializeObject(StatusList);
        }

        public void TokenGeneration()
        {
            HttpCookie aCookie = Request.Cookies["Session_Token"];
            string baseAddress = ConfigurationManager.AppSettings["TransferWebApiUrl"];
            using (var client = new HttpClient())
            {
                var form = new Dictionary<string, string>
               {
                   {"grant_type", "password"},
                   {"UserName", "cozmo"},
                   {"Password", "cozmo"},
               };
                var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;
                //var token = tokenResponse.Content.ReadAsStringAsync().Result;  
                var token = tokenResponse.Content.ReadAsStringAsync().Result;
                Token ApiResponseObj = JsonConvert.DeserializeObject<Token>(token);
                string resp = string.Empty;
                HttpCookie tokenCookie = new HttpCookie("Session_Token");
                DateTime now = DateTime.Now;
                // Set the cookie value.
                tokenCookie.Value = ApiResponseObj.AccessToken;
                // Set the cookie expiration date.
                tokenCookie.Expires = now.AddMinutes(ApiResponseObj.ExpiresIn);
                Response.SetCookie(tokenCookie);
            }
        }
        public class Token
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }
            [JsonProperty("token_type")]
            public string TokenType { get; set; }
            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }
            [JsonProperty("refresh_token")]
            public string RefreshToken { get; set; }
            [JsonProperty("error")]
            public string Error { get; set; }
            [JsonProperty("error_description")]
            public string ErrorDescription { get; set; }
        }

    }
    
}
