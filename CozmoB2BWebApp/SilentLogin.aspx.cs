﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CozmoB2BWebApp
{
    public partial class SilentLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> keys = null;
            try
            {
                if (Request.Form.AllKeys.Length > 0)
                {
                    keys = Request.Form.AllKeys.ToList();
                }
                if (keys != null && keys.Contains("AUTHTOKEN") && keys.Contains("USERNAME") && keys.Contains("TIMESTAMP"))
                {
                    if (!string.IsNullOrEmpty(Request.Form["AUTHTOKEN"].ToString()) && !string.IsNullOrEmpty(Request.Form["USERNAME"].ToString()) && !string.IsNullOrEmpty(Request.Form["TIMESTAMP"].ToString()))
                    {
                        string secretKey = GetConfigData("silentLoginSecretKey");
                        string authToken = Request.Form["AUTHTOKEN"].ToString();
                        string username = Request.Form["USERNAME"].ToString();
                        string timestamp = Request.Form["TIMESTAMP"].ToString();
                        Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login Parameters Username: " + username + " Timestamp :" + timestamp + " AuthToken :" + authToken, "");
                        string genToken = encryptAuthToken(username + "|" + timestamp, secretKey);
                        Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login Authentication Token :" + genToken, "");
                        if (!string.IsNullOrEmpty(authToken) && !string.IsNullOrEmpty(username) && genToken == authToken)
                        {
                            bool userStatus = UserMaster.silentLogin(username);
                            Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login User Email :" + username + " Status " + userStatus.ToString(), "");
                            if (userStatus)
                            {
                                Audit.Add(EventType.Exception, Severity.High, 0, "Silent login user status :" + userStatus.ToString(), "");
                                Audit.Add(EventType.Exception, Severity.High, 0, "Silent login user name :" + username.ToString(), "");
                                
                                Response.Redirect("HotelSearch?source=Flight", false);
                            }
                            else
                            {
                                throw new ArithmeticException("User Email Does not Exist!");
                            }
                        }
                        else
                        {
                            throw new ArithmeticException("AUTHTOKEN mismatch... GlobalStar Token: " + authToken + ", Cozmo Token: " + genToken);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Request.Form["AUTHTOKEN"].ToString()))
                        {
                            throw new ArithmeticException("AUTHTOKEN Missing");
                        }
                        else if (string.IsNullOrEmpty(Request.Form["USERNAME"].ToString()))
                        {
                            throw new ArithmeticException("USERNAME missing");
                        }
                        else
                        {
                            throw new ArithmeticException("TIMESTAMP missing");
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (ArithmeticException arEx)
            {
                Utility.WriteLog(arEx.ToString(), this.Title);
                lblError.Text = arEx.Message;
                lblError.Visible = true;
                Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login Failed to :" + arEx.ToString(), "");
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex, this.Title);
                lblError.Text = "You are not authorized to view this page";
                lblError.Visible = true;
                Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login Failed to :" + ex.ToString(), "");
            }
        }
        public string encryptAuthToken(string authtoken, string secretkey)
        {
            string base64Result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(secretkey))
                {
                    HMACSHA256 hmac = new HMACSHA256();
                    hmac.Key = Encoding.ASCII.GetBytes(secretkey);
                    byte[] hashValue = hmac.ComputeHash(Encoding.ASCII.GetBytes(authtoken));
                    base64Result = Convert.ToBase64String(hashValue);
                }
                else
                {
                    throw new Exception("Secret Key is not valid");
                }                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login Failed to :" + ex.ToString(), "");
                throw ex;
            }
            return base64Result;
        }
        public string GetConfigData(string appkey)
        {
            string result = string.Empty;
            try
            {
                AgentAppConfig agentAppConfig = new AgentAppConfig();
                agentAppConfig.AgentID = 1;
                List<AgentAppConfig> agentAppConfigs = agentAppConfig.GetConfigData();
                result = agentAppConfigs.Where(x => x.AppKey.ToUpper() == appkey.ToUpper()).FirstOrDefault().Description;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Silent Login Failed to :" + ex.ToString(), "");
            }
            return result;
        }
    }
}
