﻿using System;

public partial class DownloadeDoc : System.Web.UI.Page
{
    bool valid = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string path = Request.QueryString["path"];
            string docName = Request.QueryString["docName"].Split('.')[0];
            string type = Request.QueryString["docName"].Split('.')[1];
            string fileName = docName + path.Substring(path.LastIndexOf('.'));
            Response.ContentType = type;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.TransmitFile(path);
        }
        catch(Exception ex)
        {
        }
        finally
        {
            if (valid)
            {
                Response.End();
            }
            else 
            {
                Response.Write("<h1> File Not Found.</h1>");
                Response.End();
            }
        }


    }
}
