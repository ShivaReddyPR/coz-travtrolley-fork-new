﻿<%@ Page Title="View Booking For Package" MasterPageFile="~/TransactionBE.master" Language="C#" AutoEventWireup="true" Inherits="ViewBookingforPackage" Codebehind="ViewBookingforPackage.aspx.cs" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">
    <asp:HiddenField runat="server" ID="hdnUpdateValue" />

    <script type="text/javascript">

        function clearAll() {
            location.reload();

        }

        function showHistoryDiv() {
            if (document.getElementById('HistoryDiv').style.display == "none") {
                document.getElementById('HistoryDiv').style.display = "block";
            }
            else {
                document.getElementById('HistoryDiv').style.display = "none";
            }
        }


        function validate() {
            var ctrls = [];
            document.getElementById('errMess').style.display = "none";
            var valid = false;
            var inputElems = document.getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < inputElems.length; i++) {
                if (inputElems[i].type === "checkbox" && inputElems[i].checked === true) {
                    count++;
                    ctrls.push(inputElems[i].id.replace("chk", ''));
                }
            }
            if (count > 0) {
                
                for (var i = 0; i < ctrls.length; i++) {
                    var drpId = "drp" + ctrls[i];
                    var refNoId = "txtRef" + ctrls[i];
                    var remarks = "txtRemarks" + ctrls[i];

                    var remarksValue = document.getElementById("txtRemarks" + ctrls[i]).value;

                    if (remarksValue.length == 0 || remarksValue == '') {
                        alert('Please enter remarks');
                        valid = false;
                        break;
                    }
                    else {
                        valid = true;
                        var rowInfo = "";
                        rowInfo += ctrls[i];
                        rowInfo += "^";
                        rowInfo += document.getElementById(drpId).value;
                        rowInfo += "~";

                        rowInfo += document.getElementById(refNoId).value;
                        rowInfo += "~";

                        rowInfo += document.getElementById(remarks).value;

                        if (document.getElementById('<%= hdnUpdateValue.ClientID%>').value.length == 0) {
                            document.getElementById('<%= hdnUpdateValue.ClientID%>').value = rowInfo;
                        }
                        else {
                            document.getElementById('<%= hdnUpdateValue.ClientID%>').value = document.getElementById('<%= hdnUpdateValue.ClientID%>').value + "|" + rowInfo;
                        }
                    }

                }


            }
            else {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select any record";

            }
            return valid;
        }

        function enableRowControls(ctrlId) {
            var drpId = "drp" + ctrlId;
            var refNoId = "txtRef" + ctrlId;
            var remarks = "txtRemarks" + ctrlId;
            if (document.getElementById(drpId).disabled) {
                document.getElementById(drpId).disabled = false;
            }
            else {
                document.getElementById(drpId).disabled = true;
            }
            if (document.getElementById(refNoId).disabled) {
                document.getElementById(refNoId).disabled = false;
            }
            else {
                document.getElementById(refNoId).disabled = true;
            }
            if (document.getElementById(remarks).disabled) {
                document.getElementById(remarks).disabled = false;
            }
            else {
                document.getElementById(remarks).disabled = true;
            }
        }

    </script>


    <div class="body_container">

        <div class="col-md-12" id="errMess" style="color: Red; font-weight: bold; text-align: center">
        </div>

        <div>
            <a class="pull-left" href="PackageQueue.aspx">&lt;&lt;  Back to Booking Queue</a>
            <div class="clearfix"></div>
        </div>


        <div class="martop_10">

            <div class="col-md-8 padleft0 pad_xs0">

               <%-- <div class="bgdark_brown">
                    <label><strong>Supplier Details </strong></label>
                    <a class="pull-right" href="deal-voucher.html">&lt;&lt; View Voucher</a>
                </div>--%>

                <div class="table-responsive">

                    <table class="table table-bordered bg_white">

                        <tbody>
                            <tr>
                                <th>Select</th>
                                <th>Supplier  </th>
                                <th>Product  </th>
                                <th width="150">Status  </th>
                                <th width="200">Reference No.  </th>
                                <th width="200">Remarks.  </th>
                               

                            </tr>

                            <%if (dtSupplierDetails != null && dtSupplierDetails.Rows.Count > 0) %>
                            <%{
                                  foreach (System.Data.DataRow dr in dtSupplierDetails.Rows) %>
                            <%{ %>


                            <%if (dr["SuppTranxSuppName"] != DBNull.Value && dr["SuppTranxProductType"] != DBNull.Value) %>
                            <%{ %>
                            <tr>
                                 
                                <td>
                                    <%if (Convert.ToString(dr["SuppTranxConfirmStatus"]).ToUpper() == "P" || Convert.ToString(dr["SuppTranxConfirmStatus"]).ToUpper() == "W" || Convert.ToString(dr["SuppTranxConfirmStatus"]).ToUpper() == "C"  )%>
                                        <%{ %>
                                    <input onchange="enableRowControls('<%=dr["SuppTranxId"]%>')" type="checkbox" id="chk<%=dr["SuppTranxId"] %>" />
                                    <%} %>
                                </td>

                                <td>
                                    <% = Convert.ToString(dr["SuppTranxSuppName"]) %>
                                </td>
                                <td>
                                    <% = Convert.ToString(dr["SuppTranxProductType"]) %>
                                </td>
                                <td>
                                    <select disabled class="form-control" id="drp<%=dr["SuppTranxId"] %>" >
                                        <%if (Convert.ToString(dr["SuppTranxConfirmStatus"]).ToUpper() == "P")%>
                                        <%{ %>

                                        <option value="W">Waiting for Reconf.</option>
                                        <option value="C">Confirmed</option>
                                        <option value="X">Cancelled</option>
                                        <%} %>

                                        <%else if (Convert.ToString(dr["SuppTranxConfirmStatus"]).ToUpper() == "W")%>
                                        <%{ %>
                                        <option value="C">Confirmed.</option>
                                        <option value="X">Cancelled</option>

                                        <%} %>


                                        <%else if (Convert.ToString(dr["SuppTranxConfirmStatus"]).ToUpper() == "C")%>
                                        <%{ %>

                                        <option value="X">Cancelled</option>
                                        <%} %>
                                    </select>
                                </td>
                                <td>
                                   
                                    <input disabled class="form-control" type="text" id="txtRef<%=dr["SuppTranxId"] %>" />
                                    
                                </td>
                                <td>
                                    
                                    <input disabled class="form-control" type="text" id="txtRemarks<%=dr["SuppTranxId"] %>" />
                                    
                                </td>
                                

                            </tr>


                            <%} %>




                            <%} %>
                            <%} %>
                        </tbody>
                    </table>

                   
                    
                     
                       
                               <asp:Button OnClick="btnUpdate_Click" OnClientClick="return validate();" ID="btnUpdate" runat="server"
                                                    Text="Update" CssClass="button" />
                       


                  
                </div>





            </div>
            <div class="col-md-4 padleft0 pad_xs0">

                <div class="bgdark_brown">
                    <label><strong>Summary </strong></label>
                </div>

                <div>

                    <table class="table table-bordered bg_white">

                        <tbody>

                            <%if (dtSupplierDetails != null && dtSupplierDetails.Rows.Count > 0) %>
                            <%{ %>

                            <%for (int i = 0; i < dtSupplierDetails.Rows.Count; i++)%>
                            <%{ %>

                            <%if (i == 0) %>
                            <%{ %>

                            <tr>
                                <td>Booking Date : </td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["Booking_Date"]) %>  </td>


                            </tr>


                            <tr>
                                <td>Reference No.:   </td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["Reference_No"]) %>     </td>


                            </tr>


                            <tr>
                                <td>Package Name :  </td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["Package_Name"]) %>     </td>


                            </tr>


                            <tr>
                                <td>Status:   </td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["BookingStatus"]) %>   </td>


                            </tr>
                            <%if (dtSupplierDetails.Rows[i]["Promo_Code"] != DBNull.Value && !string.IsNullOrEmpty(dtSupplierDetails.Rows[i]["Promo_Code"].ToString()))
                              {%>
                            <tr>
                                <td>Applied Promo Code :</td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["Promo_Code"]) %></td>


                            </tr>
                            <%} %>
                            <tr>
                                <td><b>Total :</b>   </td>
                                <%string[] amount = (dtSupplierDetails.Rows[i]["Total_Price"]).ToString().Split(' ');
                                  decimal price = 0;
                                  if (amount.Length > 1)
                                  {
                                      price = Math.Ceiling(Convert.ToDecimal(amount[1]));
                                  }
                                  price = Math.Ceiling(price) - Math.Ceiling(Convert.ToDecimal(dtSupplierDetails.Rows[i]["Promo_Discount_Value"]));
                                      %>
                                <td><span class="text-danger"><%=amount[0] %> <%= price %></span>    </td>


                            </tr>

                            <%} %>
                            <%} %>

                            <%} %>
                        </tbody>
                    </table>






                </div>





            </div>

            <div class="clearfix"></div>
        </div>

       
            <div class="col-md-7 padleft0 pad_xs0">

                <div class="bgdark_brown">
                    <label><strong>Passenger Details</strong> </label>
                </div>
                <div>
                    <table class="table table-bordered bg_white">

                        <tbody>
                            <tr>
                                <th>Passenger Name</th>
                                <th>Type </th>
                                <th>DOB</th>
                                <th>Phone</th>
                                <th>Email</th>



                            </tr>
                            <%if (dtSupplierDetails != null && dtSupplierDetails.Rows.Count > 0) %>
                            <%{ %>

                            <%for (int i = 0; i < dtSupplierDetails.Rows.Count; i++)%>
                            <%{ %>

                            <%if (i == 0) %>
                            <%{ %>

                            <tr>
                                <td>

                                    <%= Convert.ToString(dtSupplierDetails.Rows[i]["Passenger_Name"]) %>
                                </td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["Passenger_Type"]) %></td>
                                <td><%= Convert.ToString(dtSupplierDetails.Rows[i]["Passenger_DOB"]) %>  </td>
                                <td><%=Convert.ToString(dtSupplierDetails.Rows[i]["Phone_no"]) %></td>
                                 <td><%=Convert.ToString(dtSupplierDetails.Rows[i]["Email"]) %></td>


                            </tr>

                            <%} %>
                            <%} %>

                            <%} %>
                        </tbody>
                    </table>

                </div>
            </div>
      

        <%if(dtSupplierDetailsHistory != null && dtSupplierDetailsHistory.Rows.Count >0) %>
        <%{ %>

         <a href="#" onclick="showHistoryDiv();" >&lt;&lt; View History</a>
        
          <div class="col-md-5" id="HistoryDiv">
              <strong>History</strong>
                    <table class="table table-bordered bg_white">

                        <tbody>
                            <tr>
                                <th>supplier Name</th>
                                <th>Product Type  </th>
                                <th width="150">Status  </th>                            
                                <th width="200">Remarks.  </th>
                               <th width="200">Reference No.  </th>
                                <th width="200">Modified By.  </th>


                            </tr>

                            <%if (dtSupplierDetailsHistory != null && dtSupplierDetailsHistory.Rows.Count > 0) %>
                            <%{
                                  foreach (System.Data.DataRow dr in dtSupplierDetailsHistory.Rows) %>
                            <%{ %>
                            
                            <tr>
                                <td>
                                    
                                    <% = Convert.ToString(dr["suppTranxSuppName"]) %>
                                </td>

                                <td>
                                    <% = Convert.ToString(dr["suppTranxProductType"]) %>
                                </td>
                                <td>
                                    <% = Convert.ToString(dr["suppTranxConfirmStatus"]) %>
                                </td>
                                <td>
                                  <% = Convert.ToString(dr["suppTranxRemarks"]) %>
                                </td>
                                <td>
                                   <% = Convert.ToString(dr["suppTranxRefNo"]) %>
                                </td>
                                <td>
                                    <% = Convert.ToString(dr["createdBy"]) %>
                                </td>
                                

                            </tr>


                          




                            <%} %>
                            <%} %>
                        </tbody>
                    </table>

                   
                    

                       
                            
                       


                  
                </div>


         <%} %>

    </div>


</asp:Content>
