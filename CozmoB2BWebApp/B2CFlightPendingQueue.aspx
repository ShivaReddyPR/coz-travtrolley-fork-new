﻿<%@ Page Title="B2C Flight Pending Booking Details" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="B2CFlightPendingQueue.aspx.cs" Inherits="CozmoB2BWebApp.B2CFlightPendingQueue" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <iframe id="IShimFrame" style="position: absolute; display: none;"></iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="body_container" style="margin-top: 10px; min-height: 100%;">
                <div class="row">
                    <div class="col-md-3">
                        <span>From Date</span>
                        <div class="form-group">
                            <div class="input-group date fromDate">
                                <asp:TextBox ID="txtFromDate" runat="server" placeholder="DD/MM/YYYY"  CssClass="inputEnabled form-control"></asp:TextBox>
                                <asp:Label runat="server" class="input-group-addon" onclick="showCal1()" >
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                   </asp:Label>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-3">
                        <span>To Date</span>
                        <div class="form-group">
                            <div class="input-group date toDate">
                                <asp:TextBox ID="txtToDate" runat="server"  placeholder="DD/MM/YYYY" CssClass="form-control" ></asp:TextBox>
                                <asp:Label runat="server" class="input-group-addon" onclick="showCal2()">
                                        <img id="dateLink2" src="images/call-cozmo.png" alt="Pick Date" /> 
                                   </asp:Label>
                            </div>
                        </div>                         
                    </div>
                    <div class="col-md-3">
                        <span>Payment Source</span>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlpaymentSource" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span>Booking Status</span>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlbookingStatus" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span>Select Agent</span>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlAgents" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span>Payment Id</span>
                        <div class="form-group">
                            <asp:TextBox ID="txtPaymentId" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span>Order Id</span>
                        <div class="form-group">
                            <asp:TextBox ID="txtOrderId" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span>PNR</span>
                        <div class="form-group">
                            <asp:TextBox ID="txtpnr" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="row" style="float: right; margin-right: 10px;">
                    <asp:Button ID="btnSearch" runat="server" class="btn btn-info btn-primary" Text="Search" OnClick="btnSearch_Click" />

                </div>
                <br />
            </div>
            <div class="container" style="text-align: center; overflow: auto; border-radius: 5px;">
                <asp:GridView ID="GridPendingQueue" ShowHeaderWhenEmpty="true" runat="server" CellPadding="4" CssClass="table"
                    EmptyDataText="Fligt pending Queue details not found." AllowPaging="True" Style="margin-top: 10px;"
                    AutoGenerateColumns="false" PageSize="10" OnPageIndexChanging="GridPendingQueue_PageIndexChanging">
                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                    <HeaderStyle CssClass="ns-h3" BackColor="red" Font-Bold="True" ForeColor="White" />
                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Left" />
                    <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
                    <Columns>
                        <asp:TemplateField HeaderText="SL.No">
                            <ItemTemplate>&nbsp;&nbsp;<%#Container.DataItemIndex+1 %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Passerger Details">
                            <ItemTemplate>
                                <asp:Button ID="btnViewPax" runat="server" CssClass="btn btn-info btn-primary" OnClientClick="showmodal(this.id);" Text="View" />
                                <asp:Label ID="lblPassengerInfo" Style="display: none;" runat="server" Text='<%# Eval("passengerInfo")%>' />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Source">
                            <ItemTemplate>
                                <asp:Label ID="lblpaymentSource" Style="display: none" runat="server" Text='<%# Eval("paymentGatewaySourceId")%>' />
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("PaymentGateway")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Id">
                            <ItemTemplate>
                                <asp:Label ID="lblpaymentId" CssClass="label grdof" runat="server" Text='<%# Eval("paymentId")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Order Id">
                            <ItemTemplate>
                                <asp:Label ID="lblorderId" CssClass="label grdof" runat="server" Text='<%# Eval("orderId")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PNR">
                            <ItemTemplate>
                                <asp:Label ID="lblPnr" CssClass="label grdof" runat="server" Text='<%# Eval("outPNR")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Booking Date">
                            <ItemTemplate>
                                <asp:Label ID="lnblCreatedOn" CssClass="label grdof" runat="server" Style="width: 100%;" Text='<%# Eval("createdOn","{0:yyyy-MM-dd HH:mm}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Departure date">
                            <ItemTemplate>
                                <asp:Label ID="lbldeptDate" runat="server" CssClass="label grdof" Text='<%# Eval("depDate","{0:yyyy-MM-dd HH:mm}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Return date">
                            <ItemTemplate>
                                <asp:Label ID="lblReturnDate" CssClass="label grdof" runat="server" Text='<%# Eval("returnDate","{0:yyyy-MM-dd HH:mm}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblPremoumAmount" runat="server" CssClass="label grdof" Text='<%# Eval("paymentAmount")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Booking Status">
                            <ItemTemplate>
                                <asp:Label ID="lblBookingStatus" CssClass="label grdof" runat="server" Text='<%# Eval("Booking_status")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Status">
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentStatus" CssClass="label grdof" runat="server" Text='<%# Eval("payment_status")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Onward AirLine">
                            <ItemTemplate>
                                <asp:Label ID="lblAirlinein" runat="server" CssClass="label grdof" Text='<%# Eval("airlineCodeIn")+"-"+Eval("flightNumberIn")  %>   ' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Return AirLIne">
                            <ItemTemplate>
                                <asp:Label ID="lblAirlineout" runat="server" CssClass="label grdof" Text='<%# Eval("airlineCodeOut")+"-"+Eval("flightNumberOut")  %>   ' />
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Sector">
                            <ItemTemplate>
                                <asp:Label ID="lblSector" runat="server" CssClass="label grdof" Text='<%# Eval("sector") %>   ' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" CssClass="label grdof" Text='<%# Eval("email")  %>   ' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone">
                            <ItemTemplate>
                                <asp:Label ID="lblPhone" runat="server" CssClass="label grdof" Text='<%# Eval("phone")  %>   ' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Agent">
                            <ItemTemplate>
                                <asp:Label ID="lblAgentName" runat="server" CssClass="label grdof" Text='<%# Eval("agent_name")%>' />
                                <asp:Label ID="lblDecimalValue" Style="display: none;" runat="server" Text='<%# Eval("agent_decimal")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IP Address">
                            <ItemTemplate>
                                <asp:Label ID="lblipAdd" runat="server" CssClass="label grdof" Text='<%# Eval("ipAddress")  %>   ' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" CssClass="label grdof" runat="server" Text='<%# Eval("Remark")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalpassengerInfo" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="color: white">
                    <asp:Label ID="lbltitle" Style="color: white; text-align: center; margin-top: 15px" runat="server" Text="Passenger Details"></asp:Label>
                    <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>

                <div class="modal-body" id="paxbody">
                    <div style="float: right; margin-right: 10px;">
                        <span style="font-weight: bold;">Lead Passenger Email  :</span>&nbsp;&nbsp;<span id="paxEmail"></span><br />
                        <span style="font-weight: bold;">Lead Passenger Phone :</span>&nbsp;&nbsp;<span id="paxPhone"></span>

                    </div>
                    <h1 class="ns-h3" style="border-radius: 5px; margin-top: 50px;">Passenger Details</h1>

                    <table id="tablepassenger" class="table table-hover">
                    </table>

                    <h1 class="ns-h3" style="border-radius: 5px">Fare Details</h1>
                    <table class="table table-hover" style="width: 300px; float: right; margin-top: 20px;">
                        <tr>
                            <td>BaseFare</td>
                            <td><span id="spanBaseFare"></span></td>
                        </tr>
                        <tr>
                            <td>Tax</td>
                            <td><span id="spanTax"></span></td>
                        </tr>
                        <tr id="trBaggage">
                            <td>Baggage</td>
                            <td><span id="spanBaggage"></span></td>
                        </tr>
                        <tr id="trDiscount">
                            <td>Discount</td>
                            <td><span id="spanDiscount"></span></td>
                        </tr>
                        <tr id="trVat">
                            <td>VAT</td>
                            <td><span id="spanVat"></span></td>
                        </tr>
                        <tr id="trTotalAmount">
                            <td>Total Amount</td>
                            <td><span id="spanTotalAmount"></span></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnclose" runat="server" data-dismiss="modal" CssClass="btn btn-info btn-primary" Text="Close" Style="border-radius: 5px" />
                </div>
            </div>
        </div>
    </div>

    <script>
        var cal1;
        var cal2;
        function init() {
            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select From date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select To date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }
        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
             //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear(); cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }
            var date2 = cal2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        function CheckValidDate(Day, Mn, Yr) {
            var DateVal = Mn + "/" + Day + "/" + Yr;
            var dt = new Date(DateVal);

            if (dt.getDate() != Day) {
                return false;
            }
            else if (dt.getMonth() != Mn - 1) {
                //this is for the purpose JavaScript starts the month from 0
                return false;
            }
            else if (dt.getFullYear() != Yr) {
                return false;
            }
            return (true);
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript">       
        function showmodal(id) {
            id = id.replace("btnViewPax", "lblPassengerInfo");
            var paxinfo = $('#' + id).html();
            var i = 1;
            var name, dob, paxtype, passportNo, gender, ExpiryDate, BaggageCode, BaggageType, currencyType;
            var basefare = 0, tax = 0, baggage = 0, discount = 0, total = 0, Markup = 0, B2cMarkup = 0, OutputVAT = 0;
            $("#tablepassenger tr").remove();
            var th = "<tr><th>SL.No</th> <th>Name</th> <th>Passport No</th><th>ExpiryDate</th><th>Date Of Birth</th>   <th>Gender</th>  <th>Pax Type</th><th>Baggage Code</th><th>Baggage Type</th> </tr>";
            $('#tablepassenger').append(th);
            $(paxinfo).find('WSPassenger').each(function () {
                name = "<td>" + $(this).find('Title').text() + " " + $(this).find('FirstName').text() + " " + $(this).find('LastName').text() + "</td>";
                var mydate = new Date($(this).find('DateOfBirth').text());
                dob = "<td>" + formatDate(mydate) + "</td>";
                paxtype = "<td>" + $(this).find('Type').text() + "</td>";
                passportNo = "<td>" + $(this).find('PassportNumber').text() + "</td>";
                gender = "<td>" + $(this).find('Gender').text() + "</td>";
                var passportexpiry = new Date($(this).find('PassportExpiry').text());
                ExpiryDate = "<td>" + formatDate(passportexpiry) + "</td>";
                BaggageCode = "<td>" + $(this).find('BaggageCode').text() + "</td>";
                BaggageType = "<td>" + $(this).find('BaggageType').text() + "</td>";


                $('#tablepassenger').append("<tr><td>" + i + "</td>" + name + passportNo + ExpiryDate + dob + gender + paxtype + BaggageCode + BaggageType + "</tr>");
                i++;
                var email = $(this).find('Email').text();
                var phone = $(this).find('Phone').text();
                if (email != '' && phone != '') {
                    $('#paxEmail').text(email);
                    $('#paxPhone').text(phone);
                    currencyType = $(this).find('Currency').text();
                }

                basefare += parseFloat($(this).find('BaseFare').text());
                tax += parseFloat($(this).find('Tax').text());
                Markup += parseFloat($(this).find('Markup').text());
                B2cMarkup += parseFloat($(this).find('B2CMarkup').text());
                baggage += parseFloat($(this).find('BaggageCharge').text());
                discount += parseFloat($(this).find('Discount').text());
                if ($(this).find('OutputVAT:first').text().length > 0) {
                    OutputVAT += parseFloat($(this).find('OutputVAT:first').text());
                }
            });

            id = id.replace("lblPassengerInfo", "lblDecimalValue");
            var agentdecimalValue = $('#' + id).text();
            total = basefare + tax + Markup + B2cMarkup + baggage + discount;
            $('#spanBaseFare').text(currencyType + ' ' + Math.round(basefare, agentdecimalValue).toFixed(agentdecimalValue));

            $('#spanTax').text(currencyType + ' ' + Math.round((tax + Markup + B2cMarkup), agentdecimalValue).toFixed(agentdecimalValue));

            if (OutputVAT > 0) {
                $('#trVat').show();
                $('#spanVat').text(Math.round(currencyType + ' ' + OutputVAT, agentdecimalValue).toFixed(agentdecimalValue));
            }
            else {
                $('#trVat').hide();
            }
            if (baggage > 0) {
                $('#trBaggage').show();
                $('#spanBaggage').text(currencyType + ' ' + Math.round(baggage, agentdecimalValue).toFixed(agentdecimalValue));
            }
            else {
                $('#trBaggage').hide();
            }
            if (discount > 0) {
                $('#trDiscount').show();
                $('#spanDiscount').text(currencyType + ' ' + Math.round(discount, agentdecimalValue).toFixed(agentdecimalValue));
            }
            else {
                $('#trDiscount').hide();
            }
            $('#spanTotalAmount').text(currencyType + ' ' + Math.round(total, agentdecimalValue).toFixed(agentdecimalValue));
            var rowCount = $('#tablepassenger tr').length;
            if (rowCount > 1) {
                $('#modalpassengerInfo').modal('show');
            }
        }
        function formatDate(date) {
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = date.getDate();
            if (day < 10)
                day = '0' + day;
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            return year + '/' + monthNames[monthIndex] + '/' + day;
        }   
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }
    </script>
    <style type="text/css">
        .pagination-ys {
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
            background-color: #6d6b6c;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    font-weight: bold;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }

        #GridPendingQueue thead tr th {
            text-align: center !important;
            width: auto;
        }

        #GridPendingQueue tbody tr td {
            width: 100%;
        }

        #tablepassenger tr th {
            font-weight: bold;
        }

        .modal-content {
            width: 150%;
            overflow-y: auto;
        }

        .modal-body {
            height: auto;
            overflow: auto;
        }
    </style>
     
</asp:Content>

