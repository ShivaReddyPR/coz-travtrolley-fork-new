﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;

namespace CozmoB2BWebApp
{
    public partial class SectorMaster : CT.Core.ParentPage // System.Web.UI.Page
    {
        public string TextBoxValues { get; set; }
        private string SECTOR_MAP_SESSION = "_SectorMap";
        private string SECTOR_SEARCH_SESSION = "_SectorSearchList";
        private string SECTOR_IMPORT_SESSION = "_SectorImportList";
        private SectorList CurrentObject
        {
            get
            {
                return (SectorList)Session[SECTOR_MAP_SESSION];
            }
            set
            {
                if (value == null)
                {
                    Session.Remove(SECTOR_MAP_SESSION);
                }
                else
                {
                    Session[SECTOR_MAP_SESSION] = value;
                }
            }

        }
        private DataTable SearchList
        {
            get
            {
                return (DataTable)Session[SECTOR_SEARCH_SESSION];
            }
            set
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["SMID"] };
                Session[SECTOR_SEARCH_SESSION] = value;
            }
        }

        
        private DataTable ImportList
        {
            get
            {
                return (DataTable)Session[SECTOR_IMPORT_SESSION];
            }
            set
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["Sepplier"] };
                Session[SECTOR_IMPORT_SESSION] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializePageControls();
            }
        }
        private void InitializePageControls()
        {
            try
            {
                Clear();
                BindSuppliers();
            }
            catch { throw; }
        }
        private void Clear()
        {
            try
            {
                txtOrigin.Text = "";
                btnSave.Text = "Save";
                ddlSuppliers.SelectedIndex = 0;
                ddlSuppliers.DataBind();
                CurrentObject = null;
                hdnNewSectors.Value = "";
                hdnNewOrigin.Value = "";
            }
            catch
            {
                throw;
            }
        }
        private void BindSuppliers()
        {
            try
            {
                ddlSuppliers.Items.Clear();
                ddlSuppliers.DataSource = SectorList.GetSuppliers();
                ddlSuppliers.DataValueField = "Supplier";
                ddlSuppliers.DataTextField = "Supplier";
                ddlSuppliers.DataBind();
                ddlSuppliers.Items.Insert(0, new ListItem("--select Supplier--", "-1"));
                ddlSuppliers.SelectedIndex = 0;
            }
            catch { throw; }
        }
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                FilterSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long SMID = Utility.ToLong(gvSearch.SelectedValue);
                Edit(SMID);
                this.Master.HideSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void gvSearch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowState != DataControlRowState.Edit) // check for RowState
            {
                if (e.Row.RowType == DataControlRowType.DataRow) //check for RowType
                {
                    string sector = e.Row.Cells[2].Text + "-" + e.Row.Cells[1].Text; // Get the id to be deleted
                    LinkButton lb = (LinkButton)e.Row.Cells[0].FindControl("lnkDelete"); //access the LinkButton from the TemplateField using FindControl method
                    if (lb != null)
                    {
                        lb.Attributes.Add("onclick", "return ConfirmOnDelete('" + sector + "');"); //attach the JavaScript function
                    }
                }
            }
        }
        
        protected void FilterSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string[,] textboxesNColumns ={{ "GVtxtOrigin", "Origin" },{"GVtxtSupplier", "Supplier" },{ "GVtxtCreatedBy", "CreatedBy" }};
                CommonGrid g = new CommonGrid();
                g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        private void bindSearch()
        {
            try
            {
                string supplier = ddlSuppliers.SelectedItem.Value;
                string origin = txtOrigin.Text.Trim();
                DataTable dt = SectorList.GetOrigins(supplier,origin);
                SearchList = dt;
                if (dt != null && dt.Rows.Count == 0 && string.IsNullOrEmpty(origin)) // If all sectors deleted for any source then rebind the suppliers
                    BindSuppliers();
                Session[SECTOR_SEARCH_SESSION] = dt;
                gvSearch.EditIndex = -1;
                gvSearch.DataSource = dt;
                gvSearch.DataBind();

            }
            catch { throw; }
        }

        private void SaveOrUpdate()
        {
            try
            {
                hdnNewSectors.Value = "";
                hdnNewOrigin.Value = "";
                SectorList sector;
                if (CurrentObject == null)
                {
                    sector = new SectorList();
                    sector.CreatedBy = Settings.LoginInfo.AgentId;
                }
                else
                {
                    sector = CurrentObject;
                    sector.ModifiedBy = Settings.LoginInfo.AgentId;
                    sector.ModifiedOn = DateTime.Now;
                }
                sector.Supplier = ddlSuppliers.SelectedItem.Value;
                sector.Origin = txtOrigin.Text.ToUpper().Trim();
                sector.Status = 1;

                List<string> sectorList = new List<string>();
                try
                {
                    string[] textboxValues = Request.Form.GetValues("txtSectorDynamic");
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    this.TextBoxValues = serializer.Serialize(textboxValues);
                    foreach (string textboxValue in textboxValues)
                    {
                        sectorList.Add(textboxValue.ToUpper());
                    }
                }
                catch { }
                sectorList.OrderBy(x => x);
                sector.Sectors = string.Join("-", sectorList);
                
                sector.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Sectors for ", sector.Supplier + "-" + sector.Origin, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
                Clear();
                CurrentObject = null;
            }
            catch
            {
                throw;
            }
        }
        private void Edit(long sectorMasterID)
        {
            try
            {
                SectorList sector = new SectorList(sectorMasterID);
                CurrentObject = sector;
                ddlSuppliers.SelectedValue = Utility.ToString(sector.Supplier);
                txtOrigin.Text= Utility.ToString(sector.Origin);
                string[] sectors = sector.Sectors.Split('-');
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                this.TextBoxValues = serializer.Serialize(sectors);
                ScriptManager.RegisterClientScriptBlock(this.Page , this.GetType(), "RecreateDynamicTextboxes", "RecreateDynamicTextboxes(" + this.TextBoxValues + ")", true);
                btnSave.Text = "Update";
            }
            catch
            {
                throw;
            }
        }
        private void Delete(long sectorMasterID)
        {
            try
            {
                SectorList sector = new SectorList(sectorMasterID);
                CurrentObject = sector;
                sector.Status = 0;
                sector.ModifiedBy = Settings.LoginInfo.AgentId;
                sector.ModifiedOn = DateTime.Now;
                sector.Delete();
                bindSearch();
                
            }
            catch
            {
                throw;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveOrUpdate();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                Utility.Alert(this.Page, ex.Message);
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                pnlImport.Visible = false;
                gvSearch.Visible = true;
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }


        #region Import Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.HideSearch();
                gvSearch.Visible = false;
                btnSearch.Visible = false;
                btnClear.Visible = false;
                btnSave.Visible = false;
                lblOrigin.Visible = false;
                txtOrigin.Visible = false;
                ddlSuppliers.Visible = false;
                lblSupplier.Visible = false;
                btnUpdate.Visible = true;
                divSectors.Visible = false;
                btnAdd.Visible = false;
                BindImport();
                pnlImport.Visible = true;
                lblSuccessMsg.Visible = false;
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindImport()
        {
            try
            {
                string supplier = ddlSuppliers.SelectedItem.Value;

                XmlDocument xml = new XmlDocument();
                string SectorList = System.IO.Path.Combine(System.Configuration.ConfigurationManager.AppSettings["configFiles"], "SectorList.config.xml");
                xml.Load(SectorList);
                XmlElement root = xml.DocumentElement;
                List<SectorList> lstSectors = new List<SectorList>();
                               
                foreach (XmlNode rule in xml)
                {
                    foreach (XmlNode origin in rule)
                    {
                       // if (n.Name.Contains(supplier))
                        {
                            foreach (XmlNode sector in origin)
                            {
                                SectorList objSec = new SectorList();
                                objSec.Supplier = origin.Name;
                                objSec.Origin = sector.Name;
                                objSec.Sectors = sector.InnerText;
                                objSec.CreatedBy = Settings.LoginInfo.AgentId;
                                objSec.Status = 1;
                                lstSectors.Add(objSec);
                            }
                        }
                    }
                }

                DataTable dt = CreateDataTable(lstSectors);
                ImportList = dt;

                gvImport.DataSource = lstSectors;
                gvImport.DataBind();

                Session["Import"] = lstSectors;
                
            }
            catch { throw; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvImport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvImport.PageIndex = e.NewPageIndex;
                gvImport.EditIndex = -1;
               
                gvImport.DataSource = Session["Import"] as List<SectorList>;
                gvImport.DataBind();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            try
            {
                List<SectorList> lstSector = Session["Import"] as List<SectorList>;
                SectorList objSL = new SectorList();
                objSL.Save(lstSector);
                //HttpContext.Current.Application["SectorList"] = SectorList.GetSectorList();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = "Saved Successfully";//Formatter.ToMessage("Sectors for ", sector.Supplier + "-" + sector.Origin, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
                Session["Import"] = null;
                ImportList = null;
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvImport_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvImport.EditIndex = e.NewEditIndex;
                gvImport.DataSource = Session["Import"] as List<SectorList>;
                gvImport.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvImport_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            try
            {
                gvImport.EditIndex = -1;

                Label lblSupplier = gvImport.Rows[e.RowIndex].FindControl("lblSupplier") as Label;
                Label lblOrigin = gvImport.Rows[e.RowIndex].FindControl("lblOrigin") as Label;

                List<SectorList> lstSector = Session["Import"] as List<SectorList>;
                SectorList list = lstSector.Where(x => x.Origin == lblOrigin.Text && x.Supplier == lblSupplier.Text).ToList().FirstOrDefault();
                TextBox txtsector = gvImport.Rows[e.RowIndex].FindControl("txtSectors") as TextBox;
                list.Sectors = txtsector.Text;

                gvImport.DataSource = lstSector;
                gvImport.DataBind();
                Session["Import"] = lstSector;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvImport_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvImport.EditIndex = -1;
                gvImport.DataSource = Session["Import"] as List<SectorList>;
                gvImport.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// for filter purpose
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private DataTable CreateDataTable(List<SectorList> item)
        {
            Type type = typeof(SectorList);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (SectorList entity in item)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        /// <summary>
        /// for supplier filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                List<SectorList> lstSector = Session["Import"] as List<SectorList>;

                string[,] textboxesNColumns = { { "GVtxtSupplier", "Supplier" } };
                CommonGrid g = new CommonGrid();
                g.FilterGridView(gvImport, ImportList.Copy(), textboxesNColumns);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }


        #endregion

        protected void btnBack_Click(object sender, EventArgs e)
        {
            gvSearch.Visible = true;
            btnSearch.Visible = true;
            btnClear.Visible = true;
            btnSave.Visible = true;
            lblOrigin.Visible = true;
            txtOrigin.Visible = true;
            ddlSuppliers.Visible = true;
            lblSupplier.Visible = true;
            btnUpdate.Visible = false;
            divSectors.Visible = true;
            btnAdd.Visible = true;            
            pnlImport.Visible = false;
            lblSuccessMsg.Text = string.Empty;
        }

        protected void gvSearch_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Delete(Utility.ToLong(gvSearch.DataKeys[e.RowIndex].Value.ToString()));
        }

        [WebMethod] //Checking sectors list is exist in db or not .
        public static string CheckNewOrigin(string supplier, string origin)
        {
            string isNewOrigin = "";
            DataTable dt = SectorList.GetOrigins(supplier, origin);
            return isNewOrigin = (dt != null && dt.Rows.Count == 0) ? "YES" : "NO";
        }
    }
}
