﻿using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.Corporate;
using System.Data;
using CT.TicketReceipt.Web.UI.Controls;

public partial class CorporateTravelReasonMasterGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if(!IsPostBack)
            {
                InitialiseControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }
    #region Private Methods
   private void InitialiseControls()
    {
        BindAgent();
       // BindType();
        //BindActionStatus();
    }
    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "-1"));
            //ddlAgent.Items.Insert(-1, new ListItem("--Select Agent--", "-1"));
            //ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }
    //private void BindType()
    //{
    //    try
    //    {

    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
    //private void BindActionStatus()
    //{
    //    try
    //    {

    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
    private void Save()
    {
        try
        {
            CorporateTravelReason crpreason = new CorporateTravelReason();
            if (Utility.ToInteger(hdnrRequireId.Value) > 0)
            {
                crpreason.ReasonId = Utility.ToInteger(hdnrRequireId.Value);
            }
            else
            {
                crpreason.ReasonId = -1;
            }
            crpreason.AgentId = Utility.ToInteger(ddlAgent.SelectedValue);
            crpreason.Code = txtCode.Text;
            crpreason.Description = txtDescription.Text;
            crpreason.Type = ddlType.SelectedValue;
            crpreason.ActionStatus = ddlActionStatus.SelectedValue;
            crpreason.Status = "A";
            crpreason.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
            crpreason.Save();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Successfully", "", (Utility.ToInteger(hdnrRequireId.Value) == 0 ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
        }
        catch
        {
            throw;
        }
    }
    private void Clear()
    {
        try
        {
            hdnrRequireId.Value = string.Empty;
            ddlAgent.SelectedValue = "-1";
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ddlType.SelectedValue = "-1";
            ddlActionStatus.SelectedValue = "-1";
            btnSave.Text = "Save";
            btnCancel.Text = "Clear";

        }
        catch
        {
            throw;
        }
    }
    private void BindSearch()
    {
        try
        {
            DataTable dt = CorporateTravelReason.GetList(ListStatus.Long,RecordStatus.All);
            if (dt != null && dt.Rows.Count > 0)
            {
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
        }
        catch
        {
            throw;
        }
    }
    private void Edit(int Id)
    {
        try {
        CorporateTravelReason crpreason = new CorporateTravelReason(Id);
        hdnrRequireId.Value = Utility.ToString(crpreason.ReasonId);
        ddlAgent.SelectedValue = Utility.ToString(crpreason.AgentId);
        txtCode.Text = crpreason.Code;
        txtDescription.Text = crpreason.Description;
        ddlType.SelectedValue = crpreason.Type;
        ddlActionStatus.SelectedValue = crpreason.ActionStatus;
        btnSave.Text = "Update";
        btnCancel.Text = "Cancel";
        }
        catch
        {
            throw;
        }
    }
    #endregion
    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
            lblSuccessMsg.Text = String.Empty;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            BindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #endregion
    #region Grid Events
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            BindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Int32 requirementid = Utility.ToInteger(gvSearch.SelectedValue);
             Edit(requirementid);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #endregion
}