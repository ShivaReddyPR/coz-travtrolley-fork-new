﻿#region NameSpace Region
using System;
using System.Web.UI.WebControls;
using System.Data;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
#endregion

public partial class AirportMasterUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check 
            {
                if (!IsPostBack)
                {

                    BindAirportListGrid();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI Page_Load Event " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }


    #region Airport List  Grid Events
    protected void gridViewAM_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gridViewAM.EditIndex = e.NewEditIndex;
            BindAirportListGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI page : gridViewAM_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gridViewAM_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int Id = Convert.ToInt32(gridViewAM.DataKeys[e.RowIndex].Values["Id"]);
            TextBox txtAirportIndex = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditAirportIndex");
            TextBox txtAirportCode = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditAirportCode");
            TextBox txtAirportName = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditAirportName");
            TextBox txtAirportNameAR = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditAirportNameAR");
            TextBox txtCityCode = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditCityCode");
            TextBox txtCityName = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditCityName");
            TextBox txtCityNameAR = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditCityNameAR");
            TextBox txtCountryName = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditCountryName");
            TextBox txtCountryNameAR = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditCountryNameAR");
            TextBox txtCountryCode = (TextBox)gridViewAM.Rows[e.RowIndex].FindControl("txtEditCountryCode");

            DataTable dtAirportList = Airport.GetAirportList();
            if (dtAirportList != null && dtAirportList.Rows.Count > 0)
            {
                DataRow[] results = dtAirportList.Select("AirportCode = '" + txtAirportCode.Text + "' AND Id NOT IN ('" + Id + "')");
                if (results.Length > 0)
                {
                    Utility.StartupScript(this.Page, "alert('Duplicate  Item. Airport Code Already Exists !')", "");
                    throw new Exception("Airport Code already exists !");
                }
            }
            Airport airport = new Airport();
            airport.AirportId = Id;
            airport.AirportIndex = Convert.ToInt32(txtAirportIndex.Text.Trim());
            airport.AirportCode = txtAirportCode.Text.Trim();
            airport.AirportName = txtAirportName.Text.Trim();
            airport.AirportNameAR = txtAirportNameAR.Text.Trim();
            airport.CityCode = txtCityCode.Text.Trim();
            airport.CityName = txtCityName.Text.Trim();
            airport.CityNameAR = txtCityNameAR.Text.Trim();
            airport.CountryName = txtCountryName.Text.Trim();
            airport.CountryNameAR = txtCountryNameAR.Text.Trim();
            airport.Status = "A";
            airport.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            airport.CountryCode = txtCountryCode.Text.Trim();
            airport.save();
            gridViewAM.EditIndex = -1;
            BindAirportListGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateNotification();", "SCRIPT");
            updateApplicationObj();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI page : gridViewAM_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gridViewAM_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gridViewAM.EditIndex = -1;
            BindAirportListGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI page : gridViewAM_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewAM_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            int airportId = Convert.ToInt32(gridViewAM.DataKeys[e.RowIndex].Values["Id"]);
            HiddenField hdnStatus = (HiddenField)gridViewAM.Rows[e.RowIndex].FindControl("hdnStatus");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            if (!string.IsNullOrEmpty(rowStatus) && airportId > 0)
            {
                Airport.UpdateRowStatus(airportId, rowStatus);
                BindAirportListGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
            }
            updateApplicationObj();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI page : gridViewTM_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }

    }
    protected void gridViewAM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("AddNew"))
            {

                TextBox txtAirportIndex = (TextBox)gridViewAM.FooterRow.FindControl("txtAirportIndex");
                TextBox txtAirportCode = (TextBox)gridViewAM.FooterRow.FindControl("txtAirportCode");
                TextBox txtAirportName = (TextBox)gridViewAM.FooterRow.FindControl("txtAirportName");
                TextBox txtAirportNameAR = (TextBox)gridViewAM.FooterRow.FindControl("txtAirportNameAR");
                TextBox txtCityCode = (TextBox)gridViewAM.FooterRow.FindControl("txtCityCode");
                TextBox txtCityName = (TextBox)gridViewAM.FooterRow.FindControl("txtCityName");
                TextBox txtCityNameAR = (TextBox)gridViewAM.FooterRow.FindControl("txtCityNameAR");
                TextBox txtCountryName = (TextBox)gridViewAM.FooterRow.FindControl("txtCountryName");
                TextBox txtCountryNameAR = (TextBox)gridViewAM.FooterRow.FindControl("txtCountryNameAR");
                TextBox txtCountryCode = (TextBox)gridViewAM.FooterRow.FindControl("txtCountryCode");

                DataTable dtAirportList = Airport.GetAirportList();
                DataRow[] results = dtAirportList.Select("AirportCode = '" + txtAirportCode.Text + "'");

                if (results.Length > 0)
                {
                    Utility.StartupScript(this.Page, "alert('Duplicate  Item. AirportCode Already Exists !')", "");
                    throw new Exception("AirportCode Already Exists !");
                }
                Airport airport = new Airport();
                airport.AirportId = 0;
                airport.AirportIndex = Convert.ToInt32(txtAirportIndex.Text.Trim());
                airport.AirportCode = txtAirportCode.Text.Trim();
                airport.AirportName = txtAirportName.Text.Trim();
                airport.AirportNameAR = txtAirportNameAR.Text.Trim();
                airport.CityCode = txtCityCode.Text.Trim();
                airport.CityName = txtCityName.Text.Trim();
                airport.CityNameAR = txtCityNameAR.Text.Trim();
                airport.CountryName = txtCountryName.Text.Trim();
                airport.CountryNameAR = txtCountryNameAR.Text.Trim();
                airport.Status = "A";
                airport.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                airport.CountryCode = txtCountryCode.Text.Trim();
                airport.save();
                gridViewAM.EditIndex = -1;
                BindAirportListGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveNotification();", "SCRIPT");
                updateApplicationObj();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI page : gridViewAM_RowCommand event error.Reason: " + ex.ToString(), "0");

        }

    }
    protected void gridViewAM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDeleteAM");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");


                if (btnDel != null && lblStatus != null && drv.Row["Status"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Status"])))
                {
                    if (Convert.ToString(drv.Row["Status"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }

            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "AirportMasterUI page : gridViewAM_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

   

    #region Private Methods
    private void BindAirportListGrid()
    {
        try
        {
            DataTable dtAirportList = Airport.GetAirportList();
            if (dtAirportList != null && dtAirportList.Rows.Count > 0)
            {
                gridViewAM.DataSource = dtAirportList;
                gridViewAM.DataBind();

            }
            else
            {
                dtAirportList.Rows.Add(dtAirportList.NewRow());
                gridViewAM.DataSource = dtAirportList;
                gridViewAM.DataBind();
                gridViewAM.Rows[0].Visible = false;

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void updateApplicationObj()
    {

        try
        {
            Application["tree1"] = Airport.GetAirportCodesList();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

}
