﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System.Net;
using Ionic.Zip;

public partial class ManagementQueue : CT.Core.ParentPage
{
    protected int rowsCount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            pnlDocuments.Visible = false;
            if (!IsPostBack)
            {
                txtFromDate1.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate1.Text = DateTime.Now.ToString("dd/MM/yyyy");
                BindData();

            }
            LoadDocuments();
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.lnlDownloadAll);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        
        Utility.StartupScript(this.Page, "$('.image-link, .pdf-popup').magnificPopup({ callbacks: {elementParse: function(item) {if(item.el.context.className == 'pdf-popup') {"
  + " item.type = 'iframe';} else {item.type = 'image';}  },  afterChange: function () { var magnificPopup = $.magnificPopup.instance;  var fileSrc = $(magnificPopup.currItem).attr(" + "'src'" + ");"
  + "$('.mfp-title').append('<a href=' + fileSrc + ' download target='+ '_blank' + '>Download</a>') } }, gallery:{enabled:true}, type: 'image', }); ", "ImageViewer");

        Utility.StartupScript(this.Page, "$('.image-link, .pdf-popup').click(function () { var fileSrc = $(this).attr('href');$('.mfp-title').append('<a href=' + fileSrc + ' download target='+ '_blank' + '>Download</a>') });", "ImageViewer1");
        

    }

    private void BindData()
    {
        try
        {
            PaymentCheck PC = new PaymentCheck();
            DataSet ds = PC.GetRequestQueue("RA");
            DataView dv = new DataView();
            dv.Table = ds.Tables[0];

            IFormatProvider dateformat = new System.Globalization.CultureInfo("en-GB");
            DateTime fromdate = Convert.ToDateTime(txtFromDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
            DateTime toDate = Convert.ToDateTime(txtToDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
            if (txtFromDate1.Text.Trim().Length > 0 && txtToDate1.Text.Trim().Length > 0)
            {
                dv.RowFilter = "(DocDate>= #" + fromdate + "# AND DocDate<=#" + toDate + "#)";
            }


            gvManagementQueue.DataSource = dv;
            gvManagementQueue.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        PaymentCheck PC = new PaymentCheck();
        DataSet ds = PC.GetRequestQueue("RA");
        DataView dv = new DataView();
        dv.Table = ds.Tables[0];

        IFormatProvider dateformat = new System.Globalization.CultureInfo("en-GB");
        DateTime fromdate = Convert.ToDateTime(txtFromDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
        DateTime toDate = Convert.ToDateTime(txtToDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
        if (txtFromDate1.Text.Trim().Length > 0 && txtToDate1.Text.Trim().Length > 0)
        {
            dv.RowFilter = "(DocDate>= #" + fromdate + "# AND DocDate<=#" + toDate + "#)";
        }
        if (tbCompanyName1.Text.Trim().Length > 0)
        {
            if (dv.RowFilter.Length > 0)
            {
                dv.RowFilter += "and CompanyName LIKE '" + tbCompanyName1.Text + "*'";
            }
            else
            {
                dv.RowFilter = "CompanyName LIKE '" + tbCompanyName1.Text + "*'";
            }
        }
        if (ddlCountryName1.SelectedIndex > 0)
        {
            if (dv.RowFilter.Length > 0)
            {
                dv.RowFilter += "and CountryName Like '" + ddlCountryName1.SelectedItem.Value + "*'";
            }
            else
            {
                dv.RowFilter = "CountryName Like '" + ddlCountryName1.SelectedItem.Value + "*'";
            }
        }
        gvManagementQueue.DataSource = dv;
        gvManagementQueue.DataBind();
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PaymentCheck PC = new PaymentCheck();
                DataSet statusData = PC.GetStatusDetails("Management");
                //Find the DropDownList in the Row
                DropDownList ddlStatus = (e.Row.FindControl("ddlStatus") as DropDownList);
                ddlStatus.DataSource = statusData;
                ddlStatus.DataTextField = "Status";
                ddlStatus.DataValueField = "StatusId";
                ddlStatus.DataBind();


                string status = (e.Row.FindControl("lblStatus") as Label).Text;
                for (int i = 0; i < ddlStatus.Items.Count; i++)
                {
                    if (ddlStatus.Items[i].Text == status)
                    {
                        ddlStatus.Items[i].Selected = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lbtnBeneficiaryName_OnClick(object sender, EventArgs e)
    {
        try
        {
            ViewFiles.Controls.Clear();
            lblErrorMessage1.Text = "";
            lblErrorMessage1.Visible = false;


            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string beneficiaryName = ((LinkButton)sender).Text;
            int rowno = grdrow.DataItemIndex;
            string Id = Convert.ToString((gvManagementQueue.Rows[rowno].Cells[4].FindControl("ManageId1") as HiddenField).Value);
            hdfChequeId.Value = Id;
            PaymentCheck PC = new PaymentCheck();
            DataTable dt = PC.getUploadFilesbyId(Convert.ToInt64(Id));
            if (dt.Rows.Count > 0 && dt != null)
            {
                DataRow dr = dt.Rows[0];
                hdnRowsCount.Value = Convert.ToString(dt.Rows.Count);
                string mapPath = Utility.ToString(dr["DocPath"]);

                mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\") + 1); ;
                hdfPreviewPath.Value = mapPath;
                CommonGrid grid = new CommonGrid();


                int i = 0;
                foreach (DataRow datarow in dt.Rows)
                {
                    int index = Convert.ToInt32((datarow["DocPath"].ToString()).IndexOf("Upload"));
                    string imagePath = (datarow["DocPath"].ToString()).Substring(index);


                    HtmlAnchor anchortag = new HtmlAnchor();
                    anchortag.ID = "aViewImage" + i;
                    anchortag.Attributes.Add("onclick", "saveClickDetails(" + datarow["Id"].ToString() + "," + datarow["ChequeId"].ToString() + ")");

                    string hostName = Request["HTTP_HOST"].ToString();
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
                    {
                        anchortag.HRef = "http://" + hostName + "/" + ConfigurationManager.AppSettings["RootFolder"] + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    else
                    {
                        anchortag.HRef = "http://" + hostName + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    anchortag.InnerHtml = datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();

                    if (datarow["DocType"].ToString() == "pdf")
                    {
                        anchortag.Attributes.Add("class", "pdf-popup");
                    }
                    else
                    {
                        anchortag.Attributes.Add("class", "image-link");
                    }

                    //for having break
                    HtmlGenericControl hgc = new HtmlGenericControl("br");


                    HtmlImage image = new HtmlImage();
                    image.ID = "aViewImage" + i;
                    image.Src = imagePath + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();


                    //adding the values to the Hidden fields
                    HiddenField hf = new HiddenField();
                    hf.ID = "hdnDocpath" + i;
                    hf.Value = datarow["DocPath"].ToString();
                    hf.EnableViewState = true;
                    HiddenField hf1 = new HiddenField();
                    hf1.ID = "hdnDocName" + i;
                    hf1.Value = datarow["DocName"].ToString();
                    hf1.EnableViewState = true;
                    HiddenField hf2 = new HiddenField();
                    hf2.ID = "hdnDocType" + i;
                    hf2.Value = datarow["DocType"].ToString();
                    hf2.EnableViewState = true;
                    HiddenField hf3 = new HiddenField();
                    hf3.ID = "hdnChequeId" + i;
                    hf3.Value = datarow["ChequeId"].ToString();
                    hf3.EnableViewState = true;
                    HiddenField hf4 = new HiddenField();
                    hf4.ID = "hdnDocId" + i;
                    hf4.Value = datarow["Id"].ToString();
                    hf4.EnableViewState = true;


                    hdfPath.Value = imagePath;
                    hdfType.Value = "." + datarow["DocType"].ToString();
                    hdfDocName.Value = datarow["DocName"].ToString();
                    hdnBeneficiaryName.Value = beneficiaryName;

                    ViewFiles.Controls.Add(anchortag);
                    ViewFiles.Controls.Add(hgc);
                    ViewFiles.Controls.Add(hf3);
                    ViewFiles.Controls.Add(hf);
                    ViewFiles.Controls.Add(hf2);
                    ViewFiles.Controls.Add(hf1);
                    ViewFiles.Controls.Add(hf4);

                    i++;
                }
                lnlDownloadAll.Style.Add("display", "block");
                pnlDocuments.Visible = true;
            }
            else
            {
                //lblErrorMessage1.Text = "No Data Available";
                //lblErrorMessage1.Visible = true;
                Label lblErrMsg = new Label();
                lblErrMsg.Text = "No Data Available";
                ViewFiles.Controls.Add(lblErrMsg);
                lnlDownloadAll.Style.Add("display", "none");
                pnlDocuments.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }


    private void LoadDocuments()
    {
        try
        {
            ViewFiles.Controls.Clear();
            lblErrorMessage1.Text = "";
            lblErrorMessage1.Visible = false;


            // GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string beneficiaryName = hdnBeneficiaryName.Value.Replace(" ", "_");

            // int rowno = grdrow.DataItemIndex;
            string Id = hdfChequeId.Value;
            //hdfChequeId.Value = Id;
            PaymentCheck PC = new PaymentCheck();
            DataTable dt = PC.getUploadFilesbyId(Convert.ToInt64(Id));
            if (dt.Rows.Count > 0 && dt != null)
            {
                DataRow dr = dt.Rows[0];
                hdnRowsCount.Value = Convert.ToString(dt.Rows.Count);
                string mapPath = Utility.ToString(dr["DocPath"]);

                mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\") + 1); ;
                hdfPreviewPath.Value = mapPath;
                CommonGrid grid = new CommonGrid();


                int i = 0;
                foreach (DataRow datarow in dt.Rows)
                {
                    int index = Convert.ToInt32((datarow["DocPath"].ToString()).IndexOf("Upload"));
                    string imagePath = (datarow["DocPath"].ToString()).Substring(index);


                    HtmlAnchor anchortag = new HtmlAnchor();
                    anchortag.ID = "aViewImage" + i;
                    anchortag.Attributes.Add("onclick", "saveClickDetails(" + datarow["Id"].ToString() + "," + datarow["ChequeId"].ToString() + ")");

                    string hostName = Request["HTTP_HOST"].ToString();
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
                    {
                        anchortag.HRef = "http://" + hostName + "/" + ConfigurationManager.AppSettings["RootFolder"] + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    else
                    {
                        anchortag.HRef = "http://" + hostName + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    anchortag.InnerHtml = datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();

                    if (datarow["DocType"].ToString() == "pdf")
                    {
                        anchortag.Attributes.Add("class", "pdf-popup");
                    }
                    else
                    {
                        anchortag.Attributes.Add("class", "image-link");
                    }

                    //for having break
                    HtmlGenericControl hgc = new HtmlGenericControl("br");


                    HtmlImage image = new HtmlImage();
                    image.ID = "aViewImage" + i;
                    image.Src = imagePath + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();


                    //adding the values to the Hidden fields
                    HiddenField hf = new HiddenField();
                    hf.ID = "hdnDocpath" + i;
                    hf.Value = datarow["DocPath"].ToString();
                    hf.EnableViewState = true;
                    HiddenField hf1 = new HiddenField();
                    hf1.ID = "hdnDocName" + i;
                    hf1.Value = datarow["DocName"].ToString();
                    hf1.EnableViewState = true;
                    HiddenField hf2 = new HiddenField();
                    hf2.ID = "hdnDocType" + i;
                    hf2.Value = datarow["DocType"].ToString();
                    hf2.EnableViewState = true;
                    HiddenField hf3 = new HiddenField();
                    hf3.ID = "hdnChequeId" + i;
                    hf3.Value = datarow["ChequeId"].ToString();
                    hf3.EnableViewState = true;
                    HiddenField hf4 = new HiddenField();
                    hf4.ID = "hdnDocId" + i;
                    hf4.Value = datarow["Id"].ToString();
                    hf4.EnableViewState = true;


                    hdfPath.Value = imagePath;
                    hdfType.Value = "." + datarow["DocType"].ToString();
                    hdfDocName.Value = datarow["DocName"].ToString();
                    hdnBeneficiaryName.Value = beneficiaryName;

                    ViewFiles.Controls.Add(anchortag);
                    ViewFiles.Controls.Add(hgc);
                    ViewFiles.Controls.Add(hf3);
                    ViewFiles.Controls.Add(hf);
                    ViewFiles.Controls.Add(hf2);
                    ViewFiles.Controls.Add(hf1);
                    ViewFiles.Controls.Add(hf4);

                    i++;
                }
               
            }
            else
            {
                //lblErrorMessage1.Text = "No Data Available";
                //lblErrorMessage1.Visible = true;
                Label lblErrMsg = new Label();
                lblErrMsg.Text = "No Data Available";
                ViewFiles.Controls.Add(lblErrMsg);
                lnlDownloadAll.Style.Add("display", "none");
                
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnlDownloadAll_Click(object sender, EventArgs e)
    {
        try
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                int rowsCount = Convert.ToInt32(hdnRowsCount.Value);

                PaymentCheck pc = new PaymentCheck();
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                // Get the IP
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                for (int i = 0; i < rowsCount; i++)
                {
                    //string path = null;
                    HiddenField hdfDocPath = (ViewFiles.FindControl("hdnDocpath" + i) as HiddenField);
                    HiddenField hdfDocType = (ViewFiles.FindControl("hdnDocType" + i) as HiddenField);
                    HiddenField hdfDocName = (ViewFiles.FindControl("hdnDocName" + i) as HiddenField);
                    HiddenField hdfDocId = (ViewFiles.FindControl("hdnDocId" + i) as HiddenField);
                    //string[] docPath=hdfDocPath.Value.Split(new Char[','],StringSplitOptions.RemoveEmptyEntries);
                    string[] docPath = hdfDocPath.Value.Split(',');
                    string path = docPath[0];
                    //string docName = hdfDocName.Value + "." + hdfDocType.Value;
                    string docName = hdfDocName.Value.Split(',')[0] + "." + hdfDocType.Value.Split(',')[0];
                    zip.AddFile(path + docName, "SupportDocs");//.FileName = docName + "" + hdfDocType.Value; //renameing File name
                    //zip.AddFile(path + docName);//.FileName = docName + "" + hdfDocType.Value; //renameing File name
                    int result = pc.SaveDocClicks(Convert.ToInt64(hdfChequeId.Value), Convert.ToInt64(hdfDocId.Value), myIP, Convert.ToInt64(Settings.LoginInfo.UserID));
                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", hdnBeneficiaryName.Value.Replace(" ", "_")); //creating zip name
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            Response.End();
        }
    }

    [System.Web.Services.WebMethod]
    public static void lnkDocuments_Click(string id, string chequeId)
    {
        try
        {
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            // Get the IP
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

            PaymentCheck pc = new PaymentCheck();
            int result = pc.SaveDocClicks(Convert.ToInt64(chequeId), Convert.ToInt64(id), myIP, Convert.ToInt64(Settings.LoginInfo.UserID));

        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void gvManagementQueue_PageIndexing(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvManagementQueue.PageIndex = e.NewPageIndex;
            gvManagementQueue.EditIndex = -1;
            BindData();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblErrorMessage1");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

}
