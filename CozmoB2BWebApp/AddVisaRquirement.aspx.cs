using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
public partial class AddVisaRquirement : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck(); 
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            BindAgent();
            Page.Title = "Add requirement";
            ddlCountry.DataSource = Country.GetCountryList(); // VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0,"Select");
            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int Id;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out Id);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaRequirement requirement = new VisaRequirement(Id);
                    if (requirement != null)
                    {
                        txtName.Text = requirement.RequirementName;
                        //editor.Value = Server.HtmlDecode(requirement.Description);
                        editor.Text = requirement.Description;
                        btnSave.Text = "Update";
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(requirement.CountryCode));
                        ddlAgent.SelectedIndex = ddlAgent.Items.IndexOf(ddlAgent.Items.FindByValue(Convert.ToString(requirement.AgentId)));
                        ddlAgent.Enabled = false;
                        Page.Title = "Update Visa Requirement";
                        lblStatus.Text = "Update Visa Requirement";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaRequirement.aspx,Err:" + ex.Message,"");
                }

            }
            else
            {
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue("AE"));
                if (!isAdmin)
                {
                    ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                    ddlAgent.Enabled = false;
                }
            }

        }

    }
    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            VisaRequirement requirement = new VisaRequirement();
            requirement.AgentId=Convert.ToInt32(ddlAgent.SelectedItem.Value);
            requirement.CountryCode = ddlCountry.SelectedValue;
          //  requirement.Description = Server.HtmlEncode(editor.Value);
            requirement.Description = editor.Text;
            requirement.RequirementName = txtName.Text.Trim();
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    requirement.RequirementId = Convert.ToInt32(Request.QueryString[0]);
                    requirement.LastModifiedBy = (int)Settings.LoginInfo.UserID;

                }

            }
            else
            {
                requirement.IsActive = true;
                requirement.CreatedBy = (int)Settings.LoginInfo.UserID;

            }
            requirement.save();

            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaRequirement.aspx,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaRequirementList.aspx?Country=" + ddlCountry.SelectedValue + "&AgenyId=" + ddlAgent.SelectedItem.Value);
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
