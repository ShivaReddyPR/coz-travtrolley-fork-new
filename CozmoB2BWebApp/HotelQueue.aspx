﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="HotelQueue.aspx.cs" Inherits="CozmoB2BWebApp.HotelQueueUI" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">  
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
     <%--<script src="https://ctb2cstage.cozmotravel.com/scripts/paginathing.js" type="text/javascript"></script>--%>
    <script src="scripts/paginathing.js"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
     <link href="css/style.css" rel="stylesheet" type="text/css" />
    <style>
        .modal-dialog {
            width: 680px;
        }
        .modal-header {
            padding: 10px 10px;
            color: #FFF;
        }
 a:hover {
  cursor:pointer;
 }

    </style>
    <script type="text/javascript">

    $(document).ready(function () {
        $('[data-customtoggle]').on('click', function (e) {
            e.preventDefault();
            var contentID = $(this).data('target')
            if (contentID == "#collapseTwo") {
                document.getElementById("<%= txtPaxName.ClientID %>").value = "";
                document.getElementById("<%= txtHotel.ClientID %>").value = "";
                document.getElementById("<%= txtConfirmationNo.ClientID %>").value = "";
                $('#Checkindates').attr('checked', false);
                
            }
            else {
                document.getElementById("<%= txtAgentref.ClientID %>").value = "";
            }
            parentID = $(contentID).data('parent');
            $(parentID).add('.collapse').removeClass('in show animated fadeIn');
            $(contentID).addClass('in show animated fadeIn');
            
        })
    })
	
</script>
    <script type="text/javascript">

        var SearchList;
        var TotalRecords = 0;
        jQuery(document).ready(function ($) {
            Search();
            $('#NoRecords').hide();
        });

        function showselpage(event, pageno) {
            LoadData(pageno);
            $(this).addClass('active');

        }
        
        function BindAgents(AgentId, AgentType) {
            var Agent_id = "";
            var type = "";
            var B2BAgent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
            var B2B2BAgent_id = $('#<%=ddlB2B2BAgent.ClientID%>').val();
            if (AgentType == "B2B") {
                Agent_id = $('#<%=ddlAgents.ClientID%>').val();
                if ($('#<%=ddlB2BAgent.ClientID%>').val() < 0) {
                    $('#<%=ddlB2B2BAgent.ClientID %>').prop('disabled', true);
                }
                if (Agent_id == 0) {
                    type = "BASE";
                }
            }
            else if (AgentType == "B2B2B") {
                Agent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
                if (Agent_id >= "0") {
                    $('#<%=ddlB2B2BAgent.ClientID %>').prop('disabled', false);
                }
                else {
                    $('#<%=ddlB2B2BAgent.ClientID %>').prop('disabled', true);
                }
                if (Agent_id == " 0") {

                    if ($('#<%=ddlAgents.ClientID%>').val() > "1") {
                        type = "AGENT";/*AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations*/
                        Agent_id = $('#<%=ddlAgents.ClientID%>').val();
                    }
                    else {
                        type = "B2B"; /*B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations*/
                        Agent_id = $('#<%=ddlAgents.ClientID%>').val();
                    }
                }
                else {
                    if (Agent_id == "-1") {
                        Agent_id = $('#<%=ddlAgents.ClientID%>').val();
                        if ($('#<%=ddlAgents.ClientID%>').val() == " 0") {
                            type = "BASE";/* BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations*/
                        }
                    }
                }
                if ($('#<%=ddlAgents.ClientID%>').val() == " 0") {
                    if (Agent_id == " 0") {
                        type = "BASEB2B"; /*BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations*/
                    }
                }
            }
            if (AgentType == "") {
                Agent_id = $('#<%=ddlB2B2BAgent.ClientID%>').val();
                if (Agent_id == "0") {
                    type = "B2B2B"; /*B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations*/
                    Agent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
                    if (Agent_id == "0") {
                        type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    }
                }
                else if (Agent_id == "-1") {
                    Agent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
                    if (Agent_id == "0") {
                        type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                        Agent_id = $('#<%=ddlAgents.ClientID%>').val();
                    }
                }
                if ($('#<%=ddlAgents.ClientID%>').val() == "0") {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == " 0") {
                        if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                            type = "";
                        }
                    }
                }
                if ($('#<%=ddlAgents.ClientID%>').val() != "0") {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {
                        if (('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                            type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                            Agent_id = $('#<%=ddlAgents.ClientID%>').val();
                        }
                    }
                }
            }
            if (AgentType != "") {
                var options = "";
                options += "<option value='-1'>--Select--</option><option value='0'>--All--</option>";
                $.ajax({
                    type: "POST",
                    url:"HotelQueue.aspx/BindAllAgentTypes",
                    dataType: "json",
                    contentType: "application/json",
                    data: "{'agentId':'" + AgentId + "','AgentType':'" + AgentType + "'}",
                    success: function (data) {
                        $.each(data.d, function (item) {
                            options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
                        });
                        if (AgentType == "B2B") {
                            $('#ctl00_cphTransaction_ddlB2BAgent').empty();
                            $('#ctl00_cphTransaction_ddlB2BAgent').append(options);
                            $('#' + getElement('ddlB2BAgent').id).select2('val', '-1');
                        }
                        else if (AgentType == "B2B2B") {
                            $('#ctl00_cphTransaction_ddlB2B2BAgent').empty();
                            $('#ctl00_cphTransaction_ddlB2B2BAgent').append(options);
                            $('#' + getElement('ddlB2B2BAgent').id).select2('val', '-1');
                        }

                    },
                    failure: function (error) { alert('error'); }

                });

            }
            BindLocation(Agent_id, type);
        }
        function BindLocation(Agent_id, Type) {
            var options = "";
            options += "<option value='-1'>All</option>";
            $.ajax({
                type: "POST",
                url: "HotelQueueNew.aspx/BindLocation",
                dataType: "json",
                contentType: "application/json",
                data: "{'agentId':'" + Agent_id + "','type':'" + Type + "'}",
                success: function (data) {
                    $.each(data.d, function (item) {
                        options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
                    });
                    $('#ctl00_cphTransaction_ddlLocations').empty();
                    $('#ctl00_cphTransaction_ddlLocations').append(options);
                    $('#' + getElement('ddlLocations').id).select2('val', '-1');
                },
                failure: function (error) { alert('error'); }
            });

        }

        function Refund() {
            var ConfirmationNo = $get('<%= hdfConfirmationno.ClientID %>').value;          
            var TotalPrice = $get('<%= hdfTotalPrice.ClientID %>').value; 
            var Remarks = $get('<%= hdfRemarks.ClientID %>').value; 
            
                var Obj = {};
            Obj.ConfirmationNo = ConfirmationNo;
            Obj.TotalPrice = TotalPrice;
            Obj.Remarks = Remarks;
                Obj.Adminfee = $('#<%=txtAdminFee.ClientID%>').val();
                Obj.Supplierfee = $('#<%=txtSupplierFee.ClientID%>').val();
                var jsons = JSON.stringify(Obj);
                $.ajax({
                    type: "POST",
                    url: "HotelQueue.aspx/SubmitRefundRequest",
                    dataType: "json",
                    contentType: "application/json",
                    data: jsons,
                    success: function (data) {
                        Search();
                    }
                });
            $("#SubmitRequest").modal("hide");
            document.getElementById("<%= hdfConfirmationno.ClientID %>").value = "";
            document.getElementById("<%= hdfTotalPrice.ClientID %>").value = "";
            document.getElementById("<%= hdfRemarks.ClientID %>").value = "";
            document.getElementById("<%= txtAdminFee.ClientID %>").value = "0";
            document.getElementById("<%= txtSupplierFee.ClientID %>").value = "0";
            
        }

        function Search() {
            $('#ctl00_upProgress').show();
            //TransType  Start
            var transType = "";
                  <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.TransType == "B2B")
        {%>
            $("#TransType").hide();
            transType = "B2B";
       <% }
        else if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.TransType == "B2C")
        {%>
            $("#TransType").hide();
            transType = "B2C";
           <% }
        else
        {%>
            $("#TransType").show();
            transType = $('#<%=ddlTransType.ClientID%>').val();
           <% }%>
            //Transtype End
            //AgentFilter Starts
            var AgentFilter = $('#<%=ddlAgents.ClientID%>').val();
            var agentType = "";
            if (AgentFilter == "0") {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                    agentType = ""; // null Means binding in list all BOOKINGS
                }
            }
            if (AgentFilter > "0" && $('#<%=ddlB2BAgent.ClientID%>').val() != "-1") {
                if (AgentFilter > 1) {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {

                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else {
                        AgentFilter = $('#<%=ddlB2BAgent.ClientID%>').val();
                    }
                }
                else {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {

                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    AgentFilter = $('#<%=ddlB2BAgent.ClientID%>').val();
                }
            }
            if (AgentFilter > "0" && $('#<%=ddlB2B2BAgent.ClientID%>').val() != "-1") {
                if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else {
                    AgentFilter = $('#<%=ddlB2B2BAgent.ClientID%>').val();
                }
            }
            if ($('#<%=ddlAgents.ClientID%>').val() != "0") {
                if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {
                    if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        AgentFilter = $('#<%=ddlAgents.ClientID%>').val();
                    }
                }
            }
            //AgentFilter End
            var bookingSources;
            if (document.getElementById('ddlSource')== null) {
                bookingSources = "-1";
            }
            else {
                bookingSources = $('#<%=ddlsource.ClientID%>').val();
            }

            var obj = {};
            obj.FromDate = $('#<%=txtFromDate.ClientID%>').val();
            obj.ToDate = $('#<%=txtToDate.ClientID%>').val();
            obj.Status = $('#<%=ddlBookingStatus.ClientID%>').val();
            obj.PaxName = $.trim($('#<%=txtPaxName.ClientID%>').val());
            obj.Hotel = $.trim($('#<%=txtHotel.ClientID%>').val());
            obj.LocationId = $('#<%=ddlLocations.ClientID%>').val();
            obj.ConfirmationNo = $.trim($('#<%=txtConfirmationNo.ClientID%>').val());
            obj.source = bookingSources;
            obj.AgentFilter = AgentFilter;
            obj.agentType = agentType;
            obj.transType = transType;
            obj.Agentref = $.trim($('#<%=txtAgentref.ClientID%>').val());
            obj.CheckDate = $("#Checkindates").is(":checked");
            var jsons = JSON.stringify(obj);
            $.ajax({
                type: "POST",
                url: "HotelQueue.aspx/Search",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: jsons,
                success: function (data) {
                    $('#ctl00_upProgress').hide();
                    SearchList = JSON.parse(data.d);
                    TotalRecords = SearchList.length;
                    $('.panel-footer').remove();
                    if (TotalRecords > 0) {
                        $('#NoRecords').hide();
                        //LoadData(1);
                        $('.list-group').paginathing({
                            perPage: 10,
                            limitPagination: 9,
                            containerClass: 'panel-footer',
                            pageNumbers: true,
                            totalRecords: TotalRecords
                        });
                        LoadData(1);
                     
                       
                    }
                    else {
                          $('.list-group').children().remove();
                        $('#NoRecords').show();
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
           
        }


         function LoadData(page) {
            $('.list-group').children().remove();
               var showFrom = ((Math.ceil(page) - 1) * 10) ;
             var showTo = Math.ceil(showFrom) + 9;
             
            var orgqueueTemplate = $('#templateView').html();
            for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {
            
                //var traveldate = new Date(SearchList[i].travelDate);
                var CreatedOn = new Date(SearchList[i].CREATEdOn);
                
               
                $('.list-group').append('<li class="list-group-item" id="List' + i + '">' + orgqueueTemplate + '</li>');
                $('#AgentName').attr('id', 'AgentName-' + i);
                $('#AgentName-' + i).append(' Agent : <input id="AgentName-' + i + '" type="button" class="btn-link" value="' + SearchList[i].agent_name + '" onclick="showAgent(\'' + SearchList[i].agent_name + '\',\'' + SearchList[i].agent_phone1 + '\',\'' + SearchList[i].agent_phone2 + '\',\'' + SearchList[i].agent_email1 + '\',\'' + SearchList[i].agent_current_balance + '\',\'' + SearchList[i].agent_currency + '\')" \>');
                $('#Status').attr('id', 'Status-' + i);
                if (SearchList[i].Status != "Vouchered") {
                    $('#Status-' + i).append(' Status : <label><strong><b><span id="lblstatus" style="font-weight: bold; color:Red">' + SearchList[i].Status + '</span></b><span>&nbsp;</span><b></b></strong></label>');
                }
                else {
                    $('#Status-' + i).append(' Status : <label><strong><b><span id="lblstatus" style="font-weight: bold; color:Green">' + SearchList[i].Status + '</span></b><span>&nbsp;</span><b></b></strong></label>');
                    }
                $('#ConfirmationNo').attr('id', 'ConfirmationNo-' + i);
                $('#ConfirmationNo-' + i).append(' ConfirmationNo : <label class="bold">' + SearchList[i].confirmationNo + '</label>');
                <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.Agent || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
        {%>
                $('#Supplier').attr('id', 'Supplier-' + i);
                if (SearchList[i].hotelSource == "GIMMONIX" && SearchList[i].GXSupplier != null) {
                    $('#Supplier-' + i).append(' Supplier : <label><span id="lblSupplier" style="font-weight: bold; color: #999999">' + SearchList[i].GXSupplier + '</span></label>');
                }
                else if (SearchList[i].hotelSource == "GIMMONIX" && SearchList[i].GXSupplier == null) {
                    $('#Supplier-' + i).append(' Supplier : <label><span id="lblSupplier" style="font-weight: bold; color: #999999"> GX </span></label>');
                }
                else {
                    $('#Supplier-' + i).append(' Supplier : <label><span id="lblSupplier" style="font-weight: bold; color: #999999">' + SearchList[i].hotelSource + '</span></label>');
                }
                <% }%>
                $('#SupplierRef').attr('id', 'SupplierRef-' + i);
                $('#SupplierRef-' + i).append(' Supplier Ref : <label class="bold">' + SearchList[i].BookingRefNo + '</label>');
                //$('#City').attr('id', 'City-' + i);
                //$('#City-' + i).append('City : <label class="bold">' + SearchList[i].City + ' ' + TotalFare + '</label>');
                $('#Hotel').attr('id', 'Hotel-' + i);
                $('#Hotel-' + i).append(' Hotel : <label class="bold">' + SearchList[i].hotelName + '</label>');
                $('#NoofRooms').attr('id', 'NoofRooms-' + i);
                $('#NoofRooms-' + i).append(' No of Rooms : <label class="bold">' + SearchList[i].NoOfRooms + '</label>');
                $('#TotalPrice').attr('id', 'TotalPrice-' + i);
                $('#TotalPrice-' + i).append('Total Price : <label class="bold">' + SearchList[i].TotalFare +  '</label>');
                $('#BookedOn').attr('id', 'BookedOn-' + i);
                $('#BookedOn-' + i).append(' Booked On : <label class="bold">' + SearchList[i].CREATEdOn + '</label>');
                $('#NoofGuests').attr('id', 'NoofGuests-' + i);
                $('#NoofGuests-' + i).append('  No of Guests : <label class="bold">' + SearchList[i].Guests + '</label>');
                $('#PaxName').attr('id', 'PaxName-' + i);
                $('#PaxName-' + i).append('  Pax Name : <label class="bold">' + SearchList[i].PaxName + '</label>');
                $('#Checkin').attr('id', 'Checkin-' + i);
                $('#Checkin-' + i).append(' Check In : <label class="bold">' + SearchList[i].StartDate + '</label>');
                $('#CheckOut').attr('id', 'CheckOut-' + i);
                $('#CheckOut-' + i).append(' Check Out : <label class="bold">' + SearchList[i].EndDate + '</label>');
                $('#BookedBy').attr('id', 'BookedBy-' + i);
                $('#BookedBy-' + i).append('  Booked By : <label>' + SearchList[i].Bookedby + '</label>');
                $('#Location').attr('id', 'Location-' + i);
                $('#Location-' + i).append('  Location : <label>' + SearchList[i].locationName + '</label>');
                $('#AgentRef').attr('id', 'AgentRef-' + i);
                $('#AgentRef-' + i).append('  Agent Ref : <label class="bold">' + SearchList[i].Agent_Ref + '</label>');
              <%--if ($('#<%=hdfLAgentType.ClientID%>').val() == "BaseAgent") {
                 $('#List' + i).find('#Source').attr('id', 'Source-' + i);
                $('#Source-' + i).append(' <label class="bold">Source :</label> <label class="bold">' + SearchList[i].source + '</label>');
       }          
                 $('#List' + i).find('#OpenButton').attr('id', 'OpenButton-' + i);--%>
                //
                
                $('#List' + i).find('#ViewVoucher').attr('id', 'ViewVoucher-' + i);
                //$('#ViewVoucher-' + i).append('<input id="Open-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="Open" onclick="ViewBookingForHotel(' + SearchList[i].hotelId + ')"  \>');
                if ($('#<%=hdfIsCorporate.ClientID%>').val() != "Y") {
                    $('#ViewVoucher-' + i).append('<input id="Open-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="Open" onclick="ViewBookingForHotel(' + SearchList[i].hotelId + ',\'' + "" + '\')"  \>');
                    if (SearchList[i].voucherStatus == true) {
                    if (SearchList[i].Status != "Vouchered") {
                        $('#ViewVoucher-' + i).append('<input id="Open-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="View Voucher" onclick="ViewHotelVoucher(\'' + SearchList[i].confirmationNo + '\',\'' + "" + '\',' + SearchList[i].hotelId + ')"  \>');
                    }
                    else {
                        $('#ViewVoucher-' + i).append('<input id="Open-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="View Voucher" onclick="ViewHotelVoucher(\'' + SearchList[i].confirmationNo + '\',\'' + "" + '\',' + SearchList[i].hotelId + ')"  \>');
                        $('#ViewVoucher-' + i).append('<input id="ViewInvoice-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="View Invoice" onclick="ViewInvoice(' + SearchList[i].hotelId + ',' + SearchList[i].AgencyId + ',\'' + SearchList[i].confirmationNo + '\')"  \>');
                    }
                }
                }
                else {
                    $('#ViewVoucher-' + i).append('<input id="Open-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="Open" onclick="ViewBookingForHotel(' + SearchList[i].hotelId + ',\'' + SearchList[i].HotelPolicyStatus + '\')"  \>');
                    SearchList[i].voucherStatus == true ? $('#ViewVoucher-' + i).append('<input id="Open-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="View Voucher" onclick="ViewHotelVoucher(\'' + SearchList[i].confirmationNo + '\',\'' + SearchList[i].HotelPolicyStatus + '\',' + SearchList[i].hotelId + ')"  \>') : '';
                }
                SearchList[i].voucherStatus ==true ? $('#ViewVoucher-' + i).append('<input id="ViewDocuments-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="View Documents" onclick="ViewDocs(' + SearchList[i].hotelId + ')"  \>') : '';
                
                
                $('#List' + i).find('#CancellationPolicy').attr('id', 'CancellationPolicy-' + i);
                $('#CancellationPolicy-' + i).append(' <input id="CancellationPolicy-' + i + '" type="button" class="btn btn-primary mb-2 mr-1" value="CancellationPolicy" onclick="showCancellationPolicy(\'' + SearchList[i].EssentialInfo.replace(/(\r\n|\n|\r)/gm, "") + '\',\'' + SearchList[i].CancellationPolicy + '\',\'' + SearchList[i].EssentialInfo.replace(/(\r\n|\n|\r)/gm, "") + '\',\'' + SearchList[i].hotelSource + '\')" \>');
                if (SearchList[i].TransType == "B2C" || SearchList[i].paymentMode == "CreditCard") {
                    $('#List' + i).find('#divlnkPayment').attr('id', 'divlnkPayment-' + i);
                    $('#divlnkPayment-' + i).append('<input id="lnkPayment" type="button" class="btn-link" value="Payment Information" onclick="ViewPaymentInfo(' + i + ',' + SearchList[i].bookingId + ')" \>');
                }
                $('#List' + i).find("#dvCancelService").attr('id', 'dvCancelService-' + i);
                $('#List' + i).find("#lblCancelStatus").attr('id', 'lblCancelStatus-' + i);
                if (SearchList[i].statusId == "4" && SearchList[i].Status!="Cancelled") {
                    $('#lblCancelStatus-' + i).text('Request For Cancellation');
                    $("#dvCancelService-"+i).hide();
                }
                if (SearchList[i].Status != "Cancelled") {              
                    $('#List' + i).find("#ddlRequestChange").attr('id', 'ddlRequestChange-' + i);
                    $('#ddlRequestChange-' + i).append('<div class="form-group"><label> Request Change </label>');
                    $('#ddlRequestChange-' + i).append('<select id="ddlRequest' + i + '" class="form-control"><option value="Select">Select</option><option value="Cancel Booking">Cancel Booking</option><option value="Amend Booking">Amend Booking</option></select></div>');
                    $('#List' + i).find('#ReMarks').attr('id', 'ReMarks-' + i);
                    $('#ReMarks-' + i).append('<div class="form-group"><label> Remarks </label>');
                    $('#ReMarks-' + i).append(' <textarea id="RequestRemarks-' + i + '" class="form-control"  cols="20" rows="1" style="height:auto" placeholder="Enter ReMarks Here"></textarea></div>');
                    $('#List' + i).find('#ChangeRequestButton').attr('id', 'ChangeRequestButton-' + i);
                    $('#ChangeRequestButton-' + i).append('<input id="btnRequest-' + i + '" type="button" class="btn btn-secondary mt-3" value="Submit Request" onclick="CancelAmendBooking(\'' + SearchList[i].confirmationNo + '\',\'' + SearchList[i].TotalPrice + '\',' + i + ')" \>');
                }
           
                if ($('#<%=hdfIsCorporate.ClientID%>').val() != "Y") {                    
                    if (SearchList[i].HotelPolicyStatus == "P") {
                        $('#List' + i).find("#ddlApprovStatus").attr('id', 'ddlApprovStatus-' + i);
                        $('#ddlApprovStatus-' + i).append('<div class="form-group"><label> Select Approval Status </label>');
                        $('#ddlApprovStatus-' + i).append('<select id="ddlApprovalStatus' + i + '" class="form-control"><option value="-1">Select Status</option><option value="A">Approve</option><option value="R">Reject</option></select></div>');
                        $('#List' + i).find('#CorpReMarks').attr('id', 'CorpReMarks-' + i);
                        $('#CorpReMarks-' + i).append('<div class="form-group"><label> Remarks </label>');
                        $('#CorpReMarks-' + i).append(' <textarea id="txtCorpRemarks-' + i + '" class="form-control"  cols="20" rows="1" style="height:auto" placeholder="Enter ReMarks Here"></textarea></div>');
                        $('#List' + i).find('#StatusUpdatebutton').attr('id', 'StatusUpdatebutton-' + i);
                        $('#StatusUpdatebutton-' + i).append('<input id="btnCorpRequest-' + i + '" type="button" class="btn btn-secondary mt-3" value="Update" onclick="CorpStatusUpdate(\'' + SearchList[i].hotelId + '\',' + i + ')" \>');
                    }
                   
                    if (SearchList[i].HotelPolicyStatus == "A" || SearchList[i].HotelPolicyStatus == "R" || SearchList[i].HotelPolicyStatus == "P") {
                         var ApprovalStatus;
                    switch(SearchList[i].HotelPolicyStatus) {
                      case "P":
                        ApprovalStatus = "Approval Status: Pending";
                        break;
                      case "A":
                        ApprovalStatus = "Approval Status: Approved";
                        break;
                      case "R":
                        ApprovalStatus = "Approval Status: Rejected";
                        break;
                        }
                        $('#List' + i).find("#ApprovStatusMsg").attr('id', 'ApprovStatusMsg-' + i);
                        $('#ApprovStatusMsg-' + i).append(' <label class="bold">' + ApprovalStatus + '</label>');
                        $('#List' + i).find("#Approvby").attr('id', 'Approvby-' + i);
                        if (SearchList[i].HotelPolicyStatus == "A" || SearchList[i].HotelPolicyStatus == "R") {
                            if (SearchList[i].HotelPolicyStatus == "A") {
                                $('#Approvby-' + i).append(' <label class="text-success">' + SearchList[i].HotelPolicyStatus + 'ApprovedBy: ' + SearchList[i].ApproverEmail + '</label>');
                            }
                            else if (SearchList[i].HotelPolicyStatus == "R") {
                                $('#Approvby-' + i).append(' <label class="text-danger"> RejectedBy: ' + SearchList[i].ApproverEmail + ' - Remarks: ' + SearchList[i].CorpRemarks + '</label>');
                            }
                        }
                    }
                }
              
            }
            
        }
        

        function CancelAmendBooking(confirmationNo,TotalPrice,Id) {
            if (CancelAirBooking(Id)) {   
                 document.getElementById('ctl00_upProgress').style.display = 'block';
                $.ajax({
                    type: "POST",
                    url: "HotelQueue.aspx/UpdateServiceStatus",
                    dataType: "json",
                    contentType: "application/json",
                    data: "{'confirmationNr':'" + confirmationNo + "','remarks':'" + $('#RequestRemarks-' + Id).val() +"'}",
                    success: function (data) {
                        document.getElementById('ctl00_upProgress').style.display ='none';
                        $('#dvCancelService-' + Id).hide();
                        $('#lblCancelStatus-' + Id).text('Request For Cancellation');
                    }
                });
                <%--var RequestsId = $('#ddlRequest' + Id).val();
                var RequestText = $('#ddlRequest' + Id).find('option:selected').text();
                var RequestRemarks = $('#RequestRemarks-' + Id).val();
                if (RequestText == "Cancel Booking") {
                    if (confirm("Are you sure want to cancel your Booking?")) { 
                        document.getElementById("<%= hdfConfirmationno.ClientID %>").value = confirmationNo;
                        document.getElementById("<%= hdfTotalPrice.ClientID %>").value = TotalPrice;
                        document.getElementById("<%= hdfRemarks.ClientID %>").value = RequestRemarks;
                    $("#SubmitRequest").modal("show");   
                    return false;                   
                }
                }--%>
            }
            else {
                alert("Please Enter Remarks and Select Change request");
            }
            
        }

        function CorpStatusUpdate(hotelid,Id) {
            if (CorpBooking(Id)) {               
                var RequestsId = $('#ddlApprovalStatus' + Id).val();
                //var ApprovalStatus = $('#ddlApprovalStatus' + Id).find('option:selected').text();
                var ApprovalStatus = $('#ddlApprovalStatus' + Id).find('option:selected').val();
                var ApprovalRemarks = $('#txtCorpRemarks-' + Id).val();
               var Obj = {};
            Obj.hotelid = hotelid;
            Obj.ApprovalStatus = ApprovalStatus;
            Obj.ApprovalRemarks = ApprovalRemarks;
                var jsons = JSON.stringify(Obj);
                $.ajax({
                    type: "POST",
                    url: "HotelQueueNew.aspx/CorpStatusUpdate",
                    dataType: "json",
                    contentType: "application/json",
                    data: jsons,
                    success: function (data) {
                        Search();
                    }
                });
            }
            else {
                alert("Please Enter Remarks and Select Change request");
            }
            
        }

        function CorpBooking(Id) {   
            var val = $('#txtCorpRemarks-' + Id).val();
            var dropdown = $('#ddlApprovalStatus' + Id).val();
            if ($('#ddlApprovalStatus' + Id).val() == 0) {
                return false;
            }
            else if (val.length <= 0 || val == " ") {
                return false;
            }
            else {
                return true;
            }
        }

        function CancelAirBooking(Id) {   
            var val = $('#RequestRemarks-' + Id).val();
            var dropdown = $('#ddlRequest' + Id).val();
            if ($('#ddlRequest' + Id).val() == 0) {
                return false;
            }
            else if (val.length <= 0 || val == " ") {
                return false;
            }
            else {
                return true;
            }
        }
        function ViewHotelVoucher(confirmationNo,approvalStatus,hotelId) {
            if (approvalStatus!=null &&  approvalStatus!='') {
                //window.open("CorpHotelSummary.aspx?ConfNo=" + confirmationNo + "&HotelId=" + hotelId + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CorpHotelSummary', 'sessionData':'" + (confirmationNo + '|' + hotelId) + "', 'action':'set'}");
                window.open("CorpHotelSummary.aspx", "Summary", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
            } else {
                //window.open("printHotelVoucher.aspx?ConfNo=&HotelId=" + hotelId + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');

                 AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PrintHotelVoucher', 'sessionData':'" + (confirmationNo + '|' + hotelId) + "', 'action':'set'}");
                window.open("printHotelVoucher.aspx", "Voucher", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                }
            return false;
        }
        function ViewInvoice(hotelId, AgentId, confirmationNo) {
            //window.open("CreateHotelInvoice.aspx?hotelId=" + hotelId + "&agencyId=" + AgentId + "&ConfNo=" + confirmationNo + "", '', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
			AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CreateHotelInvoice', 'sessionData':'" + (hotelId + '|' + AgentId+ '|' + confirmationNo) + "', 'action':'set'}");
            window.open("CreateHotelInvoice.aspx", "HotelInvoice", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
        }
        function ViewDocs(hotelId) {
            //var wnd = window.open('HotelInvoiceDocuments.aspx?id=' + hotelId, '_blank', 'width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no');
			AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'HotelInvoiceDocuments', 'sessionData':'" + (hotelId) + "', 'action':'set'}");
            var wnd = window.open("HotelInvoiceDocuments.aspx", "HotelInvoice","_blank", "width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no");
        }
        //function ViewBookingForHotel(hotelId) {
        //   var url = "ViewBookingForHotel.aspx?HotelId=" + hotelId + "&fromAgent=true";
        //        window.location = url;
        //}
        function ViewBookingForHotel(hotelId, pstatus) {
            
            /*var url = "ViewBookingForHotel.aspx?HotelId=" + hotelId + "&pStatus=" + pstatus + "&fromAgent=true";*/
            //var url = "ViewBookingForHotel.aspx?HotelId=" + hotelId + "&pStatus=" + pstatus + "&fromAgent=true";
			AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'ViewBookingForHotel', 'sessionData':'" + JSON.stringify({ HotelId: hotelId, pStatus: pstatus, fromAgent: true }) + "', 'action':'set'}");
                var url = "ViewBookingForHotel.aspx";
                window.location = url;
        }


        var Ajax;

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function ViewPaymentInfo(id, bookingId) {
            pblockId = id;
            var url = "PaymentInfoAjax";
            var paramList = 'isPaymentInfo=true';
            paramList += '&bookingId=' + bookingId;
            paramList += '&blockId=' + pblockId;
            document.getElementById('PaymentInfo-'+pblockId).style.display = "block";
            document.getElementById('PaymentInfo-'+ pblockId).innerHTML = "Loading...";

            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                    var numberOfRecord=eval('<%=recordsPerPage%>');
                    for (var i = 0; i < numberOfRecord; i++) {
                    if(document.getElementById('PaymentInfo-' + i)!= null)
                        document.getElementById('PaymentInfo-' + i).style.display = "none";
                        
                    }
                    document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
                        document.getElementById('PaymentInfo-' + pblockId).innerHTML = Ajax.responseText;
                       
                    }
                }
            }
        }
        function showCancellationPolicy(EssentialInfo, CancellationPolicy, Disclaimer,hotelSource) {
            //alert(Disclaimer);
            //return false;
            //if (CPolicy != null) {
            var canceldata = CancellationPolicy;
            canceldata = canceldata.replace("^Date and time is calculated based on local time of destination|", "|");
            canceldata = canceldata.replace("^", "|");
            canceldata = canceldata.replace("|", "<br />");

            var esentialinfo = EssentialInfo.includes("^") ? EssentialInfo.split("^")[0] : EssentialInfo;
            var disclaimr = Disclaimer.includes("^") ? Disclaimer.split("^")[1].split("|")[0] : "";

            $("#Disclaimer").hide();
            document.getElementById('HotelPolicyDisclaimer').style.display = "none";

            document.getElementById('HotelCancelPolicy').innerHTML = canceldata;
            if (esentialinfo != "") {
                document.getElementById('HotelPolicyDetails').innerHTML = esentialinfo;
                $("#CancelPolicy").modal("show");
                $("#ImpInfo").show();
                document.getElementById('HotelPolicyDetails').style.display = "block";
            }
            //}
            else {
                $("#CancelPolicy").modal("show");
                $("#ImpInfo").hide();
                document.getElementById('HotelPolicyDetails').style.display = "none";
               
            }
            if (hotelSource == "GIMMONIX") {
            if (disclaimr != "") {
                document.getElementById('HotelPolicyDisclaimer').innerHTML = disclaimr+"<br/><strong>Bed type availabilities depends @ Check-In time</strong>";
                $("#CancelPolicy").modal("show");
                $("#Disclaimer").show();
                document.getElementById('HotelPolicyDisclaimer').style.display = "block";
            }
            //}
            else {
                document.getElementById('HotelPolicyDisclaimer').innerHTML = "<strong>Bed type availabilities depends @ Check-In time</strong>";
                $("#CancelPolicy").modal("show");
                $("#Disclaimer").show();
                document.getElementById('HotelPolicyDisclaimer').style.display = "block";
               
                }
            }

        }
        
        function showAgent(agent_name,agent_phone1,agent_phone2,agent_email1,agent_current_balance,agent_currency) {        
            $('#AgentDetails').show();
            $('#Agent_Name').append(' <label class="bold Agent text-primary">' + agent_name + '</label>');
            $('#CreditBalance').append('<label class="Agent">Credit Balance :</label> <label class="bold Agent">' + agent_currency + ' ' + agent_current_balance + '</label>');
            $('#Mob').append('<label class="Agent">Mobile No :</label> <label class="bold Agent">' + agent_phone1 + '</label>');
            $('#Phone').append('<label class="Agent">Phone :</label> <label class="bold Agent">' + agent_phone2 + '</label>');
            $('#Email').append('<label class="Agent">Email :</label> <label class="bold Agent">' + agent_email1 + '</label>');
        }
        function AgentModalClose() {
            $('.Agent').remove();
            $('#AgentDetails').hide();
        }
        function PaymentModalClose() {
            $('.pay').remove();
            $('#PaymentInformation').hide();
        }       

    </script>
    
   <script>
        var cal1;
        var cal2;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }
        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear(); cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }
            var date2 = cal2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>
  
    <%--<iframe id="IShimFrame" style="position: absolute; display: none;"></iframe>--%>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfLAgentType" Value=""></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfIsCorporate" Value=""></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfConfirmationno" Value=""></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfTotalPrice" Value=""></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfRemarks" Value=""></asp:HiddenField>
   <%-- <a style="cursor: default; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
        onclick="return ShowHide('divParam');">Hide Param</a>--%>
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="accordion" id="hotelQueCollapse">
                <div class="card">
                    <div class="card-header p-0" id="headingOne" style="background-color: #e8e8e8;">
      <h5 class="mb-0">
        <a class="d-block text-left text-dark font-weight-bold py-2 pl-3" href="javascript:void(0);" data-customtoggle="collapse" data-target="#collapseOne">
            Advanced Search Param
        </a>
      </h5>
    </div>
                    <div id="collapseOne" class="show collapse in" data-parent="#hotelQueCollapse">
      <div class="card-body">
      <div class="row">
              
                    <div class="col-md-2">
                        <div class="form-group">
                        <label> From Date </label>
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                        </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group">
                        <label> To Date </label>
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                    <input type="checkbox" id="Checkindates" />
  <label style="font-size:xx-small; color:Green" for="check">(Search by check dates)</label>
                                </td>
                            </tr>
                        </table>
                        </div>
                    </div>                    
                    
                    <div class="col-md-2">
                        <div class="form-group">
                        <label> Booking Status </label>
                        <asp:DropDownList CssClass="form-control" ID="ddlBookingStatus"
                            runat="server">
                            <asp:ListItem Selected="True" Value="-1">All</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                        </div>
                     
                    <div class="col-md-2">
                        <div class="form-group">
                        <%--<label>Source</label>--%>
                            <asp:Label ID="lblSource" runat="server" Text="Source"></asp:Label>
                        <asp:DropDownList AppendDataBoundItems="true" CssClass="form-control" ID="ddlsource"
                            runat="server">
                            <asp:ListItem Selected="True" Value="-1">All</asp:ListItem>
                        </asp:DropDownList>
                            </div>
                    </div>
                   
                    <div class="col-md-2">
                         <div class="form-group">
                        <label> Agent </label>
                        <asp:DropDownList ID="ddlAgents"
                            runat="server" class="form-control" onchange="javascript:BindAgents(this.value,'B2B');">
                        </asp:DropDownList>
                             </div>
                    </div>
                    
                    <div class="col-md-2">
                         <div class="form-group">
                        <label> B2BAgent </label>
                        <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="form-control" onchange="javascript:BindAgents(this.value,'B2B2B');">
                        </asp:DropDownList>
                             </div>
                    </div>                  
                   
                    <div class="col-md-2">
                        <div class="form-group">
                        <label>B2B2BAgent</label>
                        <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" onchange="javascript:BindAgents(this.value,'');">
                        </asp:DropDownList>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group">
                        <label>Location</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlLocations"
                            runat="server">
                        </asp:DropDownList>
                            </div>
                    </div>                   
                    <div class="col-md-2">
                        <div class="form-group">
                        <label>Hotel</label>
                        <asp:TextBox ID="txtHotel" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                    </div>                 
                                  
                    <div class="col-md-2">
                        <div class="form-group">
                        <label>Pax Name</label>
                        <asp:TextBox ID="txtPaxName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                    </div>
                  
                    <div class="col-md-2">
                        <div class="form-group">
                        <label> Confirmation No.</label>
                        <asp:TextBox ID="txtConfirmationNo" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                    </div>
                  <%--<div ID="TransType" style="display: none;">--%>
                        
                        <div class="col-md-2" ID="TransType" style="display: none;">
                            <div class="form-group">
                        <label> TransType</label>
                            <asp:DropDownList ID="ddlTransType" CssClass="form-control" runat="server">
                                <asp:ListItem Selected="True" Value="" Text="--All--"></asp:ListItem>
                                <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                                <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                            </asp:DropDownList>
                                </div>
                        </div>
                    <%--</div>--%>
                   
                <div class="col-md-12 text-right">
                    <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" runat="server" Text="Search" OnClientClick="javascript:Search();return false;" />
                </div>
                    </div>
                </div>
            </div>
              <%--  </div>--%>
            </div>

                <h1 class="m-0 p-0 text-center"><span class="badge badge-secondary">OR</span></h1>

  <div class="card">
    <div class="card-header p-0" id="headingTwo" style="background-color: #e8e8e8;">
      <h5 class="mb-0">
        <a class="d-block text-left text-dark font-weight-bold py-2 pl-3" href="javascript:void(0);" data-customtoggle="collapse" data-target="#collapseTwo">
          Agent Reference
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" data-parent="#hotelQueCollapse">
      <div class="card-body">
        <div id="" class="row mt-2">
            <div class="col-md-2">
                <div class="form-group">
                    <label> Ref. Number</label>
                    <%--<input type="text" class="form-control">--%>
                    <asp:TextBox ID="txtAgentref" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label> &nbsp;</label>
                    <div><%--<input type="submit" value="Search" class="btn btn-primary">--%>
                        <%--<asp:Button ID="btnAgtRefSearch" CssClass="btn but_b pull-right" runat="server" Text="Search" OnClientClick="javascript:AgentRefSearch();return false;" />--%>
                        <asp:Button ID="btnAgtRefSearch" CssClass="btn but_b pull-right" runat="server" Text="Search" OnClientClick="javascript:Search();return false;" />
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
    </div>
        </asp:Panel>
    </div>
    
     <div>
        <label id="NoRecords"><b>No Records Found!</b></label>
    </div>
    <div>
       <%-- AgentDetails PopUp--%>
        <div class="modal" id="AgentDetails">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">AgentDetails</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6" id="Agent_Name">
                                </div>
                                <div class="col-md-6" id="CreditBalance">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" id="Mob">
                                </div>
                                <div class="col-md-6" id="Phone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" id="Email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="AgentModalClose();">Close</button>
                    </div>
                </div>
            </div>
        </div>

         <%-- Cancellation PopUp--%>
        <div class="modal fade in farerule-modal-style" data-backdrop="static" id="CancelPolicy" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display:none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="btnModelC">&times;</button>
                        <h4 class="modal-title">Cancellation Policy Details</h4>
                    </div>
                    <div class="modal-body"> 
                        
                        <div class="Rulesdtl" id="HotelCancelPolicy" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                        <p class="Ruleshdr" id="ImpInfo">Important Information</p>
                        <div class="Rulesdtl" id="HotelPolicyDetails" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                        <p class="Ruleshdr" id="Disclaimer">Disclaimer</p>
                        <div class="Rulesdtl" id="HotelPolicyDisclaimer" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                       
                       <%-- <div class="bg_white" id="Msg" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>--%>
                    </div>              
                </div>
            </div>
        </div>


         <%-- Refund PopUp--%>
        <div class="modal fade in farerule-modal-style" data-backdrop="static" id="SubmitRequest" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display:none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cancellation Request Details</h4>
                    </div>
                    <div class="modal-body">     
                          <%--<input type="hidden" name="filter" value='<%#Eval("ConfirmationNo") %>' runat="server" id="ConfirmationNo" />--%>                                  
                                    <div class="col-md-2" style="margin-top:7px;">                                                                                      
                                            <asp:Label ID="Label1" runat="server" Text="Admin Fee"></asp:Label>                                                                             
                                            <asp:Label ID="lblAdminCurrency" runat="server" Text=""></asp:Label>                                       
                                            <asp:TextBox ID="txtAdminFee" runat="server" Text="0" onkeypress="return restrictNumeric(this.id,'0');" CssClass="form-control"></asp:TextBox>                                                                                                                                         
                                 </div>                                                                  
                                <div class="col-md-2" style="margin-top:7px;">
                                    <asp:Label ID="Label2" runat="server" Text="Supplier Fee"></asp:Label>
                                    <asp:Label ID="lblSupplierCurrency" runat="server" Text=""></asp:Label>                                       
                                    <asp:TextBox ID="txtSupplierFee" runat="server" Text="0" CssClass="form-control"></asp:TextBox>                              
                                 </div>
                        
                                    <div class="col-md-1 marbot_10" style="margin-top:27px;">
                                    
                                 <label>
                                     <asp:Button ID="btnRefund" CssClass="btn but_b pull-right" runat="server" Text="Refund" OnClientClick="javascript:Refund();return false;" />
                                      </label>
                                        
                                    </div>
                     </div>              
                </div>
            </div>
        </div>

   

        <div class="row">
            
            <ul class="list-group">
            </ul>

        </div>
    </div>
   <%--Template To Display Queue--%>
    <div id="templateView" style="display: none;">
        <div class="tbl">
            <div class="col-md-12 padding-0 marbot_10">

                <div class="col-md-4" id="AgentName">
                </div>
                <div class="col-md-4" id="Status">
                </div>
                <div class="col-md-4" id="ConfirmationNo">
                </div>

                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="Supplier">
                </div>
                <div class="col-md-4" id="LastCancellationDate" style="display:none">
                </div>
                <div class="col-md-4" id="SupplierRef">
                </div>
               <%-- <div class="col-md-4" id="City">
                </div>--%>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="Hotel">
                </div>
                <div class="col-md-4" id="NoofRooms">
                </div>
                <div class="col-md-4" id="TotalPrice">
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="BookedOn">
                </div>
                <div class="col-md-4" id="NoofGuests">
                </div>
                <div class="col-md-4" id="PaxName">
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="Checkin">
                </div>
                <div class="col-md-4" id="CheckOut">
                </div>
                <div class="col-md-4" id="BookedBy">
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="Location">
                </div>
                <div class="col-md-4" id="AgentRef">
                </div>
                <div class="col-md-4" id="ViewVoucher">

                </div>
            </div>
            <%--<div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-12" id="ViewDocuments">
                </div>
            </div>--%>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-12" id="CancellationPolicy">
                </div>
            </div>
          
            <div class="row"> 


<div class="col-md-8"> 

<table width="100%">
			<tbody><tr>
				<td>                                         
                                                  
    <div class="row" id="dvCancelService">

    <div class="col-md-3">
<div class="form-group" id="ddlRequestChange">

</div>
</div>
    <div class="col-md-7">
<div class="form-group" id="ReMarks">
</div>
</div>
    <div class="col-md-2">
<div class="form-group">
<label> &nbsp; </label>
<div id="ChangeRequestButton"> 
</div>
</div>
</div>

</div>
                    <label id="lblCancelStatus" style="background-color:#9AF1A2;"  ></label>
 
                                                    
</td>
			</tr>
		</tbody></table>
		
                              
 </div>

 <div class="col-md-4"> 
 

                                           
<div id="PaymentInfo-0" class="visa_PaymentInfo_pop" style="position: absolute; display: none; bottom: -70px; left: 234px; 
    height: auto!important; z-index: 9999;">  </div>


               
                                           
 </div>
                    </div>
           
            <div class="row"> 


<div class="col-md-8"> 

<table width="100%">
			<tbody><tr>
				<td>                                         
                                                  
    <div class="row">

    <div class="col-md-3">
<div class="form-group" id="ddlApprovStatus">

</div>
</div>
    <div class="col-md-7">
<div class="form-group" id="CorpReMarks">
</div>
</div>
    <div class="col-md-2">
<div class="form-group">
<label> &nbsp; </label>
<div id="StatusUpdatebutton"> 
</div>
</div>
</div>

</div> 
                                                    
</td>			</tr>
		</tbody></table>		
                              
 </div>
                    </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="ApprovStatusMsg">
                </div>
                <div class="col-md-7" id="Approvby">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-12" id="FailureRemarks">
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
