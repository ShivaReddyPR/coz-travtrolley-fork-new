﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CozmoB2BWebApp
{
    public class airport
    {
        private int index;
        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        private string airportCode;
        public string AirportCode
        {
            get { return airportCode; }
            set { airportCode = value; }
        }
        private string airportName;
        public string AirportName
        {
            get { return airportName; }
            set { airportName = value; }
        }

        private string cityCode;
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }

        private string cityName;
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        private string countryName;
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
    }
}