﻿using System;
using System.Collections.Generic;
using Visa;

public partial class VisaAjax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["requestFrom"] == "VisaType")
        {
            string key = Request["searchKey"];

            List<VisaType> visaTypeList = VisaType.GetVisaTypeList(key,0);
            string value = string.Empty;
            foreach (VisaType visaType in visaTypeList)
            {
                value += visaType.VisaTypeName + "," + visaType.VisaTypeId + "/";
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            try
            {
                Response.Write(str);
               // Response.End();
            }
            catch 
            { 
            }
        }
        else if (Request["isVisapaymentInfo"] == "true")
        {
            int visaId = 0;

            bool isnumerice = int.TryParse(Request["visaId"], out visaId);
            if (isnumerice)
            {
                VisaPaymentInfo paymentInfo = new VisaPaymentInfo();
                paymentInfo.Load(visaId);
                string paymentInfoBlock = string.Empty;
                paymentInfoBlock += "<div class=\"fleft visa_PaymentInfo_top\">";
                paymentInfoBlock += "<span class=\"fleft\">Payment Information</span>";
                //paymentInfoBlock += "<img class=\"hand margin-right-5\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["packageQueueId"].ToString() + "')\" />";
                //paymentInfoBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('QueueHistory'); return false;\">Close</a></label></div>";
                paymentInfoBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + Request["visaId"].ToString() + "').style.display='none'\">Close</a></label></div>";


                paymentInfoBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
                paymentInfoBlock += "<span class=\"fleft width-150 margin-right-5\"><b>Order Id</b></span>";
                paymentInfoBlock += "<span class=\"fleft width-140 margin-right-5\"><b>Payment Id</b></span>";
                paymentInfoBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Amount</b></span>";
                paymentInfoBlock += "<span class=\"fleft width-60 margin-right-5\"><b>Status</b></span>";
                paymentInfoBlock += "<span class=\"fleft width-60px \"><b>Payment Sources</b></span>";
                paymentInfoBlock += "</div>";


                paymentInfoBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
                paymentInfoBlock += "<span class=\"fleft width-150  margin-right-5\">" + paymentInfo.OrderId + "</span>";
                paymentInfoBlock += "<span class=\"fleft width-140 margin-right-5\">" + paymentInfo.PaymentId + "</span>";
                paymentInfoBlock += "<span class=\"fleft width-90 margin-right-5\">" + paymentInfo.Amount.ToString("#0.00") + "</span>";
                paymentInfoBlock += "<span class=\"fleft width-60 margin-right-5\">" + (VisaPaymentStatus)paymentInfo.PaymentStatus + "</span>";
                paymentInfoBlock += "<span class=\"fleft width-200px \">" + paymentInfo.PGSourceId.ToString() + "</span>";
                paymentInfoBlock += "</div>";



                paymentInfoBlock += "</div>";
                Response.Write(paymentInfoBlock);
            }
        }

    }
}
