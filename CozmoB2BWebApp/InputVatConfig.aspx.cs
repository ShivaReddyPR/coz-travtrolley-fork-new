﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.BookingEngine;
using System.Collections;
using CT.TicketReceipt.Web.UI.Controls;
public partial class InputVatConfigGUI : CT.Core.ParentPage
{
    private string INPUT_VAT_SEARCH_SESSION = "_InputVatSearchSession";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                IntialiseControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #region Private Methods
    private void IntialiseControls()
    {
        try
        {
            LoadCountry();
            SupplierCountry();
            InvatProduct();
            InvatModule();
        }
        catch
        {
            throw;
        }
    }
    private void LoadCountry()
    {
        try
        {
            SortedList Countries = Country.GetCountryList();
            ddlCountry.DataSource = Countries;
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        }
        catch
        {
            throw;
        }
    }
    private void SupplierCountry()
    {
        try
        {
            SortedList Countries = Country.GetCountryList();
            ddlSupplierCountry.DataSource = Countries;
            ddlSupplierCountry.DataTextField = "key";
            ddlSupplierCountry.DataValueField = "value";
            ddlSupplierCountry.DataBind();
            ddlSupplierCountry.Items.Insert(0, new ListItem("--Select Supplier Country--", "0"));
        }
        catch
        {
            throw;
        }
    }
    private void InvatProduct()
    {
        try
        {
            Array products = Enum.GetValues(typeof(ProductType));
            ddlProduct.Items.Insert(0,new ListItem("--Select--", "-1"));
            foreach (ProductType pd in products)
            {
                ddlProduct.Items.Add(new ListItem(pd.ToString(), ((int)pd).ToString()));
            }          
        }
        catch
        {
            throw;
        }
    }
    private void InvatModule()
    {
        try
        {
            Array modules = Enum.GetValues(typeof(Module));
            ddlModule.Items.Insert(0,new ListItem("--Select--", "-1"));
            foreach (Module md in modules)
            {
                ddlModule.Items.Add(new ListItem(md.ToString(), ((int)md).ToString()));           
            }           
        }
        catch
        {
            throw;
        }
    }
    private void bindSearch()
    {
        try
        {
            DataTable dt = InputVATDetail.GetList();
            if (dt != null && dt.Rows.Count > 0)
            {
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
        }
        catch
        {
            throw;
        }
    }
    private void Clear()
    {
        try
        {
            hdfInvatId.Value = string.Empty;
            ddlCountry.SelectedValue = "0";
            ddlSupplierCountry.SelectedValue = "0";
            txtInvatvalue.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            ddlProduct.SelectedValue = "-1";
            ddlModule.SelectedValue = "-1";
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
            rbDomestic.Checked = false;
            rbInternational.Checked = false;
            rbInvatApplied.Checked = false;
            rbInvatCalculate.Checked = false;
            rbInvatCostIncluded.Checked = false;
            rbInvatCostNotIncluded.Checked = false;
            rbInvatNotApplied.Checked = false;
            rbInvatNotCalculate.Checked = false;
        }
        catch
        {
            throw;
        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[INPUT_VAT_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["invat_id"] };
            Session[INPUT_VAT_SEARCH_SESSION] = value;
        }
    }
    private void Save()
    {
        try
        {
            InputVATDetail inpvatdeatails = new InputVATDetail();
            if (Utility.ToInteger(hdfInvatId.Value) > 0)
            {
                inpvatdeatails.Id = Utility.ToInteger(hdfInvatId.Value);
            }
            else
            {
                inpvatdeatails.Id = -1;
            }
            inpvatdeatails.CountryCode = ddlCountry.SelectedValue;
            inpvatdeatails.SupplierCountryCode = ddlSupplierCountry.SelectedValue;
            if (rbInternational.Checked == true)
            {
                inpvatdeatails.DestinationType = DestinationType.International ;
            }
            else
            {
                inpvatdeatails.DestinationType = DestinationType.Domestic;
            }
            inpvatdeatails.ProductId = Utility.ToInteger(ddlProduct.SelectedValue);
            switch (ddlModule.SelectedItem.Text)
            {
            case "Ticket":
            inpvatdeatails.Module=Module.Ticket;
            break;
            case "Hotel":
            inpvatdeatails.Module=Module.Hotel;
            break;
            case "Visa":
            inpvatdeatails.Module=Module.Visa;
            break;
            case "Package":
            inpvatdeatails.Module=Module.Package;
            break;
            case "Meal":
            inpvatdeatails.Module=Module.Meal;
            break;
            case "Seat":
            inpvatdeatails.Module=Module.Seat;
            break;
            case "HALA":
            inpvatdeatails.Module=Module.HALA;
            break;
            }
            if (rbInvatApplied.Checked == true)
            {
                inpvatdeatails.Applied = true;
            }
            else
            {
                inpvatdeatails.Applied = false;
            }
            inpvatdeatails.Charge = Utility.ToDecimal(txtInvatvalue.Text);
            if (rbInvatCostIncluded.Checked == true)
            {
                inpvatdeatails.CostIncluded= true;
            }
            else
            {
                inpvatdeatails.CostIncluded = false;
            }
            if (rbInvatCalculate.Checked == true)
            {
                inpvatdeatails.Calculate = true;
            }
            else
            {
                inpvatdeatails.Calculate = false;
            }
            inpvatdeatails.Remarks = txtRemarks.Text.Trim();
            inpvatdeatails.Status = "A";
            inpvatdeatails.CreatedBy =Utility.ToInteger(Settings.LoginInfo.UserID);
            inpvatdeatails.Save();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Successfully", "", (Utility.ToInteger(hdfInvatId.Value) == 0 ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();             
        }
        catch
        {
            throw;
        }
    }
    private void Edit(int Id)
    {
        InputVATDetail invatDetails = new InputVATDetail(Id);
        hdfInvatId.Value = Utility.ToString(invatDetails.Id);
        ddlCountry.SelectedValue = invatDetails.CountryCode;
        ddlSupplierCountry.SelectedValue = invatDetails.SupplierCountryCode;
        if (invatDetails.DestinationType == DestinationType.International)
        {
            rbInternational.Checked = true;
        }
        else
        {
            rbDomestic.Checked = true;
        }
        ddlProduct.SelectedValue = Utility.ToString(invatDetails.ProductId);

        if (invatDetails.Module == Module.Ticket)
        {
            ddlModule.SelectedValue = ((int)Module.Ticket).ToString();
        }
        else if (invatDetails.Module == Module.Hotel)
        {
            ddlModule.SelectedValue = ((int)Module.Hotel).ToString();
        }
        else if (invatDetails.Module == Module.Visa)
        {
            ddlModule.SelectedValue = ((int)Module.Visa).ToString();
        }
        else if (invatDetails.Module == Module.Package)
        {
            ddlModule.SelectedValue = ((int)Module.Package).ToString();
        }
        else if (invatDetails.Module == Module.Meal)
        {
            ddlModule.SelectedValue = ((int)Module.Meal).ToString();
        }
        else if (invatDetails.Module == Module.Seat)
        {
            ddlModule.SelectedValue = ((int)Module.Seat).ToString();
        }
        else 
        {
            ddlModule.SelectedValue = ((int)Module.HALA).ToString();
        }
        if (invatDetails.Applied==true)
        {
            rbInvatApplied.Checked = true;
        }
        else
        {
            rbInvatNotApplied.Checked = true;
        }
        if (invatDetails.CostIncluded == true)
        {
            rbInvatCostIncluded.Checked = true;
        }
        else
        {
            rbInvatCostNotIncluded.Checked = true;
        }
        if (invatDetails.Calculate == true)
        {
            rbInvatCalculate.Checked = true;
        }
        else
        {
            rbInvatNotCalculate.Checked = true;
        }
        txtInvatvalue.Text = Utility.ToString(invatDetails.Charge);
        txtRemarks.Text = invatDetails.Remarks;
        btnSave.Text = "Update";
        btnClear.Text = "Cancel";      
    }
    #endregion
#region Button Events
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "InputVatConfig page " + ex.Message, "0");
        }
    }
#endregion
    #region GridEvents
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={
                                         {"HTtxtCountry", "invatcountrycode"},
                                         {"HTtxtSuppCountry", "invatsuppliercountrycode"}, 
                                         {"HTtxtDestinationType", "invat_destination_type"},
                                         {"HTtxtProductType", "invat_product"},
                                         {"HTtxtModule", "invat_module"},
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Int32 invatid = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(invatid);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #endregion
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlModule.SelectedValue = "-1";
    }
}
