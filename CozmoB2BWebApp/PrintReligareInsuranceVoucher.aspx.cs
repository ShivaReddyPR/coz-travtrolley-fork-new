﻿using CT.BookingEngine.Insurance;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using ReligareInsurance;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class PrintReligareInsuranceVoucher : CT.Core.ParentPage
    {
        protected string imagePath = "";
        protected ReligareHeader header;
        protected string logoPath = string.Empty;
        protected AgentMaster agent;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    imagePath += ConfigurationManager.AppSettings["RootFolder"];
                    if (Request.QueryString["InsId"] != null)
                    {
                        int i = Convert.ToInt32(Request.QueryString["InsId"]);
                        header = new ReligareHeader();
                        header.Load(Convert.ToInt32(Request.QueryString["InsId"]));
                        if (header.Agent_id > 0)
                        {
                            agent = new AgentMaster(header.Agent_id);
                            logoPath = "http://ctb2bstage.cozmotravel.com/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                            imgHeaderLogo.ImageUrl = logoPath;
                        }
                       // SendInsuranceVoucherMail(); 
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher :Query string header id is empty ", Request["REMOTE_ADDR"]);
                        Response.Redirect("ReligareInsurance.aspx");
                    }
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        protected void btnEmailVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                #region Sending Email
                Hashtable table = new Hashtable();

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

                toArray.Add(txtEmailId.Text.Trim());
                btnEmail.Visible = false;
                btnPrint.Visible = false;

                string message = "";

                StringWriter sWriter = new StringWriter();
                HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
                printableArea.RenderControl(htWriter);


                message = sWriter.ToString();

                message = message.Replace("class=\"themecol1\"", "style=\"background: #1c498a; height: 24px; line-height: 22px; font-size: 12px;color: #fff; padding-left: 10px;\"");
                string regex = "<td id=\"tdPolicy.*td>";
                message = Regex.Replace(message, regex, "").ToString();
                regex = "<td id=\"tdViewLink.*td>";
                message = Regex.Replace(message, regex, "").ToString();
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], txtEmailId.Text.Trim(), toArray, "Insurance Voucher", message, table);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    throw ex;
                }
                finally
                {
                    btnEmail.Visible = true;
                    btnPrint.Visible = true;
                }

                #endregion

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Email, Severity.Normal, 0, "Failed to send Fleet Voucher to " + txtEmailId.Text + " Reason " + ex.Message, Request["REMOTE_ADDR"]);
            }
        }


        //private void SendInsuranceVoucherMail()
        //{
        //    try
        //    {
        //        if (ViewState["MailSent"] == null)
        //        {
        //            btnEmail.Visible = false;
        //            btnPrint.Visible = false;
        //            VerifyRenderingInServerForm(btnPrint);
        //            VerifyRenderingInServerForm(btnEmail);
        //            string myPageHTML = "";
        //            StringWriter sw = new StringWriter();
        //            HtmlTextWriter htw = new HtmlTextWriter(sw);
        //            printableArea.RenderControl(htw);
        //            myPageHTML = sw.ToString();
        //            string regex = "<td id=\"tdPolicy.*td>";
        //            myPageHTML = Regex.Replace(myPageHTML, regex,"").ToString();
        //            regex = "<td id=\"tdViewLink.*td>";
        //            myPageHTML = Regex.Replace(myPageHTML, regex, "").ToString();
        //            //.Replace(@"Policy Link", "").Replace(@"View Certificate", "");
        //            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
        //            toArray.Add(header.ReligarePassengers[0].Email);
        //            string policyNo = header.Policy_no;
        //            hdnpolicyNo.Value = header.Policy_no;
        //            AgentMaster agency = new AgentMaster(header.Agent_id);
        //            string bccEmails = string.Empty;
        //            if (!string.IsNullOrEmpty(agency.Email1))
        //            {
        //                bccEmails = agency.Email1;
        //            }
        //            if (!string.IsNullOrEmpty(agency.Email2))
        //            {
        //                bccEmails = bccEmails + "," + agency.Email2;
        //            }
        //            try
        //            {
        //                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Insurance Confirmation - " + policyNo, myPageHTML, new Hashtable(), bccEmails);
        //            }
        //            catch { }
        //            ViewState["MailSent"] = true;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Exception, Severity.High, (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        //    }
        //    finally
        //    {
        //        btnEmail.Visible = true;
        //        btnPrint.Visible = true;
        //    }
        //}
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
        //  server control at run time. */
        //}

        //Added By Hari
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string GetPolicyPDFData(string policyNo)
        {
            string pdfData = string.Empty;
            try
            {
                ReligarePolicyPurchase purchase = new ReligarePolicyPurchase();
                var pdfResponse = purchase.GetPolicyPdf(Convert.ToInt64(policyNo));
                string response = pdfResponse.ToString();
                string[] StreamData = response.Replace("StreamData", "$").Split('$');
                if (StreamData.Length > 1)
                {
                    pdfData = StreamData[1].Replace("&gt;", "").Replace("&lt;/", "").Trim();
                }
                return pdfData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception occured while calling the Religare pdf service." + ex.ToString(), "");
            }
            return pdfData;
        }
    }
}
