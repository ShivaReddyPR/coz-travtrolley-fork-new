﻿using CT.Configuration;
using CT.Core;
using CT.GlobalVisa;
using CT.TicketReceipt.BusinessLayer;
using NetworkInternationalPay;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;

public partial class PaymentProcessingForGVVisa : System.Web.UI.Page
{
    protected string siteName = string.Empty;
    protected decimal totalAmount = 0;
    protected string ipAddr=string.Empty;
    protected string orderId = string.Empty;
    GlobalVisaSession visaSession = new GlobalVisaSession();
    protected List<GVPassenger> passengerList = new List<GVPassenger>();
    GVVisaSale visaSale = new GVVisaSale();
    protected string paymentNIURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            siteName = ExtractSiteName(Request["HTTP_HOST"]);
            if (Settings.LoginInfo == null) //Checking Logininfo
            {
                Response.Redirect("AbandonSession.aspx", false);
                return;
            }
            if (Session["GVSession"] != null)
            {
                orderId = new Random().Next(1, 2147483647).ToString(); //Genarating Order id

                visaSession = Session["GVSession"] as GlobalVisaSession;
                if (visaSession != null && visaSession.PassengerList != null)
                {
                    visaSale = visaSession.VisaSale as GVVisaSale;
                    totalAmount = visaSale.TotVisaFee;
                    passengerList = visaSession.PassengerList as List<GVPassenger>;

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]))
                    {
                        totalAmount = 1;
                    }
                    if (passengerList != null && passengerList.Count > 0) //Network International
                    {
                        orderId = "HL-" + (passengerList[0].Fpax_name.Length <= 10 ? passengerList[0].Fpax_name : passengerList[0].Fpax_name.Substring(0, 10)) + "-" + orderId;
                        orderId = orderId.Replace(" ", "");
                        //Updating Order id and payment status
                        try
                        {
                            GVVisaSale.UpdatePaymentStatus(visaSale.TransactionId, orderId, "", "P", "Payment Pending");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        string appName = ExtractSiteName(Request["HTTP_HOST"]);
                        string urlSuccessFailure = string.Empty;
                        string rootFolder = ConfigurationManager.AppSettings["RootFolder"].ToString();
                        paymentNIURL = ConfigurationSystem.NetworkInternationalPayConfig["NI_Url"].ToString();
                        if (string.IsNullOrEmpty(rootFolder))
                        {
                            urlSuccessFailure = Request.Url.Scheme+"://" + appName + "/CorpGVPayment.aspx";
                        }
                        else
                        {
                            urlSuccessFailure = Request.Url.Scheme+"://" + appName + "/" + ConfigurationManager.AppSettings["RootFolder"].ToString() + "/CorpGVPayment.aspx";
                        }
                        NetworkInternationalPaymentService pgNI = new NetworkInternationalPaymentService();
                        pgNI.MerchantOrderNumber = orderId;
                        pgNI.Amount = totalAmount;
                        pgNI.SuccessURL = urlSuccessFailure;
                        pgNI.FailureURL = urlSuccessFailure;
                        pgNI.BillToFirstName = passengerList[0].Fpax_name;
                        pgNI.BillToLastName = passengerList[0].Fpax_name;
                        pgNI.BillToStreet1 = "Street1";
                        pgNI.BillToStreet2 = "Street2";
                        pgNI.BillToCity = "City";
                        pgNI.BillToState = "State";
                        pgNI.BillToPostalCode = "3393";
                        pgNI.BillToCountry = "AE";
                        pgNI.BillToEmail = passengerList[0].Fpax_email;
                        pgNI.UDF1 = Convert.ToString(visaSale.TransactionId);
                        requestparamsNI.Value = pgNI.GetEncryptedTransactionRequest();
                        //Session["GVSession"] = null; //Clearing All Sessions
                    }
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Details is Null", Request["REMOTE_ADDR"]);
                    Response.Redirect("AbandonSession.aspx", false);
                }
            }
            else
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (ThreadAbortException)
        {
            //Do nothing when thread aborted
        }
        catch (Exception ex)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            Email.Send(hostPort["fromEmail"].ToString(), hostPort["ErrorNotificationMailingId"].ToString(), siteName + " Error", ex.Message + "\n StackTrace:" + ex.StackTrace + "\n InnerExcepstion: " + ex.InnerException + "\n source: " + ex.Source + "\n data: " + ex.Data);
            Audit.Add(EventType.Exception, Severity.High, 0, "Error message :" + ex.Message + ex.StackTrace, ipAddr);
            Response.Redirect("CorpGVPayment.aspx?ErrorPG=True");
        }
    }
    public static string ExtractSiteName(string remoteHost)
    {
        string siteName;
        if (remoteHost.Contains("www."))
        {
            siteName = remoteHost.Remove(0, 4);
        }
        else
        {
            siteName = remoteHost;
        }
        return siteName;
    }
}
