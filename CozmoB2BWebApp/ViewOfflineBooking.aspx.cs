﻿using CT.BookingEngine;
using CT.Core;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ViewOfflineBooking : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnRefNo.Value = (!IsPostBack && Request.QueryString["ref"] != null) ? 
                Convert.ToString(Request.QueryString["ref"]) + "|" + Convert.ToString(Request.QueryString["reftype"]) : string.Empty;
        }

        [WebMethod(EnableSession = true)]
        public static ArrayList GetBookingDetails(string sRefKey)
        {
            ArrayList al = new ArrayList();
            try
            {
                OfflineHotelItinerary clsOHI = new OfflineHotelItinerary();
                OfflineItinerary clsOFI = new OfflineItinerary();
                DataSet dsFlight = new DataSet();
                DataSet dsHotel = new DataSet();

                if (sRefKey.Split('|')[1] == "2")
                {
                    dsHotel = clsOHI.GetBookDetails(sRefKey.Split('|')[0], string.Empty);
                    if (dsHotel != null && dsHotel.Tables.Count > 0 && dsHotel.Tables[0].Rows.Count > 0)
                        sRefKey = Convert.ToString(dsHotel.Tables[0].Rows[0]["BookingRefNo"]);
                    dsFlight = clsOFI.GetBookDetails(sRefKey, "REF");
                }
                else
                {
                    dsFlight = clsOFI.GetBookDetails(sRefKey.Split('|')[0], string.Empty);
                    if (dsFlight != null && dsFlight.Tables.Count > 0 && dsFlight.Tables[0].Rows.Count > 0)
                        sRefKey = Convert.ToString(dsFlight.Tables[0].Rows[0]["BookingRefNo"]);
                    dsHotel = clsOHI.GetBookDetails(sRefKey, "REF");
                }

                al.Add(JsonConvert.SerializeObject(dsFlight));
                al.Add(JsonConvert.SerializeObject(dsHotel));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GetBookingDetails)Failed to get offline booking details. Reason : " + ex.ToString(), "");
            }
            return al;
        }
    }
}