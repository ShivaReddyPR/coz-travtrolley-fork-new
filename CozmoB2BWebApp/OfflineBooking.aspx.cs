﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using System.Globalization;
using CT.TicketReceipt.Common;

public partial class OfflineBookingGUI :CT.Core.ParentPage
{
    List<string> paxType;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;   
            AuthorizationCheck();
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }

            if (Request["OfflineBookingId"] == null)
            {
                BindPassengers();
                ScriptManager.RegisterStartupScript(this, GetType(), "disablefield", "disablefield();", true); 
                
            }
            else
            {
                BindDetails();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            txtFlightNo.Text = string.Empty;
            //txtPreferredAirline.Text = string.Empty;
            //txtPreferredAirlineRet.Text = string.Empty;
            Origin.Text = string.Empty;
            Destination.Text = string.Empty;
            DepDate.Text = string.Empty;
            ReturnDateTxt.Text = string.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }
    }

    void BindPassengers()
    {
        try
        {
            if (hdnAdd.Value != null)
            {
                //Adding all paxTypes
                paxType = new List<string>();
                string[] values = hdnAdd.Value.Split('|');
                for (int i = 0; i < values.Length; i++)
                {
                    paxType.Add(values[i]);
                }
            }
            string message = "";
            int adultCount = 0;
            int childCount = 0;
            int infantCount = 0;
            for (int i = 0; i < paxType.Count; i++)
            {
                if (paxType[i] == "Adult")
                {
                    adultCount++;
                }
                else if (paxType[i] == "Child")
                {
                    childCount++;
                }
                else if (paxType[i] == "Infant")
                {
                    infantCount++;
                }
                HtmlTableRow tr = new HtmlTableRow();
                HtmlTableCell tc = new HtmlTableCell();
                tc.InnerHtml += "<b>Passenger " + (i + 1).ToString() + paxType[i] + "</b>";
                tc.Attributes.Add("class", "ns-h3");

                tc.Width = "500px";
                tc.ColSpan = 8;
                tr.Cells.Add(tc);
                HtmlTableCell AddRemove = new HtmlTableCell();
                Button btnRemove = new Button();
                btnRemove.ID = "btnAdd-" + (i + 1);
                btnRemove.Attributes["class"] = "btn but_b";

                if (i != 0)
                {

                    btnRemove.Text = "Remove";
                    //btnRemove.Width = new Unit(50, UnitType.Pixel);
                    btnRemove.Click += new EventHandler(btnRemove_Click);
                    btnRemove.BackColor = System.Drawing.Color.Red;
                    AddRemove.Controls.Add(btnRemove);
                }
                AddRemove.Attributes.Add("class", "ns-h3");
                tr.Cells.Add(AddRemove);

                HtmlTableRow tr1 = new HtmlTableRow();
                //Title
                HtmlTableCell prefix = new HtmlTableCell();

                HtmlGenericControl divlblTitle = new HtmlGenericControl("Div");
                divlblTitle.Attributes.Add("class", "col-md-3");
                divlblTitle.InnerHtml = "<div class='col-md-3'>Title:</div>";

                HtmlGenericControl divDdlTitle = new HtmlGenericControl("Div");
                divDdlTitle.Attributes.Add("class", "col-md-3");

                DropDownList ddlTitle = new DropDownList();
                ddlTitle.Items.Add(new ListItem("Select", "Select"));
                ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlTitle.Items.Add(new ListItem("Mrs.", "Mrs."));
                ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
                ddlTitle.Items.Add(new ListItem("Dr.", "Dr."));
                ddlTitle.SelectedIndex = 0;
                ddlTitle.ID = "ddlTitle" + (i + 1);
                ddlTitle.CssClass = "form-control";
                ddlTitle.CausesValidation = true;
                ddlTitle.ValidationGroup = "pax";
                divDdlTitle.Controls.Add(ddlTitle);

                RequiredFieldValidator RqddlTitle = new RequiredFieldValidator();
                RqddlTitle.ControlToValidate = ddlTitle.ID;
                RqddlTitle.ErrorMessage = "Select Title";
                RqddlTitle.Display = ValidatorDisplay.Dynamic;
                RqddlTitle.ValidationGroup = "pax";
                RqddlTitle.InitialValue = "Select";
                RqddlTitle.SetFocusOnError = true;
                divDdlTitle.Controls.Add(RqddlTitle);

                prefix.Controls.Add(divlblTitle);
                prefix.Controls.Add(divDdlTitle);

                //First Name
                HtmlGenericControl divlblfName = new HtmlGenericControl("Div");
                divlblfName.Attributes.Add("class", "col-md-3");
                divlblfName.InnerHtml = "<div  class='col-md-3' >First&nbsp;Name:</Div>";
 
                HtmlGenericControl divTxtfName = new HtmlGenericControl("Div");
                divTxtfName.Attributes.Add("class", "col-md-3");

                TextBox txtFirstName = new TextBox();
                txtFirstName.ID = "txtFirstName" + (i + 1);
                txtFirstName.CssClass = "form-control";
                txtFirstName.CausesValidation = true;
                txtFirstName.MaxLength = 10;
        
                txtFirstName.ValidationGroup = "pax";
                txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtFirstName.Attributes.Add("ondrop", "return false;");
                txtFirstName.Attributes.Add("onpaste", "return false;");
                divTxtfName.Controls.Add(txtFirstName);

                RequiredFieldValidator rfFName = new RequiredFieldValidator();
                rfFName.ControlToValidate = txtFirstName.ID;
                rfFName.ErrorMessage = "Enter First Name";
                rfFName.Display = ValidatorDisplay.Dynamic;
                rfFName.InitialValue = "";
                rfFName.SetFocusOnError = true;
                rfFName.ValidationGroup = "pax";
                divTxtfName.Controls.Add(rfFName);

                CustomValidator cvFName = new CustomValidator();
                cvFName.ControlToValidate = txtFirstName.ID;
                cvFName.ClientValidationFunction = "validateName";
                cvFName.ErrorMessage = "Minimum 2 alphabets required for First Name";
                cvFName.Display = ValidatorDisplay.Dynamic;
                cvFName.SetFocusOnError = true;
                cvFName.ValidationGroup = "pax";
                divTxtfName.Controls.Add(cvFName);

                prefix.Controls.Add(divlblfName);
                prefix.Controls.Add(divTxtfName);



                //LastName
                HtmlGenericControl divlbllName = new HtmlGenericControl("Div");
                divlbllName.Attributes.Add("class", "col-md-3");
                divlbllName.InnerHtml = "<div  class='col-md-3' >Last&nbsp;Name:</Div>";

                HtmlGenericControl divtxtlName = new HtmlGenericControl("Div");
                divtxtlName.Attributes.Add("class", "col-md-3");

                TextBox txtLastName = new TextBox();
                txtLastName.ID = "txtLastName" + (i + 1);
                txtLastName.CssClass = "form-control";
                txtLastName.CausesValidation = true;
                txtLastName.MaxLength = 15;
                txtLastName.ValidationGroup = "pax";
                txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtLastName.Attributes.Add("ondrop", "return false;");
                txtLastName.Attributes.Add("onpaste", "return false;");
                divtxtlName.Controls.Add(txtLastName);


                RequiredFieldValidator rfLName = new RequiredFieldValidator();
                rfLName.ControlToValidate = txtLastName.ID;
                rfLName.ErrorMessage = "Enter Last Name";
                rfLName.Display = ValidatorDisplay.Dynamic;
                rfLName.InitialValue = "";
                rfLName.SetFocusOnError = true;
                rfLName.ValidationGroup = "pax";
                divtxtlName.Controls.Add(rfLName);

                CustomValidator cvLName = new CustomValidator();
                cvLName.ControlToValidate = txtLastName.ID;
                cvLName.ClientValidationFunction = "validateName";
                cvLName.ErrorMessage = "Minimum 2 alphabets required for First Name";
                cvLName.Display = ValidatorDisplay.Dynamic;
                cvLName.SetFocusOnError = true;
                cvLName.ValidationGroup = "pax";
                divtxtlName.Controls.Add(cvLName);

                prefix.Controls.Add(divlbllName);
                prefix.Controls.Add(divtxtlName);

                //DoB
                HtmlGenericControl divlblDOB = new HtmlGenericControl("Div"); 
                divlblDOB.Attributes.Add("class", "col-md-3");
                divlblDOB.InnerHtml = "<div  class='col-md-3' >DOB:</Div>";

                HtmlGenericControl divDdldob = new HtmlGenericControl("Div");
                divDdldob.Attributes.Add("class", "col-md-3  mt-1");

                DropDownList ddlDay = new DropDownList();
                ddlDay.ID = "ddlDay" + (i + 1);
                ddlDay.Width = new Unit(30, UnitType.Percentage);
                ddlDay.CssClass = "form-control  pull-left";
                BindDates(ddlDay);

                DropDownList ddlMonth = new DropDownList();
                ddlMonth.ID = "ddlMonth" + (i + 1);
                ddlMonth.Width = new Unit(30, UnitType.Percentage);
                ddlMonth.CssClass = "form-control pull-left ";
                BindMonths(ddlMonth);

                DropDownList ddlYear = new DropDownList();
                ddlYear.ID = "ddlYear" + (i + 1);
                ddlYear.CssClass = "form-control pull-left";
                ddlYear.Width = new Unit(40, UnitType.Percentage);
                BindYears(ddlYear);

                Label lblError = new Label();
                lblError.ID = "lblError" + (i + 1);
                lblError.Width = new Unit(150, UnitType.Pixel);

                divDdldob.Controls.Add(ddlDay);
                divDdldob.Controls.Add(ddlMonth);
                divDdldob.Controls.Add(ddlYear);

                RequiredFieldValidator rfDay = new RequiredFieldValidator();
                rfDay.ControlToValidate = ddlDay.ID;
                rfDay.ErrorMessage = "Select Date";
                rfDay.Display = ValidatorDisplay.Dynamic;
                rfDay.InitialValue = "-1";
                rfDay.SetFocusOnError = true;
                rfDay.ValidationGroup = "pax";
                divDdldob.Controls.Add(rfDay);
                divDdldob.Controls.Add(lblError);

                RequiredFieldValidator rfMonth = new RequiredFieldValidator();
                rfMonth.ControlToValidate = ddlMonth.ID;
                rfMonth.ErrorMessage = "Select Month";
                rfMonth.Display = ValidatorDisplay.Dynamic;
                rfMonth.InitialValue = "-1";
                rfMonth.SetFocusOnError = true;
                rfMonth.ValidationGroup = "pax";
                divDdldob.Controls.Add(rfMonth);

                RequiredFieldValidator rfYear = new RequiredFieldValidator();
                rfYear.ControlToValidate = ddlYear.ID;
                rfYear.ErrorMessage = "Select Year";
                rfYear.Display = ValidatorDisplay.Dynamic;
                rfYear.InitialValue = "-1";
                rfYear.SetFocusOnError = true;
                rfYear.ValidationGroup = "pax";
                divDdldob.Controls.Add(rfYear);

                RangeValidator rvYear = new RangeValidator();
                if (paxType[i] == "Adult")
                {
                    rvYear.ControlToValidate = ddlYear.ID;
                    rvYear.SetFocusOnError = true;
                    rvYear.ErrorMessage = "Passenger Age must be in 13 and 75-Years";
                    rvYear.Display = ValidatorDisplay.Dynamic;
                    rvYear.Type = ValidationDataType.Integer;
                    rvYear.ValidationGroup = "pax";
                    rvYear.MinimumValue = DateTime.Now.AddYears(-75).Year.ToString();
                    rvYear.MaximumValue = DateTime.Now.AddYears(-12).Year.ToString();
                }
                else if (paxType[i] == "Child")
                {
                    rvYear.ControlToValidate = ddlYear.ID;
                    rvYear.SetFocusOnError = true;
                    rvYear.ErrorMessage = "Passenger Age must be in between 2 and 12-Years";
                    rvYear.Display = ValidatorDisplay.Dynamic;
                    rvYear.Type = ValidationDataType.Integer;
                    rvYear.ValidationGroup = "pax";
                    rvYear.MinimumValue = DateTime.Now.AddYears(-12).Year.ToString();
                    rvYear.MaximumValue = DateTime.Now.AddYears(-2).Year.ToString();
                }
                else if (paxType[i] == "Infant")
                {
                    rvYear.ControlToValidate = ddlYear.ID;
                    rvYear.SetFocusOnError = true;
                    rvYear.ErrorMessage = "Passenger Age must be in between 30-Days and 2-Years";
                    rvYear.Display = ValidatorDisplay.Dynamic;
                    rvYear.Type = ValidationDataType.Integer;
                    rvYear.ValidationGroup = "pax";
                    rvYear.MinimumValue = DateTime.Now.AddYears(-2).Year.ToString();
                    rvYear.MaximumValue = DateTime.Now.AddDays(0).Year.ToString();
                }
                divDdldob.Controls.Add(rvYear);
                prefix.Controls.Add(divlblDOB);
                prefix.Controls.Add(divDdldob);

                //PassportNo

                HtmlGenericControl divlblPassportNo = new HtmlGenericControl("Div");
                divlblPassportNo.Attributes.Add("class", "col-md-3");
                divlblPassportNo.InnerHtml = "<div  class='col-md-3' >Passport&nbsp;No:</Div>";

                HtmlGenericControl divtxtPassportNo = new HtmlGenericControl("Div"); // For Adult Last Name
                divtxtPassportNo.Attributes.Add("class", "col-md-3");
                
                TextBox txtPassportNo = new TextBox();
                txtPassportNo.ID = "txtPassportNo" + (i + 1);
                txtPassportNo.CssClass = "form-control";
                txtPassportNo.CausesValidation = true;
                txtPassportNo.MaxLength = 15;
                txtPassportNo.ValidationGroup = "pax";
                txtPassportNo.Attributes.Add("ondrop", "return false;");
                txtPassportNo.Attributes.Add("onpaste", "return false;");
                //pass.Controls.Add(lblPassportNo);
                divtxtPassportNo.Controls.Add(txtPassportNo);


                RequiredFieldValidator rfPass = new RequiredFieldValidator();
                rfPass.ControlToValidate = txtLastName.ID;
                rfPass.ErrorMessage = "Enter PassportNo";
                rfPass.Display = ValidatorDisplay.Dynamic;
                rfPass.InitialValue = "";
                rfPass.SetFocusOnError = true;
                rfPass.ValidationGroup = "pax";
                divtxtPassportNo.Controls.Add(rfPass);

                CustomValidator cvPass = new CustomValidator();
                cvPass.ControlToValidate = txtPassportNo.ID;
                cvPass.ClientValidationFunction = "validateName";
                cvPass.ErrorMessage = "Minimum 2 alphabets required for First Name";
                cvPass.Display = ValidatorDisplay.Dynamic;
                cvPass.SetFocusOnError = true;
                cvPass.ValidationGroup = "pax";
                divtxtPassportNo.Controls.Add(cvPass);

                prefix.Controls.Add(divlblPassportNo);
                prefix.Controls.Add(divtxtPassportNo);

                //Passport Exp
                HtmlGenericControl divlblDOBPass = new HtmlGenericControl("Div");
                divlblDOBPass.Attributes.Add("class", "col-md-3");
                divlblDOBPass.InnerHtml = "<div  class='col-md-3' >Passport&nbspExpiry:</Div>";


                HtmlGenericControl divDdlDOBPass = new HtmlGenericControl("Div");
                divDdlDOBPass.Attributes.Add("class", "col-md-3");
                
                DropDownList ddlDayPass = new DropDownList();
                ddlDayPass.ID = "ddlDayPass" + (i + 1);
                ddlDayPass.Width = new Unit(30, UnitType.Percentage);
                ddlDayPass.CssClass = "form-control pull-left";
                BindDates(ddlDayPass);

                DropDownList ddlMonthPass = new DropDownList();
                ddlMonthPass.ID = "ddlMonthPass" + (i + 1);
                ddlMonthPass.CssClass = "form-control pull-left";
                ddlMonthPass.Width = new Unit(30, UnitType.Percentage);
                BindMonths(ddlMonthPass);

                DropDownList ddlYearPass = new DropDownList();
                ddlYearPass.ID = "ddlYearPass" + (i + 1);
                ddlYearPass.CssClass = "form-control pull-left";
                ddlYearPass.Width = new Unit(40, UnitType.Percentage);
                BindPassYears(ddlYearPass);

                Label lblErrorPass = new Label();
                lblErrorPass.ID = "lblErrorPass" + (i + 1);
                lblErrorPass.Width = new Unit(150, UnitType.Pixel);

                divDdlDOBPass.Controls.Add(ddlDayPass);
                divDdlDOBPass.Controls.Add(ddlMonthPass);
                divDdlDOBPass.Controls.Add(ddlYearPass);

                RequiredFieldValidator rfPassDay = new RequiredFieldValidator();
                rfPassDay.ControlToValidate = ddlDayPass.ID;
                rfPassDay.ErrorMessage = "Select Date";
                rfPassDay.Display = ValidatorDisplay.Dynamic;
                rfPassDay.InitialValue = "-1";
                rfPassDay.SetFocusOnError = true;
                rfPassDay.ValidationGroup = "pax";
                divDdlDOBPass.Controls.Add(rfPassDay);
                divDdlDOBPass.Controls.Add(lblError);

                RequiredFieldValidator rfPassMonth = new RequiredFieldValidator();
                rfPassMonth.ControlToValidate = ddlMonthPass.ID;
                rfPassMonth.ErrorMessage = "Select Month";
                rfPassMonth.Display = ValidatorDisplay.Dynamic;
                rfPassMonth.InitialValue = "-1";
                rfPassMonth.SetFocusOnError = true;
                rfPassMonth.ValidationGroup = "pax";
                divDdlDOBPass.Controls.Add(rfPassMonth);

                RequiredFieldValidator rfPassYear = new RequiredFieldValidator();
                rfPassYear.ControlToValidate = ddlYearPass.ID;
                rfPassYear.ErrorMessage = "Select Year";
                rfPassYear.Display = ValidatorDisplay.Dynamic;
                rfPassYear.InitialValue = "-1";
                rfPassYear.SetFocusOnError = true;
                rfPassYear.ValidationGroup = "pax";
                divDdlDOBPass.Controls.Add(rfPassYear);

                prefix.Controls.Add(divlblDOBPass);
                prefix.Controls.Add(divDdlDOBPass);
                //PlaceOdIssue

                HtmlGenericControl divlblplIssue = new HtmlGenericControl("Div");
                divlblplIssue.Attributes.Add("class", "col-md-3");
                divlblplIssue.InnerHtml = "<div class='col-md-3'>Place&nbspOf&nbspIssue:&nbsp:</div>";

                HtmlGenericControl divtxtplIssue = new HtmlGenericControl("Div");
                divtxtplIssue.Attributes.Add("class", "col-md-3");

                TextBox txtplIssue = new TextBox();
                txtplIssue.ID = "txtplIssue" + (i + 1);
                txtplIssue.CssClass = "form-control";
                txtplIssue.CausesValidation = true;
                txtplIssue.MaxLength = 15;
                txtplIssue.ValidationGroup = "pax";
                txtplIssue.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtplIssue.Attributes.Add("ondrop", "return false;");
                txtplIssue.Attributes.Add("onpaste", "return false;");
                divtxtplIssue.Controls.Add(txtplIssue);


                RequiredFieldValidator rfPlIssue = new RequiredFieldValidator();
                rfPlIssue.ControlToValidate = txtLastName.ID;
                rfPlIssue.ErrorMessage = "Enter Place of Issue";
                rfPlIssue.Display = ValidatorDisplay.Dynamic;
                rfPlIssue.InitialValue = "";
                rfPlIssue.SetFocusOnError = true;
                rfPlIssue.ValidationGroup = "pax";
                divtxtplIssue.Controls.Add(rfPlIssue);

                CustomValidator cvPlIssue = new CustomValidator();
                cvPlIssue.ControlToValidate = txtplIssue.ID;
                cvPlIssue.ClientValidationFunction = "validateName";
                cvPlIssue.ErrorMessage = "Minimum 2 alphabets required for First Name";
                cvPlIssue.Display = ValidatorDisplay.Dynamic;
                cvPlIssue.SetFocusOnError = true;
                cvPlIssue.ValidationGroup = "pax";
                divtxtplIssue.Controls.Add(cvPlIssue);

                prefix.Controls.Add(divlblplIssue);
                prefix.Controls.Add(divtxtplIssue);
                //Email
                if (i == 0)
                {

                    HtmlGenericControl divlblEmail = new HtmlGenericControl("Div");
                    divlblEmail.Attributes.Add("class", "col-md-3");
                    divlblEmail.InnerHtml = "<div class='col-md-3'>Email&nbsp;:*</div>";

                    HtmlGenericControl divtxtEmail = new HtmlGenericControl("Div");
                    divtxtEmail.Attributes.Add("class", "col-md-3");

                    TextBox txtEmail = new TextBox();
                    txtEmail.ID = "txtEmail" + (i + 1);
                    txtEmail.CssClass = "form-control";
                    txtEmail.CausesValidation = true;
                    txtEmail.ValidationGroup = "pax";

                    divtxtEmail.Controls.Add(txtEmail);

                    RequiredFieldValidator emailvc = new RequiredFieldValidator();
                    emailvc.ControlToValidate = txtEmail.ID;
                    emailvc.ErrorMessage = "Enter Email";
                    emailvc.Display = ValidatorDisplay.Dynamic;
                    emailvc.InitialValue = "";
                    emailvc.SetFocusOnError = true;
                    emailvc.ValidationGroup = "pax";
                    divtxtEmail.Controls.Add(emailvc);

                    RegularExpressionValidator emailrv = new RegularExpressionValidator();
                    emailrv.ControlToValidate = txtEmail.ID;
                    emailrv.ErrorMessage = "Invalid Email address";
                    emailrv.Display = ValidatorDisplay.Dynamic;
                    emailrv.SetFocusOnError = true;
                    emailrv.ValidationGroup = "pax";
                    emailrv.ValidationExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*$";
                    divtxtEmail.Controls.Add(emailrv);

                    prefix.Controls.Add(divlblEmail);
                    prefix.Controls.Add(divtxtEmail);
                }
                tr1.Cells.Add(prefix);

                tblPassenger.Controls.Add(tr);
                tblPassenger.Controls.Add(tr1);
            }
            int count=adultCount+childCount+infantCount;
            message = "Currently Added (" + count + ") |" + adultCount + "-Adult(s)|" + childCount + "-Child(s)|" + infantCount + "-Infant(s)";
            lblMessage.Text = message;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //Added By chandan on 23/01/2016 For retrieve Existing Offline Booking Details
    void BindDetails()
    {
        try
        {
                 int id = Convert.ToInt32(Request["OfflineBookingId"]);
                DataSet ds = new DataSet();
                FlightOfflineBooking offlineBooking = new FlightOfflineBooking(id);
                Airport air = new Airport(offlineBooking.DepAirport);
                Origin.Text = "(" + air.AirportCode + ")" + air.CityName + "," + air.CountryName;

                Airport airReturn = new Airport(offlineBooking.ArrAirport);
                Destination.Text = "(" + airReturn.AirportCode + ")" + airReturn.CityName + "," + airReturn.CountryName;


                DepDate.Text = offlineBooking.DepDateTime.ToString("dd/MM/yyyy");
                List<string> prefAir = new List<string>();
                prefAir = AirlineInfo.GetAllAirlines();
                if (offlineBooking.DepAirPre !=null && offlineBooking.DepAirPre.Length > 0)
                {
                    foreach (string airlineName in prefAir)
                    {
                        string code;
                        code = airlineName.Split('|')[0];
                        if (code == offlineBooking.DepAirPre)
                        {
                            txtPreferredAirline.Text = airlineName.Replace('|', ',');
                        }
                    }
                }
                else
                {
                    txtPreferredAirline.Text = "N/A";
                }
                if (offlineBooking.DepFlightNo != null && offlineBooking.DepFlightNo.Length > 0) txtFlightNo.Text = offlineBooking.DepFlightNo;
                else txtFlightNo.Text = "N/A";

                DateTime defaultTime = new DateTime();
                if (offlineBooking.ArrDateTime > defaultTime)
                {
                    roundtrip.Checked = true;
                    ReturnDateTxt.Text = offlineBooking.ArrDateTime.ToString("dd/MM/yyyy");
                    if (offlineBooking.ArrAirPre!=null && offlineBooking.ArrAirPre.Length > 0)
                    {
                        foreach (string airlineName in prefAir)
                        {
                            string code;
                            code = airlineName.Split('|')[0];
                            if (code == offlineBooking.ArrAirPre)
                            {
                                txtPreferredAirlineRet.Text = airlineName.Replace('|', ',');
                            }
                        }
                    }
                    else
                    {
                        txtPreferredAirlineRet.Text = "N/A";
                        
                    }
                    if (offlineBooking.ArrFlightNo != null && offlineBooking.ArrFlightNo.Length > 0)
                    {
                        txtFlightNoRet.Text = offlineBooking.ArrFlightNo;
                    }
                    else
                    {
                        txtFlightNoRet.Text = "N/A";
                    }
                    roundtrip.Checked = true;
                    oneway.Visible = false;
                }
                else
                {
                    oneway.Checked = true;
                    roundtrip.Visible = false;
                    //txtPreferredAirlineRet.Visible = false;
                    //ReturnDateTxt.Visible = false;
                    //txtFlightNoRet.Visible = false;
                    tblReturn.Visible = false;
                }

                hdnAdd.Value = "";
                hdnData.Value = "";
                for (int i = 0; i < offlineBooking.OfflineDetails.Rows.Count; i++)
                {
                    DataRow row = offlineBooking.OfflineDetails.Rows[i];
                    hdnAdd.Value += row["PaxType"].ToString() + "|";
                    string title = Convert.ToString(row["Title"]);
                    string firstName = Convert.ToString(row["FirstName"]);
                    string lastName = Convert.ToString(row["SurName"]);

                    string[] dateOfBirth = Convert.ToDateTime(row["DateOfBirth"]).ToString("dd/MM/yyyy").Split('/');
                    string passNo = Convert.ToString(row["PassportNo"]);
                    string[] passportExp = Convert.ToDateTime(row["PassportExpdate"]).ToString("dd/MM/yyyy").Split('/');
                    string placeOfIssue = Convert.ToString(row["PlaceOfIssue"]);
                    string email = "";
                    if (row["Email"] != DBNull.Value)
                    {
                        email = Convert.ToString(row["Email"]);
                    }


                    hdnData.Value += title + "|" +
                        firstName + "|" + lastName + "|" +
                        Convert.ToInt32(dateOfBirth[0]) + "|" + Convert.ToInt32(dateOfBirth[1]) + "|" + Convert.ToInt32(dateOfBirth[2]) + "|" +
                        passNo + "|" +
                       Convert.ToInt32(passportExp[0]) + "|" + Convert.ToInt32(passportExp[1]) + "|" + Convert.ToInt32(passportExp[2]) + "|" +
                       placeOfIssue
                       + "|" + email + "#";
                }

                hdnAdd.Value = hdnAdd.Value.TrimEnd('|');
                hdnData.Value = hdnData.Value.TrimEnd('#');
                BindPassengers();

                LoadData();
                string[] paxCount = hdnAdd.Value.Split('|');
                for (int i = 0; i < paxCount.Length - 1; i++)
                {

                    Button removeButton = tblPassenger.FindControl("btnAdd-" + (i + 2)) as Button;
                    removeButton.BackColor = System.Drawing.Color.Red;
                    removeButton.Visible = false;
                }
                imgAdd.Visible = false;
                btnContinue.Enabled = false;
                HlnkBack.Visible = true;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        try
        {
            string Id = ((System.Web.UI.Control)(sender)).ID.Split('-')[1];
            GetData();
            string[] pax = hdnAdd.Value.Split('|');
            string addpax = "";
            for (int i = 0; i < pax.Length; i++)
            {
                if ((Convert.ToInt32(Id) - 1) != i)
                {
                    if (addpax.Length > 0)
                    {
                        addpax += "|" + pax[i];
                    }
                    else
                    {
                        addpax = pax[i];
                    }
                }
            }
            hdnAdd.Value = addpax;

            string[] totalDta = hdnData.Value.Split('#');
            string totpaxDetails = "";
            for (int i = 0; i < totalDta.Length; i++)
            {
                if ((Convert.ToInt32(Id) - 1) != i)
                {
                    if (totpaxDetails.Length > 0)
                    {
                        totpaxDetails += "#" + totalDta[i];
                    }
                    else
                    {
                        totpaxDetails = totalDta[i];
                    }
                }
            }
            hdnData.Value = totpaxDetails;
            tblPassenger.Rows.Clear();
            BindPassengers();
            LoadData();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    void BindDates(DropDownList ddlDay)
    {
        ddlDay.Items.Add(new ListItem("Day", "-1"));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    void BindMonths(DropDownList ddlMonth)
    {
        ddlMonth.Items.Add(new ListItem("Month", "-1"));
        string[] months = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        for (int i = 1; i <= months.Length; i++)
        {
            ddlMonth.Items.Add(new ListItem(months[i - 1], i.ToString()));
        }
    }

    void BindYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = 1930; i <= DateTime.Now.Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }
    void BindPassYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = DateTime.Now.Year; i <= 2030; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }
    protected void lnkAdd_onclick(object sender, EventArgs e)
    {
        try
        {
            GetData();
            tblPassenger.Rows.Clear();
            //Adding paxtypes
            hdnAdd.Value += "|" + ddlPax.SelectedItem.Text;
            //Bindinng passengers
            BindPassengers();
            LoadData();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    //Data is saving hiddenfield
    void GetData()
    {
        try
        {
            hdnData.Value = "0";
            if (hdnAdd.Value != null)
            {
                paxType = new List<string>();
                string[] values = hdnAdd.Value.Split('|');
                for (int i = 0; i < values.Length; i++)
                {
                    paxType.Add(values[i]);
                }
            }
           
            for (int i = 0; i < paxType.Count; i++)
            {
                DropDownList ddlTitle = tblPassenger.FindControl("ddlTitle" + (i + 1)) as DropDownList;
                if (hdnData.Value == "0")
                {
                    hdnData.Value = ddlTitle.SelectedItem.Value;
                }
                else
                {
                    hdnData.Value += "#" + ddlTitle.SelectedItem.Value;
                }
                TextBox txtFirstName = tblPassenger.FindControl("txtFirstName" + (i + 1)) as TextBox;
                hdnData.Value += "|" + txtFirstName.Text.Trim();
                TextBox txtLastName = tblPassenger.FindControl("txtLastName" + (i + 1)) as TextBox;
                hdnData.Value += "|" + txtLastName.Text.Trim();
                DropDownList ddlDay = tblPassenger.FindControl("ddlDay" + (i + 1)) as DropDownList;
                hdnData.Value += "|" + ddlDay.SelectedItem.Value;
                DropDownList ddlMonth = tblPassenger.FindControl("ddlMonth" + (i + 1)) as DropDownList;
                hdnData.Value += "|" + ddlMonth.SelectedItem.Value;
                DropDownList ddlYear = tblPassenger.FindControl("ddlYear" + (i + 1)) as DropDownList;
                hdnData.Value += "|" + ddlYear.SelectedItem.Value;
                TextBox txtPassportNo = tblPassenger.FindControl("txtPassportNo" + (i + 1)) as TextBox;
                hdnData.Value += "|" + txtPassportNo.Text.Trim();
                DropDownList ddlDayPass = tblPassenger.FindControl("ddlDayPass" + (i + 1)) as DropDownList;
                hdnData.Value += "|" + ddlDayPass.SelectedValue;
                DropDownList ddlMonthPass = tblPassenger.FindControl("ddlMonthPass" + (i + 1)) as DropDownList;
                hdnData.Value += "|" + ddlMonthPass.SelectedValue;
                DropDownList ddlYearPass = tblPassenger.FindControl("ddlYearPass" + (i + 1)) as DropDownList;
                hdnData.Value += "|" + ddlYearPass.SelectedValue;
                TextBox txtplIssue = tblPassenger.FindControl("txtplIssue" + (i + 1)) as TextBox;
                hdnData.Value += "|" + txtplIssue.Text.Trim();
                if (i == 0)
                {
                    TextBox txtEmail = tblPassenger.FindControl("txtEmail" + (i + 1)) as TextBox;
                    hdnData.Value += "|" + txtEmail.Text.Trim();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //data assigned controls
    void LoadData()
    {
        try
        {
            if (hdnAdd.Value != null)
            {
                paxType = new List<string>();
                string[] values = hdnAdd.Value.Split('|');
                for (int i = 0; i < values.Length; i++)
                {
                    paxType.Add(values[i]);
                }
            }
            string[] totData = hdnData.Value.Split('#');
            for (int i = 0; i < totData.Length; i++)
            {
                DropDownList ddlTitle = tblPassenger.FindControl("ddlTitle" + (i + 1)) as DropDownList;
                TextBox txtFirstName = tblPassenger.FindControl("txtFirstName" + (i + 1)) as TextBox;
                TextBox txtLastName = tblPassenger.FindControl("txtLastName" + (i + 1)) as TextBox;
                DropDownList ddlDay = tblPassenger.FindControl("ddlDay" + (i + 1)) as DropDownList;
                DropDownList ddlMonth = tblPassenger.FindControl("ddlMonth" + (i + 1)) as DropDownList;
                DropDownList ddlYear = tblPassenger.FindControl("ddlYear" + (i + 1)) as DropDownList;
                TextBox txtPassportNo = tblPassenger.FindControl("txtPassportNo" + (i + 1)) as TextBox;
                DropDownList ddlDayPass = tblPassenger.FindControl("ddlDayPass" + (i + 1)) as DropDownList;
                DropDownList ddlMonthPass = tblPassenger.FindControl("ddlMonthPass" + (i + 1)) as DropDownList;
                DropDownList ddlYearPass = tblPassenger.FindControl("ddlYearPass" + (i + 1)) as DropDownList;
                TextBox txtplIssue = tblPassenger.FindControl("txtplIssue" + (i + 1)) as TextBox;
               
                string[] data = totData[i].Split('|');
                ddlTitle.SelectedValue = data[0].ToString();
                txtFirstName.Text = data[1].ToString();
                txtLastName.Text = data[2].ToString();
                ddlDay.SelectedValue = data[3].ToString();
                ddlMonth.SelectedValue = data[4].ToString();
                ddlYear.SelectedValue = data[5].ToString();
                txtPassportNo.Text = data[6].ToString();
                ddlDayPass.SelectedValue = data[7].ToString();
                ddlMonthPass.SelectedValue = data[8].ToString();
                ddlYearPass.SelectedValue = data[9].ToString();
                txtplIssue.Text = data[10].ToString();
                if (i == 0)
                {
                    TextBox txtEmail = tblPassenger.FindControl("txtEmail" + (i + 1)) as TextBox;
                    txtEmail.Text = data[11].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //saving offline Booking
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                FlightOfflineBooking offBooking = new FlightOfflineBooking();
                offBooking.DepAirport = GetCityCode(Origin.Text.Trim());
                offBooking.ArrAirport = GetCityCode(Destination.Text.Trim());
                IFormatProvider format = new CultureInfo("en-GB", true);
                offBooking.DepDateTime = Convert.ToDateTime(DepDate.Text, format);
                if (txtPreferredAirline.Text != "Type Preferred Airline")
                {
                    offBooking.DepAirPre = airlineCode.Value;
                }
                offBooking.DepFlightNo = txtFlightNo.Text;

                //Round Trip
                if (roundtrip.Checked)
                {
                    offBooking.ArrDateTime = Convert.ToDateTime(ReturnDateTxt.Text, format);
                    if (txtPreferredAirlineRet.Text != "Type Preferred Airline")
                    {
                        offBooking.ArrAirPre = airlineCodeRet.Value;
                    }
                    offBooking.ArrFlightNo = txtFlightNoRet.Text;

                }
                offBooking.AgentId = Settings.LoginInfo.AgentId;
                offBooking.UserId = (int)Settings.LoginInfo.UserID;
                offBooking.LocationId = (int)Settings.LoginInfo.LocationID;
                offBooking.CreatedBy = (int)Settings.LoginInfo.UserID;
                //PaxDetails

                hdnData.Value = "0";
                if (hdnAdd.Value != null)
                {
                    paxType = new List<string>();
                    string[] values = hdnAdd.Value.Split('|');
                    for (int i = 0; i < values.Length; i++)
                    {
                        paxType.Add(values[i]);
                    }
                }
                string customerEmail = string.Empty;
                for (int i = 0; i < paxType.Count; i++)
                {
                    DataRow row = offBooking.OfflineDetails.NewRow();
                    DropDownList ddlTitle = tblPassenger.FindControl("ddlTitle" + (i + 1)) as DropDownList;
                    TextBox txtFirstName = tblPassenger.FindControl("txtFirstName" + (i + 1)) as TextBox;
                    TextBox txtLastName = tblPassenger.FindControl("txtLastName" + (i + 1)) as TextBox;
                    DropDownList ddlDay = tblPassenger.FindControl("ddlDay" + (i + 1)) as DropDownList;
                    DropDownList ddlMonth = tblPassenger.FindControl("ddlMonth" + (i + 1)) as DropDownList;
                    DropDownList ddlYear = tblPassenger.FindControl("ddlYear" + (i + 1)) as DropDownList;
                    TextBox txtPassportNo = tblPassenger.FindControl("txtPassportNo" + (i + 1)) as TextBox;
                    DropDownList ddlDayPass = tblPassenger.FindControl("ddlDayPass" + (i + 1)) as DropDownList;
                    DropDownList ddlMonthPass = tblPassenger.FindControl("ddlMonthPass" + (i + 1)) as DropDownList;
                    DropDownList ddlYearPass = tblPassenger.FindControl("ddlYearPass" + (i + 1)) as DropDownList;
                    TextBox txtplIssue = tblPassenger.FindControl("txtplIssue" + (i + 1)) as TextBox;

                    row["Title"] = ddlTitle.SelectedItem.Text;
                    row["PaxType"] = paxType[i];
                    row["SurName"] = txtLastName.Text.Trim();
                    row["FirstName"] =txtFirstName.Text.Trim();
                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYear.SelectedItem.Value), Convert.ToInt32(ddlMonth.SelectedItem.Value), Convert.ToInt32(ddlDay.SelectedItem.Value));
                    row["DateOfBirth"] = tempDob;
                    row["PassportNo"] = txtPassportNo.Text.Trim();
                    DateTime tempDobExp = new DateTime(Convert.ToInt32(ddlYearPass.SelectedItem.Value), Convert.ToInt32(ddlMonthPass.SelectedItem.Value), Convert.ToInt32(ddlDayPass.SelectedItem.Value));
                    row["PassportExpdate"] = tempDobExp;
                    row["PlaceOfIssue"] = txtplIssue.Text.Trim();
                    row["CreatedBy"] = Settings.LoginInfo.UserID;
                    if (i == 0)
                    {
                        TextBox txtEmail = tblPassenger.FindControl("txtEmail" + (i + 1)) as TextBox;
                        row["Email"] = txtEmail.Text;
                        customerEmail = txtEmail.Text;
                    }
                    else
                    {
                        row["Email"] = string.Empty;
                    }
                    offBooking.OfflineDetails.Rows.Add(row);
                }
                offBooking.Save();//Saving
                //sending mail
                try
                {
                    string message = ConfigurationManager.AppSettings["OfflineBookingContent"].ToString();
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(customerEmail);
                    toArray.Add(Settings.LoginInfo.AgentEmail);
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "OfflineBookingRequest", message, new Hashtable());// ConfigurationManager.AppSettings["OfflineBooking"].ToString()
                }
                catch
                {
                }
                Clear();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = "SucessFully Saved";
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    //Celar all fields
    private void Clear()
    {
        try
        {
            txtFlightNo.Text = string.Empty;
            txtPreferredAirline.Text = string.Empty;
            txtPreferredAirlineRet.Text = string.Empty;
            Origin.Text = string.Empty;
            Destination.Text = string.Empty;
            DepDate.Text = string.Empty;
            ReturnDateTxt.Text = string.Empty;
            tblPassenger.Controls.Clear();
            hdnAdd.Value = "Adult";
            roundtrip.Checked = true;
            BindPassengers();
            hdnData.Value = "0";
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    
    }
    //Get CityCode
    private string GetCityCode(string searchCity) // For Flight
    {
        string cityCode = "";

        cityCode = searchCity.Split(',')[0];

        if (cityCode.Contains("(") && cityCode.Contains(")") && cityCode.Length > 3)
        {
            // cityCode = cityCode.Substring(cityCode.Length - 4, 3);
            cityCode = cityCode.Substring(1, 3);
        }
        return cityCode;
    }

}
