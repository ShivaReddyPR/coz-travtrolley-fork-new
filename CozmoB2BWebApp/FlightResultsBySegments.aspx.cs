﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.MetaSearchEngine;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using System.Collections.Generic;
using CT.Configuration;
using System.Globalization;
using CT.Corporate;
using System.Web.Script.Serialization;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json;

namespace CozmoB2BWebApp
{
    public partial class FlightResultsBySegments : CT.Core.ParentPage
    {
        #region Members
        protected ArrayList toCity;
        protected ArrayList fromCity;
        protected ArrayList tocityAirports;
        protected ArrayList fromcityAirports;
        protected int agencyId;
        protected AgentMaster agency;
        protected bool isServiceAgency;
        protected string morningChk = string.Empty;
        protected string afternoonChk = string.Empty;
        protected string eveningChk = string.Empty;
        protected string nightChk = string.Empty;
        protected string directChk = string.Empty;
        protected string oneStopChk = string.Empty;
        protected string moreThanTwoStopChk = string.Empty;
        
        protected List<string> layoverAirports = new List<string>();
        protected List<string> originAirports = new List<string>();
        protected List<string> destinationAirports = new List<string>();
        protected List<string> airlineList = new List<string>();
        protected List<string> airlineListReturn = new List<string>();
        protected List<string> airlineRemovedList = new List<string>();
        protected List<string> airlineCodeList = new List<string>();
        protected List<string> airportRemovedList = new List<string>();
        protected SearchResult[] resultDisplay = new SearchResult[0];
        protected bool onwardNonRefundableFound = false, returnNonRefundableFound = false, onwardRefundableFound = false, returnRefundableFound = false;
        protected bool onwardNonStopFound = false, returnNonStopFound = false, onwardOneStopFound = false, returnOneStopFound = false, onwardTwoStopFound = false, returnTwoStopFound = false;
        protected int maxResult, minResult;
        protected string sessionId;
        protected string priceImgSrc = "images/spacer.gif";
        protected string depImgSrc = "images/spacer.gif";
        protected string arrImgSrc = "images/spacer.gif";
        protected string durImgSrc = "images/spacer.gif";
        protected Dictionary<string, string> Airlines = new Dictionary<string, string>();        
        protected SearchResult[] filteredResults;
        //modify search
        protected string userType;
        protected SearchRequest searchRequest = new SearchRequest();
        protected SearchRequest request;
        protected string preferredAirlines = string.Empty;
        protected string airlineMessage = string.Empty;
        protected DataTable dtTravelReason = new DataTable();
        protected int filteredCount;
        protected string resultIds = string.Empty;
        protected List<SearchResult> onwardResults = new List<SearchResult>();
        protected List<SearchResult> returnResults = new List<SearchResult>();
        protected string onwardResultJson = string.Empty, returnResultJson = string.Empty, airlineArray;
        protected string preferredAirline = string.Empty, policybreakingrules = string.Empty;
        protected int decimalValue = 0;
        protected List<string> filteredOnwardTimings = new List<string>();
        protected List<string> filteredReturnTimings = new List<string>();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                request = Session["FlightRequest"] as SearchRequest;

                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                else
                {
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                var parentid = AgentMaster.GetParentId(Settings.LoginInfo.AgentId);
                if (parentid == 2125)
                {
                    radioSelf.Checked = false;
                    radioSelf.Visible = false;
                    radioAgent.Checked = true;

                    CT.TicketReceipt.Common.Utility.StartupScript(this, " disablefield();", "fileds");

                }
                Session["FZBaggageDetails"] = null;
                Session["TBOResult"] = null;
                Session["TBOBaggage"] = null;
                Session["TBOFare"] = null;
                Session["ISPRiceChangedTBO"] = null;
                Session["OtherCharges"] = null;
                Session["SearchResult"] = null;
                Session["ResultIndex"] = null;//Clear the Result Index session
                Session["BookingResponse"] = null;
                Session["FlightItinerary"] = null;
                Session["ValuesChanged"] = null;//Clear the values used for UAPI Repricing
                Session["Error"] = null;
                Session["RepricedResult"] = null;
                Session["PaxPageValues"] = null;

                if (Request.QueryString["error"] != null)
                {
                    airlineMessage = Request.QueryString["error"];
                    airlineMessage = airlineMessage.Replace("-", "<br/>");
                }

                if (Session["airlines"] != null)
                {
                    airlineList = Session["airlines"] as List<string>;
                }


                if (!IsPostBack)
                {
                    if (Settings.LoginInfo.IsCorporate == "Y")
                    {
                        try
                        {
                            DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightTravelReasons.DataSource = dtTravelReasons;
                            ddlFlightTravelReasons.DataTextField = "Description";
                            ddlFlightTravelReasons.DataValueField = "ReasonId";
                            ddlFlightTravelReasons.DataBind();

                            DataTable dtPolicies = TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightEmployee.DataSource = dtPolicies;
                            ddlFlightEmployee.DataTextField = "ProfileName";
                            ddlFlightEmployee.DataValueField = "Profile";
                            ddlFlightEmployee.DataBind();
                            //if (ddlFlightEmployee.Items.Count > 1)
                            //{
                            //    ddlFlightEmployee.Items.Insert(0, "Select Traveller");
                            //}

                            if (Settings.LoginInfo.IsCorporate == "Y" && Settings.LoginInfo.CorporateProfileId > 0 && request.AppliedPolicy)
                            {
                                dtTravelReason = TravelUtility.GetTravelReasonList("B", Settings.LoginInfo.AgentId, ListStatus.Short);// B - Policy Breaking Reason
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "Failed to Bind Corporate details for Flight Search. Reason :" + ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                    }

                    //Based On Bke_App_config value Restricting th Multi City Option .
                    AgentAppConfig clsAppCnf = new AgentAppConfig();
                    clsAppCnf.AgentID = Settings.LoginInfo.AgentId;
                    var liAppConfig = clsAppCnf.GetConfigData();
                    var showTimings = liAppConfig.Exists(x => x.AppKey.ToLower() == "showtimeintervals" && x.AppValue.ToLower() == "true");

                    //If ShowTimeIntervals is set to TRUE in BKE_APP_CONFIG for the login agent then clear existing values and add half hour time intervals in 24 hour format
                    if (showTimings)
                    {
                        ddlDepTime.Items.Clear();
                        ddlReturnTime.Items.Clear();

                        ddlDepTime.Items.Add(new ListItem("Any Time", "Any"));
                        ddlReturnTime.Items.Add(new ListItem("Any Time", "Any"));

                        for (int h = 0; h < 24; h++)
                        {
                            if (h > 0)
                            {
                                int hour = h, prevHour = h - 1;
                                string prevHourFormat = string.Empty, hourFormat = string.Empty;

                                if (prevHour.ToString().Length == 1)
                                    prevHourFormat = "0" + prevHour;
                                else
                                    prevHourFormat = prevHour.ToString();

                                if (hour.ToString().Length == 1)
                                    hourFormat = "0" + hour;
                                else
                                    hourFormat = hour.ToString();


                                ddlDepTime.Items.Add(new ListItem(prevHourFormat + ":30 - " + hourFormat + ":00", prevHourFormat + ":30 - " + hourFormat + ":00"));
                                ddlDepTime.Items.Add(new ListItem(hourFormat + ":00 - " + hourFormat + ":30", hourFormat + ":00 - " + hourFormat + ":30"));
                                ddlReturnTime.Items.Add(new ListItem(prevHourFormat + ":30 - " + hourFormat + ":00", prevHourFormat + ":30 - " + hourFormat + ":00"));
                                ddlReturnTime.Items.Add(new ListItem(hourFormat + ":00 - " + hourFormat + ":30", hourFormat + ":00 - " + hourFormat + ":30"));
                            }
                            else
                            {
                                ddlDepTime.Items.Add(new ListItem("00:00 - 00:30", "00:00 - 00:30"));
                                ddlReturnTime.Items.Add(new ListItem("00:00 - 00:30", "00:00 - 00:30"));
                            }
                        }

                        ddlDepTime.Items.Add(new ListItem("23:30 - 00:00", "23:30 - 00:00"));
                        ddlReturnTime.Items.Add(new ListItem("23:30 - 00:00", "23:30 - 00:00"));
                    }

                    //Load search controls after corporate controls are bound with data
                    LoadSearchControls();
                    GetSearchResults(request);
                }                
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadSearchControls()
        {

            try
            {
                DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                IEnumerable<DataRow> query = from agents in dtAgents.AsEnumerable() where agents.Field<string>("agent_corporate") != "Y" select agents;

                if (query.Count() > 0)
                {
                    DataTable dtNCAgents = query.CopyToDataTable<DataRow>();
                    DataView dv = dtNCAgents.DefaultView;
                    dv.RowFilter = "agent_Id NOT IN('" + Settings.LoginInfo.AgentId + "')";

                    ddlAirAgents.AppendDataBoundItems = true;
                    ddlAirAgents.Items.Add(new ListItem("Select Agent", "-1"));
                    ddlAirAgents.DataSource = dv.ToTable();
                    ddlAirAgents.DataTextField = "agent_name";
                    ddlAirAgents.DataValueField = "agent_id";
                    ddlAirAgents.DataBind();
                }

                roundtrip.Checked = true;
                DataTable dtFlightSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 1);
                //chkSuppliers.DataSource = dtFlightSources;
                //chkSuppliers.DataValueField = "Id";
                //chkSuppliers.DataTextField = "Name";
                //chkSuppliers.DataBind();
                foreach (DataRow row in dtFlightSources.Rows)
                {
                    if (Convert.ToBoolean(row["IsRouting"]))
                    {
                        ListItem item = new ListItem(row["Name"].ToString(), row["Id"].ToString());
                        chkSuppliers.Items.Add(item);
                    }
                }

                request = Session["FlightRequest"] as SearchRequest;
                foreach (ListItem item in chkSuppliers.Items)
                {
                    if (request.Sources != null)
                    {
                        if (request.Sources.Contains(item.Text))
                        {
                            item.Selected = true;
                        }                        
                    }
                    if (dtFlightSources.Rows.Count > request.Sources.Count)
                    {
                        chkFlightAll.Checked = false;
                    }
                }

                if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
                {
                    lblSearchSupp.Style.Add("display", "none");
                    chkSuppliers.Style.Add("display", "none");
                    chkFlightAll.Style.Add("display", "none");
                }

                //Modify search   implemented by brahmam 22.08.2016
                if (request != null)
                {
                    //Set Corporate profile controls selected with search parameters
                    if (Settings.LoginInfo.IsCorporate == "Y")
                    {
                        ddlFlightEmployee.ClearSelection();
                        foreach (ListItem item in ddlFlightEmployee.Items)
                        {
                            if (item.Value.StartsWith(request.CorporateTravelProfileId.ToString()))
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                        ddlFlightTravelReasons.ClearSelection();
                        foreach (ListItem item in ddlFlightTravelReasons.Items)
                        {
                            if (item.Value.StartsWith(request.CorporateTravelReasonId.ToString()))
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }

                    userType = Settings.LoginInfo.MemberType.ToString();

                    switch (request.Type)
                    {
                        case SearchType.Return:
                            roundtrip.Checked = true;
                            oneway.Checked = false;
                            multicity.Checked = false;
                            break;
                        case SearchType.OneWay:
                            oneway.Checked = true;
                            roundtrip.Checked = false;
                            multicity.Checked = false;
                            break;
                        case SearchType.MultiWay:
                            multicity.Checked = true;
                            roundtrip.Checked = false;
                            break;
                    }
                    radioSelf.Enabled = false;
                    radioAgent.Enabled = false;
                    rbtnlMaxStops.SelectedValue = request.MaxStops;
                    if (request.RefundableFares)
                    {
                        chkRefundFare.Checked = true;                        
                    }
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        radioAgent.Checked = true;
                        agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                        //txtAirAgents.Text = agency.Name;
                        //ddlAirAgents.Items.FindByValue(Settings.LoginInfo.OnBehalfAgentID.ToString()).Selected = true;
                        ddlAirAgents.SelectedValue = Settings.LoginInfo.OnBehalfAgentID.ToString();
                    }

                    if (request.Type != SearchType.MultiWay)
                    {
                        FlightSegment segment = request.Segments[0];

                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                        {
                            Origin.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                            break;
                        }
                        chkNearByPort.Checked = segment.NearByOriginPort;
                        foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                        {
                            Destination.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                            break;
                        }
                        IFormatProvider format = new CultureInfo("en-GB");
                        string[] depDate = segment.PreferredDepartureTime.ToString("dd-MM-yyyy").Split('-');
                        string[] arrDate = new string[0];
                        DepDate.Text = depDate[0] + "/" + depDate[1] + "/" + depDate[2];
                        string departureTime = segment.PreferredDepartureTime.ToString("HH:mm");
                        if (departureTime.Length > 0 && segment.PreferredDepartureTime.ToString("HHmm") != "0001")
                        {
                            string selectedTime = departureTime + " - " + segment.PreferredDepartureTime.AddMinutes(30).ToString("HH:mm");

                            if (request.TimeIntervalSpecified)
                                ddlDepTime.SelectedValue = ddlDepTime.Items.FindByValue(selectedTime).Value;
                            else
                            {
                                switch (departureTime.Trim())
                                {
                                    case "05:00":
                                        ddlDepTime.SelectedValue = "Morning";
                                        break;
                                    case "12:00":
                                        ddlDepTime.SelectedValue = "Afternoon";
                                        break;
                                    case "06:00":
                                        ddlDepTime.SelectedValue = "Evening";
                                        break;
                                    case "11:59":
                                        ddlDepTime.SelectedValue = "Night";
                                        break;                                   
                                }
                            }
                        }

                        if (request.Type == SearchType.Return)
                        {
                            ReturnDateTxt.Text = request.Segments[1].PreferredArrivalTime.ToString("dd/MM/yyyy");

                            string returnTime = request.Segments[1].PreferredArrivalTime.ToString("HH:mm");
                            if (returnTime.Length > 0 && request.Segments[1].PreferredArrivalTime.ToString("HHmm") != "0001")
                            {
                                string selectedTime = returnTime + " - " + request.Segments[1].PreferredArrivalTime.AddMinutes(30).ToString("HH:mm");

                                if (request.TimeIntervalSpecified)
                                    ddlReturnTime.SelectedValue = ddlReturnTime.Items.FindByValue(selectedTime).Value;
                                else
                                {
                                    switch (returnTime.Trim())
                                    {
                                        case "05:00":
                                            ddlReturnTime.SelectedValue = "Morning";
                                            break;
                                        case "12:00":
                                            ddlReturnTime.SelectedValue = "Afternoon";
                                            break;
                                        case "06:00":
                                            ddlReturnTime.SelectedValue = "Evening";
                                            break;
                                        case "11:59":
                                            ddlReturnTime.SelectedValue = "Night";
                                            break;                                      
                                    }
                                }
                            }
                        }
                        else
                        {
                            ReturnDateTxt.Text = segment.PreferredArrivalTime.ToString("dd/MM/yyyy");
                        }
                    }
                    else
                    {

                        for (int i = 0; i < request.Segments.Length; i++)
                        {
                            FlightSegment segment = request.Segments[i];
                            switch (i + 1)
                            {
                                case 1:
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                    {
                                        City1.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    chkNearByPort1.Checked = segment.NearByOriginPort;
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                    {
                                        City2.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    Time1.Text = Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy");

                                    break;
                                case 2:
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                    {
                                        City3.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    chkNearByPort2.Checked = segment.NearByOriginPort;
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                    {
                                        City4.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    Time2.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                    break;
                                case 3:
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                    {
                                        City5.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    chkNearByPort3.Checked = segment.NearByOriginPort;
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                    {
                                        City6.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    Time3.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                    break;
                                case 4:
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                    {
                                        City7.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    chkNearByPort4.Checked = segment.NearByOriginPort;
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                    {
                                        City8.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    Time4.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                    break;
                                case 5:
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                    {
                                        City9.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    chkNearByPort5.Checked = segment.NearByOriginPort;
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                    {
                                        City10.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    Time5.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                    break;
                                case 6:
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                    {
                                        City11.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    chkNearByPort6.Checked = segment.NearByOriginPort;
                                    foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                    {
                                        City12.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                        break;
                                    }
                                    Time6.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                    break;

                            }
                        }
                    }

                    ddlAdults.SelectedIndex = -1;
                    ddlAdults.Items.FindByValue(request.AdultCount.ToString()).Selected = true;

                    ddlChilds.SelectedIndex = -1;
                    ddlChilds.Items.FindByValue(request.ChildCount.ToString()).Selected = true;

                    ddlInfants.SelectedIndex = -1;
                    ddlInfants.Items.FindByValue(request.InfantCount.ToString()).Selected = true;

                    ddlBookingClass.SelectedIndex = -1;
                    ddlBookingClass.Items.FindByValue(((int)request.Segments[0].flightCabinClass).ToString()).Selected = true;

                    airlineCode.Value = string.Empty;

                    foreach (string pa in request.Segments[0].PreferredAirlines)
                    {
                        if (!string.IsNullOrEmpty(pa) && pa.Length <= 2)
                        {
                            if (preferredAirlines.Length > 0)
                            {
                                preferredAirlines += "," + pa;
                            }
                            else
                            {
                                preferredAirlines = pa;
                            }
                        }
                    }

                    if (request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        txtPreferredAirline.Text = request.Segments[0].PreferredAirlines[0];
                        //airlineName.Value = request.Segments[0].PreferredAirlines[1];
                        //airlineCode.Value = request.Segments[0].PreferredAirlines[0];
                        for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(request.Segments[0].PreferredAirlines[i]) && request.Segments[0].PreferredAirlines[i].Length <= 2)
                            {
                                if (airlineCode.Value.Length > 0)
                                {
                                    airlineCode.Value += "," + request.Segments[0].PreferredAirlines[i];
                                }
                                else
                                {
                                    airlineCode.Value = request.Segments[0].PreferredAirlines[i];
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        protected void GetSearchResults(SearchRequest request)
        {
            string sessionId = "";
            SearchResult[] result = new SearchResult[0];
            //if (request.Type == SearchType.OneWay)
            //{
            try
            {
                bool isSearchRestricted = false;
                if (request.InfantCount > 0 || (request.Type == SearchType.MultiWay && request.Segments.Length > 2) || request.RefundableFares)
                {
                    if (request.Sources.Count > 1)
                        request.Sources.Remove("PK");
                    else if (request.Sources.Contains("PK"))
                    {
                        if (request.RefundableFares)
                        {
                            request.Sources.Remove("PK");
                        }
                        else if (request.InfantCount > 0 && request.Segments.Length > 2)
                        {
                            isSearchRestricted = true;
                            lblMessage.Text = "Infants are not allowed currently in Search for PK. Only two connections/segments allowed in Search for PK. Please modify your search.";
                        }
                        else if (request.InfantCount > 0)
                        {
                            isSearchRestricted = true;
                            lblMessage.Text = "Infants are not allowed currently in Search for PK. Please modify your search.";
                        }
                        else if (request.Segments.Length > 2)
                        {
                            isSearchRestricted = true;
                            lblMessage.Text = "Only two connections/segments allowed in Search for PK. Please modify your search.";
                        }
                    }
                }

                if (isSearchRestricted)
                {
                    if (!string.IsNullOrEmpty(airlineMessage))
                    {
                        lblMessage.Text += "You are not allowed to search with these preferred airlines." + airlineMessage;
                    }
                }
                else
                {
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agency = new AgentMaster(Settings.LoginInfo.AgentId);
                    }
                    else
                    {
                        agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    }

                    DateTime inTimeSearch = DateTime.Now;
                    MetaSearchEngine mse = new MetaSearchEngine();
                    //StaticData sd = new StaticData();
                    //sd.BaseCurrency = agency.AgentCurrency;                
                    mse.SettingsLoginInfo = Settings.LoginInfo;

                    if (airlineCode.Value.Length > 0)
                    {
                        foreach (FlightSegment segment in request.Segments)
                        {
                            segment.PreferredAirlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        }
                    }

                    request.Segments[0].flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                    if (request.Type == SearchType.Return)
                    {
                        request.Segments[1].flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                    }

                    //if (Cache["FlightResult"] == null)
                    {
                        request.SearchBySegments = mse.SearchBySegments = true;
                        result = (SearchResult[])mse.GetResults(request);
                        TimeSpan totalTimeSearch = DateTime.Now.Subtract(inTimeSearch);

                        if (result != null && result.Length > 0)
                        {
                            //Cache.Insert("FlightResult", result, null, DateTime.Now.AddMinutes(10), System.Web.Caching.Cache.NoSlidingExpiration);
                        }
                        sessionId = mse.SessionId;
                        Session["sessionId"] = sessionId;
                    }

                    if (request.MaxStops == "0")//Filter Direct flights only from the results
                    {
                        List<SearchResult> Results = new List<SearchResult>();
                        foreach (SearchResult res in result)
                        {
                            if (res.Flights[0].Length == 1 || res.Flights[0][0].Status=="VIA")
                            {
                                Results.Add(res);
                            }
                        }
                        if (request.Type != SearchType.OneWay && Results.Count > 0)
                        {
                            result = Results.ToArray();
                        }

                        chkOnNonStop.Disabled = false;//disable if only Non Stop searched
                        chkRetNonStop.Disabled = false;
                        chkOnOneStop.Disabled = true;
                        chkRetOneStop.Disabled = true;
                        chkOnTwoStops.Disabled = true;
                        lblOnTwoStops.Disabled = true;
                        chkRetTwoStops.Disabled = true;
                        lblRetTwoStops.Disabled = true;
                    }
                    else if (request.MaxStops == "1")
                    {
                        chkOnOneStop.Disabled = false;//disable if only Single Stop searched
                        chkRetOneStop.Disabled = false;
                        chkOnNonStop.Disabled = true;
                        chkRetNonStop.Disabled = true;

                        chkOnTwoStops.Disabled = true;
                        lblOnTwoStops.Disabled = true;
                        chkRetTwoStops.Disabled = true;
                        lblRetTwoStops.Disabled = true;
                    }
                    else if (request.MaxStops == "2")
                    {
                        chkOnTwoStops.Disabled = false;//disable if only Two stops searched
                        lblOnTwoStops.Disabled = false;
                        chkRetTwoStops.Disabled = false;
                        lblRetTwoStops.Disabled = false;
                        chkOnNonStop.Disabled = true;
                        chkRetNonStop.Disabled = true;
                        chkOnOneStop.Disabled = true;
                        chkRetOneStop.Disabled = true;
                    }
                    else
                    {
                        chkOnTwoStops.Disabled = false;//disable if only Two stops searched
                        lblOnTwoStops.Disabled = false;
                        chkRetTwoStops.Disabled = false;
                        lblRetTwoStops.Disabled = false;
                        chkOnNonStop.Disabled = false;
                        chkRetNonStop.Disabled = false;
                        chkOnOneStop.Disabled = false;
                        chkRetOneStop.Disabled = false;
                    }
                   

                    DateTime inTime = DateTime.Now;

                    
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID;
                        decimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        decimalValue = Settings.LoginInfo.DecimalValue;
                    }                   
                    
                    
                    if (result != null && result.Length > 0)
                    {
                        
                        result.ToList().ForEach(searchResult =>
                        {
                            decimal markUp = 0, baseFare = 0, tax = 0, discount = 0;
                            searchResult.FareBreakdown.ToList().ForEach(fare =>
                            {
                                markUp += Convert.ToDecimal(fare.AgentMarkup.ToString("N" + decimalValue));
                                baseFare += Convert.ToDecimal(fare.BaseFare.ToString("N" + decimalValue)) + Convert.ToDecimal(fare.HandlingFee.ToString("N" + decimalValue));
                                tax += Convert.ToDecimal(fare.Tax.ToString("N" + decimalValue));
                                discount += Convert.ToDecimal(fare.AgentDiscount.ToString("N" + decimalValue));
                            });
                            tax += (markUp + (searchResult.Price.OtherCharges) + searchResult.Price.AdditionalTxnFee + searchResult.Price.SServiceFee + searchResult.Price.TransactionFee);
                            searchResult.TotalFare = Convert.ToDouble(baseFare + tax - discount);

                            //If VIA flight is there then update stops as ZERO. As we are using stops count to show stops 
                            //information further pages we need to correct this logic
                            if (searchResult.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0)
                                searchResult.Flights.ToList().ForEach(f => f.ToList().ForEach(s => s.Stops = 0));
                            else//Need to re assign the stops based on the stops we are displaying in the page. If any VIA flight is there need to reduce stops count
                                searchResult.Flights.ToList().ForEach(f => f.ToList().ForEach(s => s.Stops = searchResult.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1));
                        });

                        resultDisplay = result;
                        Session["Results"] = resultDisplay;                       

                        filteredResults = resultDisplay;//DoPaging(resultDisplay);
                        Session["FlightResults"] = filteredResults;

                        List<SearchResult> searchResults = new List<SearchResult>(filteredResults);
                        
                        onwardResults = searchResults.FindAll(x => x.Flights[0][0].Group == 0);
                        returnResults = searchResults.FindAll(x => x.Flights[0][0].Group == 1);

                        Audit.Add(EventType.Search, Severity.Low, 1, "Onward Results: " + onwardResults.Count, Request["REMOTE_ADDR"]);
                        Audit.Add(EventType.Search, Severity.Low, 1, "Return Results: " + returnResults.Count, Request["REMOTE_ADDR"]);

                        foreach (SearchResult res in onwardResults)
                        {
                            for (int j = 0; j < res.Flights[0].Length; j++)
                            {
                                if (request.Segments[0].PreferredAirlines.Length == 0 || request.Segments[0].PreferredAirlines.Contains(res.Flights[0][j].Airline))
                                {
                                    Airline airline = new Airline();
                                    airline.Load(res.Flights[0][j].Airline);
                                    if (!airlineList.Contains(airline.AirlineName + " (" + res.Flights[0][j].Airline + ")"))
                                    {
                                        airlineList.Add(airline.AirlineName + " (" + res.Flights[0][j].Airline + ")");
                                    }
                                }
                            }
                        }

                        foreach (SearchResult res in returnResults)
                        {   
                            for (int j = 0; j < res.Flights[0].Length; j++)
                            {
                                if (request.Segments[0].PreferredAirlines.Length == 0 || request.Segments[0].PreferredAirlines.Contains(res.Flights[0][j].Airline))
                                {
                                    Airline airline = new Airline();
                                    airline.Load(res.Flights[0][j].Airline);
                                    if (!airlineListReturn.Contains(airline.AirlineName + " (" + res.Flights[0][j].Airline + ")"))
                                    {
                                        airlineListReturn.Add(airline.AirlineName + " (" + res.Flights[0][j].Airline + ")");
                                    }
                                }
                            }
                        }

                        if (!request.RefundableFares)
                        {
                            onwardNonRefundableFound = onwardResults.Exists(x => x.NonRefundable);
                            returnNonRefundableFound = returnResults.Exists(x => x.NonRefundable);
                        }
                        onwardRefundableFound = onwardResults.Exists(x => x.NonRefundable == false);
                        returnRefundableFound = returnResults.Exists(x => x.NonRefundable == false);


                        //Check whether all stops are available or not for Onward flights

                        if (request.MaxStops == "0")
                        {
                            onwardNonStopFound = onwardResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0);
                            returnNonStopFound = returnResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0);
                            onwardResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 > 0);
                            returnResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 > 0);
                        }
                        else if (request.MaxStops == "1")
                        {
                            onwardOneStopFound = onwardResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1);
                            returnOneStopFound = returnResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1);
                            onwardResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 != 1);
                            returnResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 != 1);
                        }
                        else if (request.MaxStops == "2")
                        {
                            onwardTwoStopFound = onwardResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 2);
                            returnTwoStopFound = returnResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 2);
                            onwardResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 != 2);
                            returnResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 != 2);
                        }
                        else
                        {
                            onwardNonStopFound = onwardResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0);
                            returnNonStopFound = returnResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0);
                            onwardOneStopFound = onwardResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1);
                            returnOneStopFound = returnResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1);
                            onwardTwoStopFound = onwardResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 2);
                            if (onwardTwoStopFound == false)
                            {
                                chkOnTwoStops.Checked = false;
                                lblOnTwoStops.Disabled = false;

                            }
                            returnTwoStopFound = returnResults.Exists(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 2);
                            if (returnTwoStopFound == false)
                            {
                                chkRetTwoStops.Checked = false;
                                lblRetTwoStops.Disabled = false;

                            }

                        }

                        //Show/Hide the stops based on results




                        //Check whether all stops are available or not for Return flights


                        onwardResults.Select(x => x.Airline.Count() > 0 ? true : airlineList.Remove(x.Airline));
                        returnResults.Select(x => x.Airline.Count() > 0 ? true : airlineListReturn.Remove(x.Airline));

                        Session["OnwardResults"] = onwardResults;
                        Session["ReturnResults"] = returnResults;

                        onwardResultJson = JsonConvert.SerializeObject(onwardResults);
                        returnResultJson = JsonConvert.SerializeObject(returnResults);

                        List<Airline> Airlines = new List<Airline>();
                        DataTable dtAirlines = Airline.GetAllAirlines();
                        foreach (DataRow dr in dtAirlines.Rows)
                        {
                            Airline airline = new Airline();
                            airline.Load(dr["airlineCode"].ToString());
                            airline.CreatedOn = DateTime.Now;
                            airline.LastModifiedOn = DateTime.Now;
                            Airlines.Add(airline);
                        }
                        JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
                        airlineArray = scriptSerializer.Serialize(Airlines.ToArray());
                        
                        onwardResults.Sort((r1, r2) => r1.TotalFare.CompareTo(r2.TotalFare));
                        returnResults.Sort((r1, r2) => r1.TotalFare.CompareTo(r2.TotalFare));

                        BindFilterTimings(request);

                        //Assign Slider values for Onward results
                        if (onwardResults.Count>0)
                        {
                            hdnOnMinValue.Value = Math.Ceiling(onwardResults[0].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            if (onwardResults[0].ResultBookingSource == BookingSource.TBOAir)
                            {
                                hdnOnMinValue.Value = Math.Ceiling(onwardResults[0].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            }

                            hdnOnMaxValue.Value = Math.Ceiling(onwardResults[onwardResults.Count - 1].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            if (onwardResults[onwardResults.Count - 1].ResultBookingSource == BookingSource.TBOAir)
                            {
                                hdnOnMaxValue.Value = Math.Ceiling(onwardResults[onwardResults.Count - 1].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            }
                        }

                        hdnOnPriceMin.Value = hdnOnMinValue.Value;
                        hdnOnPriceMax.Value = hdnOnMaxValue.Value;

                        //Assign Slider values for Return results
                        if (returnResults.Count>0)
                        {
                            hdnRetMinValue.Value = Math.Ceiling(returnResults[0].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            if (returnResults[0].ResultBookingSource == BookingSource.TBOAir)
                            {
                                hdnRetMinValue.Value = Math.Ceiling(returnResults[0].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            }

                            hdnRetMaxValue.Value = Math.Ceiling(returnResults[returnResults.Count - 1].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            if (returnResults[returnResults.Count - 1].ResultBookingSource == BookingSource.TBOAir)
                            {
                                hdnRetMaxValue.Value = Math.Ceiling(returnResults[returnResults.Count - 1].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                            }
                        }

                        hdnRetPriceMin.Value = hdnRetMinValue.Value;
                        hdnRetPriceMax.Value = hdnRetMaxValue.Value;
                    }
                    else
                    {
                        //dlFlightResults.Visible = false;
                        Session["Results"] = null;
                    }


                    fromCity = new ArrayList();// = CT.BookingEngine.Airport.GetCityName(request.Segments[0].Origin);
                    toCity = new ArrayList();// CT.BookingEngine.Airport.GetCityName(request.Segments[0].Destination);

                    // puting source and destination city code in the session array for booking purpose

                    if (fromCity.Count == 1)
                    {
                        Array a = (Array)fromCity[0];
                        Session["origin"] = a.GetValue(0);
                    }
                    else
                    {
                        //Session["origin"] = lblOrigin.Text;
                    }

                    if (toCity.Count == 1)
                    {
                        Array a = (Array)toCity[0];
                        Session["destination"] = a.GetValue(0);
                    }
                    else
                    {
                        //Session["destination"] = lblDestination.Text;
                    }
                    //if (!isCalendarSearch && !changedDaySearch)
                    //{
                    //    Session["waytype"] = Request["waytype"];
                    //    Session["type"] = Request["type"];
                    //    Session["adult"] = adult;
                    //    Session["senior"] = senior;
                    //    Session["child"] = child;
                    //    Session["infant"] = infant;
                    //    Session["cabinclass"] = Convert.ToInt32(Request["bookingclass"]);
                    //}
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed searching Flights: " + ex.ToString(), Request["REMOTE_ADDR"]);
                string url = Request.ServerVariables["HTTP_REFERER"].ToString();
                //errorMessageSearchResult = "Error: There is some error. Please " + "<a href=\"" + url + "\">" + "Retry" + "</a>.";
                ////ErrorSearchRes = true;
                //ExceptionSearchRes = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchFlight_Click(object sender, EventArgs e)
        {
            airlineMessage = string.Empty;
            //Clearing old filtered results
            Session["FilteredAirResults"] = null;
            Session["layoverAirports"] = null;
            Session["fromcityAirports"] = null;
            Session["tocityAirports"] = null;
            Session["airlines"] = null;

            //Prepare SearchRequest object 
            try
            {
                List<Airline> restrictedAirlines = new List<Airline>();
                if (Settings.LoginInfo.IsCorporate == "Y")
                {
                    string[] profId = ddlFlightEmployee.SelectedValue.Split('~');
                    if (profId.Length > 0)
                    {

                        Settings.LoginInfo.CorporateProfileGrade = profId[1];
                        searchRequest.CorporateTravelProfileId = Convert.ToInt32(profId[0]);
                        //Settings.LoginInfo.CorporateProfileId = Convert.ToInt32(profId[0]);// Overriding Profile Id from TRaveller Lilst

                        //searchRequest.CorporateTravelReason = Convert.ToInt32(profId[0]);
                        if (ddlFlightTravelReasons.SelectedValue.Contains("~"))
                        {
                            searchRequest.CorporateTravelReasonId = Convert.ToInt32(ddlFlightTravelReasons.SelectedValue.Split('~')[0]);
                            searchRequest.AppliedPolicy = (ddlFlightTravelReasons.SelectedValue.Split('~')[1] == "P" ? true : false);
                        }
                    }
                }

                agencyId = Settings.LoginInfo.AgentId;
                hdnModifySearch.Value = "0";
                if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
                {
                    if (hdnBookingAgent.Value == "Checked" && Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        // ziyad to check
                        //Settings.LoginInfo.AgentId = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
                        Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);

                        Settings.LoginInfo.IsOnBehalfOfAgent = true;
                        AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                        Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                        Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                        StaticData sd = new StaticData();
                        sd.BaseCurrency = agent.AgentCurrency;
                        Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                        Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);

                        Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                    }
                    else
                    {
                        Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    }
                }
                //Assign the changed OnBehalf Agent Location from the dropdown
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
                }
                if (hdnWayType.Value != "multiway")
                {
                    searchRequest.SearchBySegments = true;
                    searchRequest.AdultCount = Convert.ToInt32(ddlAdults.SelectedValue);
                    searchRequest.ChildCount = Convert.ToInt32(ddlChilds.SelectedValue);
                    searchRequest.InfantCount = Convert.ToInt32(ddlInfants.SelectedValue);
                    searchRequest.RestrictAirline = false;
                    searchRequest.Segments = new FlightSegment[2];
                    FlightSegment fwdSegment = new FlightSegment();
                    FlightSegment retSegment = new FlightSegment();

                    //fwdSegment.Origin = Origin.Text.Split(')')[0].Replace("(", "");
                    //fwdSegment.Destination = Destination.Text.Split(')')[0].Replace("(", "");
                    fwdSegment.Origin = GetCityCode(Origin.Text.Trim());
                    fwdSegment.NearByOriginPort = chkNearByPort.Checked;
                    fwdSegment.Destination = GetCityCode(Destination.Text.Trim());



                    string departureDate = DepDate.Text;

                    switch (ddlDepTime.SelectedValue)
                    {
                        //case "Morning":
                        //    departureDate += " 08:00 AM";
                        //    break;
                        //case "Afternoon":
                        //    departureDate += " 02:00 PM";
                        //    break;
                        //case "Evening":
                        //    departureDate += " 07:00 PM";
                        //    break;
                        //case "Night":
                        //    departureDate += " 01:00 AM";
                        //    break;
                        //default:
                        //    break;
                        case "Morning":
                            departureDate += " 05:00 AM";
                            break;
                        case "Afternoon":
                            departureDate += " 12:00 PM";
                            break;
                        case "Evening":
                            departureDate += " 06:00 PM";
                            break;
                        case "Night":
                            departureDate += " 11:59:59 PM";
                            break;
                        default:
                            if (ddlDepTime.SelectedValue.Contains("-"))
                            {
                                departureDate += " " + ddlDepTime.SelectedValue.Split('-')[0];
                                searchRequest.TimeIntervalSpecified = true;
                            }
                            else
                                departureDate += " 00:01:00";
                            break;

                    }

                    fwdSegment.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);

                    IFormatProvider format = new CultureInfo("en-GB", true);
                    fwdSegment.PreferredDepartureTime = Convert.ToDateTime(departureDate, format);
                    fwdSegment.PreferredArrivalTime = Convert.ToDateTime(departureDate, format);

                    Airline restrictedAirline = new Airline();


                    if (airlineCode.Value.Length > 0)
                    {
                        searchRequest.RestrictAirline = true;
                        //string[] airlines = new string[2];
                        //airlines[1] = airlineName.Value;
                        //airlines[0] = airlineCode.Value;
                        //fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                        if (airlineCode.Value.Split(',').Length == 1)
                        {
                            string[] airlines = new string[1];
                            //airlines[1] = airlineName.Value;
                            airlines[0] = airlineCode.Value;
                            restrictedAirline.Load(airlineCode.Value);
                            restrictedAirlines.Add(restrictedAirline);
                            fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                        }
                        else
                        {
                            string[] airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            for (int i = 0; i < airlines.Length; i++)
                            {
                                Airline airline = new Airline();
                                airline.Load(airlines[i]);
                                restrictedAirlines.Add(airline);
                            }
                            fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                        }
                    }
                    else
                    {
                        fwdSegment.PreferredAirlines = new string[0];
                    }

                    searchRequest.Segments[0] = fwdSegment;
                    if (hdnWayType.Value == "return")
                    {
                        retSegment.Origin = fwdSegment.Destination;
                        retSegment.NearByOriginPort = fwdSegment.NearByOriginPort;
                        retSegment.Destination = fwdSegment.Origin;
                        retSegment.flightCabinClass = fwdSegment.flightCabinClass;

                        string ReturnDateTime = ReturnDateTxt.Text;

                        switch (ddlReturnTime.SelectedValue)
                        {
                            //case "Morning":
                            //    ReturnDateTime += " 08:00 AM";
                            //    break;
                            //case "Afternoon":
                            //    ReturnDateTime += " 02:00 PM";
                            //    break;
                            //case "Evening":
                            //    ReturnDateTime += " 07:00 PM";
                            //    break;
                            //case "Night":
                            //    ReturnDateTime += " 01:00 AM";
                            //    break;
                            //default:
                            //    break;
                            case "Morning":
                                ReturnDateTime += " 05:00 AM";
                                break;
                            case "Afternoon":
                                ReturnDateTime += " 12:00 PM";
                                break;
                            case "Evening":
                                ReturnDateTime += " 06:00 PM";
                                break;
                            case "Night":
                                ReturnDateTime += " 11:59:59 PM";
                                break;
                            default:
                                if (ddlReturnTime.SelectedValue.Contains("-"))
                                {
                                    ReturnDateTime += " " + ddlReturnTime.SelectedValue.Split('-')[0];
                                    searchRequest.TimeIntervalSpecified = true;
                                }
                                else
                                    ReturnDateTime += " 00:01:00";
                                break;
                        }
                        if (ReturnDateTime.Length > 0)
                        {
                            retSegment.PreferredArrivalTime = Convert.ToDateTime(ReturnDateTime, format);
                            retSegment.PreferredDepartureTime = Convert.ToDateTime(ReturnDateTime, format);
                        }
                        else
                        {
                            retSegment.PreferredArrivalTime = fwdSegment.PreferredArrivalTime.AddDays(1);
                            retSegment.PreferredDepartureTime = fwdSegment.PreferredDepartureTime.AddDays(1);
                        }

                        if (airlineCode.Value.Length > 0)
                        {
                            searchRequest.RestrictAirline = true;
                            //string[] airlines = new string[2];
                            //airlines[1] = airlineName.Value;
                            //airlines[0] = airlineCode.Value;
                            //retSegment.PreferredAirlines = airlines;
                            if (airlineCode.Value.Split(',').Length == 1)
                            {
                                string[] airlines = new string[1];
                                //airlines[1] = airlineName.Value;
                                airlines[0] = airlineCode.Value;
                                retSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                            }
                            else
                            {
                                string[] airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                retSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                            }
                        }
                        else
                        {
                            retSegment.PreferredAirlines = new string[0];
                        }

                        searchRequest.Segments[1] = retSegment;
                        searchRequest.Type = SearchType.Return;
                    }
                    else
                    {
                        searchRequest.Segments[1] = new FlightSegment();
                        searchRequest.Type = SearchType.OneWay;
                    }
                    List<string> sources = new List<string>();
                    foreach (ListItem item in chkSuppliers.Items)
                    {
                        if (item.Selected)
                        {
                            sources.Add(item.Text);
                        }
                    }


                    //if (restrictedAirline.AirlineName != null && restrictedAirline.AirlineName.Length > 0)
                    //{
                    //    if (!restrictedAirline.TBOAirAllowed && restrictedAirlines.Count == 0)
                    //    {
                    //        if (sources.Count == 1)
                    //        {
                    //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                    //        }
                    //        //if (sources.Contains("TA"))
                    //        //{
                    //        //    sources.Remove("TA");
                    //        //}
                    //    }
                    //    if (!restrictedAirline.UapiAllowed && restrictedAirlines.Count == 0)
                    //    {
                    //        if (sources.Count == 1)
                    //        {
                    //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                    //        }
                    //        if (sources.Remove("UA"))
                    //        {
                    //            sources.Remove("UA");
                    //        }
                    //    }
                    //}

                    foreach (Airline ra in restrictedAirlines)
                    {
                        if (!ra.TBOAirAllowed && sources.Exists(delegate (string s) { return s.ToUpper() == "TA"; }))
                        {
                            if (!airlineMessage.Contains(ra.AirlineName))
                            {
                                if (airlineMessage.Length > 0)
                                {
                                    airlineMessage += "," + ra.AirlineName + " for TA";
                                }
                                else
                                {
                                    airlineMessage = ra.AirlineName + " for TA";
                                }
                            }
                            //if (sources.Contains("TA"))
                            //{
                            //    sources.Remove("TA");
                            //}
                        }
                        if (!ra.UapiAllowed && sources.Exists(delegate (string s) { return s.ToUpper() == "UA"; }))
                        {
                            if (!airlineMessage.Contains(ra.AirlineName))
                            {
                                if (airlineMessage.Length > 0)
                                {
                                    airlineMessage += "," + ra.AirlineName + " for UA";
                                }
                                else
                                {
                                    airlineMessage = ra.AirlineName + " for UA";
                                }
                            }
                            //if (sources.Remove("UA"))
                            //{
                            //    sources.Remove("UA");
                            //}
                        }
                    }

                    if (airlineCode.Value.Length > 0)
                    {
                        if (!airlineCode.Value.Contains("G9") && sources.Contains("G9"))
                        {
                            sources.Remove("G9");
                        }
                        if (!airlineCode.Value.Contains("FZ") && sources.Contains("FZ"))
                        {
                            sources.Remove("FZ");
                        }
                        if (!airlineCode.Value.Contains("SG") && sources.Contains("SG"))
                        {
                            sources.Remove("SG");
                        }
                        if (!airlineCode.Value.Contains("6E") && sources.Contains("6E"))
                        {
                            sources.Remove("6E");
                        }
                        if (!airlineCode.Value.Contains("IX") && sources.Contains("IX"))
                        {
                            sources.Remove("IX");
                        }
                        if (airlineCode.Value.Contains("FZ") && sources.Contains("UA"))
                        {
                            sources.Remove("UA");//Remove UAPI if preferred airline is Fly Dubai.
                        }
                        //Remove G8 if preferred airline is not Go Air
                        if (!airlineCode.Value.Contains("G8") && sources.Contains("G8"))
                        {
                            sources.Remove("G8");
                        }
                    }
                    //sources.Add("G9");
                    //sources.Add("UA");
                    searchRequest.Sources = sources;
                    if (rbtnlMaxStops.SelectedItem.Value != "-1")
                    {
                        searchRequest.MaxStops = rbtnlMaxStops.SelectedItem.Value;
                    }
                    if (chkRefundFare.Checked)
                    {
                        searchRequest.RefundableFares = true;
                    }


                    Session["FlightRequest"] = searchRequest;
                }
                else
                {
                    searchRequest.Type = SearchType.MultiWay;

                    int adults = Convert.ToInt32(ddlAdults.SelectedValue);
                    int childs = Convert.ToInt32(ddlChilds.SelectedValue);
                    int infants = Convert.ToInt32(ddlInfants.SelectedValue);

                    searchRequest.AdultCount = adults;
                    searchRequest.ChildCount = childs;
                    searchRequest.InfantCount = infants;

                    int segmentCount = 2;

                    //string city1 = Request["city1"];
                    //string city2 = Request["city2"];
                    //string city3 = Request["city3"];
                    //string city4 = Request["city4"];

                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                    int counter = 5;
                    List<string> Cities = new List<string>();
                    for (int i = counter; i < 13; i++)
                    {
                        if (Request["ctl00$cphTransaction$City" + i].Length > 0 && Request["ctl00$cphTransaction$City" + (i + 1)].Length > 0)
                        {
                            segmentCount++;
                            Cities.Add(Request["ctl00$cphTransaction$City" + i]);
                            Cities.Add(Request["ctl00$cphTransaction$City" + (i + 1)]);
                        }
                        i++;
                    }

                    searchRequest.Segments = new FlightSegment[segmentCount];

                    FlightSegment segment1 = new FlightSegment();
                    segment1.Origin = GetCityCode(City1.Text);
                    segment1.NearByOriginPort = chkNearByPort1.Checked;
                    segment1.Destination = GetCityCode(City2.Text);
                    segment1.PreferredArrivalTime = Convert.ToDateTime(Time1.Text, dateFormat);
                    segment1.PreferredDepartureTime = Convert.ToDateTime(Time1.Text, dateFormat);
                    segment1.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                    searchRequest.Segments[0] = segment1;

                    FlightSegment segment2 = new FlightSegment();
                    segment2.Origin = GetCityCode(City3.Text);
                    segment2.NearByOriginPort = chkNearByPort2.Checked;
                    segment2.Destination = GetCityCode(City4.Text);
                    segment2.PreferredArrivalTime = Convert.ToDateTime(Time2.Text, dateFormat);
                    segment2.PreferredDepartureTime = Convert.ToDateTime(Time2.Text, dateFormat);
                    segment2.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                    searchRequest.Segments[1] = segment2;

                    int timeCounter = 3;
                    counter = 0;
                    if (Cities.Count > 0)
                    {
                        while (counter < Cities.Count)
                        {
                            FlightSegment segment = new FlightSegment();
                            segment.Origin = GetCityCode(Cities[counter]);
                            segment.NearByOriginPort = !string.IsNullOrEmpty(Request["ctl00$cphTransaction$chkNearByPort" + timeCounter]);
                            counter++;
                            segment.Destination = GetCityCode(Cities[counter]);
                            segment.PreferredArrivalTime = Convert.ToDateTime(Request["ctl00$cphTransaction$Time" + timeCounter], dateFormat);
                            segment.PreferredDepartureTime = Convert.ToDateTime(Request["ctl00$cphTransaction$Time" + timeCounter], dateFormat);
                            segment.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                            searchRequest.Segments[timeCounter - 1] = segment;

                            timeCounter++;
                            counter++;
                        }
                    }

                    //if (city7.Length > 0 && city8.Length > 0)
                    //{
                    //    FlightSegment segment4 = new FlightSegment();
                    //    segment4.Origin = GetCityCode(city7);
                    //    segment4.Destination = GetCityCode(city8);
                    //    segment4.PreferredArrivalTime = Convert.ToDateTime(Request["t4"], dateFormat);
                    //    segment4.PreferredDepartureTime = Convert.ToDateTime(Request["t4"], dateFormat);

                    //    searchRequest.Segments[3] = segment4;
                    //}

                    //if (city9.Length > 0 && city10.Length > 0)
                    //{
                    //    FlightSegment segment5 = new FlightSegment();
                    //    segment5.Origin = GetCityCode(city9);
                    //    segment5.Destination = GetCityCode(city10);
                    //    segment5.PreferredArrivalTime = Convert.ToDateTime(Request["t5"], dateFormat);
                    //    segment5.PreferredDepartureTime = Convert.ToDateTime(Request["t5"], dateFormat);

                    //    searchRequest.Segments[4] = segment5;
                    //}

                    //if (city11.Length > 0 && city12.Length > 0)
                    //{
                    //    FlightSegment segment6 = new FlightSegment();
                    //    segment6.Origin = GetCityCode(city11);
                    //    segment6.Destination = GetCityCode(city12);
                    //    segment6.PreferredArrivalTime = Convert.ToDateTime(Request["t6"], dateFormat);
                    //    segment6.PreferredDepartureTime = Convert.ToDateTime(Request["t6"], dateFormat);

                    //    searchRequest.Segments[5] = segment6;
                    //}
                    Airline restrictedAirline = new Airline();


                    if (airlineCode.Value.Length > 0)
                    {
                        searchRequest.RestrictAirline = true;
                        string[] airlines = new string[0];
                        if (airlineCode.Value.Length == 1)
                        {
                            airlines = new string[2];
                            airlines[1] = airlineName.Value;
                            airlines[0] = airlineCode.Value;
                            restrictedAirline.Load(airlineCode.Value);
                            restrictedAirlines.Add(restrictedAirline);
                        }
                        else
                        {
                            airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string code in airlines)
                            {
                                Airline airline = new Airline();
                                airline.Load(code);
                                restrictedAirlines.Add(airline);
                            }
                        }

                        searchRequest.Segments[0].PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                        searchRequest.Segments[0].PreferredAirlines = airlines;
                        if (searchRequest.Segments.Length > 2)
                        {
                            searchRequest.Segments[2].PreferredAirlines = airlines;
                        }
                        if (searchRequest.Segments.Length > 3)
                        {
                            searchRequest.Segments[3].PreferredAirlines = airlines;
                        }
                        if (searchRequest.Segments.Length > 4)
                        {
                            searchRequest.Segments[4].PreferredAirlines = airlines;
                        }
                        if (searchRequest.Segments.Length > 5)
                        {
                            searchRequest.Segments[5].PreferredAirlines = airlines;
                        }
                    }
                    else
                    {
                        searchRequest.Segments[0].PreferredAirlines = new string[0];
                        searchRequest.Segments[1].PreferredAirlines = new string[0];
                        if (searchRequest.Segments.Length > 2)
                        {
                            searchRequest.Segments[2].PreferredAirlines = new string[0];
                        }
                        if (searchRequest.Segments.Length > 3)
                        {
                            searchRequest.Segments[3].PreferredAirlines = new string[0];
                        }
                        if (searchRequest.Segments.Length > 4)
                        {
                            searchRequest.Segments[4].PreferredAirlines = new string[0];
                        }
                        if (searchRequest.Segments.Length > 5)
                        {
                            searchRequest.Segments[5].PreferredAirlines = new string[0];
                        }
                    }

                    List<string> sources = new List<string>();
                    foreach (ListItem item in chkSuppliers.Items)
                    {
                        if (item.Selected)
                        {
                            sources.Add(item.Text);
                        }
                    }

                    //if (restrictedAirline.AirlineName != null && restrictedAirline.AirlineName.Length > 0)
                    //{
                    //    if (!restrictedAirline.TBOAirAllowed && restrictedAirlines.Count == 0)
                    //    {
                    //        if (sources.Count == 1)
                    //        {
                    //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                    //        }
                    //        if (sources.Contains("TA"))
                    //        {
                    //            sources.Remove("TA");
                    //        }
                    //    }
                    //    if (!restrictedAirline.UapiAllowed && restrictedAirlines.Count == 0)
                    //    {
                    //        if (sources.Count == 1)
                    //        {
                    //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                    //        }
                    //        if (sources.Remove("UA"))
                    //        {
                    //            sources.Remove("UA");
                    //        }
                    //    }
                    //}

                    foreach (Airline ra in restrictedAirlines)
                    {
                        if (!ra.TBOAirAllowed)
                        {
                            if (!airlineMessage.Contains(ra.AirlineName))
                            {
                                if (airlineMessage.Length > 0)
                                {
                                    airlineMessage += "," + ra.AirlineName;
                                }
                                else
                                {
                                    airlineMessage = ra.AirlineName;
                                }
                            }
                            //if (sources.Contains("TA"))
                            //{
                            //    sources.Remove("TA");
                            //}
                        }
                        if (!ra.UapiAllowed)
                        {
                            if (!airlineMessage.Contains(ra.AirlineName))
                            {
                                if (airlineMessage.Length > 0)
                                {
                                    airlineMessage += "," + ra.AirlineName;
                                }
                                else
                                {
                                    airlineMessage = ra.AirlineName;
                                }
                            }
                            //if (sources.Remove("UA"))
                            //{
                            //    sources.Remove("UA");
                            //}
                        }
                    }
                    if (airlineCode.Value.Length > 0)
                    {
                        if (!airlineCode.Value.Contains("G9") && sources.Contains("G9"))
                        {
                            sources.Remove("G9");
                        }
                        if (!airlineCode.Value.Contains("FZ") && sources.Contains("FZ"))
                        {
                            sources.Remove("FZ");
                        }
                        if (!airlineCode.Value.Contains("SG") && sources.Contains("SG"))
                        {
                            sources.Remove("SG");
                        }
                        if (airlineCode.Value.Contains("FZ") && sources.Contains("UA"))
                        {
                            sources.Remove("UA");//Remove UAPI if preferred airline is Fly Dubai.
                        }
                    }
                    //sources.Add("G9");
                    //sources.Add("UA");
                    searchRequest.Sources = sources;
                    if (rbtnlMaxStops.SelectedItem.Value != "-1")
                    {
                        searchRequest.MaxStops = rbtnlMaxStops.SelectedItem.Value;
                    }
                    if (chkRefundFare.Checked)
                    {
                        searchRequest.RefundableFares = true;
                    }


                    Session["FlightRequest"] = searchRequest;
                }

                bool isSearchRestricted = false;
                int uapiRestrict = 0, tboRestrict = 0;
                if (restrictedAirlines.Count > 0)
                {
                    if (restrictedAirlines.Count == airlineMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length)
                    {
                        isSearchRestricted = true;
                    }

                    uapiRestrict = restrictedAirlines.FindAll(delegate (Airline a) { return !a.UapiAllowed; }).Count;
                    tboRestrict = restrictedAirlines.FindAll(delegate (Airline a) { return !a.TBOAirAllowed; }).Count;

                    if (restrictedAirlines.Count == uapiRestrict)
                    {
                        if (searchRequest.Sources.Contains("UA"))
                        {
                            searchRequest.Sources.Remove("UA");
                        }
                    }

                    if (restrictedAirlines.Count == tboRestrict)
                    {
                        if (searchRequest.Sources.Contains("TA"))
                        {
                            searchRequest.Sources.Remove("TA");
                        }
                    }
                }

                if (searchRequest.InfantCount > 0 || (searchRequest.Type == SearchType.MultiWay && searchRequest.Segments.Length > 2) || searchRequest.RefundableFares)
                {
                    if (searchRequest.Sources.Count > 1)
                        searchRequest.Sources.Remove("PK");
                    else if (searchRequest.Sources.Contains("PK"))
                    {
                        if (searchRequest.InfantCount > 0 && searchRequest.Segments.Length > 2)
                        {
                            isSearchRestricted = true;
                            lblMessage.Text = "Infants are not allowed currently in Search for PK. Only two connections/segments allowed in Search for PK. Please modify your search.";
                        }
                        else if (searchRequest.InfantCount > 0)
                        {
                            isSearchRestricted = true;
                            lblMessage.Text = "Infants are not allowed currently in Search for PK. Please modify your search.";
                        }
                        else if (searchRequest.Segments.Length > 2)
                        {
                            isSearchRestricted = true;
                            lblMessage.Text = "Only two connections/segments allowed in Search for PK. Please modify your search.";
                        }
                    }
                }

                if (isSearchRestricted)
                {
                    if (!string.IsNullOrEmpty(airlineMessage))
                    {
                        lblMessage.Text += "You are not allowed to search with these preferred airlines." + airlineMessage;
                    }
                }
                else
                {
                    Response.Redirect("FlightResultsBySegments.aspx");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Airline, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchCity"></param>
        /// <returns></returns>
        private string GetCityCode(string searchCity) // For Flight
        {
            string cityCode = "";

            cityCode = searchCity.Split(',')[0];

            if (cityCode.Contains("(") && cityCode.Contains(")") && cityCode.Length > 3)
            {
                cityCode = cityCode.Substring(1, 3);
            }
            return cityCode;
        }

        public static string SortResults(string sortType, string journeyType)
        {
            string resultJson = string.Empty;



            return resultJson;
        }

        void BindFilterTimings(SearchRequest request)
        {
            
            if (request.TimeIntervalSpecified)
            {
                ddlOnTimings.Items.Clear();
                ddlRetTimings.Items.Clear();
                ddlOnTimings.Items.Add(new ListItem("Any Time", "Any Time"));
                ddlRetTimings.Items.Add(new ListItem("Any Time", "Any Time"));                
                
                onwardResults.ToList().ForEach(x =>
                {
                    if (!filteredOnwardTimings.Contains(x.Flights[0][0].DepartureTime.ToString("HH:mm")))
                        filteredOnwardTimings.Add(x.Flights[0][0].DepartureTime.ToString("HH:mm"));                    
                });

                if (request.Type == SearchType.Return)
                {
                    returnResults.ForEach(x =>
                    {
                        if (!filteredReturnTimings.Contains(x.Flights[0][0].DepartureTime.ToString("HH:mm")))
                            filteredReturnTimings.Add(x.Flights[0][0].DepartureTime.ToString("HH:mm"));
                    });
                }
                filteredOnwardTimings.Sort();
                filteredReturnTimings.Sort();
                filteredOnwardTimings.ForEach(x => { ListItem item = new ListItem(x, x); ddlOnTimings.Items.Add(item); });
                if (request.Type == SearchType.Return)
                    filteredReturnTimings.ForEach(x => { ListItem item = new ListItem(x, x); ddlRetTimings.Items.Add(item); });
            }
        }
    }
}
