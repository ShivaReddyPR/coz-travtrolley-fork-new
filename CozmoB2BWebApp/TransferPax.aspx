﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TransactionBE.master" CodeBehind="TransferPax.aspx.cs" Inherits="CozmoB2BWebApp.TransferPax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
     <input type="hidden" id="hdnUserId" runat="server" />
        <input type="hidden" id="hdnAgentId" runat="server" />
        <input type="hidden" id="hdnBehalfLocation" runat="server" />        
       <input type="hidden" id="hdnSession" runat="server" />
        <input type="hidden" id="hdndecimal" runat="server" />
        <input type="hidden" id="hdnGuestInfo" runat="server" />
     <input type="hidden" id="hdnAgentType" runat="server" />
        <input type="hidden" id="hdnCorpInfo" runat="server" />
    <div class="ui-pax-details-page pt-3">

	<div class="row custom-gutter">
		<div class="col-md-8">
		   <h3 class="pt-0 pb-3">Passenger details</h3>
		   <div class="ui-form-pax-details mb-3">
<!--Room 1 Starts-->
			<div class="ui-bg-wrapper mb-1  ui-room-wrapper">			
				<%--<div class="row custom-gutter">
					<div class="col-md-12"> 
						<span class="font-weight-bold label-title"> ADULT 1 </span> 
					</div>
				</div>--%>
				<div class="row custom-gutter">
					<div class="form-group col-md-4">
						<label>Title</label>
						<select class="form-control custom-select">
							<option value="Mr" selected="selected">Mr</option>
							<option value="Mrs">Mrs</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label>First Name</label>
						<input type="text" class="form-control" placeholder="First Name" id="PassFirstName" onkeypress="return allowAlphabets(event);"/>
					</div>
					<div class="form-group col-md-4">
						<label>Last Name</label>
						<input type="text" class="form-control" placeholder="Last Name" id="PassLastName" onkeypress="return allowAlphabets(event);"/>
					</div>
                    <%--<div class="form-group col-md-4">
						<label>Country Code</label>                        
						<input type="text" class="form-control" onkeypress="return allowNumerics(event);" placeholder="Country Code" id="CountryCode" maxlength="3"/>
					</div>--%>
					<div class="form-group col-md-4">
						<label>Mobile</label>
                        <div class="clear"></div>
						<input type="text" class="form-control" onkeypress="return allowNumerics(event);" placeholder="Mobile Number" id="PassMobile"/>
					</div>
                    <div class="form-group col-md-4">
						<label>Email</label>
						<input type="email" class="form-control" onkeypress="" placeholder="Email" id="PassEmail"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<hr class="dotted" />
					</div>
				</div>
					
			</div>
<!--END:Room 1 -->
		
			<div class="ui-bg-wrapper mb-1">
	
			<div class="row custom-gutter">
				<div class="form-group col-md-6">
					<label>Pick-up sign</label>
					<input type="text" class="form-control" placeholder="Pick-up sign" id="PicUpPoint"/>
				</div>
				<div class="form-group col-md-6">
					<label>Message to driver</label>
					<textarea class="form-control" rows="2" placeholder="Message to driver" id="MessageToDriver"></textarea>
				</div>
			</div>
			   </div>
			   <div class="ui-bg-wrapper mb-1" style="min-height:267px;">
			<div class="row ui-htldtl-pmt-option">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4">
			<span class="heading-normal text-gray-light mb-3 d-block"> Pay by </span>
						</div>
						<div class="col-md-8">
							<div class="custom-radio-lg mb-3">
								<input type="radio" name="customRadio" checked="checked">
								<label></label>
								<div class="text">Account <span class="ac-balance primary-color"><em class="currency" id="agentCurrencyText"></em><span id="accBalance"></span></span></div>
							</div>
							<div class="custom-radio-lg d-none">
								<input type="radio" name="customRadio">
								<label></label>
								<span class="text">Credit / Debit Card</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 pb-2 d-none">
                  

							<div class="row payment-icons-manual">
								<div class="col-md-12">
									<div class="input-group py-2"> <span class="input-group-addon pr-5"><span class="icon-credit-card-alt"></span></span>
										<input type="text" class="form-control" placeholder="Card Number">
									</div>
								</div>
								<div class="col-md-12">
									<div class="input-group mt-2 py-2"> <span class="input-group-addon pr-5"><span class="icon-calendar"></span></span>
										<input type="text" class="form-control" placeholder="MM/YY">
									</div>
								</div>
								<div class="col-md-12">
									<div class="input-group mt-2 py-2"> <span class="input-group-addon pr-5">                                 

                                	 <span class="icon icon-check-circle"></span>                          
                                    
                                    </span>
										<input type="text" class="form-control" placeholder="CVV">
									</div>
								</div>
								<div class="col-md-12">
									<div class="input-group mt-2"> <span class="input-group-addon pr-5"><span class="icon-user"></span></span>
										<input type="text" class="form-control" placeholder="Cardholder Name">
									</div>
								</div>

                                
                               
							</div>
						</div>
                 <div class="col-md-12 mt-4">
	                                <div class="alert alert-danger small p-3" role="alert" id="ChkConfirmDiv" style="display:none">
		                                <div class="chk_confirm_msg chk_err_msg py-1 font-weight-bold">Please check the Terms checkbox to continue</div>
		                                <div class="chk_addfee_msg chk_err_msg py-1 font-weight-bold">Please check Additional Fee</div>
	                                </div>
	                                <div>
		                                <input type="checkbox" id="ChkConfirm"><label id="checkTerms"><a href="#" id="showTerms">I accept conditions of transport, &amp; Terms &amp; Conditions</a></label> 
	                                </div>
	              
	                                <%--<div class="my-3">
		                                <label id="lblSupplierPerformance" style="text-transform: none;"><b>Supplier Performance: </b> I hereby acknowledge and consent that due to the nature of the platform and its connectivity to 3rd party service providers, some processes offered by the 3rd party providers are not under Travtrolley's control. Furthermore, I accept that it is my responsibility upon signing up with 3rd party service providers to ensure their flows and processes are in compliance with my company's operational requirements.</label>
	                                </div>--%>
                                </div>
                <div class="col-md-12 mt-4">
                    Select Payment Mode :
                    <select style="width: 140px;" class=""  id="ddlPaymentType">
                        <option value="Credit">On Account</option>                                                                   
                        <option value="Card">Credit Card</option>                                                                   
                    </select>
                </div>

				<div class="col-md-12 mt-3">
                    <b class="pull-left"><span>Last cancellation date</span> <span id="lastCancelDate"></span></b>
					<button type="button" class="btn btn-primary btn-lg float-right" id="conformBooking">Confirm</button>
				</div>


			</div>
			</div>
			
			
			</div>
		</div>
		<div class="col-md-4">
	<div class="row">
	<div class="col-md-12">
	
		   <h5 class="pt-3 pb-3 small-heading">YOUR BOOKING</h5>
				<div class="ui-bg-wrapper">
					<div class="row">

						<div class="col-md-12"> <h4 class="pb-0" id="BookedClass"> Economy MPV </h4> 	</div>
						<div class="col-md-12"> <small class="text-address pb-3" id="Type"> limousine </small> </div>
						<div class="col-md-12"> <small class="text-address pb-3" id="VehicleName"> VW Touran  </small> </div>

				
						<div class="col-md-12 mt-2">
							<div class="row hotel-bookingdtl-dateinfo">
								<div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-location"></i> From</span> 
									<span class="float-right text-gray-dark text-weight-400" id="FromLocation">  </span> 
								</div>
								<div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-location"></i> To</span>	
									<span class="float-right text-gray-dark text-weight-400" id="ToLocation"> </span> 
								</div>
								<div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-calendar"></i> Date</span>
									<span class="float-right text-gray-dark text-weight-400" id="TransferDate"></span> 
								</div>
								<div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-clock"></i> Pickup Time</span>
									<span class="float-right text-gray-dark text-weight-400" id="TransferTime"></span> 
								</div>
								<div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-map-marked-alt"></i> Distance</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotaDistance"></span> 
								</div>
								
								<div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-group"></i> Passengers</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotalPassenger"></span> 
								</div>
								<div class="col-md-12 luggageDiv"> 
									<span class="float-left text-gray-light"><i class="icon icon-baggage" ></i> Luggage</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotalLuggage"></span> 
								</div>
                                <div class="col-md-12 animalDiv"> 
									<span class="float-left text-gray-light"><i class="icon icon-dog" ></i> Animal</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotalAnimals"></span> 
								</div>
                                <div class="col-md-12 sportsDiv"> 
									<span class="float-left text-gray-light"><i class="icon icon-baggage" ></i> Sports Luggage</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotalSports"></span> 
								</div>
                                <div class="col-md-12 child1Div"> 
									<span class="float-left text-gray-light"><i class="icon icon-group" ></i> Child (3-6 years (15-25kg))</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotalChild1"></span> 
								</div>
                                <div class="col-md-12 child2Div"> 
									<span class="float-left text-gray-light"><i class="icon icon-group" ></i> Child (6-12 years (22-36kg)</span>
									<span class="float-right text-gray-dark text-weight-400" id="TotalChild2"></span> 
								</div>
                                <div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-group" ></i> Start Time</span>
									<span class="float-right text-gray-dark text-weight-400" id="StartTime"></span> 
								</div>
                                <div class="col-md-12"> 
									<span class="float-left text-gray-light"><i class="icon icon-group" ></i> End Time</span>
									<span class="float-right text-gray-dark text-weight-400" id="EndTime"></span> 
								</div>
                                <div class="col-md-12" id="flightNumberDiv"> 
									<span class="float-left text-gray-light"><i class="icon icon-group" ></i> Flight Number</span>
									<span class="float-right text-gray-dark text-weight-400" id="flightNumber"></span> 
								</div>
                                <div class="col-md-12" id="departureCityDiv"> 
									<span class="float-left text-gray-light"><i class="icon icon-group" ></i> Departure City</span>
									<span class="float-right text-gray-dark text-weight-400" id="departureCity"></span> 
								</div>
							</div>
						</div>
						
					</div>
				</div>
			
				
		   <h5 class="mt-3 pt-3 pb-3 small-heading">FARE SUMMARY</h5>	
			<div class="ui-bg-wrapper">
				<div class="fare-summary">
					
					<table class="table table-condensed ui-price-table">
					  <tbody>
						<tr>
						  <th scope="row" class="font-weight-bold">CAR TYPE</th>
						  <td class="font-weight-bold">TOTAL (in <span id="AgentCurrenct"></span>)</td>
						</tr>
						<%--<tr>
						  <th scope="row">Economy MPV </th>
						  <td class="SellingPrice1"></td>
						</tr>--%>
						<tr>
						  <th scope="row">TOTAL</th>
						  <td class="SellingPrice2"></td>
						</tr>
                       <%-- <tr>
						  <th scope="row">Markup</th>
						  <td class="Markup"></td>
						</tr>--%>
                        <tr class="disCountRow">                          
						  <th scope="row">Discount</th>
						  <td class="Discount"></td>
						</tr>
                        <tr class="vatRow">
						  <th scope="row">Vat</th>
						  <td class="TaxAmount"></td>
						</tr>
                        <tr class="gstRow">
						  <th scope="row">GST</th>
						  <td class="cgstAmount"></td>
						</tr>                       
						<tr>
						  <th scope="row">GRAND TOTAL</th>
						  <td class="TotalIncludeMarkup"></td>
						</tr>
						
						<tr>
						  <th scope="row" class="font-weight-bold grand-total">PAYABLE</th>
						  <td class="font-weight-bold" id="GrandTotal"></td>
						</tr>
					  </tbody>
					</table>
			
				</div>
			</div>
            
        <div class="modal fade in farerule-modal-style" data-backdrop="static" id="TNCModal" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="">&times;</button>
                    <h4 class="modal-title" id="">Terms & Conditions</h4>
                </div>
                <div class="modal-body">                                          
                    <div class="bg_white" id="h2TNC" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;">

                        <div id="divTNC" class="padding-10 refine-result-bg-color">
                            <p class="Ruleshdr">Supplier Terms & Condition</p>
                            <br />
                            <p class="Rulesdtl">
                                <input id="lnkBookingTNC" type="button" class="btn-link" value="https://talixo.com/conditions-of-transport/" onclick="ViewBookingConditions(this.value)" \>
                             </p>
                            <br />
                            <p class="Ruleshdr mb-4">Terms &amp; Conditions</p>
                            <div class="clear"></div>
                            <p class="mb-4">
                                                                            	If you encounter any issues please call our operations hotline on any of these numbers below, available 24/7:</p>
                                                                            	<p>
Germany: <strong>+49 30 346 497 360 / +49 30 346 497 369 </strong><br>

UK: <strong>+441748220043</strong><br>

USA: <strong>+1 646 687-6714 </strong><br>

UAE : <strong>+971562138662</strong>
                                                                            </p>
<!--                            <ul class="Rulesdtl mb-3"><li class="d-flex mb-1"> <span class="icon icon-arrow_drop_right"></span><div>All rooms are guaranteed on the day of arrival. In the case of no-show, your room(s) will be released and you will subject to the terms and condition of the Cancellation/No-show policy specified at the time you made the booking as well as noted in the confirmation Email </div> </li><li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>The total price for these booking fees not include mini-bar items, telephone bills, laundry service, etc. The hotel will bill you directly. </div></li><li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>In case where breakfast is included with the room rate, please note that certain hotels may charge extra for children travelling with their parents. If applicable, the hotel will bill you directly. Upon arrival, if you have any questions, please verify with the hotel.</div> </li> <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>Any complaints related to the respective hotel services,with regards to location, rooms, food, cleaning or other services, the guest will have to directly deal with the hotel. Cozmo Holidays will not be responsible for uch complaints.</div> </li> <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>The General Hotel Policy: Check-in time at 1400hrs and check-out time 1200hrs Early check-in or late check-out is subject to availability at the time of check-in/check-out at the hotel and cannot be guaranteed at any given point in time</div> </li> <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>Interconnecting/ Adjoining rooms/any special requests are always subject to availability at the time of check-in, and Cozmo Holidays will not be responsible for any denial of such rooms to the Customer.</div> </li> <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>Most of the hotels will be asking for credit card or cash amount to be paid upon check-in as guaranteed against any expected extras by the guest, Cozmo Holidays will not be responsible in case the guest doesn’t carry a credit card or enough cash money for the same, and the guest has to follow up directly with the hotel for the refund upon check out, Cozmo holidays is not responsible in case of any delay from central bank for credit card refunds. </div></li></ul>-->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
		
	</div>
	</div>
	</div>
	</div>
	
				
		
		
		
	</div>
    <style>
        input.error {
            border: 1px solid #D8000C;
        }
        label.error{
            color:#D8000C;
        }
        .Ruleshdr { 
            float:left;font-style:normal;	
            background:#18407B;color:#fff;padding:1px 2px 0 2px;
            margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px; 
        }

        .Rulesdtl { border:1px solid #18407B;margin-right:10px;padding:10px; }
    </style>
    <link type="text/css" href="css/intlTelInput.css" rel="stylesheet" />
    <script>
        var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var MainAgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
        var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["TransferWebApiUrl"]%>';
    </script>
    <script src="scripts/intlTelInput.js"></script>
    <script src="scripts/Transfers/transferPax.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
