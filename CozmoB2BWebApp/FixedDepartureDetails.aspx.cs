﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class FixedDepartureDetailsGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected string activityImgFolder;
    protected string departureDates = "";
    protected string departureMonths = "";
    protected string departureYears = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        if (Settings.LoginInfo != null)
        {
            if (!IsPostBack)
            {
                if (Session["FixedDepartures"] != null)
                    Session.Remove("FixedDepartures");
                int id = 0;
                if (Request["id"] != null && Request["id"] != string.Empty)
                {
                    id = Convert.ToInt32(Request["id"]);
                }
                LoadData(id);
            }
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
    public void LoadData(int id)
    {
        try
        {
            Activity.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            Activity.AgentId = Settings.LoginInfo.AgentId;
            activity = new Activity(id);
            dlRoomRates.DataSource = activity.PriceDetails;
            dlRoomRates.DataBind();

            dlFDItinerary.DataSource = activity.FixedItineraryDetails;
            dlFDItinerary.DataBind();
            Session["FixedDeparture"] = activity;

            decimal Total = Convert.ToDecimal(activity.StartingFrom);

            lblAmount.Text = Total.ToString("N" + Settings.LoginInfo.DecimalValue);
            lblActivity.Text = activity.Name;
            imgActivity.ImageUrl = activityImgFolder + activity.ImagePath2;

            //Bind ExcursionDates
            int i = 1;
            foreach (DataRow dr in activity.FixedinitPriceDetails.Rows)
            {
                if (dr["PriceDate"] != null && dr["PriceDate"].ToString() != "")
                {
                    DateTime dt = DateTime.Parse(dr["PriceDate"].ToString());
                    if (ddlDepDate.Items.FindByText(Convert.ToString(dt.ToString("dd-MMM-yyyy"))) == null)
                    {
                        //string todaydate = DateTime.Now.ToString("dd/MM/yyyy");
                        DateTime dtSuppliedDate = DateTime.Parse(dt.ToString("dd-MMM-yyyy"));
                        if (dtSuppliedDate.Subtract(DateTime.Today).Days >= 0)
                        {
                            ListItem item = new ListItem(dt.ToString("dd-MMM-yyyy"), i.ToString());
                            ddlDepDate.Items.Add(item);
                        }
                    }
                    i++;
                }
            }
            ddlDepDate.Items.Insert(0, new ListItem("dd-MM-YYYY", "-1"));

            foreach (DataRow dr in activity.PriceDetails.Rows)
            {
                if (dr["PriceDate"] != null && dr["PriceDate"].ToString() != "")
                {
                    DateTime dt = DateTime.Parse(dr["PriceDate"].ToString());
                    if (departureDates.Length > 0)
                    {
                        departureDates += ",[" + dt.Year + "-" + (dt.Month - 1) + "-" + dt.Day + "]";
                    }
                    else
                    {
                        departureDates = "[" + dt.Year + "-" + (dt.Month - 1) + "-" + dt.Day + "]";
                    }
                    if (!departureMonths.Contains(dt.Year + "-" + (dt.Month - 1).ToString()))
                    {
                        if (departureMonths.Length > 0)
                        {
                            departureMonths += "," + dt.Year + "-" + (dt.Month - 1).ToString();
                        }
                        else
                        {
                            departureMonths = dt.Year + "-" + (dt.Month - 1).ToString();
                        }
                    }

                    if (!departureYears.Contains(dt.Year.ToString()))
                    {
                        if (departureYears.Length > 0)
                        {
                            departureYears += "," + dt.Year;
                        }
                        else
                        {
                            departureYears = dt.Year.ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }

    protected void imgCheck_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["FixedDeparture"] != null)
            {
                activity = Session["FixedDeparture"] as Activity;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DateTime excursionDate = Convert.ToDateTime(ddlDepDate.SelectedItem.Text, dateFormat);
                DataRow[] filteredDateRows = activity.FixedinitPriceDetails.Select("PriceDate >= '" + excursionDate.ToString("MM/dd/yyyy 00:00:00") + "' AND PriceDate <='" + excursionDate.ToString("MM/dd/yyyy 23:59:59") + "'");

                DataTable dt = activity.PriceDetails.Clone();
                foreach (DataRow row in filteredDateRows)
                {
                    dt.ImportRow(row);
                }
                activity.PriceDetails = dt;

                /************************************************************************************************
                 * ------------------------------------Checks for Booking---------------------------------------*
                 * 1. Check whether booking date is after Excursion start date and before closing date.         *         
                 * 2. Booking cut off days check against the Booking Date / Excursion Date. If available then   *
                 *    proceed otherwise prompt user to choose another                                           *
                 * 3. Check whether booking date falls between available date ranges.                           *
                 * 4. Check whether booking date falls between unavailable date ranges.                         *
                 * 5. Check whether stock is available to proceed booking.                                      *
                 * ---------------------------------------------------------------------------------------------*
                 * **********************************************************************************************/
                //Check 1
                //DateTime StartFrom = Convert.ToDateTime(activity.StartFrom, dateFormat);
                //DateTime EndTo = Convert.ToDateTime(activity.EndTo, dateFormat);

                //checking AdultStockInHand
                int adultStockInHand = 0;
                int ChildStockInHand = 0;
                int InfantStockInHand = 0;
                int adultStockUsed = 0;
                int ChildStockUsed = 0;
                int InfantStockUsed = 0;
                int adultCount = 0;
                int childCount = 0;
                int infantCount = 0;
                bool adultValid = true;
                bool childValid = true;
                bool infantValid = true;
                if (activity.PriceDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in activity.PriceDetails.Rows)
                    {
                        if (dr["PaxType"] != DBNull.Value)
                        {
                            if (Convert.ToString(dr["PaxType"]) == "Adult")
                            {
                                if (dr["StockInHand"] != DBNull.Value && dr["StockUsed"] != DBNull.Value)
                                {
                                    adultStockInHand += Convert.ToInt32(dr["StockInHand"].ToString());
                                    adultStockUsed += Convert.ToInt32(dr["StockUsed"].ToString());
                                    adultCount++;
                                }
                            }
                            if (Convert.ToString(dr["PaxType"]) == "Child")
                            {
                                if (dr["StockInHand"] != DBNull.Value && dr["StockUsed"] != DBNull.Value)
                                {
                                    ChildStockInHand += Convert.ToInt32(dr["StockInHand"].ToString());
                                    ChildStockUsed += Convert.ToInt32(dr["StockUsed"].ToString());
                                    childCount++;
                                }
                               
                            }
                            if (Convert.ToString(dr["PaxType"]) == "Infant")
                            {
                                if (dr["StockInHand"] != DBNull.Value && dr["StockUsed"] != DBNull.Value)
                                {
                                    InfantStockInHand = Convert.ToInt32(dr["StockInHand"].ToString());
                                    InfantStockUsed = Convert.ToInt32(dr["StockUsed"].ToString());
                                    infantCount++;
                                }
                                
                            }
                        }
                    }
                    if (adultCount > 0)
                    {
                        if (Convert.ToInt32(ddlAdult.SelectedItem.Value) > 0)
                        {
                            if ((adultStockInHand - adultStockUsed) <= 0 || ((adultStockInHand - adultStockUsed) - Convert.ToInt32(ddlAdult.SelectedItem.Value)) < 0)
                            {
                                adultValid = false;
                            }
                        }
                    }
                    if (childCount > 0)
                    {
                        if (Convert.ToInt32(ddlChild.SelectedItem.Value) > 0)
                        {
                            if ((ChildStockInHand - ChildStockUsed) <= 0 || ((ChildStockInHand - ChildStockUsed) - Convert.ToInt32(ddlChild.SelectedItem.Value)) < 0)
                            {
                                childValid = false;
                            }
                        }
                    }

                    if (infantCount > 0)
                    {
                        if (Convert.ToInt32(ddlInfant.SelectedItem.Value) > 0)
                        {
                            if ((InfantStockInHand - InfantStockUsed) <= 0 || ((InfantStockInHand - InfantStockUsed) - Convert.ToInt32(ddlInfant.SelectedItem.Value)) < 0)
                            {
                                infantValid = false;
                            }
                        }
                    }
                }
                if (filteredDateRows.Length > 0)
                {

                    //if ((activity.StockInHand - activity.StockUsed) > 0)
                    if (adultValid && childValid && infantValid)
                    {
                        DataRow row = null;
                        if (activity.TransactionHeader.Rows.Count > 0)
                        {
                            row = activity.TransactionHeader.Rows[0];
                        }
                        else
                        {
                            row = activity.TransactionHeader.NewRow();
                        }

                        row["ActivityId"] = activity.Id;
                        row["ActivityName"] = activity.Name;
                        row["TransactionDate"] = DateTime.Now;
                        row["Adult"] = ddlAdult.SelectedValue;
                        row["Child"] = ddlChild.SelectedValue;
                        row["Infant"] = ddlInfant.SelectedValue;
                        row["Booking"] = Convert.ToDateTime(ddlDepDate.SelectedItem.Text, dateFormat);
                        row["TransactionType"] = "B2B";
                        row["isFixedDeparture"] = "Y";// Fixed Departure
                        row["RoomCount"] = ddlRooms.SelectedValue;
                        if (activity.TransactionHeader.Rows.Count <= 0)
                        {
                            activity.TransactionHeader.Rows.Add(row);
                        }
                        Session["FixedDeparture"] = activity;

                        //Proceed Activity booking
                        Response.Redirect("FixedDepartureCategoriesList.aspx", false);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "ShowNoStockEmailPopup();", true);
                    }
                }
                else
                {
                    
                }
            }
            else
            {
                Response.Redirect("FixedDepartureResults.aspx", false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
