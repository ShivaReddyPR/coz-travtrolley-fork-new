<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSTariffMasterUI" Title="ROS Tariff Master" Codebehind="ROSTariffMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script  type="text/javascript">
    function Validate() {
        clearMessage();
        if (getElement('ddlCompany').selectedIndex == '0') addMessage('Please select Company from the list !')
        if (getElement('ddlArea').selectedIndex == '0') addMessage('Please select Area from the list !')
        if (getMessage() != '') {
            alert(getMessage());
            clearMessage(); return false;
        }
    }

</script>

<style> 

.tbl2 input, select  { width:200px; height:24px; border: solid 1px #ccc; }

.tbl2 td  { line-height:30px; }

.gray { color:#ccc; }


textarea { width:240px; border: solid 1px #ccc; height:50px; } 

</style>

<br /> 

<table class="tbl2" width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="24%" align="right">Code</td>
    <td width="76%"><asp:TextBox runat="server" ID="txtCode" Enabled="false"> </asp:TextBox></td>
  </tr>
 
  <tr>
    <td align="right">Company:</td>
    <td>
            <asp:DropDownList runat="server" ID="ddlCompany"   >         
            </asp:DropDownList>
        
        </td>            
  </tr>
  <tr>
    <td align="right">Area</td>
    <td>
    
            <asp:DropDownList runat="server" ID="ddlArea" > 
            <asp:ListItem Text="Cozmo" Value="2"></asp:ListItem>
            <asp:ListItem Text="G9" Value="1"></asp:ListItem>
            </asp:DropDownList>
        
    </td>
  </tr>
  
  
    <tr>
    <td align="right">Amount: </td>
    <td> <asp:TextBox ID="txtAmount"  onkeypress="return restrictNumeric(this.id,'2');" OnChange="setToFixed('txtAmount');" runat="server"></asp:TextBox></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td height="40" >
        <table width="200px">
            <tr>
                <td width="33%">
                    <asp:Button style="height:30px; width:auto" CssClass="button-normal" Text="Save" runat="server" id="btnSave" OnClientClick="return Validate();" OnClick="btnSave_Click"  /> </td>
                <td width="33%">
                    <asp:Button style="height:30px; width:auto" CssClass="button-normal" Text="Clear" runat="server" id="btnClear" OnClick="btnClear_Click"  /> </td>
                <td width="33%">
                    <asp:Button style="height:30px; width:auto" CssClass="button-normal" Text="Search" runat="server" id="btnSearch"  OnClick="btnSearch_Click" /> </td>
            </tr>
       </table>
    </td>
  </tr>
   <tr>
    <td colspan="2">
     <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
    </td>
    </tr>
</table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="TRF_ID" 
    EmptyDataText="No Tariff List!" AutoGenerateColumns="false" PageSize="15" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging">
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>    
    <cc1:Filter   ID="HTtxtCode"  Width="70px" HeaderText="Tariff Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("TRF_CODE") %>' CssClass="label grdof" ToolTip='<%# Eval("TRF_CODE") %>' Width="70px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>      
      
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtCompany"  Width="100px" CssClass="inputEnabled" HeaderText="Company" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("trf_company_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("trf_company_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtArea"  Width="100px" CssClass="inputEnabled" HeaderText="Area" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblArea" runat="server" Text='<%# Eval("loc_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("loc_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtAmount"  Width="100px" CssClass="inputEnabled" HeaderText="Amount" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblAmount" runat="server" Text='<%# Eval("TRF_AMOUNT") %>' CssClass="label grdof"  ToolTip='<%# Eval("TRF_AMOUNT") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    </Columns>           
    </asp:GridView>
</asp:Content>
