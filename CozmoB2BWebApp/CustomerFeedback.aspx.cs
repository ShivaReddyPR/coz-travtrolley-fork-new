﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.CRM;

public partial class CustomerFeedback : CT.Core.ParentPage 
{
    private DataTable FeedbackDetails;
    DateTime fromDate;
    DateTime toDate;
    protected Feedback obj = new Feedback();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        if (!IsPostBack)
        {
            InitializeGrid();
        }

       
    }
    protected void InitializeGrid()
    {
        try
        {
            txtFrom.Text = DateTime.Now.ToString("dd/MM/yyyy").Replace("-", "/");
            //txtFromTime.Text = DateTime.Now.ToString("00:00");
            txtTo.Text = DateTime.Now.ToString("dd/MM/yyyy").Replace("-", "/");
            //txtToTime.Text = DateTime.Now.ToString("23:59");

            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindSearch();
    }
    public void BindSearch()
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (txtFrom.Text != "")
            {
                string FromDate = (txtFrom.Text);
                fromDate = Convert.ToDateTime(FromDate, dateFormat);
            }
            if (txtTo.Text != "")
            {
                string ToDate = (txtTo.Text);
                toDate = Convert.ToDateTime(ToDate, dateFormat);
            }
            FeedbackDetails = Feedback.GetFeedbackDetails(fromDate, toDate);
            Session["FeedbackDetails"] = FeedbackDetails;
            gvFeedback.DataSource = FeedbackDetails;
            gvFeedback.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void gvFeedback_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFeedback.PageIndex = e.NewPageIndex;
            gvFeedback.EditIndex = -1;
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void gvFeedback_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label description = ((Label)e.Row.FindControl("ITlblDescription"));
            LinkButton lnkButton = ((LinkButton)e.Row.FindControl("ITlnkdesc"));
            string[] str = description.Text.Split(' ');
            string text = string.Empty;
            if (str.Length > 10)
            {
                for (int i = 0; i < 10; i++)
                {
                    text += str[i];
                    text += " ";
                }
                description.Text = text;
                lnkButton.Visible = true;
            }
            else
            {
                for (int i = 0; i < str.Length; i++)
                {
                    text += str[i];
                    text += " ";
                }
                description.Text = text;
            }
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string script = "window.open('ExportExcelCustomerFeedBack.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "Excel", script, true);
        }
        catch
        {
            throw;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void Clear()
    {
        try
        {
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
