﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CT.Core;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using System.Web.UI.WebControls;
using System.Data;
using System.Transactions;
using System.Configuration;

using CT.TicketReceipt.Common;

namespace CozmoB2BWebApp
{
    public partial class BaggageInsuranceGUI : CT.Core.ParentPage
    {
        int userID = 0;
        protected List<BaggageInsuranceMaster> baggageInsuranceMasterList = new List<BaggageInsuranceMaster>();
        protected DataSet dsPaxDetails;
        protected BaggageInsuranceHeader header;
        protected AgentMaster agent;
        protected decimal amount = 0;
        BaggageInsuranceMaster baggageInsuranceMaster = new BaggageInsuranceMaster();
        List<BaggageInsuranceMaster> objlist = new List<BaggageInsuranceMaster>();
        protected int decimalPoints=2;
        string currencyCode = string.Empty;
        long LocationUserID = 0;
        int agent_ID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    if (!IsPostBack)
                    {
                        BindAgents();
                        LoadPlanDetails();
                        userID = (int)Settings.LoginInfo.UserID;
                    }

                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
                if (hdnagentIdDeselected.Value == "AgentDeselected")
                {
                    ddlAirAgentsLocations.Items.Clear();
                    hdnagentIdDeselected.Value = "";
                }
                if (rbtnAgent.Checked)
                {
                    Utility.StartupScript(this.Page, "disablefield();", "disablefield");

                }
                hdnNoofPaxValue.Value = ddlNoOfPassengers.SelectedValue;
                GeneratePaxControls(Convert.ToInt32(ddlNoOfPassengers.SelectedValue));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in Page_Load()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {   
                LoadPlanDetails();
                if (Convert.ToInt32(hdnNoofPaxValue.Value) != Convert.ToInt32(ddlNoOfPassengers.SelectedValue))
                {
                    tblPaxDetails.Controls.Clear();
                    GeneratePaxControls(Convert.ToInt32(ddlNoOfPassengers.SelectedValue));
                }
                if (rbtnAgent.Checked && ddlAirAgents.SelectedValue != "0")
                {
                    DataTable dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(agent_ID, CT.TicketReceipt.BusinessLayer.ListStatus.Short, CT.TicketReceipt.BusinessLayer.RecordStatus.Activated, string.Empty);
                    ddlAirAgentsLocations.DataSource = dtLocations;
                    ddlAirAgentsLocations.DataTextField = "LOCATION_NAME";
                    ddlAirAgentsLocations.DataValueField = "LOCATION_ID";
                    ddlAirAgentsLocations.DataBind();
                    ddlAirAgentsLocations.Items.Insert(0, new ListItem("Select Location", "0"));
                    if (Convert.ToInt32(hdnAgentLocation.Value)==0)
                    {
                        ddlAirAgentsLocations.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlAirAgentsLocations.SelectedValue = hdnAgentLocation.Value;
                    }
                   
                }


                Utility.StartupScript(this.Page, "InitCards();", "InitCards");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in Page_PreRender()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        private void BindAgents()
        {
            try
            {
                int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                DataRow[] dr = dtAgents.Select("AGENT_ID NOT ='" + agentId + "'");
                DataTable DTagents = new DataTable();
                DTagents.Columns.Add( "AGENT_ID", typeof(int));
                DTagents.Columns.Add("AGENT_CODE", typeof(string));
                DTagents.Columns.Add("AGENT_NAME", typeof(string));
                DTagents.Columns.Add("agent_type", typeof(int));
                foreach (DataRow row in dr)
                {
                   DataRow Row=  DTagents.NewRow();
                    Row["AGENT_ID"] = Convert.ToInt32(row.ItemArray[0].ToString());
                    Row["AGENT_CODE"] = row.ItemArray[1].ToString();
                    Row["AGENT_NAME"] = row.ItemArray[2].ToString();
                    Row["agent_type"] = row.ItemArray[3].ToString();
                    DTagents.Rows.Add(Row);
                }
                ddlAirAgents.DataSource = DTagents;//dtAgents.Fin;
                ddlAirAgents.DataTextField = "Agent_Name";
                ddlAirAgents.DataValueField = "agent_id";
                ddlAirAgents.DataBind();
                ddlAirAgents.Items.Insert(0, new ListItem("--Select Agent--", "0"));
                ddlAirAgents.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in BindAgents()" + ex.ToString(), Request["REMOTE_ADDR"]);

            }
        }
        private void GeneratePaxControls(int rowCount)
        {
            try
            {
                TableHeaderRow headerRow;
                TableRow row;

                for (int i = 0; i <= rowCount; i++)
                {
                    row = new TableRow();
                    headerRow = new TableHeaderRow();
                    if (i == 0)
                    {
                        TableHeaderCell TitleHeader = new TableHeaderCell();
                        Label lblTitle = new Label();
                        lblTitle.Text = "Title";
                        TitleHeader.Controls.Add(lblTitle);
                        TitleHeader.Width = new Unit(100, UnitType.Pixel);
                        headerRow.Cells.Add(TitleHeader);

                        TableHeaderCell FirstNameHeader = new TableHeaderCell();
                        Label lblFirstName = new Label();
                        lblFirstName.Text = "First Name";
                        FirstNameHeader.Controls.Add(lblFirstName);
                        headerRow.Cells.Add(FirstNameHeader);

                        TableHeaderCell LastNameHeader = new TableHeaderCell();
                        Label lblLastName = new Label();
                        lblLastName.Text = "Last Name";
                        LastNameHeader.Controls.Add(lblLastName);
                        headerRow.Cells.Add(LastNameHeader);

                        TableHeaderCell PNRNumberHeader = new TableHeaderCell();
                        Label lblPNRNumber = new Label();
                        lblPNRNumber.Text = "PNR Number";
                        PNRNumberHeader.Controls.Add(lblPNRNumber);
                        headerRow.Cells.Add(PNRNumberHeader);

                        TableHeaderCell ticketNumberHeader = new TableHeaderCell();
                        Label lblTicketNumber = new Label();
                        lblTicketNumber.Text = "Ticket Number";
                        ticketNumberHeader.Controls.Add(lblTicketNumber);
                        headerRow.Cells.Add(ticketNumberHeader);
                        tblPaxDetails.Rows.Add(headerRow);
                    }
                    else
                    {
                        TableCell cellddltitle = new TableCell();
                        DropDownList ddlTitle = new DropDownList();
                        ddlTitle.ID = "ddlTitle" + i;
                        ddlTitle.Width = new Unit(130, UnitType.Pixel);
                        ddlTitle.CssClass = "form-control";
                        ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                        ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
                        ddlTitle.Items.Add(new ListItem("Dr.", "Dr."));
                    
                        cellddltitle.Width = new Unit(150, UnitType.Pixel);
                        cellddltitle.Controls.Add(ddlTitle);
                        row.Cells.Add(cellddltitle);

                        TableCell FirstNameCell = new TableCell();
                        TextBox txtFirstName = new TextBox();
                        txtFirstName.ID = "txtFirstName" + i;
                        txtFirstName.Width = new Unit(130, UnitType.Pixel);
                        txtFirstName.CssClass = "form-control";
                        txtFirstName.MaxLength = 10;
                        txtFirstName.Attributes.Add("onkeypress", "return isAlpha(event);");
                        FirstNameCell.Controls.Add(txtFirstName);
                        FirstNameCell.Width = new Unit(150, UnitType.Pixel);

                        row.Cells.Add(FirstNameCell);

                        TableCell LastNameCell = new TableCell();
                        TextBox txtLastName = new TextBox();
                        txtLastName.ID = "txtLastName" + i;
                        txtLastName.Width = new Unit(130, UnitType.Pixel);
                        txtLastName.CssClass = "form-control";
                        txtLastName.MaxLength = 15;
                        txtLastName.Attributes.Add("onkeypress", "return isAlpha(event);");
                        LastNameCell.Width = new Unit(150, UnitType.Pixel);
                        LastNameCell.Controls.Add(txtLastName);

                        row.Cells.Add(LastNameCell);

                        TableCell PNRNUmberCell = new TableCell();
                        TextBox txtPNRNumber = new TextBox();
                        txtPNRNumber.ID = "txtPNRNumber" + i;
                        txtPNRNumber.Width = new Unit(130, UnitType.Pixel);
                        txtPNRNumber.CssClass = "form-control";
                        txtPNRNumber.Attributes.Add("onkeypress", "return isAlphaNumeric(event);");
                        txtPNRNumber.Attributes.Add("ondrop", "return false;");
                        txtPNRNumber.Attributes.Add("onpaste", "return false;");
                        txtPNRNumber.MaxLength = 20;
                        PNRNUmberCell.Width = new Unit(150, UnitType.Pixel);
                        PNRNUmberCell.Controls.Add(txtPNRNumber);

                        row.Cells.Add(PNRNUmberCell);

                        TableCell TicketNumberCell = new TableCell();
                        TextBox txtTicketNumber = new TextBox();
                        txtTicketNumber.ID = "txtTicketNumber" + i;
                        txtTicketNumber.Width = new Unit(130, UnitType.Pixel);
                        txtTicketNumber.CssClass = "form-control";
                        txtTicketNumber.Attributes.Add("onkeypress", "return isAlphaNumeric(event);");
                        txtTicketNumber.MaxLength = 30;
                        TicketNumberCell.Width = new Unit(150, UnitType.Pixel);
                        TicketNumberCell.Controls.Add(txtTicketNumber);

                        row.Cells.Add(TicketNumberCell);


                        tblPaxDetails.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(BaggageInsurance)Error in GeneratePaxControls." + ex.ToString(), Request["REMOTE_ADDR"]);

            }
        }
        protected void ddlNoOfPassengers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadPlanDetails();
                if(ddlAirAgentsLocations.SelectedIndex<=0)
                {
                    hdnAgentLocation.Value = "0";
                }
              
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in ddlNoOfPassengers_SelectedIndexChanged()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        private void SavePax(int Bid, BaggageInsuranceMaster obj,long LocationUserID)
        {
            try
            {
                List<BaggagePassengerDetails> baggagePassengersList = new List<BaggagePassengerDetails>();
                BaggagePassengerDetails objbaggagePassengerDetails = new BaggagePassengerDetails();
                for (int i = 1; i <= Convert.ToInt32(ddlNoOfPassengers.SelectedValue); i++)
                {
                    BaggagePassengerDetails baggagePassengerDetails = new BaggagePassengerDetails();

                    DropDownList ddltitle = (DropDownList)tblPaxDetails.Rows[i].Cells[0].FindControl("ddlTitle" + i);
                    TextBox txtFirstName = (TextBox)tblPaxDetails.Rows[i].Cells[1].FindControl("txtFirstName" + i);
                    TextBox txtlastName = (TextBox)tblPaxDetails.Rows[i].Cells[2].FindControl("txtLastName" + i);
                    TextBox txtPNrNumber = (TextBox)tblPaxDetails.Rows[i].Cells[3].FindControl("txtPNRNumber" + i);
                    TextBox txtTicketNumber = (TextBox)tblPaxDetails.Rows[i].Cells[4].FindControl("txtTicketNumber" + i);

                    baggagePassengerDetails.Bid = Bid;
                    baggagePassengerDetails.Title = ddltitle.SelectedValue;
                    baggagePassengerDetails.FirstName = txtFirstName.Text;
                    baggagePassengerDetails.LastName = txtlastName.Text;
                    baggagePassengerDetails.PNRNumber = txtPNrNumber.Text;
                    baggagePassengerDetails.EmailID = txtemail.Text;
                    baggagePassengerDetails.PhoneNumber = txtPhoneNo.Text;
                    baggagePassengerDetails.TicketNumber = txtTicketNumber.Text;
                    baggagePassengerDetails.PolicyNo = "POLICY_" + Bid + "_" + DateTime.Now.ToString("MM/dd/yyyy") + "_" + i;
                    //baggagePassengerDetails.PaxAmount = ((obj.PlanAdultPrice) / Convert.ToInt32(ddlNoOfPassengers.SelectedValue))-(obj.Markup+obj.Discount+obj.GSTAmount+obj.InputVAT+obj.OutputVAT);
                    baggagePassengerDetails.PaxAmount = ((obj.PlanAdultPrice) / Convert.ToInt32(ddlNoOfPassengers.SelectedValue));//- (obj.Markup + obj.Discount + obj.GSTAmount + obj.InputVAT + obj.OutputVAT);
                    baggagePassengerDetails.Createdby = LocationUserID;
                    baggagePassengerDetails.RateOfExchange = obj.RateOfExchange;
                    baggagePassengerDetails.AgentExchangeRates = obj.AgentExchangeRates;
                    baggagePassengerDetails.SourceAmount = obj.SourceAmount+obj.ServiceFee;
                    baggagePassengerDetails.SourceCurrency = obj.SourceCurrency;
                    baggagePassengerDetails.Paxcurrency = obj.PaxCurrency;
                    //baggagePassengerDetails.Paxcurrency = currency;
                    baggagePassengerDetails.Markup = obj.Markup/ Convert.ToInt32(ddlNoOfPassengers.SelectedValue);
                    baggagePassengerDetails.MarkupValue = obj.MarkupValue;
                    baggagePassengerDetails.MarkupType = obj.MarkupType;
                    baggagePassengerDetails.Discount = obj.Discount/Convert.ToInt32(ddlNoOfPassengers.SelectedValue);
                    baggagePassengerDetails.DiscountType = obj.DiscountType;
                    baggagePassengerDetails.DiscountValue = obj.DiscountValue;
                    baggagePassengerDetails.GSTDetails = obj.GSTDetails;
                    baggagePassengerDetails.GstAmount = obj.GSTAmount / Convert.ToInt32(ddlNoOfPassengers.SelectedValue);
                    baggagePassengerDetails.InputVAT = obj.InputVAT / Convert.ToInt32(ddlNoOfPassengers.SelectedValue);
                    baggagePassengerDetails.OutputVAT = obj.OutputVAT / Convert.ToInt32(ddlNoOfPassengers.SelectedValue);
                    baggagePassengersList.Add(baggagePassengerDetails);
                }

                objbaggagePassengerDetails.SavePassengersDetails(baggagePassengersList);

                
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (Page.IsValid)
                {
                    
                    int Bid = 0;
                    int agentID = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAirAgents.SelectedValue) : Settings.LoginInfo.AgentId);
                    if (agentID == 0)
                    {
                        Utility.Alert(this.Page, "Please select the Agent");
                        ddlAirAgentsLocations.Items.Clear();
                        return;
                    }
                    if (rbtnAgent.Checked && ddlAirAgentsLocations.SelectedIndex == -1)
                    {

                        Utility.Alert(this.Page, "Please select the Location");
                        return;
                    }
                    AgentMaster agent = new AgentMaster(agentID);
                    int planId = Convert.ToInt32(hdnPlanId.Value);
                    baggageInsuranceMasterList = (List<BaggageInsuranceMaster>)Session["baggageInsuranceMasterDetails"];
                    BaggageInsuranceMaster obj = baggageInsuranceMasterList[planId - 1];
                    LocationUserID = Settings.LoginInfo.UserID;
                    decimal planAdultPrice = obj.PlanAdultPrice;
                    BaggageInsuranceHeader baggageInsuranceHeader = new BaggageInsuranceHeader();
                    baggageInsuranceHeader.PlanId = obj.PlanID;
                    baggageInsuranceHeader.NoOfSectors = Convert.ToInt32(ddlNoOfSectors.SelectedValue);
                    baggageInsuranceHeader.NoOfPassengers = Convert.ToInt32(ddlNoOfPassengers.SelectedValue);
                    TextBox PNRNo = (TextBox)tblPaxDetails.Rows[1].Cells[3].FindControl("txtPNRNumber1");
                    baggageInsuranceHeader.PNR = PNRNo.Text;
                    hdnPNRNumber.Value = baggageInsuranceHeader.PNR;
                    baggageInsuranceHeader.AgentID = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAirAgents.SelectedValue) : Settings.LoginInfo.AgentId);
                    baggageInsuranceHeader.Location = (rbtnAgent.Checked == true ? Convert.ToInt32(hdnAgentLocation.Value) : Convert.ToInt32(Settings.LoginInfo.LocationID));
                    baggageInsuranceHeader.TotalAmount = planAdultPrice + obj.Markup + obj.GSTAmount - obj.Discount;
                    baggageInsuranceHeader.CreatedBy = LocationUserID;
                    baggageInsuranceHeader.EmailID = txtemail.Text;
                    baggageInsuranceHeader.PhoneNumber = txtPhoneNo.Text;

                    decimal agentbalance = agent.UpdateBalance(0);
                    if (agentbalance <= 0 || agentbalance< baggageInsuranceHeader.TotalAmount)
                    {
                        Utility.Alert(this.Page, "Insufficent Funds.");
                        return;
                    }
                    if(baggageInsuranceHeader.TotalAmount <=0)
                    {
                        Utility.Alert(this.Page, "Plan Amount shouldn't be Zero/Negative");
                        return;
                    }
                    //decimal amount = 0;
                    using (TransactionScope updateTransaction = new TransactionScope())
                    {
                        //baggageInsuranceHeader.TotalAmount += obj.Markup - obj.Discount+obj.GSTAmount+obj.InputVAT+obj.OutputVAT+obj.ServiceFee;
                        Bid = baggageInsuranceHeader.SaveBaggageHeaderDetails();
                        if (Bid > 0)
                        {
                            //currencyCode = (rbtnAgent.Checked==true ? agent.AgentCurrency : Settings.LoginInfo.Currency);
                            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(BaggageInsurance)Saved Baggage Header Details Pax Details .", Request["REMOTE_ADDR"]);
                            SavePax(Bid, obj, LocationUserID);
                            //amount = obj.PlanAdultPrice;//+ obj.Markup - obj.Discount + obj.GSTAmount + obj.InputVAT + obj.OutputVAT+obj.ServiceFee ;
                        }
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(BaggageInsurance)Saved Pax Details .", Request["REMOTE_ADDR"]);
                        updateTransaction.Complete();
                    }
                    SaveInvoiceAndLedger(Bid, baggageInsuranceHeader.TotalAmount);
                    AgentMaster.UpdateAgentBalance(agentID, baggageInsuranceHeader.TotalAmount, Convert.ToInt32(Settings.LoginInfo.UserID));
                 
                    Response.Redirect("BaggageInsuranceVoucher.aspx?BID=" + Bid,false);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(BaggageInsurance)Error in btnSave_Click." + ex.ToString(), Request["REMOTE_ADDR"]);
                
            }
          
        }
        private void LoadPlanDetails()
        {
            try
            {
                int agentID = 0;
                int locationID = 0;
                int decimalPoint = 2;
                Dictionary<string, decimal> exchangeRates = new Dictionary<string, decimal>();
                //string currencyCode = Settings.LoginInfo.Currency;
                if(rbtnAgent.Checked)
                {
                    Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
                    agentID = Settings.LoginInfo.OnBehalfAgentID;
                    Settings.LoginInfo.IsOnBehalfOfAgent = true;
                    Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
                   
                    AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = agent.AgentCurrency;
                    if (ddlAirAgents.SelectedIndex > 0)
                    {
                        Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    }
                    Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);
                    currencyCode = agent.AgentCurrency;

                    Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                    exchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    locationID = Convert.ToInt32(hdnAgentLocation.Value);
                    decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    
                }
                else
                {
                    agentID = Settings.LoginInfo.AgentId;
                    exchangeRates= Settings.LoginInfo.AgentExchangeRates;
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    locationID = Convert.ToInt32(Settings.LoginInfo.LocationID);
                    decimalPoint = Settings.LoginInfo.DecimalValue;
                    currencyCode = Settings.LoginInfo.Currency;
                    LocationUserID = Settings.LoginInfo.UserID;
                }
                agent_ID = agentID;
                //locationID= Convert.ToInt32(ConfigurationManager.AppSettings["STATIC_LOCATION_ID_FOR_GST"]);
                if (locationID > 0)
                {
                    BaggageInsuranceMaster objbaggageInsuranceMaster = new BaggageInsuranceMaster();
                    baggageInsuranceMasterList = baggageInsuranceMaster.GetPlanDetails(agentID, exchangeRates, currencyCode, Convert.ToInt32(ddlNoOfPassengers.SelectedValue), locationID, decimalPoint);
                    hdnPlanCount.Value = baggageInsuranceMasterList.Count.ToString();
                    Session["baggageInsuranceMasterDetails"] = baggageInsuranceMasterList;
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(BaggageInsurance)Error in LoadPlanDetails." + ex.ToString(), Request["REMOTE_ADDR"]);

            }
        }
        private void SaveInvoiceAndLedger(int Bid,decimal planAmount)
        {
            int agentId= (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAirAgents.SelectedValue) : Settings.LoginInfo.AgentId);
            AgentMaster agent = new AgentMaster(agentId);
            decimal agentBalance = agent.UpdateBalance(0);
            try
            {
                if (agentBalance >= planAmount)
                {
                    Invoice invoice = new Invoice();
                    int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(Bid, CT.BookingEngine.ProductType.BaggageInsurance);

                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                    }
                    else
                    {
                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(Bid, "", (int)Settings.LoginInfo.UserID, CT.BookingEngine.ProductType.BaggageInsurance, 1);
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                            invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                            invoice.UpdateInvoice();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        private void BindLocations(int agentTypeID, int agentId)
        {
            try
            {
                string agentType = string.Empty;
                switch (agentTypeID)
                {
                    case 1:
                        agentType = "BaseAgent ";
                        break;
                    case 2:
                        agentType = "Agent";
                        break;
                    case 3:
                        agentType = "B2B";
                        break;
                    case 4:
                        agentType = "B2B2B";
                        break;

                }
                DataTable dtlocations = LocationMaster.GetList(agentId, ListStatus.Long, RecordStatus.Activated, agentType);
                ddlAirAgentsLocations.DataSource = dtlocations;
                ddlAirAgentsLocations.DataValueField = "LOCATION_ID";
                ddlAirAgentsLocations.DataTextField = "Locations";
                ddlAirAgentsLocations.DataBind();
                ddlAirAgentsLocations.Items.Insert(0, new ListItem("--Select Location--", "0"));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in BindLocations()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }
        protected void rbtnSelf_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ddlNoOfPassengers.SelectedValue = "1";
                LoadPlanDetails();
                //hdnNoofPaxValue.Value = ddlNoOfPassengers.SelectedValue;
                ddlAirAgents.SelectedIndex = -1;
                ddlAirAgentsLocations.SelectedIndex = -1;
                ddlAirAgentsLocations.Items.Clear();
                clear();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in rbtnSelf_CheckedChanged()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        
        private void clear()
        {
            try
            {
                txtemail.Text = string.Empty;
                txtPhoneNo.Text = string.Empty;
                tblPaxDetails.Controls.Clear();
                ddlNoOfSectors.SelectedIndex = -1;
                ddlNoOfPassengers.SelectedValue = "1";
                ddlAirAgents.SelectedIndex = -1;
                ddlAirAgentsLocations.Items.Clear();
                hdnPlanId.Value = "0";
                LoadPlanDetails();
                GeneratePaxControls(Convert.ToInt32(ddlNoOfPassengers.SelectedValue));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in clear()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }
        
        protected void rbtnAgent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Settings.LoginInfo.AgentId
                ddlNoOfPassengers.SelectedValue = "1";
                txtemail.Text = string.Empty;
                txtPhoneNo.Text = string.Empty;
                clear();
                //GeneratePaxControls(Convert.ToInt32(ddlNoOfPassengers.SelectedValue));
                //hdnNoofPaxValue.Value = ddlNoOfPassengers.SelectedValue;
                ddlAirAgents.SelectedIndex = -1;
                ddlAirAgentsLocations.SelectedIndex = -1;
                ddlAirAgentsLocations.Items.Clear();
              
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, userID, "(BaggageInsurance)Error in rbtnAgent_CheckedChanged()" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            clear();
        }
    } 
}
