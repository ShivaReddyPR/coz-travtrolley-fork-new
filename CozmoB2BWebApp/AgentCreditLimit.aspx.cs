﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using System.Data;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class AgentCreditLimit : CT.Core.ParentPage
    {
        protected AgentMaster agent;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                    BindAgentInfo();
                lblMessage.Style.Add("display", "none");
            }
            else
                Response.Redirect("AbandonSession.aspx");
        }

        void BindAgentInfo()
        {
            ddlAgents.DataSource = AgentMaster.GetCreditLimitAgents();
            ddlAgents.DataBind();
            if (Request["agentid"] != null)
            {
                agent = new AgentMaster(Convert.ToInt64(Request["agentid"]));
                Session["Agent"] = agent;
                ddlAgents.SelectedValue = agent.ID.ToString();
                UpdateAgent(Convert.ToInt32(agent.ID));
            }
            else
            {
                ddlAgents.Items.Insert(0, new ListItem("--Select Agent--", "0"));
                ddlAgents.SelectedIndex = 0;
                ddlAgents.Enabled = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                agent = Session["Agent"] as AgentMaster;
                agent.CreditLimit = Convert.ToDouble(txtCreditLimit.Text);
                agent.CreditBuffer = Convert.ToDouble(txtCreditBuffer.Text);
                agent.CreditDays = Convert.ToInt32(txtCreditDays.Text);
                agent.CreatedBy = Settings.LoginInfo.UserID;
                agent.UpdateCreditLimit();
                lblMessage.Style.Add("display", "block");
                lblMessage.Style.Add("color", "green");
                lblMessage.Text = "Saved Successfully"; 
                agent = new AgentMaster(Convert.ToInt32(agent.ID));
                UpdateAgent(Convert.ToInt32(agent.ID));
                ddlAgents.SelectedValue = agent.ID.ToString();
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to Save AgentCreditLimit Data" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
                lblMessage.Text = "Failed To Save.";
                lblMessage.Style.Add("display", "block");
                lblMessage.Style.Add("color", "red");
            }
        }

        protected void gvCreditHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCreditHistory.PageIndex = e.NewPageIndex;
            gvCreditHistory.DataSource = (DataTable)Session["AgentCreditData"];
            gvCreditHistory.DataBind();
        }
        protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            agent = new AgentMaster(Convert.ToInt32(ddlAgents.SelectedValue));
            Session["Agent"] = agent;
            UpdateAgent(Convert.ToInt32(agent.ID));
        }

        private void UpdateAgent(int agentId)
        {
            try
            {
                int decimalValue = agent.DecimalValue;
                txtCreditLimit.Text = agent.CreditLimit.ToString("N" + decimalValue);
                txtCreditBuffer.Text = agent.CreditBuffer.ToString("N" + decimalValue);
                txtCreditDays.Text = agent.CreditDays.ToString();
                lblCurrentBalance.Text = agent.AgentCurrency + " " + agent.CurrentBalance.ToString("N" + decimalValue);
                DataTable dtAgentCreditData = AgentMaster.GetCreditLimitHistory(Convert.ToInt32(agent.ID));
                Session["AgentCreditData"] = dtAgentCreditData;
                gvCreditHistory.DataSource = dtAgentCreditData;
                gvCreditHistory.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to UpdateAgent()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            }

        }
    }
}
