﻿using System;
using CT.Core;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using Visa;

public partial class AddVisaDocument : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();  //commented by vijesh 12-02-2015
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            Page.Title = "Add Document";

            BindAgent();
            ddlCountry.DataSource = Country.GetCountryList(); //VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0,"Select");
            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int doumentId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out doumentId);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaDocument document = new VisaDocument(doumentId);
                    if (document != null)
                    {
                        txtDocumentType.Text = document.DocumentType;
                        txtDescription.Text = document.DocumentDescription;
                        txtFileSize.Text = document.FileSize.ToString();
                        txtFileType.Text = document.FileType;
                        txtheight.Text = document.Height.ToString();
                        txtWidth.Text = document.Width.ToString();
                        ddlAgent.SelectedValue =Convert.ToString(document.AgentId);
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(document.CountryCode));
                        ddlAgent.Enabled = false;
                        btnSave.Text = "Update";
                        Page.Title = "Update Visa Document";
                        lblStatus.Text = "Update Visa Document";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaDocument.aspx,Err:" + ex.Message,"");
                }

            }
            else
            {
               // ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText("UAE"));
                if (!isAdmin)
                {
                    ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                    ddlAgent.Enabled = false;
                }
            }

        }

    }
    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            VisaDocument document = new VisaDocument();
            document.CountryCode = ddlCountry.SelectedValue;
            document.DocumentDescription = txtDescription.Text.Trim();
            document.DocumentType = txtDocumentType.Text.Trim();
            document.FileSize = Convert.ToInt32(txtFileSize.Text.Trim());
            document.FileType = txtFileType.Text.Trim();
            document.Height = Convert.ToInt32(txtheight.Text.Trim());
            document.Width = Convert.ToInt32(txtWidth.Text.Trim());
            document.AgentId =Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    document.DocumentId = Convert.ToInt32(Request.QueryString[0]);
                    document.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                }
            }
            else
            {
                document.IsActive = true;
                document.CreatedBy = (int)Settings.LoginInfo.UserID;

            }
            document.Save();
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaDocument,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaDocumentList.aspx?Country=" + ddlCountry.SelectedValue+"&Agency=" +ddlAgent.SelectedItem.Value );
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
   
}
