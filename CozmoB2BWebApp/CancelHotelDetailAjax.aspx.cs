using System;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.Core;

using CT.TicketReceipt.BusinessLayer;

public partial class CancelHotelDetailAjax :System.Web.UI.Page
{
    protected string[] cancelInfo;
    protected string[] amendmentMessage;
    protected string errorMsg = string.Empty;
    protected HotelSearchResult result;
    protected string amenities = "";
    protected string roomTypeCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        string source = Request["hotelSource"];
        string hotelCode = Request["hotelCode"];
        string hotelName = Request["hotelName"];
        //string ratePlanType = HttpUtility.UrlDecode(Request["ratePlan"]);
         roomTypeCode = Request["rCode"];
        
        string price = Request["price"];
        string cityCode = Request["cityCode"];
        HotelRequest request = (HotelRequest)Session["req"];
        HotelSearchResult[] results = Session["SearchResults"] as HotelSearchResult[];
        //result = Session[hotelCode] as HotelSearchResult;
        result = Session["SelectedHotel"] as HotelSearchResult;

       
        Dictionary<string, string> response = new Dictionary<string, string>();

        HotelItinerary itinerary = new HotelItinerary();
        List<HotelPenality> penalityList = new List<HotelPenality>();

        if(request==null)
        {
            errorMsg = "Your Session Seems To Be Expired !!";
            return;
        }
        itinerary.StartDate = request.StartDate;
        itinerary.EndDate = request.EndDate;
        if (cityCode != null && cityCode.Length > 0)
        {
            itinerary.CityCode = cityCode;
        }
        else
        {
            itinerary.CityCode = request.CityCode;
        }
        itinerary.HotelCode = hotelCode;
        itinerary.HotelName = hotelName;
        itinerary.NoOfRooms = request.NoOfRooms;
        itinerary.PassengerNationality=request.PassengerNationality;
        //SET THE ITINERARY BOOKING SOURCE
        switch (source.ToUpper())
        {
            case "DESIYA": itinerary.Source = HotelBookingSource.Desiya;
                break;
            case "GTA": itinerary.Source = HotelBookingSource.GTA;
                break;
            case "HOTELBEDS": itinerary.Source = HotelBookingSource.HotelBeds;
                break;
            case "TOURICO": itinerary.Source = HotelBookingSource.Tourico;
                break;
            case "IAN": itinerary.Source = HotelBookingSource.IAN;
                break;
            case "TBOCONNECT": itinerary.Source = HotelBookingSource.TBOConnect;
                break;
            case "MIKI": itinerary.Source = HotelBookingSource.Miki;
                break;
            case "TRAVCO": itinerary.Source = HotelBookingSource.Travco;
                break;
            case "DOTW": itinerary.Source = HotelBookingSource.DOTW;
                break;
            case "WST": itinerary.Source = HotelBookingSource.WST;
                break;
            case "HOTELCONNECT": itinerary.Source = HotelBookingSource.HotelConnect;
                break;
            case "REZLIVE": itinerary.Source = HotelBookingSource.RezLive;
                break;
            case "LOH": itinerary.Source = HotelBookingSource.LOH;
                break;
            case "TBOHOTEL": itinerary.Source = HotelBookingSource.TBOHotel;
                break;
            case "JAC": itinerary.Source = HotelBookingSource.JAC; //New Source Added by brahmam 20.09.2016
                break;
            case "EET": itinerary.Source = HotelBookingSource.EET; //New Source Added by brahmam 03.10.2016
                break;
            case "AGODA":
                itinerary.Source = HotelBookingSource.Agoda; //New Source Added by brahmam 03.10.2016
                break;
            case "YATRA":
                itinerary.Source = HotelBookingSource.Yatra; //New Source Added by somasekhar on 24/08/2018
                break;
 #region GRN 
            case "GRN":
                itinerary.Source = HotelBookingSource.GRN; //New Source Added by harish 31.08.2016
                break;
            #endregion
            case "OYO":
                itinerary.Source = HotelBookingSource.OYO; //New Source Added by somasekhar on 14/12/2018
                break;
            default: break;
        }
        int roomIndex = 0;
        string rCode = "";
        //SET THE ROOM INFORMATION HERE
        if (source == "Miki")
        {
            roomTypeCode = roomTypeCode.Split('|')[0] + "|" + roomTypeCode.Split('|')[1].Replace(' ', '+') + "|" + roomTypeCode.Split('|')[2] + "|" + roomTypeCode.Split('|')[3];
        }

        if (source == "DOTW")
        {
            HotelRoom[] roomsInfo = new HotelRoom[1];
            for (int i = 0; i < 1; i++)
            {

                HotelRoom room = new HotelRoom();
                PriceAccounts priceInfo = new PriceAccounts();
                priceInfo.Currency = price;
                room.AdultCount = request.RoomGuest[i].noOfAdults;
                room.ChildCount = request.RoomGuest[i].noOfChild;
                room.ChildAge = request.RoomGuest[i].childAge;
                room.Price = priceInfo;

                room.RoomName = result.RoomDetails[i].RoomTypeName;
                room.RoomTypeCode = roomTypeCode;
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    {
                        room.RatePlanCode = rd.RatePlanCode;
                        room.RoomName = rd.RoomTypeName;
                        break;
                    }
                }

                room.RatePlanCode = room.RatePlanCode.Replace(" ", "+");

                roomsInfo[i] = room;
            }
            itinerary.Roomtype = roomsInfo;
        }
        else if(source == "HotelBeds")
        {

            List<RoomGuestData> roomsList = new List<RoomGuestData>();
            roomsList.AddRange(request.RoomGuest);
            
            HotelRoom[] roomsInfo = new HotelRoom[1];
            for (int i = 0; i < result.RoomDetails.Length; i++)
            {
                if (result.RoomDetails[i].RoomTypeCode == roomTypeCode.Replace(" ", "+"))
                {
                    HotelRoom room = new HotelRoom();
                    PriceAccounts priceInfo = new PriceAccounts();
                    priceInfo.Currency = price;
                    List<RoomGuestData> duplicateRoom = roomsList.FindAll(delegate(RoomGuestData checkRoom)
                     { return (checkRoom.noOfAdults== result.RoomDetails[i].Occupancy["AdultCount"] && checkRoom.noOfChild == result.RoomDetails[i].Occupancy["ChildCount"]); });
                    List<int> childAges = new List<int>();
                    room.AdultCount = result.RoomDetails[i].Occupancy["AdultCount"];
                    room.ChildCount = result.RoomDetails[i].Occupancy["ChildCount"];
                    if (room.ChildCount > 0)
                    {
                        if (duplicateRoom.Count > 1)
                        {
                            for (int j = 0; j < request.RoomGuest.Length; j++)
                            {
                                if (request.RoomGuest[j].noOfAdults == result.RoomDetails[i].Occupancy["AdultCount"] && request.RoomGuest[j].noOfChild == result.RoomDetails[i].Occupancy["ChildCount"])
                                {
                                    for (int k = 0; k < request.RoomGuest[j].noOfChild; k++)
                                    {
                                        childAges.Add(request.RoomGuest[j].childAge[k]);
                                    }
                                }
                            }
                            room.ChildAge = childAges;
                        }
                        else
                        {
                            room.ChildAge = request.RoomGuest[Convert.ToInt32(result.RoomDetails[i].SequenceNo) - 1].childAge;
                        }
                    }
                    room.Price = priceInfo;
                    room.RoomName = result.RoomDetails[i].RoomTypeName;
                    room.NoOfCots = 0;
                    room.ExtraBed = false;
                    if (duplicateRoom.Count > 1)
                    {
                        room.NoOfUnits = duplicateRoom.Count.ToString();
                    }
                    else
                    {
                        room.NoOfUnits = "1";
                    }
                    room.RoomTypeCode = result.RoomDetails[i].RoomTypeCode;
                    roomsInfo[0] = room;
                    break;
                }
            }
            itinerary.Roomtype = roomsInfo;
        }
        else if (source == "RezLive") // Modified brahmam 19.09.2015
        {
            HotelRoom[] roomsInfo = new HotelRoom[request.RoomGuest.Length];
            for (int i = 0; i < request.RoomGuest.Length; i++)
            {
                roomIndex = Convert.ToInt32(roomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                HotelRoom room = new HotelRoom();
                PriceAccounts priceInfo = new PriceAccounts();
                priceInfo.Currency = price;
                room.AdultCount = request.RoomGuest[i].noOfAdults;
                room.ChildCount = request.RoomGuest[i].noOfChild;
                room.ChildAge = request.RoomGuest[i].childAge;
                room.Price = priceInfo;
                room.RoomName = result.RoomDetails[roomIndex].RoomTypeName;
                room.RoomTypeCode = result.RoomDetails[roomIndex].RoomTypeCode;
                room.RatePlanCode = result.RoomDetails[roomIndex].RatePlanCode;
                room.Price.SupplierCurrency = result.Price.SupplierCurrency;
                room.Price.SupplierPrice = result.RoomDetails[roomIndex].supplierPrice;
                roomsInfo[i] = room;
            }
            itinerary.Roomtype = roomsInfo;
        }
        else if (source == "WST") //Added by brahmam
        {
            HotelRoom[] roomsInfo = new HotelRoom[1];
            HotelRoom room = new HotelRoom();
            PriceAccounts priceInfo = new PriceAccounts();
            priceInfo.Currency = price;
            room.AdultCount = request.RoomGuest[0].noOfAdults;
            room.ChildCount = request.RoomGuest[0].noOfChild;
            room.ChildAge = request.RoomGuest[0].childAge;
            room.Price = priceInfo;

            room.RoomTypeCode = roomTypeCode;
            foreach (HotelRoomsDetails rd in result.RoomDetails)
            {
                if (rd.RoomTypeCode == roomTypeCode)
                {
                    room.RatePlanCode = rd.RatePlanCode;
                    room.RoomName = rd.RoomTypeName;
                    break;
                }
            }
            room.RatePlanCode = room.RatePlanCode;
            roomsInfo[0] = room;
            itinerary.Roomtype = roomsInfo;
        }
        else if (source == "JAC")
        {
            HotelRoom[] roomsInfo = new HotelRoom[1];
            HotelRoom room = new HotelRoom();
            int roomNo = 0;
            try
            {
                roomNo = Convert.ToInt32(roomTypeCode.Split('|')[0]) - 1;  //checking Selected room
            }
            catch { }
            PriceAccounts priceInfo = new PriceAccounts();
            priceInfo.Currency = price;
            room.AdultCount = request.RoomGuest[roomNo].noOfAdults;
            room.ChildCount = request.RoomGuest[roomNo].noOfChild;
            room.ChildAge = request.RoomGuest[roomNo].childAge;
            room.Price = priceInfo;

            room.RoomTypeCode = roomTypeCode;
            foreach (HotelRoomsDetails rd in result.RoomDetails)
            {
                if (rd.RoomTypeCode == roomTypeCode)
                {
                    room.RatePlanCode = rd.RatePlanCode;
                    room.RoomName = rd.RoomTypeName;
                    break;
                }
            }
            room.RatePlanCode = room.RatePlanCode;
            roomsInfo[0] = room;
            itinerary.Roomtype = roomsInfo;
        }
        else
        {
            HotelRoom[] roomsInfo = new HotelRoom[request.RoomGuest.Length];
            for (int i = 0; i < request.RoomGuest.Length; i++)
            {
                if (source == "LOH" || source == "EET")
                {
                    rCode = roomTypeCode.Split('-')[0];
                    if (roomTypeCode.Split('-').Length > 1)
                    {
                        roomIndex = Convert.ToInt32(roomTypeCode.Split('-')[1]);
                    }
                }
                HotelRoom room = new HotelRoom();
                PriceAccounts priceInfo = new PriceAccounts();
                priceInfo.Currency = price;
                room.AdultCount = request.RoomGuest[i].noOfAdults;
                room.ChildCount = request.RoomGuest[i].noOfChild;
                room.ChildAge = request.RoomGuest[i].childAge;
                room.Price = priceInfo;
                if ((source == "LOH" || source == "EET") && roomTypeCode.Split('-').Length > 0)
                {
                    room.RoomName = result.RoomDetails[roomIndex].RoomTypeName;
                }
                else
                {
                    room.RoomName = result.RoomDetails[i].RoomTypeName;
                }
                if (source == "HotelConnect" || source == "LOH" || source == "EET")
                {
                    if (source == "LOH" || source == "EET")
                    {
                        room.RoomTypeCode = result.RoomDetails[roomIndex].RoomTypeCode;
                        room.RatePlanCode = result.RoomDetails[roomIndex].RatePlanCode;
                    }
                    else
                    {
                        room.RoomTypeCode = roomTypeCode;
                        foreach (HotelRoomsDetails rd in result.RoomDetails)
                        {
                            if (rd.RoomTypeCode == roomTypeCode)
                            {
                                room.RatePlanCode = rd.RatePlanCode;
                                room.RoomName = rd.RoomTypeName;
                                break;
                            }
                        }
                    }

                    room.RatePlanCode = room.RatePlanCode.Replace(" ", "+");
                }
                else if (source == "GTA" || source == "TBOHotel")
                {
                    //string[] roomList = roomTypeCode.Split('|');
                    //room.RoomTypeCode = roomList[0];
                    if (source == "GTA") room.RoomTypeCode = roomTypeCode.Replace(" ", "+"); //@@@ 02082016
                    else room.RoomTypeCode = roomTypeCode;
                    room.RatePlanCode = result.RoomDetails[roomIndex].RatePlanCode;
                    //room.NoOfCots = Convert.ToInt16(roomList[1]);
                    //room.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                    room.NoOfUnits = "1";//fixed for single room
                    room.Price.SupplierCurrency = result.Price.SupplierCurrency;
                }
                else if (source == "Miki")
                {
                    room.RoomTypeCode = roomTypeCode;
                    room.RatePlanCode = result.RoomDetails[roomIndex].RatePlanCode;
                    room.NoOfUnits = "1";//fixed for single room
                    room.Price.SupplierCurrency = result.Price.SupplierCurrency;
                }
                roomsInfo[i] = room;
            }
            itinerary.Roomtype = roomsInfo;
        }
        //CALL MSE METHODS TO GET CANCELLATION POLICY
        string sessionId = string.Empty;
        if (Session["cSessionId"] != null)
        {
            sessionId = Session["cSessionId"].ToString();
        }
        else
        {
            errorMsg = "Your Session Seems To Be Expired !!";
            return;
        }
        try
        {
            int agencyId = 0;
            Dictionary<string, decimal> ExchangeRates = new Dictionary<string, decimal>();
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Settings.LoginInfo.AgentId;
                ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            }
            else
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            }
            
            //CT.TicketReceipt.BusinessLayer.AgentMaster agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(agencyId);
            //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
            //staticInfo.BaseCurrency = agent.AgentCurrency;
            //Dictionary<string, decimal> rateOfExchange = staticInfo.CurrencyROE;
            
            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(sessionId);
            mse.SettingsLoginInfo = Settings.LoginInfo;
            //mse.DecimalPoint = agent.DecimalValue;
            if (itinerary.Source == HotelBookingSource.DOTW)
            {
                
                //CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(sessionId);
                itinerary.PassengerCountryOfResidence = request.PassengerCountryOfResidence;
                itinerary.PassengerNationality = request.PassengerNationality;
                response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.DOTW);

                foreach (HotelRoom hr in itinerary.Roomtype)
                {
                    if (hr.Ameneties != null)
                    {
                        foreach (string amenity in hr.Ameneties)
                        {
                            if (!amenities.Contains(amenity))
                            {
                                if (amenities.Length > 0)
                                {
                                    amenities += "-" + amenity;
                                }
                                else
                                {
                                    amenities = "~" + hr.RoomTypeCode +"-"+ amenity;
                                }
                            }
                        }
                    }
                }
                amenities += "@";
            }
            else if( itinerary.Source == HotelBookingSource.HotelConnect)
            {
                result = Session["SelectedHotel"] as HotelSearchResult;
                CZInventory.SearchEngine se = new CZInventory.SearchEngine();
                List<string> Rooms = new List<string>();

                int RoomId = Convert.ToInt32(roomTypeCode.Split('-')[1]);
                int AccomId = Convert.ToInt32(roomTypeCode.Split('-')[3]);
                
                Rooms.Add(roomTypeCode);


                response = se.GetCancellationPolicy(ref result, request, Rooms);


                foreach (KeyValuePair<string, string> pair in response)
                {
                    //Audit.Add(EventType.Exception, Severity.Normal, 1, "Response Key=" + pair.Key + " and Value=" + pair.Value, Request["REMOTE_ADDR"]);
                }
            }
            else if (itinerary.Source == HotelBookingSource.RezLive) //modified by brahmam   19/09/2015
            {
                //decimal supplierPrice = 0;
                //foreach (HotelRoom room in itinerary.Roomtype)
                //{
                //    supplierPrice += room.Price.SupplierPrice;
                //}
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                itinerary.DestinationCountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
                response = mse.GetCancellationDetails(itinerary,ref penalityList, true, HotelBookingSource.RezLive);
            }
            else if (itinerary.Source == HotelBookingSource.LOH)
            {
                LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(sessionId);
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    jxe.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    jxe.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    jxe.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    jxe.AgentCurrency = Settings.LoginInfo.Currency;
                    jxe.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    jxe.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                decimal supplierPrice = 0;

                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                //cancelation policy is coming based on the nationality Added by brahmam
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                string passengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                response = jxe.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode.Replace(" ", "+"), itinerary.StartDate, itinerary.EndDate, result.SequenceNumber, supplierPrice,passengerNationality);
            }
             //Un commented by brahmam
            else if (itinerary.Source == HotelBookingSource.WST)
            {
                response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.WST);
            }
            else if (itinerary.Source == HotelBookingSource.HotelBeds)
            {
                response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.HotelBeds);
            }
            else if (itinerary.Source == HotelBookingSource.GTA)
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    {
                        string[] splitter = { "@#" };
                        response.Add("lastCancellationDate", itinerary.StartDate.ToString());
                        response.Add("CancelPolicy", rd.CancellationPolicy.Split(splitter, StringSplitOptions.None)[0]);
                        response.Add("AmendmentPolicy", rd.CancellationPolicy.Split(splitter, StringSplitOptions.None)[1]);
                        response.Add("HotelPolicy", "");
                        break;
                    }
                }
                //response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.GTA);
            }
            else if (itinerary.Source == HotelBookingSource.Miki)
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    {

                        response.Add("lastCancellationDate", itinerary.StartDate.ToString());
                        response.Add("CancelPolicy", rd.CancellationPolicy);
                        response.Add("HotelPolicy", result.PropertyType); //PropertyType is used for keep Hotel alert message for miki
                        break;
                    }
                }
                //response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.GTA);
            }
                //Loading Cancelation policy
            else if (itinerary.Source == HotelBookingSource.TBOHotel) //Added by brahmam TBOHotel
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode) //@@@ 02082016
                    {
                        string[] splitter = { "@#" };
                        response.Add("lastCancellationDate", rd.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1]);
                        response.Add("CancelPolicy", rd.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0]);
                        response.Add("HotelPolicy", "");
                        break;
                    }
                }
            }
            else if (itinerary.Source == HotelBookingSource.Agoda)
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    {
                        response.Add("lastCancellationDate", itinerary.StartDate.ToString());
                        response.Add("CancelPolicy", rd.CancellationPolicy);
                        break;
                    }
                }
                //response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.GTA);
            }
            else if (itinerary.Source == HotelBookingSource.Yatra)
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    {
                        response.Add("lastCancellationDate", itinerary.StartDate.ToString());
                        response.Add("CancelPolicy", rd.CancellationPolicy);
                        break;
                    }
                }
                //response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.GTA);
            }
            else if (itinerary.Source == HotelBookingSource.JAC) //Added by brahmam 20.09.2016  Loading cancellation policy
            {
                response = mse.GetCancellationDetails(itinerary, ref penalityList, true, HotelBookingSource.JAC);
            }
            else if (itinerary.Source == HotelBookingSource.EET) //Loading Cancellaton policy Added by brahmam 26.10.2016
            {
                CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET(sessionId);
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    eetApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    eetApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    eetApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    eetApi.AgentCurrency = Settings.LoginInfo.Currency;
                    eetApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    eetApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                }
                decimal supplierPrice = 0;

                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                //cancelation policy is coming based on the nationality Added by brahmam
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                string passengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                response = eetApi.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode.Replace(" ", "+"), itinerary.StartDate, itinerary.EndDate, result.SequenceNumber, supplierPrice, passengerNationality);
            }
            #region GRN Cancelation details
            else if (itinerary.Source == HotelBookingSource.GRN)
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    {
                        // response.Add("lastCancellationDate", itinerary.StartDate.ToString());
                        response.Add("CancelPolicy", rd.CancellationPolicy);
                        break;
                    }
                }
            } 
            #endregion
            #region OYO Source Cancellation Policy  -- Added by Somasekhar on 14/12/2018  
            else if (itinerary.Source == HotelBookingSource.OYO)
            {
                foreach (HotelRoomsDetails rd in result.RoomDetails)
                {
                    if (rd.RoomTypeCode == roomTypeCode)
                    { 
                        response.Add("CancelPolicy", rd.CancellationPolicy);
                        break;
                    }
                }
            }
            #endregion
            if (response.Count == 0)
            {
                errorMsg = "No cancellation details found";
            }
        }
        catch (BookingEngineException ex)
        {
            errorMsg = ex.Message;
        }
        finally
        {
            if (response == null)
            {
                response = new Dictionary<string, string>();
            }
        }
        if (response.ContainsKey("CancelPolicy"))
        {
            cancelInfo = response["CancelPolicy"].Split('|');
        }
        else
        {
            cancelInfo = new string[0];
            errorMsg = "No Cancellation policies found";
        }
        if (response.ContainsKey("AmendmentPolicy"))
        {
            amendmentMessage = response["AmendmentPolicy"].Split('|');
        }
        else
        {
            amendmentMessage = new string[0];
        }
    }
}
