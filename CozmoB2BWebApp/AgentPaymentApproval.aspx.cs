using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.BusinessLayer;
using System.IO;

public partial class AgentPaymentApprovalUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string AGENT_PAYMENT_SESSION = "_AgentPayApprovalList";
    //private string VS_OBJECT_SESSION_NAME = "_TranxVisaSale";
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport);
            //Utility.StartupScript(this.Page, "getElement('hdfParam').value='1';", "hdfParam");
            //Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            // lblError.Visible = false;
            if (!IsPostBack)
            {
                if (Request["FromDate"] != null)
                {
                    txtFromDate.Text = Request["FromDate"];
                }
                else
                {
                    txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                }

                txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                //BindGrid();
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.StartupScript(this.Page, "ShowMessageDialog('btnPNR_Click','"+ex.Message+"','Information')", "btnPNR_Click"); 
        }
       
   }
    protected void ITlnkPrint_Click(object sender, EventArgs e)
    {
        try
        {

            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            HiddenField hdfAPId = (HiddenField)gvRow.FindControl("IThdfAPId");
            if (hdfAPId != null)
            {
                long apId = Utility.ToLong(hdfAPId.Value);                
                string script = "window.open('PrintAgentPaymentReceipt.aspx?apId=" + Utility.ToString(apId) + "','','width=800,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
                Utility.StartupScript(this.Page, script, "PrintAgentPaymentReceipt");
                               
            }

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            //            btnAccounted.Visible = (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.CASHIER);
            hdfSelectCount.Value= "0";

            //added by venkatesh on 31-01-2018
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindPaymentMode();
            BindGrid();
        }
        catch
        { throw; }

    }
    #endregion
    # region Session
    private DataTable PayApprovalList
    {
        get
        {
            return (DataTable)Session[AGENT_PAYMENT_SESSION];
        }
        set
        {
            if (value == null)
                Session.Remove(AGENT_PAYMENT_SESSION);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["vs_id"] };
                Session[AGENT_PAYMENT_SESSION] = value;
            }
        }
    }
   # endregion

    // private void BindAgent()
    //{
    //    try
    //    {
    //        //string agentType = Settings.LoginInfo.AgentType;
    //       // string agentType = "BASEAGENT";

    //        int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
    //        //AgentMaster1 agent = new AgentMaster1(agentId);
    //        DataTable dtAgents = null;
    //        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
    //        {
    //            dtAgents = AgentMaster.GetList(1, "ALL", agentId, ListStatus.Short, RecordStatus.Activated);
    //        }
    //        else if (Settings.LoginInfo.AgentType == AgentType.B2B)
    //        {
    //            dtAgents = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
    //        }
    //        ddlAgent.DataSource = dtAgents;
    //        ddlAgent.DataValueField = "agent_id";
    //        ddlAgent.DataTextField = "agent_name";
    //        ddlAgent.DataBind();
    //        ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));

    //        //if (Settings.LoginInfo.MemberType == MemberType.SUPER)
    //        //{
    //        //    ddlAgent.SelectedIndex = 0;
    //        //    ddlAgent.Enabled = true;
    //        //}
    //        //else
    //        //{
    //        //    ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
    //        //    ddlAgent.Enabled = false;
    //        //}

    //    }
    //    catch { throw; }
    // }
   
  
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hdfSelectCount.Value = "0";
            BindGrid();
            
            Utility.StartupScript(this.Page, "ShowHide('divParam')", "ShowHide");
            if (ddlTransType.SelectedValue != "-1")
            {
                Utility.StartupScript(this.Page, "transType()", "transType");
            }
        }
        catch { throw; }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            hdfSelectCount.Value = "0";
            ddlAgent.SelectedIndex = 0;
            ddlAgent.Enabled = true;
            BindGrid();

        }
        catch { throw; }
    }
    #region Bind Grid
    private void BindGrid()
    {
        try
        {
            string approvalStatus = string.Empty;
            string paymentMode = string.Empty;
            string transType = string.Empty;
            string transProvision = string.Empty;
            string agentType = string.Empty;

            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            DateTime fromDate = Convert.ToDateTime(txtFromDate.Text, provider);
            DateTime toDate = Convert.ToDateTime(txtToDate.Text, provider);
            int agent = Convert.ToInt32(ddlAgent.SelectedValue);
            //if (Convert.ToInt32(ddlB2BAgent.SelectedValue) > 0)
            //{
            //    agentId = Convert.ToInt32(ddlB2BAgent.SelectedValue);
            //}
            //if (Convert.ToInt32(ddlB2B2BAgent.SelectedValue) > 0)
            //{
            //    agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedValue);
            //}
            if (ddlStatus.SelectedValue != "-1")
            {
                approvalStatus = ddlStatus.SelectedValue;
            }
            if(ddlPayMode.SelectedValue != "-1")
            {
                paymentMode = ddlPayMode.SelectedValue;
            }
            if (ddlTransType.SelectedValue != "-1")
            {
                transType = ddlTransType.SelectedValue;
                if (ddlTransType.SelectedValue == "PAY" && ddlCreditItems.SelectedValue != null)
                {
                    transProvision = ddlCreditItems.SelectedValue;
                }
                else if(ddlTransType.SelectedValue == "DEB" && ddlDebitItems.SelectedValue != null)
                {
                    transProvision = ddlDebitItems.SelectedValue;
                }
            }
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }


            // VisaDispatchStatus dispatchStatus = (ddlDispStatus.SelectedIndex > 0) ? (VisaDispatchStatus)Convert.ToInt32(Convert.ToChar(ddlDispStatus.SelectedValue)) : VisaDispatchStatus.All;
            // LoginInfo userInfo = Settings.LoginInfo;
            //PayApprovalList = AgentPaymentDetails.GetPaymentApprovalList(Utility.ToInteger(ddlAgent.SelectedItem.Value), ListStatus.Short, RecordStatus.Activated);
            //PayApprovalList = AgentPaymentDetails.GetPaymentApprovalList(Utility.ToInteger(ddlAgent.SelectedItem.Value), ListStatus.Short,RecordStatus.Activated);
            PayApprovalList = AgentPaymentDetails.GetPaymentApprovalList(fromDate, toDate, agent, agentType, approvalStatus, transType, transProvision, paymentMode, ListStatus.Short, RecordStatus.Activated);
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvAgentPayDetails, PayApprovalList);
            //loadExcelData(((DataTable)gvAgentPayDetails.DataSource).Copy()); 

        }
        catch { throw; }
       
    }
    private void loadExcelData(DataTable dt)
    {
        try
        {
            decimal totalFare = Utility.ToDecimal(dt.Compute("SUM(VISA_COST)", ""));
           // decimal totalCardRate = Utility.ToDecimal(dt.Compute("SUM(VISA_COST)", ""));
            decimal totalCashDep = Utility.ToDecimal(dt.Compute("SUM(VS_CHEQUE_AMOUNT)", "VS_DEPOSIT_MODE='CASH'"));
            decimal totalCashCollection = totalFare + totalCashDep; 
            Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
            Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");
            //string grandTotal = Utility.ToString(((DataTable)(gvAgentPayDetails.DataSource)).Copy().Compute("SUM(VISA_COST)", ""));
            string grandTotal = Utility.ToString(totalCashCollection);
            lblGrandTotal.Text = string.Format(" {0} AED", (string.IsNullOrEmpty(grandTotal)) ? Formatter.ToCurrency(0) : Formatter.ToCurrency(grandTotal));

            //DataTable dt = ((DataTable)gvAgentPayDetails.DataSource).Copy();
            


            DataRow dr = dt.NewRow();
            dr["VISA_COST"] = Formatter.ToCurrency((totalCashCollection));
            dr["VS_ID"] = -1;
            dr["vs_doc_no"] = "GRAND TOTAL:";
            dt.Rows.Add(dr);
            Session["ExportExcelCollectlist"] = dt;
            lblGrandText.Visible = true;
            lblGrandTotal.Visible = true;
            ((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();
        }
        catch { throw; }
    }
    #endregion

    #region gvAgentPayDetails Events
    
    protected void gvAgentPayDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAgentPayDetails.PageIndex = e.NewPageIndex;
            gvAgentPayDetails.EditIndex = -1;
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            selectedItem();
            string[,] textboxesNColumns ={{"HTtxtReceiptDate","ap_receipt_date"},{ "HTtxtDocNo", "ap_reciept_no" },{"HTtxtAmount", "ap_amount" },
            { "HTtxtPaymentMode", "ap_payment_mode_name" },{ "HTtxtChequeNo", "ap_cheque_no" },
            { "HTtxtChequeDate", "ap_cheque_date" },{ "HTtxtBankName", "ap_bank_name" },{ "HTtxtBankBranch", "ap_bank_branch" },{ "HTtxtBankac", "ap_bank_account" },
            { "HTtxtRemarks", "ap_remarks" }, { "HTtxtStatus","approval_status"}, { "HTtxtTransType", "AP_TRANX_TYPE"}, { "HTtxtTransProvision","ap_tranx_provision"} };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvAgentPayDetails, PayApprovalList.Copy(), textboxesNColumns);
           // loadExcelData(((DataTable)gvAgentPayDetails.DataSource).Copy()); 
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

            //Utility.StartupScript(this.Page, "ShowMessageDialog('Agent Payment list','" + ex.Message + "','Information')", "Filter_Click");
            //Utility.Alert(this.Page, ex.Message);

        }
    }

    #endregion

    # region Report
    
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcelCollectlist.aspx?&source=visalist','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel"); 
            string attachment = "attachment; filename=AgentPaymentApproval.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgAgentPaymentApprovalList.AllowPaging = false;
            dgAgentPaymentApprovalList.DataSource = PayApprovalList;
            dgAgentPaymentApprovalList.DataBind();
            dgAgentPaymentApprovalList.RenderControl(htw);
            Response.Write(sw.ToString());

        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
        finally
        {
            Response.End();
        }
    }
    protected void btnApproval_Click(object sender, EventArgs e)
    {
         try
        {

            selectedItem();
            isTrackingEmpty();

            //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            foreach (DataRow dr in PayApprovalList.Rows)
            {
                if (dr != null)
                {
                    string tranxtype = Utility.ToString(dr["AP_TRANX_TYPE"]);
                    string narrRemarks = Utility.ToString(dr["ap_tranx_provision"]);
                    string remarks = Utility.ToString(dr["AP_REMARKS"]);
                    bool selected = Utility.ToString(dr["ap_approval_status"]) == "Y" ? true : false;
                    long apId = Utility.ToLong(dr["ap_id"]);
                    decimal agentAmount = Utility.ToDecimal(dr["ap_amount"]);
                    //string pnrStatus = Utility.ToString(dr["vs_pnr_status"]);
                    //long ticketId = Utility.ToLong(dr["vs_ticket_id"]);
                    bool isUpdated = Utility.ToString(dr["ap_approval_status"]) == "U" ? true : false; ;

                    //added by brahmam mail purpose
                    string receiptNo = Utility.ToString(dr["ap_reciept_no"]);
                    string topupMode = Utility.ToString(dr["ap_payment_mode_name"]);
                    string topupType = Utility.ToString(dr["ap_tranx_provision"]);
                    int tranxAgentId= Utility.ToInteger(dr["ap_agent_id"]); 
                    if (selected && !isUpdated)
                    {
                        string Approval_status = "Y";
                        decimal currentBalance = AgentPaymentDetails.UpdateAgentPayment(apId, tranxAgentId, agentAmount, Settings.LoginInfo.UserID, Approval_status);
                        //added by brahmamto sending mail
                        //if (topupType == "TOP UP") // Only For Topup Tranx
                        //    SendMail(receiptNo, agentId, DateTime.Now, -1 * agentAmount, "TopUp", topupMode);will enable later once confirm from Vijay                                                            
                        // TO PUT IN lEDGER TRANX
                        //Leadger Transaction
                        CT.AccountingEngine.LedgerTransaction ledgerTxn;
                        CT.AccountingEngine.NarrationBuilder objNarration = new CT.AccountingEngine.NarrationBuilder();
                        ledgerTxn = new CT.AccountingEngine.LedgerTransaction();
                        //ledgerTxn.LedgerId = Utility.ToInteger(agentId);
                        ledgerTxn.LedgerId = tranxAgentId;
                        
                        ledgerTxn.Amount = Utility.ToDecimal(agentAmount) * -1;
                       
                      
                        ledgerTxn.IsLCC = false;
                        ledgerTxn.ReferenceId = Utility.ToInteger(apId);
                        if (tranxtype == "DEB")
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.PaymentReversed;// debit
                        }
                        else
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.PaymentRecieved;// credit
                        }
                        objNarration.Remarks = narrRemarks;
                        ledgerTxn.Narration = objNarration;
                            
                        ledgerTxn.Notes = remarks;
                        ledgerTxn.Date = DateTime.UtcNow;
                        //ledgerTxn.CreatedBy = Settings.LoginInfo.AgentId;
                        ledgerTxn.CreatedBy =(int) Settings.LoginInfo.UserID;
                        ledgerTxn.Save();
                       // VSORAInterface.GenerateInterfaceReceipt(apId, VisaAccountedStatus.YES, pnrStatus, ticketId, Settings.LoginInfo.UserID);
                       // long reportId = apId;
                       // string script = "window.open('printVisaSalesReport.aspx?apId=" + Utility.ToString(reportId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
                       // Utility.StartupScript(this.Page, script, "printVisaSaleReport");
                       if(tranxAgentId==Settings.LoginInfo.AgentId)  Settings.LoginInfo.AgentBalance = currentBalance;
                    }
                    
                }
                
            }
            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text= "Approved Successfully";
            hdfSelectCount.Value = "0";
      

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            //Utility.StartupScript(this.Page, "ShowMessageDialog('Payment Approval','" + ex.Message + "','Information')", "btnApproval_Click");
            Utility.Alert(this.Page, ex.Message);
            Utility.Alert(this.Page, ex.Message);
            //Utility.WriteLog(ex, this.Title);


        }
    }
    private void selectedItem()
    {
        try
        {
          //  int _selectedCount = 0;
            foreach (GridViewRow gvRow in gvAgentPayDetails.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfAPId = (HiddenField)gvRow.FindControl("IThdfAPId");
                
                foreach (DataRow dr in PayApprovalList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["ap_id"]) == hdfAPId.Value)
                        {
                          if (Utility.ToString(dr["ap_approval_status"])!="U")  dr["ap_approval_status"] = chkSelect.Checked ? "Y" : "N";
                           // if (chkSelect.Checked && chkSelect.Enabled) _selectedCount++;
                        }
                        dr.EndEdit();

                    }
                    
                }
             
            }

        }
        catch { throw; }
    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in PayApprovalList.Rows)
            {
                if (dr != null)
                {
                    bool isUpdated = Utility.ToString(dr["ap_approval_status"]) == "U" ? true : false; ;
                    if (!isUpdated && Utility.ToString(dr["ap_approval_status"]) == "Y")
                    {
                        
                        _selected = true;
                        return;
                    }
                }
            }
            string strMsg = "Please Select @least one Item ! ";
            if (!_selected) Utility.Alert(this.Page, strMsg);
                
                //Utility.StartupScript(this.Page, "ShowMessageDialog('Payment ApprOval','" + strMsg + "','Information')", "btnApproval_Click");
        }
        catch { throw; }
    }        
    //private  void StartupScript(Page page, string script, string key)
    //{

    //    script = string.Format("{0};", script);
    //    if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
    //    {
    //        if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
    //            System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
    //        else
    //            page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

    //    }
    //}
    protected void gvAgentPayDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //long vsId = Utility.ToLong(gvAgentPayDetails.DataKeys[gvAgentPayDetails.SelectedIndex].Values[0]);
            GridViewRow gvSeletedRow = gvAgentPayDetails.SelectedRow;
            long apId=Utility.ToLong(((HiddenField)gvSeletedRow.FindControl("IThdfAPId")).Value);         
            BindGrid();
           
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    # endregion

    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion

    # region GridBindMethods
    protected object StatusVisible()
    {
        return (object)(Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.CASHIER);
    }
     # endregion

    #region to send mail
    private void SendMail(string receiptNo, int agentId, DateTime approvedDate, decimal receiptAmount, string topupType, string topupMode)
    {
        try
        {

            System.Text.StringBuilder message = new System.Text.StringBuilder();
            AgentMaster agent = new AgentMaster(agentId);
            //string emailBodyContent = agent.EmailContent1;
            //string emailTimeContent = agent.EmailContent2;          
            string toMail = agent.Email1;
            string agentName = agent.Name;
            string alternateEmail = agent.Email2;
            string eMailCaption = receiptNo + " - Agent Payment Successfully processed";
            string emailCCId = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["AGENT_TOPUP_MAIL"]);
            string emailBCCId = Utility.ToString(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
            message.AppendFormat("Dear {0},\r\n", agentName);
            //emailBccId = Utility.ToString(ConfigurationManager.AppSettings["MAIL_BCC_VISA_APPROVAL"]);

            message.AppendFormat("\\n\\n\t This is an AUTOMATED MESSAGE. Please do not reply. \\n");
            message.AppendFormat("\t Action :	Request successfully processed \\n");
            message.AppendFormat("\t Request Number : {0}\\n", receiptNo);
            message.AppendFormat("\t Agency : {0}\\n", agentName);
            message.AppendFormat("\t Processed On : {0}\\n", approvedDate);
            message.AppendFormat("\t Submitted By : {0}\\n", Settings.LoginInfo.FullName);
            message.AppendFormat("\t Payment Amount : {0}\\n", receiptAmount);
            message.AppendFormat("\t Top-up Mode : {0}\\n", topupMode);
            message.AppendFormat("\t Top-up Type : {0}\\n", topupType);
            message.AppendFormat("\t The amount has been credited in your VMS account.\\n\\n");

            message.AppendFormat(" Regards, \\n  Finance Top-up Team");
            message.AppendFormat("\\n  Cozmo Travel");
            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            if (!string.IsNullOrEmpty(toMail))
                toArray.Add(toMail);
            if (!string.IsNullOrEmpty(alternateEmail))
                toArray.Add(alternateEmail);
            if (!string.IsNullOrEmpty(emailCCId))
                toArray.Add(emailCCId);

            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, eMailCaption, message.ToString(), new Hashtable(), emailBCCId);
            //VSORAInterface.SendMailQueue(0, "S", ConfigurationManager.AppSettings["MAIL_FROM_ID"].ToString(), eMailCaption, toMail, emailCCId, emailBCCId, string.Empty, string.Empty, message.ToString(), string.Empty, string.Empty);
        }
        catch { }
    }
    #endregion

    //Added by venkatesh on 31-01-2018
    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindPaymentMode()
    {
        try
        {
            //DataView dv = dtMode.DefaultView;
            //dv.RowFilter = "FIELD_VALUE NOT IN ('4','5','6')";
            ddlPayMode.DataSource = UserMaster.GetMemberTypeList("agent_pay_mode").Tables[0];
            ddlPayMode.DataValueField = "FIELD_VALUE";
            ddlPayMode.DataTextField = "FIELD_TEXT";
            ddlPayMode.DataBind();
            ddlPayMode.Items.Insert(0, new ListItem("--Select Mode--", "-1"));
            ddlPayMode.SelectedIndex = 0;
        }
        catch { throw; }
    }
    //Added By Phani 
    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            selectedItem();
            isTrackingEmpty();
            foreach (DataRow dr in PayApprovalList.Rows)
            {
                if (dr != null)
                {
                    bool selected = Utility.ToString(dr["ap_approval_status"]) == "Y" ? true : false;
                    long apId = Utility.ToLong(dr["ap_id"]);
                    decimal agentAmount = Utility.ToDecimal(dr["ap_amount"]);
                    bool isUpdated = Utility.ToString(dr["ap_approval_status"]) == "U" ? true : false; ;
                    int tranxAgentId = Utility.ToInteger(dr["ap_agent_id"]);
                    if (selected && !isUpdated)
                    {
                        string Approval_status = "R";
                         AgentPaymentDetails.UpdateAgentPayment(apId, tranxAgentId, agentAmount, Settings.LoginInfo.UserID, Approval_status);
                    }

                }

            }
            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = "Rejected Successfully";
            hdfSelectCount.Value = "0";

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
}
