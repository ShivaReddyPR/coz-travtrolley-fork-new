﻿using System;
using System.Data;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;

public partial class FlightInvAjax : System.Web.UI.Page
{
    protected int ctrlId;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Corresponding to Airline,isLCC,FareBasis,Cabin Class and Booking Class html
        if (Request["requestSource"] != null && Request["requestSource"] == "getFareCabinBookingHtml")
        {
            if (Request["id"] != null)
            {
                this.ctrlId = Convert.ToInt32(Request["id"]);
            }
        }
        //Corresponding to Seats and PNR html.
        else if (Request["requestSource"] != null && Request["requestSource"] == "getSeatsPNRHtml")
        {
            if (Request["id"] != null)
            {
                this.ctrlId = Convert.ToInt32(Request["id"]);
            }
        }
        //Corresponding to Seats PNR's Html in edit mode
        else if (Request["requestSource"] != null && Request["requestSource"] == "getSeatsPNRHtmlEditMode")
        {
            if (Request["id"] != null)
            {
                this.ctrlId = Convert.ToInt32(Request["id"]);
            }
        }

        //Corresponding to different CabinTypes.
        else if (Request["requestSource"] != null && Request["requestSource"] == "getCabinTypeList")
        {
            string response = "";
            try
            {

                foreach (CabinClass cabin in Enum.GetValues(typeof(CabinClass)))
                {
                    if (response.Length > 0)
                    {
                        response += "," + cabin.ToString() + "|" + cabin.ToString();
                    }
                    else
                    {
                        response = Request["id"] + "#" + cabin.ToString() + "|" + cabin.ToString();
                    }
                }
                
                
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }
        //Corresponding to different Airlines.
        else if (Request["requestSource"] == "getAllAirLinesList")
        {
            string response = "";
            try
            {
                DataTable dtAirLines = Airline.GetAllAirlinesList();
                DataView view = dtAirLines.DefaultView;
                view.Sort = "airlineName ASC";
                dtAirLines = view.ToTable();

                if (dtAirLines != null && dtAirLines.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtAirLines.Rows)
                    {
                        if (dr["airlineName"] != DBNull.Value && dr["airlineCode"] != DBNull.Value)
                        {

                            if (response.Length > 0)
                            {
                                response += "," + Convert.ToString(dr["airlineName"]) + "|" + Convert.ToString(dr["airlineCode"]) + "|" + Convert.ToString(dr["isLCC"]);
                            }
                            else
                            {
                                response = Request["id"] + "#" + Convert.ToString(dr["airlineName"]) + "|" + Convert.ToString(dr["airlineCode"]) + "|" + Convert.ToString(dr["isLCC"]);
                            }
                        }
                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }
       //Corresponding to cities for different countries based on the country code
        else if (Request["requestFrom"] == "cityList")
        {
            string cityString = "";
            string countryCode = Request["ccode"];
            List<RegCity> cities = CT.Core.RegCity.GetCityList(countryCode);

            foreach (CT.Core.RegCity city in cities)
            {
                if (cityString.Length > 0)
                {
                    cityString += "/" + city.CityName.ToString() + "," + city.CityCode.ToString();
                }
                else
                {
                    cityString = city.CityName.ToString() + "," + city.CityCode.ToString();
                }
            }

            Response.Write(cityString);
        }




    }
}
