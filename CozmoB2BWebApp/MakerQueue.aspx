﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="MakerQueue" Title="Queue Page" Codebehind="MakerQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <%----------------------------------the files for multiple uploads-------------------------------------------------%>
    <%--    <script src="latestJs_1.11/jquery.min.js"></script>
--%>
    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <%----------------------------------------------------------------------------------------------------------------%>
    <%----------------------------------the files for magnific popup-------------------------------------------------%>
    <%--<script src="Scripts/magnificpop/jquery.js" type="text/javascript"></script>--%>

    <script src="Scripts/magnificpop/jquery.magnific-popup.js" type="text/javascript"></script>

    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
    <%-----------------------------------------------------------------------------------%>
    <link href="css/TicketQuee.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <div id="MakerQueue1" visible="false" runat="server">

        <script type="text/javascript">
            var cal1;
            var cal2;

            function init() {

                //    showReturn();
                var today = new Date();
                // For making dual Calendar use CalendarGroup  for single Month use Calendar     
                cal1 = new YAHOO.widget.Calendar("cal1", "container1");
                //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                cal1.cfg.setProperty("title", "Select CheckIn date");
                cal1.cfg.setProperty("close", true);
                cal1.selectEvent.subscribe(setDates1);
                cal1.render();

                cal2 = new YAHOO.widget.Calendar("cal2", "container2");
                cal2.cfg.setProperty("title", "Select CheckOut date");
                cal2.selectEvent.subscribe(setDates2);
                cal2.cfg.setProperty("close", true);
                cal2.render();
            }
            function showCal1() {
                $('container2').context.styleSheets[0].display = "none";
                $('container1').context.styleSheets[0].display = "block";
                init();
                cal1.show();
                cal2.hide();
            }


            var departureDate = new Date();
            function showCal2() {
                $('container1').context.styleSheets[0].display = "none";
                cal1.hide();
                init();
                // setting Calender2 min date acoording to calendar1 selected date
                var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
                //var date1=new Date(tempDate.getDate()+1);

                if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                    var depDateArray = date1.split('/');

                    var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                    cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                    cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    cal2.render();
                }
                document.getElementById('container2').style.display = "block";
            }
            function setDates1() {
                var date1 = cal1.getSelectedDates()[0];

                $('IShimFrame').context.styleSheets[0].display = "none";
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();

                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());


                departureDate = cal1.getSelectedDates()[0];
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                //			
                var month = date1.getMonth() + 1;
                var day = date1.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

                //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
                //cal2.render();

                cal1.hide();

            }
            function setDates2() {
                var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
                if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                    return false;
                }

                var date2 = cal2.getSelectedDates()[0];

                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
                var difference = returndate.getTime() - depdate.getTime();

                //         if (difference < 1) {
                //             document.getElementById('errMess').style.display = "block";
                //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                //             return false;
                //         }
                //         if (difference == 0) {
                //             document.getElementById('errMess').style.display = "block";
                //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                //             return false;
                //         }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date2.getMonth() + 1;
                var day = date2.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
                cal2.hide();
            }
            YAHOO.util.Event.addListener(window, "load", init);
        </script>

        <script type="text/javascript">

            function show(id) {
                document.getElementById(id).style.visibility = "visible";
            }
            function hide(id) {
                document.getElementById(id).style.visibility = "hidden";
            }


            function showStuff(id) {
                document.getElementById(id).style.display = 'block';
            }


            function hidestuff(boxid) {
                document.getElementById(boxid).style.display = "none";
            }

            function showAgent(id) {

                document.getElementById('DisplayAgent' + id).style.display = 'block';
            }
            function Validation1() {
                var fromDate = document.getElementById('<%=txtFromDate.ClientID %>').value;
                var toDate = document.getElementById('<%=txtToDate.ClientID %>').value;
                var currentDate = new Date();
                if (fromDate.length > 0) {
                    if (toDate.length > 0) {
                        if (fromDate.length > 0 && toDate.length > 0) {
                            //                            if (fromDate > toDate) {
                            //                                alert("From Date should be less than ToDate");
                            //                                return false;
                            //                            }
                            if (fromDate > currentDate) {
                                alert("FromDate should be less than CurrentDate");
                                return false;
                            }
                            if (toDate > currentDate) {
                                alert("ToDate Should be less than CurrentDate");
                                return false;
                            }
                        }
                    }
                    else {
                        alert("please selectt ToDate");
                        return false;
                    }
                }
                else {
                    alert("please select FromDate");
                    return false;
                }
                return true;
            }



            function enableGridControls(chkId) {
                var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
                if (document.getElementById(chkId) != null) {
                    document.getElementById(Id + 'ddlStatus').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ddlStatus').className = document.getElementById(chkId).checked ? 'inputEnabled' : 'inputDisabled'

                    var dragDropId = Id + '_btnUploadInvoices';
                    if (document.getElementById(chkId).checked == false) {
                        document.getElementById(Id + 'btnUploadInvoices').disabled = true;
                        closepanel();
                    }
                    else { document.getElementById(Id + 'btnUploadInvoices').disabled = false; }
                }
                getMakersIdStatus();
                var tableRows = document.getElementById('ctl00_cphTransaction_gvMakerQueue').rows.length;
                var count = tableRows - 1;
                var checkedCount = 0;
                
                for (var i = 2; i <= count + 2; i++) {
                    var j = 0;
                    j = j + i;
                    var k = "0";
                    if (j < 10) {
                        j = k + j;
                    }

                    if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus')) {
                        if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus').checked) {
                            checkedCount++;
                        }
                        else {
                            document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl01_chkSelectAll').checked = false;
                        }
                    }
                }
                if (checkedCount == count) {
                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl01_chkSelectAll').checked = true;
                }
                
            }

            function getMakersIdStatus() {
                var count = document.getElementById('<%=hdnMakerRowsCount.ClientID %>').value;
                document.getElementById('<%= hdnIds1.ClientID %>').value = null;
                for (var i = 2; i <= count + 2; i++) {
                    var j = 0;
                    j = j + i;
                    var k = "0";
                    if (j < 10) {
                        j = k + j;
                    }
                    if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus')) {
                        if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus').checked) {
                            var id = document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_makerId').value;
                            var status = document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ddlStatus').value;
                            if (document.getElementById('<%= hdnIds1.ClientID %>').value != null) {
                                document.getElementById('<%= hdnIds1.ClientID %>').value += "," + id + "-" + status;
                            }
                            else {
                                document.getElementById('<%= hdnIds1.ClientID %>').value = id + "-" + status;
                            }
                        }
                    }
                }
            }

            function bindStatus() {
                getMakersIdStatus();
            }

            function resetHiddenFields() {
                document.getElementById('<%= hdfDownload.ClientID %>').value = "";
            }

            function download() {
                var path = getElement('hdfPath_Maker').value;
                var type = getElement('hdfType_Maker').value;
                var docName = getElement('hdfDocName_Maker').value;

                var open = window.open('DownloadDoc.aspx?path=' + path + '&type=' + type + '&docName=' + docName);

            }

            function uploadInvoices(id) {
                var Id = id.substring(0, id.lastIndexOf('_') + 1);
                var checkboxId = Id + "ITchkStatus";
                if (document.getElementById(checkboxId).checked == true) {
                    if (document.getElementById('<%=pnlDragandDrop.ClientID %>').style.display != "block") {
                        document.getElementById('<%=pnlDragandDrop.ClientID %>').style.display = "block";
                        var rowId = id.substring(0, id.lastIndexOf('_'));
                        var makerId = document.getElementById(rowId + "_makerId").value;
                        if (document.getElementById('ctl00_cphTransaction_hdnUploadingRow') != null) {
                            document.getElementById('ctl00_cphTransaction_hdnUploadingRow').value = makerId;
                        }
                        InitDropZone();
                    }
                    else {
                        dropZone.destroy();
                        $(".closepnldocs").on('click', function() {
                            $(".pnldocs").hide();
                            $(".pnldrop").hide();
                            if (document.getElementById('lnkRemove') != null) {
                                document.getElementById('lnkRemove').click();
                            }
                        });
                        document.getElementById('<%=pnlDragandDrop.ClientID %>').style.display = "block";
                        var rowId = id.substring(0, id.lastIndexOf('_'));
                        var makerId = document.getElementById(rowId + "_makerId").value;
                        if (document.getElementById('ctl00_cphTransaction_hdnUploadingRow') != null) {
                            document.getElementById('ctl00_cphTransaction_hdnUploadingRow').value = makerId;
                        }
                        InitDropZone();
                    }
                }
                return false;
            }

            var dropZoneObj;
            var dropZone;

            function InitDropZone() {
                $(".closepnldocs").on('click', function() {
                    $(".pnldocs").hide();
                    $(".pnldrop").hide();
                    if (document.getElementById('lnkRemove') != null) {
                        document.getElementById('lnkRemove').click();
                    }
                });
            
                var chequeId = document.getElementById('<%=hdnUploadingRow.ClientID %>').value;

                Dropzone.autoDiscover = false;

                dropZoneObj = {
                    url: "hn_SimpeFileUploader.ashx?id=" + chequeId,
                    maxFiles: 5,
                    addRemoveLinks: true,
                    success: function(file, response) {
                        var imgName = response;
                        file.previewElement.classList.add("dz-success");
                        console.log("Successfully uploaded :" + imgName);
                    },
                    error: function(file, response) {
                        file.previewElement.classList.add("dz-error");
                    }
                }
                dropZone = new Dropzone('#ctl00_cphTransaction_dZUpload', dropZoneObj);
            }

            function closeDrop() {
                dropZone.destroy();
                
                $(".closepnldocs").on('click', function() {
                    $(".pnldocs").hide();
                    $(".pnldrop").hide();
                    if (document.getElementById('lnkRemove') != null) {
                        document.getElementById('lnkRemove').click();
                    }
                });
            }

            function selectAllChecked() {
                if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl01_chkSelectAll').checked == true) {
                    if (document.getElementById('<%= hdnMakerRowsCount.ClientID %>').value != null && document.getElementById('<%= hdnMakerRowsCount.ClientID %>').value != "") {
                        var gridViewRowsCount = document.getElementById('<%= hdnMakerRowsCount.ClientID %>').value;
                        var j = 2;
                        for (var i = 0; i < gridViewRowsCount; i++) {
                            if (j < 10) {
                                if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_ITchkStatus') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_ITchkStatus').checked = true
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_ddlStatus').disabled = false;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_btnUploadInvoices').disabled = false;
                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus').checked = true;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ddlStatus').disabled = false;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_btnUploadInvoices').disabled = false;
                                }
                            }
                            j++;
                        }
                    }
                    getMakersIdStatus();
                }
                else {
                    document.getElementById('<%= hdnIds1.ClientID %>').value = null;
                    if (document.getElementById('<%= hdnMakerRowsCount.ClientID %>').value != null && document.getElementById('<%= hdnMakerRowsCount.ClientID %>').value != "") {
                        var gridViewRowsCount = document.getElementById('<%= hdnMakerRowsCount.ClientID %>').value;
                        var j = 2;
                        for (var i = 0; i < gridViewRowsCount; i++) {

                            if (j < 10) {
                                if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_ITchkStatus') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_ITchkStatus').checked = false;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_ddlStatus').disabled = true;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl0' + j + '_btnUploadInvoices').disabled = true;
                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ITchkStatus').checked = false;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_ddlStatus').disabled = true;
                                    document.getElementById('ctl00_cphTransaction_gvMakerQueue_ctl' + j + '_btnUploadInvoices').disabled = true;
                                }
                            }
                            j++;
                        }
                    }
                }
            }


            function saveClickDetails(id, chequeId) {
                PageMethods.lnkDocuments_Click(id, chequeId, onComplete);
            }

            function onComplete() {

            }
        </script>

        <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
        </iframe>
        <asp:HiddenField runat="server" ID="hdfPath_Maker"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hdfType_Maker"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hdfDocName_Maker"></asp:HiddenField>
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none;
                z-index: 9999">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none;
                z-index: 9999">
            </div>
        </div>
        <asp:Label ID="lblErrorMessage" runat="server" Visible="false" Style="float: left;
            color: Red;" class="padding-5 yellow-back width-100 center margin-top-5"></asp:Label>
        <div title="Param" id="divParam">
            <asp:Panel runat="server" ID="pnlParam" Visible="true">
                <div class="paramcon">
                    <div class="col-md-12 padding-0 marbot_10">
                        <table>
                            <tr>
                                <td>
                                    <div>
                                        From Date:
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                                        Width="100px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(null)" onclick="showCal1()">
                                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        To Date:
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(null)" onclick="showCal2()">
                                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:Label ID="lblCountryName" runat="server" Text="Country Name :"></asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:DropDownList ID="ddlCountryName" runat="server">
                                            <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="INDIA" Value="INDIA"></asp:ListItem>
                                            <asp:ListItem Text="UAE" Value="UAE"></asp:ListItem>
                                            <asp:ListItem Text="QATAR" Value="QATAR"></asp:ListItem>
                                            <asp:ListItem Text="KSA" Value="KSA"></asp:ListItem>
                                            <asp:ListItem Text="KUWAIT" Value="KUWAIT"></asp:ListItem>
                                            <asp:ListItem Text="BAHRAIN" Value="BAHRAIN"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:Label ID="lblCompanyName" runat="server" Text="Company Name"></asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:TextBox ID="tbCompanyName" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick"
                                            OnClientClick="return Validation1();" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <asp:HiddenField ID="hdnIds1" runat="server" />
        <asp:HiddenField ID="hdnMakerRowsCount" runat="server" />
        <div>
            <asp:GridView ID="gvMakerQueue" Width="100%" CssClass="tblvms" runat="server" AllowPaging="false"
                AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound" >
                <%--OnRowCommand="makerQueue_RowCommand"--%>
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" OnClick="selectAllChecked()" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="ITchkStatus" runat="server" OnClick="enableGridControls(this.id);"
                                Width="20px" CssClass="label"></asp:CheckBox>
                            <asp:HiddenField ID="makerId" runat="server" Value='<%# Eval("Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
                    <asp:BoundField DataField="DocDate" HeaderText="Document Date" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="CountryName" HeaderText="Country Name" />
                    <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                    <asp:BoundField DataField="BankName" HeaderText="BankName" />
                    <asp:BoundField DataField="DebitAcNo" HeaderText="DebitAcNo" />
                    <asp:TemplateField HeaderText="Beneficiary Name">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnBeneficiaryName" runat="server" Text='<%# Eval("BeneficiaryName") %>'
                                OnClick="lbtnBeneficiaryName_OnClick_Maker" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Currency" HeaderText="Currency" />
                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                    <asp:BoundField DataField="TransferType" HeaderText="TransferType" />
                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                    <asp:BoundField DataField="VoucherNo" HeaderText="VoucherNo" />
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:HiddenField ID="IThdftranxStatus" runat="server" Value='<%# Bind("Id") %>'>
                            </asp:HiddenField>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' Visible="false" />
                            <asp:DropDownList CssClass="form-control" Width="100px" ID="ddlStatus" runat="server"
                                Enabled="false" OnChange="bindStatus()">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Upload Invoices">
                        <ItemTemplate>
                            <%--<input type="button" id="btnUploadInvoices" disabled="disabled" name="btnUploadInvoices" onclick="return uploadInvoices(this.id);" value="Upload" />--%>
                                <asp:LinkButton ID="btnUploadInvoices" runat="server" Enabled="false" Text="Upload" OnClientClick="return uploadInvoices(this.id);" />
                                <%--OnClick="btnUploadedInvoices"--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <br />
        <br />
        <asp:HiddenField ID="hdnUploadingRow" runat="server" Value="" />
        <div align="right">
            <asp:Button ID="btnUpdate" Text="Update" runat="server" Style="display: none" OnClick="btnUpdate_OnClick"
                align="right" />
            <asp:Panel class="pnldrop" ID="pnlDragandDrop" runat="server" Style="position: absolute;
                display: none; top: 100px; width: 500; height: 500; z-index: 9999; background-color: White;
                border-color: blue">
                <span id="closeDropZone" class="closepnldocs" onclick="closeDrop()" style="color: Red;
                    font-size: 15px; font-weight: bold;">X</span>
                <div id="dZUpload" class="dropzone" runat="server">
                    <div class="dz-default dz-message">
                        Drop image here.
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hdnRowsCount" runat="server" />
            <asp:Panel class="pnldocs ManageQueuePanelDocs" ID="pnlDocuments" runat="server"
                Visible="false" >
                <div class="modal-backdrop fade in">
                </div>
                <div class="PanelDocs-inner-wrapper" style="text-align: right">
                    <div style="text-align: right" >
                        <span id="closepnldocs" onclick="hidepaneldoc();" class="close-btn-paneldocs glyphicon glyphicon-remove">
                        </span>
                        
                    </div>
                    <div class="modal-header" style="text-align: left">
                        <h4 class="modal-title">
                            Support documents</h4>
                    </div>
                    <div id="ViewFiles" class="paneldocs-contents" runat="server" style="z-index: 9999;
                        text-align: left">
                        <asp:HiddenField ID="hdnBeneficiaryName_Maker" runat="server"></asp:HiddenField>
                    </div>
                    <asp:LinkButton ID="lnlDownloadAll" runat="server" Text="Download All" CssClass="lnkEnabled"
                        OnClick="lnlDownloadAll_Click_Maker"></asp:LinkButton>
                </div>
            </asp:Panel>
        </div>
    </div>
    <%--Checker queue part--%>
    <div id="CheckerQueue" visible="false" runat="server">

        <script type="text/javascript">
            var cal1;
            var cal2;

            function init() {

                //    showReturn();
                var today = new Date();
                // For making dual Calendar use CalendarGroup  for single Month use Calendar     
                cal1 = new YAHOO.widget.Calendar("cal1", "fromContainer1");
                //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                cal1.cfg.setProperty("title", "Select CheckIn date");
                cal1.cfg.setProperty("close", true);
                cal1.selectEvent.subscribe(setDates1);
                cal1.render();

                cal2 = new YAHOO.widget.Calendar("cal2", "toContainer2");
                cal2.cfg.setProperty("title", "Select CheckOut date");
                cal2.selectEvent.subscribe(setDates2);
                cal2.cfg.setProperty("close", true);
                cal2.render();
            }
            function showCal1_checker() {
                $('toContainer2').context.styleSheets[0].display = "none";
                $('fromContainer1').context.styleSheets[0].display = "block";
                init();
                cal1.show();
                cal2.hide();
            }


            var departureDate = new Date();
            function showCal2_checker() {
                $('fromContainer1').context.styleSheets[0].display = "none";
                cal1.hide();
                init();
                // setting Calender2 min date acoording to calendar1 selected date
                var date1 = document.getElementById('<%= txtFromDate1.ClientID%>').value;
                //var date1=new Date(tempDate.getDate()+1);

                if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                    var depDateArray = date1.split('/');

                    var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                    cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                    cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    cal2.render();
                }
                document.getElementById('toContainer2').style.display = "block";
            }
            function setDates1() {
                var date1 = cal1.getSelectedDates()[0];

                $('Iframe1').context.styleSheets[0].display = "none";
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();

                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());


                departureDate = cal1.getSelectedDates()[0];
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                //			
                var month = date1.getMonth() + 1;
                var day = date1.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%= txtFromDate1.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

                //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
                //cal2.render();

                cal1.hide();

            }
            function setDates2() {
                var date1 = document.getElementById('<%=txtFromDate1.ClientID %>').value;
                if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                    return false;
                }

                var date2 = cal2.getSelectedDates()[0];

                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
                var difference = returndate.getTime() - depdate.getTime();

                //         if (difference < 1) {
                //             document.getElementById('errMess').style.display = "block";
                //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                //             return false;
                //         }
                //         if (difference == 0) {
                //             document.getElementById('errMess').style.display = "block";
                //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                //             return false;
                //         }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date2.getMonth() + 1;
                var day = date2.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%=txtToDate1.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
                cal2.hide();
            }
            YAHOO.util.Event.addListener(window, "load", init);
        </script>

        <script type="text/javascript">
            function show(id) {
                document.getElementById(id).style.visibility = "visible";
            }
            function hide(id) {
                document.getElementById(id).style.visibility = "hidden";
            }


            function showStuff(id) {
                document.getElementById(id).style.display = 'block';
            }


            function hidestuff(boxid) {
                document.getElementById(boxid).style.display = "none";
            }

            function showAgent(id) {

                document.getElementById('DisplayAgent' + id).style.display = 'block';
            }
            function Validation() {
                var fromDate = document.getElementById('<%=txtFromDate1.ClientID %>').value;
                var toDate = document.getElementById('<%=txtToDate1.ClientID %>').value;
                var currentDate = new Date();
                if (fromDate.length > 0) {
                    if (toDate.length > 0) {
                        if (fromDate.length > 0 && toDate.length > 0) {
                            if (fromDate > toDate) {
                                alert("From Date should be less than ToDate");
                                return false;
                            }
                            if (fromDate > currentDate) {
                                alert("FromDate should be less than CurrentDate");
                                return false;
                            }
                            if (toDate > currentDate) {
                                alert("ToDate Should be less than CurrentDate");
                                return false;
                            }
                        }
                    }
                    else {
                        alert("please selectt ToDate");
                        return false;
                    }
                }
                else {
                    alert("please select FromDate");
                    return false;
                }
                return true;
            }



            function enableGridControls1(chkId) {
                var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
                if (document.getElementById(chkId) != null) {
                    document.getElementById(Id + 'ddlStatus1').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ddlStatus1').className = document.getElementById(chkId).checked ? 'inputEnabled' : 'inputDisabled'

                }
                getCheckersIdStatus1();
                var tableRowsCount = document.getElementById('ctl00_cphTransaction_gvCheckerQueue').rows.length;
                var count = tableRowsCount - 1;
                var checkedCount = 0;

                for (var i = 2; i <= count + 2; i++) {
                    var j = 0;
                    j = j + i;
                    var k = "0";
                    if (j < 10) {
                        j = k + j;
                    }

                    if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1')) {
                        if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1').checked) {
                            checkedCount++;
                        }
                        else {
                            document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl01_chkSelectAll').checked = false;
                        }
                    }
                }
                if (checkedCount == count) {
                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl01_chkSelectAll').checked = true;
                }
            }

            function getCheckersIdStatus1() {
                var count = document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value;
                document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value = null;
                var rowsCount = parseInt(count) + parseInt("2");
                for (var i = 2; i <= rowsCount; i++) {
                    var j = 0;
                    j = j + i;
                    var k = "0";
                    if (j < 10) {
                        j = k + j;
                    }

                    if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1')) {
                        if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1').checked) {
                            var id = document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_checkerId1').value;
                            var status = document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ddlStatus1').value;
                            if (document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value != null) {
                                document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value += "," + id + "-" + status;
                            }
                            else {
                                document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value = id + "-" + status;
                            }
                        }
                    }
                }
                var hdnIds = document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value;
            }

            function bindStatus_Checker() {
                getCheckersIdStatus1();
            }


            function download() {
                var path = getElement('hdfPath').value;
                var type = getElement('hdfType').value;
                var docName = getElement('hdfDocName').value;

                // getElement('imgPreview').ImageUrl = "~/images/Common/Preview.png";
                var open = window.open('DownloadDoc.aspx?path=' + path + '&type=' + type + '&docName=' + docName);
                //                getElement('hdfDownload').value = "download";
                //                document.forms[0].submit();
            }

            function selectAllCheckedChecker() {
                if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl01_chkSelectAll').checked == true) {
                    if (document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value != null && document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value != "") {
                        var gridViewRowsCount = document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value;
                        var j = 2;
                        for (var i = 0; i < gridViewRowsCount; i++) {
                            if (j < 10) {
                                if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + j + '_ITchkStatus1') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + j + '_ITchkStatus1').checked = true
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + j + '_ddlStatus1').disabled = false;
                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1').checked = true;
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ddlStatus1').disabled = false;
                                }
                            }
                            j++;
                        }
                    }
                    getCheckersIdStatus1();
                }
                else {
                    document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value = null;
                    if (document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value != null && document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value != "") {
                        var gridViewRowsCount = document.getElementById('<%= hdnCheckerRowsCount.ClientID %>').value;
                        var j = 2;
                        for (var i = 0; i < gridViewRowsCount; i++) {

                            if (j < 10) {
                                if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + j + '_ITchkStatus1') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + j + '_ITchkStatus1').checked = false;
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + j + '_ddlStatus1').disabled = true;
                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1') != null) {
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ITchkStatus1').checked = false;
                                    document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl' + j + '_ddlStatus1').disabled = true;
                                }
                            }
                            j++;
                        }
                    }
                }
            }


            function saveClickDetails(id, chequeId) {
                PageMethods.lnkDocuments_Click(id, chequeId, onComplete);
            }
            function onComplete() {

            }
        </script>

        <iframe id="Iframe1" style="position: absolute; display: none;" frameborder="0">
        </iframe>
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="fromContainer1" style="position: absolute; top: 120px; left: 250px; display: none;
                z-index: 9999">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="toContainer2" style="position: absolute; top: 120px; left: 500px; display: none;
                z-index: 9999">
            </div>
        </div>
        <asp:Label ID="lblErrorMessage1" runat="server" Visible="false" Style="float: left;
            color: Red;" class="padding-5 yellow-back width-100 center margin-top-5"></asp:Label>
        <div title="Param" id="div5">
            <asp:Panel runat="server" ID="Panel1" Visible="true">
                <div class="paramcon">
                    <div class="col-md-12 padding-0 marbot_10">
                        <table>
                            <tr>
                                <td>
                                    <div>
                                        From Date:
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtFromDate1" runat="server" CssClass="inputEnabled form-control"
                                                        Width="100px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(null)" onclick="showCal1_checker()">
                                                        <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        To Date:
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtToDate1" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(null)" onclick="showCal2_checker()">
                                                        <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:Label ID="lblCountryName1" runat="server" Text="Country Name :"></asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:DropDownList ID="ddlCountryName1" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="INDIA" Value="INDIA"></asp:ListItem>
                                            <asp:ListItem Text="UAE" Value="UAE"></asp:ListItem>
                                            <asp:ListItem Text="QATAR" Value="QATAR"></asp:ListItem>
                                            <asp:ListItem Text="KSA" Value="KSA"></asp:ListItem>
                                            <asp:ListItem Text="KUWAIT" Value="KUWAIT"></asp:ListItem>
                                            <asp:ListItem Text="BAHRAIN" Value="BAHRAIN"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:Label ID="lblCompanyName1" runat="server" Text="Company Name"></asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:TextBox ID="tbCompanyName1" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <asp:Button ID="btnSearch_Checker" runat="server" CssClass="btn but_b" Text="Search"
                                            OnClick="btnSearch_Checker_OnClick" OnClientClick="return Validation();" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <asp:HiddenField ID="hdnCheckerRowsCount" runat="server" />
        <asp:HiddenField ID="hdnIds_CheckerIds1" runat="server" />
        <asp:HiddenField ID="hdnIds_Beneficiary" runat="server" />
        <div>
            <table width="100%" id="tabSearch" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:GridView ID="gvCheckerQueue" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                            OnRowDataBound="OnRowDataBound_Checker" GridLines="None" CssClass="grdTable">
                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left" Font-Size="10" Font-Bold="true"></HeaderStyle>
                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" OnClick="selectAllCheckedChecker()" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ITchkStatus1" runat="server" OnClick="enableGridControls1(this.id);"
                                            Width="20px" CssClass="label"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
                                <asp:BoundField DataField="DocDate" HeaderText="Document Date" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="CountryName" HeaderText="Country Name" />
                                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                                <asp:BoundField DataField="BankName" HeaderText="Bank Name" />
                                <asp:BoundField DataField="DebitAcNo" HeaderText="DebitAcNo" />
                                <asp:TemplateField HeaderText="Beneficiary Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnBeneficiaryName" runat="server" Text='<%# Eval("BeneficiaryName") %>'
                                            OnClick="lbtnBeneficiaryName_OnClick" />
                                        <asp:HiddenField ID="checkerId1" runat="server" Value='<%# Eval("Id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />--%>
                                <asp:BoundField DataField="Currency" HeaderText="Currency" />
                                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                <asp:BoundField DataField="TransferType" HeaderText="TransferType" />
                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                <asp:BoundField DataField="VoucherNo" HeaderText="VoucherNo" />
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("Status") %>' Visible="false" />
                                        <asp:DropDownList ID="ddlStatus1" runat="server" Enabled="false" OnChange="bindStatus_Checker()">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <div align="right">
            <asp:Button ID="btnUpdate_Checker" Text="Update" CssClass="btn but_b" runat="server" OnClick="btnUpdate_OnClick_Checker"
                align="right" />
        </div>
        <div>
            <asp:Panel class="pnldocs ManageQueuePanelDocs" ID="pnlDocuments1" runat="server"
                Visible="false" EnableViewState="true">
                <div class="modal-backdrop fade in">
                </div>
                <div class="PanelDocs-inner-wrapper" style="text-align: right">
                    <div style="text-align: right">
                        <span id="closepnldocs1" onclick='hidepaneldoc1();' class="close-btn-paneldocs glyphicon glyphicon-remove">
                        </span>
                    </div>
                    <div class="modal-header" style="text-align: left">
                        <h4 class="modal-title">
                            Support documents</h4>
                    </div>
                    <div id="ViewCheckerFiles" class="paneldocs-contents" runat="server" style="z-index: 9999;
                        text-align: left; margin-left: 10px">
                       
                    </div>
                    <asp:LinkButton ID="lnlDownloadAll1" runat="server" Text="Download All" CssClass="lnkEnabled"
                        OnClick="lnlDownloadAll_Click"></asp:LinkButton>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfPath"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfType"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfDocName"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfPreviewPath"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfDownload"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfChequeId" />
    <asp:HiddenField ID="hdnBeneficiaryName" runat="server" />
    <div id="viewFiles1" runat="server" style="z-index: 9999">
        <a id="aViewImage" runat="server" class="image-link" visible="false">
            <img src="" id="image" alt="" runat="server" width="175" height="175" onclick="download()" />
        </a><a id="aFileView" runat="server" class="pdf-popup" href="" visible="false" onclick="download()">
        </a>
    </div>
    </asp:Panel>
    <asp:HiddenField ID="checkerRowsCount" runat="server" />
    <%------------for viewing image with transperency--------------%>

    <script>	
$('.image-link, .pdf-popup').magnificPopup({
    callbacks: {
      elementParse: function(item) {
         if(item.el.context.className == 'pdf-popup') {
           item.type = 'iframe';
         } else {
           item.type = 'image';
         }
      }
    },
    gallery:{enabled:true},
    type: 'image',
});
    </script>

    <%------------for viewing image with transperency--------------%>
    <%------------for drag and drop functionality--------------%>

    <script src="DropzoneJs_scripts/dropzone.js"></script>

    <script type="text/javascript">

        function hidepaneldoc() {
            //alert("came to panel odcs");
            document.getElementById("ctl00_cphTransaction_pnlDocuments").style.display = "none";
        }
        function hidepaneldoc1() {
            //alert("came to panel odcs");
            document.getElementById("ctl00_cphTransaction_pnlDocuments1").style.display = "none";
        }

        function closepanel() {
            $(".closepnldocs").on('click', function() {
                $(".pnldocs").hide();
                $(".pnldrop").hide();
                if (document.getElementById('lnkRemove') != null) {
                    document.getElementById('lnkRemove').click();
                }
            });
        }
    </script>

    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
