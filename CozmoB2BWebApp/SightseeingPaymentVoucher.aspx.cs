﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using System.Collections.Generic;
using System.Data;

public partial class SightseeingPaymentVoucher : CT.Core.ParentPage
{
    protected BookingResponse bookingResponse;
    protected SightseeingItinerary itinerary;
    protected string logoPath=string.Empty;
    protected BookingDetail booking;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["ConfNo"] != null)
            {
                

                string logoPath = string.Empty;
                if (Settings.LoginInfo.AgentId > 1)
                {
                    logoPath = ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                    imgHeaderLogo.ImageUrl = logoPath;
                    
                }
                else
                {
                    imgHeaderLogo.ImageUrl = "images/logocozmo.jpg";
                }

                itinerary = new SightseeingItinerary();
               
                string confirmationNo = Request.QueryString["ConfNo"].ToString();

                if (Session["SItinerary"] != null)
                {
                    itinerary = (SightseeingItinerary)Session["SItinerary"];
                }
                if (Session["BookingResponse"] != null)
                {
                    bookingResponse = (BookingResponse)Session["BookingResponse"];
                }
                else if (Request.QueryString["ConfNo"] == null)
                {
                    Response.Redirect("AbandonSession.aspx");
                }


                booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(SightseeingItinerary.GetSightseeingId(confirmationNo), ProductType.SightSeeing));
                try
                {
                    //AgentMaster agent = new AgentMaster(booking.AgencyId);
                    Product[] products = BookingDetail.GetProductsLine(booking.BookingId);

                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.SightSeeing)
                        {
                            itinerary.Load(products[i].ProductId);
                            Session["SItinerary"] = itinerary;

                            break;
                        }
                    }


                    SendVoucherMail();
                    SendVoucherBccMail();


                    Session["ssReq"] = null;
                    Session["Markup"] = null;
                    Session["cSessionId"] = null;
                    Session["BookingResponse"] = null;
                    Session["searchResult"] = null;
                    Session["SItinerary"] = null;
                    Session["totalResultList"] = null;
                    Session["filteredResultList"] = null;
                }
                catch (Exception exp)
                {
                    //errorMessage = "Invalid Booking";
                    Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Exception Loading itinerary. Message: " + exp.Message, Request["REMOTE_ADDR"]);
                }
                    
                
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get Voucher. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    protected void SendVoucherMail()
    {
        try
        {
            if (itinerary != null)
            {
                lblTourName.Text = itinerary.ItemName + ", "+ itinerary.AdultCount.ToString() + "Adults," + itinerary.ChildCount.ToString() + "Children";
                lblServiceType.Text = "Tour";
                lblBookedBy.Text = Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName;
                lblBookingStatus.Text = itinerary.BookingStatus.ToString();
                lblBookingDate.Text = itinerary.CreatedOn.ToString("dd MMM yyyy");
                lblTransactionDate.Text = itinerary.TourDate.ToString("dd MMM yyyy");
                lblCity.Text = itinerary.CityName;
                //lblPassengers.Text = (itinerary.AdultCount + itinerary.ChildCount).ToString();
                //lblCountry.Text = itinerary.CountryName;

                //lblDuration.Text = itinerary.Duration == "" ? "N/A" : itinerary.Duration;
                //lblTourAddress.Text = itinerary.Address == "" ? "N/A" : itinerary.Address;

                //Use ceiling instead of Round, Changed on 06082016
                //lblTotal.Text = (Math.Ceiling(itinerary.Price.NetFare + itinerary.Price.Markup + itinerary.Price.B2CMarkup) + itinerary.Price.AsvAmount + Math.Ceiling(itinerary.Price.OutputVATAmount)).ToString("N" + Settings.LoginInfo.DecimalValue);
                //lblTotal.Text = (Math.Ceiling(itinerary.Price.NetFare).ToString("N" + Settings.LoginInfo.DecimalValue));
                if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 0)
                {
                    string[] aData = itinerary.DepPointInfo.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string depoint in aData)
                    {
                        //lblPickupPoint.Text += depoint.Split(',')[0];
                        lblPickupTime.Text += depoint;
                    }
                    
                }
                else
                {
                    //lblPickupPoint.Text = "N/A";
                    lblPickupTime.Text = "N/A";
                }
                //lblPickupTime.Text = itinerary.DepTime.Length > 0 ? itinerary.DepTime : "N/A";
                //lblAdultCount.Text = itinerary.AdultCount.ToString();
                //lblChildCount.Text = itinerary.ChildCount.ToString();
                lblBookingRef.Text = itinerary.SightseeingId.ToString();
                //lblStartingPoint.Text = lblPickupPoint.Text;
                //lblEndingPoint.Text = lblPickupPoint.Text;
                lblConfirmation.Text = itinerary.ConfirmationNo;
                lblGuestName.Text = itinerary.PaxNames[0];
                lblAdditional.Text = "none";
                lblPickingPoint.Text = itinerary.PickupPoint;
                //lblTourLanguage.Text = itinerary.Language.Split('#')[0];
                //lblPhone.Text = itinerary.TelePhno;
                //lblSupplierInfo.Text = itinerary.SupplierInfo;
                //lblSpecialName.Text = itinerary.SpecialName.Length > 0 ? itinerary.SpecialName : "N/A";

                DataTable dtSupplierdetails= SightseeingItinerary.GetSupplierEmail(Convert.ToInt32(itinerary.ItemCode));

                if (dtSupplierdetails!=null && dtSupplierdetails.Rows.Count > 0)
                {
                    lblSupplierName.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["supplierName"]);
                    lblServiceName.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["SupplierServiceName"]);
                    lblTelphone.Text= Convert.ToString(dtSupplierdetails.DefaultView[0]["SupplierTelephone"]);
                    //lblPickingPoint.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["pickup_point"]);
                }
                /*    */
            }
            int agentId = 0;

            if (Settings.LoginInfo != null)
            {
                agentId = Settings.LoginInfo.AgentId;
            }

            string serverPath = "http://ctb2b.cozmotravel.com/";

            //if (Request.Url.Port > 0)
            //{
            //    serverPath = "http://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            //}
            //else
            //{
            //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            //}

            if (agentId > 1)
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                //imgLogo.ImageUrl = logoPath;
            }
            else
            {
                logoPath = serverPath  + ConfigurationManager.AppSettings["AgentImage"] +"3.png"; //@@@@ image name required to check in server location
            }



            string myPageHTML = "";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            printableArea.Style.Add("display", "block");
            printableArea.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
           
            List<string> toArray = new List<string>();
            toArray.Add(itinerary.Email);
           

            //AgentMaster agency = new AgentMaster(agentId);
            //toArray.Add(agency.Email1);
            if (ViewState["MailSent"] == null)
            {
                ViewState["MailSent"] = true;
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Sightseeing Confirmation - " + itinerary.ConfirmationNo, myPageHTML, new Hashtable(), Settings.LoginInfo.Email);
                }
                catch { }
            }
            printableArea.Style.Add("display", "none");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Sightseeing Voucher Email: reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void SendVoucherBccMail()
    {
        String SupplierEmail = string.Empty, Bccmail=string.Empty ;
        try
        {
            if (itinerary != null)
            {
                lblSTourName.Text = itinerary.ItemName + ", " + itinerary.AdultCount.ToString() + "Adults," + itinerary.ChildCount.ToString() + "Children" ;
                lblSserviceType.Text = "Tour";
                lblSBookingDate.Text = itinerary.CreatedOn.ToString("dd MMM yyyy");
                lblSBookedBy.Text= Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName;
                lblSBookingStatus.Text = itinerary.BookingStatus.ToString();
                lblSTransactionDate.Text = itinerary.TourDate.ToString("dd MMM yyyy");
                lblSCity.Text = itinerary.CityName;
                //lblSPassengers.Text = (itinerary.AdultCount + itinerary.ChildCount).ToString();
                //lblSCountry.Text = itinerary.CountryName;

                //lblSDuration.Text = itinerary.Duration == "" ? "N/A" : itinerary.Duration;
                //lblSTourAddress.Text = itinerary.Address == "" ? "N/A" : itinerary.Address;

                //Use ceiling instead of Round, Changed on 06082016
                lblSTotal.Text = (Math.Ceiling(itinerary.Price.NetFare).ToString("N" + Settings.LoginInfo.DecimalValue));
                if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 0)
                {
                    string[] aData = itinerary.DepPointInfo.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string depoint in aData)
                    {
                        //lblSPickupPoint.Text += depoint.Split(',')[0];
                        lblSPickUpTime.Text += depoint;
                        
                    }
                    
                }
                else
                {
                    //lblSPickupPoint.Text = "N/A";
                    lblSPickUpTime.Text= "N/A";
                }
                //lblSPickUpTime.Text = itinerary.DepTime.Length > 0 ? itinerary.DepTime : "N/A";
                //lblSAdultCount.Text = itinerary.AdultCount.ToString();
                //lblSChildCount.Text = itinerary.ChildCount.ToString();
                //lblSStartingPoint.Text = lblSPickupPoint.Text;
                //lblSEndingPoint.Text = lblSPickupPoint.Text;
                lblSBookingRef.Text = itinerary.SightseeingId.ToString();
                lblSConfirmation.Text = itinerary.ConfirmationNo;
                lblSGuestName.Text = itinerary.PaxNames[0];
                lblSAdditionalRequests.Text = "none";
                lblPickingPoint.Text = itinerary.PickupPoint;
                //lblSTourLanguage.Text = itinerary.Language.Split('#')[0];
                //lblSPhone.Text = itinerary.TelePhno;
                //lblSSupplierInfo.Text = itinerary.SupplierInfo;
                //lblSSprcialName.Text = itinerary.SpecialName.Length > 0 ? itinerary.SpecialName : "N/A";
                DataTable dtsupplier = SightseeingItinerary.GetSupplierEmail(Convert.ToInt32(itinerary.ItemCode));
                if (dtsupplier != null && dtsupplier.Rows.Count > 0)
                {
                   SupplierEmail = Convert.ToString(dtsupplier.DefaultView[0]["supplierEmail"]);
                    lblSSupplierName.Text = Convert.ToString(dtsupplier.DefaultView[0]["supplierName"]);
                    lblSServiceName.Text = Convert.ToString(dtsupplier.DefaultView[0]["SupplierServiceName"]);
                    lblSTelephone.Text= Convert.ToString(dtsupplier.DefaultView[0]["SupplierTelephone"]);
                    //lblSEmail.Text = Convert.ToString(dtsupplier.DefaultView[0]["supplierEmail"]);
                    //lblSPickingPoint.Text= Convert.ToString(dtsupplier.DefaultView[0]["pickup_point"]);
                }

            }
            int agentId = 0;

            if (Settings.LoginInfo != null)
            {
                agentId = Settings.LoginInfo.AgentId;
            }

            string serverPath = "http://ctb2b.cozmotravel.com/";

           

            if (agentId > 1)
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                //imgLogo.ImageUrl = logoPath;
            }
            else
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + "3.png"; //@@@@ image name required to check in server location
            }



            string myPageHTML = "";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            PrintSupplerEmail.Style.Add("display", "block");
            PrintSupplerEmail.RenderControl(htw);
            myPageHTML = sw.ToString();
           

           
            //List<string> toArray = new List<string>();
            //toArray.Add(SupplierEmail);

            //Bcc mail getting bke_app_config
            List<AgentAppConfig> appConfigs = new List<AgentAppConfig>();
            appConfigs = new AgentAppConfig { Source =itinerary.Source.ToString() }.GetConfigDataBySource();
            if (appConfigs != null)
            {
                foreach (AgentAppConfig config in appConfigs)
                {
                    if (config.AppKey == "BCCSightSeeing")
                    {

                        Bccmail = config.Description.ToString();
                        break;
                    }
                }
            }
            List<string> toArray = new List<string>();
            toArray.Add(Bccmail);
            string[] Attachement=new string[0];
            //AgentMaster agency = new AgentMaster(agentId);
            //toArray.Add(agency.Email1);
            //if (ViewState["MailSent"] == null)
            {
                ViewState["MailSent"] = true;


                try
                {
                    //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Sightseeing Confirmation - " + itinerary.ConfirmationNo, myPageHTML, new Hashtable(), Bccmail);
                    CT.Core.Email.Send(toArray, SupplierEmail, ConfigurationManager.AppSettings["fromEmail"], "Sightseeing Confirmation - " + itinerary.ConfirmationNo, myPageHTML, new Hashtable(), Attachement);
                }
                catch { }
            }
            PrintSupplerEmail.Style.Add("display", "none");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Sightseeing Voucher Email: reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}

