﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="ItineraryPrintAddServiceDetails" Codebehind="ItineraryPrintAddServiceDetails.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <script type="text/javascript">

        function updateAgentBalance() {

            
            window.parent.document.getElementById('ctl00_lblAgentBalance').innerHTML = document.getElementById('hdfCurBal').value;
            
        }

        function printPage() {
            document.getElementById('btnPrint').style.display = "none";
            window.print();
            document.getElementById('btnPrint').style.display = "block";
        }

        

       
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
  
<div style="width:660px; margin:auto; margin-top:10%">
<asp:HiddenField runat="server" ID="hdfCurBal" />

  <input style="width:90px;" id="btnPrint" onclick="return printPage();" class="button" value="Print" type="button" />
  
  </div>


    <div style="width:660px; margin:auto; border: solid 1px #666">
     <div style="margin-bottom:10px;">
     <table width="100%" border="0">
                
                
                <tr>
                <td width="30%" align="left">
       
                </td>
                
                <td width="40%" style="font:14px; font-weight:bold; text-align:center">Receipt</td>
                
                
                <td width="30%" align="right">

                 <img src="images/logo.gif" alt="" height="40px">
                </td>
                
                </tr>
                
                
                </table>
     </div>
       
        <div style="line-height:24px"> 
        <table style="font-size:15px; " width="100%" cellpadding="0" cellspacing="0">

            
            <tr>
                <td>
                   <strong> Passenger Name:</strong>
                </td>

                <td>
                    <asp:Label runat="server" ID="lblPaxName"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                   <strong> Service Name:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblServiceName"></asp:Label>
                </td>
            </tr>



              <tr>
                <td>
                   <strong> PNR Number:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPNR"></asp:Label>
                </td>
            </tr>


            <tr>
                <td>
                   <strong> Travel Date:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblTravelDate"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                  <strong>  Nationality:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblNationality"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                   <strong> PassportNumber:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPassportNumber"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Routing:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRouting"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Reference Number:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRefNo"></asp:Label>
                </td>
            </tr>
            
            
            <tr>
                <td>
                   <strong> Remarks:</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRemarks"></asp:Label>
                </td>
            </tr>
        </table>        
      </div>




      <div style=" padding-left:10px; padding-bottom:10px; font-size:11px"> <br /> <span><i>Note: 100 AED to be paid at HALA</i></span></div>
       
        
    </div>

   

   <style type="text/css"> 
   
   td { padding: 2px 0px 2px 10px }
   
   body {  font-family:Arial, Helvetica, sans-serif }
   
   
   .button {
    background: url(images/bty.jpg);
    background-repeat: repeat;
    background-repeat: repeat-x;
    border: solid 1px #ccc;
    color: #333;
    cursor: pointer;
    font-size: 11px;
    font-family: Arial;
    font-weight: bold;
    padding: 2px 2px 2px 2px;
}

   
   
   </style>
    </form>
</body>
</html>
