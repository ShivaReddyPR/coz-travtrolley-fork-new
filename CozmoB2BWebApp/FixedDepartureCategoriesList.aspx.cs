﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class FixedDepartureCategoriesListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected ActivityDetails details = null;
    protected Activity activity = null;
    protected string imageServerPath;
    protected decimal minPrice = 0;
    protected bool stockEnquiry = false;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["FixedDeparture"] != null)
        {
            if (!IsPostBack)
            {
                activity = Session["FixedDeparture"] as Activity;
                details = ActivityDetails.GetActivityDetailById((int)activity.Id);
                imageServerPath = Convert.ToString(ConfigurationManager.AppSettings["ActivityImagesFolder"]);
                lblActivity.Text = activity.Name;
                lblName.Text = activity.Name;
                lblTitle.Text = activity.Name;
                lblAvail.Text = activity.Name;

                lblDays.Text = activity.Days.ToString();
                lblDate.Text = activity.StartFrom.ToString("dd MMM yyyy");

                DataTable dtPrice = activity.PriceDetails;
                DataTable dtTransHeader = activity.TransactionHeader;

                DataTable dtPaxPrice = new DataTable();
                if (dtPaxPrice.Columns.Count <= 0)
                {
                    dtPaxPrice.Columns.Add("Adults", typeof(int));
                    dtPaxPrice.Columns.Add("Childs", typeof(int));
                    dtPaxPrice.Columns.Add("Infants", typeof(int));
                    dtPaxPrice.Columns.Add("Label", typeof(string));
                    dtPaxPrice.Columns.Add("ActivityId", typeof(long));
                    dtPaxPrice.Columns.Add("Price", typeof(decimal));
                    dtPaxPrice.Columns.Add("Amount", typeof(decimal));
                    dtPaxPrice.Columns.Add("AgentCurrency", typeof(string));
                    dtPaxPrice.Columns.Add("AgentROE", typeof(decimal));
                    dtPaxPrice.Columns.Add("MarkupValue", typeof(decimal));
                    dtPaxPrice.Columns.Add("MarkupType", typeof(string));
                    dtPaxPrice.Columns.Add("SourceCurrency", typeof(string));
                    dtPaxPrice.Columns.Add("SourceAmount", typeof(decimal));
                    dtPaxPrice.Columns.Add("PaxType", typeof(string));
                    dtPaxPrice.Columns.Add("Markup", typeof(decimal));
                    dtPaxPrice.Columns.Add("priceId", typeof(int));
                    dtPaxPrice.Columns.Add("StockInHand", typeof(int));
                    dtPaxPrice.Columns.Add("StockUsed", typeof(int));
                }
                string supplierCurrency = "AED";
                decimal rateOfExchange = (Settings.LoginInfo.AgentExchangeRates.ContainsKey(supplierCurrency) ? Settings.LoginInfo.AgentExchangeRates[supplierCurrency] : 1);
                DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load((int)Settings.LoginInfo.AgentId, string.Empty, (int)ProductType.FixedDeparture);
                decimal markup = 0; string markupType = string.Empty;
                decimal markupValue;
                decimal sourceamount;
                decimal amount;
                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                int roomCount = 0, adults = 0, childs = 0, infants = 0;

                if (dtTransHeader != null && dtTransHeader.Rows.Count > 0)
                {
                    roomCount = Convert.ToInt32(dtTransHeader.Rows[0]["RoomCount"]);
                    adults = Convert.ToInt32(dtTransHeader.Rows[0]["Adult"]);
                    childs = Convert.ToInt32(dtTransHeader.Rows[0]["Child"]);
                    infants = Convert.ToInt32(dtTransHeader.Rows[0]["Infant"]);

                    hdnRoomCount.Value = roomCount.ToString();
                    hdnAdult.Value = adults.ToString();
                    hdnChild.Value = childs.ToString();
                    hdnInfant.Value = infants.ToString();
                }

                if (dtPrice != null && dtPrice.Rows.Count > 0)
                {
                    foreach (DataRow row in dtPrice.Rows)
                    {
                        sourceamount = (Convert.ToDecimal(row["SupplierCost"]) + Convert.ToDecimal(row["Tax"]) + Convert.ToDecimal(row["Markup"]));
                        amount = sourceamount * rateOfExchange;
                        markupValue = ((markupType == "F") ? markup : (Convert.ToDecimal(amount) * (markup / 100)));
                        decimal total = 0;
                        DataRow dr = dtPaxPrice.NewRow();
                        //dr["Adults"] = adults;
                        //dr["Childs"] = childs;
                        //dr["Infants"] = infants;
                        dr["Label"] = row["Label"].ToString();
                        dr["ActivityId"] = activity.Id;
                        dr["Price"] = Convert.ToDecimal(amount.ToString("N" + Settings.LoginInfo.DecimalValue)) + Convert.ToDecimal(markupValue.ToString("N" + Settings.LoginInfo.DecimalValue));
                        dr["Amount"] = total.ToString("N" + Settings.LoginInfo.DecimalValue);
                        dr["AgentCurrency"] = Settings.LoginInfo.Currency;
                        dr["AgentROE"] = rateOfExchange;
                        dr["MarkupValue"] = markup.ToString("N" + Settings.LoginInfo.DecimalValue);
                        dr["MarkupType"] = markupType;
                        dr["SourceCurrency"] = supplierCurrency;
                        dr["SourceAmount"] = sourceamount.ToString("N" + Settings.LoginInfo.DecimalValue); ;
                        dr["Markup"] = markupValue.ToString("N" + Settings.LoginInfo.DecimalValue);
                        dr["PaxType"] = row["PaxType"].ToString();
                        dr["priceId"] = Convert.ToInt32(row["priceId"]);
                        dr["StockInHand"] = Convert.ToInt32(row["StockInHand"]);
                        dr["StockUsed"] = Convert.ToInt32(row["StockUsed"]);
                        dtPaxPrice.Rows.Add(dr);
                    }
                }
                hdnRows.Value = dtPaxPrice.Rows.Count.ToString();
                //DataList1.DataSource = dtPaxPrice;
                //DataList1.DataBind();
                Session["PaxPrice"] = dtPaxPrice;
                BindPaxPrice();
                minPrice = Convert.ToDecimal(activity.PriceDetails.Rows[0]["Total"]);
                dlRoomRates.DataSource = activity.PriceDetails;
                dlRoomRates.DataBind();
                dlFDItinerary.DataSource = activity.FixedItineraryDetails;
                dlFDItinerary.DataBind();


            }
            else
            {
                BindPaxPrice();
            }
        }
        else
        {
            Response.Redirect("FixedDepartureResults.aspx", false);
        }
    }

    /// <summary>
    /// Bind PaxPrice Details   //brahmam
    /// </summary>
    private void BindPaxPrice()
    {
        try
        {
            DataTable dtPaxPrice = Session["PaxPrice"] as DataTable;
            for (int i = 0; i < Convert.ToInt32(hdnRoomCount.Value); i++)
            {
                HtmlTableRow hRow = new HtmlTableRow();
                HtmlTableCell hCell = new HtmlTableCell();
                
                hCell.InnerHtml += "<b>" + " ROOM " + (i + 1).ToString() + "</b>";
                hCell.Attributes.Add("class", "subgray-header");
                hRow.Cells.Add(hCell);
                tblPaxPrice.Rows.Add(hRow);
                
                for (int j = 0; j < dtPaxPrice.Rows.Count; j++)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell tc = new HtmlTableCell();
                    tc.Attributes.Add("class", "float-left");
                    Label lblCategories = new Label();
                    lblCategories.ID = "lblCategories_" + j + i.ToString();
                    lblCategories.Text = Convert.ToString(dtPaxPrice.Rows[j]["Label"]);
                    lblCategories.Width = new Unit(245, UnitType.Pixel);
                    Label lblUnitPrice = new Label();
                    lblUnitPrice.Width = new Unit(250, UnitType.Pixel);
                    lblUnitPrice.ID = "lblUnitPrice_" + j + i.ToString();
                    lblUnitPrice.Text = Convert.ToDecimal(dtPaxPrice.Rows[j]["Price"]).ToString("N" + Settings.LoginInfo.DecimalValue);

                    TextBox txtQty = new TextBox();
                    txtQty.ID="txtQty_"+j+i.ToString();
                    txtQty.Text = "0";
                    txtQty.Width = new Unit(80, UnitType.Pixel);
                    txtQty.Attributes.Add("onkeypress", "return isNumber(event);");
                    txtQty.Attributes.Add("onfocus", "Check(this.id);");
                    txtQty.Attributes.Add("onBlur", "Set(this.id);");
                    txtQty.Attributes.Add("onkeyup", "CalculateTotal()");
                    txtQty.MaxLength = 50;
                    Label lblMul = new Label();
                    lblMul.Text = "X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    Label lblBase = new Label();
                    lblBase.Text = Settings.LoginInfo.Currency;
                    lblBase.Width = new Unit(40, UnitType.Pixel);
                    Label lblBase1 = new Label();
                    lblBase1.Text = Settings.LoginInfo.Currency;
                    lblBase1.Width = new Unit(220, UnitType.Pixel);
                    lblBase1.Style.Add("text-align", "right");

                    HiddenField hdnPaxType = new HiddenField();
                    hdnPaxType.ID="hdnPaxType_"+j+i.ToString();
                    hdnPaxType.Value = Convert.ToString(dtPaxPrice.Rows[j]["PaxType"]);

                    HiddenField hdnPriceId = new HiddenField();
                    hdnPriceId.ID = "hdnPriceId_" + j + i.ToString();
                    hdnPriceId.Value = Convert.ToString(dtPaxPrice.Rows[j]["priceId"]);

                    HiddenField hdnStockInHand = new HiddenField();
                    hdnStockInHand.ID = "hdnStockInHand_" + j + i.ToString();
                    hdnStockInHand.Value = Convert.ToString(dtPaxPrice.Rows[j]["StockInHand"]);

                    HiddenField hdnStockUsed = new HiddenField();
                    hdnStockUsed.ID = "hdnStockUsed_" + j + i.ToString();
                    hdnStockUsed.Value = Convert.ToString(dtPaxPrice.Rows[j]["StockUsed"]);

                    
                    HiddenField hdnLabel = new HiddenField();
                    hdnLabel.ID = "hdn_" + Convert.ToString(dtPaxPrice.Rows[j]["priceId"]);
                    hdnLabel.Value = "0";

                    Label lblTotal = new Label();
                    lblTotal.ID = "lblTotal_" + j + i.ToString();
                    lblTotal.Text = Convert.ToDecimal(dtPaxPrice.Rows[j]["Amount"]).ToString("N" + Settings.LoginInfo.DecimalValue);
                    lblTotal.Width = new Unit(64, UnitType.Pixel);
                    lblTotal.Style.Add("text-align", "right");
                    tc.Controls.Add(lblCategories);
                    tc.Controls.Add(lblBase);
                    tc.Controls.Add(lblUnitPrice);
                    tc.Controls.Add(lblMul);
                    tc.Controls.Add(txtQty);
                    tc.Controls.Add(lblBase1);
                    tc.Controls.Add(lblTotal);
                    tc.Controls.Add(hdnPaxType);
                    tc.Controls.Add(hdnPriceId);
                    tc.Controls.Add(hdnStockInHand);
                    tc.Controls.Add(hdnStockUsed);
                    tr.Cells.Add(tc);

                    tblPaxPrice.Rows.Add(tr);
                    if (this.Form.FindControl(hdnLabel.ID)==null)
                    {
                        this.Form.Controls.Add(hdnLabel);
                    }
                }
            }
           
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            LoadPaxDataPrice();
            activity = Session["FixedDeparture"] as Activity;
            //DataTable dtTransHeader = activity.TransactionHeader;
            //int paxCount = 0;
            //if (dtTransHeader != null && dtTransHeader.Rows.Count > 0)
            //{
            //    paxCount = Convert.ToInt32(dtTransHeader.Rows[0]["Adult"]) + Convert.ToInt32(dtTransHeader.Rows[0]["Child"]) + Convert.ToInt32(dtTransHeader.Rows[0]["Infant"]);
            //}
            //if (paxCount > 0)
            //{
            //    for (int i = 0; i < paxCount; i++)
            //    {
            //        DataRow row = activity.TransactionDetail.NewRow();
            //        row["PaxSerial"] = (i + 1);
            //        row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
            //        row["CreatedDate"] = DateTime.Now;
            //        activity.TransactionDetail.Rows.Add(row);
            //    }
            //}
            activity.SaveActivityTransaction();
            divPrint.Style.Add("display", "block");
            string EmailText = "<table style='width:100%;'><tr><td>Dear&nbsp;&nbsp;" + txtName.Text.Trim() + "</td></tr>"
                                    + "<tr><td></td></tr>"
                                    + "<tr><td></td></tr>"
                                    + "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;Please find below package details.</td></tr>"
                                    + "<tr><td></td></tr></table>";


            string myPageHTML = "";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            divPrint.RenderControl(htw);
            myPageHTML = sw.ToString();
            myPageHTML =EmailText+"</br>"+ myPageHTML.Replace("\"", "/\"");


            string messageBody = "<b><font size=\"3\">Quote: </font></b><br><br>";

            string htmlTableStart = "<table style=\"border-collapse:collapse; text-align:center;\" >";
            string htmlTableEnd = "</table>";
            string htmlHeaderRowStart = "<tr style =\"background-color:#6FA1D2; color:#ffffff;\">";
            string htmlHeaderRowEnd = "</tr>";
            string htmlTrStart = "<tr style =\"color:#555555;\">";
            string htmlTrEnd = "</tr>";
            string htmlTdStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px;\">";
            string htmlTdEnd = "</td>";

            messageBody += htmlTableStart;
            messageBody += htmlHeaderRowStart;
            messageBody += htmlTdStart + "Quantity" + htmlTdEnd;
            messageBody += htmlTdStart + "RoomNo " + htmlTdEnd;
            messageBody += htmlTdStart + "PaxType " + htmlTdEnd;
            messageBody += htmlTdStart + "Amount " + htmlTdEnd;
            messageBody += htmlHeaderRowEnd;

            int i = 0;
            foreach(DataRow Row in activity.TransactionDetail.Rows)
            {
                messageBody = messageBody + htmlTrStart;
                messageBody = messageBody + htmlTdStart + "1" + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + Row["RoomNo"] + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + Row["PaxType"] + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + activity.TransactionPrice.Rows[i]["Amount"] + htmlTdEnd;
                messageBody = messageBody + htmlTrEnd;
                i++;
            }
            messageBody = messageBody + htmlTableEnd;
            myPageHTML +=  "</br>" + messageBody;

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            toArray.Add(txtEmail.Text.Trim());
            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], toArray, "Quotation", myPageHTML, new Hashtable());
            divPrint.Style.Add("display", "none");
            Response.Redirect("FixedDepartureQueue.aspx", false);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void imgSubmit_Click(object sender, EventArgs e)
    {
        LoadPaxDataPrice();
        Response.Redirect("FixedDeparturePassengerList.aspx", false);
    }

    void LoadPaxDataPrice()
    {
        try
        {
            if (Session["FixedDeparture"] != null)
            {
                activity = Session["FixedDeparture"] as Activity;

                try
                {
                    //int i = 0;
                    DataTable dtPaxPrice = Session["PaxPrice"] as DataTable;
                    if (activity.TransactionPrice != null)
                    {
                        activity.TransactionPrice.Rows.Clear();
                        activity.TransactionDetail.Rows.Clear();
                    }

                    int l = 0;
                    for (int i = 0; i < Convert.ToInt32(hdnRoomCount.Value); i++)
                    {
                        for (int j = 0; j < Convert.ToInt32(hdnRows.Value); j++)
                        {
                            Label lblFlexLabel = (Label)tblPaxPrice.FindControl("lblCategories_" + j + i.ToString());
                            Label lblPrice = (Label)tblPaxPrice.FindControl("lblUnitPrice_" + j + i.ToString());
                            TextBox txtQty = (TextBox)tblPaxPrice.FindControl("txtQty_" + j + i.ToString());
                            HiddenField hdnPaxType = (HiddenField)tblPaxPrice.FindControl("hdnPaxType_" + j + i.ToString());
                            HiddenField hdnPriceId = (HiddenField)tblPaxPrice.FindControl("hdnPriceId_" + j + i.ToString());
                            //Label lblTotal = (Label)tblPaxPrice.FindControl("lblTotal_" + j + i.ToString());

                            if (activity.TransactionPrice == null)
                            {
                                activity.TransactionDetail = ActivityDetails.GetActivityTransactionDetail(activity.Id);
                            }
                            if (Convert.ToInt32(txtQty.Text.Trim()) > 0)
                            {
                                for (int k = 0; k < Convert.ToInt32(txtQty.Text.Trim()); k++)
                                {
                                    DataRow row = activity.TransactionPrice.NewRow();
                                    DataRow[] dtRow = dtPaxPrice.Select("label='" + lblFlexLabel.Text + "'");

                                    row["Label"] = lblFlexLabel.Text;
                                    row["Amount"] = lblPrice.Text;
                                    row["LabelQty"] = 1;
                                    //row["LabelAmount"] = Convert.ToDecimal(Convert.ToDecimal(lblPrice.Text) * Convert.ToDecimal(txtQty.Text)).ToString("N" + Settings.LoginInfo.DecimalValue);
                                    row["LabelAmount"] = Convert.ToDecimal(Convert.ToDecimal(lblPrice.Text)).ToString("N" + Settings.LoginInfo.DecimalValue);
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["LastModifiedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["LastModifiedDate"] = DateTime.Now;
                                    row["AgentCurrency"] = dtRow[0]["AgentCurrency"];
                                    row["AgentROE"] = dtRow[0]["AgentROE"]; ;
                                    row["MarkupValue"] = dtRow[0]["MarkupValue"]; ;
                                    row["MarkupType"] = dtRow[0]["MarkupType"]; ;
                                    row["SourceCurrency"] = dtRow[0]["SourceCurrency"]; ;
                                    row["SourceAmount"] = dtRow[0]["SourceAmount"];
                                    row["Markup"] = dtRow[0]["Markup"];
                                    //row["RoomNo"] = (i + 1);
                                    //row["PaxType"] = hdnPaxType.Value;
                                    row["ActivityPriceId"] = Convert.ToInt32(hdnPriceId.Value);
                                    activity.TransactionPrice.Rows.Add(row);


                                    DataRow rowDetail = activity.TransactionDetail.NewRow();
                                    rowDetail["PaxSerial"] = (l + 1);
                                    rowDetail["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    rowDetail["CreatedDate"] = DateTime.Now;
                                    rowDetail["RoomNo"] = (i + 1);
                                    rowDetail["PaxType"] = hdnPaxType.Value;
                                    activity.TransactionDetail.Rows.Add(rowDetail);
                                    l++;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");
                    throw ex;
                }
                decimal total = 0;
                foreach (DataRow row in activity.TransactionPrice.Rows)
                {
                    total += Convert.ToDecimal(row["LabelAmount"]);
                }
                activity.TransactionHeader.Rows[0]["TotalPrice"] =  Math.Ceiling(total);
                activity.TransactionHeader.Rows[0]["Status"] = "A";
                activity.TransactionHeader.Rows[0]["AgencyId"] = Settings.LoginInfo.AgentId;
                activity.TransactionHeader.Rows[0]["LocationId"] = Settings.LoginInfo.LocationID;
                activity.TransactionHeader.Rows[0]["QuotedStatus"] = "Q";
                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId != null)
                {
                    activity.TransactionHeader.Rows[0]["CreatedBy"] = Settings.LoginInfo.UserID;
                    activity.TransactionHeader.Rows[0]["LastModifiedBy"] = Settings.LoginInfo.UserID;
                }
                else
                {
                    Response.Redirect("FixedDepartureResults.aspx", false);
                }
                Session["FixedDeparture"] = activity;

                
            }
            else
            {
                Response.Redirect("FixedDepartureResults.aspx", false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }
}
