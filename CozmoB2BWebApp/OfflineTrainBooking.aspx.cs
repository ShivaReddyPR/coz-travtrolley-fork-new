﻿using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
namespace CozmoB2BWebApp
{
    //Offline Train Booking
    public partial class OfflineTrainBooking : ParentPage
    {
        Dictionary<string, string> NationalityList = new Dictionary<string, string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                }
                if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                    divAgents.Attributes.CssStyle.Add("display", "block");
                else if (Settings.LoginInfo.AgentType == AgentType.Agent)
                    divAgents.Attributes.CssStyle.Add("display", "block");
                else
                    divAgents.Attributes.CssStyle.Add("display", "none");
                int agencyId = 0;
                Dictionary<string, string> diLoginInfo = new Dictionary<string, string>();
                agencyId = Settings.LoginInfo.AgentId;
                diLoginInfo.Add("Decimal", Convert.ToString(Settings.LoginInfo.DecimalValue));
                //diLoginInfo.Add("OnBehalfAgentLocation", Settings.LoginInfo.LocationCountryCode);
                diLoginInfo.Add("AgentId", Convert.ToString(agencyId));
                diLoginInfo.Add("CreatedBy", Convert.ToString(agencyId));
                diLoginInfo.Add("MemberType", Settings.LoginInfo.MemberType.ToString());
                diLoginInfo.Add("UserId", Convert.ToString(Settings.LoginInfo.UserID));
                diLoginInfo.Add("LocationID", Convert.ToString(Settings.LoginInfo.LocationID));
                diLoginInfo.Add("Currency", Convert.ToString(Settings.LoginInfo.Currency));
                hdnLoginInfo.Value = JsonConvert.SerializeObject(diLoginInfo);
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string LoadRequiredData()
        {
            try
            {
                DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//Clients
                RailwayItinerary railway = new RailwayItinerary();
                dtAgents = railway.RemoveDuplicateRows(dtAgents, "AGENT_ID");
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("Agents", dtAgents);
                string JsonData = JsonConvert.SerializeObject(Data);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at LoadRequiredData() due to:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string locationsByAgentId(string agentId)
        {
            try
            {
                DataTable dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(Convert.ToInt32(agentId), CT.TicketReceipt.BusinessLayer.ListStatus.Short, CT.TicketReceipt.BusinessLayer.RecordStatus.Activated, string.Empty);
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("Locations", dtLocations);
                AgentMaster agency = new AgentMaster();
                agency = new AgentMaster(Convert.ToInt64(agentId));
                Settings.LoginInfo.IsOnBehalfOfAgent = true;
                Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(agentId);
                Data.Add("Agency", agency);
                Data.Add("flexFields", CorporateProfile.GetAgentFlexDetailsByProduct(Convert.ToInt32(agentId), 1));
                CT.Core.StaticData sd = new CT.Core.StaticData();
                sd.BaseCurrency = agency.AgentCurrency;
                Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                string JsonData = JsonConvert.SerializeObject(Data);

                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at locationsByAgentId() due to:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string createBooking(string railwayItinerary)
        {
            try
            {
                string JsonData = string.Empty;
                RailwayItinerary itinerary = new RailwayItinerary();
                itinerary = JsonConvert.DeserializeObject<RailwayItinerary>(railwayItinerary);
                RailwayItineraryResponse response = itinerary.SaveItinerary();
                return JsonConvert.SerializeObject(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at createBooking(),reason is:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string auditSaving(string exception)
        {
            string JsonData = "";
            try
            {
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                Audit.Add(EventType.Exception, Severity.High, 0, "exception reason is" + exception, "");
                string[] errorNotificationMails = Convert.ToString(ConfigurationManager.AppSettings["ErrorNotificationMailingId"]).Split(',');
                foreach (string mails in errorNotificationMails)
                {
                    toArray.Add(mails);
                }
                //string message = ConfigurationManager.AppSettings["HOTEL_CANCEL_REQUEST"];
                try
                {
                    Hashtable table = new Hashtable();

                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Error Notification in Offline booking entry", exception, table);
                }
                catch { }
                string response = "Success";
                JsonData = JsonConvert.SerializeObject(response);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at auditSaving(),reason is:" + ex.ToString(), "");
                return null;
            }
        }
    }
}
