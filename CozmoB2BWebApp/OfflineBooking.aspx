﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="OfflineBookingGUI" Title="Offline Booking" Codebehind="OfflineBooking.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>
    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
            return ret;
        }

        function validateName(oSrc, args) {
            args.IsValid = (args.Value.length >= 2);
        }


        function Validate() {
            document.getElementById('errMessOrigin').style.display = "none";
            document.getElementById('errMessDest').style.display = "none";
            document.getElementById('errMessDestDate').style.display = "none";
            document.getElementById('errMessRetDate').style.display = "none";
            if (getElement('Origin').value == '') {
                document.getElementById('errMessOrigin').style.display = "block";
                document.getElementById('errMessOrigin').innerHTML = "Please Enter a Origin City.";
                return false;
            }
            else if (getElement('Destination').value == '') {
                document.getElementById('errMessDest').style.display = "block";
                document.getElementById('errMessDest').innerHTML = "Please Enter a Destination City.";
                return false;
            }
            else if (getElement('roundtrip').checked == true) {
                var date1 = document.getElementById('<%= DepDate.ClientID %>').value;
                var date2 = document.getElementById('<%= ReturnDateTxt.ClientID %>').value;
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();

                var todaydate = new Date(thisYear, thisMonth, thisDay);
                if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                    document.getElementById('errMessDestDate').style.display = "block";
                    document.getElementById('errMessDestDate').innerHTML = "Please Select Check In Date";
                    return false;
                }
                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMessDestDate').style.display = "block";
                    document.getElementById('errMessDestDate').innerHTML = " Invalid Check In Date";
                    return false;
                }
                var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                if (todaydate.getTime() > cInDate.getTime()) {
                    document.getElementById('errMessRetDate').style.display = "block";
                    document.getElementById('errMessRetDate').innerHTML = " Check In Date should be greater than equal to todays date";
                    return false;
                }

                if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                    document.getElementById('errMessRetDate').style.display = "block";
                    document.getElementById('errMessRetDate').innerHTML = "Please Select Check Out Date";
                    return false;
                }
                var retDateArray = date2.split('/');

                // checking if date2 is valid	
                if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                    document.getElementById('errMessRetDate').style.display = "block";
                    document.getElementById('errMessRetDate').innerHTML = " Invalid Check Out Date";
                    return false;
                }
                var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                if (todaydate.getTime() > cOutDate.getTime()) {
                    document.getElementById('errMessRetDate').style.display = "block";
                    document.getElementById('errMessRetDate').innerHTML = " Check Out Date should be greater than equal to todays date";
                    return false;
                }

                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                var difference = returndate.getTime() - depdate.getTime();

                if (difference < 0) {
                    document.getElementById('errMessRetDate').style.display = "block";
                    document.getElementById('errMessRetDate').innerHTML = "CheckOut date should be greater than  or equal to CheckIn date";
                    return false;
                }

                if (difference == 0) {
                    document.getElementById('errMessRetDate').style.display = "block";
                    document.getElementById('errMessRetDate').innerHTML = "CheckIn date and CheckOut date could not be same";
                    return false;
                }
            }
            else if (getElement('oneway').checked == true) {
                var date1 = document.getElementById('<%= DepDate.ClientID %>').value;
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();

                var todaydate = new Date(thisYear, thisMonth, thisDay);
                if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                    document.getElementById('errMessDestDate').style.display = "block";
                    document.getElementById('errMessDestDate').innerHTML = "Please Select Check In Date";
                    return false;
                }
                var depDateArray = date1.split('/');

                // checking if date1 is valid
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMessDestDate').style.display = "block";
                    document.getElementById('errMessDestDate').innerHTML = " Invalid Check In Date";
                    return false;
                }
            }
            if (document.getElementById('rules').checked == false) {
                document.getElementById('divTerms').style.display = "block";
                document.getElementById('divTerms').innerHTML = "Please Checked Terms And conditions";
                return false;

            }
            return true;
        }
        function disablefield() {
            if (getElement('roundtrip').checked == true) {
                document.getElementById('<%=tblReturn.ClientID %>').style.display = 'block';

               
                
                
            }
            else if (getElement('oneway').checked == true) {
            document.getElementById('<%=tblReturn.ClientID %>').style.display = 'none';
            }
            
        }
    </script>
<script>
    var arrayStates = new Array();
    function invokePage(url, passData) {
        if (window.XMLHttpRequest) {
            AJAX = new XMLHttpRequest();
        }
        else {
            AJAX = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if (AJAX) {
            AJAX.open("POST", url, false);
            AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            AJAX.send(passData);
            return AJAX.responseText;
        }
        else {
            return false;
        }
    }

    function getAirlines(sQuery) {

        var paramList = 'searchKey=' + sQuery;
        paramList += '&requestSource=' + "PreferredAirline";
        var url = "CityAjax";
        var arrayStates = "";
        var faltoo = invokePage(url, paramList);
        arrayStates = faltoo.split('/');
        if (arrayStates[0] != "") {
            for (var i = 0; i < arrayStates.length; i++) {
                arrayStates[i] = [arrayStates[i].split(',')[1], arrayStates[i]];
            }

            return arrayStates;
        }
        else return (false);
    }
    function autoCompInitPrefAirline() {
        oACDSPA = new YAHOO.widget.DS_JSFunction(getAirlines);
        // Instantiate third auto complete
        oAutoCompPA = new YAHOO.widget.AutoComplete('<%=txtPreferredAirline.ClientID %>', 'statescontainer4', oACDSPA);
        oAutoCompPA.prehighlightClassName = "yui-ac-prehighlight";
        oAutoCompPA.queryDelay = 0;
        oAutoCompPA.minQueryLength = 2;
        oAutoCompPA.useIFrame = true;
        oAutoCompPA.useShadow = true;

        oAutoCompPA.formatResult = function(oResultItem, sQuery) {
            document.getElementById('statescontainer4').style.display = "block";
            var toShow = oResultItem[1].split(',');
            var sMarkup = toShow[0] + ',' + toShow[1]; ;
            //var aMarkup = ["<li>", sMarkup, "</li>"]; 
            var aMarkup = [sMarkup];
            return (aMarkup.join(""));
        };
        oAutoCompPA.itemSelectEvent.subscribe(itemSelectHandlerPA);
    }
    var itemSelectHandlerPA = function(sType2, aArgs2) {

        YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
        var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
        var city = oMyAcInstance2[1].split(',');
        document.getElementById('<%=airlineCode.ClientID %>').value = city[0];
        document.getElementById('<%=airlineName.ClientID %>').value = city[1];
        document.getElementById('statescontainer4').style.display = "none";
        var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
        var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
    };
    YAHOO.util.Event.addListener(this, 'load', autoCompInitPrefAirline); //temporarily commented


    //return PreferredAirline
    function autoCompInitPrefAirline1() {
        oACDSPA = new YAHOO.widget.DS_JSFunction(getAirlines);
        // Instantiate third auto complete
        oAutoCompPA = new YAHOO.widget.AutoComplete('<%=txtPreferredAirlineRet.ClientID %>', 'statescontainer5', oACDSPA);
        oAutoCompPA.prehighlightClassName = "yui-ac-prehighlight";
        oAutoCompPA.queryDelay = 0;
        oAutoCompPA.minQueryLength = 2;
        oAutoCompPA.useIFrame = true;
        oAutoCompPA.useShadow = true;

        oAutoCompPA.formatResult = function(oResultItem, sQuery) {
            document.getElementById('statescontainer5').style.display = "block";
            var toShow = oResultItem[1].split(',');
            var sMarkup = toShow[0] + ',' + toShow[1]; ;
            //var aMarkup = ["<li>", sMarkup, "</li>"]; 
            var aMarkup = [sMarkup];
            return (aMarkup.join(""));
        };
        oAutoCompPA.itemSelectEvent.subscribe(itemSelectHandlerPA1);
    }
    var itemSelectHandlerPA1 = function(sType2, aArgs2) {

        YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
        var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
        var city = oMyAcInstance2[1].split(',');
        document.getElementById('<%=airlineCodeRet.ClientID %>').value = city[0];
        document.getElementById('<%=airlineNameRet.ClientID %>').value = city[1];
        document.getElementById('statescontainer5').style.display = "none";
        var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
        var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
    };
    YAHOO.util.Event.addListener(this, 'load', autoCompInitPrefAirline1); //temporarily commented


     
    
    var call1;
    var call2;
    //-Flight Calender control
    function init1() {
        var today = new Date();
        // Rendering Cal1
        call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
        call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        call1.cfg.setProperty("title", "Select your desired departure date:");
        call1.cfg.setProperty("close", true);
        call1.selectEvent.subscribe(setFlightDate1);
        call1.render();
        // Rendering Cal2
        call2 = new YAHOO.widget.CalendarGroup("call2", "fcontainer2");
        call2.cfg.setProperty("title", "Select your desired return date:");
        call2.selectEvent.subscribe(setFlightDate2);
        call2.cfg.setProperty("close", true);
        call2.render();
    }

    function showFlightCalendar1() {
        call2.hide();
        init1();
        document.getElementById('fcontainer1').style.display = "block";
        document.getElementById('fcontainer2').style.display = "none";
    }

    function showFlightCalendar2() {
        call1.hide();
        init1();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');
            var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
            call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() + 1) + "/" + depDateArray[2]);
            call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            call2.render();
        }
        document.getElementById('fcontainer2').style.display = "block";
    }

    function setFlightDate1() {
        var date1 = call1.getSelectedDates()[0];
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date1.getMonth() + 1;
        var day = date1.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=DepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        call1.hide();
    }

    function setFlightDate2() {
        var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select departure date.";
            return false;
        }
        var date2 = call2.getSelectedDates()[0];
        var depDateArray = date1.split('/');
        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date1 + ")";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        var month = date2.getMonth() + 1;
        var day = date2.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=ReturnDateTxt.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
        call2.hide();
    }

    YAHOO.util.Event.addListener(window, "load", init1);


   
    
</script>
    <div style="padding-top: 10px" onload="autoCompInitFlightSearch()">
    
    
    <div class="search_container">
      <div class="col-md-12"> 
    
     <div class="col-md-12"> <h3> Offline/Group Booking</h3>  </div>
     
     
     <div class="col-md-12">  <table width="250px" border="0" cellspacing="0" cellpadding="0">
     <tr>
                    <td width="50%">
                        <label>
                            <asp:RadioButton ID="oneway" GroupName="radio" onclick="disablefield();" runat="server"
                                Text="One Way" Font-Bold="true" />
                        </label>
                    </td>
                    <td>
                        <label>
                            <asp:RadioButton ID="roundtrip" GroupName="radio" onclick="disablefield();" runat="server" Checked="true"
                                Text="Round Trip" Font-Bold="true" />
                        </label>
                    </td>
                </tr></table>  </div>
                
                
               <div class="marbot_10">  
           <div class="col-md-6">  
           <div>  From</div>     
                <div>  
                
                 <asp:TextBox ID="Origin" CssClass="form-control" runat="server" onkeypress="autoCompInitFlightSearch()"></asp:TextBox>
                                            <div class="clear">
                                                <div style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 9999;"  id="statescontainer">
                                                </div>
                                            </div>
                                            <div id="multipleCity1">
                                            </div>
                                            <div class="error_msg" style="display:none;width:100%;" id="errMessOrigin"></div>
                </div>     
                
               </div> 
               
                
                
                <div class="col-md-6">  
           <div>  To</div>     
                <div> 
                         <asp:TextBox ID="Destination" CssClass="form-control" runat="server" onkeypress="autoCompInitFlightSearch()"></asp:TextBox>
                                            <div id="statesshadow2">
                <div style="width: 250px;line-height:30px; color:#000; z-index: 9999;" id="statescontainer2">
                                                </div>
                                            </div>
                                            <div id="multipleCity2">
                                            </div>
                                            <div class="error_msg" style="display:none;width:100%;" id="errMessDest"></div>
                
                </div>     
                
               </div> 
               
                
                <div class="clearfix"> </div>
                 </div> 
                 
                 
                 
                 
                      <div class="marbot_10">  
           <div class="col-md-6">
           
           <div>  Departure</div>
           
           <div> 
           
           <table> 
           
           <td><asp:TextBox ID="DepDate" CssClass="form-control" Width="110px" runat="server"></asp:TextBox> </td>
          
        <td><a href="javascript:void(null)" onclick="showFlightCalendar1()">
                                                            <img id="Img4" src="images/call-cozmo.png" alt="Pick Date"  />
                                                        </a> </td>                                            
                                                        
                                                        
</table> 
                                                        
                                                        
                                                        <div class="error_msg" style="display:none;width:100%;" id="errMessDestDate"></div>
                                                   
           
           </div>
           
           
           
             </div>
           
          
          
          
          <div class="col-md-6"> 
          <div class="col-md-6 pad_left0"> 
            <div>  Airline Prefereance </div>
            
            
            <div> 
                                               
                                                <asp:TextBox ID="txtPreferredAirline" CssClass="form-control" Text="Type Preferred Airline"
                                                    runat="server" onclick="IntDom('statescontainer4' , 'ctl00_cphTransaction_txtPreferredAirline')"
                                                    onblur="markout(this, 'Type Preferred Airline')" onfocus="markin(this, 'Type Preferred Airline')" onkeypress="autoCompInitPrefAirline()"></asp:TextBox>
                                                    
                                                <asp:HiddenField ID="airlineCode" runat="server" />
                                                <asp:HiddenField ID="airlineName" runat="server" />
                                
                           <div  id="statescontainer4"  style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;" ></div>
    
                         
                                                
                                                </div>
           </div>
          
          
           <div class="col-md-6 pad_right0">  
            <div>  Flight No </div>
    <div> <asp:TextBox CssClass="form-control" ID="txtFlightNo" runat="server"></asp:TextBox></div>
    
           
           </div>
          
          
           
           <div class=" clearfix"> </div>
          </div>
          
 
          
            
           
           
           
            <div class="clearfix"> </div>
    </div>      
              
        
     <div id="textbox_A3">  
           


 
                    <%--    <table width="100%" cellpadding="0"  id="tblReturn" style="display:block;" runat="server">
                           
                           
                           
                           <tr> 
                           <td> --%>
                           
                             <div id="tblReturn" style="display:block; margin-bottom:12px;" runat="server" > 
                           
                    <div class="col-md-6"> 
                    <div>  Return</div>
                    
                    
                    <div>  
             
             
             <table> 
<tr> 
<td> <asp:TextBox ID="ReturnDateTxt" CssClass="form-control" Width="110px" runat="server" ></asp:TextBox></td>
<td><a href="javascript:void(null)" onclick="showFlightCalendar2()">
                                                    <img id="Img3" src="images/call-cozmo.png" alt="Pick Date"  />
                                                </a> </td>

</tr>

</table>
             
             </div>
             <div class="error_msg" style="display:none;width:100%;" id="errMessRetDate"></div>
                     <div class="clearfix"> </div> 
                     </div>
                    
                        <div class="col-md-6"> 
                        
                        <div class="col-md-6 pad_left0"> 
                        
                        <div>  Airline Prefereance </div>
                        
                        
                         <div>
                                        <asp:TextBox ID="txtPreferredAirlineRet" CssClass="form-control" Text="Type Preferred Airline"
                                            runat="server" onclick="IntDom('statescontainer5' , 'ctl00_cphTransaction_txtPreferredAirlineRet')"
                                            onblur="markout(this, 'Type Preferred Airline')" onfocus="markin(this, 'Type Preferred Airline')" onkeypress="autoCompInitPrefAirline1()"></asp:TextBox>
                                        <asp:HiddenField ID="airlineCodeRet" runat="server" />
                                        <asp:HiddenField ID="airlineNameRet" runat="server" />
                                        
                                        
                                        
                                        
                            
               
               
                                    </div>
                        <div  id="statescontainer5"  style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;"></div>
                        
                        </div>
                        
                        
                        <div class="col-md-6 pad_right0"> 
                        <div>  Flight No </div>
                        
                        <div> <asp:TextBox CssClass="form-control" ID="txtFlightNoRet" runat="server"></asp:TextBox></div>
                        
                        </div>
                        
                        
           
                        <div class="clearfix"> </div>
                        
                         </div>
   
   <div class="clearfix"></div>
   
                           </div>
                           
                          <%-- </td>  
                           
                           </tr> 
                           
                           
                           </table>--%>
                           
                           
                         
  
  
  
   <div class="clearfix"> </div>
    </div>
    
    
    
    
          
    
    <div class=" clearfix"> </div>
    </div>
    
  
    
    
    
    <div class=" clearfix"> </div>
    </div>
        <div>
          
            <div style="float:right; margin-right:5px;"><asp:HyperLink ID="HlnkBack" ForeColor="White" Font-Underline="true" NavigateUrl="~/OfflineBookingList.aspx" runat="server" Text="Back To Listing Page" Visible="false" /></div>
        </div>
         
             
                 

        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
           <div class="clear" style="margin-left: 25px">
            <div id="fcontainer1" style="position: absolute; top: 179px; left: 8px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="fcontainer2" style="position: absolute; top: 179px; left: 8px; display: none;">
            </div>
        </div>
        
        
       
       
         
        <asp:HiddenField ID="hdnAdd" runat="server" Value="Adult" />
        <asp:HiddenField ID="hdnData" runat="server" Value="0" />
         
          <div class="bggray margin-top-10"> <strong> Passenger Details</strong> </div>
         
         <div class="bg_white margin-top-10"> 
         
        
        <div>  <table id="tblPassenger" class="guest-details-form" runat="server" width="100%" border="0" cellpadding="0" cellspacing="0"></table></div>
         
         </div>
        
        
        
         <div class="bg_white margin-top-10">
         
         
         <asp:Label ID="lblPax" runat="server" Text="Add More Pax:" Width="100px"></asp:Label>
         <asp:DropDownList ID="ddlPax" runat="server" Width="100px">
         <asp:ListItem Text="Adult" Value="1"></asp:ListItem>
         <asp:ListItem Text="Child" Value="2"></asp:ListItem>
         <asp:ListItem Text="Infant" Value="3"></asp:ListItem>
         </asp:DropDownList>
         <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_onclick"><asp:Image ID="imgAdd" runat="server" ImageUrl="~/images/Addplus.jpg" Width="18px" /></asp:LinkButton>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Label ID="lblMessage" runat="server" ></asp:Label>
         </div>
        
        
         <div class="ns-h3 margin-top-10">Rules And Rustirictions</div>
       
       
        <div class="bg_white pad_10"> 
        
        <font size="3"><span style='color:red;'>*</span>&nbsp;Kindly Check the spelling & reconfirm the passenger names(s) before you book.</font> </br>
        <font size="3"><span style='color:red;'>*</span>&nbsp;Ticket Name Changes are not permitted once issued</font></br></br>
        <font size="3">The above mentioned purchases are subject to cancellation,date change fee and once purchased tickets are non-transferable and </br>
        name changes are not permitted. For further details,read the over view of all the Restriction,Penalties & Cancellation Charges.
        </font>
        </br></br>
        
       <div> 
       
       
       
        <div class="col-md-6">
        
           
         <label>
                                    <input type="checkbox" name="rules" id="rules" checked="checked" />
                                    <font size="3"> I accept Terms & Conditions.</font></label>
                                    <div class="error_msg" style="display:none;width:100%;" id="divTerms"></div>
        
        </div>
       <div class="col-md-6"> <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="but but_b pull-right" CausesValidation="true" ValidationGroup="pax" OnClientClick="return Validate();"  OnClick="btnContinue_Click" /></div>
       
       
       <div class="clearfix"> </div>
       
       </div>
        
     
                                  
                                    
        </div>
        <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
    </div>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

