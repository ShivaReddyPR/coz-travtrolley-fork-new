<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="RegisterListGUI" Title="Agent Register List" Codebehind="RegisterList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript">

        //To open pop window and get documents to download 
        function GetDocs(reg_id) {

            var reg_id = parseInt(reg_id)
            getAgentReceipts(reg_id);
        }

        //To open documents
        function ShowDocs(filepath,filename) {
            var ext = filename.split('.').reverse()[0].toLowerCase();
            //var imagePath = filepath;
            var imagePath = filepath.replaceAll('/', '\\');
            imagePath = imagePath.substring(imagePath.indexOf('\\Upload\\'), imagePath.Length);
            var imageContent = '';
            if (ext == 'pdf')
                imageContent = '<object data="' + imagePath + '" type="application/pdf" height="400" width="100%"></object>';
            else
                imageContent = '<a href="' + imagePath + '" download="' + imagePath + '" target="_blank"><img src="' + imagePath + '" class="thumbnail" style="margin:0 auto;max-width:100%" ></a>';
            $('#uploadedImage').html(imageContent);
        }
        
        //To bind documents to download
        function getAgentReceipts(reg_id) {


            var data = AjaxCall('RegisterList.aspx/getDocDetails', "{'reg_id':'" + reg_id + "'}");

            $("#ReceiptDetails").html('');

            if (!IsEmpty(data)) {
                var docs = JSON.parse(data);
                var distintcodes = docs.map(item => item.doc_code).filter((value, index, self) => self.indexOf(value) === index);                
                var items = '<table class="table small mt-2"><tr>';

                var maxlength = 0;
                for (var i = 0; i < distintcodes.length; i++)
                {
                    var eachdocs = docs.filter(x => x.doc_code == distintcodes[i]);
                    maxlength = eachdocs.length > maxlength ? eachdocs.length : maxlength;
                    items += '<th>'+distintcodes[i]+'</th>';
                }

                items += '</tr></thead><tbody>';

                for (var j = 0; j < maxlength; j++)
                {
                    items += '<tr>';
                    for (var i = 0; i < distintcodes.length; i++)
                    {
                        var eachdocs = docs.filter(x => x.doc_code == distintcodes[i]);                        
                        items += '<td>' + (eachdocs.length <= j ? '' : '<a href="#viewImage" data-toggle="modal" data-target="#viewImage" onclick="javascript:ShowDocs(\'' + encodeURI(eachdocs[j].doc_file_path) + '\', \'' + eachdocs[j].doc_name + '\');">' + eachdocs[j].doc_name + '</a>') + '</td>';
                    }
                    
                    items += '</tr>'
                }

                items += '</tbody></table>';
                $("#ReceiptDetails").html(items);
            }
        }

    </script>
   
    <div style="margin: 0.5%; border: 1px solid black">
       
        <asp:GridView ID="gvRegisterList" runat="server" AutoGenerateColumns="False" Width="100%"
            EmptyDataText="No Agency List!" AllowPaging="true" PageSize="10" DataKeyNames="reg_id"
            CellPadding="0" ShowFooter="true" CellSpacing="0" GridLines="none" OnPageIndexChanging="gvRegisterList_PageIndexChanging"
            OnSelectedIndexChanged="gvRegisterList_SelectedIndexChanged" Caption="Registerd Agency List" >
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
                <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                    ControlStyle-CssClass="label" ShowSelectButton="True" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton runat="server" OnClick="DeleteRegisteredUser" OnClientClick="return confirm('Do you want to delete?')"
                            ImageUrl="Images/grid/wg_Delete.gif" CommandArgument='<%# Eval("reg_id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentName" Width="70px" HeaderText="Name"
                            CssClass="inputEnabled" runat="server" /> 
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentName" runat="server" Text='<%# Eval("reg_agencyname") %>' CssClass="labelDtl grdof"
                            ToolTip='<%# Eval("reg_agencyname") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentAddress" Width="70px" HeaderText="Address"
                            CssClass="inputEnabled" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentAddress" runat="server" Text='<%# Eval("reg_address") %>'
                            CssClass="labelDtl grdof" ToolTip='<%# Eval("reg_address") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentPhone1" Width="70px" HeaderText="Phone"
                            CssClass="inputEnabled" runat="server" /> 
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentPhone1" runat="server" Text='<%# Eval("reg_telephone") %>'
                            CssClass="labelDtl grdof" ToolTip='<%# Eval("reg_telephone") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Documents">
                    <ItemTemplate>
                      <button type="button" class="btn btn-primary btn-sm my-2" data-toggle="modal" data-target="#AgentDocumentList" onclick="GetDocs('<%# Eval("reg_id") %>');">
                        View Docs
                     </button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

<!-- Agent Documents List -->
<div class="modal fade" id="AgentDocumentList" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header py-2">
        <h5 class="modal-title" id="">Uploaded Documents</h5>
        <button type="button" class="close position-relative" data-dismiss="modal" aria-label="Close" style="top: -20px;
    right: -8px;font-size: 20px;">
         <span aria-hidden="true" class="icon icon-close"></span>
        </button>
      </div>
      <div class="modal-body">
        
         <div class="table-responsive">
             <p id="ReceiptDetails"></p>         
        </div>


      </div>
    </div>
  </div>
</div>


<!-- Agent View Document -->
<div class="modal fade" id="viewImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
       
       <button type="button" class="close text-secondary position-relative" data-dismiss="modal" aria-label="Close" style="top: -10px;right: -8px;"> <span aria-hidden="true" class="icon icon-close"></span> </button>
            
           <div id="uploadedImage">
              
            </div>
      </div>
     
    </div>
  </div>
</div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
