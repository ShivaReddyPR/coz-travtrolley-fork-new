﻿using System;
using System.Data;


//Added for cryptography purpose
using System.Security.Cryptography;
using System.Text;

//--------------------------------
using CT.TicketReceipt.BusinessLayer;
using System.IO;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;

public partial class B2BVisaModuleUI : CT.Core.ParentPage
{
    protected int b2bagentId;
    protected long userId;
    protected string userName;
    protected string product = "VISA";
    protected string password;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            //DataTable dtAgentInfo = AgentMaster.GetVMSB2BAgentInfo(Settings.LoginInfo.AgentId);
            //DataTable dtAgentInfo = CT.Core.Supplier.GetVMSB2BAgentInfoTemp(Settings.LoginInfo.AgentId);//Temp
            DataTable dtAgentInfo = AgentMaster.GetVMSB2BAgentInfo(Settings.LoginInfo.AgentId, "VMSUSER");
            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId > 0)// TODO ziya, this dall should be avoided from here as calling DB always
            {
                //Loading Current Balance
                decimal currentBal = AgentMaster.UpdateAgentBalance(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId, 0, (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID);// TEMP
                //decimal currentBal = CT.Core.Supplier.UpdateAgentBalanceTemp(loginInfo.AgentId, 0, (int)loginInfo.UserID);// TEMP
                Settings.LoginInfo.AgentBalance = currentBal;
            }
            
            if (dtAgentInfo != null && dtAgentInfo.Rows.Count > 0 && !CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentBlock) //Checking Booking block or Not Added Brahmam
            {
            //    CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "test ziyad", "");
                foreach (DataRow dr in dtAgentInfo.Rows)
                {
                    if (dr["user_login_name"] != DBNull.Value)
                    {
                        userName = Convert.ToString(dr["user_login_name"]);
                    }
                    b2bagentId = Settings.LoginInfo.AgentId;
                    userId = Settings.LoginInfo.UserID;
                    if (Request["product"] != null)
                    {
                        product = Convert.ToString(Request["product"]);
                    }
                    if (dr["user_password"] != DBNull.Value)
                    {
                        password = Encrypt(Convert.ToString(dr["user_password"])); //need to pass encrypted password to the page.
                    }

                    break;
                }
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }


    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
                
            }
        }
        return clearText;
    }


    
}
