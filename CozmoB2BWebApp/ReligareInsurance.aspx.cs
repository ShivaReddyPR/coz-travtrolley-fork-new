﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using ReligareInsurance;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace CozmoB2BWebApp
{
    public partial class ReligareInsurance : CT.Core.ParentPage
    {
        //Masters tables List:
        //1.CT_T_Religare_ProductType -- 1.Explore and 2.Student Explore
        //2.CT_T_Religare_ProductDetails -- Explore and Student Explore Will have different Sub products
        //3.CT_T_Religare_Relation_Of_Proposer -- Standalone table to maintain the Proposer Relations. 
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            try
            {
                IntialisePageControls(); 
                if (IsPostBack && (!string.IsNullOrEmpty(txtend.Text)) && (!string.IsNullOrEmpty(txtstart.Text)))
                {
                    txtEndDate.Text = Convert.ToDateTime(txtend.Text).ToString("dd/MMM/yyyy");
                    txtStartDate.Text = Convert.ToDateTime(txtstart.Text).ToString("dd/MMM/yyyy"); 
                }
                if (!IsPostBack)
                {
                    if (Session["quotation"] != null)
                    {
                        ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                        if (quotation.AgentId > 0)
                        {
                            txtstart.Text = Convert.ToDateTime(quotation.StartDate).ToString("dd/MMM/yyyy");
                            txtStartDate.Text = Convert.ToDateTime(quotation.StartDate).ToString("dd/MMM/yyyy");
                            txtend.Text = Convert.ToDateTime(quotation.EndDate).ToString("dd/MMM/yyyy");
                            txtEndDate.Text = Convert.ToDateTime(quotation.EndDate).ToString("dd/MMM/yyyy");
                            ddlPED.SelectedValue = quotation.Ped;
                            ddlTripType.SelectedValue = quotation.TripType;
                            txtDays.Text = Convert.ToString(quotation.Term);
                            divpremium.Visible = true;
                            lblPremium.Text = CTCurrencyFormat(quotation.Premium, quotation.AgentId);
                            ddlTravellers.SelectedValue = quotation.Travellers.ToString();
                            int i = 0, j = 0;
                            foreach (RadioButton radioButton in pnlRadioButtons.Controls.OfType<RadioButton>())
                            {
                                string productId = ((HiddenField)pnlRadioButtons.FindControl("hdnIns" + j)).Value;
                                if (productId == quotation.ProductType.ToString())
                                {
                                    radioButton.Checked = true;
                                    BindTravelling();
                                    ddlTravelling.SelectedValue = quotation.ProductId;
                                }
                                j++;
                            }
                            divterm.Visible = true;
                            divenddate.Visible = true;
                            if (quotation.ProductType == (int)ReligareProducts.StudentExplore)
                            {
                                divenddate.Visible = false;
                                ddltripPeriod.SelectedValue = quotation.MaxTripPeriod;
                                ddlTerm.SelectedValue = quotation.Term.ToString();
                                addListItems(1);
                                ddlTravellers.SelectedValue = "1";
                                divterm.Visible = false;
                            }
                            if (ddlTripType.SelectedValue=="MULTI")
                            {
                                txtDays.Visible = false;
                                lbldays.Text = "Trip period";
                                ddltripPeriod.Visible = true;
                                ddltripPeriod.SelectedValue = Convert.ToString(quotation.Term);
                            }
                            BindSumInsuredDetails();
                            ddlSumIns.SelectedValue = quotation.SumInsuredId;
                            BindTravellerAgeDetails();
                            foreach (TableRow tr in pnltravelers.Rows)
                            {
                                foreach (Control control in tr.Cells[0].Controls)
                                {
                                    if (control is DropDownList)
                                    {
                                        DropDownList dropDownList = (DropDownList)control;
                                        dropDownList.SelectedValue = quotation.TravellerAges[i].travellerAge.ToString();
                                        i++;
                                    }
                                }
                            }
                            
                            if (Settings.LoginInfo.AgentId != quotation.AgentId && quotation.AgentId > 0)
                            {
                                rbb2b.Checked = true;
                                bindAgentDetails();
                                ddlagents.SelectedValue = quotation.AgentId.ToString();
                                bindAgentLocation();
                                ddlAgentLoc.SelectedValue = quotation.LocationId.ToString();
                                rbself.Enabled = false;
                                rbself.Checked = false;
                            }
                            else
                            {
                                rbself.Checked = true;
                                ddlagents.Visible = false;
                                ddlAgentLoc.Visible = false;
                                divagent.Visible = false;
                                divAgentLoc.Visible = false;
                                rbb2b.Checked = false;
                            }
                        }
                        BindSumInsuredDetails();
                    }
                }
                premiumCalculation();
                // lblPremium.Text = Convert.ToDecimal(premiumCalculation()).ToString("N" + Settings.LoginInfo.DecimalValue);// premiumCalculation().ToString(); 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(ReligareInsurance)PageLoadEventError.Reason" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        public void IntialisePageControls()
        {
            try
            {
                getInsuranceTypes();
                AddTravellerAgeDetails();
            }
            catch { }           
        }
        private void addListItems(int len)
        {
            try
            {
                ddlTravellers.Items.Clear();                
                for (int i = 1; i <= len; i++)
                    ddlTravellers.Items.Add(new ListItem(i.ToString(), i.ToString()));
                ddlTravellers.Items.Insert(0,new ListItem("-- Select --", "-1"));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "failed to Binding the Travellers count:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        private void AddTravellers()
        {
            try
            {
                string prodtype = Session["productId"].ToString();
                if (Convert.ToInt32(prodtype) == (int)ReligareProducts.StudentExplore)
                    addListItems(1);
                else
                {
                    ddlTravellers.Enabled = true;
                    addListItems(6);
                }
            }
            catch
            {
            }
        }
        /// <summary>
        /// Getting the Insurance Types and Adding Panel
        /// Binds all the insurance types from the table CT_T_Religare_ProductType.
        /// </summary>
        private void getInsuranceTypes()
        {
            try
            {
                DataTable dtInsuranceTypes = ReligareQuotation.GetInsuranceTypes();
                if (dtInsuranceTypes != null && dtInsuranceTypes.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsuranceTypes.Rows.Count; i++)
                    {
                        HiddenField hdn = new HiddenField();
                        hdn.ID = "hdnIns" + i;
                        hdn.Value = dtInsuranceTypes.Rows[i]["PRODUCTID"].ToString();
                        pnlRadioButtons.Controls.Add(hdn);

                        RadioButton radioButton = new RadioButton();
                        radioButton.ID = "rbInsType" + i;
                        if (i == 0)
                        { 
                            radioButton.Checked = true;                          
                        }
                        if (!IsPostBack )
                            BindTravelling();
                        radioButton.AutoPostBack = true;
                        radioButton.CheckedChanged += new EventHandler(this.rbInsType_CheckedChanged);
                        radioButton.CausesValidation = true;
                        radioButton.CssClass = "InsuranceType";
                        radioButton.GroupName = "rb";
                        radioButton.Text = dtInsuranceTypes.Rows[i]["PRODUCTNAME"].ToString();    
                        pnlRadioButtons.Controls.Add(radioButton);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Adding the Insurance Types" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Binding Product Names based on  Product Type
        /// </summary>
        private void BindTravelling()
        {
            try
            {
                int i = 0;
                string productId = "";
                foreach (RadioButton radioButton in pnlRadioButtons.Controls.OfType<RadioButton>())
                {
                    bool b = radioButton.Checked;
                    if (radioButton.Checked)
                    {
                        productId = ((HiddenField)pnlRadioButtons.FindControl("hdnIns" + i)).Value;
                        DataTable dataTable = ReligareQuotation.GetProductDetails(ref productId);
                        ddlTravelling.DataSource = dataTable;
                        ddlTravelling.DataValueField = "PRODUCT_ID";
                        ddlTravelling.DataTextField = "PRODUCT_NAME";
                        ddlTravelling.DataBind();
                        ddlTravelling.Items.Insert(0, new ListItem("-- Select --", "-1"));
                        Session["productId"] = productId;
                    }
                    i++;
                }
                if ( (!string.IsNullOrEmpty(productId)) && Convert.ToInt32(productId) == (int)ReligareProducts.StudentExplore)                 
                {
                    lblEndDate.Text = "Term(In Months)";
                    lbldays.Text ="Trip Period";
                    txtEndDate.Visible = false;
                    txtDays.Visible = false;
                    ddltripPeriod.Visible = true;
                    ddlTerm.Visible = true;
                    divenddate.Visible = false;
                     
                }
                else  
                {
                    lblEndDate.Text = "End Date";
                    lbldays.Text = "Days";
                    txtEndDate.Visible = true; 
                    txtDays.Visible = true;
                    ddltripPeriod.Visible = false;
                    ddlTerm.Visible = false;
                    divenddate.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Binding the Travelling Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Binding the Sum Insured Details based on selection Product Id
        /// </summary>
        private void BindSumInsuredDetails()
        {
            try
            {
                if (ddlTravelling.SelectedValue != "-1" && ddlPED.SelectedValue != "-1" )
                {
                    string insuredId = ddlTravelling.SelectedValue;
                    int ped = Convert.ToInt32(ddlPED.SelectedValue);
                    int term = string.IsNullOrEmpty(txtDays.Text) ?0: Convert.ToInt32(txtDays.Text);
                    if (ddlTripType.SelectedValue == "MULTI")
                        term = ddltripPeriod.SelectedValue == "-1"? 0 : Convert.ToInt32(ddltripPeriod.SelectedValue);
                    DataTable dtAgeDetails = ReligareQuotation.GetSumInsuredDetails(ref insuredId, ref ped, ref term);
                    if (dtAgeDetails != null)
                    {
                        ddlSumIns.DataSource = dtAgeDetails;
                        ddlSumIns.DataValueField = "SUM_INSURED_ID";
                        ddlSumIns.DataTextField = "sumInsured";
                        ddlSumIns.DataBind();
                        ddlSumIns.Items.Insert(0, new ListItem("-- Select --", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Binding the sum Insured Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        ///  Binding the Traveller Age Details based on selection insurance plan.
        /// </summary>
        private void BindTravellerAgeDetails()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtstart.Text))
                {
                    string insuredId = ddlTravelling.SelectedValue;
                    int ped = Convert.ToInt32(ddlPED.SelectedValue);
                    int term = 0;
                    if (ddlTerm.Visible)
                    {
                        txtend.Text = Convert.ToDateTime(txtstart.Text).AddMonths(Convert.ToInt32(ddlTerm.SelectedValue)).ToString("dd/MMM/yyyy");
                        txtEndDate.Text = Convert.ToDateTime(txtstart.Text).AddMonths(Convert.ToInt32(ddlTerm.SelectedValue)).ToString("dd/MMM/yyyy");
                        term = Convert.ToInt32(ddlTerm.SelectedValue);
                        txtDays.Text = Convert.ToString(term);
                    }
                    else if (ddlTripType.SelectedValue == "MULTI")
                    {
                        term =ddltripPeriod.SelectedValue == "-1"? 0 : Convert.ToInt32(ddltripPeriod.SelectedValue);
                    }
                    else
                        term = string.IsNullOrEmpty(txtDays.Text) ? 0 : Convert.ToInt32(txtDays.Text);// (Convert.ToDateTime(txtend.Text) -Convert.ToDateTime(txtstart.Text)).Days;
                     
                        DataTable dtAgeDetails = ReligareQuotation.GetTravellerAgeDetails(ref insuredId, ref ped, ref term, ddlSumIns.SelectedValue);
                        foreach (TableRow tr in pnltravelers.Rows)
                        {
                            foreach (Control control in tr.Cells[0].Controls)
                            {
                                if (control is DropDownList)
                                {
                                    DropDownList dropDownList = (DropDownList)control;
                                    dropDownList.DataSource = dtAgeDetails;
                                    dropDownList.DataValueField = "SUM_INSURED_ID";
                                    dropDownList.DataTextField = "AGE_BAND";
                                    dropDownList.DataBind();
                                    dropDownList.Items.Insert(0, new ListItem("-- Select --", "-1"));
                                }
                            }
                        }
                     
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Binding the Traveller Age Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        ///  binding The travelling Details and Sum Insured Details based on Insurance Type.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rbInsType_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindTravelling();
                string prodtype = Session["productId"].ToString();
                if (Convert.ToInt32(prodtype) == (int)ReligareProducts.StudentExplore)
                {
                    lbldays.Text = "Trip period";
                    addListItems(1);
                }
                else
                {
                    lbldays.Text = "Days";
                    ddlTravellers.Enabled = true;
                    addListItems(6);
                }
                ddlTravelling.Focus();
                BindTravellerAgeDetails();
                BindSumInsuredDetails();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Binding the Traveller Age Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTravellers_SelectedIndexChanged(object sender, EventArgs e)
        {
             BindSumInsuredDetails();
        }
        protected void ddlTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSumInsuredDetails();
        }
        /// <summary>
        /// Adding the Traveller Age Controls Based on product and Traveller Count.
        /// </summary>
        private void AddTravellerAgeDetails()
        {
            try
            {
                if (Session["productId"] != null)
                {
                    short tabindex = ddlTravellers.TabIndex;
                    tabindex++;
                    ddlSumIns.TabIndex = tabindex;
                    tabindex++;
                    int travellerCount = Convert.ToInt32(ddlTravellers.SelectedValue);
                    if (Session["quotation"] != null && IsPostBack != true)
                    {
                        ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                        travellerCount = quotation.Travellers;
                    }
                    TableRow tr;
                    TableCell tableCell;
                    string prodtype = Convert.ToString(Session["productId"]);
                    if (Convert.ToInt32(prodtype) == (int)ReligareProducts.StudentExplore)
                        travellerCount = 1;
                    for (int i = 1; i <= travellerCount; i++)
                    {
                        Label lbl = new Label();
                        lbl.Text = "Traveller" + i + " Age";

                        tr = new TableRow();
                        tableCell = new TableCell();
                        //tableCell.CssClass = "col-sm-3";
                        tableCell.Controls.Add(lbl);
                        tr.Cells.Add(tableCell);
                        pnltravelers.Rows.Add(tr);

                        DropDownList ddl = new DropDownList();
                        ddl.TabIndex = tabindex;
                        tabindex++;
                        ddl.ID = "ddlTravellerAge" + i;
                        ddl.CssClass = "form-control";
                        ddl.Items.Insert(0, new ListItem("-- Select --", "-1"));
                        ddl.AutoPostBack = true;
                        ddl.SelectedIndexChanged += new EventHandler(this.ddlTravellerAge_SelectedIndexChanged);

                        tr = new TableRow();
                        tableCell = new TableCell();
                        //tableCell.CssClass = "col-sm-3";
                        tableCell.Controls.Add(ddl);
                        tr.Cells.Add(tableCell);
                        pnltravelers.Rows.Add(tr);
                    }
                    BindTravellerAgeDetails();
                    ddlTravellers.Focus();
                   
                    btnsave.TabIndex = tabindex;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Adding the controls Traveller Age Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        ///  Calculate the Premium Amount 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSumIns_SelectedIndexChanged(object sender, EventArgs e)
        {
            premiumCalculation();
            // Convert.ToDecimal( premiumCalculation()).ToString("N" + Settings.LoginInfo.DecimalValue);// premiumCalculation().ToString(); 
        }
        /// <summary>
        ///  Calculate the Premium Amount with discount Based on passenger count and procuct .
        /// </summary>
        private void premiumCalculation()
        {
            decimal premium = 0;
            try
            {                
                if (ddlSumIns.SelectedValue != "-1")
                {
                    string insuredId = ddlTravelling.SelectedValue;
                    int ped = Convert.ToInt32(ddlPED.SelectedValue);
                    int term = string.IsNullOrEmpty(txtDays.Text)?0: Convert.ToInt32(txtDays.Text);
                    if (ddlTripType.SelectedValue == "MULTI")
                    {
                        term = ddltripPeriod.SelectedValue == "-1" ? 0 : Convert.ToInt32(ddltripPeriod.SelectedValue);
                    }
                    DataTable dtAgeDetails = ReligareQuotation.GetTravellerAgeDetails(ref insuredId, ref ped, ref term,ddlSumIns.SelectedValue);

                    if (dtAgeDetails != null && dtAgeDetails.Rows.Count > 0)
                    {
                        foreach (TableRow tr in pnltravelers.Rows)
                        {
                            foreach (Control control in tr.Cells[0].Controls)
                            {
                                if (control is DropDownList)
                                {
                                    DropDownList ddl = (DropDownList)control;
                                    foreach (DataRow dr in dtAgeDetails.Rows)
                                    {
                                        if (dr["SUM_INSURED_ID"].ToString() == ddl.SelectedValue)
                                        {
                                            decimal premiumRoundOff = Convert.ToDecimal(dr["PREMIUM_ROUNDOFF_WITH_GST"]);
                                            premium += premiumRoundOff;
                                        }
                                    }
                                }
                            }
                        }
                        divpremium.Visible = true;
                        lblPremium.Text = premium.ToString();
                        MarkupDetails();
                    }
                }
                else
                    lblPremium.Text = "0.00";
               
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Premium Calculation" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            //return premium;
        }
        private void MarkupDetails()
        {
            try
            {
                decimal basefare = Convert.ToDecimal(lblPremium.Text);
                if (basefare > 0 )
                {
                    if (rbself.Checked)
                    {
                        calculateMarkupDetails(Settings.LoginInfo.AgentId, Settings.LoginInfo.LocationCountryCode, Settings.LoginInfo.DecimalValue, Settings.LoginInfo.Currency);
                    }
                    else
                    {
                        AgentMaster agent = new AgentMaster(Convert.ToInt64(ddlagents.SelectedValue));
                        LocationMaster locationMaster = new LocationMaster(Convert.ToInt64(ddlAgentLoc.SelectedValue));
                        calculateMarkupDetails(Convert.ToInt32(ddlagents.SelectedValue), locationMaster.CountryCode, agent.DecimalValue, agent.AgentCurrency);
                    }
                }                 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to confirm Religare Insurance Markup Details: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        private void calculateMarkupDetails(int agentId, string countryCode,int DecimalValue,string currency)
        {
            decimal markup = 0, handlingFee = 0 ;
            string markuptype = string.Empty, handlingFeeType = string.Empty;
             decimal basefare = Convert.ToDecimal(lblPremium.Text);
            ReligareQuotation quotation = null;
            if (Session["quotation"] != null)
                quotation = (ReligareQuotation)Session["quotation"];
            else
                quotation = new ReligareQuotation();


            // Discount Details
            int travellerCount = Convert.ToInt32(ddlTravellers.SelectedValue);
            decimal discountPercentage = Convert.ToDecimal(ReligareQuotation.GetDiscountDetails(ref travellerCount));            
            quotation.DiscountType = "P";
            quotation.Discount = basefare * discountPercentage / 100; 
            quotation.DiscountValue = discountPercentage;
            basefare = (basefare - (basefare * discountPercentage / 100));
            lblPremium.Text = basefare.ToString();
            quotation.Premium = basefare;
            try
            {
                DataTable dtAgentMarkup = UpdateMarkup.Load(agentId, "RELIGARE", (int)ProductType.Insurance);
                if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0 && (Settings.LoginInfo.AgentId > 1 || ddlagents.SelectedValue  !="-1"))
                {
                    //Markup
                    markuptype = Convert.ToString(dtAgentMarkup.Rows[0]["MarkupType"]).ToUpper();
                    markup = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Markup"]);
                    quotation.MarkupValue = markup;
                    quotation.MarkupType = markuptype;
                    if (markuptype == "P")
                    {
                        quotation.Markup = Math.Round((basefare * markup) / 100,DecimalValue);// Convert.ToDecimal(().ToString("N" + DecimalValue).ToString();
                        basefare += quotation.Markup;
                    }
                    else if (markuptype == "F")
                    {
                        quotation.Markup = Math.Round(markup * Convert.ToInt32(ddlTravellers.SelectedValue), DecimalValue);
                        basefare += quotation.Markup;
                    }
                    // Handling Fee
                    if (countryCode == "IN")
                    {
                        handlingFeeType = Convert.ToString(dtAgentMarkup.Rows[0]["HandlingFeeType"]).ToUpper();
                        handlingFee = Convert.ToDecimal(dtAgentMarkup.Rows[0]["HandlingFeeValue"]);
                        if (handlingFeeType == "P")
                        {
                            quotation.HandlingFee= Math.Round((basefare * handlingFee) / 100,DecimalValue);
                            basefare += quotation.HandlingFee;
                        }
                        else if (handlingFeeType == "F")
                        {
                            quotation.HandlingFee = Math.Round(handlingFee * Convert.ToInt32(ddlTravellers.SelectedValue),DecimalValue);
                            basefare += quotation.HandlingFee;
                        }
                        quotation.HandlingFeeType = handlingFeeType;
                        quotation.HandlingFeeValue = handlingFee;
                        
                    }    
                }
                Session["quotation"] = quotation;                
                lblPremium.Text = currency +" "+ Convert.ToDecimal(basefare).ToString("N" + DecimalValue).ToString();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Calculate the Markup and Handling Fee: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        protected void ddlTravelling_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string productId = Session["productId"].ToString();
                DataTable dataTable = ReligareQuotation.GetProductDetails(ref productId);                 
                if (dataTable !=null && dataTable.Rows.Count >0 )
                {
                    ddlTripType.ClearSelection();
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        if (dr["PRODUCT_ID"].ToString() == ddlTravelling.SelectedValue)
                        {
                            if (dr["Trip_Type"].ToString().Trim() == "MULTI")
                            {
                                divterm.Visible = true;
                                ddlTripType.SelectedValue = "MULTI";
                                ddltripPeriod.Visible = true;
                                txtDays.Visible = false;
                                lbldays.Text =  "Trip Period";
                            }
                            else
                            {
                                if (Convert.ToInt32(productId)== (int)ReligareProducts.Explore)
                                {
                                    divterm.Visible = true;
                                    ddltripPeriod.Visible = false;
                                    txtDays.Visible = true;
                                    lbldays.Text = "Days";
                                }
                                else if (Convert.ToInt32(productId) == (int)ReligareProducts.StudentExplore)
                                {
                                    divterm.Visible = false;
                                }
                                ddlTripType.SelectedValue = "SINGLE";                                 
                            }                             
                        }
                    }
                    ddlTripType.Enabled = false;
                    txtEndDate.Text = string.Empty;
                    //txtStartDate.Text = string.Empty;
                    //txtstart.Text = string.Empty;
                    txtend.Text = string.Empty;
                    txtDays.Text = string.Empty;
                }
                BindSumInsuredDetails();
                txtStartDate.Focus();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Binding sum Insured Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        protected void ddlTravellerAge_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                premiumCalculation();
              //lblPremium.Text = Convert.ToDecimal(premiumCalculation()).ToString("N" +Settings.LoginInfo.DecimalValue);// premiumCalculation().ToString(); 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Premium Calculation" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        protected void rbself_CheckedChanged(object sender, EventArgs e)
        {
            bindAgentDetails();
            ddlAgentLoc.Items.Clear();
            ddlAgentLoc.Items.Insert(0, new ListItem("-- Select --", "-1"));
        }
        private void bindAgentLocation()
        {
            try
            {
                ddlAgentLoc.Items.Clear();
                if (rbb2b.Checked && ddlagents.SelectedValue !="-1")
                {
                    DataTable dtLocations = LocationMaster.GetList(Utility.ToInteger(ddlagents.SelectedItem.Value), ListStatus.Short, RecordStatus.Activated, string.Empty);
                    if (dtLocations != null && dtLocations.Rows.Count > 0)
                    {
                        ddlAgentLoc.Items.Clear();
                        ddlAgentLoc.DataSource = dtLocations;
                        ddlAgentLoc.DataValueField = "LOCATION_ID";
                        ddlAgentLoc.DataTextField = "LOCATION_NAME";
                        ddlAgentLoc.DataBind();
                    }     
                }
                ddlAgentLoc.Items.Insert(0, new ListItem("-- Select --", "-1"));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "failed to Binding the agent Location Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        private void bindAgentDetails()
        {
            try
            {
                if (rbb2b.Checked)
                {
                    ddlagents.Items.Clear();   // getting the agents and filter the login agent id.
                    DataTable dtlist = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                    DataView dv = dtlist.DefaultView;
                    dv.RowFilter = "agent_Id NOt IN('" + Settings.LoginInfo.AgentId + "')";
                    ddlagents.AppendDataBoundItems = true;
                    ddlagents.DataSource = dtlist;
                    ddlagents.DataValueField = "agent_id";
                    ddlagents.DataTextField = "agent_name";
                    ddlagents.DataBind();                    
                    ddlAgentLoc.Visible = true;
                    divagent.Visible = true;
                    divAgentLoc.Visible = true;
                    ddlagents.Visible = true;
                }
                else
                {
                    ddlAgentLoc.Visible = false;
                    divagent.Visible = false;
                    divAgentLoc.Visible = false;
                    ddlagents.Visible = false;
                }
                ddlagents.Items.Insert(0, new ListItem("-- Select --", "-1"));
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "failed to Binding the agent Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        //taking the currency value 
        protected string CTCurrencyFormat(object currency, object AgentId)
        {
            AgentMaster agency;
            if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
            {
                agency = new AgentMaster(Convert.ToInt32(AgentId));
                return agency.AgentCurrency + " " + Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
            }
            else
            {
                agency = new AgentMaster(Convert.ToInt32(AgentId));
                return agency.AgentCurrency + " " + Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
            }
        }
        // Binding the agent location.
        protected void ddlagents_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindAgentLocation();
        }
        // getting the sum insured details when user chenge the ped status.
        protected void ddlPED_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSumInsuredDetails();
        }
        /// <summary>
        /// Saving the Insurance Plan Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["quotation"] != null)
                {
                    ReligareQuotation quotation =  (ReligareQuotation) Session["quotation"];
                    if (rbself.Checked)
                    {
                        quotation.AgentId = Settings.LoginInfo.AgentId;
                        quotation.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
                    }
                    else
                    {
                        quotation.AgentId = Convert.ToInt32(ddlagents.SelectedValue);
                        quotation.LocationId = Convert.ToInt32(ddlAgentLoc.SelectedValue);
                    }

                    quotation.ProductType = Convert.ToInt32(Session["productId"]);
                    quotation.ProductId = ddlTravelling.SelectedValue;
                    quotation.StartDate = Convert.ToDateTime(txtStartDate.Text);

                    // Checking the term and no of days.
                    if (quotation.ProductType == (int)ReligareProducts.StudentExplore && ddlTerm.Visible)
                    {
                        DateTime startDate = Convert.ToDateTime(txtStartDate.Text).AddMonths(Convert.ToInt32(ddlTerm.SelectedValue));
                        TimeSpan ts = Convert.ToDateTime(txtEndDate.Text) - startDate;
                        quotation.EndDate = startDate.AddDays(ts.Days - 1);
                    }
                    else
                        quotation.EndDate = Convert.ToDateTime(txtEndDate.Text);                   

                    quotation.ProductName = ddlTravelling.SelectedItem.Text;        
                        quotation.Term = ddlTripType.SelectedValue == "MULTI" ? Convert.ToInt32(ddltripPeriod.SelectedValue) : Convert.ToInt32(txtDays.Text);
                    quotation.Travellers = Convert.ToInt32(ddlTravellers.SelectedValue);
                    quotation.TripType = ddlTripType.SelectedValue;
                    //quotation.Premium = Convert.ToDecimal(lblPremium.Text.Replace(Settings.LoginInfo.Currency, "").Trim());
                    if (quotation.TripType == "MULTI" || quotation.ProductType == (int)ReligareProducts.StudentExplore)
                        quotation.MaxTripPeriod = ddltripPeriod.SelectedValue; 
                    quotation.Ped = ddlPED.SelectedValue;
                    quotation.SumInsuredId = ddlSumIns.SelectedValue;
                    // Get the Sum Insured Range ID. 
                    string insuredId = ddlTravelling.SelectedValue;
                    int ped = Convert.ToInt32(ddlPED.SelectedValue);
                    int term = quotation.Term;
                    int detailID = Convert.ToInt32(ddlSumIns.SelectedValue);
                    quotation.RangeId = ReligareQuotation.GetSumInsuredRangeID(ref insuredId, ref ped, ref term, ref detailID);
                    TravellerAge age = new TravellerAge();
                    List<TravellerAge> liAges = new List<TravellerAge>();
                    foreach (TableRow tr in pnltravelers.Rows)
                    {
                        foreach (Control control in tr.Cells[0].Controls)
                        {
                            if (control is DropDownList)
                            {
                                age = new TravellerAge();
                                DropDownList dropDownList = (DropDownList)control;
                                age.travellerAge = dropDownList.SelectedValue;
                                liAges.Add(age);
                            }
                        }
                    }
                    quotation.TravellerAges = liAges;
                    Session["quotation"] = quotation;
                    Response.Redirect("ReligareProposerDetails.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string msg = "Fail the request selected Policy Plan";
                Response.Redirect("ErrorPage.aspx?ReligareError=" + msg, false);
                Session["quotation"] = null;
                Audit.Add(EventType.Exception, Severity.High, 0, "Insurance failed to Saving/Updating the Insured Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
    }
}
