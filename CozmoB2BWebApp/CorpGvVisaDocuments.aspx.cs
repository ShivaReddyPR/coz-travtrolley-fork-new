﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.Corporate;
using CT.GlobalVisa;
using CT.TicketReceipt.BusinessLayer;
using DevExpress.Web;

public partial class CorpGvVisaDocumentsGUI : CT.Core.ParentPage
{
    protected GlobalVisaSession visaSession = new GlobalVisaSession();
    protected List<GVPassenger> passengerList = new List<GVPassenger>();
    protected List<CorpProfileDocuments> documentslist = new List<CorpProfileDocuments>();
    protected void Page_Load(object sender, EventArgs e)
    {
        try

        {
            if (!IsPostBack)
            {
                Session["docfiles"] = null;
                if (Session["GVSession"] != null)
                {
                    string thumbnailpaths = ("~/Thumbnails/");
                    if (!Directory.Exists(Server.MapPath(thumbnailpaths)))
                    {
                        Directory.CreateDirectory(Server.MapPath(thumbnailpaths));                        
                    }
                    visaSession = Session["GVSession"] as GlobalVisaSession;
                    if (visaSession != null && visaSession.PassengerList != null)
                    {
                        passengerList = visaSession.PassengerList as List<GVPassenger>;
                        for (int i = 0; i < passengerList.Count; i++)
                        {
                            string DocumentImg = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyyhh:mm:ss")).Replace(":", "");
                            DocumentImg = DocumentImg.Replace("-", "");
                            string paths = ("~/DocumentsUpload/") + DocumentImg +i.ToString()+ "/";
                            if (!Directory.Exists(Server.MapPath(paths)))
                            {
                                Directory.CreateDirectory(Server.MapPath(paths));
                            }
                            CorpProfileDocuments docslist = new CorpProfileDocuments();
                            List<CorpProfileDocuments> corpdocs = docslist.GetDomenttailsByPax(passengerList[i].Fpax_passport_no, Settings.LoginInfo.AgentId);
                            if (corpdocs != null && corpdocs.Count > 0)
                            {
                                if (Session["docfiles"] != null)
                                {
                                    documentslist = Session["docfiles"] as List<CorpProfileDocuments>;
                                }
                                foreach (CorpProfileDocuments docs in corpdocs)
                                {
                                    documentslist.Add(docs);
                                }
                                Session["docfiles"] = documentslist;
                            }
                            passengerList[i].Fpax_docpath = paths;
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                        Response.Redirect("AbandonSession.aspx", false);
                    }
                }
            }
            
            BindControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGvVisaDocuments error:" + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
    private void BindControls()
    {
        try
        {
            if (Session["GVSession"] != null)
            {
                visaSession = Session["GVSession"] as GlobalVisaSession;
                if (visaSession != null && visaSession.PassengerList != null)
                {
                    passengerList = visaSession.PassengerList as List<GVPassenger>;
                    for (int i = 0; i < passengerList.Count; i++) 
                    {
                        HtmlGenericControl divHeader = new HtmlGenericControl("div");
                        divHeader.ID = "divHeader" + i.ToString();
                        divHeader.Attributes["Class"] = "bggray bor_gray paddingtop_10 marbot_20";
                        HtmlTableRow hr = new HtmlTableRow();
                        Label lableName = new Label();
                        lableName.ID = "lblName" + i.ToString();
                        lableName.Text = "Passanger Name:";
                        lableName.Width = new Unit(120, UnitType.Pixel);
                        Label lableNameText = new Label();
                        lableNameText.ID = "lblNameText" + i.ToString();
                        lableNameText.Font.Bold = true;
                        lableNameText.Text = passengerList[i].Fpax_name.ToUpper();
                        lableNameText.Width = new Unit(200, UnitType.Pixel);
                        Label lablpassport = new Label();
                        lablpassport.ID = "lblpassport" + i.ToString();
                        lablpassport.Text = "Passport Number:";
                        lablpassport.Width = new Unit(120, UnitType.Pixel);
                        Label lablpassportNo = new Label();
                        lablpassportNo.ID = "lblpassportNo" + i.ToString();
                        lablpassportNo.Text = passengerList[i].Fpax_passport_no.ToUpper();
                        lablpassportNo.Width = new Unit(100, UnitType.Pixel);
                        lablpassportNo.Font.Bold = true;
                        
                        ASPxFileManager fmanager = new ASPxFileManager();                     
                        fmanager.Width = Unit.Pixel(500);
                        fmanager.Height = Unit.Pixel(300);
                        fmanager.ID = "fmanager" + i.ToString();
                        fmanager.Settings.EnableMultiSelect = true;
                        fmanager.SettingsEditing.AllowDelete = true;
                        fmanager.SettingsEditing.AllowRename = true;                       
                        fmanager.SettingsLoadingPanel.ShowImage = true;                     
                        fmanager.EnableCallBacks = true;
                        fmanager.CustomCallback += Fmanager_CustomCallback;
                        fmanager.SettingsToolbar.ShowPath = false;                                                                 
                        fmanager.SettingsUpload.Enabled = true;
                       // fmanager.SettingsUpload.UseAdvancedUploadMode = false;
                        fmanager.SettingsUpload.AutoStartUpload = false;
                        fmanager.SettingsUpload.ShowUploadPanel = true;
                        fmanager.Styles.UploadPanel.BackColor = System.Drawing.Color.AliceBlue;
                        fmanager.SettingsUpload.NullText = "No File Selected";
                        fmanager.SettingsUpload.AdvancedModeSettings.EnableMultiSelect = true;
                        fmanager.SettingsUpload.AdvancedModeSettings.EnableDragAndDrop = true;
                        fmanager.Settings.AllowedFileExtensions =new string[8] { ".jpg", ".jpeg", ".gif",".png",".txt", ".doc",".docx",".pdf" };
                        fmanager.SettingsBreadcrumbs.Visible = false;
                        fmanager.SettingsFolders.Visible = false;
                        fmanager.Settings.RootFolder = "DocumentsUploads";                       
                        string[] initfolder = passengerList[i].Fpax_docpath.Split('/');
                        fmanager.Settings.InitialFolder = initfolder[2];
                        //fmanager.CustomThumbnail += Fmanager_CustomThumbnail;
                        fmanager.Settings.ThumbnailFolder = "~/Thumbnails/";                                             
                        fmanager.FilesUploaded += Fmanager_FilesUploaded;
                        fmanager.ItemsDeleted += Fmanager_ItemsDeleted;
                        fmanager.ItemRenamed += Fmanager_ItemRenamed;                          
                        fmanager.CustomFileSystemProvider = new Customprovider(initfolder[2]);
                       
                        Panel imgpanel = new Panel();
                        imgpanel.ID = "imgpanel" + i.ToString();
                        imgpanel.BorderWidth = new Unit(1, UnitType.Pixel);
                        imgpanel.Width = new Unit(550, UnitType.Pixel);
                        imgpanel.Height = new Unit(250, UnitType.Pixel);
                        imgpanel.ScrollBars = ScrollBars.Auto;
                        HtmlTableCell tc = new HtmlTableCell();
                        divHeader.Controls.Add(lableName);
                        divHeader.Controls.Add(lableNameText);
                        divHeader.Controls.Add(lablpassport);
                        divHeader.Controls.Add(lablpassportNo);
                        tc.Controls.Add(divHeader);
                        HtmlGenericControl divelement = new HtmlGenericControl("div");
                        divelement.ID = "divelement" + i.ToString();
                        divelement.Attributes["Class"] = "col-md-12";
                        HtmlGenericControl divfmanager = new HtmlGenericControl("div");
                        divfmanager.ID = "divfmanager" + i.ToString();
                        divfmanager.Attributes["Class"] = "col-md-6";
                        divfmanager.Controls.Add(fmanager);
                        divelement.Controls.Add(divfmanager);
                        HtmlGenericControl divimgpnl = new HtmlGenericControl("div");
                        divimgpnl.ID = "divimgpnl" + i.ToString();
                        divimgpnl.Attributes["Class"] = "col-md-5";
                        CorpProfileDocuments docslist = new CorpProfileDocuments();
                        List<CorpProfileDocuments> corpdocs = docslist.GetDomenttailsByPax(passengerList[i].Fpax_passport_no, Settings.LoginInfo.AgentId);
                        if (corpdocs != null && corpdocs.Count > 0)
                        {
                            string rootFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ProfileImage"]);
                            int p = 0;
                            Label lblheader = new Label();
                            lblheader.ID = "lblheader" + i.ToString();
                            lblheader.Text = "Documents";
                            foreach (CorpProfileDocuments docs in corpdocs)
                            {
                                HtmlAnchor htmlanchor = new HtmlAnchor();
                                HtmlImage htmlimgdoc = new HtmlImage();
                                string filepath = rootFolder + "" + docs.ProfileId + "//" + docs.DocFileName;
                                htmlanchor.ID = "htmlanchor" + i.ToString() + p.ToString();
                                htmlanchor.Attributes["Class"] = "image-popup-no-margins";
                                htmlanchor.Title = docs.DocTypeName;
                                htmlanchor.HRef = filepath;
                                htmlimgdoc.ID = "htmlimgdoc" + i.ToString() + p.ToString();
                                htmlimgdoc.Width = 94;
                                htmlimgdoc.Height = 120;
                                htmlimgdoc.Attributes["Class"] = "client_docs";
                                htmlimgdoc.Src = filepath;
                                htmlanchor.Controls.Add(htmlimgdoc);                               
                                imgpanel.Controls.AddAt(p, htmlanchor);
                                p++;
                            }                            
                            divimgpnl.Controls.Add(lblheader);
                            divimgpnl.Controls.Add(imgpanel);
                            divelement.Controls.Add(divimgpnl);
                        }
                        tc.Controls.Add(divelement);
                        HtmlGenericControl divClear = new HtmlGenericControl("div");
                        divClear.ID = "divClear" + i.ToString();
                        divClear.Attributes["Class"] = "clearfix";
                        tc.Controls.Add(divClear);
                        hr.Cells.Add(tc);
                        tblMarkup.Rows.Add(hr);
                    }
                    Button btnProceed = new Button();
                    btnProceed.ID = "btnUpload";
                    btnProceed.Text = "Proceed";
                    btnProceed.Height= new Unit(35, UnitType.Pixel);
                    btnProceed.Attributes["Class"] = "btn but_b pull-right";
                    btnProceed.Click += new EventHandler(btnApply);
                    HtmlTableCell htc = new HtmlTableCell();
                    htc.Controls.Add(btnProceed);
                    HtmlTableRow htr = new HtmlTableRow();
                    htr.Cells.Add(htc);
                    tblMarkup.Rows.Add(htr);
                }
            }
            else
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGvVisaDocuments error:" + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    //private void Fmanager_CustomThumbnail(object source, FileManagerThumbnailCreateEventArgs e)
    //{
    //    //e.ThumbnailImage.Url = "http://locolhost//images//"+e.Item.Name;
    //    //e.ThumbnailImage.Url = "~/C:/inetpub/wwwroot/images/" + e.Item.Name;
    //    e.ThumbnailImage.Url = e.Item.FullName;

    //}

    private void Fmanager_CustomCallback(object sender, CallbackEventArgsBase e)
    {
        
    }

    private void Fmanager_ItemRenamed(object source, FileManagerItemRenamedEventArgs e)
    {
        try
        {
            ASPxFileManager fmn = (ASPxFileManager)source;
            string strid = fmn.ID;
            string[] i = strid.Split('r');
            int paxid = Convert.ToInt32(i[1]);
            documentslist = Session["docfiles"] as List<CorpProfileDocuments>;
            List<CorpProfileDocuments> fileItem = documentslist.Where(k => k.PassportNo.ToLower().Trim() == passengerList[paxid].Fpax_passport_no.ToLower().Trim()).Select(k => k).ToList();
            CorpProfileDocuments renameItem = fileItem.Where(j => j.DocFileName.ToLower().Trim() == e.OldName.ToLower().Trim()).SingleOrDefault();
            renameItem.DocFileName = e.Item.Name;
            renameItem.DocTypeName = e.Item.Name.Split('.')[0];
            Session["docfiles"] = documentslist;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGvVisaDocuments error:" + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
    private void Fmanager_ItemsDeleted(object source, FileManagerItemsDeletedEventArgs e)
    {
        try
        {
            ASPxFileManager fmn = (ASPxFileManager)source;
            string strid = fmn.ID;
            string[] i = strid.Split('r');
            int paxid = Convert.ToInt32(i[1]);
            FileManagerItem[] fmanagerlist = e.Items;
            documentslist = Session["docfiles"] as List<CorpProfileDocuments>;
            foreach (FileManagerFile file in fmanagerlist)
            {
                List<CorpProfileDocuments> fileItem = documentslist.Where(k => k.PassportNo.ToLower().Trim() == passengerList[paxid].Fpax_passport_no.ToLower().Trim()).Select(k => k).ToList();
                CorpProfileDocuments deleteItem = fileItem.Where(j => j.DocFileName.ToLower().Trim() == file.Name.ToLower().Trim()).SingleOrDefault();
                if (deleteItem != null)
                {
                    documentslist.Remove(deleteItem);
                    Session["docfiles"] = documentslist;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGvVisaDocuments error:" + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
    
    private void Fmanager_FilesUploaded(object source, FileManagerFilesUploadedEventArgs e)
    {
        try
        {
            ASPxFileManager fmn = (ASPxFileManager)source;
            string strid = fmn.ID;
            FileManagerFile[] fmanagerlist = e.Files;
            string[] i = strid.Split('r');    
           
            if (Session["docfiles"] != null)
            {
                documentslist = Session["docfiles"] as List<CorpProfileDocuments>;
            }
            foreach (FileManagerFile file in fmanagerlist)
            {
                CorpProfileDocuments documents = new CorpProfileDocuments();
                int paxid = Convert.ToInt32(i[1]);
                documents.PassportNo = passengerList[paxid].Fpax_passport_no;
                documents.DocTypeName = file.Name.Split('.')[0];
                documents.DocFileName = file.Name;
                documents.DocFilePath = file.Folder.ToString();
                documentslist.Add(documents);
            }
            Session["docfiles"] = documentslist;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGvVisaDocuments error:" + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
    protected void btnApply(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < passengerList.Count; i++)
            {
                string[] fileEntries = Directory.GetFiles(Server.MapPath(passengerList[i].Fpax_docpath));
                if(fileEntries.Length==0)
                {
                    throw new Exception("!Please Select Documents For:" +passengerList[i].Fpax_passport_no.ToUpper());
                }
                
              }
            if (Session["docfiles"] != null)
            {
                Response.Redirect("CorpGVVisaDetails.aspx");
            }          
        }
        catch (Exception ex)
        {
            Utility.Alert(this.Page, ex.Message);
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGvVisaDocuments error:" + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
}

