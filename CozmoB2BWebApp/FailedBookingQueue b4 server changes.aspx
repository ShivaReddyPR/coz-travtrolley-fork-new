<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="FailedBookingQueue" MasterPageFile="~/TransactionBE.master" Codebehind="FailedBookingQueue.aspx.cs" %>
    <%@ MasterType VirtualPath="~/TransactionBE.master" %>

<asp:Content ContentPlaceHolderID="cphTransaction" ID="Content1" runat="server">

    <script src="scripts/ModalPop.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ConfirmDelete(index) {
            if (confirm("Do you want to remove this entry!")) {
                document.getElementById('removeDiv').style.display = 'block';
                document.getElementById('index').value = index;
            }
            
//        ModalPop1.AcceptButton.onclick = Delete;
//        ModalPop1.AcceptButton.value = "OK";
//        ModalPop1.DenyButton.onclick = CancelDelete;
//        ModalPop1.DenyButton.value = "Cancel";
//        var modalMessage = "<div>This will remove the record. Are you sure to proceed? If so then please enter the reason.</div><div style=\"margin-top:15px;text-align:left\">Remarks: <textarea id=\"RemarksBox\" name=\"RemarksBox\" cols=\"35\" rows=\"3\"></textarea> <div style=\"font-size:10px;font-style:italic;margin-left:100px\"></div>";
//        ModalPop1.Show({x: 400, y: 200}, modalMessage);
     }
     function Delete()
     {
//        ModalPop1.AcceptButton.disabled = true;
         //        ModalPop1.DenyButton.disabled = true;
         var index = document.getElementById('index').value;
         document.getElementById('remarks').value = document.getElementById('RemarksBox').value;
         document.getElementById('RemoveForm' + index).submit();
     }

    function CancelDelete()
    {
        //ModalPop1.Hide();
        document.getElementById('RemarksBox').value = "";
        document.getElementById('removeDiv').style.display = 'none';
    }
    function GetDetails(i)
    {
       document.getElementById('DisplayPNR'+i).submit();
   }
   function ShowPage(pageNo) {
       //            if (document.getElementById('OnHold').checked == true) {
       //                document.getElementById('HOLD').value = "true";
       //            }
       //            else {
       //                document.getElementById('HOLD').value = "false";
       //            }
       for (var k = 0; k < no; k++) {
            if(document.forms['RemoveForm'+k].elements['postback']!= null){
               document.forms['RemoveForm'+k].elements['postback'].value = "";
               }
       }
       document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
       document.forms[0].submit();
   }

   function SaveHotel(i) {
   var no = <%=fblist.Count %>;
       for (var k = 0; k < no; k++) {
            if(document.forms['RemoveForm'+k].elements['postback']!= null){
               document.forms['RemoveForm'+k].elements['postback'].value = "";
               }
               if(k==i)
               {
               document.forms['RemoveForm'+k].elements['saveAgain'].value = "SaveAgain";
               }
       }

       //document.getElementById('postback').value = "";
       document.getElementById('saveAgain').value = "SaveAgain";
       document.getElementById('RemoveForm' + i).submit();
   }
    </script>

    <div id="removeDiv" style="display: none; position: absolute; left: 750px; top: 220px;
        width: 200px; overflow: auto; border: solid 1px #ccc;">
        This will remove the record. Are you sure to proceed? If so then please enter the
        reason.
        <div style="margin-top: 15px; text-align: left;">
            Remarks:
            <textarea id="RemarksBox" name="RemarksBox" cols="35" rows="3"></textarea>
            <div style="font-size: 10px; font-style: italic; margin-left: 100px;">
                <input type="button" id="btnRemove" name="btnRemove" value="Remove" onclick="javascript:Delete()" />
                <input type="button" id="btnCancel" name="btnCancel" value="Close" onclick="javascript:CancelDelete()" />
            </div>
        </div>
    </div>
        
    <div class="fleft margin-top-10 width-100">
   
        <% if ((fblist == null || fblist.Count == 0))
           {%>
        <div class="fleft italic margin-top-20 font-14" style="margin-left: 90px; margin-top: 50px">
            There is no Pending Bookings in the Queue
        </div>
        <%}%>
          
    </div>
    <div class="fright margin-top-10 width-100">
        <form id="searchForm" method="post" action="failedbookingqueue.aspx">
        <table>
        <tr>
        <td align="center"><asp:Label ID="lblSearch" runat="server" Text="PNR:"></asp:Label></td>
        <td><input name="inputPNRsearch" type="text" style="width:100px" class="auto-list" value=""/></td>
        <td style=" padding-left:10px"> <input type="submit" name="search" id="search" value="Search"  class="button" style="width:70px"/></td>
        <td><%if (productTypeId == (int)CT.BookingEngine.ProductType.Flight)
              { %> 
        <a href="failedbookingqueue.aspx?Source=Flight" style="font-size:10px;" >
        <%}
              else if(productTypeId==(int)CT.BookingEngine.ProductType.Hotel)
              { %>
              <a href="failedbookingqueue.aspx?Source=Hotel" style="font-size:10px;" >
        <%} %>
        Clear Filters</a></td>
        </tr>
        </table>
        
       
        <br />
        
         </form>
         </div>
        <%if (fblist !=null && fblist.Count > 0)
      {%>
    <div class="fleft margin-top-10 width-100">
    
    <%if (fblist.Count >0)
      {%>
        <%= show%>
       <%} %>  
        <div style="float: left; padding: 40px 0px 0px 20px;">
            <%if (errorMessage.Length > 0)
              { %>
            <div style="margin-left: 90px; margin-top: 50px; color: Red;">
                <%=errorMessage%>
            </div>
            <%} %>
            <div class="fleft margin-top-10" style="width:700px;">
                <div class="fleft font-16 bold width-90">
                <asp:Label ID="lblPNR" runat="server" Text="PNR"></asp:Label>
                </div>
                <div class="fleft font-16 bold width-140">
                    Created On</div>
                <div class="fleft font-16 bold width-120">
                    Agency Name</div>
                <%--<div class="fleft margin-left-5 font-16 bold width-95">Member Id</div>--%>
                <div class="fleft fleft font-16 bold width-90">
                    Source</div>
                <div class="fleft font-16 bold width-110px">
                    Current Status</div>
            </div>
        </div>
    </div>
    <%} %>
    <div>
        <%  
            for (int i = 0; (fblist != null && i< fblist.Count); i++)
            {
                      
                        
        %>
        <div class="corner-block-parent fleft " style="border-bottom-width: thin;">
            <div class="width-575 padding-10 fleft">
                <form action="DisplayPNRInformation.aspx?pnrNo= <%=fblist[i].PNR %>" method="post" id="DisplayPNR<%=i %>" style="display: inline">
                <input type="hidden" name="isFailed" value="true"/>
                    <div class="fleft margin-left-20 width-90">
                        <%=fblist[i].PNR%>
                    </div>
                    <div class="fleft   margin-left-40 width-170">
                        <%=CT.BookingEngine.Util.UTCToIST((fblist[i].CreatedOn)).ToString("dd/MM/yyyy HH:mm:ss")%>
                    </div>
                    <%-- <%CoreLogic.Agency a=new CoreLogic.Agency(fblist[i].AgencyId); %>--%>
                    <div class="fleft margin-left-20 width-120">
                        <%=fblist[i].AgencyName%>
                    </div>
                    <%--<div class="fleft margin-left-10  width-95"><%=fblist[i].MemberId%></div>--%>
                    <div class="fleft margin-left-35  width-90">
                    <%if (productTypeId == (int)CT.BookingEngine.ProductType.Flight)
                      {%>
                        <%=fblist[i].Source%>
                        <%}
                      else if(productTypeId == (int)CT.BookingEngine.ProductType.Hotel)
                      { %>
                       <%=fblist[i].HotelSources%>
                        <%} %>
                    </div>
                    <div class="fleft margin-left-10  width-90">
                        <%=fblist[i].CurretnStatus%>
                    </div>
                    <input type="hidden" id="j" value="<%=j %>" />
                    <%--<input type="submit" value="Get Details" />--%>
                    <div class="fleft margin-left-10  width-150">
                    <%if (productTypeId == (int)CT.BookingEngine.ProductType.Flight)
                      { %>
                    <a href="javascript:GetDetails(<%=i %>)">Get Details</a>
                    <%} %>
                    <%else if(productTypeId == (int)CT.BookingEngine.ProductType.Hotel)
                      { %>
                      <a href="javascript:SaveHotel(<%=i %>)">Save Hotel Booking</a>
                    <%} %>
                    </div>
                    <input type="hidden" name="pnrNo" value="<%=fblist[i].UniversalRecord %>" />
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.UAPI)
                      { %>
                    <input type="hidden" name="source" value="UAPI" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.Amadeus)
                      { %>
                    <input type="hidden" name="source" value="1A" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.WorldSpan)
                      { %>
                    <input type="hidden" name="source" value="1P" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.Galileo)
                      { %>
                    <input type="hidden" name="source" value="1G" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.SpiceJet)
                      { %>
                    <input type="hidden" name="source" value="SG" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.Indigo)
                      { %>
                    <input type="hidden" name="source" value="6E" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.Paramount)
                      { %>
                    <input type="hidden" name="source" value="I7" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.AirDeccan)
                      { %>
                    <input type="hidden" name="source" value="DN" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.Mdlr)
                      { %>
                    <input type="hidden" name="source" value="9H" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.GoAir)
                      { %>
                    <input type="hidden" name="source" value="G8" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.Sama)
                      { %>
                    <input type="hidden" name="source" value="ZS" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.HermesAirLine)
                      { %>
                    <input type="hidden" name="source" value="IX" />
                    <% }%>
                    <%if (fblist[i].Source == CT.BookingEngine.BookingSource.AirArabia)
                      { %>
                    <input type="hidden" name="source" value="G9" />
                    <% }%>
                    <input type="hidden" name="AdminId" id="Hidden4" value="<%= AdminId %>" />
                    <input type="hidden" name="BookingAgencyID" value="<%= fblist[i].AgencyId %>" />
                </form>
                <form action="FailedBookingQueue.aspx" method="get" style="display: inline" id="RemoveForm<%=i %>">
                    <input type="hidden" name="pnr" value="<%=fblist[i].PNR %>" />
                    <input name="postback" id="postback" type="hidden" value="remove" />
                    <input name="pageNo" type="hidden" value="<%=pageNo %>" />
                    <input type="hidden" name="remarks" value="" id="remarks"/>
                    <%if (productTypeId == (int)CT.BookingEngine.ProductType.Hotel)
                      { %>
                     <input type ="hidden" name ="Source" value="Hotel" id="htlSource" />
                    <%} %>
                   
                    <%else if (productTypeId == (int)CT.BookingEngine.ProductType.Flight)
                      { %>
                     <input type ="hidden" name ="Source" value="Flight" id="flightSource" />
                    <%} %>                   
                    <input type ="hidden" name="index" value="" id="index" />
                    <input type="hidden" name="saveAgain" value="" id="saveAgain" />
                    <%if (productTypeId == (int)CT.BookingEngine.ProductType.Flight)
                      { %>
                    <a href="javascript:ConfirmDelete('<%=i %>')">Remove</a>
                    <%} %>
                </form>
            </div>
        </div>
        <% } %>
        
        <asp:HiddenField ID="PageNoString" runat="server" Value="1" />
    </div>
</asp:Content>
