﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="IndexUI" Title="Home" Codebehind="Index.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<div class="body_container"> 

<div>
<div class="col-md-3 hidden-xs pad_right0"> 

<div style=" background:#e6791e; font-size:18px; padding: 10px 10px 10px 10px"> <center class="fcol_fff"> NEWS UPDATES</center> </div>

<div style="background:#fff;border-bottom: solid 1px #e6791e; padding: 10px;border-right: solid 1px #e6791e;border-left: solid 1px #e6791e; padding:10px; min-height:370px; max-height:370px; line-height:21px; "> 


    <marquee direction="up" scrollamount="3" height="340px" onmouseover="this.stop()" onmouseleave="this.start()">
<%if (Currency != "INR")
    { %>



Exclusive airline fares available with Cozmo Travel.
For enquiries contact: getquote@cozmotravel.com
    <br />
    <br />
​All agents purchasing visa change ticket with deposit waiver must send approved visas to a2avisa@cozmotravel.com  <br /> <br /> 



<strong style="font-size: 14px;text-decoration: underline;color: #e6791e;"> CONTACT SUPPORT CENTRE  </strong> <br /> 
Landline: +971 6 5074592<br /> 
For any Support Contact: b2bsupport@cozmotravel.com<br /> <br /> 
<br /> 


    <%}
    else
    { %>
        Exclusive airline fares available with Cozmo travel World.
        <br /> <br /> 
<strong style="font-size: 14px;text-decoration: underline;color: #e6791e;"> CONTACT SUPPORT CENTRE  </strong> <br /> 
Landline: 022 7100 4644<br />
Mobile:913 6999 700<br />
For any Support Contact: supportb2b@cozmotravelworld.com<br /> <br /> 
<br /> 
    <%} %>
    </marquee>
</div>


</div>
<div class="col-md-9">



<style> .tab-pane { border: solid 1px #ccc; background:#fff; }
.acdetails td { padding: 10px 10px 10px 10px; border-bottom: solid 1px #ccc;   }

.nav-tabs { font-family:Ebrima!important; font-size:14px!important; }

@media (min-width:1200px){
    .one-day-sale-bg img{
        height:100% !important;
    }
}

.carousel-inner>.item>a>img, .carousel-inner>.item>img {
    display: block;
    width: 100%;
    max-height: 100%;
}
</style>


<ul style=" background:#fff; border: solid 1px #ccc;" class="nav nav-tabs" role="tablist">
   
  <li class="active"><a data-toggle="tab" href="#Tab_1">Home</a></li>

   <li><a data-toggle="tab" href="#Tab_2">Bank A/C Details</a></li>
    <%if (Currency != "INR")
        { %>
   <li><a data-toggle="tab" href="#Tab_3">Agent TopUp </a></li>
    <%} %>
      <li><a data-toggle="tab" href="#Tab_4">Support </a></li>

  </ul>
  
 <div class="tab-content margin_top10">
    
    <div id="Tab_1" class="tab-pane fade in active">
    


     <script>

         $(function() {
             $('#carouselSlider,#carouselSlider2').carousel({
                 interval: 5000 //changes the speed
             });

         });

</script>
   
   
   



 <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
 <li data-target="#myCarousel" data-slide-to="2"></li> 
                  <%--<li data-target="#myCarousel" data-slide-to="3"></li>--%>

    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <%if (Currency != "INR")
            { %>
<%--<div class="item active">
        <img src="images/SPICEJET.jpg" />
      </div> --%>
               <div class="item active">
        <img src="images/Jet_Airways.jpg" />
      </div> 

        <div class="item  ">
        <img src="images/Tune.jpg" />
      </div>
      <div class="item">
        <img src="images/pic.jpg" />
      </div>

        <!--<div class="item">
        <img src="images/pic3.jpg" />
      </div>-->

      <%--<div class="item">
        <img src="images/pic4.jpg" />
      </div>--%>
         <div class="item">
        <img src="images/pic3.jpg" />
      </div>
     

      <%--<div class="item">
        <img src="images/pic2.jpg" />
      </div>--%>
    
      
      
 <%--     <div class="item">
        <img src="images/pic4.jpg" />
      </div>--%>

    <%}
    else
    {%>
          <div class="item active">
        <img src="images/GPS_Backdrop_GlobalVisa_70x48in.jpg" />
      </div>

        <div class="item">
        <img src="images/GPS_Backdrop_Holidays_70x48in.jpg" />
      </div>
         <div class="item">
        <img src="images/GPS_Backdrop_Services_70X40in.jpg" />
      </div>
        <%} %>
    </div>

    <!-- Left and right controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
  </div>







 </div>
    
  

    

<div id="Tab_2" class="tab-pane fade in">

<div class="table-responsive" style="padding:10px"> 
   <%if (Currency == "KWD")
  {%>
<h4 class="bggray">United Arab Emirates </h4>
<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Ahli United Bank</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">1 2 2 9 3 0 1 1 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">KW 18 BKME 0000 0000 0000 0012293011</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">AED</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>Branch</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Fahed Al Salem Street</td>
  </tr>
 <%-- <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EBILAEADSUK</td>
  </tr>--%>
</table><br />

<%} else if (Currency == "INR")
    { %>
    <h4 class="bggray">INDIA </h4>
<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">ICICI BANK LIMITED</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Cozmo Travel World Private Limited</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">0 0 1 1 0 5 0 2 6 1 3 9 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IFSC CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">ICIC0000011</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">INR</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>Branch</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAGAR AVENUE, GROUND FLOOR, OPP. SHOPPERS STOP, S.V. ROAD ANDHERI WEST 400058</td>
  </tr>
 <%-- <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EBILAEADSUK</td>
  </tr>--%>
</table><br />
    <%}
        else if (Currency != "SAR")
        {%>
<h4 class="bggray">United Arab Emirates </h4>
<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EMIRATES NBD</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL LLC</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">1 0 1 4 0 4 0 9 2 6 9 0 8 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">AE580260001014040926908</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">AED</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>Branch</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">Al Souk</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">EBILAEADSUK</td>
  </tr>
</table><br />

<%} else{ %>
<h4 class="bggray"> Saudi Arabia </h4> 


<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAMBA</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Account Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL LIMITED CO</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">2 1 0 0 2 0 6 0 1 0 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SA69 4000 0000 0021 0020 6010 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAR</td>
  </tr>

  <tr>
    <td valign="bottom"><strong>SWIFT    CODE</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAMBSARI</td>
  </tr>
</table>



<%--<div style=" border-bottom: solid 2px #ccc"> </div>


<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">NCB</td>
  </tr>
<tr>
    <td valign="bottom"><strong>Accoun Name</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">COZMO TRAVEL LTD.CO</td>
  </tr>
  <tr>
    <td><strong>Account Number</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">2 7 0 5 6 2 8 1 0 0 0 1 0 5 </td>
  </tr>
  <tr>
    <td valign="bottom"><strong>IBAN No</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SA53 1000 0027 0562 8100 0105</td>
  </tr>
  <tr>
    <td valign="bottom"><strong>Currency</strong></td>
    <td valign="bottom">:</td>
    <td valign="bottom">SAR</td>
  </tr>


</table>
--%>

<%} %>

</div>
    
  
    </div>

     <%if (Currency != "INR")
         { %>
    <div id="Tab_3" class="tab-pane fade in">
<div style=" padding:10px">


<div class="paramcon"> 

<div class="marbot_10"> 
<div class="col-md-3"> Amount :  <i class="fcol_red">* </i> </div> 
<div class="col-md-4"> <input type="text" class="form-control">   </div> 
<div class="clearfix"> </div> 
</div>



<div> 
<div class="col-md-3"> Booking Ref Details :  <i class="fcol_red">* </i> </div> 
<div class="col-md-4"> <textarea class="form-control" name="" cols="" rows=""></textarea>  </div> 
<div class="clearfix"> </div> 
</div>



<div class="col-md-12"> 
<h4>Payment Option </h4> 

 We accept only GCC countries Bank issued Visa / Master Card / American Express credit cards.<br>

<div> <img src="images/payment-logos.jpg">   </div> 


 Payment is processed through an online payment gateway. Your credit card transaction will be processed directly with bank which authorizes your credit card. <br>

<div> <a class="btn but_b"> Make Payment </a>   </div> 


</div> 



<div class="clearfix"> </div> 

 </div>

            
  
    

 </div>
        <div style="display:none">
            <%--<iframe id="TopUpFrame"  src="AgentPaymentDetails.aspx?ref=Topup" style="height:400px; width:100%;" ></iframe>--%>
                
        </div>
    </div>
  <%} %>
    <div id="Tab_4" class="tab-pane fade in">
   <div class="table-responsive" style="padding:10px; line-height:21px">
   
   
<table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">

<tr class="hidden-xs"> 

<td> <center> <img height="136" width="427" src="images/support-center.png" /></center></td>
</tr>

</table>   
<%if (Currency != "INR")
    { %>
<table  border="0" cellspacing="0" cellpadding="0">

<tr> 

<td colspan="3">  <strong>Contact No.</strong><br /> </td>

</tr>
<%--<tr> 
<td> Landline</td> <td width="60">: </td> <td> +971 60052444</td>


</tr>--%>


<tr> <td> Landline</td> <td>: </td> <td>+971 6 5074592 </td>
</tr>




<tr> <td> Landline(For KSA)</td> <td>: </td> <td>+966 114818646 </td></tr>

<tr> <td> Mobile</td> <td>: </td> <td>+971 56  4479659 </td></tr>

<tr> 

<td colspan="3">  <strong>Email:</strong><br /> </td>

</tr>


<tr> 
<td>  For any Support Contact</td> <td>: </td> <td> <a href="mailto:B2bsupport@cozmotravel.com">B2bsupport@cozmotravel.com</a></td>

</tr>

<tr> 

<td> For cancellations, refunds, reissuance</td> <td>: </td> <td><a href="mailto:B2bsupport@cozmotravel.com">B2bsupport@cozmotravel.com</a> </td>

</tr>

<tr> <td> For Hotels, Packages, Tours, Insurance</td> <td>: </td> <td><a href="mailto:B2bholidays@cozmotravel.com">B2bholidays@cozmotravel.com</a> </td>
</tr>


<tr> 
<td> For Sales</td> <td>: </td> <td><a href="mailto:B2bsales@cozmotravel.com">B2bsales@cozmotravel.com</a></td>

</tr>
<%if (Currency != "SAR")
    {%>
<tr> <td>  For Topup:</td> <td>: </td> <td><a href="mailto:B2btopup@cozmotravel.com">B2btopup@cozmotravel.com</a> </td></tr>
<%}
    else
    { %>

<tr> <td>  For Topup:</td> <td>: </td> <td><a href="mailto:Ksatopup@cozmotravel.com">Ksatopup@cozmotravel.com</a> </td></tr>

<%} %>


</table>
       <%}
    else
    { %>
       <table  border="0" cellspacing="0" cellpadding="0">

<tr> 

<td colspan="3">  <strong>Contact No.</strong><br /> </td>

</tr>
<%--<tr> 
<td> Landline</td> <td width="60">: </td> <td> +971 60052444</td>


</tr>--%>


<tr> <td> Landline</td> <td>: </td> <td>022 7100 4644</td>
</tr>

<tr> <td> Mobile</td> <td>: </td> <td>9967444999</td></tr>

<tr> 

<td colspan="3">  <strong>Email:</strong><br /> </td>

</tr>


<tr> 
<td>  For any Support Email Id</td> <td>: </td> <td> <a href="supportb2b@cozmotravelworld.com">supportb2b@cozmotravelworld.com</a></td>

</tr>

<tr> 

<td> For INFO Email Id</td> <td>: </td> <td><a href="infob2b@cozmotravelworld.com">infob2b@cozmotravelworld.com</a> </td>

</tr>

<tr> <td> For TOP UP Email id</td> <td>: </td> <td><a href="topupb2b@cozmotravelworld.com">topupb2b@cozmotravelworld.com</a> </td>
</tr>


<tr> 
<td> For SALES Email Id</td> <td>: </td> <td><a href=" salesb2b@cozmotravelworld.com"> salesb2b@cozmotravelworld.com</a></td>

</tr>
</table>

       <%} %>
   
 
 </div>


    </div>
    

      

</div>
  
  
  
  

 </div>
 
 <div class="clearfix"></div>
  </div>
  
  

<div class="clearfix"></div>
</div>
<script>
    $('iframe').load(function() {
        $('#TopUpFrame').contents().find('#header').hide();
        $('#TopUpFrame').contents().find('div.footer_big').hide();
    }); 
</script>
</asp:Content>

