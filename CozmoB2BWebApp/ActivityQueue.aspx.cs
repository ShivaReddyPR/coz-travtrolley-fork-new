﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class ActivityQueueGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected DataTable ActivityList;
    PagedDataSource pagedData = new PagedDataSource();
    bool isShowAll = false;
    protected int AgentId;
    protected AgentMaster agency;
    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
			this.Master.PageRole = true;
            if (!IsPostBack)
            {
                isShowAll = true;
                InitializeControls();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void InitializeControls()
    {
        BindAgent();
        int b2bAgentId;
        int b2b2bAgentId;
        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
        {
            ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        }
        else if(Settings.LoginInfo.AgentType == AgentType.Agent)
        {
            ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlAgency.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B)
        {
            ddlAgency.Enabled = false;
            b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2BAgent.Enabled = false;
        }
        else if(Settings.LoginInfo.AgentType==AgentType.B2B2B)
        {
            ddlAgency.Enabled = false;
            ddlB2BAgent.Enabled = false;
            b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
            ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
            ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2B2BAgent.Enabled = false;
        }
        BindB2BAgent(Convert.ToInt32(ddlAgency.SelectedItem.Value));
        BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        Clear();
    }
    #endregion

    #region BindEvents
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID,"AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
        }
        catch
        { }
    }
    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                //if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
        }
        catch (Exception ex)
        { }
    }

    private void BindActivityQueue()
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            string StartFromDate = string.Empty;
            //if (!isShowAll)
                StartFromDate = (txtFrom.Text);
            //else
            //    StartFromDate = DateTime.MinValue.ToString();
                //StartFromDate = DateTime.MinValue.AddYears(2010).ToString("dd/MM/yyyy");

            DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
            string StartToDate = (txtTo.Text);
            DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
            toDate = Convert.ToDateTime(toDate.ToString("MM/dd/yyyy 23:59"));
            //toDate = Convert.ToDateTime(toDate.ToString("dd/MM/yyyy 23:59"));
            string tripId = txtTripId.Text;
            string paxName = txtPaxName.Text;
            decimal price = Convert.ToDecimal(txtPrice.Text);
            //string paymentId = txtPaymentId.Text;
            int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            string agentType = string.Empty;
            if (agencyId == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty; // null Means binding in list all BOOKINGS
                }
            }
            if (agencyId > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agencyId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agencyId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agencyId > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agencyId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                    }
                }
            }
            //Adding Code here by CSharma 22/08/2015

            string transType = string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }


            //End


            ActivityList = Activity.GetActivityBookingQueue(fromDate, toDate, tripId, paxName, price, agencyId, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID,"N",agentType,string.Empty, transType);
            Session["ActivityQueue"] = ActivityList;
            dlActivityQueue.DataSource = ActivityList;
            dlActivityQueue.DataBind();
            CurrentPage = 0;
            doPaging();
            AgentId= Settings.LoginInfo.AgentId;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Button Click Events
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindActivityQueue();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Private Methods
    private void Clear()
    {
        try
        {
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTripId.Text = string.Empty;
            txtPaxName.Text = string.Empty;
            txtPrice.Text = "0.00";
            //txtPaymentId.Text = string.Empty;
            isShowAll = true;
            //ddlAgency.SelectedIndex = 0;
            Session["ActivityQueue"] = null;
            BindActivityQueue();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected string CTCurrencyFormat(object currency,object AgentId)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency+" "+Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
        }
        else
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency+" "+Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
        }
    }
    //protected string CTCurrencyName(object AgentId)
    //{
    //    if (string.IsNullOrEmpty(AgentId.ToString()) || AgentId == DBNull.Value)
    //    {
    //        agency=new AgentMaster(Convert.ToInt32(AgentId));
    //        return agency.AgentCurrency;
    //    }
    //    else
    //    {
    //        agency = new AgentMaster(Convert.ToInt32(AgentId));
    //        return agency.AgentCurrency;
    //    }
    //}
    #endregion

    #region Paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        DataTable dt = (DataTable)Session["ActivityQueue"];
        pagedData.DataSource = dt.DefaultView;
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlActivityQueue.DataSource = pagedData;
        dlActivityQueue.DataBind();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
    #endregion
}
