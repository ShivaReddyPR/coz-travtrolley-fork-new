﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="SightseeingGuestDetails" Title="Sightseeing Guest Details" Codebehind="SightseeingGuestDetails.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
<!-- Js code here -->
   <script type="text/javascript" language="javascript">
       tinyMCE.init({
            mode: "textareas",
            theme: "advanced",
            width: '100%',
            theme_advanced_buttons1_add: "bullist,numlist,outdent,indent,undo,redo,fontselect,fontsizeselect",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_disable: "styleselect,anchor,formatselect,justifyfull,help,cleanup",
            convert_fonts_to_spans: false,
            element_format: "html",            
            content_css: "/style.css"
       });
       </script>
    <script>

function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
function numbersonly()
{
   var data=document.getElementById('ctl00_cphTransaction_txtMarkup').value;
   var filter=/^\d+(?:\.\d{1,2})?$/;
   if(filter.test(data)==false)
   {   
       return false;
   }       
   else
       return true;
}
function validateName(oSrc, args) {
			    args.IsValid = (args.Value.length >= 2);
			}

function calculateMarkup()
{
    var dp = eval('<%=Settings.LoginInfo.DecimalValue%>');
    var markup =!isNumber(document.getElementById('<%=txtMarkup.ClientID %>').value)?0:document.getElementById('<%=txtMarkup.ClientID %>').value;
    document.getElementById('<%=txtMarkup.ClientID %>').value=parseFloat(markup).toFixed(dp);
    var percent = document.getElementById('<%=rbtnPercent.ClientID %>').checked;
    var fixed = document.getElementById('<%=rbtnFixed.ClientID %>').checked;
    var total;
    
    if(window.navigator.appCodeName != "Mozilla")
    {
        total = document.getElementById('<%=lblCost.ClientID %>').innerText;
    }
    else
    {
        total = document.getElementById('<%=lblCost.ClientID %>').textContent;
    }
    total = total.replace(',','');
    var temp = 0;
    
    total = parseFloat(total).toFixed(dp);
    total = Math.ceil(total);
    if(markup.length > 0)
    {
        if(percent == true)
        {
            temp =  eval(parseFloat(total)) +  eval(parseFloat((total * (markup / 100))).toFixed(dp));
                      
        }
        else if (fixed == true)
        {
            temp = eval(parseFloat(total)) + eval(parseFloat(markup).toFixed(dp));
        }
        temp = parseFloat(temp).toFixed(dp);
        if(temp == 0)
        {
            temp = total;
        }
        if(window.navigator.appCodeName != "Mozilla")
        {
            document.getElementById('<%=lblTotal.ClientID %>').innerText = temp;
        }
        else
        {
        document.getElementById('<%=lblTotal.ClientID %>').textContent = temp;
        }
    }
    else
    {
        if(window.navigator.appCodeName != "Mozilla")
        {
            document.getElementById('<%=lblTotal.ClientID %>').innerText = (total);
        }
        else
        {
            document.getElementById('<%=lblTotal.ClientID %>').textContent = (total);
        }
    }
}
function showFare(fareId) {
    if ($(fareId).style.display != "block") {
        $(fareId).style.display = "block";
    }
    else {
        $(fareId).style.display = "none";
    }
}
function hide(id) {
    $(id).style.display = "none";
}

function sendMail() {
    document.getElementById('<%=action.ClientID %>').value = "SendEmail";
    document.forms[0].submit();
}
function BookSightseeing() {
    document.getElementById('<%=action.ClientID %>').value = "BookSightseeing";
}       
       
</script> 
 <script type="text/javascript">
     var specialKeys = new Array();
     specialKeys.push(8); //Backspace
     specialKeys.push(9); //Tab
     specialKeys.push(46); //Delete
     specialKeys.push(36); //Home
     specialKeys.push(35); //End
     specialKeys.push(37); //Left
     specialKeys.push(39); //Right
     function IsAlphaNumeric(e) {
         var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
         var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
         return ret;
     }
    </script>
<!-- Js code End Here -->
<asp:HiddenField ID="action" runat="server" />
 <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    
    <div>
      
          
          
            <div class="col-md-3 padding-0 bg_white">
                <div class="">
                    <div class="ns-h3">
                        Pricing Details</div>
                 
                    <div style="padding-bottom: 5px;">
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="gray-smlheading">
                                    <strong>Product</strong>
                                </td>
                                <td class="gray-smlheading">
                                    <strong>Price</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sightseeing Cost
                                </td>
                                <td>
                                    <%=(itinerary.Currency) %>
                                    <asp:Label ID="lblCost" runat="server" Text=""></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    (Inclusive of all Taxes &amp; Fees)
                                </td>
                            </tr>
                           
                            <tr>
                                <td>
                                    <strong>Total</strong>
                                </td>
                                <td>
                                    <strong><%=(itinerary.Currency)%>
                                        <asp:Label ID="lblTotal" runat="server" ></asp:Label></strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="ns-h3">
                        Markup</div>
                    <div style="padding: 5px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="69%">
                                    <span style="font-size: 11px"><strong>Add Markup</strong></span> <strong>
                                        <label>                                            
                                        
                                            <asp:TextBox ID="txtMarkup" CssClass="inp_144" Width="50px" MaxLength="10" runat="server" onblur="calculateMarkup();" onkeypress="return isNumber(event);" onpaste="return false;" ondrop="return false;" Text="0"></asp:TextBox>
                                        </label>
                                    </strong>
                                </td>
                                <td width="15%">
                                    P
                                    <label>
                                        <asp:RadioButton ID="rbtnPercent" runat="server" Checked="True" 
                                        GroupName="markup" onclick="calculateMarkup();"/>
                                    </label>
                                </td>
                                <td width="16%">
                                    F
                                     <label>
                                    <asp:RadioButton ID="rbtnFixed" runat="server" GroupName="markup" onclick="calculateMarkup();"/>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="ns-h3">
                        Sightseeing Details</div>
                    <div style="padding-bottom: 5px;">
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="42%">
                                    <strong>Sightseeing name :</strong>
                                </td>
                                <td width="58%" style="font-weight:bold">
                                    <%=itinerary.ItemName %>
                                </td>
                            </tr>
                            <tr>
                                <td width="42%">
                                    <strong>Language :</strong>
                                </td>
                                <td width="58%" style="font-weight:bold">
                                    <%=itinerary.Language.Split('#')[0] %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>City Name : </strong>
                                    <br />
                                </td>
                                <td>
                                    <%=itinerary.CityName %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Tour Date:
                                        <br />
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.TourDate.ToString("dd-MMM-yyyy") %>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>Duration: </strong>
                                </td>
                                <td>
                                    <%=itinerary.Duration == "" ? "N/A" : itinerary.Duration%>
                                </td>
                            </tr>
                              <%int adults = 0, childs = 0;
                              
                                  adults = itinerary.AdultCount;
                                  childs = itinerary.ChildCount;
                              %>
                            <tr>
                                <td>
                                    <strong>No Of Pax: </strong></td>
                                <td>
                                    Adults 
                                   
                                    <%=adults %>
                                      ,Child 
                                   
                                    <%=childs %>                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>Price :</strong>
                                </td>
                                <td>
                                    <strong><%=(itinerary.Currency)%>
                                        <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                        </strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                   
                    <div class="clear">
                    </div>
                </div>
                
            </div>
           
           
           <div class="col-md-9 pad_right0 paramcon">
                
               
                <div class="ns-h3">
                    Please enter the details of the guest.
                </div>
                    <div class="bg_white" style=" border:solid 1px #ccc; padding:10px; padding-top:0px;">
                       
                        <%--<div style=" border:solid 1px #ccc; border-top:0px; margin-bottom:10px; padding-bottom:10px;">  --%>    
                            
                                <table id="tblAdults" width="100%" border="0" cellspacing="3" cellpadding="0" runat="server">
                                
                                </table>
                                <table>
                                 <tr>
                                <td>
                                <%if (isHotelChooseDepPoint)
                                  { %>
                               
                                
                                <div>
                                
                                <div class="col-md-4"> <label class="pull-right fl_xs">Choose&nbsp;Departure&nbsp;Point:</label> </div
                                
                                <div class="col-md-5"> <asp:DropDownList Width="300px" CssClass="form-control" ID="ddlHotels" runat="server" ></asp:DropDownList></div>
    
                                <div class="clearfix"> </div>
                                  </div>   
                
              
               <%}
                                  else if (searchResult.DepaturePointRequired && !string.IsNullOrEmpty(itinerary.DepTime))
                                  { %>
                                  
                                  <div> 
                                   <div class="col-md-4"><label  class="pull-right fl_xs">Choose&nbsp;Departure&nbsp;Time:</label>  </div>
                                  
                                   <div class="col-md-5"><asp:DropDownList Width="300px"  CssClass="form-control" ID="ddlTimes" runat="server" ></asp:DropDownList> </div>
    
    
    <div class="clearfix"> </div>
                                  </div>   
                                     
                                <%} %>
                                  </td>
                                </tr>
                                </table>
                                  <%-- </div>--%>
                    </div>
                    
              
              
             <div>  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     
                      <%--  <tr>
                            <td colspan="2">
                                   
                  <%--  <div style=" border:solid 1px #ccc; border-top:0px; margin-bottom:10px; padding-bottom:10px;">
                                <table id="tblChilds" width="100%" border="0" cellspacing="3" cellpadding="0" runat="server">
                                
                                    
                                </table><br />
                            </div>
                 
                    </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2">
                                <div style="padding: 10px;overflow:auto">
                                 
                                   <div> <span style="font-weight:bold; ">Cancellation &amp; Charges</span> </div>
                                   <ul style="float: left; font-family:Calibri; font-size:14px;">
                                        <%if (string.IsNullOrEmpty(itinerary.CancellationPolicy))
                                            { %>
                                    <%string[] cdata = cancelData.Split('|');
                                        foreach (string data in cdata)
                                        { %>
                                       <li><%=data%></li>                                           
                                          <%}%>
                                       <%} else { %>
                                       <li><%=itinerary.CancellationPolicy%></li> 
                                       <%} %>
                                          </ul>
                                        
                                          
                                          </div>
                               <%if (!string.IsNullOrEmpty(amendmentData) || amendmentData!="")
                                       { %>
                                        <div style="padding: 10px;overflow:auto">
                                        
                                            <div>
                                                <span style=" font-weight:bold; float: left">Amendment Policies</span>
                                            </div><br />
                                            <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                                <%string[] aData = amendmentData.Split('|');
                                       foreach (string amendData in aData)
                                       {%>
                                                <li>
                                                    <%=amendData%></li>
                                                <%}
                                                %>
                                            </ul>
                                        </div>
                                <%} %>
                                        <%if (itinerary.Note != null && itinerary.Note.Length > 1)
                                          { %>
                                         <div style="padding: 10px;overflow:auto">
                                        
                                            <div>
                                                <span style="font-weight:bold; float: left">Notes &amp; Additional Information</span>
                                            </div><br />
                                             <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                            
                                                 <li>
                                                     <%=itinerary.Note.Split('#')[0]%></li>
                                                     
                                                 <%if (additionalInfo != null && additionalInfo.Length > 0)
                                                   {
                                                       foreach (string addinfo in additionalInfo)
                                                       { %>
                                                       
                                                 <li>
                                                     <%=addinfo%></li>
                                                 <%} %>
                                             </ul>
                                        </div>
                                         <%}
                                          }%>

                                 <%if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 1)
                                          { %>
                                         <div style="padding: 10px;overflow:auto">
                                        
                                            <div>
                                                <span style="font-weight:bold; float: left">Pcikup Location &amp; Pcikup Date Time</span>
                                            </div><br />
                                             <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                            
                                                  <%string[] aData = itinerary.DepPointInfo.Split(',');
                                       foreach (string depoint in aData)
                                       {%>
                                                <li>
                                                    <%=depoint%></li>
                                                <%}
                                                %>
                                                     
                                             </ul>
                                        </div>
                                         <%
                                          }%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="color: Red; font-size: 14px">
                            <%=warningMsg %>
                        </div>
                            </td>
                        </tr>
                         <% bool hotelValidate = false;

               if (isHotelChooseDepPoint)
               {
                   if (hotelResults.Length > 0)
                   {
                       hotelValidate = true;
                   }
               }
               else
               {
                   hotelValidate = true;
               }
                 %>
                   <%if ( !string.IsNullOrEmpty(itinerary.CancellationPolicy) || (CancellationData.Count > 0 && CancellationData["CancelPolicy"] != null && CancellationData["CancelPolicy"].Length > 1) )
                     {%>
                
                <%if (hotelValidate)
                  { %>
                        <tr>
                            <td colspan="2" align="right">
                                
                                <label style="padding-right: 10px">                                    
                                    <asp:Button ID="imgContinue" runat="server" CssClass="but but_b" Text="Continue"  
                                    OnClientClick="BookSightseeing();" 
                                    OnClick="imgContinue_Click" CausesValidation="true" ValidationGroup="pax" />
                                    </label>
                            </td>
                        </tr>
                        <%}
                  else
                  {%>
                        <tr>
                            <td colspan="2" align="right">
                               <label style="color: Red; font-weight: bold;">No Hotels Found</label><br />
                               <a href="SightseeingResult.aspx">Back To Result Page</a>
                            </td>
                        </tr>
                        <%
                       }
                     }
                     else
                     {%>
                     <tr>
                            <td colspan="2" align="right">
                               <label style="color: Red; font-weight: bold;">No Cancellation Data Found</label><br />
                               <a href="SightseeingResult.aspx">Back To Result Page</a>
                            </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table> </div>
                
            </div>
            <div class="clear"> </div>
     
  
  </div>
  
  
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

