﻿using CT.TicketReceipt.Common;
using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;

public partial class SignatureUploadGUI : CT.Core.ParentPage
{   
    protected void Page_Load(object sender, EventArgs e)
    {
      
    }
    #region Button Events
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
               string path = ConfigurationManager.AppSettings["SignatureImages"];
                if (fuMultiple.HasFiles)
                {
                    string foldername = string.Empty;
                    foldername = path;
                    if (!Directory.Exists(foldername))
                    {
                        Directory.CreateDirectory(foldername);
                    }
                    foreach (HttpPostedFile file in fuMultiple.PostedFiles)
                    {
                        string name = file.FileName;
                        string extension = Path.GetExtension(file.FileName);
                        if (extension == ".png" || extension == ".gif" || extension == ".html" || extension == ".jpg" || extension == ".css" || extension == ".js")
                        {
                            string imagePath = foldername + name;
                            file.SaveAs(imagePath);
                        }
                    }
                    Utility.Alert(this.Page, "SuccessFully Uploaded");
                }          
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #endregion
}