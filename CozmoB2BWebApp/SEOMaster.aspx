﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="SEOMasterGUI" Title="SEO Master" Codebehind="SEOMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <table width="100%" class="label" border="0">
        <tr>
            <td align="center">
                <div title="header" class="outer-center">
                    <div class="inner-center">
                        <table cellpadding="0" border="0" cellspacing="0" class="label">
                            <tr>
                                <td colspan="2" style="height: 30px;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        Agent:*</p>
                                    <p>
                                        &nbsp;</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:DropDownList ID="ddlAgent" CssClass="inputDdlEnabled" runat="server" Width="250px">
                                        </asp:DropDownList>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        PageName:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtPageName" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        Url:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtUrl" CssClass="inputEnabled" runat="server" Width="500px" TextMode="SingleLine"
                                            ></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        URLRewriting:</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtURLRewriting" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="SingleLine" ></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        PageTitle:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtPageTitle" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        ImageAltText:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtImageAltText" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        MetaDescription:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtMetaDescription" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        MetaKeywords:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtMetaKeywords" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            
                             <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        Social Tag:*</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtSocialTag" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        H1 Tag:</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtH1Tag" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <p>
                                        H1Tag Description:</p>
                                </td>
                                <td width="30%">
                                    <p>
                                        <asp:TextBox ID="txtH1TagDesc" CssClass="inputEnabled" runat="server" Width="500px"
                                            TextMode="MultiLine" Height="55px"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left" width="100%">
                                    <asp:Button ID="btnSave" CssClass="button" Text="Save" runat="server" OnClientClick="return Save();"
                                        OnClick="btnSave_Click" Style="display: inline;" />&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" CssClass="button" Text="Clear" runat="server" OnClick="btnClear_Click"
                                        Style="display: inline;" />&nbsp;&nbsp;
                                    <asp:Button ID="btnSearch" CssClass="button" Text="Search" runat="server" OnClick="btnSearch_Click"
                                        Style="display: inline;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clear" />
            </td>
        </tr>
    </table>
<asp:Label ID="lblSuccessMsg" runat="server" Visible="false" CssClass="lblSuccess"></asp:Label>
<script>
    function Save() {

        if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please select Agent!', '');
        if (getElement('txtPageName').value == '') addMessage('PageName cannot be blank!', '');
        if (getElement('txtUrl').value == '') addMessage('URl cannot be blank!', '');
        if (getElement('txtPageTitle').value == '') addMessage('PageTitle cannot be blank!', '');
        if (getElement('txtImageAltText').value == '') addMessage('ImageAltText cannot be blank!', '');
        if (getElement('txtMetaDescription').value == '') addMessage('MetaDescription cannot be blank!', '');
        if (getElement('txtMetaKeywords').value == '') addMessage('MetaKeywords cannot be blank!', '');
        if (getElement('txtSocialTag').value == '') addMessage('SocialTags cannot be blank!', '');
        if (getMessage() != '') {

            alert(getMessage()); clearMessage(); return false;
        }
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="id"
        EmptyDataText="No SEO List Available!" AutoGenerateColumns="false" PageSize="15"
        GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
        CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-Width="25px" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtPageName" Width="120px" HeaderText="PageName" CssClass="inputEnabled"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblPageName" runat="server" Text='<%# Eval("PageName") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("PageName") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtURL" Width="120px" CssClass="inputEnabled" HeaderText="URL"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblURL" runat="server" Text='<%# Eval("URL") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("URL") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtURLRewriting" Width="120px" CssClass="inputEnabled" HeaderText="URLRewriting"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblURLRewriting" runat="server" Text='<%# Eval("URLRewriting") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("URLRewriting") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtAgentName" Width="120px" CssClass="inputEnabled" HeaderText="AgentName"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblAgentName" runat="server" Text='<%# Eval("AgentName") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("AgentName") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtMemberName" Width="120px" CssClass="inputEnabled" HeaderText="MemberName"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblMemberName" runat="server" Text='<%# Eval("MemberName") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("MemberName") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="gvHeader"></HeaderStyle>
        <RowStyle CssClass="gvRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvAlternateRow" />
    </asp:GridView>
</asp:Content>

