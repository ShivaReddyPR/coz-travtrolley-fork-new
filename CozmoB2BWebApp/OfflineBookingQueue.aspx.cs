﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class OfflineBookingQueue : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                Dictionary<string, string> List = new Dictionary<string, string>();
                List.Add("AgentType", Settings.LoginInfo.AgentType.ToString());
                List.Add("LocationId",Convert.ToString( Settings.LoginInfo.LocationID));
                List.Add("AgentId", Convert.ToString(Settings.LoginInfo.AgentId));
                List.Add("MemberType", Convert.ToString(Settings.LoginInfo.MemberType));
                hdfAgent.Value = JsonConvert.SerializeObject(List); 
            }
        }
        [WebMethod]
        public static string BindPageLoadControls()
        {
         DataSet DropdownDs = GetDropDownValues(Settings.LoginInfo.AgentId);
            string JSONString = JsonConvert.SerializeObject(DropdownDs);
            return JSONString;
        }
        [WebMethod]
        public static ArrayList BindAllAgentTypes(int agentId, string AgentType)
        {
            ArrayList list = new ArrayList();
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, AgentType, agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            if (dtAgents != null && dtAgents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtAgents.Rows)
                {
                    list.Add(new ListItem(
              dr["Agent_Name"].ToString(),
              dr["agent_id"].ToString()
               ));
                }
            }
            return list;
        }
        [WebMethod]
        public static ArrayList BindLocation(int agentId, string type)
        {
            ArrayList list = new ArrayList();
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);
            if (dtLocations != null && dtLocations.Rows.Count > 0)
            {
                foreach (DataRow dr in dtLocations.Rows)
                {
                    list.Add(new ListItem(
              dr["location_name"].ToString(),
              dr["location_id"].ToString()
               ));
                }
            }
            return list;
        }
        public static DataSet GetDropDownValues(int Agent_id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOGIN_AGENT_ID", Agent_id);
                return DBGateway.ExecuteQuery("USP_FlightQueue_Get_DropDown_values", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [WebMethod]
        //[ScriptMethod]
        public static string Search(String FromDate, String ToDate, int LocationId ,int AgentFilter, string agentType,int productId)
        {
            try
            {
                string JSONString = string.Empty;
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                startDate = (FromDate != string.Empty) ? Convert.ToDateTime(FromDate, provider) : DateTime.Now;
                endDate = (ToDate != string.Empty) ? Convert.ToDateTime(Convert.ToDateTime(ToDate, provider).ToString("dd/MM/yyyy 23:59"), provider) : DateTime.Now;
                // DataTable Dt = (productId == 1) ? (OfflineItinerary.GetOfflineFLQueueList(startDate, endDate, LocationId, AgentFilter, agentType)) : (OfflineHotelItinerary.GetOfflineHTLQueueList(startDate, endDate, LocationId, AgentFilter, agentType));
                if (productId == 1)
                {
                    DataSet Ds = OfflineItinerary.GetOfflineFLQueueList(startDate, endDate, LocationId, AgentFilter, agentType, Settings.LoginInfo.CorporateProfileId);
                    JSONString = JsonConvert.SerializeObject(Ds);
                }
                else if(productId == 2)
                {
                    DataTable Dt = OfflineHotelItinerary.GetOfflineHTLQueueList(startDate, endDate, LocationId, AgentFilter, agentType, Settings.LoginInfo.CorporateProfileId);
                         JSONString = JsonConvert.SerializeObject(Dt);
                }
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static string SaveUpdateStatus(string status, int flightId, string remarks, string productId) //To Save The Status 
        {
            string statusMsg = string.Empty;
            try
            {
                int statusUpdated = 0;
                using (TransactionScope updateTransaction = new TransactionScope())
                {
                    statusUpdated = OfflineItinerary.UpdateOffLineStatus(flightId, status, Convert.ToInt32(Settings.LoginInfo.UserID),Convert.ToInt32(productId));
                    if (statusUpdated > 0) //Saving The Offline status History 
                    {
                        int historyUpdated = OfflineItinerary.SaveOfflineStatusHistory(flightId, status, remarks, Convert.ToInt32(Settings.LoginInfo.UserID), Convert.ToInt32(productId));
                        if (historyUpdated > 0)
                        {
                            updateTransaction.Complete();
                            statusMsg = "S";
                        }
                    }
                }
                return statusMsg;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(SaveUpdateStatus) Error in Offline Booking queue status updation." + ex.ToString(), "");
                return statusMsg = ex.Message;
            }
            
        }

        /// <summary>
        /// To save approval status of the transaction
        /// </summary>
        /// <param name="status"></param>
        /// <param name="transid"></param>
        /// <param name="remarks"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string SaveApprovalStatus(string status, int transid, string remarks, int productId, 
            int approver, string hostName)
        {
            string statusMsg = "S";
            try
            {
                DataTable dt = OfflineItinerary.GetOfflineApproval(transid, productId == 1 ? "OF" : "OH", 
                    approver > 0 ? approver : Settings.LoginInfo.CorporateProfileId);

                if (dt != null && dt.Rows.Count > 0)
                {
                    statusMsg = "Failed to update status, as expense is already " + 
                        (Convert.ToString(dt.Rows[0]["ApprovalStatus"]) == "A" ? "APPROVED" : "REJECTED") + 
                        " by " + Convert.ToString(dt.Rows[0]["UpdatedUser"]) + " on " + Convert.ToString(dt.Rows[0]["UpdatedDate"]) + ".";
                }
                else
                {
                    OfflineItinerary.SaveOfflineApprovalStatus(transid, status, Convert.ToInt32(Settings.LoginInfo.CorporateProfileId),
                        remarks, (int)Settings.LoginInfo.AgentId, (int)Settings.LoginInfo.UserID, string.Empty, hostName, productId);
                }
                return statusMsg;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(SaveApprovalStatus) Error in Offline Booking queue approval status updation." + ex.ToString(), "");
                return statusMsg = ex.Message;
            }
        }
    }
}
