﻿using System;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Data;

public partial class ViewBookingforPackage : CT.Core.ParentPage// System.Web.UI.Page
{
    protected DataTable dtSupplierDetails;
    protected DataTable dtSupplierDetailsHistory;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            AuthorizationCheck();
            if (!IsPostBack)
            {
                if (Request.QueryString["tranxId"] != null && Request.QueryString.Count > 0)
                {
                    LoadSupplierDetailsByTranxId(Convert.ToInt32(Request.QueryString["tranxId"]));
                    LoadSupplierDetailsHistoryByTranxId(Convert.ToInt32(Request.QueryString["tranxId"]));
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (ViewBookingforPackage Page):" + ex.ToString(), "");
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }

    }

    private void LoadSupplierDetailsByTranxId(int tranxId)
    {
        try
        {
            dtSupplierDetails = Queue.GetPackageSuppliersDetailsByTranxId(tranxId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void LoadSupplierDetailsHistoryByTranxId(int tranxId)
    {
        try
        {
            dtSupplierDetailsHistory = Queue.GetPackageSuppliersDetailsHistoryByTranxId(tranxId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(hdnUpdateValue.Value))
            {
                string[] selSuppliers = hdnUpdateValue.Value.Split('|');

                //Eg:58^C^123^TEST|58^X^12g^TEST2|58^W^12sdf^TEST5
                //Status : P-Pending,W-Waiting for Reconf,C-Confirmed,X-Cancelled

                if (selSuppliers.Length > 0)
                {
                    
                    foreach (string selSupp in selSuppliers)
                    {
                        int id;
                        int.TryParse(selSupp.Split('^')[0], out id); //returns the id

                        string status = selSupp.Split('^')[1].Split('~')[0];
                        string refNo = selSupp.Split('^')[1].Split('~')[1];
                        string remarks = selSupp.Split('^')[1].Split('~')[2];
                        Queue.UpdateSuppTranxConfirmStatus(id, status, remarks, refNo, (int)Settings.LoginInfo.UserID);



                    }
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (ViewBookingforPackage Page):" + ex.ToString(), "");
        }
        finally
        {
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "clearAll();", "SCRIPT");
        }
    }

}