using System;
using System.Web;
using System.IO;

public partial class ImageShow : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["FilePath"] != null)
        {
            try
            {
                // Read the file and convert it to Byte Array
                string filePath = Request.QueryString["FilePath"];
                //= Request.QueryString["FileName"];
                string fileName = Path.GetFileName(filePath);
                //Path.GetExtension(Request.QueryString["FileName"].Replace(".",""));
                string contenttype = "image/" + Path.GetExtension(fileName.Replace(".", ""));
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();

                //Write the file to response Stream
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = contenttype;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch
            {
            }
        }
    }
}
