﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.Common;
using CT.MetaSearchEngine;
using CT.AccountingEngine;

public partial class SightseeingChangeRequestQueueUI : CT.Core.ParentPage
{
    protected List<int> sourcesList = new List<int>();
    protected List<int> selectedSources = new List<int>();
    protected List<SightseeingItinerary> itinerary = new List<SightseeingItinerary>();
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected ServiceRequest[] serviceRequest = new ServiceRequest[0];

    UserMaster loggedMember = null;
    protected Int64 agencyIdCSV;
    string bookingSourceCSV = null;
    string requestTypeCSV = null;
    string serviceRequestTypeCSV = null;
    int agencyTypeCSV;
    bool? isDomestic = null;
    protected List<int> requestTypes = new List<int>();
    protected List<int> requestStatuses = new List<int>();
    private int loggedMemberId;
    protected bool isAssignedRequest;
    //protected CT.Core.Queue[] relevantQueues;
    protected string agentFilter = string.Empty; // For restricted filtering
    protected string loginFilter = null; // For restricted filtering
    protected string paxFilter = null;// For restricted filtering
    protected string pnrFilter = null;// For restricted filtering
    protected string sightseeingCancel = string.Empty; // For filtering with paging 
   // protected string sightseeingAmendment = string.Empty;
    protected List<ChangeRequestQueue> data = new List<ChangeRequestQueue>();
    protected int recordsPerPage = 10;
    protected int noOfPages,noOfGuests;
    protected int pageNo;
    protected int queueCount = 0;
    protected string sightseeingFilter, show, paxFilterUI;
    protected string requestSourceIdCSV = null;
    protected string ticketFilter = null;
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            this.Master.PageRole = true;
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            if (Settings.LoginInfo != null)
            {

                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                    selectedSources = sourcesList;
                    sightseeingCancel = "SightseeingCancel";
                    //sightseeingAmendment = "SightseeingAmendment";
                }

                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 0)
                {
                    isAdmin = true;
                }
                else
                {
                    loginFilter = Convert.ToString(Session["loginName"]);
                }
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    sourcesList = SightseeingItinerary.LoadSourceId();
                    selectedSources = sourcesList;

                    txtCheckIn.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtCheckOut.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    hdfParam.Value = "0";

                    ddlAgents.DataSource = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                    ddlAgents.DataTextField = "agent_name";
                    ddlAgents.DataValueField = "agent_id";
                    ddlAgents.DataBind();

                    Array Sources = Enum.GetValues(typeof(SightseeingBookingSource));
                    foreach (SightseeingBookingSource source in Sources)
                    {
                        if (source == SightseeingBookingSource.GTA)
                        {
                            ListItem item = new ListItem(Enum.GetName(typeof(SightseeingBookingSource), source), ((int)source).ToString());
                            ddlSource.Items.Add(item);
                        }
                    }

                    ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
                    ddlLocations.DataTextField = "location_name";
                    ddlLocations.DataValueField = "location_id";
                    ddlLocations.DataBind();

                    Array Statuses = Enum.GetValues(typeof(ServiceRequestStatus));
                    foreach (ServiceRequestStatus status in Statuses)
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(ServiceRequestStatus), status), ((int)status).ToString());
                        ddlBookingStatus.Items.Add(item);
                    }
                   
                    ddlAgents.SelectedIndex = -1;
                    if (Settings.LoginInfo.AgentId > 0)
                    {
                        ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    }
                    if (Settings.LoginInfo.AgentId > 1)
                    {
                        ddlAgents.Enabled = false;
                    }
                        LoadChangeQueues();

                }
                else
                {
                    SetFilters();
                }

                // records per page from configuration

                recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);

                //int agencyId = 1;

                loggedMember = new UserMaster(Convert.ToInt32(Session["memberId"]));



            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.SightseeingCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void LoadChangeQueues()
    {
        try
        {
            if (!IsPostBack)
            {
                agencyIdCSV = Utility.ToInteger(Settings.LoginInfo.AgentId);
            }
            else
            {
                agencyIdCSV = Convert.ToInt32(ddlAgents.SelectedItem.Value);
            }
                if (requestTypeCSV == null)
                {
                    requestTypes.Add((int)RequestType.SightSeeingCancel);
                    // requestTypes.Add((int)RequestType.SightSeeingAmendment);
                    sightseeingCancel = "SightseeingCancel";
                    //sightseeingAmendment = "SightseeingAmendment";

                    requestTypeCSV = "6,7";
                }
                if (serviceRequestTypeCSV == null)
                {
                    if (isAssignedRequest)
                    {
                        requestStatuses.Add((int)ServiceRequestStatus.Unassigned);
                        requestStatuses.Add((int)ServiceRequestStatus.Assigned);
                        requestStatuses.Add((int)ServiceRequestStatus.Acknowledged);
                        requestStatuses.Add((int)ServiceRequestStatus.Closed);
                        requestStatuses.Add((int)ServiceRequestStatus.Pending);
                        requestStatuses.Add((int)ServiceRequestStatus.Rejected);
                        requestStatuses.Add((int)ServiceRequestStatus.Completed);
                        serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                    }
                }
                isDomestic = true;

                string endDate = txtCheckOut.Text;
                endDate += " 23:59:59";
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                data = CT.Core.Queue.GetSightseeingChangeRequestData(((pageNo - 1) * recordsPerPage) + 1, (pageNo * recordsPerPage), requestTypeCSV, serviceRequestTypeCSV, agencyTypeCSV, bookingSourceCSV, sightseeingFilter, agencyIdCSV, paxFilter, loginFilter, pnrFilter, ticketFilter, isDomestic, ref queueCount, requestSourceIdCSV, Convert.ToDateTime(txtCheckIn.Text, dateFormat), Convert.ToDateTime(endDate, dateFormat));
                CT.Core.Queue[] queues = new CT.Core.Queue[data.Count];
                for (int j = 0; j < data.Count; j++)
                {
                    queues[j] = new CT.Core.Queue();
                    queues[j].QueueId = data[j].QueueId;
                    queues[j].ItemId = data[j].ItemId;
                }
                AssignToVariables(queues);

                string url = "";
                if (queueCount > 0)
                {
                    if ((queueCount % recordsPerPage) > 0)
                    {
                        noOfPages = (queueCount / recordsPerPage) + 1;
                    }
                    else
                    {
                        noOfPages = (queueCount / recordsPerPage);
                    }
                }
                else
                {
                    noOfPages = 0;
                }

                if (noOfPages > 0)
                {
                    //For Paging
                    show = MetaSearchEngine.PagingJavascript(noOfPages, url, Convert.ToInt32(PageNoString.Value));
                }

                if (sightseeingFilter == null)
                {
                    sightseeingFilter = "";
                }
                if (agencyIdCSV == 0)
                {
                    agentFilter = "";
                }
                if (paxFilter == null)
                {
                    paxFilter = "";
                }
                if (pnrFilter == null)
                {
                    pnrFilter = "";
                }
                if (loginFilter == null)
                {
                    loginFilter = "";
                }
            }
        
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.SightseeingCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Change REquest Queue: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void AssignToVariables(CT.Core.Queue[] relevantQueues)
    {
        try
        {
            ArrayList listOfItemId = new ArrayList();
            int numBooking = 0;
            foreach (CT.Core.Queue queue in relevantQueues)
            {
                ServiceRequest sr = new ServiceRequest(queue.ItemId);
                listOfItemId.Add(queue.ItemId);
                numBooking++;
            }
            bookingDetail = new BookingDetail[numBooking];

            serviceRequest = new ServiceRequest[numBooking];
            for (int count = 0; count < listOfItemId.Count; count++) //@@@@ From here
            {
                ServiceRequest sRequest = new ServiceRequest(Convert.ToInt32(listOfItemId[count]));
                serviceRequest[count] = sRequest;
                BookingDetail bDetail = null;
                try
                {
                    bDetail = new BookingDetail(serviceRequest[count].BookingId);
                    bookingDetail[count] = bDetail;
                }
                catch { }
                Product[] products = BookingDetail.GetProductsLine(serviceRequest[count].BookingId);
                AgentMaster agency = new AgentMaster(bDetail.AgencyId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i] != null)
                    {
                        string productType = Enum.Parse(typeof(ProductType), products[i].ProductTypeId.ToString()).ToString();
                        switch (productType)
                        {
                           
                            case "SightSeeing": SightseeingItinerary itineary = new SightseeingItinerary();
                                itineary.Load(products[i].ProductId);
                                itineary.ProductId = products[i].ProductId;
                                itineary.ProductType = products[i].ProductType;
                                itineary.ProductTypeId = products[i].ProductTypeId;
                                itineary.Note = sRequest.Data; //Remarks will be add here for only showing purpose

                               
                                itinerary.Add(itineary);
                               
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            Session["ServiceRequests"] = serviceRequest;
            dlChangeRequests.DataSource = itinerary;
            dlChangeRequests.DataBind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.ChangeRequest, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void SetFilters()
    {

        
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAssignedRequest = true;
            isAdmin = true;
        }
       
        //agencyTypeCSV = "1";
        

            if (requestTypeCSV == null)
            {
                requestTypeCSV = "6";
            }
            sightseeingCancel = Request["SightseeingCancel"];
       
        //if (Request["SightseeingAmendment"] != null)
        //{
        //    if (requestTypeCSV == null)
        //    {
        //        requestTypeCSV = "7";
        //    }
        //    else
        //    {
        //        requestTypeCSV += ",7";
        //    }
        //    sightseeingAmendment = Request["SightseeingAmendment"];
        //}

        switch (ddlBookingStatus.SelectedItem.Text)
        {
            case "Unassigned":
                serviceRequestTypeCSV = "1";
                break;
            case "Assigned":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "2";
                }
                else
                {
                    serviceRequestTypeCSV += ",2";
                }
                break;
            case "Acknowledged":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "3";
                }
                else
                {
                    serviceRequestTypeCSV += ",3";
                }
                break;
            case "Completed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "4";
                }
                else
                {
                    serviceRequestTypeCSV += ",4";
                }
                break;
            case "Rejected":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "5";
                }
                else
                {
                    serviceRequestTypeCSV += ",5";
                }
                break;
            case "Closed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "6";
                }
                else
                {
                    serviceRequestTypeCSV += ",6";
                }
                break;
            case "Pending":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "7";
                }
                else
                {
                    serviceRequestTypeCSV += ",7";
                }
                break;
            case "All":
                serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                break;
        }


        selectedSources = new List<int>();
        sourcesList = SightseeingItinerary.LoadSourceId();
        bookingSourceCSV = null;
      

        if (ddlSource.SelectedItem.Text == "All")
        {
            for (int i = 1; i <= sourcesList.Count; i++)
            {
                
                selectedSources.Add(i);
                if (bookingSourceCSV != null)
                {
                    bookingSourceCSV += "," + i.ToString();
                }
                else
                {
                    bookingSourceCSV = i.ToString();
                }
                
            }
        }
        else
        {
            bookingSourceCSV = Convert.ToString(ddlSource.SelectedItem.Value);
        }

        if (!string.IsNullOrEmpty(txtSightseeingName.Text))
        {
            sightseeingFilter = txtSightseeingName.Text.Replace("'", "''").Trim();
        }
        if (!string.IsNullOrEmpty(txtAgentLoginName.Text.Trim()))
        {
            loginFilter = txtAgentLoginName.Text.Replace("'", "''").Trim();
        }
        if (!string.IsNullOrEmpty(txtPaxName.Text))
        {
            paxFilterUI = txtPaxName.Text;
            paxFilter = txtPaxName.Text.Replace("'", "''").Trim();
            paxFilter = paxFilter.Replace(" ", "");
        }
        if (!string.IsNullOrEmpty(txtConfirmNo.Text))
        {
            pnrFilter = txtConfirmNo.Text.Replace("'", "''").Trim();
        }
        
        if (ddlAgents.SelectedItem.Text != "All")
        {
            agencyIdCSV = Convert.ToInt32(ddlAgents.SelectedItem.Value);
        }
         requestSourceIdCSV = "2,3";
       
       
        if (agencyIdCSV == 0)
        {
            agentFilter = "";
            agencyIdCSV = 0;
        }
      

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        LoadChangeQueues();
    }
    protected void dlChangeRequests_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Refund")
            {
                TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
                Label lblError = e.Item.FindControl("lblError") as Label;
                lblMessage.Text = "";

                if (txtAdminFee.Text.Trim().Length > 0 && txtSupplierFee.Text.Trim().Length > 0)
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(SightseeingItinerary.GetSightseeingId(e.CommandArgument.ToString()), ProductType.SightSeeing));
                    SightseeingItinerary itinerary = new SightseeingItinerary();
                    AgentMaster agent = new AgentMaster(bookingDetail.AgencyId);
                    Product[] products = BookingDetail.GetProductsLine(bookingDetail.BookingId);
                    decimal bookingAmt = 0;
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.SightSeeing)
                        {
                            itinerary.Load(products[i].ProductId);
                            break;
                        }
                    }

                    decimal discount = 0;
                    
                        if (itinerary.Price.NetFare > 0)
                        {
                            bookingAmt += (itinerary.Price.NetFare + itinerary.Price.Markup+itinerary.Price.B2CMarkup); //added b2cMarkup only 
                        }

                        discount += itinerary.Price.Discount;
                    

                    bookingAmt -= discount;
                    //Use ceiling instead of Round, Changed on 05082016
                    bookingAmt = Math.Ceiling(bookingAmt);
                    Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                    if (itinerary.Source == SightseeingBookingSource.GTA)
                    {
                        CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                        cancellationData = gtaApi.CancelSightseeingBooking(itinerary);
                    }
                    else if (itinerary.Source == SightseeingBookingSource.CZA)
                    {
                        cancellationData.Add("Status", "Cancelled");
                        cancellationData.Add("Currency", itinerary.Currency);
                        cancellationData.Add("Amount", "0");
                    }
                    if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") 
                    {
                        lblError.Text = "";
                        itinerary.BookingStatus = SightseeingBookingStatus.Cancelled;
                        if (itinerary.Source == SightseeingBookingSource.GTA)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == SightseeingBookingSource.CZA)
                        {
                            itinerary.CancelId = "X-"+ itinerary.ConfirmationNo;  // X means Cancel
                        }
                        itinerary.UpdateBookingStatus();

                        serviceRequest = Session["ServiceRequests"] as ServiceRequest[];

                        loggedMemberId = (int)Settings.LoginInfo.UserID;
                        ServiceRequest sr = new ServiceRequest();
                        CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                        cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                        cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                        cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                        cancellationCharge.ReferenceId = itinerary.SightseeingId;
                        decimal exchangeRate = 0;
                        if (cancellationData["Currency"] != agent.AgentCurrency)
                        {
                            StaticData staticInfo = new StaticData();
                            staticInfo.BaseCurrency = agent.AgentCurrency;

                            Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                            exchangeRate = rateOfExList[cancellationData["Currency"]];
                            cancellationCharge.CancelPenalty = (Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate);
                        }
                        else
                        {
                            cancellationCharge.CancelPenalty = Convert.ToDecimal(cancellationData["Amount"]);
                        }
                        cancellationCharge.CreatedBy = loggedMemberId;
                        cancellationCharge.ProductType = ProductType.SightSeeing;
                        cancellationCharge.Save();

                        if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0)
                        {
                            try
                            {
                               
                                //decimal exchangeRate = 0;
                                if (cancellationData["Currency"] != agent.AgentCurrency)
                                {
                                    StaticData staticInfo = new StaticData();
                                    staticInfo.BaseCurrency = agent.AgentCurrency;
                                    Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                    exchangeRate = rateOfExList[cancellationData["Currency"]];
                                }
                                if (exchangeRate <= 0)
                                {
                                    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]);
                                }
                                else
                                {
                                    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate;
                                }
                                
                            }
                            catch { }
                        }
                    
                        // Admin & Supplier Fee save in Leadger
                        int invoiceNumber = 0;
                        decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                        //decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                        decimal adminFee = 0;
                        if (adminChar < bookingAmt)
                        {
                            adminFee = adminChar;
                        }
                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                        NarrationBuilder objNarration = new NarrationBuilder();
                        invoiceNumber = Invoice.isInvoiceGenerated(itinerary.SightseeingId, ProductType.SightSeeing);
                        //ledgerTxn = new LedgerTransaction();
                        objNarration.PaxName = itinerary.PaxNames[0].ToString();
                        objNarration.DocNo = invoiceNumber.ToString(); 
                        objNarration.TourConfirmationNo = itinerary.ConfirmationNo;
                        objNarration.TravelDate = itinerary.TourDate.ToShortDateString();
                        objNarration.Remarks = "Sightseeing Cancellation Charges";
                        //Ledger
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = -adminFee;
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.SightseeingId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.SightseeingCancellationCharge; 
                        ledgerTxn.Notes = "Confirmation NO :" + itinerary.ConfirmationNo; ;
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                        //save Refund amount
                        ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = bookingAmt;
                        objNarration.PaxName = itinerary.PaxNames[0].ToString(); ;
                        objNarration.Remarks = "Refunded for Voucher No -" + itinerary.ConfirmationNo; ;
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.SightseeingId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.SightSeeingRefund;
                        ledgerTxn.Notes = "Sightseeing Voucher Refunded";
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);


                        if (adminFee <= bookingAmt)
                        {
                            bookingAmt -= adminFee;
                        }
                       
                        if (itinerary.TransType != "B2C")
                        {
                            //Settings.LoginInfo.AgentBalance += bookingAmt;
                            agent.CreatedBy = Settings.LoginInfo.UserID;
                            agent.UpdateBalance(bookingAmt);

                            if (bookingDetail.AgencyId == Settings.LoginInfo.AgentId)
                            {
                                Settings.LoginInfo.AgentBalance += bookingAmt;
                            }
                        }


                        //Update Queue Status
                        CT.Core.Queue.SetStatus(QueueType.Request, serviceRequest[e.Item.ItemIndex].RequestId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                        sr.UpdateServiceRequestAssignment(serviceRequest[e.Item.ItemIndex].RequestId, (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);
                        //Update Stock in act_activity_master table.
                        noOfGuests = itinerary.AdultCount + itinerary.ChildCount;
                        //itinerary.UpdateStockintable(Convert.ToInt32(itinerary.ItemCode), noOfGuests);
                        //Sending Email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agent.Name);
                        table.Add("itemName", itinerary.ItemName);
                        table.Add("confirmationNo", itinerary.ConfirmationNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgentId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        try
                        {
                            string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["SIGHTSEEING_CANCEL_MAIL"]).Split(';');
                            foreach (string cnMail in cancelMails)
                            {
                                toArray.Add(cnMail);
                            }

                            string message = ConfigurationManager.AppSettings["SIGHTSEEING_REFUND"];
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Sightseeing)Response for cancellation. Confirmation No:(" + itinerary.ConfirmationNo + ")", message, table);
                        }
                        catch { }
                    }
                    else
                    {
                        lblMessage.Text = "Failed Cancellation from " + itinerary.Source.ToString();
                    }
                    LoadChangeQueues();
                }
                else
                {
                    lblError.Text = "Please enter Admin & Supplier Fee";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, "0");
        }
    }
    protected void dlChangeRequests_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                SightseeingItinerary itinerary = e.Item.DataItem as SightseeingItinerary;

                if (itinerary != null)
                {
                    Label lblSupplier = e.Item.FindControl("lblSupplier") as Label;
                    Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                    Label lblGuests = e.Item.FindControl("lblGuests") as Label;
                    Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                    Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                    TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    txtAdminFee.Attributes.Add("onblur", "Validate(" + e.Item.ItemIndex + ")");
                    Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                    Label lblAgent = e.Item.FindControl("lblAgent") as Label;
                    btnRefund.OnClientClick = "return Validate('" + e.Item.ItemIndex + "');";

                    Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                    Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                    Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                    Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                    Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;
                    Label lblCreatedBy = e.Item.FindControl("lblCreatedBy") as Label;
                    Label lblAdminCurrency = e.Item.FindControl("lblAdminCurrency") as Label;
                    Label lblSupplierCurrency = e.Item.FindControl("lblSupplierCurrency") as Label;

                    int adults = 0, childs = 0;
                    decimal totalPrice = 0;
                    if (itinerary != null)
                    {
                        adults += itinerary.AdultCount;
                        childs += itinerary.ChildCount;
                        if (itinerary.Price.AccPriceType == PriceType.PublishedFare)
                        {
                            totalPrice = itinerary.Price.PublishedFare + itinerary.Price.Tax - itinerary.Price.Discount;
                        }
                        else
                        {
                            totalPrice = itinerary.Price.NetFare + itinerary.Price.Tax + itinerary.Price.Markup + itinerary.Price.B2CMarkup - itinerary.Price.Discount; //Add B2c Markup only on 13062016 by chandan
                        }

                        //Bind the Sightseeing Booking price
                        lblPrice.Text = (itinerary.Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Price.Currency) : "AED ") + " " + (Math.Ceiling(totalPrice) + Math.Ceiling(itinerary.Price.OutputVATAmount)).ToString("N" + itinerary.Price.DecimalPoint); //Use ceiling instead of Round, Changed on 05082016
                        lblAdminCurrency.Text = itinerary.Price.Currency;
                        lblSupplierCurrency.Text = itinerary.Price.Currency;

                        lblPaxName.Text = itinerary.PaxNames[0];

                    }
                    lblGuests.Text = adults + childs + " Adults (" + adults + ") Childs (" + childs + ")";

                    lblSupplier.Text = (itinerary.Source == null ? "" : itinerary.Source.ToString());
                    lblStatus.Text = itinerary.BookingStatus.ToString();
                    try
                    {
                        //---------------------------Retrieve Agent-------------------------------------------
                        int bkgId = BookingDetail.GetBookingIdByProductId(itinerary.ProductId, itinerary.ProductType);
                        BookingDetail bkgDetail = new BookingDetail(bkgId);
                        AgentMaster agent = new AgentMaster(bkgDetail.AgencyId);
                        lblAgent.Text = agent.Name;
                        lblAgencyBalance.Text = agent.CurrentBalance.ToString("0.000");
                        lblAgencyEmail.Text = agent.Email1;
                        lblAgencyName.Text = agent.Name;
                        lblAgencyPhone1.Text = agent.Phone1;
                        lblAgencyPhone2.Text = agent.Phone2;
                        //------------------------------------------------------------------------------------
                    }
                    catch { }
                    //if (DateTime.Now.CompareTo(itinerary.LastCancellationDate) > 0)
                    if (DateTime.Now.CompareTo(itinerary.TourDate) > 0)
                    {

                        txtAdminFee.Visible = false;
                        
                        btnRefund.Visible = false;
                    }
                    else
                    {
                        
                        if (itinerary.TourDate.Subtract(DateTime.Now).Days >= 0 && itinerary.BookingStatus != SightseeingBookingStatus.Cancelled)
                        {
                            txtAdminFee.Visible = true;
                            btnRefund.Visible = true;
                        }
                        else
                        {
                            txtAdminFee.Visible = false;
                            btnRefund.Visible = false;
                        }
                    }

                    if (lblPaxName.Text == "")
                    {
                        e.Item.Visible = false;
                    }
                    try
                    {
                        UserMaster createdBy = new UserMaster(Convert.ToInt64(itinerary.CreatedBy));
                        lblCreatedBy.Text = createdBy.FirstName + " " + createdBy.LastName;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtLocations = LocationMaster.GetList(Utility.ToInteger(ddlAgents.SelectedItem.Value), ListStatus.Short, RecordStatus.Activated, string.Empty);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
        hdfParam.Value = "0";
    }
}
