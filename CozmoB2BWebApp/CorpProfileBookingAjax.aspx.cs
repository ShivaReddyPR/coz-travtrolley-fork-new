﻿using System;
using System.Data;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using System.Collections.Generic;
using System.Linq;

public partial class CorpProfileBookingAjax : System.Web.UI.Page
{
    protected DataTable dtBreakingRules;
    protected List<FlightPolicy> FlightPolicies = new List<FlightPolicy>();
    protected SearchRequest request;
    protected SearchResult resultObj;
    protected bool ShowError, ShowPopup;
    protected string errorMessage;
    protected DataTable dtTravelReason = new DataTable();
    protected HotelRequest htlrequest;
    protected HotelSearchResult htlresult;
    protected string roomTypeCode;
    protected string roomSupplierName=string.Empty;   
    int agencyId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Audit.Add(EventType.Exception, Severity.High, 1, "loading ajax: ", Request["REMOTE_ADDR"]);
            if (Request["id"] != null && Settings.LoginInfo != null && Session["FlightRequest"] != null && Session["sessionId"] != null)
            {
                string sessionId = Session["sessionId"].ToString();

                request = Session["FlightRequest"] as SearchRequest;

                int resultId = Convert.ToInt32(Request["id"]);
                resultObj = Basket.FlightBookingSession[sessionId].Result[resultId - 1];

                if (Settings.LoginInfo.IsOnBehalfOfAgent)                                  
                    agencyId = Settings.LoginInfo.OnBehalfAgentID;
                else                                   
                    agencyId = Settings.LoginInfo.AgentId;
               

                dtTravelReason = TravelUtility.GetTravelReasonList("B", agencyId, ListStatus.Short);


                if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
                {

                    //Employee is of VIP grade
                    dtBreakingRules = TravelUtility.GetPolicyBreakingRules((long)request.CorporateTravelProfileId, 1, "SECURITYRULES");

                    string flightNumbers = string.Empty;

                    for (int i = 0; i < resultObj.Flights.Length; i++)
                    {
                        for (int j = 0; j < resultObj.Flights[i].Length; j++)
                        {
                            if (flightNumbers.Length > 0)
                            {
                                flightNumbers += "-" + resultObj.Flights[i][j].Airline + resultObj.Flights[i][j].FlightNumber;
                            }
                            else
                            {
                                flightNumbers = resultObj.Flights[i][j].Airline + resultObj.Flights[i][j].FlightNumber;
                            }
                        }
                    }
                    //Audit.Add(EventType.Exception, Severity.High, 1, "flight number : "+flightNumbers, Request["REMOTE_ADDR"]);
                    try
                    {
                        FlightPolicies = FlightPolicy.GetFlightPolicies(request.CorporateTravelProfileId, flightNumbers, request.Segments[0].PreferredDepartureTime);
                    }
                    catch(Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "(CorpProfileAjax)Failed to retrieve flight policies for ProfileId: " + request.CorporateTravelProfileId + ". Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    //Audit.Add(EventType.Exception, Severity.High, 1, "flight policy count  : " + FlightPolicies.Count.ToString(), Request["REMOTE_ADDR"]);
                    if (FlightPolicies.Count > 0)//TODO: Ziya
                    {
                        //dtBreakingRules = TravelUtility.GetPolicyBreakingRules((long)request.CorporateTravelProfileId, 1, "SECURITYRULES");

                        //string flightNumbers = string.Empty;

                        //for (int i = 0; i < resultObj.Flights.Length; i++)
                        //{
                        //    for (int j = 0; j < resultObj.Flights[i].Length; j++)
                        //    {
                        //        if (flightNumbers.Length > 0)
                        //        {
                        //            flightNumbers += "-" + resultObj.Flights[i][j].Airline + resultObj.Flights[i][j].FlightNumber;
                        //        }
                        //        else
                        //        {
                        //            flightNumbers = resultObj.Flights[i][j].Airline + resultObj.Flights[i][j].FlightNumber;
                        //        }
                        //    }
                        //}

                        //FlightPolicies = FlightPolicy.GetFlightPolicies(request.CorporateTravelProfileId, flightNumbers, request.Segments[0].PreferredDepartureTime);

                        bool sameFlightCheck = false;

                        if (dtBreakingRules != null)
                        {
                            foreach (DataRow row in dtBreakingRules.Rows)
                            {
                                if (row["filterType"].ToString() == "SAMEFLIGHT")
                                {
                                    sameFlightCheck = true;
                                    break;
                                }
                            }
                        }
                        //Audit.Add(EventType.Exception, Severity.High, 1, "flight policy smae flight  : " + sameFlightCheck.ToString(), Request["REMOTE_ADDR"]);
                        //if (!resultObj.TravelPolicyResult.IsUnderPolicy)//Outside policy
                        {
                            if (sameFlightCheck)
                            {
                                //Anyone travelled same flight show security message and stop from booking
                                if (FlightPolicies.Count > 0)
                                {
                                    //Show Error Message                            
                                    ShowError = true;
                                    ShowPopup = false;
                                    errorMessage = "You cannot book Same Flight " + flightNumbers + " on Same Date : " + request.Segments[0].PreferredDepartureTime + " because it is already booked by another employee from the same department";
                                    //Add breaking rule to the result

                                    if (resultObj.TravelPolicyResult.PolicyBreakingRules == null)
                                    {
                                        resultObj.TravelPolicyResult.PolicyBreakingRules = new Dictionary<string, List<string>>();
                                    }
                                    if (resultObj.TravelPolicyResult.PolicyBreakingRules.ContainsKey("SECURITYRULES"))
                                    {
                                        resultObj.TravelPolicyResult.PolicyBreakingRules["SECURITYRULES"] = new List<string>() { errorMessage };
                                    }
                                    else
                                    {
                                        resultObj.TravelPolicyResult.PolicyBreakingRules.Add("SECURITYRULES", new List<string>() { errorMessage });
                                    }
                                }
                                else
                                {
                                    //No one travelled same flight
                                    //Directly go passenger page
                                    if (!resultObj.TravelPolicyResult.IsUnderPolicy)//Outside policy
                                    {
                                        ShowError = false;
                                        ShowPopup = true;
                                    }
                                    else//Inside the policy, directly go to PassengerDetails.aspx
                                    {
                                        ShowError = false;
                                        ShowPopup = false;
                                    }
                                }
                            }
                            else //If not same flight also show popup
                            {
                                //Show Popup
                                if (!resultObj.TravelPolicyResult.IsUnderPolicy)//Outside policy
                                {
                                    ShowError = false;
                                    ShowPopup = true;
                                }
                                else//Inside the policy, directly go to PassengerDetails.aspx
                                {
                                    ShowError = false;
                                    ShowPopup = false;
                                }
                            }
                        }
                    }
                }
                //else//If Grade is not VIP then show popup
                //if(true)//If Grade is not VIP then show popup
                {
                    if (!resultObj.TravelPolicyResult.IsUnderPolicy)//Outside policy
                    {
                        ShowError = false;
                        ShowPopup = true;
                    }
                    else//Inside the policy, directly go to PassengerDetails.aspx
                    {
                        ShowError = false;
                        ShowPopup = false;
                    }
                }


            }
            else if (Request["id"] != null && Settings.LoginInfo != null && Session["req"] != null || Session["ApiHotelResult"] != null)
            {
                htlrequest = Session["req"] as HotelRequest;
                htlresult = Session["ApiHotelResult"] as HotelSearchResult;
                roomTypeCode = Request["id"];
                HotelRoomsDetails[] RoomDetails = new HotelRoomsDetails[0];
                RoomDetails = htlresult.RoomDetails.Where(r => r.RoomTypeCode == roomTypeCode).ToArray();
                if (Settings.LoginInfo.IsCorporate == "Y" && !string.IsNullOrEmpty(htlrequest.Corptravelreason) && !string.IsNullOrEmpty(htlrequest.Corptraveler))
                {
                    roomSupplierName = RoomDetails[0].SupplierName;
                    dtTravelReason = TravelUtility.GetTravelReasonList("B", Settings.LoginInfo.AgentId, ListStatus.Short, 2);
                    if (!RoomDetails[0].TravelPolicyResult.IsUnderPolicy)//Outside policy
                    {
                        ShowError = false;
                        ShowPopup = true;
                    }
                    else//Inside the policy, directly go to PassengerDetails.aspx
                    {
                        ShowError = false;
                        ShowPopup = false;
                    }
                }

            }
            else
            {
                ShowError = true;
                ShowPopup = false;
                errorMessage = "Your login session has expired. Please Login and Search again.";
            }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to check Security rules for Corporate Profile. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
