﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine.Insurance;
using CT.Core;
using System.IO;
using CT.TicketReceipt.BusinessLayer;
using System.Linq;
public partial class InsuranceVoucher : CT.Core.ParentPage
{
    protected string imagePath = "";
    protected InsuranceHeader header;
    protected string logoPath = string.Empty;
    protected AgentMaster agent;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                imagePath += ConfigurationManager.AppSettings["RootFolder"];
                if (Request.QueryString["InsId"] != null)
                {
                    header = new InsuranceHeader();
                    header.RetrieveConfirmedPlan(Convert.ToInt32(Request.QueryString["InsId"]));
                    if (header.AgentId > 0)
                    {
                        agent = new AgentMaster(header.AgentId);
                        logoPath = Request.Url.Scheme+"://ctb2bstage.cozmotravel.com/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                        imgHeaderLogo.ImageUrl = logoPath;
                    }
                    BindVoucher(header);

                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher :Query string header id is empty ", Request["REMOTE_ADDR"]);
                    Response.Redirect("Insurance.aspx");
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void BindVoucher(InsuranceHeader header)
    {
        try
        {
            if (header.DepartureDateTime != header.ReturnDateTime)
            {
                lblSector.Text = header.DepartureStationCode + "-" + header.ArrivalStationCode + "-" + header.DepartureStationCode;
                lblJourneyDates.Text = Convert.ToDateTime(header.DepartureDateTime).ToString("dd-MM-yyyy") + " - " + Convert.ToDateTime(header.ReturnDateTime).ToString("dd-MM-yyyy");
            }
            else
            {
                lblSector.Text = header.DepartureStationCode + "-" + header.ArrivalStationCode;
                lblJourneyDates.Text = Convert.ToDateTime(header.DepartureDateTime).ToString("dd-MM-yyyy");
            }
            //if (header.InsPlans != null)
            //{
            //    for (int n = 0; n < header.InsPassenger.Count; n++)
            //    {
            //        HtmlGenericControl divPaxNameValue = new HtmlGenericControl();
            //        divPaxNameValue.Attributes.Add("class", "col-md-3 col-xs-6");
            //        Label lblPaxName = new Label();
            //        lblPaxName.Font.Bold = true;
            //        lblPaxName.Text = header.InsPassenger[n].FirstName + " " + header.InsPassenger[n].LastName;
            //        divPaxNameValue.Controls.Add(lblPaxName);
            //        HtmlGenericControl divClearPax = new HtmlGenericControl("Div");
            //        divClearPax.Attributes.Add("class", "clearfix");
            //        divPlan.Controls.Add(divPaxNameValue);
            //        divPlan.Controls.Add(divClearPax);
            //        for (int k = 0; k < header.InsPlans.Count; k++)
            //        {

            //            HtmlGenericControl divlblcertificationValue = new HtmlGenericControl("Div"); // For Adult City
            //            divlblcertificationValue.Attributes.Add("class", "col-md-3 col-xs-6");
            //            Label lblCertification = new Label();
            //            lblCertification.Text = header.InsPlans[k].PolicyNo;
            //            divlblcertificationValue.Controls.Add(lblCertification);

            //            HtmlGenericControl divlblPlanNameValue = new HtmlGenericControl("Div"); // For Adult City
            //            divlblPlanNameValue.Attributes.Add("class", "col-md-3 col-xs-6");
            //            Label lblPlanName = new Label();
            //            lblPlanName.Text = header.InsPlans[k].InsPlanCode;
            //            divlblPlanNameValue.Controls.Add(lblPlanName);


            //            //policyNo
            //            HtmlGenericControl divPolicyNo = new HtmlGenericControl("Div"); // For Adult City
            //            divPolicyNo.Attributes.Add("class", "col-md-3 col-xs-6");
            //            Label lblPolicyNo = new Label();
            //            if (header.InsPassenger[n].PolicyNo.Split('|').Length >= k)
            //            {

            //                lblPolicyNo.Text = header.InsPassenger[n].PolicyNo.Split('|')[k];

            //            }
            //            divPolicyNo.Controls.Add(lblPolicyNo);

            //            //certificate Link
            //            HtmlGenericControl divCertificate = new HtmlGenericControl("Div"); // For Adult City
            //            divCertificate.Attributes.Add("class", "col-md-3 col-xs-6");
            //            Label lblCerticate = new Label();
            //            if (header.InsPassenger[n].PolicyUrlLink.Split('|').Length >= k)
            //            {
            //                lblCerticate.Text = "<a target='_blank' href='" + header.InsPassenger[n].PolicyUrlLink.Split('|')[k] + "' >View Certificate</a>";
            //                divCertificate.Controls.Add(lblCerticate);
            //            }
            //            HtmlGenericControl divClear = new HtmlGenericControl("Div");
            //            divClear.Attributes.Add("class", "clearfix");
            //            divPlan.Controls.Add(divlblcertificationValue);
            //            divPlan.Controls.Add(divlblPlanNameValue);
            //            divPlan.Controls.Add(divPolicyNo);
            //            divPlan.Controls.Add(divCertificate);
            //            divPlan.Controls.Add(divClear);
            //        }

            //    }
            //}
            SendInsuranceVoucherMail();
            Session["Header"] = null;
            Session["SelectedPlans"] = null;
            Session["InsRequest"] = null;
            Session["InsPlansRespnose"] = null;
            Session["InsPassengers"] = null;
        }
        catch { throw; }
       

    }

    private void SendInsuranceVoucherMail()
    {
        try
        {
            //if (ViewState["MailSent"] == null)
            if (ViewState["MailSent"] == null)
            {
                string myPageHTML = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                printableArea.RenderControl(htw);
                myPageHTML = sw.ToString();

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                toArray.Add(header.InsPassenger[0].Email);
                string policyNo = string.Empty;
                for (int m = 0; m < header.InsPlans.Count; m++)
                {
                    if (!string.IsNullOrEmpty(policyNo))
                    {
                        policyNo = policyNo + "," + header.InsPlans[m].PolicyNo;
                    }
                    else
                    {
                        policyNo = header.InsPlans[m].PolicyNo;
                    }

                }
                AgentMaster agency = new AgentMaster(header.AgentId);
                string bccEmails = string.Empty;
                if (!string.IsNullOrEmpty(agency.Email1))
                {
                    bccEmails = agency.Email1;
                }
                if (!string.IsNullOrEmpty(agency.Email2))
                {
                    bccEmails = bccEmails + "," + agency.Email2;
                }
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Insurance Confirmation - " + policyNo, myPageHTML, new Hashtable(), bccEmails);
                }
                catch { }
                ViewState["MailSent"] = true;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
