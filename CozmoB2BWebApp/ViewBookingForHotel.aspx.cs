﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Script.Services;

namespace CozmoB2BWebApp
{
	public partial class ViewBookingForHotel : CT.Core.ParentPage// System.Web.UI.Page
    {
        protected BookingResponse bookingResponse;
        protected HotelItinerary itinerary;
        protected PriceType priceType;
        protected decimal rateOfExchange = 1;
        HotelPassenger hPax = null;
        HotelRoom hRoom;
        PriceAccounts priceInfo = new PriceAccounts();
        bool isMultiRoom;
        protected HotelDetails hotelDetails = new HotelDetails();
        protected int nights;
        protected AgentMaster agent;
        protected BookingDetail booking;
        protected LocationMaster location = new LocationMaster();
        protected AgentMaster parentAgent;
        public static List<Comment> CommentData;
        protected int bookingId = 0;
        protected string spantext = "Show Comments";
        List<BookingHistory> bkgHistory;
        
        protected void Page_Load(object sender, EventArgs e)
		{

            //if (Request.QueryString["ConfNo"] != null)
            //{

            //    int agentId = 0;

            //if (Settings.LoginInfo.IsOnBehalfOfAgent)
            //{
            //    agentId = Settings.LoginInfo.OnBehalfAgentID;
            //    agent = new AgentMaster(agentId);
            //}
            //else
            //{
            //    agentId = Settings.LoginInfo.AgentId;
            //    agent = new AgentMaster(agentId);
            //    location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);

            //}
           
            //}

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetHotelItineraryInfo(string HotelId)
        {           
            try
            {
                DataTable dt = CT.BookingEngine.HotelItinerary.GetHotelItineraryInfo(Convert.ToInt32(HotelId));
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                var Result = (from data in dt.AsEnumerable()
                              select new
                              {                                 
                                  hotelName = data.Field<string>("hotelName"),
                                  addressLine1 = data.Field<string>("addressLine1"),
                                  checkInDate = data.Field<DateTime>("checkInDate"),
                                  checkOutDate = data.Field<DateTime>("checkOutDate"),
                                  nights = ((data.Field<DateTime>("checkOutDate")) - (data.Field<DateTime>("checkInDate"))).TotalDays,
                                  confNo = data.Field<string>("confirmationNo"),
                                  agencyId = data.Field<int>("agencyId"),                                 
                                  noOfRooms = data.Field<int>("noOfRooms"),
                                  source = data.Field<int>("source"),
                                  hotelCancelPolicy = data.Field<string>("hotelCancelPolicy").Replace("^Date and time is calculated based on local time of destination|", "|"),
                                 // hotelPolicyDetails = data.Field<string>("hotelPolicyDetails").Contains("^") ? data.Field<string>("hotelPolicyDetails").Split('^')[0] : data.Field<string>("hotelPolicyDetails"),
                                  //Disclaimer = data.Field<string>("hotelPolicyDetails").Contains("^") ? data.Field<string>("hotelPolicyDetails").Split('^')[1].Split('|')[0] : ""
                                  hotelPolicyDetails = data.Field<string>("hotelPolicyDetails").Replace("br /", "").Replace("<>", "").Replace("<ul>", "").Replace("</p>", "").Replace("<p>", "").Replace("<li>", "").Replace("</ul>", "").Replace("<br />", "").Replace("\n", "").Replace("\r", "").Replace("</li>", "").Contains("^") ? data.Field<string>("hotelPolicyDetails").Replace("br /", "").Replace("<>", "").Replace("<ul>", "").Replace("</p>", "").Replace("<p>", "").Replace("<li>", "").Replace("</ul>", "").Replace("<br />", "").Replace("\n", "").Replace("\r", "").Replace("</li>", "").Split('^')[0] : data.Field<string>("hotelPolicyDetails").Replace("br /", "").Replace("<>", "").Replace("<ul>", "").Replace("</p>", "").Replace("<p>", "").Replace("<li>", "").Replace("</ul>", "").Replace("<br />", "").Replace("\n", "").Replace("\r", "").Replace("</li>", ""),
                                  Disclaimer = data.Field<string>("hotelPolicyDetails").Replace("br /", "").Replace("<>", "").Replace("<ul>", "").Replace("</p>", "").Replace("<p>", "").Replace("<li>", "").Replace("</ul>", "").Replace("<br />", "").Replace("\n", "").Replace("\r", "").Replace("</li>", "").Contains("^") ? data.Field<string>("hotelPolicyDetails").Replace("br /", "").Replace("<>", "").Replace("<ul>", "").Replace("</p>", "").Replace("<p>", "").Replace("<li>", "").Replace("</ul>", "").Replace("<br />", "").Replace("\n", "").Replace("\r", "").Replace("</li>", "").Split('^')[1].Split('|')[0] : "",
                                  status= data.Field<int>("status")
                              }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(Result);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetHotelItineraryPaxInfo(string HotelId)
        {
            try
            {
                DataTable dt = CT.BookingEngine.HotelPassenger.GetHotelItineraryPaxInfo(Convert.ToInt32(HotelId));
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                var Result = (from data in dt.AsEnumerable()
                              select new
                              {
                                  Name = data.Field<string>("title") + " " + (data.Field<string>("firstName")) + " " + (data.Field<string>("lastName")),                                
                                  email = data.Field<string>("email"),
                                  phone = data.Field<string>("phone"),                         
                                  nationality = data.Field<string>("nationality")

                              }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(Result);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetHotelAllPaxInfo(string HotelId)
        {
            try
            {
                DataTable dt = CT.BookingEngine.HotelPassenger.GetHotelAllPaxInfo(Convert.ToInt32(HotelId));
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                var Result = (from data in dt.AsEnumerable()
                              select new
                              {
                                  Name = data.Field<string>("title") + " " + (data.Field<string>("firstName")) + " " + (data.Field<string>("lastName")),
                                  email = data.Field<string>("email"),
                                  phone = data.Field<string>("phone"),
                                  nationality = data.Field<string>("nationality"),
                                  paxType = data.Field<byte>("paxType"),                                  
                                  Age = data.Field<int>("age"),
                                  leadPassenger = data.Field<bool>("leadPassenger"),
                                  roomId = data.Field<int>("roomId")

                              }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(Result);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetHotelItineraryRoomInfo(string HotelId)
        {           
            try
            {
                DataTable dt = CT.BookingEngine.HotelRoom.GetHotelItineraryRoomInfo(Convert.ToInt32(HotelId));
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();                
                var Result = (from data in dt.AsEnumerable()
                              select new
                              {
                                  roomName = data.Field<string>("roomName"),                                 
                                  noOfGuests = data.Field<int>("adultCount") + data.Field<int>("childCount"),                                
                                  adultCount = data.Field<int>("adultCount"),
                                  childCount = data.Field<int>("childCount"),                                 
                                  MealPlanDesc = data.Field<string>("MealPlanDesc"),
                                  netFare = data.Field<decimal>("netFare"),
                                  markup = data.Field<decimal>("markup"),
                                  INVAT_AMOUNT = data.Field<decimal>("INVAT_AMOUNT"),
                                  OUTVAT_AMOUNT = data.Field<decimal>("OUTVAT_AMOUNT"),
                              }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(Result);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetBookingHistory(int HotelId)
        {
             BookingDetail booking = new BookingDetail();
            booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelId, ProductType.Hotel));
            try
            {           
                DataTable dt = CT.BookingEngine.BookingHistory.GetBookingHistoryItinerary(Convert.ToInt32(booking.BookingId));
                System.Data.DataColumn newColumn = new System.Data.DataColumn("LastModifiedName", typeof(System.String));               
                dt.Columns.Add(newColumn);
                foreach (DataRow dr in dt.Rows)
                {
                    UserMaster user = new UserMaster(Convert.ToInt32(dr["LastModifiedBy"]));
                    dr["LastModifiedName"] = user.FirstName + " " + user.LastName;
                }
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                var Result = (from data in dt.AsEnumerable()
                              select new
                              {
                                  narration = data.Field<string>("remarks"),
                                  date = Convert.ToString(data.Field<DateTime>("createdOn").ToString("dd MMM yyyy")),
                                  Time = Convert.ToString(data.Field<DateTime>("createdOn").ToString("HH:mm tt")),
                                  LastModifiedName = data.Field<string>("LastModifiedName")                                 
                              }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(Result);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string AddComment(int HotelId, string Comments)
        {
            BookingDetail booking = new BookingDetail();
            booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelId, ProductType.Hotel));
            //try
            //{
            Comment comment = new Comment();
            comment.CommentDescription = Comments;
            comment.ParentId = Convert.ToInt32(booking.BookingId);
            comment.CommentTypes = CommmentType.Booking;
            comment.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            comment.LastModifiedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            if (Convert.ToInt32(Settings.LoginInfo.AgentId) != 0)
            {
                comment.IsPublic = true;
            }
            else
            {
                comment.IsPublic = false;
            }
            comment.ParentCommentId = 0;
            comment.AgencyId = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId);

            //Booking History
            BookingHistory bookingHistory = new BookingHistory();
            bookingHistory.BookingId = Convert.ToInt32(booking.BookingId);
            bookingHistory.EventCategory = EventCategory.Miscellaneous;
            bookingHistory.Remarks = "\'" + Comments + "\'";
            //bookingHistory.Remarks = "\'" + Comments + "\' Commment is added by " + Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName;
            bookingHistory.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            try
            {              
                comment.AddComment();
                bookingHistory.Save();
                //Load Comment
                CommentData = comment.GetComments(CommmentType.Booking, Convert.ToInt32(booking.BookingId), (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId));
                          }
            catch (ArgumentException excep)
            {
                throw excep;
            }
            string JSONString = string.Empty;           
            return JSONString;
        }
    }


}

    
