﻿using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class TransferPax : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            else
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            hdnUserId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.UserID);
            hdnAgentType.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentType);
        }
    }
}