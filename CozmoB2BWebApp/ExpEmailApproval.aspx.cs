﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ExpEmailApproval : System.Web.UI.Page
    {
        public string authError = string.Empty;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.QueryString["NU"] == null || string.IsNullOrEmpty(Request.QueryString["NU"]) || Request.QueryString["DIU"] == null) {

                authError = "Invalid User name.";
                return;
            }

            string sLoginName = GenericStatic.DecryptData(Request.QueryString["NU"]);
            string sPassword = UserMaster.GetPasswordByLoginName(sLoginName);
            if (string.IsNullOrEmpty(sPassword) || !UserMaster.IsAuthenticatedUser(sLoginName, sPassword, 1))
            {
                authError = "User authentication failed.";
                return;
            }

            Settings.LoginInfo = LoginInfo.GetLoginInfo(Convert.ToInt32(Request.QueryString["DIU"]));
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}