﻿using CT.BookingEngine;
using CT.CMS;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class FindHotelsDummy : ParentPage
    {
        protected int AdminId = 1; // 
        protected string displaySearchBox;
        protected RoomGuestData[] guestInfo = new RoomGuestData[0];
        //Make a Request Object
        protected HotelRequest reqObj = new HotelRequest();
        protected SearchRequest searchRequest = new SearchRequest();
        protected System.Collections.Generic.List<string> topDestination = new List<string>();
        //Variables for request;
        protected List<HotelDeal> allHotels = new List<HotelDeal>();
        protected string errorMessage = string.Empty;
        protected string originCity, tabIndex;
        protected string departureDate = "DD/MM/YYYY", returnDate = "DD/MM/YYYY";
        protected bool dateChangeSearch = false;
        protected string sessionIdValue = string.Empty;
        protected string calendarSource = string.Empty;
        protected string hotelEnable = "style='display:none'", activityEnable = "style='display:none'", flightEnable = "style='display:none'", flightsCountEnable = "class='hide'", hotelsCountEnable = "class='hide'", flightImg = "style='position: absolute; left: 90px'";
        protected string packageEnable = "style='display:none'", insEnable = "style='display:none'";
        protected string flightDiv = "class='hide'", hotelDiv = "class='hide'";
        protected string cityName = "Enter city name";
        protected string adults = "", childs = "", childAges = "";
        protected string routingSources = string.Empty;
        protected bool POIEnable = false;//for POI Enable or disable basedon Agent
        // protected ResourceManager resourceMgr;
        #region variables fro B2B2B agents setting
        protected bool isDomesticHotelSearchAllow;
        protected bool isInterNationalHotelSearchAllow;
        protected bool isMiscSearchAllow;
        protected int agencyId;
        protected string userType;
        DataTable dtHotelSources;
        protected string airlineMessage = string.Empty;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Master.PageRole = true;
            try
            {
                // radioAgent.InputAttributes.Add("class", "tgl tgl-light");
                rbtnAgent.InputAttributes.Add("class", "tgl tgl-light");

                //roundtrip.Checked = true;
                if (Settings.LoginInfo == null)
                {
                    Response.Redirect("AbandonSession.aspx");
                }

                //if (Request.QueryString["error"] != null)
                //{
                //    airlineMessage = Request.QueryString["error"];
                //}

                Page.Title = "Hotel Search";
                AuthorizationCheck();
                displaySearchBox = "none";


                /****************************************************************************************
                 *              Fill DOTW Country List once if the table is empty                       *                        
                 *              Changed according to the DOTW documentation version 2                   * 
                 * **************************************************************************************/
                Session["cSessionId"] = null;
                Session["CriteriaItineraries"] = null;//Clear Corporate profile Flight Itineraries            
                Session["layoverAirports"] = null;
                Session["fromcityAirports"] = null;
                Session["tocityAirports"] = null;
                Session["airlines"] = null;

                if (!IsPostBack)
                {
                    bindCountryList();

                    DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                    DataView dv = dtAgents.DefaultView;
                    dv.RowFilter = "agent_Id NOt IN('" + Settings.LoginInfo.AgentId + "')";
                    ddlAgents.AppendDataBoundItems = true;
                    ddlAgents.Items.Add("Select Client");
                    ddlAgents.DataSource = dv.ToTable();
                    ddlAgents.DataTextField = "agent_Name";
                    ddlAgents.DataValueField = "agent_Id";
                    ddlAgents.DataBind();

                    //ddlAirAgents.AppendDataBoundItems = true;
                    //ddlAirAgents.Items.Add(new ListItem("Select Client", "-1"));
                    //ddlAirAgents.DataSource = dv.ToTable();
                    //ddlAirAgents.DataTextField = "agent_name";
                    //ddlAirAgents.DataValueField = "agent_id";
                    //ddlAirAgents.DataBind();
                    if (Settings.LoginInfo.IsCorporate == "Y")
                    {
                        try
                        {
                            DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightTravelReasons.DataSource = dtTravelReasons;
                            ddlFlightTravelReasons.DataTextField = "Description";
                            ddlFlightTravelReasons.DataValueField = "ReasonId";
                            ddlFlightTravelReasons.DataBind();

                            DataTable dtProfiles = TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightEmployee.DataSource = dtProfiles;
                            ddlFlightEmployee.DataTextField = "ProfileName";
                            ddlFlightEmployee.DataValueField = "Profile";
                            ddlFlightEmployee.DataBind();
                            if (ddlFlightEmployee.Items.Count > 1)
                            {
                                ddlFlightEmployee.Items.Insert(0, "Select Traveller");
                            }

                            //Same thing apply for Hotel also TODO: Ziya

                            //dtTravelReasons = TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short);
                            //ddlHotelTravelReasons.DataSource = dtTravelReasons;
                            //ddlHotelTravelReasons.DataTextField = "Description";
                            //ddlHotelTravelReasons.DataValueField = "ReasonId";
                            //ddlHotelTravelReasons.DataBind();

                            //dtPolicies = TravelUtility.GetProfileList(6, Settings.LoginInfo.AgentId, ListStatus.Long);
                            //ddlHotelEmployee.DataSource = dtPolicies;
                            //ddlHotelEmployee.DataTextField = "Name";
                            //ddlHotelEmployee.DataValueField = "EmployeeId";
                            //ddlHotelEmployee.DataBind();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "Failed to Bind Corporate details for Flight Search. Reason :" + ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                    }

                    try
                    {
                        if ((Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack))
                        {
                            if (Page.PreviousPage != null && Page.PreviousPage.Title == "Hotel Search Results")
                            {
                                reqObj = Session["req"] as HotelRequest;

                                cityName = reqObj.CityName + "," + reqObj.CountryName;

                                CheckIn.Text = reqObj.StartDate.ToString("dd/MM/yyyy");
                                CheckOut.Text = reqObj.EndDate.ToString("dd/MM/yyyy");
                                txtHotelName.Text = reqObj.HotelName;
                                //  hdnAdults.Value = PreviousPage.adults;
                                //  hdnChilds.Value = PreviousPage.childs;
                                //  hdnChildAges.Value = PreviousPage.childAges;
                                hdnRating.Value = ((int)reqObj.Rating).ToString();



                                for (int i = 0; i < reqObj.NoOfRooms; i++)
                                {
                                    if (adults.Length > 0)
                                    {
                                        adults += "," + (i + 1) + "-" + reqObj.RoomGuest[i].noOfAdults;
                                    }
                                    else
                                    {
                                        adults = (i + 1) + "-" + reqObj.RoomGuest[i].noOfAdults;
                                    }
                                    if (childs.Length > 0)
                                    {
                                        childs += "," + (i + 1) + "-" + reqObj.RoomGuest[i].noOfChild;
                                    }
                                    else
                                    {
                                        childs = (i + 1) + "-" + reqObj.RoomGuest[i].noOfChild;
                                    }
                                    if (reqObj.RoomGuest[i].childAge != null)
                                    {
                                        foreach (int age in reqObj.RoomGuest[i].childAge)
                                        {
                                            if (childAges.Length > 0)
                                            {
                                                childAges += "," + age.ToString();
                                            }
                                            else
                                            {
                                                childAges = age.ToString();
                                            }
                                        }
                                    }
                                }
                                //if (reqObj.PassengerCountryOfResidence != null && reqObj.PassengerCountryOfResidence.Length > 0)
                                //{
                                //    try
                                //    {
                                //        ddlResidence.SelectedIndex = -1;
                                //        ddlResidence.Items.FindByValue(reqObj.PassengerCountryOfResidence).Selected = true;
                                //    }
                                //    catch { ddlResidence.SelectedIndex = 0; }
                                //}

                                if (reqObj.PassengerNationality != null && reqObj.PassengerNationality.Length > 0)
                                {
                                    try
                                    {
                                        ddlNationality.SelectedIndex = -1;
                                        ddlNationality.Items.FindByValue(reqObj.PassengerNationality).Selected = true;
                                    }
                                    catch { ddlNationality.SelectedIndex = 0; }
                                }

                                try
                                {
                                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                    {
                                        rbtnAgent.Checked = true;
                                        ddlAgents.SelectedValue = Settings.LoginInfo.OnBehalfAgentID.ToString();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Failed to Set On Behalf Agent", ex);
                                }
                            }
                            else if ((Page.PreviousPage != null && Page.PreviousPage.Title == "Flight Search Results"))
                            {
                                //BindFlightRequest();//do not load previous values while loading this page from results page                           
                            }
                            else
                            {
                                Settings.LoginInfo.IsOnBehalfOfAgent = false;
                                Settings.LoginInfo.OnBehalfAgentLocation = 0;
                            }

                        }
                        else
                        {
                            Settings.LoginInfo.IsOnBehalfOfAgent = false;
                            Settings.LoginInfo.OnBehalfAgentLocation = 0;
                            //BindFlightRequest();//do not load previous values while loading this page from menu
                        }

                        //When preferred airlines are restricted bind the flight request and show the restriction message
                        //if (Request.QueryString["R"] != null)
                        //{
                        //    BindFlightRequest();

                        //    if (Session["Error"] != null)
                        //    {
                        //        lblMessage.Text = Session["Error"].ToString();
                        //        Session["Error"] = null;
                        //    }
                        //}
                    }
                    catch (Exception ex) { throw new Exception(ex.Message, ex); }
                    //Clear Session
                    Session["hItinerary"] = null;
                    Session["BookingResponse"] = null;
                    Session["SearchResults"] = null;
                    Session["cSessionId"] = null;
                    Session["BookingAgencyID"] = null;
                    Session["Result"] = null;
                    Session["hCode"] = null;
                    Session["rCode"] = null;
                    Session["UserBookings"] = null;
                    Session["BookingMade"] = null;
                    Session["Warning"] = null;
                    Session["MailSent"] = null;
                    Session["TimesChanged"] = null;//Clear Repricing data   
                    Session["FlightRequest"] = null;
                    //-----------------------------------Active sources for Admin Begin------------------------------//


                    int id = 0;
                    HtmlTableRow row = new HtmlTableRow();

                    HtmlTableCell cell1 = new HtmlTableCell();
                    CheckBox chkSource1 = new CheckBox();
                    chkSource1.ID = "chkSource" + id.ToString();
                    chkSource1.Text = "All";
                    chkSource1.Checked = true;
                    chkSource1.Attributes.Add("onclick", "selectHotelSources()");
                    cell1.Controls.Add(chkSource1);

                    row.Cells.Add(cell1);
                    id++;
                    dtHotelSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 2,true);
                    ViewState["dtHotelSources"] = dtHotelSources;
                    foreach (DataRow dr in dtHotelSources.Rows)
                    {
                        HtmlTableCell cell = new HtmlTableCell();
                        CheckBox chkSource = new CheckBox();
                        chkSource.ID = "chkSource" + id.ToString();
                        chkSource.Text = Convert.ToString(dr["Name"]);
                        HiddenField hiddenField = new HiddenField();
                        hiddenField.ID = "hdfSupplierId" + id.ToString();
                        hiddenField.Value= Convert.ToString(dr["gimmonixSuppId"]);
                        if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack && Page.PreviousPage.Title == "Hotel Search Results")
                        {
                            foreach (string source in reqObj.Sources)
                            {
                                if (dr["Name"].ToString().Equals(source))
                                {
                                    chkSource.Checked = true;
                                }
                            }
                            if (dtHotelSources.Rows.Count > reqObj.Sources.Count)
                            {
                                chkSource1.Checked = false;
                            }
                        }
                        else
                        {
                            chkSource.Checked = true;
                        }

                        chkSource.Attributes.Add("onclick", "setCheck('ctl00_cphTransaction_" + chkSource.ID + "')");
                        cell.Controls.Add(chkSource);
                        cell.Controls.Add(hiddenField);
                        if (hdnSelectedSources.Value.Length > 0)
                        {
                            hdnSelectedSources.Value += "," + dr["Name"];
                        }
                        else
                        {
                            hdnSelectedSources.Value = dr["Name"].ToString();
                        }
                        row.Cells.Add(cell);
                        id++;
                    }


                    hdnActiveSources.Value = id.ToString();

                    tblSources.Rows.Add(row);
                    //-------------------------------Active Sources for Admin END----------------------------------------------//
                    //-------------------------------POI Enable based on Agents Start----------------------------------------------//
                    AgentAppConfig appConfig = new AgentAppConfig();
                    appConfig.AgentID = Settings.LoginInfo.AgentId;
                    List<AgentAppConfig> AppConfigList  = appConfig.GetConfigData();
                    if(AppConfigList!=null && AppConfigList.Count() > 0)
                    {
                        var Count = AppConfigList.Where(x => x.ProductID == 2 && x.Source == "GIMMONIX" && x.AppKey== "POI" && x.AppValue.ToUpper()=="TRUE").Count();
                        if (Count>0)
                        {
                            POIEnable = true;
                        }
                    }
                    //-------------------------------POI Enable based on Agents END----------------------------------------------//
                    //bindCountryList();

                    string agent_products = Settings.LoginInfo.AgentProduct;



                    string[] products = agent_products.Split(',');
                    bool isHotelEnable = true;
                    bool isFlightEnable = false;
                    //if (Request.QueryString["Source"] != null && Request.QueryString["Source"] != string.Empty)
                    //{
                    //    if (Request.QueryString["Source"] == "Hotel")
                    //    {
                    //isHotelEnable = true;
                    //}
                    //else if (Request.QueryString["Source"] == "Flight")
                    //{
                    //    isFlightEnable = true;
                    //}
                    //}
                    foreach (string prod in products)
                    {
                        if (prod == "2")// HOTEL
                        {

                            if (isHotelEnable)
                            {
                                hotelEnable = "class='current' style='display:block'";
                                hotelDiv = "";
                                //current = true;
                            }
                            else
                            {
                                hotelEnable = "style='display:block'";


                            }
                            hotelsCountEnable = "class='current'";


                        }
                        if (prod == "1") // FLIGHT
                        {
                            if (isFlightEnable)
                            {
                                flightEnable = "class='current' style='display:block'";
                                flightDiv = "";
                                //current = true;
                            }
                            else
                            {
                                flightEnable = " style='display:block'";
                                flightImg = "style = 'position: absolute; left: 0px'";

                            }
                            flightsCountEnable = "class='current'";
                        }
                        if (prod == "3") // Packages
                        {
                            packageEnable = " style='display:block;padding-left:5px'";
                        }
                        if (prod == "4") // ACtivity
                        {
                            activityEnable = " style='display:block;padding-left:5px'";
                        }
                        if (prod == "5") // Insurance
                        {
                            insEnable = " style='display:block;padding-left:5px'";
                        }
                    }

                    agencyId = Settings.LoginInfo.AgentId;
                    userType = Settings.LoginInfo.MemberType.ToString();
                    //DataSet ds = new DataSet();//BookingDetail.GetBookingCount(agencyId);
                    long userId = 0;
                    if (Settings.LoginInfo.MemberType == MemberType.OPERATIONS)
                    {
                        userId = Settings.LoginInfo.UserID;
                    }
                    DataSet ds = BookingDetail.GetBookingCount(agencyId, userId);
                    lblFlightsBookingCountToday.Text = "(0)";// Default SHowing 0
                    lblHotelsBookingCountToday.Text = "(0)";// Default SHowing 0
                    if (ds.Tables.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[0].Rows[i]["productTypeId"].ToString())) == ProductType.Hotel.ToString())
                            {
                                lblHotelsBookingCountToday.Text = "(" + (ds.Tables[0].Rows[i]["bookingcount"]).ToString() + ")";
                                lblHotelsBookingCountToday.NavigateUrl += DateTime.Now.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHotelsBookingCountToday.Text = "(0)";
                            }
                            if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[0].Rows[i]["productTypeId"].ToString())) == ProductType.Flight.ToString())
                            {
                                lblFlightsBookingCountToday.Text = "(" + (ds.Tables[0].Rows[i]["bookingcount"]).ToString() + ")";
                                lblFlightsBookingCountToday.NavigateUrl += DateTime.Now.ToString("dd/MM/yyyy");
                            }
                            else if (lblFlightsBookingCountToday.Text.Length == 0)
                            {
                                lblFlightsBookingCountToday.Text = "(0)";
                            }
                        }
                    }



                    if (ds.Tables.Count > 1)
                    {
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[1].Rows[i]["productTypeId"].ToString())) == ProductType.Hotel.ToString())
                            {
                                lblHotelsBkgWeeklyCount.Text = "(" + (ds.Tables[1].Rows[i]["bookingcount"]).ToString() + ")";
                                lblHotelsBkgWeeklyCount.NavigateUrl += DateTime.Now.AddDays(-7).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHotelsBkgWeeklyCount.Text = "(0)";
                            }
                            if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[1].Rows[i]["productTypeId"].ToString())) == ProductType.Flight.ToString())
                            {
                                lblFlightsBkgWeeklyCount.Text = "(" + (ds.Tables[1].Rows[i]["bookingcount"]).ToString() + ")";
                                lblFlightsBkgWeeklyCount.NavigateUrl += DateTime.Now.AddDays(-7).ToString("dd/MM/yyyy");
                            }
                            else if (lblFlightsBkgWeeklyCount.Text.Length == 0)
                            {
                                lblFlightsBkgWeeklyCount.Text = "(0)";
                            }


                        }
                    }



                    if (ds.Tables.Count > 2)
                    {
                        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                        {
                            if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[2].Rows[i]["productTypeId"].ToString())) == ProductType.Hotel.ToString())
                            {
                                lblHotelsBkgMonthlyCount.Text = "(" + (ds.Tables[2].Rows[i]["bookingcount"]).ToString() + ")";
                                lblHotelsBkgMonthlyCount.NavigateUrl += DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHotelsBkgMonthlyCount.Text = "(0)";
                            }
                            if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[2].Rows[i]["productTypeId"].ToString())) == ProductType.Flight.ToString())
                            {
                                lblFlightsBkgMonthlyCount.Text = "(" + (ds.Tables[2].Rows[i]["bookingcount"]).ToString() + ")";
                                lblFlightsBkgMonthlyCount.NavigateUrl += DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
                            }
                            else if (lblFlightsBkgMonthlyCount.Text.Length == 0)
                            {
                                lblFlightsBkgMonthlyCount.Text = "(0)";
                            }

                        }
                    }



                    BindMessages();
                    //  BindSuppliers();
                    //if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
                    //{
                    //    lblSearchSupp.Style.Add("display", "none");
                    //    chkSuppliers.Style.Add("display", "none");
                    //    chkFlightAll.Style.Add("display", "none");
                    //}
                }


                if (Request["calendarSearch"] != null && Request["calendarSearch"] == "CalendarSearch")
                {
                    searchRequest = (SearchRequest)Session["request"];
                    calendarSource = Request["calendarSearch"];
                }
                // resourceMgr = ResourceManager.CreateFileBasedResourceManager("Technology.Hindi",Server.MapPath("App_GlobalResources"), null);
                #region variables fro B2B2B agents setting
                if (!Convert.ToBoolean(Session["isB2B2BAgent"]))
                {
                    isDomesticHotelSearchAllow = true;
                    isInterNationalHotelSearchAllow = true;
                    isMiscSearchAllow = true;
                }
                else
                {
                    if (Session["isDomesticHotelAllow"].ToString() == "True")
                    {
                        isDomesticHotelSearchAllow = true;
                    }
                    else
                    {
                        isDomesticHotelSearchAllow = false;
                    }
                    if (Session["isInterNationalHotelAllow"].ToString() == "True")
                    {
                        isInterNationalHotelSearchAllow = true;
                    }
                    else
                    {
                        isInterNationalHotelSearchAllow = false;
                    }
                    if (Session["isMiscSearchAllow"].ToString() == "True")
                    {
                        isMiscSearchAllow = true;
                    }
                    else
                    {
                        isMiscSearchAllow = false;
                    }
                }
                #endregion
                //try
                //{
                //    //HotelDeal HotelData = new HotelDeal();
                //    //allHotels = HotelData.GetAllFeaturedHotelDeals();
                //}
                //catch (Exception excep)
                //{
                //    // errorMessage = "Unable to load Hotel Search Page. Contact our Administrator";
                //    Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Session["agencyId"]), "Exception Loading Hotel Search Page. Message: " + excep.Message, "");
                //}

                //If the user clicks on Search button.
                if (hdnSubmit.Value == "search")
                {
                    SearchHotels();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Search Hotel Details. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        void SearchHotels()
        {
            // Reading Corporate Traveller Grade
            if (Settings.LoginInfo.IsCorporate == "Y")
            {
                string[] profId = ddlFlightEmployee.SelectedValue.Split('-');
                if (profId.Length > 0)
                {
                    Settings.LoginInfo.CorporateProfileGrade = profId[1];
                }
            }
                    string type = string.Empty;
            if (hdnSubmit.Value.Length > 0)
            {
                if (hdnSubmit.Value == "search")
                {
                    Session["error"] = null;
                    //WebClient Client = new WebClient();
                    //Client.DownloadFile("<%=Request.Url.Scheme%>://209.85.13.194/DesiyaImages/Image/nxd/maw/uyf/lbp/HO.jpg", "c:\\kishorebabuDesiya.jpg");

                    Session["memberId"] = 1;
                    //gets the city details.
                    string cityDetail = string.Empty;
                    if (Request["dest_type"] == "domestic")
                    {
                        reqObj.IsDomestic = true;
                        type = "Dom";
                    }
                    else
                    {
                        reqObj.IsDomestic = false;
                        type = "Intl";
                        //if (Request["isMultiIntl"] == "true")
                        //{
                        //    reqObj.IsMultiRoom = true;
                        //}
                        //else
                        //{
                        //    reqObj.IsMultiRoom = false;
                        //}
                    }

                    if (Request["searchByArea"] == "on")
                    {
                        reqObj.SearchByArea = true;
                    }
                    else
                    {
                        reqObj.SearchByArea = false;
                    }
                    HotelSource sourceInfo = new HotelSource();
                    List<string> sourceList = new List<string>();
                    List<string> sources = new List<string>();

                    if (reqObj.IsDomestic)
                    {
                        sourceList = sourceInfo.Load(HotelSourceGeographyType.Domestic);

                        if (Request["destination"] != null && Request["destination"].Length > 0)
                        {
                            cityDetail = Request["destination"].ToUpper();
                            cityDetail = cityDetail.Trim();
                            string[] cityArray = cityDetail.Split(',');

                            reqObj.CityName = cityArray[0];
                            reqObj.CityId = HotelCity.GetCityIdFromCityName(reqObj.CityName);
                        }

                        reqObj.CountryName = "India";
                        Session["city"] = cityDetail;
                    }
                    else
                    {
                        sourceList = sourceInfo.Load(HotelSourceGeographyType.International);
                        if (Convert.ToDouble(hdnLongtitude.Value) != 0 && Convert.ToDouble(hdnlatitude.Value) != 0)
                        {
                            reqObj.Longtitude = Convert.ToDouble(hdnLongtitude.Value);
                            reqObj.Latitude = Convert.ToDouble(hdnlatitude.Value);
                            reqObj.Radius = Convert.ToInt32(hdnRadius.Value);
                            reqObj.CountryName = Convert.ToString(hdnPOICityName.Value);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Request.Form["cityCode"]))
                            {
                                //reqObj.CityId = HotelCity.GetCityIdFromCityName(Request.Form["City"].Split(',')[0], Request.Form["City"].Split(',')[1].Trim());
                                reqObj.CityId = Convert.ToInt32(Request.Form["cityCode"]);
                                if (!string.IsNullOrEmpty(Request.Form["city"]) &&Request.Form["city"].Split(',').Length > 2)
                                {
                                    string[] multiplecities = Request.Form["city"].Split(',');
                                    reqObj.CountryName = Request.Form["city"].Split(',')[multiplecities.Length - 1];
                                }
                                else
                                {
                                    reqObj.CountryName = Request.Form["city"].Split(',')[1];
                                }
                                
                            }
                            reqObj.CityName = Request.Form["City"].Split(',')[0];
                            Session["city"] = Request.Form["City"];
                        }
                       
                    }
                    Settings.LoginInfo.OnBehalfAgentID = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgents.SelectedValue) : Settings.LoginInfo.AgentId);

                    try
                    {
                        if (rbtnAgent.Checked)
                        {
                            Settings.LoginInfo.IsOnBehalfOfAgent = true;
                            AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                            Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                            Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                            Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
                            StaticData sd = new StaticData();
                            sd.BaseCurrency = agent.AgentCurrency;
                            Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                            Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);

                            Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;

                        }
                        else
                        {
                            Settings.LoginInfo.IsOnBehalfOfAgent = false;
                            Settings.LoginInfo.OnBehalfAgentID = 0;
                        }
                    }
                    catch { }

                    if (Convert.ToBoolean(Session["isB2B2BAgent"]) && sources.Contains("IAN"))
                    {
                        sources.Remove("IAN");
                    }
                    sources = new List<string>();
                    sources.Add("GIMMONIX");
                        if (hdnSelectedSources.Value != "" && (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent))
                        {
                            string[] selSources = hdnSelectedSources.Value.Split(',');
                        List<int> SupplierIdsList = new List<int>();
                            if (selSources.Length > 0)
                            {
                                for (int i = 0; i < selSources.Length; i++)
                                {
                                SupplierIdsList.Add(Convert.ToInt32(selSources[i]));
                                }
                            reqObj.SupplierIds = SupplierIdsList.ToArray();
                            }
                        }
                    else
                    {
                        reqObj.SupplierIds = null;
                    }
                       
                    reqObj.Sources = sources;
                    //stores the details.
                    string sDate = CheckIn.Text;//Request["CheckIn"];
                    string eDate = CheckOut.Text;//Request["CheckOut"];

                    IFormatProvider format = new CultureInfo("en-GB", true);

                    reqObj.StartDate = DateTime.Parse(sDate, format);
                    if (eDate.Length > 0)
                    {
                        reqObj.EndDate = DateTime.Parse(eDate, format);
                    }
                    reqObj.PassengerCountryOfResidence = ddlNationality.SelectedItem.Value;
                    reqObj.PassengerNationality = ddlNationality.SelectedItem.Value;

                    //////////////////////////////////////////////////////////////////////////////////
                    //                  Update Hotel Static Data for DOTW API                       //
                    try
                    {
                        if (reqObj.Sources.Contains("DOTW"))
                        {
                            CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi();
                            HotelRequest request = new HotelRequest();
                            Dictionary<string, DateTime> hotelCityDates = HotelStaticData.GetStaticDataCityDates(HotelBookingSource.DOTW);

                            HotelCity hc = HotelCity.Load(reqObj.CityId);
                            request.CityCode = hc.DOTWCode;
                            request.Currency = "366";
                            request.StartDate = reqObj.StartDate;
                            request.EndDate = reqObj.EndDate;
                            if (hotelCityDates != null && hotelCityDates.Count > 0)
                            {
                                int updateInterval = Convert.ToInt32(CT.Configuration.ConfigurationSystem.DOTWConfig["StaticDataUpdateInterval"]);

                                if (hotelCityDates.ContainsKey(hc.DOTWCode))
                                {
                                    DateTime lastUpdated = hotelCityDates[hc.DOTWCode];

                                    if (DateTime.Now.Subtract(lastUpdated).Days >= updateInterval)
                                    {
                                        List<HotelStaticData> staticData = dotw.GetHotelStaticData(request);
                                    }
                                }
                                else
                                {
                                    List<HotelStaticData> staticData = dotw.GetHotelStaticData(request);
                                }
                            }
                            else
                            {
                                List<HotelStaticData> staticData = dotw.GetHotelStaticData(request);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.HotelSearch, Severity.High, (int)Settings.LoginInfo.UserID, "Failed To Download StaticData(DOTW):" + ex.Message, Request["REMOTE_ADDR"]);
                    }
                    //finally
                    //{
                    //    //Update Application object with latest dates
                    //    Application["HOTEL_STATIC_DATA"] = HotelStaticData.GetStaticDataCityDates();
                    //}
                    //                                  End                                         //    
                    //////////////////////////////////////////////////////////////////////////////////


                    //////////////////////////////////////////////////////////////////////////////////
                    //                  Update Hotel Static Data for Agoda API                       //
                    try
                    {
                        if (reqObj.Sources.Contains("Agoda"))
                        {
                            CT.BookingEngine.GDS.Agoda agodaApi = new CT.BookingEngine.GDS.Agoda();
                            HotelRequest request = new HotelRequest();
                            Dictionary<string, DateTime> hotelCityDates = HotelStaticData.GetStaticDataCityDates(HotelBookingSource.Agoda);
                            HotelCity hc = HotelCity.Load(reqObj.CityId);
                            if (hotelCityDates != null && hotelCityDates.Count > 0)
                            {
                                int updateInterval = Convert.ToInt32(CT.Configuration.ConfigurationSystem.AgodaConfig["StaticDataUpdateInterval"]);

                                if (hotelCityDates.ContainsKey(hc.AgodaCode))
                                {
                                    DateTime lastUpdated = hotelCityDates[hc.AgodaCode];

                                    if (DateTime.Now.Subtract(lastUpdated).Days >= updateInterval)
                                    {
                                        agodaApi.GetHotelStaticData(hc.AgodaCode);
                                    }
                                }
                                else
                                {
                                    agodaApi.GetHotelStaticData(hc.AgodaCode);
                                }
                            }
                            else
                            {
                                agodaApi.GetHotelStaticData(hc.AgodaCode);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.HotelSearch, Severity.High, (int)Settings.LoginInfo.UserID, "Failed To Download StaticData(Agoda):" + ex.Message, Request["REMOTE_ADDR"]);
                    }
                    //finally
                    //{
                    //    //Update Application object with latest dates
                    //    Application["HOTEL_STATIC_DATA"] = HotelStaticData.GetStaticDataCityDates();
                    //}
                    //                                  End                                         //    
                    //////////////////////////////////////////////////////////////////////////////////
                    #region GRN static Data Cheking
                    try
                    {
                        if (reqObj.Sources.Contains("GRN"))
                        {
                            bool isGetStaticData = false;
                            CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                            HotelRequest request = new HotelRequest();
                            Dictionary<string, DateTime> hotelCityDates = HotelStaticData.GetStaticDataCityDates(HotelBookingSource.GRN);
                            HotelCity hc = HotelCity.Load(reqObj.CityId);

                            if (hotelCityDates != null && hotelCityDates.Count > 0)
                            {
                                int updateInterval = Convert.ToInt32(CT.Configuration.ConfigurationSystem.GrnConfig["StaticDataUpdateInterval"]);
                                if (hotelCityDates.ContainsKey(hc.GrnCode))
                                {
                                    DateTime lastUpdated = hotelCityDates[hc.GrnCode];

                                    if (DateTime.Now.Subtract(lastUpdated).Days >= updateInterval)
                                    {
                                        isGetStaticData = true;
                                    }
                                }
                                else
                                {
                                    isGetStaticData = true;
                                }
                            }
                            else
                            {
                                isGetStaticData = true;
                            }
                            if (isGetStaticData)
                            {
                                grnApi.GrnUserId = Settings.LoginInfo.UserID;
                                grnApi.GetHotelStaticData(hc.GrnCode);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.HotelSearch, Severity.High, (int)Settings.LoginInfo.UserID, "Failed To Download StaticData(GRN):" + ex.Message, Request["REMOTE_ADDR"]);
                    }
                    #endregion

                    switch (Convert.ToInt16(rating.SelectedValue))
                    {
                        case 0:
                            reqObj.MinRating = 0;
                            reqObj.MaxRating = 5;
                            break;
                        case 1:
                            reqObj.MinRating = 1;
                            reqObj.MaxRating = 1;
                            break;
                        case 2:
                            reqObj.MinRating = 2;
                            reqObj.MaxRating = 2;
                            break;
                        case 3:
                            reqObj.MinRating = 3;
                            reqObj.MaxRating = 3;
                            break;
                        case 4:
                            reqObj.MinRating = 4;
                            reqObj.MaxRating = 4;
                            break;
                        case 5:
                            reqObj.MinRating = 5;
                            reqObj.MaxRating = 5;
                            break;
                            //Presently we are not using(we are having rating from 1-5 only)
                        //case 6:
                        //    reqObj.MinRating = 1;
                        //    reqObj.MaxRating = 5;
                        //    break;
                        //case 7:
                        //    reqObj.MinRating = 2;
                        //    reqObj.MaxRating = 5;
                        //    break;
                        //case 8:
                        //    reqObj.MinRating = 3;
                        //    reqObj.MaxRating = 5;
                        //    break;
                        //case 9:
                        //    reqObj.MinRating = 4;
                        //    reqObj.MaxRating = 5;
                        //    break;
                        //case 10:
                        //    reqObj.MinRating = 5;
                        //    reqObj.MaxRating = 5;
                        //    break;
                    }

                    //reqObj.CityName = ddlCity.SelectedValue.Split(',')[0];
                    if (txtHotelName.Text.Length > 0)
                    {
                        reqObj.HotelName = txtHotelName.Text;
                    }
                    else
                    {
                        reqObj.HotelName = "";
                    }

                    reqObj.Corptravelreason = ddlFlightTravelReasons.SelectedIndex > 0 ? ddlFlightTravelReasons.SelectedValue : string.Empty;
                    reqObj.Corptraveler = ddlFlightEmployee.SelectedIndex > 0 ? ddlFlightEmployee.SelectedValue : string.Empty;

                    Session["req"] = reqObj;
                    //Session["BookingAgencyID"] = Request["BookingAgencyID"];

                    //if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
                    //{
                    //    if (hdnBookingAgent.Value == "Checked" && Convert.ToInt32(ddlAirAgents.SelectedItem.Value) > 1)
                    //    {
                    //        //Settings.LoginInfo.AgentId = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
                    //        Session["BookingAgencyID"] = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
                    //    }
                    //}
                    //Session["AgentName"] = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgents.SelectedValue) : Settings.LoginInfo.AgentId);
                    if (reqObj.StartDate.Equals(reqObj.EndDate))
                    {
                        errorMessage = "The Checkin CheckOut should not be same!";

                        return;
                    }


                    string rooms = Request["roomCount"];
                    reqObj.NoOfRooms = Convert.ToInt16(rooms);
                    if (reqObj.NoOfRooms > 1)
                    {
                        reqObj.IsMultiRoom = true;
                    }

                    guestInfo = new RoomGuestData[reqObj.NoOfRooms];
                    int oyoPaxCount = 0;//  // Added by Somasekhar on 12/12/2018 for OYO Source -- To get paxCount of request.
                    int paxCount = 0;
                    for (int i = 0; i < reqObj.NoOfRooms; i++)
                    {
                        int adults = 0, childs = 0;
                        //Checks whether Number of children more than 1.
                        string request = "chdRoom-" + (i + 1);
                        if (Request[request] != "none" && Convert.ToInt16(Request[request]) > 0)
                        {
                            guestInfo[i].noOfChild = Convert.ToInt16(Request[request]);
                            paxCount += guestInfo[i].noOfChild;
                            childs += guestInfo[i].noOfChild;
                            List<int> childInfo = new List<int>();
                            string numChild = string.Empty; ;
                            for (int j = 1; j <= guestInfo[i].noOfChild; j++)
                            {
                                numChild = "ChildBlock-" + (i + 1) + "-ChildAge-" + j;
                                childInfo.Add(Convert.ToInt16(Request[numChild]));
                            }
                            guestInfo[i].childAge = childInfo;
                        }
                        else
                        {
                            guestInfo[i].noOfChild = 0;
                            guestInfo[i].childAge = new List<int>();
                        }
                        string adultStr = "adtRoom-" + (i + 1);
                        guestInfo[i].noOfAdults = Convert.ToInt16(Request[adultStr]);
                        paxCount += guestInfo[i].noOfAdults;
                        adults += guestInfo[i].noOfAdults;
                        // Added by Somasekhar on 12/12/2018 for OYO Source -- To get paxCount of request.
                        // As per current OYO policy >5 ages treated as adult 
                        oyoPaxCount += adults;
                        for (int k = 0; k < guestInfo[i].noOfChild; k++)
                        {
                            if (guestInfo[i].childAge[k] >= 6)
                            {
                                oyoPaxCount += 1;
                            }
                        }
                        //================================================
                        if (adults == 1 && childs > 2)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 2 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 3 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 4 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 5 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 6 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 7 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 8 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 9 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                        else if (adults == 10 && childs > 4)
                        {
                            reqObj.Sources.Remove("DOTW");
                        }
                    }

                    /////////////////////////////////////////////////////////////////////////////////////////////////////
                    //      Restrict RezLive from search queue as they are restricting at their end                
                    //      because REzLive won't allow to book when MaxRoom > 3 or MaxPax > 9
                    int maxRooms = 3, maxPax = 9, DOTWMaxPax = 14; //Hard-coding in config values not available for RexLive

                    //if (CT.Configuration.ConfigurationSystem.RezLiveConfig.ContainsKey("MaxRooms"))
                    //{
                    //    maxRooms = Convert.ToInt32(CT.Configuration.ConfigurationSystem.RezLiveConfig["MaxRooms"]);
                    //}

                    //if (CT.Configuration.ConfigurationSystem.RezLiveConfig.ContainsKey("MaxPax"))
                    //{
                    //    maxPax = Convert.ToInt32(CT.Configuration.ConfigurationSystem.RezLiveConfig["MaxPax"]);
                    //}

                    //foreach (string source in reqObj.Sources)
                    //{
                    //    Audit.Add(EventType.HotelSearch, Severity.Normal, Settings.LoginInfo.AgentId, "Hotel Sources For " + Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName + " : " + source, Request["REMOTE_ADDR"]);
                    //}

                    if (reqObj.NoOfRooms > maxRooms || paxCount > maxPax)
                    {
                        reqObj.Sources.Remove("RezLive");  //because REzLive won't allow to book when MaxRoom > 3 or MaxPax > 9  uncommented by brahmam 14.12.2015
                        reqObj.Sources.Remove("LOH");
                    }
                    if (paxCount > maxPax)
                    {
                        //reqObj.Sources.Remove("RezLive");
                        // reqObj.Sources.Remove("LOH");

                        reqObj.Sources.Remove("GTA");
                    }
                    // Added by Somasekhar on 12/12/2018
                    if (reqObj.NoOfRooms > oyoPaxCount || oyoPaxCount > (3 * reqObj.NoOfRooms))
                    {
                        reqObj.Sources.Remove("OYO");
                    }
                    //========================


                    //Audit.Add(EventType.HotelSearch, Severity.Normal, Settings.LoginInfo.AgentId, "Pax Count searched For " + Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName + " : " + paxCount, Request["REMOTE_ADDR"]);

                    //if (DOTWMaxPax < paxCount)
                    //{
                    //    reqObj.Sources.Remove("DOTW");
                    //}

                    //
                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    reqObj.RoomGuest = guestInfo;
                    if (reqObj.Sources.Count > 0)
                    {
                        //Response.Redirect("HotelResults.aspx", false);

                        Response.Redirect("ApiHotelResults.aspx", false);

                    }
                    else
                    {
                        Session["error"] = "Please change your search parameters and try!";
                        Response.Redirect("HotelSearch.aspx?source=Hotel", false);
                    }
                }
            }
        }

        /// <summary>
        /// Method for binding Search request details
        /// </summary>
        //void BindFlightRequest()
        //{
        //    searchRequest = Session["FlightRequest"] as SearchRequest;

        //    if (searchRequest != null)
        //    {
        //        switch (searchRequest.Type)
        //        {
        //            case SearchType.Return:
        //                roundtrip.Checked = true;
        //                oneway.Checked = false;
        //                multicity.Checked = false;
        //                break;
        //            case SearchType.OneWay:
        //                oneway.Checked = true;
        //                roundtrip.Checked = false;
        //                multicity.Checked = false;
        //                break;
        //            case SearchType.MultiWay:
        //                multicity.Checked = true;
        //                roundtrip.Checked = false;
        //                break;
        //        }


        //        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        //        {

        //            radioAgent.Checked = true;

        //            radioAgent.Attributes.Add("onclick", "javascript:return disablefield()");
        //            string script = " <script type=\"text/javascript\"> disablefield    ();   </script> ";
        //            this.Page.ClientScript.RegisterStartupScript(typeof(Page), "callfn", script);
        //            ddlAirAgents.Items.FindByValue(Settings.LoginInfo.OnBehalfAgentID.ToString()).Selected = true;
        //        }

        //        try
        //        {
        //            if (searchRequest.Type != SearchType.MultiWay)
        //            {
        //                FlightSegment segment = searchRequest.Segments[0];

        //                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                {
        //                    Origin.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                    break;
        //                }

        //                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                {
        //                    Destination.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                    break;
        //                }
        //                IFormatProvider format = new CultureInfo("en-GB");
        //                string[] depDate = segment.PreferredDepartureTime.ToString("dd-MM-yyyy").Split('-');
        //                string[] arrDate = new string[0];
        //                DepDate.Text = depDate[0] + "/" + depDate[1] + "/" + depDate[2];
        //                if (searchRequest.Type == SearchType.Return)
        //                {
        //                    ReturnDateTxt.Text = searchRequest.Segments[1].PreferredArrivalTime.ToString("dd/MM/yyyy");
        //                }
        //                else
        //                {
        //                    ReturnDateTxt.Text = segment.PreferredArrivalTime.ToString("dd/MM/yyyy");
        //                }
        //            }
        //            else
        //            {

        //                for (int i = 0; i < searchRequest.Segments.Length; i++)
        //                {
        //                    FlightSegment segment = searchRequest.Segments[i];
        //                    switch (i + 1)
        //                    {
        //                        case 1:
        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                            {
        //                                City1.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }

        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                            {
        //                                City2.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }
        //                            Time1.Text = Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy");

        //                            break;
        //                        case 2:
        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                            {
        //                                City3.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }

        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                            {
        //                                City4.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }
        //                            Time2.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
        //                            break;
        //                        case 3:
        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                            {
        //                                City5.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }

        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                            {
        //                                City6.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }
        //                            Time3.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
        //                            break;
        //                        case 4:
        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                            {
        //                                City7.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }

        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                            {
        //                                City8.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }
        //                            Time4.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
        //                            break;
        //                        case 5:
        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                            {
        //                                City9.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }

        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                            {
        //                                City10.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }
        //                            Time5.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
        //                            break;
        //                        case 6:
        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
        //                            {
        //                                City11.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }

        //                            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
        //                            {
        //                                City12.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
        //                                break;
        //                            }
        //                            Time6.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
        //                            break;

        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {
        //        }

        //        ddlAdults.SelectedIndex = -1;
        //        ddlAdults.Items.FindByValue(searchRequest.AdultCount.ToString()).Selected = true;

        //        ddlChilds.SelectedIndex = -1;
        //        ddlChilds.Items.FindByValue(searchRequest.ChildCount.ToString()).Selected = true;

        //        ddlInfants.SelectedIndex = -1;
        //        ddlInfants.Items.FindByValue(searchRequest.InfantCount.ToString()).Selected = true;

        //        ddlBookingClass.SelectedIndex = -1;
        //        ddlBookingClass.Items.FindByValue(((int)searchRequest.Segments[0].flightCabinClass).ToString()).Selected = true;

        //        if (searchRequest.Segments[0].PreferredAirlines.Length > 0)
        //        {
        //            Airline airline = new Airline();
        //            airline.Load(searchRequest.Segments[0].PreferredAirlines[0]);
        //            txtPreferredAirline.Text = airline.AirlineName;
        //            //airlineName.Value = searchRequest.Segments[0].PreferredAirlines[1];
        //            airlineCode.Value = searchRequest.Segments[0].PreferredAirlines[0];
        //        }
        //    }
        //}


        // binding Param(nationality,residence)
        private void bindCountryList()
        {
            DOTWCountry dotwC = new DOTWCountry();
            Dictionary<string, string> countryList = dotwC.GetAllCountries();

            ddlNationality.DataSource = countryList;
            ddlNationality.DataValueField = "key";
            ddlNationality.DataTextField = "value";
            ddlNationality.DataBind();
            //Added by somasekhar on 26/09/2018  
            if (Settings.LoginInfo.LocationCountryCode == "IN")
            {
                ddlNationality.SelectedIndex = -1;
                ddlNationality.Items.FindByText("INDIA").Selected = true;
            }
            //ddlNationality.Items.Insert(0, new ListItem("Choose from list"));

            //ddlResidence.DataSource = countryList;
            //ddlResidence.DataValueField = "key";
            //ddlResidence.DataTextField = "value";
            //ddlResidence.DataBind();
            //Added by somasekhar on 26/09/2018
            //if (Settings.LoginInfo.LocationCountryCode == "IN")
            //{
            //    ddlResidence.SelectedIndex = -1;
            //    ddlResidence.Items.FindByText("INDIA").Selected = true;
            //}
            //ddlResidence.Items.Insert(0, new ListItem("Choose from list"));

        }
        /// <summary>
        /// This is for Authentication.
        /// </summary>
        private void AuthorizationCheck()
        {
            //if (Session["roleId"] == null)
            //{
            //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
            //    values += "&requestUri=" + Request.Url.ToString();
            //    Response.Redirect("Default.aspx" + values, true);
            //}
            //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelSearch))
            //{
            //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
            //    values += "&requestUri=" + Request.Url.ToString();
            //    Response.Redirect("Default.aspx" + values, true);
            //}
        }

        //protected void btnSearchFlight_Click(object sender, EventArgs e)
        //{
        //    airlineMessage = string.Empty;
        //    //Prepare SearchRequest object 
        //    try
        //    {
        //        List<Airline> restrictedAirlines = new List<Airline>();
        //        searchRequest.SearchBySegments = (rbtnSearchBySegment.Checked ? true : false);// For Segment Wise Search
        //                                                                                      // Adding Corporate values in Search Request

        //        if (Settings.LoginInfo.IsCorporate == "Y")
        //        {
        //            string[] profId = ddlFlightEmployee.SelectedValue.Split('-');
        //            if (profId.Length > 0)
        //            {

        //                searchRequest.CorporateTravelProfileId = Convert.ToInt32(profId[0]);
        //                //Settings.LoginInfo.CorporateProfileId = Convert.ToInt32(profId[0]);// Overriding Profile Id from TRaveller Lilst
        //                Settings.LoginInfo.CorporateProfileGrade = profId[1];
        //                searchRequest.CorporateTravelProfileId = Convert.ToInt32(profId[0]);
        //                //searchRequest.CorporateTravelReason = Convert.ToInt32(profId[0]);
        //                if (ddlFlightTravelReasons.SelectedValue.Contains("~"))
        //                {
        //                    searchRequest.CorporateTravelReasonId = Convert.ToInt32(ddlFlightTravelReasons.SelectedValue.Split('~')[0]);
        //                    searchRequest.AppliedPolicy = (ddlFlightTravelReasons.SelectedValue.Split('~')[1] == "P" ? true : false);
        //                }
        //            }
        //        }


        //        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        //        {
        //            if (hdnBookingAgent.Value == "Checked" && Convert.ToInt32(ddlAirAgents.SelectedItem.Value) > 1)
        //            {
        //                // ziyad to check
        //                //Settings.LoginInfo.AgentId = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
        //                Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);

        //                Settings.LoginInfo.IsOnBehalfOfAgent = true;
        //                Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
        //                AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
        //                Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
        //                Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
        //                StaticData sd = new StaticData();
        //                sd.BaseCurrency = agent.AgentCurrency;
        //                Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
        //                Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);

        //                Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;

        //            }
        //            else
        //            {
        //                Settings.LoginInfo.IsOnBehalfOfAgent = false;
        //            }
        //        }

        //        DataTable dtFlightSources = new DataTable();
        //        List<string> FlightSources = new List<string>();
        //        if (Session["FlightSources"] != null)
        //        {
        //            dtFlightSources = Session["FlightSources"] as DataTable;
        //            foreach (DataRow dr in dtFlightSources.Rows)
        //            {
        //                FlightSources.Add(dr["Name"].ToString());
        //            }
        //        }
        //        if (hdnWayType.Value != "multiway")
        //        {

        //            searchRequest.AdultCount = Convert.ToInt32(ddlAdults.SelectedValue);
        //            searchRequest.ChildCount = Convert.ToInt32(ddlChilds.SelectedValue);
        //            searchRequest.InfantCount = Convert.ToInt32(ddlInfants.SelectedValue);
        //            searchRequest.RestrictAirline = false;
        //            searchRequest.Segments = new FlightSegment[2];
        //            FlightSegment fwdSegment = new FlightSegment();
        //            FlightSegment retSegment = new FlightSegment();

        //            //fwdSegment.Origin = Origin.Text.Split(')')[0].Replace("(", "");
        //            //fwdSegment.Destination = Destination.Text.Split(')')[0].Replace("(", "");
        //            fwdSegment.Origin = GetCityCode(Origin.Text.Trim());
        //            fwdSegment.Destination = GetCityCode(Destination.Text.Trim());

        //            string departureDate = DepDate.Text;

        //            switch (ddlDepTime.SelectedValue)
        //            {
        //                //case "Morning":
        //                //    departureDate += " 08:00 AM";
        //                //    break;
        //                //case "Afternoon":
        //                //    departureDate += " 02:00 PM";
        //                //    break;
        //                //case "Evening":
        //                //    departureDate += " 07:00 PM";
        //                //    break;
        //                //case "Night":
        //                //    departureDate += " 01:00 AM";
        //                //    break;
        //                //default:
        //                //    break;
        //                case "Morning":
        //                    departureDate += " 05:00 AM";
        //                    break;
        //                case "Afternoon":
        //                    departureDate += " 12:00 PM";
        //                    break;
        //                case "Evening":
        //                    departureDate += " 06:00 PM";
        //                    break;
        //                case "Night":
        //                    departureDate += " 11:59:59 PM";
        //                    break;
        //                default:
        //                    break;
        //            }
        //            //searchRequest.Unrestrictedcarrier = txtAgentFee.Text;Adding Agent markup fee

        //            fwdSegment.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);

        //            IFormatProvider format = new CultureInfo("en-GB", true);
        //            fwdSegment.PreferredDepartureTime = Convert.ToDateTime(departureDate, format);
        //            fwdSegment.PreferredArrivalTime = Convert.ToDateTime(departureDate, format);

        //            Airline restrictedAirline = new Airline();


        //            //if (txtPreferredAirline.Text.Length > 0)
        //            if (airlineCode.Value.Length > 0)
        //            {
        //                searchRequest.RestrictAirline = true;
        //                if (airlineCode.Value.Split(',').Length == 1)
        //                {
        //                    string[] airlines = new string[1];
        //                    //airlines[1] = airlineName.Value;
        //                    airlines[0] = airlineCode.Value;
        //                    restrictedAirline.Load(airlineCode.Value);
        //                    restrictedAirlines.Add(restrictedAirline);
        //                    fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
        //                }
        //                else
        //                {
        //                    string[] airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                    for (int i = 0; i < airlines.Length; i++)
        //                    {
        //                        Airline airline = new Airline();
        //                        airline.Load(airlines[i]);
        //                        restrictedAirlines.Add(airline);
        //                    }
        //                    fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
        //                }

        //            }
        //            else
        //            {
        //                fwdSegment.PreferredAirlines = new string[0];
        //            }

        //            searchRequest.Segments[0] = fwdSegment;
        //            if (hdnWayType.Value == "return")
        //            {
        //                retSegment.Origin = fwdSegment.Destination;
        //                retSegment.Destination = fwdSegment.Origin;
        //                retSegment.flightCabinClass = fwdSegment.flightCabinClass;

        //                string ReturnDateTime = ReturnDateTxt.Text;

        //                switch (ddlReturnTime.SelectedValue)
        //                {
        //                    //case "Morning":
        //                    //    ReturnDateTime += " 08:00 AM";
        //                    //    break;
        //                    //case "Afternoon":
        //                    //    ReturnDateTime += " 02:00 PM";
        //                    //    break;
        //                    //case "Evening":
        //                    //    ReturnDateTime += " 07:00 PM";
        //                    //    break;
        //                    //case "Night":
        //                    //    ReturnDateTime += " 01:00 AM";
        //                    //    break;
        //                    //default:
        //                    //    break;
        //                    case "Morning":
        //                        ReturnDateTime += " 05:00 AM";
        //                        break;
        //                    case "Afternoon":
        //                        ReturnDateTime += " 12:00 PM";
        //                        break;
        //                    case "Evening":
        //                        ReturnDateTime += " 06:00 PM";
        //                        break;
        //                    case "Night":
        //                        ReturnDateTime += " 11:59:59 PM";
        //                        break;
        //                    default:
        //                        break;
        //                }
        //                if (ReturnDateTime.Length > 0)
        //                {
        //                    retSegment.PreferredArrivalTime = Convert.ToDateTime(ReturnDateTime, format);
        //                    retSegment.PreferredDepartureTime = Convert.ToDateTime(ReturnDateTime, format);
        //                }
        //                else
        //                {
        //                    retSegment.PreferredArrivalTime = fwdSegment.PreferredArrivalTime.AddDays(1);
        //                    retSegment.PreferredDepartureTime = fwdSegment.PreferredDepartureTime.AddDays(1);
        //                }

        //                //if (txtPreferredAirline.Text.Length > 0)
        //                //string[] airlines1 = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                //airlines1 = Array.ConvertAll<string, string>(airlines1, delegate(string s) { return s.ToLower(); });

        //                if (airlineCode.Value.Length > 0)
        //                {
        //                    if (airlineCode.Value.Split(',').Length == 1)
        //                    {
        //                        string[] airlines = new string[1];
        //                        //airlines[1] = airlineName.Value;
        //                        airlines[0] = airlineCode.Value;
        //                        retSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
        //                    }
        //                    else
        //                    {
        //                        string[] airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        //                        retSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
        //                    }
        //                }
        //                else
        //                {
        //                    retSegment.PreferredAirlines = new string[0];
        //                }

        //                searchRequest.Segments[1] = retSegment;
        //                searchRequest.Type = SearchType.Return;
        //            }
        //            else
        //            {
        //                searchRequest.Segments[1] = new FlightSegment();
        //                searchRequest.Type = SearchType.OneWay;
        //            }
        //            List<string> sources = new List<string>();
        //            foreach (ListItem item in chkSuppliers.Items)
        //            {
        //                if (item.Selected)
        //                {
        //                    sources.Add(item.Text);
        //                }
        //            }



        //            //if (restrictedAirline.AirlineName != null && restrictedAirline.AirlineName.Length > 0)
        //            //{
        //            //    if (!restrictedAirline.TBOAirAllowed && restrictedAirlines.Count == 0)
        //            //    {
        //            //        if (sources.Count == 1)
        //            //        {
        //            //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
        //            //        }
        //            //        //if (sources.Contains("TA"))
        //            //        //{
        //            //        //    sources.Remove("TA");
        //            //        //}
        //            //    }
        //            //    if (!restrictedAirline.UapiAllowed && restrictedAirlines.Count == 0)
        //            //    {
        //            //        if (sources.Count == 1)
        //            //        {
        //            //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
        //            //        }
        //            //        //if (sources.Remove("UA"))
        //            //        //{
        //            //        //    sources.Remove("UA");
        //            //        //}
        //            //    }
        //            //}

        //            foreach (Airline ra in restrictedAirlines)
        //            {
        //                if (!ra.TBOAirAllowed && sources.Exists(delegate (string s) { return s.ToUpper() == "TA"; }))
        //                {
        //                    if (!airlineMessage.Contains(ra.AirlineName))
        //                    {
        //                        if (airlineMessage.Length > 0)
        //                        {
        //                            airlineMessage += "," + ra.AirlineName + " for TA";
        //                        }
        //                        else
        //                        {
        //                            airlineMessage = ra.AirlineName + " for TA";
        //                        }
        //                    }
        //                    //if (sources.Contains("TA"))
        //                    //{
        //                    //    sources.Remove("TA");
        //                    //}
        //                }
        //                if (!ra.UapiAllowed && sources.Exists(delegate (string s) { return s.ToUpper() == "UA"; }))
        //                {
        //                    if (!airlineMessage.Contains(ra.AirlineName))
        //                    {
        //                        if (airlineMessage.Length > 0)
        //                        {
        //                            airlineMessage += "," + ra.AirlineName + " for UA";
        //                        }
        //                        else
        //                        {
        //                            airlineMessage = ra.AirlineName + " for UA";
        //                        }
        //                    }
        //                    //if (sources.Remove("UA"))
        //                    //{
        //                    //    sources.Remove("UA");
        //                    //}
        //                }
        //            }

        //            //if (txtPreferredAirline.Text.Length > 0 && txtPreferredAirline.Text != "Type Preferred Airline")
        //            //{
        //            //    if (airlineCode.Value != "G9")
        //            //    {
        //            //        sources.Remove("G9");
        //            //    }
        //            //    else
        //            //    {
        //            //        sources.Remove("UA");
        //            //    }
        //            //}
        //            //If Preferred airline list contains G9 (AirArabia) or FZ (FlyDubai) or SG (SpiceJet)
        //            //then allow searching for that source otherwise remove from the sources list
        //            if (airlineCode.Value.Length > 0)
        //            {
        //                if (!airlineCode.Value.Contains("G9") && sources.Contains("G9"))
        //                {
        //                    sources.Remove("G9");
        //                }
        //                if (!airlineCode.Value.Contains("FZ") && sources.Contains("FZ"))
        //                {
        //                    sources.Remove("FZ");
        //                }
        //                if (!airlineCode.Value.Contains("SG") && sources.Contains("SG"))
        //                {
        //                    sources.Remove("SG");
        //                }
        //                if (airlineCode.Value.Contains("FZ") && sources.Contains("UA"))
        //                {
        //                    sources.Remove("UA");//Remove UAPI if preferred airline is Fly dubai.
        //                }
        //                if (!airlineCode.Value.Contains("6E") && sources.Contains("6E"))
        //                {
        //                    sources.Remove("6E");
        //                }
        //                if (!airlineCode.Value.Contains("IX") && sources.Contains("IX"))
        //                {
        //                    sources.Remove("IX");
        //                }
        //            }
        //            //sources.Add("G9");
        //            //sources.Add("UA");
        //            searchRequest.Sources = sources;
        //            if (rbtnlMaxStops.SelectedItem.Value != "-1")
        //            {
        //                searchRequest.MaxStops = rbtnlMaxStops.SelectedItem.Value;
        //            }
        //            if (chkRefundFare.Checked)
        //            {
        //                searchRequest.RefundableFares = true;
        //            }
        //            Session["FlightRequest"] = searchRequest;
        //        }
        //        else
        //        {
        //            searchRequest.Type = SearchType.MultiWay;

        //            int adults = Convert.ToInt32(ddlAdults.SelectedValue);
        //            int childs = Convert.ToInt32(ddlChilds.SelectedValue);
        //            int infants = Convert.ToInt32(ddlInfants.SelectedValue);

        //            searchRequest.AdultCount = adults;
        //            searchRequest.ChildCount = childs;
        //            searchRequest.InfantCount = infants;

        //            int segmentCount = 2;

        //            //string city1 = Request["city1"];
        //            //string city2 = Request["city2"];
        //            //string city3 = Request["city3"];
        //            //string city4 = Request["city4"];

        //            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

        //            int counter = 5;
        //            List<string> Cities = new List<string>();
        //            for (int i = counter; i < 13; i++)
        //            {
        //                if (Request["ctl00$cphTransaction$City" + i].Length > 0 && Request["ctl00$cphTransaction$City" + (i + 1)].Length > 0)
        //                {
        //                    segmentCount++;
        //                    Cities.Add(Request["ctl00$cphTransaction$City" + i]);
        //                    Cities.Add(Request["ctl00$cphTransaction$City" + (i + 1)]);
        //                }
        //                i++;
        //            }

        //            searchRequest.Segments = new FlightSegment[segmentCount];

        //            FlightSegment segment1 = new FlightSegment();
        //            segment1.Origin = GetCityCode(City1.Text);
        //            segment1.Destination = GetCityCode(City2.Text);
        //            segment1.PreferredArrivalTime = Convert.ToDateTime(Time1.Text, dateFormat);
        //            segment1.PreferredDepartureTime = Convert.ToDateTime(Time1.Text, dateFormat);
        //            segment1.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
        //            searchRequest.Segments[0] = segment1;

        //            FlightSegment segment2 = new FlightSegment();
        //            segment2.Origin = GetCityCode(City3.Text);
        //            segment2.Destination = GetCityCode(City4.Text);
        //            segment2.PreferredArrivalTime = Convert.ToDateTime(Time2.Text, dateFormat);
        //            segment2.PreferredDepartureTime = Convert.ToDateTime(Time2.Text, dateFormat);
        //            segment2.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
        //            searchRequest.Segments[1] = segment2;

        //            int timeCounter = 3;
        //            counter = 0;
        //            if (Cities.Count > 0)
        //            {
        //                while (counter < Cities.Count)
        //                {
        //                    FlightSegment segment = new FlightSegment();
        //                    segment.Origin = GetCityCode(Cities[counter]);
        //                    counter++;
        //                    segment.Destination = GetCityCode(Cities[counter]);
        //                    segment.PreferredArrivalTime = Convert.ToDateTime(Request["ctl00$cphTransaction$Time" + timeCounter], dateFormat);
        //                    segment.PreferredDepartureTime = Convert.ToDateTime(Request["ctl00$cphTransaction$Time" + timeCounter], dateFormat);
        //                    segment.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
        //                    searchRequest.Segments[timeCounter - 1] = segment;

        //                    timeCounter++;
        //                    counter++;
        //                }
        //            }

        //            //if (city7.Length > 0 && city8.Length > 0)
        //            //{
        //            //    FlightSegment segment4 = new FlightSegment();
        //            //    segment4.Origin = GetCityCode(city7);
        //            //    segment4.Destination = GetCityCode(city8);
        //            //    segment4.PreferredArrivalTime = Convert.ToDateTime(Request["t4"], dateFormat);
        //            //    segment4.PreferredDepartureTime = Convert.ToDateTime(Request["t4"], dateFormat);

        //            //    searchRequest.Segments[3] = segment4;
        //            //}

        //            //if (city9.Length > 0 && city10.Length > 0)
        //            //{
        //            //    FlightSegment segment5 = new FlightSegment();
        //            //    segment5.Origin = GetCityCode(city9);
        //            //    segment5.Destination = GetCityCode(city10);
        //            //    segment5.PreferredArrivalTime = Convert.ToDateTime(Request["t5"], dateFormat);
        //            //    segment5.PreferredDepartureTime = Convert.ToDateTime(Request["t5"], dateFormat);

        //            //    searchRequest.Segments[4] = segment5;
        //            //}

        //            //if (city11.Length > 0 && city12.Length > 0)
        //            //{
        //            //    FlightSegment segment6 = new FlightSegment();
        //            //    segment6.Origin = GetCityCode(city11);
        //            //    segment6.Destination = GetCityCode(city12);
        //            //    segment6.PreferredArrivalTime = Convert.ToDateTime(Request["t6"], dateFormat);
        //            //    segment6.PreferredDepartureTime = Convert.ToDateTime(Request["t6"], dateFormat);

        //            //    searchRequest.Segments[5] = segment6;
        //            //}


        //            //if (txtPreferredAirline.Text.Length > 0 && txtPreferredAirline.Text != "Type Preferred Airline")
        //            //{
        //            //    string[] airlines = new string[2];
        //            //    airlines[1] = airlineName.Value;
        //            //    airlines[0] = airlineCode.Value;
        //            //    searchRequest.Segments[0].PreferredAirlines = airlines;
        //            //    searchRequest.Segments[1].PreferredAirlines = airlines;
        //            //    if (searchRequest.Segments.Length > 2)
        //            //    {
        //            //        searchRequest.Segments[2].PreferredAirlines = airlines;
        //            //    }
        //            //    if (searchRequest.Segments.Length > 3)
        //            //    {
        //            //        searchRequest.Segments[3].PreferredAirlines = airlines;
        //            //    }
        //            //    if (searchRequest.Segments.Length > 4)
        //            //    {
        //            //        searchRequest.Segments[4].PreferredAirlines = airlines;
        //            //    }
        //            //    if (searchRequest.Segments.Length > 5)
        //            //    {
        //            //        searchRequest.Segments[5].PreferredAirlines = airlines;
        //            //    }
        //            //}
        //            //else
        //            //{
        //            //    searchRequest.Segments[0].PreferredAirlines = new string[0];
        //            //    searchRequest.Segments[1].PreferredAirlines = new string[0];
        //            //    if (searchRequest.Segments.Length > 2)
        //            //    {
        //            //        searchRequest.Segments[2].PreferredAirlines = new string[0];
        //            //    }
        //            //    if (searchRequest.Segments.Length > 3)
        //            //    {
        //            //        searchRequest.Segments[3].PreferredAirlines = new string[0];
        //            //    }
        //            //    if (searchRequest.Segments.Length > 4)
        //            //    {
        //            //        searchRequest.Segments[4].PreferredAirlines = new string[0];
        //            //    }
        //            //    if (searchRequest.Segments.Length > 5)
        //            //    {
        //            //        searchRequest.Segments[5].PreferredAirlines = new string[0];
        //            //    }
        //            //}

        //            Airline restrictedAirline = new Airline();


        //            if (airlineCode.Value.Length > 0)
        //            {
        //                string[] airlines = new string[0];
        //                if (airlineCode.Value.Length == 1)
        //                {
        //                    airlines = new string[2];
        //                    airlines[1] = airlineName.Value;
        //                    airlines[0] = airlineCode.Value;
        //                    restrictedAirline.Load(airlineCode.Value);
        //                    restrictedAirlines.Add(restrictedAirline);
        //                }
        //                else
        //                {
        //                    airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        //                    foreach (string code in airlines)
        //                    {
        //                        Airline airline = new Airline();
        //                        airline.Load(code);
        //                        restrictedAirlines.Add(airline);
        //                    }
        //                }

        //                searchRequest.Segments[0].PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
        //                searchRequest.Segments[0].PreferredAirlines = airlines;
        //                if (searchRequest.Segments.Length > 2)
        //                {
        //                    searchRequest.Segments[2].PreferredAirlines = airlines;
        //                }
        //                if (searchRequest.Segments.Length > 3)
        //                {
        //                    searchRequest.Segments[3].PreferredAirlines = airlines;
        //                }
        //                if (searchRequest.Segments.Length > 4)
        //                {
        //                    searchRequest.Segments[4].PreferredAirlines = airlines;
        //                }
        //                if (searchRequest.Segments.Length > 5)
        //                {
        //                    searchRequest.Segments[5].PreferredAirlines = airlines;
        //                }
        //            }
        //            else
        //            {
        //                searchRequest.Segments[0].PreferredAirlines = new string[0];
        //                searchRequest.Segments[1].PreferredAirlines = new string[0];
        //                if (searchRequest.Segments.Length > 2)
        //                {
        //                    searchRequest.Segments[2].PreferredAirlines = new string[0];
        //                }
        //                if (searchRequest.Segments.Length > 3)
        //                {
        //                    searchRequest.Segments[3].PreferredAirlines = new string[0];
        //                }
        //                if (searchRequest.Segments.Length > 4)
        //                {
        //                    searchRequest.Segments[4].PreferredAirlines = new string[0];
        //                }
        //                if (searchRequest.Segments.Length > 5)
        //                {
        //                    searchRequest.Segments[5].PreferredAirlines = new string[0];
        //                }
        //            }

        //            List<string> sources = new List<string>();
        //            foreach (ListItem item in chkSuppliers.Items)
        //            {
        //                if (item.Selected)
        //                {
        //                    sources.Add(item.Text);
        //                }
        //            }



        //            //if (restrictedAirline.AirlineName != null && restrictedAirline.AirlineName.Length > 0)
        //            //{
        //            //    if (!restrictedAirline.TBOAirAllowed && restrictedAirlines.Count == 0)
        //            //    {
        //            //        if (sources.Count == 1)
        //            //        {
        //            //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
        //            //        }
        //            //        //if (sources.Contains("TA"))
        //            //        //{
        //            //        //    sources.Remove("TA");
        //            //        //}
        //            //    }
        //            //    if (!restrictedAirline.UapiAllowed && restrictedAirlines.Count == 0)
        //            //    {
        //            //        if (sources.Count == 1)
        //            //        {
        //            //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
        //            //        }
        //            //        //if (sources.Remove("UA"))
        //            //        //{
        //            //        //    sources.Remove("UA");
        //            //        //}
        //            //    }
        //            //}

        //            foreach (Airline ra in restrictedAirlines)
        //            {
        //                if (!ra.TBOAirAllowed)
        //                {
        //                    if (!airlineMessage.Contains(ra.AirlineName))
        //                    {
        //                        if (airlineMessage.Length > 0)
        //                        {
        //                            airlineMessage += "," + ra.AirlineName;
        //                        }
        //                        else
        //                        {
        //                            airlineMessage = ra.AirlineName;
        //                        }
        //                    }
        //                    //if (sources.Contains("TA"))
        //                    //{
        //                    //    sources.Remove("TA");
        //                    //}
        //                }
        //                if (!ra.UapiAllowed)
        //                {
        //                    if (!airlineMessage.Contains(ra.AirlineName))
        //                    {
        //                        if (airlineMessage.Length > 0)
        //                        {
        //                            airlineMessage += "," + ra.AirlineName;
        //                        }
        //                        else
        //                        {
        //                            airlineMessage = ra.AirlineName;
        //                        }
        //                    }
        //                    //if (sources.Remove("UA"))
        //                    //{
        //                    //    sources.Remove("UA");
        //                    //}
        //                }
        //            }

        //            if (airlineCode.Value.Length > 0)
        //            {
        //                if (!airlineCode.Value.Contains("G9") && sources.Contains("G9"))
        //                {
        //                    sources.Remove("G9");
        //                }
        //                if (!airlineCode.Value.Contains("FZ") && sources.Contains("FZ"))
        //                {
        //                    sources.Remove("FZ");
        //                }
        //                if (!airlineCode.Value.Contains("SG") && sources.Contains("SG"))
        //                {
        //                    sources.Remove("SG");
        //                }
        //                if (airlineCode.Value.Contains("FZ") && sources.Contains("UA"))
        //                {
        //                    sources.Remove("UA");//Remove UAPI if preferred airline is Fly dubai.
        //                }
        //            }
        //            //sources.Add("G9");
        //            //sources.Add("UA");
        //            searchRequest.Sources = sources;
        //            if (rbtnlMaxStops.SelectedItem.Value != "-1")
        //            {
        //                searchRequest.MaxStops = rbtnlMaxStops.SelectedItem.Value;
        //            }
        //            if (chkRefundFare.Checked)
        //            {
        //                searchRequest.RefundableFares = true;
        //            }

        //            Session["FlightRequest"] = searchRequest;
        //        }

        //        bool isSearchRestricted = false;
        //        int uapiRestrict = 0, tboRestrict = 0;
        //        if (restrictedAirlines.Count > 0)
        //        {
        //            if (restrictedAirlines.Count == airlineMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length)
        //            {
        //                isSearchRestricted = true;
        //            }

        //            uapiRestrict = restrictedAirlines.FindAll(delegate (Airline a) { return !a.UapiAllowed; }).Count;
        //            tboRestrict = restrictedAirlines.FindAll(delegate (Airline a) { return !a.TBOAirAllowed; }).Count;

        //            if (restrictedAirlines.Count == uapiRestrict)
        //            {
        //                if (searchRequest.Sources.Contains("UA"))
        //                {
        //                    searchRequest.Sources.Remove("UA");
        //                }
        //            }

        //            if (restrictedAirlines.Count == tboRestrict)
        //            {
        //                if (searchRequest.Sources.Contains("TA"))
        //                {
        //                    searchRequest.Sources.Remove("TA");
        //                }
        //            }
        //        }

        //        if (searchRequest.InfantCount > 0 || (searchRequest.Type == SearchType.MultiWay && searchRequest.Segments.Length > 2) || searchRequest.RefundableFares)
        //        {
        //            if (searchRequest.Sources.Count > 1)
        //                searchRequest.Sources.Remove("PK");
        //            else if (searchRequest.Sources.Contains("PK"))
        //            {
        //                if (searchRequest.InfantCount > 0 && searchRequest.Segments.Length > 2)
        //                {
        //                    isSearchRestricted = true;
        //                    lblMessage.Text = "Infants are not allowed currently in Search for PK. Only two connections/segments allowed in Search for PK. Please modify your search.";
        //                }
        //                else if (searchRequest.InfantCount > 0)
        //                {
        //                    isSearchRestricted = true;
        //                    lblMessage.Text = "Infants are not allowed currently in Search for PK. Please modify your search.";
        //                }
        //                else if (searchRequest.Segments.Length > 2)
        //                {
        //                    isSearchRestricted = true;
        //                    lblMessage.Text = "Only two connections/segments allowed in Search for PK. Please modify your search.";
        //                }
        //            }
        //        }

        //        Session["FlightRequest"] = searchRequest;

        //        if (!isSearchRestricted)
        //        {
        //            if (!rbtnSearchByPrice.Checked && !rbtnSearchBySegment.Checked)
        //            {
        //                Response.Redirect("FlightResult.aspx");
        //            }
        //            else if (rbtnSearchByPrice.Checked)
        //            {
        //                Response.Redirect("FlightResult.aspx");
        //            }
        //            else if (rbtnSearchBySegment.Checked)
        //            {
        //                Response.Redirect("FlightResultsBySegments.aspx");
        //            }
        //        }
        //        else
        //        {
        //            if (!string.IsNullOrEmpty(airlineMessage))
        //            {
        //                lblMessage.Text += Environment.NewLine + "You are not allowed to search with these preferred airlines." + airlineMessage;
        //            }
        //            //Session["Error"] = lblMessage.Text;
        //            //Response.Redirect("HotelSearch.aspx?source=Flight&R=Yes");
        //            CT.TicketReceipt.Common.Utility.StartupScript(this, "alert('" + lblMessage.Text + "');window.location.href='HotelSearch.aspx?source=Flight'", "Search Error");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Airline, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, "0");
        //    }
        //}

        private void BindMessages()
        {
            int agencyId = (int)Settings.LoginInfo.AgentId;
            DataSet Messages = IMPMessages.GetMessages(agencyId);
            Session["Messages"] = Messages;
            gvMessages.DataSource = Messages;
            gvMessages.DataBind();

        }

        //private void BindSuppliers()
        //{
        //    try
        //    {
        //        DataTable dtFlightSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 1);
        //        Session["FlightSources"] = dtFlightSources;
        //        //List<string> FlightSource = new List<string>();
        //        //System.Collections.Generic.Dictionary<string, string> sourcesConfig = CT.Configuration.ConfigurationSystem.ActiveSources;
        //        //foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourcesConfig)
        //        //{
        //        //    if (pair.Key == "UA" || pair.Key == "G9")
        //        //    {
        //        //        FlightSource.Add(pair.Key);
        //        //    }
        //        //}
        //        chkSuppliers.DataSource = dtFlightSources;
        //        chkSuppliers.DataValueField = "Id";
        //        chkSuppliers.DataTextField = "Name";
        //        chkSuppliers.DataBind();
        //        foreach (ListItem item in chkSuppliers.Items)
        //        {
        //            if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack && Page.PreviousPage.Title == "Flight Search Results")
        //            {
        //                if (searchRequest == null)
        //                {
        //                    searchRequest = Session["FlightReq"] as SearchRequest;
        //                }

        //                if (searchRequest.Sources != null)
        //                {
        //                    if (searchRequest.Sources.Contains(item.Text))
        //                    {
        //                        item.Selected = true;
        //                    }
        //                }
        //                if (dtFlightSources.Rows.Count > searchRequest.Sources.Count)
        //                {
        //                    chkFlightAll.Checked = false;
        //                }
        //            }
        //            else
        //            {
        //                item.Selected = true;
        //            }
        //        }

        //        if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.IsRoutingEnabled)
        //        {
        //            foreach (DataRow row in dtFlightSources.Rows)
        //            {
        //                if (Convert.ToBoolean(row["IsRouting"]))
        //                {
        //                    if (!string.IsNullOrEmpty(routingSources))
        //                    {
        //                        routingSources += "," + row["Name"].ToString();
        //                    }
        //                    else
        //                    {
        //                        routingSources = row["Name"].ToString();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected void gvMessages_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMessages.PageIndex = e.NewPageIndex;
            gvMessages.EditIndex = -1;
            DataSet Messages = (DataSet)Session["Messages"];
            gvMessages.DataSource = Messages;
            gvMessages.DataBind();
        }

        private string GetCityCode(string searchCity) // For Flight
        {
            string cityCode = "";

            cityCode = searchCity.Split(',')[0];

            if (cityCode.Contains("(") && cityCode.Contains(")") && cityCode.Length > 3)
            {
                // cityCode = cityCode.Substring(cityCode.Length - 4, 3);
                cityCode = cityCode.Substring(1, 3);
            }

            //if (cityCode.Contains(" "))
            //{
            //    cityCode = cityCode.Split(' ')[0].Split(')')[0].Replace("(", "");
            //}
            //else
            //{
            //    cityCode = cityCode.Split(')')[0].Replace("(", "");
            //}
            return cityCode;
        }


    }
}
