﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Corporate;

public partial class CorporateProfileSetupUI : CT.Core.ParentPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (!IsPostBack)
            {

                string setUp = Utility.ToString(Request.QueryString["Setup"]);
                //string setUp = "GD";

                ddlSetup.SelectedValue = setUp;
                lblHeaderSetup.Text = ddlSetup.SelectedItem.Text;
                ddlSetup.Enabled = false;
                lblAllSetups.Text = "ALL " + lblHeaderSetup.Text + "S";
                lblAddSetups.Text = "ADD " + lblHeaderSetup.Text;

                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();

                //Clear();
                //lblSuccessMsg.Text = string.Empty;

            }
            else
                hdfTab.Value = "111";
            //StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {

        try
        {
           // Utility.StartupScript(this.Page, "ShowTab('A');", "showTab");
        
        }
        catch { throw; }
    }
    private void InitializePageControls()
    {
        try
        {

            // Clear();

            BindAgent();

            BindSetupGrid();
            //Clear();
            
        }
        catch { throw; }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Client--", "0"));
            
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.Enabled = false;
            }
            try
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            catch { }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Corporate Profile page " + ex.Message, "0");
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            Save();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
            Utility.StartupScript(this.Page, "ShowTab();", "showTab");
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
           
        }
    }

    protected void Save()
    {
        try
        {

            CorporateProfileSetup profileSetup = new CorporateProfileSetup();

            profileSetup.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            profileSetup.SetupId= Utility.ToLong(hdfTranxId.Value)>0?  Utility.ToLong(hdfTranxId.Value):-1;
            profileSetup.Code = txtCode.Text.Trim();
            profileSetup.Name = txtDescription.Text.Trim();
            profileSetup.Type = ddlSetup.SelectedItem.Value;
            profileSetup.Status = Settings.ACTIVE;
            profileSetup.Save();
            string descValue = txtDescription.Text.Trim();
            string setupName = ddlSetup.SelectedItem.Text;
            long modeVal=Utility.ToLong(hdfTranxId.Value);
            Clear();
            string message = string.Empty;
            if (modeVal <= 0)
                message = "Corporate Profile " + setupName + " "+ descValue + " is created !";
            else message = "Corporate Profile " + setupName + " " + descValue + " is updated !";

            Utility.Alert(this.Page, message);
            
            BindSetupGrid();
           // Utility.StartupScript(this.Page, "ShowTab('L');", "showTab");     //TO SHOW LIST TAB 

        }

        catch { throw; }
    
    
    }

    private void BindSetupGrid()
    {
        try
        {

            DataTable dtSetup = CorporateProfileSetup.GetList(Utility.ToInteger(ddlAgent.SelectedItem.Value), ddlSetup.SelectedItem.Value, ListStatus.Long);
            CommonGrid grid = new CommonGrid();
            //ViewState["gvTax"] = dtTax;
            grid.BindGrid(gvSetupDetails, dtSetup);         
            //Clear();
            //Utility.StartupScript(this.Page, "ShowTab();", "showTab");



        }
        catch { }
    }


    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            //if (agentId == 0) agentId = 1;
            BindSetupGrid();
            Clear();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void Clear()
    {

        try
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            hdfTranxId.Value = "0";
            btnAdd.Text = "Add";
            Utility.StartupScript(this.Page, "ShowTab();", "showTab");
        }
        catch { throw; }

    
    }


    protected void gvSetupDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
          

            GridViewRow gvRow = gvSetupDetails.Rows[e.RowIndex];
            long serial = Utility.ToLong(gvSetupDetails.DataKeys[e.RowIndex].Value);

            CorporateProfileSetup deleteSetup = new CorporateProfileSetup(serial);
            deleteSetup.Status = Settings.DELETED;
            deleteSetup.CreatedBy =Utility.ToInteger(Settings.LoginInfo.UserID);
            deleteSetup.Save();
            //DataRow dr = dtTax.Rows.Find(serial);
            //if (gvSetupDetails.Rows.Count > 1)
            //{
              //  dtTax.Rows.Find(serial).Delete();
                BindSetupGrid();

              
            //}

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void ITbtnEdit_Click(object sender, EventArgs e)
    {
        GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
        HiddenField IThdfSetupId = (HiddenField)gvRow.FindControl("IThdfSetupId");
        CorporateProfileSetup corpSetUp = new CorporateProfileSetup(Utility.ToLong(IThdfSetupId.Value));
        hdfTranxId.Value = Utility.ToString(corpSetUp.SetupId);
        txtCode.Text = Utility.ToString(corpSetUp.Code);
        txtDescription.Text = Utility.ToString(corpSetUp.Name);
        btnAdd.Text = "Update";
        Utility.StartupScript(this.Page, "ShowTab();", "showTab");
        //hdfTab.Value = "A";
        
    }
}
