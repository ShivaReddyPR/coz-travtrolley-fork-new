<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintReport" Codebehind="PrintReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>Print Receipt</title>
    <link type="text/css" href="Styles/CTStyle.css" rel="stylesheet" />
    <link type="text/css" href="Styles/default.css" rel="stylesheet" />
</head>
<script type="text/javascript" language="javascript">
//    function print()
//    {
//        document.getElementById('Print').style.display="none";
//	    window.print();	 
//	    setTimeout('showButtons()',100000);
//    }
//    function showButtons()
//    {
//        document.getElementById('Print').style.display="block";	    
//    }
 function printPage()
    {
        document.getElementById('btnPrint').style.display="none";
	    window.print();	 
	    //alert('2');
	    setTimeout('showButtons()',1000);
	   // alert('3');
    }
    function showButtons()
    {
        document.getElementById('btnPrint').style.display="block";	    
    }
    </script>

<body>

    <form id="form1" runat="server">
    <table width="100%" style="text-align:center">
    <tr>
    <td  align="center">
    <div  style="width:630px">
    <div id="divPrintButton" style="text-align:left">
    <input style="width:100px;" id="btnPrint" onclick="return printPage();" class="button" type="button" value="Print Ticket" />
    </div>
    <div style="border:1px solid #000; padding:5px; width:100%; overflow:hidden; position:relative">
    
    <asp:HiddenField runat="server" id="hdfDecimalLocations" Value="3,4,196" ></asp:HiddenField><%--LocationIds--%>
    <asp:HiddenField runat="server" id="hdfDecimals" Value="3-2,4-2,196-2" ></asp:HiddenField><%--locationId-DecimalNo--%>
    <asp:HiddenField runat="server" id="hdfDecimalNo" Value="2" ></asp:HiddenField><%--Decimal No--%>
   
  <%-- <script  type="text/javascript">
       PAGE_DECIMALVAL = parseInt(getElement('hdfDecimalNo').value);
       GLOBAL_DECIVAL = PAGE_DECIMALVAL
       //alert(getElement('hdfDecimalNo').value);
       //alert(PAGE_DECIMALVAL);
 </script>--%>
<table cellpadding="0" cellspacing="0" class="label"  width="100%" >
        <tr>
        <td>
        <table style="width:100%" >
        <tr>
        
          <td align="left">
                    <label style="font-style:italic;font-size:10px;color:Gray" class="rptLabel">Customer Copy</label>
                </td>
            <td  align="right">
                <div class="logo"><img src="images/logo.gif" height="50px" width="150px" alt="" /></div>
            </td>
            </tr>
        </table>
        </td>
        </tr>    
        <tr>
        <td align="center">
        <%--<label class="rptLabel" style="text-decoration:underline">Receipt</label>--%>
        <asp:Label ID="lblHeading" style="text-decoration:underline" CssClass="rptLabel" Text="Receipt" runat="server" ></asp:Label>
        </td>
        </tr>
        <tr>
            <td style="text-align:left">
            <br />
      <table cellpadding="0" cellspacing="0" class="rptTable"  >
       <%-- <tr>
            <td style="width:150px;"></td>
            <td></td>
        </tr>--%>
        <tr>
            <td style="width:150px" align="left"><asp:Label ID="lblReceiptNo" CssClass="rptLabel" runat="server" Text="No.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblReceiptNoValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblDate" CssClass="rptLabel" runat="server" Text="DATE:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblDateValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblStaffId" CssClass="rptLabel"  runat="server" Text="StaffId.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblStaffIdValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCompany" CssClass="rptLabel"  runat="server" Text="Company:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCompanyValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblPaxName" CssClass="rptLabel"  runat="server" Text="Pax Name:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblPaxNameValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        
         <tr>
            <td align="left"><asp:Label ID="lblContactNo" CssClass="rptLabel"  runat="server" Text="Contact No:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblContactValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblTotalFare" CssClass="rptLabel" runat="server" Text="To Collect:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblTotalFareValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCollectionMode" CssClass="rptLabel" runat="server" Text="Collection Mode:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCollectionModeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
       
         <tr>
            <td align="left"><asp:Label ID="lblTickedBy" CssClass="rptLabel" runat="server" Text="Ticketed By:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblTickedByValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td align="left"><asp:Label ID="lblRemarks" CssClass="rptLabel" runat="server" Text="Remarks:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblRemarksValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
    </table> 
    
    </td>
    </tr>
    <tr>
   <td align="center">
   <br />
  
  <center>
  
    <%--<p><img src="images/URDU_LA.gif" width="600px" alt="" /></p>--%>
     <asp:Label ID="lblLocationTerms" runat="server" style="font-style:italic;"></asp:Label><br /><br />
   <asp:Label ID="lblLocationAds" runat="server" style="font-size:12px;  padding-bottom:5px;"></asp:Label>
   <%-- <p style="font-style:italic;">Dear Customer: Prior to Payment, Please Reconfirm Your Booking Details. Once Payment is made all Cozmo Travel Terms And condition will be Applicable</p>
   
    <p style="font-size:14px;  padding-bottom:5px;">Cozmo Travel L.L.C, P.O.Box: 3393, Tower 400, Sharjah- UAE, TEL: 06 507 4444</p>--%>
   
   </center>
    </td>
    
    </tr>
    <tr>
        <td align="center"><br />
            <label style="font-style:italic;font-weight:bold;font-size:13px">** This receipt is not valid without company stamp **</label>
        </td>
    </tr>
 </table>
 
 
 
 <label id="lblStamp" runat="server" style=" position:absolute; bottom: 0px; right:0; "> <img src="images/stamp.png" />   </label>
 
 
    
     </div>
 <br />
  <p style="text-align:center; font-size:14px; font-weight:bold; border-top:1px solid #000;"></p>
  
 <br />
   <div style="border:1px solid #000; padding:5px; width:100%; overflow:hidden; position:relative">
<table cellpadding="0" cellspacing="0" class="label" width="100%"  >
        <tr>
        <td>
        <table style="width:100%" >
        <tr>
        
          <td align="left">
                    <label style="font-style:italic;font-size:10px;color:Gray" class="rptLabel">Accounts Copy</label>
                </td>
            <td  align="right">
                <div class="logo"><img src="images/logo.gif" height="50px" width="150px" alt="" /></div>
            </td>
            </tr>
        </table>
        </td>
        </tr>     
        <tr>
        <td align="center">
        
        
        <asp:Label ID="lblCopyHeading" style="text-decoration:underline" CssClass="rptLabel" Text="Receipt" runat="server" ></asp:Label>
        </td>
        </tr>
        <tr>
            <td style="text-align:left">
                    
            <br />
      <table cellpadding="0" cellspacing="0" class="rptTable"  >
        <%--<tr>
            <td style="width:150px;height:40px"></td>
            <td></td>
        </tr>--%>
        <tr>
            <td  align="left"><asp:Label ID="lblCopyReceiptNo" CssClass="rptLabel" runat="server" Text="No.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyReceiptNoValue" CssClass="rptLabelValue" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopyDate" CssClass="rptLabel" runat="server" Text="DATE:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyDateValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopyStaffId" CssClass="rptLabel"  runat="server" Text="StaffId.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyStaffIdValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        
         <tr>
            <td align="left"><asp:Label ID="lblCopyCompany" CssClass="rptLabel"  runat="server" Text="Company:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyCompanyValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        
        <tr>
            <td align="left"><asp:Label ID="lblCopyPaxName" CssClass="rptLabel"  runat="server" Text="Pax Name:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyPaxNameValue" CssClass="rptLabelValue"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopyTotalFare" CssClass="rptLabel" runat="server" Text="To Collect:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyTotalFareValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopyCollectionMode" CssClass="rptLabel" runat="server" Text="Collection Mode:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyCollectionModeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
         <tr>
            <td align="left"><asp:Label ID="lblCopyTicketedBy" CssClass="rptLabel" runat="server" Text="Ticketed By:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyTicketedByValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td align="left"><asp:Label ID="lblCopyRemarks" CssClass="rptLabel" runat="server" Text="Remarks:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyRemarksValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
    </table> 
    
    </td>
    </tr>
    <tr>
    <td align="center">
    <br />
    <%--<p><img src="images/URDU_LA.gif" width="600px" alt="" /></p>--%>
    <center>
    
     <asp:Label ID="lblLocationTermsCopy" runat="server" style="font-style:italic;"></asp:Label><br /><br />
   <asp:Label ID="lblLocationAdsCopy" runat="server" style="font-size:12px;  padding-bottom:5px;"></asp:Label>
    </center>
    <%--<p style="font-style:italic;">Dear Customer: Prior to Payment, Please Reconfirm Your Booking Details. Once Payment is made all Cozmo Travel Terms And condition will be Applicable</p>
   
    <p style="font-size:14px;  padding-bottom:5px;">Cozmo Travel L.L.C, P.O.Box: 3393, Tower 400, Sharjah- UAE, TEL: 06 507 4444</p>--%>
    </td>
    
    </tr>
 </table>
    
    
    <%--<label style=" position:absolute; bottom: 0px; right:0; "> <img src="images/stamp.png" />   </label>--%>
     </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="33%"><strong>Checked By: </strong></td>
    <td width="36%"><strong>Approved By: </strong></td>
    <td width="31%"><strong>Recieved By: </strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>Contact No :</strong></td>
  </tr>
</table>
</div>
 </td></tr>
    </table>

    </form>
</body>
</html>
