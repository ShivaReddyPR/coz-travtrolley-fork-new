﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.Insurance;
using System.Data;

namespace CozmoB2BWebApp
{
    public partial class BaggageInsuranceInvoiceGUI : CT.Core.ParentPage
    {
        protected Invoice invoice = new Invoice();
        protected AgentMaster agency;
        protected string agencyAddress;
        protected RegCity agencyCity = new RegCity();
        protected UserMaster loggedMember = new UserMaster();
        protected int insHdrId;
        protected int agencyId;
        protected int cityId = 0;
        protected string remarks = string.Empty;
        protected string pnr;
        protected BaggageInsuranceHeader header = new BaggageInsuranceHeader();
        protected bool isServiceAgency;
        protected string supplierName = string.Empty;
        protected bool paymentDoneAgainstInvoice = false;
        protected string planName = string.Empty;
        protected string planDesc = string.Empty;
        protected string policy = string.Empty;
        protected string paxName = string.Empty;
        protected DataSet dsPaxDetails = null;

        protected void Page_Load(object sender, EventArgs e)
        {
             
            loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
            int invoiceNumber = 0;
            int insHdrId = 0;
            decimal amount = 0;
            decimal baseFee = 0;
            int paxCount = 1;
            decimal primiumamount = 0;
            //checking if the page is reached with proper data
            if (Request["insHdrId"] != null)
            {
                insHdrId = Convert.ToInt32(Request["insHdrId"]);
            }
            else
            {
                invoiceNumber = Convert.ToInt32(Request["invoiceNumber"]);
            }
            BaggageInsuranceHeader header = new BaggageInsuranceHeader();
            dsPaxDetails = new DataSet();
            dsPaxDetails = header.LoadBaggageHeader(insHdrId);
            if (dsPaxDetails != null && dsPaxDetails.Tables[0].Rows.Count > 0)
            {
                 amount = Convert.ToDecimal(dsPaxDetails.Tables[0].Rows[0]["BI_TOTAL_PRICE"]);

                 if(dsPaxDetails.Tables[1].Rows.Count>0)
                {
                    paxCount = dsPaxDetails.Tables[1].Rows.Count;
                    baseFee = Convert.ToDecimal(dsPaxDetails.Tables[1].Rows[0]["PAX_PRICE"]);
                    baseFee = baseFee * paxCount;
                }
            }

            paxName = header.FirstName + "" + header.LastName;
            BaggageInsuranceQueue obj = new BaggageInsuranceQueue();
            DataTable dtPlans = obj.GetPlanDetailsById(insHdrId);
            foreach (DataRow  dr in dtPlans.Rows)
            {
                planName= dr["PLAN_TITLE"].ToString() +", " + dr["PLAN_DESCRIPTION"].ToString();
                policy = dr["PolicyNo"].ToString();
            }
           
            ///raiseInvoice = RaiseInvoice(baggageheader, remarks, memberId, rateOfExchange);

            pnr = header.PNR;
            agency = new AgentMaster(header.AgentID);
            DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
            DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

            if (cities != null && cities.Length > 0)
            {
                cityId = Convert.ToInt32(cities[0]["city_id"]);
            }
            isServiceAgency = true;// agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
            if (agency.City.Length == 0)
            {
                agencyCity.CityName = agency.City;
            }
            else
            {
                agencyCity = RegCity.GetCity(cityId);
            }
            
            if (agency.Address != null && agency.Address.Length > 0)
            {
                agencyAddress += agency.Address;
                agencyAddress.Trim();
            }
            //plans = header.Bid;
            // Generating invoice.
            if (header.Bid > 0)
            {
                if (invoiceNumber == 0)
                {
                    invoiceNumber = Invoice.isInvoiceGenerated(header.Bid, CT.BookingEngine.ProductType.BaggageInsurance);
                }
            }
            if (invoiceNumber > 0)
            {
                invoice = new Invoice();
                invoice.Load(invoiceNumber);
                supplierName = Invoice.GetSupplierByInvoiceNumber(invoiceNumber);
            }
            else
            {
                invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(header.Bid, string.Empty, (int)loggedMember.ID, ProductType.BaggageInsurance, 1);
                invoice.Load(invoiceNumber);
            }
            paymentDoneAgainstInvoice = Invoice.IsPaymentDoneAgainstInvoice(invoiceNumber);
            LocationMaster location = new LocationMaster(header.Location);
            lblLocation.Text = location.Name;
            //Fare calculations;
            decimal TotalPremiumAmt = amount;
            decimal Markup = 0;
            decimal Discount = 0;
            decimal B2CMarkup = 0;
            decimal gst = 0;
            decimal inputVAT = 0;
            decimal outputVAT = 0;

            foreach (DataRow dr in dsPaxDetails.Tables[1].Rows)
            {
             
            //if (dsPaxDetails != null && dsPaxDetails.Tables[1].Rows.Count>0)
            //{
                    Markup += Convert.ToDecimal(dr["PAX_MarkUp"]);
                    Discount += Convert.ToDecimal(dr["PAX_Discount"]);
                    gst += Convert.ToDecimal(dr["PAX_GST"]);
                    inputVAT += Convert.ToDecimal(dr["PAX_INPUTVAT"]);
                    outputVAT += Convert.ToDecimal(dr["PAX_OUTPUTVAT"]);  
            }

            if (Settings.LoginInfo.AgentId > 1)
            {
                lblMarkup.Visible = false;
                lblMarkupVal.Visible = false;
                lblGST.Visible = false;
                lblGSTValue.Visible = false;
                TotalPremiumAmt = (baseFee + gst + inputVAT + outputVAT + Markup + B2CMarkup);
                lblPremiumAmount.Text = agency.AgentCurrency + " " + (baseFee + gst+ inputVAT+ outputVAT+ Markup  + B2CMarkup).ToString("N" + agency.DecimalValue); 

                //lblPremiumAmount.Text = agency.AgentCurrency + " " + (TotalPremiumAmt).ToString("N" + agency.DecimalValue); 
            }
            else
            {
                lblMarkup.Visible = true;
                lblMarkupVal.Visible = true;
                lblGST.Visible = true;
                lblGSTValue.Visible = true;
                lblPremiumAmount.Text = agency.AgentCurrency + " " + baseFee.ToString("N" + agency.DecimalValue); //((baseFee)- gst + inputVAT + outputVAT + Markup + B2CMarkup).ToString("N" + agency.DecimalValue);
                lblMarkupVal.Text = agency.AgentCurrency + " " + (Markup + B2CMarkup).ToString("N" + agency.DecimalValue);
                lblGSTValue.Text = agency.AgentCurrency + " " + (gst).ToString("N" + agency.DecimalValue);
                TotalPremiumAmt = (baseFee + gst + inputVAT + outputVAT + Markup + B2CMarkup);
            }
            if (Discount > 0)
            {
                lblDiscount.Visible = true;
                lblDiscountVal.Visible = true;
                lblDiscountVal.Text = agency.AgentCurrency + " " + Discount.ToString("N" + agency.DecimalValue);
            }
            //lblTotal.Text = agency.AgentCurrency + " " + (TotalPremiumAmt+ gst + inputVAT +servicefee + outputVAT+ Markup + B2CMarkup - Discount).ToString("N" + agency.DecimalValue);
            lblTotal.Text = agency.AgentCurrency + " " + (TotalPremiumAmt - Discount).ToString("N" + agency.DecimalValue);
        }
    }
}
