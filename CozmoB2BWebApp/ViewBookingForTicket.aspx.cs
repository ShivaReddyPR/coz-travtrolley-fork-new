using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class ViewBookingForTicket : CT.Core.ParentPage// System.Web.UI.Page
{
    protected UserMaster loggedMember = new UserMaster();
    public FlightItinerary itinerary;
    protected BookingDetail booking;
    protected Airline airline1;
    protected AgentMaster agency;
    protected Fare[] fareBreakDown;
    protected decimal[] otherCharges;
    protected double grandTotal = 0;
    protected bool locked = false;
    protected bool lockedByMe = false;
    protected string message = string.Empty;
    protected string routing = string.Empty;
    protected bool isAdmin = false;
    protected bool ticketed = false;
    protected bool isHold = false;
    protected bool canCancell = false;
    protected bool canTicket = false;
    protected bool canAutoTicketIntl = false;
    protected bool canAutoTicketDomestic = false;
    protected bool isDomestic = true;
    protected bool instantPurchase = false;
    protected bool eTicketEligible = true;
    protected bool airlineEligible = true;
    protected string ticketAdvisory = string.Empty;
    protected bool overrideCreditAllowed = false;
    protected decimal amountToCompare = 0;
    protected decimal handlingCharge;
    protected decimal tdsHc;
    protected decimal serviceTax;
    protected decimal transactionFee;
    protected decimal addHandlingCharge;
    protected decimal tdsAddHandlingCharge;
    protected decimal totalPublished;
    protected decimal totalAgentprice;
    protected decimal reverseHandlingCharge;
    protected string releaseSeatRedirectUrl = string.Empty;
    //protected bool statusNotConfirmed = true;    
    protected List<Comment> CommentData;
    protected List<SSR> SSRData;
    protected string displayReplyBox = "none";
    protected string displayComment = "none";
    protected bool error = false;
    protected string errorMsg = string.Empty;
    protected string bookingSource = string.Empty;
    protected Product[] products;
    protected decimal additionalTxnFee;
    protected bool isServiceAgency;
    //protected Insurance insurance;
    //protected decimal totalInsurancePrice = 0;
    protected int pendingId = 0;
    protected int bookingId = 0;
    int saveAgain = 0;
    protected Ticket[] ticket;
    protected bool isTicketedButFailed = false;
    protected bool islcc = false;
    public string strForm1 = "";
    protected string spantext = "Show Comments";
    List<BookingHistory> bkgHistory;
    /// <summary>
    /// Corporate booking Params
    /// </summary>
    protected bool isBookingSuccess = false;
    protected List<Ticket> ticketList;
    protected FlightItinerary flightItinerary;
    protected string inBaggage = "", outBaggage = "";
    protected CT.Core.Airline airline;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected decimal charges = 0, discount = 0;
    protected int emailCounter = 1;//Checking for Agent details display in Email body
    protected AutoResetEvent[] BookingEvents;
    protected int agencyId = 0;
    protected string corpTripApprovalStatus = "P";//Initially Pending
    protected LocationMaster location;
    protected bool isOneToOneSearch = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "View Booking for Ticketing";
        if (Session["FlightItinerary"] != null)
        {
            Session["FlightItinerary"] = null;
        }
        AuthorizationCheck();
        if (Request.QueryString["fromAgent"] != null)
        {
            string req = Request.QueryString["fromAgent"];
        }
        strForm1 = "<form id='SSRInfo' action='UpdateSSR.aspx' method='post'>";
        if (Request.QueryString["pendingId"] != null)
        {
            pendingId = Convert.ToInt32(Request.QueryString["pendingId"]);
            isTicketedButFailed = true;
        }
        #region Reading from session
        ///Reading from session for setting status and showing invoice button for failed bookings.
        if (Session["pendingId"] != null)
        {
            pendingId = Convert.ToInt32(Session["pendingId"]);
            Session.Remove("pendingId");
        }
        if (Session["isTicketedButFailed"] != null)
        {
            isTicketedButFailed = Convert.ToBoolean(Session["isTicketedButFailed"]);
            Session.Remove("isTicketedButFailed");
        }
        #endregion
        if (Settings.LoginInfo != null)
        {
            if (Settings.LoginInfo.AgentId == 0)
            {
                isAdmin = true;
                releaseSeatRedirectUrl = "BookingQueue.aspx";
            }
            else
            {
                releaseSeatRedirectUrl = "AgentBookingQueue.aspx";
            }
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }

        loggedMember = new UserMaster(Settings.LoginInfo.UserID);

        if (Request.QueryString["message"] != null)
        {
            message = Request.QueryString["message"];
        }   // else nothing to do. optional parameter, displayed in page if given.

        // Reading booking and flight itinerary corresponding to bookingId.
        if ((Request.QueryString["pendingId"] != null && Request.QueryString["pendingId"] != "0") || (Request.QueryString["VPI"] != null && Convert.ToBoolean(Request.QueryString["VPI"])))
        {
            int agencyId = 0;
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                agencyId = Settings.LoginInfo.AgentId;
            }
            itinerary = FailedBooking.LoadItinerary(pendingId);



            booking = new BookingDetail();
            booking.Status = BookingStatus.Ticketed;
            booking.CreatedBy = (itinerary != null ? itinerary.CreatedBy : (int)Settings.LoginInfo.UserID);
            booking.AgencyId = (itinerary != null ? itinerary.AgencyId : agencyId);
            agency = new AgentMaster(booking.AgencyId);
            List<string> pmTicketNo = new List<string>();
            if (itinerary != null)
            {
                if (itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.Paramount)
                {
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        pmTicketNo.Add(itinerary.PNR);
                    }
                }
            }
            bool isNotSaved = false;
            saveAgain = BookingUtility.SaveQueueAgain(itinerary, booking, pmTicketNo, ref isNotSaved, loggedMember, agency);

            if (saveAgain > 0)
            {
                if (Convert.ToBoolean(Request.QueryString["VPI"]))
                {
                    if (!isNotSaved)
                    {
                        int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
                        string routings = itinerary.Origin + "-" + itinerary.Destination;
                        Response.Redirect("CreateInvoice.aspx?flightId=" + flightId + "&pnr=" + itinerary.PNR + "&agencyId=" + agency.ID + "&bookingId=" + saveAgain + "&routing=" + routings + "&fromAgent=true");
                    }
                    else
                    {
                        Session["pendingId"] = pendingId;
                        Session["isTicketedButFailed"] = isTicketedButFailed;
                        Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + saveAgain + "&error=true" + "&fromAgent=true", true);
                    }
                }
                else
                {
                    Session["pendingId"] = pendingId;
                    Session["isTicketedButFailed"] = isTicketedButFailed;
                    Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + saveAgain + "&fromAgent=true", true);
                }
            }
            else
            {
                if (Convert.ToBoolean(Request.QueryString["VPI"]))
                {
                    error = true;
                    errorMsg = "There is some problem in showing the invoice.Please try again after some time";
                }
            }
            FailedBooking fb = FailedBooking.Load((itinerary.PNR));
            booking.CreatedOn = fb.CreatedOn;
        }
        else if (Request.QueryString["bookingId"] != null && Request.QueryString["bookingId"] != "0")
        {
            bookingId = Convert.ToInt32(Request.QueryString["bookingId"]);
            products = BookingDetail.GetProductsLine(bookingId);
            booking = new BookingDetail(bookingId);
            agency = new AgentMaster(booking.AgencyId);
            //TODO Ziyad
            //isServiceAgency = agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
            if (Request.QueryString["error"] != null && Convert.ToBoolean(Request.QueryString["error"]) && !(booking.Status == BookingStatus.Ticketed))
            {
                error = true;
                errorMsg = "There is some problem in showing the invoice.Please try again after some time";
            }
            hdnBookingId.Value = bookingId.ToString();
        }

        //if (!isAdmin && booking.AgencyId != loggedMember.AgentId)
        //{
        //    //ErrorMessage.Text = "Access Denied.";
        //    //MultiViewBooking.ActiveViewIndex = 1;
        //}
        //else
        //{
        //    //MultiViewBooking.ActiveViewIndex = 0;
        //}
        
        if (bookingId != 0)
        {
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.Flight)
                {
                    itinerary = new FlightItinerary(products[i].ProductId);                    
                    break;
                }
            }
            switch (itinerary.PaymentMode)
            {
                case ModeOfPayment.CreditCard:
                    ddlPaymentType.SelectedValue = "Card";
                    ddlPaymentType.Enabled = false;
                    agencyId = itinerary.AgencyId;                    
                    LoadPaymentGateways();
                    break;
                case ModeOfPayment.Credit:
                    ddlPaymentType.SelectedValue = "Credit";
                    ddlPaymentType.Enabled = false;
                    break;
            }

            //List<BookingDetail> insuranceBooking = BookingDetail.GetChildBookings(booking.BookingId);
            //for (int i = 0; i < insuranceBooking.Count; i++)
            //{
            //    Product[] product = insuranceBooking[i].ProductsList;
            //    for (int j = 0; j < product.Length; j++)
            //    {
            //        if (product[j].ProductType == ProductType.Insurance)
            //        {
            //            insurance = new Insurance();
            //            insurance.Load(product[j].ProductId);
            //        }
            //    }
            //}
            //if (insurance != null)
            //{
            //    for (int i = 0; i < insurance.Policy.Length; i++)
            //    {
            //        totalInsurancePrice += insurance.Policy[i].DisplayPrice;
            //    }
            //}
        }
        if (itinerary != null)
        {
            routing = QueueMethods.GetItineraryString(itinerary);

        }
        //if (Worldspan.IsInstantPurchase(itinerary))
        {
            instantPurchase = true;
        }
        if (booking != null)
        {
            if (booking.Status < BookingStatus.Ticketed && Util.IsToday(booking.CreatedOn))
            {
                if (isAdmin)
                {
                    if (itinerary.TicketAdvisory != null)
                    {
                        ticketAdvisory = itinerary.TicketAdvisory.Replace('*', ' ').Trim();
                    }
                    else
                    {
                        ticketAdvisory = string.Empty;
                    }
                }
                if (!isAdmin && instantPurchase)
                {
                    ticketAdvisory = "Instant ticket required.";
                }
            }
            // Checking lock status
            if (booking.LockedBy <= 0)
            {
                locked = false;
                lockedByMe = false;
            }
            else
            {
                locked = true;
                if (booking.LockedBy == (int)loggedMember.ID)
                {
                    lockedByMe = true;
                }
            }
            if (booking.Status == BookingStatus.Ticketed)
            {
                ticketed = true;
            }
            else if (booking.Status == BookingStatus.Hold)
            {
                isHold = true;
            }
        }
        //if (Role.IsAllowedTask(loggedMember.RoleId, (int)Task.CancelFlightBookings))
        //{
        //    canCancell = true;
        //}
        //if (Role.IsAllowedTask(loggedMember.RoleId, (int)Task.IssueFlightTickets))
        //{
        //    canTicket = true;
        //}
        //if (Role.IsAllowedTask(loggedMember.RoleId, (int)Task.OverrideCreditLimit))
        //{
        //    overrideCreditAllowed = true;
        //}
        //if (Role.IsAllowedTask(loggedMember.RoleId, (int)Task.DomesticAutoTicketing))
        //{
        //    canAutoTicketDomestic = true;
        //}
        //if (Role.IsAllowedTask(loggedMember.RoleId, (int)Task.InternationalAutoTicketing))
        //{
        //    canAutoTicketIntl = true;
        //}
        if (itinerary != null)
        {
            string originCountry = itinerary.Segments[0].Origin.CountryCode;
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (originCountry != itinerary.Segments[i].Destination.CountryCode)
                {
                    isDomestic = false;
                    break;
                }
            }
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                Airline airline = new Airline();
                airline.Load(itinerary.Segments[i].Airline);
                if (!airline.ETicketEligible)
                {
                    airlineEligible = false;
                    break;
                }
            }
            //TODO Ziyad
            bookingSource = itinerary.FlightBookingSource.ToString();//Util.GetBookingSourceCode(itinerary.FlightBookingSource);
            // Reading fare breakdown.
            // array contains one Fare object for each pax type.
            // if a pax type does not exist then its element is null.
            fareBreakDown = new Fare[4];
            otherCharges = new decimal[4];
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                totalPublished += itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.OtherCharges + itinerary.Passenger[i].Price.AdditionalTxnFee;
                if (!isServiceAgency)
                {
                    handlingCharge += itinerary.Passenger[i].Price.AgentCommission;
                    addHandlingCharge += itinerary.Passenger[i].Price.AgentPLB;
                    tdsHc += itinerary.Passenger[i].Price.TdsCommission;
                    tdsAddHandlingCharge += itinerary.Passenger[i].Price.TDSPLB;
                }
                serviceTax += itinerary.Passenger[i].Price.SeviceTax;
                transactionFee += itinerary.Passenger[i].Price.TransactionFee;
                additionalTxnFee += itinerary.Passenger[i].Price.AdditionalTxnFee;
                reverseHandlingCharge += itinerary.Passenger[i].Price.ReverseHandlingCharge;
                if (!isServiceAgency)
                {
                    totalAgentprice += itinerary.Passenger[i].Price.GetAgentPrice();
                }
                else
                {
                    totalAgentprice += itinerary.Passenger[i].Price.GetServiceAgentPrice();
                }
                int j = (int)itinerary.Passenger[i].Type - 1;
                if (fareBreakDown[j] == null)
                {
                    fareBreakDown[j] = new Fare();
                    fareBreakDown[j].PassengerType = itinerary.Passenger[i].Type;
                    fareBreakDown[j].PassengerCount = 1;
                    fareBreakDown[j].BaseFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare);
                    fareBreakDown[j].TotalFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax);
                    fareBreakDown[j].SellingFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax - itinerary.Passenger[i].Price.AgentCommission - itinerary.Passenger[i].Price.AgentPLB);
                    fareBreakDown[j].AdditionalTxnFee = Convert.ToDecimal(itinerary.Passenger[i].Price.AdditionalTxnFee);
                    otherCharges[j] = itinerary.Passenger[i].Price.OtherCharges;
                    //fareBreakDown[j].BaseFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare - itinerary.Passenger[i].Price.AgentCommission - itinerary.Passenger[i].Price.AgentPLB);
                    //fareBreakDown[j].TotalFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare - itinerary.Passenger[i].Price.AgentCommission - itinerary.Passenger[i].Price.AgentPLB + itinerary.Passenger[i].Price.Tax);
                }
                else
                {
                    fareBreakDown[j].PassengerCount++;
                    fareBreakDown[j].BaseFare += Convert.ToDouble(itinerary.Passenger[i].Price.PublishedFare);
                    fareBreakDown[j].TotalFare += Convert.ToDouble(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax);
                    fareBreakDown[j].AdditionalTxnFee += Convert.ToDecimal(itinerary.Passenger[i].Price.AdditionalTxnFee);
                    otherCharges[j] = itinerary.Passenger[i].Price.OtherCharges;
                }
            }
            if (booking.Status == BookingStatus.InProgress || booking.Status == BookingStatus.Hold || booking.Status == BookingStatus.Ready)
            {
                btnCreateTicket.Visible = true;
                if (itinerary.LastTicketDate != null && itinerary.LastTicketDate != DateTime.MinValue)
                {
                    lastTicketingDate.Visible = true;
                    lastTicketingDateValue.Visible = true;
                    lastTicketingDateValue.Text = itinerary.LastTicketDate.ToString("dd-MMM-yyy HH:MM");
                }
            }
            else
            {
                btnCreateTicket.Visible = false;
                lastTicketingDateValue.Visible = false;
                lastTicketingDateValue.Visible = false;
            }
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (!itinerary.Segments[i].ETicketEligible)
                {
                    eTicketEligible = false;
                    break;
                }
            }

            BindUploadedDocs();
        }

        if (!IsPostBack)
        {
            Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
                        
            if (Session["SeatError"] != null && !string.IsNullOrEmpty((string)Session["SeatError"]))
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "javascript:Showseatalert('" + (string)Session["SeatError"] + "');", true);

            Session["SeatError"] = null;
        }


        if (Request.QueryString["bookingId"] != null)
        {
            amountToCompare = Settings.LoginInfo.AgentBalance;
            //TODO Ziyad
            Comment comment = new Comment();
            //If Reply to a comment
            if (Request.QueryString["replyCommentID"] != null && Request.QueryString["replyCommentID"] != "")
            {
                comment.CommentDescription = Request.QueryString["CommentDetail"].Trim();
                comment.CommentId = Convert.ToInt32(Request.QueryString["replyCommentID"]);
                comment.LastModifiedBy = Convert.ToInt32(Session["memberId"]);
                comment.ParentId = Convert.ToInt32(Request.QueryString["bookingId"]);
                comment.CommentTypes = CommmentType.Booking;
                comment.CreatedBy = Convert.ToInt32(Session["memberId"]);
                comment.LastModifiedBy = Convert.ToInt32(Session["memberId"]);
                comment.IsPublic = true;
                comment.AgencyId = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId);
                comment.ParentCommentId = Convert.ToInt32(Request.QueryString["replyCommentID"]);

                //Booking History
                BookingHistory bookingHistory = new BookingHistory();
                bookingHistory.BookingId = Convert.ToInt32(Request.QueryString["bookingId"]);
                bookingHistory.EventCategory = EventCategory.Miscellaneous;
                bookingHistory.Remarks = "'" + Request.QueryString["CommentDetail"].Trim() + "' Commment is added by " + Session["firstName"].ToString() + " " + Session["lastName"];
                bookingHistory.CreatedBy = Convert.ToInt32(Session["memberId"]);
                try
                {
                    //comment.AddComment();
                    bookingHistory.Save();
                }
                catch (ArgumentException excep)
                {
                    error = true;
                    errorMsg = "Error:" + excep.Message;
                }
                displayComment = "block";
            }
            //If Edit any Comment
            else if (Request.QueryString["editedCommentid"] != null && Request.QueryString["editedCommentid"] != "")
            {
                comment.CommentDescription = Request.QueryString["CommentDetail"].Trim();
                comment.CommentId = Convert.ToInt32(Request.QueryString["editedCommentid"]);
                comment.LastModifiedBy = Convert.ToInt32(Session["memberId"]);
                try
                {
                    Comment.EditComment(Convert.ToInt32(Request.QueryString["editedCommentid"]), Request.QueryString["CommentDetail"].ToString(), Convert.ToInt32(Session["memberId"]));
                }
                catch (ArgumentException excep)
                {
                    error = true;
                    errorMsg = "Error:" + excep.Message;
                }
                displayComment = "block";
            }
            //Load All Comment
            try
            {
                CommentData = comment.GetComments(CommmentType.Booking, bookingId, (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId));
                //SSRData = comment.GetComments(CommmentType.UpdateSSR, bookingId, Convert.ToInt32(Session["agencyId"]));
                SSR ssr = new SSR();
                SSRData = SSR.GetList(itinerary.FlightId);

                dlSSR.DataSource = SSRData;
                dlSSR.DataBind();
            }
            catch (Exception excep)
            {
                error = true;
                errorMsg = "Error:" + excep.Message;
            }
        }

        if (itinerary != null && (itinerary.FlightBookingSource == BookingSource.SpiceJet) || (itinerary.FlightBookingSource == BookingSource.Indigo) || (itinerary.FlightBookingSource == BookingSource.Paramount) || (itinerary.FlightBookingSource == BookingSource.AirDeccan) || (itinerary.FlightBookingSource == BookingSource.Mdlr)  || (itinerary.FlightBookingSource == BookingSource.GoAir) || (itinerary.FlightBookingSource == BookingSource.Sama) || (itinerary.FlightBookingSource == BookingSource.HermesAirLine) || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
        {
            islcc = true;
        }
        location = new LocationMaster(itinerary.LocationId);
        UserMaster user = new UserMaster(itinerary.CreatedBy);
        lblBookedBy.Text = user.FirstName + " " + user.LastName;
        lblBookingAgent.Text = agency.Name;//new AgentMaster(itinerary.AgencyId).Name;
        lblBookingDate.Text = itinerary.CreatedOn.ToString("dd MMM yyyy");
        lblBookingStatus.Text = booking.Status.ToString();
        lblTravelDate.Text = itinerary.TravelDate.ToString("dd MMM yyyy");
        lblPNR.Text = itinerary.PNR;
        try
        {
            FlightItinerary[] itineraries = new FlightItinerary[1];
            itineraries[0] = itinerary;

            //Commented By Lokesh On 25Sep2019 as we are not using the below datalist to display the flights information.
            //dlTicketDetails.DataSource = itineraries;
            //dlTicketDetails.DataBind();

            dlTicketDetails1.DataSource = itineraries[0].Segments;
            dlTicketDetails1.DataBind();

            dlSaleSummaryDetails.DataSource = itineraries;
            dlSaleSummaryDetails.DataBind();

            if (itinerary.FlightBookingSource != BookingSource.SpiceJet  || itinerary.FlightBookingSource != BookingSource.SpiceJetCorp)
            {
                CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session.SessionID);
                mse.SettingsLoginInfo = Settings.LoginInfo;//Pass the Login info details
                List<FareRule> fareRules = mse.GetFareRule(itinerary);
                itinerary.FareRules = fareRules;
                itineraries[0] = itinerary;
                ddlFareRules.DataSource = itineraries;
                ddlFareRules.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, Ex.ToString(), Request["REMOTE_ADDR"]);
        }

        btnUpdate.OnClientClick = "return UpdateStatus('" + itinerary.PNR + "'," + bookingId + ")";

        bkgHistory = BookingHistory.GetBookingHistory(bookingId);

        dlBookingHistory.DataSource = bkgHistory;
        dlBookingHistory.DataBind();
        btnCreateTicket.Attributes.Add("onclick", "return Loading('" + bookingId + "');");
        //btnCreateTicket.OnClientClick = "window.location.href='CreateTicket.aspx?bookingId=" + bookingId + "'";

        //Complete additional corporate bookings
        if (Settings.LoginInfo.IsCorporate == "Y" && string.IsNullOrEmpty(Request.QueryString["pageFrom"]))
        {
            string ticketing = "No";
            if (Request.QueryString["ticketing"] != null)
                ticketing = Request.QueryString["ticketing"];
            // For corporate, booking additional 2 more options based on some conditions
            //Response.Redirect("CorporateTripSummary.aspx?flightId=" + itinerary.FlightId + "&status=P", true);
            Response.Redirect("CorporateTripSummary.aspx?flightId=" + itinerary.FlightId + "&TripId=" + itinerary.TripId + "&ticketing=" + ticketing, true);
            //Thread CorpThread = new Thread(new ParameterizedThreadStart (DoCorporateBookings));
            //CorpThread.Start(Settings.LoginInfo);            
        }

        try
        {
            if (itinerary != null && !string.IsNullOrEmpty(itinerary.TripId))
            {
                GetCorpTripApprovalStatus();
            }
        }
        catch { }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

    /// <summary>
    /// Add Comment here
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtComments.Text.Trim().Length > 0)
        {
            lblError.Text = "";
            Comment comment = new Comment();
            comment.CommentDescription = txtComments.Text.Trim();
            comment.ParentId = Convert.ToInt32(Request.QueryString["bookingId"]);
            comment.CommentTypes = CommmentType.Booking;
            comment.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            comment.LastModifiedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            if (Convert.ToInt32(Settings.LoginInfo.AgentId) != 0)
            {
                comment.IsPublic = true;
            }
            //else if (rbtnPublic.Checked)
            //{
            //    comment.IsPublic = true;
            //}
            else
            {
                comment.IsPublic = false;
            }
            comment.ParentCommentId = 0;
            comment.AgencyId = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId);

            //Booking History
            BookingHistory bookingHistory = new BookingHistory();
            bookingHistory.BookingId = Convert.ToInt32(Request.QueryString["bookingId"]);
            bookingHistory.EventCategory = EventCategory.Miscellaneous;
            bookingHistory.Remarks = "\'" + txtComments.Text.Trim() + "\' Commment is added by " + Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName;
            bookingHistory.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);

            try
            {
                //Check For Not Inserting Duplicate Data
                if (Session["update"].ToString() == ViewState["update"].ToString())
                {
                    comment.AddComment();
                    bookingHistory.Save();
                    //Load Comment
                    CommentData = comment.GetComments(CommmentType.Booking, bookingId, (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId));
                    Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
                }
                else
                {
                    txtComments.Text = "";
                    txtComments.Focus();
                    return;
                }
            }
            catch (ArgumentException excep)
            {
                error = true;
                errorMsg = "Error:" + excep.Message;
            }

            //Added by brahmam    
            bkgHistory = BookingHistory.GetBookingHistory(bookingId);
            dlBookingHistory.DataSource = bkgHistory;
            dlBookingHistory.DataBind();

            txtComments.Text = "";
            txtComments.Focus();
            //rbtnPublic.Checked = true;
            displayComment = "block";
            spantext = "Hide Comments";
        }
        else
        {
            lblError.Text = "Enter Comments";
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Session["update"];
    }

    protected void ddlFareRules_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            FlightItinerary obj = e.Item.DataItem as FlightItinerary;

            if (obj != null)
            {
                Label lblFareType = e.Item.FindControl("lblFareType") as Label;
                Label lblFareRuleKey1 = e.Item.FindControl("lblFareRuleKey1") as Label;
                Label lblFareRuleDetail1 = e.Item.FindControl("lblFareRuleDetail1") as Label;

                Label lblFareRuleKey2 = e.Item.FindControl("lblFareRuleKey2") as Label;
                Label lblFareRuleDetail2 = e.Item.FindControl("lblFareRuleDetail2") as Label;

                Label lblFareRuleKey3 = e.Item.FindControl("lblFareRuleKey3") as Label;
                Label lblFareRuleDetail3 = e.Item.FindControl("lblFareRuleDetail3") as Label;

                Label lblFareRuleKey4 = e.Item.FindControl("lblFareRuleKey4") as Label;
                Label lblFareRuleDetail4 = e.Item.FindControl("lblFareRuleDetail4") as Label;

                //lblFareType.Text = obj.FareType;
                for (int j = 0; j < obj.FareRules.Count; j++)
                {
                    switch (j)
                    {
                        case 0:
                            lblFareRuleKey1.Text = obj.FareRules[j].FareRuleKeyValue;
                            lblFareRuleDetail1.Text = obj.FareRules[j].FareRuleDetail;
                            break;
                        case 1:
                            lblFareRuleKey2.Text = obj.FareRules[j].FareRuleKeyValue;
                            lblFareRuleDetail2.Text = obj.FareRules[j].FareRuleDetail;
                            break;
                        case 2:
                            lblFareRuleKey3.Text = obj.FareRules[j].FareRuleKeyValue;
                            lblFareRuleDetail3.Text = obj.FareRules[j].FareRuleDetail;
                            break;
                        case 3:
                            lblFareRuleKey4.Text = obj.FareRules[j].FareRuleKeyValue;
                            lblFareRuleDetail4.Text = obj.FareRules[j].FareRuleDetail;
                            break;
                    }
                }


            }
        }
    }

    protected void dlTicketDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            FlightItinerary obj = e.Item.DataItem as FlightItinerary;

            if (obj != null)
            {
                //Label lblOnCarrierName = e.Item.FindControl("lblOnCarrierName") as Label;
                Label lblOnOrigin = e.Item.FindControl("lblOnOrigin") as Label;
                Label lblOnDepartureDate = e.Item.FindControl("lblOnDepartureDate") as Label;
                Label lblOnDestination = e.Item.FindControl("lblOnDestination") as Label;
                Label lblOnArrivalDate = e.Item.FindControl("lblOnArrivalDate") as Label;
                Label lblOnOrigin1 = e.Item.FindControl("lblOnOrigin1") as Label;
                Label lblOnDepartureDate1 = e.Item.FindControl("lblOnDepartureDate1") as Label;
                Label lblOnDestination1 = e.Item.FindControl("lblOnDestination1") as Label;
                Label lblOnArrivalDate1 = e.Item.FindControl("lblOnArrivalDate1") as Label;

                Label lblOnOrigin2 = e.Item.FindControl("lblOnOrigin2") as Label;
                Label lblOnDepartureDate2 = e.Item.FindControl("lblOnDepartureDate2") as Label;
                Label lblOnDestination2 = e.Item.FindControl("lblOnDestination2") as Label;
                Label lblOnArrivalDate2 = e.Item.FindControl("lblOnArrivalDate2") as Label;

                Label lblOnWayType = e.Item.FindControl("lblOnWayType") as Label;
                Label lblOnWaytype2 = e.Item.FindControl("lblOnWaytype2") as Label;
                Label lblOnWaytype1 = e.Item.FindControl("lblOnWaytype1") as Label;
                Label lblOnDuration = e.Item.FindControl("lblOnDuration") as Label;
                Label lblOnDuration1 = e.Item.FindControl("lblOnDuration1") as Label;
                Label lblOnDuration2 = e.Item.FindControl("lblOnDuration2") as Label;


                Label lblRetCarrierName = e.Item.FindControl("lblRetCarrierName") as Label;
                Label lblRetOrigin3 = e.Item.FindControl("lblRetOrigin3") as Label;
                Label lblRetDepartureDate3 = e.Item.FindControl("lblRetDepartureDate3") as Label;
                Label lblRetDestination3 = e.Item.FindControl("lblRetDestination3") as Label;   //lblRetArrival3
                Label lblRetArrivalDate3 = e.Item.FindControl("lblRetArrivalDate3") as Label;
                Label lblRetOrigin1 = e.Item.FindControl("lblRetOrigin1") as Label;
                Label lblRetDepartureDate1 = e.Item.FindControl("lblRetDepartureDate1") as Label;
                Label lblRetDestination1 = e.Item.FindControl("lblRetDestination1") as Label;
                Label lblRetArrivalDate1 = e.Item.FindControl("lblRetArrivalDate1") as Label;

                Label lblRetOrigin2 = e.Item.FindControl("lblRetOrigin2") as Label;
                Label lblRetDepartureDate2 = e.Item.FindControl("lblRetDepartureDate2") as Label;
                Label lblRetDestination2 = e.Item.FindControl("lblRetDestination2") as Label;
                Label lblRetArrivalDate2 = e.Item.FindControl("lblRetArrivalDate2") as Label;

                Label lblRetWayType1 = e.Item.FindControl("lblRetWayType1") as Label;
                Label lblRetWaytype2 = e.Item.FindControl("lblRetWaytype2") as Label;
                Label lblRetWaytype3 = e.Item.FindControl("lblRetWaytype3") as Label;
                Label lblRetDuration3 = e.Item.FindControl("lblRetDuration3") as Label;
                Label lblRetDuration1 = e.Item.FindControl("lblRetDuration1") as Label;
                Label lblRetDuration2 = e.Item.FindControl("lblRetDuration2") as Label;

                Image imgOnCarrier1 = e.Item.FindControl("imgOnCarrier1") as Image;
                Label lblOnFlightCode = e.Item.FindControl("lblOnFlightCode") as Label;
                Image imgRetCarrier1 = e.Item.FindControl("imgRetCarrier1") as Image;
                Label lblRetFlightCode1 = e.Item.FindControl("lblRetFlightCode1") as Label;

                Label lblOnFlightCode1 = e.Item.FindControl("lblOnFlightCode1") as Label;
                Label lblOnFlightCode2 = e.Item.FindControl("lblOnFlightCode2") as Label;
                Label lblRetFlightCode2 = e.Item.FindControl("lblRetFlightCode2") as Label;
                Label lblRetFlightCode3 = e.Item.FindControl("lblRetFlightCode3") as Label;

                Image imgOnCarrier2 = e.Item.FindControl("imgOnCarrier2") as Image;
                Image imgOnCarrier3 = e.Item.FindControl("imgOnCarrier3") as Image;
                Image imgRetCarrier2 = e.Item.FindControl("imgRetCarrier2") as Image;
                Image imgRetCarrier3 = e.Item.FindControl("imgRetCarrier3") as Image;

                HtmlTableRow OnStopArr1 = e.Item.FindControl("OnStopArr1") as HtmlTableRow;
                HtmlTableRow OnStopDep1 = e.Item.FindControl("OnStopDep1") as HtmlTableRow;
                HtmlTableRow OnStopTime1 = e.Item.FindControl("OnStopTime1") as HtmlTableRow;

                HtmlTableRow OnStopArr2 = e.Item.FindControl("OnStopArr2") as HtmlTableRow;
                HtmlTableRow OnStopDep2 = e.Item.FindControl("OnStopDep2") as HtmlTableRow;
                HtmlTableRow OnStopTime2 = e.Item.FindControl("OnStopTime2") as HtmlTableRow;

                HtmlTableRow RetStopArr1 = e.Item.FindControl("RetStopArr1") as HtmlTableRow;
                HtmlTableRow RetStopDep1 = e.Item.FindControl("RetStopDep1") as HtmlTableRow;
                HtmlTableRow RetStopTime1 = e.Item.FindControl("RetStopTime1") as HtmlTableRow;

                HtmlTableRow RetStopArr2 = e.Item.FindControl("RetStopArr2") as HtmlTableRow;
                HtmlTableRow RetStopDep2 = e.Item.FindControl("RetStopDep2") as HtmlTableRow;
                HtmlTableRow RetStopTime2 = e.Item.FindControl("RetStopTime2") as HtmlTableRow;


                Label lblBaseFare = e.Item.FindControl("lblBaseFare") as Label;
                Label lblTax = e.Item.FindControl("lblTax") as Label;
                Label lblServiceFee = e.Item.FindControl("lblServiceFee") as Label;
                Label lblAdultCount = e.Item.FindControl("lblAdultCount") as Label;
                Label lblAdultCount1 = e.Item.FindControl("lblAdultCount1") as Label;
                Label lblTotalForAdult = e.Item.FindControl("lblTotalForAdult") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
                Label lblVAT = e.Item.FindControl("lblVAT") as Label;                

                bool isReturn = false;

                if (itinerary.Origin == obj.Segments[obj.Segments.Length - 1].Destination.CityCode)
                {
                    isReturn = true;      //Checking oneway or twoway 
                }

                for (int i = 0; i < obj.Segments.Length; i++)
                {
                    string rootFolder = Airline.logoDirectory + "/";
                    if (i == 0)
                    {
                        for (int j = 0; j < obj.Segments[i].Stops + 1; j++)
                        {
                            Airline departingAirline = new Airline();
                            departingAirline.Load(obj.Segments[j].Airline);
                            if (!isReturn)
                            {
                                obj.Segments[j].Stops = obj.Segments.Length - 1;
                            }
                            switch (j)
                            {

                                case 0://non stop
                                    imgOnCarrier1.ImageUrl = rootFolder + departingAirline.LogoFile;
                                    lblOnFlightCode.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.Segments[j].FlightNumber;
                                    lblOnOrigin.Text = obj.Segments[j].Origin.CityName;
                                    lblOnDestination.Text = obj.Segments[j].Destination.CityName;
                                    lblOnDepartureDate.Text = obj.Segments[j].DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                                    lblOnArrivalDate.Text = obj.Segments[j].ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                                    lblOnDuration.Text = obj.Segments[j].Duration.Hours + "hrs " + obj.Segments[j].Duration.Minutes + "mins";
                                    hdnStop.Value = obj.Segments[j].Stops.ToString();
                                    switch (obj.Segments[j].Stops)
                                    {
                                        case 0:
                                            lblOnWayType.Text = "Non Stop";
                                            break;
                                        case 1:
                                            lblOnWayType.Text = "Single Stop";
                                            break;
                                        case 2:
                                            lblOnWayType.Text = "Two Stops";
                                            break;
                                    }
                                    lblOnWayType.Text += "<br/>" + obj.Segments[j].SegmentFareType;
                                    break;
                                case 1://one stops
                                    OnStopArr1.Visible = true;
                                    OnStopDep1.Visible = true;
                                    OnStopTime1.Visible = true;
                                    imgOnCarrier2.ImageUrl = rootFolder + departingAirline.LogoFile;
                                    lblOnFlightCode1.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.Segments[j].FlightNumber;
                                    lblOnOrigin1.Text = obj.Segments[j].Origin.CityName;
                                    lblOnDestination1.Text = obj.Segments[j].Destination.CityName;
                                    lblOnDepartureDate1.Text = obj.Segments[j].DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                                    lblOnArrivalDate1.Text = obj.Segments[j].ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                                    lblOnDuration1.Text = obj.Segments[j].Duration.Hours + "hrs " + obj.Segments[j].Duration.Minutes + "mins";
                                    switch (obj.Segments[j].Stops)
                                    {
                                        case 0:
                                            lblOnWaytype1.Text = "Non Stop";
                                            break;
                                        case 1:
                                            lblOnWaytype1.Text = "Single Stop";
                                            break;
                                        case 2:
                                            lblOnWaytype1.Text = "Two Stops";
                                            break;
                                    }
                                    lblOnWaytype1.Text += "<br/>" + obj.Segments[j].SegmentFareType;
                                    break;
                                case 2://Two stops
                                    OnStopArr2.Visible = true;
                                    OnStopDep2.Visible = true;
                                    OnStopTime2.Visible = true;
                                    imgOnCarrier3.ImageUrl = rootFolder + departingAirline.LogoFile;
                                    lblOnFlightCode2.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.Segments[j].FlightNumber;
                                    lblOnOrigin2.Text = obj.Segments[j].Origin.CityName;
                                    lblOnDestination2.Text = obj.Segments[j].Destination.CityName;
                                    lblOnDepartureDate2.Text = obj.Segments[j].DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                                    lblOnArrivalDate2.Text = obj.Segments[j].ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");

                                    lblOnDuration2.Text = obj.Segments[j].Duration.Hours + "hrs " + obj.Segments[j].Duration.Minutes + "mins";
                                    switch (obj.Segments[j].Stops)
                                    {
                                        case 0:
                                            lblOnWaytype2.Text = "Non Stop";
                                            break;
                                        case 1:
                                            lblOnWaytype2.Text = "Single Stop";
                                            break;
                                        case 2:
                                            lblOnWaytype2.Text = "Two Stops";
                                            break;
                                    }
                                    lblOnWaytype2.Text += "<br/>" + obj.Segments[j].SegmentFareType;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (i == Utility.ToInteger(hdnStop.Value) + 1)
                        {
                            for (int j = 0; j < obj.Segments[i].Stops + 1; j++)
                            {
                                Airline departingAirline = new Airline();
                                departingAirline.Load(obj.Segments[i + j].Airline);
                                switch (j)
                                {
                                    case 0://Non Stop
                                        imgRetCarrier1.ImageUrl = rootFolder + departingAirline.LogoFile;
                                        lblRetFlightCode1.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.Segments[j + i].FlightNumber;
                                        lblRetOrigin1.Text = obj.Segments[j + i].Origin.CityName;
                                        lblRetDestination1.Text = obj.Segments[j + i].Destination.CityName;
                                        lblRetDepartureDate1.Text = obj.Segments[j + i].DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                                        lblRetArrivalDate1.Text = obj.Segments[j + i].ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                                        switch (obj.Segments[j + i].Stops)
                                        {
                                            case 0:
                                                lblRetWayType1.Text = "Non Stop";
                                                break;
                                            case 1:
                                                lblRetWayType1.Text = "Single Stop";
                                                break;
                                            case 2:
                                                lblRetWayType1.Text = "Two Stops";
                                                break;
                                        }
                                        lblRetWayType1.Text += "<br/>" + obj.Segments[j + i].SegmentFareType;
                                        //TimeSpan duration = obj.Segments[j+i].ArrivalTime.Subtract(obj.Segments[j+i].DepartureTime);
                                        lblRetDuration1.Text = obj.Segments[j + i].Duration.Hours + "hrs " + obj.Segments[j + i].Duration.Minutes + "mins";
                                        break;
                                    case 1://Single Stop
                                        RetStopArr1.Visible = true;
                                        RetStopDep1.Visible = true;
                                        RetStopTime1.Visible = true;
                                        imgRetCarrier2.ImageUrl = rootFolder + departingAirline.LogoFile;
                                        lblRetFlightCode2.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.Segments[j + i].FlightNumber;
                                        lblRetOrigin2.Text = obj.Segments[j + i].Origin.CityName;
                                        lblRetDestination2.Text = obj.Segments[j + i].Destination.CityName;
                                        lblRetDepartureDate2.Text = obj.Segments[j + i].DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                                        lblRetArrivalDate2.Text = obj.Segments[j + i].ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                                        lblRetDuration2.Text = obj.Segments[j + i].Duration.Hours + "hrs " + obj.Segments[j + i].Duration.Minutes + "mins";
                                        switch (obj.Segments[i + j].Stops)
                                        {
                                            case 0:
                                                lblRetWaytype2.Text = "Non Stop";
                                                break;
                                            case 1:
                                                lblRetWaytype2.Text = "Single Stop";
                                                break;
                                            case 2:
                                                lblRetWaytype2.Text = "Two Stops";
                                                break;
                                        }
                                        lblRetWaytype2.Text += "<br/>" + obj.Segments[j + i].SegmentFareType;
                                        break;
                                    case 2://Two Stops
                                        RetStopArr2.Visible = true;
                                        RetStopDep2.Visible = true;
                                        RetStopTime2.Visible = true;
                                        imgRetCarrier3.ImageUrl = rootFolder + departingAirline.LogoFile;
                                        lblRetFlightCode3.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.Segments[j + i].FlightNumber;
                                        lblRetOrigin3.Text = obj.Segments[i + j].Origin.CityName;
                                        lblRetDestination3.Text = obj.Segments[i + j].Destination.CityName;
                                        lblRetDepartureDate3.Text = obj.Segments[i + j].DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                                        lblRetArrivalDate3.Text = obj.Segments[i + j].ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                                        lblRetDuration3.Text = obj.Segments[i + j].Duration.Hours + "hrs " + obj.Segments[i + j].Duration.Minutes + "mins";
                                        switch (obj.Segments[i + j].Stops)
                                        {
                                            case 0:
                                                lblRetWaytype3.Text = "Non Stop";
                                                break;
                                            case 1:
                                                lblRetWaytype3.Text = "Single Stop";
                                                break;
                                            case 2:
                                                lblRetWaytype3.Text = "Two Stops";
                                                break;
                                        }
                                        lblRetWaytype3.Text += "<br/>" + obj.Segments[j + i].SegmentFareType;
                                        break;
                                }
                            }
                        }
                    }
                }


                //FlightPassenger pax;
                decimal Tax = 0;
                decimal BaseFare = 0;
                decimal AsvAmount = 0;
                decimal baggage = 0, outputVAT = 0;
                bool isBasefare = false;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {

                    if (obj.FareType.ToUpper() == "PUB")
                    {
                        BaseFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                        //lblTotalForAdult.Text = (pax.Price.PublishedFare + pax.Price.SeviceTax + pax.Price.Tax).ToString("0.000");
                        //lblTotal.Text = lblTotalForAdult.Text;
                    }
                    else
                    {
                        BaseFare += pax.Price.NetFare;
                        //lblTotalForAdult.Text = (pax.Price.NetFare + pax.Price.Markup + pax.Price.SeviceTax + pax.Price.Tax).ToString("0.000");
                        //lblTotal.Text = lblTotalForAdult.Text;
                    }
                    Tax += pax.Price.Tax + pax.Price.Markup;
                    
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        Tax += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                    }
                    AsvAmount += pax.Price.AsvAmount;
                    if (pax.Price.AsvElement == "BF")
                    {
                        isBasefare = true;
                    }
                    else
                    {
                        isBasefare = false;
                    }
                    baggage += pax.Price.BaggageCharge;
                    outputVAT += pax.Price.OutputVATAmount;
                    lblServiceFee.Text = pax.Price.SeviceTax.ToString("N" + agency.DecimalValue);
                }
                if (isBasefare)
                {
                    BaseFare += AsvAmount;
                }
                else
                {
                    Tax += AsvAmount;
                }
                lblBaseFare.Text = BaseFare.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblTax.Text = Tax.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblBaggage.Text = baggage.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                //lblAdultCount.Text = string.Empty;
                //lblAdultCount1.Text = string.Empty;
                lblVAT.Text = outputVAT.ToString("N" + agency.DecimalValue);
                if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                {
                    lblTotal.Text = Math.Ceiling(BaseFare + Tax + baggage + outputVAT).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                }
                else
                {
                    lblTotal.Text = (BaseFare + Tax + baggage + outputVAT).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                }
            }
        }
    }

    protected void dlBookingHistory_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            BookingHistory history = e.Item.DataItem as BookingHistory;

            Label lblModifiedDate = e.Item.FindControl("lblModifiedDate") as Label;
            Label lblModifiedTime = e.Item.FindControl("lblModifiedTime") as Label;
            Label lblRemarks = e.Item.FindControl("lblRemarks") as Label;
            Label lblModifiedBy = e.Item.FindControl("lblModifiedBy") as Label;

            if (history != null)
            {
                lblModifiedDate.Text = history.LastModifiedOn.ToString("dd MMM yyyy");
                lblModifiedTime.Text = history.LastModifiedOn.ToString("HH:mm tt");
                lblRemarks.Text = history.Remarks;

                UserMaster user = new UserMaster(history.LastModifiedBy);
                lblModifiedBy.Text = user.FirstName + " " + user.LastName;
            }
        }
    }

    protected void dlSSRInfo_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            SSR ssrObj = e.Item.DataItem as SSR;

            if (ssrObj != null)
            {
                Label lblLeadPaxName = e.Item.FindControl("lblLeadPaxName") as Label;
                Label lblSSRCode = e.Item.FindControl("lblSSRCode") as Label;
                Label lblSSRStatus = e.Item.FindControl("lblSSRStatus") as Label;
                Label lblSSRDetail = e.Item.FindControl("lblSSRDetail") as Label;
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;


                lblLeadPaxName.Text = FlightPassenger.GetPaxFullName(ssrObj.PaxId);
                lblSSRCode.Text = ssrObj.SsrCode;
                lblSSRDetail.Text = ssrObj.Detail;
                lblSSRStatus.Text = ssrObj.SsrStatus;
                lblStatus.Text = ssrObj.Status.ToString();
            }
        }
    }

    #region Corporate Bookings Block
    /// <summary>
    /// Initiates the optional/additional bookings for corporate profile
    /// </summary>
    protected void DoCorporateBookings(object LoginInfo)
    {
        try
        {
            LoginInfo loginInfo = LoginInfo as LoginInfo;
            if (Session["Result"] != null && Session["FlightRequest"] != null && Session["TripId"] != null && Session["CriteriaItineraries"] != null && Session["sessionId"] != null && Session["CorpBookingResponses"] != null)
            {
                SearchRequest request = null;

                request = Session["FlightRequest"] as SearchRequest;

                FlightItinerary itinerary = null;
                //HOLD remaining TWO criteria results for Corporate profile
                //TODO: Ziya
                if (loginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
                {
                    BookingResponse corpBookingResponse = new BookingResponse();
                    Dictionary<FlightItinerary, BookingResponse> BookingResponses = Session["CorpBookingResponses"] as Dictionary<FlightItinerary, BookingResponse>;

                    foreach (KeyValuePair<FlightItinerary, BookingResponse> pair in BookingResponses)
                    {
                        itinerary = pair.Key;
                        break;
                    }
                    agency = new AgentMaster(loginInfo.AgentId);
                    agencyId = loginInfo.AgentId;

                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());

                    if (Session["CriteriaItineraries"] != null)
                    {
                        List<FlightItinerary> CriteriaItineries = Session["CriteriaItineraries"] as List<FlightItinerary>;
                        //Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                        ////Book other optional bookings simultaneously

                        //listOfThreads.Add("0", new WaitCallback(MakeCorporateBookings));
                        //listOfThreads.Add("1", new WaitCallback(MakeCorporateBookings));
                        //List<AutoResetEvent> Events = new List<AutoResetEvent>();
                        int i = 0;
                        foreach (FlightItinerary fi in CriteriaItineries)
                        {
                            flightItinerary = fi;
                            //Audit.Add(EventType.Exception, Severity.High, 0, "(CorpBooking)Additional Booking Source " + (i + 1) + " : " + flightItinerary.FlightBookingSource.ToString(), "");
                            if (!flightItinerary.IsLCC)
                            {
                                if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                {
                                    //CalculateBaggageFare(flightItinerary, i);
                                }
                                flightItinerary.TripId = Session["TripId"].ToString();//Set the trip id for remaining flights
                                //Audit.Add(EventType.Book, Severity.Normal, (int)loginInfo.UserID, "(CorpBooking)Addtional Booking " + (i + 1) + " trip Id assigned : " + flightItinerary.TripId, "");
                                if (flightItinerary.FlightBookingSource != BookingSource.AirArabia)
                                {
                                    //List<object> Objects = new List<object>();
                                    //Objects.Add(flightItinerary);//Add Itinerary
                                    //Objects.Add(mse);//Add MSE
                                    //Objects.Add(i);//Add ThreadPool Index
                                    //Objects.Add(BookingResponses);                                    
                                    //Objects.Add(loginInfo);
                                    //ThreadPool.QueueUserWorkItem(listOfThreads[i.ToString()], Objects);//Pass Object collection as Paramter to MakeCorporateBookings(...)                                    
                                    //Audit.Add(EventType.Book, Severity.Normal, (int)loginInfo.UserID, "(CorpBooking)Addtional Booking " + (i + 1) + " thread initiated", "");
                                    //AutoResetEvent aevent = new AutoResetEvent(false);
                                    //Events.Add(aevent);
                                    mse.SettingsLoginInfo = loginInfo;
                                    corpBookingResponse = mse.Book(ref flightItinerary, agencyId, (flightItinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(flightItinerary.CreatedBy), true, "");
                                    BookingResponses.Add(flightItinerary, corpBookingResponse);
                                }
                                else
                                {
                                    corpBookingResponse = mse.Book(ref flightItinerary, agencyId, (flightItinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(flightItinerary.CreatedBy), true, "");
                                    BookingResponses.Add(flightItinerary, corpBookingResponse);
                                }
                                i++;//increment counter only when result is not LCC
                            }
                        }

                        //try
                        //{
                        //    if (Events.Count > 0)
                        //    {
                        //        BookingEvents = Events.ToArray();
                        //    }
                        //    if (i != 0 && Events.Count > 0)
                        //    {
                        //        //If any thread takes more than 2 minutes stop the thread
                        //        if (!WaitHandle.WaitAll(BookingEvents, new TimeSpan(0, 1, 0), true))
                        //        {
                        //            //TODO: audit which thread is timed out                
                        //        }
                        //    }
                        //}
                        //catch { }
                    }


                    isBookingSuccess = true;//to send HOLD emails

                    string EmailBody = string.Empty, subject = string.Empty;

                    //Send Corporate Emails for successful bookings
                    foreach (KeyValuePair<FlightItinerary, BookingResponse> pair in BookingResponses)
                    {
                        //If booking is success send email
                        if (pair.Value.Status == BookingResponseStatus.Successful)
                        {
                            //Combine all 3 email bodies in one
                            EmailBody += SendFlightTicketEmail(pair.Key, loginInfo);
                            try
                            {
                                if (pair.Key.FareRules[0].FareRuleDetail != null && itinerary.FlightBookingSource == BookingSource.UAPI)
                                {
                                    //Append the Penalties Fare Rule to the Email Body
                                    EmailBody += "<br/><hr/>Fare Rules :" + pair.Key.FareRules[0].FareRuleDetail.Substring(0, pair.Key.FareRules[0].FareRuleDetail.IndexOf("/p><p") + 3) + "<hr/>";
                                }
                            }
                            catch { }
                            //Combine PNR's
                            if (subject.Length > 0)
                            {
                                subject += "," + pair.Key.PNR;
                            }
                            else
                            {
                                subject = pair.Key.PNR;
                            }
                            emailCounter++;//Increment the counter for hiding the Agent details in Email body
                        }
                        else
                        {
                            //TODO: Failure email
                        }
                    }

                    try
                    {
                        List<string> toArray = new List<string>();
                        toArray.Add(itinerary.Passenger[0].Email);
                        if (!string.IsNullOrEmpty(loginInfo.AgentEmail))
                        {
                            toArray.Add(loginInfo.AgentEmail);
                        }
                        if (!string.IsNullOrEmpty(loginInfo.Email))
                        {
                            toArray.Add(loginInfo.Email);
                        }

                        //Send Corporate email
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Booking Confirmation - " + subject, EmailBody, new Hashtable());
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, 1, "(CorpBooking)Failed to Send Corporate Emails. Reason : " + ex.ToString(), "");
                    }
                    finally
                    {
                        Session["CorpBookingResponses"] = null;
                        Session["CriteriaItineraries"] = null;
                        Session["FlightItinerary"] = null;
                        Session["TripId"] = null;
                        Session.Remove("CriteriaResults");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Book, Severity.High, 1, "(CorpBooking)Failed to book Cheapest & Shortest Flights. Reason : " + ex.ToString(), "");
        }
    }

    /// <summary>
    /// Retrieves Baggage fare for Air Arabia
    /// </summary>
    /// <param name="itinerary"></param>
    /// <param name="index"></param>
    protected void CalculateBaggageFare(FlightItinerary itinerary, int index)
    {
        try
        {
            SearchResult result = (Session["CriteriaResults"] as List<SearchResult>)[index];
            string sessionId = Session["sessionId"].ToString();
            SearchRequest request = Session["FlightRequest"] as SearchRequest;
            if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                //Hold booking is not allowed for G9

                string[] RPH = new string[result.Flights.Length];
                List<string> rph = new List<string>();
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    //for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        //RPH[i] = result.Flights[i][j].FareInfoKey.ToString() + "|" + result.Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber;
                        rph.Add(itinerary.Segments[i].UapiDepartureTime.ToString() + "|" + itinerary.Segments[i].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + itinerary.Segments[i].Airline + itinerary.Segments[i].FlightNumber);
                        //rph.Add(result.Flights[i][j].UapiDepartureTime.ToString());
                    }
                }
                RPH = rph.ToArray();
                int arraySize = 0;

                //if (itinerary.Segments.Length <= 1)
                //{
                //    arraySize = (itinerary.Passenger.Length - request.InfantCount) ;
                //}
                //else
                {
                    arraySize = (itinerary.Passenger.Length - request.InfantCount) * itinerary.Segments.Length;
                }

                List<string> paxBaggages = new List<string>();
                

                for (int i = 0; i < (itinerary.Passenger.Length - request.InfantCount); i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        string travelerRPH = "";
                        if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                        {
                            travelerRPH = "A" + (i + 1);
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            travelerRPH = "C" + (i + 1);
                        }

                        string bagCode = itinerary.Passenger[i].BaggageCode;
                        string mealCode = itinerary.Passenger[i].MealType;
                        string seatCode = itinerary.Passenger[i].SeatInfo;
                        for (int k = 0; k < result.Flights.Length; k++)
                        {
                            for (int j = 0; j < result.Flights[k].Length; j++)
                            {
                                if (!string.IsNullOrEmpty(bagCode) && bagCode.Split(',').Length>k)
                                    paxBaggages.Add(bagCode.Split(',')[k].Trim() + "-BAG" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                if (!string.IsNullOrEmpty(mealCode) && mealCode.Split(',').Length > k)
                                    paxBaggages.Add(mealCode.Split(',')[k].Trim() + "-MEAL" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                if (!string.IsNullOrEmpty(seatCode) && seatCode.Split('|').Length > k)
                                    paxBaggages.Add(seatCode.Split('|')[k].Trim().Split('(')[0] + "-SEAT" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                            }
                        }
                    }
                }

                CT.BookingEngine.GDS.AirArabia aaObj = new CT.BookingEngine.GDS.AirArabia();
                //if (Settings.LoginInfo.IsOnBehalfOfAgent)
                //{
                //    aaObj.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                //    aaObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                //    aaObj.Code = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                //    aaObj.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                //    aaObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                //}
                //else
                //{
                //    aaObj.UserName = Settings.LoginInfo.AgentSourceCredentials["G9"].UserID;
                //    aaObj.Password = Settings.LoginInfo.AgentSourceCredentials["G9"].Password;
                //    aaObj.Code = Settings.LoginInfo.AgentSourceCredentials["G9"].HAP;
                //    aaObj.AgentCurrency = Settings.LoginInfo.Currency;
                //    aaObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                //}
                CT.BookingEngine.GDS.AirArabia.SessionData data = (CT.BookingEngine.GDS.AirArabia.SessionData)Basket.BookingSession[sessionId][result.Airline];
                string fareXml = "";
                System.Net.CookieContainer cookie = new System.Net.CookieContainer();
                //aaObj.AppUserId = Settings.LoginInfo.UserID.ToString();
                aaObj.SessionID = Session["sessionId"].ToString();
                Dictionary<string, decimal> PaxBagFares = new Dictionary<string, decimal>();
                //Modified by Lokesh on 4-April-2018
                //This is for Only G9 Air Source
                //Need to pass the state code(mandatory) and taxRegNo(Not mandatory) only if the customer is travelling from India.
                string[] baggageFare = aaObj.GetBaggageFare(ref result, request, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref paxBaggages, itinerary.Passenger[0].GSTStateCode, itinerary.Passenger[0].GSTTaxRegNo, ref PaxBagFares);
                data.SelectedResultInfo = fareXml;
                // TodO ziya: update itineray according to last price quote
                // foreach (FlightPassenger pax in itinerary.Passenger)
                //  {
                //     for (int i = 0; i < result.FareBreakdown.Length; i++)
                //     {
                //         if (pax.Type == result.FareBreakdown[i].PassengerType && result.FareBreakdown[i].SupplierFare > 0)
                //        {
                //            pax.Price.SupplierPrice = (decimal)result.FareBreakdown[i].SupplierFare / result.FareBreakdown[i].PassengerCount;
                //        }
                //     }
                //  }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorpBooking)Failed to get Baggage price for G9. Reason " + ex.ToString(), "");
        }
    }

    /// <summary>
    /// Makes bookings for optional results in case of Corporate profile
    /// </summary>
    protected void MakeCorporateBookings(object collection)
    {
        FlightItinerary itinerary = null;
        int eventIndex = 0;
        try
        {
            List<object> BookingObjects = collection as List<object>;
            itinerary = BookingObjects[0] as FlightItinerary;

            CT.MetaSearchEngine.MetaSearchEngine mse = BookingObjects[1] as CT.MetaSearchEngine.MetaSearchEngine;
            eventIndex = Convert.ToInt32(BookingObjects[2]);
            mse.SettingsLoginInfo = BookingObjects[4] as LoginInfo;
            //Audit.Add(EventType.Book, Severity.Normal, (int)mse.SettingsLoginInfo.UserID, "(CorpBooking)Addtional Booking " + (eventIndex + 1) + " before going to MetaSearchEngine", "");
            BookingResponse corpBookingResponse = mse.Book(ref itinerary, agencyId, (itinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(itinerary.CreatedBy), true, "10.200.44.26");
            //Audit.Add(EventType.Book, Severity.Normal, (int)mse.SettingsLoginInfo.UserID, "(CorpBooking)Addtional Booking " + (eventIndex + 1) + " after returning from MetaSearchEngine. Booking Status : " + corpBookingResponse.Status.ToString(), "");
            Dictionary<FlightItinerary, BookingResponse> BookingResponses = BookingObjects[3] as Dictionary<FlightItinerary, BookingResponse>;
            //Add the booked response along with Itinerary for sending emails
            BookingResponses.Add(itinerary, corpBookingResponse);

            BookingEvents[eventIndex].Set();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorpBooking)Failed to Book Corporate booking - Source :" + itinerary.FlightBookingSource + ". Reason : " + ex.ToString(), "");
            BookingEvents[eventIndex].Set();
        }
    }

    /// <summary>
    /// Bind the Booking information to the email div id='EmailDiv'
    /// </summary>
    /// <param name="FlightId"></param>
    protected void BindTicketInfo(int FlightId)
    {
        try
        {
            if (FlightId > 0)
            {
                flightItinerary = new FlightItinerary(FlightId);
                agency = new AgentMaster(flightItinerary.AgencyId);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(FlightId));
                if (booking.Status == BookingStatus.Ticketed)
                {
                    ticketList = Ticket.GetTicketList(FlightId);
                }
            }
            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai)
            {
                foreach (FlightPassenger pax in flightItinerary.Passenger)
                {
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                }
            }
            airline = new Airline();
            airline.Load(flightItinerary.ValidatingAirlineCode);

            if (airline.LogoFile.Trim().Length != 0)
            {
                string[] fileExtension = airline.LogoFile.Split('.');
                AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Method to send Corporate booking email
    /// </summary>
    /// <param name="itinerary"></param>
    /// <returns></returns>
    protected string SendFlightTicketEmail(FlightItinerary itinerary, LoginInfo loginInfo)
    {
        string myPageHTML = string.Empty;
        try
        {
            BindTicketInfo(itinerary.FlightId);
            string logoPath = "";
            int agentId = loginInfo.AgentId;

            string serverPath = "";

            //if (Request.Url.Port > 0)
            //{
            //    serverPath = "http://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            //}
            //else
            //{
            //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            //}
            serverPath = Request.Url.Scheme+"://ctb2bstage.cozmotravel.com";
            if (agentId > 1)
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
            }



            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
            myPageHTML = myPageHTML.Replace("none", "block");

            //System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            //toArray.Add(itinerary.Passenger[0].Email);

            string subject = "Ticket Confirmation - " + itinerary.PNR;

            //if (ViewState["MailSent"] == null)//If booking in not corporate then send email
            {
                //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable());
                //ViewState["MailSent"] = true;
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)loginInfo.UserID, "(CorpBooking)Failed to Send Flight Corporate Booking Email PNR " + itinerary.PNR + ": reason - " + ex.ToString(), "");
        }
        return myPageHTML;
    }

    #endregion


    protected void dlTicketDetails1_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            FlightInfo obj = e.Item.DataItem as FlightInfo;

            if (obj != null)
            {
                Image imgFlightCarrierCode = e.Item.FindControl("imgFlightCarrierCode") as Image;
                Label lblFlightOriginCode = e.Item.FindControl("lblFlightOriginCode") as Label;
                Label lblFlightDestinationCode = e.Item.FindControl("lblFlightDestinationCode") as Label;
                Label lblFlightDuration = e.Item.FindControl("lblFlightDuration") as Label;
                Label lblOnFlightWayType = e.Item.FindControl("lblOnFlightWayType") as Label;

                Label lblFlightCode = e.Item.FindControl("lblFlightCode") as Label;
                Label lblFlightDepartureDate = e.Item.FindControl("lblFlightDepartureDate") as Label;
                Label lblFlightArrivalDate = e.Item.FindControl("lblFlightArrivalDate") as Label;



                string rootFolder = Airline.logoDirectory + "/";
                Airline departingAirline = new Airline();
                departingAirline.Load(obj.Airline);
                imgFlightCarrierCode.ImageUrl = rootFolder + departingAirline.LogoFile;
                lblFlightOriginCode.Text = obj.Origin.CityName;
                lblFlightDestinationCode.Text = obj.Destination.CityName;
                lblFlightDuration.Text = obj.Duration.Hours + "hrs " + obj.Duration.Minutes + "mins";

                switch (obj.Stops)
                {
                    case 0:
                        lblOnFlightWayType.Text = "Non Stop";
                        break;
                    case 1:
                        lblOnFlightWayType.Text = "Single Stop";
                        break;
                    case 2:
                        lblOnFlightWayType.Text = "Two Stops";
                        break;
                    default:
                        lblOnFlightWayType.Text = "Two + Stops";
                        break;
                }
                lblOnFlightWayType.Text += "<br/>" + obj.SegmentFareType;
                lblFlightCode.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.FlightNumber;
                lblFlightDepartureDate.Text = obj.DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                lblFlightArrivalDate.Text = obj.ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");

            }
            /* if (dlTicketDetails1.Items.Count > 0)
             {
                 DataTable tbl = ((DataRowView)e.Item.DataItem).Row.Table;
                 if (e.Item.ItemIndex == tbl.Rows.Count - 1)
                 {
                    
                 }
             } */

        }
    }


    protected void dlSaleSummaryDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            FlightItinerary obj = e.Item.DataItem as FlightItinerary;

            if (obj != null)
            {

                Label lblBaseFare = e.Item.FindControl("lblBaseFare") as Label;
                Label lblTax = e.Item.FindControl("lblTax") as Label;
                Label lblServiceFee = e.Item.FindControl("lblServiceFee") as Label;
                Label lblAdultCount = e.Item.FindControl("lblAdultCount") as Label;
                Label lblAdultCount1 = e.Item.FindControl("lblAdultCount1") as Label;
                Label lblTotalForAdult = e.Item.FindControl("lblTotalForAdult") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
                Label lblMeal = e.Item.FindControl("lblMeal") as Label;
                Label lblSeat = e.Item.FindControl("lblSeat") as Label;
                Label lblVAT = e.Item.FindControl("lblVAT") as Label;
                Label lblK3Tax = e.Item.FindControl("lblK3Tax") as Label;
                decimal k3Tax = 0;
                Ticket ticket = new Ticket();
                if(!string.IsNullOrEmpty(obj.RoutingTripId))
                {
                    isOneToOneSearch = true;
                }
                if (booking.Status == BookingStatus.Ticketed)
                {
                    ticketList = Ticket.GetTicketList(itinerary.FlightId);

                    List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                    ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));
                    
                    switch(itinerary.FlightBookingSource)
                    {
                        case BookingSource.GoAir:
                        case BookingSource.GoAirCorp:
                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT") ? t.TaxValue : 0);
                            break;
                        case BookingSource.UAPI:
                        case BookingSource.TBOAir:
                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                            break;
                        case BookingSource.SpiceJet:
                        case BookingSource.SpiceJetCorp:
                        case BookingSource.Indigo:
                        case BookingSource.IndigoCorp:
                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                            break; 
                    }
                }
                k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
                lblK3Tax.Text = k3Tax.ToString("N" + agency.DecimalValue) +" " + agency.AgentCurrency;
                //FlightPassenger pax;
                decimal Tax = 0;
                decimal BaseFare = 0;
                decimal AsvAmount = 0;
                decimal baggage = 0, outputVAT = 0m, inputvat = 0m ,meal=0, seatprice = 0;
                bool isBasefare = false;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {

                    if (obj.FareType.ToUpper() == "PUB")
                    {
                        BaseFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                        //lblTotalForAdult.Text = (pax.Price.PublishedFare + pax.Price.SeviceTax + pax.Price.Tax).ToString("0.000");
                        //lblTotal.Text = lblTotalForAdult.Text;
                    }
                    else
                    {
                        BaseFare += pax.Price.NetFare;
                        //lblTotalForAdult.Text = (pax.Price.NetFare + pax.Price.Markup + pax.Price.SeviceTax + pax.Price.Tax).ToString("0.000");
                        //lblTotal.Text = lblTotalForAdult.Text;
                    }
                    Tax += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;
                    discount += pax.Price.Discount;
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        Tax += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                    }
                    if (itinerary.FlightBookingSource == BookingSource.OffLine)
                    {
                        Tax -= (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                    }
                    AsvAmount += pax.Price.AsvAmount;
                    if (pax.Price.AsvElement == "BF")
                    {
                        isBasefare = true;
                    }
                    else
                    {
                        isBasefare = false;
                    }
                    baggage += pax.Price.BaggageCharge;
                    meal += pax.Price.MealCharge;
                    seatprice += pax.Price.SeatPrice;
                    outputVAT += (pax.Price.OutputVATAmount);

                    lblServiceFee.Text = pax.Price.SeviceTax.ToString("N" + agency.DecimalValue);
                }
                if (isBasefare)
                {
                    BaseFare += AsvAmount;
                }
                else
                {
                    Tax += AsvAmount;
                }
                if(!isOneToOneSearch)
                {
                    k3Tax = 0;
                }
                Tax = Math.Round(Tax, agency.DecimalValue);
                lblBaseFare.Text = BaseFare.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblTax.Text = (Tax-k3Tax).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblBaggage.Text = baggage.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblMeal.Text = meal.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblSeat.Text = seatprice.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                discount = Math.Round(discount, agency.DecimalValue);
                lblVAT.Text = outputVAT.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                BaseFare = Convert.ToDecimal(BaseFare.ToString("N" + agency.DecimalValue));
                outputVAT = Math.Round(outputVAT, agency.DecimalValue);
                //lblAdultCount.Text = string.Empty;
                //lblAdultCount1.Text = string.Empty;
                if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                {
                    lblTotal.Text = Math.Ceiling(BaseFare + Tax + baggage + outputVAT - discount).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                    hdnBookingAmount.Value = Math.Ceiling(BaseFare + Tax + baggage + outputVAT - discount).ToString("N" + agency.DecimalValue);
                }
                else
                {
                    hdnBookingAmount.Value = (BaseFare + Tax + baggage + meal + seatprice + outputVAT - discount).ToString("N" + agency.DecimalValue);
                    lblTotal.Text = (BaseFare + Tax + baggage + meal + seatprice + outputVAT - discount).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                }
            }
        }
    }


    public void GetCorpTripApprovalStatus()
    {
        //added product Id as param to fetch the status.
        corpTripApprovalStatus = FlightPolicy.GetTripApprovalStatus(itinerary.FlightId,(int)ProductType.Flight);
    }

    private void LoadPaymentGateways()
    {
        try
        {
            if (booking.Status == BookingStatus.InProgress || booking.Status == BookingStatus.Hold || booking.Status == BookingStatus.Ready)
            {
                DataTable dtPaymentGateways = AgentMaster.GetAgentPaymentgateways(agencyId, (int)ProductType.Flight);

                if (dtPaymentGateways.Rows.Count > 0)
                {
                    //agentPGCount = dtPaymentGateways.Rows.Count;
                    foreach (DataRow dr in dtPaymentGateways.Rows)
                    {
                        rblAgentPG.Items.Add(new ListItem(Convert.ToString(dr["name"]), Convert.ToString(dr["pgcharge"])));

                    }
                    rblAgentPG.ClearSelection();
                    if (rblAgentPG.Items.Count > 0)
                    {
                        rblAgentPG.Items[0].Selected = true;
                        charges = Convert.ToDecimal(rblAgentPG.Items[0].Value);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to load Agent PaymentGateways " + ". reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

   
    protected void btnUploadFiles_Click(object sender, EventArgs e)
    {
        List<HttpPostedFile> files = new List<HttpPostedFile>();
        if (Session["GSTFiles"] != null)
        {
            try
            {
                files = Session["GSTFiles"] as List<HttpPostedFile>;
                string gstFolderPath = (ConfigurationManager.AppSettings["GSTFolderPath"] != null ? Server.MapPath(ConfigurationManager.AppSettings["GSTFolderPath"]) : Server.MapPath(@"\upload\GSTDocs\"));
                gstFolderPath = Path.Combine(gstFolderPath, "FL_"+itinerary.FlightId.ToString());
                if (!Directory.Exists(gstFolderPath))
                {
                    Directory.CreateDirectory(gstFolderPath);
                }
                
                foreach (HttpPostedFile file in files)
                {
                    string filePath = Path.Combine(gstFolderPath, file.FileName.Replace(" ", "_"));
                    file.SaveAs(filePath);                    
                }
                Session["GSTFiles"] = null;
                BindUploadedDocs();
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(GSTDocs)Failed to Upload GST Docs. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            finally
            {
                Session["GSTFiles"] = null;
                BindUploadedDocs();
            }
        }
    }


    /// <summary>
    /// Method to bind the uploaded documents
    /// </summary>
    void BindUploadedDocs()
    {
        try
        {
            string gstFolderPath = (ConfigurationManager.AppSettings["GSTFolderPath"] != null ? Server.MapPath(ConfigurationManager.AppSettings["GSTFolderPath"]) : Server.MapPath(@"\upload\GSTDocs\"));
            gstFolderPath = Path.Combine(gstFolderPath, "FL_" + itinerary.FlightId.ToString());

            if (Directory.Exists(gstFolderPath))
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(gstFolderPath);
                FileInfo[] fileList = directoryInfo.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                pnlFiles.Controls.Clear();

                if (fileList.Length > 0)
                {
                    for (int i = 0; i < fileList.Length; i++)
                    {
                        FileInfo file = fileList[i];
                        HyperLink link = new HyperLink();
                        link.ID = "hlFile" + i;                        
                        link.NavigateUrl = "DownloadeDoc.aspx?path=" + file.FullName + "&type=" + file.Extension + "&docName=" + file.Name;
                        link.Text = (i + 1) + "." + file.Name;
                        LiteralControl ctrl = new LiteralControl();
                        ctrl.Text = "<br/>";
                        pnlFiles.Controls.Add(link);
                        pnlFiles.Controls.Add(ctrl);
                    }
                    pnlUploadedFiles.Visible = true;
                }
                else
                {
                    pnlUploadedFiles.Visible = false;
                }
            }
            else
            {
                pnlUploadedFiles.Visible = false;
            }
        }
        catch { }
    }

    /// <summary>
    /// Event to download all uploaded documents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbDownloadAll_Click1(object sender, EventArgs e)
    {
        string gstFolderPath = (ConfigurationManager.AppSettings["GSTFolderPath"] != null ? Server.MapPath(ConfigurationManager.AppSettings["GSTFolderPath"]) : Server.MapPath(@"\upload\GSTDocs\"));
        gstFolderPath = Path.Combine(gstFolderPath, "FL_" + itinerary.FlightId.ToString());

        if (Directory.Exists(gstFolderPath))
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(gstFolderPath);
            FileInfo[] fileList = directoryInfo.GetFiles().OrderBy(p => p.CreationTime).ToArray();

            if (fileList.Length > 0)
            {
                try
                {
                    using (ZipFile zipFile = new ZipFile())
                    {
                        zipFile.AlternateEncodingUsage = ZipOption.AsNecessary;
                        foreach (FileInfo file in fileList)
                        {
                            zipFile.AddFile(file.FullName);
                        }

                        Response.Clear();
                        Response.BufferOutput = false;
                        Response.ContentType = "application/zip";
                        //Set zip file name  
                        Response.AddHeader("content-disposition", "attachment; filename=L_" + itinerary.PNR + ".zip");
                        //Save the zip content in output stream  
                        zipFile.Save(Response.OutputStream);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                }
                catch(Exception ex) { Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(GSTDocs)Failed to download all documents. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]); }
                finally { Response.End(); }
            }
        }
    }
    
    /// <summary>
    /// To reprice the PNR before proceed with ticketing
    /// </summary>
    /// <param name="sPNR"></param>
    /// <param name="sFlightId"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public static TicketingResponse RepriceBeforeTicketing(string sPNR, string sFlightId)
    {
        TicketingResponse clsresponse = new TicketingResponse();
        try
        {
            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
            mse.SettingsLoginInfo = Settings.LoginInfo;
            mse.SessionId = (HttpContext.Current.Session["sessionId"] == null) ? Guid.NewGuid().ToString() : HttpContext.Current.Session["sessionId"].ToString();
            clsresponse = mse.RepricePNR(sFlightId, sPNR, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
            HttpContext.Current.Session["UAPIURImpresp"] = mse.clsURImpresp;
            HttpContext.Current.Session["UAPIReprice"] = mse.clsUAPIRepriceResp;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(Reprice)Failed to do Reprice, before ticketing. Reason : " + ex.ToString(), "");
            clsresponse.Status = TicketingResponseStatus.OtherError;
            clsresponse.Message = ex.Message;
        }
        return clsresponse;
    }
}



