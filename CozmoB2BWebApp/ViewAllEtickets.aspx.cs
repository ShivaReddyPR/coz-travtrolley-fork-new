using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
public partial class ViewAllEtickets :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Ticket ticket;
    protected FlightItinerary flightItinerary;
    protected AgentMaster agency;
    protected string logo = string.Empty;
    protected string cityName = string.Empty;
    protected int paxIndex = -1;
    protected string paxName = string.Empty;
    protected int paxTypeIndex = 0;   // for ptcDetail
    protected bool isAgent = true;
    protected UserMaster loggedUserMaster;
    protected string preferenceValue = string.Empty;
    protected string ffNumber = string.Empty;
    protected UserPreference preference = new UserPreference();
    //protected Insurance insurance;
    //protected PolicyDetail policy;
    //protected decimal totalInsurancePrice = 0;
    protected string segmentMessage = string.Empty;
    protected string flightNumberString = string.Empty;
    protected UserPreference serviceFeePref = new UserPreference();
    protected decimal serviceFee = 0;
    protected bool showServiceFee;
    protected bool isDN = false;
    BookingMode mode;
    protected BookingDetail booking;
    protected List<Ticket> ticketList = new List<Ticket>();
    protected Airline airline;
    protected bool IsShowLogo = false;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected string AgentLogoPath = CT.Configuration.ConfigurationSystem.Core["virtualAgentLogoPath"];
    protected HotelSearchResult[] search = new HotelSearchResult[0];
    protected HotelRequest req;
    protected bool IsShowHotelAd = false;   // Bug ID : 0029414- E-Ticket Ads
    protected int cityId;

    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        loggedUserMaster = new UserMaster(Convert.ToInt32(Session["UserMasterId"]));
        if (loggedUserMaster.AgentId == 0)
        {
            isAgent = false;
        }
        if (Request["flightId"] != null || Request["pendingId"] != null)
        {
            if (Request["pendingId"] != null)
            {
                int pendingId = Convert.ToInt32(Request["pendingId"]);
                flightItinerary = FailedBooking.LoadItinerary(pendingId);
                booking = new BookingDetail();
                booking.Status = BookingStatus.Ticketed;
                booking.CreatedBy = flightItinerary.CreatedBy;
                booking.AgencyId = flightItinerary.AgencyId;
                FailedBooking fb = FailedBooking.Load((flightItinerary.PNR));
                booking.CreatedOn = fb.CreatedOn;
                mode = BookingMode.Auto;
            }
            else
            {
                int flightid = Convert.ToInt32(Request["flightId"]);
                ticket = new Ticket();
                ticketList = Ticket.GetTicketList(flightid);
                ticket = ticketList[0];
                ticket.PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(flightid);
                flightItinerary = new FlightItinerary(flightid);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(ticket.FlightId));
                int productid = Product.GetProductIdByBookingId(booking.BookingId, ProductType.Flight);
                mode = Product.GetBookingMode(productid, ProductType.Flight);
            }

            #region  // Bug ID : 0029414- E-Ticket Ads
            UserMaster UserMaster = new UserMaster();
            int agencyId = Ticket.GetAgencyIdForTicket(ticket.TicketId);
            agency = new AgentMaster(agencyId);
            UserMaster = new UserMaster(Settings.LoginInfo.UserID);
            UserPreference pref = new UserPreference();
            pref = pref.GetPreference((int)UserMaster.ID, pref.ShowHotelAdOnEticket, pref.ETicket);
            if (pref.Value != null)
            {
                if (pref.Value.ToUpper() == "TRUE")
                {
                    IsShowHotelAd = true;
                }
            }
            #endregion
            if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]) && IsShowHotelAd == true)
            {
                req = new HotelRequest();
                CityMaster city = new CityMaster();                
                //city.Load(flightItinerary.Destination);
                req.CityName = city.Name;
                //if (flightItinerary.IsDomestic)
                //{
                //    city = new CityMaster();
                //    //city.Load(flightItinerary.Destination);
                //    HotelSearchResult res = new HotelSearchResult();
                //    search = res.LoadTBOResults(city.Name);
                //}
            }
            if (mode == BookingMode.Manual)
            {
                string firstName = flightItinerary.Passenger[0].FirstName;
                string lastName = flightItinerary.Passenger[0].LastName;
                string pnr = flightItinerary.PNR;
                string origin = flightItinerary.Origin;
                string destination = flightItinerary.Destination;
                if (flightItinerary.FlightBookingSource == BookingSource.WorldSpan)
                {
                    Response.Redirect("https://mytripandmore.com/Frameset.aspx?pageName=Itinerary.aspx&PNR=" + pnr + "&clockFormat=12&lastName=" + ticket.PaxFirstName + "");
                }
                else
                {
                    Response.Redirect("EticketInformation.aspx?bookingSource=" + flightItinerary.FlightBookingSource + "&pnr=" + pnr + "&firstName=" + firstName + "&lastName=" + lastName + "&origin=" + origin + "&destination=" + destination + "");
                }
            }

            if (isAgent && booking.AgencyId != loggedUserMaster.AgentId)
            {
                ErrorMessage.Text = "Access Denied.";
                MultiViewETicket.ActiveViewIndex = 1;
            }
            else
            {
                if (Request["serviceFee"] != null)
                {
                    ticket.ServiceFee = Convert.ToDecimal(Request["serviceFee"]);
                    ticket.ShowServiceFee = (ServiceFeeDisplay)Enum.Parse(typeof(ServiceFeeDisplay), Request["serviceFeeDisplay"].ToString());
                    ticket.Save();
                }
                MultiViewETicket.ActiveViewIndex = 0;
                if (Request["flightId"] != null)
                {
                    for (int i = 0; i < flightItinerary.Passenger.Length; i++)
                    {
                        if (flightItinerary.Passenger[i].PaxId == ticket.PaxId)
                        {
                            paxIndex = i;
                            break;
                        }
                    }
                    if (paxIndex >= 0)
                    {
                        paxName = FlightPassenger.GetPaxFullName(flightItinerary.Passenger[paxIndex].PaxId);
                    }
                    else
                    {
                        paxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
                    }
                    paxName = paxName.Trim();
                    ffNumber = flightItinerary.Passenger[paxIndex].FFAirline + flightItinerary.Passenger[paxIndex].FFNumber;
                    airline = new Airline();
                    airline.Load(flightItinerary.ValidatingAirlineCode);

                    pref = pref.GetPreference((int)UserMaster.ID, pref.ShowLogoOnEticket, pref.ETicket);
                    if (pref.Value != null)
                    {
                        if (pref.Value.ToUpper() == "TRUE")
                        {
                            IsShowLogo = true;
                        }
                    }

                    //if (UserMaster.LogoFile != null && UserMaster.LogoFile.Trim().Length > 0)
                    //{
                    //    string[] fileExtension = UserMaster.LogoFile.Split('.');
                    //    AgentLogoPath = AgentLogoPath + "\\" + UserMaster.LoginName + "." + fileExtension[fileExtension.Length - 1];
                    //}
                    if (airline.LogoFile.Trim().Length != 0)
                    {
                        string[] fileExtension = airline.LogoFile.Split('.');
                        AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
                    }

                    for (int i = 0; i < ticket.PtcDetail.Count; i++)
                    {
                        if (ticket.PaxType == FlightPassenger.GetPassengerType(ticket.PtcDetail[i].PaxType))
                        {
                            paxTypeIndex = i;
                            break;
                        }
                    }
                    //policy = PolicyDetail.GetPolicyDetailByPaxId(flightItinerary.Passenger[paxIndex].PaxId);
                    serviceFee = ticket.ServiceFee;
                    if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                    {
                        showServiceFee = true;
                    }
                    else
                    {
                        showServiceFee = false;
                    }
                }
                else if (Request["pendingId"] != null)
                {
                    paxIndex = Convert.ToInt32(Request["PaxId"]);
                    paxName = flightItinerary.Passenger[paxIndex].FirstName + flightItinerary.Passenger[paxIndex].LastName;
                    ffNumber = flightItinerary.Passenger[paxIndex].FFAirline + flightItinerary.Passenger[paxIndex].FFNumber;
                    agency = new AgentMaster(booking.AgencyId);
                }

                DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
                DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

                if (cities != null && cities.Length > 0)
                {
                    cityId = Convert.ToInt32(cities[0]["city_id"]);
                }
                //if (Settings.LoginInfo.IsOnBehalfOfAgent)
                //{
                //    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                //}
                //else
                //{
                //    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                //}
                agency = new AgentMaster(flightItinerary.AgencyId);
                if (agency.City.Length > 0)
                {
                    cityName = (RegCity.GetCity(cityId) != null ? RegCity.GetCity(cityId).CityName : agency.City);
                }
                else
                {
                    cityName = Settings.LoginInfo.LocationName;
                }
                //logo = AgentMaster.GetAgencyLogo(agency.AgencyId);
                //for (int i = 0; i < ticket.PtcDetail.Count; i++)
                //{
                //    if (ticket.PaxType == FlightPassenger.GetPassengerType(ticket.PtcDetail[i].PaxType))
                //    {
                //        paxTypeIndex = i;
                //        break;
                //    }
                //}
                preference = preference.GetPreference((int)Settings.LoginInfo.UserID, preference.ETicket, preference.ETicket);
                if (preference.Value != null)
                {
                    preferenceValue = preference.Value.ToUpper();
                }
                else
                {
                    preferenceValue = preference.ETicketShowFare;
                }
                //policy = PolicyDetail.GetPolicyDetailByPaxId(flightItinerary.Passenger[paxIndex].PaxId);
                //serviceFee = ticket.ServiceFee;
                //if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                //{
                //    showServiceFee = true;
                //}
                //else
                //{
                //    showServiceFee = false;
                //}
            }
        }
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }
}

