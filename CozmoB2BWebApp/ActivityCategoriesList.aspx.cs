﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class ActivityCategoriesListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
protected ActivityDetails details = null;
    protected Activity activity = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Activity"] != null)
        {
            if (!IsPostBack)
            {
                activity = Session["Activity"] as Activity;
                details = ActivityDetails.GetActivityDetailById((int)activity.Id);

                lblActivity.Text = activity.Name;
                lblName.Text = activity.Name;
                lblTitle.Text = activity.Name;
                lblAvail.Text = activity.Name;

                lblDays.Text = activity.Days.ToString();
                lblDate.Text = activity.StartFrom.ToString("dd MMM yyyy");

                DataTable dtPrice = activity.PriceDetails;
                //DataTable dtTransHeader = activity.TransactionHeader;

                DataTable dtPaxPrice = new DataTable();
                if (dtPaxPrice.Columns.Count <= 0)
                {                    
                    dtPaxPrice.Columns.Add("Adults", typeof(int));
                    dtPaxPrice.Columns.Add("Childs", typeof(int));
                    dtPaxPrice.Columns.Add("Infants", typeof(int));
                    dtPaxPrice.Columns.Add("Label", typeof(string));
                    dtPaxPrice.Columns.Add("ActivityId", typeof(long));
                    dtPaxPrice.Columns.Add("Price", typeof(decimal));
                    dtPaxPrice.Columns.Add("Amount", typeof(decimal));
                    dtPaxPrice.Columns.Add("AgentCurrency", typeof(string));
                    dtPaxPrice.Columns.Add("AgentROE", typeof(decimal));
                    dtPaxPrice.Columns.Add("MarkupValue", typeof(decimal));
                    dtPaxPrice.Columns.Add("MarkupType", typeof(string));
                    dtPaxPrice.Columns.Add("SourceCurrency", typeof(string));
                    dtPaxPrice.Columns.Add("SourceAmount", typeof(decimal));
                    dtPaxPrice.Columns.Add("Markup", typeof(decimal));
                    dtPaxPrice.Columns.Add("MinPax", typeof(int));
                    dtPaxPrice.Columns.Add("PriceId", typeof(int));
                }
                string supplierCurrency = "AED";
                decimal rateOfExchange = (Settings.LoginInfo.AgentExchangeRates.ContainsKey(supplierCurrency) ? Settings.LoginInfo.AgentExchangeRates[supplierCurrency] : 1);
                DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load((int)Settings.LoginInfo.AgentId, string.Empty, (int)ProductType.Activity);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;
                decimal markupValue;
                decimal sourceamount;
                decimal amount;
                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                int adults = 0, childs = 0, infants = 0;

                //if (dtTransHeader != null && dtTransHeader.Rows.Count > 0)
                //{
                //    adults = Convert.ToInt32(dtTransHeader.Rows[0]["Adult"]);
                //    childs = Convert.ToInt32(dtTransHeader.Rows[0]["Child"]);
                //    infants = Convert.ToInt32(dtTransHeader.Rows[0]["Infant"]);
                //    hdnAdult.Value = adults.ToString();
                //    hdnChild.Value = childs.ToString();
                //    hdnInfant.Value = infants.ToString();
                //    hdnPaxCount.Value = (adults + childs + infants).ToString();
                //}

                if (dtPrice != null && dtPrice.Rows.Count > 0)
                {
                    foreach (DataRow row in dtPrice.Rows)
                    {
                        sourceamount = (Convert.ToDecimal(row["SupplierCost"]) + Convert.ToDecimal(row["Tax"]) + Convert.ToDecimal(row["Markup"]));
                        amount = sourceamount * rateOfExchange;
                        markupValue = ((markupType == "F") ? markup : (Convert.ToDecimal(amount) * (markup / 100)));
                        decimal total = 0;
                        DataRow dr = dtPaxPrice.NewRow();                        
                        dr["Adults"] = adults;
                        dr["Childs"] = childs;
                        dr["Infants"] = infants;
                        dr["Label"] = row["Label"].ToString();
                        dr["ActivityId"] = activity.Id;
                        dr["Price"] = Convert.ToDecimal(amount.ToString("N" + Settings.LoginInfo.DecimalValue)) + Convert.ToDecimal(markupValue.ToString("N" + Settings.LoginInfo.DecimalValue));
                        dr["Amount"] = total.ToString("N" + Settings.LoginInfo.DecimalValue);
                        dr["AgentCurrency"] = Settings.LoginInfo.Currency;
                        dr["AgentROE"] = rateOfExchange;
                        dr["MarkupValue"] = markup.ToString("N" + Settings.LoginInfo.DecimalValue);
                        dr["MarkupType"] = markupType;
                        dr["SourceCurrency"] = supplierCurrency;
                        dr["SourceAmount"] = sourceamount.ToString("N" + Settings.LoginInfo.DecimalValue); ;
                        dr["Markup"] = markupValue.ToString("N"+Settings.LoginInfo.DecimalValue);
                        dr["MinPax"] = Convert.ToInt32(row["MinPax"]);
                        dr["PriceId"] = Convert.ToInt32(row["priceId"]);
                        dtPaxPrice.Rows.Add(dr);
                    }
                }
                hdnRows.Value = dtPaxPrice.Rows.Count.ToString();
                DataList1.DataSource = dtPaxPrice;
                DataList1.DataBind();
                Session["PaxPrice"] = dtPaxPrice;
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "CalculateTotal()", "CalculateTotal");
            }
        }
        else
        {
            Response.Redirect("ActivityResults.aspx", false);
        }
    }

    protected void imgSubmit_Click(object sender, EventArgs e)
    {
        if (Session["Activity"] != null)
        {
            activity = Session["Activity"] as Activity;

            try
            {
                //valied or not checking Excursion date
                activity = Session["Activity"] as Activity;
                bool isAvailable = true;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DateTime excursionDate = Convert.ToDateTime(txtDepDate.Text, dateFormat);

                /************************************************************************************************
            * ------------------------------------Checks for Booking---------------------------------------*
            * 1. Check whether booking date is after Excursion start date and before closing date.         *         
            * 2   proceed otherwise prompt user to choose another                                           *
            * 3. Check whether booking date falls between available date ranges.                           *
            * 4. Check whether booking date falls between unavailable date ranges.                         *
            * 5. Check whether stock is available to proceed booking.                                      *
            * ---------------------------------------------------------------------------------------------*
            * **********************************************************************************************/
                DateTime StartFrom = Convert.ToDateTime(activity.AvailableFrom, dateFormat);
                DateTime EndTo = Convert.ToDateTime(activity.AvailableTo, dateFormat);
                int totalDays = 0;
                if (excursionDate.CompareTo(DateTime.Now) > 0)
                {
                    totalDays = Convert.ToInt32(Math.Ceiling(excursionDate.Subtract(DateTime.Now).TotalDays));
                }
                else
                {
                    totalDays = Convert.ToInt32(DateTime.Now.Subtract(excursionDate).TotalDays);
                }
                if (activity.BookingCutOff <= totalDays)
                {
                    if (excursionDate.CompareTo(activity.AvailableFrom) >= 0 && excursionDate.CompareTo(activity.AvailableTo) < 0)
                    {
                        foreach (DateTime uad in activity.UnavailableDates)
                        {
                            if (excursionDate.CompareTo(uad) != 0)
                            {
                                isAvailable = true;
                            }
                            else
                            {
                                isAvailable = false;
                                break;
                            }
                        }
                        if (isAvailable)
                        {
                            if ((activity.StockInHand - activity.StockUsed) > 0)
                            {
                                DataRow rowHeader = null;
                                if (activity.TransactionHeader.Rows.Count > 0)
                                {
                                    rowHeader = activity.TransactionHeader.Rows[0];
                                }
                                else
                                {
                                    rowHeader = activity.TransactionHeader.NewRow();
                                }

                                rowHeader["ActivityId"] = activity.Id;
                                rowHeader["ActivityName"] = activity.Name;
                                rowHeader["TransactionDate"] = DateTime.Now;
                                ///new Flow Avaliability page removed so dropdowns is not there 
                                ///Here Adult count assiging TotalQty Value (ex: AdultQty+ChildQty+InfantQty)
                                ///Added by brahmam 17.10.2016
                                rowHeader["Adult"] = Convert.ToInt32(hdnAdult.Value);//ddlAdult.SelectedValue;
                                rowHeader["Child"] = 0;//ddlChild.SelectedValue;
                                rowHeader["Infant"] = 0;//ddlInfant.SelectedValue;
                                rowHeader["Booking"] = Convert.ToDateTime(txtDepDate.Text, dateFormat);
                                rowHeader["TransactionType"] = "B2B";
                                rowHeader["isFixedDeparture"] = "N";// Activity

                                if (activity.TransactionHeader.Rows.Count <= 0)
                                {
                                    activity.TransactionHeader.Rows.Add(rowHeader);
                                }

                                int i = 0;
                                DataTable dtPaxPrice = Session["PaxPrice"] as DataTable;
                                activity.TransactionPrice.Rows.Clear();
                                foreach (DataListItem Item in DataList1.Items)
                                {
                                    if (Item.ItemType == ListItemType.AlternatingItem || Item.ItemType == ListItemType.Item)
                                    {
                                        Label lblFlexLabel = Item.FindControl("lblFlexLabel") as Label;
                                        Label lblPrice = Item.FindControl("lblPrice") as Label;
                                        DropDownList ddlQty = Item.FindControl("ddlQty") as DropDownList;
                                        Label lblTotal = Item.FindControl("lblTotal") as Label;

                                        if (activity.TransactionPrice == null)
                                        {
                                            activity.TransactionDetail = ActivityDetails.GetActivityTransactionDetail(activity.Id);
                                        }
                                        if (Convert.ToInt32(ddlQty.SelectedItem.Value) > 0)
                                        {
                                            DataRow row = activity.TransactionPrice.NewRow();

                                            row["Label"] = lblFlexLabel.Text;
                                            row["Amount"] = lblPrice.Text;
                                            row["LabelQty"] = ddlQty.SelectedItem.Value;
                                            row["LabelAmount"] = lblTotal.Text;
                                            row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                            row["CreatedDate"] = DateTime.Now;
                                            row["LastModifiedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                            row["LastModifiedDate"] = DateTime.Now;
                                            row["AgentCurrency"] = dtPaxPrice.Rows[i]["AgentCurrency"];
                                            row["AgentROE"] = dtPaxPrice.Rows[i]["AgentROE"]; ;
                                            row["MarkupValue"] = dtPaxPrice.Rows[i]["MarkupValue"]; ;
                                            row["MarkupType"] = dtPaxPrice.Rows[i]["MarkupType"]; ;
                                            row["SourceCurrency"] = dtPaxPrice.Rows[i]["SourceCurrency"]; ;
                                            row["SourceAmount"] = dtPaxPrice.Rows[i]["SourceAmount"];
                                            row["Markup"] = dtPaxPrice.Rows[i]["Markup"];
                                            activity.TransactionPrice.Rows.Add(row);
                                        }
                                    }
                                    i++;
                                }
                                Session["Activity"] = activity;
                                Response.Redirect("ActivityPassengerList.aspx", false);
                            }
                            else
                            {
                                lblError.Visible = true;
                                Image1.Visible = true;
                            }
                        }
                        else
                        {
                            lblError.Visible = true;
                            Image1.Visible = true;
                        }
                    }
                    else
                    {
                        lblError.Visible = true;
                        Image1.Visible = true;
                    }
                }
                else
                {
                    lblError.Visible = true;
                    Image1.Visible = true;
                }
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "CalculateTotal()", "CalculateTotal");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, 1, "ActivityCategoriesList Page" + ex.Message, "0");
            }
        }
        else
        {
            Response.Redirect("ActivityResults.aspx", false);
        }
    }
    //Added by brahmam Qty Dropdown purpose
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlQty = e.Item.FindControl("ddlQty") as DropDownList;
                HiddenField hdnMinPax = e.Item.FindControl("hdnMinPax") as HiddenField;
                if (!string.IsNullOrEmpty(hdnMinPax.Value))
                {
                    for (int i = Convert.ToInt32(hdnMinPax.Value); i <= 9; i++)
                    {
                        ddlQty.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Book, Severity.High, 1, "ActivityCategoriesList Page" + ex.Message, "0");
        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }
}
