﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="BaggageInsuranceQueue.aspx.cs" Inherits="CozmoB2BWebApp.BaggageInsuranceQueueGUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<%--<link href="css-holiday/main-style.css" rel="stylesheet" type="text/css" />--%>

<style> .button { background:#1c498a!important }</style>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
<%--    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />--%>
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>
    <script type="text/javascript" src="ash.js"></script>
     <link rel="stylesheet" href="css/style.css">
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
     <link href="css/style.css" rel="stylesheet" type="text/css" /> <!--Added by chandan on  13062016 -->
     
     <link href="css/main-style.css" rel="stylesheet" type="text/css" />

    <div style="padding-top: 10px; position: relative">
        
       <div class="clear" style="margin-left: 30px">
            <div id="container1" style="position: absolute; top: 0px; left: 46%; display: none;
                z-index: 9999;">
            </div>
        </div>
     
        <div class="col-md-12 padding-0 margin-bottom-10">
                <div class="col-md-2">
                    Agency:</div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlAgency" runat="server" CssClass="inputDdlEnabled form-control"
                         AutoPostBack="true" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control"
                         AutoPostBack="true" OnSelectedIndexChanged ="ddlB2BAgent_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            <div class="col-md-2">
                    <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control">
                    </asp:DropDownList>
                </div>
                <div class="clearfix">
                </div>
            </div>

        <div class="col-md-12 padding-0 margin-bottom-10">
               
             <div class="col-md-2">
                    <asp:Label ID="lblPNRno" Text="PNR No:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox CssClass="form-control" ID="txtPNRno" runat="server"></asp:TextBox></div>
                <div class="col-md-2">
                    <asp:Label ID="lblCreatedDate" Text="Created Dt:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCreatedDate" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar1()">
                                    <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div> 
                       <div class="col-md-2">
                    <asp:Label ID="lblTransType" runat="server" Text="TransType:"></asp:Label></div>
                <div class="col-md-2">
                    <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server" Visible="true">
                        <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                        <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                        <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearfix">
                </div>
            </div>

        <div class="col-md-12 padding-0 margin-bottom-10">
                <label class=" f_R padding-left-5">
                    <asp:Button runat="server" ID="btnSearch" Text="Search" 
                        CssClass="btn btn-primary btn_custom"  OnClick="btnSearch_Click" /></label>
                <label class=" f_R padding-left-5">
                    <asp:Button runat="server" ID="btnClear" Text="Clear"  CssClass="btn btn-primary btn_custom"  OnClick="btnClear_Click"/></label>
            </div>
        <div style="padding-bottom: 10px;" width="100%">
            <div <%=pagingEnable %>>
                Page:
                <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First" /></asp:LinkButton>
                <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                <asp:Label ID="lblCurrentPage" runat="server" CssClass=""></asp:Label>
                <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last"></asp:LinkButton>
            </div>
            <div class="clear">
            </div>
        </div>
        <div>
            <asp:DataList ID="dlBaggageInsQueue" runat="server" CellPadding="4" DataKeyField="BI_ID"
                Width="100%"  OnItemDataBound="dlBaggageInsQueue_ItemDataBound"  OnItemCommand="dlBaggageInsQueue_ItemCommand">
                <ItemTemplate>
                   <div class="bg_white bor_gray pad_10 marbot_10" id="Result">
                        <div>
                          <table class="table table-bordered"> 
                            <tr> 
                             <td>Policy No:<strong><%# Eval("BI_POLICY_NO") %></strong> </td>
                             <td>Status:<strong><asp:Label ID="lblStatus" runat="server"></asp:Label></strong> </td> 
                               
                                <td> Agent Name :<strong><%#Eval("AgentName") %> </strong>  </td>
                               
                            
                            </tr>
                            <tr> 
                            <td>Created Date: <strong><%# Convert.ToDateTime(Eval("BI_CREATED_ON")).ToString("dd-MMM-yyyy HH:mm:ss")%> </strong></td> 
                            
                            <td>Booked By Location: <strong><%# Eval("LocationName")%></strong> </td> 
                            
                            <td>Booked By User: <strong><%# Eval("UserName")%> </strong> </td>
                            </tr>  
                            <tr> 
                            <td colspan="2"> </td> 
                            
                            <td> <strong> Amount: </strong> <asp:Label CssClass="fcol_red" ID="lblTotal" runat="server"><%# Eval("PAX_Source_Currency") %>  <%# Eval("BI_TOTAL_PRICE")%></asp:Label> </td>
                            </tr>
                                
                                
                                   
                            </table>
                        </div>
                          <table width="99%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRequestStatus" BackColor="#9AF1A2" ForeColor="Black" runat="server"
                                            Font-Italic="True" Font-Names="Arial" Font-Size="10pt"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        
                                       <div class="col-md-12 padding-0 marbot_10">
                                       
                                       
                                           <div id="accordion" class="panel-group">
                                               <div class="panel panel-default">
                                                   <div class="panel-heading">
                                                       <h4 class="panel-title">
   <a href="#collapseOne<%#Container.ItemIndex %>" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle btn-block collapsed" aria-expanded="false"> <span class="glyphicon pull-right font_bold glyphicon-minus"> </span>View Plan Details
                                                               
                                                           </a>
                                                       </h4>
                                                   </div>
                                                   <div class="panel-collapse collapse" id="collapseOne<%#Container.ItemIndex %>" aria-expanded="false"
                                                       style="height: 0px;">
                                                       <div>
                                                           <table class="table"  runat="server" id="divPlans">
                                                           </table>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                            
                                            <script>


                                                $('.collapse').on('shown.bs.collapse', function() {
                                                    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                                                }).on('hidden.bs.collapse', function() {
                                                    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                                                });

</script> 
                                            
                                         <div class="clearfix">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table style="margin-top: 10px" id="tblChangeRequest" visible="false" width="100%" DataKeyField="BI_ID"
                                            border='0' cellspacing='0' cellpadding='2' runat="server">
                                            <tr>
                                                <td>
                                                    <div class="col-md-12 padding-0 paramcon">
                                                        <div class="col-md-2">
                                                            Request Change :
                                                        </div>
                                                        <div class="col-md-2">
                                                            <asp:DropDownList CssClass="form-control" ID="ddlChangeRequestType" runat="server">
                                                                <asp:ListItem Selected="True" Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Cancel Request" Value="Cancel Request"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtRemarks" Width="100%" TextMode="MultiLine" Rows="2" Text="Enter Remarks here"
                                                                runat="server" Height="80px" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"
                                                                onblur="this.style.color='#000000'; if( this.value=='' ) { this.value='Enter Remarks here'; }"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <b id="errRemarks" style="color: Red" runat="server"></b>
                                                            <asp:Button ID="btnRequest" runat="server" 
                                                                Text="Submit Request" CssClass="but but_b"  CommandName="InsChangeRequest" CommandArgument='<%#Eval("BI_ID") %>' />
                                                        </div>
                                                       
                                                        <div class="clearfix">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                         <div> 
                                      
                                      <div class="col-md-9"> </div>
                                         
                                                 
                                                 
                                                 
                                                 
                                                 
                                               <div class="col-md-3">
                                                            <label class="margin-left-5">
                                                                <asp:Label ID="lblViewInvoice" runat="server" Text=""></asp:Label>
                                                            </label>
                                                            <label class="margin-left-5">
                                                                <asp:Label ID="lblViewBooking" runat="server" Text=""></asp:Label>
                                                            </label>
                                                        </div>
                                      
                                      
                                      <div class="clearfix"> </div>
                                      
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" style="padding-left: 20px">
                                        <div style="position: relative">
                                            <asp:LinkButton ID="lnkPayment" runat="server" Visible="false" Text="Payment Information"
                                                href="#"></asp:LinkButton>
                                            <div id="PaymentInfo-<%#Container.ItemIndex %>" class="visa_PaymentInfo_pop" style="position: absolute;
                                                display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            </div>                   
                                      
                                      
                            <div class="clearfix"> </div>
                </ItemTemplate>
            </asp:DataList>
            <div class="clear">
            </div>
        </div>
        
    </div>
    <script type="text/javascript">
        var cal1;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select Creeated date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

        }
        function showCalendar1() {
           
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            init();
        }
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

//            $('IShimFrame').context.styleSheets[0].display = "none";
//            this.today = new Date();
//            var thisMonth = this.today.getMonth();
//            var thisDay = this.today.getDate();
//            var thisYear = this.today.getFullYear();

//            var todaydate = new Date(thisYear, thisMonth, thisDay);
//            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
//            var difference = (depdate.getTime() - todaydate.getTime());

//            if (difference < 0) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
//                return false;
//            }
//            departureDate = cal1.getSelectedDates()[0];
//            document.getElementById('errMess').style.display = "none";
//            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtCreatedDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        YAHOO.util.Event.addListener(window, "load", init);

        function CancelInsPlan(index) {
            var val = document.getElementById('ctl00_cphTransaction_dlBaggageInsQueue_ctl0' + index + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlBaggageInsQueue_ctl0' + index + '_ddlChangeRequestType').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlBaggageInsQueue_ctl0' + index + '_errRemarks').innerHTML = "Please Select Request type!";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
                document.getElementById('ctl00_cphTransaction_dlBaggageInsQueue_ctl0' + index + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            
        }
    </script>
    
    <script type="text/javascript">
        function ViewBooking(insID) {
            var finalurl = "ViewBookingForBaggageInsurance?bookingId=" + insID;
            window.location = finalurl;
        }
        function ViewInvoice(insHdrId) {
            window.open("BaggageInsuranceInvoice?insHdrId=" + insHdrId, 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
        }
    </script>

</asp:Content>



