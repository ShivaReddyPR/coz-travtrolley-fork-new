﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ActivityConfirmationGUI" Title="Activity Confirmation" Codebehind="ActivityConfirmation.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">
//        function PaymentRedirection(amount) {
//            //alert(amount);
//            if (document.getElementById('CozmoPg') && document.getElementById('CozmoPg').checked == true) {
//                if (document.getElementById('rules').checked == true) {

//                    if (document.getElementById('CozmoPg') && document.getElementById('CozmoPg').checked)//cozmo payment gateway
//                    {
//                        location.href = "PaymentProcessingForActivities.aspx?paymentGateway=CozmoPg";
//                    }
//                    else {
//                        location.href = "PaymentProcessingForActivities.aspx?paymentGateway=HDFC";
//                    }
//                }
//                else {
//                    document.getElementById('checkRules').style.display = "block";
//                }
//            }
//        }
        function validate() {
            if (document.getElementById('rules').checked == false) {
                document.getElementById('checkRules').style.display = "block";
                return false;
            }
            else {
                document.getElementById('checkRules').style.display = "none";
                return true;
            }
        }
    </script>
    
    
    

    <%--<form id="form1" runat="server">--%>
    
        <div style=" padding-top:10px">
            <div class="ns-h3"><asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></div>
            
            
            <div class="wraps0ppp">
            
             <table> 
             
             <tr> 
             
             <td valign="top"> 
             <asp:Image  style="width:120px; height:70px" ID="imgActivity" runat="server" />
             </td>
             
             
             <td> 
                        <p class="pad_left10">
                                    <asp:Label ID="lblTitle1" runat="server" Text=""></asp:Label>
                    
                                    <strong>Location :</strong>
                                    <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label><br>
                                    <strong>Duration :</strong>
                                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                    hours 
                                    
                                    <br /> 
                                  
                                    
                                    <asp:Label ID="lblOverview" runat="server" Text=""></asp:Label>
                                    <br /> 
                                    
                                    <strong> 
                                     Total :<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                    
                                    </strong>
                                 
                                    </p> 
                               
                                
                               
                               
                      
             
             </td>
             
             </tr>
             
             
             </table>
               
                
                
                
                
            </div>
            <div class="clear">
            </div>
        </div>
        <div style="padding: 10px 0px 10px 0px">
            <div style="width: 50%; float: left" class="Holiday_Packages">
               
               
               <div class="ns-h3">
                Passenger Details
                </div>
                <div class="wrap_auto" style="overflow-y: scroll; min-height:300px">
                    <asp:DataList ID="dlPaxList" runat="server" Width="100%">
                        <ItemTemplate>
                            <div>
                              
                              <div style=" padding-bottom:5px;">
                              <h5> 
                              
                              <strong>  <%#Eval("FirstName") %>
                                    <%#Eval("LastName") %></strong>
                              
                              
                              </h5>
                                    
                                    
                                    </div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            Phone :
                                        </td>
                                        <td>
                                            <%#Eval("Phone") %>
                                        </td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td>
                                            Address :
                                        </td>
                                        <td>
                                            400 tower, Sharjah, UAE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            E-mail:
                                        </td>
                                        <td>
                                            <%#Eval("Email") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="b_bot">
                                        </td>
                                    </tr>
                                </table>
                                <span class="b_bot"></span>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                    <div class="clear">
                    </div>
                </div>
                <div class="fff_round">
                    <div class="fff_round_1">
                    </div>
                    <div class="fff_round_2">
                    </div>
                    <div class="fff_round_3">
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div style="width: 45%; float: right" class="Honeymoon_Packages">
                
                      
               <div class="ns-h3">
                Payment Details1
                </div>
                
                
                <div style="min-height:300px;" class="wrap_auto">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <%--<tr>
                            <td width="75%">
                                <li class="pymn">Activity changes may incur penalities and/or increased fares.</li>
                                <li class="pymn">Activity is non-transferable.</li>
                                
                        </tr>--%>
                        <%--<tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>--%>
                       
                       <tr>
                       
                       <td> 
                       
                      <div style=" font-size:11px">
                       Activity changes may incur penalities and/or increased fares.<br />
                            Activity is non-transferable.
                      
                      </div>
                       
                       <br />
                       </td>
                       
                       </tr>
                       
                        <tr>
                            <td>
                                <label class="mychk1">
                                    <input type="checkbox" name="rules" id="rules" />
                                    <b> have read and accept the rules & restrictions.</b></label>
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="fnt11_gray">(please check the
                                    box to continue) </span>
                                <div id="checkRules" class="error_msg" style="display: none">
                                    <div id="errorMessage" class="error_heading">
                                        Please check rules &amp; restrictions box to confirm booking.
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <hr class="b_bot_1 " />
                            </td>
                        </tr>
                        <tr>
                            <td>
                             <table width="100%">
                            <tbody><tr style=" background:#ccc" class="nA-h4">
                                <td>
                                    Available Balance :
                                </td>
                                <td align="right">
                                <asp:Label ID="lblAgentBalance" runat="server" Font-Bold="true"></asp:Label>
                                    <%--<span style="font-weight:bold;" id="ctl00_cphTransaction_lblAgentBalance">AED -21,907.00</span>--%>
                                </td>
                            </tr>
                            <tr class="nA-h4">
                                <td>
                                    Amount to be Paid :
                                </td>
                                <td align="right">
                                <asp:Label ID="lblAmountPaid" runat="server" Font-Bold="true"></asp:Label>
                                    <%--<span style="font-weight:bold;" id="ctl00_cphTransaction_lblAmountPaid">AED 124.78</span>--%>
                                </td>
                            </tr>
                            
                            <tr>
                              
                                <td align="left" colspan="2">
                                    <span style="font-weight:bold; color:Red" id="ctl00_cphTransaction_lblBalanceEtrror"></span>
                                </td>
                            </tr>
                            
                        </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<a class="blue_button" onclick="PaymentRedirection('<%=( activity.TransactionHeader.Rows[0]["TotalPrice"].ToString() )%>');">
                                    <span>Make Payment</span></a>--%>
                                    <asp:Button ID="btnConfirm" CssClass="button-new" runat="server" Text= "Confirm" OnClientClick="return validate();" OnClick="btnConfirm_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label style=" color:Red" ID="lblBalanceEtrror" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="fff_round">
                    <div class="fff_round_1">
                    </div>
                    <div class="fff_round_2">
                    </div>
                    <div class="fff_round_3">
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
       
  
    <%--</form>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

