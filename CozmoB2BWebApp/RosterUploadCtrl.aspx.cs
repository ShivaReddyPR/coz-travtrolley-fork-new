﻿using System;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.Roster;


public partial class RosterUploadCtrlUI : System.Web.UI.Page
{
    
    //private String strConnection = "Server=DEVELOPER;DataBase=CozmoTravel;Integrated Security=True";
    private String strConnection = System.Configuration.ConfigurationManager.AppSettings["dbSetting"];
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            RosterUpload();
            //string source = CT.TicketReceipt.Common.Utility.ToString(Request.QueryString["source"]);
            //if(source=="VisaReco")
            //    visaReconcile();
            //else
            //    handlingReconcile();
            
        }
        catch { throw; }
    }
    private void RosterUpload()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
        try
        {
            
            string path = string.Empty;
            string fileName = string.Empty;
            //OleDbDataReader dReader = null;
            OleDbConnection excelConnection = null;
            OleDbCommand cmd = null;
            OleDbDataAdapter oleDAdapter = null;
            try
            {
                
                //making string value with current date and tiime to generate doc number and attach with upload file name 
                String docNumber = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss").Replace("/", "");
                 docNumber = docNumber.Replace(":", "");
                 docNumber = docNumber.Replace(" ", "");
                //file upload path
                path = fileuploadExcel.PostedFile.FileName;
                fileName = path.Substring(path.LastIndexOf('\\') + 1);
                string fileType = path.Substring(path.LastIndexOf('.'));
                //if (fileType == ".xlsx" ||  fileType == ".xls" ||  fileType == ".txt")
                if (fileType == ".txt")
                {
                    
                }
                else
                    throw new Exception("Invalid File Type");

                string fileNameOnly = fileName.Replace(path.Substring(path.LastIndexOf('.')), docNumber);
                string cnctionPath = Server.MapPath(@"Uploads\Roster\");  //to pass to get connection string with out file name
                path = Server.MapPath(@"Uploads\Roster\") + fileNameOnly+fileType;
                

                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
                fileuploadExcel.PostedFile.SaveAs(path);
                
                //fileuploadExcel.PostedFile.SaveAs(Server.MapPath(@"\Uploads\Reconcile\") + fileName);
                string appliedThru = ddlAppliedThrue.SelectedItem.Value;
                //Create connection string to Excel work book
                string excelConnectionString=string.Empty ;
                if (fileType == ".csv" || fileType == ".txt")
                    excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + cnctionPath + ";Extended Properties=\"Text;HDR=Yes;FORMAT=Delimited\";Persist Security Info=False";
                else
                    excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                //Create Connection to Excel work book
                excelConnection = new OleDbConnection(excelConnectionString);
                
                  excelConnection.Open();
                  
                DataTable Sheets = new DataTable();
                string sheetName= string.Empty ;
            if(fileType !=".csv")
            {
                Sheets = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                sheetName = Sheets.Rows[0][2].ToString().Replace("'", "");
            }
                
                               
                SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection);
                if (fileType == ".csv")
                    cmd = new OleDbCommand("Select *  from  [" + fileNameOnly + "#csv]", excelConnection);
                else if(fileType==".txt")
                    cmd = new OleDbCommand("Select *  from  [" + fileNameOnly + "#txt]", excelConnection);
                else
                    cmd = new OleDbCommand("Select [STAFF_ID],[FLIGHTNO],[FROM_SECTOR],[TO_SECTOR],[DATE],[DUTY_HOURS],[ETD],[ETA] from [" + sheetName + "]", excelConnection);
                
                
                        oleDAdapter = new OleDbDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        //DataTable dtActual= new DataTable();

                        //dtActual.Columns.AddRange(new DataColumn[] { 
                        // new DataColumn("STAFF_ID", typeof(string)), 
                        // new DataColumn("STAFF_NAME", typeof(string)),
                        //new DataColumn("FLIGHTNO", typeof(string)),
                        //new DataColumn("FROM_SECTOR", typeof(string)),
                        //new DataColumn("TO_SECTOR", typeof(string)),
                        //new DataColumn("DATE", typeof(DateTime)),
                        //new DataColumn("SIGNIN", typeof(DateTime)),
                        //new DataColumn("SIGNOUT", typeof(DateTime)),
                        //new DataColumn("DUTY_HOURS", typeof(DateTime)),
                        //new DataColumn("ETD", typeof(DateTime)),
                        //new DataColumn("ETA", typeof(DateTime)),
                        //});
                        //oleDAdapter.Fill(dtActual); 
                        oleDAdapter.Fill(dt);

                        DataView dv = dt.DefaultView;
                        dv.RowFilter = "STAFF_ID <> '' AND FROM_SECTOR<>'' AND TO_SECTOR<>''";
                        docNumber = "ROS/" + docNumber;

                        DataTable dtRoster = dv.ToTable(); 
                        int homeDriverTime = Utility.ToInteger(System.Configuration.ConfigurationManager.AppSettings["ROS_HOME_DRIVER_TIME"]);
                        int airportDriverTime = Utility.ToInteger(System.Configuration.ConfigurationManager.AppSettings["ROS_AIRPORT_DRIVER_TIME"]);

                        List<RosterUpload> uploadList = new List<RosterUpload>();
                        foreach (DataRow dr in dtRoster.Rows)
                        {
                        RosterUpload rosUpload = new RosterUpload();
                        rosUpload.Upld_id = -1;
                        rosUpload.Upld_staffId =Utility.ToString(dr["STAFF_ID"]);
                        rosUpload.Upld_flightno= Utility.ToString(dr["FLIGHTNO"]);
                        rosUpload.Upld_from_sector = Utility.ToString(dr["FROM_SECTOR"]);
                        rosUpload.Upld_to_sector = Utility.ToString(dr["TO_SECTOR"]);
                        string dutyHours = Utility.ToDate(dr["DUTY_HOURS"]).ToString("hh:mm");
                        string[] dutyHrandMin = dutyHours.Split(':');
                        rosUpload.Upld_duty_hrs = dutyHrandMin[0] + " hrs " + dutyHrandMin[1] + " mins";
                        rosUpload.Upld_rm_date = Utility.ToDate(dr["DATE"]);
                        rosUpload.Upld_date = DateTime.Now;
                        rosUpload.Upld_etd = Utility.ToDate(dr["ETD"]).ToString("HH:mm");
                        rosUpload.Upld_eta = Utility.ToDate(dr["ETA"]).ToString("HH:mm");
                        rosUpload.Upld_doc_number = docNumber;
                        rosUpload.Upld_file_name = fileNameOnly+fileType;
                        rosUpload.Upld_created_by = Utility.ToInteger(Settings.LoginInfo.UserID);
                        rosUpload.Home_driver_time = homeDriverTime;//in Mins
                        rosUpload.Airport_driver_time = airportDriverTime;//in Mins
                        uploadList.Add(rosUpload);      
                            
                         }

                        CT.Roster.RosterUpload.SaveRosterUploadList(uploadList);                     

                
                excelConnection.Close();
                hdfDocNumber.Value = docNumber;
                txtDocNumber.Text = docNumber;
                Utility.Alert(this.Page,"Uploaded the roster successfully with Roster Number "+docNumber+" !");
                
            }
            catch (Exception ex)
            {
                CT.TicketReceipt.Common.Utility.WriteLog(ex, "Roster Uploader");
                CT.TicketReceipt.Common.Utility.Alert(this.Page, ex.Message);
                if(excelConnection!=null)excelConnection.Dispose();
                if (cmd != null) cmd.Dispose();
                
            }
            finally
            {
                CT.TicketReceipt.Common.Utility.WriteLog("finally start", "Roster Uploader");
                //if (System.IO.File.Exists(path)) System.IO.File.Delete(path); //TOREMOVE

                CT.TicketReceipt.Common.Utility.WriteLog("finally end", "Roster Uploader");

            }
        }
        catch { throw; }
    }
  
    

}


