using System;
using System.Web.UI;

using CT.BookingEngine;
using CozmoB2BWebApp;

public partial class StatesAjax : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["requestFrom"] == "PassengerDetail")
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            string str = "";
            string agencyId = "";
            foreach (char c in key)
            {
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
                {
                    tempKey += c;
                }
            }
            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == null)
            {
               
            }
            else
            {
                agencyId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId.ToString();
                key = tempKey.ToLower();
                key = agencyId + "A" + key;
                string value = string.Empty;
                foreach (string customer in ((Trie<string>)Page.Application["trieForAgencyCustomers"]).PrefixFind(key))
                {
                    value = value + customer + "//";
                }

                if (value.Length != 0)
                {
                    str = value.Remove(value.Length - 2);
                }
            }
            Response.Write(str);
            Response.End();
        }
        //else if (Request["requestFrom"] == "TrainTatkalRequest" && Request["requestFor"] == "trainList")
        //{
        //    string key = Request["searchKey"];

        //    key = key.ToLower();
        //    string tempKey = string.Empty;
        //    string presentIndex = string.Empty;
        //    foreach (char c in key)
        //    {
        //        if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
        //        {
        //            tempKey += c;
        //        }
        //    }
        //    key = tempKey.ToLower();
        //    string value = string.Empty;
        //    //foreach (Train train in ((Trie<Train>)Page.Application["trieForTrainList"]).PrefixFind(key))
        //    //{
        //    //    value = value + train.TrainName + "  (" + train.TrainNumber + ")//";
        //    //}
        //    string str = string.Empty;
        //    if (value.Length != 0)
        //    {
        //        str = value.Remove(value.Length - 2);
        //    }
        //    Response.Write(str);
        //    Response.End();
        //}
        //else if (Request["requestFrom"] == "TrainTatkalRequest" && Request["requestFor"] == "trainStations")
        //{
        //    //The below code gives result for train's autocomplete
        //    string key = Request["searchKey"];
        //    key = key.ToLower();
        //    string tempKey = string.Empty;
        //    string presentIndex = string.Empty;
        //    foreach (char c in key)
        //    {
        //        if (c >= 'a' && c <= 'z')
        //        {
        //            tempKey += c;
        //        }
        //    }
        //    key = tempKey.ToLower();
        //    string value = string.Empty;

        //    foreach (IRCTCStation station in ((Trie<IRCTCStation>)Page.Application["trieForTrainStation"]).PrefixFind(key))
        //    {
        //        value = value + station.StationName + "  (" + station.StationCode + ")//";
        //    }
        //    string str = string.Empty;
        //    if (value.Length != 0)
        //    {
        //        str = value.Remove(value.Length - 2);
        //    }
        //    Response.Write(str);
        //    Response.End();
        //}
        //else if (Request["requestFrom"] == "TrainSearch")
        //{
        //    //The below code gives result for train's autocomplete
        //    string key = Request["searchKey"];
        //    key = key.ToLower();
        //    string tempKey = string.Empty;
        //    string presentIndex = string.Empty;
        //    foreach (char c in key)
        //    {
        //        if (c >= 'a' && c <= 'z')
        //        {
        //            tempKey += c;
        //        }
        //    }
        //    key = tempKey.ToLower();
        //    string value = string.Empty;

        //    foreach (IRCTCStation station in ((Trie<IRCTCStation>)Page.Application["trieForTrainStation"]).PrefixFind(key))
        //    {
        //        value = value + station.StationName + "  (" + station.StationCode + ")//";
        //    }
        //    string str = string.Empty;
        //    if (value.Length != 0)
        //    {
        //        str = value.Remove(value.Length - 2);
        //    }
        //    Response.Write(str);
        //    Response.End();
        //}
        else if (Request["requestFrom"] == "HotelSearchDomestic")
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            foreach (HotelCity city in ((Trie<HotelCity>)Page.Application["trieForHotelCity"]).PrefixFind(key))
            {
                value = value + city.CityName + "/";
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
        else if (Request["requestFrom"] == "ReturnSearch")
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z' || c== ' ')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            /************************************************************************************************
             *                                  Country name search
             * **********************************************************************************************/
            foreach (Airport aPort in ((CT.BookingEngine.Trie<Airport>)Page.Application["tree1"]))
            {
                if (!string.IsNullOrEmpty(aPort.CountryName))
                {
                    if (aPort.CountryName.Length > key.Length)
                    {
                        if (aPort.CountryName.Substring(0, key.Length).ToLower() == key)
                        {
                            if (!presentIndex.Contains(aPort.AirportIndex + ","))
                            {
                                presentIndex += aPort.AirportIndex + "," + aPort.CityName + ",";
                                value = value + "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName + "/";//- " + aPort.AirportName + "/"; // + "," + aPort.CountryName + "-" + aPort.AirportName + " (" + aPort.AirportCode + ")" + "/";
                            }
                        }
                    }
                    else
                    {
                        if (aPort.CountryName.ToLower() == key)
                        {
                            if (!presentIndex.Contains(aPort.AirportIndex + ","))
                            {
                                presentIndex += aPort.AirportIndex + "," + aPort.CityName + ",";
                                value = value + "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName + "/"; //- " + aPort.AirportName + "/"; // + "," + aPort.CountryName + "-" + aPort.AirportName + " (" + aPort.AirportCode + ")" + "/";
                            }
                        }
                    }
                }
            }
            /************************************************************************************************
             *                                  City wise search
             * **********************************************************************************************/
            foreach (Airport aPort in ((CT.BookingEngine.Trie<Airport>)Page.Application["tree1"]).PrefixFind(key.Replace(" ", "")))
            {
                if (!value.Contains("(" + aPort.AirportCode + ")"))
                {
                    presentIndex += aPort.AirportIndex + "," + aPort.AirportCode + ",";
                    value = value + "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName + " - " + aPort.AirportName + "/";
                }
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
        else if (Request["requestFrom"] == "HotelSearchInternational")
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            foreach (GTACity aPort in ((Trie<GTACity>)Page.Application["tree2"]).PrefixFind(key))
            {
                if (!(presentIndex.Contains(aPort.Index + ",") || presentIndex.Contains(aPort.CityName + ",")))
                {
                    presentIndex += aPort.Index + "," + aPort.CityName + ",";
                    value = value + aPort.CityCode + "," + aPort.CityName + "," + aPort.CountryName + "/";
                }
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
        else if (Request["requestFrom"] == "SearchGTAAirport")
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(key))
            {
                if (!(presentIndex.Contains(aPort.Index + ",") || presentIndex.Contains(aPort.AirportCode + ",")))
                {
                    presentIndex += aPort.Index + "," + aPort.AirportCode + ",";
                    value = value + "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName + "/";
                }
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
    }

}
