﻿using CT.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ExpCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// To set/get the page parameters using session instead of query string params
        /// </summary>
        /// <param name="sessionKey"></param>
        /// <param name="sessionData"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string GetSetPageParams(string sessionKey, string sessionData, string action)
        {
            return GenericStatic.GetSetPageParams(sessionKey, sessionData, action);
        }
    }
}