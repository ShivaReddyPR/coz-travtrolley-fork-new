﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ActivityAvailabilityGUI" Title="Activity Availability" Codebehind="ActivityAvailability.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>


<!-- ----------------------For Calender Control--------------------------- -->
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<!-- ----------------------End Calender Control--------------------------- -->

 

    <!-- Add jQuery library -->

    
    <!-- Add fancyBox main JS and CSS files -->

    <script type="text/javascript" src="Scripts/Jquery/jquery.fancybox.js"></script>
<script type="text/javascript">

    var cal1;
    var cal2;
    function init() {
        var dt = new Date();
        cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
        cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + (dt.getDate()+1) + "/" + dt.getUTCFullYear());//Stopping same day selection in Calendar by Shiva 15 Oct 2016
        cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
//        cal1.cfg.setProperty("title", "Select your desired checkin date:");
        cal1.cfg.setProperty("close", true);
        cal1.cfg.setProperty("iframe", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();        
    }

    function showCalendar1() {
        
        document.getElementById('container1').style.display = "block";
        document.getElementById('Outcontainer1').style.display = "block";        
    }

    var departureDate = new Date();
    
    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMessHotel').style.visibility = "visible";
            document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=txtDepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        cal1.hide();
        document.getElementById('Outcontainer1').style.display = "none";
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
    
    

    
    </script>
 <script type="text/javascript">

     function Validate() {
         var msg = true;
         var date = document.getElementById('<%=txtDepDate.ClientID %>').value;
         if (date.length <= 0 || date =='DD/MM/YYYY') {
             document.getElementById('errMessHotel').style.display = 'block';
             document.getElementById('errMessHotel').innerHTML = 'Enter Valid Date';
             msg = false;
         }
         else {
             document.getElementById('errMessHotel').style.display = "none";
             msg = true;
         }
         return msg;
     }
     function ShowDiv(name) {
         if (name == "home") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "Inclusions") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "Itinerary") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "RoomRates") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "Terms") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             return false;
         }
     }

     
    </script>
    
    
      <script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<link rel="stylesheet" href="css/ajax_tab_css.css" />
   
  
    
     <script type="text/javascript">


         $(function() {
             $('.tabs').tabs()
         });

     $('.tabs').bind('change', function(e) {
         var nowtab = e.target // activated tab
         var divid = $(nowtab).attr('href').substr(1);
         if (divid == "ajax") {
             $.getJSON('<%=Request.Url.Scheme%>://1.upstatic.sinaapp.com/api.php').success(function(data) {
                 $("#" + divid).text(data.msg);
             });
         }


     });
  
    </script>
   
   
    <div>
        
          <div class="ns-h3">
                
                 <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                </div>
                
                
                
                
                
            
            <div class="wraps0ppp">
            
            
            
                 <div class="col-md-8 gap_pck pad_left0"> 
                 
                 
              <asp:Image ID="imgActivity" runat="server" Width="624px" Height="270px"/>
                 
                 
                 
                  
              
               
               </div> 
            
            
            
            <div class="col-md-4 paramcon">
                                          
                                          
                                          
                                         
                        <div id="BookingDetails">
                                            </div>
                                            <div id="plztryagain">
                                            <div class="error_msg" style="display:none;" id="errMessHotel"> </div>
                                                
                                                
                                                <table width="100%" style="margin-left:10px" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="20" align="left" valign="top">
                                                            <strong>Select Dates</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Excursion Date :
                                                                        </td>
                                                                        <td>
                                                                            
                                                                            <asp:TextBox Width="90px" ID="txtDepDate" runat="server" CssClass="form-control" Text="DD/MM/YYYY"></asp:TextBox>
                                                                        </td>
                                                                        <td valign="bottom">
                                                                            <a href="javascript:void(null)" onclick="showCalendar1()"/><img src="images/call-cozmo.png"></a>
                                                                        </td>
                                                                       
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style=" border-bottom: solid 1px #ccc; height:10px;"> </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" align="left" valign="top">
                                                            <strong>Select No. of Passengers</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" border="0" width="200">
                                                              
                                                                    <tr>
                                                                        <td align="left">
                                                                           
                                                                            Adults:</td>
                                                                                                    <td>
                                                                                                        Childs</td>
                                                                                                    <td>
                                                                                                        Infants</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                   <asp:DropDownList ID="ddlAdult" CssClass="form-control" runat="server">                                                                            
                                                                            <asp:ListItem Selected="True" Value="1" Text="1"></asp:ListItem>                                                                                
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left">
                                                                   <asp:DropDownList ID="ddlChild"  CssClass="form-control" runat="server">                                                                            
                                                                                <asp:ListItem Selected="True" value="0" Text="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left">
                                                                     <asp:DropDownList ID="ddlInfant" CssClass="form-control" runat="server">                                                                            
                                                                                <asp:ListItem Selected="True" Value="0" Text="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                               
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                         <td style=" border-bottom: solid 1px #ccc; height:10px;"> </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                                                                                            
                                                           
                                                           <div class="martop_20"> 
                                                           
                                                           <asp:LinkButton ID="imgCheck" CssClass="button"  Text="Check Availability"
                                                                runat="server" Font-Bold="true" onclick="imgCheck_Click" OnClientClick="return Validate();" />
                                                           
                                                           </div>     
                                                                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                                                <tr>
                                                                    <td width="11%">                                                                        
                                                                        <asp:Image ID="Image1" ImageUrl="images/error.gif" Width="29" Height="24" Visible="false" runat="server" />
                                                                    </td>
                                                                    <td width="89%">
                                                                        
                                                                            <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="#9a0406" Text="Selected tour is not available on the specified dates. Please change your search
                                                                            parameters."></asp:Label>
                                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>                                     
                                                        
                                            
                                             
                                             
                                   
                                             
                                             
                                                
                                                
                                           
                     </div>
              
         
                            <div class="clear">
                            </div>
            </div>
           
        </div>
        
        
            

            
            
              <div class="col-md-12 padding-0 margin_top20 marbot_20"> 

 

    
    <!--easy reponsive tabs-->
    <script src="Scripts/easyResponsiveTabs.js"></script>
    
    <link rel="stylesheet" type="text/css" href="css/easy-responsive-tabs.css" />
  
    
     	<!--Plug-in Initialisation-->
	<script type="text/javascript">
	    $(document).ready(function() {
	        //Horizontal Tab
	        $('#parentHorizontalTab').easyResponsiveTabs({
	            type: 'default', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            fit: true, // 100% fit in a container
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });

	        // Child Tab
	        $('#ChildVerticalTab_1').easyResponsiveTabs({
	            type: 'vertical',
	            width: 'auto',
	            fit: true,
	            tabidentify: 'ver_1', // The tab groups identifier
	            activetab_bg: '#fff', // background color for active tabs in this group
	            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
	            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
	            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
	        });

	        //Vertical Tab
	        $('#parentVerticalTab').easyResponsiveTabs({
	            type: 'vertical', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            fit: true, // 100% fit in a container
	            closed: 'accordion', // Start closed if in accordion view
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo2');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });
	    });
</script>

<div id="parentVerticalTab">
            <ul class="resp-tabs-list hor_1">
                <li>Overview</li>
                <li>Inclusions & Exclusions</li>
                
                <li>Itinerary</li>
                
                <li>Price</li> 
             
                         
                <li>Terms & Conditions</li> 
                                
            </ul>
           
           
            <div class="resp-tabs-container hor_1">
                
              

<div  id="Overview">   <%=(activity != null ? activity.Overview : "") %></div>


<div id="Div1">
 <label>
                                        <h4>Inclusions:</h4>
                                        <ul></ul>
                                        <%if (activity != null && activity.Inclusions != null && activity.Exclusions != null)
                                          { %>
                                            <%foreach (string item in activity.Inclusions)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%} %></label>
                                        <label>
                                        <h4>Exclusions:</h4>
                                            <%foreach (string item in activity.Exclusions)
                                              { %>
                                            <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%}
                                          }%></label>

 </div>
 
<div id="Div2"> <label>
                                            <%=(activity != null ? activity.Itinerary1 : "") %></label>
                                        <label>
                                            <%=(activity != null ? activity.Itinerary2 : "") %></label>
                                            <%=(activity != null ? activity.Details : "") %></label>
                                            <h4>Transfer included</h4>
                                           <li style=" list-style-type:circle; margin-left:20px;"> <%=(activity != null ? activity.TransferIncluded : "") %></li>
                                            <h4>Meals Included</h4>
                                            <ul>
                                            <%if(activity != null && activity.MealsIncluded != null){ %>
                                            <%string[] mealItems = activity.MealsIncluded.Split(','); %>
                                            <%foreach (string meal in mealItems)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=meal %></li>
                                            <%} %>
                                            <%} %>
                                            </ul> </div>
  
   
<div id="Term-condition">   <label>
                                    <h4>Room Rates</h4>
                                            <asp:DataList ID="dlRoomRates" runat="server" Width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                                  
                                                        <tr>
                                                            <td height="25px" width="30%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                           
                                                            <td width="70%">
                                                            <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                              <%#CTCurrencyFormat(Eval("Total"))%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                            </label>  </div>
    
    
<div id="Div3"> 

    <ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                                        
                                        
                                        <h4>Things to bring</h4>
                                        <ul>
                                        <%if (activity != null && activity.ThingsToBring != null)
                                          { %>
                                        <%string[] things = activity.ThingsToBring.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string thing in things)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=thing %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>

</div>
            
            
</div>

</div> 
    
    <%--</form>--%>
    
    </div>
          
    
    <div id="Outcontainer1" style="position:absolute;display: none; right:60px; top:164px;  border:solid 0px #ccc;  z-index:200;  " >

    <a onclick="document.getElementById('Outcontainer1').style.display = 'none'">
        <%--<img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />--%>

</a>

<div id="container1">
</div>

</div>
   <%--</form>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

