﻿using System;
using System.Collections.Generic;
using CT.BookingEngine.GDS;
using System.Globalization;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;

public partial class AgodaHotelBookingListGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                deStart.Date = DateTime.Now.Date;
                deEnd.Date = DateTime.Now.Date;
                LoadData();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    public void LoadData()
    {
        try
        {
            string frmDt = string.Empty, toDt = string.Empty, bookingRefNo = string.Empty;
            frmDt = deStart.Text; toDt = deEnd.Text; bookingRefNo = txtBookingRefNo.Text.Trim();

            Agoda_HotelsList ObjAgoda = new Agoda_HotelsList();

            if (!string.IsNullOrEmpty(frmDt) && !string.IsNullOrEmpty(toDt))
            {
                frmDt = DateTime.Parse(frmDt.Trim()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                toDt = DateTime.Parse(toDt.Trim()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            CT.BookingEngine.GDS.Agoda agodaApi = new CT.BookingEngine.GDS.Agoda();
            List<Agoda_HotelsList> BookingList = agodaApi.GetHotelDetailsList(frmDt, toDt, bookingRefNo);
            if (BookingList != null && BookingList.Count > 0)
            {
                if (string.IsNullOrEmpty(BookingList[0].ErrorMessage))
                {
                    GrdBkngList.DataSource = BookingList;
                    GrdBkngList.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            LoadData();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void grid_PageIndexChanging(object sender, EventArgs e)
    {
        try
        {
            LoadData();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }  

}
