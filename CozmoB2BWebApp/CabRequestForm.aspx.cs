﻿using CT.BookingEngine;
using System;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using System.Data;
using CT.TicketReceipt.BusinessLayer;
using System.Transactions;
using CT.TicketReceipt.Common;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace CozmoB2BWebApp
{
    public partial class CabRequestForm : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null && !IsPostBack)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Id", typeof(int)), new DataColumn("FromDate"), new DataColumn("ToDate"), new DataColumn("PickUpTime"), new DataColumn("RentalType"), new DataColumn("CabModel"), new DataColumn("CabPreference"), new DataColumn("PickUpFrom"), new DataColumn("DropAt"), new DataColumn("SpecialRequest"), new DataColumn("Email"),new DataColumn("FlightNumber") });
                    Session["CabRequestData"] = dt;
                    GetCabDetails();
                    GetCabReferenceNumber();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        gvData.DataSource = dt;
                        gvData.DataBind();
                    }
                    BindAgents();
                    btnUpdate.Visible = false;
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in page_Load." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
         
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Utility.StartupScript(this.Page, "HideSuccessMessage();", "HideSuccessMessage");
        }

        [WebMethod] //To Display the Cab charge based on selection of rentalType,cabmodel, cab preference
        public static decimal GetCabCharge(string rentalType, string cabModel, string cabPreference)
        {
            decimal amount = 0;
            CabRequestDetails cabRequestDetails = new CabRequestDetails();
            TextBox text = new TextBox();

            
            DataTable dt = new DataTable();
            dt=cabRequestDetails.GetCabCharge(rentalType,cabModel, cabPreference);
            if(dt.Rows.Count>0)
            {
                amount = Convert.ToDecimal(dt.Rows[0][0].ToString());
            }
            return amount;
        }

        //To get Cab Preference Number prefic with 'CAB' .
        public void GetCabReferenceNumber()
        {
            CabRequestHeader cabRequestHeader = new CabRequestHeader();
            string RefNUmber = cabRequestHeader.GetCabReferenceNumber();
            hdnRefNumber.Value = RefNUmber; 
        }

        private void BindAgents()
        {
            try
            {
                int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                if (dtAgents != null)
                {
                    ddlAgents.DataSource = dtAgents.Select("AGENT_ID NOT ='" + agentId + "'").CopyToDataTable();
                    ddlAgents.DataTextField = "AGENT_NAME";
                    ddlAgents.DataValueField = "AGENT_ID";
                    ddlAgents.DataBind();
                    ddlAgents.Items.Insert(0, new ListItem("--Select Agent--", "0"));
                    ddlAgents.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(CabRequestForm)Error in BindAgents()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
               
            }
        }

        public void GetCabDetails()
        {
            try
            {
                DataRow[] dr;
                CabRequestHeader cabRequest = new CabRequestHeader();
                DataTable dt = cabRequest.GetCabPreferences();
                dr = dt.Select("LST_CODE= " + "'CabType'");
                if (dr!=null && dr.Length>0)
                {
                    ddlCabPreference.DataSource = dr.CopyToDataTable();
                    ddlCabPreference.DataTextField = "LST_DESCP";
                    ddlCabPreference.DataValueField = "LST_VALUE";
                    ddlCabPreference.DataBind();
                    ddlCabPreference.Items.Insert(0, new ListItem("--Select--", "-1"));
                   
                }
                dr = dt.Select("LST_CODE= " + "'SerType'");
                if(dr!=null && dr.Length>0)
                {
                    ddlRentalType.DataSource = dr.CopyToDataTable();
                    ddlRentalType.DataTextField = "LST_DESCP";
                    ddlRentalType.DataValueField = "LST_VALUE";
                    ddlRentalType.DataBind();
                    ddlRentalType.Items.Insert(0, new ListItem("--Select--", "-1"));
                }
                dr = dt.Select("LST_CODE= " + "'CabModel'");
                if(dr!=null && dr.Length>0)
                {
                    ddlCabModel.DataSource = dr.CopyToDataTable();
                    ddlCabModel.DataTextField = "LST_DESCP";
                    ddlCabModel.DataValueField = "LST_VALUE";
                    ddlCabModel.DataBind();
                    ddlCabModel.Items.Insert(0, new ListItem("--Select--", "-1"));
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in GetCabPreferences." + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    DataTable dt; 
                    if (Session["CabRequestData"] == null)
                    {
                        dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Id", typeof(int)), new DataColumn("FromDate"), new DataColumn("ToDate"), new DataColumn("PickUpTime"), new DataColumn("RentalType"), new DataColumn("CabModel"), new DataColumn("CabPreference"), new DataColumn("PickUpFrom"), new DataColumn("DropAt"), new DataColumn("SpecialRequest"), new DataColumn("Email"),new DataColumn("FlightNumber") });
                        Session["CabRequestData"] = dt;
                    }
                    else
                    {
                        dt = (DataTable)Session["CabRequestData"];
                    }
                    int RowId = dt.Rows.Count;
                    RowId++;
                    //dt.Rows.Add(RowId, FromDate.DateText(), ToDate.DateText(), PickUpTime.TimeText(), ddlRentalType.SelectedValue, ddlCabModel.SelectedValue, ddlCabPreference.SelectedValue, txtPickUpFrom.Text, txtDropAt.Text, txtSpecialRequest.Text, txtEmail.Text, txtFlightNumber.Text);
                    dt.Rows.Add(RowId, FromDate.Text, ToDate.Text, ddlPickuptime.SelectedValue, ddlRentalType.SelectedValue, ddlCabModel.SelectedValue, ddlCabPreference.SelectedValue, txtPickUpFrom.Text, txtDropAt.Text, txtSpecialRequest.Text, txtEmail.Text, txtFlightNumber.Text);

                    Session["CabRequestData"] = dt;
                    BindGrid();
                    btnSave.Visible = true;
                    btnClearVal.Visible = true;
                    btnUpdate.Visible = false;
                    Clear();
                    LoadLocations();


                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in btnAdd_Click." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        private void Clear()
        {
            txtPickUpFrom.Text = string.Empty;
            txtDropAt.Text = string.Empty;
            txtSpecialRequest.Text = string.Empty;
            txtEmail.Text = string.Empty;
            FromDate.Text = string.Empty;
            ToDate.Text = string.Empty;
            txtFlightNumber.Text = string.Empty;
            ddlRentalType.SelectedIndex = -1;
            ddlCabPreference.SelectedIndex = -1;
            ddlCabModel.SelectedIndex = -1;
            lblSuccessMsg.Text = string.Empty;
            lblSuccessMsg.Style.Add("display", "none");
            LoadLocations();
            btnUpdate.Visible = false;
            btnDelete.Visible = false;
            btnAdd.Visible = true;
            Utility.StartupScript(this.Page, "SetDefaultPickupTime();", "SetDefaultPickupTime");

        }

        private void BindGrid()
        {
            try
            {
                gvData.Visible = true;
                gvData.DataSource = (DataTable)Session["CabRequestData"];
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in BindGrid." + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }
        private void LoadLocations()
        {
            try
            {
                if (radioAgent.Checked)
                {
                    wrapper1.Style.Add("display", "block");
                    wrapper2.Style.Add("display", "block");
                    Utility.StartupScript(this.Page, "LoadAgentLocations(this.id, 'F');", "LoadAgentLocations");
                }
                else
                {
                    hdnLocation.Value = "";
                    wrapper1.Style.Add("display", "none");
                    wrapper2.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in LoadLocations." + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                GridViewRow gvRow = gvData.SelectedRow;
                hdnId.Value = gvData.SelectedRow.RowIndex.ToString();
                //FromDate.SetDateTime(Convert.ToDateTime(gvRow.Cells[1].Text), 0);
                //ToDate.SetDateTime(Convert.ToDateTime(gvRow.Cells[2].Text), 0);
                FromDate.Text = gvRow.Cells[1].Text;
                ToDate.Text = gvRow.Cells[2].Text;
                ddlPickuptime.SelectedValue= gvRow.Cells[3].Text;
                ddlRentalType.SelectedValue = gvRow.Cells[4].Text;
                ddlCabModel.SelectedValue = gvRow.Cells[5].Text;
                ddlCabPreference.SelectedValue = gvRow.Cells[6].Text;
                txtPickUpFrom.Text = gvRow.Cells[7].Text;
                txtDropAt.Text = gvRow.Cells[8].Text;
                txtSpecialRequest.Text = gvRow.Cells[9].Text;
                txtEmail.Text = gvRow.Cells[10].Text;
                txtFlightNumber.Text = gvRow.Cells[11].Text.Replace("&nbsp;", "");
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnDelete.Visible = true;
                btnSave.Visible = true;
                btnClearVal.Visible = true;
                LoadLocations();
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in gvData_SelectedIndexChanged." + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = (DataTable)Session["CabRequestData"];
                int RowId = Convert.ToInt32(hdnId.Value);
                RowId++;
                DataRow dr = dt.Select("Id=" + RowId).FirstOrDefault();
                dr[1] = FromDate.Text;
                dr[2] = ToDate.Text;
                dr[3] = ddlPickuptime.SelectedValue;
                dr[4] = ddlRentalType.SelectedValue;
                dr[5] = ddlCabModel.SelectedValue;
                dr[6] = ddlCabPreference.SelectedValue;
                dr[7] = txtPickUpFrom.Text;
                dr[8] = txtDropAt.Text;
                dr[9] = txtSpecialRequest.Text;
                dr[10] = txtEmail.Text;
                dr[11] = txtFlightNumber.Text.Replace("&nbsp;", ""); 

                Session["CabRequestData"] = dt;
                BindGrid();
                Clear();
                lblSuccessMsg.Style.Add("display", "block");
                lblSuccessMsg.Text = "Successfully Updated";
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in btnUpdate_Click." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int cabHeaderID = 0;
                string replyTo = string.Empty;
                CabRequestHeader cabRequestHeader = new CabRequestHeader();
                cabRequestHeader.Name = txtName.Text;
                cabRequestHeader.MobileNumber = txtMobileNumber.Text;
                cabRequestHeader.AgentId = (radioAgent.Checked == true ? Convert.ToInt32(ddlAgents.SelectedValue) : Settings.LoginInfo.AgentId);
                cabRequestHeader.LocationId = (radioAgent.Checked == true ? Convert.ToInt32(hdnLocation.Value) : (int)Settings.LoginInfo.LocationID);
                cabRequestHeader.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                cabRequestHeader.ReferenceNumber = hdnRefNumber.Value;


                using (TransactionScope updateTransaction = new TransactionScope())
                {
                    cabHeaderID = cabRequestHeader.Save();
                    if (cabHeaderID > 0)
                    {
                        foreach (GridViewRow row in gvData.Rows)
                        {
                            IFormatProvider dateFormat = new CultureInfo("en-GB", true);
                            CabRequestDetails cabRequestDetails = new CabRequestDetails();
                            cabRequestDetails.DetId = cabHeaderID;
                            cabRequestDetails.FromDate = Convert.ToDateTime(row.Cells[1].Text,dateFormat);
                            cabRequestDetails.ToDate = Convert.ToDateTime(row.Cells[2].Text,dateFormat);
                            cabRequestDetails.PickUpTime = Convert.ToDateTime(row.Cells[3].Text);
                            cabRequestDetails.RentalType = row.Cells[4].Text;
                            cabRequestDetails.CabModel = row.Cells[5].Text;
                            cabRequestDetails.CabPreference = row.Cells[6].Text;
                            cabRequestDetails.PickUpFrom = row.Cells[7].Text;
                            cabRequestDetails.DropAt = row.Cells[8].Text;
                            cabRequestDetails.SpecialRequest = row.Cells[9].Text;
                            cabRequestDetails.Email = row.Cells[10].Text;
                            replyTo = string.IsNullOrEmpty(replyTo) ? cabRequestDetails.Email : replyTo;
                            cabRequestDetails.FlightNumber = row.Cells[11].Text.Replace("&nbsp;", "");
                            cabRequestDetails.AgentId = (radioAgent.Checked == true ? Convert.ToInt32(ddlAgents.SelectedValue) : Settings.LoginInfo.AgentId);
                            cabRequestDetails.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                            cabRequestDetails.ReferenceNumber = hdnRefNumber.Value;
                            cabRequestDetails.Status = "REQ";
                            cabRequestDetails.Save();
                        }
                        updateTransaction.Complete();
                    }
                }

                //Reading the bcc email and path of the email format from bke-App-config table then sending email.
                string data = string.Empty;
                string finalString = string.Empty;
                string bcc = string.Empty;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter sw = new System.IO.StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gvData.RenderControl(hw);
                data = sb.ToString();

                AgentAppConfig appConfig = new AgentAppConfig();
                appConfig.AgentID = Settings.LoginInfo.AgentId;
                AgentAppConfig config = appConfig.GetConfigData().Find(c => c.AppKey.ToUpper() == "CABREQUEST");
                if (config != null && !string.IsNullOrEmpty(config.AppValue) && !string.IsNullOrEmpty(config.Description))
                {
                    bcc = config.AppValue;//bcc email
                    StreamReader sr = new StreamReader(config.Description);//path of email pattern.
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        finalString += line;
                    }
                }
                
                Hashtable hashtable = new Hashtable();
                hashtable.Add("name", txtName.Text.Trim());
                hashtable.Add("mobileno", txtMobileNumber.Text.Trim());
                hashtable.Add("cabdata", data);

                List<string> toArray = new List<string>();
                toArray.Add(replyTo.TrimEnd(','));

                if(!string.IsNullOrEmpty(finalString))
                   CT.Core.Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], replyTo, toArray, "Cab Request", finalString, hashtable, bcc);

                Clear();
                GetCabReferenceNumber();
                Session["CabRequestData"] = null;
                gvData.Visible = false;
                btnSave.Visible = false;
                btnClearVal.Visible = false;
                txtName.Text = string.Empty;
                txtMobileNumber.Text = string.Empty;
                btnDelete.Visible = false;
                lblSuccessMsg.Style.Add("display", "block");
                lblSuccessMsg.Text = "SuccessFully Saved";
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in btnSave_Click." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvData.PageIndex = e.NewPageIndex;
            gvData.DataSource= (DataTable)Session["CabRequestData"];
            gvData.DataBind();

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btnClearVal_Click(object sender, EventArgs e)
        {
            Clear();
            Session["CabRequestData"] = null;
            gvData.Visible = false;
            btnSave.Visible = false;
            btnClearVal.Visible = false;
            txtName.Text = string.Empty;
            txtMobileNumber.Text = string.Empty;
            ddlAgents.SelectedIndex = -1;
            radioSelf.Checked = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int rowId = Convert.ToInt32(hdnId.Value);
                rowId++;
                DataTable dt = (DataTable)Session["CabRequestData"];
                for (int i = dt.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dt.Rows[i];
                    if (Convert.ToInt32(dr["Id"]) == rowId)
                        dr.Delete();
                }
                dt.AcceptChanges();
                Session["CabRequestData"] = dt;
                if (dt.Rows.Count > 0)
                {
                    BindGrid();
                }
                else
                {
                    gvData.Visible = false;
                    btnSave.Visible = false;
                    btnClearVal.Visible = false;
                }
                Clear();
                LoadLocations();
                lblSuccessMsg.Style.Add("display", "block");
                lblSuccessMsg.Text = "Successfully Deleted";
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CabRequestForm) Error in btnDelete_Click." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        // Override the VerifyRenderingInServerForm method to ensure that this control is nested in an HtmlForm server control, between a <form runat=server>
        // opening tag and a </form> closing tag.
        public override void VerifyRenderingInServerForm(Control control) { }
    }
}
