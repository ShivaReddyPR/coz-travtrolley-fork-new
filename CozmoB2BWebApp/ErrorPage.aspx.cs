using System;
using CT.TicketReceipt.Common;

public partial class ErrorPageGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrMsg.Text = Utility.ToString(Request.QueryString["Err"]);
        string ErrorCode = Convert.ToString(Request.QueryString["ReligareError"]);
        if (!string.IsNullOrEmpty(ErrorCode))
        {
            btnredirect.PostBackUrl = "ReligareInsurance.aspx";
            lblErrMsg.Text = Utility.ToString(Request.QueryString["ReligareError"]);
            Session["quotation"] = null;
            Session["religareHeader"] = null;
        }
        else
        {
            btnredirect.PostBackUrl = "HotelSearch.aspx?source=Flight";
        }
    }
}
