﻿using System;
using System.Web;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.Core;
using System.IO;
using System.Collections.Generic;

public partial class FleetBookingConfirm : CT.Core.ParentPage
{
    protected FleetItinerary itinerary = new FleetItinerary();
    protected decimal rateOfExchange = 1;
    protected decimal markup = 0, total = 0;
    protected MetaSearchEngine mse;
    protected string warningMsg = "";
    protected decimal totalCharge = 0;
    protected AgentMaster agency;
    protected decimal amountToCompare = 0;
    protected bool samePax = false;
    protected BookingResponse bookRes = new BookingResponse();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (Session["fleetItinerary"] != null)
                {
                    itinerary = Session["fleetItinerary"] as FleetItinerary;
                    if (Session["Markup"] != null)
                    {
                        markup = Convert.ToDecimal(Session["Markup"]);
                    }
                    rateOfExchange = (Settings.LoginInfo.ExchangeRate > 0 ? Settings.LoginInfo.ExchangeRate : 1);
                    if (itinerary.Price.AccPriceType == PriceType.NetFare)
                    {
                        total = Math.Round((itinerary.Price.NetFare + itinerary.Price.Markup - +itinerary.Price.Discount), itinerary.Price.DecimalPoint);
                    }
                    //calucating with services totalAmount
                    totalCharge = Math.Round(total + itinerary.Price.Tax, itinerary.Price.DecimalPoint);
                    markup += Math.Round(total, itinerary.Price.DecimalPoint);
                    if (!IsPostBack)
                    {
                        mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                        mse.SettingsLoginInfo = Settings.LoginInfo;
                       
                    }

                }
                else
                {
                    Response.Redirect("FleetSearch.aspx");
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
            PaymentMultiView.ActiveViewIndex = 1;
            lblError.Text = ex.Message;

        }
    }
    protected void imgMakePayment_Click(object sender, EventArgs e)
    {
        try
        {
            itinerary = Session["fleetItinerary"] as FleetItinerary;
            //getting here check balance
            agency = new AgentMaster(Settings.LoginInfo.AgentId);
            agency.UpdateBalance(0);

            //assign update balance to current agent id
            Settings.LoginInfo.AgentBalance = agency.CurrentBalance;

            amountToCompare = agency.CurrentBalance;

            decimal fleetCost = 0;

            fleetCost = itinerary.Price.GetAgentFleetPrice();
            if (fleetCost > amountToCompare)
            {
                lblMessage.Text = "Dear Agent you do not have sufficient balance to book Fleet.";
                return;
            }
            else
            {
                itinerary.ProductType = ProductType.Car;
                Product prod = (Product)itinerary;
                //Product prod = new Product();

                // Temp purpose as vijay need ASAP to club asv amount to pass to sayara WS :TODO ziya

                if (itinerary.Price.AsvAmount > 0)
                {
                    rateOfExchange = (itinerary.Price.RateOfExchange > 0 ? itinerary.Price.RateOfExchange : 1);
                    itinerary.Price.SupplierPrice = itinerary.Price.SupplierPrice + (itinerary.Price.AsvAmount / rateOfExchange);
                    itinerary.Price.NetFare = itinerary.Price.NetFare;
                }

                prod.ProductId = itinerary.ProductId;
                prod.ProductType = itinerary.ProductType;
                prod.ProductTypeId = itinerary.ProductTypeId;

                if (itinerary.ProductType != ProductType.Car)
                {
                    itinerary.ProductType = ProductType.Car;
                }
                itinerary.CancelId = "";
                itinerary.CreatedOn = DateTime.Now;
                itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
               
                if (Session["BookingMade"] != null)
                {
                    samePax = true;
                }
                //checking page refreshing
                Session["BookingMade"] = true;
                if (warningMsg.Length <= 0 && !samePax)
                {
                    mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                    bookRes = mse.Book(ref prod, Settings.LoginInfo.AgentId, BookingStatus.Ready, Settings.LoginInfo.UserID);

                    if (bookRes.Status != BookingResponseStatus.Successful)
                    {
                        string bookingMsg = "Price has been revised for this Fleet. Please search again to book this Fleet.";
                        PaymentMultiView.ActiveViewIndex = 1;
                        if (bookRes.Error != null && bookRes.Error.Length > 0)
                        {
                            bookingMsg = bookRes.Error;
                        }

                        //Show failed message
                        lblError.Text = bookingMsg;

                        Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloader");
                        PaymentMultiView.ActiveViewIndex = 1;
                    }
                    else
                    {
                        int numFiles = 1;
                        string str_image = "";

                        //Need to save files if any uploaded here.
                        if (itinerary.PassengerInfo != null)
                        {
                            try
                            {

                                #region Documents Upload Section
                                string dirFullPath = HttpContext.Current.Server.MapPath("~/" + CT.Configuration.ConfigurationSystem.SAYARAConfig["UploadFleetDoc"]) + @"\";
                                string clientFolder = dirFullPath + "Pax_" + itinerary.PassengerInfo[0].PaxId + @"\";

                                if (!Directory.Exists(clientFolder)) //Creating new folder based on paxid
                                {
                                    Directory.CreateDirectory(clientFolder);
                                }


                                if (Session["documentsPath"] != null) //Checking existing documents folder path
                                {
                                    if (!Directory.Exists(clientFolder))
                                    {
                                        Directory.CreateDirectory(clientFolder);
                                    }
                                    string[] files = Directory.GetFiles(Session["documentsPath"] as string);
                                    if (files.Length > 0)
                                    {
                                        foreach (string s in files) //Existing files coping to new folder
                                        {
                                            string fileName = System.IO.Path.GetFileName(s);
                                            string destinationFolder = System.IO.Path.Combine(clientFolder, fileName);
                                            File.Copy(s, destinationFolder, true);

                                        }
                                    }
                                }

                                if (Session["files"] != null) //new upload files saving in folder
                                {

                                    List<HttpPostedFile> filesList = Session["files"] as List<HttpPostedFile>;
                                    List<string> uploadedFiles = new List<string>();
                                    for (int i = 0; i < filesList.Count; i++)
                                    {
                                        string[] files = System.IO.Directory.GetFiles(clientFolder);
                                        numFiles = files.Length;
                                        numFiles = numFiles + 1;
                                        uploadedFiles = new List<string>() { clientFolder };
                                        HttpPostedFile file = filesList[i];
                                        string fileName = filesList[i].FileName;
                                        string fileExtension = filesList[i].ContentType;
                                        if (!string.IsNullOrEmpty(fileName))
                                        {
                                            fileExtension = Path.GetExtension(fileName);
                                            str_image = numFiles.ToString() + "_" + "Doc_" + fileName;
                                            string pathToSave = clientFolder + str_image;

                                            file.SaveAs(pathToSave); //Files saving new folder
                                        }
                                    }
                                }

                                #endregion
                            }
                            catch(Exception ex) { Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to update Agent Balance. Error:Test For Documents "+ex.ToString() , Request["REMOTE_ADDR"]); }

                        }

                        Session["fleetItinerary"] = itinerary;
                        Session["BookingResponse"] = bookRes;
                        //Clear all uploaded files sessions
                        Session["files"] = null;
                        Session["previouslyUploadedFiles"] = null;
                        Session["documentsPath"] = null;

                        //Load or save the invoice
                        int invoiceNumber = Invoice.isInvoiceGenerated(itinerary.FleetId, ProductType.Car);
                        Invoice invoice = new Invoice();
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FleetId, string.Empty, (int)Settings.LoginInfo.UserID, ProductType.Car, rateOfExchange);
                            invoice.Load(invoiceNumber);
                            invoice.Status = InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                            invoice.UpdateInvoice();

                        }
                        try
                        {
                            //Reduce Agent Balance
                            decimal total = 0;

                            if (itinerary.Price.AccPriceType == PriceType.NetFare)
                            {
                                total = (itinerary.Price.NetFare + itinerary.Price.Markup - itinerary.Price.Discount + itinerary.Price.Tax);
                            }

                            //No need to deduct from agent balance as it is addl markup only. We are not adding markup to total
                            //so no need to deduct addl markup from total now. commented by shiva on 28 Aug 2014 4:30 PM
                            //total = Math.Ceiling(total) - Convert.ToDecimal(Session["Markup"]);

                            total = Math.Ceiling(total);
                            if (bookRes.Status == BookingResponseStatus.Successful)
                            {
                                //Update agent balance, reduce the fleet booking amount from the balance.
                                agency = new AgentMaster(Settings.LoginInfo.AgentId);
                                agency.CreatedBy = Settings.LoginInfo.UserID;
                                agency.UpdateBalance(-total);

                                //after transaction update current agent balance
                                Settings.LoginInfo.AgentBalance -= total;

                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to update Agent Balance. Error: " + ex.Message, Request["REMOTE_ADDR"]);
                        }
                        Response.Redirect("FleetPaymentVoucher.aspx?ConfNo=" + bookRes.ConfirmationNo, false);
                    }
                }
                else//In Case of Warning message search again.
                {
                    if (warningMsg.Length > 0)
                    {
                        PaymentMultiView.ActiveViewIndex = 1;
                        lblError.Text = warningMsg;
                    }
                    else if (samePax)
                    {
                        lblError.Text = "Duplicate bookings are not allowed for the same fleet passengers";
                        PaymentMultiView.ActiveViewIndex = 1;
                    }

                }

            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed Fleet Booking. Error: " + ex.Message, Request["REMOTE_ADDR"]);
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErr");
            PaymentMultiView.ActiveViewIndex = 1;

            if (!string.IsNullOrEmpty(ex.Message))
            {
                lblError.Text = ex.Message;
            }
            else
            {
                lblError.Text = "There was a problem booking your Fleet Item. Please try after some time.";
            }
        }
    }
}
