﻿using System;
using CT.Core;
using System.Web.UI;
using Visa;
using CT.TicketReceipt.BusinessLayer;

public partial class AddVisaCountry : CT.Core.ParentPage
{
    public int UpdateCountry = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        if (!IsPostBack)
        {
            Page.Title = "Add Country";

            ddlRegion.DataSource = VisaRegion.GetActiveRegionList(); ;  
            ddlRegion.DataTextField = "regionName";
            ddlRegion.DataValueField = "regionId";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0,"Select");

            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int visaCountryId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out visaCountryId);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaCountry country = new VisaCountry(visaCountryId);
                    if (country != null)
                    {
                        UpdateCountry = 1;
                        ddlRegion.SelectedIndex = ddlRegion.Items.IndexOf(ddlRegion.Items.FindByValue(country.RegionId.ToString()));
                        txtCountryCode.Text = country.CountryCode.ToUpper().Trim();
                        txtCountryName.Text = country.CountryName;
                        txtARCountryName.Text = country.CountryNameAR;
                        btnSave.Text = "Update";
                        Page.Title = "Update Country";
                    }
                }

                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaCountry,Err:" + ex.Message,"");
                }           
            }            
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        int errMsg = 0;
        try
        {
           
            VisaCountry country = new VisaCountry();
            country.CountryCode = txtCountryCode.Text.Trim();
            country.CountryName = txtCountryName.Text.Trim();
            country.RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            country.CountryNameAR = txtARCountryName.Text.Trim();
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    country.CountryId = Convert.ToInt32(Request.QueryString[0]);
                    country.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    errMsg = country.UpdateCountry();
                }
            }
            else
            {
                country.IsActive = true;
                country.CreatedBy = (int)Settings.LoginInfo.UserID;
                errMsg = country.Save();
            }
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaCountry,Err:" + ex.Message, "");
        }
        if (errMsg == 0)
        {
            Response.Redirect("VisaCountryList.aspx");
        }
        else if (errMsg == 1)
        {
            lblErrorMessage.Text = "Visa country code already exists";
        }
        else if (errMsg == 2)
        {
            lblErrorMessage.Text = "Visa country name already exists";
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
