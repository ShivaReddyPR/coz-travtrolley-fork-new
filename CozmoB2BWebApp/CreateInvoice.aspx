<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateInvoice.aspx.cs" Inherits="CreateInvoiceGUI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flight Invoice</title>
      <link rel="stylesheet" href="css/main-style.css" />
    <script type="text/javascript">
    var Ajax;

    if (window.XMLHttpRequest) {
        Ajax = new XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject('Microsoft.XMLHTTP');
    }
    function printPage() {
        document.getElementById('btnPrintInv').style.display = "none";
        window.print();
//        var divToPrint = document.getElementById('middle-container');
//        var popupWin = window.open('', 'FlightInvoice', 'width=850,height=500');
//        popupWin.document.open();
//        popupWin.document.write('<html><head><style media=\"screen\">.FlightInvoice td { line-height:22px; font-size:13px; }.FlightInvoice h3 {  background:url(images/cozmovisa-sprite.jpg) repeat-x; height:20px;margin-top:10px; color:#fff; padding-left:10px; }</style></head><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
//        popupWin.document.close();
        setTimeout('showButtons()', 1000);
    }
    function showButtons() {
        document.getElementById('btnPrintInv').style.display = "block";
    }

    function HideEmailDiv() {
        document.getElementById('emailSent').style.display = 'none';
        document.getElementById('emailBlock').style.display = 'none';
    }

    function ShowEmailDivD() {
        document.getElementById("addressBox").value = '';
        document.getElementById('messageText').innerHTML = '';
        document.getElementById('emailBlock').style.display = 'block';
        document.getElementById('addressBox').focus();
    }

    function SendMail() {
        var ValidEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z][a-zA-Z]+)$/;
        var addressList = document.getElementById("addressBox").value;
        if ((addressList) == '') {
            document.getElementById('emailSent').style.display = 'block';
            document.getElementById('messageText').innerHTML = 'Please fill the email address';
            return;
        }
        if (!ValidEmail.test((addressList))) {
            document.getElementById('emailSent').style.display = 'block';
            document.getElementById('messageText').innerHTML = 'Please fill correct email address';
            return;
        }
        var invoiceNumber = document.getElementById('invoiceNo').value;
        var paramList = 'invoiceNumber=' + invoiceNumber;
        paramList += "&addressList=" + addressList;
        paramList += "&isInvoice=true";
        var url = "EmailItineraryPage";

        Ajax.onreadystatechange = DisplayMessage;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }
    function DisplayMessage() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                HideEmailDiv();
                document.getElementById('emailSent').style.display = 'block';
                document.getElementById('messageText').innerHTML = 'Email Sent Successfully';
            }
        }
    }
</script>
</head>
<body>
<%--<div id="middle-container">--%>
<div style="padding: 10px;">
    <form id="form1" runat="server">
    <div>
<div style="position:absolute; right:700px; top:530px; display:none"  id="emailBlock">
                    
                    
                    <div style="border: solid 4px #ccc; width:200px" class="ShowMsgDiv"> 
                    <div class="showMsgHeading"> Enter Email Address <a style=" float:right; padding-right:10px" class="closex" href="javascript:HideEmailDiv()">X</a></div>
                    
                           <div style=" padding:10px">
                                    <input style=" width:100%; border: solid 1px #7F9DB9" id="addressBox" name="" type="text"/></div>
                               <div style=" padding-left:10px; padding-bottom:10px">
                                    <input onclick="SendMail()" type="button" value="Send mail" />
                                    
                                    </div>
                                    
                                    
                                    
                    </div>
                    

</div>

</div>
<div>

<input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 








<table id="tblInvoice" width="100%" border="0" cellspacing="0" cellpadding="0" style="display:none;">
  <tr>
    <td>
    <table class="FlightInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" align="left">  <%--<img src="images/logo.jpg" alt="Agent Logo" height="50">--%>
    <%--<asp:Image ID="imgLogo" Width="80px" Height="40px" runat="server" AlternateText="AgentLogo" ImageUrl="" />--%>
    </td>
    <td width="33%" align="center"><h1 style=" font-size:26px">Invoice</h1>
         <div class="small">
             <%if (location.CountryCode == "IN")
                 { %>
             GSTIN:100019554300003
             <%}
    else {%>
             TRN:100019554300003
             <%} %>

         </div>
    </td>
    <td width="33%" align="right"><input style="width: 100px;" id="btnPrint" onclick="return printPage();" type="button"
                        value="Print Invoice" /></td>
  </tr>
</table>
    </td>
  </tr>
 
 
  <tr>
    <td colspan="2">
    <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
      <tbody>
                        <tr>
                            <td width="50%" style="vertical-align: top;">
                                <table width="100%" style="font-size: 12px;font-family: 'Arial'">
                                    <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Client Details</h3>                                            </td>
                                        </tr>
                                     <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Agency Name: </b></td>
                                            <td style="vertical-align: middle;"> <% = agency.Name %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Agency Code: </b></td>
                                            <td style="vertical-align: middle;"> <% = agency.Code %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Agency Address: </b></td>
                                            <td style="vertical-align: middle;"><% = agencyAddress %></td>
                                        </tr>
                                            <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>TelePhone No: </b></td>
                                                <td style="vertical-align: middle;">
                                                    <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                        {%>
                                                    Phone:
                                                    <% = agency.Phone1%>
                                                    <%  }
                                                        else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                        { %>
                                                    Phone:
                                                    <% = agency.Phone2%>
                                                    <%  } %>
                                                    <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                        { %>
                                                    Fax:
                                                    <% = agency.Fax %>
                                                    <%  } %>                                                </td>
                                        </tr>
                                        <%if (location.CountryCode == "IN")
                                            { %>
                                        <tr>
                                            <td style="vertical-align: middle; width: 120px;"><b>GST Number: </b></td>

                                            <td style="vertical-align: middle;"><% = location.GstNumber %></td>
                                        </tr>
                                        <%}
                                        else if (agency.Telex != null && agency.Telex.Length > 0)
                                        {%>

                                        <tr>
                                            <td style="vertical-align: middle; width: 120px;"><b>TRN Number: </b></td>

                                            <td style="vertical-align: middle;"><% = agency.Telex %></td>
                                        </tr>
                                        <%} %>
                                            
                                     
                                    </tbody>
                                </table>                            </td>
                          <td width="50%" style="vertical-align: top;">
                                <table width="90%" align="right" style="font-size: 12px;font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px; font-family: 'Arial';">Invoice Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Invoice No: </b></td>
                                            <td style="vertical-align: middle;"><%=invoice.CompleteInvoiceNumber%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Invoice Date: </b></td>
                                            <td style="vertical-align: middle;"><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>PNR NO: </b></td>
                                            <td style="vertical-align: middle;"> <% = pnr %></td>
                                        </tr>
                                    </tbody>
                                </table>                          </td>
                        </tr>
                    </tbody>
                </table>
     
     </td>
     </tr>
   <tr>
   <td colspan="2">
     <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
      <tbody>
                        <tr>
                            <td width="50%" style="vertical-align: top;">
                            <table width="100%" style="font-size: 12px;font-family: 'Arial'">
                              <tbody>
                                <tr>
                                  <td colspan="4" style="vertical-align: top;"><h3 style="font-size: 16px;font-family: 'Arial';">Booking Details</h3></td>
                                </tr>
                                <tr height="30px">
                                  <td style="vertical-align: middle;width: 91px;"><b>Booking Date: </b></td>
                                  <td style="vertical-align: middle;"><%=itinerary.CreatedOn.ToString("dd MMM yy") %></td>
                                  
                                  <td style="vertical-align: middle;width: 91px;"><b>Travel Date: </b></td>
                                  <td style="vertical-align: middle;"><%=itinerary.TravelDate.ToString("dd MMM yy") %></td>
                                  
                                </tr>
                               <%-- <tr>
                                  <td style="vertical-align: top;width: 91px;"><b>Check In: </b></td>
                                  <td style="vertical-align: top;"></td>
                                  <td style="vertical-align: top;width: 91px;"><b>Check Out: </b></td>
                                  <td style="vertical-align: top;"></td>
                                </tr>--%>
                              </tbody>
                            </table></td>
                            
                            <td width="50%" style="vertical-align: top;">
                                <table width="90%" align="right" style="font-size: 12px;font-family: 'Arial'">
      <tbody>
                                        <tr>
                                            <td style="vertical-align: top;" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Flight Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Flight Name: </b></td>
                                            <td style="vertical-align: middle;"><%=itinerary.AirlineCode %>  <%=itinerary.Segments[0].FlightNumber%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Sectors: </b></td>
                                            <td style="vertical-align: middle;"><%= routing %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Class: </b></td>
                                            <td style="vertical-align: middle;"><%= bookingClass %></td>
                                        </tr>
                                      <%--  <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Address1: </b></td>
                                            <td style="vertical-align: top;"></td>
                                        </tr>
                                           <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>City: </b></td>
                                            <td style="vertical-align: top;"></td>
                                        </tr>--%>
                                  </tbody>
                                </table>                          </td>
                        </tr>
                    </tbody>
                </table> 
       </td>
  </tr>
  
  
  
  <tr>
    <td colspan="2" valign="top">
    
    <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td width="50%" style="vertical-align: top;">
                                                <%--<h3 style="font-size: 16px; font-family: 'Arial';">Lead Passenger Details</h3>                                            --%>
                                                <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4" style="vertical-align: top;">
                                                                <h3 style="font-size: 16px; font-family: 'Arial';">
                                                                    Lead Passenger Details</h3>
                                                            </td>
                                                        </tr>
                                                        <tr height="30px">
                                                            <td style="vertical-align: middle; width: 120px;">
                                                                <b>Pax Name: </b>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                                <% = itinerary.Passenger[0].FirstName%>
                                                                <%= itinerary.Passenger[0].LastName %></td>
                                                                <td style="vertical-align: middle; width: 120px;">
                                                                <b>Pax Type: </b>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                                <%= itinerary.Passenger[0].Type %></td>
                                                                
                                                        </tr>
                                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Ticket NO: </b></td>
                                            <td style="vertical-align: middle;"><% = ticketList[0].TicketNumber %></td>
                                        </tr>
                                                       <%-- <tr>
                                                            <td style="vertical-align: top; width: 91px;">
                                                                <b>Check In: </b>
                                                            </td>
                                                            <td style="vertical-align: top;">
                                                            </td>
                                                            <td style="vertical-align: top; width: 91px;">
                                                                <b>Check Out: </b>
                                                            </td>
                                                            <td style="vertical-align: top;">
                                                            </td>
                                                        </tr>--%>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="50%" style="vertical-align: top;">
                                            <table class="FlightInvoice" border="0" width="90%" align="right" style="font-size: 12px;font-family: 'Arial'">
                            <tbody>
                             
                              <tr>
                                <td style="vertical-align: top;" colspan="3"><h3 style="font-size: 16px;font-family: 'Arial';">Rate Details</h3></td>
                              </tr>
                                <tr>
                                    <td style="vertical-align: middle; width: 120px;">
                                        <b>Base Fare: </b>
                                    </td>
                                    <%--<td style="vertical-align: middle;">
                                        <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %>
                                        <%=((totalPrice - ticketList[0].Price.Markup) * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </td>--%>
                                    <td style="vertical-align: middle;width:100px;" align="right">
                                    <asp:Label ID="lblBaseFare" runat="server" Text="" ></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; width: 120px;">
                                        <b>Taxes & Fees: </b>
                                    </td>
                                    <td style="vertical-align: middle;" align="right">
                                    <asp:Label ID="lblTax" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; width: 120px;">
                                        <b><asp:Label ID="lblMarkup" runat="server" Text="Markup:" Visible="false"></asp:Label></b>
                                    </td>
                                    <td style="vertical-align: middle;" align="right">
                                    <asp:Label ID="lblMarkupVal" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <%if(itinerary.IsLCC)
                                  { %>
                                   <tr>
                                    <td style="vertical-align: middle; width: 120px;">
                                        <b>Baggage Fee: </b>
                                    </td>
                                    <td style="vertical-align: middle; width:100px;" align="right">
                                    <asp:Label ID="lblBaggage" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <%} %>
                                 <tr>
                                <td style="vertical-align: middle;width: 120px;"><b>Gross Amount: </b></td>
                                <td style="vertical-align: middle;" align="right">
                                <asp:Label ID="lblGross" runat="server" Text=""></asp:Label></td>
                              </tr>
                              
                                <%if (ticketList[0].Price.AsvAmount > 0)
                                  {%> 
                                <tr>
                                    <td style="vertical-align: middle; width: 120px;">
                                        <b>Add-Markup: </b>
                                    </td>
                                    <%--<td style="vertical-align: middle;">
                                        <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %>
                                        <%=ticketList[0].Price.Markup.ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </td>--%>
                                    <td style="vertical-align: middle;" align="right">
                                    <asp:Label ID="lblAddMarkup" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <%} %>
                                
                                <tr >
                                    <td style="vertical-align: middle; width: 120px;"><b><asp:Label ID="lblinpVAT" runat="server" Text="" Visible="false"></asp:Label></b></td>
                                    <td style="vertical-align: middle;" align="right">
                                        <asp:Label ID="lblinputVAT" Visible="false" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="vertical-align: middle; width: 120px;"><b><asp:Label ID="lbloutVAT" runat="server" Text="" Visible="false"></asp:Label></b></td>
                                    <td style="vertical-align: middle;" align="right">
                                        <asp:Label ID="lblVAT" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="vertical-align: middle; width: 120px;"><b><asp:Label ID="lblIGST" runat="server" Visible="false"></asp:Label></b></td>
                                    <td style="vertical-align: middle;" align="right">
                                        <asp:Label ID="lblIGSTValue" Visible="false" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="vertical-align: middle; width: 120px;"><b><asp:Label ID="lblCGST" runat="server" Visible="false"></asp:Label></b></td>
                                    <td style="vertical-align: middle;" align="right">
                                        <asp:Label ID="lblCGSTValue" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                 <tr >
                                    <td style="vertical-align: middle; width: 120px;"><b><asp:Label ID="lblSGST" runat="server" Visible="false"></asp:Label></b></td>
                                    <td style="vertical-align: middle;" align="right">
                                        <asp:Label ID="lblSGSTValue" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>


                              <tr style="display:none">
                                <td style="vertical-align: middle;width: 120px;"><b>Net Amount: </b></td>
                                <td style="vertical-align: middle;" align="right">
                                <asp:Label ID="lblNetAmount" runat="server" Text=""></asp:Label>
                                </td>
                              </tr>
                              <tr style="display:none">
                                <td style="vertical-align: middle;width: 120px;"><b>Net Receivable: </b></td>
                                <td style="vertical-align: middle;" align="right">
                                <asp:Label ID="lblNetReceivable" runat="server" Text=""></asp:Label>
                                </td>
                              </tr>
                              <tr>
                                <td style="vertical-align: middle;width: 120px;"><b>Total Amount: </b></td>
                                <td style="vertical-align: middle;" align="right">
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                                            
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pax Name: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.Passenger[0].FirstName%> <%= itinerary.Passenger[0].LastName %>
                                            
                                             </tr>--%>
                                    </tbody>
                                </table>
                                
                                
                                </td>
   </tr>
 
  <tr>

    <td colspan="2">
    
    <table border="0" class="FlightInvoice" width="100%" style="font-family: 'Arial'">
<tr>
                 <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Invoiced By: </b>
                 </td>
                 <td style="vertical-align: top;">
                     Cozmo Travel
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Created By: </b>
                 </td>
                 <td style="vertical-align: top;">
                     <%= member.FirstName + " " + member.LastName%>
                 </td>
             </tr>
               <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Location: </b>
                 </td>
                 <td style="vertical-align: top;">
                     <%= itinerary.LocationName%>
                 </td>
             </tr>
    <%--         <tr>
                 <td>
                      <%if (Request["invoiceModified"] == null)%>
                  <%{%>
                     <a href="AgentBookingQueue.aspx" >Back</a>
                  <%}%>
                  
                  <%else%>
                  <%{%>
                      <a href="AgentBookingQueue.aspx" >Back</a>   
                  <%}%>
                 </td>
             </tr>--%>
             <tr>
             <td style="width:130px">
             <div class="width-100 fleft text-right" style="display:none"><img src="images/Email1.gif" />
           <a href="javascript:ShowEmailDivD()">Email Invoice</a>
          </div>
          
          </td>
          <td style="width:160px">
          <span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;"></span> 
              </td>
                    <td>
                        <div class="email-message-parent" style="display:none;" id="emailSent">
                    <span  style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                    </span>
                    
           </div>
             </td>
             </tr>
         </table>
    </td>
  </tr>
</table>

    <table width="100%" style="font-size:12px; font-family:Arial, sans-serif">
        <tr>
            <td width="33%" align="right" colspan="3"><input style="width: 100px;" id="btnPrintInv" onclick="return printPage();" type="button"
                        value="Print Invoice" /></td>
        </tr>
        <tr>            
            <td align="center" colspan="3">
                <%AgentMaster agentMaster = new AgentMaster();
                    //For bookings done in India location use booked agency details in the header
                    //Otherwise use Base Agent details in the header
                    if (location.CountryCode == "IN")
                    {
                        agentMaster = agency;
                    }
                    else
                    {
                        //Load Base Agent for booking outside India
                        agentMaster = new AgentMaster(1);
                    }%>
                <table width="100%">
                    <tr>
                        <td rowspan="7" colspan="2" align="left" style="margin-left:10px;" >
                            <img alt="Logo" src="Uploads/AgentLogos/<%=agentMaster.ImgFileName %>" width="159px" height="51px"  />
                        </td>
                        
                    </tr>
                    <tr>
                        <td colspan="2" align="right"><%=agentMaster.Name %></td>                        
                    </tr>
                    <tr>
                        <td colspan="2" align="right"><%=agentMaster.Address %></td>
                    </tr>
                    <tr><td align="right">Email: <%=agentMaster.Email1 %> &nbsp; <%=(string.IsNullOrEmpty(agentMaster.Website) ? "" : "| Web: " + agentMaster.Website) %></td></tr>
                    <tr><td align="right">Phone: <%=agentMaster.Phone1 %> &nbsp; <%=(string.IsNullOrEmpty(agentMaster.Fax) ? "" : "| Fax: " + agentMaster.Fax) %></td></tr>
                    <%
                        if (location.CountryCode == "IN")
                        { %>
                    <tr><td colspan="2" align="right">GSTIN: <%=location.GstNumber %></td></tr>
                    <%}else{ %>
                    <tr><td colspan="2" align="right">TRN No: <%=agentMaster.Telex %></td></tr>
                    <%} %>
                </table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td colspan="3" align="center">TAX INVOICE</td>
        </tr>
        <tr>
            <td colspan="3"><hr /></td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                CATEGORY OF SERVICE - AIR TRAVEL AGENT
            </td>
        </tr>
        <tr>
            
            <td style="width:40%" valign="top">
                <table width="100%" style="border:2px solid #1C498A;background:#A0C3FF;height:180px">
                    <tr>
                        <td><%=parentAgent.Name %></td>
                    </tr>
                    <tr>
                        <td><%=parentLocation.Address %></td>
                    </tr>
                    <tr><td>Email: <%=parentAgent.Email1 %></td></tr>
                    <tr><td>Phone: <%=parentAgent.Phone1 %></td></tr>
                    <%if (parentLocation.CountryCode == "IN")
                            { %>
                    <tr><td>State: <%=(parentLocation.StateId > 0 ? parentLocation.StateId.ToString() : "") %> &nbsp;&nbsp;&nbsp; StateName: <%=GetStateName(parentLocation.StateId, parentLocation.CountryCode) %></td></tr>                    
                    
                    <tr><td>GTSIN: <%=parentLocation.GstNumber %></td></tr>
                    <%}
                            else
                            { %>
                    <tr><td>TRN No: <%=parentAgent.Telex %></td></tr>
                    <%} %>
                </table>
            </td>
            <td style="width:20%" valign="top">
                <table width="100%"  style="border:2px solid #1C498A;background:#A0C3FF;height:180px">
                    <tr>
                        <td colspan="3" align="center">
                            INVOICE DETAIL
                        </td>
                    </tr>
                    <tr>
                        <td style="width:45%">Invoice</td>
                        <td style="width:10%">:</td>
                        <td style="width:45%"><%=invoice.DocTypeCode %> <%=invoice.DocumentNumber %></td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>:</td>
                        <td><%=invoice.CreatedOn.ToString("dd/MM/yyyy") %></td>
                    </tr>                    
                    <tr>
                        <td>PNR No.</td>
                        <td>:</td>
                        <td><%=itinerary.PNR %></td>
                    </tr>
                </table>
            </td>
            <td style="width:40%" valign="top">
                <table width="100%" style="border:2px solid #1C498A;background:#A0C3FF;height:180px">
                     <tr>
                        <td><%=agency.Name %></td>
                    </tr>
                    <tr>
                        <td><%=location.Address %></td>
                    </tr>                    
                    <%if (location.CountryCode == "IN")
                            { %>
                    <tr><td>State: <%=(location.StateId > 0 ? location.StateId.ToString() : "") %> &nbsp;&nbsp;&nbsp; StateName: <%=GetStateName(location.StateId,location.CountryCode) %></td></tr>                    
                    <tr><td>GTSIN: <%=location.GstNumber %></td></tr>
                    <%}
                            else
                            { %>
                    <tr><td>TRN No: <%=agency.Telex %></td></tr>
                    <%} %>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">Your account has been debited for the Purchase of Following tickets</td>
            <td>AIRLINE: <%CT.Core.Airline airline = new CT.Core.Airline(); airline.Load(itinerary.Segments[0].Airline); %><%=airline.AirlineName %></td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%" style="border:1px solid #1C498A;" cellpadding="0" cellspacing="0">
                    <tr style="background:#A0C3FF;border:1px solid #1C498A;">
                        <th style="border:1px solid #1C498A;">Ticket Number</th>
                        <th style="border:1px solid #1C498A;">Pax Name</th>
                        <th style="border:1px solid #1C498A;">Sector</th>
                        <th style="border:1px solid #1C498A;">Class</th>
                        <th style="border:1px solid #1C498A;">Flight No.</th>
                        <th style="border:1px solid #1C498A;">Date of Travel</th>
                        <th style="border:1px solid #1C498A;">Basic Fare</th>                        
                        <%if (location.CountryCode == "IN")
                                { %>
                        <th style="border:1px solid #1C498A;">YQ Tax</th>
                        <th style="border:1px solid #1C498A;">YR Tax</th>
                        <th style="border:1px solid #1C498A;">K3 Tax</th>
                        <%}
                                else
                                { %>
                        <th style="border:1px solid #1C498A;">K7 Tax</th>
                        <%} %>
                        <th style="border:1px solid #1C498A;">Airline Tax</th>
                        <th style="border:1px solid #1C498A;">Total</th>
                    </tr>
                    <% decimal TotalFare = 0m, BaseFare = 0m, Tax = 0m, Markup = 0m, GrandTotal = 0m, GST = 0m, Baggage = 0m, InputVat = 0m, addlMarkup = 0m, IGST = 0, CGST = 0, SGST = 0, discount = 0, TransFee = 0m, AddlTransFee = 0m, TransactionFees = 0m, _meal = 0m, CommEarned = 0m, TDSDeducted = 0m, _seat = 0m;
                        int CGSTPer = 0, IGSTPer = 0, SGSTPer = 0;

                        decimal k3TaxComponent = 0;
                        //For TBOAir Corp do not show Commission and calculate Commission
                        //by default IsServiceTaxOnBaseFarePlusYQ will be false normal bookings
                        //so for TBOAir corp we are saving TRUE in this column
                        bool showCommission = !itinerary.Passenger[0].Price.IsServiceTaxOnBaseFarePlusYQ;
                        List<GSTTaxDetail> taxDetList = new List<GSTTaxDetail>();
                        taxDetList = GSTTaxDetail.LoadGSTDetailsByPriceId(itinerary.Passenger[0].Price.PriceId);
                        if (taxDetList != null && taxDetList.Count > 0)
                        {
                            CGST += taxDetList.Where(p => p.TaxCode == "CGST").Sum(p => p.TaxAmount);
                            SGST += taxDetList.Where(p => p.TaxCode == "SGST").Sum(p => p.TaxAmount);
                            IGST += taxDetList.Where(p => p.TaxCode == "IGST").Sum(p => p.TaxAmount);
                            GSTTaxDetail objTaxDet = taxDetList.Where(p => p.TaxCode == "CGST").FirstOrDefault();
                            if (objTaxDet != null)
                            {
                                CGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                            }
                            objTaxDet = taxDetList.Where(p => p.TaxCode == "SGST").FirstOrDefault();
                            if (objTaxDet != null)
                            {
                                SGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                            }
                            objTaxDet = taxDetList.Where(p => p.TaxCode == "IGST").FirstOrDefault();
                            if (objTaxDet != null)
                            {
                                IGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                            }
                        }
                        //Get the total Adult count for calculating commission and TDS as we are saving these values for Adult only
                        //This calculations are only applicable for TBOAir bookings made with India Locations
                        int adultCount = 0;
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir && location.CountryCode == "IN")
                        {
                            adultCount = itinerary.Passenger.AsQueryable().Count(x => x.Type == PassengerType.Adult);
                            FlightPassenger pax = itinerary.Passenger[0];
                            CommEarned = pax.Price.AgentCommission + pax.Price.AgentPLB + pax.Price.IncentiveEarned;
                            TDSDeducted = pax.Price.TdsCommission + pax.Price.TDSIncentive + pax.Price.TDSPLB;
                        }
                        string sectors = GetSegments(), flightNums = GetFlightNumbers(), travelDates = GetTravelDates();
                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {
                            FlightPassenger pax = itinerary.Passenger[i];
                            Ticket ticket = ticketList[i];
                            //If booking with India location do not add HandlingFee in BaseFare
                            if (location.CountryCode == "IN")
                            {
                                BaseFare = pax.Price.PublishedFare;
                            }
                            else
                            {
                                BaseFare = pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                            }
                            if (itinerary.FlightBookingSource == BookingSource.TBOAir && pax.Type == PassengerType.Adult && location.CountryCode == "IN" && showCommission)
                            {
                                BaseFare += ((CommEarned - TDSDeducted) / adultCount);
                            }
                            InputVat += pax.Price.InputVATAmount;
                            if (location.CountryCode != "IN")
                            {
                                if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                {
                                    BaseFare -= pax.Price.InputVATAmount;
                                }
                                else
                                {
                                    InputVat = 0;
                                }
                            }

                            Tax = (pax.Price.Tax);
                            if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                Tax += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                            }
                            //If booking with India location add HandlingFee in Tax
                            if (location.CountryCode == "IN")
                            {
                                Tax += pax.Price.HandlingFeeAmount;
                            }
                            if (itinerary.FlightBookingSource == BookingSource.OffLine)
                            {
                                TransactionFees += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                            }
                            if(!string.IsNullOrEmpty(itinerary.RoutingTripId))
                             {
                                k3TaxComponent += (location.CountryCode == "IN" ? GetK3Tax(i) : 0);
                             }
                            
                            decimal TaxComp = (location.CountryCode == "IN" ? GetK3Tax(i) + GetYQTax(i) + GetYRTax(i) : GetK7Tax(i));
                            if (Tax > TaxComp || (pax.Type==PassengerType.Infant &&  Tax>=TaxComp))
                            {
                                Tax -= TaxComp;
                            }
                            else
                            {
                                TaxComp = 0;
                            }
                            Markup += pax.Price.Markup + pax.Price.B2CMarkup;
                            if (Settings.LoginInfo.AgentId > 1 && location.CountryCode != "IN")
                            {
                                Tax += Markup;
                                Markup = 0;
                            }
                            discount += pax.Price.Discount;
                            TotalFare += BaseFare + Tax + TaxComp;
                            Baggage += pax.Price.BaggageCharge;
                            _meal += pax.Price.MealCharge;
                            _seat += pax.Price.SeatPrice;
                            GST += pax.Price.OutputVATAmount;
                            addlMarkup += pax.Price.AsvAmount;
                            TransFee += pax.Price.TransactionFee;
                            AddlTransFee += pax.Price.AdditionalTxnFee;
                            if (itinerary.FlightBookingSource == BookingSource.OffLine)
                            {
                                Markup = Markup + addlMarkup;
                                addlMarkup = 0; //for offline entry we are adding addl Markup in mark up , so assigning addlmarkup =0
                            }


                            if (location.CountryCode != "IN")
                            {
                                GrandTotal = TotalFare + Markup + GST + Baggage + InputVat + addlMarkup - discount + _meal + _seat;
                            }
                            else
                            {
                                if (showCommission)
                                {
                                    GrandTotal = TotalFare + Markup + GST + Baggage + InputVat - discount + _meal + _seat - CommEarned + TDSDeducted;
                                }
                                else
                                {
                                    GrandTotal = TotalFare + Markup + GST + Baggage + InputVat - discount + _meal + _seat;
                                }
                            }
                            if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                GrandTotal = Math.Ceiling(GrandTotal);
                            }
                            if (itinerary.FlightBookingSource == BookingSource.OffLine)
                            {
                                GrandTotal -= TransactionFees;
                            }
                            else
                            {
                                GrandTotal = Math.Round(GrandTotal, agency.DecimalValue);
                            }

                            %>
                    <tr style="text-align:center;border:1px solid #1C498A;">
                        <td align="left" style="border:1px solid #1C498A;"><%=ticket.TicketNumber %></td>
                        <td align="left" style="border:1px solid #1C498A;"><%=pax.Title %>.<%=pax.FirstName %> <%=pax.LastName %></td>
                        <td style="border:1px solid #1C498A;"><%=sectors %></td>
                        <td style="border:1px solid #1C498A;"><%=itinerary.Segments[0].BookingClass %></td>
                        <td style="border:1px solid #1C498A;"><%=flightNums %></td>
                        <td style="border:1px solid #1C498A;"><%=travelDates %></td>
                        <td style="border:1px solid #1C498A;"><%=(BaseFare).ToString("N" + agency.DecimalValue) %></td>
                        <%if (location.CountryCode == "IN")
    { %>
                        <td style="border:1px solid #1C498A;"><%=(TaxComp > 0 ? GetYQTax(i) : 0).ToString("N" + agency.DecimalValue) %></td>
                        <td style="border:1px solid #1C498A;"><%=(TaxComp > 0 ? GetYRTax(i) : 0).ToString("N" + agency.DecimalValue) %></td>
                        <td style="border:1px solid #1C498A;"><%=(TaxComp > 0 ? GetK3Tax(i) : 0).ToString("N" + agency.DecimalValue) %></td>
                        <%}
    else
    { %>
                        <td style="border:1px solid #1C498A;"><%=(TaxComp > 0 ? GetK7Tax(i) : 0).ToString("N" + agency.DecimalValue) %></td>
                        <%} %>
                        <td style="border:1px solid #1C498A;"><%=(Tax).ToString("N" + agency.DecimalValue) %></td>
                        <td style="border:1px solid #1C498A;text-align: right"><%=(BaseFare + Tax + TaxComp).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%} %>
                    
                </table>
            </td>
        </tr>
       
        <tr >
            <td align="left" valign="bottom" style="text-transform:uppercase">
                <%=itinerary.Passenger[0].Price.Currency %> <%=ConvertToWords((GrandTotal).ToString()) %>
            </td>            
            <td colspan="2">
                <table   style="width: 100%">
                    <tr>
                        <td style="width: 85%; text-align: right">Total Gross Fare</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(TotalFare-k3TaxComponent).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%if (location.CountryCode == "IN" && itinerary.FlightBookingSource == BookingSource.TBOAir && showCommission)
                        { %>
                    <tr>
                        <td style="width: 85%; text-align: right">(-)Commission Earned</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(CommEarned).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                     <tr>
                        <td style="width: 85%; text-align: right">(+)TDS on Commission</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(TDSDeducted).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%} %>
                    <%if (Settings.LoginInfo.AgentId == 1)
                        {%>
                                   <tr>
                        <td style="width: 85%; text-align: right">(+)Markup</td>
                        <td style="width: 5%">:</td>
                                      

                        <td style="width: 10%; text-align: right"><%=Markup.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%}%>
    <%else if (location.CountryCode == "IN")
    { %>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Service Charge</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=Markup.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%}

                        if (addlMarkup > 0 && location.CountryCode != "IN" && itinerary.FlightBookingSource != BookingSource.OffLine)
                        {%>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Addl Markup</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=addlMarkup.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%} if (itinerary.IsLCC)
    { %>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Baggage</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(Baggage).ToString("N" + agency.DecimalValue) %></td>
                    </tr>

                    <%if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.SalamAir) %>
                    <%{ %>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Meal</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(_meal).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Seat</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(_seat).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%} %>

                    <%}%>
                    <%if (!string.IsNullOrEmpty(itinerary.RoutingTripId))
                        { %>
                     <tr>
                        <td style="width: 85%; text-align: right">(+)K3 Tax</td>
                        <td style="width: 5%">:</td>        
                        <td style="width: 10%; text-align: right"><%=(k3TaxComponent).ToString("N" + agency.DecimalValue)%></td>
                    </tr>
                    <%} %>
                    
       <% if (location.CountryCode != "IN")
           {
               if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
               {%>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Input VAT</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=InputVat.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)Output VAT</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=GST.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%}
                            else {%>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)VAT</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=GST.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%}
                        }
                        else
                        { %>
                    <%if (IGSTPer > 0)
    { %>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)<%=( "IGST") %> on (SChg)(<%=((IGSTPer > 0 ? IGSTPer : 18)) %>% on <%=Markup.ToString("N" + agency.DecimalValue) %> )</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(GST).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%}
    else
    { %>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)<%=("CGST") %> on (SChg)(<%=((CGSTPer > 0 ? CGSTPer : 9)) %>% on <%=Markup.ToString("N" + agency.DecimalValue) %> )</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(GST/2).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <tr>
                        <td style="width: 85%; text-align: right">(+)<%=("SGST") %> on (SChg)(<%=((SGSTPer > 0 ? SGSTPer : 9)) %>% on <%=Markup.ToString("N" + agency.DecimalValue) %> )</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=(GST/2).ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                   <%}
    } if(discount > 0){%>
                     <tr>
                        <td style="width: 85%; text-align: right">(-)Discount</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=discount.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%}if (itinerary.FlightBookingSource == BookingSource.OffLine)
                        { %>
                    <tr>
                        <td style="width: 85%; text-align: right">(-)TransFee</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=TransFee.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <tr>
                        <td style="width: 85%; text-align: right">(-)AddlTransFee</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right"><%=AddlTransFee.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <%} %>
                    <tr>
                        <td colspan="3" align="right">
                            <hr  style="width: 50%"/>
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 85%; text-align: right">Total Net Amount</td>
                        <td style="width: 5%">:</td>
                        <td style="width: 10%; text-align: right;font-weight:bold;"><%=GrandTotal.ToString("N" + agency.DecimalValue) %></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <hr  style="width: 50%"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            For <%=agency.Name %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                              <% if (location.CountryCode == "IN")
                                {%> 
                            <img src="images/ctw stamp.png" width="80px" height="80px" alt="Cozmo Travel World" />
                            <%} %>
                        </td>
                    </tr>
                </table>
            </td>            
        </tr>
         <% if (location.CountryCode != "IN")
             {%> 
         <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <%} %>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>Booked by: <% UserMaster bookedBy = new UserMaster(itinerary.CreatedBy); %><%=bookedBy.FirstName +" "+bookedBy.LastName %></td>
            <td></td>
            <td></td>
        </tr>
        
        <tr>
            <td>Place: <%=GetStateName(location.StateId, location.CountryCode) %></td>
            <td></td>
            <td></td>
        </tr>
        <tr>                 
            <td align="right" colspan="3" >Authorised Signatory</td>
        </tr>
        <tr>        
            <td colspan="3" ><hr /></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>        
            <td colspan="3" > <%=location.Terms %></td>
        </tr>
    </table>    
</div>

    </form>
    </div>
</body>
</html>
