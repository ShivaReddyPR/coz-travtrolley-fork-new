﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Roster;

public partial class ROSEmployeeMasterGUI : CT.Core.ParentPage
{
    string ROSEMP_SEARCH_SESSION = "_rosEmpMasterSearchList";
    string ROSEMP_SESSION = "_rosEmpMaster";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!IsPostBack)
            {
                ViewState["Pwd"] = string.Empty;
                ViewState["ConPwd"] = string.Empty;
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
            else
            {
                ViewState["Pwd"] = txtEmpPassword.Text;
                ViewState["ConPwd"] = txtEmpConfirmPassword.Text;
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

        
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        txtEmpPassword.Attributes.Add("value", ViewState["Pwd"].ToString());
        txtEmpConfirmPassword.Attributes.Add("value", ViewState["ConPwd"].ToString());

    }
    #region Session Properties
    private ROSEmployeeMaster CurrentObject
    {
        get
        {
            return (ROSEmployeeMaster)Session[ROSEMP_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ROSEMP_SESSION);
            }
            else
            {
                Session[ROSEMP_SESSION] = value;
            }

        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[ROSEMP_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["emp_Id"] };

            Session[ROSEMP_SEARCH_SESSION] = value;
        }
    }
    #endregion

    protected void InitializePageControls()
    {
        
        BindLocation();
        //BindTariff();
    }
    private void BindLocation()
    {
        try
        {
            DataTable dtLocation=null;
            dtLocation = ROSLocationMaster.GetROSLocMasterList(RecordStatus.Activated, ListStatus.Short);
            ddlEmpLocFrom.DataSource = dtLocation;
            ddlEmpLocFrom.DataValueField = "loc_id";
            ddlEmpLocFrom.DataTextField = "loc_name";
            ddlEmpLocFrom.DataBind();
            ddlEmpLocFrom.Items.Insert(0, new ListItem("Select Location", "-1"));

            ddlEmpLocTo.DataSource = dtLocation;
            ddlEmpLocTo.DataValueField = "loc_id";
            ddlEmpLocTo.DataTextField = "loc_name";
            ddlEmpLocTo.DataBind();
            ddlEmpLocTo.Items.Insert(0, new ListItem("Select Location", "-1"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }

    private void BindTariff()
    {
        try
        {
            DataTable dt = ROSTariffMaster.GetList(RecordStatus.Activated, ListStatus.Short);
            ddlTariff.DataSource = dt;
            ddlTariff.DataValueField = "TRF_ID";
            ddlTariff.DataTextField = "TRF_CODE";
            ddlTariff.DataBind();
            ddlTariff.Items.Insert(0, new ListItem("Select Tariff", "-1"));
        }
        catch { throw; }
    }
    protected void ddlEmpLocFrom_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmpLocFrom.SelectedIndex > 0)
            {
                
                txtEmpLocFromDetail.Text = ROSLocationMaster.GetLocDetailsByLocId(Utility.ToInteger(ddlEmpLocFrom.SelectedValue));
                txtEmpLocFromMap.Focus();
            }
            else
            {
                txtEmpLocFromDetail.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void ddlEmpLocTo_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmpLocTo.SelectedIndex > 0)
            {
               
               txtEmpLocToDetail.Text = ROSLocationMaster.GetLocDetailsByLocId(Utility.ToInteger(ddlEmpLocTo.SelectedValue));
              
                txtEmpLocToMap.Focus();
            }
            else
            {
                txtEmpLocToDetail.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
   
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void Save()
    {
        try
        {
            ROSEmployeeMaster rosEmpMaster;
            if (CurrentObject == null)
            {
                rosEmpMaster = new ROSEmployeeMaster();
            }
            else
            {
                rosEmpMaster = CurrentObject;
            }

            rosEmpMaster.StaffId = txtEmpStaffId.Text;
            rosEmpMaster.Password = txtEmpPassword.Text;
            rosEmpMaster.Title = ddlEmpTitle.SelectedValue;
            rosEmpMaster.FirstName = txtEmpFName.Text;
            rosEmpMaster.LastName = txtEmpLName.Text;
            rosEmpMaster.Email = txtEmpEmail.Text;
            rosEmpMaster.MobileNo = txtEmpMobNo.Text;
            rosEmpMaster.AlternateNo = txtEmpAltMobNo.Text;
            rosEmpMaster.StaffType = ddlEmpStaffType.SelectedValue;
            rosEmpMaster.LocFrom = Utility.ToInteger(ddlEmpLocFrom.SelectedValue);
            rosEmpMaster.LocFromDetail = txtEmpLocFromDetail.Text;
            rosEmpMaster.LocFromMap = txtEmpLocFromMap.Text;
            rosEmpMaster.LocTo = Utility.ToInteger(ddlEmpLocTo.SelectedValue);
            rosEmpMaster.LocToDetails = txtEmpLocToDetail.Text;
            rosEmpMaster.LocToMap = txtEmpLocToMap.Text;
            rosEmpMaster.Designation = txtEmpDesig.Text;
            rosEmpMaster.CreatedBy = Settings.LoginInfo.AgentId;
            if(ddlTariff.SelectedItem!=null) rosEmpMaster.TariffId = Utility.ToLong(ddlTariff.SelectedItem.Value);
            rosEmpMaster.TariffAmount = Utility.ToDecimal(txtTariffAmount.Text.Trim());
            rosEmpMaster.ServiceStatus = ddlService.SelectedItem.Value;
            rosEmpMaster.Remarks = txtRemarks.Text.Trim();

            rosEmpMaster.Save();

            lblMessage.Visible = true;
            lblMessage.Text = Formatter.ToMessage(("Transaction For "), txtEmpFName.Text, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated)) + " Successfully.";
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            Clear();
        }
        catch (Exception ex)
        {
            
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    private void Clear()
    {

        try
        {
            txtEmpStaffId.Text = string.Empty;
            txtEmpPassword.Text = string.Empty;
            txtEmpPassword.Enabled = true;
            txtEmpConfirmPassword.Text = string.Empty;
            txtEmpConfirmPassword.Enabled = true;

            ddlEmpTitle.SelectedIndex = 0;
            txtEmpFName.Text = string.Empty;
            txtEmpLName.Text = string.Empty;
            txtEmpEmail.Text = string.Empty;
            txtEmpMobNo.Text = string.Empty;
            txtEmpAltMobNo.Text = string.Empty;
            ddlEmpStaffType.SelectedIndex = 0;
            ddlEmpLocFrom.SelectedIndex = 0;
            txtEmpLocFromDetail.Text = string.Empty;
            txtEmpLocFromMap.Text = string.Empty;
            ddlEmpLocTo.SelectedIndex = 0;
            txtEmpLocToDetail.Text = string.Empty;
            txtEmpLocToMap.Text = string.Empty;
            txtEmpDesig.Text = string.Empty;
            ddlTariff.SelectedIndex = 0;
            txtTariffAmount.Text = Formatter.ToCurrency(0);
            ddlService.SelectedIndex = 0;
            txtRemarks.Text = string.Empty;

            hdfMode.Value = "0";
            ViewState["Pwd"] = string.Empty;
            ViewState["ConPwd"] = string.Empty;

            CurrentObject = null;
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
           
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = ROSEmployeeMaster.GetROSEmpMasterList(RecordStatus.Activated,ListStatus.Long,string.Empty);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;

        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{"HTtxtEmpStaffId", "staff_id" }
                                             ,{ "HTtxtEmpName", "emp_Name" }
                                             ,{ "HTtxtEmpMobNo", "mobileNo" }
                                              ,{ "HTtxtEmpAltrNo", "alternateNo" }
                                              ,{"HTtxtEmpEmail", "email" }
                                             ,{ "HTtxtEmpStaffType", "staffType" }
                                             ,{ "HTtxtEmpLocFrom", "locationFrom" }
                                             ,{ "HTtxtEmpLocFromDet", "locationFromDetails" }
                                             ,{ "HTtxtEmpLocFromMap", "locationFromMap" }
                                             ,{ "HTtxtEmpLocTo", "locationTo" }
                                             ,{ "HTtxtEmpLocToDet", "locationToDetails" }
                                             ,{ "HTtxtEmpLocToMap", "locationToMap" }
                                             ,{ "HTtxtEmpDesig", "designation" }
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            int empId = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(empId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    private void Edit(int EmpId)
    {
        try
        {
            lblMessage.Visible = false;

            ROSEmployeeMaster getEmpMasterData = new ROSEmployeeMaster(EmpId);
            CurrentObject = getEmpMasterData;


            txtEmpStaffId.Text = getEmpMasterData.StaffId;
            ddlEmpTitle.SelectedValue = getEmpMasterData.Title;
            txtEmpFName.Text = getEmpMasterData.FirstName;
            txtEmpLName.Text = getEmpMasterData.LastName;
            txtEmpEmail.Text = getEmpMasterData.Email;
            txtEmpMobNo.Text = getEmpMasterData.MobileNo;
            txtEmpAltMobNo.Text = getEmpMasterData.AlternateNo;
            txtEmpPassword.Text = string.Empty;
           
            txtEmpPassword.Enabled = false;
            txtEmpConfirmPassword.Text = string.Empty;
            txtEmpConfirmPassword.Enabled = false;
            ddlEmpStaffType.SelectedValue = getEmpMasterData.StaffType;
            ddlEmpLocFrom.SelectedValue = Utility.ToString(getEmpMasterData.LocFrom);
            txtEmpLocFromDetail.Text = getEmpMasterData.LocFromDetail;
            txtEmpLocFromMap.Text = getEmpMasterData.LocFromMap;
            ddlEmpLocTo.SelectedValue = Utility.ToString(getEmpMasterData.LocTo);
            txtEmpLocToDetail.Text = getEmpMasterData.LocToDetails;
            txtEmpLocToMap.Text = getEmpMasterData.LocToMap;
            txtEmpDesig.Text = getEmpMasterData.Designation;
            ddlTariff.SelectedValue = Utility.ToString(getEmpMasterData.TariffId) == "0" ? "-1" : Utility.ToString(getEmpMasterData.TariffId);
            txtTariffAmount.Text = Formatter.ToCurrency(getEmpMasterData.TariffAmount);
            ddlService.SelectedValue = getEmpMasterData.ServiceStatus == string.Empty ? "A" : getEmpMasterData.ServiceStatus;
            txtRemarks.Text = getEmpMasterData.Remarks;
            hdfMode.Value = "1";
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }

    }



}
