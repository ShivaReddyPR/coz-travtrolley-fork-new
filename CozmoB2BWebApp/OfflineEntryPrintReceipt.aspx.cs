using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI;
using System.Data;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System.Web.UI.WebControls;

public partial class OfflineEntryPrintReceiptUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected LocationMaster location = new LocationMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoginInfo loginInfo = Settings.LoginInfo;
            if (loginInfo == null)
            {
                Response.Redirect("SessionExpired.aspx");
                // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
            }
            if (!string.IsNullOrEmpty(Request.QueryString["flightId"]))
            {
                //To get the agent location for location country code
                location = Settings.LoginInfo.IsOnBehalfOfAgent ? new LocationMaster(Settings.LoginInfo.OnBehalfAgentID) : new LocationMaster(Settings.LoginInfo.AgentId);
                long flightId = Utility.ToLong(Request.QueryString["flightId"]);
                ShowData(flightId);
            }
        }
    }
    private void ShowData(long flightId)
    {
        try
        {
            string modeDescription = string.Empty;
            PaymentMode mode = PaymentMode.Cash;
            DataSet dsFlightDtls = OffLineBO.GetOfflineEntryData(flightId);
            DataTable dtItenarary = new DataTable(); //0
            DataTable dtPaxandPrice = new DataTable();//1   pax details price details and ticket number
            DataTable dtInfo = new DataTable();//2  sectors
            decimal fare = 0;
            decimal handlingFee = 0;
            decimal tax = 0;
            decimal discount = 0;
            decimal markup = 0;
            decimal addlMarkup = 0;
            decimal totalAmount = 0;
            decimal grandTotal = 0;
            decimal VATAmount = 0;
            decimal baggageAmount = 0;
            string currency = string.Empty;
            string payMode = string.Empty;
            decimal cancellCharge = 0;
            int agencyId;
            if (dsFlightDtls != null)
            {
                if (dsFlightDtls.Tables[0] != null && dsFlightDtls.Tables[0].Rows.Count != 0)
                {
                    dtItenarary = dsFlightDtls.Tables[0];
                    if (dtItenarary.Rows.Count > 0)
                    {
                        lblPNRNoValue.Text = Utility.ToString(dtItenarary.Rows[0]["pnr"]);

                        lblTravelDateValue.Text = CZTDateTimeFormat(Utility.ToString(dtItenarary.Rows[0]["travelDate"]));
                        payMode = Utility.ToString(dtItenarary.Rows[0]["paymentMode"])=="1"?"CASH": Utility.ToString(dtItenarary.Rows[0]["paymentMode"]) == "2"?"CREDIT":"CARD";
                        lblTicketedByValue.Text = Utility.ToString(dtItenarary.Rows[0]["createdBy"]);
                        lblDateValue.Text = CZTDateTimeFormat(Utility.ToString(dtItenarary.Rows[0]["docDate"]));
                        lbltermsAndCondition.Text = dtItenarary.Rows[0]["location_terms"].ToString();
                        lblagentAddress.Text= " | " + dtItenarary.Rows[0]["location_address"].ToString();
                        if((BookingStatus)int.Parse(dtItenarary.Rows[0]["statusId"].ToString())==BookingStatus.Refunded)
                        {
                            lblReceiptHeader.Text = "Credit Note";
                            cancellChargeLbl.Visible = true;
                            cancellChargeValu.Visible = true;
                        }
                        lblRCPT.Text = "RCPT/" + dtItenarary.Rows[0]["location_code"].ToString() +"/"+ dtItenarary.Rows[0]["flightId"].ToString();
                        agencyId = int.Parse(dtItenarary.Rows[0]["agencyId"].ToString());
                        location = new LocationMaster(int.Parse(dtItenarary.Rows[0]["location_id"].ToString()));
                    }
                }

                if (dsFlightDtls.Tables[1] != null && dsFlightDtls.Tables[1].Rows.Count != 0)
                {
                    dtPaxandPrice = dsFlightDtls.Tables[1];
                    foreach (DataRow dr in dtPaxandPrice.Rows)
                    {
                        TableRow tRow = new TableRow();
                        TableCell tCellTitle = new TableCell();
                        tCellTitle.Text = dr["titleName"].ToString();
                        TableCell tCellName = new TableCell();
                        tCellName.Text = dr["paxName"].ToString();
                        TableCell tCellTicketNo = new TableCell();
                        tCellTicketNo.Text = dr["ticketNumber"].ToString();
                        tRow.Cells.Add(tCellTitle);
                        tRow.Cells.Add(tCellName);
                        tRow.Cells.Add(tCellTicketNo);
                        tblPaxDetails.Rows.Add(tRow);
                        fare = fare + Utility.ToDecimal(Utility.ToString(dr["publishedFare"]));
                        currency = Utility.ToString(dr["currency"]);
                        discount = discount + Utility.ToDecimal(Utility.ToString(dr["discount"]));
                        tax = tax + Utility.ToDecimal(Utility.ToString(dr["tax"]));
                        markup = markup + Utility.ToDecimal(Utility.ToString(dr["markup"]));
                        addlMarkup = addlMarkup + Utility.ToDecimal(Utility.ToString(dr["asvAmount"]));
                        baggageAmount = baggageAmount + Utility.ToDecimal(Utility.ToString(dr["baggageCharge"]));
                        VATAmount = VATAmount + Utility.ToDecimal(Utility.ToString(dr["OUTVAT_AMOUNT"]));
                        cancellCharge = cancellCharge + decimal.Parse(dr["agent_cancellation_amount"].ToString()) +
                            decimal.Parse(dr["supplier_cancellation_amount"].ToString()) +
                            decimal.Parse(dr["invat_cancellation"].ToString()) +
                            decimal.Parse(dr["outvat_cancellation"].ToString());
                    }
                    handlingFee = markup + addlMarkup;
                    lblHandlingFeeValue.Text = Formatter.ToCurrency(handlingFee);                   
                    cancellChargeValu.Text = Formatter.ToCurrency(cancellCharge.ToString());
                    fare = fare + tax + baggageAmount - discount;
                    totalAmount = fare + handlingFee;
                    grandTotal = totalAmount + VATAmount - cancellCharge;
                    lblFareValue.Text = Formatter.ToCurrency(fare);
                    lblToCollectValue.Text = Formatter.ToCurrency(grandTotal);
                    lblVATValue.Text = Formatter.ToCurrency(VATAmount);
                    if (discount == 0)
                    {
                        lblDiscountAmt.Visible = false;
                        lblDiscountValue.Visible = false;
                    }
                    else
                    {
                        lblDiscountAmt.Visible = true;
                        lblDiscountValue.Visible = true;
                        lblDiscountValue.Text = Formatter.ToCurrency(discount);
                    }

                    //source amount = publishedFare+tax-discount+markup+addlmarkup
                    //lblCollectionModeValue.Text = payMode + ":" + Formatter.ToCurrency(grandTotal) + "(" + currency + ")";
                }

                if (dsFlightDtls.Tables[2] != null && dsFlightDtls.Tables[2].Rows.Count != 0)
                {
                    string routing = string.Empty;
                    dtInfo = dsFlightDtls.Tables[2];

                    if (dtInfo.Rows.Count > 0)
                    {
                        if (dtInfo.Rows.Count == 1)
                        {
                            routing = Utility.ToString(dtInfo.Rows[0]["depAirport"]) + "-" + Utility.ToString(dtInfo.Rows[0]["arrAirport"]);
                        }
                        else
                        {
                            for (int i = 0; i < dtInfo.Rows.Count; i++)
                            {
                                if(string.IsNullOrEmpty(routing))
                                    routing = Utility.ToString(dtInfo.Rows[i]["depAirport"]);
                                else
                                    routing = routing + "-" + Utility.ToString(dtInfo.Rows[i]["depAirport"]);

                                if (i == dtInfo.Rows.Count-1) routing = routing + "-" + Utility.ToString(dtInfo.Rows[i]["arrAirport"]);
                            }
                        }

                        lblSectorsValue.Text = routing;
                    }
                }

                if (dsFlightDtls.Tables[3] != null && dsFlightDtls.Tables[3].Rows.Count != 0)
                {
                    for(int trCount=0;trCount< dsFlightDtls.Tables[3].Rows.Count;trCount++)
                    {
                        TableRow tRow = new TableRow();
                        TableCell tCellAmount = new TableCell();
                        tCellAmount.Text = Formatter.ToCurrency(decimal.Parse(dsFlightDtls.Tables[3].Rows[trCount]["amount"].ToString()))+ "(" + currency + ")";
                        TableCell tCellMode = new TableCell();
                        tCellMode.Text = dsFlightDtls.Tables[3].Rows[trCount]["settlement_type"].ToString();
                        TableCell tCellCCharge = new TableCell();                        
                        if (dsFlightDtls.Tables[3].Rows[trCount]["settlement_type"].ToString() =="Card")
                        {
                            tCellCCharge.Text = Formatter.ToCurrency(decimal.Parse(dsFlightDtls.Tables[3].Rows[trCount]["FOP_detail_3"].ToString()));
                            fare = fare + decimal.Parse(dsFlightDtls.Tables[3].Rows[trCount]["FOP_detail_3"].ToString());
                            grandTotal= grandTotal + decimal.Parse(dsFlightDtls.Tables[3].Rows[trCount]["FOP_detail_3"].ToString());
                            totalAmount = totalAmount + decimal.Parse(dsFlightDtls.Tables[3].Rows[trCount]["FOP_detail_3"].ToString());
                        }                        
                        tRow.Cells.Add(tCellAmount);
                        tRow.Cells.Add(tCellMode);
                        tRow.Cells.Add(tCellCCharge);
                        SettlementData.Rows.Add(tRow);
                        SettlementData.Rows.Add(tRow);
                    }                    
                }
                else
                {
                    SettlementData.Visible = false;
                }
                lblTotCostValue.Text = Formatter.ToCurrency(totalAmount);
                lblFareValue.Text = Formatter.ToCurrency(fare + discount);
                lblToCollectValue.Text = Formatter.ToCurrency(grandTotal);
            }
        }
        catch(Exception ex)
        {
            Utility.Alert(this.Page, ex.ToString());
        }
    }

    protected string CZTDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
}

