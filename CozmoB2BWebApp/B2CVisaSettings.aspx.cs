﻿using System;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;

public partial class B2CVisaSettings : CT.Core.ParentPage
{
    //DataTable dtProducts;
    //DataTable dtMarkupList;

    string layout = "VISA SETTINGS";
    string eMailId = "EMAILIDFORVISA";
    string waitingText = "WAITINGTEXTFORVISA";
    string googleScriptType = "GOOGLESCRIPTTYPE";
    string googleScript = "GOOGLESCRIPT";
    string waitingFile = "WAITINGLOGOFORVISA";

    string enbdPG = "ENBD-PG";
    string enbdCreditCardCharges = "ENBD-CREDITCARD-CHARGES";
    string ccAvenuePG = "CCAVENUE-PG";
    string ccAvenueCreditCardCharges = "CCAVENUE-CREDITCARD-CHARGES";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }
        else
        {
            if (!IsPostBack)
            {
                //ClearMarkupControls();
                ClearSettingsControls();
                InitializePageControls();
            }
            else
            {
                //BindControls();
            }
            lblSuccessMsg.Text = string.Empty;
            errMess.Style.Add("display", "none");
            errorMessage.InnerHtml = string.Empty;
        }
    }

    private void InitializePageControls()
    {
        try
        {
            
            BindSettings();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void BindSettings()
    {
        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            UserMaster member = new UserMaster(memberId);



            Dictionary<string, string> preferences = UserPreference.GetPreferenceList(memberId, ItemType.Visa);

            if (preferences.ContainsKey(eMailId))
            {
                txtEmail.Text = preferences[eMailId];
            }
            if (preferences.ContainsKey(googleScriptType))
            {
                string gScriptType = preferences[googleScriptType];

                switch (gScriptType)
                {
                    case "Google Analytics":
                        rbtnGoogleAnalytics.Checked = true;
                        break;
                    case "Google PPC":
                        rbtnGooglePPC.Checked = true;
                        break;
                }
            }

            if (preferences.ContainsKey(googleScript))
            {
                txtGoogleScript.Text = preferences[googleScript];
            }
            if (preferences.ContainsKey(waitingText))
            {
                txtWaitingText.Text = preferences[waitingText];
            }
            //if (preferences.ContainsKey(discount))
            //{
            //    txtDiscount.Text = preferences[discount];
            //}

            if (preferences.ContainsKey(enbdPG))
            {
                chkENBD.Checked = Convert.ToBoolean(preferences[enbdPG]);
            }

            if (preferences.ContainsKey(enbdCreditCardCharges))
            {
                txtENBDCharges.Text = preferences[enbdCreditCardCharges];
            }

            if (preferences.ContainsKey(ccAvenuePG))
            {
                chkCCA.Checked = Convert.ToBoolean(preferences[ccAvenuePG]);
            }

            if (preferences.ContainsKey(ccAvenueCreditCardCharges))
            {
                txtCCACharges.Text = preferences[ccAvenueCreditCardCharges];
            }

        }
    }

    

    protected void btnNext_Click(object sender, EventArgs e)
    {
        Button btnNext = sender as Button;

        switch (btnNext.ID)
        {
            case "btnVisaViewNext":
                mvSettings.ActiveViewIndex = 1;
                break;
           
        }
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        Button btnPrev = sender as Button;

        switch (btnPrev.ID)
        {
            case "btnMarkupPrev":
                mvSettings.ActiveViewIndex = 0;
                break;
           
        }
    }

    #region VisaSettings Control Events

    protected void btnSaveSettings_Click(object sender, EventArgs e)
    {

        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            int agentId = Convert.ToInt32(Request["agentId"]);
            try
            {
                UserPreference pref = new UserPreference();
                pref.Save(memberId, eMailId, txtEmail.Text, layout, ItemType.Visa);
                pref.Save(memberId, waitingText, txtWaitingText.Text, layout, ItemType.Visa);

                pref.Save(memberId, googleScriptType, (rbtnGoogleAnalytics.Checked ? rbtnGoogleAnalytics.Text : rbtnGooglePPC.Text), layout, ItemType.Visa);
                pref.Save(memberId, googleScript, txtGoogleScript.Text, layout, ItemType.Visa);



                lblSuccessMsg.Text = "Settings saved successfully";
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to save B2C Visa Settings " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
    }

    protected void btnClearSettings_Click(object sender, EventArgs e)
    {
        ClearSettingsControls();
    }

    void ClearSettingsControls()
    {
        txtEmail.Text = "";
        txtWaitingText.Text = "";
        txtGoogleScript.Text = "";
    }

    #endregion

    #region Markup and PaymentGateway Events

    protected void btnSaveMarkup_Click(object sender, EventArgs e)
    {
        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            int agentId = Convert.ToInt32(Request["agentId"]);
            try
            {
                UserPreference pref = new UserPreference();
                pref.Save(memberId, enbdPG, chkENBD.Checked.ToString(), layout, ItemType.Visa);
                pref.Save(memberId, enbdCreditCardCharges, txtENBDCharges.Text, layout, ItemType.Visa);

                pref.Save(memberId, ccAvenuePG, chkCCA.Checked.ToString(), layout, ItemType.Visa);
                pref.Save(memberId, ccAvenueCreditCardCharges, txtCCACharges.Text, layout, ItemType.Visa);

               // pref.Save(memberId, string.Empty, string.Empty, layout, ItemType.Visa);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to save B2C PG Settings " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            lblSuccessMsg.Text = "Updated successfully";
            
        }
    }

    protected void btnClearMarkup_Click(object sender, EventArgs e)
    {
        ClearMarkupControls();
    }

    void ClearMarkupControls()
    {
        txtENBDCharges.Text = "0";
        txtCCACharges.Text = "0";
        chkCCA.Checked = false;
    }

    #endregion


}
