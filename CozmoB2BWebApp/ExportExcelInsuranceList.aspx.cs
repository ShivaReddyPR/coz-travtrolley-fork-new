﻿using System;
using System.Data;
using System.Web.UI;
using System.IO;

public partial class ExportExcelInsuranceListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            dgInsuranceList.DataSource = Session["InsuranceList"] as DataTable;
            dgInsuranceList.DataBind();
            string attachment = "attachment; filename=InsuranceList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgInsuranceList.AllowPaging = false;
            dgInsuranceList.DataSource = Session["InsuranceList"] as DataTable;
            dgInsuranceList.DataBind();
            Form.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
