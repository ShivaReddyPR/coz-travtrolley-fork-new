﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class AgentSourceCredentialsGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private Dictionary<string, SourceDetails> dtSources;
    int agentId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //if (!IsPostBack)
            //{
            if (Request.QueryString["agencyId"] != null && Request.QueryString["agencyId"] != string.Empty)
            {
                agentId = Convert.ToInt32(Request.QueryString["agencyId"]);
            }
            //}
            if (agentId > 0)
            {
                BindSources(agentId);
            }
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }

    private void BindSources(int agentId)
    {
        try
        {
            dtSources = AgentMaster.GetAirlineCredentials(agentId);
            HtmlTableRow th = new HtmlTableRow();
            th.Height = "40px";
            th.VAlign = "middle";
            HtmlTableCell cell1 = new HtmlTableCell();
            Label lblSourceName = new Label();
            lblSourceName.Text = "Source";
            lblSourceName.Font.Bold = true;
            cell1.Controls.Add(lblSourceName);
            th.Cells.Add(cell1);
            //HtmlTableCell cell2 = new HtmlTableCell();
            //Label lblAgentCode = new Label();
            //lblAgentCode.Text = "AgentCode";
            //lblAgentCode.Font.Bold = true;
            //cell2.Controls.Add(lblAgentCode);
            //th.Cells.Add(cell2);
            HtmlTableCell cell3 = new HtmlTableCell();
            Label lblUserId = new Label();
            lblUserId.Text = "UserID";
            lblUserId.Font.Bold = true;
            cell3.Controls.Add(lblUserId);
            th.Cells.Add(cell3);
            HtmlTableCell cell4 = new HtmlTableCell();
            Label lblPassword = new Label();
            lblPassword.Text = "Password";
            lblPassword.Font.Bold = true;
            cell4.Controls.Add(lblPassword);
            th.Cells.Add(cell4);
            HtmlTableCell cell5 = new HtmlTableCell();
            Label lblHAP = new Label();
            lblHAP.Text = "HAP/Agent Code/Company Code";
            lblHAP.Font.Bold = true;
            cell5.Controls.Add(lblHAP);
            th.Cells.Add(cell5);

            HtmlTableCell cell6 = new HtmlTableCell();
            Label lblPCC = new Label();
            lblPCC.Text = "PCC";
            lblPCC.Font.Bold = true;
            cell6.Controls.Add(lblPCC);
            th.Cells.Add(cell6);
            HtmlTableCell cell7 = new HtmlTableCell();
            Label lblEndPoint = new Label();
            lblEndPoint.Text = "Endpoint";
            lblEndPoint.Font.Bold = true;
            cell7.Controls.Add(lblEndPoint);
            th.Cells.Add(cell7);

            //Added by Anji Binding PreferredLCC,PreferredGDS,PrivateFareCodes coloums on 21/02/19
            HtmlTableCell cell8 = new HtmlTableCell();
            Label lblPreferredLCC = new Label();
            lblPreferredLCC.Text = "PreferredLCC";
            lblPreferredLCC.Font.Bold = true;
            cell8.Controls.Add(lblPreferredLCC);
            th.Cells.Add(cell8);
           
            HtmlTableCell cell9 = new HtmlTableCell();
            Label lblPreferredGDS = new Label();
            lblPreferredGDS.Text = "PreferredGDS";
            lblPreferredGDS.Font.Bold = true;
            cell9.Controls.Add(lblPreferredGDS);
            th.Cells.Add(cell9);
            
            HtmlTableCell cell10 = new HtmlTableCell();
            Label lblPrivateFareCodes = new Label();
            lblPrivateFareCodes.Text = "PrivateFareCodes";
            lblPrivateFareCodes.Font.Bold = true;
            cell10.Controls.Add(lblPrivateFareCodes);
            th.Cells.Add(cell10);

            HtmlTableCell cell2 = new HtmlTableCell();
            Label lblIspassive = new Label();
            lblIspassive.Text = "CreatePassieveReservation";
            lblIspassive.Font.Bold = true;
            cell2.Controls.Add(lblIspassive);
            th.Cells.Add(cell2);
            //End
            tblAgentSources.Rows.Add(th);

            int i = 1;
            foreach (KeyValuePair<string, SourceDetails> source in dtSources)
            {
                SourceDetails AgentDetails = source.Value;
                HtmlTableRow tr = new HtmlTableRow();
                tr.Height = "25px";
                HtmlTableCell cell11 = new HtmlTableCell();
                Label lblSourceNameVal = new Label();
                lblSourceNameVal.ID = "lblSourceName_" + i.ToString();
                lblSourceNameVal.Text = source.Key;
                cell11.Controls.Add(lblSourceNameVal);
                tr.Cells.Add(cell11);

                //HtmlTableCell cell12 = new HtmlTableCell();
                //TextBox txtAgentCode = new TextBox();
                //txtAgentCode.ID = "txtAgentCode_" + i.ToString();
                //txtAgentCode.Text = AgentDetails.AgentCode;
                //cell12.Controls.Add(txtAgentCode);
                //tr.Cells.Add(cell12);

                HtmlTableCell cell13 = new HtmlTableCell();
                TextBox txtUserID = new TextBox();
                txtUserID.ID = "txtUserID_" + i.ToString();
                txtUserID.Text = AgentDetails.UserID;
                cell13.Controls.Add(txtUserID);
                tr.Cells.Add(cell13);

                HtmlTableCell cell14 = new HtmlTableCell();
                TextBox txtPassword = new TextBox();
                txtPassword.ID = "txtPassword_" + i.ToString();
                txtPassword.Text = AgentDetails.Password;
                cell14.Controls.Add(txtPassword);
                tr.Cells.Add(cell14);

                HtmlTableCell cell15 = new HtmlTableCell();
                TextBox txtHAP = new TextBox();
                txtHAP.ID = "txtHAP_" + i.ToString();
                txtHAP.Text = AgentDetails.HAP;
                cell15.Controls.Add(txtHAP);
                tr.Cells.Add(cell15);

                HtmlTableCell cell16 = new HtmlTableCell();
                TextBox txtPCC = new TextBox();
                txtPCC.ID = "txtPCC_" + i.ToString();
                txtPCC.Text = AgentDetails.PCC;
                cell16.Controls.Add(txtPCC);
                tr.Cells.Add(cell16);

                TextBox txtType = new TextBox();
                txtType.ID = "txtType_" + i.ToString();
                txtType.Text = AgentDetails.Type.ToString();
                cell16.Controls.Add(txtType);
                //HtmlTableCell cell17 = new HtmlTableCell();
                //TextBox txtType = new TextBox();
                //txtType.ID = "txtType_" + i.ToString();
                //txtType.Text = AgentDetails.Type.ToString();
                //cell17.Controls.Add(txtType);
                //tr.Cells.Add(cell17);
                txtType.Visible = false;

                HtmlTableCell cell18 = new HtmlTableCell();
                TextBox txtEndPoint = new TextBox();
                txtEndPoint.ID = "txtEndPoint_" + i.ToString();
                txtEndPoint.Text = (string.IsNullOrEmpty(AgentDetails.EndPoint) ? "" : AgentDetails.EndPoint.ToString());
                cell18.Controls.Add(txtEndPoint);
                tr.Cells.Add(cell18);

                //Added by Anji to add PreferredLCC,PreferredGDS,PrivateFareCodes in the Credentials
                HtmlTableCell cell19 = new HtmlTableCell();
                TextBox txtPreferredLCC = new TextBox();
                txtPreferredLCC.ID = "txtPreferredLCC_" + i.ToString();
                txtPreferredLCC.Text = (string.IsNullOrEmpty(AgentDetails.PreferredLCC) ? "" : AgentDetails.PreferredLCC.ToString());
                cell19.Controls.Add(txtPreferredLCC);
                tr.Cells.Add(cell19);

                HtmlTableCell cell20 = new HtmlTableCell();
                TextBox txtPreferredGDS = new TextBox();
                txtPreferredGDS.ID = "txtPreferredGDS_" + i.ToString();
                txtPreferredGDS.Text = (string.IsNullOrEmpty(AgentDetails.PreferredGDS) ? "" : AgentDetails.PreferredGDS.ToString());
                cell20.Controls.Add(txtPreferredGDS);
                tr.Cells.Add(cell20);

                HtmlTableCell cell21 = new HtmlTableCell();
                TextBox txtPrivateFareCodes = new TextBox();
                txtPrivateFareCodes.ID = "txtPrivateFareCodes_" + i.ToString();
                txtPrivateFareCodes.Text = (string.IsNullOrEmpty(AgentDetails.PrivateFareCodes) ? "" : AgentDetails.PrivateFareCodes.ToString());
                cell21.Controls.Add(txtPrivateFareCodes);
                tr.Cells.Add(cell21);

                HtmlTableCell cell22 = new HtmlTableCell();
                CheckBox chkIsPassive = new CheckBox();
                chkIsPassive.ID = "chkIsPassive_" + i.ToString();
                chkIsPassive.CssClass ="form-control pull-right";
                chkIsPassive.Checked = (AgentDetails.CreatePassieveReservation == "Y");
                cell22.Controls.Add(chkIsPassive);
                tr.Cells.Add(cell22);

                //End by Anji


               
                tblAgentSources.Rows.Add(tr);
                i++;
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            Label lblSourceName = new Label();
            //TextBox txtAgentCode = new TextBox();
            TextBox txtUserID = new TextBox();
            TextBox txtPassword = new TextBox();
            TextBox txtHAP = new TextBox();
            TextBox txtPCC = new TextBox();
            TextBox txtType = new TextBox();
            TextBox txtEndpoint = new TextBox();
            TextBox txtPreferredLCC = new TextBox();
            TextBox txtPreferredGDS = new TextBox();
            TextBox txtPrivateFareCodes = new TextBox();
            CheckBox chkIsPassive = new CheckBox();
            for (int i = 1; i < tblAgentSources.Rows.Count; i++)
            {
                lblSourceName = tblAgentSources.Rows[i].FindControl("lblSourceName_" + i) as Label;
                //txtAgentCode = tblAgentSources.Rows[i].FindControl("txtAgentCode_" + i) as TextBox;
                txtUserID = tblAgentSources.Rows[i].FindControl("txtUserID_" + i) as TextBox;
                txtPassword = tblAgentSources.Rows[i].FindControl("txtPassword_" + i) as TextBox;
                txtHAP = tblAgentSources.Rows[i].FindControl("txtHAP_" + i) as TextBox;
                txtPCC = tblAgentSources.Rows[i].FindControl("txtPCC_" + i) as TextBox;
                txtType = tblAgentSources.Rows[i].FindControl("txtType_" + i) as TextBox; //Added by Ravi to add Product type in the credentials
                txtEndpoint = tblAgentSources.Rows[i].FindControl("txtEndPoint_" + i) as TextBox;
                //Added by Anji to add PreferredLCC,PreferredGDS,PrivateFareCodes in the credentials 
                txtPreferredLCC = tblAgentSources.Rows[i].FindControl("txtPreferredLCC_" + i) as TextBox;
                txtPreferredGDS = tblAgentSources.Rows[i].FindControl("txtPreferredGDS_" + i) as TextBox;
                txtPrivateFareCodes = tblAgentSources.Rows[i].FindControl("txtPrivateFareCodes_" + i) as TextBox;
                chkIsPassive = tblAgentSources.Rows[i].FindControl("chkIsPassive_" + i) as CheckBox;
                //End by Anji
                SourceDetails details = default(SourceDetails);

                //details.AgentCode = txtAgentCode.Text.Trim();
                details.UserID = txtUserID.Text.Trim();
                details.Password = txtPassword.Text.Trim();
                details.HAP = txtHAP.Text.Trim();
                details.PCC = txtPCC.Text.Trim();
                details.Type = txtType.Text != string.Empty ? Convert.ToInt32(txtType.Text) : 0;
                details.EndPoint = txtEndpoint.Text.Trim();
                details.PreferredLCC = txtPreferredLCC.Text.Trim();
                details.PreferredGDS = txtPreferredGDS.Text.Trim();
                details.PrivateFareCodes = txtPrivateFareCodes.Text.Trim();
                details.CreatePassieveReservation = chkIsPassive.Checked ? "Y" : "N";
                AgentMaster.UpdateAgentSourceCredentials(agentId, lblSourceName.Text, details);
            }
            lblSuccessMsg.Text = "Successfully updated.";

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
            lblSuccessMsg.Text = ex.Message;
        }
    }
}
