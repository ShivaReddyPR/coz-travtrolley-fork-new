﻿using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ExpAffidavit : System.Web.UI.Page
    {
        public string imagePath = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo == null)
                return;

            AgentMaster clsAgentMaster = new AgentMaster(Settings.LoginInfo.AgentId);
            imagePath = Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port > 0 ? ":" + Request.Url.Port : string.Empty) +
                Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");

            imagePath = clsAgentMaster.ID <= 1 ? "images/logo.jpg" : imagePath + "/" + ConfigurationManager.AppSettings["AgentImage"] + clsAgentMaster.ImgFileName;
        }
    }
}