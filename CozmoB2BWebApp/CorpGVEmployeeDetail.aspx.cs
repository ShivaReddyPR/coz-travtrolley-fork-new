﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Web.UI.WebControls;
using CT.GlobalVisa;
using CT.Corporate;
using System.Collections;
using System.Collections.Generic;

public partial class CorpGVEmployeeDetailGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected GlobalVisaSession visaSession = new GlobalVisaSession();
    protected CorporateProfile cropProfileDetails;
    protected SortedList nationalityList;
    protected SortedList countryList;
    protected List<int> passExpiryYearList = new List<int>();
    protected List<int> passIssueYearList = new List<int>();
    protected List<string> passDayList = new List<string>();
    protected bool IsCorporate = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo == null) //Checking Logininfo
            {
                Response.Redirect("AbandonSession.aspx", false);
                return;
            }
            if (!string.IsNullOrEmpty(Settings.LoginInfo.IsCorporate) && Settings.LoginInfo.IsCorporate == "Y")
            {
                IsCorporate = true;
            }
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                //Loading All Nationality List
                nationalityList = Country.GetNationalityList();
                //Loading All Country List
                countryList = Country.GetCountryList();
                for (int i = DateTime.Now.Year; i <= (DateTime.Now.Year + 50); i++)
                {
                    passExpiryYearList.Add(i);
                }
                for (int j = 1920; j <= (DateTime.Now.Year); j++)
                {
                    passIssueYearList.Add(j);
                }
                for (int i = 1; i <= 31; i++)
                {
                    if (i <= 9)
                    {
                        passDayList.Add("0" + i.ToString());
                    }
                    else
                    {
                        passDayList.Add(i.ToString());
                    }
                }
            }
            if (Session["GVSession"] != null)
            {
                visaSession = Session["GVSession"] as GlobalVisaSession;
                if (IsCorporate) ////If Corporate We need to Featch All corporate Details
                {
                    if (visaSession != null && visaSession.CorpProfileDetails != null)
                    {
                        cropProfileDetails = visaSession.CorpProfileDetails as CorporateProfile;
                        //Getting latest Documents and latest Updated Details 
                        cropProfileDetails = new CorporateProfile(cropProfileDetails.ProfileId, Settings.LoginInfo.AgentId);
                        visaSession.CorpProfileDetails = cropProfileDetails;
                        Session["GVSession"] = visaSession;
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Details is Null", Request["REMOTE_ADDR"]);
                        Response.Redirect("AbandonSession.aspx", false);
                    }
                }                
            }
            else
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVEmployeeDetail page load exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["GVSession"] != null)
            {
                visaSession = Session["GVSession"] as GlobalVisaSession;
                if (visaSession != null)
                {
                    List<GVPassenger> passengerList = new List<GVPassenger>();
                    int totPax = visaSession.Adult + visaSession.Child + visaSession.Infant;
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    for (int i = 0; i < totPax; i++)
                    {
                        if (i == 0)
                        {
                            visaSession.NationalityCode = Request["ddlNationality-" + i];
                            visaSession.ResCountryCode = Request["ddlResCountry-" + i];
                        }
                        GVPassenger passenger = new GVPassenger();
                        passenger.Fpax_name = Request["txtApplicantName-" + i];
                        passenger.Fpax_pax_type = Request["ddlPaxType-" + i];

                        passenger.Fpax_gender = Request["ddlGender-" + i];
                        passenger.Fpax_marital_status = Request["ddlMaritalSt-" + i];
                        passenger.Fpax_profession = Request["txtProfession-" + i];
                        passenger.Fpax_email = Request["txtEmail-" + i];
                        passenger.Fpax_phone = Request["txtPhone-" + i];
                        passenger.Fpax_passport_type = Request["txtPassportType-" + i];
                        passenger.Fpax_passport_no = Request["txtPassportNo-" + i];
                        passenger.Fpax_country_id = Request["ddlPassIssueCountry-" + i];
                        passenger.Fpax_psp_issue_date = Convert.ToDateTime(Request["ddlPassIssueDay-" + i] + "/" + Request["ddlPassIssueMonth-" + i] + "/" + Request["ddlPassIssueYear-" + i], dateFormat);
                        passenger.Fpax_psp_expiry_date = Convert.ToDateTime(Request["ddlPassExpiryDay-" + i] + "/" + Request["ddlPassExpiryMonth-" + i] + "/" + Request["ddlPassExpiryYear-" + i], dateFormat);
                        passenger.Fpax_fax = Request["txtFax-" + i];
                        passenger.Fpax_city = Request["txtCity-" + i];
                        passenger.Fpax_zip_code = Request["txtZip-" + i];
                        passenger.Fpax_adds1 = Request["txtOffice-" + i];
                        passenger.Fpax_adds2 = Request["txtResidence-" + i];
                        passenger.Fpax_created_by = Settings.LoginInfo.UserID;
                        passengerList.Add(passenger);
                    }
                    visaSession.PassengerList = passengerList;
                    if (IsCorporate)
                    {
                        Response.Redirect("CorpGVVisaDetails.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("CorpGvVisaDocuments.aspx", false);
                    }
                    
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Details is Null", Request["REMOTE_ADDR"]);
                    Response.Redirect("AbandonSession.aspx", false);
                }
            }
            else
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                Response.Redirect("AbandonSession.aspx", false);

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVEmployeeDetail page procced button Clieck exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
}