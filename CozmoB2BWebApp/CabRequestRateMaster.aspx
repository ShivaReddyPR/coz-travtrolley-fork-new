﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TransactionVisaTitle.master" CodeBehind="CabRequestRateMaster.aspx.cs" EnableEventValidation="false" Inherits="CozmoB2BWebApp.RateMaster" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
   <script type="text/javascript">
       function isNumbers(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
       }

       function validate() {

           var isValid = true;

           document.getElementById('rentalType').style.display = 'none';
           document.getElementById('cabpreference').style.display = 'none';
           document.getElementById('cabmodel').style.display = 'none';
           document.getElementById('amount').style.display = 'none';

           if (document.getElementById('<%=ddlRentalType.ClientID%>').value == "-1") {
                document.getElementById('rentalType').style.display = 'block';
                document.getElementById('rentalType').innerHTML = 'Please Select Rental Type.'
                isValid = false;
            }

            if (document.getElementById('<%=ddlCabPreference.ClientID%>').value == "-1") {
                document.getElementById('cabpreference').style.display = 'block';
                document.getElementById('cabpreference').innerHTML = 'Please Select Cab Preference.'
                isValid = false;
            } 
            if (document.getElementById('<%=ddlCabModel.ClientID%>').value == "-1") {
                document.getElementById('cabmodel').style.display = 'block';
                document.getElementById('cabmodel').innerHTML = 'Please Select Cab Model.'
                isValid = false;
           }

           if (document.getElementById('<%=txtAmount.ClientID%>').value.trim().length <= 0) {
               document.getElementById('amount').style.display = 'block';
               document.getElementById('amount').innerHTML = 'Please enter amount';
               isValid = false;
           }
           return isValid;
       }
   </script>
    <asp:HiddenField ID="hdnId" runat="server" Value="0" />  
       
        <h2 class="title-style"><span id="ctl00_lblMainTitle" class="maintitle">Cab Request Rate Master</span></h2>
         
         <div class="body_container">
		<div class=" paramcon" title="header">
			<div class="row marbot_10   m-0 mb-3">
				<div class="col-md-2">
					Rental Type:
				</div>			
				<div class="col-md-2">
					 <asp:DropDownList ID="ddlRentalType" runat="server" CssClass="form-control"></asp:DropDownList>
                    <span class="red_span" style="display: none" id="rentalType"></span>
				</div>
				<div class="col-md-2"> 
					Cab Preference:
				</div>
				<div class="col-md-2">
					<asp:DropDownList ID="ddlCabPreference" runat="server" CssClass="form-control"></asp:DropDownList>
                    <span class="red_span" style="display: none" id="cabpreference"></span>
				</div>
				<div class="col-md-2">				
					Cab Model:
				</div>
				<div class="col-md-2">
					<asp:DropDownList ID="ddlCabModel" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    <span class="red_span" style="display: none" id="cabmodel"></span>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="row marbot_10 m-0 mb-3">
				<div class="col-md-2">
					Amount
				</div>			
				<div class="col-md-2">
					  <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" Text="0.00" onKeyPress="return isNumbers(event);" maxlength="7"></asp:TextBox>
				 <span class="red_span" style="display: none" id="amount"></span>
                </div>
               
				<div class="col-md-2"> 
					Status
				</div>
				<div class="col-md-2">
					<asp:CheckBox   ID= "chkStatus" runat="server" Checked="true" />
				</div>
				
				<div class="clearfix"></div>
			</div>
			
			
			<div class="row marbot_10 m-0 mb-3">
				<div class="col-md-12">
					<label style=" padding-right:5px" class="f_R">
						
						 <asp:Button ID="btnSearch" runat="server" CssClass="btn but_b"  Text="Search"  OnClick="btnSearch_Click"/>
					</label>
					<label style=" padding-right:5px" class="f_R">
						 <asp:Button ID="btnClear" runat="server" CssClass="btn but_b"  Text="Clear"  OnClick="btnClear_Click"/>
					</label>
					<label style=" padding-right:5px" class="f_R">
						  <asp:Button ID="btnSave" runat="server" CssClass="btn but_b"  Text="Save" OnClick="btnSave_Click" OnClientClick="return validate();"/>
					</label>
					
					<asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"  Visible="false"></asp:Label>
					   
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
         
          <div>
   
      
       
        



    </div>


</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView  ID="gvSearch" Width="100%"  runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" DataKeyNames="Id" EmptyDataText="No Data Available!" GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" OnPageIndexChanging="gvSearch_PageIndexChanging">
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
                <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                    ControlStyle-CssClass="label" ShowSelectButton="True" />
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtRentalType" Width="70px" HeaderText="Rental Type" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblRentalType" runat="server" Text='<%#Eval("RentalType") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("RentalType") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtCabPref" Width="70px" HeaderText="Cab Preference" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCabPrefernce" runat="server" Text='<%#Eval("CabPreference") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("CabPreference") %>' Width="70px"></asp:Label> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtCabModel" Width="70px" HeaderText="Cab Model" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCabModel" runat="server" Text='<%#Eval("CabModel") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("CabModel") %>' Width="70px"></asp:Label> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAMount" Width="70px" HeaderText="Amount" CssClass="inputEnabled"
                            runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Amount") %>' Width="70px"></asp:Label> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtStatus" Width="70px" HeaderText="Status" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Status") %>' Width="70px"></asp:Label> 
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>

        </asp:GridView>

</asp:Content>
