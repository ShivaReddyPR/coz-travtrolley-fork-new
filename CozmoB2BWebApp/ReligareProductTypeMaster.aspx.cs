﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareProductTypeMaster : CT.Core.ParentPage
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;

            try
            {
                lblSuccessMsg.Text = string.Empty;
                if (!IsPostBack)
                {
                    
                }
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                Utility.Alert(this.Page, ex.Message);

            }

        }
        #region Methods

        void Save()
        {
            try
            {
                ReligareProductType religareproduct=new ReligareProductType();
                if (Utility.ToInteger(hdfmeid.Value)>0)
                {
                    religareproduct.ID = Utility.ToLong(hdfmeid.Value);
                }
                else
                {
                    religareproduct.ID = -1;
                }
                string productName = string.Empty;
                religareproduct.PRODUCTNAME = txtProductName.Text;
                religareproduct.CREATED_BY= Settings.LoginInfo.UserID;
                religareproduct.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfmeid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        
        void bindSearch()
        {

            try
            {

                DataTable dt = ReligareProductType.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        void Clear()
        {
            txtProductName.Text = string.Empty;
            
        }

        void Edit(long id)
        {
            try
            {
                ReligareProductType religareproduct = new ReligareProductType(id);
                hdfmeid.Value = Utility.ToString(religareproduct.ID);
                txtProductName.Text = religareproduct.PRODUCTNAME;

                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Button Click evetns
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductTypeMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductTypeMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductTypeMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion 
        #region Gridview Events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long Productid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(Productid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductTypeMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();
               
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductTypeMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        #endregion

    }
}
