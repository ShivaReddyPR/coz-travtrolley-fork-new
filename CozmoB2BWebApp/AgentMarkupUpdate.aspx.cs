﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.Web.UI.Controls;

public partial class AgentMarkupUpdateGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    DataTable dtProducts;
    DataTable dtMarkupList;
    PagedDataSource pagedData = new PagedDataSource();
    protected int Mrdid;
    protected decimal HandlingFee;
    protected string HandlingType = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            this.Master.PageRole = true;
            AuthorizationCheck();
            if (!IsPostBack)
            {
                InitializePageControls();
            }
            //else
            //{
            //    BindControls();
            //}
            lblSuccessMsg.Text = string.Empty;
            errMess.Style.Add("display", "none");
            errorMessage.InnerHtml = string.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region Private methods

    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            //BindProcduct();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgent()
    {
        try
        {
            int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
            //AgentMaster1 agent = new AgentMaster1(agentId);
            DataTable dtAgents = null;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                dtAgents = AgentMaster.GetList(1, "ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                dtAgents = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //private void BindProcduct()
    //{
    //    try
    //    {
    //        Session["dtProducts"] = null;
            
    //        dtProducts = AgentMarkup.ProductGetList(ListStatus.Short, RecordStatus.Activated,Settings.LoginInfo.AgentId);
    //        chkProduct.DataSource = dtProducts;
    //        chkProduct.DataTextField = "productType";
    //        chkProduct.DataValueField = "productTypeId";
    //        chkProduct.DataBind();
    //       // BindControls();
    //       // DisableDataList();
    //        //BindMarkupList();
    //        Session["dtProducts"] = dtProducts;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //private void BindControls()
    //{
    //    if (Session["dtProducts"] != null)
    //    {
    //        dtProducts = (DataTable)Session["dtProducts"];
    //    }
    //    //List<string> HotelSource = new List<string>();
    //    //List<string> FlightSource = new List<string>();

    //    //System.Collections.Generic.Dictionary<string, string> sourcesConfig = CT.Configuration.ConfigurationSystem.ActiveSources;
    //    //foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourcesConfig)
    //    //{
    //    //    if (pair.Value.Split(',')[0].ToLower() == "hotel")
    //    //    {
    //    //        HotelSource.Add(pair.Key);
    //    //    }
    //    //    else if (pair.Value.Split(',')[0].ToLower() == "flight")
    //    //    {
    //    //        FlightSource.Add(pair.Key);
    //    //    }
    //    //}
    //    int agentId = Convert.ToInt32(ddlAgent.SelectedValue);
    //    HtmlTableRow hr = new HtmlTableRow();
    //    for (int i = 0; i < dtProducts.Rows.Count; i++) //Binding all Controls
    //    {
    //        //13.10.2014 Added by brahmam
    //        Label lblAgentMarkup = new Label();
    //        lblAgentMarkup.ID = "lblAgentMarkup_" + i.ToString();
    //        lblAgentMarkup.Text = "Agent Markup";
    //        lblAgentMarkup.Width = new Unit(90, UnitType.Pixel);
    //        lblAgentMarkup.Style.Add("display", "none");
    //        TextBox txtAgentMarkup = new TextBox();
    //        txtAgentMarkup.ID = "txtAgentMarkup_" + i.ToString();
    //        txtAgentMarkup.Text = "0.0000";
    //        txtAgentMarkup.Width = new Unit(80, UnitType.Pixel);
    //        txtAgentMarkup.Style.Add("display", "none");
    //        txtAgentMarkup.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'0');");
    //        txtAgentMarkup.Attributes.Add("onchange", "SetValue();");
    //        txtAgentMarkup.Attributes.Add("onfocus", "Check(this.id);");
    //        txtAgentMarkup.Attributes.Add("onBlur", "Set(this.id);");
    //        Label lblOurComm = new Label();
    //        lblOurComm.ID = "lblOurComm_" + i.ToString();
    //        lblOurComm.Text = "Our Commission";
    //        lblOurComm.Width = new Unit(80, UnitType.Pixel);
    //        lblOurComm.Style.Add("display", "none");
    //        TextBox txtOurComm = new TextBox();
    //        txtOurComm.ID = "txtOurComm_" + i.ToString();
    //        txtOurComm.Text = "0.0000";
    //        txtOurComm.Width = new Unit(80, UnitType.Pixel);
    //        txtOurComm.Style.Add("display", "none");
    //        txtOurComm.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'4');");
    //        txtOurComm.Attributes.Add("onchange", "SetValue();");
    //        txtOurComm.Attributes.Add("onfocus", "Check(this.id);");
    //        txtOurComm.Attributes.Add("onBlur", "Set(this.id);");
            
    //        Label lblMarkup = new Label();
    //        lblMarkup.ID = "lblMarkUp_" + i.ToString();
    //        lblMarkup.Text = "Markup";
    //        lblMarkup.Width = new Unit(80, UnitType.Pixel);
    //        lblMarkup.Style.Add("display", "none");
    //        TextBox txtMark = new TextBox();
    //        txtMark.ID = "txtMarkUp_" + i.ToString();
    //        txtMark.Text = "0.0000";
    //        txtMark.Width = new Unit(80, UnitType.Pixel);
    //        txtMark.Style.Add("display", "none");
    //        txtMark.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'0');");
    //        txtMark.Attributes.Add("onchange", "setToFixedThis(this.id);");
    //        txtMark.Attributes.Add("onfocus", "Check(this.id);");
    //        txtMark.Attributes.Add("onBlur", "Set(this.id);");
    //        txtMark.Enabled = false;
    //        Label lblmarkUpType = new Label();
    //        lblmarkUpType.ID = "lblmarkUpType_" + i.ToString();
    //        lblmarkUpType.Text = "MarkupType";
    //        lblmarkUpType.Width = new Unit(80, UnitType.Pixel);
    //        lblmarkUpType.Style.Add("display", "none");
    //        DropDownList ddlMarkupType = new DropDownList();
    //        ddlMarkupType.ID = "ddlMarkupType_" + i.ToString();
    //        ddlMarkupType.Items.Insert(0, new ListItem("Fixed", "F"));
    //        ddlMarkupType.Items.Insert(0, new ListItem("Percentage", "P"));
    //        ddlMarkupType.Width = new Unit(80, UnitType.Pixel);
    //        ddlMarkupType.Style.Add("display", "none");
    //        HtmlTableCell tc = new HtmlTableCell();
    //        TextBox txtDiscount = new TextBox();
    //        txtDiscount.Width = new Unit(80, UnitType.Pixel);
    //        txtDiscount.ID = "txtDiscount_" + i.ToString();
    //        txtDiscount.Text = "0.0000";
    //        txtDiscount.Style.Add("display", "none");
    //        txtDiscount.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'0');");
    //        txtDiscount.Attributes.Add("onchange", "setToFixedThis(this.id);");
    //        txtDiscount.Attributes.Add("onfocus", "Check(this.id);");
    //        txtDiscount.Attributes.Add("onBlur", "Set(this.id);");
    //        txtDiscount.Visible = false;
    //        Label lblDiscountType = new Label();
    //        lblDiscountType.ID = "lblDiscountType_" + i.ToString();
    //        lblDiscountType.Text = "Disc Type";
    //        lblDiscountType.Style.Add("display", "none");
    //        lblDiscountType.Width = new Unit(80, UnitType.Pixel);
    //        lblDiscountType.Visible = false;
    //        DropDownList ddlDiscountType = new DropDownList();
    //        ddlDiscountType.ID = "ddlDiscountType_" + i.ToString();
    //        ddlDiscountType.Style.Add("display", "none");
    //        ddlDiscountType.Items.Insert(0, new ListItem("Fixed", "F"));
    //        ddlDiscountType.Items.Insert(0, new ListItem("Percentage", "P"));
    //        ddlDiscountType.Width = new Unit(80, UnitType.Pixel);
    //        ddlDiscountType.Visible = false;
    //        Label lblDiscount = new Label();
    //        lblDiscount.ID = "lblDiscount_" + i.ToString();
    //        lblDiscount.Text = "Discount";
    //        lblDiscount.Style.Add("display", "none");
    //        lblDiscount.Width = new Unit(80, UnitType.Pixel);
    //        lblDiscount.Visible = false;



    //        // Modified by  brahmam 05 sep 2014
    //        if (i == 4 || i==9)  //Insurance and Car only showing Discount controls
    //        {
    //            txtDiscount.Visible = true;
    //            lblDiscountType.Visible = true;
    //            ddlDiscountType.Visible = true;
    //            lblDiscount.Visible = true;
    //        }
                      

    //        if (i == 0)
    //        {
    //            Label lblSource = new Label();
    //            lblSource.ID = "lblSource_" + i.ToString();
    //            lblSource.Text = "Sources";
    //            lblSource.Style.Add("display", "none");
    //            lblSource.Width = new Unit(100, UnitType.Pixel);
    //            tc.Controls.Add(lblSource);
    //            DropDownList ddlSource = new DropDownList();
    //            ddlSource.ID = "ddlSource_" + i.ToString();
    //            if (agentId > 0)
    //            {
    //                ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.Flight);
    //                ddlSource.DataValueField = "Id";
    //                ddlSource.DataTextField = "Name";
    //                ddlSource.DataBind();
    //            }
    //            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //            ddlSource.Width = new Unit(100, UnitType.Pixel);
    //            ddlSource.Style.Add("display", "none");
    //            ddlSource.AutoPostBack = true;
    //            ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);

    //            Label lblFlightType = new Label();
    //            lblFlightType.ID = "lblFlightType_" + i.ToString();
    //            lblFlightType.Text = "Flight Type";
    //            lblFlightType.Style.Add("display", "none");
    //            lblFlightType.Width = new Unit(80, UnitType.Pixel);
                
    //            DropDownList ddlFlightType = new DropDownList();
    //            ddlFlightType.ID = "ddlFlightType_" + i;
    //            ddlFlightType.Style.Add("display", "none");
    //            ddlFlightType.Items.Add(new ListItem("All", "All"));
    //            ddlFlightType.Items.Add(new ListItem("International", "INT"));
    //            ddlFlightType.Items.Add(new ListItem("Domestic", "DOM"));
    //            ddlFlightType.Width = new Unit(120, UnitType.Pixel);
    //            ddlFlightType.SelectedIndexChanged += DdlFlightType_SelectedIndexChanged;
    //            ddlFlightType.AutoPostBack = true;                 // added by arun, 05/07/2018

    //            Label lblJourneyType = new Label();
    //            lblJourneyType.ID = "lblJourneyType_" + i.ToString();
    //            lblJourneyType.Text = "Journey Type";
    //            lblJourneyType.Style.Add("display", "none");
    //            lblJourneyType.Width = new Unit(100, UnitType.Pixel);
                
    //            DropDownList ddlJourneyType = new DropDownList();
    //            ddlJourneyType.ID = "ddlJourneyType_" + i;
    //            ddlJourneyType.Style.Add("display", "none");
    //            ddlJourneyType.Items.Add(new ListItem("All", "All"));
    //            ddlJourneyType.Items.Add(new ListItem("Onward", "ONW"));
    //            ddlJourneyType.Items.Add(new ListItem("Return", "RET"));
    //            ddlJourneyType.Width = new Unit(100, UnitType.Pixel);
    //            ddlJourneyType.SelectedIndexChanged += DdlJourneyType_SelectedIndexChanged;
    //            ddlJourneyType.AutoPostBack = true;

    //            Label lblCarrierType = new Label();
    //            lblCarrierType.ID = "lblCarrierType_" + i.ToString();
    //            lblCarrierType.Text = "Carrier Type";
    //            lblCarrierType.Style.Add("display", "none");
    //            lblCarrierType.Width = new Unit(80, UnitType.Pixel);
                
    //            DropDownList ddlCarrierType = new DropDownList();
    //            ddlCarrierType.ID = "ddlCarrierType_" + i;
    //            ddlCarrierType.Style.Add("display", "none");
    //            ddlCarrierType.Items.Add(new ListItem("All", "All"));
    //            ddlCarrierType.Items.Add(new ListItem("GDS", "GDS"));
    //            ddlCarrierType.Items.Add(new ListItem("LCC", "LCC"));
    //            ddlCarrierType.Width = new Unit(80, UnitType.Pixel);
    //            ddlCarrierType.SelectedIndexChanged += DdlCarrierType_SelectedIndexChanged;
    //            ddlCarrierType.AutoPostBack = true;

    //            tc.Controls.Add(ddlSource);
    //            tc.Controls.Add(lblFlightType);
    //            tc.Controls.Add(ddlFlightType);
    //            tc.Controls.Add(lblJourneyType);
    //            tc.Controls.Add(ddlJourneyType);
    //            tc.Controls.Add(lblCarrierType);
    //            tc.Controls.Add(ddlCarrierType);

    //        }
    //        else if (i == 1)
    //        {
    //            Label lblSource = new Label();
    //            lblSource.ID = "lblSource_" + i.ToString();
    //            lblSource.Text = "Sources";
    //            lblSource.Style.Add("display", "none");
    //            lblSource.Width = new Unit(80, UnitType.Pixel);
    //            tc.Controls.Add(lblSource);
    //            DropDownList ddlSource = new DropDownList();
    //            ddlSource.ID = "ddlSource_" + i.ToString();
    //            if (agentId > 0)
    //            {
    //                ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.Hotel);
    //                ddlSource.DataValueField = "Id";
    //                ddlSource.DataTextField = "Name";
    //                ddlSource.DataBind();
    //            }
    //            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //            ddlSource.Width = new Unit(100, UnitType.Pixel);
    //            ddlSource.Style.Add("display", "none");
    //            ddlSource.AutoPostBack = true;
    //            ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
    //            tc.Controls.Add(ddlSource);
    //        }
    //        else if (i == 4)
    //        {
    //             Label lblSource = new Label();
    //            lblSource.ID = "lblSource_" + i.ToString();
    //            lblSource.Text = "Sources";
    //            lblSource.Style.Add("display", "none");
    //            lblSource.Width = new Unit(80, UnitType.Pixel);
    //            tc.Controls.Add(lblSource);
    //            DropDownList ddlSource = new DropDownList();
    //            ddlSource.ID = "ddlSource_" + i.ToString();
    //            if (agentId > 0)
    //            {
    //                ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.Insurance);
    //                ddlSource.DataValueField = "Id";
    //                ddlSource.DataTextField = "Name";
    //                ddlSource.DataBind();
    //            }
    //            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //            ddlSource.Width = new Unit(80, UnitType.Pixel);
    //            ddlSource.Style.Add("display", "none");
    //            ddlSource.AutoPostBack = true;
    //            ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
    //            tc.Controls.Add(ddlSource);
    //        }
    //        else if (i == 5)
    //        {
    //            Label lblSource = new Label();
    //            lblSource.ID = "lblSource_" + i.ToString();
    //            lblSource.Text = "Sources";
    //            lblSource.Style.Add("display", "none");
    //            lblSource.Width = new Unit(80, UnitType.Pixel);
    //            tc.Controls.Add(lblSource);
    //            DropDownList ddlSource = new DropDownList();
    //            ddlSource.ID = "ddlSource_" + i.ToString();
    //            if (agentId > 0)
    //            {
    //                ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.SightSeeing);
    //                ddlSource.DataValueField = "Id";
    //                ddlSource.DataTextField = "Name";
    //                ddlSource.DataBind();
    //            }
    //            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //            ddlSource.Width = new Unit(80, UnitType.Pixel);
    //            ddlSource.Style.Add("display", "none");
    //            ddlSource.AutoPostBack = true;
    //           ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
    //            tc.Controls.Add(ddlSource);
    //        }
    //            //@@@@ Added by chandan on 18022016
    //        else if (i == 6)
    //        {
    //            Label lblSource = new Label();
    //            lblSource.ID = "lblSource_" + i.ToString();
    //            lblSource.Text = "Sources";
    //            lblSource.Style.Add("display", "none");
    //            lblSource.Width = new Unit(80, UnitType.Pixel);
    //            tc.Controls.Add(lblSource);
    //            DropDownList ddlSource = new DropDownList();
    //            ddlSource.ID = "ddlSource_" + i.ToString();
    //            if (agentId > 0)
    //            {
    //                ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), (int)ProductType.Transfers);// '9' is for Transfer
    //                ddlSource.DataValueField = "Id";
    //                ddlSource.DataTextField = "Name";
    //                ddlSource.DataBind();
    //            }
    //            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //            ddlSource.Width = new Unit(80, UnitType.Pixel);
    //            ddlSource.Style.Add("display", "none");
    //            ddlSource.AutoPostBack = true;
    //           ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
    //            tc.Controls.Add(ddlSource);
    //        }
    //        else if (i == 9)
    //        {
    //            Label lblSource = new Label();
    //            lblSource.ID = "lblSource_" + i.ToString();
    //            lblSource.Text = "Sources";
    //            lblSource.Style.Add("display", "none");
    //            lblSource.Width = new Unit(80, UnitType.Pixel);
    //            tc.Controls.Add(lblSource);
    //            DropDownList ddlSource = new DropDownList();
    //            ddlSource.ID = "ddlSource_" + i.ToString();
    //            if (agentId > 0)
    //            {
    //                ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), (int)ProductType.Car);// '9' is for Car
    //                ddlSource.DataValueField = "Id";
    //                ddlSource.DataTextField = "Name";
    //                ddlSource.DataBind();
    //            }
    //            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //            ddlSource.Width = new Unit(80, UnitType.Pixel);
    //            ddlSource.Style.Add("display", "none");
    //            ddlSource.AutoPostBack = true;
    //      //      ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
    //            tc.Controls.Add(ddlSource);
    //        }
    //        else
    //        {
    //        }
    //        tc.Controls.Add(lblAgentMarkup);
    //        tc.Controls.Add(txtAgentMarkup);
    //        tc.Controls.Add(lblOurComm);
    //        tc.Controls.Add(txtOurComm);
    //        tc.Controls.Add(lblMarkup);
    //        tc.Controls.Add(txtMark);
    //        tc.Controls.Add(lblmarkUpType);
    //        tc.Controls.Add(ddlMarkupType);
    //        tc.Controls.Add(lblDiscount);
    //        tc.Controls.Add(txtDiscount);
    //        tc.Controls.Add(lblDiscountType);
    //        tc.Controls.Add(ddlDiscountType);

    //        tc.Width = "100px";
    //        tc.VAlign = "top";
    //        hr.Cells.Add(tc);
    //    }
    //    tblMarkup.Rows.Add(hr);
    //}

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }
    }

    //private void LoadControls()
    //{
    //    Label lblFlightType = (Label)tblMarkup.FindControl("lblFlightType_0");
    //    DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_0");
    //    Label lblJourneyType = (Label)tblMarkup.FindControl("lblJourneyType_0");
    //    DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_0");
    //    Label lblCarrierType = (Label)tblMarkup.FindControl("lblCarrierType_0");
    //    DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_0");
    //    DropDownList ddlsource = (DropDownList)tblMarkup.FindControl("ddlSource_0");
         
    //    //ddlsource.ClearSelection();       // To clear Selectin on Unchecking Checkbox
    //    //ddlFlightType.ClearSelection();
    //    //ddlJourneyType.ClearSelection();
    //    //ddlCarrierType.ClearSelection();


    //    dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //    for (int i = 0; i < chkProduct.Items.Count; i++)
    //    {
    //        Label lblAgentMarkup = (Label)tblMarkup.FindControl("lblAgentMarkup_" + i.ToString());
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + i.ToString());
    //        Label lblOurComm = (Label)tblMarkup.FindControl("lblOurComm_" + i.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + i.ToString());
    //        Label lblMarkup = (Label)tblMarkup.FindControl("lblMarkUp_" + i.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + i.ToString());
    //        Label lblMarkupType = (Label)tblMarkup.FindControl("lblmarkUpType_" + i.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + i.ToString());
    //        Label lblDiscount = (Label)tblMarkup.FindControl("lblDiscount_" + i.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + i.ToString());
    //        Label lblDiscountType = (Label)tblMarkup.FindControl("lblDiscountType_" + i.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + i.ToString());
    //        Label lblSource = (Label)tblMarkup.FindControl("lblSource_" + i.ToString());
    //        DropDownList ddlSource = (DropDownList)tblMarkup.FindControl("ddlSource_" + i.ToString());

    //        txtAgentMarkup.Text = "";
    //        txtMarkup.Text = "";             // to clear when checkbox unchecked
    //        txtOurComm.Text = "";


    //        if (chkProduct.Items[i].Selected)
    //        {
    //            lblAgentMarkup.Style.Add("display", "block");
    //            txtAgentMarkup.Style.Add("display", "block");
    //            lblOurComm.Style.Add("display", "block");
    //            txtOurComm.Style.Add("display", "block");
    //            lblMarkup.Style.Add("display", "block");
    //            txtMarkup.Style.Add("display", "block");
    //            lblMarkupType.Style.Add("display", "block");
    //            ddlMarkupType.Style.Add("display", "block");
    //            lblDiscount.Style.Add("display", "block");
    //            txtDiscount.Style.Add("display", "block");
    //            lblDiscountType.Style.Add("display", "block");
    //            ddlDiscountType.Style.Add("display", "block");
    //            if (i == 0)
    //            {
                   


    //                lblFlightType.Style.Add("display", "block");
    //                ddlFlightType.Style.Add("display", "block");
    //                lblJourneyType.Style.Add("display", "block");
    //                ddlJourneyType.Style.Add("display", "block");
    //                lblCarrierType.Style.Add("display", "block");
    //                ddlCarrierType.Style.Add("display", "block");
    //            }
    //            if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) // Sources is there Just displaying
    //            {
    //                //ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), Convert.ToInt32(i));
    //                //ddlSource.DataValueField = "Id";
    //                //ddlSource.DataTextField = "Name";
    //                //ddlSource.DataBind();
    //                //ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
    //                //ddlSource.Width = new Unit(100, UnitType.Pixel);
    //                //ddlSource.Style.Add("display", "none");
    //                //ddlSource.AutoPostBack = true;
    //                //ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);

    //                lblSource.Style.Add("display", "block");
    //                ddlSource.Style.Add("display", "block");
    //            }
    //            if (i == 2 || i == 3 || i == 7 || i == 8) // Here source is not there Loading markup directly
    //            {
                    

    //                int agent = 0;
    //                string find = string.Empty;
    //                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
    //                if (ddlAgent.SelectedItem.Value != "0")
    //                {
    //                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //                }
    //                if (agent == 0)
    //                {
    //                    find = "ProductId='" + productId + "' AND AgentId IS NULL";
    //                }
    //                else
    //                {
    //                    find = "ProductId='" + productId + "' AND AgentId = '" + agent + "'  AND transType='B2B'";
    //                }
    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                //DataTable dtMarkupDetails = UpdateMarkup.GetMarkupDetails(productId, agent);
    //                if (foundRows.Length > 0)
    //                {
    //                    if (foundRows[0]["AgentMarkup"] != DBNull.Value)
    //                    {
    //                        txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //                    }
    //                    else
    //                    {
    //                        txtAgentMarkup.Text = "0.00";
    //                    }
    //                    if (foundRows[0]["OurCommission"] != DBNull.Value)
    //                    {
    //                        txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //                    }
    //                    else
    //                    {
    //                        txtOurComm.Text = "0.00";
    //                    }
    //                    txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //                    string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

    //                    if (!string.IsNullOrEmpty(markuptype))
    //                        ddlMarkupType.SelectedValue = markuptype;
    //                    //  ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //                    txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //                    ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //                }
    //                else
    //                {
    //                    txtAgentMarkup.Text = "0.00";
    //                    txtOurComm.Text = "0.00";
    //                    txtMarkup.Text = "0.00";
    //                    txtDiscount.Text = "0.00";
    //                }
    //            }
    //        }
    //        else
    //        {
    //            lblAgentMarkup.Style.Add("display", "none");
    //            txtAgentMarkup.Style.Add("display", "none");
    //            lblOurComm.Style.Add("display", "none");
    //            txtOurComm.Style.Add("display", "none");
    //            lblMarkup.Style.Add("display", "none");
    //            txtMarkup.Style.Add("display", "none");
    //            lblMarkupType.Style.Add("display", "none");
    //            ddlMarkupType.Style.Add("display", "none");
    //            lblDiscount.Style.Add("display", "none");
    //            txtDiscount.Style.Add("display", "none");
    //            lblDiscountType.Style.Add("display", "none");
    //            ddlDiscountType.Style.Add("display", "none");
    //            if (i == 0)
    //            {
    //                lblFlightType.Style.Add("display", "none");
    //                ddlFlightType.Style.Add("display", "none");
    //                lblJourneyType.Style.Add("display", "none");
    //                ddlJourneyType.Style.Add("display", "none");
    //                lblCarrierType.Style.Add("display", "none");
    //                ddlCarrierType.Style.Add("display", "none");
    //            }
    //            if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //Checking only sources products only
    //            {
    //                lblSource.Style.Add("display", "none");
    //                ddlSource.Style.Add("display", "none");
    //            }
    //        }
    //    }
    //}

    private void BindMarkupList(ProductType productId)
    {
        try
        {
            string agentType = string.Empty;

            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            //if (Settings.LoginInfo.AgentType == AgentType.B2B)
            //{
            //    if (agentId <= 0)
            //    {
            //        agentType = "B2B";
            //        agentId = Settings.LoginInfo.AgentId;
            //    }
            //}
            if (agentId > 0)
            {
                dtMarkupList = AgentMarkup.GetAgentMarkupDetails((int)productId, string.Empty, agentId, agentType, string.Empty, string.Empty, string.Empty);
                if (dtMarkupList != null)// && dtMarkupList.Rows.Count > 0)
                {
                    CommonGrid grid = new CommonGrid();
                    if (productId == ProductType.Flight)//Flight Markup
                    {
                        grid.BindGrid(gvFlightMkp, dtMarkupList);
                        if (hdfFlightMode.Value == "2")
                            gvFlightMkp.FooterRow.Visible = false;
                        else
                            gvFlightMkp.FooterRow.Visible = true;
                    }
                    else if (productId == ProductType.Hotel)
                    {
                        grid.BindGrid(gvHotelMkp, dtMarkupList);
                        if (hdfHotelMode.Value == "2")
                            gvHotelMkp.FooterRow.Visible = false;
                        else
                            gvHotelMkp.FooterRow.Visible = true;
                    }
                    else if (productId == ProductType.Packages)
                    {
                        grid.BindGrid(gvPackageMkp, dtMarkupList);
                        if (dtMarkupList.Rows.Count >= 1) gvPackageMkp.FooterRow.Visible = false;
                    }
                    else if (productId == ProductType.Activity)
                    {
                        grid.BindGrid(gvActivityMkp, dtMarkupList);
                        if (dtMarkupList.Rows.Count >= 1) gvActivityMkp.FooterRow.Visible = false;
                    }
                    else if (productId == ProductType.Insurance)
                    {
                        grid.BindGrid(gvInsuranceMkp, dtMarkupList);
                        if (hdfInsurancemode.Value == "2")
                            gvInsuranceMkp.FooterRow.Visible = false;
                    }
                    else if (productId == ProductType.FixedDeparture)
                    {
                        grid.BindGrid(gvFixedDepMkp, dtMarkupList);
                        if (hdfFixedDepMode.Value == "2")
                            gvFixedDepMkp.FooterRow.Visible = false;
                        else
                            gvFixedDepMkp.FooterRow.Visible = true;
                    }
                    else if (productId == ProductType.Car)
                    {
                        grid.BindGrid(gvCarMkp, dtMarkupList);
                        if (hdfCarMode.Value == "2")
                            gvCarMkp.FooterRow.Visible = false;
                        else
                            gvCarMkp.FooterRow.Visible = true;
                    }
                    else if (productId == ProductType.SightSeeing)
                    {
                        grid.BindGrid(gvSightMkp, dtMarkupList);
                        if (hdfSightMode.Value == "2")
                            gvSightMkp.FooterRow.Visible = false;
                        else
                            gvSightMkp.FooterRow.Visible = true;
                    }
                    else if (productId == ProductType.Transfers )
                    {
                        grid.BindGrid(gvTransferMkp , dtMarkupList);
                        if (hdfTransferMode.Value == "2")
                            gvTransferMkp.FooterRow.Visible = false;
                        else
                            gvTransferMkp.FooterRow.Visible = true;
                    }

                    setRowId(productId);
                }
                //else
                //{
                //    DisableDataList();
                //}
            }
            //else
            //{
            //    DisableDataList();
            //}
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
        }
    }

    //Vijesh DB list

    
    //private void DisableDataList()
    //{
    //    dlMarkup.Visible = false;
    //    btnPrev.Visible = false;
    //    btnFirst.Visible = false;
    //    btnLast.Visible = false;
    //    btnNext.Visible = false;
    //    lblMessage.Visible = true;
    //    lblCurrentPage.Text = string.Empty;
    //    lblMessage.Text = "No Results Found.";
    //}

    private void BindSource(DropDownList ddlSource , ProductType prodType)
    {
        ddlSource.DataSource = AgentMaster.GetAgentSources(Utility.ToInteger(ddlAgent.SelectedItem.Value), (int)prodType);
        ddlSource.DataValueField = "Name";
        ddlSource.DataTextField = "Name";
        ddlSource.DataBind();
        ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
        ddlSource.SelectedIndex = 0;
    }
    private void setRowId(ProductType  product)
    {
        try
        {
            DropDownList ddlSource = null;
            DropDownList ddlFlightType = null;
            DropDownList ddlJourneyType = null;
            DropDownList ddlCarrierType = null;
            DropDownList ddlMarkupType = null;
            DropDownList ddlDiscountType = null;



            if (product == ProductType.Flight)
            {

                if (gvFlightMkp.EditIndex > -1)
                {
                    hdfFlightMKupRowId.Value = gvFlightMkp.Rows[gvFlightMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvFlightMkp.Rows[gvFlightMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.Flight);
                    //((TextBox)gvPaxDetails.Rows[gvPaxDetails.EditIndex].FindControl("EITtxtQty")).Text = "0";
                    //((TextBox)gvPaxDetails.Rows[gvPaxDetails.EditIndex].FindControl("EITtxtRate")).Text = Formatter.ToCurrency("0");
                    //((TextBox)gvPaxDetails.Rows[gvPaxDetails.EditIndex].FindControl("EITtxtValue")).Text = Formatter.ToCurrency("0");
                }
                else
                {
                    hdfFlightMKupRowId.Value = gvFlightMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvFlightMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.Flight);
                    // ((TextBox)gvPaxDetails.FooterRow.FindControl("FTtxtQty")).Text = "0";
                    //  ((TextBox)gvPaxDetails.FooterRow.FindControl("FTtxtAmount")).Text = Formatter.ToCurrency("0");
                    //((TextBox)gvPaxDetails.FooterRow.FindControl("FTtxtValue")).Text = Formatter.ToCurrency("0");
                }
            }
            //Hotel Details
            else if (product == ProductType.Hotel)
            {
                if (gvHotelMkp.EditIndex > -1)
                {
                    hdfHotelMKupRowId.Value = gvHotelMkp.Rows[gvHotelMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvHotelMkp.Rows[gvHotelMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.Hotel);
                }
                else
                {
                    hdfHotelMKupRowId.Value = gvHotelMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvHotelMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.Hotel);
                }
            }
            else if (product == ProductType.Insurance)
            {
                if (gvInsuranceMkp.EditIndex > -1)
                {
                    hdfInsuranceMKupRowId.Value = gvInsuranceMkp.Rows[gvInsuranceMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvInsuranceMkp.Rows[gvInsuranceMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.Insurance);
                }
                else
                {
                    hdfInsuranceMKupRowId.Value = gvInsuranceMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvInsuranceMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.Insurance);
                }
            }
            else if (product == ProductType.FixedDeparture)
            {
                if (gvFixedDepMkp.EditIndex > -1)
                {
                    hdfFixedDepMKupRowId.Value = gvFixedDepMkp.Rows[gvFixedDepMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvFixedDepMkp.Rows[gvFixedDepMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.FixedDeparture);                   
                }
                else
                {
                    hdfFixedDepMKupRowId.Value = gvFixedDepMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvFixedDepMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.FixedDeparture);
                    
                }
            }
            else if (product == ProductType.Car )
            {
                if (gvCarMkp.EditIndex > -1)
                {
                    hdfCarMKupRowId.Value = gvCarMkp.Rows[gvCarMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvCarMkp.Rows[gvCarMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.Car );
                }
                else
                {
                    hdfCarMKupRowId.Value = gvCarMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvCarMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.Car);

                }
            }
            else if (product == ProductType.SightSeeing )
            {
                if (gvSightMkp.EditIndex > -1)
                {
                    hdfSightMKupRowId.Value = gvSightMkp.Rows[gvSightMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvSightMkp.Rows[gvSightMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.SightSeeing);
                }
                else
                {
                    hdfSightMKupRowId.Value = gvSightMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvSightMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.SightSeeing);
                }
            }
            else if (product == ProductType.Transfers)
            {
                if (gvTransferMkp.EditIndex > -1)
                {
                    hdfTransferMKupRowId.Value = gvTransferMkp.Rows[gvTransferMkp.EditIndex].ClientID;
                    ddlSource = (DropDownList)gvTransferMkp.Rows[gvTransferMkp.EditIndex].FindControl("EITddlSource");
                    BindSource(ddlSource, ProductType.Transfers);
                }
                else
                {
                    hdfTransferMKupRowId.Value = gvTransferMkp.FooterRow.ClientID;
                    ddlSource = (DropDownList)gvTransferMkp.FooterRow.FindControl("FTddlSource");
                    BindSource(ddlSource, ProductType.Transfers);
                }
            }
            // UpdatePanel upPanal = (UpdatePanel)this.Master.FindControl("upnllocation");
            //upPanal.Update();
            //Utility.StartupScript(this.Page, "calcTotalValues()", " calcTotalValues");
            //Utility.StartupScript(this.Page, "calcAmount()", " calcAmount");
            //Utility.StartupScript(this.Page, "calcGSACommission()", " calcGSACommission");


        }
        catch { throw; }
    }

    #endregion private

    #region dropdown events

    //private void DdlCarrierType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlSource = sender as DropDownList;
    //        int agent = 0;
    //        int rowIndex = 0;
    //        string find = string.Empty;
    //        rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
    //        if (ddlAgent.SelectedItem.Value != "0")
    //        {
    //            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //        }

    //        string source = ddlSource.SelectedItem.Text;
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
    //        DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
    //        DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
    //        DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
    //        DropDownList ddlSourcelist = (DropDownList)tblMarkup.FindControl("ddlSource_" + rowIndex.ToString());

    //        int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);

    //        //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1, ddlSourcelist.SelectedItem.Text, -1, string.Empty,ddlFlightType.SelectedValue,ddlJourneyType.SelectedValue,ddlCarrierType.SelectedValue);
    //        if (agent == 0)
    //        {
    //            find = "SourceId='" + source + "' AND AgentId IS NULL";
    //        }
    //        else
    //        {
    //            find = "ProductId='" + productId + "'AND CarrierType='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //            DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //            //DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

    //            //if (sources != null && productId == 1 && ddlCarrierType != null)
    //            //{
    //            //    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
    //            //}
    //        }
    //        DataRow[] foundRows = dtMarkupList.Select(find);
    //        if (foundRows.Length > 0)
    //        {
    //            if (foundRows[0]["AgentMarkup"] != DBNull.Value)
    //            {
    //                txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //            }
    //            else
    //            {
    //                txtAgentMarkup.Text = "0.00";
    //            }
    //            if (foundRows[0]["OurCommission"] != DBNull.Value)
    //            {
    //                txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //            }
    //            else
    //            {
    //                txtOurComm.Text = "0.00";
    //            }
    //            txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //            string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

    //            if (!string.IsNullOrEmpty(markuptype))
    //                ddlMarkupType.SelectedValue = markuptype;
    //            // ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //            txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //            ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //            if (productId == 1)
    //            {
    //                if (foundRows[0]["CarrierType"] != DBNull.Value)
    //                {
    //                    ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
    //                }
    //                if (foundRows[0]["FlightType"] == DBNull.Value)
    //                {
    //                    ddlFlightType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["FlightType"]))
    //                    {
    //                        case "All":
    //                            ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
    //                            break;
    //                        case "Domestic":
    //                            ddlFlightType.SelectedValue = "DOM";
    //                            break;
    //                        case "International":
    //                            ddlFlightType.SelectedValue = "INT";
    //                            break;
    //                    }
    //                }
    //                if (foundRows[0]["JourneyType"] == DBNull.Value)
    //                {
    //                    ddlJourneyType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["JourneyType"]))
    //                    {
    //                        case "All":
    //                            ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
    //                            break;
    //                        case "Onward":
    //                            ddlJourneyType.SelectedValue = "ONW";
    //                            break;
    //                        case "Return":
    //                            ddlJourneyType.SelectedValue = "RET";
    //                            break;
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            txtAgentMarkup.Text = "0.00";
    //            txtOurComm.Text = "0.00";
    //            txtMarkup.Text = "0.00";
    //            txtDiscount.Text = "0.00";
    //        }

    //    }
    //    catch 
    //    {

    //    }
    //}

    //private void DdlJourneyType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlSource = sender as DropDownList;
    //        int agent = 0;
    //        int rowIndex = 0;
    //        string find = string.Empty;
    //        rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
    //        if (ddlAgent.SelectedItem.Value != "0")
    //        {
    //            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //        }

    //        string source = ddlSource.SelectedItem.Text;
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
    //        DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
    //        DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
    //        DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
    //        DropDownList ddlSourcelist = (DropDownList)tblMarkup.FindControl("ddlSource_" + rowIndex.ToString());

    //        int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);

    //        //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1, ddlSourcelist.SelectedItem.Text, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        if (agent == 0)
    //        {
    //            find = "SourceId='" + source + "' AND AgentId IS NULL";
    //        }
    //        else
    //        {
    //            find = "ProductId='" + productId + "'AND JourneyType='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //           // DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //           // DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

    //           // if (sources != null && productId == 1 && ddlCarrierType != null)
    //           // {
    //            //    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
    //           // }
    //        }
    //        DataRow[] foundRows = dtMarkupList.Select(find);
    //        if (foundRows.Length > 0)
    //        {
    //            if (foundRows[0]["AgentMarkup"] != DBNull.Value)
    //            {
    //                txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //            }
    //            else
    //            {
    //                txtAgentMarkup.Text = "0.00";
    //            }
    //            if (foundRows[0]["OurCommission"] != DBNull.Value)
    //            {
    //                txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //            }
    //            else
    //            {
    //                txtOurComm.Text = "0.00";
    //            }
    //            txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //            string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

    //            if (!string.IsNullOrEmpty(markuptype))
    //                ddlMarkupType.SelectedValue = markuptype;

    //            // ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //            txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //            ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //            if (productId == 1)
    //            {
    //                if (foundRows[0]["CarrierType"] != DBNull.Value)
    //                {
    //                    ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
    //                }
    //                if (foundRows[0]["FlightType"] == DBNull.Value)
    //                {
    //                    ddlFlightType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["FlightType"]))
    //                    {
    //                        case "All":
    //                            ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
    //                            break;
    //                        case "Domestic":
    //                            ddlFlightType.SelectedValue = "DOM";
    //                            break;
    //                        case "International":
    //                            ddlFlightType.SelectedValue = "INT";
    //                            break;
    //                    }
    //                }
    //                if (foundRows[0]["JourneyType"] == DBNull.Value)
    //                {
    //                    ddlJourneyType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["JourneyType"]))
    //                    {
    //                        case "All":
    //                            ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
    //                            break;
    //                        case "Onward":
    //                            ddlJourneyType.SelectedValue = "ONW";
    //                            break;
    //                        case "Return":
    //                            ddlJourneyType.SelectedValue = "RET";
    //                            break;
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            txtAgentMarkup.Text = "0.00";
    //            txtOurComm.Text = "0.00";
    //            txtMarkup.Text = "0.00";
    //            txtDiscount.Text = "0.00";
    //        }

    //    }
    //    catch 
    //    {

    //    }
    //}

    //private void DdlFlightType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlSource = sender as DropDownList;
    //        int agent = 0;
    //        int rowIndex = 0;
    //        string find = string.Empty;
    //        rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
    //        if (ddlAgent.SelectedItem.Value != "0")
    //        {
    //            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //        }

    //        string source = ddlSource.SelectedItem.Text;
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
    //        DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
    //        DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
    //        DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
    //        DropDownList ddlSourcelist = (DropDownList)tblMarkup.FindControl("ddlSource_" + rowIndex.ToString());

    //        int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);

    //        //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1, ddlSourcelist.SelectedItem.Text, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        if (agent == 0)
    //        {
    //            find = "SourceId='" + source + "' AND AgentId IS NULL";
    //        }
    //        else
    //        {
    //            find = "ProductId='" + productId + "'AND FlightType='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //            //DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //            //DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

    //            //if (sources != null && productId == 1 && ddlCarrierType != null)
    //            //{
    //            //    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
    //            //}
    //        }
    //        DataRow[] foundRows = dtMarkupList.Select(find);
    //        if (foundRows.Length > 0)
    //        {
    //            if (foundRows[0]["AgentMarkup"] != DBNull.Value)
    //            {
    //                txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //            }
    //            else
    //            {
    //                txtAgentMarkup.Text = "0.00";
    //            }
    //            if (foundRows[0]["OurCommission"] != DBNull.Value)
    //            {
    //                txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //            }
    //            else
    //            {
    //                txtOurComm.Text = "0.00";
    //            }
    //            txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //            string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

    //            if (!string.IsNullOrEmpty(markuptype))
    //                ddlMarkupType.SelectedValue = markuptype;
    //            // ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //            txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //            ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //            if (productId == 1)
    //            {
    //                if (foundRows[0]["CarrierType"] != DBNull.Value)
    //                {
    //                    ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
    //                }
    //                if (foundRows[0]["FlightType"] == DBNull.Value)
    //                {
    //                    ddlFlightType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["FlightType"]))
    //                    {
    //                        case "All":
    //                            ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
    //                            break;
    //                        case "Domestic":
    //                            ddlFlightType.SelectedValue = "DOM";
    //                            break;
    //                        case "International":
    //                            ddlFlightType.SelectedValue = "INT";
    //                            break;
    //                    }
    //                }
    //                if (foundRows[0]["JourneyType"] == DBNull.Value)
    //                {
    //                    ddlJourneyType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["JourneyType"]))
    //                    {
    //                        case "All":
    //                            ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
    //                            break;
    //                        case "Onward":
    //                            ddlJourneyType.SelectedValue = "ONW";
    //                            break;
    //                        case "Return":
    //                            ddlJourneyType.SelectedValue = "RET";
    //                            break;
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            txtAgentMarkup.Text = "0.00";
    //            txtOurComm.Text = "0.00";
    //            txtMarkup.Text = "0.00";
    //            txtDiscount.Text = "0.00";
    //        }

    //    }
    //    catch 
    //    {

    //    }
    //}

    //protected void ddlSource_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
           
    //        DropDownList ddlSource = sender as DropDownList;
    //        int agent = 0;
    //        int rowIndex = 0;
    //        string find = string.Empty;
    //        rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
    //        if (ddlAgent.SelectedItem.Value != "0")
    //        {
    //            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //        }

    //        int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);
    //        string source = ddlSource.SelectedItem.Text;
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());

    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
    //        if (productId == 1)
    //        {
    //            DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
    //            DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
    //            DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());

    //            ddlCarrierType.Enabled = true;//Enable by default
    //            ddlFlightType.Enabled = true;
    //            ddlJourneyType.Enabled = true;

    //            ddlFlightType.ClearSelection();       // To clear previously selected data on changing sourcedropdown(ddlSource)
    //            ddlJourneyType.ClearSelection();       // added by arun.                      5/june/2018  
    //            ddlCarrierType.ClearSelection();

    //            ddlMarkupType.ClearSelection();
    //            txtAgentMarkup.Text = "";
    //            txtDiscount.Text = "";
    //            txtMarkup.Text = "";
    //            txtOurComm.Text = "";


    //            //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
    //            // dtMarkupList = UpdateMarkup.GetMarkupList(-1, source, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //            if (agent == 0)
    //            {
    //                find = "SourceId='" + source + "' AND AgentId IS NULL";
    //            }
    //            else
    //            {
    //                find = "ProductId='" + productId + "'AND SourceId='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //                DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //                DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

    //                if (sources != null && productId == 1 && ddlCarrierType != null && sources.Length > 0)
    //                {
    //                    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
    //                }
    //            }


    //            dtMarkupList = UpdateMarkup.GetMarkupList(-1, source, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);


    //            DataRow[] foundRows = dtMarkupList.Select(find);
    //            if (foundRows != null && foundRows.Length > 0)
    //            {
    //                if (foundRows[0]["AgentMarkup"] != DBNull.Value)
    //                {
    //                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //                }
    //                else
    //                {
    //                    txtAgentMarkup.Text = "0.00";
    //                }
    //                if (foundRows[0]["OurCommission"] != DBNull.Value)
    //                {
    //                    txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //                }
    //                else
    //                {
    //                    txtOurComm.Text = "0.00";
    //                }
    //                if (foundRows[0]["Markup"] != DBNull.Value)
    //                {
    //                    txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //                }
    //                else
    //                {
    //                    txtMarkup.Text = "0.00";
    //                }
    //                string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);
    //                if (!string.IsNullOrEmpty(markuptype))
    //                    ddlMarkupType.SelectedValue = markuptype;

    //                txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //                ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //                if (productId == 1)
    //                {
    //                    if (foundRows[0]["CarrierType"] != DBNull.Value)
    //                    {
    //                        ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);                            
    //                    }
    //                    else
    //                    {
    //                        ddlCarrierType.Enabled = false;//Disable for null saving
    //                    }
    //                    if (foundRows[0]["FlightType"] == DBNull.Value)
    //                    {
    //                        ddlFlightType.SelectedValue = "All";
    //                        ddlFlightType.Enabled = false;//Disable for null saving
    //                    }
    //                    else
    //                    {                            
    //                        switch (Convert.ToString(foundRows[0]["FlightType"]))
    //                        {
    //                            case "All":
    //                                ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
    //                                break;
    //                            case "Domestic":
    //                                ddlFlightType.SelectedValue = "DOM";
    //                                break;
    //                            case "International":
    //                                ddlFlightType.SelectedValue = "INT";
    //                                break;
    //                        }
    //                    }
    //                    if (foundRows[0]["JourneyType"] == DBNull.Value)
    //                    {
    //                        ddlJourneyType.SelectedValue = "All";
    //                        ddlJourneyType.Enabled = false;//Disable for null saving
    //                    }
    //                    else
    //                    {                            
    //                        switch (Convert.ToString(foundRows[0]["JourneyType"]))
    //                        {
    //                            case "All":
    //                                ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
    //                                break;
    //                            case "Onward":
    //                                ddlJourneyType.SelectedValue = "ONW";
    //                                break;
    //                            case "Return":
    //                                ddlJourneyType.SelectedValue = "RET";
    //                                break;
    //                        }
    //                    }
                        
    //                }
    //            }
    //            else
    //            {
                    
    //                txtAgentMarkup.Text = "0.00";
    //                txtOurComm.Text = "0.00";
    //                txtMarkup.Text = "0.00";
    //                txtDiscount.Text = "0.00";
    //            }
    //        }
    //        else
    //        {
    //            dtMarkupList = UpdateMarkup.GetMarkupList(-1, source, -1, string.Empty, string.Empty, string.Empty, string.Empty);
    //            if (agent == 0)
    //            {
    //                find = "SourceId='" + source + "' AND AgentId IS NULL";
    //            }
    //            else
    //            {
    //                find = "ProductId='" + productId + "'AND SourceId='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //                DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //                DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");


    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                if (foundRows != null && foundRows.Length > 0)
    //                {
    //                    if (foundRows[0]["AgentMarkup"] != DBNull.Value)
    //                    {
    //                        txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //                    }
    //                    else
    //                    {
    //                        txtAgentMarkup.Text = "0.00";
    //                    }
    //                    if (foundRows[0]["OurCommission"] != DBNull.Value)
    //                    {
    //                        txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //                    }
    //                    else
    //                    {
    //                        txtOurComm.Text = "0.00";
    //                    }
    //                    txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //                    string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);
    //                    if (!string.IsNullOrEmpty(markuptype))
    //                        ddlMarkupType.SelectedValue = markuptype;

    //                    txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //                    ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);


    //                }
    //                else
    //                {
    //                    txtAgentMarkup.Text = "0.00";
    //                    txtOurComm.Text = "0.00";
    //                    txtMarkup.Text = "0.00";
    //                    txtDiscount.Text = "0.00";
    //                }

    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    protected void ddlAgent_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            
            //foreach (ListItem item in chkProduct.Items)
            //{
            //    item.Selected = false;
            //}
            //tblMarkup.Rows.Clear();
            Clear();
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataTable dtProducts = new DataTable();
            if (agentId > 0)
            {
                dtProducts = AgentMarkup.AgentProductGetList(RecordStatus.Activated, agentId);

                lnkFlight.Visible = lnkHotel.Visible = lnkPackage.Visible = lnkActivity.Visible = lnkInsurance.Visible = lnkCar.Visible = false;

                foreach (DataRow dr in dtProducts.Rows)
                {
                    if ((int)ProductType.Flight == Utility.ToInteger(dr["ProductActive"]))
                        lnkFlight.Visible = true;
                    else if ((int)ProductType.Hotel == Utility.ToInteger(dr["ProductActive"]))
                        lnkHotel.Visible = true;
                    else if ((int)ProductType.Packages == Utility.ToInteger(dr["ProductActive"]))
                        lnkPackage.Visible = true;
                    else if ((int)ProductType.Activity == Utility.ToInteger(dr["ProductActive"]))
                        lnkActivity.Visible = true;
                    else if ((int)ProductType.Insurance == Utility.ToInteger(dr["ProductActive"]))
                        lnkInsurance.Visible = true;
                    else if ((int)ProductType.Car == Utility.ToInteger(dr["ProductActive"]))
                        lnkCar.Visible = true;
                    else if ((int)ProductType.SightSeeing  == Utility.ToInteger(dr["ProductActive"]))
                        lnkSight.Visible = true;
                    else if ((int)ProductType.Transfers  == Utility.ToInteger(dr["ProductActive"]))
                        lnkTransfer.Visible = true;
                }

                if (lnkFlight.Visible)
                {
                    BindMarkupList(ProductType.Flight);    // default tab 
                    string script = "ShowMarkups('Flight');";
                    Utility.StartupScript(this.Page, script, "Flightcollapse");
                }
                else if (lnkHotel.Visible)
                {
                    BindMarkupList(ProductType.Hotel);    // default tab 
                    string script = "ShowMarkups('Hotel');";
                    Utility.StartupScript(this.Page, script, "Hotelcollapse");
                }
                else if (lnkPackage.Visible)
                {
                    BindMarkupList(ProductType.Packages);    // default tab 
                    string script = "ShowMarkups('Package');";
                    Utility.StartupScript(this.Page, script, "Packagecollapse");
                }
                else if (lnkActivity.Visible)
                {
                    BindMarkupList(ProductType.Activity);    // default tab 
                    string script = "ShowMarkups('Activity');";
                    Utility.StartupScript(this.Page, script, "Activitycollapse");
                }
                else if (lnkInsurance.Visible)
                {
                    BindMarkupList(ProductType.Insurance);    // default tab 
                    string script = "ShowMarkups('Insurance');";
                    Utility.StartupScript(this.Page, script, "Insurancecollapse");
                }
                else if (lnkCar.Visible)
                {
                    BindMarkupList(ProductType.Car);    // default tab 
                    string script = "ShowMarkups('Car');";
                    Utility.StartupScript(this.Page, script, "Carcollapse");
                }
                else if (lnkSight.Visible)
                {
                    BindMarkupList(ProductType.SightSeeing );    // default tab 
                    string script = "ShowMarkups('Sight');";
                    Utility.StartupScript(this.Page, script, "Sightcollapse");
                }
                else if (lnkTransfer.Visible)
                {
                    BindMarkupList(ProductType.Transfers );    // default tab 
                    string script = "ShowMarkups('Transfer');";
                    Utility.StartupScript(this.Page, script, "Transfercollapse");
                }
            }
            //BindMarkupList(ProductType.Hotel);
            //BindMarkupList(ProductType.Packages);
            //BindMarkupList(ProductType.Activity );
            //BindMarkupList(ProductType.Insurance);
            //BindMarkupList(ProductType.FixedDeparture );
           // BindMarkupList(ProductType.Car);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    //protected void btnUpdate_OnClick(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlFlightType = tblMarkup.Rows[0].Cells[0].FindControl("ddlFlightType_0") as DropDownList;
    //        DropDownList ddlJourneyType = tblMarkup.Rows[0].Cells[0].FindControl("ddlJourneyType_0") as DropDownList;
    //        DropDownList ddlCarrierType = tblMarkup.Rows[0].Cells[0].FindControl("ddlCarrierType_0") as DropDownList;
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1,string.Empty,-1,string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        for (int i = 0; i < chkProduct.Items.Count; i++)
    //        {
    //            if (chkProduct.Items[i].Selected)
    //            {
    //                int agent = 0;
    //                string find = string.Empty;
    //                string source = string.Empty;
    //                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
    //                if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //sources binding based on product
    //                {
    //                    DropDownList ddlSource = tblMarkup.Rows[0].Cells[i].FindControl("ddlSource_" + i.ToString()) as DropDownList;
    //                    source = ddlSource.SelectedItem.Text;
    //                    if (string.IsNullOrEmpty(source))
    //                    {
    //                        throw new Exception("Please select a source");
    //                    }
    //                }
    //                if (ddlAgent.SelectedItem.Value != "0")
    //                {
    //                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //                }
    //                TextBox txtAgentMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtAgentMarkup_" + i.ToString()) as TextBox;
    //                TextBox txtOurComm = tblMarkup.Rows[0].Cells[i].FindControl("txtOurComm_" + i.ToString()) as TextBox;
    //                TextBox txtMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtMarkUp_" + i.ToString()) as TextBox;
    //                DropDownList ddlMarkUp = tblMarkup.Rows[0].Cells[i].FindControl("ddlMarkupType_" + i.ToString()) as DropDownList;
    //                TextBox txtDiscount = tblMarkup.Rows[0].Cells[i].FindControl("txtDiscount_" + i.ToString()) as TextBox;
    //                DropDownList ddlDiscount = tblMarkup.Rows[0].Cells[i].FindControl("ddlDiscountType_" + i.ToString()) as DropDownList;


    //                //DataTable dtMarkUp = UpdateMarkup.GetMarkupRules(productId, source,agent);
    //                if (agent == 0)
    //                {
    //                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId IS NULL";
    //                }
    //                else
    //                {
    //                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + agent + "' AND transType='B2B'";
    //                }

    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                UpdateMarkup objUpdateMarkup = new UpdateMarkup();
    //                if (foundRows.Length > 0)
    //                {
    //                    objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
    //                    objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
    //                    objUpdateMarkup.HandlingFee = Utility.ToDecimal(foundRows[0]["HandlingFeeValue"]);
    //                   string handlingType = Utility.ToString(foundRows[0]["HandlingFeeType"]);
    //                    if (handlingType == "Percentage")
    //                        handlingType = "P";
    //                    else if (handlingType == "Fixed")
    //                         handlingType = "F";
    //                    objUpdateMarkup.HandlingType = handlingType;

    //                }
    //                objUpdateMarkup.ProductId = productId;
    //                objUpdateMarkup.SourceId = source;

    //                objUpdateMarkup.AgentId = agent;
    //                objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);
    //                objUpdateMarkup.OurCommission = Utility.ToDecimal(txtOurComm.Text);
    //                objUpdateMarkup.Markup = Utility.ToDecimal(txtMarkup.Text);
    //                objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
    //                objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
    //                objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;

    //                if(productId == 1)
    //                {
    //                    objUpdateMarkup.FlightType = ddlFlightType.SelectedValue;
    //                    objUpdateMarkup.JourneyType = ddlJourneyType.SelectedValue;
    //                    objUpdateMarkup.CarrierType = ddlCarrierType.SelectedValue;
    //                }

    //                objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
    //                objUpdateMarkup.UserId = 0;// to insert null for b2b user
    //                objUpdateMarkup.Save();
    //                chkProduct.Items[i].Selected = false;
    //            }
    //        }
    //        lblSuccessMsg.Text = "Updated successfully";
    //        //rblType.SelectedIndex = 0;
    //        //ddlAgent.SelectedIndex = 0;
    //        //ddlAgent.Style.Add("display", "none");
    //        tblMarkup.Rows.Clear();
    //        BindMarkupList();

    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
    //        errorMessage.InnerHtml = ex.Message;
    //        errMess.Style["display"] = "block";
    //    }
    //}

    //protected void btnClear_OnClick(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //rblType.SelectedIndex = 0;
    //        ddlAgent.SelectedIndex = 0;
    //        ddlAgent.Style.Add("display", "block");
    //        tblMarkup.Rows.Clear();
    //        chkProduct.ClearSelection();
    //        BindMarkupList();
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
    //    }
    //}

  
    //private void LoadControls()
    //{
    //    Label lblFlightType = (Label)tblMarkup.FindControl("lblFlightType_0");
    //    DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_0");
    //    Label lblJourneyType = (Label)tblMarkup.FindControl("lblJourneyType_0");
    //    DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_0");
    //    Label lblCarrierType = (Label)tblMarkup.FindControl("lblCarrierType_0");
    //    DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_0");

    //    dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //    for (int i = 0; i < chkProduct.Items.Count; i++)
    //    {
    //        Label lblAgentMarkup = (Label)tblMarkup.FindControl("lblAgentMarkup_" + i.ToString());
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + i.ToString());
    //        Label lblOurComm = (Label)tblMarkup.FindControl("lblOurComm_" + i.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + i.ToString());
    //        Label lblMarkup = (Label)tblMarkup.FindControl("lblMarkUp_" + i.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + i.ToString());
    //        Label lblMarkupType = (Label)tblMarkup.FindControl("lblmarkUpType_" + i.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + i.ToString());
    //        Label lblDiscount = (Label)tblMarkup.FindControl("lblDiscount_" + i.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + i.ToString());
    //        Label lblDiscountType = (Label)tblMarkup.FindControl("lblDiscountType_" + i.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + i.ToString());
    //        Label lblSource = (Label)tblMarkup.FindControl("lblSource_" + i.ToString());
    //        DropDownList ddlSource = (DropDownList)tblMarkup.FindControl("ddlSource_" + i.ToString());




    //        if (chkProduct.Items[i].Selected)
    //        {
    //            lblAgentMarkup.Style.Add("display", "block");
    //            txtAgentMarkup.Style.Add("display", "block");
    //            lblOurComm.Style.Add("display", "block");
    //            txtOurComm.Style.Add("display", "block");
    //            lblMarkup.Style.Add("display", "block");
    //            txtMarkup.Style.Add("display", "block");
    //            lblMarkupType.Style.Add("display", "block");
    //            ddlMarkupType.Style.Add("display", "block");
    //            lblDiscount.Style.Add("display", "block");
    //            txtDiscount.Style.Add("display", "block");
    //            lblDiscountType.Style.Add("display", "block");
    //            ddlDiscountType.Style.Add("display", "block");
    //            if (i == 0)
    //            {
    //                lblFlightType.Style.Add("display", "block");
    //                ddlFlightType.Style.Add("display", "block");
    //                lblJourneyType.Style.Add("display", "block");
    //                ddlJourneyType.Style.Add("display", "block");
    //                lblCarrierType.Style.Add("display", "block");
    //                ddlCarrierType.Style.Add("display", "block");
    //            }
    //            if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) // Sources is there Just displaying
    //            {
    //                lblSource.Style.Add("display", "block");
    //                ddlSource.Style.Add("display", "block");
    //            }
    //            if (i == 2 || i == 3 || i == 7 || i == 8) // Here source is not there Loading markup directly
    //            {
    //                int agent = 0;
    //                string find = string.Empty;
    //                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
    //                if (ddlAgent.SelectedItem.Value != "0")
    //                {
    //                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //                }
    //                if (agent == 0)
    //                {
    //                    find = "ProductId='" + productId + "' AND AgentId IS NULL";
    //                }
    //                else
    //                {
    //                    find = "ProductId='" + productId + "' AND AgentId = '" + agent + "'  AND transType='B2B'";
    //                }
    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                //DataTable dtMarkupDetails = UpdateMarkup.GetMarkupDetails(productId, agent);
    //                if (foundRows.Length > 0)
    //                {
    //                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //                    txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //                    txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //                    ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //                    txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //                    ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //                }
    //                else
    //                {
    //                    txtAgentMarkup.Text = "0.00";
    //                    txtOurComm.Text = "0.00";
    //                    txtMarkup.Text = "0.00";
    //                    txtDiscount.Text = "0.00";
    //                }
    //            }
    //        }
    //        else
    //        {
    //            lblAgentMarkup.Style.Add("display", "none");
    //            txtAgentMarkup.Style.Add("display", "none");
    //            lblOurComm.Style.Add("display", "none");
    //            txtOurComm.Style.Add("display", "none");
    //            lblMarkup.Style.Add("display", "none");
    //            txtMarkup.Style.Add("display", "none");
    //            lblMarkupType.Style.Add("display", "none");
    //            ddlMarkupType.Style.Add("display", "none");
    //            lblDiscount.Style.Add("display", "none");
    //            txtDiscount.Style.Add("display", "none");
    //            lblDiscountType.Style.Add("display", "none");
    //            ddlDiscountType.Style.Add("display", "none");
    //            if (i == 0)
    //            {
    //                lblFlightType.Style.Add("display", "none");
    //                ddlFlightType.Style.Add("display", "none");
    //                lblJourneyType.Style.Add("display", "none");
    //                ddlJourneyType.Style.Add("display", "none");
    //                lblCarrierType.Style.Add("display", "none");
    //                ddlCarrierType.Style.Add("display", "none");
    //            }
    //            if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //Checking only sources products only
    //            {
    //                lblSource.Style.Add("display", "none");
    //                ddlSource.Style.Add("display", "none");
    //            }
    //        }
    //    }
    //}

    //private void BindMarkupList()
    //{
    //    try
    //    {
    //        string agentType = string.Empty;

    //        int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
    //        if (Settings.LoginInfo.AgentType == AgentType.B2B)
    //        {
    //            if (agentId <= 0)
    //            {
    //                agentType = "B2B";
    //                agentId = Settings.LoginInfo.AgentId;
    //            }
    //        }
    //        if (agentId > 0)
    //        {
    //            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, agentId, agentType, string.Empty,string.Empty,string.Empty);
    //            if (dtMarkupList != null && dtMarkupList.Rows.Count > 0)
    //            {
    //                dlMarkup.Visible = true;
    //                lblMessage.Visible = false;
    //                dlMarkup.DataSource = dtMarkupList;
    //                ViewState["Markup"] = dtMarkupList;
    //                dlMarkup.DataBind();
    //                doPaging();
    //            }
    //            else
    //            {
    //                DisableDataList();
    //            }
    //        }
    //        else
    //        {
    //            DisableDataList();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
    //    }
    //}
    //private void DisableDataList()
    //{
    //    dlMarkup.Visible = false;
    //    btnPrev.Visible = false;
    //    btnFirst.Visible = false;
    //    btnLast.Visible = false;
    //    btnNext.Visible = false;
    //    lblMessage.Visible = true;
    //    lblCurrentPage.Text = string.Empty;
    //    lblMessage.Text = "No Results Found.";
    //}

    //protected void ddlSource_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlSource = sender as DropDownList;
    //        int agent = 0;
    //        int rowIndex = 0;
    //        string find = string.Empty;
    //        rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
    //        if (ddlAgent.SelectedItem.Value != "0")
    //        {
    //            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //        }
            
    //        string source = ddlSource.SelectedItem.Text;
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
    //        DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
    //        DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
    //        DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
    //        int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);
            
    //        //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1,string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        if (agent == 0)
    //        {
    //            find = "SourceId='" + source + "' AND AgentId IS NULL";
    //        }
    //        else
    //        {
    //            find = "ProductId='" + productId + "'AND SourceId='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //            DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //            DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

    //            if (sources != null && productId == 1 && ddlCarrierType != null)
    //            {
    //                ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
    //            }
    //        }
    //        DataRow[] foundRows = dtMarkupList.Select(find);
    //        if (foundRows.Length > 0)
    //        {
    //            txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //            txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //            txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //            ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //            txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //            ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //            if (productId == 1)
    //            {
    //                if (foundRows[0]["CarrierType"] != DBNull.Value)
    //                {
    //                    ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
    //                }
    //                if (foundRows[0]["FlightType"] == DBNull.Value)
    //                {
    //                    ddlFlightType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["FlightType"]))
    //                    {
    //                        case "All":
    //                            ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
    //                            break;
    //                        case "Domestic":
    //                            ddlFlightType.SelectedValue = "DOM";
    //                            break;
    //                        case "International":
    //                            ddlFlightType.SelectedValue = "INT";
    //                            break;
    //                    }
    //                }
    //                if(foundRows[0]["JourneyType"] == DBNull.Value)
    //                {
    //                    ddlJourneyType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["JourneyType"]))
    //                    {
    //                        case "All":
    //                            ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
    //                            break;
    //                        case "Onward":
    //                            ddlJourneyType.SelectedValue = "ONW";
    //                            break;
    //                        case "Return":
    //                            ddlJourneyType.SelectedValue = "RET";
    //                            break;
    //                    }
    //                }                        
    //            }
    //        }
    //        else
    //        {
    //            txtAgentMarkup.Text = "0.00";
    //            txtOurComm.Text = "0.00";
    //            txtMarkup.Text = "0.00";
    //            txtDiscount.Text = "0.00";
    //        }

    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    //protected void rblType_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (rblType.SelectedItem.Value == "A")
    //        {
    //            ddlAgent.Style.Add("display", "block");
    //            tblMarkup.Rows.Clear();
    //            foreach (ListItem item in chkProduct.Items)
    //            {
    //                item.Selected = false;
    //            }
    //        }
    //        else
    //        {
    //            ddlAgent.Style.Add("display", "none");
    //            ddlAgent.SelectedIndex = 0;
    //            tblMarkup.Rows.Clear();
    //            foreach (ListItem item in chkProduct.Items)
    //            {
    //                item.Selected = false;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    //protected void ddlAgent_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        foreach (ListItem item in chkProduct.Items)
    //        {
    //            item.Selected = false;
    //        }
    //        tblMarkup.Rows.Clear();
    //        BindMarkupList();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //private void AuthorizationCheck()
    //{
    //    if (Settings.LoginInfo == null)
    //    {
    //        Response.Redirect("AbandonSession.aspx", true);
    //    }
    //}

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Markup"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["Markup"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        //btnPrev.Visible = (!pagedData.IsFirstPage);
        //btnFirst.Visible = (!pagedData.IsFirstPage);
        //btnNext.Visible = (!pagedData.IsLastPage);
        //btnLast.Visible = (!pagedData.IsLastPage);
        //lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        //dlMarkup.DataSource = pagedData;
        //dlMarkup.DataBind();
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }
    #region button events

    //protected void btnUpdate_OnClick(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlFlightType = tblMarkup.Rows[0].Cells[0].FindControl("ddlFlightType_0") as DropDownList;
    //        DropDownList ddlJourneyType = tblMarkup.Rows[0].Cells[0].FindControl("ddlJourneyType_0") as DropDownList;
    //        DropDownList ddlCarrierType = tblMarkup.Rows[0].Cells[0].FindControl("ddlCarrierType_0") as DropDownList;
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        for (int i = 0; i < chkProduct.Items.Count; i++)
    //        {
    //            if (chkProduct.Items[i].Selected)
    //            {
    //                int agent = 0;
    //                string find = string.Empty;
    //                string source = string.Empty;
    //                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
    //                if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //sources binding based on product
    //                {
    //                    DropDownList ddlSource = tblMarkup.Rows[0].Cells[i].FindControl("ddlSource_" + i.ToString()) as DropDownList;
    //                    source = ddlSource.SelectedItem.Text;
    //                    if (string.IsNullOrEmpty(source) || ddlSource.SelectedIndex<1)
    //                    {
    //                        throw new Exception("Please select a source");
    //                    }
    //                }
    //                if (ddlAgent.SelectedItem.Value != "0")
    //                {
    //                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //                }
    //                TextBox txtAgentMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtAgentMarkup_" + i.ToString()) as TextBox;
    //                TextBox txtOurComm = tblMarkup.Rows[0].Cells[i].FindControl("txtOurComm_" + i.ToString()) as TextBox;
    //                TextBox txtMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtMarkUp_" + i.ToString()) as TextBox;
    //                DropDownList ddlMarkUp = tblMarkup.Rows[0].Cells[i].FindControl("ddlMarkupType_" + i.ToString()) as DropDownList;
    //                TextBox txtDiscount = tblMarkup.Rows[0].Cells[i].FindControl("txtDiscount_" + i.ToString()) as TextBox;
    //                DropDownList ddlDiscount = tblMarkup.Rows[0].Cells[i].FindControl("ddlDiscountType_" + i.ToString()) as DropDownList;


    //                //DataTable dtMarkUp = UpdateMarkup.GetMarkupRules(productId, source,agent);
    //                if (agent == 0)
    //                {
    //                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId IS NULL";
    //                }
    //                else
    //                {
    //                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + agent + "' AND transType='B2B'";
    //                }

    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                UpdateMarkup objUpdateMarkup = new UpdateMarkup();
    //                if (foundRows.Length > 0)
    //                {
    //                    objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
    //                    objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
    //                    objUpdateMarkup.HandlingFee = Utility.ToDecimal(foundRows[0]["HandlingFeeValue"]);
    //                    if (!string.IsNullOrEmpty(Convert.ToString(foundRows[0]["HandlingFeeType"])))
    //                        objUpdateMarkup.HandlingType = Utility.ToString(foundRows[0]["HandlingFeeType"]).Substring(0, 1);
    //                    else
    //                        objUpdateMarkup.HandlingType = "P";
   
    //                }
    //                else
    //                { objUpdateMarkup.HandlingType = "P"; }
    //                dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, string.Empty, string.Empty, string.Empty);
    //                DataRow[] Mrid = dtMarkupList.Select(find);
    //                if (Mrid.Length > 0)
    //                {
    //                    objUpdateMarkup.Id = Utility.ToInteger(Mrid[0]["MRId"]);
    //                }
    //                objUpdateMarkup.ProductId = productId;
                 
    //                objUpdateMarkup.SourceId = source;

    //                objUpdateMarkup.AgentId = agent;
    //                objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);
    //                objUpdateMarkup.OurCommission = Utility.ToDecimal(txtOurComm.Text);
    //                objUpdateMarkup.Markup = Utility.ToDecimal(txtMarkup.Text);
    //                objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
    //                objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
    //                objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;

    //                if (productId == 1)
    //                {
    //                    objUpdateMarkup.FlightType = ddlFlightType.SelectedValue;
    //                    objUpdateMarkup.JourneyType = ddlJourneyType.SelectedValue;
    //                    objUpdateMarkup.CarrierType = ddlCarrierType.SelectedValue;
    //                }

    //                objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
    //                objUpdateMarkup.UserId = 0;// to insert null for b2b user
    //                objUpdateMarkup.Save();
    //                chkProduct.Items[i].Selected = false;
    //            }
    //        }
    //        lblSuccessMsg.Text = "Updated successfully";
    //        //rblType.SelectedIndex = 0;
    //        //ddlAgent.SelectedIndex = 0;
    //        //ddlAgent.Style.Add("display", "none");
    //        tblMarkup.Rows.Clear();
    //        //BindMarkupList();

    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
    //        errorMessage.InnerHtml = ex.Message;
    //        errMess.Style["display"] = "block";
    //    }
    //}

    protected void btnClear_OnClick(object sender, EventArgs e)
    {
        try
        {
            //rblType.SelectedIndex = 0;
            ddlAgent.SelectedIndex = 0;
            ddlAgent.Style.Add("display", "block");
            Clear();
            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

          //  Response.Redirect("AgentMarkupUpdate.aspx", true);
            // tblMarkup.Rows.Clear();
            // chkProduct.ClearSelection();
            //BindMarkupList();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
        }
    }

    private void Clear()
    {
        try
        {

            hdfFlightMode.Value  = "0";
            hdfHotelMode.Value = "0";
            hdfPackageMode.Value = "0";
            hdfActivityMode.Value = "0";
            hdfInsurancemode.Value = "0";
            hdfFixedDepMode.Value = "0";
            hdfCarMode.Value = "0";
            hdfSightMode.Value = "0";
            hdfTransferMode.Value = "0";
            gvFlightMkp.DataSource = null;
            gvFlightMkp.DataBind();

            gvHotelMkp.DataSource = null;
            gvHotelMkp.DataBind();

            gvPackageMkp.DataSource = null;
            gvPackageMkp.DataBind();

            gvActivityMkp.DataSource = null;
            gvActivityMkp.DataBind();

            gvInsuranceMkp.DataSource = null;
            gvInsuranceMkp.DataBind();

            gvCarMkp.DataSource = null;
            gvCarMkp.DataBind();

            gvSightMkp.DataSource = null;
            gvSightMkp.DataBind();

            gvTransferMkp.DataSource = null;
            gvTransferMkp.DataBind();

            lnkFlight.Visible = lnkHotel.Visible = lnkPackage.Visible = lnkActivity.Visible = lnkInsurance.Visible = lnkCar.Visible = lnkSight.Visible =lnkTransfer.Visible = true;


        }
        catch { throw; }
    }

    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

    #endregion

    private void Save(ProductType productTYPE, DataTable dt)
    {
        try
        {
            //1-Flight   2-Hotel    3-Packages     4-Activity  5-Insurance   6-SightSeeing   9-Transfer    11- FixedDeparture   13-Car

            AgentMarkup mkpDetails = new AgentMarkup();
            long MRid = -1;
            
            
            if (dt.Rows.Count > 0)
            {
                if (Utility.ToLong(dt.Rows[0]["MRId"]) > 0)
                    mkpDetails.Id = Utility.ToInteger(dt.Rows[0]["MRId"]);
                else
                    mkpDetails.Id = -1;
                //Added by Anji
                if (Utility.ToInteger(dt.Rows[0]["MRDId"]) > 0)
                    mkpDetails.MrDId = Utility.ToInteger(dt.Rows[0]["MRDId"]);
                else
                    mkpDetails.MrDId = -1;

                mkpDetails.ProductId = (int)productTYPE;

                mkpDetails.SourceId = Utility.ToString(dt.Rows[0]["SourceId"]);

                mkpDetails.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                mkpDetails.AgentMarkupVal = Utility.ToDecimal(dt.Rows[0]["AgentMarkup"]); 
                mkpDetails.OurCommission = Utility.ToDecimal(dt.Rows[0]["OurCommission"]);
                mkpDetails.Markup = Utility.ToDecimal(dt.Rows[0]["Markup"]);
                mkpDetails.MarkupType = Utility.ToString(dt.Rows[0]["MarkupType"]);
                mkpDetails.Discount = Utility.ToDecimal(dt.Rows[0]["Discount"]);
                mkpDetails.DiscountType = Utility.ToString(dt.Rows[0]["DiscountType"]);
                if (productTYPE == ProductType.Flight )
                {
                    mkpDetails.FlightType = Utility.ToString(dt.Rows[0]["FlightType"]);
                    mkpDetails.JourneyType = Utility.ToString(dt.Rows[0]["JourneyType"]);
                    mkpDetails.CarrierType = Utility.ToString(dt.Rows[0]["CarrierType"]);
                    //Added by somasekhar for Booking Type 
                    mkpDetails.BookingType = Utility.ToString(dt.Rows[0]["BookingType"]);
                }
                if(productTYPE==ProductType.Hotel)
                  mkpDetails.BookingType = Utility.ToString(dt.Rows[0]["BookingType"]);
                mkpDetails.MFDiscount = Utility.ToDecimal(dt.Rows[0]["MFDiscount"]);
                mkpDetails.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
                mkpDetails.UserId = 0;// to insert null for b2b user
                //Added by Anji on 24/02/2020, assigning handlingfee and handlingType.
                if (Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]) > 0)
                {
                    mkpDetails.HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
                }
                if (Utility.ToString(dt.Rows[0]["HandlingFeeType"]).Length > 0)
                {
                    mkpDetails.HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
                }
                //End.
                mkpDetails.Save();

                BindMarkupList(productTYPE);
             }
            
            lblSuccessMsg.Visible = true;
           // lblSuccessMsg.Text = Formatter.ToMessage("Tour Booking ", txtBookingNo.Text, CT.TicketReceipt.Common.Action.Saved);

            //  long reportId = tPurchaseOrder.RetTPOId;
            //string script = "window.open('PrintTicketPurchaseOrder.aspx?phId=" + Utility.ToString(reportId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            // Utility.StartupScript(this.Page, script, "printVisaSaleReport");
            // Clear();
        }
        catch
        {
            throw;
        }

    }

    private DataRow SetMarkupDetails(DataRow dr, GridViewRow gvRow, string mode, ProductType pdtType , int mrdid,decimal handlingfee,string handlingtype) //decimal discount,string discountType)
    {
        //DataTable dt = new DataTable();
        try
        {

            dr["MRId"] = ((HiddenField)gvRow.FindControl(mode + "HdfMRId")).Value;
            if (mrdid > 0)
            {
                dr["MRDId"] = mrdid;
            }
           

            if (pdtType != ProductType.Activity && pdtType != ProductType.Packages)
            {
                dr["SourceId"] = ((DropDownList)gvRow.FindControl(mode + "ddlSource")).SelectedItem.Value;
            }
            if (pdtType == ProductType.Flight)
            { 
                dr["FlightType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlFlightType")).SelectedItem.Value);
                dr["JourneyType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlJourneyType")).SelectedItem.Value);
                dr["CarrierType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlCarrierType")).SelectedItem.Value);
                // Added by Somasekhar on 19-02-2019  for Booking Type
                dr["BookingType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlBookingType")).SelectedItem.Value);
            }
            if (pdtType == ProductType.Hotel)
            {
                dr["BookingType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlBookingType")).SelectedItem.Value);
                dr["MFDiscount"] = ((TextBox)gvRow.FindControl(mode + "txtMFDiscount")).Text;
            }
            //dr["pax_bkd_nationality"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlNationality")).SelectedItem.Value);
            //dr["pax_bkd_nationality_name"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlNationality")).SelectedItem.Text);
            dr["AgentMarkup"] = ((TextBox)gvRow.FindControl(mode + "txtAgentMarkup")).Text;
            dr["OurCommission"] = ((TextBox)gvRow.FindControl(mode + "txtOurCommission")).Text;
            dr["Markup"] = ((TextBox)gvRow.FindControl(mode + "txtMarkup")).Text;
            dr["MarkupType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlMarkupType")).SelectedItem.Value);
            //if (pdtType == ProductType.Insurance || pdtType==ProductType.SightSeeing || pdtType == ProductType.Transfers )
            //{
                dr["Discount"] = ((TextBox)gvRow.FindControl(mode + "txtDiscount")).Text;
                dr["DiscountType"] = Utility.ToString(((DropDownList)gvRow.FindControl(mode + "ddlDiscountType")).SelectedItem.Value);
            //}
            // dt.Rows.Add(dr);
            //Added by Anji on 24/02/2020 to store handlingfee and hanldingtype.
            if (handlingfee > 0)
            {
                dr["HandlingFeeValue"] = handlingfee;
            }
            if (!string.IsNullOrEmpty(handlingtype))
            {
                dr["HandlingFeeType"] = handlingtype;
            }
            //End.
            return dr;
        }
        catch { throw; }
    }
    #region GridEvents
    protected void gvFlightMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFlightMkp.PageIndex = e.NewPageIndex;
            //gvFlightMkp.EditIndex = -1;
            //FilterSearchGrid();
            //BindGrid();
            BindMarkupList(ProductType.Flight);
            hdfFlightMode.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlightMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            gvFlightMkp.EditIndex = -1;
            hdfFlightMode.Value = "0";
            BindMarkupList(ProductType.Flight);
           

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlightMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvFlightMkp.FooterRow;
                DataTable dt= AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

               DataRow  drMkp =  SetMarkupDetails(dr, gvRow, "FT", ProductType.Flight,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Flight,dt);
                //BindMarkupList()
                //BookingPaxDetails.Rows[]
                // lnkSettlement_Click(null, null);

                string script = "ShowMarkups('Flight');";
                Utility.StartupScript(this.Page, script, "Flightcollapse");
            }
        }
        catch (Exception ex)
        {

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvFlightMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {

            GridViewRow gvRow = gvFlightMkp.Rows[e.RowIndex];
            int mrId = Utility.ToInteger(gvFlightMkp.DataKeys[e.RowIndex].Value);
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            AgentMarkup.DeleteMarkupRules(mrId, agentId,Utility.ToInteger(Settings.LoginInfo.UserID) );
            BindMarkupList(ProductType.Flight);

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlightMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {

        try
        {
            
            if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if(hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvFlightMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Flight);

            int  serial = Utility.ToInteger(gvFlightMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvFlightMkp.Rows[e.NewEditIndex];
            
            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.Flight);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Somasekhar on 19-02-2019 For Booking Type
            DropDownList ddlBookingType = (DropDownList)gvRow.FindControl("EITddlBookingType");
            ddlBookingType.SelectedValue = Utility.ToString(dr["BookingType"]);
            //------------------------------

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvFlightMkp.FooterRow.Visible = false;
            hdfFlightMode.Value = "2";

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");
        }
        catch (Exception ex)
        {

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlightMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvFlightMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvFlightMkp.DataKeys[e.RowIndex].Value);
            //TextBox txtItem = (TextBox)gvRow.FindControl("txtAirLine");
            //string airline = txtItem.Text.Trim();
            //if (BookingPaxDetails.Select(string.Format("pod_item_name='{0}'AND pod_id<>{1}", airline, serial)).Length > 0)
            //{
            //     throw new Exception("Item cannot be duplicated !");
            // }

            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
             Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
                DataRow dr = dt.NewRow();
                dr["MRId"] = serial;
                
            DataRow  drMkp =  SetMarkupDetails(dr, gvRow, "EIT" ,ProductType.Flight,Mrdid,HandlingFee,HandlingType);
                dt.Rows.Add(drMkp);
                Save(ProductType.Flight, dt);
            //lblSuccessMsg.Text = string.Format("Location  '{0}'  is updated successfully!", location.Code);          
            //lblSuccessMsg.Text = Formatter.ToMessage("Location Code", location.Code, Action.Updated);
            gvFlightMkp.EditIndex = -1;
            hdfFlightMode.Value = "0";
            BindMarkupList(ProductType.Flight);            


            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");
        }
        catch (Exception ex)
        {

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }


    //Hotel MKP
    protected void gvHotelMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvHotelMkp.PageIndex = e.NewPageIndex;
            gvHotelMkp.EditIndex = -1;
            BindMarkupList(ProductType.Hotel);
            hdfHotelMode.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }


    }
    protected void gvHotelMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvHotelMkp.EditIndex = -1;
        hdfHotelMode.Value = "0";
        BindMarkupList(ProductType.Hotel);
        

        string script = "ShowMarkups('Hotel');";        
        Utility.StartupScript(this.Page, script, "Hotelcollapse");
    }
    protected void gvHotelMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvHotelMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT",ProductType.Hotel,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Hotel, dt);

                string script = "ShowMarkups('Hotel');";
                Utility.StartupScript(this.Page, script, "Hotelcollapse");
            }
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Hotel');";
            Utility.StartupScript(this.Page, script, "Hotelcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvHotelMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvHotelMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }
            gvHotelMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Hotel);

            int serial = Utility.ToInteger(gvHotelMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvHotelMkp.Rows[e.NewEditIndex];

            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.Hotel);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 17-03-2020 For Booking Type
            DropDownList ddlBookingType = (DropDownList)gvRow.FindControl("EITddlBookingType");
            ddlBookingType.SelectedValue = Utility.ToString(dr["BookingType"]);
            //------------------------------
            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvHotelMkp.FooterRow.Visible = false;
            hdfHotelMode.Value = "2";

            string script = "ShowMarkups('Hotel');";
            Utility.StartupScript(this.Page, script, "Hotelcollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Hotel');";
            Utility.StartupScript(this.Page, script, "Hotelcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvHotelMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvHotelMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvHotelMkp.DataKeys[e.RowIndex].Value);
         
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
             //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
             Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            dr["MRId"] = serial;
            

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.Hotel,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.Hotel, dt);
            
            gvHotelMkp.EditIndex = -1;
            hdfHotelMode.Value = "0";
            BindMarkupList(ProductType.Hotel);            

            string script = "ShowMarkups('Hotel');";
            Utility.StartupScript(this.Page, script, "Hotelcollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Hotel');";
            Utility.StartupScript(this.Page, script, "Hotelcollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    //Packages

    protected void gvPackageMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvPackageMkp.PageIndex = e.NewPageIndex;
            gvPackageMkp.EditIndex = -1;
            BindMarkupList(ProductType.Packages);
            hdfPackageMode.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvPackageMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        { 
        gvPackageMkp.EditIndex = -1;
        hdfPackageMode.Value = "0";
        BindMarkupList(ProductType.Packages);      

        string script = "ShowMarkups('Package');";
        Utility.StartupScript(this.Page, script, "Packagecollapse");
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }

    }
    protected void gvPackageMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvPackageMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.Packages,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Packages, dt);

                string script = "ShowMarkups('Package');";
                Utility.StartupScript(this.Page, script, "Packagecollapse");
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvPackageMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvPackageMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
             if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }           
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvPackageMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Packages);

            int serial = Utility.ToInteger(gvPackageMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvPackageMkp.Rows[e.NewEditIndex];

            //DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            //BindSource(ddlSource, ProductType.Flight);
            //ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvPackageMkp.FooterRow.Visible = false;
            hdfPackageMode.Value = "2";

            string script = "ShowMarkups('Package');";
            Utility.StartupScript(this.Page, script, "Packagecollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Package');";
            Utility.StartupScript(this.Page, script, "Packagecollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvPackageMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        { 
        GridViewRow gvRow = gvPackageMkp.Rows[e.RowIndex];
        int serial = Utility.ToInteger(gvPackageMkp.DataKeys[e.RowIndex].Value);
       
        DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
        dr["MRId"] = serial;

        DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.Packages,Mrdid,HandlingFee,HandlingType);
        dt.Rows.Add(drMkp);
        Save(ProductType.Packages, dt);
      
        gvPackageMkp.EditIndex = -1;
        hdfPackageMode.Value = "0";
        BindMarkupList(ProductType.Packages);        

            string script = "ShowMarkups('Package');";
            Utility.StartupScript(this.Page, script, "Packagecollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Package');";
            Utility.StartupScript(this.Page, script, "Packagecollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    //Activity

    protected void gvActivityMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvActivityMkp.PageIndex = e.NewPageIndex;
            gvActivityMkp.EditIndex = -1;
            BindMarkupList(ProductType.Activity);
            hdfActivityMode.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvActivityMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvActivityMkp.EditIndex = -1;
            hdfActivityMode.Value = "0";
            BindMarkupList(ProductType.Activity);


            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvActivityMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvActivityMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.Activity,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Activity, dt);


                string script = "ShowMarkups('Activity');";
                Utility.StartupScript(this.Page, script, "Activitycollapse");
            }
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvActivityMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvActivityMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {

        try
        {
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }           
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvActivityMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Activity);

            int serial = Utility.ToInteger(gvActivityMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvActivityMkp.Rows[e.NewEditIndex];

            //DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            //BindSource(ddlSource, ProductType.Flight);
            //ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvActivityMkp.FooterRow.Visible = false;
            hdfActivityMode.Value = "2";

            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvActivityMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvActivityMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvActivityMkp.DataKeys[e.RowIndex].Value);
          
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            dr["MRId"] = serial;

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.Activity,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.Activity, dt);
            
            gvActivityMkp.EditIndex = -1;
            hdfActivityMode.Value = "0";
            BindMarkupList(ProductType.Activity);                       

            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    //Insurance
   
    protected void gvInsuranceMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvInsuranceMkp.PageIndex = e.NewPageIndex;
            gvInsuranceMkp.EditIndex = -1;
            BindMarkupList(ProductType.Insurance);
            hdfInsurancemode.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvInsuranceMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvInsuranceMkp.EditIndex = -1;
            hdfInsurancemode.Value = "0";
            BindMarkupList(ProductType.Insurance);           

            string script = "ShowMarkups('Insurance');";
            Utility.StartupScript(this.Page, script, "Insurancecollapse");
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvInsuranceMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvInsuranceMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.Insurance,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Insurance, dt);

                string script = "ShowMarkups('Insurance');";
                Utility.StartupScript(this.Page, script, "Insurancecollapse");
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvInsuranceMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvInsuranceMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {

        try
        {
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }            
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvInsuranceMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Insurance);

            int serial = Utility.ToInteger(gvInsuranceMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvInsuranceMkp.Rows[e.NewEditIndex];

            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.Insurance);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

           

            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);


            gvInsuranceMkp.FooterRow.Visible = false;
            hdfInsurancemode.Value = "2";

            string script = "ShowMarkups('Insurance');";
            Utility.StartupScript(this.Page, script, "Insurancecollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Insurance');";
            Utility.StartupScript(this.Page, script, "Insurancecollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvInsuranceMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvInsuranceMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvInsuranceMkp.DataKeys[e.RowIndex].Value);
            
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            dr["MRId"] = serial;

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.Insurance,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.Insurance , dt);
            
            gvInsuranceMkp.EditIndex = -1;
            hdfInsurancemode.Value = "0";
            BindMarkupList(ProductType.Insurance );            

            string script = "ShowMarkups('Insurance');";
            Utility.StartupScript(this.Page, script, "Insurancecollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    //Fixed Departure

    protected void gvFixedDepMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFixedDepMkp.PageIndex = e.NewPageIndex;
            gvFixedDepMkp.EditIndex = -1;
            BindMarkupList(ProductType.FixedDeparture);
            hdfFixedDepMode.Value = "0";

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvFixedDepMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvFixedDepMkp.EditIndex = -1;
            hdfFixedDepMode.Value = "0";
            BindMarkupList(ProductType.FixedDeparture);
        


        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFixedDepMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvFixedDepMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.FixedDeparture,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.FixedDeparture, dt);
                //BindMarkupList()
                //BookingPaxDetails.Rows[]
                // lnkSettlement_Click(null, null);

                //FilterSearchGrid();
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvFixedDepMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvFixedDepMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {   
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }            
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvFixedDepMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.FixedDeparture);

            int serial = Utility.ToInteger(gvFixedDepMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvFixedDepMkp.Rows[e.NewEditIndex];

            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.FixedDeparture);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvFixedDepMkp.FooterRow.Visible = false;
            hdfFixedDepMode.Value = "2";

            //string script = "$('.nav-tabs li:eq(1) a').tab('show')";
            //Utility.StartupScript(this.Page, script, "collapse");
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFixedDepMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvFixedDepMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvFixedDepMkp.DataKeys[e.RowIndex].Value);
            //TextBox txtItem = (TextBox)gvRow.FindControl("txtAirLine");
            //string airline = txtItem.Text.Trim();
            //if (BookingPaxDetails.Select(string.Format("pod_item_name='{0}'AND pod_id<>{1}", airline, serial)).Length > 0)
            //{
            //     throw new Exception("Item cannot be duplicated !");
            // }

            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            dr["MRId"] = serial;

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.FixedDeparture,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.FixedDeparture, dt);
            //lblSuccessMsg.Text = string.Format("Location  '{0}'  is updated successfully!", location.Code);          
            //lblSuccessMsg.Text = Formatter.ToMessage("Location Code", location.Code, Action.Updated);
            gvFixedDepMkp.EditIndex = -1;
            hdfFixedDepMode.Value = "0";
            BindMarkupList(ProductType.FixedDeparture);            
            //FilterSearchGrid();

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }


    //Car

    protected void gvCarMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvCarMkp.PageIndex = e.NewPageIndex;
            gvCarMkp.EditIndex = -1;
            BindMarkupList(ProductType.Car);
            hdfCarMode.Value = "0";

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvCarMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            gvCarMkp.EditIndex = -1;
            hdfCarMode.Value = "0";
            BindMarkupList(ProductType.Car);            

            string script = "ShowMarkups('Car');";
            Utility.StartupScript(this.Page, script, "Carcollapse");
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvCarMkp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvCarMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.Car,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Car, dt);

                string script = "ShowMarkups('Car');";
                Utility.StartupScript(this.Page, script, "Carcollapse");
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvCarMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvCarMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvCarMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Car);

            int serial = Utility.ToInteger(gvCarMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvCarMkp.Rows[e.NewEditIndex];

            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.Car);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvCarMkp.FooterRow.Visible = false;
            hdfCarMode.Value = "2";

            string script = "ShowMarkups('Car');";
            Utility.StartupScript(this.Page, script, "Carcollapse");
        }
        catch (Exception ex)
        {

            string script = "ShowMarkups('Car');";
            Utility.StartupScript(this.Page, script, "Carcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvCarMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvCarMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvCarMkp.DataKeys[e.RowIndex].Value);
           
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            
            dr["MRId"] = serial;

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.Car,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.Car, dt);
            
            gvCarMkp.EditIndex = -1;
            hdfCarMode.Value = "0";
            BindMarkupList(ProductType.Car);            

            string script = "ShowMarkups('Car');";
            Utility.StartupScript(this.Page, script, "Carcollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Car');";
            Utility.StartupScript(this.Page, script, "Carcollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }



    //Sight Seeing
    //Sight Seeing MKP
    protected void gvSightMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSightMkp.PageIndex = e.NewPageIndex;
            gvSightMkp.EditIndex = -1;
            BindMarkupList(ProductType.SightSeeing);
            hdfSightMode.Value = "0";

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvSightMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvSightMkp.EditIndex = -1;
        hdfSightMode.Value = "0";
        BindMarkupList(ProductType.SightSeeing);


        string script = "ShowMarkups('Sight');";
        Utility.StartupScript(this.Page, script, "Sightcollapse");
    }
    protected void gvSightMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvSightMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.SightSeeing,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.SightSeeing, dt);

                string script = "ShowMarkups('Sight');";
                Utility.StartupScript(this.Page, script, "Sightcollapse");
            }
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Sight');";
            Utility.StartupScript(this.Page, script, "Sightcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvSightMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvSightMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfTransferMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Transfer Markup Details are in edit mode');", "Err");
                throw new Exception("Transfer Markup Details are in edit mode");
            }

            gvSightMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.SightSeeing);

            int serial = Utility.ToInteger(gvSightMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvSightMkp.Rows[e.NewEditIndex];

            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.SightSeeing);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvSightMkp.FooterRow.Visible = false;
            hdfSightMode.Value = "2";

            string script = "ShowMarkups('Sight');";
            Utility.StartupScript(this.Page, script, "Sightcollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Sight');";
            Utility.StartupScript(this.Page, script, "Sightcollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvSightMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvSightMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvSightMkp.DataKeys[e.RowIndex].Value);

            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            dr["MRId"] = serial;

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.SightSeeing,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.SightSeeing , dt);

            gvSightMkp.EditIndex = -1;
            hdfSightMode.Value = "0";
            BindMarkupList(ProductType.SightSeeing);

            string script = "ShowMarkups('Sight');";
            Utility.StartupScript(this.Page, script, "Sightcollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Sight');";
            Utility.StartupScript(this.Page, script, "Sightcollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    //Transfer

    protected void gvTransferMkp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvTransferMkp.PageIndex = e.NewPageIndex;
            gvTransferMkp.EditIndex = -1;
            BindMarkupList(ProductType.Transfers);
            hdfTransferMode.Value = "0";

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvTransferMkp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvTransferMkp.EditIndex = -1;
        hdfTransferMode.Value = "0";
        BindMarkupList(ProductType.Transfers);


        string script = "ShowMarkups('Transfer');";
        Utility.StartupScript(this.Page, script, "Transfercollapse");
    }
    protected void gvTransferMkp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvTransferMkp.FooterRow;
                DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(0);
                DataRow dr = dt.NewRow();
                dr["MRId"] = -1;

                DataRow drMkp = SetMarkupDetails(dr, gvRow, "FT", ProductType.Transfers,0,0,"");
                dt.Rows.Add(drMkp);
                //BindGrid();
                Save(ProductType.Transfers, dt);

                string script = "ShowMarkups('Transfer');";
                Utility.StartupScript(this.Page, script, "Transfercollapse");
            }
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Transfer');";
            Utility.StartupScript(this.Page, script, "Transfercollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvTransferMkp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvTransferMkp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            if (hdfFlightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Flight Markup Details are in edit mode');", "Err");
                throw new Exception("Flight Markup Details are in edit mode");
            }
            else if (hdfHotelMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Hotel Markup Details are in edit mode');", "Err");
                throw new Exception("Hotel Markup Details are in edit mode");
            }
            else if (hdfPackageMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Package Markup Details are in edit mode');", "Err");
                throw new Exception("Package Markup Details are in edit mode");
            }
            else if (hdfActivityMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Activity Markup Details are in edit mode');", "Err");
                throw new Exception("Activity Markup Details are in edit mode");
            }
            else if (hdfInsurancemode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Insurance Markup Details are in edit mode');", "Err");
                throw new Exception("Insurance Markup Details are in edit mode");
            }
            else if (hdfFixedDepMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Fixed Dep. Markup Details are in edit mode');", "Err");
                throw new Exception("Fixed Dep. Markup Details are in edit mode");
            }
            else if (hdfCarMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Car Markup Details are in edit mode');", "Err");
                throw new Exception("Car Markup Details are in edit mode");
            }
            else if (hdfSightMode.Value != "0")
            {
                Utility.StartupScript(this.Page, "alert('Sight Seeing Markup Details are in edit mode');", "Err");
                throw new Exception("Sight Seeing Markup Details are in edit mode");
            }

            gvTransferMkp.EditIndex = e.NewEditIndex;
            BindMarkupList(ProductType.Transfers);

            int serial = Utility.ToInteger(gvTransferMkp.DataKeys[e.NewEditIndex].Value);
            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            DataRow dr = dt.Rows[0];

            GridViewRow gvRow = gvTransferMkp.Rows[e.NewEditIndex];

            DropDownList ddlSource = (DropDownList)gvRow.FindControl("EITddlSource");
            BindSource(ddlSource, ProductType.Transfers);
            ddlSource.SelectedValue = Utility.ToString(dr["SourceId"]);

            //DropDownList ddlFlightType = (DropDownList)gvRow.FindControl("EITddlFlightType");
            //ddlFlightType.SelectedValue = Utility.ToString(dr["FlightType"]);

            //DropDownList ddlJourneyTpe = (DropDownList)gvRow.FindControl("EITddlJourneyType");
            //ddlJourneyTpe.SelectedValue = Utility.ToString(dr["JourneyType"]);

            //DropDownList ddlCarrierType = (DropDownList)gvRow.FindControl("EITddlCarrierType");
            //ddlCarrierType.SelectedValue = Utility.ToString(dr["CarrierType"]);

            DropDownList ddlMarkupType = (DropDownList)gvRow.FindControl("EITddlMarkupType");
            ddlMarkupType.SelectedValue = Utility.ToString(dr["MarkupType"]);

            //Added by Anji on 21-01-2020 For Discount Type
            DropDownList ddlDiscountType = (DropDownList)gvRow.FindControl("EITddlDiscountType");
            ddlDiscountType.SelectedValue = Utility.ToString(dr["DiscountType"]);
            //End

            gvTransferMkp.FooterRow.Visible = false;
            hdfTransferMode.Value = "2";

            string script = "ShowMarkups('Transfer');";
            Utility.StartupScript(this.Page, script, "Transfercollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Transfer');";
            Utility.StartupScript(this.Page, script, "Transfercollapse");

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvTransferMkp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvTransferMkp.Rows[e.RowIndex];
            int serial = Utility.ToInteger(gvTransferMkp.DataKeys[e.RowIndex].Value);

            DataTable dt = AgentMarkup.GetAgentMarkupDetailsbyMRId(serial);
            //Added by Anji on 10/12/19 , regarding when duplicate mrid avodaing to update.
            Mrdid = Utility.ToInteger(dt.Rows[0]["MRDId"]);
            HandlingFee = Utility.ToDecimal(dt.Rows[0]["HandlingFeeValue"]);
            HandlingType = Utility.ToString(dt.Rows[0]["HandlingFeeType"]);
            dt.Clear();
            //End
            DataRow dr = dt.NewRow();
            dr["MRId"] = serial;

            DataRow drMkp = SetMarkupDetails(dr, gvRow, "EIT", ProductType.Transfers,Mrdid,HandlingFee,HandlingType);
            dt.Rows.Add(drMkp);
            Save(ProductType.Transfers, dt);

            gvTransferMkp.EditIndex = -1;
            hdfTransferMode .Value = "0";
            BindMarkupList(ProductType.Transfers);

            string script = "ShowMarkups('Transfer');";
            Utility.StartupScript(this.Page, script, "Transfercollapse");
        }
        catch (Exception ex)
        {
            string script = "ShowMarkups('Transfer');";
            Utility.StartupScript(this.Page, script, "Transfercollapse");

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #endregion

    #region Link Event

    protected void lnkFlight_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Flight);

            string script = "ShowMarkups('Flight');";
            Utility.StartupScript(this.Page, script, "Flightcollapse");
        }
        catch { throw; }
    }


    protected void lnkHotel_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Hotel);
            string script = "ShowMarkups('Hotel');";
//            string script = "$('.nav-tabs li:eq(1) a').tab('show')";
          Utility.StartupScript(this.Page, script, "Hotelcollapse");
        }
        catch { throw; }
    }
    

    protected void lnkPackage_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Packages);

            string script = "ShowMarkups('Package');";
            Utility.StartupScript(this.Page, script, "Packagecollapse");
        }
        catch { throw; }
    }

    protected void lnkActivity_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Activity);

            string script = "ShowMarkups('Activity');";
            Utility.StartupScript(this.Page, script, "Activitycollapse");
        }
        catch { throw; }
    }

    protected void lnkInsurance_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Insurance);

            string script = "ShowMarkups('Insurance');";
            Utility.StartupScript(this.Page, script, "Insurancecollapse");
        }
        catch { throw; }
    }

    protected void lnkCar_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Car);

            string script = "ShowMarkups('Car');";
            Utility.StartupScript(this.Page, script, "Carcollapse");
        }
        catch { throw; }
    }


    protected void lnkSight_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.SightSeeing);

            string script = "ShowMarkups('Sight');";
            Utility.StartupScript(this.Page, script, "Sightcollapse");
        }
        catch { throw; }
    }

    protected void lnkTransfer_Click(object sender, EventArgs e)
    {
        try
        {
            BindMarkupList(ProductType.Transfers);

            string script = "ShowMarkups('Transfer');";
            Utility.StartupScript(this.Page, script, "Transfercollapse");
        }
        catch { throw; }
    }


    #endregion

}
