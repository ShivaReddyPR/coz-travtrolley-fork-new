﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Core;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

public partial class RegisterListGUI : CT.Core.ParentPage
{
    private string RL_DETAIL_SESSION = "_RLDeatilList";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    BindGrid();

                }
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }

    }
    #region gvRegisterList(GridView Control) Events

    protected void gvRegisterList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvRegisterList.PageIndex = e.NewPageIndex;

            FilterSearchGrid();


        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void DeleteRegisteredUser(object sender, EventArgs e)
    {
        try
        {
            ImageButton lnkDeleteUser = (ImageButton)sender;
            int RegId = Utility.ToInteger(lnkDeleteUser.CommandArgument);
            AgentRegistration.UpdateAgentRegStatus(RegId, "D");
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvRegisterList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            long regId = Utility.ToLong(gvRegisterList.SelectedValue);
            Response.Redirect("AgentMaster.aspx?RegId=" + regId);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }



    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {
            FilterSearchGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void FilterSearchGrid()
    {
        try
        {
            string[,] filterValues = { { "RLtxtAgentName", "reg_agencyname" }, 
                                       { "RLtxtAgentAddress", "reg_address" }, 
                                       { "RLtxtAgentPhone1", "reg_telephone" }
                                       //{ "RLtxtAgentEmail1", "reg_email" } 
                                                                                                                
                                    };
            CommonGrid grid = new CommonGrid();
            grid.FilterGridView(gvRegisterList, RegisterListDetails.Copy(), filterValues);
        }
        catch
        {
            throw;
        }
    }


    private DataTable RegisterListDetails
    {
        get
        {
            DataTable dtRL = null;
            dtRL = ViewState[RL_DETAIL_SESSION] as DataTable;
            return dtRL;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["reg_id"] };
            ViewState[RL_DETAIL_SESSION] = value;
        }
    }

    private void BindGrid()
    {
        try
        {
            int agentCountry = 0;
            //for getting agent Country ID
            if (Settings.LoginInfo != null && Settings.LoginInfo.AgentId > 0)
            {
                AgentMaster agency = new AgentMaster(Settings.LoginInfo.AgentId);
                if (agency != null && agency.Country > 0 && Country.GetCountryCodeByCountryId(agency.Country) == "IN")
                {
                    agentCountry = agency.Country;
                }
            }
            DataTable dt = AgentRegistration.GetList(agentCountry);
            RegisterListDetails = dt;
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvRegisterList, dt);
        }
        catch
        {
            throw;
        }
    }

    //To get document details
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string getDocDetails(int reg_id)
    {
        try
        {
            //DataTable dt = AgentMaster.GetDocDetails(reg_id);
            DataTable dt = AgentRegistration.GetDocDetails(reg_id);
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dt);
            return JSONString;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }





    #endregion

}
