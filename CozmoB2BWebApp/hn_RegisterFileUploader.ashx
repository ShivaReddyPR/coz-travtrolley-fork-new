﻿<%@ WebHandler Language="C#" Class="hn_RegisterFileUploader" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Collections.Generic;
using CT.Core;

public class hn_RegisterFileUploader : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (context.Session["RegisterFiles"] != null)
            {
                files = context.Session["RegisterFiles"] as List<HttpPostedFile>;
            }
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                files.Add(file);
            }
            context.Session["RegisterFiles"] = files;
        }
        catch(Exception ex) {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }



}
