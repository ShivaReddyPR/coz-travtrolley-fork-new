﻿#region NameSpace Region
using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using CT.CMS;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.IO;
using CT.BookingEngine;
#endregion

public partial class CMSMaster : CT.Core.ParentPage
{
    #region Variables
    protected int _cmsId;  /*Based on this the respective footer links  and content details are loaded */
    protected string _cmsDocPath; /*Used to save the uploaded file image to the corresponding folder */
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            _cmsDocPath = System.Configuration.ConfigurationManager.AppSettings["CMSDocPath"];
            if (Session["CMSID"] != null && !IsPostBack)
            {
                _cmsId = Convert.ToInt32(Session["CMSID"]);
                BannersDiv.Visible = true;

                /************************ NOTE:PLEASE DO NOT REMOVE THE BELOW COMMENTS  ***************************************
                 * Tables List: CMS_HEADER,CMS_CONTENT_DETAILS,CMS_FOOTER_LINKS 
                 * 
                 * Product Id'S List:  FLIGHT =1, HOTEL =2,HOLIDAYS =3, VISA=12 ,SOCIALMEDIA & TAGSMANAGER = 0
                 * 
                 * DIFFERENT CONTENT TYPES:  
                 * 
                 * FOR HOME SETTINGS TAB:
                 * FLIGHT HOME TAB(1):HERO,PROMOMID,DEALSOFMONTH,DEALSOFMONTHSLIDER,VIDEO
                 * HOTEL HOME TAB (2):HERO,PROMOMID
                 * HOLIDAYS HOME TAB(3):HERO,VIDEO
                 * VISA HOME TAB(12):HERO,PROMOMID   
                 * 
                 * SOCIAL MEDIA TAB(0):SOCIALFB,SOCIALTW,SOCIALINST,SOCIALLIIN,SOCIALYOU,SOCIALPI
                 * 
                 * TAGS MANAGER TAB(0): GOOGLETAG,FACEBOOKTAG,TWITTERTAG,OTHERTAG 
                 * ****************************************************************************************************************/

                /***********Start:Bind Header Tab Details***********/
                BindHeaderDetails();
                /***********End: Bind Header Tab Details***********/

                /***********Start: Flight Product Banners***********/
                BindFlightHeroGrid();
                BindFlightPromoMidGrid();
                BindFlightDealsGrid();
                BindFlightDealsSliderGrid();
                BindFlightVideoGrid();
                /***********End: Flight Product Banners***********/

                /***********Start: Hotel Product Banners***********/
                BindHotelHeroGrid();
                BindHotelPromoMidGrid();
                /***********End: Hotel Product Banners***********/

                /***********Start: Holidays Product Banners***********/
                BindHolidaysHeroGrid();
                BindPackagesVideoGrid();
                /***********End:Hotel Product Banners**************/

                /***********Start: Visa Product Banners***********/
                BindVisaHeroGrid();
                BindVisaPromoMidGrid();
                /***********End: Visa Product Banners***********/

                /***********Start: Footer,Social Media,Tags Manager Grids***********/
                BindFooterLinksGrid();
                BindTagsLinksGrid();
                BindSocialMediaLinksGrid();
                /***********Start: Footer,Social Media,Tags Manager Grids***********/
               
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMasterPage Page_Load Event " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    #region Header Tab Details
    private void BindHeaderDetails()
    {
        try
        {
            if (Session["CMSID"] != null)
            {
                CMSHeader header = new CMSHeader(Convert.ToInt32(Session["CMSID"]));
                ddlCountry.SelectedValue = Convert.ToString(header.CountryCode);
                ddlCulture.SelectedValue = Convert.ToString(header.CultureCode);
                if (!string.IsNullOrEmpty(header.CopyRightText))
                {
                    txtCopyRight.Text = header.CopyRightText;
                }
                if (!string.IsNullOrEmpty(header.PhoneNo))
                {
                    txtContactNumber.Text = header.PhoneNo;
                }
                if (!string.IsNullOrEmpty(header.LogoDescription))
                {
                    txtLogoDesc.Text = header.LogoDescription;
                }

                if (header.ShowFooterMenu)
                {
                    chkFooterMenu.Checked = true;
                }
                if (header.ShowSocialIcons)
                {
                    chkSocialIcons.Checked = true;
                }
                if (header.ShowTopMenu)
                {
                    chkTopMenu.Checked = true;
                }
                if (!string.IsNullOrEmpty(header.LogoPath))
                {
                    ImageHeader.Visible = true;
                    ImageHeader.ImageUrl = header.LogoPath;
                    hdfHeaderImage.Value = "1";
                }
                ddlCountry.Enabled = false;
                ddlCulture.Enabled = false;
                btnSave.Visible = false;
                btnFinalSave.Visible = true;  /*INITIALLY WE WILL NOT DISPLAY THE CANCEL BUTTON TO THE USER*/
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Flight Product Banners
    private void BindFlightHeroGrid()
    {
        /*******************FLIGHT: HERO BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Flight);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvHeroF.DataSource = dtHeroBannerDetails;
            gvHeroF.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvHeroF.DataSource = dtHeroBannerDetails;
            gvHeroF.DataBind();
            gvHeroF.Rows[0].Visible = false;
        }
    }
    private void BindFlightPromoMidGrid()
    {
        /*******************FLIGHT: PROMOMID BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "PROMOMID", (int)ProductType.Flight);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvMidF.DataSource = dtHeroBannerDetails;
            gvMidF.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvMidF.DataSource = dtHeroBannerDetails;
            gvMidF.DataBind();
            gvMidF.Rows[0].Visible = false;
        }
    }
    private void BindFlightDealsGrid()
    {
        /*******************FLIGHT: DEALSOFMONTH ******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "DEALSOFMONTH", (int)ProductType.Flight);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvDealF.DataSource = dtHeroBannerDetails;
            gvDealF.DataBind();
            gvDealF.ShowFooter = false;
            gvDealF.FooterRow.Visible = false;
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvDealF.DataSource = dtHeroBannerDetails;
            gvDealF.DataBind();
            gvDealF.Rows[0].Visible = false;

        }
    }
    private void BindFlightDealsSliderGrid()
    {
        /*******************FLIGHT: DEALSOFMONTHSLIDER ******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "DEALSOFMONTHSLIDER", (int)ProductType.Flight);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvDealsF.DataSource = dtHeroBannerDetails;
            gvDealsF.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvDealsF.DataSource = dtHeroBannerDetails;
            gvDealsF.DataBind();
            gvDealsF.Rows[0].Visible = false;
        }
    }
    private void BindFlightVideoGrid()
    {
        /*******************FLIGHT: VIDEO ******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "VIDEO", (int)ProductType.Flight);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvFVideo.DataSource = dtHeroBannerDetails;
            gvFVideo.DataBind();
            gvFVideo.ShowFooter = false;
            gvFVideo.FooterRow.Visible = false;
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvFVideo.DataSource = dtHeroBannerDetails;
            gvFVideo.DataBind();
            gvFVideo.Rows[0].Visible = false;
        }
    }
    #endregion

    #region Hotel Product Banners
    private void BindHotelHeroGrid()
    {   /*******************HOTEL: HERO BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Hotel);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvHeroH.DataSource = dtHeroBannerDetails;
            gvHeroH.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvHeroH.DataSource = dtHeroBannerDetails;
            gvHeroH.DataBind();
            gvHeroH.Rows[0].Visible = false;
        }
    }
    private void BindHotelPromoMidGrid()
    {
        /*******************HOTEL: PROMOMID BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "PROMOMID", (int)ProductType.Hotel);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvMidH.DataSource = dtHeroBannerDetails;
            gvMidH.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvMidH.DataSource = dtHeroBannerDetails;
            gvMidH.DataBind();
            gvMidH.Rows[0].Visible = false;
        }
    }
    #endregion

    #region Packages Product Banners

    private void BindHolidaysHeroGrid()
    {
        /*******************PACKAGES: HERO BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Packages);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvHeroP.DataSource = dtHeroBannerDetails;
            gvHeroP.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvHeroP.DataSource = dtHeroBannerDetails;
            gvHeroP.DataBind();
            gvHeroP.Rows[0].Visible = false;
        }
    }

    private void BindPackagesVideoGrid()
    {
        /*******************PACKAGES: VIDEO******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "VIDEO", (int)ProductType.Packages);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvPVideo.DataSource = dtHeroBannerDetails;
            gvPVideo.DataBind();
            gvPVideo.ShowFooter = false;
            gvPVideo.FooterRow.Visible = false;
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvPVideo.DataSource = dtHeroBannerDetails;
            gvPVideo.DataBind();
            gvPVideo.Rows[0].Visible = false;
        }
    }
    #endregion

    #region Visa Product Banners
    private void BindVisaHeroGrid()
    {
        /*******************VISA: HERO BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Visa);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvHeroV.DataSource = dtHeroBannerDetails;
            gvHeroV.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvHeroV.DataSource = dtHeroBannerDetails;
            gvHeroV.DataBind();
            gvHeroV.Rows[0].Visible = false;
        }
    }
    private void BindVisaPromoMidGrid()
    {
        /*******************VISA: PROMOMID BANNER******************/
        DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "PROMOMID", (int)ProductType.Visa);
        if (dtHeroBannerDetails != null && dtHeroBannerDetails.Rows.Count > 0)
        {
            gvMidV.DataSource = dtHeroBannerDetails;
            gvMidV.DataBind();
        }
        else
        {
            dtHeroBannerDetails.Rows.Add(dtHeroBannerDetails.NewRow());
            gvMidV.DataSource = dtHeroBannerDetails;
            gvMidV.DataBind();
            gvMidV.Rows[0].Visible = false;
        }
    }
    #endregion

    #region Footer Links Grid
    private void BindFooterLinksGrid()
    {
        /*******************FOOTER LINKS BASED ON THE CMS ID******************/
        DataTable dtLinks = CMSFooterLinks.GetAllLinksList(Convert.ToInt32(Session["CMSID"]));
        if (dtLinks != null && dtLinks.Rows.Count > 0)
        {
            gridViewL.DataSource = dtLinks;
            gridViewL.DataBind();
        }
        else
        {
            dtLinks.Rows.Add(dtLinks.NewRow());
            gridViewL.DataSource = dtLinks;
            gridViewL.DataBind();
            gridViewL.Rows[0].Visible = false;
        }
    }
    #endregion

    #region Social Media and Tags Links Grid

    private void BindSocialMediaLinksGrid()
    {
        /*******************SOCIAL MEDIA******************/
        DataTable dtDetails = CMSContentDetails.GetSocialMediaList(Convert.ToInt32(Session["CMSID"]), 0);
        if (dtDetails != null && dtDetails.Rows.Count > 0)
        {
            gridViewSM.DataSource = dtDetails;
            gridViewSM.DataBind();
        }
        else
        {
            dtDetails.Rows.Add(dtDetails.NewRow());
            gridViewSM.DataSource = dtDetails;
            gridViewSM.DataBind();
            gridViewSM.Rows[0].Visible = false;
        }
    }
    private void BindTagsLinksGrid()
    {
        DataTable dtDetails = CMSContentDetails.GetTagsList(Convert.ToInt32(Session["CMSID"]), 0);
        if (dtDetails != null && dtDetails.Rows.Count > 0)
        {
            gridViewTM.DataSource = dtDetails;
            gridViewTM.DataBind();
        }
        else
        {
            dtDetails.Rows.Add(dtDetails.NewRow());
            gridViewTM.DataSource = dtDetails;
            gridViewTM.DataBind();
            gridViewTM.Rows[0].Visible = false;
        }
    }

    #endregion

    #region Flight Product Grid Banners Events

    #region Hero Banner
    protected void gvHeroF_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvHeroF.EditIndex = e.NewEditIndex;
            BindFlightHeroGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroF_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroF_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            int heroDetailId = Convert.ToInt32(gvHeroF.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fileUploadHero = (FileUpload)gvHeroF.Rows[e.RowIndex].FindControl("fuEditHeroF");
            bool imageValidation = true;
            DateTime effDate = (gvHeroF.Rows[e.RowIndex].FindControl("dcEditHeroImgEffeF") as DateControl).Value;
            DateTime expDate = (gvHeroF.Rows[e.RowIndex].FindControl("dcEditHeroImgExpF") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fileUploadHero.HasFile)
            {
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                if (size > 1024)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 1920)
                {
                    custValHeroF.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroF.Visible = true;
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                }
                else if (height < 600)
                {
                    custValHeroF.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValHeroF.Text = "Effective date cannot be blank!";
                custValHeroF.Visible = true;
                custValHeroF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValHeroF.Text = "Expiry date cannot be blank!";
                custValHeroF.Visible = true;
                custValHeroF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValHeroF.Text = "Effective date must be greater than todays date!";
            //    custValHeroF.Visible = true;
            //    custValHeroF.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValHeroF.Text = "Expiry date must be greater than effective date!";
                custValHeroF.Visible = true;
                custValHeroF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }



            if (imageValidation)
            {
                custValHeroF.Visible = false;
                custValHeroF.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme+"://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvHeroF.Rows[e.RowIndex].FindControl("txtEditHeroTitleF");
                TextBox txtHeroBannerURl = (TextBox)gvHeroF.Rows[e.RowIndex].FindControl("txtEditHeroBannerURlF");
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvHeroF.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILEPATHF");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvHeroF.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILENAMEF");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvHeroF.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILETYPEF");
                HiddenField hdfHeroOrderNo = (HiddenField)gvHeroF.Rows[e.RowIndex].FindControl("hdfHeroOrderNoF");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "HERO";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                // objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Flight;
                objContent.save();

                if (fileUploadHero.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fileUploadHero.FileName;
                    fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fileUploadHero.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvHeroF.EditIndex = -1;
                BindFlightHeroGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroF_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroF_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValHeroF.Visible = false;
            custValHeroF.Text = string.Empty;
            gvHeroF.EditIndex = -1;
            BindFlightHeroGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroF_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroF_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvHeroF.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvHeroF.Rows[e.RowIndex].FindControl("hdnHeroStatusF");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindFlightHeroGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroF_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroF_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fileUploadHero = (FileUpload)gvHeroF.FooterRow.FindControl("fuHeroF");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);

                

                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvHeroF.FooterRow.FindControl("dcHeroImgEffeFlight") as DateControl).Value;
                DateTime expDate = (gvHeroF.FooterRow.FindControl("dcHeroImgExpF") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Flight);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvHeroF.FooterRow.FindControl("ddlHeroBannerOrderF");
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                if (size > 1024)
                {

                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                else if (width < 1920)
                {
                    custValHeroF.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroF.Visible = true;
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                }
                else if (height < 600)
                {
                    custValHeroF.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                }
                else if (effDate == DateTime.MinValue)
                {
                    custValHeroF.Text = "Effective date cannot be blank!";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                }
                else if (expDate == DateTime.MinValue)
                {
                    custValHeroF.Text = "Expiry date cannot be blank!";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValHeroF.Text = "Effective date must be greater than todays date!";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValHeroF.Text = "Expiry date must be greater than effective date !";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValHeroF.Text = "Duplicate  Order!";
                    custValHeroF.ForeColor = System.Drawing.Color.Red;
                    custValHeroF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValHeroF.Visible = false;
                    custValHeroF.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme+"://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvHeroF.FooterRow.FindControl("txtHeroTitleF");
                    TextBox txtHeroBannerURl = (TextBox)gvHeroF.FooterRow.FindControl("txtHeroBannerURlF");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Flight;
                    objContent.ContentType = "HERO";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fileUploadHero.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fileUploadHero.FileName;
                        fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fileUploadHero.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindFlightHeroGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroF_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelHeroF");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusHeroF");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroF_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Middle Banner
    protected void gvMidF_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvMidF.EditIndex = e.NewEditIndex;
            BindFlightPromoMidGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidF_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidF_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            int heroDetailId = Convert.ToInt32(gvMidF.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fuEditMidF = (FileUpload)gvMidF.Rows[e.RowIndex].FindControl("fuEditMidF");
            bool imageValidation = true;
            DateTime effDate = (gvMidF.Rows[e.RowIndex].FindControl("dcEditMidImgEffeF") as DateControl).Value;
            DateTime expDate = (gvMidF.Rows[e.RowIndex].FindControl("dcEditMidImgExpF") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fuEditMidF.HasFile)
            {
                int size = fuEditMidF.PostedFile.ContentLength / 1024;
                if (size > 200)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuEditMidF.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 380)
                {
                    custValMidF.Text = "Image width should be greater than or equal to 380px.";
                    custValMidF.Visible = true;
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 260)
                {
                    custValMidF.Text = "Image height should be greater than or equal to 260px.";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                    imageValidation = false;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValMidF.Text = "Effective date cannot be blank!";
                custValMidF.Visible = true;
                custValMidF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
                //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValMidF.Text = "Expiry date cannot be blank!";
                custValMidF.Visible = true;
                custValMidF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
                //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValMidF.Text = "Effective date must be greater than todays date!";
            //    custValMidF.Visible = true;
            //    custValMidF.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValMidF.Text = "Expiry date must be greater than effective date!";
                custValMidF.Visible = true;
                custValMidF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
                //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }



            if (imageValidation)
            {
                custValMidF.Visible = false;
                custValMidF.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvMidF.Rows[e.RowIndex].FindControl("txtEditMidTitleF");
                TextBox txtHeroBannerURl = (TextBox)gvMidF.Rows[e.RowIndex].FindControl("txtEditMidBannerURlF");
                TextBox txtHeroContentDesc = (TextBox)gvMidF.Rows[e.RowIndex].FindControl("txtEditMidContentDescF");
                TextBox txtHeroButtonCaption = (TextBox)gvMidF.Rows[e.RowIndex].FindControl("txtEditMidButtonCaptionF");

                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvMidF.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILEPATHF");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvMidF.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILENAMEF");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvMidF.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILETYPEF");
                HiddenField hdfHeroOrderNo = (HiddenField)gvMidF.Rows[e.RowIndex].FindControl("hdfMidOrderNoF");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "PROMOMID";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                objContent.Flex1 = txtHeroButtonCaption.Text;
                //objContent.Status = "A";
                objContent.ContentDescription = txtHeroContentDesc.Text;
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Flight;
                objContent.save();

                if (fuEditMidF.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fuEditMidF.FileName;
                    fileExtension = System.IO.Path.GetExtension(fuEditMidF.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fuEditMidF.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvMidF.EditIndex = -1;
                BindFlightPromoMidGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidF_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidF_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValMidF.Visible = false;
            custValMidF.Text = string.Empty;
            gvMidF.EditIndex = -1;
            BindFlightPromoMidGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidF_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidF_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvMidF.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvMidF.Rows[e.RowIndex].FindControl("hdnMidStatusF");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindFlightPromoMidGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidF_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidF_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            hdntabId.Value = "2-F";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fuMidF = (FileUpload)gvMidF.FooterRow.FindControl("fuMidF");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuMidF.PostedFile.InputStream);
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvMidF.FooterRow.FindControl("dcMidImgEffeFlight") as DateControl).Value;
                DateTime expDate = (gvMidF.FooterRow.FindControl("dcMidImgExpF") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "PROMOMID", (int)ProductType.Flight);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvMidF.FooterRow.FindControl("ddlMidBannerOrderF");
                int size = fuMidF.PostedFile.ContentLength / 1024;
                if (size > 200)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                else if (width < 380)
                {
                    custValMidF.Text = "Image width should be greater than or equal to 380px.";
                    custValMidF.Visible = true;
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 260)
                {
                    custValMidF.Text = "Image height should be greater than or equal to 260px.";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValMidF.Text = "Effective date cannot be blank!";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValMidF.Text = "Expiry date cannot be blank!";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValMidF.Text = "Effective date must be greater than todays date!";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValMidF.Text = "Expiry date must be greater than effective date !";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValMidF.Text = "Duplicate  Order!";
                    custValMidF.ForeColor = System.Drawing.Color.Red;
                    custValMidF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValMidF.Visible = false;
                    custValMidF.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvMidF.FooterRow.FindControl("txtMidTitleF");
                    TextBox txtHeroBannerURl = (TextBox)gvMidF.FooterRow.FindControl("txtMidBannerURlF");
                    TextBox txtHeroContentDesc = (TextBox)gvMidF.FooterRow.FindControl("txtMidContentDescF");
                    TextBox txtHeroButtonCaption = (TextBox)gvMidF.FooterRow.FindControl("txtMidButtonCaptionF");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Flight;
                    objContent.ContentType = "PROMOMID";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.ContentDescription = txtHeroContentDesc.Text;
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    objContent.Flex1 = txtHeroButtonCaption.Text;
                    int detailId = objContent.save();

                    if (fuMidF.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fuMidF.FileName;
                        fileExtension = System.IO.Path.GetExtension(fuMidF.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fuMidF.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindFlightPromoMidGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidF_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelMidF");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusMidF");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidF_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Deals Banner

    protected void gvDealF_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvDealF.EditIndex = e.NewEditIndex;
            BindFlightDealsGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealF_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealF_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            int heroDetailId = Convert.ToInt32(gvDealF.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fileUploadHero = (FileUpload)gvDealF.Rows[e.RowIndex].FindControl("fuEditDealF");
            bool imageValidation = true;
            DateTime effDate = (gvDealF.Rows[e.RowIndex].FindControl("dcEditDealImgEffeF") as DateControl).Value;
            DateTime expDate = (gvDealF.Rows[e.RowIndex].FindControl("dcEditDealImgExpF") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fileUploadHero.HasFile)
            {
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                if (size > 1024)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 1550)
                {
                    custValDealF.Text = "Image width should be greater than or equal to 1550px.";
                    custValDealF.Visible = true;
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 550)
                {
                    custValDealF.Text = "Image height should be greater than or equal to 550px.";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                    imageValidation = false;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValDealF.Text = "Effective date cannot be blank!";
                custValDealF.Visible = true;
                custValDealF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValDealF.Text = "Expiry date cannot be blank!";
                custValDealF.Visible = true;
                custValDealF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValDealF.Text = "Effective date must be greater than todays date!";
            //    custValDealF.Visible = true;
            //    custValDealF.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValDealF.Text = "Expiry date must be greater than effective date!";
                custValDealF.Visible = true;
                custValDealF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }


            if (imageValidation)
            {
                custValDealF.Visible = false;
                custValDealF.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme+"://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvDealF.Rows[e.RowIndex].FindControl("txtEditDealTitleF");
                //  TextBox txtHeroBannerURl = (TextBox)gvDealF.Rows[e.RowIndex].FindControl("txtEditDealBannerURlF");
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvDealF.Rows[e.RowIndex].FindControl("hdfDealCONTENTFILEPATHF");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvDealF.Rows[e.RowIndex].FindControl("hdfDealCONTENTFILENAMEF");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvDealF.Rows[e.RowIndex].FindControl("hdfDealCONTENTFILETYPEF");
                HiddenField hdfHeroOrderNo = (HiddenField)gvDealF.Rows[e.RowIndex].FindControl("hdfDealOrderNoF");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "DEALSOFMONTH";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                //  objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                // objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Flight;
                objContent.save();

                if (fileUploadHero.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fileUploadHero.FileName;
                    fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fileUploadHero.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvDealF.EditIndex = -1;
                BindFlightDealsGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealF_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealF_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValDealF.Visible = false;
            custValDealF.Text = string.Empty;
            gvDealF.EditIndex = -1;
            BindFlightDealsGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealF_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealF_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvDealF.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvDealF.Rows[e.RowIndex].FindControl("hdnDealStatusF");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindFlightDealsGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealF_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealF_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fileUploadHero = (FileUpload)gvDealF.FooterRow.FindControl("fuDealF");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvDealF.FooterRow.FindControl("dcDealImgEffeFlight") as DateControl).Value;
                DateTime expDate = (gvDealF.FooterRow.FindControl("dcDealImgExpF") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "DEALSOFMONTH", (int)ProductType.Flight);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvDealF.FooterRow.FindControl("ddlDealBannerOrderF");
                if (size > 1024)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                else if (width < 1550)
                {
                    custValDealF.Text = "Image width should be greater than or equal to 1550px.";
                    custValDealF.Visible = true;
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 550)
                {
                    custValDealF.Text = "Image height should be greater than or equal to 550px.";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValDealF.Text = "Effective date cannot be blank!";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                   /// hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValDealF.Text = "Expiry date cannot be blank!";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValDealF.Text = "Effective date must be greater than todays date!";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValDealF.Text = "Expiry date must be greater than effective date !";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValDealF.Text = "Duplicate  Order!";
                    custValDealF.ForeColor = System.Drawing.Color.Red;
                    custValDealF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValDealF.Visible = false;
                    custValDealF.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvDealF.FooterRow.FindControl("txtDealTitleF");
                    //  TextBox txtHeroBannerURl = (TextBox)gvDealF.FooterRow.FindControl("txtDealBannerURlF");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Flight;
                    objContent.ContentType = "DEALSOFMONTH";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    // objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fileUploadHero.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fileUploadHero.FileName;
                        fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fileUploadHero.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindFlightDealsGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealF_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelDealF");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusDealF");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealF_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Deals Slider Banner

    protected void gvDealsF_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvDealsF.EditIndex = e.NewEditIndex;
            BindFlightDealsSliderGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealsF_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealsF_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            int heroDetailId = Convert.ToInt32(gvDealsF.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fuEditDealsF = (FileUpload)gvDealsF.Rows[e.RowIndex].FindControl("fuEditDealsF");
            bool imageValidation = true;
            DateTime effDate = (gvDealsF.Rows[e.RowIndex].FindControl("dcEditDealsImgEffeF") as DateControl).Value;
            DateTime expDate = (gvDealsF.Rows[e.RowIndex].FindControl("dcEditDealsImgExpF") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fuEditDealsF.HasFile)
            {
                int size = fuEditDealsF.PostedFile.ContentLength / 1024;
                if (size > 200)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuEditDealsF.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 150)
                {
                    custValDealsF.Text = "Image width should be greater than or equal to 150px.";
                    custValDealsF.Visible = true;
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 150)
                {
                    custValDealsF.Text = "Image height should be greater than or equal to 150px.";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValDealsF.Text = "Effective date cannot be blank!";
                custValDealsF.Visible = true;
                custValDealsF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValDealsF.Text = "Expiry date cannot be blank!";
                custValDealsF.Visible = true;
                custValDealsF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValDealsF.Text = "Effective date must be greater than todays date!";
            //    custValDealsF.Visible = true;
            //    custValDealsF.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValDealsF.Text = "Expiry date must be greater than effective date!";
                custValDealsF.Visible = true;
                custValDealsF.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }



            if (imageValidation)
            {
                custValDealsF.Visible = false;
                custValDealsF.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvDealsF.Rows[e.RowIndex].FindControl("txtEditDealsTitleF");
                //  TextBox txtHeroBannerURl = (TextBox)gvDealsF.Rows[e.RowIndex].FindControl("txtEditDealsBannerURlF");
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvDealsF.Rows[e.RowIndex].FindControl("hdfDealsCONTENTFILEPATHF");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvDealsF.Rows[e.RowIndex].FindControl("hdfDealsCONTENTFILENAMEF");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvDealsF.Rows[e.RowIndex].FindControl("hdfDealsCONTENTFILETYPEF");
                HiddenField hdfHeroOrderNo = (HiddenField)gvDealsF.Rows[e.RowIndex].FindControl("hdfDealsOrderNoF");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "DEALSOFMONTHSLIDER";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                //objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Flight;
                objContent.save();

                if (fuEditDealsF.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fuEditDealsF.FileName;
                    fileExtension = System.IO.Path.GetExtension(fuEditDealsF.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fuEditDealsF.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvDealsF.EditIndex = -1;
                BindFlightDealsSliderGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealsF_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealsF_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValDealsF.Visible = false;
            custValDealsF.Text = string.Empty;
            gvDealsF.EditIndex = -1;
            BindFlightDealsSliderGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealsF_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealsF_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvDealsF.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvDealsF.Rows[e.RowIndex].FindControl("hdnDealsStatusF");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindFlightDealsSliderGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealsF_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealsF_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fuDealsF = (FileUpload)gvDealsF.FooterRow.FindControl("fuDealsF");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuDealsF.PostedFile.InputStream);
                int size = fuDealsF.PostedFile.ContentLength / 1024;
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvDealsF.FooterRow.FindControl("dcDealsImgEffeFlight") as DateControl).Value;
                DateTime expDate = (gvDealsF.FooterRow.FindControl("dcDealsImgExpF") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "DEALSOFMONTHSLIDER", (int)ProductType.Flight);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvDealsF.FooterRow.FindControl("ddlDealsBannerOrderF");
                if (size > 200)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                else if (width < 150)
                {
                    custValDealsF.Text = "Image width should be greater than or equal to 150px.";
                    custValDealsF.Visible = true;
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 150)
                {
                    custValDealsF.Text = "Image height should be greater than or equal to 150px.";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValDealsF.Text = "Effective date cannot be blank!";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValDealsF.Text = "Expiry date cannot be blank!";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValDealsF.Text = "Effective date must be greater than todays date!";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValDealsF.Text = "Expiry date must be greater than effective date !";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValDealsF.Text = "Duplicate  Order!";
                    custValDealsF.ForeColor = System.Drawing.Color.Red;
                    custValDealsF.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValDealsF.Visible = false;
                    custValDealsF.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvDealsF.FooterRow.FindControl("txtDealsTitleF");
                    // TextBox txtHeroBannerURl = (TextBox)gvDealsF.FooterRow.FindControl("txtDealsBannerURlF");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Flight;
                    objContent.ContentType = "DEALSOFMONTHSLIDER";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    //  objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fuDealsF.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fuDealsF.FileName;
                        fileExtension = System.IO.Path.GetExtension(fuDealsF.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fuDealsF.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindFlightDealsSliderGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealsF_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvDealsF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelDealsF");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusDealsF");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvDealsF_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Video Banner Grid
    protected void gvFVideo_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvFVideo.EditIndex = e.NewEditIndex;
            BindFlightVideoGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvFVideo_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvFVideo_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-F";
            int VideoDetailId = Convert.ToInt32(gvFVideo.DataKeys[e.RowIndex].Values["DETAILID"]);
            DateTime effDate = (gvFVideo.Rows[e.RowIndex].FindControl("dcEditVideoImgEffeF") as DateControl).Value;
            DateTime expDate = (gvFVideo.Rows[e.RowIndex].FindControl("dcEditVideoImgExpF") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (effDate == DateTime.MinValue)
            {
                custValVideoF.Text = "Effective date cannot be blank!";
                custValVideoF.Visible = true;
                custValVideoF.ForeColor = System.Drawing.Color.Red;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

            }
            else if (expDate == DateTime.MinValue)
            {
                custValVideoF.Text = "Expiry date cannot be blank!";
                custValVideoF.Visible = true;
                custValVideoF.ForeColor = System.Drawing.Color.Red;
                //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValVideoF.Text = "Effective date must be greater than todays date!";
            //    custValVideoF.Visible = true;
            //    custValVideoF.ForeColor = System.Drawing.Color.Red;

            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValVideoF.Text = "Expiry date must be greater than effective date!";
                custValVideoF.Visible = true;
                custValVideoF.ForeColor = System.Drawing.Color.Red;
                //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

            }
            else
            {
                custValVideoF.Visible = false;
                custValVideoF.Text = string.Empty;
                TextBox txtVideoBannerURl = (TextBox)gvFVideo.Rows[e.RowIndex].FindControl("txtEditVideoBannerURlF");
                HiddenField hdfVideoOrderNo = (HiddenField)gvFVideo.Rows[e.RowIndex].FindControl("hdfVideoOrderNoF");
                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = VideoDetailId;
                objContent.ContentType = "VIDEO";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtVideoBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfVideoOrderNo.Value);
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ProductType = (int)ProductType.Flight;
                objContent.save();
                gvFVideo.EditIndex = -1;
                BindFlightVideoGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvFVideo_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvFVideo_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValVideoF.Visible = false;
            custValVideoF.Text = string.Empty;
            gvFVideo.EditIndex = -1;
            BindFlightVideoGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvFVideo_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvFVideo_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int VideoDetailId = Convert.ToInt32(gvFVideo.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvFVideo.Rows[e.RowIndex].FindControl("hdnVideoStatusF");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(VideoDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindFlightVideoGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvFVideo_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvFVideo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("AddNew"))
            {
                //FileUpload fileUploadVideo = (FileUpload)gvFVideo.FooterRow.FindControl("fuVideoF");
                //System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadVideo.PostedFile.InputStream);
                //int height = img.Height;
                //int width = img.Width;
                DateTime effDate = (gvFVideo.FooterRow.FindControl("dcVideoImgEffeFlight") as DateControl).Value;
                DateTime expDate = (gvFVideo.FooterRow.FindControl("dcVideoImgExpF") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtVideoBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "VIDEO", (int)ProductType.Flight);
                DropDownList ddlVideoBannerOrder = (DropDownList)gvFVideo.FooterRow.FindControl("ddlVideoBannerOrderF");

                if (effDate == DateTime.MinValue)
                {
                    custValVideoF.Text = "Effective date cannot be blank!";
                    custValVideoF.ForeColor = System.Drawing.Color.Red;
                    custValVideoF.Visible = true;
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValVideoF.Text = "Expiry date cannot be blank!";
                    custValVideoF.ForeColor = System.Drawing.Color.Red;
                    custValVideoF.Visible = true;
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValVideoF.Text = "Effective date must be greater than todays date!";
                    custValVideoF.ForeColor = System.Drawing.Color.Red;
                    custValVideoF.Visible = true;
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValVideoF.Text = "Expiry date must be greater than effective date !";
                    custValVideoF.ForeColor = System.Drawing.Color.Red;
                    custValVideoF.Visible = true;
                }
                else if (dtVideoBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlVideoBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValVideoF.Text = "Duplicate  Order!";
                    custValVideoF.ForeColor = System.Drawing.Color.Red;
                    custValVideoF.Visible = true;
                }
                else
                {
                    custValVideoF.Visible = false;
                    custValVideoF.Text = string.Empty;

                    TextBox txtVideoBannerURl = (TextBox)gvFVideo.FooterRow.FindControl("txtVideoBannerURlF");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ProductType = (int)ProductType.Flight;
                    objContent.ContentType = "VIDEO";
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtVideoBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlVideoBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    // objContent.ContentFileName = string.Empty;
                    //  objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();
                    BindFlightVideoGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvFVideo_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvFVideo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelVideoF");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusVideoF");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvFVideo_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #endregion

    #region Hotel Product Grid Banners Events

    #region Hero Banner
    protected void gvHeroH_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvHeroH.EditIndex = e.NewEditIndex;
            BindHotelHeroGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroH_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroH_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-H";
            int heroDetailId = Convert.ToInt32(gvHeroH.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fileUploadHero = (FileUpload)gvHeroH.Rows[e.RowIndex].FindControl("fuEditHeroH");
            bool imageValidation = true;
            DateTime effDate = (gvHeroH.Rows[e.RowIndex].FindControl("dcEditHeroImgEffeH") as DateControl).Value;
            DateTime expDate = (gvHeroH.Rows[e.RowIndex].FindControl("dcEditHeroImgExpH") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fileUploadHero.HasFile)
            {
                //FileUpload fuDealsF = (FileUpload)gvHeroH.Rows[e.RowIndex].FindControl("fuEditHeroH");//ziya edited
                int size = fuDealsF.PostedFile.ContentLength / 1024;
                if (size > 1024)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 1920)
                {
                    custValHeroH.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroH.Visible = true;
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 600)
                {
                    custValHeroH.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValHeroH.Text = "Effective date cannot be blank!";
                custValHeroH.Visible = true;
                custValHeroH.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValHeroH.Text = "Expiry date cannot be blank!";
                custValHeroH.Visible = true;
                custValHeroH.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValHeroH.Text = "Effective date must be greater than todays date!";
            //    custValHeroH.Visible = true;
            //    custValHeroH.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValHeroH.Text = "Expiry date must be greater than effective date!";
                custValHeroH.Visible = true;
                custValHeroH.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }


            if (imageValidation)
            {
                custValHeroH.Visible = false;
                custValHeroH.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvHeroH.Rows[e.RowIndex].FindControl("txtEditHeroTitleH");
                TextBox txtHeroBannerURl = (TextBox)gvHeroH.Rows[e.RowIndex].FindControl("txtEditHeroBannerURlH");
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvHeroH.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILEPATHH");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvHeroH.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILENAMEH");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvHeroH.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILETYPEH");
                HiddenField hdfHeroOrderNo = (HiddenField)gvHeroH.Rows[e.RowIndex].FindControl("hdfHeroOrderNoH");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "HERO";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                // objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Hotel;
                objContent.save();

                if (fileUploadHero.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fileUploadHero.FileName;
                    fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fileUploadHero.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvHeroH.EditIndex = -1;
                BindHotelHeroGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroH_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroH_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValHeroH.Visible = false;
            custValHeroH.Text = string.Empty;
            gvHeroH.EditIndex = -1;
            BindHotelHeroGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroH_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroH_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvHeroH.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvHeroH.Rows[e.RowIndex].FindControl("hdnHeroStatusH");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindHotelHeroGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroH_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroH_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-H";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fileUploadHero = (FileUpload)gvHeroH.FooterRow.FindControl("fuHeroH");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvHeroH.FooterRow.FindControl("dcHeroImgEffeHotel") as DateControl).Value;
                DateTime expDate = (gvHeroH.FooterRow.FindControl("dcHeroImgExpH") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Hotel);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvHeroH.FooterRow.FindControl("ddlHeroBannerOrderH");
                if (size > 1024)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                else if (width < 1920)
                {
                    custValHeroH.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroH.Visible = true;
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 600)
                {
                    custValHeroH.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValHeroH.Text = "Effective date cannot be blank!";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValHeroH.Text = "Expiry date cannot be blank!";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValHeroH.Text = "Effective date must be greater than todays date!";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValHeroH.Text = "Expiry date must be greater than effective date !";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValHeroH.Text = "Duplicate  Order!";
                    custValHeroH.ForeColor = System.Drawing.Color.Red;
                    custValHeroH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValHeroH.Visible = false;
                    custValHeroH.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvHeroH.FooterRow.FindControl("txtHeroTitleH");
                    TextBox txtHeroBannerURl = (TextBox)gvHeroH.FooterRow.FindControl("txtHeroBannerURlH");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Hotel;
                    objContent.ContentType = "HERO";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fileUploadHero.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fileUploadHero.FileName;
                        fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fileUploadHero.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindHotelHeroGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroH_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroH_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelHeroH");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusHeroH");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroH_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #region Middle Banner
    protected void gvMidH_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvMidH.EditIndex = e.NewEditIndex;
            BindHotelPromoMidGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidH_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidH_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-H";
            int heroDetailId = Convert.ToInt32(gvMidH.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fuEditMidH = (FileUpload)gvMidH.Rows[e.RowIndex].FindControl("fuEditMidH");
            bool imageValidation = true;
            DateTime effDate = (gvMidH.Rows[e.RowIndex].FindControl("dcEditMidImgEffeH") as DateControl).Value;
            DateTime expDate = (gvMidH.Rows[e.RowIndex].FindControl("dcEditMidImgExpH") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fuEditMidH.HasFile)
            {
                int size = fuEditMidH.PostedFile.ContentLength / 1024;
                if (size > 200)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuEditMidH.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 380)
                {
                    custValMidH.Text = "Image width should be greater than or equal to 380px.";
                    custValMidH.Visible = true;
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                }
                else if (height < 260)
                {
                    custValMidH.Text = "Image height should be greater than or equal to 260px.";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                    imageValidation = false;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValMidH.Text = "Effective date cannot be blank!";
                custValMidH.Visible = true;
                custValMidH.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

            }
            else if (expDate == DateTime.MinValue)
            {
                custValMidH.Text = "Expiry date cannot be blank!";
                custValMidH.Visible = true;
                custValMidH.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValMidH.Text = "Effective date must be greater than todays date!";
            //    custValMidH.Visible = true;
            //    custValMidH.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValMidH.Text = "Expiry date must be greater than effective date!";
                custValMidH.Visible = true;
                custValMidH.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

            }



            if (imageValidation)
            {
                custValMidH.Visible = false;
                custValMidH.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvMidH.Rows[e.RowIndex].FindControl("txtEditMidTitleH");
                TextBox txtHeroBannerURl = (TextBox)gvMidH.Rows[e.RowIndex].FindControl("txtEditMidBannerURlH");
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvMidH.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILEPATHH");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvMidH.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILENAMEH");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvMidH.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILETYPEH");
                HiddenField hdfHeroOrderNo = (HiddenField)gvMidH.Rows[e.RowIndex].FindControl("hdfMidOrderNoH");

                TextBox txtEditMidCONTENTNAMEH = (TextBox)gvMidH.Rows[e.RowIndex].FindControl("txtEditMidCONTENTNAMEH");
                TextBox txtEditMidCONTENTDESCH = (TextBox)gvMidH.Rows[e.RowIndex].FindControl("txtEditMidCONTENTDESCH");
                TextBox txtEditMidCONTENTVALUEH = (TextBox)gvMidH.Rows[e.RowIndex].FindControl("txtEditMidCONTENTVALUEH");


                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentName = txtEditMidCONTENTNAMEH.Text;
                objContent.ContentDescription = txtEditMidCONTENTDESCH.Text;
                objContent.ContentValue = txtEditMidCONTENTVALUEH.Text;
                objContent.ContentType = "PROMOMID";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                // objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Hotel;
                objContent.save();

                if (fuEditMidH.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fuEditMidH.FileName;
                    fileExtension = System.IO.Path.GetExtension(fuEditMidH.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fuEditMidH.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvMidH.EditIndex = -1;
                BindHotelPromoMidGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidH_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidH_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValMidH.Visible = false;
            custValMidH.Text = string.Empty;
            gvMidH.EditIndex = -1;
            BindHotelPromoMidGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidH_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidH_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvMidH.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvMidH.Rows[e.RowIndex].FindControl("hdnMidStatusH");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindHotelPromoMidGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidH_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidH_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-H";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fuMidH = (FileUpload)gvMidH.FooterRow.FindControl("fuMidH");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuMidH.PostedFile.InputStream);
                int size = fuMidH.PostedFile.ContentLength / 1024;
               
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvMidH.FooterRow.FindControl("dcMidImgEffeHotel") as DateControl).Value;
                DateTime expDate = (gvMidH.FooterRow.FindControl("dcMidImgExpH") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "PROMOMID", (int)ProductType.Hotel);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvMidH.FooterRow.FindControl("ddlMidBannerOrderH");
                if (size > 200)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                else if (width < 380)
                {
                    custValMidH.Text = "Image width should be greater than or equal to 380px.";
                    custValMidH.Visible = true;
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 260)
                {
                    custValMidH.Text = "Image height should be greater than or equal to 260px.";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValMidH.Text = "Effective date cannot be blank!";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValMidH.Text = "Expiry date cannot be blank!";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValMidH.Text = "Effective date must be greater than todays date!";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValMidH.Text = "Expiry date must be greater than effective date !";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValMidH.Text = "Duplicate  Order!";
                    custValMidH.ForeColor = System.Drawing.Color.Red;
                    custValMidH.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValMidH.Visible = false;
                    custValMidH.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvMidH.FooterRow.FindControl("txtMidTitleH");
                    TextBox txtHeroBannerURl = (TextBox)gvMidH.FooterRow.FindControl("txtMidBannerURlH");

                    TextBox txtEditMidCONTENTNAMEH = (TextBox)gvMidH.FooterRow.FindControl("txtMidCONTENTNAMEH");
                    TextBox txtEditMidCONTENTDESCH = (TextBox)gvMidH.FooterRow.FindControl("txtMidCONTENTDESCH");
                    TextBox txtEditMidCONTENTVALUEH = (TextBox)gvMidH.FooterRow.FindControl("txtMidCONTENTVALUEH");

                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Hotel;
                    objContent.ContentType = "PROMOMID";
                    objContent.ContentName = txtEditMidCONTENTNAMEH.Text;
                    objContent.ContentDescription = txtEditMidCONTENTDESCH.Text;
                    objContent.ContentValue = txtEditMidCONTENTVALUEH.Text;
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fuMidH.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fuMidH.FileName;
                        fileExtension = System.IO.Path.GetExtension(fuMidH.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fuMidH.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindHotelPromoMidGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidH_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidH_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelMidH");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusMidH");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidH_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #endregion

    #region Holdays Product Grid Banners Events

    #region Hero Banner
    protected void gvHeroP_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvHeroP.EditIndex = e.NewEditIndex;
            BindHolidaysHeroGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroP_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroP_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-P";
            int heroDetailId = Convert.ToInt32(gvHeroP.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fileUploadHero = (FileUpload)gvHeroP.Rows[e.RowIndex].FindControl("fuEditHeroP");
            bool imageValidation = true;
            DateTime effDate = (gvHeroP.Rows[e.RowIndex].FindControl("dcEditHeroImgEffeP") as DateControl).Value;
            DateTime expDate = (gvHeroP.Rows[e.RowIndex].FindControl("dcEditHeroImgExpP") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fileUploadHero.HasFile)
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                if (size > 1024)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                int height = img.Height;
                int width = img.Width;
                if (width < 1920)
                {
                    custValHeroP.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroP.Visible = true;
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 600)
                {
                    custValHeroP.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValHeroP.Text = "Effective date cannot be blank!";
                custValHeroP.Visible = true;
                custValHeroP.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValHeroP.Text = "Expiry date cannot be blank!";
                custValHeroP.Visible = true;
                custValHeroP.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValHeroP.Text = "Effective date must be greater than todays date!";
            //    custValHeroP.Visible = true;
            //    custValHeroP.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValHeroP.Text = "Expiry date must be greater than effective date!";
                custValHeroP.Visible = true;
                custValHeroP.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }


            if (imageValidation)
            {
                custValHeroP.Visible = false;
                custValHeroP.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvHeroP.Rows[e.RowIndex].FindControl("txtEditHeroTitleP");
                TextBox txtHeroBannerURl = (TextBox)gvHeroP.Rows[e.RowIndex].FindControl("txtEditHeroBannerURlP");
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvHeroP.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILEPATHP");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvHeroP.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILENAMEP");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvHeroP.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILETYPEP");
                HiddenField hdfHeroOrderNo = (HiddenField)gvHeroP.Rows[e.RowIndex].FindControl("hdfHeroOrderNoP");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "HERO";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                // objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Packages;
                objContent.save();

                if (fileUploadHero.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fileUploadHero.FileName;
                    fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fileUploadHero.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvHeroP.EditIndex = -1;
                BindHolidaysHeroGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroP_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroP_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValHeroP.Visible = false;
            custValHeroP.Text = string.Empty;
            gvHeroP.EditIndex = -1;
            BindHolidaysHeroGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroP_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroP_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvHeroP.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvHeroP.Rows[e.RowIndex].FindControl("hdnHeroStatusP");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindHolidaysHeroGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroP_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroP_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-P";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fileUploadHero = (FileUpload)gvHeroP.FooterRow.FindControl("fuHeroP");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvHeroP.FooterRow.FindControl("dcHeroImgEffePkg") as DateControl).Value;
                DateTime expDate = (gvHeroP.FooterRow.FindControl("dcHeroImgExpP") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Packages);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvHeroP.FooterRow.FindControl("ddlHeroBannerOrderP");
                if (size > 1024)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                else if (width < 1920)
                {
                    custValHeroP.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroP.Visible = true;
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 600)
                {
                    custValHeroP.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValHeroP.Text = "Effective date cannot be blank!";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValHeroP.Text = "Expiry date cannot be blank!";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }

                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValHeroP.Text = "Effective date must be greater than todays date!";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValHeroP.Text = "Expiry date must be greater than effective date !";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValHeroP.Text = "Duplicate  Order!";
                    custValHeroP.ForeColor = System.Drawing.Color.Red;
                    custValHeroP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValHeroP.Visible = false;
                    custValHeroP.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvHeroP.FooterRow.FindControl("txtHeroTitleP");
                    TextBox txtHeroBannerURl = (TextBox)gvHeroP.FooterRow.FindControl("txtHeroBannerURlP");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Packages;
                    objContent.ContentType = "HERO";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fileUploadHero.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fileUploadHero.FileName;
                        fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fileUploadHero.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindHolidaysHeroGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroP_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroP_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelHeroP");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusHeroP");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroP_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #region Video Banner Grid
    protected void gvPVideo_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvPVideo.EditIndex = e.NewEditIndex;
            BindPackagesVideoGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvPVideo_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvPVideo_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {

            hdntabId.Value = "2-P";
            int VideoDetailId = Convert.ToInt32(gvPVideo.DataKeys[e.RowIndex].Values["DETAILID"]);
            DateTime effDate = (gvPVideo.Rows[e.RowIndex].FindControl("dcEditVideoImgEffeP") as DateControl).Value;
            DateTime expDate = (gvPVideo.Rows[e.RowIndex].FindControl("dcEditVideoImgExpP") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (effDate == DateTime.MinValue)
            {
                custValVideoP.Text = "Effective date cannot be blank!";
                custValVideoP.Visible = true;
                custValVideoP.ForeColor = System.Drawing.Color.Red;
                //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");


            }
            else if (expDate == DateTime.MinValue)
            {
                custValVideoP.Text = "Expiry date cannot be blank!";
                custValVideoP.Visible = true;
                custValVideoP.ForeColor = System.Drawing.Color.Red;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");


            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValVideoP.Text = "Effective date must be greater than todays date!";
            //    custValVideoP.Visible = true;
            //    custValVideoP.ForeColor = System.Drawing.Color.Red;

            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValVideoP.Text = "Expiry date must be greater than effective date!";
                custValVideoP.Visible = true;
                custValVideoP.ForeColor = System.Drawing.Color.Red;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");


            }
            else
            {
                custValVideoP.Visible = false;
                custValVideoP.Text = string.Empty;
                TextBox txtVideoBannerURl = (TextBox)gvPVideo.Rows[e.RowIndex].FindControl("txtEditVideoBannerURlP");
                HiddenField hdfVideoOrderNo = (HiddenField)gvPVideo.Rows[e.RowIndex].FindControl("hdfVideoOrderNoP");
                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = VideoDetailId;
                objContent.ContentType = "VIDEO";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtVideoBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfVideoOrderNo.Value);
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ProductType = (int)ProductType.Packages;
                objContent.save();
                gvPVideo.EditIndex = -1;
                BindPackagesVideoGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvPVideo_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvPVideo_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValVideoP.Visible = false;
            custValVideoP.Text = string.Empty;
            gvPVideo.EditIndex = -1;
            BindPackagesVideoGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvPVideo_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvPVideo_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int VideoDetailId = Convert.ToInt32(gvPVideo.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvPVideo.Rows[e.RowIndex].FindControl("hdnVideoStatusP");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(VideoDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindPackagesVideoGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvPVideo_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvPVideo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-P";
            if (e.CommandName.Equals("AddNew"))
            {
                DateTime effDate = (gvPVideo.FooterRow.FindControl("dcVideoImgEffePkg") as DateControl).Value;
                DateTime expDate = (gvPVideo.FooterRow.FindControl("dcVideoImgExpP") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtVideoBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "VIDEO", (int)ProductType.Packages);
                DropDownList ddlVideoBannerOrder = (DropDownList)gvPVideo.FooterRow.FindControl("ddlVideoBannerOrderP");

                if (effDate == DateTime.MinValue)
                {
                    custValVideoP.Text = "Effective date cannot be blank!";
                    custValVideoP.ForeColor = System.Drawing.Color.Red;
                    custValVideoP.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValVideoP.Text = "Expiry date cannot be blank!";
                    custValVideoP.ForeColor = System.Drawing.Color.Red;
                    custValVideoP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValVideoP.Text = "Effective date must be greater than todays date!";
                    custValVideoP.ForeColor = System.Drawing.Color.Red;
                    custValVideoP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValVideoP.Text = "Expiry date must be greater than effective date !";
                    custValVideoP.ForeColor = System.Drawing.Color.Red;
                    custValVideoP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtVideoBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlVideoBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValVideoP.Text = "Duplicate  Order!";
                    custValVideoP.ForeColor = System.Drawing.Color.Red;
                    custValVideoP.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValVideoP.Visible = false;
                    custValVideoP.Text = string.Empty;

                    TextBox txtVideoBannerURl = (TextBox)gvPVideo.FooterRow.FindControl("txtVideoBannerURlP");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ProductType = (int)ProductType.Packages;
                    objContent.ContentType = "VIDEO";
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtVideoBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlVideoBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    int detailId = objContent.save();
                    BindPackagesVideoGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvPVideo_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvPVideo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelVideoP");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusVideoP");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvPVideo_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion


    #endregion

    #region Visa Product Grid Banners Events

    #region Hero Banner
    protected void gvHeroV_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvHeroV.EditIndex = e.NewEditIndex;
            BindVisaHeroGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroV_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-V";
            int heroDetailId = Convert.ToInt32(gvHeroV.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fileUploadHero = (FileUpload)gvHeroV.Rows[e.RowIndex].FindControl("fuEditHeroV");
            bool imageValidation = true;
            DateTime effDate = (gvHeroV.Rows[e.RowIndex].FindControl("dcEditHeroImgEffeV") as DateControl).Value;
            DateTime expDate = (gvHeroV.Rows[e.RowIndex].FindControl("dcEditHeroImgExpV") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fileUploadHero.HasFile)
            {
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                if (size > 1024)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 1920)
                {
                    custValHeroV.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroV.Visible = true;
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 600)
                {
                    custValHeroV.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                    imageValidation = false;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValHeroV.Text = "Effective date cannot be blank!";
                custValHeroV.Visible = true;
                custValHeroV.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValHeroV.Text = "Expiry date cannot be blank!";
                custValHeroV.Visible = true;
                custValHeroV.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValHeroV.Text = "Effective date must be greater than todays date!";
            //    custValHeroV.Visible = true;
            //    custValHeroV.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValHeroV.Text = "Expiry date must be greater than effective date!";
                custValHeroV.Visible = true;
                custValHeroV.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }


            if (imageValidation)
            {
                custValHeroV.Visible = false;
                custValHeroV.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvHeroV.Rows[e.RowIndex].FindControl("txtEditHeroTitleV");
                TextBox txtHeroBannerURl = (TextBox)gvHeroV.Rows[e.RowIndex].FindControl("txtEditHeroBannerURlV");
               
                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvHeroV.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILEPATHV");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvHeroV.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILENAMEV");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvHeroV.Rows[e.RowIndex].FindControl("hdfHeroCONTENTFILETYPEV");
                HiddenField hdfHeroOrderNo = (HiddenField)gvHeroV.Rows[e.RowIndex].FindControl("hdfHeroOrderNoV");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "HERO";
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
              
                //objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Visa;
                objContent.save();

                if (fileUploadHero.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fileUploadHero.FileName;
                    fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fileUploadHero.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvHeroV.EditIndex = -1;
                BindVisaHeroGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroV_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValHeroV.Visible = false;
            custValHeroV.Text = string.Empty;
            gvHeroV.EditIndex = -1;
            BindVisaHeroGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroV_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvHeroV.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvHeroV.Rows[e.RowIndex].FindControl("hdnHeroStatusV");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindVisaHeroGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroV_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-V";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fileUploadHero = (FileUpload)gvHeroV.FooterRow.FindControl("fuHeroV");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileUploadHero.PostedFile.InputStream);
                int size = fileUploadHero.PostedFile.ContentLength / 1024;
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvHeroV.FooterRow.FindControl("dcHeroImgEffeVisa") as DateControl).Value;
                DateTime expDate = (gvHeroV.FooterRow.FindControl("dcHeroImgExpV") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "HERO", (int)ProductType.Visa);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvHeroV.FooterRow.FindControl("ddlHeroBannerOrderV");
                if (size > 1024)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 1024 KB (1 MB) only');", "SCRIPT");
                }
                else if (width < 1920)
                {
                    custValHeroV.Text = "Image width should be greater than or equal to 1920px.";
                    custValHeroV.Visible = true;
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 600)
                {
                    custValHeroV.Text = "Image height should be greater than or equal to 600px.";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValHeroV.Text = "Effective date cannot be blank!";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValHeroV.Text = "Expiry date cannot be blank!";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValHeroV.Text = "Effective date must be greater than todays date!";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValHeroV.Text = "Expiry date must be greater than effective date !";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValHeroV.Text = "Duplicate  Order!";
                    custValHeroV.ForeColor = System.Drawing.Color.Red;
                    custValHeroV.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValHeroV.Visible = false;
                    custValHeroV.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvHeroV.FooterRow.FindControl("txtHeroTitleV");
                    TextBox txtHeroBannerURl = (TextBox)gvHeroV.FooterRow.FindControl("txtHeroBannerURlV");
                   
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Visa;
                    objContent.ContentType = "HERO";
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    int detailId = objContent.save();

                    if (fileUploadHero.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fileUploadHero.FileName;
                        fileExtension = System.IO.Path.GetExtension(fileUploadHero.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fileUploadHero.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindVisaHeroGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvHeroV_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelHeroV");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusHeroV");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Middle Banner
    protected void gvMidV_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvMidV.EditIndex = e.NewEditIndex;
            BindVisaPromoMidGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidV_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidV_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-V";
            int heroDetailId = Convert.ToInt32(gvMidV.DataKeys[e.RowIndex].Values["DETAILID"]);
            FileUpload fuEditMidV = (FileUpload)gvMidV.Rows[e.RowIndex].FindControl("fuEditMidV");
            bool imageValidation = true;
            DateTime effDate = (gvMidV.Rows[e.RowIndex].FindControl("dcEditMidImgEffeV") as DateControl).Value;
            DateTime expDate = (gvMidV.Rows[e.RowIndex].FindControl("dcEditMidImgExpV") as DateControl).Value;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (fuEditMidV.HasFile)
            {
                int size = fuEditMidV.PostedFile.ContentLength / 1024;
                if (size > 200)
                {
                    imageValidation = false;
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuEditMidV.PostedFile.InputStream);
                int height = img.Height;
                int width = img.Width;
                if (width < 380)
                {
                    custValMidV.Text = "Image width should be greater than or equal to 380px.";
                    custValMidV.Visible = true;
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    imageValidation = false;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 260)
                {
                    custValMidV.Text = "Image height should be greater than or equal to 260px.";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                    imageValidation = false;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
            }
            else if (effDate == DateTime.MinValue)
            {
                custValMidV.Text = "Effective date cannot be blank!";
                custValMidV.Visible = true;
                custValMidV.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            else if (expDate == DateTime.MinValue)
            {
                custValMidV.Text = "Expiry date cannot be blank!";
                custValMidV.Visible = true;
                custValMidV.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
              //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }
            //else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    custValMidV.Text = "Effective date must be greater than todays date!";
            //    custValMidV.Visible = true;
            //    custValMidV.ForeColor = System.Drawing.Color.Red;
            //    imageValidation = false;
            //}
            else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                custValMidV.Text = "Expiry date must be greater than effective date!";
                custValMidV.Visible = true;
                custValMidV.ForeColor = System.Drawing.Color.Red;
                imageValidation = false;
               // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
            }



            if (imageValidation)
            {
                custValMidV.Visible = false;
                custValMidV.Text = string.Empty;
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}

                TextBox txtHeroTitle = (TextBox)gvMidV.Rows[e.RowIndex].FindControl("txtEditMidTitleV");
                TextBox txtHeroBannerURl = (TextBox)gvMidV.Rows[e.RowIndex].FindControl("txtEditMidBannerURlV");

                TextBox txtEditMidNameV = (TextBox)gvMidV.Rows[e.RowIndex].FindControl("txtEditMidNameV");
                TextBox txtEditMidDescV = (TextBox)gvMidV.Rows[e.RowIndex].FindControl("txtEditMidDescV");
                TextBox txtHeroButtonCaption = (TextBox)gvMidV.Rows[e.RowIndex].FindControl("txtEditMidButtonCaptionV");

                HiddenField hdfHeroCONTENTFILEPATH = (HiddenField)gvMidV.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILEPATHV");
                HiddenField hdfHeroCONTENTFILENAME = (HiddenField)gvMidV.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILENAMEV");
                HiddenField hdfHeroCONTENTFILETYPE = (HiddenField)gvMidV.Rows[e.RowIndex].FindControl("hdfMidCONTENTFILETYPEV");
                HiddenField hdfHeroOrderNo = (HiddenField)gvMidV.Rows[e.RowIndex].FindControl("hdfMidOrderNoV");

                CMSContentDetails objContent = new CMSContentDetails();
                objContent.DetailId = heroDetailId;
                objContent.ContentTitle = txtHeroTitle.Text;
                objContent.ContentType = "PROMOMID";
                objContent.ContentName = txtEditMidNameV.Text;
                objContent.ContentDescription = txtEditMidDescV.Text;
                objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objContent.ContentURL = txtHeroBannerURl.Text;
                objContent.OrderNo = Convert.ToInt32(hdfHeroOrderNo.Value);
                //objContent.Status = "A";
                objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                objContent.ContentFilePath = Convert.ToString(hdfHeroCONTENTFILEPATH.Value);
                objContent.ContentFileName = Convert.ToString(hdfHeroCONTENTFILENAME.Value);
                objContent.ContentFileType = Convert.ToString(hdfHeroCONTENTFILETYPE.Value);
                objContent.ProductType = (int)ProductType.Visa;
                objContent.Flex1 = txtHeroButtonCaption.Text;
                objContent.save();

                if (fuEditMidV.HasFile && heroDetailId > 0)
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                    string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                    if (!Directory.Exists(clientFolder))
                    {
                        Directory.CreateDirectory(clientFolder);
                    }
                    fileName = fuEditMidV.FileName;
                    fileExtension = System.IO.Path.GetExtension(fuEditMidV.FileName);
                    str_image = Convert.ToString(heroDetailId) + fileExtension;
                    pathToSave = clientFolder + str_image;
                    fuEditMidV.SaveAs(pathToSave);
                    string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                    objContent.UpdateContentFileDetails(heroDetailId, contentFilePath, str_image, fileExtension);
                }
                gvMidV.EditIndex = -1;
                BindVisaPromoMidGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateAlert();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidV_RowUpdating event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidV_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            custValMidV.Visible = false;
            custValMidV.Text = string.Empty;
            gvMidV.EditIndex = -1;
            BindVisaPromoMidGrid();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidV_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidV_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvMidV.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gvMidV.Rows[e.RowIndex].FindControl("hdnMidStatusV");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindVisaPromoMidGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidV_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidV_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            hdntabId.Value = "2-V";
            if (e.CommandName.Equals("AddNew"))
            {
                FileUpload fuMidV = (FileUpload)gvMidV.FooterRow.FindControl("fuMidV");
                System.Drawing.Image img = System.Drawing.Image.FromStream(fuMidV.PostedFile.InputStream);
                int size = fuMidV.PostedFile.ContentLength / 1024;
                
                int height = img.Height;
                int width = img.Width;
                DateTime effDate = (gvMidV.FooterRow.FindControl("dcMidImgEffeVisa") as DateControl).Value;
                DateTime expDate = (gvMidV.FooterRow.FindControl("dcMidImgExpV") as DateControl).Value;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DataTable dtHeroBannerDetails = CMSContentDetails.GetBannersList(Convert.ToInt32(Session["CMSID"]), "PROMOMID", (int)ProductType.Visa);
                DropDownList ddlHeroBannerOrder = (DropDownList)gvMidV.FooterRow.FindControl("ddlMidBannerOrderV");
                TextBox txtEditMidNameV = (TextBox)gvMidV.FooterRow.FindControl("txtMidNameV");
                TextBox txtEditMidDescV = (TextBox)gvMidV.FooterRow.FindControl("txtMidDescV");
                
                if (size > 200)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than or equal to 200 KB only');", "SCRIPT");
                }
                else if (width < 380)
                {
                    custValMidV.Text = "Image width should be greater than or equal to 380px.";
                    custValMidV.Visible = true;
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (height < 260)
                {
                    custValMidV.Text = "Image height should be greater than or equal to 260px.";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (effDate == DateTime.MinValue)
                {
                    custValMidV.Text = "Effective date cannot be blank!";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                    //hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (expDate == DateTime.MinValue)
                {
                    custValMidV.Text = "Expiry date cannot be blank!";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                    hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    custValMidV.Text = "Effective date must be greater than todays date!";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                   // hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    custValMidV.Text = "Expiry date must be greater than effective date !";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else if (dtHeroBannerDetails.Select(string.Format("ORDERNO='{0}'", ddlHeroBannerOrder.SelectedItem.Value)).Length > 0)
                {
                    custValMidV.Text = "Duplicate  Order!";
                    custValMidV.ForeColor = System.Drawing.Color.Red;
                    custValMidV.Visible = true;
                  //  hdntabId.Value = Convert.ToString(ViewState["tabId"]);
                    Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                }
                else
                {
                    custValMidV.Visible = false;
                    custValMidV.Text = string.Empty;
                    string str_image = string.Empty;
                    string pathToSave = string.Empty;
                    string fileName = string.Empty;
                    string fileExtension = string.Empty;
                    string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //Gets a value indicating whether the http connection uses secure sockets i.e https)
                    //if (HttpContext.Current.Request.IsSecureConnection)
                    //{
                    //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    //if (Request.Url.Port > 0)
                    //{
                    //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                    //}
                    TextBox txtHeroTitle = (TextBox)gvMidV.FooterRow.FindControl("txtMidTitleV");
                    TextBox txtHeroBannerURl = (TextBox)gvMidV.FooterRow.FindControl("txtMidBannerURlV");
                    TextBox txtHeroButtonCaption = (TextBox)gvMidV.FooterRow.FindControl("txtMidButtonCaptionV");
                    CMSContentDetails objContent = new CMSContentDetails();
                    objContent.DetailId = -1;
                    objContent.CmsId = Convert.ToInt32(Session["CMSID"]);
                    objContent.ContentTitle = txtHeroTitle.Text;
                    objContent.ProductType = (int)ProductType.Visa;
                    objContent.ContentType = "PROMOMID";
                    objContent.ContentName = txtEditMidNameV.Text;
                    objContent.ContentDescription = txtEditMidDescV.Text;
                    objContent.ContentFilePath = string.Empty;
                    objContent.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                    objContent.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                    objContent.ContentURL = txtHeroBannerURl.Text;
                    objContent.OrderNo = Convert.ToInt32(ddlHeroBannerOrder.SelectedItem.Value);
                    objContent.Status = "A";
                    objContent.CreatedBy = (int)Settings.LoginInfo.UserID;
                    objContent.ContentFileName = string.Empty;
                    objContent.ContentFileType = string.Empty;
                    objContent.Flex1 = txtHeroButtonCaption.Text;
                    int detailId = objContent.save();

                    if (fuMidV.HasFile && detailId > 0)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fuMidV.FileName;
                        fileExtension = System.IO.Path.GetExtension(fuMidV.FileName);
                        str_image = Convert.ToString(detailId) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fuMidV.SaveAs(pathToSave);
                        string contentFilePath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        objContent.UpdateContentFileDetails(detailId, contentFilePath, str_image, fileExtension);
                    }
                    BindVisaPromoMidGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidV_RowCommand event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gvMidV_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelMidV");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusMidV");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvMidV_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #endregion

    #region Footer Links Grid Events
    protected void gridViewL_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gridViewL.EditIndex = e.NewEditIndex;
            BindFooterLinksGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewL_RowEditing event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewL_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            hdntabId.Value = "3";
            int linkId = Convert.ToInt32(gridViewL.DataKeys[e.RowIndex].Values["LINKID"]);
            //DropDownList ddlEditFooterLinkType = (DropDownList)gridViewL.Rows[e.RowIndex].FindControl("ddlEditFooterLinkType");
            //DropDownList ddlEditFooterCopyRightOrder = (DropDownList)gridViewL.Rows[e.RowIndex].FindControl("ddlEditFooterCopyRightOrder");
            TextBox txtEditMenuItem = (TextBox)gridViewL.Rows[e.RowIndex].FindControl("txtEditMenuItem");
            TextBox txtEditMenuURL = (TextBox)gridViewL.Rows[e.RowIndex].FindControl("txtEditMenuURL");

            HiddenField hdfFooterLinksType = (HiddenField)gridViewL.Rows[e.RowIndex].FindControl("hdfFooterLinksType");
            HiddenField hdfFooterLinksOrderNo = (HiddenField)gridViewL.Rows[e.RowIndex].FindControl("hdfFooterLinksOrderNo");


            DateTime effDate = (gridViewL.Rows[e.RowIndex].FindControl("dcEditLinkImageEffective") as DateControl).Value;
            DateTime expDate = (gridViewL.Rows[e.RowIndex].FindControl("dcEditLImageExpiry") as DateControl).Value;
            if (effDate == DateTime.MinValue)
            {
                Utility.StartupScript(this.Page, "alert('Effective date cannot be blank!')", "");
                throw new Exception("Effective date cannot be blank!");
            }

            if (expDate == DateTime.MinValue)
            {
                Utility.StartupScript(this.Page, "alert('Expiry date cannot be blank!')", "");
                throw new Exception("Expiry date cannot be blank !");
            }

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            //if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    Utility.StartupScript(this.Page, "alert('Effective date must be greater than todays date!')", "");
            //    throw new Exception("Effective date must be greater than todays date !");
            //}

            if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                Utility.StartupScript(this.Page, "alert('Expiry date must be greater than effective date!')", "");
                throw new Exception("Expiry date must be greater than effective date !");
            }



            CMSFooterLinks objLink = new CMSFooterLinks();
            objLink.LinkId = linkId;
            objLink.LinkType = Convert.ToString(hdfFooterLinksType.Value);
            objLink.OrderNo = Convert.ToInt32(hdfFooterLinksOrderNo.Value);
            objLink.LinkName = txtEditMenuItem.Text;
            objLink.LinkURL = txtEditMenuURL.Text;
            objLink.CmsId = Convert.ToInt32(Session["CMSID"]);
            objLink.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
            objLink.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
            objLink.Status = "A";
            objLink.CreatedBy = (int)Settings.LoginInfo.UserID;
            objLink.save();

            gridViewL.EditIndex = -1;
            BindFooterLinksGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateNotification();", "SCRIPT");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewL_RowUpdating event error.Reason: " + ex.ToString(), "0");
           
            Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

        }
    }
    protected void gridViewL_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gridViewL.EditIndex = -1;
            BindFooterLinksGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewL_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewL_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            try
            {
                int heroDetailId = Convert.ToInt32(gridViewL.DataKeys[e.RowIndex].Values["LINKID"]);
                HiddenField hdnStatus = (HiddenField)gridViewL.Rows[e.RowIndex].FindControl("hdnFooterLinksStatusL");
                string rowStatus = string.Empty;
                if (hdnStatus != null && hdnStatus.Value == "A")
                {
                    rowStatus = "D";
                }
                else if (hdnStatus != null && hdnStatus.Value == "D")
                {
                    rowStatus = "A";
                }
                CMSFooterLinks.DeleteLinkDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
                BindFooterLinksGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewL_RowDeleting event error.Reason: " + ex.ToString(), "0");
            }




        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewL_RowDeleting event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewL_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            
            if (e.CommandName.Equals("AddNew"))
            {
                DropDownList ddlEditFooterLinkType = (DropDownList)gridViewL.FooterRow.FindControl("ddlFooterLinkType");
                DropDownList ddlEditFooterCopyRightOrder = (DropDownList)gridViewL.FooterRow.FindControl("ddlFooterCopyRightOrder");
                TextBox txtEditMenuItem = (TextBox)gridViewL.FooterRow.FindControl("txtMenuItem");
                TextBox txtEditMenuURL = (TextBox)gridViewL.FooterRow.FindControl("txtMenuURL");
                DateTime effDate = (gridViewL.FooterRow.FindControl("dcLinkImageEffective") as DateControl).Value;
                DateTime expDate = (gridViewL.FooterRow.FindControl("dcLImageExpiry") as DateControl).Value;

                DataTable dtLinks = CMSFooterLinks.GetAllLinksList(Convert.ToInt32(Session["CMSID"]));

                DataRow[] results = dtLinks.Select("LINKTYPE = '" + ddlEditFooterLinkType.SelectedItem.Value + "' AND ORDERNO = '" + ddlEditFooterCopyRightOrder.SelectedItem.Value + "'");
                if (results.Length > 0)
                {
                    Utility.StartupScript(this.Page, "alert('Duplicate  Order!')", "");
                    throw new Exception("Order Already Exists !");
                }

                if (effDate == DateTime.MinValue)
                {
                    Utility.StartupScript(this.Page, "alert('Effective date cannot be blank!')", "");
                    throw new Exception("Effective date cannot be blank!");
                }

                if (expDate == DateTime.MinValue)
                {
                    Utility.StartupScript(this.Page, "alert('Expiry date cannot be blank!')", "");
                    throw new Exception("Expiry date cannot be blank !");
                }

                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    Utility.StartupScript(this.Page, "alert('Effective date must be greater than todays date!')", "");
                    throw new Exception("Effective date must be greater than todays date !");
                }

                if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    Utility.StartupScript(this.Page, "alert('Expiry date must be greater than effective date!')", "");
                    throw new Exception("Expiry date must be greater than effective date !");
                }




                CMSFooterLinks objLink = new CMSFooterLinks();
                objLink.LinkId = -1;
                objLink.CmsId = Convert.ToInt32(Session["CMSID"]);
                objLink.LinkType = ddlEditFooterLinkType.SelectedItem.Value;
                objLink.OrderNo = Convert.ToInt32(ddlEditFooterCopyRightOrder.SelectedItem.Value);
                objLink.LinkName = txtEditMenuItem.Text;
                objLink.LinkURL = txtEditMenuURL.Text;
                //IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                objLink.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objLink.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objLink.Status = "A";
                objLink.CreatedBy = (int)Settings.LoginInfo.UserID;
                objLink.save();

                BindFooterLinksGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveNotification();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewL_RowCommand event error.Reason: " + ex.ToString(), "0");
           
        }

    }
    protected void gridViewL_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("ButtonDeleteL");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusFooterL");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvHeroV_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion Footer Links Grid Events

    #region Social Media Links Grid  Events
    protected void gridViewSM_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gridViewSM.EditIndex = e.NewEditIndex;
            BindSocialMediaLinksGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewSM_RowEditing event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewSM_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            
            int linkId = Convert.ToInt32(gridViewSM.DataKeys[e.RowIndex].Values["DETAILID"]);
            TextBox txtEditMenuItem = (TextBox)gridViewSM.Rows[e.RowIndex].FindControl("smtxtEditMenuItem");
            TextBox txtEditMenuURL = (TextBox)gridViewSM.Rows[e.RowIndex].FindControl("smtxtEditMenuURL");

            HiddenField hdfFooterLinksType = (HiddenField)gridViewSM.Rows[e.RowIndex].FindControl("smhdfFooterLinksType");
            //  HiddenField hdfFooterLinksOrderNo = (HiddenField)gridViewSM.Rows[e.RowIndex].FindControl("smhdfFooterLinksOrderNo");


            DateTime effDate = (gridViewSM.Rows[e.RowIndex].FindControl("smdcEditLinkImageEffective") as DateControl).Value;
            DateTime expDate = (gridViewSM.Rows[e.RowIndex].FindControl("smdcEditLImageExpiry") as DateControl).Value;
            if (effDate == DateTime.MinValue)
            {
                Utility.StartupScript(this.Page, "alert('Effective date cannot be blank!')", "");
                throw new Exception("Effective date cannot be blank!");
            }

            if (expDate == DateTime.MinValue)
            {
                Utility.StartupScript(this.Page, "alert('Expiry date cannot be blank!')", "");
                throw new Exception("Expiry date cannot be blank !");
            }

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            //if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    Utility.StartupScript(this.Page, "alert('Effective date must be greater than todays date!')", "");
            //    throw new Exception("Effective date must be greater than todays date !");
            //}

            if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                Utility.StartupScript(this.Page, "alert('Expiry date must be greater than effective date!')", "");
                throw new Exception("Expiry date must be greater than effective date !");
            }

            CMSContentDetails objGoogle = new CMSContentDetails();
            objGoogle.DetailId = linkId;
            objGoogle.CmsId = Convert.ToInt32(Session["CMSID"]);
            objGoogle.ProductType = 0;
            objGoogle.ContentType = hdfFooterLinksType.Value;
            objGoogle.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
            objGoogle.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
            objGoogle.ContentURL = txtEditMenuURL.Text;
            objGoogle.CreatedBy = (int)Settings.LoginInfo.UserID;
            objGoogle.save();
            gridViewSM.EditIndex = -1;
            BindSocialMediaLinksGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateNotification();", "SCRIPT");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewSM_RowUpdating event error.Reason: " + ex.ToString(), "0");
           

        }
    }
    protected void gridViewSM_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gridViewSM.EditIndex = -1;
            BindSocialMediaLinksGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewSM_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewSM_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            int heroDetailId = Convert.ToInt32(gridViewSM.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gridViewSM.Rows[e.RowIndex].FindControl("smhdnFooterLinksStatusL");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindSocialMediaLinksGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewSM_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }

    }
    protected void gridViewSM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            
            if (e.CommandName.Equals("AddNew"))
            {
                DropDownList ddlEditFooterLinkType = (DropDownList)gridViewSM.FooterRow.FindControl("smddlFooterLinkType");
                //DropDownList ddlEditFooterCopyRightOrder = (DropDownList)gridViewSM.FooterRow.FindControl("smddlFooterCopyRightOrder");

                TextBox txtEditMenuURL = (TextBox)gridViewSM.FooterRow.FindControl("smtxtMenuURL");
                DateTime effDate = (gridViewSM.FooterRow.FindControl("smdcLinkImageEffective") as DateControl).Value;
                DateTime expDate = (gridViewSM.FooterRow.FindControl("smdcLImageExpiry") as DateControl).Value;

                DataTable dtLinks = CMSContentDetails.GetSocialMediaList(Convert.ToInt32(Session["CMSID"]), 0);

                DataRow[] results = dtLinks.Select("CONTENTTYPE = '" + ddlEditFooterLinkType.SelectedItem.Value + "'");
                if (results.Length > 0)
                {
                    Utility.StartupScript(this.Page, "alert('Duplicate  Item!')", "");
                    throw new Exception("Order Already Exists !");
                }

                if (effDate == DateTime.MinValue)
                {
                    Utility.StartupScript(this.Page, "alert('Effective date cannot be blank!')", "");
                    throw new Exception("Effective date cannot be blank!");
                }

                if (expDate == DateTime.MinValue)
                {
                    Utility.StartupScript(this.Page, "alert('Expiry date cannot be blank!')", "");
                    throw new Exception("Expiry date cannot be blank !");
                }

                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    Utility.StartupScript(this.Page, "alert('Effective date must be greater than todays date!')", "");
                    throw new Exception("Effective date must be greater than todays date !");
                }

                if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    Utility.StartupScript(this.Page, "alert('Expiry date must be greater than effective date!')", "");
                    throw new Exception("Expiry date must be greater than effective date !");
                }

                CMSContentDetails objGoogle = new CMSContentDetails();
                objGoogle.DetailId = -1;
                objGoogle.CmsId = Convert.ToInt32(Session["CMSID"]);
                objGoogle.ProductType = 0;
                objGoogle.ContentType = ddlEditFooterLinkType.SelectedItem.Value;
                objGoogle.Status = "A";
                objGoogle.ContentURL = txtEditMenuURL.Text;
                objGoogle.CreatedBy = (int)Settings.LoginInfo.UserID;
                objGoogle.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objGoogle.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objGoogle.save();
                gridViewSM.EditIndex = -1;
                BindSocialMediaLinksGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveNotification();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewSM_RowCommand event error.Reason: " + ex.ToString(), "0");
       
        }

    }
    protected void gridViewSM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("smButtonDeleteL");
                Label lblStatus = (Label)e.Row.FindControl("smlblStatusFooterL");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewSM_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #region Tags Manager Links Grid Events
    protected void gridViewTM_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gridViewTM.EditIndex = e.NewEditIndex;
            BindTagsLinksGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowEditing event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewTM_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            
            int linkId = Convert.ToInt32(gridViewTM.DataKeys[e.RowIndex].Values["DETAILID"]);
            TextBox txtEditMenuItem = (TextBox)gridViewTM.Rows[e.RowIndex].FindControl("tmtxtEditMenuItem");
            TextBox txtEditMenuURL = (TextBox)gridViewTM.Rows[e.RowIndex].FindControl("tmtxtEditMenuURL");

            TextBox headerTagsDesc = (TextBox)gridViewTM.Rows[e.RowIndex].FindControl("txtEditHeaderTag");
            HiddenField hdfFooterLinksType = (HiddenField)gridViewTM.Rows[e.RowIndex].FindControl("tmhdfFooterLinksType");
            HiddenField hdfFooterLinksOrderNo = (HiddenField)gridViewTM.Rows[e.RowIndex].FindControl("tmhdfFooterLinksOrderNo");


            DateTime effDate = (gridViewTM.Rows[e.RowIndex].FindControl("tmdcEditLinkImageEffective") as DateControl).Value;
            DateTime expDate = (gridViewTM.Rows[e.RowIndex].FindControl("tmdcEditLImageExpiry") as DateControl).Value;
            if (effDate == DateTime.MinValue)
            {
                Utility.StartupScript(this.Page, "alert('Effective date cannot be blank!')", "");
                throw new Exception("Effective date cannot be blank!");
            }

            if (expDate == DateTime.MinValue)
            {
                Utility.StartupScript(this.Page, "alert('Expiry date cannot be blank!')", "");
                throw new Exception("Expiry date cannot be blank !");
            }

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            //if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
            //{
            //    Utility.StartupScript(this.Page, "alert('Effective date must be greater than todays date!')", "");
            //    throw new Exception("Effective date must be greater than todays date !");
            //}

            if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
            {
                Utility.StartupScript(this.Page, "alert('Expiry date must be greater than effective date!')", "");
                throw new Exception("Expiry date must be greater than effective date !");
            }

            CMSContentDetails objGoogle = new CMSContentDetails();
            objGoogle.DetailId = linkId;
            objGoogle.CmsId = Convert.ToInt32(Session["CMSID"]);
            objGoogle.ProductType = 0;
            objGoogle.ContentType = hdfFooterLinksType.Value;
            objGoogle.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
            objGoogle.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
            objGoogle.ContentDescription = txtEditMenuURL.Text;
            objGoogle.CreatedBy = (int)Settings.LoginInfo.UserID;
            objGoogle.ContentTitle = headerTagsDesc.Text;//Save the header tags description into the content title column.
            objGoogle.save();
            gridViewTM.EditIndex = -1;
            BindTagsLinksGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateNotification();", "SCRIPT");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowUpdating event error.Reason: " + ex.ToString(), "0");
            

        }
    }
    protected void gridViewTM_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gridViewTM.EditIndex = -1;
            BindTagsLinksGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewTM_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            int heroDetailId = Convert.ToInt32(gridViewTM.DataKeys[e.RowIndex].Values["DETAILID"]);
            HiddenField hdnStatus = (HiddenField)gridViewTM.Rows[e.RowIndex].FindControl("tmhdnFooterLinksStatusL");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSContentDetails.DeleteContentDetail(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            BindTagsLinksGrid();
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }

    }
    protected void gridViewTM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            
            if (e.CommandName.Equals("AddNew"))
            {
                DropDownList ddlEditFooterLinkType = (DropDownList)gridViewTM.FooterRow.FindControl("tmddlFooterLinkType");
                DropDownList ddlEditFooterCopyRightOrder = (DropDownList)gridViewTM.FooterRow.FindControl("tmddlFooterCopyRightOrder");

                TextBox txtEditMenuURL = (TextBox)gridViewTM.FooterRow.FindControl("tmtxtMenuURL");
                DateTime effDate = (gridViewTM.FooterRow.FindControl("tmdcLinkImageEffective") as DateControl).Value;
                DateTime expDate = (gridViewTM.FooterRow.FindControl("tmdcLImageExpiry") as DateControl).Value;

                DataTable dtLinks = CMSContentDetails.GetTagsList(Convert.ToInt32(Session["CMSID"]), 0);
                DataRow[] results = dtLinks.Select("CONTENTTYPE = '" + ddlEditFooterLinkType.SelectedItem.Value + "'");
                TextBox headerTagsDesc = (TextBox)gridViewTM.FooterRow.FindControl("txtHeaderTag");

                if (results.Length > 0)
                {
                    Utility.StartupScript(this.Page, "alert('Duplicate  Item!')", "");
                    throw new Exception("Order Already Exists !");
                }

                if (effDate == DateTime.MinValue)
                {
                    Utility.StartupScript(this.Page, "alert('Effective date cannot be blank!')", "");
                    throw new Exception("Effective date cannot be blank!");
                }

                if (expDate == DateTime.MinValue)
                {
                    Utility.StartupScript(this.Page, "alert('Expiry date cannot be blank!')", "");
                    throw new Exception("Expiry date cannot be blank !");
                }

                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                if (Convert.ToDateTime(effDate, dateFormat) < DateTime.Today)
                {
                    Utility.StartupScript(this.Page, "alert('Effective date must be greater than todays date!')", "");
                    throw new Exception("Effective date must be greater than todays date !");
                }

                if (Convert.ToDateTime(expDate, dateFormat) < Convert.ToDateTime(effDate, dateFormat))
                {
                    Utility.StartupScript(this.Page, "alert('Expiry date must be greater than effective date!')", "");
                    throw new Exception("Expiry date must be greater than effective date !");
                }

                CMSContentDetails objGoogle = new CMSContentDetails();
                objGoogle.DetailId = -1;
                objGoogle.CmsId = Convert.ToInt32(Session["CMSID"]);
                objGoogle.ProductType = 0;
                objGoogle.ContentType = ddlEditFooterLinkType.SelectedItem.Value;
                objGoogle.Status = "A";
                objGoogle.ContentDescription = txtEditMenuURL.Text;
                objGoogle.CreatedBy = (int)Settings.LoginInfo.UserID;
                objGoogle.EffectiveDate = Convert.ToDateTime(effDate, dateFormat);
                objGoogle.ExpiryDate = Convert.ToDateTime(expDate, dateFormat);
                objGoogle.ContentTitle = headerTagsDesc.Text;  //Save the header tags description into the content title column.
                objGoogle.save();
                gridViewTM.EditIndex = -1;
                BindTagsLinksGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveNotification();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowCommand event error.Reason: " + ex.ToString(), "0");
           

        }

    }
    protected void gridViewTM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("tmButtonDeleteL");
                Label lblStatus = (Label)e.Row.FindControl("tmlblStatusFooterL");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //CMS_HEADER Details
            CMSHeader header = new CMSHeader();
            header.CountryCode = ddlCountry.SelectedItem.Value;
            header.Status = "A";
            header.CreatedBy = (int)Settings.LoginInfo.UserID;
            header.CmsId = -1;
            header.CultureCode = ddlCulture.SelectedItem.Value;//Passing CultureCode Addedby Harish on 20-Feb-2018
            _cmsId = header.save();
            Session["CMSID"] = _cmsId;
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "showBannersDiv();", "SCRIPT");



        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : btnSave_Click event error.Reason: " + ex.ToString(), "0");
        }


    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {

            Session["CMSID"] = null;
            //ViewState["tabId"] = null;
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "editModeReloadPage();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster Page- btnCancel_Click event .Error: " + ex.ToString(), "0");
        }
    }
    public void btnSaveHeaderTab_Click(object sender, EventArgs e)
    {
        try
        {

            if (Session["CMSID"] != null)
            {
                string str_image = string.Empty;
                string pathToSave = string.Empty;
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                string serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //Gets a value indicating whether the http connection uses secure sockets i.e https)
                //if (HttpContext.Current.Request.IsSecureConnection)
                //{
                //    serverPath = "https://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                //if (Request.Url.Port > 0)
                //{
                //    serverPath = "http://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                //}
                if (fuLogo.HasFile)
                {
                    hdntabId.Value = "1";
                    decimal size = Math.Round(((decimal)fuLogo.PostedFile.ContentLength / (decimal)1024), 2);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(fuLogo.PostedFile.InputStream);
                    int height = img.Height;
                    int width = img.Width;
                    if (width < 160)
                    {
                        lblHeaderVal.Text = "Image width should be greater than or equal to 160px.";
                        lblHeaderVal.Visible = true;
                        lblHeaderVal.ForeColor = System.Drawing.Color.Red;
                        
                        Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");

                    }
                    else if (height < 42)
                    {
                        lblHeaderVal.Text = "Image height should be greater than or equal to 42px.";
                        lblHeaderVal.ForeColor = System.Drawing.Color.Red;
                        lblHeaderVal.Visible = true;
           
                        Utility.StartupScript(this.Page, "navigateToTab();", "SCRIPT");
                    }
                    else if (size < 100)// 100 kb
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~/" + _cmsDocPath) + @"\";
                        string clientFolder = dirFullPath + Convert.ToInt32(Session["CMSID"]) + @"\";
                        if (!Directory.Exists(clientFolder))
                        {
                            Directory.CreateDirectory(clientFolder);
                        }
                        fileName = fuLogo.FileName;
                        fileExtension = System.IO.Path.GetExtension(fuLogo.FileName);
                        str_image = Convert.ToString("LOGO_" + Session["CMSID"]) + fileExtension;
                        pathToSave = clientFolder + str_image;
                        fuLogo.SaveAs(pathToSave);

                        CMSHeader header = new CMSHeader();
                        header.CmsId = Convert.ToInt32(Session["CMSID"]);
                        header.PhoneNo = txtContactNumber.Text;
                        header.CopyRightText = txtCopyRight.Text;
                        header.LogoPath = serverPath + _cmsDocPath + @"/" + Convert.ToInt32(Session["CMSID"]) + @"/" + str_image;
                        header.LogoDescription = txtLogoDesc.Text;
                        header.CreatedBy = (int)Settings.LoginInfo.UserID;
                        if (chkTopMenu.Checked)
                        {
                            header.ShowTopMenu = true;
                        }
                        if (chkFooterMenu.Checked)
                        {
                            header.ShowFooterMenu = true;
                        }
                        if (chkSocialIcons.Checked)
                        {
                            header.ShowSocialIcons = true;
                        }
                        
                        header.SaveHeaderTabInfo();
                        if (header.CmsId > 0)
                        {
                            if (!string.IsNullOrEmpty(header.LogoPath))
                            {
                                ImageHeader.Visible = true;
                                ImageHeader.ImageUrl = header.LogoPath;
                                hdfHeaderImage.Value = "1";
                            }
                        }
                        CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveAlert();", "SCRIPT");
                    }
                    else
                    {
                        CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ErrorAlert('Please Upload file less than 100 KB only');", "SCRIPT");
                    }
                }
            }
            //Response.Redirect("B2CClearCache.aspx");
            //WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["B2CClearCacheURL"].ToString());
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster Page- btnSaveHeaderTab_Click event .Error: " + ex.ToString(), "0");
        }
    }
    #endregion Button Events

    #region SearchGrid Events
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.Master.HideSearch();
            long vsId = Utility.ToLong(gvSearch.SelectedValue);
            Session["CMSID"] = Convert.ToInt32(vsId);

            Utility.StartupScript(this.Page, "editModeReloadPage()", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster Page- gvSearch_SelectedIndexChanged event .Error: " + ex.ToString(), "0");
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : btnSearch_Click event error.Reason: " + ex.ToString(), "0");
        }
    }
    private void bindSearch()
    {
        try
        {

            DataTable dt = CMSHeader.GetAllCMSList('A');
            if (dt != null && dt.Rows.Count > 0)
            {
                gvSearch.DataSource = dt;
                gvSearch.DataBind();
            }

        }
        catch { throw; }
    }

    protected void gvSearch_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int heroDetailId = Convert.ToInt32(gvSearch.DataKeys[e.RowIndex].Values["CMSID"]);
            HiddenField hdnStatus = (HiddenField)gvSearch.Rows[e.RowIndex].FindControl("hdnSearchStatus");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            CMSHeader.UpdateRowStatus(heroDetailId, (int)Settings.LoginInfo.UserID, rowStatus);
            bindSearch();
            // CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvSearch_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }
    }

    protected void gvSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDelSearchCMS");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusSearchCMS");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }


            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gvSearch_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Helper Methods
    protected string CZDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MM-yyyy");
        }
    }
    protected string CZDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion
}
