﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using Visa;
using CT.Core;
using System.Collections.Generic;

public partial class VisaTypeList : CT.Core.ParentPage
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            Page.Title = "Visa Type List";
            AuthorizationCheck();
            if (!IsPostBack)
            {
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "Visatypelist Page :" + ex.ToString(), "");
        }

    }

    private void InitializePageControls()
    {
        try
        {
            BindCountry();
            BindAgent();
            if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
            {
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Request.QueryString[0].Trim()));
            }
            else
            {
                ddlCountry.SelectedIndex = 1;
            }

            BindData();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "VisaTypeList Page: " + ex.ToString(), "");
        }
    }

    private void BindCountry()
    {
        ddlCountry.DataSource = VisaCountry.GetActiveVisaCountryList();
        ddlCountry.DataTextField = "countryName";
        ddlCountry.DataValueField = "countryCode";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));
    }

    private void BindAgent()
    {
        ddlAgent.DataSource = AgentMaster.GetB2CAgentList();
        ddlAgent.DataTextField = "agent_name";
        ddlAgent.DataValueField = "agent_id";
        ddlAgent.DataBind();
        ddlAgent.Items.Insert(0, new ListItem("--Select--", "-1"));
    }

    private void BindData()
    {
        string countryCode = ddlCountry.SelectedValue != "-1" ? ddlCountry.SelectedValue : string.Empty;
        string memberId = ddlAgent.SelectedValue != "-1" ? ddlAgent.SelectedValue : "0";

        List <VisaType> allVisaType = VisaType.GetVisaTypeList(countryCode, Convert.ToInt32(memberId));
        if (allVisaType == null || allVisaType.Count == 0)
        {
            lbl_msg.Text = "There are no record.";
            lbl_msg.Visible = true;

        }
        else
        {
            lbl_msg.Visible = false;
        }
        GridView1.DataSource = allVisaType;
        GridView1.DataBind();

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = ((LinkButton)e.Row.Cells[7].Controls[1]);
                lb.CommandArgument = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
                if (e.Row.Cells[5].Text == "True")
                {
                    lb.Text = "Deactivate";
                    e.Row.Cells[5].Text = "Active";
                }
                else
                {
                    lb.Text = "Activate";
                    e.Row.Cells[5].Text = "Inactive";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "VisaTypeList Page: "+ex.ToString(), "");
        }

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "ChangeStatus")
            {
                bool isActive;
                string status = ((LinkButton)e.CommandSource).Text;
                int rowNumber = Convert.ToInt32(e.CommandArgument);
                // int i = Convert.ToInt32(GridView1.DataKeys[rowNumber].Value);
                if (status == "Activate")
                {
                    isActive = true;
                }
                else
                {
                    isActive = false;
                }
                VisaType.ChangeStatus(rowNumber, isActive);
                BindData();


            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "VisaTypeList Page: " + ex.ToString(), "");
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridView1.PageIndex = e.NewPageIndex;
            BindData();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "VisaTypeList Page: " + ex.ToString(), "");
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindData();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "VisaTypeList Page: " + ex.ToString(), "");
        }
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
