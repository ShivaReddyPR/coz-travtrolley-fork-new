﻿using CT.Core;
using System;
using System.Collections.Generic;
using System.Configuration;

public static class CozmoExpenseApi
{
    public static string sAPIURL = ConfigurationManager.AppSettings["WebApiExpenseUrl"];

    /// <summary>
    /// To update reimburse revise status
    /// </summary>
    /// <returns></returns>
    public static void ReimburseRevise(List<object> liExpnses)
    {
        try
        {
            object objInput = new
            {
                AgentInfo = CozmoApi.GetAgentInfo(),
                ApproverReports = liExpnses,
                ApproverQueue = string.Empty,
                type = "AQ",
                Host = string.Empty
            };

            string sUrl = sAPIURL.TrimEnd('/') + "/api/expTransactions/saveExpApprovalReports";
            var results = CozmoApi.WebReqCall(sUrl, string.Empty, new List<string>(), string.Empty, CozmoApi.SerializeJson(objInput), string.Empty);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to ReimburseRevise at(CozmoExpenseApi.cs) Error: " + ex.ToString(), string.Empty);
        }
    }
}
