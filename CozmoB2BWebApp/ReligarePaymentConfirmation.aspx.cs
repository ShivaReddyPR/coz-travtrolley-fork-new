﻿using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using ReligareInsurance;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using CT.AccountingEngine;
using CT.BookingEngine.Insurance;
using ZeusInsB2B.Zeus;
using System.Web.UI;
using CT.TicketReceipt.Common;
using System.IO;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Collections;
 
namespace CozmoB2BWebApp
{
    public partial class ReligarePaymentConfirmation : CT.Core.ParentPage
    {
        protected ReligareHeader header;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Add_PassengerDetails();          
                if (Session["quotation"] != null)
                {
                    // Quotation Details
                    ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];                    
                    lblStartDate.Text = Convert.ToDateTime(quotation.StartDate).ToString("dd/MMM/yyyy");
                    lblEndDate.Text = Convert.ToDateTime(quotation.EndDate).ToString("dd/MMM/yyyy");
                    lblTravellingTo.Text = quotation.ProductName;
                    lblPED.Text = quotation.Ped == "0" ? "YES" : "NO"; 
                    lblPremium.Text = quotation.Premium.ToString();
                }
                MarkupDetails();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Payment Confirmation Failed to Quotation Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Displaying Agent balance Details and Fare Details.
        /// </summary>
        private void MarkupDetails()
        {
            try
            {
                decimal gst = 0, basefare = 0;
                header = (ReligareHeader)Session["religareHeader"];                
                basefare = Convert.ToDecimal(header.Premium_Amount + header.Markup + header.HandlingFee);
                // Agent Balance 
                AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                //Onbehalf of agent BAlance.
                lblagentBal.Visible = false;
                if (header.Agent_id != Settings.LoginInfo.AgentId)
                {
                    agent = new AgentMaster(header.Agent_id);
                    lblagentBal.Text = "Cr.Balance :" + CTCurrencyFormat(agent.CurrentBalance, header.Agent_id) + " Booking on behalf of " + agent.Name + " " + agent.City;
                    lblagentBal.Visible = true;
                }
                lblAgentBalance.Text = Convert.ToDecimal(agent.CurrentBalance).ToString("N" + agent.DecimalValue);
                // For GST Calculation.
                List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                gst = Math.Round(GSTTaxDetail.LoadGSTValues(ref gstTaxList, Convert.ToDecimal(header.Markup), (header.Location_id)), agent.DecimalValue); ;
                lblBaseFare.Text = Convert.ToDecimal(header.Premium_Amount + header.Markup + header.HandlingFee).ToString("N" + agent.DecimalValue); //basefare.ToString();
                lblMarkup.Text = Convert.ToDecimal(header.MarkupValue).ToString("N" + agent.DecimalValue);
                lblTax.Text = Convert.ToDecimal("0.00").ToString("N" + agent.DecimalValue);
                lblVATAmount.Text = Convert.ToDecimal(gst).ToString("N" + agent.DecimalValue);
                lblMarkupTotal.Text = Convert.ToDecimal(header.Premium_Amount + gst + header.Markup + header.HandlingFee).ToString("N" + agent.DecimalValue);
                lblTxnAmount.Text = Convert.ToDecimal(header.Premium_Amount + gst + header.Markup + header.HandlingFee).ToString("N" + agent.DecimalValue);
                //lblPremium.Text = Convert.ToDecimal(header.Premium_Amount + gst + header.Markup + header.HandlingFee).ToString("N" + agent.DecimalValue);
                lblPremium.Text = CTCurrencyFormat(header.Premium_Amount + gst + header.Markup + header.HandlingFee,header.Agent_id);
                header.GstValue = gst; 
                Session["gstTaxList"] = gstTaxList;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to confirm Religare Insurance Markup Details: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Adding Passenger Details.
        /// </summary>
        private void Add_PassengerDetails()
        {
            try
            {
                ReligareHeader religareHeader = (ReligareHeader)Session["religareHeader"];
                List<ReligarePassengers> obj_liPassengers = religareHeader.ReligarePassengers;
                TableRow tr;
                TableRow paxtr;
                Table table;
                Label lbl;
                TableCell tableCell;
                TableCell tc;
                string[] lblcontrol = "Name,Passport,Phone,Address,Email".Split(',');
                for (int i = 0; i <= religareHeader.NoofPax; i++)
                {
                    tr = new TableRow();
                    tableCell = new TableCell();
                    tableCell.Text = i==0  ? "Proposer" : "Passenger : " + Convert.ToInt32(i); 
                    tableCell.Font.Bold = true;
                    tr.Cells.Add(tableCell);
                    tableCell = new TableCell();

                    table = new Table();
                    table.CssClass = "table";
                    table.Width = Unit.Percentage(100);
                    table.ID = "pax" + i;
                    for (int j = 0; j < 5; j++)
                    {
                        paxtr = new TableRow();
                        tc = new TableCell();
                        tc.Text = lblcontrol[j];
                        paxtr.Cells.Add(tc);

                        lbl = new Label();
                        lbl.ID = "pax" + lblcontrol[j] + i;
                        if (lblcontrol[j] == "Name")
                        {
                            lbl.Text = obj_liPassengers[i].FirstName + " " + obj_liPassengers[i].LastName;
                            lbl.Font.Bold = true;
                        }
                        else if (lblcontrol[j] == "Passport")
                            lbl.Text = obj_liPassengers[i].PassportNo;
                        else if (lblcontrol[j] == "Phone")
                            lbl.Text = obj_liPassengers[0].Mobileno;
                        else if (lblcontrol[j] == "Address")
                            lbl.Text = obj_liPassengers[0].Address1 + " , " + obj_liPassengers[0].Address2;
                        else if (lblcontrol[j] == "Email")
                            lbl.Text = obj_liPassengers[0].Email;
                        tc = new TableCell();
                        tc.Controls.Add(lbl);
                        paxtr.Cells.Add(tc);
                        table.Rows.Add(paxtr);
                        tableCell.Controls.Add(table);
                    }
                    tr.Cells.Add(tableCell);
                    tblPassengerDetails.Rows.Add(tr);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Payment Confirmation failed to Adding Passenger Details:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Editing the Quotation Details.Navigating to ReligareInsurance.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["quotation"] != null)
                {
                    Response.Redirect("ReligareInsurance.aspx",false);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Payment Confirmation failed to Navigate the Insurance Page :" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Editing The passenger Details and navigating to ReligareProposerDetails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtnPaxEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["religareHeader"] != null)
                {
                    Response.Redirect("ReligareProposerDetails.aspx",false);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Payment Confirmation failed to navigate the proposer Details:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        protected void btnPayment_Click(object sender, EventArgs e)
        {
            try
            {
                string subject = string.Empty;
                if (Session["religareHeader"] != null)
                {
                    AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                    decimal currentAgentBalance = agent.UpdateBalance(0);
                    // Saving Religare Header Details
                    header = (ReligareHeader)Session["religareHeader"];
                    //header.Premium_Amount = Convert.ToDecimal(lblMarkupTotal.Text);
                    //header.DiscountValue = (Convert.ToDecimal(lblMarkupTotal.Text) * header.Discount) / 100;
                    header.Policy_no = "";
                    header.Voucher_status = "P";
                    if (header.Agent_id != Settings.LoginInfo.AgentId)
                    {
                        agent = new AgentMaster(header.Agent_id);
                        currentAgentBalance = agent.UpdateBalance(0);
                        header.Currency = agent.AgentCurrency;
                    }
                    header.Save();
                    if (header.Header_id > 0)
                    {
                        // Saving GST Details.
                        List<GSTTaxDetail> gstTaxList = (List<GSTTaxDetail>)Session["gstTaxList"];
                        if (gstTaxList != null)
                        {
                            foreach (GSTTaxDetail taxdetail in gstTaxList)
                            {
                                taxdetail.PriceId = header.Header_id;
                                taxdetail.ProductID = (int)ProductType.Insurance;
                                taxdetail.Save();
                            }
                        }
                        int headerId = header.Header_id;
                        // Saving Insurance Details
                        Invoice invoice = new Invoice();
                        if (currentAgentBalance > Convert.ToDecimal(lblTxnAmount.Text))
                        {
                            MetaSearchEngine mse = new MetaSearchEngine();
                            if (header.Agent_id != Settings.LoginInfo.AgentId)
                            {
                                agent = new AgentMaster(header.Agent_id);
                                Settings.LoginInfo.IsOnBehalfOfAgent = true;
                                Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                                Settings.LoginInfo.DecimalValue = agent.DecimalValue;
                                Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(header.Location_id);
                                Settings.LoginInfo.OnBehalfAgentID = header.Agent_id;
                                Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);
                                Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                            }
                            else
                            {
                                Settings.LoginInfo.IsOnBehalfOfAgent = false;
                            }
                            mse.SettingsLoginInfo = Settings.LoginInfo;
                            header = Session["religareHeader"] as ReligareHeader;
                            //Proposal Number Request
                            var proposalresponse = mse.ConfirmReligarePolicyPurchase(ref header);
                            if (proposalresponse != null)
                            {
                                string proposalresp = proposalresponse.ToString();
                                string regex = @"\<proposal-num\>[0-9]{0,15}\<\/proposal-num\>";
                                var proposalData = Regex.Match(proposalresp, regex);
                                string proposalNo = proposalData.ToString().Replace("<proposal-num>", "").Split('<')[0];
                                regex = @"\<premium\>[0-9,\.]{0,15}\<\/premium\>";
                                proposalData = Regex.Match(proposalresp, regex);
                                string premium = proposalData.ToString().Replace("<premium>", "").Split('<')[0];
                                if (!(string.IsNullOrEmpty(proposalNo)))
                                {
                                    //Policy Number Request
                                    var policyresponse = mse.ConfirmReligarepaymentService(ref proposalNo);
                                    proposalresp = policyresponse.ToString();
                                    regex = @"\<policy-num\>[0-9]{0,15}\<\/policy-num\>";
                                    proposalData = Regex.Match(proposalresp, regex);
                                    string policyNo = proposalData.ToString().Replace("<policy-num>", "").Split('<')[0];
                                    if (!string.IsNullOrEmpty(policyNo))
                                    {
                                        //Updating Religare Header Status
                                        header.Status = (int)InsuranceBookingStatus.Confirmed;
                                        header.Policy_no = policyNo;
                                        header.Agency_ref = proposalNo;
                                        header.updateStatus();
                                        //Invoice 
                                        int invoiceNumber = 0;
                                        try
                                        {
                                            invoice.AgencyId = header.Agent_id;
                                            invoice.Status = CT.BookingEngine.InvoiceStatus.PartiallyPaid;
                                            invoice.PaymentMode = "";
                                            invoice.Remarks = "Religare";
                                            invoice.StaffRemarks = "Religare";
                                            invoice.AgentBalance = Settings.LoginInfo.AgentBalance;
                                            invoice.CreatedBy = (Int32)Settings.LoginInfo.UserID;
                                            invoice.DocTypeCode = "RG";
                                            invoice.InvoiceDueDate = DateTime.Now.AddDays(1);
                                            invoice.TotalPrice = Convert.ToDecimal(lblMarkupTotal.Text);
                                            invoice.TranxHeaderId = header.Header_id;
                                            List<InvoiceLineItem> li_lineItem = new List<InvoiceLineItem>();
                                            InvoiceLineItem lineItem;
                                            foreach (ReligarePassengers pax in header.ReligarePassengers)
                                            {
                                                lineItem = new InvoiceLineItem();
                                                PriceAccounts accounts = new PriceAccounts();
                                                accounts.PriceId = Convert.ToInt32(header.Header_id);
                                                lineItem.Price = accounts;
                                                lineItem.ItemReferenceNumber = header.Header_id;
                                                lineItem.ItemTypeId = (int)ProductType.Insurance;
                                                lineItem.ItemDescription = "RG";
                                                li_lineItem.Add(lineItem);
                                            }
                                            invoice.LineItem = li_lineItem;
                                            invoice.Save(ProductType.Insurance, false);
                                            header.Header_id = headerId;
                                            invoiceNumber = CT.BookingEngine.Invoice.isReligareInvoiceGenerated(header.Header_id, CT.BookingEngine.ProductType.Insurance, "RG");
                                            if (invoiceNumber > 0)
                                            {
                                                invoice.Load(invoiceNumber);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                                            toArray.Add(header.ReligarePassengers[0].Email);
                                            AgentMaster agency = new AgentMaster(header.Agent_id);
                                            string bccEmails = string.Empty;
                                            if (!string.IsNullOrEmpty(agency.Email1))
                                            {
                                                bccEmails = agency.Email1;
                                            }
                                            if (!string.IsNullOrEmpty(agency.Email2))
                                            {
                                                bccEmails = bccEmails + "," + agency.Email2;
                                            }
                                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Religare Insurance failed for - " + policyNo, ex.ToString(), new Hashtable(), bccEmails);
                                            Response.Redirect("ErrorPage.aspx?ReligareError=Failed to Loading the Invoice Details", false);
                                        }
                                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(header.Header_id, "RG", (int)Settings.LoginInfo.UserID, CT.BookingEngine.ProductType.Insurance, 1);
                                        if (invoiceNumber > 0)
                                        {
                                            invoice.Load(invoiceNumber);
                                            invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                            invoice.UpdateInvoice();
                                        }
                                        // adding the booking transaction in ledger.
                                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                                        NarrationBuilder objNarration = new NarrationBuilder();
                                        invoiceNumber = Invoice.isInvoiceGenerated(header.Header_id, ProductType.Insurance);
                                        ledgerTxn.ReferenceId = (int)ProductType.Insurance;
                                        ledgerTxn.LedgerId = header.Agent_id;
                                        ledgerTxn.ReferenceType = ReferenceType.InsuranceCreated;
                                        ledgerTxn.Amount -= Convert.ToDecimal(lblMarkupTotal.Text);
                                        objNarration.PolicyNo = header.Policy_no;
                                        objNarration.DocNo = "RG" + invoiceNumber.ToString();
                                        objNarration.TravelDate = Convert.ToDateTime(header.Startdate).ToString("dd/MMM/yyyy");
                                        objNarration.Remarks = "Religare Insurance Booking Policy No " + policyNo + " created.";
                                        objNarration.PaxName = string.Format("{0} {1}", header.ReligarePassengers[0].FirstName, header.ReligarePassengers[0].LastName).ToUpper();
                                        ledgerTxn.Narration = objNarration;
                                        ledgerTxn.IsLCC = true;
                                        ledgerTxn.ReferenceId = header.Header_id;
                                        ledgerTxn.Notes = "";
                                        ledgerTxn.Date = DateTime.UtcNow;
                                        ledgerTxn.CreatedBy = header.Created_by;
                                        ledgerTxn.Save();
                                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);
                                        //Deduct the premium amount from the Agent Balance and display vouhcer
                                        currentAgentBalance = agent.UpdateBalance(-Convert.ToDecimal(lblTxnAmount.Text));
                                        Settings.LoginInfo.AgentBalance = currentAgentBalance;
                                        Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                                        lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);

                                        Response.Redirect("ReligareInsuranceVocher.aspx?InsId=" + header.Header_id, false);
                                    }
                                    else
                                    {
                                        regex = @"\<err-description\>.*<\/err-description\>";
                                        proposalData = Regex.Match(proposalresp, regex);
                                        lblMessage.InnerText = proposalData.ToString().Replace("<err-description>", "").Split('<')[0];
                                        // Sending Mail when Policy Number Not generated.
                                        subject = "Failed To Generating the Policy Number";
                                        SendReligareInsuranceMail(header.Header_id, subject, lblMessage.InnerText);
                                        //lblMessage.InnerText = string.IsNullOrEmpty(proposalNo) ? "Failed to generating the Proposal Number" : "Failed to generating the Policy Number (Insufficient Balance)";
                                        Response.Redirect("ErrorPage.aspx?ReligareError=" + lblMessage.InnerText, false);
                                    }
                                }
                                else
                                {
                                    regex = @"\<err-description\>.*<\/err-description\>";
                                    proposalData = Regex.Match(proposalresp, regex);
                                    lblMessage.InnerText = proposalData.ToString().Replace("<err-description>", "").Split('<')[0];
                                    // Sending Mail when Policy Number Not generated.
                                    subject = "Failed To Generating the Proposal Number";
                                    SendReligareInsuranceMail(header.Header_id, subject, lblMessage.InnerText);
                                    lblMessage.InnerText = string.IsNullOrEmpty (lblMessage.InnerText)?"Failed to generating the Proposal Number":lblMessage.InnerText;
                                    Response.Redirect("ErrorPage.aspx?ReligareError=" + lblMessage.InnerText, false);
                                }
                            }
                            else
                            {
                                subject = "Server Connection Failure";
                                // Sending Mail when Proposal Number Not generated.
                                SendReligareInsuranceMail(header.Header_id, subject, lblMessage.InnerText);
                                Response.Redirect("ErrorPage.aspx?ReligareError=Server 404 not found.", false);
                            }
                        }
                        else
                        {
                            lblBalanceError.Text = "Insufficient Credit Balance ! please contact Admin";
                            subject = "Insufficient Credit Balance";
                            // Sending Mail when Agent balance is low.                       
                            SendReligareInsuranceMail(header.Header_id, subject, lblBalanceError.Text);
                            Response.Redirect("ErrorPage.aspx?ReligareError=" + lblBalanceError.Text, false);
                        }
                    }
                    else
                    {
                        SendReligareInsuranceMail(header.Header_id, subject, "Failed to saving the Insured Details");
                        Response.Redirect("ErrorPage.aspx?ReligareError=Failed to saving the Insured Details.", false);
                    }
                }
                else
                {
                    string errorMessage = "Insured Details Can not be null";
                    subject = "Session Timeout for Insured Details";
                    // Sending Mail when Insured Details Null.    
                    SendReligareInsuranceMail(header.Header_id, subject, errorMessage);
                    Response.Redirect("ErrorPage.aspx?ReligareError=" + errorMessage, false);
                }
            }
            catch (Exception ex)
            {
                SendReligareInsuranceMail(header.Header_id, "Failed to saving the Insured Details",ex.Message);
                Response.Redirect("ErrorPage.aspx?ReligareError=" + ex.Message, false);
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to confirm Religare Insurance: " + ex.ToString(), Request["REMOTE_ADDR"]);
                ////Session["quotation"] = null;
                ////Session["religareHeader"] = null;
            }
        }
        //taking the currency value 
        protected string CTCurrencyFormat(object currency, object AgentId)
        {
            AgentMaster agency;
            if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
            {
                agency = new AgentMaster(Convert.ToInt32(AgentId));
                return agency.AgentCurrency + " " + Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
            }
            else
            {
                agency = new AgentMaster(Convert.ToInt32(AgentId));
                return agency.AgentCurrency + " " + Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
            }
        }

        private void SendReligareInsuranceMail(int headerId, string subject, string exMsg)
        {           
            string messageText = string.Empty;
            messageText += "Dear Team,<br />";
            messageText += "Following Error Occured :" + exMsg;
            if (headerId > 0)
                messageText += "<br />please refer the Religare Header ID : " + headerId;
            try
            {               
                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"],CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], subject, messageText);
            }
            catch (System.Net.Mail.SmtpException excep)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "SMTP Exception returned from Purchasing the religare policy. Error Message:" + excep.Message + " | " + DateTime.Now.ToString(), "");
            }
        }
    }
}
