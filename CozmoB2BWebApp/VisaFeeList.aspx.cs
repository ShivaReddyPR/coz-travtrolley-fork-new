﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using Visa;
using System.Collections.Generic;

public partial class VisaFeeList : CT.Core.ParentPage
{
    protected bool isAdmin = true;
    protected void Page_Load(object sender, EventArgs e)
    {

        Page.Title = "Visa Type List";
        this.Master.PageRole = true;
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            isAdmin = true;
        }
        try
        {
             AuthorizationCheck();   //commented by vijesh 12-02-2015
            if (!IsPostBack)
            {
                BindAgent();
                
                ddlCountry.DataSource = VisaCountry.GetActiveVisaCountryList();
                ddlCountry.DataTextField = "countryName";
                ddlCountry.DataValueField = "countryCode";
                ddlCountry.DataBind();                
                ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));

                ddlNationality.DataSource = VisaNationality.GetNationalityList(); 
                ddlNationality.DataTextField = "nationalityName";
                ddlNationality.DataValueField = "nationalityCode";
                ddlNationality.DataBind();
                ddlNationality.Items.Insert(0, new ListItem("Select Nationality", "-1"));
                ddlNationality.Items.Insert(1, new ListItem("All", "0"));

                ddlVisaType.Items.Clear();
                ddlVisaType.Items.Insert(0, new ListItem("Select Visa Type", "0"));

                if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
                {
                    ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Request.QueryString[0].Trim()));
                    ddlAgent.SelectedIndex = ddlAgent.Items.IndexOf(ddlAgent.Items.FindByValue(Request.QueryString[1].Trim()));
                    ddlCountry_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlCountry.SelectedIndex = 1;
                    ddlAgent.SelectedIndex = 0;
                    if (!isAdmin)
                    {
                        ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                        ddlAgent.Enabled = false;
                    }
                }

                BindData();
            }
        }
        catch { throw; }
    }
    private void BindAgent()
    {
        try
        {
            //int agentId = 0;
            //if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            //string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            //ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = AgentMaster.GetB2CAgentList();
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private void BindData()
    {
        List<VisaFee> allVisaFee = VisaFee.GetVisaFeeList(ddlCountry.SelectedValue,Convert.ToInt32(ddlAgent.SelectedItem.Value),
            Convert.ToInt32(ddlVisaType.SelectedItem.Value), ddlNationality.SelectedItem.Value);
        if (allVisaFee == null || allVisaFee.Count == 0)
        {
            lbl_msg.Text = "There are no record.";
            lbl_msg.Visible = true;
        }
        else
        {
            lbl_msg.Visible = false;

        }
                
        GridView1.DataSource = allVisaFee;
        GridView1.DataBind();

    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = ((LinkButton)e.Row.Cells[10].Controls[1]);
            lb.CommandArgument = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            if (e.Row.Cells[8].Text == "True")
            {
                lb.Text = "Deactivate";
                e.Row.Cells[8].Text = "Active";
            }
            else
            {
                lb.Text = "Activate";
                e.Row.Cells[8].Text = "Inactive";
            }
        }

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChangeStatus")
        {
            bool isActive;
            string status = ((LinkButton)e.CommandSource).Text;
            int rowNumber = Convert.ToInt32(e.CommandArgument);
            // int i = Convert.ToInt32(GridView1.DataKeys[rowNumber].Value);
            if (status == "Activate")
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            VisaFee.ChangeStatus(rowNumber, isActive);
            BindData();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlCountry.SelectedItem.Value!="-1")
        {
            ddlVisaType.Items.Clear();
            ddlVisaType.DataSource = VisaType.GetVisaTypeList(ddlCountry.SelectedItem.Value, Convert.ToInt32(ddlAgent.SelectedValue));
            ddlVisaType.DataTextField = "visaTypeName";
            ddlVisaType.DataValueField = "visaTypeId";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("Select Visa Type", "0"));            
        }
        else
        {
            ddlVisaType.Items.Clear();
            ddlVisaType.Items.Insert(0, new ListItem("Select Visa Type", "0"));
        }
        
    }
}
