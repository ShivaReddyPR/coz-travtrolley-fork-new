﻿using System;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using System.Globalization;
using System.Collections.Generic;
public partial class FleetSearch : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            Clear();
            if (!IsPostBack)
            {
                List<CT.BookingEngine.FleetCity> cityList = new List<CT.BookingEngine.FleetCity>();
                List<CT.BookingEngine.FleetLocation> locationList = new List<CT.BookingEngine.FleetLocation>();
                if (Session["CityList"] == null && Session["LoactionList"] == null)
                {
                    Sayara.SayaraApi sayarRestApi = new Sayara.SayaraApi();
                    //Loading Citi and Location list
                    cityList = sayarRestApi.GetCityList(ref locationList);

                    if (cityList != null && cityList.Count > 0 && locationList != null && locationList.Count > 0)
                    {
                        //storing all cities and locations in session
                        Session["CityList"] = cityList;
                        Session["LoactionList"] = locationList;
                    }
                }
                else
                {
                    cityList = Session["CityList"] as List<CT.BookingEngine.FleetCity>;
                }

                //binding pickup and dropup cities
                foreach (CT.BookingEngine.FleetCity city in cityList)
                {
                    ListItem PickupCity = new ListItem(city.CityName, city.CityCode);
                    ListItem ReturnCity = new ListItem(city.CityName, city.CityCode);
                    ddlPickupCity.Items.Add(PickupCity);
                    ddlReturnCity.Items.Add(ReturnCity);
                }

            }
            //post back time ccalling javascript
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "selectReturnLocation();", "Script");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetSearch, Severity.High, 0, "FleetSearch page " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Clear(); //Clear all sessions
            //Creating request object
            FleetRequest fleetReq = new FleetRequest();
            string fromDatetime = CheckIn.Text.Trim() + " " + ddlPickuptime.SelectedItem.Value;
            string toDatetime = CheckOut.Text.Trim() + " " + ddlReturntime.SelectedItem.Value;
            fleetReq.FromCity = ddlPickupCity.SelectedItem.Text;
            fleetReq.FromCityCode = ddlPickupCity.SelectedItem.Value;
            fleetReq.FromLocation = ddlPickupLocation.SelectedItem.Text;
            fleetReq.FromLocationCode = ddlPickupLocation.SelectedItem.Value;
            IFormatProvider format = new CultureInfo("en-GB", true);
            fleetReq.FromDateTime = DateTime.Parse(fromDatetime, format);
            fleetReq.ToDateTime = Convert.ToDateTime(toDatetime,format);
            //selected diffrent city
            if (chkRetunLocation.Checked)
            {
                fleetReq.ToCity = ddlReturnCity.SelectedItem.Text;
                fleetReq.ToCityCode = ddlReturnCity.SelectedItem.Value;
                fleetReq.ToLocation = ddlReturnLocation.SelectedItem.Text;
                fleetReq.ToLocationCode = ddlReturnLocation.SelectedItem.Value;
                fleetReq.ReturnStatus = "Y";
            }
            else
            {

                fleetReq.ReturnStatus = "N";
            }
            if (ddlCarType.SelectedIndex > 0)
            {
                fleetReq.CarType = ddlCarType.SelectedItem.Value;
            }
            else
            {
                fleetReq.CarType = string.Empty;
            }
            //Storing request object in session
            Session["FleetRequest"] = fleetReq;
            //navigating results page
            Response.Redirect("FleetResults.aspx", false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetSearch, Severity.High, 0, "FleetSearch page " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void Clear()
    {
        Session["filteredResultList"] = null;
        Session["searchResult"] = null;
        Session["fleetItinerary"] = null;
        Session["FleetRequest"] = null;
        Session["BookingMade"] = null;
    }

    protected void ddlPickupCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            List<CT.BookingEngine.FleetCity> cityList = new List<CT.BookingEngine.FleetCity>();
            List<CT.BookingEngine.FleetLocation> locationList = new List<CT.BookingEngine.FleetLocation>();
            if (Session["LoactionList"] == null)
            {
                //Loading Citi and Location list
                Sayara.SayaraApi sayarRestApi = new Sayara.SayaraApi();
                cityList = sayarRestApi.GetCityList(ref locationList);
                if (cityList != null && cityList.Count > 0 && locationList != null && locationList.Count > 0)
                {
                    Session["CityList"] = cityList;
                    Session["LoactionList"] = locationList;
                }
            }
            else
            {
                locationList = Session["LoactionList"] as List<CT.BookingEngine.FleetLocation>;
            }
            ddlPickupLocation.Items.Clear();
            ddlPickupLocation.Items.Add(new ListItem("Select Location", "0"));
            //binding pickupLocation based on pickup city
            if (locationList != null)
            {
                foreach (FleetLocation location in locationList)
                {
                    if (ddlPickupCity.SelectedItem.Value.ToUpper() == location.CityCode.ToUpper())
                    {
                        ListItem pickupLocation = new ListItem(location.LocationName, location.LocationCode);
                        ddlPickupLocation.Items.Add(pickupLocation);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Exception Loading PickupLocation In FleetSearch Page. Message:" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void ddlReturnCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            List<CT.BookingEngine.FleetCity> cityList = new List<CT.BookingEngine.FleetCity>();
            List<CT.BookingEngine.FleetLocation> locationList = new List<CT.BookingEngine.FleetLocation>();
            if (Session["LoactionList"] == null)
            {
                //Loading City and Location list
                Sayara.SayaraApi sayarRestApi = new Sayara.SayaraApi();
                cityList = sayarRestApi.GetCityList(ref locationList);
                if (cityList != null && cityList.Count > 0 && locationList != null && locationList.Count > 0)
                {
                    Session["CityList"] = cityList;
                    Session["LoactionList"] = locationList;
                }
            }
            else
            {
                locationList = Session["LoactionList"] as List<CT.BookingEngine.FleetLocation>;
            }
            ddlReturnLocation.Items.Clear();
            ddlReturnLocation.Items.Add(new ListItem("Select Location", "0"));
            //binding drop location baed on dropcity
            if (locationList != null)
            {
                foreach (FleetLocation location in locationList)
                {
                    if (ddlReturnCity.SelectedItem.Value.ToUpper() == location.CityCode.ToUpper())
                    {
                        ListItem returnLocation = new ListItem(location.LocationName, location.LocationCode);
                        ddlReturnLocation.Items.Add(returnLocation);
                    }
                }
            }
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Exception Loading returnLocation In FleetSearch Page. Message:" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    
}
