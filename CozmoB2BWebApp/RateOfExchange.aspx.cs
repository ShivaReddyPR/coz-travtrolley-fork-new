﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class RateOfExchangeGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected DataTable dtCurrency;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
            else
            {
                BindSearch();
            }
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }

    private void InitializePageControls()
    {
        try
        {
            ddlCurrency.SelectedIndex = -1;
            BindCurrency();
        }
        catch { throw; }
    }

    private void BindCurrency()
    {
        try
        {
            ddlCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCurrency.DataValueField = "CURRENCY_CODE";
            ddlCurrency.DataTextField = "CURRENCY_CODE";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, new ListItem("--Select Currency--", "0"));
        }
        catch { throw; }
    }

    protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            tblRateExchange.Controls.Clear();
            BindSearch();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }

    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        tblRateExchange.Controls.Clear();
    //        BindSearch();
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
    //    }
    //}

    private void BindSearch()
    {
        try
        {
            if (ddlCurrency.SelectedIndex > 0)
            {
                Session["SearchData"] = null;
                dtCurrency = ExchangeRateMaster.GetListByCurrnctCode(ddlCurrency.SelectedItem.Text);
                Session["SearchData"] = dtCurrency;
                HtmlTableRow hr = new HtmlTableRow();
                for (int i = 0; i < dtCurrency.Columns.Count - 1; i++)
                {
                    Label lblEmpty = new Label();
                    lblEmpty.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    Label lblheader = new Label();
                    lblheader.Text = dtCurrency.Columns[i].ColumnName;
                    HtmlTableCell tc = new HtmlTableCell();
                    tc.Width = "50px";
                    tc.Attributes["style"] = "color:black; font-weight:bold; border: none;background-color: #ccc;  height: 1px; left:20px;";
                    if (i == 0 || i == 2)
                    {
                        tc.Controls.Add(lblEmpty);
                    }
                    tc.Controls.Add(lblheader);
                    hr.Cells.Add(tc);
                }
                tblRateExchange.Rows.Add(hr);
                for (int j = 0; j < dtCurrency.Rows.Count; j++)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    for (int k = 0; k < dtCurrency.Columns.Count - 1; k++)
                    {
                        if (k == 2)
                        {
                            Label lblEqual = new Label();
                            lblEqual.Text = "=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            Label lblBase = new Label();
                            lblBase.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ddlCurrency.SelectedItem.Text;
                            if (dtCurrency.Rows[j][k - 2].ToString() != ddlCurrency.SelectedItem.Text)
                            {
                                TextBox txt = new TextBox();
                                txt.ID = "txtRate_" + j.ToString() + "_" + k.ToString();
                                txt.Text = dtCurrency.Rows[j][k].ToString();
                                if (txt.Text == string.Empty) txt.Text = "0.000";
                                txt.Width = new Unit(60, UnitType.Pixel);
                                txt.Attributes.Add("onkeypress", "return isNumber(event);");
                                txt.Attributes.Add("onfocus", "Check(this.id);");
                                txt.Attributes.Add("onBlur", "Set(this.id);");
                                txt.MaxLength = 30;
                                HtmlTableCell td = new HtmlTableCell();
                                td.Controls.Add(lblEqual);
                                td.Controls.Add(txt);
                                td.Controls.Add(lblBase);
                                td.Width = "30px";
                                tr.Cells.Add(td);
                            }
                            else
                            {
                                Label lbl = new Label();
                                lbl.Text = "1.000";
                                lbl.Width = new Unit(60, UnitType.Pixel);
                                HtmlTableCell td = new HtmlTableCell();
                                td.Controls.Add(lblEqual);
                                td.Controls.Add(lbl);
                                td.Controls.Add(lblBase);
                                td.Width = "30px";
                                tr.Cells.Add(td);
                            }
                        }
                        else
                        {
                            Label lblOne = new Label();
                            lblOne.Text = "1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            Label lbl = new Label();
                            lbl.Text = dtCurrency.Rows[j][k].ToString();
                            HtmlTableCell td = new HtmlTableCell();
                            if (k == 0) td.Controls.Add(lblOne);
                            td.Controls.Add(lbl);
                            td.Width = "30px";
                            tr.Cells.Add(td);

                        }
                    }
                    tblRateExchange.Rows.Add(tr);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["SearchData"] != null)
            {
                dtCurrency = Session["SearchData"] as DataTable;
            }
            int count = 0;
            for (int i = 0; i < dtCurrency.Rows.Count; i++)
            {
                List<string> rates = new List<string>();
                for (int j = 0; j < dtCurrency.Columns.Count-1; j++)
                {
                    if (j == 2)
                    {
                        if (dtCurrency.Rows[i][j - 2].ToString() != ddlCurrency.SelectedItem.Text)
                        {
                            TextBox rate = tblRateExchange.Rows[i].Cells[j].FindControl("txtRate_" + i.ToString() + "_" + j.ToString()) as TextBox;
                            if (rate.Text != dtCurrency.Rows[i][j].ToString() && rate.Text!="0.000")
                            {
                                ExchangeRateMaster rateExchange = new ExchangeRateMaster();
                                if (dtCurrency.Rows[i][j + 1].ToString() != string.Empty)
                                {
                                    rateExchange.Id = Utility.ToLong(dtCurrency.Rows[i][j + 1]);
                                }
                                rateExchange.CurrencyCode = dtCurrency.Rows[i][j-2].ToString();
                                rateExchange.Rate = Utility.ToDecimal(rate.Text);
                                rateExchange.BaseCurrencyCode = ddlCurrency.SelectedItem.Text;
                                rateExchange.Status = Settings.ACTIVE;
                                rateExchange.CreatedBy = Settings.LoginInfo.UserID;
                                rateExchange.Remarks = "";
                                rateExchange.Save();
                                if (count == 0)
                                {
                                    count = 1;
                                }
                                else
                                {
                                    count++;
                                }
                            }
                        }
                    }

                }
            }
            lblSuccessMsg.Text = count + " Records Updated";
            tblRateExchange.Controls.Clear();
            ddlCurrency.SelectedIndex = -1;
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


}
