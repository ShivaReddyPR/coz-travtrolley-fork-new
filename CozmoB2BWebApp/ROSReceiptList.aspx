<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSReceiptListUI" Title="ROS Receipt List"  ValidateRequest="false" EnableEventValidation="false" Codebehind="ROSReceiptList.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc2" %>
<%--<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphtransaction" Runat="Server">
<%--<div id="divSearchbar" class="content" style="height:456px;width:100%;" >--%>
<asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label" >
            <tr>
                    <td style="width:100px" align="right"><a style="cursor:Hand;font-weight:bold;font-size:8pt;color:Black;" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a></td>
          </tr>
         </table>
    <div>
    
    
    <div class="paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
        
        
        
        
             <div class="col-md-12 padding-0 marbot_10">     
             
                
 <div class="col-md-2"><asp:Label ID="lblFromDate" CssClass="pull-right fl_xs" Text="From Date:" runat="server"></asp:Label> </div>
 
 
    <div class="col-md-2"><uc1:DateControl Id="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblToDate" CssClass="pull-right fl_xs" Text="To Date:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><uc1:DateControl Id="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
     
     
 
<div class="clearfix"></div>
    </div>
    
    
    
    
    
     <div class="col-md-12 padding-0 marbot_10">             
    
    
                                                 
   
 <div class="col-md-2"><asp:Label CssClass="pull-right fl_xs" ID="lblAgent" runat="server" Text="Agent:"></asp:Label> </div>
 
 
    <div class="col-md-2"> <asp:DropDownList ID="ddlAgent"  AutoPostBack="true"  OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" CssClass="inputDdlEnabled form-control" runat="server" ></asp:DropDownList></div>
    
    
    <div class="col-md-2"><asp:Label Visible="false" CssClass="pull-right fl_xs" ID="lblSubAgent" runat="server" Text="B2B Agent:"></asp:Label><asp:Label ID="lblLocation" CssClass="pull-right fl_xs" runat="server" Text="Location:"></asp:Label> </div>
    
    
     <div class="col-md-2"> <asp:DropDownList Visible="false" ID="ddlB2BAgent" AutoPostBack="true"  CssClass="inputDdlEnabled form-control"  runat="server"></asp:DropDownList><asp:DropDownList ID="ddlLocation" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList></div>
     



    
    <div class="clearfix"></div>
    </div>
    
       
       
       <div> 
        
          <div class="col-md-2"> <asp:Label Visible="false" CssClass="pull-right fl_xs"  ID="lblB2B2BAgent" runat="server" Text="B2B2B Agent:"></asp:Label></div>
     
         <div class="col-md-2"><asp:DropDownList Visible="false" ID="ddlB2B2BAgent" AutoPostBack="true"  CssClass="inputDdlEnabled form-control"  runat="server"></asp:DropDownList> </div>
         
         </div>
         
    
         <div class="col-md-12 padding-0 marbot_10">                                      

 
     


<div class="clearfix"></div>



    </div>
    
    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblSettlementMode" CssClass="pull-right fl_xs" runat="server" Text="Sett. Mode:"></asp:Label></div>
 
    <div class="col-md-2"> <asp:DropDownList ID="ddlSettlementMode" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList></div>
   
    <div class="col-md-2"> <asp:Label ID="lblStatus" CssClass="pull-right fl_xs" runat="server" Text="Status:"></asp:Label></div>
    
     <div class="col-md-2"> <asp:DropDownList ID="ddlStatus"  CssClass="inputDdlEnabled form-control"  runat="server">
     
               <asp:ListItem Text="Active" Value="A" Selected="True"></asp:ListItem>
               <asp:ListItem Text="Cancelled" Value="D"></asp:ListItem>
               </asp:DropDownList></div>
    
   
       
       
         <div class="col-md-4 martop_xs10">
         <asp:Button runat="server" ID="btnSearch" Text="Search"  OnClientClick="return ValidateParam();" CssClass="btn but_b pull-right" OnClick="btnSearch_Click" />
         
          </div>

<div class="clearfix"></div>
    </div>
    
    
        
        
      
        
        </asp:Panel>


    </div>

<div class="grdScrlTrans"> 

    <table id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <%--<tr>    
    <td align="left" style="width:100%" class="titlebarSearch"><asp:Label ID="lblTitleSearch" runat="server" Text="Search"></asp:Label></td>
    </tr>
    <tr>--%>
    <tr>
    <td>
     <div title="Delete" id="divDelete" style="position:absolute;display:none;">
        <table style="border:solid 1px skyblue;background-color:White" border="0" >
        <tr >
            <td valign="top" align="right"><asp:Label ID="lblRemarks" Text="Remarks:" runat="server" CssClass="label" ></asp:Label></td>
            <td><asp:TextBox ID="txtRemarks" TextMode="multiline" Height="45" width="150px" runat="server" CssClass="inputEnabled" ></asp:TextBox> </td>
        </tr>
        <tr>
        <td colspan="2" align="right" valign="top">
                <asp:Button ID="btnOk" runat="server" CssClass="button" Text="Ok" Width="60px" OnClientClick="return validateRemarks();" OnClick="btnOk_Click" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" Width="60px" OnClientClick="return HideDelete();" />
          </td>
         </tr>
        </table>
    </div>
    <asp:HiddenField ID="hdfReceiptId" runat="server" ></asp:HiddenField>
    <asp:HiddenField ID="hdfOK2BStatus" runat="server" ></asp:HiddenField>
    <asp:GridView ID="gvReceipt" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="RECEIPT_ID" 
    EmptyDataText="No Receipt List!" AutoGenerateColumns="false" PageSize="20" GridLines="none"  CssClass="grdTable"
     CellPadding="4" CellSpacing="0" PagerSettings-Mode="NumericFirstLast"
    OnPageIndexChanging="gvReceipt_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvRow01" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvRow02" />    
    <Columns> 
    
    <%-- <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    
    <HeaderTemplate>
   
    </HeaderTemplate>
    <ItemStyle  />
   
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label"  Checked='<%# Eval("RECEIPT_ACCOUNTED_STATUS").ToString()=="Y"%>' Enabled='<%# Eval("RECEIPT_ACCOUNTED_UPDATED").ToString()!="U" && Eval("RECEIPT_STATUS").ToString()=="A" %>'></asp:CheckBox>
    </ItemTemplate>    
    </asp:TemplateField>   --%>
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label grdof" ToolTip='<%# Container.DataItemIndex %>' Width="40px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>  
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <label style=" float:left"> Receipt Number</label>             
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptNo" runat="server" Text='<%# Eval("receipt_number") %>' CssClass="label grdof" ToolTip='<%# Eval("receipt_number") %>'></asp:Label>
    <asp:HiddenField id="IThdfReceiptId" runat="server" Value='<%# Bind("receipt_id") %>'></asp:HiddenField>   
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
    <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
   <%-- <table>    
    <tr>
    
    <td>
    <uc1:DateControl ID="HTtxtReceiptDate" runat="server" DateOnly="true" />
    </td>
    <td>
    <asp:ImageButton ID="HTbtnReceiptDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="Filter_Click" />
    </td>
    <td>
    <asp:ImageButton ID="HTbtnChequeReturnDateAdvance"  runat="server" ImageUrl="~/Images/wg_filterAdvance.GIF"  ImageAlign="AbsMiddle"  OnClientClick="return validateAdvanceFilter(this.id,'A');" Alt="Advance Filter" /></td>
    </tr></table>--%>
    <label style="width:140px; float:left" class="filterHeaderText">Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptDate" runat="server" Text='<%# IDDateTimeFormat(Eval("receipt_date")) %>' CssClass="label grdof pull-left"  ToolTip='<%# Eval("receipt_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>  
   
   <asp:TemplateField><HeaderTemplate>
    <label class="pull-left">Company Name </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    
    
    <ItemTemplate>
    <asp:Label ID="ITlblCompanyName" runat="server" Text='<%# Eval("RECEIPT_COMPANY_NAME") %>' CssClass="label grdof pull-left"  ToolTip='<%# Eval("RECEIPT_COMPANY_NAME") %>'></asp:Label>                
    </ItemTemplate>    
    
    
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <label style=" float:left">Pax Name </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblPaxName" runat="server" Text='<%# Eval("receipt_pax_name") %>' CssClass="label grdof pull-left"  ToolTip='<%# Eval("receipt_pax_name") %>'></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate>
    
    <label style=" float:left">Status</label>               
    </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("receipt_status_name") %>' ToolTip='<%# Eval("receipt_status_name") %>' Width="60px"></asp:Label>--%>
    <asp:LinkButton ID="ITlnkCancel"  Text='<%# Eval("receipt_status_name") %>' CssClass="label" Visible='<%# (bool)StatusVisible(Eval("RECEIPT_ACCOUNTED_STATUS"))  %>'   
    Enabled='<%# (bool)Eval("receipt_status").Equals("A") && Eval("RECEIPT_ACCOUNTED_STATUS").ToString()=="N" ?true:false %>' OnClientClick="return setDelete(this.id)" runat="server" CommandName="Select" CausesValidation="True"></asp:LinkButton>
    </ItemTemplate>    
    </asp:TemplateField>   
    
    <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <label style=" float:left" class="filterHeaderText">Print</label>    
    
    <%--<cc2:Filter ID="HTtxtStatus" TextBoxWidth="60px" OnClick="Filter_Click" runat="server" />                 --%>
    </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <asp:LinkButton ID="ITlnkPrint" runat="Server" onClick="ITlnkPrint_Click" Text="Print" width="70px"  ></asp:LinkButton>
    <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("receipt_status_name") %>' ToolTip='<%# Eval("receipt_status_name") %>' Width="60px"></asp:Label>--%>
    </ItemTemplate>    
    </asp:TemplateField> 
    
      
    
     <asp:TemplateField><HeaderTemplate>
    
    <label>Adults</label>                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="center" />
    <ItemTemplate>
  <label style="float:left">    <asp:Label ID="ITlblAdults" runat="server" Text='<%# Eval("receipt_adults") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_adults") %>' Width="40px"></asp:Label>     </label>         
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <label>Children</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="center" />
    <ItemTemplate>
    <label style="float:left"> <asp:Label ID="ITlblChildren" runat="server" Text='<%# Eval("receipt_children") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_children") %>' Width="40px"></asp:Label> </label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <label>Infant</label>
    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="center" />
    <ItemTemplate>
    <label style="float:left">    <asp:Label ID="ITlblInfants" runat="server" Text='<%# Eval("receipt_Infants") %>' CssClass="label grdof"   ToolTip='<%# Eval("receipt_Infants") %>' Width="40px"></asp:Label>   </label>         
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Fare</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblFare" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("receipt_fare")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_fare") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   

    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>Total Fare</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblTotalFare" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("receipt_total_fare")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_total_fare") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> 
    <%--vij--%>
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Mode</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblMode" runat="server" Text='<%# Eval("RECEIPT_SETTLEMENT_MODE_DESCRIPTION") %>' CssClass="label grdof"  ToolTip='<%# Eval("RECEIPT_SETTLEMENT_MODE_DESCRIPTION") %>' ></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Currency</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblCurrency" runat="server" Text='<%# Eval("receipt_currency_code") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_currency_code") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> 
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Cash</label>                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblCash"  runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("cash_base_amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("cash_base_amount") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Credit</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblCredit"  runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("credit_base_amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("credit_base_amount") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Card</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblCard"  runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("card_base_amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("card_base_amount") %>' ></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Employe</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmployee"  runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("employee_base_amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("employee_base_amount") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Others</label>
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblOthers"  runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("others_base_amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("others_base_amount") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>     
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>To Collect(Base)</label>
    
    </HeaderTemplate>
     <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblBaseToCollect" style="margin-right:5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("receipt_base_to_collect")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_base_to_collect") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>To Collect(Local)</label>
    </HeaderTemplate>
     <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblLocalToCollect" style="margin-right:5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("receipt_local_to_collect")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_local_to_collect") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
    
    
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>StaffID</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblStaffId" runat="server" Text='<%# Eval("receipt_staff_id") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_staff_id") %>' ></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <label>Vehicle</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblVehicle" runat="server" Text='<%# Eval("VEHICLE_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("VEHICLE_NAME") %>' Width="150px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
        <label>Notes</label>
    
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblNotes" runat="server" Text='<%# Eval("receipt_notes") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_notes") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    
    
     <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
   
    <label style="width:140px; float:left" class="filterHeaderText">From Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblFromDate" runat="server" Text='<%# IDDateTimeFormat(Eval("receipt_from_date")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_from_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField> 
    
    <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
   
    <label style="width:140px; float:left" class="filterHeaderText">To Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblToDate"  runat="server" Text='<%# IDDateTimeFormat(Eval("receipt_to_date")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_to_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField> 
    
    <%--vij--%>
    <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
    <label>User</label>
    
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblUSer" runat="server" Text='<%# Eval("receipt_created_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_created_name") %>' Width="120px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> 
    
     <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <label>Location</label>
    
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("receipt_location_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_location_name") %>'></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <label>Agent</label>
    
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("receipt_agent_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_agent_name") %>' Width="100px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>Remarks</label>
    
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("receipt_remarks") %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_remarks") %>' ></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
          
    </Columns>           
    </asp:GridView>
    </td></tr>
    <tr>
        <td align="left">
        <table>
        <tr>
        <td width="80px"><asp:Button OnClick="btnExport_Click"  runat="server" id="btnExport" Text="Export To Excel"  CssClass="button" /> </td>
        <td>
        </td>
        
        </tr>
        </table>
         </td>
    </tr>
    </table>
    
    
    
    
    <!-- advance filter -->
    <div id="divAdvanceFilterDocDate" runat="server" class="content" style="position:absolute;border-width:2px;display:none;height:75px;">
<asp:Panel id ="pnlAdvanceFilterDocDate" runat="server"  style="display:none">
<table cellpadding="0" cellspacing="0" >
<tr>
<td align="right" style="width:75px"><asp:Label runat = "server" id = "lblDocDateFrom" Text = "Date From:" CssClass="label" ></asp:Label></td>
<td align="left" style="width:125px"><uc1:DateControl ID="dcDocDateFrom" DateOnly="true" runat="server" DateDelimiter="Hyfen" DateFormat="DDMMMYYYY" /></td>
</tr><tr>
<td align="right" style="width:75px"><asp:Label runat = "server" id = "lblDocDateTo" Text = "To:" CssClass="label"  ></asp:Label></td>
<td align="left" style="width:125px"><uc1:DateControl ID="dcDocDateTo" DateOnly="true" runat="server" DateDelimiter="Hyfen" DateFormat="DDMMMYYYY" /></td>
</tr>

<tr >
<td  align="right" colspan="2"><asp:Button ID="btnDocDateApply" Text="Apply" runat="server"  CssClass="button" Width="50px" OnClientClick="return ValidateDate(this.id)" ></asp:Button>
<asp:Button ID="btnDocDateCancel" Text="Cancel" runat="server" CssClass="button" Width="50px" OnClientClick="return validateAdvanceFilter(this.id,'C');"></asp:Button></td>
</tr >


</table>

</asp:Panel> 
</div>
    
    
    
    </div>
 
 
    <%--<asp:Label Cssclass="grandTotal" id="lblGrandText" runat="server" text="Grand Total Of Total Fare:">
    </asp:Label><asp:Label Cssclass="grandTotal" id="lblGrandTotal" runat="server" text=""></asp:Label>--%>
    
  <%--  </div>--%>

 <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<asp:Label style="COLOR: #dd1f10" id="lblError" runat="server"></asp:Label> 
<div>
    <asp:DataGrid ID="dgReceiptList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="Receipt Number" DataField="receipt_number" HeaderStyle-Font-Bold="true" ></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Date" DataField="receipt_date" HeaderStyle-Font-Bold="true"></asp:BoundColumn>   
     
     
    <asp:BoundColumn HeaderText="Pax Name" DataField="receipt_pax_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>  
    <asp:BoundColumn HeaderText="Adults" DataField="receipt_adults" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Children" DataField="receipt_children" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Infant" DataField="receipt_Infants" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Fare" DataField="receipt_fare" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="Total Fare" DataField="receipt_total_fare" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="Coll.Mode" DataField="receipt_settlement_mode_description" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Currency" DataField="receipt_currency_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="CASH" DataField="cash_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="CREDIT" DataField="credit_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="CARD" DataField="card_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="EMPLOYEE" DataField="employee_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="OTHERS" DataField="others_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
       
    
    
    <asp:BoundColumn HeaderText="User" DataField="receipt_created_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Location" DataField="receipt_location_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Remarks" DataField="receipt_remarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Status" DataField="receipt_status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
    </asp:DataGrid>
    </div>
 
 </div>

<script  type="text/javascript">


function validateAdvanceFilter(id,mode)
 {

        if(mode=='A')
        {       
             
             getElement('pnlAdvanceFilterDocDate').style.display="block";  
             getElement('divAdvanceFilterDocDate').style.display="block";
             var positions=getRelativePositions(document.getElementById(id));                        
             getElement('divAdvanceFilterDocDate').style.left= positions[0]+'px';
             getElement('divAdvanceFilterDocDate').style.top=(positions[1]+20)+'px';   
               
        }
        else
        {
             getElement('pnlAdvanceFilterDocDate').style.display="none";  
             getElement('divAdvanceFilterDocDate').style.display="none"; 
             getElement('dcDocDateTo_Date').value=''; 
             getElement('dcDocDateFrom_Date').value=''; 
         
        }
        return false;
    
 }
 function getRelativePositions(obj)
    {
        var curLeft = 0;
        var curTop = 0;
        if(obj.offsetParent)
        {
            do
            {
                curLeft+=obj.offsetLeft;
                curTop+=obj.offsetTop;                
            }while(obj=obj.offsetParent);
            
        }
        return [curLeft, curTop];
    }
 function getElement(id)
 {
    return document.getElementById('ctl00_cphTransaction_'+id);
 }
 
 function ValidateDate(id)
 {
  var fromDate =GetDateObject('ctl00_cphTransaction_dcDocDateFrom');
  var toDate =GetDateObject('ctl00_cphTransaction_dcDocDateTo');   
  if(fromDate!=null && toDate!= null && (fromDate>toDate)) addMessage('To-Date should be later than or equal to From-Date !','');
  if(getMessage()!=''){ 
  alert(getMessage()); clearMessage(); return false; }
 }
function ShowHide(div)
{
    if(getElement('hdfParam').value=='1')
        {
            //alert(document.getElementById(div));
            //alert(document.getElementById(div).innerHtml);
            //getElement(div).value;
            document.getElementById('ancParam').innerHTML='Show Param'
            document.getElementById(div).style.display='none';
            getElement('hdfParam').value='0';
        }
        else
        {
            document.getElementById('ancParam').innerHTML='Hide Param'
           document.getElementById('ancParam').value='Hide Param'
            document.getElementById(div).style.display='block';
            getElement('hdfParam').value='1';
        }
}
function ValidateParam()
{
     clearMessage();
    var fromDate=GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
    var fromTime=getElement('dcFromDate_Time');
    var toDate=GetDateTimeObject('ctl00_cphTransaction_dcToDate');
    var toTime=getElement('dcToDate_Time');
    if(fromDate==null) addMessage('Please select From Date !','');
    //alert(fromTime);
    if(fromTime.value=='') addMessage('Please select From Time!','');
    if(toDate==null) addMessage('Please select To Date !','');
    if(toTime.value=='') addMessage('Please select To Time!','');
    if((fromDate!=null && toDate!=null) && fromDate>toDate) addMessage('From Date should not be later than To Date!','');
    if(getMessage()!=''){ 
    alert(getMessage()); 
    //ShowMessageDialog('Receipt List',getMessage(),'Information');
    clearMessage(); return false; }
}
function setDelete(id) {
    var rowId = id.substring(0, id.lastIndexOf('_'));
    var receiptId = document.getElementById(rowId + '_IThdfReceiptId').value
    document.getElementById('<%= hdfReceiptId.ClientID %>').value = receiptId;
    document.getElementById('divDelete').style.display = "block";
    var positions = getRelativePositions(document.getElementById(id));
    document.getElementById('divDelete').style.left = (positions[0]) + 'px';
    document.getElementById('divDelete').style.top = (positions[1] + 20) + 'px';
    return false;
}
function validateRemarks() {
    if (getElement('txtRemarks').value == '') addMessage('Remarks cannot be blank!', '');
    if (getMessage() != '') {
        alert(getMessage()); clearMessage(); return false;
    }
}
function HideDelete() {
    document.getElementById('divDelete').style.display = "none";
    document.getElementById('<%= txtRemarks.ClientID %>').value = "";
    return false;
}


</script>
</asp:Content>

