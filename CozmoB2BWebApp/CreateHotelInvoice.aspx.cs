using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.IO;

public partial class CreateHotelInvoiceGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected HotelRoom[] hRoomList = new HotelRoom[0];
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
    protected RegCity agencyCity = new RegCity();
    protected UserMaster loggedMember = new UserMaster();
    protected int hotelId;
    protected int agencyId;
    protected int cityId = 7129;
    protected string remarks = string.Empty;
    protected string confNo;
    protected string routing;
    protected HotelItinerary itinerary = new HotelItinerary();
    protected string errorMessage = string.Empty;
    protected LocationMaster location;
    protected AgentType agencyType;
    protected AgentMaster parentAgency;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Hotel Invoice Details";
        AuthorizationCheck();
        loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        //agency = new AgentMaster(Settings.LoginInfo.AgentId);
        # region Hotel Invoice

        try
        {
            var pageParams = GenericStatic.GetSetPageParams("CreateHotelInvoice", "", "get").Split('|');

            if (Request["hotelId"] != null && Request["agencyId"] != null && Request["confNo"] != null)
            {
                hotelId = Convert.ToInt32(Request["hotelId"]);
                agencyId = Convert.ToInt32(Request["agencyId"]);

                confNo = Request["confNo"];
            }
            else
            {
                //hotelId = Convert.ToInt32(Request.QueryString["hotelId"]);
                //agencyId = Convert.ToInt32(Request.QueryString["agencyId"]);

                //confNo = Request.QueryString["confNo"];
                hotelId = Convert.ToInt32(pageParams[0]);
                agencyId = Convert.ToInt32(pageParams[1]);

                confNo = pageParams[2];
            }

            //agency = new AgentMaster(Settings.LoginInfo.AgentId);
            agency = new AgentMaster(agencyId);
            
                parentAgency = new AgentMaster(agencyId,"GetParentData");
            string serverPath = "";
            if (Request.Url.Port > 0)
            {
                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }

            string logoPath = "";
            string ImageFilename = "";
            if (agency.ID > 1)
            {                
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"];
                ImageFilename = Convert.ToString(agency.AgentParantId) != "0" ? parentAgency.ImgFileName : agency.ImgFileName;
                imgLogo.ImageUrl = logoPath+ ImageFilename;                
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "images/logo.jpg";
            } 
           
            itinerary.Load(hotelId);
            HotelPassenger hPax = new HotelPassenger();
            hPax.Load(hotelId);
            itinerary.HotelPassenger = hPax;

            if (Request["city"] != null)
            {
                routing = Request["city"];
            }

            if (Request["remarks"] != null)
            {
                remarks = Request["remarks"];
            }

            // Reading agency information.
            DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
            DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

            if (cities != null && cities.Length > 0)
            {
                cityId = Convert.ToInt32(cities[0]["city_id"]);
            }
            //agency = new AgentMaster(agencyId);
            if (agency.City.Length > 0)
            {
                agencyCity.CityName = agency.City;
            }
            else
            {   
                agencyCity = RegCity.GetCity(cityId);
            }
            // Formatting agency address for display.
            agencyAddress = agency.Address;
            agencyAddress.Trim();
            //if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            //{
            //    agencyAddress += ",";
            //}
            //if (agency.Address != null && agency.Address.Length > 0)
            //{
            //    agencyAddress += agency.Address;
            //}
            //agencyAddress.Trim();
            //if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            //{
            //    agencyAddress += ",";
            //}

            location = new LocationMaster(itinerary.LocationId);
            // Getting ticket list from DB       
            HotelRoom hRoom = new HotelRoom();
            hRoomList = hRoom.Load(hotelId);
            //Price info


            int invoiceNumber = 0;
            try
            {
                // Generating invoice.

                if (hRoomList.Length > 0)
                {
                    invoiceNumber = Invoice.isInvoiceGenerated(hRoomList[0].RoomId, ProductType.Hotel);
                }
                if (invoiceNumber > 0)
                {
                    invoice = new Invoice();
                    invoice.Load(invoiceNumber);
                }
                else
                {
                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(hotelId, string.Empty, (int)loggedMember.ID, ProductType.Hotel, 1);
                    invoice.Load(invoiceNumber);
                }


            }
            catch (Exception exp)
            {
                errorMessage = "Invalid Invoice Number";
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
                return;
            }
        }
        catch (Exception exp)
        {
            errorMessage = "Your Session is Expired!";
            Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
        }
        #endregion
    }
    /// <summary>
    /// This is for Authentication.
    /// </summary>
    private void AuthorizationCheck()
    {
        try
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            else
            {
                agencyId = Settings.LoginInfo.AgentId;
                agencyType = Settings.LoginInfo.AgentType;
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
       

        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelBooking))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }

    /// <summary>
    /// Button click event to send invoice email 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEmailInvoice_Click(object sender, EventArgs e)
    {
        string sMsg = string.Empty;
        try
        {
            btnEmail.Visible = false;
            btnPrint.Visible = false;

            StringWriter sWriter = new StringWriter();
            HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
            divInvoice.RenderControl(htWriter);

            EmailParams clsEmail = new EmailParams();
            clsEmail.ToEmail = txtEmail.Text;
            clsEmail.FromEmail = ConfigurationManager.AppSettings["fromEmail"];
            clsEmail.Subject = "Hotel Invoice";
            clsEmail.EmailBody = sWriter.ToString();

            try
            {
                Email.Send(clsEmail);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                throw ex;
            }
            finally
            {
                btnEmail.Visible = true;
                btnPrint.Visible = true;
            }
            sMsg = "Email sent successfully";
        }
        catch (Exception ex)
        {
            sMsg = "Failed to send email";
            Audit.Add(EventType.Email, Severity.High, 1, "(btnEmailInvoice_Click)Failed to send mail from Create hotel invoice screen. Reason : " + ex.ToString(), "");
        }
        CT.TicketReceipt.Common.Utility.StartupScript(this, "Showalert('" + sMsg + "');", "script");
    }
}
