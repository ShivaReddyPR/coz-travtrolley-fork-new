using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System.Linq;

public partial class CreateUserUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string USER_SESSION = "_UserMaster";
    //private string USER_SEARCH_SESSION = "_UserMasterSearchList";
    private string USER_ROLE_SESSION = "_UserRoleList";
    
    private const string ACTIVE = "A";
    private const string DELETED = "D";

    private enum PageMode
    {
        Add = 1,
        Update = 2
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            lblSuccessMsg.Text = string.Empty;      
            if (!IsPostBack)
            {
                ViewState["Pwd"] = string.Empty;
                ViewState["ConPwd"] = string.Empty;
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                Mode = PageMode.Add;
                InitializePageControls();
                //Clear();
                lblSuccessMsg.Text = string.Empty;


                //string userId = Request.QueryString["userid"];
                //if (!string.IsNullOrEmpty(userId))
                //{
                //    Mode = PageMode.Update;
                //    Edit(Utility.ToLong(userId));
                //    hdfMode.Value = "1";
                //}


                }
            else 
            {
                ViewState["Pwd"] = txtPassword.Text;
                ViewState["ConPwd"]=txtConfirmPassword.Text;
              
               // txtPassword.Attributes.Add("value", ViewState["Pwd"].ToString());
                //lblPassText.Text = "Password entered: " + ViewState["Pwd"].ToString();

            }
           //else Mode = PageMode.Add; 
        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
        protected void Page_PreRender(object sender, EventArgs e)
        {
                txtPassword.Attributes.Add("value", ViewState["Pwd"].ToString());
            txtConfirmPassword.Attributes.Add("value",ViewState["ConPwd"].ToString());
        }
    private void InitializePageControls()
    {
        try
        {
           // Clear();
            DataTable dt = UserMaster.GetMemberTypeList("user_type").Tables[0];
            DataView dv = dt.DefaultView;
            if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                dv.RowFilter = "FIELD_VALUE='OPERATIONS' OR FIELD_VALUE='SUPERVISOR'";
                ddlTransType.Enabled = false;
            }
            ddlMemberType.DataSource = dv.ToTable();
            ddlMemberType.DataValueField = "FIELD_VALUE";
            ddlMemberType.DataTextField = "FIELD_TEXT";
            ddlMemberType.DataBind();
            ddlMemberType.Items.Insert(0,new ListItem("--Select Memeber Type--", "-1"));
            BindAgent();
            BindSubAgent();
            Clear();
            BindLocation(Utility.ToInteger(ddlAgent.SelectedItem.Value));
            if (ddlAgent.SelectedIndex == 0) ddlLocation.Enabled = false;
            BindUserSpecialAccess(0); // Initially loading special access from CT_T_FIELD_TYPE_MASTER 

        }
        catch { throw; }

        //ddlMemberType bind

    }
    private void BindLocation(int agentId)
    {
        try
        {
            ddlLocation.DataSource = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, "");
            ddlLocation.DataValueField = "LOCATION_ID";
            ddlLocation.DataTextField = "LOCATION_NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "-1"));
        }
        catch { throw; }
    }

    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));

            if (agentType == AgentType.B2B.ToString() || agentType == AgentType.B2B2B.ToString())
            {
                ddlAgent.Items.Insert(1, new ListItem(Settings.LoginInfo.AgentName, Settings.LoginInfo.AgentId.ToString()));
            }
            ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
            
        }
        catch { throw; }
    }

    private void BindSubAgent()
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedValue);
            //getting AgentType and SubAgent enable only B2B time
            int agentType = AgentMaster.GetAgentType(agentId);
            if (agentType == (int)AgentType.BaseAgent)
            {
                ddlSubAgent.Enabled = false;
                lblSubAgent.Enabled = false;
            }
            else if (agentType == (int)AgentType.Agent)
            {
                lblSubAgent.Enabled = false;
                ddlSubAgent.Enabled = false;
            }
            else
            {
                lblSubAgent.Enabled = true;
                ddlSubAgent.Enabled = true;
            }
            AgentMaster agent = new AgentMaster(agentId);
            DataTable dt = null;
            if (agent.AgentType == (int)AgentType.BaseAgent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (agent.AgentType == (int)AgentType.Agent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (agent.AgentType == (int)AgentType.B2B)
            {
                dt = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }

            ddlSubAgent.DataSource = dt;
            ddlSubAgent.DataTextField = "agent_name";
            ddlSubAgent.DataValueField = "agent_id";
            ddlSubAgent.DataBind();
            ddlSubAgent.Items.Insert(0, new ListItem("--Select SubAgent--", "-1"));
        }
        catch { }
    }
    # region Page Members

    private PageMode Mode
    {
        get { return (PageMode)ViewState["_UserPageMode"]; }
        set { ViewState["_UserPageMode"] = value; }
    }

    # endregion
    private UserMaster CurrentObject
    {
        get
        {
            return (UserMaster)Session[USER_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(USER_SESSION);
            }
            else
            {
                Session[USER_SESSION] = value;
            }

        }

    }
    private void Clear()
    {
        try
        {
            UserMaster tempUser = new UserMaster(-1, Settings.LoginInfo.AgentType.ToString());
            UserRoleDetails = tempUser.UseRole;
         
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtLoginName.Text = string.Empty;
            txtLoginSuffix.Text = string.Empty;
            txtLoginName.Enabled = true;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtPassword.Enabled = true;
            txtConfirmPassword.Enabled = true;
            ddlMemberType.SelectedIndex = 0;
            ddlLocation.SelectedIndex = -1;
           // ddlCompany.SelectedIndex = 0;
            txtAddress.Text = string.Empty;
            CurrentObject = null;
            hdfMode.Value = "0";
            ddlTransType.SelectedIndex =0;
            btnSave.Text = "Save";
            txtMobNo.Text = string.Empty;
			txtCUserCode.Text = string.Empty;
            //if (Mode == PageMode.Update) Response.Redirect("UserList.aspx");
            BindGrid();
            ViewState["Pwd"] = string.Empty;
            ViewState["ConPwd"] = string.Empty;
            //ddlAgent.Enabled = Settings.LoginInfo.MemberType == MemberType.SUPER ? true : false;
            if (Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlAgent.Enabled = true;
            }
            else ddlAgent.Enabled = false;

            if (Settings.LoginInfo.AgentId == 0 )
            {
                ddlAgent.SelectedValue = Settings.LoginInfo.AgentId == 0 ? Utility.ToString(-1) : Utility.ToString(Settings.LoginInfo.AgentId);
            }
            BindUserSpecialAccess(0);//To Load default user Access.
            //ddlAgent.SelectedValue = Settings.LoginInfo.AgentId == 0 ? Utility.ToString(-1) : Utility.ToString(Settings.LoginInfo.AgentId);
            //ddlAgent.SelectedValue = "-1";
            Utility.StartupScript(this.Page, "setLoginSuffix('" + ddlAgent.ClientID + "');", "setLoginSuffix");
            
            
        }
        catch
        {
            throw;
        }
    }
    private void Edit(long id)
    {
        try
        {
            txtLoginSuffix.Text = string.Empty;
            UserMaster user = new UserMaster(id,Settings.LoginInfo.AgentType.ToString());
            CurrentObject = user;
            txtFirstName.Text = user.FirstName;
            txtLastName.Text = user.LastName;
            txtEmailID.Text = user.Email;
            txtLoginName.Text = user.LoginName;
            txtLoginName.Enabled = false;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtPassword.Enabled = false;
            txtConfirmPassword.Enabled = false;
            ddlMemberType.SelectedValue = user.MemeberType.ToString();
            BindUserSpecialAccess(id);//Loading User Special access from CT_T_UserSpecialAccess table & fieldTypeMaster Table.
            
            //ddlCompany.SelectedValue = Utility.ToString(user.CompanyId);
            if (user.ParentAgentId > 1)
            {
                try
                {
                    ddlAgent.SelectedValue = user.ParentAgentId.ToString();
                    BindSubAgent();
                    ddlSubAgent.SelectedValue = user.AgentId.ToString();
                }
                catch { }
            }
            else
            {
                ddlAgent.SelectedValue = Utility.ToString(user.AgentId);
                BindSubAgent();
            }
            BindLocation(Utility.ToInteger(user.AgentId));
            try
            {
                ddlLocation.SelectedValue = Utility.ToString(user.LocationID);
            }
            catch { }

            ddlTransType.SelectedValue = user.TransType.ToString();
           
            txtAddress.Text = user.Address;
            txtMobNo.Text = user.MobileNo;
			txtCUserCode.Text = user.UserCode;
            UserRoleDetails = user.UseRole;
            btnSave.Text = "Update";
            btnCancel.Text = "Cancel";
            BindGrid();
            hdfMode.Value  = "1";
            
        }
        catch
        {
            throw;
        }
    }

    //private DataTable SearchList
    //{
    //    get
    //    {
    //        return (DataTable)Session[USER_SEARCH_SESSION];
    //    }
    //    set
    //    {
    //        value.PrimaryKey = new DataColumn[] { value.Columns["USER_ID"] };
    //        Session[USER_SEARCH_SESSION] = value;
    //    }
    //}
    private void Save()
    {
        try
        {
            UserMaster user;
            if (CurrentObject == null)
            {
                user = new UserMaster();
            }
            else
            {
                user = CurrentObject;
            }
            user.FirstName = txtFirstName.Text.Trim();
            user.LastName = txtLastName.Text.Trim();
            user.Email = txtEmailID.Text;
            user.LoginName = txtLoginName.Text.Trim() + "" + txtLoginSuffix.Text.Trim(); 
            user.Password = txtPassword.Text;
            string type = ddlMemberType.SelectedValue;
            if (type == MemberType.ADMIN.ToString())
            {
                user.MemeberType = MemberType.ADMIN;
            }
            else if (type == MemberType.CASHIER.ToString())
            {
                user.MemeberType = MemberType.CASHIER;
            }
            else if (type == MemberType.OPERATIONS.ToString())
            {
                user.MemeberType = MemberType.OPERATIONS;
            }
            else if (type == MemberType.BACKOFFICE.ToString())
            {
                user.MemeberType = MemberType.BACKOFFICE;
            }
            else if (type == MemberType.SUPERVISOR.ToString())
            {
                user.MemeberType = MemberType.SUPERVISOR;
            }
            else if (type == MemberType.SUPER.ToString())
            {
                user.MemeberType = MemberType.SUPER;
            }
            else if (type == MemberType.GVOPERATIONS.ToString())
            {
                user.MemeberType = MemberType.GVOPERATIONS;
            }
            else if (type == MemberType.HALASERVICEUSER.ToString())
            {
                user.MemeberType = MemberType.HALASERVICEUSER;
            }
            else if (type == MemberType.TRAVELCORDINATOR.ToString())
            {
                user.MemeberType = MemberType.TRAVELCORDINATOR;
            }
            else if (type == MemberType.SALESEXECUTIVE.ToString())
            {
                user.MemeberType = MemberType.SALESEXECUTIVE;
            }
            else if (type == MemberType.VMSUSER.ToString())
            {
                user.MemeberType = MemberType.VMSUSER;
            }
            user.LocationID=Utility.ToLong(ddlLocation.SelectedItem.Value);
            user.AgentId = (ddlSubAgent.SelectedIndex > 0 ? Utility.ToInteger(ddlSubAgent.SelectedValue) : Utility.ToInteger(ddlAgent.SelectedValue));
            user.Address = txtAddress.Text;
            user.Status = ACTIVE;
            user.CreatedBy = Settings.LoginInfo.UserID;
            user.UseRole = UserRoleDetails;
            user.TransType = ddlTransType.SelectedItem.Value;
            user.MobileNo = txtMobNo.Text.Trim();
			user.UserCode = txtCUserCode.Text.Trim();
            user.Save();
            if (user.ID > 0) //Saving & Updating the User Special access 
            {
                foreach (ListItem item in lstUserAccess.Items)
                {
                    UserSpecialAccess userSpecialAccess = new UserSpecialAccess();
                    userSpecialAccess.UserId = user.ID;
                    userSpecialAccess.AccessType = item.Text;
                    userSpecialAccess.AccessValue = (item.Selected) ? 1 : 0;
                    userSpecialAccess.Status = true;
                    userSpecialAccess.CreatedBy = Settings.LoginInfo.UserID;
                    userSpecialAccess.ModuleName = string.Empty;
                    UserSpecialAccess.SaveUserSpecialAccess(userSpecialAccess);
                }
            }
            lblSuccessMsg.Text = Formatter.ToMessage("User ", txtFirstName.Text.Trim(), Mode != PageMode.Add ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
            Clear();
           // lblErrorMsg.Text = string.Format("User '{0}' is saved successfully !", user.LoginName);
        }
        catch
        {
            throw;
        }
        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
            lblSuccessMsg.Text = String.Empty;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            BindSearch  ();
            //Clear();
        }
        catch (Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            //Utility.WriteLog(ex, this.Title);
        }
    }

    private void BindGrid()
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvUserRoleDetails , UserRoleDetails );
            //setRowId();
        }
        catch
        {
            throw;
        }
    }
    private DataTable UserRoleDetails
    {
        get
        {
            return (DataTable)Session[USER_ROLE_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["Role_id"] };
            Session[USER_ROLE_SESSION] = value;
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlAgent.SelectedIndex > 0)
            {
                ddlLocation.Enabled = true;
                BindLocation(Utility.ToInteger(ddlAgent.SelectedItem.Value));
                BindSubAgent();
            }
            else ddlLocation.Enabled = false;
            //if (Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0)
            //    BindCityList(ddlCityVisit, Utility.ToInteger(ddlCountry.SelectedItem.Value));
            //else
            //{
            //    ddlCityVisit.Enabled = false;
            //    ddlCityVisit.SelectedIndex = 0;
            //}
            //Utility.StartupScript(this.Page, "getElement('ddlCountry').focus()","key1");

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void ddlSubAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSubAgent.SelectedIndex > 0)
        {
            BindLocation(Convert.ToInt32(ddlSubAgent.SelectedValue));
        }
        else
        {
            BindLocation(Convert.ToInt32(ddlAgent.SelectedValue));
        }
    }

    protected void HTchkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");


            foreach (GridViewRow gvRow in gvUserRoleDetails.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                chkSelect.Checked = chkSelectAll.Checked;
                long funcId = Utility.ToLong(gvUserRoleDetails.DataKeys[gvRow.RowIndex].Value);
                SetRowStatus(funcId, chkSelect.Checked);

            }
        }
        catch { throw; }
    }

    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectOne = (CheckBox)gvHdrRow.FindControl("ITchkSelect");
            long functionId = Utility.ToLong(gvUserRoleDetails.DataKeys[gvHdrRow.RowIndex].Value);
            SetRowStatus(functionId, chkSelectOne.Checked);
            //BindGrid();

        }
        catch { throw; }
    }

    private void SetRowStatus(long functionId, Boolean check)
    {
        try
        {
            DataRow dr = UserRoleDetails.Rows.Find(functionId);
            if (dr != null)
            {
                if (check)
                {
                    UserRoleDetails.Rows.Find(functionId)["urd_status"] = (char)RecordStatus.Activated;
                    //UserRoleDetails.Rows.Find(functionId)["checked_status"] = (char)RecordStatus.Activated;
                }
                else
                {
                    UserRoleDetails.Rows.Find(functionId)["urd_status"] = (char)RecordStatus.Deactivated;
                    //UserRoleDetails.Rows.Find(functionId)["checked_status"] = (char)RecordStatus.Deactivated;
                }
            }

            if (Utility.ToLong(UserRoleDetails.Rows.Find(functionId)["URD_ROLE_ID"]) > 0)
            {
                dr.AcceptChanges();
                dr.SetModified();
            }
            else
            {
                dr.AcceptChanges();
                dr.SetAdded();
            }


        }
        catch { throw; }

        }
    protected void gvUserRoleDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvUserRoleDetails.PageIndex = e.NewPageIndex;
            gvUserRoleDetails.EditIndex = -1;
            BindGrid();

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {

            GridViewRow gvRow = gvUsers.Rows[e.RowIndex];
            long userId = Utility.ToLong(gvUsers.DataKeys[e.RowIndex].Value);
            UserMaster user = new UserMaster(userId);
            user.Status = DELETED ;
            user.CreatedBy = Settings.LoginInfo.UserID;

            user.Save();

            //lblSuccessMsg.Text = string.Format("user  '{0}'  is deleted successfully!", user.LoginName);

            FilterSearchGrid();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            // selectedItem();
            //BindGrid(); 
            string[,] textboxesNColumns ={ { "HTtxtRoleName", "Role_name" } };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvUserRoleDetails, UserRoleDetails.Copy(), textboxesNColumns);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    #region Serchregion
    private void BindSearch()
    {

        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvUsers, UserMaster.GetList(agentId, ListStatus.Long, RecordStatus.All));


        }
        catch
        {
            throw;
        }
    }
    //protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    try
    //    {
    //        if (e.Row.RowType == DataControlRowType.DataRow)
    //        {
    //            long userID = Utility.ToLong(gvUsers.DataKeys[e.Row.RowIndex].Value);
    //            string script = string.Format("return EditUser('{0}');", userID);
    //            ((LinkButton)e.Row.FindControl("lnkEdit")).Attributes.Add("onclick", script);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteLog(ex, this.Title);
    //        Label lblMasterError = (Label)this.Master.FindControl("lblError");
    //        lblMasterError.Visible = true;
    //        lblMasterError.Text = ex.Message;
    //    }
    //}
    protected void gvUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long userId = Utility.ToLong(gvUsers.SelectedValue);
            Edit(userId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvUsers.PageIndex = e.NewPageIndex;
            FilterSearchGrid();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {

        try
        {
            FilterSearchGrid();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void FilterSearchGrid()
    {
        try
        {
            string[,] filterValues = { { "HTtxtFirstName", "USER_FIRST_NAME" }, { "HTtxtLastName", "USER_LAST_NAME" }, { "HTtxtAddress", "USER_ADDRESS" },
                                        { "HTtxtLoginName", "USER_LOGIN_NAME" }, { "HTtxtEmail", "USER_EMAIL" } , { "HTtxtMemberType", "USER_MEMBER_TYPE_NAME" },{ "HTtxtLocation", "LOCATION_NAME" },{ "HTtxtAgent", "AGENT_NAME" },
			{ "HTtxtUserCode","user_code"} };
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            CommonGrid grid = new CommonGrid();
            grid.FilterGridView(gvUsers, UserMaster.GetList(agentId, ListStatus.Long, RecordStatus.All), filterValues);

        }
        catch
        {
            throw;
        }
    }

    private void BindUserSpecialAccess(long userId) //Bind User Special Access 
    {
        try
        {
            lstUserAccess.Items.Clear();
            //Bind User Special Access from CT_T_User_Special_Access 
            if (userId > 0)
            {
                List<UserSpecialAccess> userAccessList = UserSpecialAccess.GetUserSpecialAccesses(userId);
                if (userAccessList.Count > 0)
                {
                    DataTable dtUserSpecialAccess = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(Newtonsoft.Json.JsonConvert.SerializeObject(userAccessList));
                    foreach (DataRow dr in dtUserSpecialAccess.Rows)
                    {
                        ListItem item = new ListItem();
                        item.Selected = (Convert.ToInt32(dr["AccessValue"])==1) ? true : false;
                        item.Text = dr["AccessType"].ToString();
                        item.Value = dr["AccessType"].ToString();
                        lstUserAccess.Items.Add(item);
                    }
                }
            }
            //Bind default User Access From  Field Type Master Table
            DataTable dtUserDefaultAccess = UserSpecialAccess.LoadUserAccess();
            foreach (DataRow dr in dtUserDefaultAccess.Rows)
            {
                if (!lstUserAccess.Items.Contains(new ListItem(dr["access_type"].ToString())))
                {
                    ListItem listItem = new ListItem();
                    listItem.Selected = false;
                    listItem.Text = dr["access_type"].ToString();
                    listItem.Value = dr["access_type"].ToString();
                    lstUserAccess.Items.Add(listItem);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    #endregion
}

