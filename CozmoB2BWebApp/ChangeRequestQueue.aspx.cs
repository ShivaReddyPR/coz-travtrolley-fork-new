﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.Common;
using CT.MetaSearchEngine;
using CT.AccountingEngine;
using System.Net;
using System.Web.Script.Serialization;
using System.Linq;

public partial class ChangeRequestQueueUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected List<int> bookingModes = new List<int>();
    private List<int> shownStatuses = new List<int>();
    protected List<int> sourcesList = new List<int>();
    protected List<int> selectedSources = new List<int>();
    protected Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
    protected HotelSource sourceInfo = new HotelSource();
    protected List<HotelItinerary> itinerary = new List<HotelItinerary>();
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected ServiceRequest[] serviceRequest = new ServiceRequest[0];

    UserMaster loggedMember = null;
    protected Int64 agencyIdCSV;

    string bookingSourceCSV = null;
    string requestTypeCSV = null;
    string serviceRequestTypeCSV = null;
    int agencyTypeCSV;
    bool? isDomestic = null;
    protected List<int> requestTypes = new List<int>();
    protected List<int> requestStatuses = new List<int>();
    private int loggedMemberId;
    protected bool isAssignedRequest;
    protected CT.Core.Queue[] relevantQueues;
    protected string Cancelled = string.Empty; // For filtering with paging 
    protected string Confirmed = string.Empty; // For filtering with paging 
    protected string lastCanDate = string.Empty;
    protected int agentFilter; // For restricted filtering
    protected string loginFilter = null; // For restricted filtering
    protected string HotelFilter = string.Empty; // For restricted filtering
    protected string paxFilter = null;// For restricted filtering
    protected string pnrFilter = null;// For restricted filtering
    protected string hotelCancel = string.Empty; // For filtering with paging 
    protected string hotelAmendment = string.Empty;
    //protected string Refund = string.Empty; // For filtering with paging
    protected string Unassigned = string.Empty; // For filtering with paging
    protected string Assigned = string.Empty; // For filtering with paging
    protected string Acknowledged = string.Empty; // For filtering with paging
    protected string Completed = string.Empty; // For filtering with paging
    protected string Rejected = string.Empty; // For filtering with paging
    protected string Closed = string.Empty; // For filtering with paging 
    protected string Pending = string.Empty; // For filtering with paging 

    protected List<ChangeRequestQueue> data = new List<ChangeRequestQueue>();
    protected int recordsPerPage = 10;
    protected int noOfPages;
    protected int pageNo;
    protected int queueCount = 0;
    protected string airlineFilter, show, paxFilterUI, IntDom;
    protected string requestSourceIdCSV = null;
    protected string ticketFilter = null;
    protected bool isAdmin = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            if (Settings.LoginInfo != null)
            {

                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                    selectedSources = sourcesList;
                    hotelCancel = "HotelCancel";
                    hotelAmendment = "HotelAmendment";
                }

                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 0)
                {
                    isAdmin = true;
                }
                else
                {
                    loginFilter = Convert.ToString(Session["loginName"]);
                }
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    sourcesList = sourceInfo.Load();
                    selectedSources = sourcesList;

                    txtCheckIn.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtCheckOut.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    hdfParam.Value = "0";
                    InitializeControls();
                    LoadChangeQueues();

                }
                else
                {
                    SetFilters();
                }

                // records per page from configuration

                recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);

                //int agencyId = 1;

                loggedMember = new UserMaster(Convert.ToInt32(Session["memberId"]));



            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    #region Agent and location dropdown binding methods.
    private void InitializeControls()
    {
        try
        {
            BindAgent();
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlLocations.Enabled = true;
            }
            else
            {
                ddlSource.Visible = false;
            }
            //sources is binding
            Array Sources = Enum.GetValues(typeof(HotelBookingSource));
            foreach (HotelBookingSource source in Sources)
            {
                if (source == HotelBookingSource.DOTW || source == HotelBookingSource.RezLive || source == HotelBookingSource.LOH || source == HotelBookingSource.HotelBeds || source == HotelBookingSource.GTA || source == HotelBookingSource.WST || source == HotelBookingSource.Miki || source == HotelBookingSource.TBOHotel || source == HotelBookingSource.JAC || source == HotelBookingSource.EET || source == HotelBookingSource.Agoda || source == HotelBookingSource.Yatra || source == HotelBookingSource.GRN || source == HotelBookingSource.OYO || source == HotelBookingSource.GIMMONIX) //added by chandan on 29122015
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingSource), source), ((int)source).ToString());
                    ddlSource.Items.Add(item);
                }
            }
            //Status is binding
            Array Statuses = Enum.GetValues(typeof(ServiceRequestStatus));
            foreach (ServiceRequestStatus status in Statuses)
            {
                ListItem item = new ListItem(Enum.GetName(typeof(ServiceRequestStatus), status), ((int)status).ToString());
                ddlBookingStatus.Items.Add(item);
            }
            BindLocation();

            int b2bAgentId;
            int b2b2bAgentId;

            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgents.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgents.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgents.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }

            BindB2BAgent(Convert.ToInt32(ddlAgents.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));

            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Change REquest Queue: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "agent_name";
            ddlAgents.DataValueField = "agent_id";
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindLocation()
    {
        try
        {
            DataTable dtlocation = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            ddlLocations.DataSource = dtlocation;
            ddlLocations.DataTextField = "LOCATION_NAME";
            ddlLocations.DataValueField = "LOCATION_ID";
            ddlLocations.DataBind();
            ddlLocations.SelectedIndex = -1;
            ddlLocations.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "LOCATION_NAME";
        ddlLocations.DataValueField = "LOCATION_ID";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
        hdfParam.Value = "0";
    }
    #endregion
    void LoadChangeQueues()
    {
        try
        {
            //Added by Anji on 04/12/19, reason B2BAgent and B2B2BAgent functionality implemenation.
            if (!IsPostBack)
            {
                agencyIdCSV = Utility.ToInteger(Settings.LoginInfo.AgentId);
            }
            else
            {

                string agentType = string.Empty;

                agencyIdCSV = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                if (agencyIdCSV == 0)
                {
                    agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                    }
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = string.Empty; // null Means binding in list all BOOKINGS
                    }
                }
                if (agencyIdCSV > 0 && ddlB2BAgent.SelectedIndex > 0)
                {
                    if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                    {
                        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                        }
                        else
                        {
                            agencyIdCSV = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                        }
                        agencyIdCSV = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                if (agencyIdCSV > 0 && ddlB2B2BAgent.SelectedIndex > 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                    }
                    else
                    {
                        agencyIdCSV = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                    }
                }
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                            agencyIdCSV = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                        }
                    }
                }
            }
            //End by Anji.

            if (requestTypeCSV == null)
            {
                requestTypes.Add((int)RequestType.HotelCancel);
                requestTypes.Add((int)RequestType.HotelAmendment);
                hotelCancel = "HotelCancel";
                hotelAmendment = "HotelAmendment";
                requestTypeCSV = "4,5";
            }
            if (serviceRequestTypeCSV == null)
            {
                if (isAssignedRequest)
                {
                    requestStatuses.Add((int)ServiceRequestStatus.Unassigned);
                    requestStatuses.Add((int)ServiceRequestStatus.Assigned);
                    requestStatuses.Add((int)ServiceRequestStatus.Acknowledged);
                    requestStatuses.Add((int)ServiceRequestStatus.Closed);
                    requestStatuses.Add((int)ServiceRequestStatus.Pending);
                    requestStatuses.Add((int)ServiceRequestStatus.Rejected);
                    requestStatuses.Add((int)ServiceRequestStatus.Completed);
                    serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                }
            }
            isDomestic = true;

            string endDate = txtCheckOut.Text;
            endDate += " 23:59:59";
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            data = CT.Core.Queue.GetHotelChangeRequestData(((pageNo - 1) * recordsPerPage) + 1, (pageNo * recordsPerPage), requestTypeCSV, serviceRequestTypeCSV, agencyTypeCSV, bookingSourceCSV, airlineFilter, agencyIdCSV, paxFilter, loginFilter, pnrFilter, ticketFilter, isDomestic, ref queueCount, requestSourceIdCSV, Convert.ToDateTime(txtCheckIn.Text, dateFormat), Convert.ToDateTime(endDate, dateFormat));
            CT.Core.Queue[] queues = new CT.Core.Queue[data.Count];
            for (int j = 0; j < data.Count; j++)
            {
                queues[j] = new CT.Core.Queue();
                queues[j].QueueId = data[j].QueueId;
                queues[j].ItemId = data[j].ItemId;
            }
            AssignToVariables(queues);
            string url = "AdminHotelChangeRequest.aspx?pageType=bookingdate";
            if (queueCount > 0)
            {
                if ((queueCount % recordsPerPage) > 0)
                {
                    noOfPages = (queueCount / recordsPerPage) + 1;
                }
                else
                {
                    noOfPages = (queueCount / recordsPerPage);
                }
            }
            else
            {
                noOfPages = 0;
            }

            if (noOfPages > 0)
            {
                show = MetaSearchEngine.PagingJavascript(noOfPages, url, Convert.ToInt32(PageNoString.Value));
            }

            if (airlineFilter == null)
            {
                airlineFilter = "";
            }
            if (agencyIdCSV == null)
            {
                agentFilter = 0;
            }
            if (paxFilter == null)
            {
                paxFilter = "";
            }
            if (pnrFilter == null)
            {
                pnrFilter = "";
            }
            if (loginFilter == null)
            {
                loginFilter = "";
            }
        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Change REquest Queue: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void AssignToVariables(CT.Core.Queue[] relevantQueues)
    {
        try
        {
            ArrayList listOfItemId = new ArrayList();
            int numBooking = 0;
            foreach (CT.Core.Queue queue in relevantQueues)
            {
                ServiceRequest sr = new ServiceRequest(queue.ItemId);
                listOfItemId.Add(queue.ItemId);
                numBooking++;
            }
            bookingDetail = new BookingDetail[numBooking];

            serviceRequest = new ServiceRequest[numBooking];
            for (int count = 0; count < listOfItemId.Count; count++)
            {
                ServiceRequest sRequest = new ServiceRequest(Convert.ToInt32(listOfItemId[count]));
                serviceRequest[count] = sRequest;
                BookingDetail bDetail = null;
                try
                {
                    bDetail = new BookingDetail(serviceRequest[count].BookingId);
                    bookingDetail[count] = bDetail;
                }
                catch { }
                Product[] products = BookingDetail.GetProductsLine(serviceRequest[count].BookingId);
                AgentMaster agency = new AgentMaster(bDetail.AgencyId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i] != null)
                    {
                        string productType = Enum.Parse(typeof(ProductType), products[i].ProductTypeId.ToString()).ToString();
                        switch (productType)
                        {
                            case "Flight":
                                break;
                            case "Car":
                                break;
                            case "Hotel":
                                HotelItinerary itineary = new HotelItinerary();
                                itineary.Load(products[i].ProductId);
                                itineary.ProductId = products[i].ProductId;
                                itineary.ProductType = products[i].ProductType;
                                itineary.ProductTypeId = products[i].ProductTypeId;
                                itineary.AgencyReference = agency.Name;
                                itineary.SpecialRequest = sRequest.Data;

                                HotelPassenger pax = new HotelPassenger();
                                pax.Load(products[i].ProductId);
                                itineary.HotelPassenger = pax;

                                itineary.Roomtype = new HotelRoom().Load(products[i].ProductId);

                                if (itineary.Source == HotelBookingSource.DOTW && itineary.LastCancellationDate.Subtract(DateTime.Now).TotalDays > 0 && sRequest.RequestType == RequestType.HotelCancel)
                                {
                                    itinerary.Add(itineary);
                                }
                                else
                                {
                                    itinerary.Add(itineary);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }


            Session["ServiceRequests"] = serviceRequest;
            dlChangeRequests.DataSource = itinerary;
            dlChangeRequests.DataBind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.ChangeRequest, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void SetFilters()
    {

        //bool isAssignedRequest = Role.IsAllowedTask((int)Session["roleId"], (int)Task.AssignServiceRequest);
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAssignedRequest = true;
            isAdmin = true;
        }
        //if (ddlAgents.SelectedIndex > 0)
        //{
        //agencyTypeCSV = 1;
        //}


        //if (txtHotelName.Text != string.Empty)
        {
            if (requestTypeCSV == null)
            {
                requestTypeCSV = "4";
            }
            hotelCancel = Request["HotelCancel"];
        }
        if (Request["HotelAmendment"] != null)
        {
            if (requestTypeCSV == null)
            {
                requestTypeCSV = "5";
            }
            else
            {
                requestTypeCSV += ",5";
            }
            hotelAmendment = Request["HotelAmendment"];
        }

        switch (ddlBookingStatus.SelectedItem.Text)
        {
            case "Unassigned":
                serviceRequestTypeCSV = "1";
                break;
            case "Assigned":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "2";
                }
                else
                {
                    serviceRequestTypeCSV += ",2";
                }
                break;
            case "Acknowledged":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "3";
                }
                else
                {
                    serviceRequestTypeCSV += ",3";
                }
                break;
            case "Completed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "4";
                }
                else
                {
                    serviceRequestTypeCSV += ",4";
                }
                break;
            case "Rejected":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "5";
                }
                else
                {
                    serviceRequestTypeCSV += ",5";
                }
                break;
            case "Closed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "6";
                }
                else
                {
                    serviceRequestTypeCSV += ",6";
                }
                break;
            case "Pending":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "7";
                }
                else
                {
                    serviceRequestTypeCSV += ",7";
                }
                break;
            case "All":
                serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                break;
        }


        selectedSources = new List<int>();
        sourcesList = sourceInfo.Load();
        bookingSourceCSV = null;
        //for (int i = 1; i <= sourcesList.Count; i++)
        //{
        //    if (ddlSource.Items.Count > 0)
        //    {
        //        selectedSources.Add(i);
        //        if (bookingSourceCSV != null)
        //        {
        //            bookingSourceCSV += "," + i.ToString();
        //        }
        //        else
        //        {
        //            bookingSourceCSV = i.ToString();
        //        }
        //    }
        //}

        //if (sourcesList.Count <= 0)
        //{
        //    bookingSourceCSV = "1,2,3,4,5,6,7,8,9,10";
        //}

        if (ddlSource.SelectedItem.Text == "All")
        {
            for (int i = 1; i <= sourcesList.Count; i++)
            {
                //if (ddlSource.Items.Count > 0)
                //{
                selectedSources.Add(i);
                if (bookingSourceCSV != null)
                {
                    bookingSourceCSV += "," + i.ToString();
                }
                else
                {
                    bookingSourceCSV = i.ToString();
                }
                //}
            }
        }
        else
        {
            bookingSourceCSV = Convert.ToString(ddlSource.SelectedItem.Value);
        }

        if (!string.IsNullOrEmpty(txtHotelName.Text))
        {
            airlineFilter = txtHotelName.Text.Replace("'", "''").Trim();
        }
        if (!string.IsNullOrEmpty(txtAgentLoginName.Text.Trim()))
        {
            loginFilter = txtAgentLoginName.Text.Replace("'", "''").Trim();
        }
        if (!string.IsNullOrEmpty(txtPaxName.Text))
        {
            paxFilterUI = txtPaxName.Text;
            paxFilter = txtPaxName.Text.Replace("'", "''").Trim();
            paxFilter = paxFilter.Replace(" ", "");
        }
        if (!string.IsNullOrEmpty(txtConfirmNo.Text))
        {
            pnrFilter = txtConfirmNo.Text.Replace("'", "''").Trim();
        }
        //if (!string.IsNullOrEmpty(txtConfirmNo.Text))
        //{
        //    ticketFilter = txtConfirmNo.Text;
        //}
        //if (txtAgentLoginName.Text != string.Empty)
        if (ddlAgents.SelectedItem.Text != "All")
        {
            //agencyIdCSV = ddlAgents.SelectedItem.Text.Replace("'", "''").Trim();
        }
        //if (Request["AgencyLive"] != null && Request["AgencyLive"] != string.Empty)
        {
            //2 is for WhiteLabel and 3 is for BookingAPI as per ServiceRequest.RequestSource enum
            requestSourceIdCSV = "2,3";
        }
        //if (airlineFilter == null)
        //{
        //    airlineFilter = "";
        //}
        //if (agencyIdCSV == null)
        //{
        //    agentFilter =0;
        //    agencyIdCSV = "";
        //}
        //if (paxFilter == null)
        //{
        //    paxFilter = "";
        //}
        //if (pnrFilter == null)
        //{
        //    pnrFilter = "";
        //}
        //if (loginFilter == null)
        //{
        //    loginFilter = "";
        //}    

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        LoadChangeQueues();
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }

            BindLocation(agentId, type);
        }
        catch (Exception ex)
        { }

    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void dlChangeRequests_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Refund")
            {
                loggedMemberId = (int)Settings.LoginInfo.UserID;
                TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
                Label lblRemarks = e.Item.FindControl("lblRemarks") as Label;
                Label lblBookingPrice = e.Item.FindControl("lblBookingPrice") as Label;
                lblMessage.Text = "";

                if (txtAdminFee.Text.Trim().Length > 0 && txtSupplierFee.Text.Trim().Length > 0)
                {
                    decimal bookingAmount = Math.Ceiling(Convert.ToDecimal(lblBookingPrice.Text));
                    decimal feeAmount = Math.Ceiling(Convert.ToDecimal(txtAdminFee.Text) + Convert.ToDecimal(txtSupplierFee.Text));
                    // for Checking Admin Fee and Supplier Fee should not be Greater Then Booking Amount
                    if (feeAmount < 0 || bookingAmount < feeAmount)
                    {
                        lblMessage.Text = "Admin Fee and Supplier Fee should not be Greater Then Booking Amount";
                    }
                    else
                    {
                        BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(e.CommandArgument.ToString()), ProductType.Hotel));
                        HotelItinerary itinerary = new HotelItinerary();
                        HotelRoom room = new HotelRoom();
                        HotelPassenger passInfo = new HotelPassenger();
                        AgentMaster agent = new AgentMaster(bookingDetail.AgencyId);
                        Product[] products = BookingDetail.GetProductsLine(bookingDetail.BookingId);
                        decimal bookingAmt = 0;
                        for (int i = 0; i < products.Length; i++)
                        {
                            if (products[i].ProductTypeId == (int)ProductType.Hotel)
                            {
                                itinerary.Load(products[i].ProductId);
                                passInfo.Load(products[i].ProductId);
                                itinerary.HotelPassenger = passInfo;
                                itinerary.Roomtype = room.Load(products[i].ProductId);
                                break;
                            }
                        }

                        decimal discount = 0;
                        foreach (HotelRoom hroom in itinerary.Roomtype)
                        {
                            if (hroom.Price.NetFare > 0)
                            {
                                bookingAmt += (hroom.Price.NetFare + hroom.Price.Markup + hroom.Price.B2CMarkup);//Need to refund B2B markup also
                                                                                                                 // bookingAmt += (hroom.Price.NetFare + hroom.Price.B2CMarkup);//Added only B2c Markup by chandan 
                            }
                            //else
                            //{
                            //    bookingAmt += (hroom.Price.PublishedFare + hroom.Price.Tax);
                            //}
                            discount += hroom.Price.Discount;
                        }

                        //bookingAmt -= discount;
                        bookingAmt = Math.Ceiling(bookingAmt);
                        // Hashtable ht = new Hashtable();
                        Dictionary<string, Dictionary<string, string>> CancelDetails = new Dictionary<string, Dictionary<string, string>>();
                        Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                        if (itinerary.Source == HotelBookingSource.DOTW)
                        {
                            MetaSearchEngine mse = new MetaSearchEngine();
                            cancellationData = mse.CancelHotel(itinerary, null);
                        }
                        else if (itinerary.Source == HotelBookingSource.HotelBeds)  // Added by brahmam 26.09.2014
                        {
                            CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                            cancellationData = hBeds.CancelBooking(itinerary);
                        }
                        //else if (itinerary.Source == HotelBookingSource.RezLive)
                        //{
                        //    RezLive.XmlHub rezAPI = new RezLive.XmlHub();
                        //    cancellationData = rezAPI.CancelHotelBooking(itinerary.ConfirmationNo, itinerary.BookingRefNo);
                        //}
                        else if (itinerary.Source == HotelBookingSource.LOH)
                        {
                            LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine();
                            cancellationData = jxe.CancelHotel(itinerary.ConfirmationNo);
                        }
                        else if (itinerary.Source == HotelBookingSource.GTA)
                        {
                            CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                            cancellationData = gtaApi.CancelHotelBooking(itinerary);
                        }
                        else if (itinerary.Source == HotelBookingSource.TBOHotel)
                        {
                            string remarks = lblRemarks.Text.Trim();
                            TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                            cancellationData = tboHotel.CancelHotelBooking(itinerary.BookingRefNo, remarks);
                        }
                        else if (itinerary.Source == HotelBookingSource.Miki)
                        {
                            CT.BookingEngine.GDS.MikiApi mikiApi = new CT.BookingEngine.GDS.MikiApi(Settings.LoginInfo.LocationCountryCode);
                            string[] confirmationCodes = itinerary.ConfirmationNo.Split('|');
                            foreach (string confirmationCode in confirmationCodes)
                            {
                                cancellationData = mikiApi.CancelBooking(confirmationCode);
                                CancelDetails.Add(confirmationCode, cancellationData);
                                // Dictionary<string, string>[] d = new Dictionary<string, string>[2];

                            }

                        }
                        else if (itinerary.Source == HotelBookingSource.HotelConnect)
                        {
                            CZInventory.SearchEngine his = new CZInventory.SearchEngine();
                            cancellationData = his.CancelBooking(itinerary.ConfirmationNo, true);
                        }
                        //WST Cancellation added by brahmam 27.06.2016
                        else if (itinerary.Source == HotelBookingSource.WST)
                        {
                            string remarks = lblRemarks.Text.Trim();
                            CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                            cancellationData = wstApi.CancelBooking(itinerary.ConfirmationNo, remarks);
                        }
                        //JAC Source Added by brahmam 20.09.2016
                        else if (itinerary.Source == HotelBookingSource.JAC)
                        {
                            CT.BookingEngine.GDS.JAC jacApi = new CT.BookingEngine.GDS.JAC();
                            cancellationData = jacApi.CancelHotelBooking(itinerary.ConfirmationNo);
                        }
                        //EET Source Added by brahmam 10.03.2016
                        else if (itinerary.Source == HotelBookingSource.EET)
                        {
                            CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET();
                            cancellationData = eetApi.CancelHotel(itinerary.ConfirmationNo);
                        }
                        //Agoda Source Added by brahmam 08.02.2018
                        else if (itinerary.Source == HotelBookingSource.Agoda)
                        {
                            CT.BookingEngine.GDS.Agoda agodaApi = new CT.BookingEngine.GDS.Agoda();
                            cancellationData = agodaApi.CancelHotelBooking(itinerary.ConfirmationNo);
                        }
                        //Yatra Source Added by somasekhar on 14/09/2018
                        else if (itinerary.Source == HotelBookingSource.Yatra)
                        {
                            CT.BookingEngine.GDS.Yatra yatraApi = new CT.BookingEngine.GDS.Yatra();
                            yatraApi.AppUserId = loggedMemberId;
                            cancellationData = yatraApi.CancelHotelBooking(itinerary);
                        }
                        //GRN Source Added by Harish 09.19.2018
                        #region GRN
                        else if (itinerary.Source == HotelBookingSource.GRN)
                        {
                            CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                            grnApi.GrnUserId = Settings.LoginInfo.UserID;
                            cancellationData = grnApi.CancelHotelBooking(itinerary.BookingRefNo);
                        }
                        #endregion
                        //OYO Source Added by Somasekhar on 20/12/2018
                        else if (itinerary.Source == HotelBookingSource.OYO)
                        {
                            CT.BookingEngine.GDS.OYO oyoApi = new CT.BookingEngine.GDS.OYO();
                            oyoApi.AppUserId = loggedMemberId;
                            cancellationData = oyoApi.CancelHotelBooking(itinerary);
                        }
                        //Get Cancel Info for Gimmonix  source 
                        else if (itinerary.Source == HotelBookingSource.GIMMONIX || itinerary.Source == HotelBookingSource.UAH|| itinerary.Source == HotelBookingSource.Illusions|| itinerary.Source == HotelBookingSource.RezLive)
                        {
                            try
                            {
                                string apiUrl = string.Empty;
                                apiUrl = ConfigurationManager.AppSettings["WebApiHotelUrl"].ToString();
                                if (string.IsNullOrEmpty(apiUrl))
                                {
                                    apiUrl = Request.Url.Scheme + "://" + Request.Url.Host + "/HotelWebApi";
                                    // if (Request.IsSecureConnection == true)
                                    // {
                                    //     apiUrl = "https://" + Request.Url.Host + "/HotelWebApi";
                                    // }
                                    // else
                                    // {
                                    //     apiUrl = "http://" + Request.Url.Host + "/HotelWebApi";
                                    //  }
                                }
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL:" + apiUrl, "");
                                ////string apiUrl = ConfigurationManager.AppSettings["WebApiHotelUrl"];
                                //string apiUrl = string.Empty;
                                //if (Request.IsSecureConnection == true)
                                //{
                                //    apiUrl = "https://" + Request.Url.Host + "/HotelWebApi";
                                //}
                                //else
                                //{
                                //    apiUrl = "http://" + Request.Url.Host+ "/HotelWebApi";
                                //}
                                string cancelRefNo = string.Empty;
                                string cancelSource = string.Empty;
                                if (itinerary.Source == HotelBookingSource.GIMMONIX)
                                {
                                    cancelRefNo = itinerary.BookingRefNo;
                                    cancelSource = "GIMMONIX";
                                }
                                if (itinerary.Source == HotelBookingSource.UAH)
                                {
                                    cancelSource = "UAH";
                                    cancelRefNo = itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo;  //"DHB190430162048432|3900673|3871174" //
                                }
                                if (itinerary.Source == HotelBookingSource.RezLive)
                                {
                                    cancelSource = "RezLive";
                                    cancelRefNo = itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo;  //"DHB190430162048432|3900673|3871174" //
                                }
                                if (itinerary.Source == HotelBookingSource.Illusions)
                                {
                                    cancelSource = "Illusions";
                                    string roomKey = String.Join("|", itinerary.Roomtype.Select(x => x.RoomId + "-" + x.RoomTypeCode.Split('-')[3]).ToArray());
                                    cancelRefNo = itinerary.HotelCode.Split('-')[0] + "|" + itinerary.ConfirmationNo + "^" + itinerary.BookingRefNo + "^" + roomKey+"^"+itinerary.HotelId;  //"32|7729^7729/1|7729/2^3077-1|3078-2^2477" //
                                }
                                object input = new
                                {
                                    agentId = itinerary.AgencyId,
                                    userId = itinerary.CreatedBy,
                                    source = cancelSource,// itinerary.Source.ToString(),
                                    //source = itinerary.Source == HotelBookingSource.GIMMONIX ? "GIMMONIX" : "UAH",
                                    // bookingRefNo = itinerary.Source == HotelBookingSource.GIMMONIX ? itinerary.BookingRefNo : itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo  //"DHB190430162048432|3900673|3871174" //
                                    bookingRefNo = cancelRefNo
                                };
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 200 :" + apiUrl, "");
                                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 300 :" + apiUrl, "");
                                WebClient client = new WebClient();
                                client.Headers["Content-type"] = "application/json";
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:input 400 :" + inputJson, "");
                                string CancelInfo = client.UploadString(apiUrl + "/api/HotelCancellation/GetCancelInfo", inputJson);
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 500 :" + apiUrl, "");
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:inputJson:" + inputJson, "");
                                cancellationData = (new JavaScriptSerializer()).Deserialize<Dictionary<string, string>>(CancelInfo);
                            }
                            catch (Exception exClient)
                            {
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:exClient" + exClient.ToString(), "");
                            }
                            //cancellationData = HttpContext.Current.Session["ApiCancelInfo"] as Dictionary<string, string>;
                        }
                        //Get Cancel Info for Illusion  source
                        //else if(itinerary.Source==HotelBookingSource.Illusions)
                        //{
                        //    CT.BookingEngine.GDS.IllusionsApi illusion = new CT.BookingEngine.GDS.IllusionsApi();
                        //    string illusionSource = itinerary.HotelCode.Split('-')[0];
                        //    string confirmaionNo = itinerary.ConfirmationNo;
                        //    string[] SubResNos = itinerary.BookingRefNo.Split('|');
                        //    foreach (string SubResNo in SubResNos)
                        //    {
                        //        illusion.CancelHotelBooking(illusionSource, confirmaionNo, SubResNo);
                        //    }                           
                        //    cancellationData = illusion.RetriveHotelBookingRquest(itinerary);
                        //}
                        //Added conditon For get cancellation details of Miki & Non-Miki product
                        if (itinerary.Source == HotelBookingSource.Miki)
                        {
                            string currency = "";
                            decimal CancelAmount = 0;
                            //string cancelId = "";
                            foreach (KeyValuePair<string, Dictionary<string, string>> cancelDetail in CancelDetails)
                            {
                                cancellationData = cancelDetail.Value;


                                if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") //Added by chandan
                                {
                                    if (itinerary.CancelId != null && itinerary.CancelId.Length > 0)
                                    {
                                        itinerary.CancelId += "|" + cancellationData["ID"];
                                    }
                                    else
                                    {
                                        itinerary.CancelId = cancellationData["ID"]; ;
                                    }
                                    if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0)
                                    {
                                        CancelAmount += Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));

                                    }
                                    currency = cancellationData["Currency"];

                                }

                                else
                                {
                                    lblMessage.Text = "Failed Cancellation from " + itinerary.Source.ToString() + " Confirmation No: " + itinerary.ConfirmationNo;
                                    Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), lblMessage.Text, null);

                                }

                            }
                            //cancellationData.Add("Amount",Convert.ToString(CancelAmount));
                            // cancellationData.Add("currency", currency);
                            //cancellationData.Clear();

                            //Initialize again with update price and ID For Miki+Non Miki Item
                            cancellationData["Amount"] = Convert.ToString(CancelAmount);
                            cancellationData["Currency"] = Convert.ToString(currency);
                            cancellationData["ID"] = itinerary.CancelId;
                        }
                        else if (itinerary.Source == HotelBookingSource.HotelExtranet)
                        {
                            //CZInventory.SearchEngine his = new CZInventory.SearchEngine();
                            CZInventory.CZHISSearchEngine his = new CZInventory.CZHISSearchEngine();
                            cancellationData = his.CancelBooking(itinerary.ConfirmationNo, true);
                        }
                        if ((cancellationData.ContainsKey("Status")&& (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") )||(cancellationData.ContainsKey("status")&& cancellationData["status"] == "Cancelled")) //Added by brahmam
                        {

                            itinerary.Status = HotelBookingStatus.Cancelled;
                            if (itinerary.Source == HotelBookingSource.DOTW)
                            {
                                itinerary.CancelId = cancellationData["ID"];
                            }
                            else if (itinerary.Source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
                            {
                                itinerary.CancelId = itinerary.ConfirmationNo;
                            }
                            else if (itinerary.Source == HotelBookingSource.RezLive)
                            {
                                //Temp code for testing
                                itinerary.CancelId = itinerary.ConfirmationNo;
                            }
                            else if (itinerary.Source == HotelBookingSource.GTA)
                            {
                                itinerary.CancelId = itinerary.ConfirmationNo;
                            }
                            else if (itinerary.Source == HotelBookingSource.WST)
                            {
                                itinerary.CancelId = itinerary.ConfirmationNo;
                            }
                            else if (itinerary.Source == HotelBookingSource.HotelConnect)
                            {
                                itinerary.CancelId = itinerary.ConfirmationNo;
                            }
                            else if (itinerary.Source == HotelBookingSource.TBOHotel)
                            {
                                itinerary.CancelId = cancellationData["ID"];
                            }
                            // Condition  ---  itinerary.Source == HotelBookingSource.Yatra  
                            // Condition added by somasekhar on 14/09/02018 for Yatra
                            // Condition  ---  itinerary.Source == HotelBookingSource.OYO  
                            // Condition added by somasekhar on 20/1202018 for OYO
                            else if (itinerary.Source == HotelBookingSource.JAC || itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.OYO)
                            {
                                itinerary.CancelId = cancellationData["ID"];
                            }
                            else if (itinerary.Source == HotelBookingSource.GRN)
                            {
                                itinerary.CancelId = cancellationData["ID"];
                            }
                            else if (itinerary.Source == HotelBookingSource.GIMMONIX)
                            {
                                itinerary.CancelId = cancellationData.ContainsKey("ID") ? cancellationData["ID"]: cancellationData["id"];
                            }
                            else if (itinerary.Source == HotelBookingSource.Illusions)
                            {
                                itinerary.CancelId = cancellationData.ContainsKey("ID")? cancellationData["ID"]: cancellationData["id"];

                            }
                            else if (itinerary.Source == HotelBookingSource.HotelExtranet)
                            {
                                itinerary.CancelId = itinerary.ConfirmationNo;
                            }
                            itinerary.Update();

                            serviceRequest = Session["ServiceRequests"] as ServiceRequest[];

                            //                        loggedMemberId = (int)Settings.LoginInfo.UserID;
                            ServiceRequest sr = new ServiceRequest();

                            //TODO: Create a PaymentDetails for cancellation details and update the cancellation charges: Shiva

                            CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                            cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                            cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                            cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                            cancellationCharge.ReferenceId = itinerary.Roomtype[0].RoomId;
                            decimal exchangeRate = 0;
                            if ((cancellationData.ContainsKey("Currency")&&cancellationData["Currency"] != agent.AgentCurrency)|| (cancellationData.ContainsKey("currency") && cancellationData["currency"] != agent.AgentCurrency))
                            {
                                StaticData staticInfo = new StaticData();
                                staticInfo.BaseCurrency = agent.AgentCurrency;

                                Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                if (itinerary.Source == HotelBookingSource.GRN && cancellationData.ContainsKey("BookingId") && cancellationData.ContainsKey("ID") && (cancellationData["BookingId"] == cancellationData["ID"]))
                                {
                                    cancellationData["Currency"] = agent.AgentCurrency;
                                    string cancelAmount = cancellationData["Amount"];
                                    if (cancelAmount.Contains("%"))
                                    {
                                        cancelAmount = cancelAmount.Replace('%', ' ');
                                        cancellationData["Amount"] = cancelAmount;
                                        cancellationData["Amount"] = Convert.ToString(bookingAmt * Convert.ToDecimal(cancellationData["Amount"]) / 100);
                                    }
                                }
                                exchangeRate = cancellationData.ContainsKey("Currency")? rateOfExList[cancellationData["Currency"]]: rateOfExList[cancellationData["currency"]];
                                cancellationCharge.CancelPenalty = cancellationData.ContainsKey("Amount")?(Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate): (Convert.ToDecimal(cancellationData["amount"]) * exchangeRate);
                                //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                if (itinerary.Source == HotelBookingSource.Yatra)
                                {
                                    //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                    //Yatra supplier directly giving refund amt, then here we are calculating Cancellation charge Amt 
                                    cancellationCharge.CancelPenalty = bookingAmt - Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate));
                                }
                            }
                            else
                            {
                                cancellationCharge.CancelPenalty = cancellationData.ContainsKey("Amount") ? Convert.ToDecimal(cancellationData["Amount"]): Convert.ToDecimal(cancellationData["amount"]);
                                //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                if (itinerary.Source == HotelBookingSource.Yatra)
                                {
                                    //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                    //Yatra supplier directly giving refund amt, then here we are calculating Cancellation charge Amt 
                                    cancellationCharge.CancelPenalty = bookingAmt - Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                                }
                            }
                            cancellationCharge.CreatedBy = loggedMemberId;
                            cancellationCharge.ProductType = ProductType.Hotel;
                            cancellationCharge.Save();
                            //(itinerary.Source == HotelBookingSource.Yatra && Convert.ToDecimal(cancellationData["Amount"]) >= 0)
                            //above condition added for yatra. If non refundable cancellation they are giving refound amount "0"
                            if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0 || (itinerary.Source == HotelBookingSource.Yatra && Convert.ToDecimal(cancellationData["Amount"]) >= 0))
                            {
                                try
                                {
                                    //if (itinerary.Source == HotelBookingSource.DOTW)
                                    //{
                                    //    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]);
                                    //}
                                    //else if (itinerary.Source != HotelBookingSource.DOTW)
                                    //{
                                    //decimal exchangeRate = 0;
                                    if (cancellationData["Currency"] != agent.AgentCurrency)
                                    {
                                        StaticData staticInfo = new StaticData();
                                        staticInfo.BaseCurrency = agent.AgentCurrency;
                                        Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                        exchangeRate = rateOfExList[cancellationData["Currency"]];
                                    }
                                    if (exchangeRate <= 0)
                                    {
                                        bookingAmt -= Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                                        //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                        if (itinerary.Source == HotelBookingSource.Yatra)
                                        {
                                            //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                            // If non refundable cancellation they are giving refound amount "0"
                                            //the bellow we get cancellation refund amount=0, then in ledger we updeted refund amount 0;
                                            bookingAmt -= (bookingAmt - (Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]))));
                                        }
                                    }
                                    else if (itinerary.Source != HotelBookingSource.HotelConnect)
                                    {
                                        bookingAmt -= Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate));
                                        //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                        if (itinerary.Source == HotelBookingSource.Yatra)
                                        {
                                            //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                            // If non refundable cancellation they are giving refound amount "0"
                                            //the bellow we get cancellation refund amount=0, then in ledger we updeted refund amount 0;
                                            bookingAmt -= (bookingAmt - (Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate))));
                                        }
                                    }
                                    //}
                                }
                                catch { }
                            }
                            bookingAmt = Math.Ceiling(bookingAmt);
                            // Admin & Supplier Fee save in Leadger
                            int invoiceNumber = 0;
                            decimal adminChar = Math.Ceiling(Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text));
                            //decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                            decimal adminFee = 0;
                            if (adminChar < bookingAmt)
                            {
                                adminFee = adminChar;
                            }
                            LedgerTransaction ledgerTxn = new LedgerTransaction();
                            NarrationBuilder objNarration = new NarrationBuilder();
                            invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                            //ledgerTxn = new LedgerTransaction();
                            ledgerTxn.LedgerId = bookingDetail.AgencyId;
                            ledgerTxn.Amount = -adminFee;
                            objNarration.HotelConfirmationNo = itinerary.ConfirmationNo.Replace('|', '@'); ///modified by brahmam MIKI purpose
                            objNarration.TravelDate = itinerary.StartDate.ToShortDateString();
                            if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelCancellationCharge;
                                objNarration.Remarks = "Card Hotel Cancellation Charges";
                            }
                            else
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelCancellationCharge;
                                objNarration.Remarks = "Hotel Cancellation Charges";
                            }
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.IsLCC = true;
                            ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                            ledgerTxn.Notes = "";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = loggedMemberId;
                            ledgerTxn.TransType = itinerary.TransType;
                            ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                            ledgerTxn.Save();
                            LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                            //save Refund amount
                            ledgerTxn = new LedgerTransaction();
                            ledgerTxn.LedgerId = bookingDetail.AgencyId;
                            ledgerTxn.Amount = bookingAmt;
                            objNarration.PaxName = string.Format("{0} {1}", itinerary.HotelPassenger.Firstname, itinerary.HotelPassenger.Lastname);
                            if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit  Added by brahmam
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelRefund;
                                ledgerTxn.Notes = "Card Hotel Voucher Refunded";
                                objNarration.Remarks = "Card Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                            }
                            else
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelRefund;
                                ledgerTxn.Notes = "Hotel Voucher Refunded";
                                objNarration.Remarks = "Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                            }
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.IsLCC = true;
                            ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = loggedMemberId;
                            ledgerTxn.TransType = itinerary.TransType;
                            ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                            ledgerTxn.Save();
                            LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);


                            if (adminFee <= bookingAmt)
                            {
                                bookingAmt -= adminFee;
                            }
                            //agent = new AgentMaster(bookingDetail.AgencyId);
                            if (itinerary.PaymentMode != ModeOfPayment.CreditCard)
                            {
                                if (itinerary.TransType != "B2C")
                                {
                                    //Settings.LoginInfo.AgentBalance += bookingAmt;
                                    agent.CreatedBy = Settings.LoginInfo.UserID;
                                    agent.UpdateBalance(bookingAmt);

                                    if (bookingDetail.AgencyId == Settings.LoginInfo.AgentId)
                                    {
                                        Settings.LoginInfo.AgentBalance += bookingAmt;
                                    }
                                }
                            }


                            //Update Queue Status
                            CT.Core.Queue.SetStatus(QueueType.Request, serviceRequest[e.Item.ItemIndex].RequestId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                            sr.UpdateServiceRequestAssignment(serviceRequest[e.Item.ItemIndex].RequestId, (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);

                            //Sending Email.
                            Hashtable table = new Hashtable();
                            table.Add("agentName", agent.Name);
                            table.Add("hotelName", itinerary.HotelName);
                            table.Add("confirmationNo", itinerary.ConfirmationNo);

                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                            AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                            toArray.Add(bookedBy.Email);
                            toArray.Add(bookedAgency.Email1);
                            //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                            string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(';');
                            foreach (string cnMail in cancelMails)
                            {
                                toArray.Add(cnMail);
                            }
                            //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> has been processed. Confirmation No:(" + itineary.ConfirmationNo + ")";
                            string message = ConfigurationManager.AppSettings["HOTEL_REFUND"];
                            if (!Request.Url.Host.ToLower().Contains("ibyta.com"))
                            {
                                message = message.Replace("Cozmo Travel", "ibyta");
                            }
                                try
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Hotel)Response for cancellation. Confirmation No:(" + itinerary.ConfirmationNo + ")", message, table);
                            }
                            catch { }

                            //TODO Ziyad
                            //Generate an Invoice for the cancellation amount.
                        }
                        else
                        {
                            if (itinerary.Source == HotelBookingSource.TBOHotel)
                            {
                                lblMessage.Text = "Your request is inprocess,kindly contact supplier.";
                            }
                            else if (itinerary.Source == HotelBookingSource.GIMMONIX && cancellationData["Status"] == "ERC")
                            {
                                lblMessage.Text = "Please contact the supplier to verify the reservation status and/or cancel it directly on the supplier system.";
                            }
                            else
                            {
                                lblMessage.Text = "Failed Cancellation from " + itinerary.Source.ToString();
                            }
                        }
                        LoadChangeQueues();
                        //Response.Redirect("ChangeRequestQueue.aspx",false);
                    }
                }
                else
                {
                    lblMessage.Text = "Please enter Admin & Supplier Fee";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
    }
    protected void dlChangeRequests_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HotelItinerary itinerary = e.Item.DataItem as HotelItinerary;

                if (itinerary != null)
                {
                    Label lblSupplier = e.Item.FindControl("lblSupplier") as Label;
                    Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                    Label lblGuests = e.Item.FindControl("lblGuests") as Label;
                    Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                    Label lblBookingPrice = e.Item.FindControl("lblBookingPrice") as Label;
                    Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                    TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    txtAdminFee.Attributes.Add("onblur", "Validate(" + e.Item.ItemIndex + ")");
                    Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                    Label lblAgent = e.Item.FindControl("lblAgent") as Label;
                    btnRefund.OnClientClick = "return Validate('" + e.Item.ItemIndex + "');";

                    Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                    Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                    Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                    Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                    Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;
                    Label lblCreatedBy = e.Item.FindControl("lblCreatedBy") as Label;
                    Label lblAdminCurrency = e.Item.FindControl("lblAdminCurrency") as Label;
                    Label lblSupplierCurrency = e.Item.FindControl("lblSupplierCurrency") as Label;

                    int adults = 0, childs = 0;
                    decimal totalPrice = 0, outVatAmount = 0;
                    if (itinerary.Roomtype != null)
                    {
                        for (int k = 0; k < itinerary.Roomtype.Length; k++)
                        {
                            adults += itinerary.Roomtype[k].AdultCount;
                            childs += itinerary.Roomtype[k].ChildCount;
                            if (itinerary.Roomtype[0].Price.AccPriceType == PriceType.PublishedFare)
                            {
                                totalPrice += itinerary.Roomtype[k].Price.PublishedFare - itinerary.Roomtype[k].Price.Discount;
                            }
                            else
                            {
                                totalPrice += itinerary.Roomtype[k].Price.NetFare + itinerary.Roomtype[k].Price.Markup + itinerary.Roomtype[k].Price.B2CMarkup - itinerary.Roomtype[k].Price.Discount;
                                outVatAmount += itinerary.Roomtype[k].Price.OutputVATAmount;
                            }
                            if (itinerary.Source == HotelBookingSource.GIMMONIX)
                            {
                                if (!string.IsNullOrEmpty(itinerary.Roomtype[k].Gxsupplier))
                                {
                                    if (string.IsNullOrEmpty(lblSupplier.Text))
                                        lblSupplier.Text = itinerary.Roomtype[k].Gxsupplier;
                                    else
                                        lblSupplier.Text += "," + itinerary.Roomtype[k].Gxsupplier;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(lblSupplier.Text))
                                        lblSupplier.Text = "GX";
                                }
                            }
                        }
                        totalPrice = Math.Ceiling(totalPrice) + Math.Ceiling(outVatAmount);
                        //Bind the Hotel Booking price
                        lblPrice.Text = (itinerary.Roomtype[0].Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Roomtype[0].Price.Currency) : "AED ") + " " + Math.Ceiling(totalPrice);
                        lblBookingPrice.Text = Math.Ceiling(totalPrice).ToString();
                        lblAdminCurrency.Text = itinerary.Roomtype[0].Price.Currency;
                        lblSupplierCurrency.Text = itinerary.Roomtype[0].Price.Currency;

                        if (itinerary.Roomtype.Length > 0 && itinerary.Roomtype[0].PassenegerInfo[0].Title != null)
                        {
                            lblPaxName.Text = (itinerary.Roomtype[0].PassenegerInfo[0].Title) + itinerary.Roomtype[0].PassenegerInfo[0].Firstname + " " + itinerary.Roomtype[0].PassenegerInfo[0].Lastname;
                        }
                    }
                    lblGuests.Text = adults + childs + " Adults (" + adults + ") Childs (" + childs + ")";
                    if (itinerary.Source != HotelBookingSource.GIMMONIX)
                    {
                        lblSupplier.Text = (itinerary.Source.ToString());
                    }
                    lblStatus.Text = itinerary.Status.ToString();
                    int tranxContryId = 0;// to check india to open refund button
                    try
                    {
                        //---------------------------Retrieve Agent-------------------------------------------
                        int bkgId = BookingDetail.GetBookingIdByProductId(itinerary.ProductId, itinerary.ProductType);
                        BookingDetail bkgDetail = new BookingDetail(bkgId);
                        AgentMaster agent = new AgentMaster(bkgDetail.AgencyId);
                        lblAgent.Text = agent.Name;
                        lblAgencyBalance.Text = agent.CurrentBalance.ToString("0.000");
                        lblAgencyEmail.Text = agent.Email1;
                        lblAgencyName.Text = agent.Name;
                        lblAgencyPhone1.Text = agent.Phone1;
                        lblAgencyPhone2.Text = agent.Phone2;
                        tranxContryId = agent.Country;
                        //------------------------------------------------------------------------------------
                    }
                    catch { }
                    //if (DateTime.Now.CompareTo(itinerary.LastCancellationDate) > 0)
                    if (tranxContryId == 3 && itinerary.Status != HotelBookingStatus.Cancelled)// For india is open
                    {
                        txtAdminFee.Visible = true;
                        btnRefund.Visible = true;
                    }
                    else if (DateTime.Now.CompareTo(itinerary.StartDate) > 0)
                    {
                        //if (itinerary.LastCancellationDate.Subtract(DateTime.Now).Days <= 0 && itinerary.Status != HotelBookingStatus.Cancelled)
                        //{
                        //    txtAdminFee.Visible = true;
                        //    btnRefund.Visible = true;
                        //}
                        //else
                        //{
                        txtAdminFee.Visible = false;
                        btnRefund.Visible = false;
                        //}
                    }
                    else
                    {
                        //if (itinerary.LastCancellationDate.Subtract(DateTime.Now).Days >= 0 && itinerary.Status != HotelBookingStatus.Cancelled)
                        if (itinerary.StartDate.Subtract(DateTime.Now).Days >= 0 && itinerary.Status != HotelBookingStatus.Cancelled)
                        {
                            txtAdminFee.Visible = true;
                            btnRefund.Visible = true;
                        }
                        else
                        {
                            txtAdminFee.Visible = false;
                            btnRefund.Visible = false;
                        }
                    }

                    if (lblPaxName.Text == "")
                    {
                        e.Item.Visible = false;
                    }
                    try
                    {
                        UserMaster createdBy = new UserMaster(Convert.ToInt64(itinerary.CreatedBy));
                        lblCreatedBy.Text = createdBy.FirstName + " " + createdBy.LastName;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindLocation(Utility.ToInteger(ddlAgents.SelectedItem.Value), type);
    }
}
