﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.MetaSearchEngine;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using System.Collections.Generic;
using CT.Configuration;
using System.Globalization;
using CT.Corporate;
using CozmoB2BWebApp;
using System.Linq;

public partial class FlightResultUI : CT.Core.ParentPage//System.Web.UI.Page
{
    #region Members
    protected ArrayList toCity;
    protected ArrayList fromCity;
    protected ArrayList tocityAirports;
    protected ArrayList fromcityAirports;
    protected int agencyId;
    protected AgentMaster agency;
    protected bool isServiceAgency;
    protected string morningChk = string.Empty;
    protected string afternoonChk = string.Empty;
    protected string eveningChk = string.Empty;
    protected string nightChk = string.Empty;
    protected string directChk = string.Empty;
    protected string oneStopChk = string.Empty;
    protected string moreThanTwoStopChk = string.Empty;
    protected int pageNo = 1;
    protected int resultsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["FlightSearchResultPerPage"]);
    protected string show = string.Empty;
    protected string sortTypePaxDetail = string.Empty;
    protected string sortOrderPaxDetail = string.Empty;
    protected List<string> layoverAirports = new List<string>();
    protected List<string> originAirports = new List<string>();
    protected List<string> destinationAirports = new List<string>();
    protected List<string> airlineList = new List<string>();
    protected List<string> airlineRemovedList = new List<string>();
    protected List<string> airlineCodeList = new List<string>();
    protected List<string> airportRemovedList = new List<string>();
    protected SearchResult[] resultDisplay = new SearchResult[0];
    int currentPageNo = 1;
    
    bool changedDaySearch = false;
    protected int maxResult, minResult;
    protected string sessionId;
    protected string priceImgSrc = "images/spacer.gif";
    protected string depImgSrc = "images/spacer.gif";
    protected string arrImgSrc = "images/spacer.gif";
    protected string durImgSrc = "images/spacer.gif";
    protected Dictionary<string, string> Airlines = new Dictionary<string, string>();
    bool isClearPageCount = false;
    protected SearchResult[] filteredResults;
   
    #endregion
    //modify search
    protected string userType;
    protected SearchRequest searchRequest = new SearchRequest();
    protected SearchRequest request;
    protected string preferredAirlines = string.Empty;
    protected string airlineMessage = string.Empty;
    protected DataTable dtTravelReason = new DataTable();
    protected int filteredCount;
    protected string resultIds = string.Empty;
    protected List<string> filteredOnwardTimings = new List<string>();
    protected List<string> filteredReturnTimings = new List<string>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Settings.LoginInfo != null)
        {
            request = Session["FlightRequest"] as SearchRequest;
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agency = new AgentMaster(Settings.LoginInfo.AgentId);
            }
            else
            {
                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            }
            if (!IsPostBack)
            {
                if (request.Type != SearchType.Return)//Hide return timings filter
                    ddlReturnTimings.Visible = false;

                if (Settings.LoginInfo.IsCorporate == "Y")
                {
                    // ddlBookingClass.Items.Add(new ListItem("Any", Convert.ToInt32(CabinClass.All).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.Economy.ToString(), Convert.ToInt32(CabinClass.Economy).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.PremiumEconomy.ToString(), Convert.ToInt32(CabinClass.PremiumEconomy).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.Business.ToString(), Convert.ToInt32(CabinClass.Business).ToString()));
                    // ddlBookingClass.Items.Add(new ListItem(CabinClass.First.ToString(), Convert.ToInt32(CabinClass.First).ToString()));
                }
                else
                {
                    ddlBookingClass.Items.Add(new ListItem("Any", Convert.ToInt32(CabinClass.All).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.Economy.ToString(), Convert.ToInt32(CabinClass.Economy).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.PremiumEconomy.ToString(), Convert.ToInt32(CabinClass.PremiumEconomy).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.Business.ToString(), Convert.ToInt32(CabinClass.Business).ToString()));
                    ddlBookingClass.Items.Add(new ListItem(CabinClass.First.ToString(), Convert.ToInt32(CabinClass.First).ToString()));


                }
                

                //Based On Bke_App_config value Restricting th Multi City Option .
                AgentAppConfig clsAppCnf = new AgentAppConfig();
                clsAppCnf.AgentID = (int)agency.ID;
                var liAppConfig = clsAppCnf.GetConfigData();
                if (liAppConfig.Where(x => x.AppKey.ToUpper() == "MULTICITYALLOWED").Select(y => y.AppValue).FirstOrDefault() == "False")
                {
                    multicity.Style.Add("display", "none");
                }


                var showTimings = liAppConfig.Exists(x => x.AppKey.ToLower() == "showtimeintervals" && x.AppValue.ToLower() == "true");

                //If ShowTimeIntervals is set to TRUE in BKE_APP_CONFIG for the login agent then clear existing values and add half hour time intervals in 24 hour format
                if (showTimings)
                {
                    ddlDepTime.Items.Clear();
                    ddlReturnTime.Items.Clear();

                    ddlDepTime.Items.Add(new ListItem("Any Time", "Any"));
                    ddlReturnTime.Items.Add(new ListItem("Any Time", "Any"));

                    for (int h = 0; h < 24; h++)
                    {
                        if (h > 0)
                        {
                            int hour = h, prevHour = h - 1;
                            string prevHourFormat = string.Empty, hourFormat = string.Empty;

                            if (prevHour.ToString().Length == 1)
                                prevHourFormat = "0" + prevHour;
                            else
                                prevHourFormat = prevHour.ToString();

                            if (hour.ToString().Length == 1)
                                hourFormat = "0" + hour;
                            else
                                hourFormat = hour.ToString();


                            ddlDepTime.Items.Add(new ListItem(prevHourFormat + ":30 - " + hourFormat + ":00", prevHourFormat + ":30 - " + hourFormat + ":00"));
                            ddlDepTime.Items.Add(new ListItem(hourFormat + ":00 - " + hourFormat + ":30", hourFormat + ":00 - " + hourFormat + ":30"));
                            ddlReturnTime.Items.Add(new ListItem(prevHourFormat + ":30 - " + hourFormat + ":00", prevHourFormat + ":30 - " + hourFormat + ":00"));
                            ddlReturnTime.Items.Add(new ListItem(hourFormat + ":00 - " + hourFormat + ":30", hourFormat + ":00 - " + hourFormat + ":30"));
                        }
                        else
                        {
                            ddlDepTime.Items.Add(new ListItem("00:00 - 00:30", "00:00 - 00:30"));
                            ddlReturnTime.Items.Add(new ListItem("00:00 - 00:30", "00:00 - 00:30"));
                        }
                    }

                    ddlDepTime.Items.Add(new ListItem("23:30 - 00:00", "23:30 - 00:00"));
                    ddlReturnTime.Items.Add(new ListItem("23:30 - 00:00", "23:30 - 00:00"));
                }
            }
            if (Settings.LoginInfo.IsCorporate == "Y")
            {
                divPolicy.Style.Add("display", "block");
                chkpolicies.Style.Add("display", "block");
                if (hdnFilter.Value == "ClearFilter")
                {
                    chkInpolicy.Checked = true;
                    chkOutpolicy.Checked = true;
                }
            }
            
          

            if (Settings.LoginInfo.AgentId == 2125)
            {
                radioSelf.Checked = false;
                radioAgent.Checked = true;
                radioSelf.Visible = false;
                CT.TicketReceipt.Common.Utility.StartupScript(this, " disablefield();", "fileds");

            }
            Session["FZBaggageDetails"] = null;
            Session["TBOResult"] = null;
            Session["TBOBaggage"] = null;
            Session["TBOFare"] = null;
            Session["ISPRiceChangedTBO"] = null;
            Session["OtherCharges"] = null;
            Session["SearchResult"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;
            Session["ValuesChanged"] = null;//Clear the values used for UAPI Repricing
            Session["Error"] = null;
            Session["RepricedResult"] = null;

            if (Request.QueryString["error"] != null)
            {
                airlineMessage = Request.QueryString["error"];
                airlineMessage = airlineMessage.Replace("-", "<br/>");
            }

            if (Session["airlines"] != null)
            {
                airlineList = Session["airlines"] as List<string>;
            }

            // if (!IsPostBack)
            {
                //Load Corporate profile travel reasons

                //Note:Previously We do not have any predefined corporate profiles
                //Note:Now we have predefined corporate profiles -1 Client -2 Vendor
                //So verifying CorporateProfileId != 0

                if (Settings.LoginInfo.IsCorporate == "Y" && Settings.LoginInfo.CorporateProfileId != 0 && request != null && request.AppliedPolicy)
                {
                    dtTravelReason = TravelUtility.GetTravelReasonList("B", (int)agency.ID, ListStatus.Short);// B - Policy Breaking Reason
                }
            }

            if (hdnModifySearch.Value == "0" && string.IsNullOrEmpty(hdnFilter.Value))
            {
                hdnSourceBinding.Value = "true";

                //Adding Preferred airline to a string for checking using Javascript in .aspx
                foreach (string pa in request.Segments[0].PreferredAirlines)
                {
                    if (!string.IsNullOrEmpty(pa) && pa.Length <= 2)
                    {
                        if (preferredAirlines.Length > 0)
                        {
                            preferredAirlines += "," + pa;
                        }
                        else
                        {
                            preferredAirlines = pa;
                        }
                    }
                }



                if (Change.Value.Length <= 0)
                {
                    if (Session["FlightRequest"] != null)
                    {


                        if (Settings.LoginInfo.IsCorporate == "Y")
                        {
                            try
                            {
                                DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", (int)agency.ID, ListStatus.Short);// S- Policy checking 
                                ddlFlightTravelReasons.DataSource = dtTravelReasons;
                                ddlFlightTravelReasons.DataTextField = "Description";
                                ddlFlightTravelReasons.DataValueField = "ReasonId";
                                ddlFlightTravelReasons.DataBind();

                                int profileId = Settings.LoginInfo.IsOnBehalfOfAgent ? -1 : Settings.LoginInfo.CorporateProfileId;

                                DataTable dtPolicies = TravelUtility.GetProfileList(profileId, (int)agency.ID, ListStatus.Short);
                                ddlFlightEmployee.DataSource = dtPolicies;
                                ddlFlightEmployee.DataTextField = "ProfileName";
                                ddlFlightEmployee.DataValueField = "Profile";
                                ddlFlightEmployee.DataBind();
                                //if (ddlFlightEmployee.Items.Count > 1)
                                //{
                                //    ddlFlightEmployee.Items.Insert(0, "Select Traveller");
                                //}
                                //Check the refundable fares property for corporate
                                //Verify the key 
                                //If it is true.Check the check box and make it non visible on the UI
                                AgentAppConfig clsAppCnf = new AgentAppConfig();
                                clsAppCnf.AgentID = (int)agency.ID;
                                if (clsAppCnf.GetConfigData().Where(x => x.AppKey.Trim().ToUpper() == "REFUNDFARESONLY").Select(y => y.AppValue).FirstOrDefault().ToUpper() == "TRUE")
                                {
                                    //chkRefundFare.Checked = true; //temp
                                    //chkRefundFare.Checked = false;
                                    chkRefundFare.Visible = false;
                                }

                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.Search, Severity.High, 0, "Failed to Bind Corporate details for Flight Search. Reason :" + ex.ToString(), Request["REMOTE_ADDR"]);
                            }
                        }

                        //Load search controls after corporate controls are bound with data
                        LoadSearchControls();


                    }


                    //DataSet dsCities = CityMaster.GetCityList();
                    //string dest = dsCities.Tables[0].Select("cityCode='" + request.Segments[0].Destination + "'")[0]["cityName"].ToString();
                    //string org = dsCities.Tables[0].Select("cityCode='" + request.Segments[0].Origin + "'")[0]["cityName"].ToString();
                    string dest = request.Segments[0].Destination;
                    string org = request.Segments[0].Origin;
                    lblAdultCount.Text = request.AdultCount.ToString();
                    lblChildCount.Text = request.ChildCount.ToString();
                    lblDep.Text = request.Segments[0].PreferredArrivalTime.ToString("dd MMM yyyy");
                    lblDestination.Text = dest;
                    lblInfantCount.Text = request.InfantCount.ToString();
                    lblOrigin.Text = org;
                    lblDepDay.Text = "(" + request.Segments[0].PreferredDepartureTime.DayOfWeek.ToString() + ")";
                    if (request.Type == SearchType.Return)
                    {
                        lblRetDay.Text = "(" + request.Segments[1].PreferredDepartureTime.DayOfWeek.ToString() + ")";
                    }
                    lblClass.Text = request.Segments[0].flightCabinClass.ToString();

                    if (request.Segments[1] != null)
                    {
                        if (request.Segments[1].PreferredDepartureTime.ToString("dd MMM yyyy") != "01 Jan 0001")
                        {
                            lblRet.Text = request.Segments[1].PreferredDepartureTime.ToString("dd MMM yyyy");
                        }
                        else
                        {
                            lblRet.Text = "";
                        }
                    }

                    switch (request.Type)
                    {
                        case SearchType.MultiWay:
                            lblTrip.Text = "Multi Way";
                            break;
                        case SearchType.OneWay:
                            lblTrip.Text = "One Way";
                            break;
                        case SearchType.Return:
                            lblTrip.Text = "Return";
                            break;
                    }

                    if (Session["Error"] == null)
                    {
                        if (!IsCrossPagePostBack)
                        {
                            Session["FZBaggageDetails"] = null;
                            GetSearchResults(request);
                            hdnResultCount.Value = resultDisplay.Length.ToString();
                        }
                    }
                    //else
                    //{
                    //    lblMessage.Text = Session["Error"].ToString();
                    //    Session["Error"] = null;
                    //}
                }
            }
            else
            {
                if (hdnModifySearch.Value == "1")
                    hdnFilter.Value = "";

                if (Change.Value == "true")
                {
                    if (Session["airlines"] != null)
                    {
                        airlineList = Session["airlines"] as List<string>;
                    }
                    else
                    {
                        Response.Redirect("HotelSearch.aspx", false);
                    }

                    foreach (string airline in airlineList)
                    {
                        ListItem item = new ListItem(airline, airline.Split('(')[1].Replace(")", ""));
                        item.Selected = true;
                        if (!chkListAirlines.Items.Contains(item))
                        {
                            chkListAirlines.Items.Add(item);
                        }
                    }
                    if (hdnFilter.Value.Length > 0)
                    {
                        if (hdnFilter.Value == "ClearFilter")//If Clear Filter is clicked
                        {
                            ClearFilters();
                        }
                        else//Sort accordingly
                        {
                            SortResult();
                        }
                    }
                }
                else if (PageNoString.Value.Length > 0)
                {
                    currentPageNo = Convert.ToInt16(PageNoString.Value);
                    if (Session["FilteredAirResults"] != null)
                    {
                        resultDisplay = Session["FilteredAirResults"] as SearchResult[];
                    }
                    else
                    {
                        resultDisplay = Session["Results"] as SearchResult[];
                    }
                    if (Settings.LoginInfo.IsCorporate == "Y") //Added by Suresh for Corporate Users policy filter checkbox visible condition
                    {
                        
                        chkInpolicy.Visible = resultDisplay.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == true).ToArray().Length > 0 ? true : false;
                        chkOutpolicy.Visible = resultDisplay.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == false).ToArray().Length > 0 ? true : false;
                        

                    }
                    filteredResults = DoPaging(resultDisplay);

                    Session["FlightResults"] = filteredResults;
                    dlFlightResults.DataSource = filteredResults;
                    dlFlightResults.DataBind();
                }
                if (hdnFilter.Value.Length > 0)
                {
                    if (hdnFilter.Value == "ClearFilter")//If Clear Filter is clicked
                    {
                        ClearFilters();
                    }
                    else//Sort accordingly
                    {
                        SortResult();
                    }
                }
            }

        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }

    }    

    private void LoadSearchControls()
    {

        try
        {
            
            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            //IEnumerable<DataRow> query = from agents in dtAgents.AsEnumerable() where agents.Field<string>("agent_corporate") != "Y" select agents;
            IEnumerable<DataRow> query = from agents in dtAgents.AsEnumerable() select agents;
            if (query.Count() > 0)
            {
                query.ToList().ForEach(x => { if (x.Field<string>("agent_corporate") == "Y") x.SetField<string>("agent_name", "(" + x.ItemArray[2].ToString() + ")"); });
                DataTable dtNCAgents = query.CopyToDataTable<DataRow>();
                DataView dv = dtNCAgents.DefaultView;
                dv.RowFilter = "agent_Id NOT IN('" + Settings.LoginInfo.AgentId + "')";

                ddlAirAgents.AppendDataBoundItems = true;
                ddlAirAgents.Items.Add(new ListItem("Select Client", "-1"));
                ddlAirAgents.DataSource = dv.ToTable();
                ddlAirAgents.DataTextField = "agent_name";
                ddlAirAgents.DataValueField = "agent_id";
                ddlAirAgents.DataBind();
            }

            roundtrip.Checked = true;
            DataTable dtFlightSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 1);
            chkSuppliers.DataSource = dtFlightSources;
            chkSuppliers.DataValueField = "Id";
            chkSuppliers.DataTextField = "Name";
            chkSuppliers.DataBind();
            request = Session["FlightRequest"] as SearchRequest;
            foreach (ListItem item in chkSuppliers.Items)
            {
                if (request.Sources != null)
                {
                    if (request.Sources.Contains(item.Text))
                    {
                        item.Selected = true;
                    }
                }
                if (dtFlightSources.Rows.Count > request.Sources.Count)
                {
                    chkFlightAll.Checked = false;
                }
            }

            if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                lblSearchSupp.Style.Add("display", "none");
                chkSuppliers.Style.Add("display", "none");
                chkFlightAll.Style.Add("display", "none");
            }
           
            //Modify search   implemented by brahmam 22.08.2016
            if (request != null)
            {             
                
                userType = Settings.LoginInfo.MemberType.ToString();

                switch (request.Type)
                {
                    case SearchType.Return:
                        roundtrip.Checked = true;
                        oneway.Checked = false;
                        multicity.Checked = false;
                        break;
                    case SearchType.OneWay:
                        oneway.Checked = true;
                        roundtrip.Checked = false;
                        multicity.Checked = false;
                        break;
                    case SearchType.MultiWay:
                        multicity.Checked = true;
                        roundtrip.Checked = false;
                        break;
                }
                radioSelf.Enabled = false;
                radioAgent.Enabled = false;
                rbtnlMaxStops.SelectedValue = request.MaxStops;
                if (request.RefundableFares)
                {
                    chkRefundFare.Checked = true;
                }
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    radioAgent.Checked = true;
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    ddlAirAgents.SelectedValue = Settings.LoginInfo.OnBehalfAgentID.ToString();
                }
                else
                {
                    radioSelf.Checked = true;
                }

                //Set Corporate profile controls selected with search parameters
                if (Settings.LoginInfo.IsCorporate == "Y")
                {
                    ddlFlightEmployee.ClearSelection();
                    foreach (ListItem item in ddlFlightEmployee.Items)
                    {
                        if (item.Value.StartsWith(request.CorporateTravelProfileId.ToString()))
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                    ddlFlightTravelReasons.ClearSelection();
                    foreach (ListItem item in ddlFlightTravelReasons.Items)
                    {
                        if (item.Value.StartsWith(request.CorporateTravelReasonId.ToString()))
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }

                if (request.Type != SearchType.MultiWay)
                {
                    FlightSegment segment = request.Segments[0];

                    foreach (Airport aPort in ((Trie<Airport>)Page.Application["tree1"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                    {
                        Origin.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                        break;
                    }
                    chkNearByPort.Checked = segment.NearByOriginPort;
                    foreach (Airport aPort in ((Trie<Airport>)Page.Application["tree1"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                    {
                        Destination.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                        break;
                    }
                    IFormatProvider format = new CultureInfo("en-GB");
                    string[] depDate = segment.PreferredDepartureTime.ToString("dd-MM-yyyy").Split('-');
                    string[] arrDate = new string[0];
                    DepDate.Text = depDate[0] + "/" + depDate[1] + "/" + depDate[2];
                    string departureTime = segment.PreferredDepartureTime.ToString("HH:mm");
                    if (departureTime.Length > 0 && segment.PreferredDepartureTime.ToString("HHmm") != "0001")
                    {
                        string selectedTime = departureTime + " - " + segment.PreferredDepartureTime.AddMinutes(30).ToString("HH:mm");

                        if (request.TimeIntervalSpecified)
                            ddlDepTime.SelectedValue = ddlDepTime.Items.FindByValue(selectedTime).Value;
                        else
                        {
                            switch (departureTime.Trim())
                            {
                                case "05:00":
                                    ddlDepTime.SelectedValue = "Morning";
                                    break;
                                case "12:00":
                                    ddlDepTime.SelectedValue = "Afternoon";
                                    break;
                                case "06:00":
                                    ddlDepTime.SelectedValue = "Evening";
                                    break;
                                case "11:59":
                                    ddlDepTime.SelectedValue = "Night";
                                    break;                                
                            }
                        }
                    }

                    if (request.Type == SearchType.Return)
                    {
                        ReturnDateTxt.Text = request.Segments[1].PreferredArrivalTime.ToString("dd/MM/yyyy");

                        string returnTime = request.Segments[1].PreferredArrivalTime.ToString("HH:mm");
                        if (returnTime.Length > 0 && request.Segments[1].PreferredArrivalTime.ToString("HHmm") != "0001")
                        {
                            string selectedTime = returnTime + " - " + request.Segments[1].PreferredArrivalTime.AddMinutes(30).ToString("HH:mm");

                            if (request.TimeIntervalSpecified)
                                ddlReturnTime.SelectedValue = ddlReturnTime.Items.FindByValue(selectedTime).Value;
                            else
                            {
                                switch (returnTime.Trim())
                                {
                                    case "05:00":
                                        ddlReturnTime.SelectedValue = "Morning";
                                        break;
                                    case "12:00":
                                        ddlReturnTime.SelectedValue = "Afternoon";
                                        break;
                                    case "06:00":
                                        ddlReturnTime.SelectedValue = "Evening";
                                        break;
                                    case "11:59":
                                        ddlReturnTime.SelectedValue = "Night";
                                        break;                                   
                                }
                            }
                        }
                    }
                    else
                    {
                        ReturnDateTxt.Text = segment.PreferredArrivalTime.ToString("dd/MM/yyyy");
                    }
                }
                else
                {

                    for (int i = 0; i < request.Segments.Length; i++)
                    {
                        FlightSegment segment = request.Segments[i];
                        switch (i + 1)
                        {
                            case 1:
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                {
                                    City1.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                chkNearByPort1.Checked = segment.NearByOriginPort;
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                {
                                    City2.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                Time1.Text = Convert.ToDateTime(segment.PreferredDepartureTime).ToString("dd/MM/yyyy");

                                break;
                            case 2:
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                {
                                    City3.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                chkNearByPort2.Checked = segment.NearByOriginPort;
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                {
                                    City4.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                Time2.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                break;
                            case 3:
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                {
                                    City5.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                chkNearByPort3.Checked = segment.NearByOriginPort;
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                {
                                    City6.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                Time3.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                break;
                            case 4:
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                {
                                    City7.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                chkNearByPort4.Checked = segment.NearByOriginPort;
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                {
                                    City8.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                Time4.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                break;
                            case 5:
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                {
                                    City9.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                chkNearByPort5.Checked = segment.NearByOriginPort;
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                {
                                    City10.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                Time5.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                break;
                            case 6:
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Origin.ToLower()))// For Airport Code
                                {
                                    City11.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                chkNearByPort6.Checked = segment.NearByOriginPort;
                                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(segment.Destination.ToLower()))// For Airport Code
                                {
                                    City12.Text = "(" + aPort.AirportCode + ")" + aPort.CityName + "," + aPort.CountryName;
                                    break;
                                }
                                Time6.Text = segment.PreferredDepartureTime.ToString("dd/MM/yyyy");
                                break;

                        }
                    }
                }

                ddlAdults.SelectedIndex = -1;
                ddlAdults.Items.FindByValue(request.AdultCount.ToString()).Selected = true;

                ddlChilds.SelectedIndex = -1;
                ddlChilds.Items.FindByValue(request.ChildCount.ToString()).Selected = true;

                ddlInfants.SelectedIndex = -1;
                ddlInfants.Items.FindByValue(request.InfantCount.ToString()).Selected = true;

                ddlBookingClass.SelectedIndex = -1;
                ddlBookingClass.Items.FindByValue(((int)request.Segments[0].flightCabinClass).ToString()).Selected = true;

                airlineCode.Value = string.Empty;
                if (request.Segments[0].PreferredAirlines.Length > 0)
                {
                    txtPreferredAirline.Text = request.Segments[0].PreferredAirlines[0];
                    //airlineName.Value = request.Segments[0].PreferredAirlines[1];
                    //airlineCode.Value = request.Segments[0].PreferredAirlines[0];
                    for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(request.Segments[0].PreferredAirlines[i]) && request.Segments[0].PreferredAirlines[i].Length <= 2)
                        {
                            if (airlineCode.Value.Length > 0)
                            {
                                airlineCode.Value += "," + request.Segments[0].PreferredAirlines[i];
                            }
                            else
                            {
                                airlineCode.Value = request.Segments[0].PreferredAirlines[i];
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 1, "Failed to reload search params. " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void GetSearchResults(SearchRequest request)
    {
        string sessionId = "";
        SearchResult[] result = new SearchResult[0];
        //if (request.Type == SearchType.OneWay)
        //{
        try
        {
            bool isSearchRestricted = false;
            if (request.InfantCount > 0 || (request.Type == SearchType.MultiWay && request.Segments.Length > 2) || request.RefundableFares)
            {
                if (request.Sources.Count > 1)
                    request.Sources.Remove("PK");
                else if (request.Sources.Contains("PK"))
                {
                    if (request.RefundableFares)
                    {
                        request.Sources.Remove("PK");
                    }
                    else if (request.InfantCount > 0 && request.Segments.Length > 2)
                    {
                        isSearchRestricted = true;
                        lblMessage.Text = "Infants are not allowed currently in Search for PK. Only two connections/segments allowed in Search for PK. Please modify your search.";
                    }
                    else if (request.InfantCount > 0)
                    {
                        isSearchRestricted = true;
                        lblMessage.Text = "Infants are not allowed currently in Search for PK. Please modify your search.";
                    }
                    else if (request.Segments.Length > 2)
                    {
                        isSearchRestricted = true;
                        lblMessage.Text = "Only two connections/segments allowed in Search for PK. Please modify your search.";
                    }
                }
            }

            if (isSearchRestricted)
            {
                if (!string.IsNullOrEmpty(airlineMessage))
                {
                    lblMessage.Text += "You are not allowed to search with these preferred airlines." + airlineMessage;
                }
            }
            else
            {
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                else
                {
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }

                DateTime inTimeSearch = DateTime.Now;
                MetaSearchEngine mse = new MetaSearchEngine();
                //StaticData sd = new StaticData();
                //sd.BaseCurrency = agency.AgentCurrency;                
                mse.SettingsLoginInfo = Settings.LoginInfo;

                if (airlineCode.Value.Length > 0)
                {
                    foreach (FlightSegment segment in request.Segments)
                    {
                        segment.PreferredAirlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }

                request.Segments[0].flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                if (request.Type == SearchType.Return)
                {
                    request.Segments[1].flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                }

                //if (Cache["FlightResult"] == null)
                {
                    result = (SearchResult[])mse.GetResults(request);
                    TimeSpan totalTimeSearch = DateTime.Now.Subtract(inTimeSearch);

                    if (result != null && result.Length > 0)
                    {
                        //Cache.Insert("FlightResult", result, null, DateTime.Now.AddMinutes(10), System.Web.Caching.Cache.NoSlidingExpiration);
                    }
                    sessionId = mse.SessionId;
                    Session["sessionId"] = sessionId;
                }
                //else
                //{
                //    result = Cache["FlightResult"] as SearchResult[];
                //}


                //Audit.Add(EventType.Issue, Severity.Normal, 0, "Total time search in mse is   " + totalTimeSearch.TotalMilliseconds.ToString(), "");
                //audit = " MSE Time is   " + totalTimeSearch.TotalMilliseconds.ToString();

                if (request.MaxStops == "0")//Filter Direct flights only from the results
                {
                    List<SearchResult> Results = new List<SearchResult>();
                    foreach (SearchResult res in result)
                    {
                        if (res.Flights[0].Length == 1 && (request.Type != SearchType.OneWay && res.Flights[1].Length == 1))
                        {
                            Results.Add(res);
                        }
                    }
                    if (request.Type != SearchType.OneWay && Results.Count > 0)
                    {
                        result = Results.ToArray();
                    }

                    chkNonStop.Enabled = false;//disable if only Non Stop searched
                }
                else if (request.MaxStops == "1")
                {
                    chkOneStop.Enabled = false;//disable if only Single Stop searched
                }
                else if (request.MaxStops == "2")
                {
                    chkTwoStops.Enabled = false;//disable if only Two stops searched
                }
                //if(request.Sources.Count == 1 && request.Sources.Contains("PK"))
                //{
                //    chkRefundable.Visible = false;
                //}
                //For adding Agent Markup Fee from search page.
                //if (request.Unrestrictedcarrier != null)
                //{
                //    foreach (SearchResult res in result)
                //    {
                //        res.TotalFare += Convert.ToInt32(request.Unrestrictedcarrier);                    
                //        res.Price.AsvAmount = Convert.ToInt32(request.Unrestrictedcarrier);
                //        res.Price.AsvElement = "Tax";
                //        res.Price.AsvType = "T";
                //    }
                //}

                //}



                DateTime inTime = DateTime.Now;


                for (int i = 0; i < result.Length; i++)
                {
                    if (result[i] != null)
                    {
                        //computing layover airports and origin, destination airports...
                        if (!originAirports.Contains(result[i].Flights[0][0].Origin.AirportName + " (" + result[i].Flights[0][0].Origin.AirportCode + ")"))
                        {
                            originAirports.Add(result[i].Flights[0][0].Origin.AirportName + " (" + result[i].Flights[0][0].Origin.AirportCode + ")");
                        }
                        if (!destinationAirports.Contains(result[i].Flights[0][result[i].Flights[0].Length - 1].Destination.AirportName + " (" + result[i].Flights[0][result[i].Flights[0].Length - 1].Destination.AirportCode + ")"))
                        {
                            destinationAirports.Add(result[i].Flights[0][result[i].Flights[0].Length - 1].Destination.AirportName + " (" + result[i].Flights[0][result[i].Flights[0].Length - 1].Destination.AirportCode + ")");
                        }
                        for (int j = 0; j < result[i].Flights.Length; j++)
                        {
                            if (result[i].Flights[j].Length > 1)
                            {
                                for (int k = 1; k < result[i].Flights[j].Length; k++)
                                {
                                    if (!layoverAirports.Contains(result[i].Flights[j][k].Origin.AirportName + " (" + result[i].Flights[j][k].Origin.AirportCode + ")"))
                                    {
                                        layoverAirports.Add(result[i].Flights[j][k].Origin.AirportName + " (" + result[i].Flights[j][k].Origin.AirportCode + ")");
                                    }
                                    if (request.Segments[0].PreferredAirlines.Length == 0 || request.Segments[0].PreferredAirlines.Contains(result[i].Flights[j][k].Airline))
                                    {
                                        if (!airlineCodeList.Contains(result[i].Flights[j][k].Airline))
                                        {
                                            Airline airline = new Airline();
                                            airline.Load(result[i].Flights[j][k].Airline);
                                            airlineList.Add(airline.AirlineName + " (" + result[i].Flights[j][k].Airline + ")");
                                            airlineCodeList.Add(result[i].Flights[j][k].Airline);
                                        }
                                    }
                                }
                            }
                            if (request.Segments[0].PreferredAirlines.Length == 0 || request.Segments[0].PreferredAirlines.Contains(result[i].Flights[j][0].Airline))
                            {
                                if (!airlineCodeList.Contains(result[i].Flights[j][0].Airline))
                                {
                                    Airline airline = new Airline();
                                    airline.Load(result[i].Flights[j][0].Airline);
                                    airlineList.Add(airline.AirlineName + " (" + result[i].Flights[j][0].Airline + ")");
                                    airlineCodeList.Add(result[i].Flights[j][0].Airline);
                                }
                            }
                        }                        
                    }
                }
                TimeSpan totalTime = DateTime.Now.Subtract(inTime);
                //audit += " | GetPrice Time - " + totalTime.TotalMilliseconds.ToString();
                //Audit.Add(EventType.Issue, Severity.Normal, 0, "Total time in GetPrice ms is  " + totalTime.TotalMilliseconds.ToString(), "");

                layoverAirports.Sort();
                airlineList.Sort();
                Session["layoverAirports"] = layoverAirports;
                Session["fromcityAirports"] = originAirports;
                Session["tocityAirports"] = destinationAirports;
                Session["airlines"] = airlineList;


                foreach (string airline in airlineList)
                {
                    ListItem item = new ListItem(airline, airline.Split('(')[1].Replace(")", ""));
                    item.Selected = true;
                    if (!chkListAirlines.Items.Contains(item))
                    {
                        chkListAirlines.Items.Add(item);
                    }
                }

                // puting source and destination city code in the session array for booking purpose
                //Basket.FlightBookingSession[sessionId].Result = result;
                if (result != null && result.Length > 0)
                {
                    resultDisplay = result;
                    Session["Results"] = resultDisplay;
                    //Collected filtered timings for onward and return
                    BindFilterTimings(request);
                    //In order to assign the slider with Min and Max values from the entire results
                    //Sort them on price
                    List<SearchResult> OriginalResults = new List<SearchResult>();
                    OriginalResults.AddRange(resultDisplay);
                    //Sort by price 
                    OriginalResults.Sort((r1, r2) => r1.TotalFare.CompareTo(r2.TotalFare));
                    //Assign the Min value
                    hdnMinValue.Value = Math.Ceiling(OriginalResults[0].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                    if (resultDisplay[0].ResultBookingSource == BookingSource.TBOAir)
                    {
                        hdnMinValue.Value = Math.Ceiling(OriginalResults[0].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                    }

                    hdnMaxValue.Value = Math.Ceiling(OriginalResults[OriginalResults.Count - 1].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                    if (OriginalResults[OriginalResults.Count - 1].ResultBookingSource == BookingSource.TBOAir)
                    {
                        hdnMaxValue.Value = Math.Ceiling(OriginalResults[OriginalResults.Count - 1].TotalFare).ToString("N" + agency.DecimalValue).Replace(",", "");
                    }

                    hdnPriceMin.Value = hdnMinValue.Value;
                    hdnPriceMax.Value = hdnMaxValue.Value;
                    OriginalResults = null;//Release the List<SearchResults>
                    // Added by hari malla on 24-05-2019 for not showing the refundable fares when selecting refundable fares only.
                    //if (request.RefundableFares)
                    //{
                    //    resultDisplay = resultDisplay.Where(x => x.NonRefundable == false).ToArray();
                    //}
                    // Added by hari malla on 11-10-2019.  Based on results showing the fares and stop controls. 
                    chkRefundable.Visible = resultDisplay.Where(x => x.NonRefundable == false).ToArray().Length > 0 ? true : false;
                    chkNonRefundable.Visible = resultDisplay.Where(x => x.NonRefundable == true).ToArray().Length > 0 ? true : false;

                    // stops validations added by bangar

                    chkNonStop.Visible = false;
                    chkOneStop.Visible = false;
                    chkTwoStops.Visible = false;

                    if (request.MaxStops == "0")
                        chkNonStop.Visible = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 == 0)).Count > 0 ? true : false;
                    else if (request.MaxStops == "1")
                        chkOneStop.Visible = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 == 1)).Count > 0 ? true : false;
                    else if (request.MaxStops == "2")
                        chkTwoStops.Visible = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 > 1)).Count > 0 ? true : false;
                    else
                    {
                        chkNonStop.Visible = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 == 0)).Count > 0 ? true : false;
                        chkOneStop.Visible = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 == 1)).Count > 0 ? true : false;
                        chkTwoStops.Visible = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 > 1)).Count > 0 ? true : false;
                    }

                    if (Settings.LoginInfo.IsCorporate == "Y")//Added by Suresh for Corporate Users policy filter checkbox visible condition
                    {
                        //chkInpolicy.Visible = resultDisplay.ToList().Where(searchResult => searchResult.TravelPolicyResult.IsUnderPolicy == true).ToArray().Length > 0 ? true : false;
                        //chkOutpolicy.Visible = resultDisplay.ToList().Where(searchResult => searchResult.TravelPolicyResult.IsUnderPolicy == false).ToArray().Length > 0 ? true : false;
                        chkInpolicy.Visible = resultDisplay.ToList().Where(searchResult => searchResult.TravelPolicyResult!=null && searchResult.TravelPolicyResult.IsUnderPolicy == true).ToArray().Length > 0 ? true : false;
                        chkOutpolicy.Visible = resultDisplay.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == false).ToArray().Length > 0 ? true : false;
                    }
                    // Added by hari malla on 27-05-2019 for filtering the preferred airline results
                    if (request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        List<SearchResult> searchResults = new List<SearchResult>();
                        foreach (string airlinecode in request.Segments[0].PreferredAirlines)
                        {
                            searchResults.AddRange(resultDisplay.Where(x => x.Airline.Contains(airlinecode)).ToArray());
                        }
                        resultDisplay = searchResults.ToArray();
                    }
                    // Added by hari malla on 27 - 05 - 2019 for filtering the preferred airline stops results 

                    //if (!string.IsNullOrEmpty(request.MaxStops)) // commeneted by bangar
                    //{
                    //     resultDisplay = resultDisplay.ToList().FindAll(x => x.Flights.All(y => y.Select(z => z.FlightNumber).Distinct().Count() - 1 == Convert.ToInt32(request.MaxStops))).ToArray();
                    //}

                    ////END
                    filteredResults = DoPaging(resultDisplay);
                    Session["FlightResults"] = filteredResults;
                    dlFlightResults.Visible = true;
                    dlFlightResults.DataSource = filteredResults;
                    dlFlightResults.DataBind();
                }
                else
                {
                    dlFlightResults.Visible = false;
                    Session["Results"] = null;
                }


                fromCity = new ArrayList();// = CT.BookingEngine.Airport.GetCityName(request.Segments[0].Origin);
                toCity = new ArrayList();// CT.BookingEngine.Airport.GetCityName(request.Segments[0].Destination);

                // puting source and destination city code in the session array for booking purpose

                if (fromCity.Count == 1)
                {
                    Array a = (Array)fromCity[0];
                    Session["origin"] = a.GetValue(0);
                }
                else
                {
                    Session["origin"] = lblOrigin.Text;
                }

                if (toCity.Count == 1)
                {
                    Array a = (Array)toCity[0];
                    Session["destination"] = a.GetValue(0);
                }
                else
                {
                    Session["destination"] = lblDestination.Text;
                }
                chkRefundable.Enabled = request.RefundableFares ?false: true ;

                //if (!isCalendarSearch && !changedDaySearch)
                //{
                //    Session["waytype"] = Request["waytype"];
                //    Session["type"] = Request["type"];
                //    Session["adult"] = adult;
                //    Session["senior"] = senior;
                //    Session["child"] = child;
                //    Session["infant"] = infant;
                //    Session["cabinclass"] = Convert.ToInt32(Request["bookingclass"]);
                //}
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed searching Flights: " + ex.ToString(), Request["REMOTE_ADDR"]);
            string url = Request.ServerVariables["HTTP_REFERER"].ToString();
            //errorMessageSearchResult = "Error: There is some error. Please " + "<a href=\"" + url + "\">" + "Retry" + "</a>.";
            ////ErrorSearchRes = true;
            //ExceptionSearchRes = true;
        }
    }

    private SearchResult[] DoPaging(SearchResult[] resultDisplay)
    {
        filteredResults = new SearchResult[0];
        if (resultDisplay != null)
        {
            filteredCount = resultDisplay.Length;
            resultIds = string.Empty;
            foreach (SearchResult res in resultDisplay)
            {
                if (string.IsNullOrEmpty(resultIds))
                {
                    resultIds = res.ResultId.ToString();
                }
                else
                {
                    resultIds += "," + res.ResultId;
                }
            }

            int numberOfPages = 0;
            int resultsPerPage = 50;// Convert.ToInt32(ConfigurationSystem.PagingConfig["FlightSearchResultPerPage"]);
            if (resultDisplay.Length % resultsPerPage > 0)
            {
                numberOfPages = (resultDisplay.Length / resultsPerPage) + 1;
            }
            else
            {
                numberOfPages = resultDisplay.Length / resultsPerPage;
            }
            //Showing first page by default (while filtering)
            if (currentPageNo == 0 || currentPageNo > numberOfPages)
            {
                currentPageNo = 1;
            }
            string urlForPaging = string.Empty;
            show = BookingUtility.PagingJavascript(numberOfPages, urlForPaging, currentPageNo);

            List<SearchResult> results = new List<SearchResult>();

            if (resultDisplay.Length > 0)
            {
                if ((currentPageNo * resultsPerPage) <= resultDisplay.Length)
                {
                    for (int i = (currentPageNo * resultsPerPage) - resultsPerPage; i < (currentPageNo * resultsPerPage); i++)
                    {
                        if (i < resultDisplay.Length)
                        {
                            results.Add(resultDisplay[i]);
                        }
                    }
                }
                else
                {
                    for (int i = (currentPageNo * resultsPerPage) - resultsPerPage; i < (currentPageNo * resultsPerPage); i++)
                    {
                        if (i < resultDisplay.Length)
                        {
                            results.Add(resultDisplay[i]);
                        }
                    }
                }
            }
            filteredResults = new SearchResult[results.Count];
            
            for (int i = 0; i < results.Count; i++)
            {                
                filteredResults[i] = results[i];
            }

            maxResult = filteredResults.Length * currentPageNo;
            if (currentPageNo * resultsPerPage < maxResult)
            {
                maxResult = currentPageNo * resultsPerPage;//filteredResults.Length;
            }

            minResult = ((currentPageNo * resultsPerPage) - resultsPerPage);
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }

        BindFilterTimings(request);

        return filteredResults;
    }
    
    protected int IntTime(DateTime time)
    {
        return (time.Hour * 60 + time.Minute);
    }

    private void SortResult()
    {
        try
        {
            if (Session["sessionId"] != null)
            {
                sessionId = Session["sessionId"].ToString();
            }
            SearchResult[] tempResult = new SearchResult[0];
            //if (Basket.FlightBookingSession.ContainsKey(sessionId))
            //{
            //    tempResult = Basket.FlightBookingSession[sessionId].Result;
            //}
            //else
            //{
            //    Response.Redirect("HotelSearch.aspx", true);
            //}
            if (Session["Results"] != null)
            {
                tempResult = Session["Results"] as SearchResult[];
            }
            else
            {
                Response.Redirect("HotelSearch.aspx?source=Flight", true);
            }
            string sortString = string.Empty;
            //if (Request["sortString"] != null && !changedDaySearch)
            //{
            //    sortString = Request["sortString"];
            //}
            //else
            //{
            //    sortString = Session["sortString"].ToString();
            //}
            if (!request.TimeIntervalSpecified)
            {
                switch (ddlTimings.SelectedItem.Text)
                {
                    case "Morning":
                        sortString = "1|0|0|0";
                        break;
                    case "After Noon":
                        sortString = "0|1|0|0";
                        break;
                    case "Evening":
                        sortString = "0|0|1|0";
                        break;
                    case "Night":
                        sortString = "0|0|0|1";
                        break;
                    case "All":
                        sortString = "1|1|1|1";
                        break;
                }
            }
            else
                sortString = "1|1|1|1";
            

            if (ddlPrice.SelectedValue == "Lowest")
            {
                sortString += "-P|A";
            }
            else
            {
                sortString += "-P|D";
            }

            if (ddlDeparture.SelectedIndex > 0)//Departure sorting check
            {
                sortString += "|D|" + ddlDeparture.SelectedValue;
            }

            else if (ddlArrival.SelectedIndex > 0)//Arrival sorting check
            {
                sortString += "|A|" + ddlArrival.SelectedValue;
            }
            else if (ddlAirline.SelectedIndex > 0)//Airline sorting check
            {
                sortString += "|F|" + ddlAirline.SelectedValue;
            }
            else if (ddlStops.SelectedIndex > 0)//Stops sorting check
            {
                sortString += "|S|" + ddlStops.SelectedValue;
            }
            //Duration Sorting check
            if (hdnFilter.Value.Length > 0)
            {
                if (hdnFilter.Value == "TD")
                {
                    sortString += "|DR|TD";//Total Duration
                }
                else if (hdnFilter.Value == "FD")
                {
                    sortString += "|DR|FD";//Flying Duration
                }
            }

            if (PageNoString.Value.Length > 0)
            {
                sortString += "-" + PageNoString.Value;
            }
            else
            {
                sortString += "-1";
            }
            sortString += "-0-";

            if (chkNonStop.Checked)
            {
                sortString += "1|";
            }
            else
            {
                sortString += "0|";
            }

            if (chkOneStop.Checked)
            {
                sortString += "1|";
            }
            else
            {
                sortString += "0|";
            }
            if (chkTwoStops.Checked)
            {
                sortString += "1|";
            }
            else
            {
                sortString += "0";
            }
            sortString += "-null-";
            bool allAirlines = false;
            foreach (ListItem item in chkListAirlines.Items)
            {
                if (item.Selected)
                {
                    allAirlines = true;
                }
                else
                {
                    allAirlines = false;
                    break;
                }
            }

            if (!allAirlines)
            {
                foreach (ListItem item in chkListAirlines.Items)
                {
                    if (!item.Selected)
                    {
                        sortString += item.Value + "|";
                    }                                     
                }
                sortString += "-";
                foreach (ListItem item in chkListAirlines.Items)
                {
                    if (item.Selected)
                    {
                        sortString += item.Value + "|";
                    }
                }
            }
            else
            {
                sortString += "|null";
            }

            if (tempResult.Length > 0)
            {

                char[] splitParOuter = { '-' };
                char[] splitParInner = { '|' };
                string[] sortStringArray = sortString.Split(splitParOuter, StringSplitOptions.RemoveEmptyEntries);
                string[] timeSortArray = sortStringArray[0].Split(splitParInner, StringSplitOptions.RemoveEmptyEntries);
                string[] sortTypeArray = sortStringArray[1].Split(splitParInner, StringSplitOptions.RemoveEmptyEntries);
                if (!isClearPageCount)
                {
                    currentPageNo = Convert.ToInt32(sortStringArray[2]);
                }
                else
                {
                    currentPageNo = 1;
                }
                string[] stopArray = sortStringArray[4].Split(splitParInner, StringSplitOptions.RemoveEmptyEntries);
                string[] airportArray = sortStringArray[5].Split(splitParInner, StringSplitOptions.RemoveEmptyEntries);
                string[] airlineArray = sortStringArray[6].Split(splitParInner, StringSplitOptions.RemoveEmptyEntries);
                string[] airlineSelArray = new string[0];
                if (sortStringArray.Length >= 8)
                {
                    airlineSelArray = sortStringArray[7].Split(splitParInner, StringSplitOptions.RemoveEmptyEntries);
                }

                string departureDateSortString = tempResult[0].Flights[0][0].DepartureTime.ToShortDateString();
                //DateTime departureDateSort = DateTime.ParseExact(departureDateSortString, "dd/MMM/yy", null); //M/d/yyyy
                DateTime departureDateSort = DateTime.ParseExact(departureDateSortString, "M/d/yyyy", null); //M/d/yyyy
                DateTime morning = departureDateSort.AddHours(5);//.AddHours(4);//05:00 AM to 11:59 AM
                DateTime afternoon = departureDateSort.AddHours(12);//.AddHours(11);//12:00 PM to 05:59 PM
                DateTime evening = departureDateSort.AddHours(18);//.AddHours(16);//06:00 PM to 11:59 PM
                DateTime night = departureDateSort;//.AddHours(21);Night 12:00 AM to 04:59 AM

                if(request.TimeIntervalSpecified)
                {
                    morning = afternoon = evening = departureDateSort;
                }

                //Stops filtering logic has been changed on 01 October 2019 by Shiva.
                //OneStop - Both onward and return segments should be non stop.
                //OneStop/TwoStop - Either any of the segments should be OneStop/TwoStop

                List<SearchResult> searchResultList = new List<SearchResult>();
                for (int i = 0; i < tempResult.Length; i++)
                {
                    //if (tempResult[i].Flights[0][0].DepartureTime >= morning && tempResult[i].Flights[0][0].DepartureTime < afternoon && Convert.ToInt32(timeSortArray[0]) == 1 && ((tempResult[i].Flights[0].Length == 1 && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights[0].Length == 2 && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights[0].Length > 2 && Convert.ToInt32(stopArray[2]) == 1)))
                    if (tempResult[i].Flights[0][0].DepartureTime >= morning && tempResult[i].Flights[0][0].DepartureTime < afternoon && Convert.ToInt32(timeSortArray[0]) == 1 && ((tempResult[i].Flights.All(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 0)  && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 1) && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) > 1) && Convert.ToInt32(stopArray[2]) == 1)))
                    {
                        searchResultList.Add(tempResult[i]);
                    }
                    //if (tempResult[i].Flights[0][0].DepartureTime >= afternoon && tempResult[i].Flights[0][0].DepartureTime < evening && Convert.ToInt32(timeSortArray[1]) == 1 && ((tempResult[i].Flights[0].Length == 1 && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights[0].Length == 2 && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights[0].Length > 2 && Convert.ToInt32(stopArray[2]) == 1)))
                    if (tempResult[i].Flights[0][0].DepartureTime >= afternoon && tempResult[i].Flights[0][0].DepartureTime < evening && Convert.ToInt32(timeSortArray[1]) == 1 && ((tempResult[i].Flights.All(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 0) && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 1) && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) > 1) && Convert.ToInt32(stopArray[2]) == 1)))
                    {
                        searchResultList.Add(tempResult[i]);
                    }
                    //Night will be starting from 12 AM so time hour will be 00. so if we compare 0 with hour nothing with be displayed.
                    //For this fix we are using 24 as 12 AM in 24 hour format
                    //if (tempResult[i].Flights[0][0].DepartureTime >= evening && tempResult[i].Flights[0][0].DepartureTime.Hour < 24 && Convert.ToInt32(timeSortArray[2]) == 1 && ((tempResult[i].Flights[0].Length == 1 && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights[0].Length == 2 && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights[0].Length > 2 && Convert.ToInt32(stopArray[2]) == 1)))
                    if (tempResult[i].Flights[0][0].DepartureTime >= evening && tempResult[i].Flights[0][0].DepartureTime.Hour < 24 && Convert.ToInt32(timeSortArray[2]) == 1 && ((tempResult[i].Flights.All(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 0) && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 1) && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) > 1) && Convert.ToInt32(stopArray[2]) == 1)))
                    {
                        searchResultList.Add(tempResult[i]);
                    }
                    //if ((tempResult[i].Flights[0][0].DepartureTime > night && tempResult[i].Flights[0][0].DepartureTime < morning) && Convert.ToInt32(timeSortArray[3]) == 1 && ((tempResult[i].Flights[0].Length == 1 && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights[0].Length == 2 && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights[0].Length > 2 && Convert.ToInt32(stopArray[2]) == 1)))
                    if ((tempResult[i].Flights[0][0].DepartureTime > night && tempResult[i].Flights[0][0].DepartureTime < morning) && Convert.ToInt32(timeSortArray[3]) == 1 && ((tempResult[i].Flights.All(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 0) && Convert.ToInt32(stopArray[0]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) == 1) && Convert.ToInt32(stopArray[1]) == 1) || (tempResult[i].Flights.Any(f => (f.Select(p => p.FlightNumber).Distinct().Count() - 1) > 1) && Convert.ToInt32(stopArray[2]) == 1)))
                    {
                        searchResultList.Add(tempResult[i]);
                    }
                }

                if (airportArray[0] != "null" || airlineArray[0] != "null")
                {
                    for (int i = 0; i < searchResultList.Count; i++)
                    {
                        bool matchFound = false;
                        SearchResult result = searchResultList[i];

                        //No need to loop thru for airline filter
                        //foreach (string airlineCode in airlineArray)
                        {
                            if (allAirlines || (!allAirlines && airlineSelArray.Length > 0))
                            {
                                if (request.Type != SearchType.MultiWay)//do not check for multi city
                                {
                                    try
                                    {
                                        //If any of the segments contains filtered airline then do not remove
                                        //sortStringArray[7] contains filtered airline
                                        if (result.Flights.Any(f => f.Any(s => !sortStringArray[7].Contains(s.Airline))))
                                        {
                                            matchFound = true;
                                            break;
                                        }
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                matchFound = true;
                                break;
                            }
                        }
                        //No Filter related to Airport so commented by Shiva
                        //for (int j = 0; j < result.Flights.Length; j++)
                        //{
                        //    for (int k = 0; k < result.Flights[j].Length; k++)
                        //    {
                        //        foreach (string airportCode in airportArray)
                        //        {
                        //            if (result.Flights[j][k].Origin.AirportCode == airportCode || result.Flights[j][k].Destination.AirportCode == airportCode)
                        //            {
                        //                matchFound = true;
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}
                        if (chkNonRefundable.Checked && (chkRefundable != null && chkRefundable.Checked))
                        {
                            //do nothing. show both
                        }
                        else
                        {
                            if (chkNonRefundable.Checked)
                            {
                                //Remove refundable flights
                                if (!result.NonRefundable)
                                {
                                    matchFound = true;
                                    break;
                                }
                            }
                            if (chkRefundable != null && chkRefundable.Checked)
                            {
                                //Remove non refundable flights
                                if (result.NonRefundable)
                                {
                                    matchFound = true;
                                    break;
                                }
                            }
                        }

                        if (matchFound)
                        {
                            searchResultList.Remove(result);
                            i--;
                        }
                    }
                }
                if (airportArray[0] != "null")
                {
                    for (int i = 0; i < airportArray.Length; i++)
                    {
                        airportRemovedList.Add(airportArray[i]);
                    }
                }
                if (airlineArray[0] != "null")
                {
                    for (int i = 0; i < airlineArray.Length; i++)
                    {
                        airlineRemovedList.Add(airlineArray[i]);
                    }
                }
                SearchResult[] sortResult = searchResultList.ToArray();

                for (int i = 0; i < sortResult.Length; i++)
                {
                    bool airlineFound = true;
                    SearchResult result = sortResult[i];
                    for (int j = 0; j < result.Flights.Length; j++)
                    {
                        for (int k = 0; k < result.Flights[j].Length; k++)
                        {
                            foreach (string airline in airlineSelArray)
                            {
                                if (result.Flights[j][k].Airline != airline)
                                {
                                    airlineFound = false;
                                }
                                else
                                {
                                    airlineFound = true;
                                    break;
                                }
                            }
                            if (airlineFound)
                                break;
                        }
                        if (airlineFound)
                            break;
                    }
                    if (!airlineFound)
                    {
                        searchResultList.Remove(result);
                    }
                    if (!chkRefundable.Checked && !result.NonRefundable)
                    {
                        searchResultList.Remove(result);
                    }
                    if (!chkNonRefundable.Checked && result.NonRefundable)
                    {
                        searchResultList.Remove(result);
                    }
                }

                sortResult = searchResultList.ToArray();

                for (int s = 0; s < sortTypeArray.Length; s = s + 2)
                {
                    switch (sortTypeArray[s])
                    {
                        case "P"://Price sort
                            sortTypePaxDetail = "price";
                            //double calculatedCost;
                            //double calculatedCostNext;
                            if (sortTypeArray[1] == "A")
                            {
                                sortOrderPaxDetail = "asc";
                                priceImgSrc = "images/up-arrow.gif";
                                //for (int i = 0; i < (sortResult.Length - 1); i++)
                                //{
                                //    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                //    {
                                //        calculatedCost = Convert.ToDouble(sortResult[j].TotalFare + Convert.ToDouble(sortResult[j].Price.OtherCharges + sortResult[j].Price.AdditionalTxnFee + sortResult[j].Price.TransactionFee));
                                //        calculatedCostNext = Convert.ToDouble(sortResult[j + 1].TotalFare + Convert.ToDouble(sortResult[j + 1].Price.OtherCharges + sortResult[j + 1].Price.AdditionalTxnFee + sortResult[j + 1].Price.TransactionFee));
                                //        if (calculatedCost > calculatedCostNext)
                                //        {
                                //            SearchResult temp = sortResult[j];
                                //            sortResult[j] = sortResult[j + 1];
                                //            sortResult[j + 1] = temp;
                                //        }
                                //    }
                                //}
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                List<SearchResult> sortResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);

                                sortedResults.Sort(
                                    delegate(SearchResult p1, SearchResult p2)
                                    {
                                        return p1.TotalFare.CompareTo(p2.TotalFare);
                                    }
                                );
                                sortResult = sortedResults.ToArray();
                            }
                            else if (sortTypeArray[1] == "D")//Descending
                            {
                                sortOrderPaxDetail = "desc";
                                priceImgSrc = "images/down-arrow.gif";
                                //for (int i = 0; i < (sortResult.Length - 1); i++)
                                //{
                                //    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                //    {
                                //        calculatedCost = Convert.ToDouble(sortResult[j].TotalFare + Convert.ToDouble(sortResult[j].Price.OtherCharges + sortResult[j].Price.AdditionalTxnFee + sortResult[j].Price.TransactionFee));
                                //        calculatedCostNext = Convert.ToDouble(sortResult[j + 1].TotalFare + Convert.ToDouble(sortResult[j + 1].Price.OtherCharges + sortResult[j + 1].Price.AdditionalTxnFee + sortResult[j + 1].Price.TransactionFee));

                                //        if (calculatedCost < calculatedCostNext)
                                //        {
                                //            SearchResult temp = sortResult[j];
                                //            sortResult[j] = sortResult[j + 1];
                                //            sortResult[j + 1] = temp;
                                //        }
                                //    }
                                //}

                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);

                                sortedResults.Sort((p1, p2) => p2.TotalFare.CompareTo(p1.TotalFare));
                                sortResult = sortedResults.ToArray();
                            }

                            break;
                        case "D"://Departure sort   
                            if (sortTypeArray[3] == "0")//Ascending
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);

                                sortedResults.Sort((p1, p2) => p1.Flights[0][0].DepartureTime.CompareTo(p2.Flights[0][0].DepartureTime));
                                sortResult = sortedResults.ToArray();
                            }
                            else if (sortTypeArray[3] == "1")//Descending
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);

                                sortedResults.Sort((p1, p2) => p2.Flights[0][0].DepartureTime.CompareTo(p1.Flights[0][0].DepartureTime));
                                sortResult = sortedResults.ToArray();
                            }
                            break;
                        case "A"://Arrival sort
                            //Arrival sorting
                            if (sortTypeArray[3] == "0")//Ascending 
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                List<SearchResult> sortResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((x, y) => x.Flights[0][0].ArrivalTime.CompareTo(y.Flights[0][0].ArrivalTime));
                                sortResult = sortedResults.ToArray();
                            }
                            else if (sortTypeArray[3] == "1")//Descending 
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                List<SearchResult> sortResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((x, y) => y.Flights[0][0].ArrivalTime.CompareTo(x.Flights[0][0].ArrivalTime));
                                sortResult = sortedResults.ToArray();
                            }
                            break;
                        case "F"://Airline sort
                            if (sortTypeArray[3] == "0")//Ascending 
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((p1, p2) => GetAirlineName(p1.Flights[0][0].Airline).CompareTo(GetAirlineName(p2.Flights[0][0].Airline)));
                                sortResult = sortedResults.ToArray();
                            }
                            else if (sortTypeArray[3] == "1")//Descending 
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((p1, p2) => GetAirlineName(p2.Flights[0][0].Airline).CompareTo(GetAirlineName(p1.Flights[0][0].Airline)));
                                sortResult = sortedResults.ToArray();
                            }
                            break;
                        case "S"://Stops sorting
                            if (sortTypeArray[3] == "0")//Ascending 
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((x, y) => x.Flights[0].Length.CompareTo(y.Flights[0].Length));//stops are updated wrongly so compare segments always
                                sortResult = sortedResults.ToArray();
                            }
                            else if (sortTypeArray[3] == "1")//Descending 
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((x, y) => y.Flights[0].Length.CompareTo(x.Flights[0].Length));//stops are updated wrongly so compare segments always
                                sortResult = sortedResults.ToArray();
                            }
                            break;
                        case "DR"://Duration sort
                            if (sortTypeArray[3] == "TD")//Total duration sorting
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((x, y) => GetTotalDuration(x).CompareTo(GetTotalDuration(y)));
                                sortResult = sortedResults.ToArray();
                            }
                            else if (sortTypeArray[3] == "FD")//Flying duration sorting
                            {
                                List<SearchResult> sortedResults = new List<SearchResult>();
                                sortedResults.AddRange(sortResult);
                                sortedResults.Sort((x, y) => GetFlyingDuration(x).CompareTo(GetFlyingDuration(y)));
                                sortResult = sortedResults.ToArray();
                            }
                            break;
                        case "Dep":
                            sortTypePaxDetail = "depTime";
                            if (sortTypeArray[1] == "A")
                            {
                                depImgSrc = "images/up-arrow.gif";
                                sortOrderPaxDetail = "asc";
                                for (int i = 0; i < (sortResult.Length - 1); i++)
                                {
                                    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                    {
                                        if (sortResult[j].Flights[0][0].DepartureTime > sortResult[j + 1].Flights[0][0].DepartureTime)
                                        {
                                            SearchResult temp = sortResult[j];
                                            sortResult[j] = sortResult[j + 1];
                                            sortResult[j + 1] = temp;
                                        }
                                    }
                                }
                            }

                            if (sortTypeArray[1] == "D")
                            {
                                sortOrderPaxDetail = "desc";
                                depImgSrc = "images/down-arrow.gif";
                                for (int i = 0; i < (sortResult.Length - 1); i++)
                                {
                                    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                    {
                                        if (sortResult[j].Flights[0][0].DepartureTime < sortResult[j + 1].Flights[0][0].DepartureTime)
                                        {
                                            SearchResult temp = sortResult[j];
                                            sortResult[j] = sortResult[j + 1];
                                            sortResult[j + 1] = temp;
                                        }
                                    }
                                }
                            }
                            break;
                        case "Arr":
                            sortTypePaxDetail = "arrTime";
                            if (sortTypeArray[1] == "A")
                            {
                                sortOrderPaxDetail = "asc";
                                arrImgSrc = "images/up-arrow.gif";
                                for (int i = 0; i < (sortResult.Length - 1); i++)
                                {
                                    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                    {
                                        if (sortResult[j].Flights[1][sortResult[j].Flights[1].Length - 1].ArrivalTime > sortResult[j + 1].Flights[1][sortResult[j + 1].Flights[1].Length - 1].ArrivalTime)
                                        {
                                            SearchResult temp = sortResult[j];
                                            sortResult[j] = sortResult[j + 1];
                                            sortResult[j + 1] = temp;
                                        }
                                    }
                                }
                            }

                            if (sortTypeArray[1] == "D")
                            {
                                sortOrderPaxDetail = "desc";
                                arrImgSrc = "images/down-arrow.gif";
                                for (int i = 0; i < (sortResult.Length - 1); i++)
                                {
                                    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                    {
                                        if (sortResult[j].Flights[1][sortResult[j].Flights[1].Length - 1].ArrivalTime < sortResult[j + 1].Flights[1][sortResult[j + 1].Flights[1].Length - 1].ArrivalTime)
                                        {
                                            SearchResult temp = sortResult[j];
                                            sortResult[j] = sortResult[j + 1];
                                            sortResult[j + 1] = temp;
                                        }
                                    }
                                }
                            }
                            break;
                        case "Dur":
                            sortTypePaxDetail = "duration";
                            if (sortTypeArray[1] == "A")
                            {
                                sortOrderPaxDetail = "asc";
                                durImgSrc = "images/up-arrow.gif";
                                for (int i = 0; i < (sortResult.Length - 1); i++)
                                {
                                    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                    {
                                        if (sortResult[j].Flights[0][sortResult[j].Flights[0].Length - 1].AccumulatedDuration > sortResult[j + 1].Flights[0][sortResult[j + 1].Flights[0].Length - 1].AccumulatedDuration)
                                        {
                                            SearchResult temp = sortResult[j];
                                            sortResult[j] = sortResult[j + 1];
                                            sortResult[j + 1] = temp;
                                        }
                                    }
                                }
                            }

                            if (sortTypeArray[1] == "D")
                            {
                                sortOrderPaxDetail = "desc";
                                durImgSrc = "images/down-arrow.gif";
                                for (int i = 0; i < (sortResult.Length - 1); i++)
                                {
                                    for (int j = 0; j < (sortResult.Length - i - 1); j++)
                                    {
                                        if (sortResult[j].Flights[0][sortResult[j].Flights[0].Length - 1].AccumulatedDuration < sortResult[j + 1].Flights[0][sortResult[j + 1].Flights[0].Length - 1].AccumulatedDuration)
                                        {
                                            SearchResult temp = sortResult[j];
                                            sortResult[j] = sortResult[j + 1];
                                            sortResult[j + 1] = temp;
                                        }
                                    }
                                }
                            }
                            break;

                    }
                }
                resultDisplay = sortResult;
                //resultDisplay = null;
                //sainadh
                List<SearchResult> results = new List<SearchResult>(0);
                for (int i = 0; i < resultDisplay.Length; i++)
                {
                    if (resultDisplay[0].ResultBookingSource == BookingSource.TBOAir)
                    {
                        if (Math.Ceiling(resultDisplay[i].TotalFare) >= Convert.ToDouble(hdnPriceMin.Value) && resultDisplay[i].TotalFare <= Convert.ToDouble(hdnPriceMax.Value))
                        {
                            results.Add(resultDisplay[i]);
                        }
                    }
                    else
                    {
                        if (Math.Ceiling(resultDisplay[i].TotalFare) >= Convert.ToDouble(hdnPriceMin.Value) && Math.Ceiling(resultDisplay[i].TotalFare) <= Convert.ToDouble(hdnPriceMax.Value))
                        {
                            results.Add(resultDisplay[i]);
                        }
                    }
                }
                if (Settings.LoginInfo.IsCorporate == "Y") //Added by Suresh for Corporate Users policy filter checkbox visible condition
                {
                    chkInpolicy.Visible = results.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == true).ToArray().Length > 0 ? true : false;
                    chkOutpolicy.Visible = results.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == false).ToArray().Length > 0 ? true : false;
                }
                
                //In / Out Policy for corporate user
                List<SearchResult> corpResults = new List<SearchResult>();
                if (Settings.LoginInfo.IsCorporate == "Y" && chkInpolicy.Checked && chkOutpolicy.Checked)
                {
                    resultDisplay = results.ToArray();
                }
                else if (Settings.LoginInfo.IsCorporate == "Y" && !chkInpolicy.Checked && !chkOutpolicy.Checked)
                {
                    resultDisplay = corpResults.ToArray();
                }
                else if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId != 0 && request.AppliedPolicy && (hdnFilter.Value == "Policy" || hdnFilter.Value== "Airline"))
                {

                    bool isPolicyChecked = (chkInpolicy.Checked ? true : false);

                    results.ToList().ForEach(searchResult =>
                    {
                        if (searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == isPolicyChecked)
                        {
                            corpResults.Add(searchResult);
                        }
                    });
                    
                    resultDisplay = corpResults.ToArray();
                }
                else
                {
                    resultDisplay = results.ToArray();
                }

                if (request.TimeIntervalSpecified)
                {
                    //if (hdnOnTimingFilter.Value != "Any Time" && request.Type == SearchType.Return && hdnRetTimingFilter.Value != "Any Time")
                    //    resultDisplay = results.FindAll(x => x.Flights[0][0].DepartureTime.ToString("HH:mm") == hdnOnTimingFilter.Value && x.Flights[1][0].DepartureTime.ToString("HH:mm") == hdnRetTimingFilter.Value).ToArray();
                    //else if (hdnOnTimingFilter.Value != "Any Time" && (hdnRetTimingFilter.Value == "Any Time" || request.Type != SearchType.Return))
                    //    resultDisplay = results.FindAll(x => x.Flights[0][0].DepartureTime.ToString("HH:mm") == hdnOnTimingFilter.Value).ToArray();
                    //else if (request.Type == SearchType.Return && hdnOnTimingFilter.Value == "Any Time" && hdnRetTimingFilter.Value != "Any Time")
                    //    resultDisplay = results.FindAll(x => x.Flights[1][0].DepartureTime.ToString("HH:mm") == hdnOnTimingFilter.Value).ToArray();

                    if (request.Type == SearchType.Return)
                        resultDisplay = results.FindAll(x => x.Flights[0][0].DepartureTime.ToString("HH:mm") == (hdnOnTimingFilter.Value != "Any Time" ? hdnOnTimingFilter.Value : x.Flights[0][0].DepartureTime.ToString("HH:mm")) && x.Flights[1][0].DepartureTime.ToString("HH:mm") == (hdnRetTimingFilter.Value != "Any Time" ? hdnRetTimingFilter.Value : x.Flights[1][0].DepartureTime.ToString("HH:mm"))).ToArray();
                    else
                        resultDisplay = results.FindAll(x => x.Flights[0][0].DepartureTime.ToString("HH:mm") == (hdnOnTimingFilter.Value != "Any Time" ? hdnOnTimingFilter.Value : x.Flights[0][0].DepartureTime.ToString("HH:mm"))).ToArray();
                }
                Session["FilteredAirResults"] = resultDisplay;

                filteredResults = DoPaging(resultDisplay);

                Session["FlightResults"] = filteredResults;
                if (filteredResults != null && filteredResults.Length > 0)
                {
                    dlFlightResults.Visible = true;
                    dlFlightResults.DataSource = filteredResults;
                    dlFlightResults.DataBind();
                }
                else
                {
                    dlFlightResults.Visible = false;
                }


                pageNo = currentPageNo;
                #region Session["sortString"] explanation
                //M|A|E|N-SS(P|Dpt|Arr|Dur)|AD-PN-MP-STPD|STP1|STP2-...
                //(MAEN=Morning, afternoon, evening, night. SS= sort by price, dep time, arrival time, or duration,AD=ascending or descending, Pn=Page no, STP=no of stops, MP=maximun price 
                /* In M|A|E|N every check box which will be checked, value will be 1 else 0, by default all are checked so the string will be 1|1|1|1
                 * For SS(P|Dpt|AT|Dur)|AD by default sorting is for price in ascending order, so value for SS is "P" and AD is "A" so the string will be -P|A
                 * Pn is page no and by default it is 1st page so string will be: -1
                 * MP is Not Defined when results come for the 1st time. The value is set after user sets it.
                 * STPDirect, STP1 and STP2 or more, by default all are selected, even if the flight is direct, but after the results are shown, it is modified to either the no of stops actually present in the segement or the no of stops selected by the user. String is -1|1|1
                 * The fianl default string will be: 1|1|1|1-P|A-1-ND-1|1|1
                 */
                /// <summary>
                /// Saves a string per session id, using which sorting and filtering of search results is done when request for choose another fare is generated from PassengerDetail.aspx page.
                /// Default string: 1|1|1|1-P|A-1-0-1|1|1
                /// Initial string seperator: '-' , sub string seperator: '|'
                /// 1|1|1|1 : For Morning, Afternoon, Evening, Night respectively. Value is 1 for chech box selected, else 0.
                /// P|A : For sorting of results. P stands for sorting by Price. P can be replaced by: Dep(Departure Time), Arr(Arrival time, in case of return flight) and Dur(Duration). A stands for Ascending, Can be replaced by D(Descending).
                /// 1 : This comes after P|A, this is the page number which was selected by the user.
                /// 0 : This comes after P|A-1-, this stands for maximum price to be selected by the user. Currently it is not used in the structure.
                /// 1|1|1 : For number of stops selected by the user. Direct, one stop and two or more stops respectively. Value is 1 if check box is selected or is available, else 0.
                /// </summary>
                #endregion
                Session["sortString"] = sortString;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
        }
    }

    protected string GetAirlineName(string airlineCode)
    {
        string airlineName = string.Empty;

        try
        {
            Airline airline = new Airline();
            airline.Load(airlineCode);
            airlineName = airline.AirlineName;
        }
        catch { }

        return airlineName;
    }

    protected int GetTotalDuration(SearchResult result)
    {
        int totalMinutes = 0;

        for (int i = 0; i < result.Flights.Length; i++)
        {
            for (int j = 0; j < result.Flights[i].Length; j++)
            {
                if (j > 0)
                {
                    totalMinutes += (int)Math.Ceiling(result.Flights[i][j].DepartureTime.Subtract(result.Flights[i][j - 1].ArrivalTime).TotalMinutes);
                }
                totalMinutes += (int)Math.Ceiling(result.Flights[i][j].Duration.TotalMinutes);               
               
            }
        }

        return totalMinutes;
    }

    protected int GetFlyingDuration(SearchResult result)
    {
        int totalMinutes = 0;

        for (int i = 0; i < result.Flights.Length; i++)
        {
            for (int j = 0; j < result.Flights[i].Length; j++)
            {
                totalMinutes += (int)Math.Ceiling(result.Flights[i][j].Duration.TotalMinutes);
            }
        }

        return totalMinutes;
    }

    /// <summary>
    /// Method used for Clearing all filters and show original results
    /// </summary>
    protected void ClearFilters()
    {        
        hdnPriceMin.Value = hdnMinValue.Value;//Reset slider min value to default
        hdnPriceMax.Value = hdnMaxValue.Value;//Reset slider max value to default        
        //Sort by price (Default sorting)
        SearchResult[] tempResult = null;
        if(Session["Results"] != null)
        {
            tempResult = Session["Results"] as SearchResult[];
        }
        else
        {
            Response.Redirect("HotelSearch.aspx?source=Flight", true);
        }
        List<SearchResult> Results = new List<SearchResult>();
        Results.AddRange(tempResult);

        //Results.Sort(delegate(SearchResult p1, SearchResult p2)// Shortest and Cheapest
        //{
        //    int compare = p1.Flights[0][0].Stops.CompareTo(p2.Flights[0][0].Stops);//Sort by shortest flight first
        //    if (compare == 0)
        //    {
        //        compare = p1.TotalFare.CompareTo(p2.TotalFare);//then sort by cheapest in shortest
        //    }
        //    return compare;
        //});
        if (Settings.LoginInfo.IsCorporate == "Y") //Added by Suresh for Corporate Users policy filter checkbox visible condition
        {
            chkInpolicy.Visible = Results.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == true).ToArray().Length > 0 ? true : false;
            chkOutpolicy.Visible = Results.ToList().Where(searchResult => searchResult.TravelPolicyResult != null && searchResult.TravelPolicyResult.IsUnderPolicy == false).ToArray().Length > 0 ? true : false;
        }
        tempResult = Results.ToArray();

        Session["FilteredAirResults"] = tempResult;
        filteredResults = DoPaging(tempResult);
        
        dlFlightResults.Visible = true;
        dlFlightResults.DataSource = filteredResults;
        dlFlightResults.DataBind();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        if (Change.Value == "true")
        {
            isClearPageCount = true;
            SortResult();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlFlightResults_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                SearchResult resultObj = e.Item.DataItem as SearchResult;

                if (resultObj != null)
                {

                    #region Old Commented code
                    //Label lblOnCarrierName = e.Item.FindControl("lblOnCarrierName") as Label;
                    //Label lblOnOrigin = e.Item.FindControl("lblOnOrigin") as Label;
                    //Label lblOnDepartureDate = e.Item.FindControl("lblOnDepartureDate") as Label;
                    //Label lblOnDestination = e.Item.FindControl("lblOnDestination") as Label;
                    //Label lblOnArrivalDate = e.Item.FindControl("lblOnArrivalDate") as Label;
                    //Label lblOnOrigin1 = e.Item.FindControl("lblOnOrigin1") as Label;
                    //Label lblOnDepartureDate1 = e.Item.FindControl("lblOnDepartureDate1") as Label;
                    //Label lblOnDestination1 = e.Item.FindControl("lblOnDestination1") as Label;
                    //Label lblOnArrivalDate1 = e.Item.FindControl("lblOnArrivalDate1") as Label;

                    //Label lblOnOrigin2 = e.Item.FindControl("lblOnOrigin2") as Label;
                    //Label lblOnDepartureDate2 = e.Item.FindControl("lblOnDepartureDate2") as Label;
                    //Label lblOnDestination2 = e.Item.FindControl("lblOnDestination2") as Label;
                    //Label lblOnArrivalDate2 = e.Item.FindControl("lblOnArrivalDate2") as Label;

                    //Label lblOnwardStops = e.Item.FindControl("lblOnwardStops") as Label;
                    //Label lblOnwardDuration = e.Item.FindControl("lblOnwardDuration") as Label;
                    //Label lblOnwardDuration1 = e.Item.FindControl("lblOnwardDuration1") as Label;
                    //Label lblOnwardDuration2 = e.Item.FindControl("lblOnwardDuration2") as Label;

                    //Label lblRemarks = e.Item.FindControl("lblRemarks") as Label; // To show Airline Remarks
                    //Label lblRetCarrierName = e.Item.FindControl("lblRetCarrierName") as Label;
                    //Label lblRetRemarks = e.Item.FindControl("lblRetRemarks") as Label;
                    //Label lblRetOrigin = e.Item.FindControl("lblRetOrigin") as Label;
                    //Label lblRetDepartureDate = e.Item.FindControl("lblRetDepartureDate") as Label;
                    //Label lblRetDestination = e.Item.FindControl("lblRetArrival") as Label;
                    //Label lblRetArrivalDate = e.Item.FindControl("lblRetArrivalDate") as Label;
                    //Label lblRetOrigin1 = e.Item.FindControl("lblRetOrigin1") as Label;
                    //Label lblRetDepartureDate1 = e.Item.FindControl("lblRetDepartureDate1") as Label;
                    //Label lblRetDestination1 = e.Item.FindControl("lblRetArrival1") as Label;
                    //Label lblRetArrivalDate1 = e.Item.FindControl("lblRetArrivalDate1") as Label;

                    //Label lblRetOrigin2 = e.Item.FindControl("lblRetOrigin2") as Label;
                    //Label lblRetDepartureDate2 = e.Item.FindControl("lblRetDepartureDate2") as Label;
                    //Label lblRetDestination2 = e.Item.FindControl("lblRetArrival2") as Label;
                    //Label lblRetArrivalDate2 = e.Item.FindControl("lblRetArrivalDate2") as Label;

                    //Label lblReturnStops = e.Item.FindControl("lblReturnStops") as Label;
                    //Label lblReturnDuration = e.Item.FindControl("lblReturnDuration") as Label;
                    //Label lblReturnDuration1 = e.Item.FindControl("lblReturnDuration1") as Label;
                    //Label lblReturnDuration2 = e.Item.FindControl("lblReturnDuration2") as Label;


                    //Image imgOnwardLogo = e.Item.FindControl("imgOnwardLogo") as Image;
                    //Label lblOnCarrier = e.Item.FindControl("lblOnCarrier") as Label;
                    //Image imgReturnLogo = e.Item.FindControl("imgReturnLogo") as Image;
                    //Label lblRetCarrier = e.Item.FindControl("lblRetCarrier") as Label;
                    //Image imgReturn = e.Item.FindControl("imgReturn") as Image;
                    //Added brahmam
                    //Label lblOnwardClass = e.Item.FindControl("lblOnwardClass") as Label;
                    //Label lblReturnClass = e.Item.FindControl("lblReturnClass") as Label;
                    //sai
                    //brahmam 

                    //HtmlGenericControl divOnwards = e.Item.FindControl("divOnwards") as HtmlGenericControl;
                    //HtmlGenericControl divOnwards1 = e.Item.FindControl("divOnwards1") as HtmlGenericControl;
                    //HtmlTable tblOnwards = e.Item.FindControl("tblOnwards") as HtmlTable;
                    ////HtmlTable tblOnwards1 = e.Item.FindControl("tblOnwards1") as HtmlTable;
                    //HtmlTable tblStops = e.Item.FindControl("tblStops") as HtmlTable;
                    //HtmlTable tblDuration = e.Item.FindControl("tblDuration") as HtmlTable;divStops
                    //HtmlGenericControl divStops = e.Item.FindControl("divStops") as HtmlGenericControl;

                    //HtmlGenericControl divReturn1 = e.Item.FindControl("divReturn1") as HtmlGenericControl;
                    //HtmlGenericControl divReturn = e.Item.FindControl("divReturn") as HtmlGenericControl;
                    //HtmlTable tblReturn = e.Item.FindControl("tblReturn") as HtmlTable;
                    //HtmlTable tblReturn1 = e.Item.FindControl("tblReturn1") as HtmlTable;
                    //HtmlGenericControl divRtnStops = e.Item.FindControl("divRtnStops") as HtmlGenericControl; 
                    //HtmlTable tblRtnStops = e.Item.FindControl("tblRtnStops") as HtmlTable;
                    //HtmlTable tblRtnDuration = e.Item.FindControl("tblRtnDuration") as HtmlTable;
                    // 
                    #endregion

                    Label lblFareSummary = e.Item.FindControl("lblFareSummary") as Label;

                    Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                    Label lblBookingSource = e.Item.FindControl("lblBookingSource") as Label;
                    HtmlGenericControl divMain = e.Item.FindControl("divMain") as HtmlGenericControl;
                    Label lblBabbage = e.Item.FindControl("lblBaggage") as Label;
                    Literal lblTotalDuration = e.Item.FindControl("lblTotalDuration") as Literal;
                    HtmlTable flightDetails = e.Item.FindControl("flightDetails") as HtmlTable;
                    HyperLink btnBookNow = e.Item.FindControl("btnBookNow") as HyperLink;
                    
                    //Added by lokesh on 7-June-2018
                    //Need to display the additional label as Global Fares for PK Source
                    Label lblPKGlobalFares = e.Item.FindControl("lblPKGlobalFares") as Label;
                    int paxCount = 0;
                    request = null;
                  
                    if (Session["FlightRequest"] != null)
                    {
                        request = Session["FlightRequest"] as SearchRequest;
                        paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                    }

                    //string FareSummary = " <div style='position: absolute; top: 190px; left: 350px; z-index: 1; display: none' id='fare_summary_" + resultObj.ResultId + "'><div class='cnt091'>"
                    //    + "<h3>Fare summary</h3><table width='100%' border='0' cellpadding='0' cellspacing='0' class='hd_tabl'><tr><td width='54%'>Passengers</td><td width='46%'>"
                    //    + "Adult X " + request.AdultCount + ", Child X " + request.ChildCount + ", Infants X " + request.InfantCount + "</td></tr><tr><td>Base Fare</td><td>AED " + resultObj.BaseFare.ToString("0.000") + "</td></tr><tr><td>Taxes &amp; Fees</td>"
                    //    + "<td>AED " + resultObj.Tax.ToString("0.000") + " & " + resultObj.Fees + "</td></tr><tr><td>Per Passenger</td><td>AED " + resultObj.TotalFare.ToString("0.000") + " X " + paxCount + "</td></tr>"
                    //    + "<tr><td>Total Fare</td><td>AED " + resultObj.TotalFare.ToString("0.000") + "</td></tr><tr><td>Total Price</td><td>AED " + resultObj.TotalFare.ToString("0.000")
                    //    + "</td></tr><tr><td colspan='2'><div class='term_s'><span>*</span> There might a "
                    //    + "slight difference in the total price in fare summarydue to rounding off.</div></td></tr></table></div></div>";
                    //lblFareSummary.Text = FareSummary;

                    if (resultObj.ResultBookingSource == BookingSource.TBOAir && !resultObj.IsLCC)
                    {
                        btnBookNow.ToolTip = "VOID restrictions applicable on this flight. Kindly contact Support center before proceeding.";
                        btnBookNow.Attributes.Add("data-toggle", "tooltip");
                        btnBookNow.Attributes.Add("data-placement","bottom");                        
                    }


                    // For Fare Summary Break up
                    int adultCount = 0;
                    int childCount = 0;
                    int infantCount = 0;
                    decimal adultBaseFare = 0;
                    decimal childBaseFare = 0;
                    decimal infantBaseFare = 0;
                    decimal adultTax = 0;
                    decimal childTax = 0;
                    decimal infantTax = 0;
                    decimal totalFare = 0;
                    decimal adultMarkup = 0;
                    decimal childMarkup = 0;
                    decimal infantMarkup = 0;
                    int decimalPoint = 0;
                    decimal otherCharges = 0;
                    decimal discount = 0;
                    if (resultObj.ResultBookingSource == BookingSource.TBOAir)
                    {
                        otherCharges = ((resultObj.Price.OtherCharges + resultObj.Price.TransactionFee + resultObj.Price.SServiceFee) / (request.AdultCount + request.ChildCount + request.InfantCount));
                    }
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        decimalPoint = Settings.LoginInfo.DecimalValue;
                    }

                    decimal inputVAT = 0m;

                    if(resultObj.Price.TaxDetails !=null && resultObj.Price.TaxDetails.InputVAT != null && !resultObj.Price.TaxDetails.InputVAT.CostIncluded)
                    {
                        inputVAT = resultObj.Price.InputVATAmount;
                    }
                  
                    string FareSummary = " <div style='position: absolute; margin-left:auto; margin-right:auto; left:0; right:0; width:600px; z-index: 9999; display: none' id='fare_summary_" + resultObj.ResultId + "'><div class='cnt091'>"
                        + "<h4>Fare summary</h4>"
                        + "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='hd_tabl'><tr><td><b>Passengers</b></td><td><b>Base Fare</b></td><td><b>Taxes & Fees</b></td><td><b>Per Passenger</b></td><td><b>Total Fare</b></td></tr>";
                    for (int i = 0; i < resultObj.FareBreakdown.Length; i++)
                    {
                        if (resultObj.FareBreakdown[i].PassengerType == PassengerType.Adult)
                        {
                            adultCount = resultObj.FareBreakdown[i].PassengerCount;
                            adultMarkup = (resultObj.FareBreakdown[i].AgentMarkup);
                            adultBaseFare = ((decimal)resultObj.FareBreakdown[i].BaseFare / adultCount) + resultObj.FareBreakdown[i].HandlingFee / adultCount;
                            discount += resultObj.FareBreakdown[i].AgentDiscount;

                            adultTax = (resultObj.FareBreakdown[i].Tax + resultObj.FareBreakdown[i].AdditionalTxnFee + resultObj.FareBreakdown[i].AirlineTransFee + adultMarkup) / adultCount;
                            adultTax += otherCharges;                            

                            // Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Tax"+resultObj.FareBreakdown[i].Tax.ToString() +",addTXNfee "+ resultObj.FareBreakdown[i].AdditionalTxnFee.ToString() +",AilineFee "+ resultObj.FareBreakdown[i].AirlineTransFee.ToString() +", adt markup "+ adultMarkup, "0");
                            totalFare += (Math.Round((adultBaseFare + adultTax), decimalPoint) * adultCount);
                            //totalFare += inputVAT;
                            FareSummary += "<tr><td >Adults</td><td align='center'>" + adultBaseFare.ToString("N" + decimalPoint) + "</td><td align='center'>" + (adultTax ).ToString("N" + decimalPoint) + "</td><td align='center'> " + (adultBaseFare + adultTax).ToString("N" + decimalPoint) + " X " + adultCount + "</td><td align='right'> " + (Math.Round((adultBaseFare + adultTax), decimalPoint) * adultCount).ToString("N" + decimalPoint) + "</td></tr>";
                        }
                        if (resultObj.FareBreakdown[i].PassengerType == PassengerType.Child)
                        {
                            childCount = resultObj.FareBreakdown[i].PassengerCount;
                            childMarkup = (resultObj.FareBreakdown[i].AgentMarkup);
                            childBaseFare = ((decimal)resultObj.FareBreakdown[i].BaseFare / childCount) + resultObj.FareBreakdown[i].HandlingFee / childCount;
                            discount += resultObj.FareBreakdown[i].AgentDiscount;

                            childTax = (resultObj.FareBreakdown[i].Tax + resultObj.FareBreakdown[i].AdditionalTxnFee + resultObj.FareBreakdown[i].AirlineTransFee + childMarkup) / childCount;
                            childTax += otherCharges;
                            totalFare += (Math.Round((childBaseFare + childTax), decimalPoint) * childCount);
                            FareSummary += "<tr><td >Childs</td><td align='center'>" + childBaseFare.ToString("N" + decimalPoint) + "</td><td align='center'>" + childTax.ToString("N" + decimalPoint) + "</td><td align='center'> " + (childBaseFare + childTax).ToString("N" + agency.DecimalValue) + " X " + childCount + "</td><td align='right'> " + (Math.Round((childBaseFare + childTax), decimalPoint) * childCount).ToString("N" + agency.DecimalValue) + "</td></tr>";
                        }
                        if (resultObj.FareBreakdown[i].PassengerType == PassengerType.Infant && resultObj.FareBreakdown[i].PassengerCount > 0)
                        {
                            infantCount = resultObj.FareBreakdown[i].PassengerCount;
                            infantMarkup = (resultObj.FareBreakdown[i].AgentMarkup);
                            infantBaseFare = ((decimal)resultObj.FareBreakdown[i].BaseFare / infantCount) + resultObj.FareBreakdown[i].HandlingFee / infantCount;
                            discount += resultObj.FareBreakdown[i].AgentDiscount;

                            infantTax = (resultObj.FareBreakdown[i].Tax + resultObj.FareBreakdown[i].AdditionalTxnFee + resultObj.FareBreakdown[i].AirlineTransFee + infantMarkup) / infantCount;
                            infantTax += otherCharges;
                            totalFare += (Math.Round((infantBaseFare + infantTax), decimalPoint) * infantCount);
                            FareSummary += "<tr><td>Infants</td><td align='center'>" + infantBaseFare.ToString("N" + decimalPoint) + "</td><td align='center'>" + infantTax.ToString("N" + decimalPoint) + "</td><td align='center'> " + (infantBaseFare + infantTax).ToString("N" + agency.DecimalValue) + " X " + infantCount + "</td><td align='right'> " + (Math.Round((infantBaseFare + infantTax), decimalPoint) * infantCount).ToString("N" + agency.DecimalValue) + "</td></tr>";
                        }
                    }
                    discount = Math.Round(discount, agency.DecimalValue);
                    if (resultObj.ResultBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                    {
                        if (discount > 0)
                        {
                            FareSummary += "<tr><td colspan='4'>Discount</td><td align='right'>(-) " + (discount).ToString("N" + agency.DecimalValue) + "</td></tr>";
                        }
                        FareSummary += "<tr><td colspan='4'><b>Total Price</b></td><td align='right'><b> " + agency.AgentCurrency + " " + Math.Ceiling(totalFare - discount).ToString("N" + agency.DecimalValue) + "</b></td></tr>";
                    }
                    else
                    {
                        if (discount > 0)
                        {
                            FareSummary += "<tr><td colspan='4'>Discount</td><td align='right'>(-) " + (discount).ToString("N" + agency.DecimalValue) + "</td></tr>";
                        }
                        FareSummary += "<tr><td colspan='4'><b>Total Price</b></td><td align='right'><b> " + agency.AgentCurrency + " " + resultObj.TotalFare.ToString("N" + agency.DecimalValue) + "</b></td></tr>";
                    }
                    FareSummary += "<tr><td colspan='5'><div class='term_s'><span>*</span> There might a slight difference in the total price in fare summary due to rounding off.</div></td></tr></table></div></div>";

                    lblFareSummary.Text = FareSummary;

                    HtmlTable tblBaggage = e.Item.FindControl("BaggageTable") as HtmlTable;

                    #region Coporate Policy Logic
                    try
                    {
                        //Note:Previously We do not have any predefined corporate profiles
                        //Note:Now we have predefined corporate profiles -1 Client -2 Vendor
                        //So verifying CorporateProfileId != 0
                        if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId != 0 && request.AppliedPolicy && resultObj.TravelPolicyResult != null)
                        {
                            HtmlGenericControl DivPrefAirline = e.Item.FindControl("DivPrefAirline") as HtmlGenericControl;
                            HtmlGenericControl DivInOut = e.Item.FindControl("DivInOut") as HtmlGenericControl;
                            HtmlGenericControl DivNegotiatedFare = e.Item.FindControl("DivNegotiatedFare") as HtmlGenericControl;
                            HtmlGenericControl DivLLF = e.Item.FindControl("DivLLF") as HtmlGenericControl;

                            Image ImgPreferredAirline = e.Item.FindControl("ImgPreferredAirline") as Image;
                            Image ImgInOutPolicy = e.Item.FindControl("ImgInOutPolicy") as Image;
                            HtmlGenericControl spnInOutPolicy = e.Item.FindControl("spnInOutPolicy") as HtmlGenericControl;
                            Image ImgNegotiatedFare = e.Item.FindControl("ImgNegotiatedFare") as Image;

                            string preferredAirline = string.Empty, policybreakingrules = string.Empty;

                            foreach (KeyValuePair<string, List<string>> pair in resultObj.TravelPolicyResult.PolicyBreakingRules)
                            {
                                if (pair.Key == "PREFERREDAIRLINES")
                                {
                                    foreach (string rule in pair.Value)
                                    {
                                        if (preferredAirline.Length > 0)
                                        {
                                            preferredAirline += "," + rule;
                                        }
                                        else
                                        {
                                            preferredAirline = rule;
                                        }
                                    }
                                }
                                //else if (pair.Key != "RESTRICTBOOKING" && pair.Key != "AVOIDTRANSITTIME" && pair.Key != "TOTALBOOKINGAMOUNT" && pair.Key != "BOOKINGTHRESHOLD")
                                else if (pair.Key != "AVOIDTRANSITTIME" && pair.Key != "TOTALBOOKINGAMOUNT" && pair.Key != "BOOKINGTHRESHOLD")
                                {
                                    foreach (string rule in pair.Value)
                                    {
                                        if (policybreakingrules.Length > 0)
                                        {
                                            policybreakingrules += "," + rule;
                                        }
                                        else
                                        {
                                            policybreakingrules = rule;
                                        }
                                    }
                                    if (pair.Key == "RESTRICTBOOKING")
                                    {
                                        if (pair.Value[0] == "Y" && !resultObj.TravelPolicyResult.IsUnderPolicy)
                                        {
                                            policybreakingrules = policybreakingrules.Replace("Y", "Restricted Booking");
                                            btnBookNow.Visible = false;
                                        }
                                    }
                                }
                                
                                else if(pair.Key == "TOTALBOOKINGAMOUNT")
                                {
                                    policybreakingrules += ", Booking Amount Exceeded for TOTAL OOKING AMOUNT";
                                }
                                else if (pair.Key == "BOOKINGTHRESHOLD")
                                {
                                    policybreakingrules += ", BOOKING THRESHOLD";
                                }
                            }

                            if (!resultObj.TravelPolicyResult.IsPreferredAirline)
                            {
                                //If not Preferred Airline hide the Icon
                                if (DivPrefAirline != null)
                                {
                                    ImgPreferredAirline.ImageUrl = "images/icon-not-preferred-flight.png";
                                    ImgPreferredAirline.AlternateText = "Non-Preferred Airline";
                                    //ImgPreferredAirline.ToolTip = ImgPreferredAirline.AlternateText;
                                    ImgPreferredAirline.ToolTip = "Non-Preferred Airline";
                                    DivPrefAirline.Attributes.Add("title", (preferredAirline.Length > 0 ? preferredAirline : "Non-Preferred Airline"));
                                    DivPrefAirline.Attributes.CssStyle.Add("display", "block");
                                }
                            }
                            else
                            {
                                ImgPreferredAirline.ImageUrl = "images/icon-preferred-flight.png";
                                ImgPreferredAirline.AlternateText = "Preferred Airline";
                                ImgPreferredAirline.ToolTip = ImgPreferredAirline.AlternateText;
                                DivPrefAirline.Attributes.Add("title", (preferredAirline.Length > 0 ? preferredAirline : "Preferred Airline"));
                                DivPrefAirline.Attributes.CssStyle.Add("display", "block");
                            }

                            if (resultObj.TravelPolicyResult.IsNegotiatedFare)
                            {
                                DivNegotiatedFare.Attributes.CssStyle.Add("display", "block");
                            }
                            // Display Base LLF invetory
                            if (resultObj.TravelPolicyResult.IsLLFResult)
                            {
                                DivLLF.Attributes.CssStyle.Add("display", "block");
                            }



                            if (!resultObj.TravelPolicyResult.IsUnderPolicy)//Outside policy
                            {
                                if (DivInOut != null)
                                {
                                    DivInOut.Attributes.Add("title","Out of policy - "+ policybreakingrules);
                                    ImgInOutPolicy.ImageUrl = "images/icon-outside-policy.png";
                                    ImgInOutPolicy.AlternateText = "Out of policy";
                                    //spnInOutPolicy.te = "Outside Policy";
                                    //ImgInOutPolicy.ToolTip = ImgInOutPolicy.AlternateText;
                                    spnInOutPolicy.InnerText = "Out of policy - "+ policybreakingrules;
                                    DivInOut.Attributes.CssStyle.Add("display", "block");
                                }
                                //Employee is of VIP grade
                                //if (Settings.LoginInfo.CorporateProfileGrade == "V")//TODO: Ziya
                                if (Settings.LoginInfo.IsCorporate == "Y")//TODO: Ziya
                                {
                                    //In order to show security error message or reason code popup 
                                    btnBookNow.Attributes.Add("onclick", "Book('" + resultObj.ResultId + "');return false;");
                                }
                                else
                                {
                                    //In order to show popup for travel reason code input
                                    btnBookNow.Attributes.Add("onclick", "ShowPopup('" + resultObj.ResultId + "');");
                                }
                            }
                            else //Inside Policy
                            {
                                if (DivInOut != null)
                                {
                                    ImgInOutPolicy.ImageUrl = "images/icon-inside-policy.png";
                                    ImgInOutPolicy.AlternateText = "In Policy";
                                    ImgInOutPolicy.ToolTip = ImgInOutPolicy.AlternateText;
                                    spnInOutPolicy.InnerText = ImgInOutPolicy.AlternateText;
                                    DivInOut.Attributes.CssStyle.Add("display", "block");
                                }
                                //Employee is of VIP grade
                                //if (Settings.LoginInfo.CorporateProfileGrade == "V")//TODO: Ziya
                               if (Settings.LoginInfo.IsCorporate == "Y")//TODO: Ziya
                                {
                                    //In order to show security error message or reason code popup 
                                    btnBookNow.Attributes.Add("onclick", "Book('" + resultObj.ResultId + "');");
                                }
                                else
                                {
                                    //Directly go to passenger page
                                    btnBookNow.Attributes.Add("onclick", "ShowLoader('" + resultObj.ResultId + "','" + "0" + "');");
                                    btnBookNow.NavigateUrl = "PassengerDetails.aspx";
                                }
                            }
                        }
                        else
                        {
                            btnBookNow.Attributes.Add("onclick", "ShowLoader('" + resultObj.ResultId + "','" + "0" + "');");
                            btnBookNow.NavigateUrl = "PassengerDetails.aspx";

                        }
                    }
                    catch { }

                    #endregion

                    //lblPrice.Text = resultObj.TotalFare.ToString("0.00");
                    if (resultObj.ResultBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                    {
                        lblPrice.Text = Math.Ceiling(resultObj.TotalFare).ToString("N" + decimalPoint);
                    }
                    else
                    {
                        lblPrice.Text = resultObj.TotalFare.ToString("N" + decimalPoint);
                    }
                    //if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                    if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                    {
                        lblBookingSource.Text = resultObj.ResultBookingSource.ToString();                        
                    }
                    else
                    {
                        lblBookingSource.Visible = false;                        
                    }
                    //Added by lokesh on 7-June-2018
                    //Need to display the additional label as Global Fares for PK Source
                    //The below label should be displayed only if the booking source is PK Fares and only to the base agent
                    if (resultObj.ResultBookingSource == BookingSource.PKFares)
                    {
                        lblPKGlobalFares.Visible = true;
                    }
                    else
                    {
                        lblPKGlobalFares.Visible = false;
                    }

                    int totalDuration = 0, flyingDuration = 0;
                    for (int i = 0; i < resultObj.Flights.Length; i++)
                    {

                        //if (resultObj.Flights.Length <= 1)
                        //{
                        //    imgReturn.Visible = false;
                        //    //imgReturnLogo.Visible = false;
                        //}
                        string rootFolder = Airline.logoDirectory + "/";
                        if (i == 0) // out bound flights (INWARDS FLIGHTS)
                        {

                            string remarks = string.Empty;
                            //Changed by brahmam
                            HtmlGenericControl divOutBound = new HtmlGenericControl("Div"); //onwards Main div Added by brahmam
                            if (resultObj.Flights.Length > 1)
                            {
                                divOutBound.Attributes.Add("class", "border_bot1 borright paddingtop_10 onwards");
                            }
                            else
                            {
                                divOutBound.Attributes.Add("class", "paddingtop_10 onwards");
                            }

                            HtmlGenericControl divMainOrigin = new HtmlGenericControl("Div");   //Onway Origin Main div   Added by brahmam
                            divMainOrigin.Attributes.Add("class", "col-md-10 pad0");
                            for (int j = 0; j < resultObj.Flights[i].Length; j++)
                            {
                                /**************************************************************************************
                                 *                          Reading Baggage details from Airline
                                 * ************************************************************************************/
                                string baggage = string.Empty;
                                FlightInfo finfo = resultObj.Flights[i][j];

                                if (resultObj.Flights[i].Length > 0 && j > 0)
                                {
                                    totalDuration += (int)Math.Ceiling(resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime).TotalMinutes);
                                }
                                totalDuration += (int)Math.Ceiling(resultObj.Flights[i][j].Duration.TotalMinutes);
                                flyingDuration += (int)Math.Ceiling(resultObj.Flights[i][j].Duration.TotalMinutes);

                                //Modified By Suresh V for Binding the default Baggage for All sources segment wise 
                                if (resultObj.BaggageIncludedInFare != null)
                                {
                                    baggage = (!string.IsNullOrEmpty(resultObj.Flights[i][j].DefaultBaggage)) ? resultObj.Flights[i][j].DefaultBaggage : "As Per Airline Policy";
                                }
                                else
                                {
                                    baggage = "As Per Airline Policy";
                                }
                                HtmlTableRow row = new HtmlTableRow();
                                HtmlTableCell cell1 = new HtmlTableCell();
                                HtmlTableCell cell2 = new HtmlTableCell();
                                cell1.InnerText = finfo.Origin.CityCode + " - " + finfo.Destination.CityCode + " " + finfo.Airline + " " + finfo.FlightNumber;
                                cell2.InnerText = baggage;
                                row.Cells.Add(cell1);
                                row.Cells.Add(cell2);
                                tblBaggage.Rows.Add(row);

                                /**************************************************************************************
                                 *                               End Baggage details 
                                 * ************************************************************************************/
                                HtmlGenericControl divMainImages = new HtmlGenericControl("Div"); //onwards Images Main div
                                divMainImages.Attributes.Add("class", "col-md-2 text-center text-lg-left");

                                HtmlGenericControl divOnwardLogo = new HtmlGenericControl("Div");
                                Image imgOnwardLogo = new Image();
                                imgOnwardLogo.Height = new Unit(20, UnitType.Pixel);

                                Airline departingAirline = new Airline();
                                departingAirline.Load(resultObj.Flights[i][j].Airline);


                                HtmlGenericControl divOnCarrier = new HtmlGenericControl("Div");
                                Label lblOnCarrier = new Label();


                                HtmlGenericControl divRefundImage = new HtmlGenericControl("Div");
                                divRefundImage.Attributes.Add("class", "iconpad");
                                Image aircraftImage = new Image();
                                aircraftImage.ImageUrl = "images/aircraft_icon.png";
                                aircraftImage.ToolTip = (resultObj.ResultBookingSource == BookingSource.AirArabia ? resultObj.Flights[i][j].Airline + " " + resultObj.Flights[i][j].FlightNumber : resultObj.Flights[i][j].Craft);
                                Image refundableImage = new Image();
                                refundableImage.ImageUrl = "images/refundable_icon.png";
                                refundableImage.ToolTip = "Refundable Fare";
                                Image nonRefundableImage = new Image();
                                nonRefundableImage.ImageUrl = "images/nonrefundable_icon.png";
                                nonRefundableImage.ToolTip = "Non Refundable Fare";

                                divRefundImage.Controls.Add(aircraftImage);
                                Label lbl = new Label();
                                lbl.Text = "&nbsp;";
                                divRefundImage.Controls.Add(lbl);//Create gap between airline logo and refund image
                                if (!resultObj.NonRefundable)
                                {
                                    divRefundImage.Controls.Add(refundableImage);
                                }
                                else
                                {
                                    divRefundImage.Controls.Add(nonRefundableImage);
                                }

                                //if (j == 0) //Commented by brahmam should be show all images
                                {

                                    imgOnwardLogo.ImageUrl = rootFolder + departingAirline.LogoFile;
                                    lblOnCarrier.Text = departingAirline.AirlineName + "<br/> (" + departingAirline.AirlineCode + ")" + " " + resultObj.Flights[i][j].FlightNumber;
                                    //lblOnCarrierName.Text = resultObj.Flights[i][j].FlightNumber;
                                    //Added by ravi to display airiine remarks for onward flights
                                    if (resultObj.ResultBookingSource == BookingSource.TBOAir || resultObj.ResultBookingSource == BookingSource.FlightInventory || resultObj.ResultBookingSource == BookingSource.CozmoBus)
                                    {
                                        remarks = !string.IsNullOrEmpty(resultObj.Flights[i][j].SegmentFareType) ? resultObj.Flights[i][j].SegmentFareType : string.Empty;
                                    }
                                    else if (resultObj.ResultBookingSource == BookingSource.AirArabia)
                                        remarks = !string.IsNullOrEmpty(resultObj.Flights[i][j].UapiSegmentRefKey) ? resultObj.Flights[i][j].UapiSegmentRefKey : string.Empty;
                                }
                                if (j == 0)
                                {
                                    divOnwardLogo.Controls.Add(imgOnwardLogo);
                                    divOnCarrier.Controls.Add(lblOnCarrier);

                                    //divOnCarrierName.Controls.Add(lblOnCarrierName);
                                    divMainImages.Controls.Add(divOnwardLogo);
                                    divMainImages.Controls.Add(divOnCarrier);
                                    divMainImages.Controls.Add(divRefundImage);
                                    divOutBound.Controls.Add(divMainImages);
                                }
                                //divMain.Controls.Add(divMainImages);
                                #region Binding Origin, Destination, Departure Date and Arrival Date
                                //HtmlGenericControl divMainOrigin = new HtmlGenericControl("Div");   //Onway Origin Main div   Added by brahmam
                                //divMainOrigin.Attributes.Add("class", "col-md-10 pad0");

                                HtmlGenericControl divMainOrigin1 = new HtmlGenericControl("Div");
                                if (j == 0 && resultObj.Flights[i].Length > 1)
                                {
                                    divMainOrigin1.Attributes.Add("class", "divider");
                                }

                                HtmlGenericControl divOnOrigin = new HtmlGenericControl("Div");
                                divOnOrigin.Attributes.Add("class", "col-xs-6 col-md-3");
                                Label lblOnOrigin = new Label();
                                lblOnOrigin.ID = "lblOnOrigin" + i + j.ToString();
                                //lblOnOrigin.Font.Bold = true;
                                lblOnOrigin.Text = "<strong>" + resultObj.Flights[i][j].Origin.CityName + "</strong></br>" + "" + resultObj.Flights[i][j].DepartureTime.ToString("dd-MMM,HH:mm tt") + "</br>";
                                if (!string.IsNullOrEmpty(resultObj.Flights[i][j].DepTerminal))
                                {
                                    if (resultObj.Flights[i][j].DepTerminal.ToLower().Contains("terminal"))
                                    {
                                        lblOnOrigin.Text += resultObj.Flights[i][j].DepTerminal;
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(resultObj.Flights[i][j].DepTerminal)) lblOnOrigin.Text += "Terminal " + resultObj.Flights[i][j].DepTerminal;
                                    }
                                }
                                divOnOrigin.Controls.Add(lblOnOrigin);


                                HtmlGenericControl divOnDestination = new HtmlGenericControl("Div");
                                divOnDestination.Attributes.Add("class", "col-xs-6 col-md-3 text-right text-lg-left");
                                Label lblOnDestination = new Label();
                                lblOnDestination.ID = "lblOnDestination" + i + j.ToString();
                                //lblOnDestination.Font.Bold = true;
                                lblOnDestination.Text = "<strong>" + resultObj.Flights[i][j].Destination.CityName + "</strong></br>" + "" + resultObj.Flights[i][j].ArrivalTime.ToString("dd-MMM,HH:mm tt") + "</br>";
                                if (!string.IsNullOrEmpty(resultObj.Flights[i][j].DepTerminal))
                                {
                                    if (resultObj.Flights[i][j].DepTerminal.ToLower().Contains("terminal"))
                                    {
                                        lblOnDestination.Text += resultObj.Flights[i][j].ArrTerminal;
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(resultObj.Flights[i][j].ArrTerminal)) lblOnDestination.Text += "Terminal " + resultObj.Flights[i][j].ArrTerminal;
                                    }
                                }
                                divOnDestination.Controls.Add(lblOnDestination);



                                //HtmlGenericControl divOnArrivalDate = new HtmlGenericControl("Div");
                                //Label lblOnArrivalDate = new Label();
                                //lblOnArrivalDate.ID = "lblOnArrivalDate" + i + j.ToString();
                                //lblOnArrivalDate.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //divOnArrivalDate.Controls.Add(lblOnArrivalDate);

                                //divMainDestination.Controls.Add(divOnArrivalDate);


                                #endregion

                                #region Binding Stops Information
                                //HtmlGenericControl divMainStops = new HtmlGenericControl("Div");  //onway MainStops Main div Added by brahmam
                                //divMainStops.Attributes.Add("class", "col-md-3 col-xs-6 col_xsfare col-md-push-6");
                                HtmlGenericControl divOnwardStops = new HtmlGenericControl("Div");
                                divOnwardStops.Attributes.Add("class", "text-center text-lg-left col-md-3");
                                //if (j == 0)
                                {
                                    //If VIA flight is there then update stops as ZERO. As we are using stops to show stops  
                                    //information further pages we need to correct this logic
                                    if ((resultObj.Flights[i].Select(f => f.FlightNumber).Distinct().Count() - 1) == 0)
                                        resultObj.Flights[i].ToList().ForEach(f => f.Stops = 0);
                                    else//Need to re assign the stops based on the stops we are displaying in the page. If any VIA flight is there need to reduce stops count
                                        resultObj.Flights[i].ToList().ForEach(f => f.Stops = (resultObj.Flights[i].Select(s => s.FlightNumber).Distinct().Count() - 1));

                                    Label lblOnwardStops = new Label();
                                    
                                    lblOnwardStops.ID = "lblOnwardStops" + i + j.ToString();

                                    switch (resultObj.Flights[i].Select(p => p.FlightNumber).Distinct().Count() - 1)
                                    {
                                        case 0:
                                            lblOnwardStops.Text = "Non Stop";
                                            break;
                                        case 1:
                                            lblOnwardStops.Text = "Single Stop";
                                            break;
                                        case 2:
                                            lblOnwardStops.Text = "Two Stops";
                                            break;
                                        default:
                                            lblOnwardStops.Text = "Two+ Stops";
                                            break;
                                    }
                                    Label lblOnwardClass = new Label();
                                    lblOnwardClass.ID = "lblOnwardClass" + i + j.ToString();
                                    lblOnwardClass.Font.Bold = true;

                                    if (resultObj.ResultBookingSource != BookingSource.Jazeera)//For J9 Source There Will Be J9 Business and J9 Economy Fare Types Only
                                    {
                                        if (j == 0 && resultObj.Flights[i][j].CabinClass != null && resultObj.ResultBookingSource != BookingSource.Indigo && resultObj.ResultBookingSource != BookingSource.IndigoCorp)// For indigo Class will be always Economy
                                        {
                                            //Displaying cabin class as Business in View Flight Details Tab 
                                            if((resultObj.ResultBookingSource==BookingSource.SpiceJet || resultObj.ResultBookingSource==BookingSource.SpiceJetCorp) && resultObj.Flights!=null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                            {
                                                lblOnwardClass.Text = resultObj.Flights[i][j].CabinClass + "</br>";
                                            }
                                            else if (resultObj.Flights[i][j].CabinClass != resultObj.Flights[i][j].BookingClass)//Avoid display C-C
                                            {
                                                lblOnwardClass.Text = resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                            else
                                            {
                                                lblOnwardClass.Text = resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                        }
                                        else if (j == 0 && resultObj.Flights[i][j].CabinClass != null)
                                        {
                                            lblOnwardClass.Text = resultObj.Flights[i][j].BookingClass + "</br>";
                                        }
                                        else if (resultObj.ResultBookingSource != BookingSource.TBOAir && resultObj.ResultBookingSource != BookingSource.UAPI && resultObj.ResultBookingSource != BookingSource.Indigo && resultObj.ResultBookingSource != BookingSource.PKFares && resultObj.ResultBookingSource != BookingSource.IndigoCorp)
                                        {
                                            lblOnwardClass.Text = "Economy-" + resultObj.Flights[i][j].BookingClass + "</br>";
                                        }
                                        else if (j > 0)
                                        {
                                            if (resultObj.Flights[i][j].CabinClass != resultObj.Flights[i][j].BookingClass)//Avoid display C-C
                                            {
                                                lblOnwardClass.Text = resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                            else
                                            {
                                                lblOnwardClass.Text = resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                        }
                                    }
                                    if (resultObj.ResultBookingSource == BookingSource.FlyDubai || resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.TBOAir || resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp || resultObj.ResultBookingSource == BookingSource.Jazeera || resultObj.ResultBookingSource==BookingSource.SalamAir || resultObj.ResultBookingSource==BookingSource.AirArabia)
                                    {
                                        if (resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp)
                                        {
                                            //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                            if (resultObj.ResultBookingSource==BookingSource.SpiceJet || resultObj.ResultBookingSource==BookingSource.SpiceJetCorp && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                            {
                                                lblOnwardClass.Text += "</br>";
                                            }
                                            else if (!lblOnwardClass.Text.Contains("Economy"))
                                            {
                                                lblOnwardClass.Text = "Economy</br>";
                                            }
                                        }
                                        if (resultObj.FareType != null)
                                        {
                                            if (request.Type != SearchType.OneWay)
                                            {
                                                if ((resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp || resultObj.ResultBookingSource == BookingSource.Jazeera || resultObj.ResultBookingSource == BookingSource.SalamAir) && !string.IsNullOrEmpty(resultObj.Flights[i][j].SegmentFareType))
                                                {
                                                    lblOnwardClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.Flights[i][j].SegmentFareType + "</b>";
                                                }
                                                else
                                                {
                                                    lblOnwardClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.FareType.Split(',')[0] + "</b>";
                                                }
                                            }
                                            else
                                            {
                                                if ((resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJet|| resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp|| resultObj.ResultBookingSource == BookingSource.Jazeera || resultObj.ResultBookingSource == BookingSource.SalamAir) && !string.IsNullOrEmpty(resultObj.Flights[i][j].SegmentFareType))
                                                {
                                                    lblOnwardClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.Flights[i][j].SegmentFareType + "</b>";
                                                }
                                                else
                                                {
                                                    lblOnwardClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.FareType + "</b>";
                                                }
                                            }
                                        }
                                    }
                                    lblOnwardStops.Text = lblOnwardStops.Text + "</br>" + lblOnwardClass.Text;
                                    divOnwardStops.Controls.Add(lblOnwardStops);
                                    //divMainStops.Controls.Add(divOnwardStops);
                                    //divMainStops.Controls.Add(divOnwardClass);
                                }
                                #endregion

                                #region Binding Duration Information

                                HtmlGenericControl divOnwardDuration = new HtmlGenericControl("Div"); //onway Duration Main Div
                                divOnwardDuration.Attributes.Add("class", "text-center text-lg-left col-md-3");
                                Label lblOnwardDuration = new Label();
                                lblOnwardDuration.ID = "lblOnwardDuration" + i + j.ToString();
                                //lblOnwardDuration.Font.Bold = true;
                                //if Duration Hours >= 24 then TotalHours is having decimals some times 
                                //so we need non decimal value. Thats why we are doing Math.Floor to get original value
                                //ex: TotalHours = 39.1232 Math.Floor(TotalHours) = 39
                                lblOnwardDuration.Text = (resultObj.Flights[i][j].Duration.Days >= 1 ? Math.Floor(resultObj.Flights[i][j].Duration.TotalHours) : resultObj.Flights[i][j].Duration.Hours) + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                lblOnwardDuration.Font.Bold = true;
                                divOnwardDuration.Controls.Add(lblOnwardDuration);

                                //divMainStops.Controls.Add(divOnwardDuration);

                                HtmlGenericControl divClear = new HtmlGenericControl("Div");
                                divClear.Attributes.Add("class", "clearfix");



                                divMainOrigin1.Controls.Add(divOnOrigin);
                                divMainOrigin1.Controls.Add(divOnDestination);
                                divMainOrigin1.Controls.Add(divOnwardStops);
                                divMainOrigin1.Controls.Add(divOnwardDuration);
                                divMainOrigin1.Controls.Add(divClear);
                                if (j == 0 && resultObj.Flights[i].Length > 1)
                                {
                                    if ((!resultObj.IsLCC && resultObj.Flights[i][j].OperatingCarrier != null && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0) || (resultObj.ResultBookingSource == BookingSource.Indigo && resultObj.Flights[i][j].OperatingCarrier != "6E" && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0))//Modified for Indigo CodeShare
                                    {
                                        Label lblCodeShare = new Label();
                                        lblCodeShare.Text = "Operated by : " + resultObj.Flights[i][j].OperatingCarrier;
                                        HtmlGenericControl divCodeShare = new HtmlGenericControl("Div");
                                        divCodeShare.Style.Add("text-align", "center");
                                        divCodeShare.Style.Add("font-weight", "bold");
                                        divCodeShare.Style.Add("class", "col-md-12");
                                        divCodeShare.Controls.Add(lblCodeShare);
                                        divMainOrigin1.Controls.Add(divCodeShare);
                                    }
                                    divMainOrigin.Controls.Add(divMainOrigin1);
                                }
                                else
                                {
                                    divMainOrigin.Controls.Add(divOnOrigin);
                                    divMainOrigin.Controls.Add(divOnDestination);
                                    divMainOrigin.Controls.Add(divOnwardStops);
                                    divMainOrigin.Controls.Add(divOnwardDuration);
                                    divMainOrigin.Controls.Add(divClear);
                                    if ((!resultObj.IsLCC && resultObj.Flights[i][j].OperatingCarrier != null && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0) || (resultObj.ResultBookingSource == BookingSource.Indigo && resultObj.Flights[i][j].OperatingCarrier != "6E" && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0))//Modified for Indigo CodeShare
                                    {
                                        Label lblCodeShare = new Label();
                                        lblCodeShare.Text = "Operated by : " + resultObj.Flights[i][j].OperatingCarrier;
                                        HtmlGenericControl divCodeShare = new HtmlGenericControl("Div");
                                        divCodeShare.Style.Add("text-align", "center");
                                        divCodeShare.Style.Add("font-weight", "bold");
                                        divCodeShare.Style.Add("class", "col-md-12");
                                        divCodeShare.Controls.Add(lblCodeShare);
                                        divMainOrigin.Controls.Add(divCodeShare);
                                    }
                                }




                                divOutBound.Controls.Add(divMainOrigin);
                                //divOutBound.Controls.Add(divClear1);
                                //divMain.Controls.Add(divOutBound);

                                #endregion

                                #region Binding View Flights Details
                                //flight details
                                HtmlTableRow detailsRow = new HtmlTableRow();
                                HtmlTableRow connectingRow = new HtmlTableRow();

                                HtmlTableCell logoCell = new HtmlTableCell();
                                HtmlTableCell originCell = new HtmlTableCell();
                                HtmlTableCell destCell = new HtmlTableCell();
                                HtmlTableCell duraCell = new HtmlTableCell();
                                HtmlTableCell aircraftCell = new HtmlTableCell();

                                originCell.Attributes.Add("class", "f2");
                                destCell.Attributes.Add("class", "f3");
                                duraCell.Attributes.Add("class", "f4");
                                aircraftCell.RowSpan = 1;
                                aircraftCell.Attributes.Add("class", "f5");

                                logoCell.Width = "40px";
                                logoCell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                logoCell.InnerHtml += departingAirline.AirlineName + "<br/> (" + departingAirline.AirlineCode + ")";
                                logoCell.Align = "Center";
                                logoCell.Attributes.Add("class", "f1");
                                //flight details code
                                logoCell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "<br/>";

                                originCell.Attributes.Add("class", "f2");
                                destCell.Attributes.Add("class", "f3");
                                duraCell.Attributes.Add("class", "f4");
                                aircraftCell.RowSpan = 1;
                                aircraftCell.Attributes.Add("class", "f5");

                                //lblOnOrigin.Text = resultObj.Flights[i][j].Origin.CityName;
                                originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                originCell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, HH:mm tt");
                                //lblOnDestination.Text = resultObj.Flights[i][j].Destination.CityName;
                                destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                destCell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, HH:mm tt");
                                //lblOnDepartureDate.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //lblOnArrivalDate.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //added by brahmam  18.06.2014
                                //switch (j)
                                //{
                                //    case 0:
                                //        lblOnwardStops.Text = "Non Stop";
                                //        break;
                                //    case 1:
                                //        lblOnwardStops.Text = "Single Stop";
                                //        break;
                                //    case 2:
                                //        lblOnwardStops.Text = "Two Stops";
                                //        break;
                                //}
                                //duraCell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";

                                //lblOnwardDuration.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //if Duration Hours >= 24 then TotalHours is having decimals some times 
                                //so we need non decimal value. Thats why we are doing Math.Floor to get original value
                                //ex: TotalHours = 39.1232 Math.Floor(TotalHours) = 39
                                duraCell.InnerHtml = (resultObj.Flights[i][j].Duration.Days >= 1 ? Math.Floor(resultObj.Flights[i][j].Duration.TotalHours) : resultObj.Flights[i][j].Duration.Hours) + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                if (resultObj.Flights[i][j].CabinClass != null)
                                {
                                    //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                    if ((resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp) && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                    {
                                        duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass;
                                    }
                                    else if (resultObj.Flights[i][j].CabinClass != resultObj.Flights[i][j].BookingClass)
                                    {
                                        duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                    }
                                    else
                                    {
                                        duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].BookingClass;
                                    }
                                }
                                else if (resultObj.Flights[i][j].CabinClass == null)
                                {
                                    duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].BookingClass;
                                }
                                else if (resultObj.ResultBookingSource != BookingSource.TBOAir && (resultObj.ResultBookingSource != BookingSource.Indigo && resultObj.ResultBookingSource != BookingSource.IndigoCorp))
                                {
                                    duraCell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                }
                                if (resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp)
                                {
                                    //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                    if (resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp && resultObj.Flights != null && resultObj.Flights[0][0].CabinClass.Contains("Business"))
                                    {
                                        duraCell.InnerHtml += "</br>";
                                    }
                                    else if (!duraCell.InnerHtml.Contains("Economy"))
                                    {
                                        duraCell.InnerHtml += "Economy</br>";
                                    }
                                }
                                duraCell.InnerHtml += "</br>";
                                //aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span><br/>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span><br/>" + (!string.IsNullOrEmpty(resultObj.Flights[i][j].DepTerminal) ? "Terminal: " + resultObj.Flights[i][j].DepTerminal + " - " : "") + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                if (j > 0)
                                {
                                    TimeSpan connectionTime = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                                    HtmlTableCell connectCell = new HtmlTableCell();
                                    connectCell.ColSpan = 5;
                                    connectCell.InnerHtml = "<div class='flight_amid_info'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + connectionTime.Hours + "hrs " + connectionTime.Minutes + "mins</strong> ";
                                    connectCell.InnerHtml += "<span class='cccline'>|</span> Connecting flight may departs from different Terminal</div>";
                                    connectingRow.Cells.Add(connectCell);
                                    flightDetails.Rows.Add(connectingRow);
                                }
                                detailsRow.Cells.Add(logoCell);
                                detailsRow.Cells.Add(originCell);
                                detailsRow.Cells.Add(destCell);
                                detailsRow.Cells.Add(duraCell);
                                detailsRow.Cells.Add(aircraftCell);
                                flightDetails.Rows.Add(detailsRow);

                                #endregion

                                #region Old Code using Switch Case
                                //switch (j)
                                //{
                                //    case 0://non stop
                                //        //HtmlTableCell logoCell = new HtmlTableCell();
                                //        //HtmlTableCell originCell = new HtmlTableCell();
                                //        //HtmlTableCell destCell = new HtmlTableCell();
                                //        //HtmlTableCell duraCell = new HtmlTableCell();
                                //        //HtmlTableCell aircraftCell = new HtmlTableCell();

                                //        //flight details code
                                //        logoCell.Width = "40px";
                                //        logoCell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                //        logoCell.InnerHtml += departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + "</br>";
                                //        logoCell.Align = "Center";
                                //        logoCell.Attributes.Add("class", "f1");
                                //        //flight details code
                                //        logoCell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "</br>";


                                //        //originCell.Attributes.Add("class", "f2");
                                //        //destCell.Attributes.Add("class", "f3");
                                //        //duraCell.Attributes.Add("class", "f4");
                                //        //aircraftCell.RowSpan = 1;
                                //        //aircraftCell.Attributes.Add("class", "f5");

                                //        //lblOnOrigin.Text = resultObj.Flights[i][j].Origin.CityName;
                                //        //originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                //        //originCell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblOnDestination.Text = resultObj.Flights[i][j].Destination.CityName;
                                //        //destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                //        //destCell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblOnDepartureDate.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblOnArrivalDate.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //        //added by brahmam  18.06.2014
                                //        switch (j)
                                //        {
                                //            case 0:
                                //                lblOnwardStops.Text = "Non Stop";
                                //                break;
                                //            case 1:
                                //                lblOnwardStops.Text = "Single Stop";
                                //                break;
                                //            case 2:
                                //                lblOnwardStops.Text = "Two Stops";
                                //                break;
                                //        }
                                //        //duraCell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";

                                //        lblOnwardDuration.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        duraCell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        if (resultObj.Flights[i][j].CabinClass != null)
                                //        {
                                //            duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        else
                                //        {
                                //            duraCell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                //        //flight details code
                                //        firstRow.Cells.Add(logoCell);
                                //        firstRow.Cells.Add(originCell);
                                //        firstRow.Cells.Add(destCell);
                                //        firstRow.Cells.Add(duraCell);
                                //        firstRow.Cells.Add(aircraftCell);
                                //        flightDetails.Rows.Add(firstRow);
                                //        break;
                                //    case 1://one stops
                                //        HtmlTableCell logo1Cell = new HtmlTableCell();
                                //        HtmlTableCell origin1Cell = new HtmlTableCell();
                                //        HtmlTableCell dest1Cell = new HtmlTableCell();
                                //        HtmlTableCell dura1Cell = new HtmlTableCell();
                                //        HtmlTableCell connectCell = new HtmlTableCell();
                                //        HtmlTableCell aircraft1Cell = new HtmlTableCell();

                                //        //flight details code
                                //        //logo1Cell.Width = "40px";
                                //        //logo1Cell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                //        //logo1Cell.InnerHtml += departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + "</br>";
                                //        //logo1Cell.Align = "Center";
                                //        //logo1Cell.Attributes.Add("class", "f1");
                                //        //logo1Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "</br>";

                                //        origin1Cell.Attributes.Add("class", "f2");
                                //        dest1Cell.Attributes.Add("class", "f3");
                                //        dura1Cell.Attributes.Add("class", "f4");

                                //        connectCell.ColSpan = 5;

                                //        aircraft1Cell.RowSpan = 1;
                                //        aircraft1Cell.Attributes.Add("class", "f5");

                                //        //lblOnOrigin1.Text = resultObj.Flights[i][j].Origin.CityName;
                                //        origin1Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                //        origin1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblOnDestination1.Text = resultObj.Flights[i][j].Destination.CityName;
                                //        dest1Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                //        dest1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblOnDepartureDate1.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblOnArrivalDate1.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //        switch (j)
                                //        {
                                //            case 0:
                                //                lblOnwardStops.Text = "Non Stop";
                                //                break;
                                //            case 1:
                                //                lblOnwardStops.Text = "Single Stop";
                                //                break;
                                //            case 2:
                                //                lblOnwardStops.Text = "Two Stops";
                                //                break;
                                //        }

                                //        //lblOnwardDuration1.Text = duration.Hours + "hrs " + duration.Minutes + "mins";
                                //        //dura1Cell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";

                                //        lblOnwardDuration1.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        dura1Cell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        //dura1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        if (resultObj.Flights[i][j].CabinClass != null)
                                //        {
                                //            dura1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        else
                                //        {
                                //            dura1Cell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                //        }

                                //        aircraft1Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                //        //TimeSpan connectDuration = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                                //        connectCell.InnerHtml = "<div class='flight_amid_info'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins</strong> ";
                                //        connectCell.InnerHtml += "<span class='cccline'>|</span> Connecting flight may departs from different Terminal</div>";

                                //        //flight details code
                                //        connectingRow.Cells.Add(connectCell);
                                //        flightDetails.Rows.Add(connectingRow);

                                //        secondRow.Cells.Add(logo1Cell);
                                //        secondRow.Cells.Add(origin1Cell);
                                //        secondRow.Cells.Add(dest1Cell);
                                //        secondRow.Cells.Add(dura1Cell);
                                //        flightDetails.Rows.Add(secondRow);


                                //        break;
                                //    case 2://Two stops
                                //        HtmlTableCell logo2Cell = new HtmlTableCell();
                                //        HtmlTableCell origin2Cell = new HtmlTableCell();
                                //        HtmlTableCell dest2Cell = new HtmlTableCell();
                                //        HtmlTableCell dura2Cell = new HtmlTableCell();
                                //        HtmlTableCell aircraft2Cell = new HtmlTableCell();
                                //        HtmlTableCell connect2Cell = new HtmlTableCell();
                                //        HtmlTableRow connectingRow2 = new HtmlTableRow();
                                //        origin2Cell.Attributes.Add("class", "f2");
                                //        dest2Cell.Attributes.Add("class", "f3");
                                //        dura2Cell.Attributes.Add("class", "f4");

                                //        //flight details code
                                //        logo2Cell.Width = "40px";
                                //        logo2Cell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                //        logo2Cell.InnerHtml += departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + "</br>";
                                //        logo2Cell.Align = "Center";
                                //        logo2Cell.Attributes.Add("class", "f1");
                                //        logo2Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "</br>"; 

                                //        connect2Cell.ColSpan = 5;

                                //        aircraft2Cell.RowSpan = 1;
                                //        aircraft2Cell.Attributes.Add("class", "f5");

                                //        lblOnOrigin2.Text = resultObj.Flights[i][j].Origin.CityName;
                                //        origin2Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                //        origin2Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblOnDestination2.Text = resultObj.Flights[i][j].Destination.CityName;
                                //        dest2Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                //        dest2Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblOnDepartureDate2.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblOnArrivalDate2.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //        //lblOnwardStops.Text = resultObj.Flights[i][j].Stops.ToString();

                                //        switch (j)
                                //        {
                                //            case 0:
                                //                lblOnwardStops.Text = "Non Stop";
                                //                break;
                                //            case 1:
                                //                lblOnwardStops.Text = "Single Stop";
                                //                break;
                                //            case 2:
                                //                lblOnwardStops.Text = "Two Stops";
                                //                break;
                                //        }
                                //        //dura2Cell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";

                                //        lblOnwardDuration2.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        dura2Cell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        //dura2Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass+"-" + resultObj.Flights[i][j].BookingClass;
                                //        if (resultObj.Flights[i][j].CabinClass != null)
                                //        {
                                //            dura2Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        else
                                //        {
                                //            dura2Cell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        aircraft2Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");

                                //        //TimeSpan connectDuration3 = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                                //        connect2Cell.InnerHtml = "<div class='flight_amid_info'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins</strong> ";
                                //        connect2Cell.InnerHtml += "<span class='cccline'>|</span> Connecting flight may departs from different Terminal</div>";

                                //        //flight details code
                                //        connectingRow2.Cells.Add(connect2Cell);
                                //        flightDetails.Rows.Add(connectingRow2);

                                //        thirdRow.Cells.Add(logo2Cell);
                                //        thirdRow.Cells.Add(origin2Cell);
                                //        thirdRow.Cells.Add(dest2Cell);
                                //        thirdRow.Cells.Add(dura2Cell);
                                //        flightDetails.Rows.Add(thirdRow);
                                //        break;
                                //}
                                #endregion
                            }
                            HtmlGenericControl divClear1 = new HtmlGenericControl("Div");
                            divClear1.Attributes.Add("class", "clearfix");
                            divOutBound.Controls.Add(divClear1);
                            divMain.Controls.Add(divOutBound);

                            if ((resultObj.ResultBookingSource == BookingSource.TBOAir || resultObj.ResultBookingSource == BookingSource.FlightInventory || resultObj.ResultBookingSource == BookingSource.CozmoBus || resultObj.ResultBookingSource==BookingSource.AirArabia) && !string.IsNullOrEmpty(remarks))
                            {
                                string text = "<MARQUEE><label style='color:Red;font-size:Smaller;'>" + remarks + "</label></MARQUEE>";
                                HtmlGenericControl divMarque = new HtmlGenericControl("Div");
                                divMarque.InnerHtml = text;
                                divMainOrigin.Controls.Add(divMarque);
                            }
                        }
                        else  //inbound flight details  (RETURN FLIGHTS)
                        {
                            //string rootFolder = Airline.logoDirectory + "/";
                            //DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());

                            //imgReturnLogo.ImageUrl = logoInfo.Value.ToString();
                            //lblRetCarrier.Text = logoInfo.Key.ToString();
                            HtmlTableRow seperationRow = new HtmlTableRow();
                            HtmlTableCell seperationCell = new HtmlTableCell();
                            seperationCell.ColSpan = 5;
                            seperationCell.InnerHtml = "<div class='flight_amid_info'>";
                            seperationRow.Cells.Add(seperationCell);
                            flightDetails.Rows.Add(seperationRow);

                            //HtmlGenericControl divMainLine = new HtmlGenericControl("Div");
                            //divMainLine.InnerHtml = "<hr /> ";
                            //divMain.Controls.Add(divMainLine);
                            string remarks = string.Empty;
                            HtmlGenericControl divInbound = new HtmlGenericControl("Div");   //Return Main div
                            divInbound.Attributes.Add("class", "paddingtop_10 borright paddingbot_10 returns");

                            HtmlGenericControl divMainOrigin1 = new HtmlGenericControl("Div"); //Return Origin Main div
                            divMainOrigin1.Attributes.Add("class", "col-md-10 pad0");
                            for (int j = 0; j < resultObj.Flights[i].Length; j++)
                            {
                                if (resultObj.Flights[i].Length > 0 && j > 0)
                                {
                                    totalDuration += (int)Math.Ceiling(resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime).TotalMinutes);
                                }
                                totalDuration += (int)Math.Ceiling(resultObj.Flights[i][j].Duration.TotalMinutes);
                                flyingDuration += (int)Math.Ceiling(resultObj.Flights[i][j].Duration.TotalMinutes);
                                /**************************************************************************************
                                *                          Reading Baggage details from Airline
                                * ************************************************************************************/
                                string baggage = string.Empty;
                                FlightInfo finfo = resultObj.Flights[i][j];
                                //For Indigo, Spicejet, GoAir We Will Get The Default Baggage Only in PassengerDetails Page
                                //Modified By Suresh V for Binding the default Baggage for All sources segment wise 
                                if (resultObj.BaggageIncludedInFare != null)
                                {
                                    baggage = (!string.IsNullOrEmpty(resultObj.Flights[i][j].DefaultBaggage)) ? resultObj.Flights[i][j].DefaultBaggage : "As Per Airline Policy";
                                }
                                else
                                {
                                    baggage = "As Per Airline Policy";
                                }
                                HtmlTableRow row = new HtmlTableRow();
                                HtmlTableCell cell1 = new HtmlTableCell();
                                HtmlTableCell cell2 = new HtmlTableCell();
                                cell1.InnerText = finfo.Origin.CityCode + " - " + finfo.Destination.CityCode + " " + finfo.Airline + " " + finfo.FlightNumber;
                                cell2.InnerText = baggage;
                                row.Cells.Add(cell1);
                                row.Cells.Add(cell2);
                                tblBaggage.Rows.Add(row);


                                /**************************************************************************************
                                 *                               End Baggage details 
                                 * ************************************************************************************/
                                HtmlGenericControl divMainImages1 = new HtmlGenericControl("Div");  //Return Main Image div
                                divMainImages1.Attributes.Add("class", "col-md-2 text-center text-lg-left");

                                //HtmlGenericControl divOnwardLogo = new HtmlGenericControl("Div");
                                //Image imgOnwardLogo = new Image();
                                //imgOnwardLogo.Height = new Unit(20, UnitType.Pixel);
                                //HtmlGenericControl divOnCarrier = new HtmlGenericControl("Div");
                                //Label lblOnCarrier = new Label();
                                //HtmlGenericControl divOnCarrierName = new HtmlGenericControl("Div");
                                //Label lblOnCarrierName = new Label();


                                Airline departingAirline = new Airline();
                                departingAirline.Load(resultObj.Flights[i][j].Airline);
                                HtmlGenericControl divRetdLogo = new HtmlGenericControl("Div");
                                Image imgReturnLogo = new Image();
                                imgReturnLogo.Height = new Unit(20, UnitType.Pixel);
                                HtmlGenericControl divRetCarrier = new HtmlGenericControl("Div");
                                Label lblRetCarrier = new Label();


                                HtmlGenericControl divRefundImage1 = new HtmlGenericControl("Div");
                                divRefundImage1.Attributes.Add("class", "iconpad");
                                Image aircraftImage = new Image();
                                aircraftImage.ImageUrl = "images/aircraft_icon.png";
                                aircraftImage.ToolTip = (resultObj.ResultBookingSource == BookingSource.AirArabia ? "Airbus A320" : resultObj.Flights[i][j].Craft);
                                Image refundableImage = new Image();
                                refundableImage.ImageUrl = "images/refundable_icon.png";
                                refundableImage.ToolTip = "Refundable Fare";
                                Image nonRefundableImage = new Image();
                                nonRefundableImage.ImageUrl = "images/nonrefundable_icon.png";
                                nonRefundableImage.ToolTip = "Non Refundable Fare";

                                divRefundImage1.Controls.Add(aircraftImage);
                                Label lbl = new Label();
                                lbl.Text = "&nbsp;";
                                divRefundImage1.Controls.Add(lbl);//Create gap between airline logo and refund image
                                if (!resultObj.NonRefundable)
                                {
                                    divRefundImage1.Controls.Add(refundableImage);
                                }
                                else
                                {
                                    divRefundImage1.Controls.Add(nonRefundableImage);
                                }

                                //HtmlGenericControl divRetCarrierName = new HtmlGenericControl("Div");
                                //Label lblRetCarrierName = new Label();
                                //if (j == 0) //Commented by brahmam Should be show all images
                                {
                                    imgReturnLogo.ImageUrl = rootFolder + departingAirline.LogoFile;
                                    lblRetCarrier.Text = departingAirline.AirlineName + "<br/> (" + departingAirline.AirlineCode + ")" + "" + resultObj.Flights[i][j].FlightNumber;
                                    //added by brahmam
                                    //lblRetCarrierName.Text = resultObj.Flights[i][j].FlightNumber;
                                    if (resultObj.ResultBookingSource == BookingSource.TBOAir || resultObj.ResultBookingSource == BookingSource.FlightInventory || resultObj.ResultBookingSource == BookingSource.CozmoBus)
                                    {
                                        remarks = !string.IsNullOrEmpty(resultObj.Flights[i][j].SegmentFareType) ? resultObj.Flights[i][j].SegmentFareType : string.Empty;
                                    }
                                    else if (resultObj.ResultBookingSource == BookingSource.AirArabia)
                                        remarks = !string.IsNullOrEmpty(resultObj.Flights[i][j].UapiSegmentRefKey) ? resultObj.Flights[i][j].UapiSegmentRefKey : string.Empty;
                                }
                                if (j == 0)
                                {
                                    divRetdLogo.Controls.Add(imgReturnLogo);
                                    divRetCarrier.Controls.Add(lblRetCarrier);
                                    //divRetCarrierName.Controls.Add(lblRetCarrierName);
                                    divMainImages1.Controls.Add(divRetdLogo);
                                    divMainImages1.Controls.Add(divRetCarrier);
                                    divMainImages1.Controls.Add(divRefundImage1);
                                    divInbound.Controls.Add(divMainImages1);
                                }
                                //divMainImages1.Controls.Add(divRetCarrierName);
                                #region Binding Origin, Destination, Departure Date and Arrival Date


                                HtmlGenericControl divMainOrigin2 = new HtmlGenericControl("Div"); //Return Origin Main div
                                if (j == 0 && resultObj.Flights[i].Length > 1)
                                {
                                    divMainOrigin2.Attributes.Add("class", "divider");
                                }

                                HtmlGenericControl divRetOrigin = new HtmlGenericControl("Div");
                                divRetOrigin.Attributes.Add("class", "col-xs-6 col-md-3");
                                Label lblRetOrigin = new Label();
                                lblRetOrigin.ID = "lblRetOrigin" + i + j.ToString();
                                //lblRetOrigin.Font.Bold = true;
                                lblRetOrigin.Text = "<strong>" + resultObj.Flights[i][j].Origin.CityName + "</strong></br>" + "" + resultObj.Flights[i][j].DepartureTime.ToString("dd-MMM,HH:mm tt") + "</br>";
                                if (resultObj.ResultBookingSource != BookingSource.FlightInventory && resultObj.ResultBookingSource != BookingSource.CozmoBus)
                                {
                                    if (!string.IsNullOrEmpty(resultObj.Flights[i][j].ArrTerminal))
                                    {
                                        if (resultObj.Flights[i][j].ArrTerminal.ToLower().Contains("terminal"))
                                        {
                                            lblRetOrigin.Text += resultObj.Flights[i][j].DepTerminal;
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(resultObj.Flights[i][j].DepTerminal)) lblRetOrigin.Text += "Terminal " + resultObj.Flights[i][j].DepTerminal;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(resultObj.Flights[i][j].DepTerminal)) lblRetOrigin.Text += "Terminal " + resultObj.Flights[i][j].DepTerminal;
                                }
                                divRetOrigin.Controls.Add(lblRetOrigin);

                                //HtmlGenericControl divRetDepartureDate = new HtmlGenericControl("Div");
                                //Label lblRetDepartureDate = new Label();
                                //lblRetDepartureDate.ID = "lblRetDepartureDate" + i + j.ToString();
                                //lblRetDepartureDate.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //divRetDepartureDate.Controls.Add(lblRetDepartureDate);



                                //divMainOrigin1.Controls.Add(divRetDepartureDate);

                                //HtmlGenericControl divMainDestination1 = new HtmlGenericControl("Div");  //Return Destination div
                                //divMainDestination1.Attributes.Add("class", "col-md-3 col-xs-6 col_xsfare col-xs-push-6 col-md-push-0");

                                HtmlGenericControl divRetDestination = new HtmlGenericControl("Div");
                                divRetDestination.Attributes.Add("class", "col-xs-6 col-md-3 text-right text-lg-left");
                                Label lblRetDestination = new Label();
                                lblRetDestination.ID = "lblRetDestination" + i + j.ToString();
                                //lblRetDestination.Font.Bold = true;
                                lblRetDestination.Text = "<strong>" + resultObj.Flights[i][j].Destination.CityName + "</strong></br>" + "" + resultObj.Flights[i][j].ArrivalTime.ToString("dd-MMM,HH:mm tt") + "</br>";
                                if (!string.IsNullOrEmpty(resultObj.Flights[i][j].ArrTerminal))
                                {
                                    if (resultObj.Flights[i][j].ArrTerminal.ToLower().Contains("terminal"))
                                    {
                                        lblRetDestination.Text += resultObj.Flights[i][j].ArrTerminal;
                                    }
                                    else
                                    {
                                        lblRetDestination.Text += "Terminal " + resultObj.Flights[i][j].ArrTerminal;
                                    }
                                }
                                divRetDestination.Controls.Add(lblRetDestination);

                                //HtmlGenericControl divRetArrivalDate = new HtmlGenericControl("Div");
                                //Label lblRetArrivalDate = new Label();
                                //lblRetArrivalDate.ID = "lblRetArrivalDate" + i + j.ToString();
                                //lblRetArrivalDate.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //divRetArrivalDate.Controls.Add(lblRetArrivalDate);



                                #endregion

                                #region Binding Stops Information
                                //HtmlGenericControl divMainStops1 = new HtmlGenericControl("Div");   //return stops main div
                                //divMainStops1.Attributes.Add("class", "col-md-3 col-xs-6 col_xsfare col-md-push-6");
                                HtmlGenericControl divReturnStops = new HtmlGenericControl("Div");
                                divReturnStops.Attributes.Add("class", "col-md-3 text-center text-lg-left");
                                //if (j == 0)
                                {
                                    //If VIA flight is there then update stops as ZERO. As we are using stops to show stops  
                                    //information further pages we need to correct this logic
                                    if ((resultObj.Flights[i].Select(f => f.FlightNumber).Distinct().Count() - 1) == 0)
                                        resultObj.Flights[i].ToList().ForEach(f => f.Stops = 0);
                                    else//Need to re assign the stops based on the stops we are displaying in the page. If any VIA flight is there need to reduce stops count
                                        resultObj.Flights[i].ToList().ForEach(f => f.Stops = (resultObj.Flights[i].Select(s => s.FlightNumber).Distinct().Count() - 1));


                                    Label lblReturnStops = new Label();
                                    //lblReturnStops.Width = new Unit(100, UnitType.Pixel);
                                    lblReturnStops.ID = "lblReturnStops" + i + j.ToString();
                                    
                                        switch (resultObj.Flights[i].Select(p => p.FlightNumber).Distinct().Count() - 1)
                                        {
                                            case 0:
                                                lblReturnStops.Text = "Non Stop";
                                                break;
                                            case 1:
                                                lblReturnStops.Text = "Single Stop";
                                                break;
                                            case 2:
                                                lblReturnStops.Text = "Two Stops";
                                                break;
                                           default:
                                            lblReturnStops.Text = "Two+ Stops";
                                            break;
                                        }

                                    Label lblReturnClass = new Label();
                                    lblReturnClass.ID = "lblReturnClass" + i + j.ToString();
                                    lblReturnClass.Font.Bold = true;

                                    if (resultObj.ResultBookingSource != BookingSource.Jazeera)
                                    {
                                        if (j == 0 && resultObj.Flights[i][j].CabinClass != null && resultObj.ResultBookingSource != BookingSource.Indigo && resultObj.ResultBookingSource != BookingSource.IndigoCorp)// indiago Classs alwys Economy
                                        {
                                            //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                            if ((resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp) && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                            {
                                                lblReturnClass.Text = resultObj.Flights[i][j].CabinClass + "</br>";
                                            }
                                            else if (resultObj.Flights[i][j].CabinClass != resultObj.Flights[i][j].BookingClass)
                                            {
                                                lblReturnClass.Text = resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                            else
                                            {
                                                lblReturnClass.Text = resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                        }
                                        else if (j == 0 && resultObj.Flights[i][j].CabinClass == null)
                                        {
                                            lblReturnClass.Text = resultObj.Flights[i][j].BookingClass + "</br>";
                                        }

                                        else if (resultObj.ResultBookingSource != BookingSource.TBOAir && resultObj.ResultBookingSource != BookingSource.UAPI && resultObj.ResultBookingSource != BookingSource.Indigo && resultObj.ResultBookingSource != BookingSource.PKFares && resultObj.ResultBookingSource != BookingSource.IndigoCorp)
                                        {
                                            lblReturnClass.Text = "Economy-" + resultObj.Flights[i][j].BookingClass + "</br>";
                                        }
                                        else if (j > 0)
                                        {
                                            if (resultObj.Flights[i][j].CabinClass != resultObj.Flights[i][j].BookingClass)//Avoid display C-C
                                            {
                                                lblReturnClass.Text = resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                            else
                                            {
                                                lblReturnClass.Text = resultObj.Flights[i][j].BookingClass + "</br>";
                                            }
                                        }
                                    }
                                    if (resultObj.ResultBookingSource == BookingSource.FlyDubai || resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.TBOAir || resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp || resultObj.ResultBookingSource == BookingSource.GoAirCorp|| resultObj.ResultBookingSource == BookingSource.Jazeera || resultObj.ResultBookingSource == BookingSource.SalamAir || resultObj.ResultBookingSource==BookingSource.AirArabia)
                                    {
                                        if (resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp)
                                        {
                                            //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                            if ((resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp) && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                            {
                                                lblReturnClass.Text += "</br>";
                                            }
                                            else if (!lblReturnClass.Text.Contains("Economy"))
                                            {
                                                lblReturnClass.Text = "Economy</br>";
                                            }
                                        }
                                        if (resultObj.FareType != null)
                                        {
                                            if (request.Type != SearchType.OneWay && resultObj.ResultBookingSource != BookingSource.TBOAir)
                                            {
                                                //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                                if ((resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp) && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                                {
                                                    lblReturnClass.Text = Environment.NewLine + "<b style='color:red;'>" + resultObj.Flights[i][j].SegmentFareType + "</b>";
                                                }
                                                else if ((resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp|| resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp || resultObj.ResultBookingSource == BookingSource.Jazeera || resultObj.ResultBookingSource == BookingSource.SalamAir) && !string.IsNullOrEmpty(resultObj.Flights[i][j].SegmentFareType))
                                                {
                                                    lblReturnClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.Flights[i][j].SegmentFareType + "</b>";
                                                }
                                                else
                                                {
                                                    lblReturnClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.FareType.Split(',')[1] + "</b>";
                                                }
                                            }
                                            else
                                            {
                                                if ((resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp || resultObj.ResultBookingSource == BookingSource.Jazeera || resultObj.ResultBookingSource==BookingSource.SalamAir) && !string.IsNullOrEmpty(resultObj.Flights[i][j].SegmentFareType))
                                                {
                                                    lblReturnClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.Flights[i][j].SegmentFareType + "</b>";
                                                }
                                                else
                                                {
                                                    lblReturnClass.Text += Environment.NewLine + "<b style='color:red;'>" + resultObj.FareType + "</b>";
                                                }
                                            }
                                        }
                                    }
                                    lblReturnStops.Text = lblReturnStops.Text + "</br>" + lblReturnClass.Text;
                                    divReturnStops.Controls.Add(lblReturnStops);

                                    //divMainStops1.Controls.Add(divReturnStops);
                                    //divMainStops1.Controls.Add(divReturnClass);
                                }
                                #endregion

                                #region Binding Duration Information

                                HtmlGenericControl divReturnDuration = new HtmlGenericControl("Div"); //Return Duration Main div
                                divReturnDuration.Attributes.Add("class", "col-md-3 text-center text-lg-left");
                                Label lblReturnDuration = new Label();
                                lblReturnDuration.ID = "lblReturnDuration" + i + j.ToString();
                                //lblReturnDuration.Font.Bold = true;
                                //if Duration Hours >= 24 then TotalHours is having decimals some times 
                                //so we need non decimal value. Thats why we are doing Math.Floor to get original value
                                //ex: TotalHours = 39.1232 Math.Floor(TotalHours) = 39
                                lblReturnDuration.Text = (resultObj.Flights[i][j].Duration.Days >= 1 ? Math.Floor(resultObj.Flights[i][j].Duration.TotalHours) : resultObj.Flights[i][j].Duration.Hours) + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                lblReturnDuration.Font.Bold = true;
                                divReturnDuration.Controls.Add(lblReturnDuration);

                                //divMainStops1.Controls.Add(divReturnDuration);


                                HtmlGenericControl divClear1 = new HtmlGenericControl("Div");
                                divClear1.Attributes.Add("class", "clearfix");

                                //if (j != 0)
                                //{
                                //    divInbound.Controls.Add(divLine);
                                //}

                                divMainOrigin2.Controls.Add(divRetOrigin);
                                divMainOrigin2.Controls.Add(divRetDestination);
                                divMainOrigin2.Controls.Add(divReturnStops);
                                divMainOrigin2.Controls.Add(divReturnDuration);
                                divMainOrigin2.Controls.Add(divClear1);
                                if (j == 0 && resultObj.Flights[i].Length > 1)
                                {
                                    if ((!resultObj.IsLCC && resultObj.Flights[i][j].OperatingCarrier != null && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0) || (resultObj.ResultBookingSource == BookingSource.Indigo && resultObj.Flights[i][j].OperatingCarrier != "6E" && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0))//Modified for Indigo CodeShare
                                    {
                                        Label lblCodeShare = new Label();
                                        lblCodeShare.Text = "Operated by : " + resultObj.Flights[i][j].OperatingCarrier;
                                        HtmlGenericControl divCodeShare = new HtmlGenericControl("Div");
                                        divCodeShare.Style.Add("text-align", "center");
                                        divCodeShare.Style.Add("font-weight", "bold");
                                        divCodeShare.Style.Add("class", "col-md-12");
                                        divCodeShare.Controls.Add(lblCodeShare);
                                        divMainOrigin2.Controls.Add(divCodeShare);
                                    }
                                    divMainOrigin1.Controls.Add(divMainOrigin2);
                                }
                                else
                                {
                                    divMainOrigin1.Controls.Add(divRetOrigin);
                                    divMainOrigin1.Controls.Add(divRetDestination);
                                    divMainOrigin1.Controls.Add(divReturnStops);
                                    divMainOrigin1.Controls.Add(divReturnDuration);
                                    divMainOrigin1.Controls.Add(divClear1);
                                    if ((!resultObj.IsLCC && resultObj.Flights[i][j].OperatingCarrier != null && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0) || (resultObj.ResultBookingSource == BookingSource.Indigo && resultObj.Flights[i][j].OperatingCarrier != "6E" && resultObj.Flights[i][j].OperatingCarrier.Trim().Length > 0))//Modified for Indigo CodeShare
                                    {
                                        Label lblCodeShare = new Label();
                                        lblCodeShare.Text = "Operated by : " + resultObj.Flights[i][j].OperatingCarrier;
                                        HtmlGenericControl divCodeShare = new HtmlGenericControl("Div");
                                        divCodeShare.Style.Add("text-align", "center");
                                        divCodeShare.Style.Add("font-weight", "bold");
                                        divCodeShare.Style.Add("class", "col-md-12");
                                        divCodeShare.Controls.Add(lblCodeShare);
                                        divMainOrigin1.Controls.Add(divCodeShare);
                                    }
                                }


                                //divInbound.Controls.Add(divMainStops1);
                                //divInbound.Controls.Add(divMainDestination1);



                                #endregion

                                #region Binding View Flights Details
                                //flight details
                                HtmlTableRow detailsRow = new HtmlTableRow();
                                HtmlTableRow connectingRow = new HtmlTableRow();

                                HtmlTableCell logoCell = new HtmlTableCell();
                                HtmlTableCell originCell = new HtmlTableCell();
                                HtmlTableCell destCell = new HtmlTableCell();
                                HtmlTableCell duraCell = new HtmlTableCell();
                                HtmlTableCell aircraftCell = new HtmlTableCell();

                                originCell.Attributes.Add("class", "f2");
                                destCell.Attributes.Add("class", "f3");
                                duraCell.Attributes.Add("class", "f4");
                                aircraftCell.RowSpan = 1;
                                aircraftCell.Attributes.Add("class", "f5");

                                logoCell.Width = "40px";
                                logoCell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                logoCell.InnerHtml += departingAirline.AirlineName + "<br/> (" + departingAirline.AirlineCode + ")";
                                logoCell.Align = "Center";
                                logoCell.Attributes.Add("class", "f1");
                                //flight details code
                                logoCell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "<br/>";

                                originCell.Attributes.Add("class", "f2");
                                destCell.Attributes.Add("class", "f3");
                                duraCell.Attributes.Add("class", "f4");
                                aircraftCell.RowSpan = 1;
                                aircraftCell.Attributes.Add("class", "f5");

                                //lblOnOrigin.Text = resultObj.Flights[i][j].Origin.CityName;
                                originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                originCell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, HH:mm tt");
                                //lblOnDestination.Text = resultObj.Flights[i][j].Destination.CityName;
                                destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                destCell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, HH:mm tt");
                                //lblOnDepartureDate.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //lblOnArrivalDate.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //added by brahmam  18.06.2014
                                //switch (j)
                                //{
                                //    case 0:
                                //        lblOnwardStops.Text = "Non Stop";
                                //        break;
                                //    case 1:
                                //        lblOnwardStops.Text = "Single Stop";
                                //        break;
                                //    case 2:
                                //        lblOnwardStops.Text = "Two Stops";
                                //        break;
                                //}
                                //duraCell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";

                                //lblOnwardDuration.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //if Duration Hours >= 24 then TotalHours is having decimals some times 
                                //so we need non decimal value. Thats why we are doing Math.Floor to get original value
                                //ex: TotalHours = 39.1232 Math.Floor(TotalHours) = 39
                                duraCell.InnerHtml = (resultObj.Flights[i][j].Duration.Days >= 1 ? Math.Floor(resultObj.Flights[i][j].Duration.TotalHours) : resultObj.Flights[i][j].Duration.Hours) + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                if (resultObj.Flights[i][j].CabinClass != null)
                                {
                                    //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                    if ((resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp) && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                    {
                                        duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass;
                                    }
                                    else if (resultObj.Flights[i][j].CabinClass != resultObj.Flights[i][j].BookingClass)
                                    {
                                        duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                    }
                                    else
                                    {
                                        duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].BookingClass;
                                    }
                                }
                                else if (resultObj.Flights[i][j].CabinClass == null)
                                {
                                    duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].BookingClass;
                                }
                                else if (resultObj.ResultBookingSource != BookingSource.TBOAir && resultObj.ResultBookingSource != BookingSource.Indigo && resultObj.ResultBookingSource != BookingSource.IndigoCorp)
                                {
                                    duraCell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                }
                                if (resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.Indigo || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp || resultObj.ResultBookingSource == BookingSource.IndigoCorp || resultObj.ResultBookingSource == BookingSource.GoAir || resultObj.ResultBookingSource == BookingSource.GoAirCorp)
                                {
                                    //Displaying cabin class as Business in View Flight Details Tab for Spice Jet Business
                                    if ((resultObj.ResultBookingSource == BookingSource.SpiceJet || resultObj.ResultBookingSource == BookingSource.SpiceJetCorp) && resultObj.Flights != null && resultObj.Flights[i][j].CabinClass.Contains("Business"))
                                    {
                                        duraCell.InnerHtml += "</br>";
                                    }
                                    else if (!duraCell.InnerHtml.Contains("Economy"))
                                    {
                                        duraCell.InnerHtml += "Economy</br>";
                                    }
                                }

                                aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span><br/>" + (!string.IsNullOrEmpty(resultObj.Flights[i][j].ArrTerminal) ? "Terminal: " + resultObj.Flights[i][j].ArrTerminal + " - " : "") + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                if (j > 0)
                                {
                                    TimeSpan connectionTime = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                                    HtmlTableCell connectCell = new HtmlTableCell();
                                    connectCell.ColSpan = 5;
                                    connectCell.InnerHtml = "<div class='flight_amid_info'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + connectionTime.Hours + "hrs " + connectionTime.Minutes + "mins</strong> ";
                                    connectCell.InnerHtml += "<span class='cccline'>|</span> Connecting flight may departs from different Terminal</div>";
                                    connectingRow.Cells.Add(connectCell);
                                    flightDetails.Rows.Add(connectingRow);
                                }
                                detailsRow.Cells.Add(logoCell);
                                detailsRow.Cells.Add(originCell);
                                detailsRow.Cells.Add(destCell);
                                detailsRow.Cells.Add(duraCell);
                                detailsRow.Cells.Add(aircraftCell);
                                flightDetails.Rows.Add(detailsRow);

                                #endregion

                                #region old code handling returns flights binding
                                //switch (j)
                                //{
                                //    case 0://Non Stop
                                //        //flight details code
                                //        HtmlTableCell logo1Cell = new HtmlTableCell();
                                //        logo1Cell.Width = "40px";
                                //        logo1Cell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                //        logo1Cell.InnerHtml += departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + "</br>";
                                //        logo1Cell.Align = "Center";
                                //        logo1Cell.Attributes.Add("class", "f1");
                                //        logo1Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber +"</br>";
                                //        //logo1Cell.InnerHtml += logoInfo.Key.ToString() + " " + resultObj.Flights[i][j].FlightNumber + "</br>";

                                //        HtmlTableCell originCell = new HtmlTableCell();
                                //        HtmlTableCell destCell = new HtmlTableCell();
                                //        HtmlTableCell duraCell = new HtmlTableCell();
                                //        HtmlTableCell aircraftCell = new HtmlTableCell();

                                //        originCell.Attributes.Add("class", "f2");
                                //        destCell.Attributes.Add("class", "f3");
                                //        duraCell.Attributes.Add("class", "f4");
                                //        aircraftCell.RowSpan = 1;
                                //        aircraftCell.Attributes.Add("class", "f5");

                                //        //lblRetOrigin.Text += resultObj.Flights[i][j].Origin.CityName;
                                //        originCell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                //        originCell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblRetDestination.Text += resultObj.Flights[i][j].Destination.CityName;
                                //        destCell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                //        destCell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblRetDepartureDate.Text += resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblRetArrivalDate.Text += resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblReturnStops.Text = resultObj.Flights[i][j].Stops.ToString();
                                //        //lblReturnStops.Text = j.ToString();
                                //        //added by brahmam  18.06.2014
                                //        //switch (j)
                                //        //{
                                //        //    case 0:
                                //        //        lblReturnStops.Text = "Non Stop";
                                //        //        break;
                                //        //    case 1:
                                //        //        lblReturnStops.Text = "Single Stop";
                                //        //        break;
                                //        //    case 2:
                                //        //        lblReturnStops.Text = "Two Stops";
                                //        //        break;
                                //        //}
                                //        //lblReturnDuration.Text = duration.Hours + "hrs " + duration.Minutes + "mins";
                                //        //duraCell.InnerHtml = duration.Hours + "hrs " + duration.Minutes + "mins";

                                //        //lblReturnDuration.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        duraCell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        if (resultObj.Flights[i][j].CabinClass != null)
                                //        {
                                //            duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        else
                                //        {
                                //            duraCell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        //duraCell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass+ "-" + resultObj.Flights[i][j].BookingClass;
                                //        aircraftCell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");
                                //        //flight details code
                                //        fourthRow.Cells.Add(logo1Cell);
                                //        fourthRow.Cells.Add(originCell);
                                //        fourthRow.Cells.Add(destCell);
                                //        fourthRow.Cells.Add(duraCell);
                                //        fourthRow.Cells.Add(aircraftCell);
                                //        flightDetails.Rows.Add(fourthRow);
                                //        break;
                                //    case 1://Single Stop
                                //        HtmlTableCell logo2Cell = new HtmlTableCell();
                                //        logo2Cell.Width = "40px";
                                //        logo2Cell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                //        logo2Cell.InnerHtml += departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + "</br>";
                                //        logo2Cell.Align = "Center";
                                //        logo2Cell.Attributes.Add("class", "f1");
                                //        logo2Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "</br>";

                                //        HtmlTableCell origin1Cell = new HtmlTableCell();
                                //        HtmlTableCell dest1Cell = new HtmlTableCell();
                                //        HtmlTableCell dura1Cell = new HtmlTableCell();
                                //        HtmlTableCell aircraft1Cell = new HtmlTableCell();
                                //        HtmlTableCell connectCell = new HtmlTableCell();
                                //        connectCell.ColSpan = 5;

                                //        origin1Cell.Attributes.Add("class", "f2");
                                //        dest1Cell.Attributes.Add("class", "f3");
                                //        dura1Cell.Attributes.Add("class", "f4");
                                //        aircraft1Cell.RowSpan = 1;
                                //        aircraft1Cell.Attributes.Add("class", "f5");

                                //        lblRetOrigin1.Text = resultObj.Flights[i][j].Origin.CityName;
                                //        origin1Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                //        origin1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblRetDestination1.Text = resultObj.Flights[i][j].Destination.CityName;
                                //        dest1Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                //        dest1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblRetDepartureDate1.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblRetArrivalDate1.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        //lblReturnStops.Text = resultObj.Flights[i][j].Stops.ToString();
                                //        //lblReturnStops.Text = j.ToString();
                                //        switch (j)
                                //        {
                                //            case 0:
                                //                lblReturnStops.Text = "Non Stop";
                                //                break;
                                //            case 1:
                                //                lblReturnStops.Text = "Single Stop";
                                //                break;
                                //            case 2:
                                //                lblReturnStops.Text = "Two Stops";
                                //                break;
                                //        }

                                //        //TimeSpan duration1 = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //        //lblReturnDuration1.Text = duration1.Hours + "hrs " + duration1.Minutes + "mins";
                                //        //dura1Cell.InnerHtml = duration1.Hours + "hrs " + duration1.Minutes + "mins";
                                //        lblReturnDuration1.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        dura1Cell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        if (resultObj.Flights[i][j].CabinClass != null)
                                //        {
                                //            dura1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        else
                                //        {
                                //            dura1Cell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        //dura1Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        aircraft1Cell.InnerHtml = "Aircraft: " + resultObj.Flights[i][j].OperatingCarrier + " " + resultObj.Flights[i][j].Craft + "<span class='cccline'>|</span>Terminal: " + resultObj.Flights[i][j].DepTerminal + "<span class='cccline'>|</span>" + (resultObj.NonRefundable ? "Non Refundable" : "Refundable");

                                //        //TimeSpan connectDuration = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                                //        connectCell.InnerHtml = "<div class='flight_amid_info'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins</strong> ";
                                //        connectCell.InnerHtml += "<span class='cccline'>|</span> Connecting flight may departs from different Terminal</div>";

                                //        //flight details code
                                //        connectingRow.Cells.Add(connectCell);
                                //        flightDetails.Rows.Add(connectingRow);
                                //        //flight details code
                                //        fifthRow.Cells.Add(logo2Cell);
                                //        fifthRow.Cells.Add(origin1Cell);
                                //        fifthRow.Cells.Add(dest1Cell);
                                //        fifthRow.Cells.Add(dura1Cell);
                                //        flightDetails.Rows.Add(fifthRow);
                                //        break;
                                //    case 2://Two Stops
                                //    case 3://Three Stops
                                //    case 4://four Stops
                                //    case 5://Five Stops
                                //        HtmlTableCell logo3Cell = new HtmlTableCell();
                                //        logo3Cell.Width = "40px";
                                //        logo3Cell.InnerHtml = "<span class='p20'><img src='" + rootFolder + departingAirline.LogoFile + "' width='50px' height='35px'/></span></br>";
                                //        logo3Cell.InnerHtml += departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + "</br>";
                                //        logo3Cell.Align = "Center";
                                //        logo3Cell.Attributes.Add("class", "f1");
                                //        logo3Cell.InnerHtml += resultObj.Flights[i][j].FlightNumber + "</br>";

                                //        HtmlTableCell origin2Cell = new HtmlTableCell();
                                //        HtmlTableCell dest2Cell = new HtmlTableCell();
                                //        HtmlTableCell dura2Cell = new HtmlTableCell();
                                //        HtmlTableCell connect3Cell = new HtmlTableCell();

                                //        HtmlTableRow connectingRow3 = new HtmlTableRow();
                                //        connect3Cell.ColSpan = 5;

                                //        origin2Cell.Attributes.Add("class", "f2");
                                //        dest2Cell.Attributes.Add("class", "f3");
                                //        dura2Cell.Attributes.Add("class", "f4");

                                //        lblRetOrigin2.Text = resultObj.Flights[i][j].Origin.CityName;                                        
                                //        lblRetDestination2.Text = resultObj.Flights[i][j].Destination.CityName;                                        
                                //        lblRetDepartureDate2.Text = resultObj.Flights[i][j].DepartureTime.ToString("ddd, dd MMM, hh:mm tt");
                                //        lblRetArrivalDate2.Text = resultObj.Flights[i][j].ArrivalTime.ToString("ddd, dd MMM, hh:mm tt");
                                //         //added by brahmam  18.06.2014
                                //        switch (j)
                                //        {
                                //            case 0:
                                //                lblReturnStops.Text = "Non Stop";
                                //                break;
                                //            case 1:
                                //                lblReturnStops.Text = "Single Stop";
                                //                break;
                                //            case 2:
                                //                lblReturnStops.Text = "Two Stops";
                                //                break;
                                //        }
                                //        //TimeSpan duration2 = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                //        //lblReturnDuration2.Text = duration2.Hours + "hrs " + duration2.Minutes + "mins";

                                //        origin2Cell.InnerHtml = resultObj.Flights[i][j].Origin.CityName;
                                //        dest2Cell.InnerHtml = resultObj.Flights[i][j].Destination.CityName;
                                //        //dura2Cell.InnerHtml = duration2.Hours + "hrs " + duration2.Minutes + "mins";
                                //        lblReturnDuration2.Text = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        dura2Cell.InnerHtml = resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins";
                                //        //added by brahmam  18.06.2014
                                //        if (resultObj.Flights[i][j].CabinClass != null)
                                //        {
                                //            dura2Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        else
                                //        {
                                //            dura2Cell.InnerHtml += "</br>" + "Economy-" + resultObj.Flights[i][j].BookingClass;
                                //        }
                                //        //dura2Cell.InnerHtml += "</br>" + resultObj.Flights[i][j].CabinClass + "-" + resultObj.Flights[i][j].BookingClass;
                                //        //TimeSpan connectDuration3 = resultObj.Flights[i][j].DepartureTime.Subtract(resultObj.Flights[i][j - 1].ArrivalTime);
                                //        connect3Cell.InnerHtml = "<div class='flight_amid_info'>Change planes at " + resultObj.Flights[i][j].Origin.CityName + ". Time between flights: <strong>" + resultObj.Flights[i][j].Duration.Hours + "hrs " + resultObj.Flights[i][j].Duration.Minutes + "mins</strong> ";
                                //        connect3Cell.InnerHtml += "<span class='cccline'>|</span> Connecting flight may departs from different Terminal</div>";

                                //        //flight details code
                                //        connectingRow3.Cells.Add(connect3Cell);
                                //        flightDetails.Rows.Add(connectingRow3);

                                //        sixthRow.Cells.Add(logo3Cell);
                                //        sixthRow.Cells.Add(origin2Cell);
                                //        sixthRow.Cells.Add(dest2Cell);
                                //        sixthRow.Cells.Add(dura2Cell);
                                //        flightDetails.Rows.Add(sixthRow);
                                //        break;
                                //}
                                #endregion

                            }
                            HtmlGenericControl divClear2 = new HtmlGenericControl("Div");
                            divClear2.Attributes.Add("class", "clearfix");
                            if ((resultObj.ResultBookingSource == BookingSource.TBOAir || resultObj.ResultBookingSource == BookingSource.FlightInventory || resultObj.ResultBookingSource == BookingSource.CozmoBus || resultObj.ResultBookingSource==BookingSource.AirArabia) && !string.IsNullOrEmpty(remarks))
                            {
                                string text = "<MARQUEE><label style='color:Red;font-size:Smaller;'>" + remarks + "</label></MARQUEE>";
                                HtmlGenericControl divMarque = new HtmlGenericControl("Div");
                                divMarque.InnerHtml = text;
                                divMainOrigin1.Controls.Add(divMarque);
                            }
                            divInbound.Controls.Add(divMainOrigin1);
                            divInbound.Controls.Add(divClear2);
                            divMain.Controls.Add(divInbound);

                        }
                    }
                    TimeSpan TotalDuration = new TimeSpan(0, totalDuration, 0);
                    TimeSpan FlyingDuration = new TimeSpan(0, flyingDuration, 0);
                    string totDuration = TotalDuration.ToString();
                    string flyDuration = FlyingDuration.ToString();
                    //if Duration Hours >= 24 then TotalHours is having decimals some times 
                    //so we need non decimal value. Thats why we are doing Math.Floor to get original value
                    //ex: TotalHours = 39.1232 Math.Floor(TotalHours) = 39
                    if (TotalDuration.Days >= 1)
                    {
                        totDuration = Math.Floor(TotalDuration.TotalHours) + ":" + (TotalDuration.Minutes.ToString().Length == 1 ? "0" + TotalDuration.Minutes : TotalDuration.Minutes.ToString()) + ":" + (TotalDuration.Seconds.ToString().Length == 1 ? "0" + TotalDuration.Seconds : TotalDuration.Seconds.ToString()) ;
                    }
                    if (FlyingDuration.Days >= 1)
                    {
                        flyDuration = Math.Floor(FlyingDuration.TotalHours) + ":" + (FlyingDuration.Minutes.ToString().Length == 1 ? "0" + FlyingDuration.Minutes : FlyingDuration.Minutes.ToString()) + ":" + (FlyingDuration.Seconds.ToString().Length == 1 ? "0" + FlyingDuration.Seconds : FlyingDuration.Seconds.ToString()) ;
                    }
                    if (request.Type != SearchType.MultiWay)
                    {
                        lblTotalDuration.Text = "<td width='20%'>Total Journey : " + totDuration + "</td><td width='20%'>Flying Duration : " + flyDuration + "</td>";
                        // lblTotalDuration.Text = "<table width='100%'><tr><td>Total Journey : " + totDuration + "< Flying Duration : " + flyDuration + "</tr></table>";
                    }
                    else
                    {
                        lblTotalDuration.Text = "<td width='30%'>Flying Duration : " + flyDuration + "</td>";
                    }
                }
            }
            else
            {
                e.Item.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void dlFlightResults_ItemCommand(object sender, DataListCommandEventArgs e)
    //{
    //    if (e.CommandName == "Book")
    //    {
    //        SearchResult resultObj = e.Item.DataItem as SearchResult;

    //        Response.Redirect("PassengerDetails.aspx?id=" + resultObj.ResultId);
    //    }
    //}
    protected void lnkPrevDayOB_Click(object sender, EventArgs e)
    {

    }
    protected void lnkNextDayOB_Click(object sender, EventArgs e)
    {

    }
    protected void lnkPrevDayIB_Click(object sender, EventArgs e)
    {

    }
    protected void lnkNextDayIB_Click(object sender, EventArgs e)
    {

    }
    protected void btnSearchFlight_Click(object sender, EventArgs e)
    {
        airlineMessage = string.Empty;
        //Clearing old filtered results
        Session["FilteredAirResults"] = null;
        Session["layoverAirports"] = null;
        Session["fromcityAirports"] = null;
        Session["tocityAirports"] = null;
        Session["airlines"] = null;

        //Prepare SearchRequest object 
        try
        {
            List<Airline> restrictedAirlines = new List<Airline>();
            if (Settings.LoginInfo.IsCorporate == "Y")
            {
                //Previously Corpoarte Profiles are combined with ProfileId - Grade 
                //Now Corpoarte Profiles are combined with ProfileId ~ Grade 
                //Reason For Change:Since For Predefined profiles the ProfileId will be -1,-2.
                //-1 ~ -1   Profile Id : -1,Grade :-1,Profile Name : Client
                //-2 ~ -2   Profile Id : -2,Grade :-2,Profile Name : Vendor
                if(ddlFlightEmployee.SelectedValue.Contains("~") && ddlFlightTravelReasons.SelectedValue.Contains("~"))
                {
                    string[] profId = ddlFlightEmployee.SelectedValue.Split('~');
                    if (profId.Length > 0)
                    {

                        Settings.LoginInfo.CorporateProfileGrade = profId[1];
                        searchRequest.CorporateTravelProfileId = Convert.ToInt32(profId[0]);
                        //Settings.LoginInfo.CorporateProfileId = Convert.ToInt32(profId[0]);// Overriding Profile Id from TRaveller Lilst

                        //searchRequest.CorporateTravelReason = Convert.ToInt32(profId[0]);
                        if (ddlFlightTravelReasons.SelectedValue.Contains("~"))
                        {
                            searchRequest.CorporateTravelReasonId = Convert.ToInt32(ddlFlightTravelReasons.SelectedValue.Split('~')[0]);
                            searchRequest.AppliedPolicy = (ddlFlightTravelReasons.SelectedValue.Split('~')[1] == "P" ? true : false);
                        }
                    }
                }
                else
                {
                    string[] profId = hdnFlightEmployee.Value.Split('~');
                    Settings.LoginInfo.CorporateProfileId = Convert.ToInt32(profId[0]);

                    if (profId.Length > 0)
                    {
                        searchRequest.CorporateTravelProfileId = Convert.ToInt32(profId[0]);
                        Settings.LoginInfo.CorporateProfileGrade = profId[1];
                        searchRequest.CorporateTravelProfileId = Convert.ToInt32(profId[0]);

                        if (hdnTravelReason.Value.Contains("~"))
                        {
                            searchRequest.CorporateTravelReasonId = Convert.ToInt32(hdnTravelReason.Value.Split('~')[0]);
                            searchRequest.AppliedPolicy = (hdnTravelReason.Value.Split('~')[1] == "P" ? true : false);
                        }
                    }
                }
            }
            hdnModifySearch.Value = "0";
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                if (hdnBookingAgent.Value == "Checked" && Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    // ziyad to check
                    //Settings.LoginInfo.AgentId = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
                    Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
                    if (ddlAirAgents.SelectedItem.Text.StartsWith("(") && ddlAirAgents.SelectedItem.Text.EndsWith(")"))
                    {
                        Settings.LoginInfo.CorporateProfileId = ddlFlightEmployee.SelectedValue.Contains("~") ? Convert.ToInt32(ddlFlightEmployee.SelectedValue.Split('~')[0]) : Convert.ToInt32(hdnFlightEmployee.Value.Split('~')[0]); 
                        Settings.LoginInfo.IsCorporate = "Y";
                    }
                    Settings.LoginInfo.IsOnBehalfOfAgent = true;
                    AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID, RecordStatus.Activated);

                    Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                }
                else
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                }
            }
            //Assign the changed OnBehalf Agent Location from the dropdown
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
            }
            if (hdnWayType.Value != "multiway")
            {

                searchRequest.AdultCount = Convert.ToInt32(ddlAdults.SelectedValue);
                searchRequest.ChildCount = Convert.ToInt32(ddlChilds.SelectedValue);
                searchRequest.InfantCount = Convert.ToInt32(ddlInfants.SelectedValue);
                searchRequest.RestrictAirline = false;
                searchRequest.Segments = new FlightSegment[2];
                FlightSegment fwdSegment = new FlightSegment();
                FlightSegment retSegment = new FlightSegment();

                //fwdSegment.Origin = Origin.Text.Split(')')[0].Replace("(", "");
                //fwdSegment.Destination = Destination.Text.Split(')')[0].Replace("(", "");
                fwdSegment.Origin = GetCityCode(Origin.Text.Trim());
                fwdSegment.NearByOriginPort = chkNearByPort.Checked;
                fwdSegment.Destination = GetCityCode(Destination.Text.Trim());



                string departureDate = DepDate.Text;

                switch (ddlDepTime.SelectedValue)
                {
                    //case "Morning":
                    //    departureDate += " 08:00 AM";
                    //    break;
                    //case "Afternoon":
                    //    departureDate += " 02:00 PM";
                    //    break;
                    //case "Evening":
                    //    departureDate += " 07:00 PM";
                    //    break;
                    //case "Night":
                    //    departureDate += " 01:00 AM";
                    //    break;
                    //default:
                    //    break;
                    case "Morning":
                        departureDate += " 05:00 AM";
                        break;
                    case "Afternoon":
                        departureDate += " 12:00 PM";
                        break;
                    case "Evening":
                        departureDate += " 06:00 PM";
                        break;
                    case "Night":
                        departureDate += " 11:59:59 PM";
                        break;
                    default:
                        if (ddlDepTime.SelectedValue.Contains("-"))
                        {
                            departureDate += " " + ddlDepTime.SelectedValue.Split('-')[0];
                            searchRequest.TimeIntervalSpecified = true;
                        }
                        else
                            departureDate += " 00:01:00";
                        break;
                    
                }

                fwdSegment.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);

                IFormatProvider format = new CultureInfo("en-GB", true);
                fwdSegment.PreferredDepartureTime = Convert.ToDateTime(departureDate, format);
                fwdSegment.PreferredArrivalTime = Convert.ToDateTime(departureDate, format);

                Airline restrictedAirline = new Airline();

                // added by bangar for preferredairline search
                if (txtPreferredAirline.Text != "Type Preferred Airline")
                {
                    airlineCode.Value = txtPreferredAirline.Text.Trim();
                    airlineCode.Value += !string.IsNullOrEmpty(Request.Form.Get("txtPreferredAirline0")) ? "," + Request.Form.Get("txtPreferredAirline0").Trim() : string.Empty;
                    airlineCode.Value += !string.IsNullOrEmpty(Request.Form.Get("txtPreferredAirline1")) ? "," + Request.Form.Get("txtPreferredAirline1").Trim() : string.Empty;
                    airlineCode.Value += !string.IsNullOrEmpty(Request.Form.Get("txtPreferredAirline2")) ? "," + Request.Form.Get("txtPreferredAirline2").Trim() : string.Empty;
                }

                if (airlineCode.Value.Length > 0)
                {
                    searchRequest.RestrictAirline = true;
                    //string[] airlines = new string[2];
                    //airlines[1] = airlineName.Value;
                    //airlines[0] = airlineCode.Value;
                    //fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                    if (airlineCode.Value.Split(',').Length == 1)
                    {
                        string[] airlines = new string[1];
                        //airlines[1] = airlineName.Value;
                        airlines[0] = airlineCode.Value;
                        restrictedAirline.Load(airlineCode.Value);
                        restrictedAirlines.Add(restrictedAirline);
                        fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                    }
                    else
                    {
                        string[] airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < airlines.Length; i++)
                        {
                            Airline airline = new Airline();                            
                            airline.Load(airlines[i]);
                            restrictedAirlines.Add(airline);
                        }
                        fwdSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                    }
                }
                else
                {
                    fwdSegment.PreferredAirlines = new string[0];
                }

                searchRequest.Segments[0] = fwdSegment;
                if (hdnWayType.Value == "return")
                {
                    retSegment.Origin = fwdSegment.Destination;
                    retSegment.NearByOriginPort = fwdSegment.NearByOriginPort;
                    retSegment.Destination = fwdSegment.Origin;
                    retSegment.flightCabinClass = fwdSegment.flightCabinClass;

                    string ReturnDateTime = ReturnDateTxt.Text;

                    switch (ddlReturnTime.SelectedValue)
                    {
                        //case "Morning":
                        //    ReturnDateTime += " 08:00 AM";
                        //    break;
                        //case "Afternoon":
                        //    ReturnDateTime += " 02:00 PM";
                        //    break;
                        //case "Evening":
                        //    ReturnDateTime += " 07:00 PM";
                        //    break;
                        //case "Night":
                        //    ReturnDateTime += " 01:00 AM";
                        //    break;
                        //default:
                        //    break;
                        case "Morning":
                            ReturnDateTime += " 05:00 AM";
                            break;
                        case "Afternoon":
                            ReturnDateTime += " 12:00 PM";
                            break;
                        case "Evening":
                            ReturnDateTime += " 06:00 PM";
                            break;
                        case "Night":
                            ReturnDateTime += " 11:59:59 PM";
                            break;
                        default:
                            if (ddlReturnTime.SelectedValue.Contains("-"))
                            {
                                ReturnDateTime += " " + ddlReturnTime.SelectedValue.Split('-')[0];
                                searchRequest.TimeIntervalSpecified = true;
                            }
                            else
                                ReturnDateTime += " 00:01:00";
                            break;
                    }
                    if (ReturnDateTime.Length > 0)
                    {
                        retSegment.PreferredArrivalTime = Convert.ToDateTime(ReturnDateTime, format);
                        retSegment.PreferredDepartureTime = Convert.ToDateTime(ReturnDateTime, format);
                    }
                    else
                    {
                        retSegment.PreferredArrivalTime = fwdSegment.PreferredArrivalTime.AddDays(1);
                        retSegment.PreferredDepartureTime = fwdSegment.PreferredDepartureTime.AddDays(1);
                    }

                    if (airlineCode.Value.Length > 0)
                    {
                        searchRequest.RestrictAirline = true;
                        //string[] airlines = new string[2];
                        //airlines[1] = airlineName.Value;
                        //airlines[0] = airlineCode.Value;
                        //retSegment.PreferredAirlines = airlines;
                        if (airlineCode.Value.Split(',').Length == 1)
                        {
                            string[] airlines = new string[1];
                            //airlines[1] = airlineName.Value;
                            airlines[0] = airlineCode.Value;
                            retSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                        }
                        else
                        {
                            string[] airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            retSegment.PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                        }
                    }
                    else
                    {
                        retSegment.PreferredAirlines = new string[0];
                    }

                    searchRequest.Segments[1] = retSegment;
                    searchRequest.Type = SearchType.Return;
                }
                else
                {
                    searchRequest.Segments[1] = new FlightSegment();
                    searchRequest.Type = SearchType.OneWay;
                }
                List<string> sources = new List<string>();
                foreach (ListItem item in chkSuppliers.Items)
                {
                    if (item.Selected)
                    {
                        sources.Add(item.Text);
                    }
                }

                
                //if (restrictedAirline.AirlineName != null && restrictedAirline.AirlineName.Length > 0)
                //{
                //    if (!restrictedAirline.TBOAirAllowed && restrictedAirlines.Count == 0)
                //    {
                //        if (sources.Count == 1)
                //        {
                //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                //        }
                //        //if (sources.Contains("TA"))
                //        //{
                //        //    sources.Remove("TA");
                //        //}
                //    }
                //    if (!restrictedAirline.UapiAllowed && restrictedAirlines.Count == 0)
                //    {
                //        if (sources.Count == 1)
                //        {
                //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                //        }
                //        if (sources.Remove("UA"))
                //        {
                //            sources.Remove("UA");
                //        }
                //    }
                //}

                foreach (Airline ra in restrictedAirlines)
                {
                    if (!ra.TBOAirAllowed && sources.Exists(delegate (string s) { return s.ToUpper() == "TA"; }))
                    {
                        if (!airlineMessage.Contains(ra.AirlineName))
                        {
                            if (airlineMessage.Length > 0)
                            {
                                airlineMessage += "," + ra.AirlineName + " for TA";
                            }
                            else
                            {
                                airlineMessage = ra.AirlineName + " for TA";
                            }
                        }
                        //if (sources.Contains("TA"))
                        //{
                        //    sources.Remove("TA");
                        //}
                    }
                    if (!ra.UapiAllowed && sources.Exists(delegate (string s) { return s.ToUpper() == "UA"; }))
                    {
                        if (!airlineMessage.Contains(ra.AirlineName))
                        {
                            if (airlineMessage.Length > 0)
                            {
                                airlineMessage += "," + ra.AirlineName + " for UA";
                            }
                            else
                            {
                                airlineMessage = ra.AirlineName + " for UA";
                            }
                        }
                        //if (sources.Remove("UA"))
                        //{
                        //    sources.Remove("UA");
                        //}
                    }
                }

                if (airlineCode.Value.Length > 0)
                {
                    if (!airlineCode.Value.Contains("G9") && sources.Contains("G9"))
                    {
                        sources.Remove("G9");
                    }
                    if (!airlineCode.Value.Contains("FZ") && sources.Contains("FZ"))
                    {
                        sources.Remove("FZ");                        
                    }
                    if (!airlineCode.Value.Contains("SG") && sources.Contains("SG"))
                    {
                        sources.Remove("SG");
                    }
                    if (!airlineCode.Value.Contains("6E") && sources.Contains("6E"))
                    {
                        sources.Remove("6E");
                    }
                    if (!airlineCode.Value.Contains("IX") && sources.Contains("IX"))
                    {
                        sources.Remove("IX");
                    }
                    if (airlineCode.Value.Contains("FZ") && sources.Contains("UA"))
                    {
                        sources.Remove("UA");//Remove UAPI if preferred airline is Fly Dubai.
                    }
                    //Remove G8 if preferred airline is not Go Air
                    if (!airlineCode.Value.Contains("G8") && sources.Contains("G8"))
                    {
                        sources.Remove("G8");
                    }
                    //Remove OV if preferred airline is not SalamAir
                    if (!airlineCode.Value.Contains("OV") && sources.Contains("OV"))
                    {
                        sources.Remove("OV");
                    }
                }
                //sources.Add("G9");
                //sources.Add("UA");
                searchRequest.Sources = sources;
                if (rbtnlMaxStops.SelectedItem.Value != "-1")
                {
                    searchRequest.MaxStops = rbtnlMaxStops.SelectedItem.Value;
                }
                if (chkRefundFare.Checked)
                {
                    searchRequest.RefundableFares = true;
                }
                

                Session["FlightRequest"] = searchRequest;
            }
            else
            {
                searchRequest.Type = SearchType.MultiWay;

                int adults = Convert.ToInt32(ddlAdults.SelectedValue);
                int childs = Convert.ToInt32(ddlChilds.SelectedValue);
                int infants = Convert.ToInt32(ddlInfants.SelectedValue);

                searchRequest.AdultCount = adults;
                searchRequest.ChildCount = childs;
                searchRequest.InfantCount = infants;

                int segmentCount = 2;

                //string city1 = Request["city1"];
                //string city2 = Request["city2"];
                //string city3 = Request["city3"];
                //string city4 = Request["city4"];

                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                int counter = 5;
                List<string> Cities = new List<string>();
                for (int i = counter; i < 13; i++)
                {
                    if (Request["ctl00$cphTransaction$City" + i].Length > 0 && Request["ctl00$cphTransaction$City" + (i + 1)].Length > 0)
                    {
                        segmentCount++;
                        Cities.Add(Request["ctl00$cphTransaction$City" + i]);
                        Cities.Add(Request["ctl00$cphTransaction$City" + (i + 1)]);
                    }
                    i++;
                }

                searchRequest.Segments = new FlightSegment[segmentCount];

                FlightSegment segment1 = new FlightSegment();
                segment1.Origin = GetCityCode(City1.Text);
                segment1.NearByOriginPort = chkNearByPort1.Checked;
                segment1.Destination = GetCityCode(City2.Text);
                segment1.PreferredArrivalTime = Convert.ToDateTime(Time1.Text, dateFormat);
                segment1.PreferredDepartureTime = Convert.ToDateTime(Time1.Text, dateFormat);
                segment1.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                searchRequest.Segments[0] = segment1;

                FlightSegment segment2 = new FlightSegment();
                segment2.Origin = GetCityCode(City3.Text);
                segment2.NearByOriginPort = chkNearByPort2.Checked;
                segment2.Destination = GetCityCode(City4.Text);
                segment2.PreferredArrivalTime = Convert.ToDateTime(Time2.Text, dateFormat);
                segment2.PreferredDepartureTime = Convert.ToDateTime(Time2.Text, dateFormat);
                segment2.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                searchRequest.Segments[1] = segment2;

                int timeCounter = 3;
                counter = 0;
                if (Cities.Count > 0)
                {
                    while (counter < Cities.Count)
                    {
                        FlightSegment segment = new FlightSegment();
                        segment.Origin = GetCityCode(Cities[counter]);
                        segment.NearByOriginPort = !string.IsNullOrEmpty(Request["ctl00$cphTransaction$chkNearByPort" + timeCounter]);
                        counter++;
                        segment.Destination = GetCityCode(Cities[counter]);
                        segment.PreferredArrivalTime = Convert.ToDateTime(Request["ctl00$cphTransaction$Time" + timeCounter], dateFormat);
                        segment.PreferredDepartureTime = Convert.ToDateTime(Request["ctl00$cphTransaction$Time" + timeCounter], dateFormat);
                        segment.flightCabinClass = (CabinClass)Convert.ToInt32(ddlBookingClass.SelectedValue);
                        searchRequest.Segments[timeCounter - 1] = segment;

                        timeCounter++;
                        counter++;
                    }
                }

                //if (city7.Length > 0 && city8.Length > 0)
                //{
                //    FlightSegment segment4 = new FlightSegment();
                //    segment4.Origin = GetCityCode(city7);
                //    segment4.Destination = GetCityCode(city8);
                //    segment4.PreferredArrivalTime = Convert.ToDateTime(Request["t4"], dateFormat);
                //    segment4.PreferredDepartureTime = Convert.ToDateTime(Request["t4"], dateFormat);

                //    searchRequest.Segments[3] = segment4;
                //}

                //if (city9.Length > 0 && city10.Length > 0)
                //{
                //    FlightSegment segment5 = new FlightSegment();
                //    segment5.Origin = GetCityCode(city9);
                //    segment5.Destination = GetCityCode(city10);
                //    segment5.PreferredArrivalTime = Convert.ToDateTime(Request["t5"], dateFormat);
                //    segment5.PreferredDepartureTime = Convert.ToDateTime(Request["t5"], dateFormat);

                //    searchRequest.Segments[4] = segment5;
                //}

                //if (city11.Length > 0 && city12.Length > 0)
                //{
                //    FlightSegment segment6 = new FlightSegment();
                //    segment6.Origin = GetCityCode(city11);
                //    segment6.Destination = GetCityCode(city12);
                //    segment6.PreferredArrivalTime = Convert.ToDateTime(Request["t6"], dateFormat);
                //    segment6.PreferredDepartureTime = Convert.ToDateTime(Request["t6"], dateFormat);

                //    searchRequest.Segments[5] = segment6;
                //}
                Airline restrictedAirline = new Airline();
                

                if (airlineCode.Value.Length > 0)
                {
                    searchRequest.RestrictAirline = true;
                    string[] airlines = new string[0];
                    if (airlineCode.Value.Length == 1)
                    {
                        airlines = new string[2];
                        airlines[1] = airlineName.Value;
                        airlines[0] = airlineCode.Value;
                        restrictedAirline.Load(airlineCode.Value);
                        restrictedAirlines.Add(restrictedAirline);
                    }
                    else
                    {
                        airlines = airlineCode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string code in airlines)
                        {
                            Airline airline = new Airline();
                            airline.Load(code);
                            restrictedAirlines.Add(airline);
                        }
                    }

                    searchRequest.Segments[0].PreferredAirlines = airlines;//txtPreferredAirline.Text.Split('|');
                    searchRequest.Segments[0].PreferredAirlines = airlines;
                    if (searchRequest.Segments.Length > 2)
                    {
                        searchRequest.Segments[2].PreferredAirlines = airlines;
                    }
                    if (searchRequest.Segments.Length > 3)
                    {
                        searchRequest.Segments[3].PreferredAirlines = airlines;
                    }
                    if (searchRequest.Segments.Length > 4)
                    {
                        searchRequest.Segments[4].PreferredAirlines = airlines;
                    }
                    if (searchRequest.Segments.Length > 5)
                    {
                        searchRequest.Segments[5].PreferredAirlines = airlines;
                    }
                }
                else
                {
                    searchRequest.Segments[0].PreferredAirlines = new string[0];
                    searchRequest.Segments[1].PreferredAirlines = new string[0];
                    if (searchRequest.Segments.Length > 2)
                    {
                        searchRequest.Segments[2].PreferredAirlines = new string[0];
                    }
                    if (searchRequest.Segments.Length > 3)
                    {
                        searchRequest.Segments[3].PreferredAirlines = new string[0];
                    }
                    if (searchRequest.Segments.Length > 4)
                    {
                        searchRequest.Segments[4].PreferredAirlines = new string[0];
                    }
                    if (searchRequest.Segments.Length > 5)
                    {
                        searchRequest.Segments[5].PreferredAirlines = new string[0];
                    }
                }

                List<string> sources = new List<string>();
                foreach (ListItem item in chkSuppliers.Items)
                {
                    if (item.Selected)
                    {
                        sources.Add(item.Text);
                    }
                }

                //if (restrictedAirline.AirlineName != null && restrictedAirline.AirlineName.Length > 0)
                //{
                //    if (!restrictedAirline.TBOAirAllowed && restrictedAirlines.Count == 0)
                //    {
                //        if (sources.Count == 1)
                //        {
                //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                //        }
                //        if (sources.Contains("TA"))
                //        {
                //            sources.Remove("TA");
                //        }
                //    }
                //    if (!restrictedAirline.UapiAllowed && restrictedAirlines.Count == 0)
                //    {
                //        if (sources.Count == 1)
                //        {
                //            airlineMessage = "Preferred Airline (" + restrictedAirline.AirlineName + ") has been restricted";
                //        }
                //        if (sources.Remove("UA"))
                //        {
                //            sources.Remove("UA");
                //        }
                //    }
                //}

                foreach (Airline ra in restrictedAirlines)
                {
                    if (!ra.TBOAirAllowed)
                    {
                        if (!airlineMessage.Contains(ra.AirlineName))
                        {
                            if (airlineMessage.Length > 0)
                            {
                                airlineMessage += "," + ra.AirlineName;
                            }
                            else
                            {
                                airlineMessage = ra.AirlineName;
                            }
                        }
                        //if (sources.Contains("TA"))
                        //{
                        //    sources.Remove("TA");
                        //}
                    }
                    if (!ra.UapiAllowed)
                    {
                        if (!airlineMessage.Contains(ra.AirlineName))
                        {
                            if (airlineMessage.Length > 0)
                            {
                                airlineMessage += "," + ra.AirlineName;
                            }
                            else
                            {
                                airlineMessage = ra.AirlineName;
                            }
                        }
                        //if (sources.Remove("UA"))
                        //{
                        //    sources.Remove("UA");
                        //}
                    }
                }
                if (airlineCode.Value.Length > 0)
                {
                    if (!airlineCode.Value.Contains("G9") && sources.Contains("G9"))
                    {
                        sources.Remove("G9");
                    }
                    if (!airlineCode.Value.Contains("FZ") && sources.Contains("FZ"))
                    {
                        sources.Remove("FZ");                        
                    }
                    if (!airlineCode.Value.Contains("SG") && sources.Contains("SG"))
                    {
                        sources.Remove("SG");
                    }
                    if (airlineCode.Value.Contains("FZ") && sources.Contains("UA"))
                    {
                        sources.Remove("UA");//Remove UAPI if preferred airline is Fly Dubai.
                    }
                }
                //sources.Add("G9");
                //sources.Add("UA");
                searchRequest.Sources = sources;
                if (rbtnlMaxStops.SelectedItem.Value != "-1")
                {
                    searchRequest.MaxStops = rbtnlMaxStops.SelectedItem.Value;
                }
                if (chkRefundFare.Checked)
                {
                    searchRequest.RefundableFares = true;
                }
                

                Session["FlightRequest"] = searchRequest;
            }

            bool isSearchRestricted = false;
            int uapiRestrict = 0, tboRestrict = 0;
            if (restrictedAirlines.Count > 0)
            {
                if (restrictedAirlines.Count == airlineMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length)
                {
                    isSearchRestricted = true;
                }

                uapiRestrict = restrictedAirlines.FindAll(delegate(Airline a) { return !a.UapiAllowed; }).Count;
                tboRestrict = restrictedAirlines.FindAll(delegate(Airline a) { return !a.TBOAirAllowed; }).Count;

                if (restrictedAirlines.Count == uapiRestrict)
                {
                    if (searchRequest.Sources.Contains("UA"))
                    {
                        searchRequest.Sources.Remove("UA");
                    }
                }

                if (restrictedAirlines.Count == tboRestrict)
                {
                    if (searchRequest.Sources.Contains("TA"))
                    {
                        searchRequest.Sources.Remove("TA");
                    }
                }
            }
            
            if (searchRequest.InfantCount > 0 || (searchRequest.Type == SearchType.MultiWay && searchRequest.Segments.Length > 2) || searchRequest.RefundableFares)
            {
                if (searchRequest.Sources.Count > 1)
                    searchRequest.Sources.Remove("PK");
                else if(searchRequest.Sources.Contains("PK"))
                {
                    if (searchRequest.InfantCount > 0 && searchRequest.Segments.Length > 2)
                    {
                        isSearchRestricted = true;
                        lblMessage.Text = "Infants are not allowed currently in Search for PK. Only two connections/segments allowed in Search for PK. Please modify your search.";
                    }
                    else if (searchRequest.InfantCount > 0)
                    {
                        isSearchRestricted = true;
                        lblMessage.Text = "Infants are not allowed currently in Search for PK. Please modify your search.";
                    }
                    else if (searchRequest.Segments.Length > 2)
                    {
                        isSearchRestricted = true;
                        lblMessage.Text = "Only two connections/segments allowed in Search for PK. Please modify your search.";
                    }
                }
            }

            if (isSearchRestricted)
            {
                if (!string.IsNullOrEmpty(airlineMessage))                
                {
                    lblMessage.Text += "You are not allowed to search with these preferred airlines." + airlineMessage;
                }                
            }
            else
            {
                Response.Redirect("FlightResult.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    private string GetCityCode(string searchCity) // For Flight
    {
        string cityCode = "";

        cityCode = searchCity.Split(',')[0];

        if (cityCode.Contains("(") && cityCode.Contains(")") && cityCode.Length > 3)
        {
            cityCode = cityCode.Substring(1, 3);
        }
        return cityCode;
    }

    void BindFilterTimings(SearchRequest request)
    {
        SearchResult[] results = Session["Results"] as SearchResult[];
        if (request.TimeIntervalSpecified)
        {
            ddlOnwardTimings.Items.Clear();
            ddlReturnTimings.Items.Clear();
            ddlOnwardTimings.Items.Add(new ListItem("Any Time", "Any Time"));
            ddlReturnTimings.Items.Add(new ListItem("Any Time", "Any Time"));
            results.ToList().ForEach(x =>
            {
                if (!filteredOnwardTimings.Contains(x.Flights[0][0].DepartureTime.ToString("HH:mm")))
                    filteredOnwardTimings.Add(x.Flights[0][0].DepartureTime.ToString("HH:mm"));
                if (x.Flights.Length > 1)
                {
                    if (!filteredReturnTimings.Contains(x.Flights[1][0].DepartureTime.ToString("HH:mm")))
                        filteredReturnTimings.Add(x.Flights[1][0].DepartureTime.ToString("HH:mm"));
                }
            });
            filteredOnwardTimings.Sort();
            filteredReturnTimings.Sort();
            filteredOnwardTimings.ForEach(x => { ListItem item = new ListItem(x, x); ddlOnwardTimings.Items.Add(item); });
            if (request.Type == SearchType.Return)
                filteredReturnTimings.ForEach(x => { ListItem item = new ListItem(x, x); ddlReturnTimings.Items.Add(item); });
        }
    }
    

    
}
