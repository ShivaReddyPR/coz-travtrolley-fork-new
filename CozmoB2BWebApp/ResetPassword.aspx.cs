﻿using System;
using System.Drawing;
using CT.TicketReceipt.BusinessLayer;

public partial class ResetPasswordGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["requestId"] == null || UserMaster.ValidatePasswordChangeRequest(Request["requestId"]) <= 0)
        {
            ResetPasswordMsg.Text = "Access Denied !";
            ResetPasswordMsg.ForeColor = Color.Red;
            ResetPasswordPanel.Visible = false;
        }
        else
        {
            requestId.Value = Request["requestId"];
        }
    }
    protected void ResetPasswordButton_Click(object sender, EventArgs e)
    {
        if (ComparePassword.IsValid)
        {
            int memberId = UserMaster.ValidatePasswordChangeRequest(requestId.Value);
            if (memberId > 0)
            {
                UserMaster member = new UserMaster(memberId);
                member.Password = NewPassword.Text;
                member.Save();
                ResetPasswordMsg.Text = "Your password has been changed successfully !";
                UserMaster.DeletePasswordChangeRequest(requestId.Value);
                ResetPasswordPanel.Visible = false;
                Response.Redirect("Login.aspx?errMessage=Your password has been changed successfully !");
            }
            else
            {
                ResetPasswordMsg.Text = "Access Denied !";
                ResetPasswordMsg.ForeColor = Color.Red;
                ResetPasswordPanel.Visible = false;
            }
        }
    }
}
