﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System.IO;


public partial class AgentBalSummary : CT.Core.ParentPage
{
    protected int AgentId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport);
            lblSuccessMsg.Text = string.Empty;            
            if (!IsPostBack)
            {
               
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitalizeControls();
                lblSuccessMsg.Text = string.Empty;
                AgentId = Settings.LoginInfo.AgentId;

            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void InitalizeControls()
    {
        try
        {
            BindAgent();
            dcFromDate.Value = DateTime.Now;
        }
        catch (Exception ex)
        { 
            throw ex ;
        }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));
        }
        catch { throw; }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtBalance=AgentMaster.GetBalanceSummery(Utility.ToInteger(ddlAgent.SelectedItem.Value),dcFromDate.Value);

            string attachment = "attachment; filename=AgentBalanceReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgAgencyBalReportList.AllowPaging = false;
            dgAgencyBalReportList.DataSource = dtBalance;
            dgAgencyBalReportList.DataBind();
            dgAgencyBalReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
        finally
        {
            Response.End();
        }
    }
}
