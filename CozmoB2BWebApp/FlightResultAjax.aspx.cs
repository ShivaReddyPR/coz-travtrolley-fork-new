﻿using System;
using System.Collections.Generic;
using System.Linq;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class FlightResultAjax : System.Web.UI.Page
    {
        protected List<SearchResult> onwardResults = new List<SearchResult>();
        protected List<SearchResult> returnResults = new List<SearchResult>();
        Dictionary<SearchType, List<SearchResult>> filteredResults = new Dictionary<SearchType, List<SearchResult>>();
        protected List<SearchResult> onwardFilteredResults = new List<SearchResult>();
        protected List<SearchResult> returnFilteredResults = new List<SearchResult>();
        protected SearchResult searchResult = new SearchResult();
        protected string dtimeActive = string.Empty, stopsActive = string.Empty, atimeActive = string.Empty, priceActive = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["type"] != null && Session["OnwardResults"] != null)
            {
                string type = Request["type"].ToLower();
                SearchRequest request = Session["FlightRequest"] as SearchRequest;
                if (type == "on")
                {
                    onwardResults = Session["OnwardResults"] as List<SearchResult>;
                    
                }
                else
                {
                    returnResults = Session["ReturnResults"] as List<SearchResult>;
                    
                }

                //Filtering results
                //Always flights will be filtered first from the original results list and then all other filters will be applied on the remaining list of results
                if (!string.IsNullOrEmpty(Request["airlines"]))
                {
                    string filter = Request["airlines"];

                    if (type == "on")
                    {
                        if (filter.Contains("false"))
                        {
                            foreach (string airline in filter.Split(','))
                            {
                                string[] arr = airline.Split('-');
                                if (Convert.ToBoolean(arr[1]))
                                {
                                    onwardFilteredResults.AddRange(onwardResults.FindAll(r => r.Flights[0].Any(x => x.Airline == arr[0].Trim())));

                                }
                            }
                        }
                        else
                        {
                            onwardFilteredResults.AddRange(onwardResults);
                        } 
                    }   
                    else
                    {
                        if(filter.Contains("false"))
                        {
                            foreach (string airline in filter.Split(','))
                            {
                                string[] arr = airline.Split('-');
                                if (Convert.ToBoolean(arr[1]))
                                {
                                    returnFilteredResults.AddRange(returnResults.FindAll(r => r.Flights[0].Any(x => x.Airline == arr[0].Trim())));

                                }
                            }
                        }
                        else
                        {
                            returnFilteredResults.AddRange(returnResults);
                        }
                    }
                        
                    
                }

                if (!string.IsNullOrEmpty(Request["fares"]))
                {
                    //ref-true,nonref-false; according to this only refundable fare flights will be kept and rest will be removed
                    //ref means refundable fare flights
                    //nonref means non refundable fare flights
                    //true means show the result 
                    //false means remove the result
                    string[] filter = Request["fares"].Split(',');
                    string[] refund = filter[0].Split('-');
                    string[] nonrefund = filter[1].Split('-');
                    if (type == "on")
                    {
                        if (!Convert.ToBoolean(refund[1]))
                        {
                            onwardFilteredResults.RemoveAll(x => x.NonRefundable == false);
                        }
                        if (!Convert.ToBoolean(nonrefund[1]))
                        {
                            onwardFilteredResults.RemoveAll(x => x.NonRefundable);
                        }
                    }
                    else
                    {
                        if (!Convert.ToBoolean(refund[1]))
                        {
                            returnFilteredResults.RemoveAll(x => x.NonRefundable == false);
                        }
                        if (!Convert.ToBoolean(nonrefund[1]))
                        {
                            returnFilteredResults.RemoveAll(x => x.NonRefundable);                            
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request["stops"]))
                {
                    //0-true,1-false,2-false; according to this only non stop flights will be kept and rest will be removed
                    //0 means direct or non stop flights
                    //1 means one stop flights
                    //2 means more than two stop flights
                    //true means show the result 
                    //false means remove the result
                    string[] filter = Request["stops"].Split(',');
                    string[] direct = new string[0];
                    string[] onestop = new string[0];
                    string[] twostop = new string[0];
                    foreach (string stop in filter)
                    {
                        if (stop.Split('-')[0].Contains("0"))
                        {
                            direct = stop.Split('-');
                        }
                        else if (stop.Split('-')[0].Contains("1"))
                        {
                            onestop = stop.Split('-');
                        }
                        else
                        {
                            twostop = stop.Split('-');
                        }
                    }
                    if (type == "on")
                    {                        
                        if(!Convert.ToBoolean(twostop[1]))
                        {
                            int count = onwardFilteredResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() -1 >1);
                        }
                        if (!Convert.ToBoolean(onestop[1]))
                        {
                            int count = onwardFilteredResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() -1 == 1);
                        }
                        if (!Convert.ToBoolean(direct[1]))
                        {
                            int count = onwardFilteredResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count()-1 == 0);
                        }                        
                    }
                    else
                    {
                        if (!Convert.ToBoolean(twostop[1]))
                        {
                            int count = returnFilteredResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 > 1);
                        }
                        if (!Convert.ToBoolean(onestop[1]))
                        {
                            int count = returnFilteredResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1);
                        }
                        if (!Convert.ToBoolean(direct[1]))
                        {
                            int count = returnFilteredResults.RemoveAll(x => x.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request["timings"]))
                {
                    string filter = Request["timings"];
                    //DateTime departureDateSort = (departureDateSortString, "M/d/yyyy", null); //M/d/yyyy
                    //DateTime morning = departureDateSort.AddHours(5);//.AddHours(4);//05:00 AM to 11:59 AM
                    //DateTime afternoon = departureDateSort.AddHours(12);//.AddHours(11);//12:00 PM to 05:59 PM
                    //DateTime evening = departureDateSort.AddHours(18);//.AddHours(16);//06:00 PM to 11:59 PM
                    //DateTime night = departureDateSort;//.AddHours(21);Night 12:00 AM to 04:59 AM

                    if (request.TimeIntervalSpecified)
                    {
                        if (filter != "Any Time" && type == "on")
                            onwardFilteredResults.RemoveAll(x => x.Flights[0][0].DepartureTime.ToString("HH:mm") != filter);
                        else if (filter != "Any Time" && type == "ret")
                            returnFilteredResults.RemoveAll(x => x.Flights[0][0].DepartureTime.ToString("HH:mm") != filter);
                    }
                    else
                    {
                        switch (filter)
                        {
                            case "morning":// from 5 AM to 11:59:59 AM
                                if (type == "on")
                                    onwardFilteredResults = onwardFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 5 && r.Flights[0][0].DepartureTime.Hour < 12);
                                else
                                    returnFilteredResults = returnFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 5 && r.Flights[0][0].DepartureTime.Hour < 12);
                                break;
                            case "noon"://12 PM to 5:59:59 PM
                                if (type == "on")
                                    onwardFilteredResults = onwardFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 12 && r.Flights[0][0].DepartureTime.Hour < 18);
                                else
                                    returnFilteredResults = returnFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 12 && r.Flights[0][0].DepartureTime.Hour < 18);
                                break;
                            case "evening":// 6 PM to 11:59:59 PM
                                if (type == "on")
                                    onwardFilteredResults = onwardFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 18 && r.Flights[0][0].DepartureTime.Hour < 0);
                                else
                                    returnFilteredResults = returnFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 18 && r.Flights[0][0].DepartureTime.Hour < 0);
                                break;
                            case "night": //12:00 AM to 4:59:59 AM
                                if (type == "on")
                                    onwardFilteredResults = onwardFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 0 && r.Flights[0][0].DepartureTime.Hour < 5);
                                else
                                    returnFilteredResults = returnFilteredResults.FindAll(r => r.Flights[0][0].DepartureTime.Hour >= 0 && r.Flights[0][0].DepartureTime.Hour < 5);
                                break;
                        }
                    }
                }
                if (Request["price"] != null)
                {
                    string[] filter = Request["price"].Split('-');
                    if (type == "on")
                        onwardFilteredResults = (onwardFilteredResults.FindAll(r => Math.Ceiling(r.TotalFare) >= Convert.ToDouble(filter[0]) && Math.Ceiling(r.TotalFare) <= Convert.ToDouble(filter[1])));
                    else
                        returnFilteredResults = (returnFilteredResults.FindAll(r => Math.Ceiling(r.TotalFare) >= Convert.ToDouble(filter[0]) && Math.Ceiling(r.TotalFare) <= Convert.ToDouble(filter[1])));
                }

                if (Request["sort"] != null)
                {
                    //If no filters are applied and doing only sorting then get the original results and apply sorting on them
                    //otherwise sorting will be applied on the filtered list of results
                    if (string.IsNullOrEmpty(Request["airlines"]))
                    {
                        onwardFilteredResults = onwardResults;
                        returnFilteredResults = returnResults;
                    }
                    string sort = Request["sort"];//Sort by Price, Airline, Dep time, Arr time, Stops
                    string order = Request["order"].ToLower();//Order by DESC or ASC
                    if(string.IsNullOrEmpty(order))
                    {
                        sort = "price";
                        order = "asc";
                    }
                    switch (sort)
                    {
                        case "price":
                            priceActive = (order == "desc" ? "active" : "");
                            if (type == "on")//Onward
                                onwardFilteredResults = (order == "desc" ? onwardFilteredResults.OrderByDescending(x => x.TotalFare).ToList() : onwardFilteredResults.OrderBy(x => x.TotalFare).ToList());                            
                            else//return
                                returnFilteredResults = (order == "desc" ? returnFilteredResults.OrderByDescending(x => x.TotalFare).ToList() : returnFilteredResults.OrderBy(x => x.TotalFare).ToList());
                            break;
                        case "dtime"://Departure time
                            dtimeActive = (order == "desc" ? "active" : "");
                            if (type == "on")
                                onwardFilteredResults = (order == "desc" ? SortByDepartureTime(onwardFilteredResults, false) : SortByDepartureTime(onwardFilteredResults, true));
                            else
                                returnFilteredResults = (order == "desc" ? SortByDepartureTime(returnFilteredResults, false) : SortByDepartureTime(returnFilteredResults, true));
                            break;
                        case "atime"://Arrival time
                            atimeActive = (order == "desc" ? "active" : "");
                            if (type == "on")
                                onwardFilteredResults = (order == "desc" ? SortByArrivalTime(onwardFilteredResults, false) : SortByArrivalTime(onwardFilteredResults, true));
                            else
                                returnFilteredResults = (order == "desc" ? SortByArrivalTime(returnFilteredResults, false) : SortByArrivalTime(returnFilteredResults, true));
                            break;
                        case "aline"://Airline
                            if (type == "on")
                                onwardFilteredResults = (order == "desc" ? onwardFilteredResults.OrderByDescending(x => x.Flights[0][0].Airline).ToList() : onwardFilteredResults.OrderBy(x => x.Flights[0][0].Airline).ToList());
                            else
                                returnFilteredResults = (order == "desc" ? returnFilteredResults.OrderByDescending(x => x.Flights[0][0].Airline).ToList() : returnFilteredResults.OrderBy(x => x.Flights[0][0].Airline).ToList());
                            break;
                        case "stops"://Stops
                            stopsActive = (order == "desc" ? "active" : "");
                            if (type == "on")
                                onwardFilteredResults = (order == "desc" ? onwardFilteredResults.OrderByDescending(x => x.Flights[0].Length).ToList() : onwardFilteredResults.OrderBy(x => x.Flights[0].Length).ToList());
                            else
                                returnFilteredResults = (order == "desc" ? returnFilteredResults.OrderByDescending(x => x.Flights[0].Length).ToList() : returnFilteredResults.OrderBy(x => x.Flights[0].Length).ToList());
                            break;
                    }
                }
            }
            else
            {
                Response.Write("Your Login Session Expired. Please Search again");
            }
        }

        static List<SearchResult> SortByDepartureTime(List<SearchResult> searchResults, bool ascendingOrder)
        {
            if (ascendingOrder)
                searchResults.Sort((x, y) => x.Flights[0][0].DepartureTime.TimeOfDay.TotalMinutes.CompareTo(y.Flights[0][0].DepartureTime.TimeOfDay.TotalMinutes));
            else
                searchResults.Sort((x, y) => y.Flights[0][0].DepartureTime.TimeOfDay.TotalMinutes.CompareTo(x.Flights[0][0].DepartureTime.TimeOfDay.TotalMinutes));

            return searchResults;
        }

        static List<SearchResult> SortByArrivalTime(List<SearchResult> searchResults, bool ascendingOrder)
        {
            if (ascendingOrder)
                searchResults.Sort((x, y) => x.Flights[0][x.Flights[0].Length - 1].ArrivalTime.TimeOfDay.TotalMinutes.CompareTo(y.Flights[0][y.Flights[0].Length - 1].ArrivalTime.TimeOfDay.TotalMinutes));            
            else
                searchResults.Sort((x, y) => y.Flights[0][y.Flights[0].Length - 1].ArrivalTime.TimeOfDay.TotalMinutes.CompareTo(x.Flights[0][x.Flights[0].Length - 1].ArrivalTime.TimeOfDay.TotalMinutes));
                

            return searchResults;
        }
    }
}
