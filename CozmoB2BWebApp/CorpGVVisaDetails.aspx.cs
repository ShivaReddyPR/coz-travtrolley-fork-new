﻿using CT.TicketReceipt.Common;
using CT.Core;
using CT.Corporate;
using CT.GlobalVisa;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Collections;
using CT.AccountingEngine;
using System.IO;

public partial class CorpGVVisaDetailsGUI : CT.Core.ParentPage
{
    protected GlobalVisaSession visaSession = new GlobalVisaSession();
    protected CorporateProfile cropProfileDetails;
    protected List<GVPassenger> passengerList = new List<GVPassenger>();
    protected string passIsseCountry = string.Empty;
    protected string residenceCountry = string.Empty;
    protected GVEnquiry gvEnquiryDet = new GVEnquiry();
    protected GVRequirementMaster gvRequire = new GVRequirementMaster();
    protected GVTermsMaster gvTerms = new GVTermsMaster();
    protected List<GVHandlingFeeMaster> gvHandlingFee = new List<GVHandlingFeeMaster>();
    protected List<GVFeeMaster> gvFeeList = new List<GVFeeMaster>();
    protected int approvalCount = 0;
    protected string approverNames = string.Empty;
    protected int approverId = 0;
    protected GVVisaSale detail = new GVVisaSale();
    protected string errorMessage = string.Empty;
    protected bool IsCorporate = false;//Default False
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo == null) //Checking Logininfo
            {
                Response.Redirect("AbandonSession.aspx", false);
                return;
            }
            if (!string.IsNullOrEmpty(Settings.LoginInfo.IsCorporate) && Settings.LoginInfo.IsCorporate == "Y")
            {
                IsCorporate = true;
            }
            if (!IsPostBack)
            {
                if (Session["GVSession"] != null)
                {
                    visaSession = Session["GVSession"] as GlobalVisaSession;
                    if (visaSession != null && visaSession.PassengerList != null)
                    {
                        if (visaSession != null && visaSession.CorpProfileDetails != null)
                        {
                            cropProfileDetails = visaSession.CorpProfileDetails as CorporateProfile;
                        }
                        passengerList = visaSession.PassengerList as List<GVPassenger>;
                        if (passengerList != null && passengerList.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(passengerList[0].Fpax_country_id))
                            {
                                passIsseCountry = Country.GetCountry(passengerList[0].Fpax_country_id).CountryName;
                            }
                        }
                        if (!string.IsNullOrEmpty(visaSession.ResCountryCode))
                        {
                            residenceCountry = Country.GetCountry(visaSession.ResCountryCode).CountryName;
                        }
                        //Loading All Details(Enquiry Details,Terms&Condition and Visa Fee Details)
                        gvEnquiryDet = GVVisaSale.GetEnquiryDetails(visaSession.TravelToCountryCode, visaSession.NationalityCode, visaSession.VisaTypeId, visaSession.ResCountryCode,Settings.LoginInfo.AgentId); ;
                        if (gvEnquiryDet != null)
                        {
                            if (gvEnquiryDet.GvRequirements != null)
                            {
                                gvRequire = gvEnquiryDet.GvRequirements;
                            }
                            if (gvEnquiryDet.GvTerms != null)
                            {
                                gvTerms = gvEnquiryDet.GvTerms;
                            }
                            if (gvEnquiryDet.GvFeeList != null)
                            {
                                gvFeeList = gvEnquiryDet.GvFeeList;
                            }
                            if(gvEnquiryDet.GVHandlFee !=null)
                            {
                                gvHandlingFee = gvEnquiryDet.GVHandlFee;
                            }
                            visaSession.VisaEnquireList = gvEnquiryDet;
                        }
                        if (IsCorporate && cropProfileDetails != null && cropProfileDetails.ProfileApproversList !=null)
                        {
                            approvalCount = cropProfileDetails.ProfileApproversList.Where(p => p.Type == "V").Count();
                        }
                        if (IsCorporate && !string.IsNullOrEmpty(visaSession.ActionStatus) && visaSession.ActionStatus.Trim() == "E" ) // E ActionStatus Means Cardit Card Payment
                        {
                            lblOnAccount.Text = "Card";
                            visaSession.OnAccountStatus = "C";
                        }
                        else
                        {
                            if (approvalCount > 0)  //Need to Send Approvals
                            {
                                lblOnAccount.Text = "";
                                btnApply.Text = "Approved";
                                visaSession.OnAccountStatus = "A";
                            }
                            else //OnAccount Direct Payment
                            {
                                lblOnAccount.Text = "OnAccount";
                                visaSession.OnAccountStatus = "O";
                            }
                        }
                        Session["GVSession"] = visaSession;
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Details is Null", Request["REMOTE_ADDR"]);
                        Response.Redirect("AbandonSession.aspx", false);
                    }
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                    Response.Redirect("AbandonSession.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVVisaDetails page load exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
 
    protected void btnApply_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["GVSession"] != null && Utility.ToInteger(hdnSelectedFeeId.Value) > 0)
            {
                visaSession = Session["GVSession"] as GlobalVisaSession;
                int visaFeeId = Utility.ToInteger(hdnSelectedFeeId.Value);
                int visaHandlingFee = Utility.ToInteger(hdnSelectedHandlingFeeId.Value);
                GVVisaSale objVisaSale = new GVVisaSale();
                if (visaSession != null && visaSession.PassengerList != null)
                {
                    if (visaSession.CorpProfileDetails != null && IsCorporate)
                    {
                        cropProfileDetails = visaSession.CorpProfileDetails as CorporateProfile;
                    }
                    passengerList = visaSession.PassengerList as List<GVPassenger>;
                    GVFeeMaster objGvFee = new GVFeeMaster();
                    GVHandlingFeeMaster objGVHandlingfee = new GVHandlingFeeMaster();
                    if (visaSession.VisaEnquireList != null)
                    {
                        gvEnquiryDet = visaSession.VisaEnquireList as GVEnquiry;
                        if (gvEnquiryDet != null)
                        {
                            gvFeeList = gvEnquiryDet.GvFeeList;
                            objGvFee = gvFeeList.Where(p => p.TransactionId == visaFeeId).FirstOrDefault();//Loading Selected FeeDetails
                        }
                        if (gvEnquiryDet != null && gvEnquiryDet.GVHandlFee != null)
                        {
                            gvHandlingFee = gvEnquiryDet.GVHandlFee;
                            objGVHandlingfee = gvHandlingFee.Where(p => p.TransactionId == visaHandlingFee).FirstOrDefault();//Loading Selected Handling FeeDetails
                        }
                    }
                            if (passengerList != null && objGvFee != null && passengerList.Count > 0)
                    {
                        objVisaSale.PassportType = passengerList[0].Fpax_passport_type;
                        objVisaSale.DocDate = DateTime.Now;
                        objVisaSale.LocationId = Settings.LoginInfo.LocationID;
                        objVisaSale.CompanyId = Settings.LoginInfo.CompanyID;
                        objVisaSale.CreatedBy = Settings.LoginInfo.UserID;
                        objVisaSale.AgentId = Settings.LoginInfo.AgentId;
                        objVisaSale.CountryId = visaSession.TravelToCountryCode;
                        objVisaSale.NationalityId = visaSession.NationalityCode;
                        objVisaSale.VisaTypeId = visaSession.VisaTypeId;
                        objVisaSale.ResidencyId = visaSession.ResCountryCode;
                        objVisaSale.VisaCategory = objGvFee.VisaCategory;
                        objVisaSale.TTTravelDate = visaSession.VisaDate;
                        objVisaSale.Adults = visaSession.Adult;
                        objVisaSale.Children = visaSession.Child;
                        objVisaSale.Infants = visaSession.Infant;
                        objVisaSale.TravelReason = visaSession.TravelResionName;
                        decimal TotalAdultsCharge = 0;
                        decimal TotalChildCharge = 0;
                        decimal TotalInfantCharge = 0;
                        //decimal adSvcTax = 0;
                        //decimal chSvcTax = 0;
                        //decimal inSvcTax = 0;
                        decimal adaddlfee = 0;
                        decimal chaddlfee = 0;
                        decimal inaddlfee = 0;
                        decimal totVisaFee = 0;
                        decimal GVTotvisafee = (objGvFee.AdultVisaFee * visaSession.Adult)+(objGvFee.ChildVisaFee* visaSession.Child)+(objGvFee.InfantVisaFee* visaSession.Infant);
                        TotalAdultsCharge = objGvFee.TotalChargeAdult+ objGVHandlingfee.HandlingFee;
                        TotalChildCharge = objGvFee.TotalChargeChild + objGVHandlingfee.HandlingFee;
                        TotalInfantCharge = objGvFee.TotalChargeInfant + objGVHandlingfee.HandlingFee;
                        //adSvcTax = objGvFee.AdultService;
                        //chSvcTax = objGvFee.ChildService;
                        //inSvcTax = objGvFee.InfantVisaFee;
                        adaddlfee = objGvFee.AdultAdd1Fee;
                        chaddlfee = objGvFee.ChildAdd1Fee;
                        inaddlfee = objGvFee.ChildAdd1Fee;
                        objVisaSale.AdultVisaFee = Utility.ToDecimal((TotalAdultsCharge - adaddlfee- objGVHandlingfee.HandlingFee) * visaSession.Adult);
                        objVisaSale.ChildVisaFee = Utility.ToDecimal((TotalChildCharge - chaddlfee- objGVHandlingfee.HandlingFee) * visaSession.Child);
                        objVisaSale.InfantVisaFee = Utility.ToDecimal((TotalInfantCharge - inaddlfee- objGVHandlingfee.HandlingFee) * visaSession.Infant);
                        objVisaSale.AdultSvcCharge = Utility.ToDecimal((adaddlfee + objGVHandlingfee.HandlingFee) * visaSession.Adult);
                        objVisaSale.ChildSvcCharge = Utility.ToDecimal((chaddlfee + objGVHandlingfee.HandlingFee) * visaSession.Child);
                        objVisaSale.InfantSvcCharge = Utility.ToDecimal((inaddlfee + objGVHandlingfee.HandlingFee) * visaSession.Infant);
                        totVisaFee = Utility.ToDecimal((TotalAdultsCharge * visaSession.Adult) + (TotalChildCharge * visaSession.Child) + (TotalInfantCharge * visaSession.Infant));
                        objVisaSale.TotVisaFee = totVisaFee;
                        objVisaSale.ApprovedStatus = "N";
                        objVisaSale.AgentId = Settings.LoginInfo.AgentId;
                        if (visaSession.OnAccountStatus == "C")
                        {
                            objVisaSale.SettlementMode = Utility.ToString(CT.TicketReceipt.BusinessLayer.PaymentMode.Card);
                            objVisaSale.PaymentStatus = "P";
                        }
                        else
                        {
                            if (visaSession.OnAccountStatus == "A")
                            {
                                objVisaSale.ApprovedStatus = "Y";
                                objVisaSale.ApprovedBy = Settings.LoginInfo.AgentId;
                                objVisaSale.ProfileApproversList = cropProfileDetails.ProfileApproversList.Where(p => p.Type == "V").ToList();
                            }
                            objVisaSale.SettlementMode = Utility.ToString(CT.TicketReceipt.BusinessLayer.PaymentMode.Credit);
                        }
                        objVisaSale.CurrencyCode = Settings.LoginInfo.Currency;
                        objVisaSale.ExchangeRate = 1;
                        objVisaSale.PassengerList = passengerList;
                        objVisaSale.ProfileId = visaSession.TravelEmployeeId;
                        AgentMaster agency = new AgentMaster(Settings.LoginInfo.AgentId);
                        agency.UpdateBalance(0);
                        Settings.LoginInfo.AgentBalance = agency.CurrentBalance;
                        if (totVisaFee > agency.CurrentBalance && visaSession.OnAccountStatus == "O")
                        {
                            errorMessage = "Dear Agent you do not have sufficient balance to book a room for this hotel.";
                            return;
                        }
                        else
                        {                            
                            objVisaSale.Save(); //Saving All pAx Details
                            totVisaFee = totVisaFee - GVTotvisafee;// As per Jojy, Need to Exclude Visa fee from Agency balance deduction
                            if (!IsCorporate)
                            {
                                List<CorpProfileDocuments> uploadedFiles = new List<CorpProfileDocuments>();
                                uploadedFiles = Session["docfiles"] as List<CorpProfileDocuments>;

                                foreach (GVPassenger passenger in objVisaSale.PassengerList)
                                {
                                    if (passenger.Fpax_id > 0 && passenger.Fpax_passport_no != string.Empty)
                                    {
                                        if (uploadedFiles != null && uploadedFiles.Count > 0)
                                        {
                                            List<CorpProfileDocuments> docs = uploadedFiles.Where(i => i.PassportNo == passenger.Fpax_passport_no).Select(p => p).ToList();
                                            string tempPath = ConfigurationManager.AppSettings["ProfileImage"];
                                            tempPath = tempPath + "/" + passenger.Fpax_id;
                                            string paxpath = Server.MapPath(tempPath);
                                            if (!Directory.Exists(paxpath))
                                            {
                                                Directory.CreateDirectory(paxpath);
                                            }
                                            string oldimagepath = passenger.Fpax_docpath;
                                            foreach (CorpProfileDocuments docments in docs)
                                            {
                                                if (docments.ProfileId > 0)
                                                {
                                                    File.Move(Server.MapPath(ConfigurationManager.AppSettings["ProfileImage"] + "/" + docments.ProfileId) + "//" + docments.DocFileName, Path.Combine(paxpath, docments.DocFileName));                                       
                                                }                                             
                                                docments.ProfileId = passenger.Fpax_id;
                                                docments.DocTypeName = docments.DocTypeName;
                                                docments.DocFileName = docments.DocFileName;
                                                docments.DocFilePath = paxpath;
                                                docments.CreatedBy = (int)Settings.LoginInfo.UserID;
                                                docments.Save();
                                                if (docments.RetDocId > 0)
                                                {
                                                    if (File.Exists(Server.MapPath(oldimagepath) + "//" + docments.DocFileName))
                                                    {
                                                        if(!File.Exists(paxpath+ "//" + docments.DocFileName))
                                                        {
                                                            File.Move(Server.MapPath(oldimagepath) + "//" + docments.DocFileName, Path.Combine(paxpath, docments.DocFileName));
                                                            File.Delete(Server.MapPath(oldimagepath) + "//" + docments.DocFileName);
                                                        }
                                                    }
                                                }
                                            }                                           
                                        }
                                    }
                                }
                            }

                            visaSession.VisaSale = objVisaSale;
                            Session["GVSession"] = visaSession;
                            long visaId = objVisaSale.TransactionId;
                            if (objVisaSale.TransactionId > 0)
                            {
                                if (visaSession.OnAccountStatus == "C") //OnAccountStatus C Means Need to Send Payment Getway
                                {
                                    Response.Redirect("PaymentProcessingForGVVisa.aspx", false);
                                }
                                else if (visaSession.OnAccountStatus == "O")
                                {
                                    //Agent Balance Deducting
                                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                                    agency.CreatedBy = Settings.LoginInfo.UserID;
                                    agency.UpdateBalance(-totVisaFee);
                                    Settings.LoginInfo.AgentBalance -= totVisaFee;
                                    //Saving Ledger
                                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                                    NarrationBuilder objNarration = new NarrationBuilder();
                                    //ledgerTxn = new LedgerTransaction();
                                    ledgerTxn.LedgerId = Settings.LoginInfo.AgentId;
                                    ledgerTxn.Amount = -totVisaFee;
                                    objNarration.HotelConfirmationNo = objVisaSale.DocNumber;
                                    objNarration.TravelDate = objVisaSale.TTTravelDate.ToShortDateString();
                                    ledgerTxn.ReferenceType = ReferenceType.GlobalVisaBooked;
                                    objNarration.Remarks = "Global Visa Booking Charges";
                                    ledgerTxn.Narration = objNarration;
                                    ledgerTxn.IsLCC = true;
                                    ledgerTxn.ReferenceId = Utility.ToInteger(objVisaSale.TransactionId);

                                    ledgerTxn.Notes = "";
                                    ledgerTxn.Date = DateTime.UtcNow;
                                    ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    ledgerTxn.TransType = "B2B";
                                    ledgerTxn.Save();
                                    Session["GVSession"] = null;
                                    Response.Redirect("CorpGVAcknowledgement.aspx?gvId=" + visaId, false);
                                }
                                else if (visaSession.OnAccountStatus == "A")
                                {
                                    Session["GVSession"] = null; //All Session Clearing
                                    try
                                    {

                                        detail = new GVVisaSale(visaId);
                                        if (detail != null && detail.ProfileApproversList != null)
                                        {
                                            //int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
                                            try
                                            {
                                                List<string> toArray = new System.Collections.Generic.List<string>();
                                                List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == 1).Select(i => i.ApproverId).ToList();
                                                if (listOfAppId != null && listOfAppId.Count > 0)
                                                {
                                                    foreach (int appId in listOfAppId)
                                                    {
                                                        approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverName).FirstOrDefault();
                                                        string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                                                        if (!string.IsNullOrEmpty(appEmail))
                                                        {
                                                            toArray.Add(appEmail);
                                                            System.IO.StringWriter stWr = new System.IO.StringWriter();
                                                            HtmlTextWriter htwr = new HtmlTextWriter(stWr);
                                                            EmailDivApprover.RenderControl(htwr);
                                                            string myPageHT = string.Empty;
                                                            myPageHT = stWr.ToString();
                                                            myPageHT = myPageHT.Replace("none", "block");
                                                            Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Visa Approval Request", myPageHT, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                                            toArray.Clear();
                                                            approverNames = string.Empty;
                                                        }
                                                    }
                                                }
                                                if (detail.PassengerList != null && detail.PassengerList.Count > 0)
                                                {
                                                    approverNames = detail.PassengerList[0].Fpax_name;
                                                    string empEmail = detail.PassengerList[0].Fpax_email;
                                                    if (!string.IsNullOrEmpty(empEmail))
                                                    {
                                                        toArray.Add(empEmail);
                                                    }
                                                    System.IO.StringWriter sw = new System.IO.StringWriter();
                                                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                                                    EmailDivEmployee.RenderControl(htw);
                                                    string myPageHTML = string.Empty;
                                                    myPageHTML = sw.ToString();
                                                    myPageHTML = myPageHTML.Replace("none", "block");
                                                    string subject = "Visa Status Change Notification";
                                                    if (toArray != null && toArray.Count > 0)
                                                    {
                                                        Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                                    }
                                                    toArray.Clear();
                                                    approverNames = string.Empty;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateGlobalVisaApprovalsQueue.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
                                            }
                                        }
                                    }
                                    catch { }
                                    Response.Redirect("CorpGVAcknowledgement.aspx?gvId=" + visaId, false);
                                }
                            }
                            else
                            {
                                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Transaction Id Empty", Request["REMOTE_ADDR"]);
                                Response.Redirect("AbandonSession.aspx", false);
                            }
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile PassengerDetails is Null", Request["REMOTE_ADDR"]);
                        Response.Redirect("AbandonSession.aspx", false);
                    }
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Details is Null", Request["REMOTE_ADDR"]);
                    Response.Redirect("AbandonSession.aspx", false);
                }
            }
            else
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVVisaDetails Apply Click Event exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }        
    }
}
