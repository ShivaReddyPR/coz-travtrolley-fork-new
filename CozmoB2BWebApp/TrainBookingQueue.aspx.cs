using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System.Linq;
using System.Text;

public partial class TrainBookingQueue : CT.Core.ParentPage// System.Web.UI.Page
{
    //Offline Train Booking
    protected FlightItinerary[] flightItinerary = new FlightItinerary[0];
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected AgentMaster agency;
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["AgentBookingQueueRecordsPerPage"]);
    protected int queueCount = 0;
    protected int noOfPages;
    protected int pageNo;
    public string message = string.Empty;
    protected string pageType = string.Empty;
    protected decimal isAgentCapable ;
    public string PaxEmail;
    public string PaxName;
    public string BoardingDate;
    public string Adults;
    public string Children;
    public string TrainName;
    public string TrainNumber;
    public string FromStation;
    public string ToStation;
    public string BoardingFrom;
    public string ReservationUpto;
    public string Class;
    public string Quota;
    public string PNR;
    public string CreatedOn;
    public string Status;
    public string ReferenceId;
    public string Remarks;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblErrorMessage.Visible = false;
        hdfParam.Value = "1";
        Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
        AuthorizationCheck();

        if (!IsPostBack)
        {
            hdfParam.Value = "0";
            Settings.LoginInfo.IsOnBehalfOfAgent = false;
            txtCreatedFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtCreatedTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            //ddlAgency.SelectedValue = Settings.LoginInfo.AgentId.ToString();
            isAgentCapable = Settings.LoginInfo.AgentBalance;
            if (Request["message"] != null)
            {
                message = Request["message"];
            }
            BindAgent();
            RailwayBookingSearch search = new RailwayBookingSearch();
            BindSearch(search);
        }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            RailwayItinerary railway = new RailwayItinerary();
            //Used existing method GetList to get the agent list. Following method is used to remove the duplicate items in dropdown list
            dtAgents = railway.RemoveDuplicateRows(dtAgents, "AGENT_ID");
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            if (!ddlAgency.Items.Contains(new ListItem("--All--", "0")))
                ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = new RailwayBookingSearch();
        BindSearch(search);
    }
    private void BindSearch(RailwayBookingSearch search)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            DateTime? fromDate = null;
            DateTime? toDate = null;
            try
            {
                fromDate = Convert.ToDateTime(txtCreatedFrom.Text.Trim(), dateFormat);
                toDate = Convert.ToDateTime(txtCreatedTo.Text.Trim(), dateFormat);
            }
            catch { }
            if(ddlAgency.SelectedItem.Value!="0")
                search.AgentId = Convert.ToInt64(ddlAgency.SelectedItem.Value);
            if (txtBoardingDate.Text.Trim() != "")
            {
                try
                {
                    search.BoardingDate = Convert.ToDateTime(txtBoardingDate.Text.Trim(), dateFormat);
                }
                catch { }
            }
            search.TrainNumber = txtTrainNumber.Text.Trim() != "" ? txtTrainNumber.Text.Trim() : null;
            search.TrainName = txtTrainName.Text.Trim() != "" ? txtTrainName.Text.Trim() : null;
            search.Email = txtEmail.Text.Trim() != "" ? txtEmail.Text.Trim() : null;
            search.PNR = txtPNR.Text.Trim() != "" ? txtPNR.Text.Trim() : null;
            search.CreatedFrom = fromDate;
            search.CreatedTo = toDate;
            search.ReferenceId = txtReferenceID.Text.Trim() != "" ? txtReferenceID.Text.Trim() : null;
            search.Status = ddlStatus.SelectedItem.Value != "All" ? ddlStatus.SelectedItem.Value : null;
            search.PaxName = txtPaxName.Text.Trim() != "" ? txtPaxName.Text.Trim() : null;
            DataSet ds = RailwayBookingSearch.Search(search);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dtOfflineBooking = ds.Tables[0];
                    dlBookingQueue.DataSource = dtOfflineBooking;
                }
                else
                {
                    lblErrorMessage.Text = "No records found";
                    lblErrorMessage.Visible = true;
                }
            }
            dlBookingQueue.DataBind();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[1].Rows.Count > 0)
                {
                    int resultCount = Convert.ToInt32(ds.Tables[1].Rows[0]["Count"]);
                    //Pagination
                    doPaging(search, resultCount);
                }
            }
            ViewState["Search"] = search;
        }
        catch
        {
            lblErrorMessage.Text = "Error in search. Please try again.";
            lblErrorMessage.Visible = true;
            divPagination.Style.Add("display", "none");
            throw;
        }
    }
    // Access perm
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        
    }
    protected void dlBookingQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "SubmitRemarks")
            {
                Label lblDataListErrorMessage = (Label)e.Item.FindControl("lblDataListErrorMessage");
                HiddenField hdnPendingList = (HiddenField)e.Item.FindControl("hdnPendingList");
                HiddenField hdnInprogressList = (HiddenField)e.Item.FindControl("hdnInprogressList");
                HiddenField hdnConfirmedList = (HiddenField)e.Item.FindControl("hdnConfirmedList");

                HiddenField hdnAgentID = (HiddenField)e.Item.FindControl("hdnAgentID");
                HiddenField hdnReservationUpto = (HiddenField)e.Item.FindControl("hdnReservationUpto");
                HiddenField hdnQuota = (HiddenField)e.Item.FindControl("hdnQuota");
                HiddenField hdnCurrentPNR = (HiddenField)e.Item.FindControl("hdnCurrentPNR");
                HiddenField hdnCurrentStatus = (HiddenField)e.Item.FindControl("hdnCurrentStatus");
                HiddenField hdnItemIndex = (HiddenField)e.Item.FindControl("hdnItemIndex");

                TextBox txtRemarks = (TextBox)e.Item.FindControl("txtRemarks");
                TextBox txtPNR = (TextBox)e.Item.FindControl("txtPNR");

                Label lblEmail = (Label)e.Item.FindControl("lblEmail");
                Label lblPaxName = (Label)e.Item.FindControl("lblPaxName");
                Label lblBoardingDate = (Label)e.Item.FindControl("lblBoardingDate");
                Label lblAdults = (Label)e.Item.FindControl("lblAdults");
                Label lblChildren = (Label)e.Item.FindControl("lblChildren");
                Label lblTrainName = (Label)e.Item.FindControl("lblTrainName");
                Label lblTrainNumber = (Label)e.Item.FindControl("lblTrainNumber");
                Label lblFromStation = (Label)e.Item.FindControl("lblFromStation");
                Label lblToStation = (Label)e.Item.FindControl("lblToStation");
                Label lblBoardingFrom = (Label)e.Item.FindControl("lblBoardingFrom");
                Label lblClass = (Label)e.Item.FindControl("lblClass");
                Label lblCreatedOn = (Label)e.Item.FindControl("lblCreatedOn");
                Label lblReferenceId = (Label)e.Item.FindControl("lblReferenceId");

                string currentStatus = hdnCurrentStatus.Value;
                string remarks = txtRemarks.Text.Trim();
                string passengerEmail = lblEmail.Text;
                string itemIndex = hdnItemIndex.Value;
                long bookingId = Convert.ToInt64(e.CommandArgument);
                string newStatus = "";
                string pnr = txtPNR.Text.Trim();
                if (currentStatus == "Pending")
                    newStatus = hdnPendingList.Value;
                if (currentStatus == "In Progress")
                    newStatus = hdnInprogressList.Value;
                if (currentStatus == "Confirmed")
                    newStatus = hdnConfirmedList.Value;
                PNR = hdnCurrentPNR.Value;
                RailwayBookingRemarks remarksObj = new RailwayBookingRemarks();
                if (currentStatus != "Confirmed" && newStatus == "Confirmed" && pnr != "")
                {
                    remarksObj.PNR = pnr;
                    PNR = pnr;
                }
                remarksObj.BookingID = bookingId;
                remarksObj.RemarksStatus = newStatus;
                remarksObj.Remarks = remarks;
                remarksObj.CreatedBy = Settings.LoginInfo.AgentId;
                int saveStatus = remarksObj.SaveRemarks();
                
                //Store the values to prepare the email body
                PaxEmail = lblEmail.Text;
                PaxName = lblPaxName.Text;
                BoardingDate = lblBoardingDate.Text;
                Adults = lblAdults.Text;
                Children = lblChildren.Text;
                TrainName = lblTrainName.Text;
                TrainNumber = lblTrainNumber.Text;
                FromStation = lblFromStation.Text;
                ToStation = lblToStation.Text;
                BoardingFrom = lblBoardingFrom.Text;
                Class = lblClass.Text;
                CreatedOn = lblCreatedOn.Text;
                ReferenceId = lblReferenceId.Text;

                ReservationUpto = hdnReservationUpto.Value;
                Quota = hdnQuota.Value;
                Status = newStatus;
                Remarks = remarks;

                if (saveStatus == 0)
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "showErr", "showErr('" + itemIndex + "','PNR already exist','error');", true);
                else
                {
                    string subject = "";
                    string body = "";
                    CreateRemarksMail(ref subject, ref body);
                    RailwayItinerary.SendMail(Convert.ToInt64(hdnAgentID.Value), passengerEmail, subject, body);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "showErr", "showErr('" + itemIndex + "','Remarks successfully updated','success');", true);
                }
            }
            if (e.CommandName == "ShowHistory")
            {
                long bookingId = Convert.ToInt64(e.CommandArgument);
                RailwayBookingRemarks remarksObj = new RailwayBookingRemarks();
                DataSet dt = remarksObj.GetAllRemarks(bookingId);
                if (dt.Tables.Count > 0)
                {
                    if ((dt.Tables[0].Rows.Count > 0))
                    {
                        dlRemarksHistory.DataSource = dt;
                        dlRemarksHistory.DataBind();
                    }
                }
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "showModal", "showModal();", true);
            }
            BindAgent();
            RailwayBookingSearch search = new RailwayBookingSearch();
            BindSearch(search);
        }
        catch
        {
            throw;
        }
    }
    public void CreateRemarksMail(ref string subject, ref string body)
    {
        string adultString = Convert.ToInt32(Adults) > 1 ? "Adults" : "Adult";
        string childtString = Convert.ToInt32(Children) > 1 ? "Children" : "Child";
        subject = "Your Booking status has been updated";
        StringBuilder sb = new StringBuilder();
        sb.Append("<body style=\"font-family:arial;font-size: 12px;margin: 0px;padding: 0px;background-color: White;color:#4c4c4b;\">");
        sb.Append("<div style=\"width:778px;margin: auto;\">");
        sb.Append("<div style=\"width:778px;float:left;\">");
        sb.Append("<div style=\"float:left;width:100%;border: solid 1px #c0c0c0;\">");
        sb.Append("<div style=\"float:left;width:97%;margin:0px;padding-top:20px;padding-left:0px;\">");
        sb.Append("<div style=\"float:left;width:100%;padding: 10px;\">");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Reference ID:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + ReferenceId + "</span></p>");
        if (PNR != "")
        {
            sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
            sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">PNR:</span> ");
            sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + PNR + "</span></p>");
        }
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Passenger Name:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + PaxName + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Email:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + PaxEmail + "</span></p>");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Boarding Date:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + BoardingDate + "</span></p>");
        sb.Append(" <p style=\"float:left; width:100%; margin:0; padding:0;\">"
            + "<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\"> No.of Guest(s):</span>"
            + "<span style=\"float:left;width:150px;font-weight:bold;font-size:15px;\">" + (Convert.ToInt32(Adults) + Convert.ToInt32(Children)).ToString() + "  </span>");
        sb.Append("<span style=\"float:left;width:120px;font-weight:bold;font-size:15px;\">" + Adults.ToString() + " " + adultString + "</span>");
        if (Convert.ToInt32(Children) > 0)
            sb.Append("<span style=\"float:left;width:120px;font-weight:bold;font-size:15px;\">" + Children.ToString() + " " + childtString + "</span>");
        sb.Append("</p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Train Number:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + TrainNumber + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Train Name:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + TrainName + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Trip:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + FromStation + " - " + ToStation + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Boarding From:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + BoardingFrom + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Reservation Upto:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + ReservationUpto + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Requested on:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + CreatedOn + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Class:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Class + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Quota:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Quota + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Status:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Status + "</span></p>");
        sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
        sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:200px;\">Remarks:</span> ");
        sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + Remarks + "</span></p>");
        sb.Append("</div></div>");
        sb.Append("</div></div></div></body>");
        body = sb.ToString();
    }

    //Pagination
    private void doPaging(RailwayBookingSearch search, int resultCount)
    {
        if (resultCount > 0)
        {
            int noOfPages = (resultCount / 10) + (resultCount % 10 == 0 ? 0 : 1);
            int totalPageset = (noOfPages / 10) + (noOfPages % 10 == 0 ? 0 : 1);
            search.TotalPageSet = totalPageset;
            search.NumberOfPages = noOfPages;
            search.ResultCount = resultCount;
            DataTable dtPagination = new DataTable();
            dtPagination.Columns.Add("PageNumber", typeof(int));
            dtPagination.Columns.Add("Class", typeof(string));
            dtPagination.Columns.Add("Enabled", typeof(bool));
            int limit = search.PageSet == totalPageset ? ((search.PageSet - 1) * 10) + (noOfPages % 10) : search.PageSet * 10;
            for (int i = (search.PageSet - 1) * 10 + 1; i <= limit; i++)
            {
                DataRow dr = dtPagination.NewRow();
                dr["PageNumber"] = i;
                if (search.PageNumber == i)
                {
                    dr["Class"] = "disabled active";
                    dr["Enabled"] = false;
                }
                else
                {
                    dr["Class"] = "";
                    dr["Enabled"] = true;
                }
                dtPagination.Rows.Add(dr);
            }
            rptPagination.DataSource = dtPagination;
            rptPagination.DataBind();
            divPagination.Style.Add("display", "block");
            liPrevious.Attributes.Add("class", "page-item");
            liPreviousSet.Attributes.Add("class", "page-item");
            liNext.Attributes.Add("class", "page-item");
            liNextSet.Attributes.Add("class", "page-item");
            liFirst.Attributes.Add("class", "page-item");
            liLast.Attributes.Add("class", "page-item");
            btnPrevious.Enabled = true;
            btnPreviousSet.Enabled = true;
            btnNext.Enabled = true;
            btnNextSet.Enabled = true;
            btnFirst.Enabled = true;
            btnLast.Enabled = true;
            if (search.PageNumber == 1)
            {
                btnPrevious.Enabled = false;
                btnFirst.Enabled = false;
                liPrevious.Attributes.Add("class", "page-item disabled");
                liFirst.Attributes.Add("class", "page-item disabled");
            }
            if (search.PageSet == 1)
            {
                btnPreviousSet.Enabled = false;
                liPreviousSet.Attributes.Add("class", "page-item disabled");
            }
            if (search.PageNumber == noOfPages)
            {
                btnNext.Enabled = false;
                btnLast.Enabled = false;
                liNext.Attributes.Add("class", "page-item disabled");
                liLast.Attributes.Add("class", "page-item disabled");
            }
            if (search.PageSet == search.TotalPageSet)
            {
                btnNextSet.Enabled = false;
                liNextSet.Attributes.Add("class", "page-item disabled");
            }

        }
        else
        {
            divPagination.Style.Add("display", "none");
        }
    }
    protected void rptPagination_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "pagination")
            {
                int pageNumber = Convert.ToInt32(e.CommandArgument);
                RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
                search.PageNumber = pageNumber;
                BindSearch(search);
                ViewState["Search"] = search;
            }
        }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
        int pageNumber = search.PageNumber - 1;
        if (pageNumber > 0)
        {
            if (search.PageNumber % 10 == 1 && search.PageNumber > 10)
            {
                search.PageSet = search.PageSet - 1;
            }
            search.PageNumber = pageNumber;
        }
        BindSearch(search);
        ViewState["Search"] = search;
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
        int pageNumber = search.PageNumber + 1;
        if (pageNumber <= search.NumberOfPages)
        {
            if (search.PageNumber % 10 == 0 && search.PageNumber <= search.NumberOfPages)
            {
                search.PageSet = search.PageSet + 1;
            }
            search.PageNumber = pageNumber;
        }
        BindSearch(search);
        ViewState["Search"] = search;
    }
    protected void btnPreviousSet_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
        int pageNumber = search.PageNumber - (10 + ((search.PageNumber % 10 == 0 ? 10 : search.PageNumber % 10) - 1));
        if (pageNumber > 0)
        {
            search.PageNumber = pageNumber;
            search.PageSet = search.PageSet - 1;
        }
        BindSearch(search);
        ViewState["Search"] = search;
    }
    protected void btnNextSet_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
        int pageNumber = search.PageNumber + (11 - (search.PageNumber % 10 == 0 ? 10 : search.PageNumber % 10));
        if (pageNumber <= search.NumberOfPages)
        {
            search.PageNumber = pageNumber;
            search.PageSet = search.TotalPageSet == search.PageSet ? search.PageSet : search.PageSet + 1;
        }
        BindSearch(search);
        ViewState["Search"] = search;
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
        search.PageNumber = 1;
        search.PageSet = 1;
        BindSearch(search);
        ViewState["Search"] = search;
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        RailwayBookingSearch search = (RailwayBookingSearch)ViewState["Search"];
        search.PageNumber = search.NumberOfPages;
        search.PageSet = search.TotalPageSet;
        BindSearch(search);
        ViewState["Search"] = search;
    }
}

