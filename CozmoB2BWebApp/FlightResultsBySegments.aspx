﻿<%@ Page Title="Flight Results" Language="C#" MasterPageFile="~/TransactionBE.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="FlightResultsBySegments.aspx.cs" Inherits="CozmoB2BWebApp.FlightResultsBySegments" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <style type="text/css">


.onlythis {
    z-index: 101;
    cursor: pointer;
}
     .body_container {
         min-height: calc(100vh - 166px);
     }
     .only-this-list:hover .onlythis{
         opacity:1;
     }
    .filter-category .custom-checkbox-style label {
        font-size: 12px;
    }
    .collapse.pub.in {
    display: none;
}
    @media (max-width: 992px){
        .flight-list-wrapper .book-now-section {
            margin-top:5px;
        }
    }
   .corp-travel-dropDown {
    min-width: 220px !important;
    min-height: 200px !important;
    top: 150px !important;
    bottom: auto;
    padding: 15px !important;
    margin: 0 !important;
    right: 25% !important;
    left: auto !important;
    box-shadow: rgba(0,0,0,0.6) 0 1px 11px;
}
    .corpPolicyTip{
        font-size: 11px;
        font-weight: 700;
        color: #575857;
        text-align:left;
        max-height: 300px;
        overflow: auto;
        width:170px;
    }
    </style>    
    <link href="css/select2.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jsBE/SearchResult.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link href="css/select2.css" rel="stylesheet" />    
    <script src="Scripts/SliderJquery/jquery-ui-1.12.js" type="text/javascript"></script>
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script src="Scripts/MultiCity.js" type="text/javascript" defer="defer"></script>
  <script src="Scripts/PrefAirline.js" type="text/javascript" defer="defer"></script>
    <!-- Results Selection and Booking Script -->
    <script>
        <%CT.Core.AgentAppConfig appConfig = new CT.Core.AgentAppConfig();
                appConfig.AgentID = Settings.LoginInfo.AgentId;
                List<CT.Core.AgentAppConfig> liappConfigs = appConfig.GetConfigData();
        if ((onwardResults.Count > 0) || (onwardResults.Count > 0 && returnResults.Count > 0)){
            %>
        var onwardResultJson = [];
        var returnResultJson = [];
        var airlines = JSON.parse('<%=airlineArray.Replace("'","")%>');
        try {
            var onwardResultJson = JSON.parse('<%=onwardResultJson.Replace("'","")%>');
        } catch (e) {
            //console.log('error converting onward res: '+e);
        }
        try {
            var returnResultJson = JSON.parse('<%=returnResultJson.Replace("'","")%>');
        } catch (e) {
           // console.log('error converting return res: '+e);
        }
        <%}%>
        var showPub = '';
        var onSelResultId = '', retSelResultId = '', onSelCounter = 0, retSelCounter = 0;
        var totalPrice = eval(0), onDiscountVal = eval(0), retDiscountVal = eval(0), decimalValue=eval('<%=decimalValue%>');
        var onFareType = '', retFareType = '', currency = '', onSource = '', retSource = '', onAirline = '', retAirline = '';       
        var onCorpBookingAmt = eval(0), retCorpBookingAmt = eval(0), totalBkgAmt = eval(0);
        var policyRules = '', prefAirlines = '';
        var isOnPrefAirline = false, isRetPrefAirline = true, isOnNegotiableFare = false, isRetNegotiableFare = true, isOnInsidePolicy = false, isRetInsidePolicy = true;
        var restrictBooking = false;

        //This method is used to show selected Result values on top header
        function SelectResult(index, type) {
            try {
                
                if ($('#lbtext').html() == "Hide Pub Fare") {
                    showPub = 'in';
                }
                else {
                    showPub = '';
                }
                if (type == 'ONWARD') {

                    //Start Onward=========Show dynamic Flight Details Div Based on selection=========
                    if (index != null) {
                        if (document.getElementById('divFD' + index) != null) {
                            var parentId = document.getElementById('divFD' + index).parentNode.parentNode.id;
                            if (document.getElementById(parentId) != null) {
                                $('div.onwardFD', '#' + parentId).hide();
                                $('#divFD' + index).show();

                            }
                        }
                    }
                    //End=========Show dynamic Flight Details Div Based on selection=========

                    //Get ResultId
                    onDiscountVal = 0;
                    onSelCounter = index;
                    var resultId = parseInt(eval($('#hdnOnResultId' + index).val()));
                    onSelResultId = resultId;//Assign selected ResultId for Booking
                    onFareType = $('#hdnOnFareType' + index).val();//Assign Fare Type
                    onSource = $('#hdnOnSource' + index).val();//Assign Source name
                    for (var i in onwardResultJson) {
                        if (onwardResultJson[i].ResultId == resultId) {//When Selected ResultId is matched then entire result JSON will be fetched                        
                            var res = onwardResultJson[i];
                            currency = res.Currency;//Assign Currency for display
                            onAirline = res.Airline;//Assign Airline Code for displaying Logo 
                            var onpriceVal = parseFloat($('#hdnOnPrice' + index).val());
                            $('#hdnOnPrice').val(onpriceVal.toFixed(2));//Assign Selected Onward Price for displaying Total 
                            var onSeg = res.Flights[0][0];
                            var retSeg = res.Flights[0][res.Flights[0].length - 1];

                        <%if (Settings.LoginInfo.IsCorporate == "Y")     {%> 
                            if (res.TravelPolicyResult != null) {
                                if (res.TravelPolicyResult.PolicyBreakingRules != null) {
                                    if (res.TravelPolicyResult.PolicyBreakingRules.TOTALBOOKINGAMOUNT != null) {
                                        onCorpBookingAmt = eval(res.TravelPolicyResult.PolicyBreakingRules.TOTALBOOKINGAMOUNT[0].split(':')[1]);
                                    }
                                    isOnInsidePolicy = res.TravelPolicyResult.IsUnderPolicy;
                                    $('#onpolicy').html("Inside Policy: " + isOnInsidePolicy);

                                    for (var key in res.TravelPolicyResult.PolicyBreakingRules) {

                                        for (var value in res.TravelPolicyResult.PolicyBreakingRules[key]) {
                                            if (key != "PREFERREDAIRLINES" && key != "RESTRICTBOOKING" && key != "AVOIDTRANSITTIME" && key != "COMPLETEBOOKING") {
                                                if (policyRules.length > 0 && !policyRules.split(',').includes(res.TravelPolicyResult.PolicyBreakingRules[key][value]))
                                                    policyRules += ',' + res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                                else if (!policyRules.split(',').includes(res.TravelPolicyResult.PolicyBreakingRules[key][value]))
                                                    policyRules += res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                            }
                                            else if (key == "PREFERREDAIRLINES") {
                                                if (prefAirlines.length > 0)
                                                    prefAirlines += ', ' + res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                                else
                                                    prefAirlines = res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                            }
                                            else if (key == "RESTRICTBOOKING") {
                                                if (res.TravelPolicyResult.PolicyBreakingRules[key][value] == "Y")
                                                    restrictBooking = true;
                                            }
                                        }
                                    }
                                }

                                if (!res.TravelPolicyResult.IsPreferredAirline) {
                                    isOnPrefAirline = false;
                                }
                                else {
                                    isOnPrefAirline = true;
                                }
                                if (!res.TravelPolicyResult.IsNegotiatedFare) {
                                    isOnNegotiableFare = false;
                                }
                                else {
                                    isOnNegotiableFare = true;
                                }
                            }
                        <%}%>
                            var airlineName;
                            for (var j in airlines) {
                                if (airlines[j].AirlineCode == res.Airline) {
                                    airlineName = airlines[j].AirlineName;//Assign Airline Name
                                    break;
                                }
                            }
                            var stop;
                            if (onSeg.Stops == 0) {
                                stop = "NONSTOP";
                            }
                            else if (onSeg.Stops == 1) {
                                stop = "ONESTOP";
                            }
                            else {
                                stop = "TWOSTOPS";
                            }

                            for (var f in res.FareBreakdown) {
                                onDiscountVal += res.FareBreakdown[f].AgentDiscount;
                            }

                            if (onSource == 'TBOAir') {//If Source is TBOAir then do price Ceiling
                                $('#onprice').html("<div class='collapse pub " + showPub + "'>Pub Fare : <span class='currency'>" + currency + "</span> " + Math.ceil(eval(eval(res.TotalFare) + eval(onDiscountVal))).toFixed(decimalValue) + "</div><br/>Offer Fare : <span class='currency'>" + currency + "</span> " + Math.ceil(res.TotalFare).toFixed(decimalValue));
                            } else {
                                $('#onprice').html("<div class='collapse pub " + showPub + "'>Pub Fare : <span class='currency'>" + currency + "</span> " + eval(eval(res.TotalFare) + eval(onDiscountVal)).toFixed(decimalValue) + "</div><br/>Offer Fare : <span class='currency'>" + currency + "</span> " + res.TotalFare.toFixed(decimalValue));
                            }


                            var depTime = new Date(onSeg.DepartureTime);
                            var arrTime = new Date(retSeg.ArrivalTime);

                            var dhours = depTime.getHours();
                            var dmins = depTime.getMinutes();
                            if (depTime.getHours().toString().length == 1) {
                                dhours = '0' + depTime.getHours();
                            }
                            if (depTime.getMinutes().toString().length == 1) {
                                dmins = '0' + depTime.getMinutes();
                            }
                            var ahours = arrTime.getHours();
                            var amins = arrTime.getMinutes();
                            if (arrTime.getHours().toString().length == 1) {
                                ahours = '0' + arrTime.getHours();
                            }
                            if (arrTime.getMinutes().toString().length == 1) {
                                amins = '0' + arrTime.getMinutes();
                            }
                            $('#onlogo').attr("src", "images/AirlineLogo/" + onSeg.Airline + ".gif");
                            $('#onairline').html(airlineName + " <span>" + onSeg.Airline + " " + onSeg.FlightNumber + "</span>");
                            $('#ondtime').html(dhours + ":" + dmins + "<em>" + (depTime.getHours() > 12 ? "PM" : "AM") + "</em>");
                            $('#onatime').html(ahours + ":" + amins + "<em>" + (arrTime.getHours() > 12 ? "PM" : "AM") + "</em>");
                            $('#ondaport').html(onSeg.Origin.AirportCode + ",<span class='city'>" + onSeg.Origin.CityName + "</span>");
                            $('#onaaport').html(retSeg.Destination.AirportCode + ",<span class='city'>" + retSeg.Destination.CityName + "</span>");
                            if (res.FareType != null) {//FOR UAPI THERE WILL BE NO FARE TYPE
                                $('#onstop').html(stop + "<br/>" + (res.FareType != null ? res.FareType : ""));
                            } else {
                                $('#onstop').html(stop + "<br/>");
                            }

                            
                        }
                    }
                }
                else {

                    //Start Return=========Show dynamic Flight Details Div Based on selection=========
                    if (index != null) {
                        if (document.getElementById('divRFD' + index) != null) {
                            var parentId = document.getElementById('divRFD' + index).parentNode.parentNode.id;
                            if (document.getElementById(parentId) != null) {
                                $('div.returnFD', '#' + parentId).hide();
                                $('#divRFD' + index).show();

                            }
                        }
                    }
                    //End=========Show dynamic Flight Details Div Based on selection=========

                    //Return Result selection logic
                    var resultId = parseInt(eval($('#hdnRetResultId' + index).val()));
                    retSelResultId = resultId;
                    retDiscountVal = 0;
                    retSelCounter = index;
                    retFareType = $('#hdnRetFareType' + index).val();
                    retSource = $('#hdnRetSource' + index).val();
                    for (var i in returnResultJson) {
                        if (returnResultJson[i].ResultId == resultId) {
                            var res = returnResultJson[i];
                            retAirline = res.Airline;
                            var retpriceVal = parseFloat($('#hdnRetPrice' + index).val());
                            $('#hdnRetPrice').val(retpriceVal.toFixed(2));
                            var onSeg = res.Flights[0][0];
                            var retSeg = res.Flights[0][res.Flights[0].length - 1];
                            var airlineName;
                            for (var j in airlines) {
                                if (airlines[j].AirlineCode == res.Airline) {
                                    airlineName = airlines[j].AirlineName;
                                    break;
                                }
                            }
                            var stop;
                            if (onSeg.Stops == 0) {
                                stop = "NONSTOP";
                            }
                            else if (onSeg.Stops == 1) {
                                stop = "ONESTOP";
                            }
                            else {
                                stop = "TWOSTOPS";
                            }

                        <%if (Settings.LoginInfo.IsCorporate == "Y")  {%> 
                            if (res.TravelPolicyResult != null) {
                                if (res.TravelPolicyResult.PolicyBreakingRules != null) {
                                    if (res.TravelPolicyResult.PolicyBreakingRules.TOTALBOOKINGAMOUNT != null) {
                                        retCorpBookingAmt = eval(res.TravelPolicyResult.PolicyBreakingRules.TOTALBOOKINGAMOUNT[0].split(':')[1]);
                                    }
                                    isRetInsidePolicy = res.TravelPolicyResult.IsUnderPolicy;
                                    $('#retpolicy').html("Inside Policy: " + isRetInsidePolicy);

                                    for (var key in res.TravelPolicyResult.PolicyBreakingRules) {
                                        for (var value in res.TravelPolicyResult.PolicyBreakingRules[key]) {
                                            if (key != "PREFERREDAIRLINES" && key != "RESTRICTBOOKING" && key != "AVOIDTRANSITTIME" && key != "COMPLETEBOOKING") {
                                                if (policyRules.length > 0 && !policyRules.split(',').includes(res.TravelPolicyResult.PolicyBreakingRules[key][value]))
                                                    policyRules += ',' + res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                                else if (!policyRules.split(',').includes(res.TravelPolicyResult.PolicyBreakingRules[key][value]))
                                                    policyRules += res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                            }
                                            else if (key == "PREFERREDAIRLINES") {
                                                if (prefAirlines.length > 0)
                                                    prefAirlines += ', ' + res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                                else
                                                    prefAirlines = res.TravelPolicyResult.PolicyBreakingRules[key][value];
                                            }
                                            else if (key == "RESTRICTBOOKING") {
                                                if (res.TravelPolicyResult.PolicyBreakingRules[key][value] == "Y")
                                                    restrictBooking = true;
                                            }
                                        }
                                    }
                                }

                                if (!res.TravelPolicyResult.IsPreferredAirline) {
                                    isRetPrefAirline = false;
                                }
                                else {
                                    isRetPrefAirline = true;
                                }
                                if (!res.TravelPolicyResult.IsNegotiatedFare) {
                                    isRetNegotiableFare = false;
                                }
                                else {
                                    isRetNegotiableFare = true;
                                }
                            }

                        <%}%>
                            for (var f in res.FareBreakdown) {
                                retDiscountVal += res.FareBreakdown[f].AgentDiscount;
                            }

                            if (retSource == 'TBOAir') {
                                $('#retprice').html("<div id='divPubFare' class='collapse pub " + showPub + "'>Pub Fare : <span class='currency'>" + currency + "</span> " + Math.ceil(eval(eval(res.TotalFare) + eval(retDiscountVal))).toFixed(decimalValue) + "</div><br/>Offer Fare : <span class='currency'>" + currency + "</span> " + Math.ceil(res.TotalFare).toFixed(decimalValue));
                            }
                            else {
                                $('#retprice').html("<div class='collapse pub " + showPub + "'>Pub Fare : <span class='currency'>" + currency + "</span> " + eval(res.TotalFare + retDiscountVal).toFixed(decimalValue) + "</div><br/>Offer Fare : <span class='currency'>" + currency + "</span> " + res.TotalFare.toFixed(decimalValue));
                            }

                            var depTime = new Date(onSeg.DepartureTime);
                            var arrTime = new Date(retSeg.ArrivalTime);

                            var dhours = depTime.getHours();
                            var dmins = depTime.getMinutes();
                            if (depTime.getHours().toString().length == 1) {
                                dhours = '0' + depTime.getHours();
                            }
                            if (depTime.getMinutes().toString().length == 1) {
                                dmins = '0' + depTime.getMinutes();
                            }
                            var ahours = arrTime.getHours();
                            var amins = arrTime.getMinutes();
                            if (arrTime.getHours().toString().length == 1) {
                                ahours = '0' + arrTime.getHours();
                            }
                            if (arrTime.getMinutes().toString().length == 1) {
                                amins = '0' + arrTime.getMinutes();
                            }

                            $('#retlogo').attr("src", "images/AirlineLogo/" + onSeg.Airline + ".gif");
                            $('#retairline').html(airlineName + " <span>" + onSeg.Airline + " " + onSeg.FlightNumber + "</span>");
                            $('#retdtime').html(dhours + ":" + dmins + "<em>" + (depTime.getHours() > 12 ? "PM" : "AM") + "</em>");
                            $('#retatime').html(ahours + ":" + amins + "<em>" + (arrTime.getHours() > 12 ? "PM" : "AM") + "</em>");
                            $('#retdaport').html(onSeg.Origin.AirportCode + ",<span class='city'>" + onSeg.Origin.CityName + "</span>");
                            $('#retaaport').html(retSeg.Destination.AirportCode + ",<span class='city'>" + retSeg.Destination.CityName + "</span>");
                            $('#retstop').html(stop + "<br/>" + (res.FareType != null ? res.FareType : ""));                            
                        }
                    }
                }

                //Display the Fare if both onward results and return results are there
            <%if (onwardResults.Count > 0 && returnResults.Count > 0) {%>
                if (onSource == retSource && onSource == 'TBOAir') {//If Source is TBOAir then do Price Ceiling
                    $('#price').html("<span class='currency'>" + currency + "</span> <br/><div id='divPubFare' class='collapse pub " + showPub + "'>Pub Fare :" + Math.ceil(eval(eval($('#hdnOnPrice').val()) + eval($('#hdnRetPrice').val()) + eval(onDiscountVal) + eval(retDiscountVal))).toFixed(decimalValue) + "</div><br/> Offer Fare : " + Math.ceil(eval(eval($('#hdnOnPrice').val()) + eval($('#hdnRetPrice').val()))).toFixed(decimalValue));
                }
                else {
                    $('#price').html("<span class='currency'>" + currency + "</span> <br/><div class='collapse pub " + showPub + "'>Pub Fare :" + parseFloat(eval(eval($('#hdnOnPrice').val()) + eval($('#hdnRetPrice').val()) + eval(onDiscountVal) + eval(retDiscountVal))).toFixed(decimalValue) + "</div><br/> Offer Fare : " + parseFloat(eval(eval($('#hdnOnPrice').val()) + eval($('#hdnRetPrice').val()))).toFixed(decimalValue));
                }
            <%}%>
                var insidePolicy = true;
            <%if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0)
        {%>             
                if (onCorpBookingAmt > 0 || retCorpBookingAmt > 0) {
                    totalBkgAmt = parseFloat(eval($('#hdnOnPrice').val()).toFixed(decimalValue));
                    if (retCorpBookingAmt > 0)
                        totalBkgAmt += parseFloat(eval($('#hdnRetPrice').val()).toFixed(decimalValue));
                    
                    console.log('Corp Booking Amt: ' + onCorpBookingAmt);
                    console.log('Total Amt: ' + totalBkgAmt);
                    if (onSource == retSource && onSource == 'TBOAir') {
                        if (Math.ceil(onCorpBookingAmt) >= Math.ceil(totalBkgAmt)) {
                            insidePolicy = true;
                        }
                        else {
                            insidePolicy = false;
                        }
                    }
                    else {
                        if (eval(onCorpBookingAmt) >= eval(totalBkgAmt)) {
                            insidePolicy = true;
                        }
                        else {
                            insidePolicy = false;
                        }
                    }

                    if (insidePolicy == true) {
                        if (!isOnInsidePolicy || !isRetInsidePolicy)
                            insidePolicy = false;
                    }

                    SetCorpPolicy(eval(insidePolicy));
                }
                else {
                    if (!isOnInsidePolicy || !isRetInsidePolicy)
                        SetCorpPolicy(false);
                    else if (isOnInsidePolicy || isRetInsidePolicy)
                        SetCorpPolicy(true);
                }

                if (restrictBooking) {
                    if (!insidePolicy || !isOnInsidePolicy || !isRetInsidePolicy)
                        $('#bookNowBtn').hide();
                    else
                        $('#bookNowBtn').show();
                }

                var DivPrefAirline = $('#DivPrefAirline');
                var DivNegotiatedFare = $('#DivNegotiatedFare');
                if (isOnPrefAirline && isRetPrefAirline) {
                    var ImgPreferredAirline = $('#ImgPreferredAirline');
                    ImgPreferredAirline.attr('src', 'images/icon-preferred-flight.png');
                    ImgPreferredAirline.attr('alt', 'Preferred Airline');
                    ImgPreferredAirline.attr('title', 'Preferred Airline');
                    DivPrefAirline.attr('title', prefAirlines);
                    DivPrefAirline.show();
                }
                else {
                    var ImgPreferredAirline = $('#ImgPreferredAirline');
                    ImgPreferredAirline.attr('src', 'images/icon-not-preferred-flight.png');
                    ImgPreferredAirline.attr('alt', 'Non-Preferred Airline');
                    ImgPreferredAirline.attr('title', 'Non-Preferred Airline');
                    DivPrefAirline.attr('title', prefAirlines);
                    DivPrefAirline.show();
                }

                if (isOnNegotiableFare && isRetNegotiableFare) {
                    DivNegotiatedFare.show();
                }
                else {
                    DivNegotiatedFare.hide();
                }
            <%}%>
            }
            catch (e) {
                console.log(e);
            }
        }

        function SetCorpPolicy(inside) {
            if (inside) {
                $('#ImgInOutPolicy').attr('src', 'images/icon-inside-policy.png');
                $('#ImgInOutPolicy').attr('alt', 'Inside Policy');
                $('#spnInOutPolicy').html('Inside Policy');
                $('#DivInOut').show();
            }
            else {
                $('#ImgInOutPolicy').attr('src', 'images/icon-outside-policy.png');
                $('#ImgInOutPolicy').attr('alt', 'Outside Policy');
                $('#spnInOutPolicy').html('Outside Policy - ' + policyRules);
                $('#DivInOut').show();                
            }
        }
        
        //This method will be called when we click Book Now button
        function BookNow() {
            <%if (request.Type == CT.BookingEngine.SearchType.OneWay)
        {%> 
            if (parseInt(eval(onSelResultId)) >= 0) {
                SendBook();
            }
            else {
                alert('Please select any flight to continue Booking.');
                return false;
            }
            <%}
        else
        {%>
            if (parseInt(eval(onSelResultId)) >= 0 && parseInt(eval(retSelResultId)) >= 0) {

                //6E--SPECIAL ROUND TRIP FARE
                //SG--Special Return Trip
                //Both the fare types should match then only allow for booking.
                if (onFareType == "SPECIAL ROUND TRIP FARE" || onFareType == "Special Return Trip" || retFareType == "SPECIAL ROUND TRIP FARE" || retFareType == "Special Return Trip") {
                    if (onFareType != retFareType) {
                        alert('The fare types should be SPECIAL ROUND TRIP FARE or Special Return Trip Fare');
                        return false;
                    }
                    //1.If both onward and return sources are same then allow booking
                    //SGCORP != SG(INVALID)
                    //SGCORP == SGCORP (VALID)
                    //6ECORP == 6ECORP(VALID)
                    //SG ==SG(VALID)
                    //6E==6E(VALID)
                    //6ECORP != 6E(INVALID)
                    //else if (onSource != retSource) {
                    //    alert('The fare types should be SPECIAL ROUND TRIP FARE or Special Return Trip Fare of same supplier');
                    //    return false;
                    //}
                    else {
                        SendBook();
                    }
                }
                else {
                    SendBook();
                }
            }
            else {
                alert('Please select Onward and Return flights to continue Booking.');
                return false;
            }
        <%}%>
        }
        var preApprovalRequired = true;
        <%if (Settings.LoginInfo.IsCorporate.ToLower() == "y")
        {%> 
        <%if (liappConfigs.Exists(x => x.AppKey.ToLower() == "preapprovalforcorp" && x.AppValue.ToLower() == "false"))
        { %>
        preApprovalRequired = false;
        <%}
        }%>
        function SendBook() {
            
            if ("<%=Settings.LoginInfo.IsCorporate%>"=="Y" && eval(<%=request.CorporateTravelProfileId%>) > 0) {
                if ($('#ImgInOutPolicy').attr('alt') == 'Outside Policy') {
                    var onResId = 0, retResId = 0;
                    if ($('#hdnCorp' + onSelResultId) != null)
                        onResId = onSelResultId;

                    if ($('#hdnCorp' + retSelResultId) != null)
                        retResId = retSelResultId;

                    Book(onResId, retResId);
                }
                else if (preApprovalRequired) {
                    var passData = "id=" + onSelResultId + "," + retSelResultId + "&flight=combi&sendemail=yes";
                    console.log(passData);
                    var url = "CorpEmailAjax?";
                    $('#bookNowBtn').attr('href', url + passData);
                    $('#mainContentDiv').hide();
                    $('#PreLoader').show();
                    //window.location.href = url + passData;
                }
                else {
                    var passData = "id=" + onSelResultId + "," + retSelResultId;
                    console.log(passData);
                    AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PageParams', 'sessionData':'" + onSelResultId + "," + retSelResultId + "', 'action':'set'}");
                    var url = "PassengerDetailsBySegments";
                    $('#bookNowBtn').attr('href', url);
                    $('#mainContentDiv').hide();
                    $('#PreLoader').show();
                    //window.location.href = url + passData;
                }
            }
            else {
                AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PageParams', 'sessionData':'" + onSelResultId + "," + retSelResultId + "', 'action':'set'}");
                var passData = "id=" + onSelResultId + "," + retSelResultId;
                console.log(passData);
                var url = "PassengerDetailsBySegments";
                $('#bookNowBtn').attr('href', url);
                $('#mainContentDiv').hide();
                $('#PreLoader').show();
                //window.location.href = url + passData;
            }
        }
                var Ajax;
        if (window.XMLHttpRequest) {
            Ajax = new XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var ID;
        function Book(onwardResId, returnResId) {
            var outsidePolicy = false;            

            onSelResultId = onwardResId;
            retSelResultId = returnResId;

            if (eval(returnResId) > 0  && $('#ImgInOutPolicy').attr('alt') == $('#ImgInOutPolicy').attr('alt') && $('#ImgInOutPolicy').attr('alt') == 'Outside Policy') {
                outsidePolicy = true;
            }
            else if ($('#ImgInOutPolicy').attr('alt') == 'Outside Policy') {
                outsidePolicy = true;
            }
            var passData = "id=" + onwardResId + ',' + returnResId + '&flight=combi&outsidePolicy=' + outsidePolicy;
            if (eval(returnResId) == 0)
                passData = "id=" + onwardResId + '&flight=combi&outsidePolicy=' + outsidePolicy;

            Ajax.onreadystatechange = ShowTravelReason
            Ajax.open("POST", "CorpProfileBookingAjax");
            Ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            Ajax.send(passData);
        }

        function ShowTravelReason() {
            
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById('divTravelReason').style.display = 'block';
                        document.getElementById('divTravelReason').innerHTML = Ajax.responseText;
                    }
                    else {
                        SendForApproval();
                    }
                }
            }
        }

        function SendForApproval() {
            var passData = "id=" + onSelResultId + "," + retSelResultId + "&flight=combi&sendemail=yes&trid=" + $('#ddlTravelReason').val();
            if (retSelResultId != '' && eval(retSelResultId) == 0 || retSelResultId == '')
                passData = "id=" + onSelResultId + "&flight=combi&sendemail=yes&trid=" + $('#ddlTravelReason').val();
            
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PageParams', 'sessionData':'" + onSelResultId + "," + retSelResultId + ","+$('#ddlTravelReason').val().split('~')[0]+"', 'action':'set'}");
            var url = "PassengerDetailsBySegments";
            console.log(passData);
            if (preApprovalRequired)
                url = "CorpEmailAjax?" + passData;

            window.location.href = url ;
            
            $('#mainContentDiv').hide();
            $('#divMsg').html('We are sending your Itinerary for Approval');
            $('#PreLoader').show();
        }

        function ShowPopup() {
            document.getElementById('divTravelPopup').style.display = 'block';
        }


        function HidePopup() {
            document.getElementById('divTravelPopup').style.display = 'none';
            document.getElementById("errorReason").style.display = "none";
        }


        function HideTravelReason() {
            document.getElementById('divTravelReason').style.display = 'none';
            document.getElementById("errorReason").style.display = "none";            
        }

        function BookFlight() {
            if (document.getElementById("ddlTravelReason").value != "-1") {
                document.getElementById("errorReason").style.display = "none";               
                SendForApproval();
            }
            else {
                document.getElementById("errorReason").style.display = "block";
            }
        }
    </script>
    <!-- Slider Script -->
    <script type="text/javascript">
        $(function() {

            $("#slider-range-onward").slider({
            range: true,
                min: parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value),
                max: parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value),
                step: 1,
                values: [parseFloat(document.getElementById('<%=hdnOnPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnOnPriceMax.ClientID %>').value)],
                slide: function(event, ui) {

                    if (ui.values[0] <= ui.values[1]) {
                        $("#onwardamount").val(ui.values[0] + "-" + ui.values[1]);
                        document.getElementById('<%=hdnOnPriceMin.ClientID %>').value = ui.values[0];
                        document.getElementById('<%=hdnOnPriceMax.ClientID %>').value = ui.values[1];
                    }
                    else {
                        $("#onwardamount").val(parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value) + "-" + parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value));
                        document.getElementById('<%=hdnOnPriceMin.ClientID %>').value = parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value);
                        document.getElementById('<%=hdnOnPriceMax.ClientID %>').value = parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value);
                    }
                }
            });
            $("#onwardamount").val($("#slider-range-onward").slider("values", 0) + "-" + $("#slider-range-onward").slider("values", 1));

        });

        $(function() {
         $("#slider-range-return").slider({
               range: true,
                min: parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value),
                max: parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value),
                step: 1,
                values: [parseFloat(document.getElementById('<%=hdnRetPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnRetPriceMax.ClientID %>').value)],
                slide: function(event, ui) {

                    if (ui.values[0] <= ui.values[1]) {
                        $("#returnamount").val(ui.values[0] + "-" + ui.values[1]);
                        document.getElementById('<%=hdnRetPriceMin.ClientID %>').value = ui.values[0];
                        document.getElementById('<%=hdnRetPriceMax.ClientID %>').value = ui.values[1];
                    }
                    else {
                        $("#returnamount").val(parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value) + "-" + parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value));
                        document.getElementById('<%=hdnRetPriceMin.ClientID %>').value = parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value);
                        document.getElementById('<%=hdnRetPriceMax.ClientID %>').value = parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value);
                    }
                }
            });
            $("#returnamount").val($("#slider-range-return").slider("values", 0) + "-" + $("#slider-range-return").slider("values", 1));

        });

        $(function() {
            $("#slider-range-onward").slider({
                change: function() {
                    var duration = document.getElementById('onwardamount').value;
                    var min = duration.split('-')[0];
                    var max = duration.split('-')[1];                   

                    document.getElementById('<%=hdnOnPriceMin.ClientID %>').value = min;
                    document.getElementById('<%=hdnOnPriceMax.ClientID %>').value = max;                   
                    
                    FilterByType('Price', 'On');
                    
                }
            });

            $("#slider-range-return").slider({
                change: function() {
                    var duration = document.getElementById('returnamount').value;
                    var min = duration.split('-')[0];
                    var max = duration.split('-')[1];                   

                    document.getElementById('<%=hdnRetPriceMin.ClientID %>').value = min;
                    document.getElementById('<%=hdnRetPriceMax.ClientID %>').value = max;
                   
                    FilterByType('Price', 'Ret');
                    
                }
            });
        });
    </script>
    <!-- Modify Search Script -->
    <script type="text/javascript" language="javascript">
        function disablefield() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if ('<%=userType %>' == 'ADMIN') {

                if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true) {
                    document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "Checked";
                    document.getElementById('wrapper1').style.display = "block";
                    if (document.getElementById('divLocations') != null)
                        document.getElementById('divLocations').style.display = "block";
                }
                else {
                    if (document.getElementById('wrapper1') != null)
                        document.getElementById('wrapper1').style.display = "none";

                    document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "UnChecked";
                    if (document.getElementById('divLocations') != null)
                        document.getElementById('divLocations').style.display = "none";
                }
            }

            if (document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                document.getElementById('tblNormal').style.display = 'block';
                document.getElementById('tblMultiCity').style.display = 'none';
                document.getElementById('textbox_A3').style.visibility = "visible";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "return";

                // document.getElementById('tblGrid').style.display='none';
                document.getElementById('tblMultiCity').style.display = 'none';
                if (document.getElementById('<%=hdnSourceBinding.ClientID%>').value == 'false') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            checkbox[i].checked = true;
                            checkbox[i].disabled = false;
                            document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                            document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                        }
                    }
                }
            }
            else if (document.getElementById('<%=oneway.ClientID %>').checked == true) {
                document.getElementById('tblNormal').style.display = 'block';
                document.getElementById('tblMultiCity').style.display = 'none';
                document.getElementById('textbox_A3').style.visibility = "hidden";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "oneway";
                // document.getElementById('tblGrid').style.display='none';
                document.getElementById('tblMultiCity').style.display = 'none';
                if (document.getElementById('<%=hdnSourceBinding.ClientID%>').value == 'false') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            checkbox[i].checked = true;
                            checkbox[i].disabled = false;
                            document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                            document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                        }
                    }
                }
            }
            else if (document.getElementById('<%= multicity.ClientID%>').checked == true) {
                document.getElementById('tblNormal').style.display = 'none';
                document.getElementById('tblMultiCity').style.display = 'block';
                // document.getElementById('tblGrid').style.display='block';
                document.getElementById('<%=hdnWayType.ClientID %>').value = "multiway";
                var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");

                var reqType = document.getElementById("<%=hdnWayType.ClientID %>");
                if (reqType.value == 'multiway') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            var source = checkbox[i].nextSibling.innerHTML;
                            if (source == "G9" || source == "FZ" || source == "SG" || source == "6E" || source == "IX" ) {
                                checkbox[i].checked = false;
                                checkbox[i].disabled = true;
                                document.getElementById("<%=chkFlightAll.ClientID %>").checked = false;
                                document.getElementById("<%=chkFlightAll.ClientID %>").disabled = true;
                            }
                        }
                    }
                }
            }
            document.getElementById('<%=hdnSourceBinding.ClientID%>').value = 'false'
        }

        var Ajax;
    var loc;
    var product = 1;
        function LoadAgentLocations() {

            if (document.getElementById('<%=ddlAirAgents.ClientID %>').value != "-1") {
                var location = 'ctl00_cphTransaction_ddlAirAgentsLocations';

                loc = location;
                var sel = document.getElementById('<%=ddlAirAgents.ClientID %>').value;

                var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + sel + '&id=' + location;;
                var url = "CityAjax";

                if (window.XMLHttpRequest) {
                    Ajax = new XMLHttpRequest();
                }
                else {
                    Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                }
                Ajax.onreadystatechange = BindLocationsList;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }
            else {
                $("#ctl00_cphTransaction_ddlAirAgentsLocations").empty();
            }
        }
        function BindLocationsList() {
            var ddl = document.getElementById(loc);
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {

                if (ddl != null) {
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);
                    //routing agent Id|location name#location id
                    var values = Ajax.responseText.split('#')[1].split(',');
                    for (var i = 0; i < values.length; i++) {
                        var opt = values[i];
                        if (opt.length > 0 && opt.indexOf('|') > 0) {
                            var el = document.createElement("option");
                            el.textContent = opt.split('|')[0];
                            el.value = opt.split('|')[1];
                            ddl.appendChild(el);
                        }
                    }

                   <%if (Settings.LoginInfo.OnBehalfAgentLocation > 0)
                    {%>
                        $('#' + ddl.id).select2('val', '<%=Settings.LoginInfo.OnBehalfAgentLocation.ToString()%>');
                    <%}else{%>
                        $('#' + ddl.id).select2('val', '-1');//Select Location                
                    <%} %>
                       
                }
                else {//Clear previous agent locations if no locations found for the current agent
                    if (ddl != null) {
                        ddl.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Location";
                        el.value = "-1";
                        ddl.add(el, 0);
                        $('#' + ddl.id).select2('val', '-1');//Select Location
                    }
                }
            }
        }

        var call1;
        var call2;
        //-Flight Calender control
        function init1() {
            var today = new Date();
            // Rendering Cal1
            call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
            call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            call1.cfg.setProperty("title", "Select your desired departure date:");
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setFlightDate1);
            call1.render();
            // Rendering Cal2
            call2 = new YAHOO.widget.CalendarGroup("call2", "fcontainer2");
            //            call2.cfg.setProperty("title", "Select your desired return date:");
            call2.selectEvent.subscribe(setFlightDate2);
            call2.cfg.setProperty("close", true);
            call2.render();
        }

        function showFlightCalendar1() {
            call2.hide();
            document.getElementById('fcontainer1').style.display = "block";
            document.getElementById('fcontainer2').style.display = "none";

            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            if (date1.length > 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                call1.cfg.setProperty("selected", eval(depDateArray[1]) + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                //call1.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
            }
            call1.render();
        }

        function showFlightCalendar2() {
            call1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            var date2 = document.getElementById('<%=ReturnDateTxt.ClientID %>').value;
            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                var retDateArray = date2.split('/');
                var arrMinDate = new Date(depDateArray[2], eval(depDateArray[1]) - 1, depDateArray[0]);
                if (document.getElementById('<%=ReturnDateTxt.ClientID %>').value.length <= 0) {
                    call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() ) + "/" + depDateArray[2]);
                    call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                } else {
                    call2.cfg.setProperty("selected", eval(retDateArray[1]) + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                    //call2.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate()) + "/" + depDateArray[2]);
                }
                call2.render();
            }
            document.getElementById('fcontainer2').style.display = "block";
        }

        function setFlightDate1() {
            var date1 = call1.getSelectedDates()[0];
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=DepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            call1.hide();
            //when we select date on Deperturedate,returndate calender shold be opened default in Modify Search
            if (document.getElementById('<%=roundtrip.ClientID %>') != null && document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                showFlightCalendar2();
            }
        }

        function setFlightDate2() {
            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select departure date.";
                return false;
            }
            var date2 = call2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date1 + ")";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=ReturnDateTxt.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            call2.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init1);



        function autoCompInitPrefAirline() {
            oACDSPA = new YAHOO.widget.DS_JSFunction(getAirlines);
            // Instantiate third auto complete
            oAutoCompPA = new YAHOO.widget.AutoComplete('<%=txtPreferredAirline.ClientID %>', 'statescontainer4', oACDSPA);
            oAutoCompPA.prehighlightClassName = "yui-ac-prehighlight";
            oAutoCompPA.queryDelay = 0;
            oAutoCompPA.minQueryLength = 2;
            oAutoCompPA.useIFrame = true;
            oAutoCompPA.useShadow = true;

            oAutoCompPA.formatResult = function(oResultItem, sQuery) {
                document.getElementById('statescontainer4').style.display = "block";
                var toShow = oResultItem[1].split(',');
                var sMarkup = toShow[0] + ',' + toShow[1]; ;
                //var aMarkup = ["<li>", sMarkup, "</li>"]; 
                var aMarkup = [sMarkup];
                return (aMarkup.join(""));
            };
            oAutoCompPA.itemSelectEvent.subscribe(itemSelectHandlerPA);
        }
        var itemSelectHandlerPA = function(sType2, aArgs2) {

            YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
            var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
            var city = oMyAcInstance2[1].split(',');
            document.getElementById('<%=airlineCode.ClientID %>').value = city[0];
            document.getElementById('<%=airlineName.ClientID %>').value = city[1];
            document.getElementById('statescontainer4').style.display = "none";
            var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
            var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
        };
        YAHOO.util.Event.addListener(this, 'load', autoCompInitPrefAirline); //temporarily commented

        var AJAX;
        var arrayStates = new Array();
        function invokePage(url, passData) {
            if (window.XMLHttpRequest) {
                AJAX = new XMLHttpRequest();
            }
            else {
                AJAX = new ActiveXObject("Microsoft.XMLHTTP");
            }
            if (AJAX) {
                AJAX.open("POST", url, false);
                AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                AJAX.send(passData);
                return AJAX.responseText;
            }
            else {
                return false;
            }
        }
        function getAirlines(sQuery) {

            var paramList = 'searchKey=' + sQuery;
            paramList += '&requestSource=' + "PreferredAirline";
            var url = "CityAjax";
            var arrayStates = "";
            var faltoo = invokePage(url, paramList);
            arrayStates = faltoo.split('/');
            if (arrayStates[0] != "") {
                for (var i = 0; i < arrayStates.length; i++) {
                    arrayStates[i] = [arrayStates[i].split(',')[1], arrayStates[i]];
                }

                return arrayStates;
            }
            else return (false);
        }
        function FlightSearch() {
            if (CheckCorporateEntries() && CheckFlightCities() && checkFlightDates()) {
                if (eval(document.getElementById('<%=ddlAdults.ClientID %>').value) < eval(document.getElementById('<%=ddlInfants.ClientID %>').value)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Infant count should not be greater than adult count";
                    return false;
                } else if (document.getElementById('divPrefAirline0').style.display == 'block' && document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your first preferred airline";
                    document.getElementById('ctl00_cphTransaction_txtPreferredAirline').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline0').style.display == 'block' && document.getElementById('txtPreferredAirline0').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your second preferred airline";
                    document.getElementById('txtPreferredAirline0').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline1').style.display == 'block' && document.getElementById('txtPreferredAirline1').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your third preferred airline";
                    document.getElementById('txtPreferredAirline1').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline2').style.display == 'block' && document.getElementById('txtPreferredAirline2').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your fourth preferred airline";
                    document.getElementById('txtPreferredAirline2').focus();
                    return false;
                }
               else if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true && (document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value.length == 0 || document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "-1"))
                {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Location !";
                    return false;
                }
                else if ((eval(document.getElementById('<%=ddlAdults.ClientID %>').value) + eval(document.getElementById('<%=ddlInfants.ClientID %>').value) + eval(document.getElementById('<%=ddlChilds.ClientID %>').value)) > 9) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Passengers count should not be greater than 9 ";
                    return false;
                }
                else if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null) {
                    var travelReason = document.getElementById('<%=ddlFlightTravelReasons.ClientID %>');

                    if (travelReason.options[travelReason.selectedIndex].text == "Entitlement" && document.getElementById('ctl00_cphTransaction_DepDate').value.length > 0) {
                        var depVal = document.getElementById('ctl00_cphTransaction_DepDate').value;
                        var departure = depVal.split('/');
                        var depDate = new Date(departure[2], eval(departure[1]) - 1, departure[0]);
                        var today = new Date();
                        var validDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 30);
                        if (validDate > depDate) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please select Travel Date after 30 days from current date";
                            document.getElementById('ctl00_cphTransaction_DepDate').focus();
                            return false;
                        }
                    }
                }
                else if (!ValidateAirline(0)) {
                    return false;
                }
                else if (!ValidateAirline(1)) {
                    return false;
                }
                else if (!ValidateAirline(2)) {
                    return false;
                }
                if (flightSourceValidate()) {
                    document.getElementById('<%=hdnModifySearch.ClientID %>').value = "1"; //Modify Search purpose
                    $('#FlightPreLoader').show();
                    $('#mainContentDiv').hide();  

                    if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value != "-1") {
                        document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value;
                    }

                    //detecting all IE vesions of browsers
                    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                        document.getElementById('imgFlightLoading').innerHTML = document.getElementById('imgFlightLoading').innerHTML;
                    }
                    
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function CheckCorporateEntries() {
            if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null && document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').value == "-1") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Travel Reason";
                document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlFlightEmployee.ClientID %>') != null && document.getElementById('<%=ddlFlightEmployee.ClientID %>').value == "-1") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Traveller / Employee";
                document.getElementById('<%=ddlFlightEmployee.ClientID %>').focus();
                return false;
            }
            else {
                return true;
            }
        }
        
        function CheckFlightCities() {

            if (document.getElementById('<%=hdnWayType.ClientID %>').value != "multiway") {
                var origin = document.getElementById('<%=Origin.ClientID %>').value;
                var dest = document.getElementById('<%=Destination.ClientID %>').value;

                if (origin.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a Origin City";
                    return false;
                }
                else if (dest.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a Destination City";
                    return false;
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                    var arrOrigin = origin.split(',')[0].split(')')[1];
                    var arrDest = dest.split(',')[0].split(')')[1];
                    document.getElementById('flightOnwards').innerHTML = origin.substring(1, 4) + ' ' + '(' + arrOrigin + ')' + ' - ' + dest.substring(1, 4) + ' ' + '(' + arrDest + ')';
                    document.getElementById('flightReturn').innerHTML = dest.substring(1, 4) + ' ' + '(' + arrDest + ')' + ' - ' + origin.substring(1, 4) + ' ' + '(' + arrOrigin + ')';
                    if (document.getElementById('<%=hdnWayType.ClientID %>').value == "oneway") {
                        document.getElementById('twowaytable').style.display = 'none';
                    }
                    
                    return true;
                }
            }
            else {
                var city1 = document.getElementById('<%=City1.ClientID%>').value;
                var city2 = document.getElementById('<%=City2.ClientID%>').value;
                var city3 = document.getElementById('<%=City3.ClientID%>').value;
                var city4 = document.getElementById('<%=City4.ClientID%>').value;
                var city5 = document.getElementById('<%=City5.ClientID%>').value;
                var city6 = document.getElementById('<%=City6.ClientID%>').value;
                var city7 = document.getElementById('<%=City7.ClientID%>').value;
                var city8 = document.getElementById('<%=City8.ClientID%>').value;
                var city9 = document.getElementById('<%=City9.ClientID%>').value;
                var city10 = document.getElementById('<%=City10.ClientID%>').value;
                var city11 = document.getElementById('<%=City11.ClientID%>').value;
                var city12 = document.getElementById('<%=City12.ClientID%>').value;

                if (city1.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter first City";
                    document.getElementById('<%=City1.ClientID%>').focus();
                    return false;
                } else if (city2.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter second City";
                    document.getElementById('<%=City2.ClientID%>').focus();
                    return false;
                } else if (city3.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter third City";
                    document.getElementById('<%=City3.ClientID%>').focus();
                    return false;
                } else if (city4.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter fourth City";
                    document.getElementById('<%=City4.ClientID%>').focus();
                    return false;
                } else if (tblRow3.style.display == 'block' && city5.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 5th City";

                    document.getElementById('<%=City5.ClientID%>').focus();
                    return false;
                } else if (tblRow3.style.display == 'block' && city6.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 6th City";

                    document.getElementById('<%=City6.ClientID%>').focus();
                    return false;
                } else if (tblRow4.style.display == 'block' && city7.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 7th City";

                    document.getElementById('<%=City7.ClientID%>').focus();
                    return false;
                } else if (tblRow4.style.display == 'block' && city8.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 8th City";

                    document.getElementById('<%=City8.ClientID%>').focus();
                    return false;
                } else if (tblRow5.style.display == 'block' && city9.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 9th City";

                    document.getElementById('<%=City9.ClientID%>').focus();
                    return false;
                } else if (tblRow5.style.display == 'block' && city10.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 10th City";

                    document.getElementById('<%=City10.ClientID%>').focus();
                    return false;
                } else if (tblRow6.style.display == 'block' && city11.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 11th City";

                    document.getElementById('<%=City11.ClientID%>').focus();
                    return false;
                } else if (tblRow6.style.display == 'block' && city12.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 12th City";

                    document.getElementById('<%=City12.ClientID%>').focus();
                    return false;
                }
                else {
                    document.getElementById('errMulti').style.display = "none";

                    var success = true;
                    document.getElementById('onetwoDiv').style.display = 'none';
                    document.getElementById('multiDiv').style.display = 'block';
                    var arrCity1 = city1.split(',')[0].split(')')[1];
                    var arrCity2 = city2.split(',')[0].split(')')[1];
                    var arrCity3 = city3.split(',')[0].split(')')[1];
                    var arrCity4 = city4.split(',')[0].split(')')[1];
                    document.getElementById('multiway1').innerHTML = city1.substring(1, 4) + ' ' + '(' + arrCity1 + ')' + ' - ' + city2.substring(1, 4) + ' ' + '(' + arrCity2 + ')';
                    document.getElementById('multiway2').innerHTML = city3.substring(1, 4) + ' ' + '(' + arrCity3 + ')' + ' - ' + city4.substring(1, 4) + ' ' + '(' + arrCity4 + ')';
                    var tdIndex = 3;
                    for (var i = 5; i < 13; i++) {
                        if (document.getElementById('tblRow' + tdIndex).style.display == "block") {
                            var addlCity1 = document.getElementById('ctl00_cphTransaction_City' + i).value;
                            var addlCity2 = document.getElementById('ctl00_cphTransaction_City' + (i + 1)).value;
                            var arraddlCity1 = addlCity1.split(',')[0].split(')')[1];
                            var arraddlCity2 = addlCity2.split(',')[0].split(')')[1];

                            if (addlCity1.length <= 0 || addlCity2.length <= 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter all Cities";
                                success = false;
                                break;
                            }
                            document.getElementById('multiwayable' + tdIndex).style.display = "block";
                            document.getElementById('multiway' + tdIndex).innerHTML = addlCity1.substring(1, 4) + ' ' + '(' + arraddlCity1 + ')' + ' - ' + addlCity2.substring(1, 4) + ' ' + '(' + arraddlCity2 + ')';

                            tdIndex++;
                        }
                        i++;
                    }
                    //                    if (window.navigator.appName == "Netscape") {
                    //                        setTimeout('document.images.imgPreloader.src = "images/preloader11.gif"', 200);
                    //                    }

                    document.getElementById('imgPreloader').src = "images/preloaderFlight.gif";
                    return true;

                }
            }
        }
        function checkFlightDates() {

            if (document.getElementById('<%=hdnWayType.ClientID %>').value != "multiway") {
                var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
                var date2 = document.getElementById('<%=ReturnDateTxt.ClientID %>').value;
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                document.getElementById('deptDate').innerHTML = date1;
                document.getElementById('arrivalDate').innerHTML = date2;
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Departure Date";
                    return false;
                }
                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                // Removing same day validation
//                if (todaydate.getTime() > cInDate.getTime()) {
//                    document.getElementById('errMess').style.display = "block";
//                    document.getElementById('errorMessage').innerHTML = " Departure Date should be greater than equal to todays date";
//                    return false;
//                }
                if (document.getElementById('<%=roundtrip.ClientID%>').checked == true) {
                    if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Return Date";
                        return false;
                    }

                    var retDateArray = date2.split('/');

                    // checking if date2 is valid	
                    if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = " Invalid Return Date";
                        return false;
                    }
                    var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    if (todaydate.getTime() > cOutDate.getTime()) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Return Date should be greater than equal to todays date";
                        return false;
                    }

                    var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    var difference = returndate.getTime() - depdate.getTime();

                    if (difference < 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Departure date should be greater than or equal to Return date";
                        return false;
                    }
                }
                return true;
            }
            else if (document.getElementById('<%=hdnWayType.ClientID %>').value == "multiway") {

                var date1 = document.getElementById('ctl00_cphTransaction_Time1').value;
                var date2 = document.getElementById('ctl00_cphTransaction_Time2').value;
                var date3 = document.getElementById('ctl00_cphTransaction_Time3').value;
                var date4 = document.getElementById('ctl00_cphTransaction_Time4').value;
                var date5 = document.getElementById('ctl00_cphTransaction_Time5').value;
                var date6 = document.getElementById('ctl00_cphTransaction_Time6').value;

                if (date1.length == 0 || date2.length == 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                    if (date1.length == 0) {
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                    }
                    else {
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                    }
                    return false;
                }
                else if (date1.length > 0 && date2.length > 0) {
                    this.today = new Date();
                    var thisMonth = this.today.getMonth();
                    var thisDay = this.today.getDate();
                    var thisYear = this.today.getFullYear();

                    var todaydate = new Date(thisYear, thisMonth, thisDay);
                    if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                        return false;
                    }
                    var depDateArray = date1.split('/');

                    // checking if date1 is valid		    
                    if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                        return false;
                    }
                    var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    if (todaydate.getTime() > cInDate.getTime()) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Departure Date should be greater than equal to todays date";
                        return false;
                    }

                    if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Return Date";
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                        return false;
                    }

                    var retDateArray = date2.split('/');

                    // checking if date2 is valid	
                    if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                        return false;
                    }
                    var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    if (todaydate.getTime() > cOutDate.getTime()) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Return Date should be greater than equal to todays date";
                        return false;
                    }

                    var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    var difference = returndate.getTime() - depdate.getTime();

                    if (difference < 0) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Departure date should be greater than or equal to Return date";
                        return false;
                    }

                    if ((tblRow3.style.display == 'block' && date3.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time3').focus();
                        return false;
                    }
                    else if ((tblRow4.style.display == 'block' && date4.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time4').focus();
                        return false;
                    }
                    else if ((tblRow5.style.display == 'block' && date5.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time5').focus();
                        return false;
                    }
                    else if ((tblRow6.style.display == 'block' && date6.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time6').focus();
                        return false;
                    }
                    else if ((tblRow3.style.display == 'block' && date3.length > 0)) {
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        if (date3 != null && (date3 == "DD/MM/YYYY" || date3 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time3').focus();
                            return false;
                        }
                        var depDateArray = date3.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time3').focus();
                            return false;
                        }
                        //                       
                    }

                    else if ((tblRow4.style.display == 'block' && date4.length > 0)) {
                        if (date4 != null && (date4 == "DD/MM/YYYY" || date4 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time4').focus();
                            return false;
                        }
                        var retDateArray = date4.split('/');

                        // checking if date2 is valid	
                        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time4').focus();
                            return false;
                        }

                    }

                    else if ((tblRow5.style.display == 'block' && date5.length > 0)) {
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        if (date5 != null && (date5 == "DD/MM/YYYY" || date5 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time5').focus();
                            return false;
                        }
                        var depDateArray = date5.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time5').focus();
                            return false;
                        }

                    }

                    else if ((tblRow6.style.display == 'block' && date6.length > 0)) {
                        if (date6 != null && (date6 == "DD/MM/YYYY" || date6 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time6').focus();
                            return false;
                        }
                        var retDateArray = date6.split('/');

                        // checking if date2 is valid	
                        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time6').focus();
                            return false;
                        }

                    }

                    document.getElementById('multiDate1').innerHTML = date1;
                    document.getElementById('multiDate2').innerHTML = date2;
                    for (var k = 3; k <= 6; k++) {
                        if (document.getElementById('tblRow' + k).style.display == "block") {
                            document.getElementById('multiDate' + k).innerHTML = document.getElementById('ctl00_cphTransaction_Time' + k).value;
                        }
                    }
                }
                return true;
            }
        }
        function flightSourceValidate() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                var counter = 0;
                for (var i = 0; i < checkbox.length; i++) {
                    if (checkbox[i].checked) {
                        if (document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).nextSibling.innerHTML == "G8") {
                            if (eval(document.getElementById('<%=ddlAdults.ClientID %>').value) +  eval(document.getElementById('<%=ddlChilds.ClientID %>').value) + eval(document.getElementById('<%=ddlInfants.ClientID %>').value) > 6) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "GoAir Doesn't allow morethan 6 passengers";
                                return false;
                            }
                        }
                        counter++;
                    }
                }
                if (counter == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
                    return false;
                }
            }
            return true;
        }
        function setCheck(ctrl) {
            if (document.getElementById(ctrl).checked == false) {
                document.getElementById('ctl00_cphTransaction_chkSource0').checked = false;
            }
        }
        function setUncheck() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                for (var i = 0; i < checkbox.length; i++) {
                    if (!checkbox[i].checked) {
                        document.getElementById("<%=chkFlightAll.ClientID%>").checked = false;
                    }
                }
            }

        }
    </script>
    <!-- Multi City Script -->
    <script language="javascript">
        var cntr = 0;
        var rid = 3;

        //add a new row to the table
        function addRow() {
            document.getElementById('tblRow' + rid).style.display = 'block';
            if (rid < 6) {
                rid = rid + 1;
            }

            cntr = cntr + 1;
            //alert(cntr);
            if (cntr == 4) {
                document.getElementById('btnAddRow').style.display = 'none';
            }

        }

        //deletes the specified row from the table
        function removeRow(src) {

            document.getElementById('tblRow' + src).style.display = 'none';
            if (src == 3) {
                document.getElementById('ctl00_cphTransaction_Time3').value = "";
                document.getElementById('ctl00_cphTransaction_City5').value = "";
                document.getElementById('ctl00_cphTransaction_City6').value = "";
            }
            else if (src == 4) {
                document.getElementById('ctl00_cphTransaction_Time4').value = "";
                document.getElementById('ctl00_cphTransaction_City7').value = "";
                document.getElementById('ctl00_cphTransaction_City8').value = "";
            }
            else if (src == 5) {
                document.getElementById('ctl00_cphTransaction_Time5').value = "";
                document.getElementById('ctl00_cphTransaction_City9').value = "";
                document.getElementById('ctl00_cphTransaction_City10').value = "";
            }
            else {
                document.getElementById('ctl00_cphTransaction_Time6').value = "";
                document.getElementById('ctl00_cphTransaction_City11').value = "";
                document.getElementById('ctl00_cphTransaction_City12').value = "";
            }
            rid = src;
            cntr = cntr - 1;
            if (cntr < 4)
                document.getElementById('btnAddRow').style.display = 'block';



        }
        function selectFlightSources() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                if (document.getElementById('<%=chkFlightAll.ClientID %>') != null) {
                    if (document.getElementById('<%=chkFlightAll.ClientID %>').checked) {
                        for (i = 0; i < checkbox.length; i++) {
                            document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).checked = true;
                        }
                    }
                    else {
                        for (i = 0; i < checkbox.length; i++) {
                            document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).checked = false;
                        }
                    }
                }
            }
        }

        //Adding prefered Airline Added by brahmam 14.10.2016
        function AddPrefAirline() {

            if (document.getElementById('divPrefAirline0').style.display == "none") {
                document.getElementById('divPreferredAirline0').style.display = 'block';
                document.getElementById('divPrefAirline0').style.display = 'block';
                document.getElementById('removeRowLink0').style.display = 'block';
                document.getElementById('txtPreferredAirline0').value = '';
            } else if (document.getElementById('divPrefAirline1').style.display == "none") {
                document.getElementById('divPreferredAirline1').style.display = 'block';
                document.getElementById('divPrefAirline1').style.display = 'block';
                document.getElementById('removeRowLink1').style.display = 'block';
                document.getElementById('txtPreferredAirline1').value = '';
            }
            else if (document.getElementById('divPrefAirline2').style.display == "none") {
                document.getElementById('divPreferredAirline2').style.display = 'block';
                document.getElementById('divPrefAirline2').style.display = 'block';
                document.getElementById('removeRowLink2').style.display = 'block';
                document.getElementById('txtPreferredAirline2').value = '';
            }
            if (document.getElementById('divPrefAirline0').style.display == "block" && document.getElementById('divPrefAirline1').style.display == "block" && document.getElementById('divPrefAirline2').style.display == "block") {
                document.getElementById('addRowLink').style.display = 'none';
            }

        }
        //removing prefered Airline Added by brahmam 14.10.2016
        function RemovePrefAirline(index) {
            document.getElementById('divPreferredAirline' + index).style.display = 'none';
            document.getElementById('divPrefAirline' + index).style.display = 'none';
            document.getElementById('removeRowLink' + index).style.display = 'none';
            document.getElementById('addRowLink').style.display = 'block';
            var airlineCodes = document.getElementById('<%=airlineCode.ClientID %>').value.split(',');
            airlineCodes.splice((index + 1), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
            document.getElementById('<%=airlineCode.ClientID %>').value = arCodes;
        }

        //To Validate Duplicate Preferred Airline
        function ValidateAirline(id) {
            var isValid = true;
            var prefAir1 = document.getElementById('txtPreferredAirline' + id).value;
            if (document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value.trim().length > 0 && prefAir1.trim().length > 0) {
                if (document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value == prefAir1) {
                    errMess.style.display = 'block';
                    errorMessage.innerHTML = 'Duplicate Preferred Airline not allowed';
                    document.getElementById('txtPreferredAirline' + id).focus();
                    isValid = false;
                }
                else if (document.getElementById('txtPreferredAirline' + eval(id - 1)) != null && document.getElementById('txtPreferredAirline' + eval(id - 1)).value == prefAir1) {
                    errMess.style.display = 'block';
                    errorMessage.innerHTML = 'Duplicate Preferred Airline not allowed';
                    document.getElementById('txtPreferredAirline' + id).focus();
                    isValid = false;
                }
            }
            return isValid;
        }
        //To Remove input preferred airline code from airlineCode hidden field
        //when preferred airline cleared from textbox
        function RecheckAirline(index) {

            if (index == 0 && document.getElementById('<%=txtPreferredAirline.ClientID %>').value != '')
                return;

            if (index > 0 && document.getElementById('txtPreferredAirline' + (index - 1)) != null && document.getElementById('txtPreferredAirline' + (index - 1)).value != '')
                return;

            var airlineCodes = document.getElementById('<%=airlineCode.ClientID %>').value.split(',');
            airlineCodes.splice((index), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
            document.getElementById('<%=airlineCode.ClientID %>').value = arCodes;
        }
    </script>
    <!-- Filter Controls Selection Script -->
    <script>        
        function SelectUnSelect(control, type) {//type=On means Onward, type=Ret means Return control
            if (control == 'Airlines') {
                var counter = eval('<%=airlineList.Count%>');
                if (type == 'Ret') {
                    counter = eval('<%=airlineListReturn.Count%>');
                }
                var all = $('#chk' + type + 'AllAirlines').prop("checked");
                for (var i = 0; i < counter; i++) {
                    $('#chk' + type + 'Airline' + eval(i)).prop("checked", all);
                }

            }
            else if (control == 'Refund') {//Non Refundable & Refundable Checkbox selection
                var all = $('#chk' + type + 'AllRefund').prop("checked");
                $('#chk' + type + 'Refund').prop("checked", all);
                $('#chk' + type + 'NonRefund').prop("checked", all);
            }
            else if (control == 'Stops') {//Non Stop checkbox selection
                if (type == 'On') {
                    var all = $('#chkOnAllStops').prop("checked");
                    $('#<%=chkOnOneStop.ClientID%>').prop("checked", all);
                    $('#<%=chkOnNonStop.ClientID%>').prop("checked", all);
                    $('#<%=chkOnTwoStops.ClientID%>').prop("checked", all);
                }
                else {
                    var all = $('#chkRetAllStops').prop("checked");
                    $('#<%=chkRetOneStop.ClientID%>').prop("checked", all);
                    $('#<%=chkRetNonStop.ClientID%>').prop("checked", all);
                    $('#<%=chkRetTwoStops.ClientID%>').prop("checked", all);
                }
            }
            if ($('#lbtext').html() == "Hide Pub Fare") {
                showPub = 'in';
            }
            else {
                showPub = '';
            }
            FilterByType('Airline', type);
        }

        //This method is for Particular airline selection and calls Filter method
        function OnlyThis(index, journeyType) {
            document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            var count = eval('<%=airlineList.Count%>');
            if (journeyType == "Ret") {
                count = eval('<%=airlineListReturn.Count%>');
            }
            for (var i = 0; i < eval(count); i++) {
                if (document.getElementById('chk' + journeyType + 'Airline' + i) != null) {
                    if (i == index) {
                        document.getElementById('chk' + journeyType + 'Airline' + i).checked = true;
                    } else {
                        document.getElementById('chk' + journeyType + 'Airline' + i).checked = false;
                    }
                }
            }
            if ($('#lbtext').html() == "Hide Pub Fare") {
                showPub = 'in';
            }
            else {
                showPub = '';
            }
            
            document.getElementById('chk' + journeyType + 'AllAirlines').checked = false;
            FilterByType('Airline', journeyType);
             
        }

        //This method is called when click the timings buttons i.e. Morning, Noon etc
        var timings = "";
        function FilterTiming(timing, type) {//type=On means Onward, type=Ret means Return control
            <%if (!request.TimeIntervalSpecified) {%>
            if (timing == "Night") {
                $('#btn' + type + 'Night').addClass('btn-primary');
                $('#btn' + type + 'Morning').removeClass('btn-primary');
                $('#btn' + type + 'Evening').removeClass('btn-primary');
                $('#btn' + type + 'Noon').removeClass('btn-primary');
                timings = "night";
            }
            else if (timing == "Morning") {
                $('#btn' + type + 'Night').removeClass('btn-primary');
                $('#btn' + type + 'Morning').addClass('btn-primary');
                $('#btn' + type + 'Evening').removeClass('btn-primary');
                $('#btn' + type + 'Noon').removeClass('btn-primary');
                timings = "morning";
            }
            else if (timing == "Noon") {
                $('#btn' + type + 'Night').removeClass('btn-primary');
                $('#btn' + type + 'Morning').removeClass('btn-primary');
                $('#btn' + type + 'Evening').removeClass('btn-primary');
                $('#btn' + type + 'Noon').addClass('btn-primary');
                timings = "noon";
            }
            else if (timing == "Evening") {
                $('#btn' + type + 'Night').removeClass('btn-primary');
                $('#btn' + type + 'Morning').removeClass('btn-primary');
                $('#btn' + type + 'Evening').addClass('btn-primary');
                $('#btn' + type + 'Noon').removeClass('btn-primary');
                timings = "evening";
            }
            <%}else{%>
            timings = $("#ctl00_cphTransaction_ddl" + type + "Timings").val();
            <%}%>
            if ($('#lbtext').html() == "Hide Pub Fare") {
                showPub = 'in';
            }
            else {
                showPub = '';
            }
            FilterByType("timings", type);
        }
        
    </script>
    <!-- Sorting and Filtering Results Script -->
    <script>
        //For Sorting and Filtering RenderResults will be used to update the results html from ajax page
        //Parameters Info
        //1. journeyType=On (Onward) or Ret (Return)
        //2. sortType = price, dtime(departure time), atime(arrival time), aline(Airline), stops(Non Stop, One Stop etc)
        //3. order=asc (Ascending) or desc (Descending)
        var jtype;
        var sortData;
        function SortResults(journeyType, sortType, order) {
            jtype = journeyType;

            SortToDefault(journeyType, sortType);

            //Always sort after applying filters based on journeyType=On or Ret
            //Direct Ajax call not required here use the sortData value in the FilterResults(...)
            sortData = "type=" + journeyType + "-&sort=" + sortType + "&order=" + order;

            FilterByType('Airline', journeyType);


            //if (window.XMLHttpRequest) {
            //    Ajax = new XMLHttpRequest();
            //}
            //else {
            //    Ajax = new ActiveXObject('Microsoft.XMLHTTP');
            //}            

            //var url = "FlightResultAjax";
            //Ajax.onreadystatechange = RenderResults;
            //Ajax.open('POST', url);
            //Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            //Ajax.send(passData);

        }

        function GetActiveSortBy(journeyType)
        {
            if ($("#" + journeyType.toLowerCase() + "ddlDepSort option:selected").text() == "Shortest" || $("#" + journeyType.toLowerCase() + "ddlDepSort option:selected").text() == "Longest")
            {
                order = $("#" + journeyType.toLowerCase() + "ddlDepSort").val();
                sortData = "type=" + journeyType + "-&sort=" + "dtime" + "&order=" + order;
            }
            else if ($("#" + journeyType.toLowerCase() + "ddlArrSort option:selected").text() == "Shortest" || $("#" + journeyType.toLowerCase() + "ddlArrSort option:selected").text() == "Longest")
            {
                order = $("#" + journeyType.toLowerCase() + "ddlArrSort").val();
                sortData = "type=" + journeyType + "-&sort=" + "atime" + "&order=" + order;
            }
            else if ($("#" + journeyType.toLowerCase() + "ddlAirSort option:selected").text() == "Ascending" || $("#" + journeyType.toLowerCase() + "ddlAirSort option:selected").text() == "Descending")
            {
                order = $("#" + journeyType.toLowerCase() + "ddlAirSort").val();
                sortData = "type=" + journeyType + "-&sort=" + "aline" + "&order=" + order;
            }
            else if ($("#" + journeyType.toLowerCase() + "ddlStopSort option:selected").text() == "Direct" || $("#" + journeyType.toLowerCase() + "ddlStopSort option:selected").text() == "Non-Direct")
            {
                order = $("#" + journeyType.toLowerCase() + "ddlStopSort").val();
                sortData = "type=" + journeyType + "-&sort=" + "stops" + "&order=" + order;
            }
            else if ($("#" + journeyType.toLowerCase() + "ddlPriceSort option:selected").text() == "Lowest Price" || $("#" + journeyType.toLowerCase() + "ddlPriceSort option:selected").text() == "Highest Price")
            {
                order = $("#" + journeyType.toLowerCase() + "ddlPriceSort").val();
                sortData = "type=" + journeyType + "-&sort=" + "price" + "&order=" + order;
            }
        }

        function SortToDefault(journeyType, sortType) {

            if (sortType == 'stops') {

                $("#" + journeyType.toLowerCase() + "DepSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "AirSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "ArrSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "PriceSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("val", "ASC");
                //$("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("val", "Lowest Price");

            }
            if (sortType == 'aline') {
                $("#" + journeyType.toLowerCase() + "DepSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "StopSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "ArrSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "PriceSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("val", "ASC");
                //$("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("text", "Lowest Price");
            }
            if (sortType == 'atime') {

               $("#" + journeyType.toLowerCase() + "DepSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "StopSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "AirSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "PriceSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("val", "ASC");
                //$("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("text", "Lowest Price");
            }
            if (sortType == 'dtime')
            {
                $("#" + journeyType.toLowerCase() + "ArrSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "StopSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "AirSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "PriceSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("val", "ASC");
                //$("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("text", "Lowest Price");
            }
            if (sortType == 'price')
            {
                $("#" + journeyType.toLowerCase() + "ArrSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "StopSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "AirSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "DepSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").select2('val','-1');
            }
        }
        //ParameterInfo
        //1. filterType=Used for checking whether normal filter or clear filter applied. 
        //   If filterType has value means normal filter otherwise clear filter. filterTypeVal will be assigned with filterType for checking in RenderResults() based 
        //   on whether first result must be selected for the journey type will be decided.
        //2. journeyType=On (Onward) or Ret (Return)
        var filterTypeVal;
        //JourneyType = On or Ret, which means Onward or Return
        //We are not filterType param right now
        function FilterByType(filterType, journeyType) {

            var alines = '', fares = '', stops = '', price = '', chkCount = 0;
            filterTypeVal = filterType;

            GetActiveSortBy(journeyType);

            //document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            //-----------------------------Airlines filter logic--------------------------------
            var airlineCount = eval('<%=airlineList.Count%>');
            if (journeyType == "Ret") {
                airlineCount = eval('<%=airlineListReturn.Count%>');
            }

            for (var i = 0; i < airlineCount; i++) {
                if (document.getElementById('chk' + journeyType + 'Airline' + i).checked) {//If selected or checked then pass true otherwise pass false to FlightResultAjax page
                    if (alines.length > 0) {
                        alines += "," + $('#lbl' + journeyType + 'Airline' + i).text().split('(')[1].replace(')', '') + "-true";
                    }
                    else {
                        alines = $('#lbl' + journeyType + 'Airline' + i).text().split('(')[1].replace(')', '') + "-true";
                    }
                }
                else {
                    if (alines.length > 0) {
                        alines += "," + $('#lbl' + journeyType + 'Airline' + i).text().split('(')[1].replace(')', '') + "-false";
                    }
                    else {
                        alines = $('#lbl' + journeyType + 'Airline' + i).text().split('(')[1].replace(')', '') + "-false";
                    }
                }
            }
            //-----------------------------------End Airlines filter--------------------------------------------


            //----------------------------------Refundable and Non Refundable fare filter logic-----------------------
            if (document.getElementById('chk' + journeyType + 'Refund') != null && document.getElementById('chk' + journeyType + 'Refund').checked) {
                if (fares.length > 0) {//If selected or checked then pass true otherwise pass false to FlightResultAjax page
                    fares += ",ref-true";
                }
                else {
                    fares = "ref-true";
                }
            }
            else {
                if (fares.length > 0) {
                    fares += ",ref-false";
                }
                else {
                    fares = "ref-false";
                }
            }
            if (document.getElementById('chk' + journeyType + 'NonRefund') != null && document.getElementById('chk' + journeyType + 'NonRefund').checked) {
                if (fares.length > 0) {//If selected or checked then pass true otherwise pass false to FlightResultAjax page
                    fares += ",nonref-true";
                }
                else {
                    fares = "nonref-true";
                }
            }
            else {
                if (fares.length > 0) {
                    fares += ",nonref-false";
                }
                else {
                    fares = "nonref-false";
                }
            }
            //--------------------------------------End--------------------------------


            //----------------------------Stops filter logic-------------------------------
            if (document.getElementById('ctl00_cphTransaction_chk' + journeyType + 'NonStop')!=null && document.getElementById('ctl00_cphTransaction_chk' + journeyType + 'NonStop').checked) {
                if (stops.length > 0) {//If selected or checked then pass true otherwise pass false to FlightResultAjax page
                    stops += ",0-true";
                }
                else {
                    stops = "0-true";
                }
            }
            else {
                if (stops.length > 0) {
                    stops += ",0-false";
                }
                else {
                    stops = "0-false";
                }
            }
            if (document.getElementById('ctl00_cphTransaction_chk' + journeyType + 'OneStop') != null && document.getElementById('ctl00_cphTransaction_chk' + journeyType + 'OneStop').checked) {
                if (stops.length > 0) {//If selected or checked then pass true otherwise pass false to FlightResultAjax page
                    stops += ",1-true";
                }
                else {
                    stops = "1-true";
                }
            }
            else {
                if (stops.length > 0) {
                    stops += ",1-false";
                }
                else {
                    stops = "1-false";
                }
            }
            if (document.getElementById('ctl00_cphTransaction_chk' + journeyType + 'TwoStops') != null && document.getElementById('ctl00_cphTransaction_chk' + journeyType + 'TwoStops').checked) {
                if (stops.length > 0) {//If selected or checked then pass true otherwise pass false to FlightResultAjax page
                    stops += ",2-true";
                }
                else {
                    stops = "2-true";
                }
            }
            else {
                if (stops.length > 0) {
                    stops += ",2-false";
                }
                else {
                    stops = "2-false";
                }
            }
            //-----------------------------------End-----------------------------------

            //----------------------------Price Slider filter--------------------
            if (journeyType == "On") {
                price = $("#onwardamount").val();
            }
            else {
                price = $("#returnamount").val();
            }
            if ($('#lbtext').html() == "Hide Pub Fare") {
                showPub = 'in';
            }
            else {
                showPub = '';
            }  
            FilterResults(alines, fares, stops, timings, price, journeyType);
            //Based on the Journey we are making hdnIsOnSorted true/false
            // after that calling GetSelEmailCount () to maintain the result count and assigning the selected indexes.
            //after every sort action we are calling FilterByType so, based on the journey type we are making hdnIsOnSorted/hdnIsRetSorted as true.
            //after sort action checkbox will be uncheked state so we are removing the selected indexes from  hdnCheckboxEmail and maintaining the result count as same.
            if (journeyType == 'On') {
                document.getElementById('ctl00_cphTransaction_hdnIsOnSorted').value = "true";
                document.getElementById('ctl00_cphTransaction_hdnIsRetSorted').value = "false";
            }
            else {
                document.getElementById('ctl00_cphTransaction_hdnIsRetSorted').value = "true";
                document.getElementById('ctl00_cphTransaction_hdnIsOnSorted').value = "false";
            }
            GetSelEmailCount();
           
        }

        //All filters will be passed to the ajax page as we need to combine filters
        //Parameter Info
        //1. airline = Filtering based on airlines. airlines for the journeyType will added by comma(,) seperated in a format like this EK-true,AI-false. 
        //   If we want to omit particular airline then we will append false and vice-versa against airline code seperated by hyphen (-) as shown above.        
        //2. fares = Filtering based on Non Refundable or Refundable fares. fares for the journeyType will be added by comma(,) 
        //   seperated in a format like this ref - true, nonref - false,  true or false against fare type means either show or hide the result with fare type
        //3. stops = Filtering based on Non Stop, One stop etc. stops for the journeyType will be added by comma(,) 
        //   seperated in a format like this 0-true,1-false,2-false. 0-true means show Non Stop flights, 1-false means hide One Stop flights, 2-false means hide flights
        //   with 2 or more stops.
        //4. timings = Filtering based on timings like Morning(5:00 am-11:59:59 pm), Noon(12:00 pm - 5:59:59 pm), Evening(6:00 pm - 13:59:59 pm), Night(00:00 am - 4:59:59 am) etc
        function FilterResults(airline, fares, stops, timings, price, journeyType) {
            jtype = journeyType;
            
            if ($('#lbtext').html() == "Hide Pub Fare") {
                showPub = 'in';
            }
            else {
                showPub = '';
            }
            var passData = "airlines=" + airline + "&fares=" + fares + "&stops=" + stops + "&timings=" + timings + "&price=" + price + "&type=" + journeyType;
            if (sortData != undefined && sortData.split('-')[0].split('=')[1] == journeyType) {
                passData += sortData.split('-')[1] + "&showPub=" + showPub;
            }
            else {
                passData += "&sort=price&order=asc&showPub=" + showPub;
            }
            if (window.XMLHttpRequest) {
                Ajax = new XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject('Microsoft.XMLHTTP');
            }

            var url = "FlightResultAjax";
            Ajax.onreadystatechange = RenderResults;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(passData);
        }

        function RenderResults() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    $('#' + jtype + 'Results').html("");//Clear the existing html
                    var response = Ajax.responseText;
                    //console.log(response);//write the response html to console
                    $('#' + jtype + 'Results').html(response);//update the response html to the corresponding div based on jtype=On (Onward),Ret (Return)

                    if (filterTypeVal.length == 0) {//Clear Filter executed
                        //Set the first onward and return result selected
                        $('#rb' + jtype + 'Result0').click();
                        $('#' + jtype + 'Result0').addClass('checked');
                        $("#" + jtype + "Result0").click(function () {
                            $(this).closest('.flight-list-wrapper').find('.item').removeClass('checked')
                            if ($(this).prop("checked")) {
                                $(this).closest('.item').addClass('checked')
                            }
                        });
                    }
                }
            }
        }     

        //Sorting functions for results -
        // Begin Onward results sorting

        function PriceSort(journeyType) {
            //sdocument.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            //SelectAll(journeyType);
            $("#" + journeyType.toLowerCase() + "PriceSort").find('span').toggleClass('active');
            $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
            if ($("#" + journeyType.toLowerCase() + "ddlPriceSort").val() == "ASC" || $("#" + journeyType.toLowerCase() + "ddlPriceSort").val() == "-1") {
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").val("DESC");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").attr("text", "Highest");
                SortResults(journeyType, 'price', 'desc')
            }
            else {
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").val("ASC");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").attr("text", "Lowest");
                SortResults(journeyType, 'price', 'asc');
            }
        }

        //Arrival time sorting
        function ArrivalTimeSort(journeyType) {
            //document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            //SelectAll(journeyType);
            $("#" + journeyType.toLowerCase() + "ArrSort").find('span').toggleClass('active');
            $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
            if ($("#" + journeyType.toLowerCase() + "ddlArrSort").val() == "ASC" || $("#" + journeyType.toLowerCase() + "ddlArrSort").val() == "-1") {
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("DESC");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").attr("text", "Longest");
                SortResults(journeyType, 'atime', 'desc')
            }
            else {
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("ASC");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").attr("text", "Shortest");
                SortResults(journeyType, 'atime', 'asc');
            } 
        }

        //Departure time sorting
        function DepartureTimeSort(journeyType) {
            //document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            //SelectAll(journeyType);
            $("#" + journeyType.toLowerCase() + "DepSort").find('span').toggleClass('active');
            $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
            if ($("#" + journeyType.toLowerCase() + "ddlDepSort").val() == "ASC" || $("#" + journeyType.toLowerCase() + "ddlDepSort").val() == "-1") {
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("DESC");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").attr("text", "Longest");
                SortResults(journeyType, 'dtime', 'desc')
            }
            else {
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("ASC");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").attr("text", "Shortest");
                SortResults(journeyType, 'dtime', 'asc');
            }
            
        }

        //Stops sorting
        function StopsSort(journeyType) {
            //document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            //SelectAll(journeyType);
            $("#" + journeyType.toLowerCase() + "StopSort").find('span').toggleClass('active');
            $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
            $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
            if ($("#" + journeyType.toLowerCase() + "ddlStopSort").val() == "ASC" || $("#" + journeyType.toLowerCase() + "ddlStopSort").val() == "-1") {
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("DESC");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").attr("text", "Longest");
                SortResults(journeyType, 'stops', 'desc')
            }
            else {
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("ASC");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").attr("text", "Shortest");
                SortResults(journeyType, 'stops', 'asc');
            }
            
        }
        //End Return results sorting

        function ClearFilters(journeyType) {
            //Set Checked=true for all checkbox controls
            $('#chk' + journeyType + 'AllAirlines').prop('checked', true);
            $('#chk' + journeyType + 'AllRefund').prop('checked', true);
            $('#chk' + journeyType + 'AllStops').prop('checked', true);
            var airlineCount = eval('<%=airlineList.Count%>');
             if (journeyType == "Ret") {
                airlineCount = eval('<%=airlineListReturn.Count%>');
            }
            for (var i = 0; i < airlineCount; i++) {
                $('#chk' + journeyType + 'Airline' + eval(i)).prop('checked', true);
            }

            $('#chk' + journeyType + 'Refund').prop('checked', true);
            $('#chk' + journeyType + 'NonRefund').prop('checked', true);

            $('#ctl00_cphTransaction_chk' + journeyType + 'NonStop').prop('checked', true);
            $('#ctl00_cphTransaction_chk' + journeyType + 'OneStop').prop('checked', true);
            $('#ctl00_cphTransaction_chk' + journeyType + 'TwoStops').prop('checked', true);

            //Reset Sort Dropdowns
            
                $("#" + journeyType.toLowerCase() + "DepSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlDepSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "AirSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlAirSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "ArrSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlArrSort").select2('val','-1');

                $("#" + journeyType.toLowerCase() + "PriceSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlPriceSort").select2("val", "ASC");


                $("#" + journeyType.toLowerCase() + "StopSort").find('span').removeClass("active");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").val("-1");
                $("#" + journeyType.toLowerCase() + "ddlStopSort").select2('val','-1');           

            //Reset Timing Range
            $('#btn' + journeyType + 'Morning').removeClass("btn-primary");
            $('#btn' + journeyType + 'Noon').removeClass("btn-primary");
            $('#btn' + journeyType + 'Evening').removeClass("btn-primary");
            $('#btn' + journeyType + 'Night').removeClass("btn-primary");

            
           

            if (journeyType == "On") {
                //Reset onward Price slider to normal
                $("#slider-range-onward").slider("destroy");

                //Re intialize
                $(function () {
                    var $slide = $("#slider-range-onward").slider({
                        range: true,
                        min: parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value),
                        max: parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value),
                        step: 1,
                        values: [parseFloat(document.getElementById('<%=hdnOnPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnOnPriceMax.ClientID %>').value)],
                        slide: function (event, ui) {

                            if (ui.values[0] <= ui.values[1]) {
                                $("#onwardamount").val(ui.values[0] + "-" + ui.values[1]);
                                document.getElementById('<%=hdnOnPriceMin.ClientID %>').value = ui.values[0];
                                document.getElementById('<%=hdnOnPriceMax.ClientID %>').value = ui.values[1];
                            }
                            else {
                                $("#onwardamount").val(parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value) + "-" + parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value));
                                document.getElementById('<%=hdnOnPriceMin.ClientID %>').value = parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value);
                                document.getElementById('<%=hdnOnPriceMax.ClientID %>').value = parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value);
                            }
                        }
                    });
                    //Set the min max values to the original values to reset the slider
                    $("#slider-range-onward").slider("values", "0", parseFloat(document.getElementById('<%=hdnOnMinValue.ClientID %>').value));
                    $("#slider-range-onward").slider("values", "1", parseFloat(document.getElementById('<%=hdnOnMaxValue.ClientID %>').value));
                    //Assign the original values to the slider values label
                    $("#onwardamount").val($("#slider-range-onward").slider("values", 0) + "-" + $("#slider-range-onward").slider("values", 1));
                });

                //Attach slider change event
                $(function () {
                    $("#slider-range-onward").slider({
                        change: function () {
                            var duration = document.getElementById('onwardamount').value;
                            var min = duration.split('-')[0];
                            var max = duration.split('-')[1];

                            document.getElementById('<%=hdnOnPriceMin.ClientID %>').value = min;
                            document.getElementById('<%=hdnOnPriceMax.ClientID %>').value = max;

                            FilterByType('Price', 'On');
                        }
                    });
                });
            }
            else {
                //Reset return price slider to normal
                $("#slider-range-return").slider("destroy");

                //Re initialize
                $(function () {
                    var $slide = $("#slider-range-return").slider({
                        range: true,
                        min: parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value),
                        max: parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value),
                        step: 1,
                        values: [parseFloat(document.getElementById('<%=hdnRetPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnRetPriceMax.ClientID %>').value)],
                        slide: function (event, ui) {

                            if (ui.values[0] <= ui.values[1]) {
                                $("#returnamount").val(ui.values[0] + "-" + ui.values[1]);
                                document.getElementById('<%=hdnRetPriceMin.ClientID %>').value = ui.values[0];
                                document.getElementById('<%=hdnRetPriceMax.ClientID %>').value = ui.values[1];
                            }
                            else {
                                $("#returnamount").val(parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value) + "-" + parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value));
                                document.getElementById('<%=hdnRetPriceMin.ClientID %>').value = parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value);
                                document.getElementById('<%=hdnRetPriceMax.ClientID %>').value = parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value);
                            }
                        }
                    });
                    //Set the min max values to the original values to reset the slider
                    $("#slider-range-return").slider("values", "0", parseFloat(document.getElementById('<%=hdnRetMinValue.ClientID %>').value));
                    $("#slider-range-return").slider("values", "1", parseFloat(document.getElementById('<%=hdnRetMaxValue.ClientID %>').value));
                    //Assign the original values to the slider values label
                    $("#returnamount").val($("#slider-range-return").slider("values", 0) + "-" + $("#slider-range-return").slider("values", 1));
                });

                //Attach slider change event
                $(function () {
                    $("#slider-range-return").slider({
                        change: function () {
                            var duration = document.getElementById('returnamount').value;
                            var min = duration.split('-')[0];
                            var max = duration.split('-')[1];

                            document.getElementById('<%=hdnRetPriceMin.ClientID %>').value = min;
                            document.getElementById('<%=hdnRetPriceMax.ClientID %>').value = max;

                            FilterByType('Price', 'Ret');
                        }
                    });
                });

            }
            
            $('#ctl00_cphTransaction_hdn' + journeyType + 'TimingFilter').val('Any Time');            
            
            sortData = "type=" + journeyType + "-&sort=price&order=asc";
            timings = '';

            //Apply default filters
            FilterByType('', journeyType);
           
        }
    </script>
    <!-- Email and Download Itineraries Script -->
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 46)) {
                return false;
            }
            return true;
        }
        //@@@@ select multiple items with select checkbox . chandan 14/12/2015
        function selectBox(id, journeyType) {
            //if any check box selects then we are making hidden field as false.
            if (journeyType == 'On') {
                document.getElementById('ctl00_cphTransaction_hdnIsOnSorted').value = "false";
            }
            else {
                document.getElementById('ctl00_cphTransaction_hdnIsRetSorted').value = "false";
            }
            GetSelEmailCount();
        }

        function DownloadResults() {
            if (document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value.length > 0) {
                HideEmailDiv('downloadBlock');
                window.location.href = "ExcelDownload?ids=" + document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value + "&markup=" + (document.getElementById('txtDownloadMarkup').value.trim().length > 0 ? document.getElementById('txtDownloadMarkup').value : "0")+"&routing=true";
            }
            else {
                document.getElementById('emailStatus').innerHTML = 'Please select at least one Itinerary';
                document.getElementById('emailStatus').style.display = 'block';
            }
        }

      

        $(document).ready(function () {
            if (document.getElementById('onwardFiltered') != null) {
                onResultCount = document.getElementById('onwardFiltered').value;
            }
            if (document.getElementById('returnFiltered') != null) {
                retResulTcount = document.getElementById('returnFiltered').value;
            }
            //disableEmailItineraries();
        })

        //$("#onward-tab").on('click', function () { disableEmailItineraries();});
        //$("#return-tab").on('click', function () { disableEmailItineraries();});

         /*
         * Function for Selecting all results irrespective of pagination based on the result indexes
         * and checked indexes will be stored in ctl00_cphTransaction_hdnCheckboxEmail
         * */


        function AssignSelEmailResultIds(journeyType, resultCount) {

            var hdnField;//Variable WhichHolds the Email Count.Used to display in the Pop up.
           
            if (document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail') != null) {
                hdnField = document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail');
            }

            
            //2.Now Get the Selected Checkbox count.
            for (var i = 0; i < eval(resultCount); i++) {
                if (document.getElementById('chk' + journeyType + 'Res-' + i) != null && document.getElementById('chk' + journeyType + 'Res-' + i).checked) {
                    var index = $('#hdn' + journeyType + 'ResultId' + i).val();//Get the selected checkbox result id.
                    if (hdnField.value != "") {
                         hdnField.value += ',' + index;
                        }
                        else {
                            hdnField.value = index;
                        }
                }
            }
            
        }


        //Gets the selected email count
        function GetSelEmailCount() {

            var filteredIds;//Onward Results Count.
            var returnFilteredIds;//Return Results Count.

            if (document.getElementById('onwardFiltered') != null) {
                filteredIds = document.getElementById('onwardFiltered').value;
            }
            if (document.getElementById('returnFiltered') != null) {
                returnFilteredIds = document.getElementById('returnFiltered').value;
            }
            if (document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail') != null) {
                hdnField = document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail');
                hdnField.value = ""; //Clear all the previous values.
            }
            if (document.getElementById('ctl00_cphTransaction_hdnIsOnSorted').value == "false" ) //hdnIsOnSorted false means assigning the seleced result indexes to hdnCheckboxEmail.
            {
                AssignSelEmailResultIds('On', filteredIds); //1.1Iterate through onward results.
               
            }
   
            if (document.getElementById('ctl00_cphTransaction_hdnIsRetSorted').value == "false") // hdnIsRetSorted false means assigning the seleced result indexes to hdnCheckboxEmail.
            {
                 AssignSelEmailResultIds('Ret', returnFilteredIds);//1.1Iterate through return results.
            }
            
            //chkOnDownAll
            //chkRetDownAll
            //Assign the previous state of select all check box
            if (document.getElementById('chkOnDownAll') != null) {
                var onSelResCount = 0;
                var journeyType = 'On';
                for (var i = 0; i < eval(filteredIds); i++) {
                    if (document.getElementById('chk' + journeyType + 'Res-' + i) != null && document.getElementById('chk' + journeyType + 'Res-' + i).checked) {
                        onSelResCount++;
                    }
                
                }
                //if (eval(onSelResCount) == eval(filteredIds) && eval(onSelResCount)>0 && eval(filteredIds)>0) {
                //    document.getElementById('chkOnDownAll').checked = true;
                //}
                //else {
                //    document.getElementById('chkOnDownAll').checked = false;
                //}
            }

            if (document.getElementById('chkRetDownAll') != null) {
                var onSelResCount = 0;
                var journeyType = 'Ret';
                for (var i = 0; i < eval(returnFilteredIds); i++) {
                    if (document.getElementById('chk' + journeyType + 'Res-' + i) != null && document.getElementById('chk' + journeyType + 'Res-' + i).checked) {
                        onSelResCount++;
                    }
                }
                //if (eval(onSelResCount) == eval(returnFilteredIds) && eval(onSelResCount)>0 && eval(returnFilteredIds)>0) {
                //    document.getElementById('chkRetDownAll').checked = true;
                //}
                //else {
                //    document.getElementById('chkRetDownAll').checked = false;
                //}
            }
        }


        function SelectAll(journeyType) {
            
            var chkAll;
            var filteredIds;//Selected Journey Type Results Count

            if (journeyType == 'On' && document.getElementById('onwardFiltered') != null) {
                filteredIds = document.getElementById('onwardFiltered').value;
            }
            if (journeyType == 'Ret' && document.getElementById('returnFiltered') != null) {
                filteredIds = document.getElementById('returnFiltered').value;
            }

            if (document.getElementById('chk' + journeyType + 'DownAll') != null) {//If Select All Checked True
                chkAll = document.getElementById('chk' + journeyType + 'DownAll');
                if (chkAll.checked) {
                    for (var i = 0; i < eval(filteredIds); i++) {
                        if (document.getElementById('chk' + journeyType + 'Res-' + i) != null) {
                            document.getElementById('chk' + journeyType + 'Res-' + i).checked = true;
                        }
                    }
                }
                else {//If Select All Checked False;
                    chkAll.checked = false;
                    for (var i = 0; i < eval(filteredIds); i++) {
                        if (document.getElementById('chk' + journeyType + 'Res-' + i) != null) {
                            document.getElementById('chk' + journeyType + 'Res-' + i).checked = false;
                        }
                    }
                }
            }
           //if we select select All check box then we are making hdnIsOnSorted & hdnIsRetSorted as false 
            if (journeyType == 'On') {
                document.getElementById('ctl00_cphTransaction_hdnIsOnSorted').value = "false";
            }
            else {
                document.getElementById('ctl00_cphTransaction_hdnIsRetSorted').value = "false";
            }
            GetSelEmailCount();//Assigns the selected Email Count Based on the checkbox selection.
        }
        function showClientFields() {//Displays the selected fields

            if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true) {
                document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "Checked";
                if (document.getElementById('wrapper1') != null)
                    document.getElementById('wrapper1').style.display = "block";
                if (document.getElementById('divLocations') != null)
                    document.getElementById('divLocations').style.display = "block";
                LoadAgentLocations();
            }
            else {
                if (document.getElementById('wrapper1') != null)
                    document.getElementById('wrapper1').style.display = "none";

                document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "UnChecked";
                if (document.getElementById('divLocations') != null)
                    document.getElementById('divLocations').style.display = "none";
            }
        }

    </script>
    <asp:HiddenField ID="hdnAgentLocation" runat="server" />
    <asp:HiddenField ID="hdnOnPriceMax" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOnPriceMin" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOnMinValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOnMaxValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRetPriceMax" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRetPriceMin" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRetMinValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRetMaxValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnFilter" runat="server" />
    <asp:HiddenField ID="hdnCheckboxEmail" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnChkOnEmailIds" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnChkRetEmailIds" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="PageNoString" runat="server" />
    <asp:HiddenField ID="Change" runat="server" />
    <asp:HiddenField ID="hdnSubmit" runat="server" />
    <asp:HiddenField ID="hdnWayType" runat="server" Value="return" />
    <asp:HiddenField ID="hdnModifySearch" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSourceBinding" runat="server" Value="false" />
    <asp:HiddenField ID="hdnIsOnSorted" runat="server" Value="false"></asp:HiddenField> 
    <asp:HiddenField ID="hdnIsRetSorted" runat="server" Value="false"></asp:HiddenField>
    <asp:HiddenField ID="hdnOnTimingFilter" runat="server" Value="Any Time" />
    <asp:HiddenField ID="hdnRetTimingFilter" runat="server" Value="Any Time" />
    <div id="mcontainer1" style="position: absolute; top: 260px; left: 54%; display: none; z-index: 9999"></div>
    <div id="mcontainer2" style="position: absolute; top: 270px; left: 54%; display: none; z-index: 9999"></div>
    <div id="mcontainer3" style="position: absolute; top: 280px; left: 54%; display: none; z-index: 9999"></div>
    <div id="mcontainer4" style="position: absolute; top: 290px; left: 54%; display: none; z-index: 9999"></div>
    <div id="mcontainer5" style="position: absolute; top: 300px; left: 54%; display: none; z-index: 9999"></div>
    <div id="mcontainer6" style="position: absolute; top: 310px; left: 54%; display: none; z-index: 9999"></div>
    <input type="hidden" id="hdnSelectedResults" />
    <input type="hidden" id="hdnOnPrice" />
    <input type="hidden" id="hdnRetPrice" />    
    <div class="error_module">
             <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </div>
                    
       <%-- Adding Class based on Roundtrip/Oneway --%>           
       <%
           if (onwardResults.Count > 0 && returnResults.Count > 0) %>
		<%{
                %>
		     
			<div id="mainContentDiv" class="flight-result-page multi-view">
		<% }else { %>
			<div id="mainContentDiv" class="flight-result-page oneway-view">
		<% } %>
   

        <%-- Flight Summary / Modify --%>
        <div class="modify-flight-panel px-3 py-1">
            <div class="row">
                <div class="col-12 col-md-2">
                    <span class="text-title"><%=(request.Type == CT.BookingEngine.SearchType.Return ? "ROUNDTRIP" : "ONEWAY") %> </span>
                    <div class="sector d-flex justify-content-between">
                        <span class="airline bold-text"><%=request.Segments[0].Origin %></span>
                        <i class="font-icon icon-loop-alt4"></i>
                        <span class="airline bold-text"><%=request.Segments[0].Destination %></span>
                    </div>
                </div>
                <div class="col-6 col-md-2">
                    <span class="text-title">DEPART(<%=request.Segments[0].PreferredDepartureTime.ToString("dddd") %>)</span>
                    <div class="date bold-text"><%=request.Segments[0].PreferredDepartureTime.ToString("dd MMM yyyy") %></div>
                </div>
                <div class="col-6 col-md-2 text-right">                  
                    <span class="text-title">RETURN(<%= (request.Type == CT.BookingEngine.SearchType.Return ? request.Segments[1].PreferredDepartureTime.ToString("dddd") : "")%>)</span>
                    <div class="date bold-text"><%=(request.Type == CT.BookingEngine.SearchType.Return ? request.Segments[1].PreferredDepartureTime.ToString("dd MMM yyyy") :"") %></div>
                </div>
                <div class="col-4 col-md-2">
                    <span class="text-title">CLASS</span>
                    <div class="bold-text class"><%=request.Segments[0].flightCabinClass %></div>
                </div>
                <div class="col-8 col-md-2 text-right">
                    <span class="text-title d-block pb-2">TRAVELLERS</span>
                    <span class="pax-count">Adult - <%=request.AdultCount %></span>
                    <span class="pax-count">Child - <%=request.ChildCount %></span>
                    <span class="pax-count">Infant - <%=request.InfantCount %></span>
                </div>
                <div class="col-12 col-md-2 text-right">
                    <button type="button" onclick="showClientFields()" class="btn btn-outline-light" data-toggle="collapse" data-target="#DivModify" aria-expanded="false" aria-controls="DivModify" >Modify</button>
                </div>
                <div id="DivModify" class="margin_bot20 bg_white bor_gray marbot_10 collapse">
                    <div class="search_container" id="ModifySearch">
                        <div class="col-md-12">
                            <div class="col-md-12">

                                <h3>Modify Search </h3>

                            </div>

                            <div id="errMess" class="error_module" style="display: none;">
                                <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                </div>
                            </div>
                            <div>
                                <asp:HiddenField ID="hdnBookingAgent" runat="server" />
                                <%if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
                                    { %>
                                <div id="trAgent" class="custom-radio-table">
                                    <div class="col-md-4 col-xs-4">
                                        <asp:RadioButton ID="radioSelf" GroupName="Flight" Checked="true" Text="For Self"
                                            onclick="disablefield();" runat="server" />
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <asp:RadioButton ID="radioAgent" Text="Client" GroupName="Flight" runat="server" onchange="disablefield();"  />
                                    </div>
                                    <div class="col-md-2 col-xs-12">
                                        <div id="wrapper1" style="display: none">
                                            Select Client:
                                    <%--<asp:TextBox ID="txtAirAgents" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlAirAgents" CssClass="form-control" runat="server" onchange="LoadAgentLocations()">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div id="divLocations" style="display: none">
                                        <div class="col-md-2 col-xs-2">
                                            <div id="wrapper3">
                                                Select Location:                                                    
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-xs-2">
                                            <div id="wrapper2">
                                                <asp:DropDownList ID="ddlAirAgentsLocations" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                                <%} %>
                            </div>
                            <div class="custom-radio-table">
                                <%if (Settings.LoginInfo.IsCorporate == "Y")
                                    { %>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <!--<label>Select Travel Reason</label>-->
                                        <asp:DropDownList ID="ddlFlightTravelReasons" AppendDataBoundItems="true" CssClass="form-control select-element" runat="server">
                                            <asp:ListItem Selected="True" Value="-1" Text="Select Travel Reason"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <!--<label>Specify Traveller / Employee Number</label>-->
                                        <asp:DropDownList ID="ddlFlightEmployee" AppendDataBoundItems="true" CssClass="form-control select-element" runat="server">
                                            <asp:ListItem Selected="True" Value="-1" Text="Select Traveller / Employee"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <%} %>
                                <div class="clearfix"></div>

                                <div class="col-md-4 col-xs-4">
                                    <asp:RadioButton ID="oneway" GroupName="radio" onclick="disablefield();" runat="server"
                                        Text="One Way" Font-Bold="true"  />
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <asp:RadioButton ID="roundtrip" GroupName="radio" runat="server"
                                        Text="Round Trip" Font-Bold="true" onchange="disablefield();" />
                                </div>
                                <div class="col-md-4 col-xs-4 hidden-xs">
                                    <label style="display: block">
                                        <asp:RadioButton ID="multicity" GroupName="radio" onclick="disablefield();" runat="server"
                                            Text="Multi City" Font-Bold="true" style="display:none" /></label>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                            <div id="tblNormal">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <div>
                                                <div class="col-md-6">
                                                    <div>
                                                        From
                                                    </div>
                                                    <div>
                                                        <asp:TextBox ID="Origin" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <div class="clear">
                                                            <div style="width: 300px; line-height: 30px; color: #000;" id="statescontainer">
                                                            </div>
                                                        </div>
                                                        <div id="multipleCity1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div>
                                                        Going To
                                                    </div>
                                                    <div>
                                                        <asp:TextBox ID="Destination" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <div id="Div1">
                                                            <div style="width: 300px; line-height: 30px; color: #000;" id="statescontainer2">
                                                            </div>
                                                        </div>
                                                        <div id="multipleCity2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr><td><div class="col-md-6"><asp:CheckBox ID="chkNearByPort" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></div></td></tr>
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-12 padding-0">
                                                <div class="col-md-6 col-xs-12 div1024">


                                                    <div style="display: none" id="fcontainer1"></div>







                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="3">Departure
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox Width="110px" ID="DepDate" data-calendar-contentID="#fcontainer1" CssClass="form-control" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(null)" onclick="showFlightCalendar1()">
                                                                    <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlDepTime" CssClass="form-control" runat="server">
                                                                    <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                                                    <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                                    <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                                    <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                                    <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="textbox_A3" class="col-md-6 col-xs-12 div1024">


                                                    <div style="display: none" id="fcontainer2"></div>



                                                    <div>
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td colspan="3">Return
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox Width="110px" ID="ReturnDateTxt" data-calendar-contentID="#fcontainer2" CssClass="form-control" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:void(null)" onclick="showFlightCalendar2()">
                                                                        <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlReturnTime" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                                                        <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                                        <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                                        <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                                        <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>


                                            <style>
                                                @media only screen and (min-device-width:768px) and (max-device-width:1024px) {
                                                    .div1024 {
                                                        width: 100%;
                                                    }
                                                }
                                            </style>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
                            </div>
                            <style>
                                #tblMultiCity input[type=text] {
                                    width: 96%;
                                }

                                .time_t {
                                    width: 100px !important;
                                }
                            </style>
                            <div class="col-md-12 martop_14">
                                <table style="display: none; width: 100%" id="tblMultiCity">
                                    <tr>
                                        <td colspan="2">
                                            <table id="tblGrid" runat="server">
                                                <tr>
                                                    <td style="width: 320px;">Departure
                                                    </td>
                                                    <td style="width: 320px;">Arrival
                                                    </td>
                                                    <td>Date
                                                    </td>
                                                    <td style="width: 100px;"></td>
                                                    <td style="width: 100px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="City1" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="City2" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer2">
                                                        </div>
                                                    </td>
                                                    <td valign="bottom">
                                                        <asp:TextBox ID="Time1" runat="server" data-calendar-contentID="#mcontainer1" CssClass="form-control time_t"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(null)" onclick="showMultiCityCal1()">
                                                            <img id="Img5" src="images/call-cozmo.png" alt="Pick Date" />
                                                        </a>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr><td><asp:CheckBox ID="chkNearByPort1" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <table style="margin-bottom: 10px; margin-top: 10px">
                                                            <tr>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City3" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer3">
                                                                    </div>
                                                                </td>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City4" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div id="statesshadow2">
                                                                        <div style="width: 300px; position: absolute; display: none" id="citycontainer4">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td valign="bottom">
                                                                    <asp:TextBox ID="Time2" data-calendar-contentID="#mcontainer2" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <a href="javascript:void(null)" onclick="showMultiCityCal2()">
                                                                        <img id="Img6" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a>
                                                                </td>
                                                                <td style="width: 100px;">&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr><td><asp:CheckBox ID="chkNearByPort2" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <table id="tblRow3" style="display: none; margin-bottom: 10px">
                                                            <tr>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City5" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer5">
                                                                </td>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City6" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer6">
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Time3" data-calendar-contentID="#mcontainer3" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <a href="javascript:void(null)" onclick="showMultiCityCal3()">
                                                                        <img id="Img7" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <input type='image' name='imageField' id='image1' src='images/minus.gif' onclick='removeRow(3); return false;'>
                                                                </td>
                                                            </tr>
                                                            <tr><td><asp:CheckBox ID="chkNearByPort3" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <table id="tblRow4" style="display: none; margin-bottom: 10px">
                                                            <tr>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City7" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer7">
                                                                    </div>
                                                                </td>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City8" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer8">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Time4" data-calendar-contentID="#mcontainer4" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <a href="javascript:void(null)" onclick="showMultiCityCal4()">
                                                                        <img id="Img8" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <input type='image' name='imageField' id='image2' src='images/minus.gif' onclick='removeRow(4); return false;'>
                                                                </td>
                                                            </tr>
                                                            <tr><td><asp:CheckBox ID="chkNearByPort4" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <table id="tblRow5" style="display: none; margin-bottom: 10px">
                                                            <tr>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City9" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer9">
                                                                    </div>
                                                                </td>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City10" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer10">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Time5" data-calendar-contentID="#mcontainer5" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <a href="javascript:void(null)" onclick="showMultiCityCal5()">
                                                                        <img id="Img9" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <input type='image' name='imageField' id='image3' src='images/minus.gif' onclick='removeRow(5); return false;'>
                                                                </td>
                                                            </tr>
                                                            <tr><td><asp:CheckBox ID="chkNearByPort5" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <table id="tblRow6" style="display: none; margin-bottom: 10px">
                                                            <tr>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City11" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer11">
                                                                    </div>
                                                                </td>
                                                                <td style="width: 320px;">
                                                                    <asp:TextBox ID="City12" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                        id="citycontainer12">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Time6" data-calendar-contentID="#mcontainer6" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <a href="javascript:void(null)" onclick="showMultiCityCal6()">
                                                                        <img id="Img10" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle; margin: 0px;" />
                                                                    </a>
                                                                </td>
                                                                <td style="width: 100px;">
                                                                    <input type='image' name='imageField' id='image4' src='images/minus.gif' onclick='removeRow(6); return false;'>
                                                                </td>
                                                            </tr>
                                                            <tr><td><asp:CheckBox ID="chkNearByPort6" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                            <div style="height: 36px">
                                                <u><a class="cursor_point" id="btnAddRow" onclick="addRow();">+ Add
                                            Row </a></u>
                                            </div>
                                            <div id="errMulti" style="display: none">
                                                <div id="errorMulti" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <table width="100%" border="0" cellpadding="5" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div>
                                                Adult
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlAdults" CssClass="form-control" runat="server">
                                                    <asp:ListItem Selected="True" Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div>
                                                Child
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlChilds" CssClass="form-control" runat="server">
                                                    <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div>
                                                infant
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlInfants" CssClass="form-control" runat="server">
                                                    <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div>
                                                Class
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlBookingClass" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True" Text="Any" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Economy" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Premium Economy" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Business" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="First" Value="6"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <%
                                    string showPA = "display: block;";
                                    CT.Core.AgentAppConfig showPAForAgent = liappConfigs.Find(x => x.AppKey.ToLower() == "showpreferredairline");
                                    if (showPAForAgent != null && showPAForAgent.AppValue.ToLower() == "false")
                                        showPA = "display:none;";%>
                                <td>
                                    <div class="col-md-12 martop_14">
                                        <div style="<%=showPA%>">
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtPreferredAirline" CssClass="form-control" Text="Type Preferred Airline"
                                                runat="server" onclick="IntDom('statescontainer4' , 'ctl00_cphTransaction_txtPreferredAirline')"
                                                onblur="markout(this, 'Type Preferred Airline')" onfocus="markin(this, 'Type Preferred Airline')" onchange="RecheckAirline(0)"></asp:TextBox>
                                            <asp:HiddenField ID="airlineCode" runat="server" />
                                            <asp:HiddenField ID="airlineName" runat="server" />



                                            <div id="statescontainer4" style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none; z-index: 103;">
                                            </div>



                                        </div>




                                        <div class="col-md-3 martop_10">
                                            <a class="fcol_fff cursor_point" id="addRowLink" onclick="AddPrefAirline()">+ Add New Preferred Airline   <%--<img src="images/plus.gif" --%></a>


                                        </div>
                                        </div>

                                        <div class="col-md-3 martop_10">
                                            <asp:RadioButtonList Width="100%" CssClass="custom-radio-table" ID="rbtnlMaxStops" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="-1" Text="All Flights" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Direct"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="1 Stop"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2 Stops"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-3 martop_10">
                                            <asp:CheckBox ID="chkRefundFare" CssClass="custom-checkbox-table" runat="server" Text="Refundable Fares Only" />
                                            <%chkRefundFare.Checked = (Settings.LoginInfo.IsCorporate.ToLower() == "y" ? true : false);
                                                chkRefundFare.Visible = (liappConfigs.Exists(x => x.AppKey.ToLower() == "hiderefundablefares" && x.AppValue.ToLower() == "true") ? false : true);%>
                                        </div>


                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="<%=showPA%>">
                                    <div class="col-md-12">

                                        <div class="col-md-3 martop_10" id="divPreferredAirline0">
                                            <div id="divPrefAirline0" style="display: none">
                                                <input type="text" id="txtPreferredAirline0" name="txtPreferredAirline0" class="form-control" onclick="IntDom('statescontainer5', 'txtPreferredAirline0')"
                                                    placeholder="Type Preferred Airline" onchange="RecheckAirline(1)" />
                                                <div id="statescontainer5" style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none; z-index: 103;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 martop_10">
                                            <a class="fcol_fff cursor_point martop_10" id="removeRowLink0"
                                                style="cursor: pointer; display: none">
                                                <img src="images/minus.gif" onclick="RemovePrefAirline(0)" /></a>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="divPreferredAirline1">
                                        <div class="col-md-3 martop_10">
                                            <div id="divPrefAirline1" style="display: none">
                                                <input type="text" id="txtPreferredAirline1" name="txtPreferredAirline1" class="form-control"
                                                    onclick="IntDom('statescontainer6', 'txtPreferredAirline1')" placeholder="Type Preferred Airline" onchange="RecheckAirline(2)" />
                                                <div id="statescontainer6" style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none; z-index: 103;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 martop_10">
                                            <a class="fcol_fff cursor_point martop_10" id="removeRowLink1"
                                                style="cursor: pointer; display: none">
                                                <img src="images/minus.gif" onclick="RemovePrefAirline(1)" /></a>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="divPreferredAirline2">
                                        <div class="col-md-3 martop_10">
                                            <div id="divPrefAirline2" style="display: none">
                                                <input type="text" id="txtPreferredAirline2" name="txtPreferredAirline2" class="form-control"
                                                    onclick="IntDom('statescontainer7', 'txtPreferredAirline2')" placeholder="Type Preferred Airline" onchange="RecheckAirline(3)" />
                                                <div id="statescontainer7" style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none; z-index: 103;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 martop_10">
                                            <a class="fcol_fff cursor_point martop_10" id="removeRowLink2"
                                                style="cursor: pointer; display: none">
                                                <img src="images/minus.gif" onclick="RemovePrefAirline(2)" /></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-md-12" style="margin-top: 14px" id="Additional-Div">
                                        <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                            { %>
                                        <%} %>
                                        <div class="col-md-4">
                                            <asp:Label ID="lblSearchSupp" runat="server" Text="Search Suppliers :" Visible="true"
                                                Font-Bold="true"></asp:Label>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="pull-left chckboxTravel">
                                                <asp:CheckBox ID="chkFlightAll" CssClass="chkChoice custom-checkbox-table" runat="server" Text="All" Checked="true" Visible="true"
                                                    onclick="selectFlightSources();" />
                                            </label>
                                            <label class="pull-left table-responsive chkChoiceTableTravel">
                                                <asp:CheckBoxList ID="chkSuppliers" CssClass="chkChoice table table-condensed custom-checkbox-table" runat="server" RepeatDirection="Horizontal" Visible="true"
                                                    onclick="setUncheck();">
                                                </asp:CheckBoxList>
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-md-12">
                                        <label class=" pull-right">
                                            <asp:Button CssClass="but but_b pull-right btn-xs-block" runat="server" ID="btnSearchFlight"
                                                Text="Search Flights" OnClientClick="return FlightSearch();" OnClick="btnSearchFlight_Click" />
                                        </label>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="selected-flight-panel mt-2 flight-result-default px-3 py-1 d-none d-lg-block">
            <div class="row">
              
                  <%-- Changing width of column based on Roundtrip/Oneway --%>                  
                  <%if (onwardResults.Count > 0 && returnResults.Count > 0) %>
					<%{ %>
						   <div class="col-12 col-md-5 onward separator-right">
					<% }else { %>
						    <div class="col-12 col-md-10 onward separator-right">
					<% } %>
                   
                    <div class="row no-gutter">
                        <div class="col-12 headline-text pb-2">ONWARD</div>
                        <div class="col-12 col-lg-3">
                            <img id="onlogo" src="" alt="SG-Airline" class="airline-logo float-left pr-2" />
                            <span id="onairline" class="airline-name"> <span></span>
                            </span>
                        </div>
                        <div class="col-md-5 col-lg-3 text-right">
                            <div id="ondtime" class="time"><em id="tt">AM</em></div>
                            <div id="ondaport" class="airport-code"><span class="city"></span></div>
                        </div>
                        <div class="col-md-2 col-lg-3 text-align-center">
                            <i class="font-icon icon-flight-right"></i>
                            <span id="onstop" class="stops"></span>
                            <span id="onpolicy" class="stops"></span>
                        </div>
                        <div class="col-md-5 col-lg-3">
                            <div id="onatime" class="time"><em>AM</em></div>
                            <div id="onaaport" class="airport-code"><span class="city"></span></div>
                        </div>
                        <div id="onprice" class="col-12 headline-text pb-2 text-align-center">                           
                            
                        </div>
                    </div>
                </div>
                <%if (onwardResults.Count > 0 && returnResults.Count > 0)
                    { %>
                <div id="returnRes" class="col-12 col-md-5 return separator-right">
                    <div class="row">
                        <div class="col-12 headline-text pb-2">RETURN</div>
                        <div class="col-12 col-lg-3">
                            <img id="retlogo" src="images/AirlineLogo/UL.gif" alt="SG-Airline" class="airline-logo float-left pr-2" />
                            <span id="retairline" class="airline-name"><span></span>
                            </span>
                        </div>
                        <div class="col-md-5 col-lg-3 text-right">
                            <div id="retdtime" class="time"><em>AM</em></div>
                            <div id="retdaport" class="airport-code"><span class="city"></span></div>
                        </div>
                        <div class="col-md-2 col-lg-3 text-align-center">
                            <i class="font-icon icon-flight-right"></i>
                            <span id="retstop" class="stops"></span>
                            <span id="retpolicy" class="stops"></span>
                        </div>
                        <div class="col-md-5 col-lg-3">
                            <div id="retatime" class="time"><em>AM</em></div>
                            <div id="retaaport" class="airport-code"><span class="city"></span></div>
                        </div>
                        <div id="retprice" class="col-12 headline-text pb-2 text-align-center">                           
                            
                        </div>
                    </div>
                </div>
               <%} %>

                <%if (onwardResults.Count > 0 || (onwardResults.Count > 0 && returnResults.Count > 0))
                    { %>
                               <div class="col-12 col-md-2 book-now-section text-center">
                                   <div id="price" class="price"><span class="currency"></span></div>
                                   <%--<button class="btn btn-primary" onclick="SendBook()">BOOK NOW</button>--%>
                                   <a onclick="BookNow()" class="btn btn-primary" id="bookNowBtn">BOOK NOW</a>
                                   
                                   <div class="flightResultDropdown-wrap">
                                       <div id="DivPrefAirline" class="dropdown flightResultDropdown" style="display: none">
                                           <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                               <img id="ImgPreferredAirline" />
                                           </button>
                                       </div>
                                       <div id="DivInOut" class="dropdown flightResultDropdown" style="display: none">
                                           <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                               <img id="ImgInOutPolicy" />
                                           </button>
                                       </div>
                                       <div id="DivNegotiatedFare" class="dropdown flightResultDropdown" style="display: none">
                                           <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                               <img id="ImgNegotiatedFare" src="images/icon-negotiate.png" />
                                           </button>
                                       </div>
                                       
                                       <div class="corpPolicyTip">
                                           <span id="spnInOutPolicy"></span>
                                       </div>
                                   </div>
                               </div>


                               <div id='divTravelReason' class="dropdown-menu corp-travel-dropDown" style="top:100px" aria-labelledby="dLabel">
                               </div>


                               <%} %>
            </div>
        </div>

        <div class="flight-result-wrapper flight-result-default pt-3">
            <div>
                <div class="email-itenary" id="emailBlock">
                    <div class="fleft search-child-block" style="background-color: Red;">
                        <div class="display-none yellow-back center" id="error">
                        </div>
                    </div>
                    <div class="showMsgHeading">
                        Enter Email Address <a class="closex" href="#" onclick="HideEmailDiv('emailBlock');" style="position: absolute; right: 5px; top: 3px;">X</a>
                    </div>
                    <div class="pad_10">
                        <div id="itineraryCount">
                            Selected 2 Itineraries for Email
                        </div>
                        <div>

                            <input class="form-control" id="addressBox" type="text" name="" />
                            <span style="color: Red" id="errortext"></span>

                        </div>
                       

                           <div style="display: <% =Settings.LoginInfo.IsCorporate=="Y"?"none":"block" %>">
                            Add Markup:
                        </div>
                          <div style="display: <% =Settings.LoginInfo.IsCorporate=="Y"?"none":"block" %>">
                            <input class="form-control" type="text" id="txtMarkup" onkeypress="return isNumber(event);"
                                name="txtMarkup" value="0" maxlength="5" />
                        </div>
                     
                        <div class="fleft center width-100 margin-top-5">
                            <p id="emailStatus" style="color: #18407B; display: none;">
                            </p>
                        </div>
                        <div>
                            <input class="btn but_b" type="button" value="Submit" onclick="SendMail('email')" />
                        </div>
                    </div>
                    <div class=" clear">
                    </div>
                </div>

                <div class="email-itenary" id="downloadBlock">
                    <div class="fleft search-child-block" style="background-color: Red;">
                        <div class="display-none yellow-back center" id="error">
                        </div>
                    </div>
                    <div class="showMsgHeading">
                        Download Selected Iitneraries <a class="closex" href="#" onclick="HideEmailDiv('downloadBlock');" style="position: absolute; right: 5px; top: 3px;">X</a>
                    </div>
                    <div class="pad_10">
                        <div id="itineraryCount1">
                            Selected 2 Itineraries for Download
                        </div>

                       

                         <div style="display: none">
                            Add Markup:
                        </div>
                        <div style="display: none">
                            <input class="form-control" type="text" id="txtDownloadMarkup" onkeypress="return isNumber(event);"
                                name="txtDownloadMarkup" value="0" maxlength="5" />
                        </div>
                      
                        <div class="fleft center width-100 margin-top-5">
                            <p id="emailStatus" style="color: #18407B; display: none;">
                            </p>
                        </div>
                        <div>
                            <input class="btn but_b" type="button" value="Download" onclick="DownloadResults()" />
                        </div>
                    </div>
                    <div class=" clear">
                    </div>
                </div>
            </div>
            <div class="row custom-gutter">
                <div class="col-12 col-md-2">
                    <%if ((onwardResults.Count > 0) || (onwardResults.Count > 0 && returnResults.Count > 0))
                        { %>         
                    <div class="filter-panel d-none d-lg-block">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link active"  id="onward-tab" data-toggle="tab" href="#onward" role="tab" aria-controls="onward" aria-selected="true">ONWARD</a>
                            </li>
                            <!--Display the return tab if both onward and return results are there for only round trip-->
                            <%if (onwardResults.Count > 0 && returnResults.Count > 0) %>
                            <%{ %>
                            <li class="nav-item">
                                <a class="nav-link" id="return-tab" data-toggle="tab" href="#return" role="tab" aria-controls="return" aria-selected="false">RETURN</a>
                            </li>
                             <%} %>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active in" id="onward" role="tabpanel" aria-labelledby="onward-tab">
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        
                                        <a href="javascript:ClearFilters('On')">Clear Filters</a>
                                    </div>
                                    
                                </div>
                                <div class="filter-category airline-filter">
                                    <div class="filter-title">
                                        <div class="custom-checkbox-style">
                                            <input type="checkbox" id="chkOnAllAirlines" onchange="SelectUnSelect('Airlines','On')" checked="checked" class="form-control" />
                                            <label></label>
                                        </div>
                                        <h4>Airlines</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <% int acounter = 0;
                                            foreach (string airline in airlineList)
                                            {
                                        %>
                                        <li class="only-this-list position-relative">
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkOnAirline<%=acounter %>" checked="checked" onchange="FilterByType('aline','On')" class="form-control" />
                                                <label id="lblOnAirline<%=acounter %>"><%=airline %> </label>

                                            </div>
                                            <span  class="onlythis" style="top:5px" onclick="OnlyThis(<%=acounter %>,'On')">Only this</span>
                                        </li>
                                        <%acounter++;
                                            } %>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <div class="custom-checkbox-style">
                                            <input type="checkbox" id="chkOnAllRefund" checked="checked" onchange="SelectUnSelect('Refund','On')" class="form-control" /><label></label>
                                        </div>
                                        <h4>Flight Details</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <%if (onwardRefundableFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkOnRefund" checked="checked" onchange="FilterByType('fare','On')" class="form-control" />
                                                <label>Refundable </label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%}
                                            if (onwardNonRefundableFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkOnNonRefund" checked="checked" onchange="FilterByType('fare','On')" class="form-control" />
                                                <label>Non Refundable </label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <div class="custom-checkbox-style">
                                            <input type="checkbox" id="chkOnAllStops" onchange="SelectUnSelect('Stops','On')" checked="checked" class="form-control" /><label></label>
                                        </div>
                                        <h4>No. of Stops</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <%if (request != null  && onwardNonStopFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkOnNonStop" runat="server" onchange="FilterByType('stops','On')" class="form-control" checked="checked" />
                                                <label>Non Stop</label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%}
                                            if (request != null && onwardOneStopFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkOnOneStop" runat="server" onchange="FilterByType('stops','On')" class="form-control" checked="checked" />
                                                <label>One Stop</label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%}
                                            if (request != null && onwardTwoStopFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input id="chkOnTwoStops" runat="server" type="checkbox" onchange="FilterByType('stops','On')" class="form-control" checked="checked" />
                                                <label id="lblOnTwoStops" runat="server">Two Stops</label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <h4>Sort By</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <li class="mt-3 mb-3">
                                            <select id="onddlPriceSort" onchange="SortResults('On','price',this.value)" class="form-control">
                                                <option selected="selected" value="ASC">Lowest Price</option>
                                                <option value="DESC">Highest Price</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="onddlDepSort" onchange="SortResults('On','dtime',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Departure</option>
                                                <option value="ASC">Shortest</option>
                                                <option value="DESC">Longest</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="onddlArrSort" onchange="SortResults('On','atime',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Arrival</option>
                                                <option value="ASC">Shortest</option>
                                                <option value="DESC">Longest</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="onddlAirSort" onchange="SortResults('On','aline',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Airline</option>
                                                <option value="ASC">Ascending</option>
                                                <option value="DESC">Descending</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="onddlStopSort" onchange="SortResults('On','stops',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Stops</option>
                                                <option value="ASC">Direct</option>
                                                <option value="DESC">Non-Direct</option>

                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <h4>Timing Range</h4>
                                    </div>
                                    <%if (!request.TimeIntervalSpecified)
                                        { %>
                                    <ul class="filter-list list-unstyled">
                                        <li class="mt-3 mb-3">
                                            <div class="timing-range-selector">
                                                <ul>
                                                    <li>
                                                        <button id="btnOnNight" type="button" class="timing-filter night" onclick="FilterTiming('Night','On')" style="background-color: rgb(255, 255, 255);">
                                                            00:00<em>-</em>04:59
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button id="btnOnMorning" type="button" class="timing-filter morning" onclick="FilterTiming('Morning','On')" style="background-color: rgb(255, 255, 255);">
                                                            05:00<em>-</em>11:59
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button id="btnOnNoon" type="button" class="timing-filter noon" onclick="FilterTiming('Noon','On')" style="background-color: rgb(255, 255, 255);">
                                                            12:00<em>-</em>17:59
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button id="btnOnEvening" type="button" class="timing-filter evening" onclick="FilterTiming('Evening','On')" style="background-color: rgb(255, 255, 255);">
                                                            18:00<em>-</em>23:59
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <%}
                                                                             else
                                                                             { %>
                                     
                                   <asp:DropDownList ID="ddlOnTimings" CssClass="form-control" onchange="FilterTiming('All','On')" runat="server" AppendDataBoundItems="true">
                                       <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                   </asp:DropDownList>                                
                                   
                                    <%} %>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <h4>Price Range</h4>
                                        <input type="text" id="onwardamount" onkeypress="return false;" readonly="readonly" style="text-align: center; border: 0; font-weight: bold;width:100%" />
                                    </div>
                                    <div class="pad-5">
                                        <div style="width: 90%; margin: auto" id="slider-range-onward" class="flight-slider-range">
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        
                                        <a href="javascript:ClearFilters('On')">Clear Filters</a>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="tab-pane fade" id="return" role="tabpanel" aria-labelledby="return-tab">
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        
                                        <a href="javascript:ClearFilters('Ret')">Clear Filters</a>
                                    </div>
                                    
                                </div>
                                <div class="filter-category airline-filter">
                                    <div class="filter-title">
                                        <div class="custom-checkbox-style">
                                            <input type="checkbox" id="chkRetAllAirlines" onchange="SelectUnSelect('Airlines','Ret')" checked="checked" class="form-control" />
                                            <label></label>
                                        </div>
                                        <h4>Airlines</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <% acounter = 0;
                                            foreach (string airline in airlineListReturn)
                                            {
                                        %>
                                        <li class="only-this-list position-relative">
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkRetAirline<%=acounter %>" checked="checked" onchange="FilterByType('aline','Ret')" class="form-control" />
                                                <label id="lblRetAirline<%=acounter %>"><%=airline %> </label>

                                            </div>
                                            <span  class="onlythis"  style="top:5px" onclick="OnlyThis(<%=acounter %>,'Ret')">Only this</span>
                                        </li>
                                        <%acounter++;
                                            } %>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <div class="custom-checkbox-style">
                                            <input type="checkbox" id="chkRetAllRefund" onchange="SelectUnSelect('Refund','Ret')" checked="checked" class="form-control" /><label></label>
                                        </div>
                                        <h4>Flight Details</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <%if (returnRefundableFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkRetRefund" checked="checked" onchange="FilterByType('fare','Ret')" class="form-control" />
                                                <label>Refundable </label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%}
                                            if (returnNonRefundableFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkRetNonRefund" checked="checked" onchange="FilterByType('fare','Ret')" class="form-control" />
                                                <label>Non Refundable </label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <div class="custom-checkbox-style">
                                            <input type="checkbox" id="chkRetAllStops" onchange="SelectUnSelect('Stops','Ret')" checked="checked" class="form-control" /><label></label>
                                        </div>
                                        <h4>No. of Stops</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <%if (request != null && returnNonStopFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkRetNonStop" runat="server" onchange="FilterByType('stops','Ret')" class="form-control" checked="checked" />
                                                <label>Non Stop</label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%}
                                            if (request != null && returnOneStopFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input type="checkbox" id="chkRetOneStop" runat="server" onchange="FilterByType('stops','Ret')" class="form-control" checked="checked" />
                                                <label>One Stop</label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%}
                                            if (request != null &&  returnTwoStopFound)
                                            { %>
                                        <li>
                                            <div class="custom-checkbox-style">
                                                <input id="chkRetTwoStops" runat="server" type="checkbox" onchange="FilterByType('stops','Ret')" class="form-control" checked="checked" />
                                                <label id="lblRetTwoStops" runat="server">Two Stops</label>
                                            </div>
                                            <span class="only-this">Only this</span>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <h4>Sort By</h4>
                                    </div>
                                    <ul class="filter-list list-unstyled">
                                        <li class="mt-3 mb-3">
                                            <select id="retddlPriceSort" onchange="SortResults('Ret','price',this.value)" class="form-control">
                                                <option selected="selected" value="ASC">Lowest Price</option>
                                                <option value="DESC">Highest Price</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="retddlDepSort" onchange="SortResults('Ret','dtime',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Departure</option>
                                                <option value="ASC">Shortest</option>
                                                <option value="DESC">Longest</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="retddlArrSort" onchange="SortResults('Ret','atime',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Arrival</option>
                                                <option value="ASC">Shortest</option>
                                                <option value="DESC">Longest</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="retddlAirSort" onchange="SortResults('Ret','aline',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Airline</option>
                                                <option value="ASC">Ascending</option>
                                                <option value="DESC">Descending</option>

                                            </select>
                                        </li>
                                        <li class="mb-3">
                                            <select id="retddlStopSort" onchange="SortResults('Ret','stops',this.value)" class="form-control">
                                                <option selected="selected" value="-1">Stops</option>
                                                <option value="ASC">Direct</option>
                                                <option value="DESC">Non-Direct</option>

                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <h4>Timing Range</h4>
                                    </div>
                                    <%if(!request.TimeIntervalSpecified){ %>
                                    <ul class="filter-list list-unstyled">
                                        <li class="mt-3 mb-3">
                                            <div class="timing-range-selector">
                                                <ul>
                                                    <li>
                                                        <button id="btnRetNight" type="button" class="timing-filter night" onclick="FilterTiming('Night','Ret')" style="background-color: rgb(255, 255, 255);">
                                                            00:00<em>-</em>04:59
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button id="btnRetMorning" type="button" class="timing-filter morning" onclick="FilterTiming('Morning','Ret')" style="background-color: rgb(255, 255, 255);">
                                                            05:00<em>-</em>11:59
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button id="btnRetNoon" type="button" class="timing-filter noon" onclick="FilterTiming('Noon','Ret')" style="background-color: rgb(255, 255, 255);">
                                                            12:00<em>-</em>17:59
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button id="btnRetEvening" type="button" class="timing-filter evening" onclick="FilterTiming('Evening','Ret')" style="background-color: rgb(255, 255, 255);">
                                                            18:00<em>-</em>23:59
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <%}else{ %>
                                    
                                   <asp:DropDownList ID="ddlRetTimings" CssClass="form-control" onchange="FilterTiming('All','Ret')" runat="server" AppendDataBoundItems="true">
                                       <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                   </asp:DropDownList>
                                    <%} %>
                                </div>
                                <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        <h4>Price Range</h4>
                                        <input type="text" id="returnamount" onkeypress="return false;" readonly="readonly" style="text-align: center; border: 0; font-weight: bold;" />
                                    </div>
                                    <div class="pad-5">
                                        <div style="width: 90%; margin: auto" id="slider-range-return" class="flight-slider-range">
                                        </div>
                                    </div>
                                </div>
                                 <div class="filter-category refundable-filter">
                                    <div class="filter-title">
                                        
                                        <a href="javascript:ClearFilters('Ret')">Clear Filters</a>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
                <!--Based on the below hidden field only email itinerary select all function will work -->
                <input type="hidden" id="onwardFiltered" value="<%=onwardResults.Count %>" />
                 <input type="hidden" id="returnFiltered" value="<%=returnResults.Count %>" />

                <div class="col-12 col-lg-10">
                    <%if ((onwardResults.Count > 0) || (onwardResults.Count > 0 && returnResults.Count > 0))
                        { %>
                    <button type="button" id="btnPub" class="btn btn-primary btn-sm btn-flight-details" data-toggle="collapse" style="display:none">
                                                        <span id="lbtext">Hide Pub Fare</span> <span class="font-icon icon-arrow_drop_down"></span>
                                                    </button>
                    <ul class="pagination pull-right mt-2 ml-4">
                        <li><a id="EmailLink" style="display: block; font-weight: bold;" href="javascript:ShowEmailDiv('Email')"><i class="fa fa-envelope"></i>Email Itineraries</a></li>
                        <%if (agency.RequiredItinerary)
    { %>
                        <li><a id="DownloadLink" style="display: block; font-weight: bold;" href="javascript:ShowEmailDiv('Download')"><i class="fa fa-envelope"></i>Download Itineraries</a></li>
                        <%}%>
                    </ul>
                    <br />
                    
                    <div class="listing-panel">
                        <div class="d-flex">
                            <div class="headline-text text-align-center flex-fill">ONWARD</div>
                            <%if (onwardResults.Count > 0 && returnResults.Count > 0) %>
                            <%{ %>
                            <div class="headline-text text-align-center flex-fill">RETURN</div>
                            <%} %>
                        </div>
                        <div class="row no-gutters inner">
                           
                               <%if (onwardResults.Count > 0 && returnResults.Count > 0) %>
                            <%{ %>
                           		 <div class="col-6" id="OnResults">
                            <% }
    else
    { %>
                        		   <div class="col-12" id="OnResults">
 							<% } %>
                           
                          
 
                                <div class="sorting-wrapper d-none d-md-flex">
                                    <div class="flex-fill" style="max-width:120px;">
                                        <a href="javascript:void(0);" id="onDepSort" onclick="DepartureTimeSort('On')" class="flight-sort-btn">Departure <span class="glyphicon glyphicon-arrow-down"></span></a>
                                    </div>
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="onStopSort" onclick="StopsSort('On')" class="flight-sort-btn">Stops <span class="glyphicon glyphicon-arrow-down"></span></a>
                                    </div>
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="onArrSort" onclick="ArrivalTimeSort('On')" class="flight-sort-btn">Arrival <span class="glyphicon glyphicon-arrow-down"></span></a>

                                    </div>
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="onPriceSort" onclick="PriceSort('On')" class="flight-sort-btn">Price <span class="glyphicon glyphicon-arrow-down"></span></a>
                                        
                                        <input type="checkbox" id="chkOnDownAll" title="Select All for Download" onchange="SelectAll('On')" class="position-absolute" style="right: 10px;top: 5px;" />
                                        <%--Select All  --%>
                                    </div>
                                </div>



                                 
                              

                                <!--Start:New Style:Result Clubbing Based On Fare Type With Single Flight Information-->

                               <ul class="list-unstyled flight-list-wrapper">
                                       
                                    <%int counter = 0, counterFD = 0;//Flag which determines whether to display the flight information or not.
                                                                                                              //Step-1:Grouping the results based on the resultKey
                                        var groupedOnwardList = onwardResults
                                           .GroupBy(u => u.ResultKey)
                                           .Select(grp => grp.ToList())
                                           .ToList();
                                        //Step-2:Now Each Group Has set of results.//Iterate through the groups
                                        foreach (var group in groupedOnwardList)
                                        {
                                            SearchResult res = group.FirstOrDefault();
                                            FlightInfo onwardSegment = res.Flights[0][0];
                                            FlightInfo returnSegment = res.Flights[0][res.Flights[0].Length - 1];
                                            TimeSpan totalDuration = new TimeSpan();
                                            List<FlightInfo> flightInfos = new List<FlightInfo>(res.Flights[0]);
                                            //flightInfos.ForEach(f => f.Duration = f.ArrivalTime.Subtract(f.DepartureTime));NOT REQUIRED AT THE MSE UTC DURATION IS ASSIGNED
                                            foreach (FlightInfo seg in flightInfos)
                                            {
                                                totalDuration = totalDuration.Add(seg.Duration);
                                            }
                                            CT.Core.Airline airline = new CT.Core.Airline();

                                %>
                               
                                    <li id="OnResult<%=counter %>" class="item <%=(counter == 0 ? "checked" : "") %>">
                                        <div class="row">
                                           
                                            <div class="col-12">
                                                <img src="images/AirlineLogo/<%=onwardSegment.Airline %>.gif" alt="SG-Airline" class="airline-logo float-left pr-2" />
                                                <%
    airline.Load(onwardSegment.Airline);%>
                                                <span class="airline-name pb-2">
                                                    <%=airline.AirlineName %> <span><%=airline.AirlineCode %> <%=onwardSegment.FlightNumber %></span>
                                                </span>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-3">
                                                <div class="time"><%=onwardSegment.DepartureTime.ToString("HH:mm") %><em><%=onwardSegment.DepartureTime.ToString("tt") %></em></div>
                                                <div class="airport-code"><%=onwardSegment.Origin.AirportCode %><span class="city"><%=onwardSegment.Origin.CityName %></span></div>
                                            </div>
                                           
                                            <div class="col-md-1 col-lg-1  text-align-center">
                                                <i class="font-icon icon-flight-right"></i>
                                          
                                            </div>
                                           
                                            <div class="col-md-5 col-lg-3 separator-right text-right">
                                                <div class="time"><%=returnSegment.ArrivalTime.ToString("HH:mm") %><em><%=returnSegment.ArrivalTime.ToString("tt") %></em></div>
                                                <div class="airport-code"><%=returnSegment.Destination.AirportCode %><span class="city"><%=returnSegment.Destination.CityName %></span></div>
                                            </div>

                                            <div class="col-md-12 col-lg-5 book-now-section  d-flex d-lg-block">
                                                <div class="row no-gutters">
                                                     <div class="col-12 mb-2"> 
                                                         <span class="currency"><%=res.Currency %></span>
                                                     </div>
                                                   <%foreach (SearchResult res1 in group) %>
                                                    <%{ %>

                                        <input type="hidden" id="hdnOnFareType<%=counter %>" value="<%=res1.FareType %>" />
                                        <input type="hidden" id="hdnOnAirline<%=counter %>" value="<%=onwardSegment.Airline %>" />
                                        <input type="hidden" id="hdnOnStops<%=counter %>" value="<%=onwardSegment.Stops %>" />
                                        <input type="hidden" id="hdnOnRefundable<%=counter %>" value="<%=res1.NonRefundable %>" />
                                        <input type="hidden" id="hdnOnDeparture<%=counter %>" value="<%=onwardSegment.DepartureTime %>" />
                                        <input type="hidden" id="hdnOnArrival<%=counter %>" value="<%=onwardSegment.ArrivalTime %>" />
                                        <input type="hidden" id="hdnOnPrice<%=counter %>" value="<%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare) : res1.TotalFare) %>" />
                                        <input type="hidden" id="hdnOnResultId<%=counter %>" value="<%=res1.ResultId %>" />
                                        <input type="hidden" id="hdnOnSource<%=counter %>" value="<%=res1.ResultBookingSource %>" />
                                     
                                                   <div class="col-12 custom-radio-flight"> 
                                                  <label class="control-label">  
                                                   <input type="radio" name="OnwardflightresultItem" id="rbOnResult<%=counter %>" class="flight-radio-button" onchange="SelectResult(<%=counter %>,'ONWARD')">
                                                   <label></label> 
                                                    </label>
                                                        <%decimal discount = res1.FareBreakdown.Sum(x => x.AgentDiscount);%>
                                                       <label class="control-label float-right">   <input type="checkbox" id="chkOnRes-<%=counter %>" onchange="selectBox(this.id,'On')"/> </label>
                                                    <label class="control-label price"> <span class="stops"><%=res1.FareType %></span>
                                                        <%--<span  class="stops">Pub Fare</span><%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare + (double)discount).ToString("N" + decimalValue) : (res1.TotalFare + (double)discount).ToString("N" + decimalValue)) %>--%>
                                                        <%--<span  class="stops">Offer Fare</span>--%> 
                                                        <span class="fare"><%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare).ToString("N" + decimalValue) : res1.TotalFare.ToString("N" + decimalValue)) %></span>
                                                        <%if (res1.TravelPolicyResult != null)
    {
        string pbRules = string.Empty;
        foreach (KeyValuePair<string, List<string>> pair in res1.TravelPolicyResult.PolicyBreakingRules)
        {
            if (pair.Key != "RESTRICTBOOKING" && pair.Key != "COMPLETEBOOKING" && pair.Key != "AVOIDTRANSITTIME" && pair.Key != "BOOKINGTHRESHOLD")
            {
                pbRules += string.Join(",", pair.Value.ToArray());
            }
        }%>
                                                        <span class="stops"><%=res1.TravelPolicyResult.IsUnderPolicy ? "Inside Policy" : "Outside Policy" %> </span>
                                                        <%if (!res1.TravelPolicyResult.IsUnderPolicy)
    { %>
                                                        <span class="stops" title="<%=pbRules %>"><img style="width:15px;height:15px" src="images/icon-outside-policy.png"/></span>
                                                        <%}
    } %>
                                                       </label>                                                    
                                                          </div>                                                    
                                                    <%counter++; %> 
                                                     <%} %>                                                    
                                                </div>
                                            </div>
                                           
                                            <%
    bool displayDetails = true;
    foreach (SearchResult res2 in group) %>
                                                    <%{ %>
                                            <%if (displayDetails) %>
                                            <%{ %>
                                            <div class="col-12 onwardFD" id="divFD<%=counterFD %>">
                                                 <%}%>
                                                <%else %>
                                                <%{ %>
                                                   <div style="display:none" class="col-12 onwardFD" id="divFD<%=counterFD %>">
                                                <%}%>
                                                <button class="btn btn-primary btn-sm btn-flight-details" type="button" data-toggle="collapse" data-target="#flightdetails-<%=res2.ResultId %>" aria-expanded="false" aria-controls="flightdetails-<%=res2.ResultId %>">
                                                    FLIGHT DETAILS <span class="font-icon icon-arrow_drop_down"></span>
                                                </button>
                                                <span class="stops float-left pt-3 px-3"><%=(res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0 ? "NONSTOP" : (res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1 ? "ONESTOP" : "TWOSTOPS")) %>
                                                    <%--<%=res.FareType %>--%></span>
                                                <span class="total-duration">Total Duration:<em><%=(totalDuration.Days >= 1) ? Math.Floor(totalDuration.TotalHours) : totalDuration.Hours %>.<%=totalDuration.Minutes %></em></span>
                                                       
                                                 <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
    {%>
                                                <span class="booking-source small"><%=res2.ResultBookingSource.ToString() %></span>
                                                <%} %>
                                                
                                                <div class="clearfix"></div>
                                                <div class="collapse flighdtl-collapse-content" id="flightdetails-<%=res2.ResultId %>">
                                                    <div class="card card-body">
                                                        <%--Flight Details Tabbed Panel--%>

                                                        <ul class="nav nav-pills mb-3" id="flightdetails-tab" role="tablist">
                                                            <li class="nav-item active">
                                                                <a class="nav-link active" data-toggle="pill" href="#pills-flightInfo-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-flightInfo-tab-<%=res2.ResultId %>" aria-selected="true">Flight Info</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-fareRules-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-fareRules-tab-<%=res2.ResultId %>" onclick="GetFareRule(<%=res2.ResultId %>,'<%=Session["SessionId"].ToString() %>','ONWARD',<%=counterFD %>)" aria-selected="false">Fare Rules</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-baggage-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-baggage-tab-<%=res2.ResultId %>" aria-selected="false">Baggage</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="flightdetails-tabContent">
                                                            <div class="tab-pane fade show active in" id="pills-flightInfo-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                <table id="ctl00_cphTransaction_dlFlightResults_ctl03_flightDetails" class="flight_details_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <%foreach (FlightInfo seg in res2.Flights[0])
    {
        airline.Load(seg.Airline);
                                                                        %>
                                                                        <tr>
                                                                            <td width="40px" align="Center" class="f1"><span class="p20">
                                                                                <img src="images/AirlineLogo/<%=seg.Airline %>.GIF" width="50px" height="35px"></span><br>
                                                                                <%=airline.AirlineName %><br>
                                                                                (<%=seg.Airline %>) <%=seg.FlightNumber %><br>
                                                                            </td>
                                                                            <td class="f2"><%=seg.Origin.CityName %><br>
                                                                                <%=seg.DepartureTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br>
                                                                                Terminal: <%=seg.DepTerminal %></td>
                                                                            <td class="f3"><%=seg.Destination.CityName %><br>
                                                                                <%=seg.ArrivalTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br />
                                                                                Terminal: <%=seg.ArrTerminal %></td>
                                                                            <td class="f4"><%=(seg.Duration.Days>=1) ? Math.Floor(seg.Duration.TotalHours) : seg.Duration.Hours %>Hrs <%=seg.Duration.Minutes %>Mins<br>
                                                                                <%=(string.IsNullOrEmpty(seg.CabinClass) ? seg.BookingClass : seg.CabinClass + "-" + seg.BookingClass) %><br>
                                                                                <br>
                                                                            </td>
                                                                            <td rowspan="1" class="f5">Aircraft:  <%=seg.Craft %><span class="cccline">|</span> <span class="cccline">|</span><%=(res2.NonRefundable ? "Non Refundable" : "Refundable") %></td>
                                                                        </tr>
                                                                        <%if (res2.Flights[0].Length > 0)
    { %>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <div class="flight_amid_info"></div>
                                                                            </td>
                                                                        </tr>
                                                                        <%}
    } %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-fareRules-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-fareRules-tab-<%=res2.ResultId %>">
                                                                <div class="height-200 overflow-hidden" style="overflow-y: auto">
                                                                    <div class="small" id="onFareRules<%=counterFD %>">
                                                                        <div class="padding-10 refine-result-bg-color">
                                                                            Please wait ...
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-baggage-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-baggage-tab-<%=res2.ResultId %>">
                                                                <table class="table table-bordered small" width="100%" border="1" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th style="width: 50%"><strong>Sector / Flight</strong></th>
                                                                            <th style="width: 50%"><strong>Baggage</strong></th>
                                                                        </tr>
                                                                        <%for (int i = 0; i < res2.Flights[0].Length; i++)
    {
        FlightInfo seg = res2.Flights[0][i];
                                                                        %>
                                                                        <tr>
                                                                            <td><%=seg.Origin.AirportCode %> - <%=seg.Destination.AirportCode %> <%=seg.Airline %> <%=seg.FlightNumber %></td>
                                                                              <%if (!string.IsNullOrEmpty(seg.DefaultBaggage))
    {%>
                                                                                        <td><%=(seg.DefaultBaggage)%> </td><%}%>
                                                                                        <%else
    {%>
                                                                                          <td>As Per Airline Policy</td>
                                                                                          <%} %>
                                                                        </tr>
                                                                        <%} %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <%counterFD++; displayDetails = false; %> 
                                            <%} %>

                                        
                                            </div>
                                            
                                    </li>
                                  
                                

                                    

                                            
                                       <%}  %>

                                   </ul>

                                <!--End:New Style :Result Clubbing Based On Fare Type With Single Flight Information-->


                               



                            </div><!--End of Onward Results Div -->
                            
                            <%}
    if (onwardResults.Count > 0 && returnResults.Count > 0) %>
                            <%{ %>
                            <div id="RetResults" class="col-6">
                                
                
                                <div class="sorting-wrapper d-none d-md-flex">
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="retDepSort" onclick="DepartureTimeSort('Ret')" class="flight-sort-btn">Departure <span class="glyphicon glyphicon-arrow-down"></span></a>
                                    </div>
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="retStopSort" onclick="StopsSort('Ret')" class="flight-sort-btn">Stops <span class="glyphicon glyphicon-arrow-down"></span></a>
                                    </div>
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="retArrSort" onclick="ArrivalTimeSort('Ret')" class="flight-sort-btn">Arrival <span class="glyphicon glyphicon-arrow-down"></span></a>

                                    </div>
                                    <div class="flex-fill">
                                        <a href="javascript:void(0);" id="retPriceSort" onclick="PriceSort('Ret')" class="flight-sort-btn">Price <span class="glyphicon glyphicon-arrow-down"></span></a>
                                        
                                        <input type="checkbox" id="chkRetDownAll" title="Select All for Download" onchange="SelectAll('Ret')" class="position-absolute" style="right: 10px;top: 5px;" />
                                       <%-- Select All  --%>
                                    </div>
                                </div>

                                <!--Start:New Style:Result Clubbing Based On Fare Type With Single Flight Information-->
                                <ul class="list-unstyled flight-list-wrapper">
                                    <%int counterR = 0,counterRFD = 0;//Flag which determines whether to display the flight information or not.
                                                                       //Step-1:Grouping the results based on the resultKey;
                                        var groupedReturnList = returnResults
                                               .GroupBy(u => u.ResultKey)
                                               .Select(grp => grp.ToList())
                                               .ToList();
                                        //Step-2:Now Each Group Has set of results.//Iterate through the groups
                                        foreach (var group in groupedReturnList)
                                        {
                                            SearchResult res = group.FirstOrDefault();
                                            FlightInfo onwardSegment = res.Flights[0][0];
                                            FlightInfo returnSegment = res.Flights[0][res.Flights[0].Length - 1];
                                            TimeSpan totalDuration = new TimeSpan();
                                            List<FlightInfo> flightInfos = new List<FlightInfo>(res.Flights[0]);
                                            //flightInfos.ForEach(f => f.Duration = f.ArrivalTime.Subtract(f.DepartureTime));NOT REQUIRED AT THE MSE UTC DURATION IS ASSIGNED
                                            foreach(FlightInfo seg in flightInfos)
                                            {
                                                totalDuration = totalDuration.Add(seg.Duration);
                                            }
                                            CT.Core.Airline airline = new CT.Core.Airline();
                                            %>
                                    
                                    <li id="RetResult<%=counterR %>" class="item <%=(counterR==0?"checked":"") %>">
                                       
                                        <div class="row">
                                           
                                            <div class="col-12">
                                                <img src="images/AirlineLogo/<%=onwardSegment.Airline %>.gif" alt="SG-Airline" class="airline-logo float-left pr-2" />
                                                <%
                                                    airline.Load(onwardSegment.Airline);%>
                                                <span class="airline-name pb-2">
                                                    <%=airline.AirlineName %> <span><%=airline.AirlineCode %> <%=onwardSegment.FlightNumber %></span>
                                                </span>
                                            </div>
                                           
                                            <div class="col-md-5 col-lg-3">
                                                <div class="time"><%=onwardSegment.DepartureTime.ToString("HH:mm") %><em><%=onwardSegment.DepartureTime.ToString("tt") %></em></div>
                                                <div class="airport-code"><%=onwardSegment.Origin.AirportCode %><span class="city"><%=onwardSegment.Origin.CityName %></span></div>
                                            </div>

                                            <div class="col-md-1 col-lg-1  text-align-center">
                                                <i class="font-icon icon-flight-right"></i>
                                                
                                            </div>
                                           
                                            <div class="col-md-5 col-lg-3 separator-right text-right">
                                                <div class="time"><%=returnSegment.ArrivalTime.ToString("HH:mm") %><em><%=returnSegment.ArrivalTime.ToString("tt") %></em></div>
                                                <div class="airport-code"><%=returnSegment.Destination.AirportCode %><span class="city"><%=returnSegment.Destination.CityName %></span></div>
                                            </div>


                                            <div class="col-md-12 col-lg-5 book-now-section  d-flex d-lg-block">
                                                <div class="row no-gutters">
                                                     <div class="col-12 mb-2"> 
                                                         <span class="currency"><%=res.Currency %></span>
                                                     </div>
                                                   <%foreach (SearchResult res1 in group) %>
                                                    <%{ %>

                                        <input type="hidden" id="hdnRetFareType<%=counterR %>" value="<%=res1.FareType %>" />
                                        <input type="hidden" id="hdnRetAirline<%=counterR %>" value="<%=onwardSegment.Airline %>" />
                                        <input type="hidden" id="hdnRetStops<%=counterR %>" value="<%=onwardSegment.Stops %>" />
                                        <input type="hidden" id="hdnRetRefundable<%=counterR %>" value="<%=res1.NonRefundable %>" />
                                        <input type="hidden" id="hdnRetDeparture<%=counterR %>" value="<%=onwardSegment.DepartureTime %>" />
                                        <input type="hidden" id="hdnRetArrival<%=counterR %>" value="<%=onwardSegment.ArrivalTime %>" />
                                        <input type="hidden" id="hdnRetPrice<%=counterR %>" value="<%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare) : res1.TotalFare) %>" />
                                        <input type="hidden" id="hdnRetResultId<%=counterR %>" value="<%=res1.ResultId %>" />
                                        <input type="hidden" id="hdnRetSource<%=counterR %>" value="<%=res1.ResultBookingSource %>" />

                                                   <div class="col-12 custom-radio-flight"> 
                                                  <label class="control-label">  
                                                   <input type="radio" name="ReturnflightresultItem" id="rbRetResult<%=counterR %>" class="flight-radio-button" onchange="SelectResult(<%=counterR %>,'RETURN')" />
                                                   <label></label> 
                                                    </label>
                                                        <%decimal discount = res1.FareBreakdown.Sum(x => x.AgentDiscount);%>
                                                       <label class="control-label float-right">  <input type="checkbox" id="chkRetRes-<%=counterR %>" onchange="selectBox(this.id,'Ret')"/></label>
                                                    <label class="control-label price"> <span class="stops"><%=res1.FareType %></span>
                                                        <%--<span  class="stops">Pub Fare</span><%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare + (double)discount).ToString("N" + decimalValue) : (res1.TotalFare + (double)discount).ToString("N" + decimalValue)) %>--%>
                                                        <%--<span  class="stops">Offer Fare</span>--%> 
                                                        <span class="fare"><%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare).ToString("N" + decimalValue) : res1.TotalFare.ToString("N" + decimalValue)) %></span>
                                                    <%if (res1.TravelPolicyResult != null)
    {
        string pbRules = string.Empty;
        foreach (KeyValuePair<string, List<string>> pair in res1.TravelPolicyResult.PolicyBreakingRules)
        {
            if (pair.Key != "RESTRICTBOOKING" && pair.Key != "COMPLETEBOOKING" && pair.Key != "AVOIDTRANSITTIME" && pair.Key != "BOOKINGTHRESHOLD")
            {
                pbRules += string.Join(",", pair.Value.ToArray());
            }
        }%>
                                                        <span class="stops"><%=res1.TravelPolicyResult.IsUnderPolicy ? "Inside Policy" : "Outside Policy" %> </span>
                                                        <%if (!res1.TravelPolicyResult.IsUnderPolicy)
    { %>
                                                        <span class="stops" title="<%=pbRules %>"><img style="width:15px;height:15px" src="images/icon-outside-policy.png"/></span>
                                                        <%}
    } %>   
                                                    </label>
                                                    
                                                    
                                                          </div>
 <%counterR++; %> 
                                                     <%} %>


                                                     
                                                </div>
                                            </div>

                                           
                                              <% bool displayDetails = true;
                                                  foreach (SearchResult res2 in group) %>
                                                    <%{ %>
                                         <%if (displayDetails) %>
                                            <%{ %>
                                            <div class="col-12 returnFD" id="divRFD<%=counterRFD %>">
                                                 <%}%>
                                                <%else %>
                                                <%{ %>
                                                   <div style="display:none" class="col-12 returnFD" id="divRFD<%=counterRFD %>">
                                                <%}%>
                                                <button class="btn btn-primary btn-sm btn-flight-details" type="button" data-toggle="collapse" data-target="#flightdetails-<%=res2.ResultId %>" aria-expanded="false" aria-controls="flightdetails-1">
                                                    FLIGHT DETAILS <span class="font-icon icon-arrow_drop_down"></span>
                                                </button>
                                                       <span class="stops float-left pt-3 px-3"><%=(res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0 ? "NONSTOP" : (res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1 ? "ONESTOP" : "TWOSTOPS")) %>
                                                   <%-- <%=res.FareType %>--%></span>
                                                <span class="total-duration">Total Duration:<em><%=(totalDuration.Days >= 1) ? Math.Floor(totalDuration.TotalHours) : totalDuration.Hours %>.<%=totalDuration.Minutes %></em></span>                                               

                                                        <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                                    {%>
                                                <span class="booking-source small"><%=res2.ResultBookingSource.ToString() %></span>
                                                <%} %>
                                                <div class="clearfix"></div>
                                                <div class="collapse flighdtl-collapse-content" id="flightdetails-<%=res2.ResultId %>">
                                                    <div class="card card-body">
                                                        <%--Flight Details Tabbed Panel--%>

                                                        <ul class="nav nav-pills mb-3" id="flightdetails-tab" role="tablist">
                                                            <li class="nav-item active">
                                                                <a class="nav-link active" data-toggle="pill" href="#pills-flightInfo-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-flightInfo-tab-<%=res2.ResultId %>" aria-selected="true">Flight Info</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-fareRules-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-fareRules-tab-<%=res2.ResultId %>" onclick="GetFareRule(<%=res2.ResultId %>,'<%=Session["SessionId"].ToString() %>','RETURN',<%=counterRFD %>)" aria-selected="false">Fare Rules</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-baggage-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-baggage-tab-<%=res2.ResultId %>" aria-selected="false">Baggage</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="flightdetails-tabContent">
                                                            <div class="tab-pane fade show active in" id="pills-flightInfo-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                <table id="ctl00_cphTransaction_dlFlightResults_ctl03_flightDetails" class="flight_details_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <%foreach (FlightInfo seg in res2.Flights[0])
    {
        airline.Load(seg.Airline);
                                                                        %>
                                                                        <tr>
                                                                            <td width="40px" align="Center" class="f1"><span class="p20">
                                                                                <img src="images/AirlineLogo/<%=seg.Airline %>.GIF" width="50px" height="35px"></span><br>
                                                                                <%=airline.AirlineName %><br>
                                                                                (<%=seg.Airline %>) <%=seg.FlightNumber %><br>
                                                                            </td>
                                                                            <td class="f2"><%=seg.Origin.CityName %><br>
                                                                                <%=seg.DepartureTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br>
                                                                                Terminal: <%=seg.DepTerminal %></td>
                                                                            <td class="f3"><%=seg.Destination.CityName %><br>
                                                                                <%=seg.ArrivalTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br />
                                                                                Terminal: <%=seg.ArrTerminal %></td>
                                                                            <td class="f4"><%=(seg.Duration.Days>=1)? Math.Floor(seg.Duration.TotalHours) : seg.Duration.Hours %>Hrs <%=seg.Duration.Minutes %>Mins<br>
                                                                                <%=(string.IsNullOrEmpty(seg.CabinClass) ? seg.BookingClass : seg.CabinClass + "-" + seg.BookingClass) %><br>
                                                                                <br>
                                                                            </td>
                                                                            <td rowspan="1" class="f5">Aircraft:  <%=seg.Craft %><span class="cccline">|</span> <span class="cccline">|</span><%=(res2.NonRefundable ? "Non Refundable" : "Refundable") %></td>
                                                                        </tr>
                                                                        <%if (res2.Flights[0].Length > 0)
    { %>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <div class="flight_amid_info"></div>
                                                                            </td>
                                                                        </tr>
                                                                        <%}
    } %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-fareRules-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-fareRules-tab-<%=res2.ResultId %>">
                                                                <div class="height-200 overflow-hidden" style="overflow-y: auto">
                                                                    <div class="small" id="retFareRules<%=counterRFD %>">
                                                                        <div class="padding-10 refine-result-bg-color">
                                                                            Please wait ...
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-baggage-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-baggage-tab-<%=res2.ResultId %>">
                                                                <table id="" class="table table-bordered small" width="100%" border="1" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th style="width: 50%"><strong>Sector / Flight</strong></th>
                                                                            <th style="width: 50%"><strong>Baggage</strong></th>
                                                                        </tr>
                                                                        <%for (int i = 0; i < res2.Flights[0].Length; i++)
    {
        FlightInfo seg = res2.Flights[0][i];
                                                                        %>
                                                                        <tr>
                                                                            <td><%=seg.Origin.AirportCode %> - <%=seg.Destination.AirportCode %> <%=seg.Airline %> <%=seg.FlightNumber %></td>
                                                                           <%if (!string.IsNullOrEmpty(seg.DefaultBaggage)){%>
                                                                                        <td><%=(seg.DefaultBaggage)%> </td><%}%>
                                                                                        <%else
                                                                                          {%>
                                                                                          <td>As Per Airline Policy</td>
                                                                                          <%} %>
                                                                        <%} %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%counterRFD++; displayDetails = false; %>
                                            <%} %>



                                        </div>
                                    </li>
                                
                                            
                                       <%}  %>

                                         </ul>

                                <!--End:New Style :Result Clubbing Based On Fare Type With Single Flight Information-->
                            </div><!--End of Return Results Div -->
                            <%} %>
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div id="FlightPreLoader" style="width: 100%; z-index: 999; display: none;">
        <div class="loadingDiv">
            <div id="imgFlightLoading">
                <img id="imgPreloader" height="200px" src="https://gocozmo.com/images/preloaderFlight.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Please wait... </strong>
            </div>
            <div style="font-size: 21px;">
                We are searching for the best available flights</div>
            <div class="" id="onetwoDiv">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table id="onewaytable" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="2%">
                                        <img src="images/departure_flight.png" width="16" height="15" />
                                    </td>
                                    <td width="98%" align="left">
                                        <label class="primary-color">
                                            <strong><span id="flightOnwards"></span></strong>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        
                                    </td>
                                    <td align="left">
                                        <span style="font-size: 21px" id="deptDate"></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table id="twowaytable" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="2%">
                                        <img src="images/arrival_flight.png" width="16" height="15" />
                                    </td>
                                    <td width="98%" align="left">
                                        <label class="primary-color">
                                            <strong><span id="flightReturn"></span></strong>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        
                                    </td>
                                    <td align="left">
                                        <span style="font-size: 21px" id="arrivalDate"></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="" id="multiDiv" style="display: none;">
                <center>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table id="multiwayable1" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway1"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate1"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table id="multiwayable2" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway2"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate2"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="multiwayable3" style="display: none;" width="90%" border="0" align="right"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway3"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate3"></span>
                                        </td>
                                    </tr>
                                </table>
                                <td>
                                    <table id="multiwayable4" style="display: none;" width="90%" border="0" align="right"
                                        cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="2%">
                                                <img src="images/departure_flight.png" width="16" height="15" />
                                            </td>
                                            <td width="98%" align="left">
                                                <label class="primary-color">
                                                    <strong><span id="multiway4"></span></strong>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                                
                                            </td>
                                            <td align="left">
                                                <span style="font-size: 21px" id="multiDate4"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="multiwayable5" style="display: none;" width="90%" border="0" align="right"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway5"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate5"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table id="multiwayable6" style="display: none;" width="90%" border="0" align="right"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway6"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate6"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
    <div id="PreLoader" style="width: 100%; z-index: 999; top: 0px; display: none;">
        <div class="loadingDiv">
            <div id="Div2">
                <img id="img1" height="200px" src="https://gocozmo.com/images/preloaderFlight.gif" />
            </div>
            <div style="font-size: 16px; color: #999999"><strong>Please wait... </strong></div>
            <div style="font-size: 21px;">We're re-confirming availability on the chosen flights...</div>
        </div>
    </div>
    <script>

       

        $("#btnPub").click(function () {
            $(".pub").toggle();

            if ($('#lbtext').html() == "Hide Pub Fare") {
                $('#lbtext').html("Show Pub Fare");                
            }
            else {
                $('#lbtext').html("Hide Pub Fare");                
            }
        });

        function ShowDefaultValues() {
            <%if((onwardResults.Count > 0) || (onwardResults.Count > 0 && returnResults.Count > 0)){%>
            //console.log("onward count: " + onwardResultJson.length + " return count:" + returnResultJson.length);
            $('#rbOnResult0').click();
            $('#rbRetResult0').click();
            //$('#OnResult0').addClass('checked');
            //$('#RetResult0').addClass('checked');
            //$("#OnResult0").click(function () {
            //    $(this).closest('.flight-list-wrapper').find('.item').removeClass('checked')
            //    if ($(this).prop("checked")) {
            //        $(this).closest('.item').addClass('checked')
            //    }
            //});
            //$("#RetResult0").click(function () {
            //    $(this).closest('.flight-list-wrapper').find('.item').removeClass('checked')
            //    if ($(this).prop("checked")) {
            //        $(this).closest('.item').addClass('checked')
            //    }
            //});

            <%}%>
            disablefield();  
            <%if(Settings.LoginInfo.OnBehalfAgentLocation > 0) {%> 
            LoadAgentLocations();            
            <%}%>

            //Added by suresh V to display prefered Airlines .
            var prefAirlines = '<%=preferredAirlines %>';        
        
        if (prefAirlines.split(',').length > 1) {//additional preferred airlines are added then add them for modify search

            if (prefAirlines.split(',').length >= 2) {
                document.getElementById('removeRowLink0').style.display = 'block'; //show remove link
                document.getElementById('txtPreferredAirline0').value = prefAirlines.split(',')[1];
                document.getElementById('divPrefAirline0').style.display = 'block';
                prow = 1;
            }
            if (prefAirlines.split(',').length >= 3) {
                document.getElementById('removeRowLink1').style.display = 'block'; //show remove link
                document.getElementById('txtPreferredAirline1').value = prefAirlines.split(',')[2];
                document.getElementById('divPrefAirline1').style.display = 'block';
                prow = 2;
            }
            if (prefAirlines.split(',').length >= 4) {
                document.getElementById('removeRowLink2').style.display = 'block'; //show remove link
                document.getElementById('txtPreferredAirline2').value = prefAirlines.split(',')[3];
                document.getElementById('divPrefAirline2').style.display = 'block';
                document.getElementById('addRowLink').style.display = 'none'; //hide add link
                prow = 3;
            }
            
        }
        }

        $(document).ready(function () {
            ShowDefaultValues();
            $("#mainContentDiv").on( 'click','.flight-radio-button', function () {
                $(this).closest('.flight-list-wrapper').find('.item.checked').removeClass('checked')

                if ($(this).prop("checked")) {
                    $(this).closest('.item').addClass('checked')
                }
            });
        });


       <%-- Sys.Application.add_load(function() {
            ShowFirstResultDetails();
            disablefield();  
            <%if(Settings.LoginInfo.OnBehalfAgentLocation > 0) {%> 
            LoadAgentLocations();            
            <%}%>
        });--%>

       
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

