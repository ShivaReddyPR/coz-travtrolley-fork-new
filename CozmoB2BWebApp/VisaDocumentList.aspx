﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="VisaDocumentList" Title="Cozmo Travels" Codebehind="VisaDocumentList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    
    
    
    
            <div> <h4>Visa Document List</h4> </div>
   
        <div class=" bg_white bor_gray pad_10 paramcon">
    <div class="col-md-2"> <label class="pull-right fl_xs">
                 Search by country:</label></div>
    
    <div class="col-md-2"> 
    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                </asp:DropDownList></div>
                
    <div class="col-md-2"> <label class="pull-right fl_xs">
                    Agent:
                </label></div>
    <div class="col-md-2"> <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server">
                </asp:DropDownList></div>
    <div class="col-md-2 xspadtop10">
    

    
    <asp:Button ID="btnSearch" CssClass="but but_b" runat="server" Text="Search" OnClick="btnSearch_Click" />
    
    
     </div>
    <div class="col-md-2"> 
    <asp:HyperLink ID="HyperLink1" CssClass="fcol_blue" runat="server" NavigateUrl="~/AddVisaDocument.aspx">Add New Visa Document </asp:HyperLink>
    
    
    
    </div>
       
    
    <div class="clearfix"> </div> 
    </div>
  
  
      <div style=" padding-top:10px">
                <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
            </div>
    
  
           
           
                <div class="table-responsive margin-top-10">  
        
            <asp:GridView ID="GridView1" runat="server" CssClass="datagrid" AutoGenerateColumns="False"
                CellPadding="1" Width="100%" DataKeyNames="documentId" ForeColor="#333333" GridLines="None"
                OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" AllowPaging="True"
                OnPageIndexChanging="GridView1_PageIndexChanging">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <Columns>
                <asp:BoundField DataField="agency" HeaderText="Agent" ReadOnly="True">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="80px" />
            </asp:BoundField>
                    <asp:BoundField DataField="documentType" HeaderText="Document Type" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="122px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="documentDescription" HeaderText="Description">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle Width="322px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="countryCode" HeaderText="Country Code">
                        <HeaderStyle HorizontalAlign="left" Width="82px" />
                        <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="isActive" HeaderText="Is Active" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="left" Width="60px" />
                        <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="documentId" HeaderText="Edit"
                        DataNavigateUrlFormatString="AddVisaDocument.aspx?id={0}">
                        
                        <HeaderStyle HorizontalAlign="left" Width="30px" />
                        <ItemStyle HorizontalAlign="Left" Width="30px" />
                    </asp:HyperLinkField>
                    <asp:TemplateField HeaderText="Activate/Deactivate">
                        <HeaderStyle HorizontalAlign="left" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="8px" />
                        <ItemTemplate>
                            <asp:LinkButton ID="linkButtonStatus" runat="server"  Text="Activate"
                                CommandName="ChangeStatus">
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                  <RowStyle />
        <EditRowStyle />
        <SelectedRowStyle  Font-Bold="True" />
        <PagerStyle HorizontalAlign="Left" Font-Bold="True" />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
         <AlternatingRowStyle CssClass="altrow" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
            NextPageText="Next" PreviousPageText="Previous" />
    </asp:GridView>
        
        
        </div>
 

</asp:Content>
