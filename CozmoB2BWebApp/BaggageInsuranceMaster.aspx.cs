﻿using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using System.IO;
using CT.TicketReceipt.DataAccessLayer;

namespace CozmoB2BWebApp
{
    public partial class BaggageInsuranceMasterGUI : CT.Core.ParentPage
    {
        private string BaggageInsurance_SESSION = "_BaggageInsuranceMaster";
        private string BaggageInsurance_SEARCH_SESSION = "_BaggageInsuranceSearchList";

        protected void Page_Load(object sender, EventArgs e)
           
        {
            this.Master.PageRole = true;

            try
            {
                lblSuccessMsg.Text = string.Empty;
                if (!IsPostBack)
                {

                }
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                Utility.Alert(this.Page, ex.Message);

            }
        }
        
        #region Baggageinsurance currentobject
        private BaggageInsuranceMaster CurrentObject
        {
            get
            {
                return (BaggageInsuranceMaster)Session[BaggageInsurance_SESSION];
            }
            set
            {
                if(value==null)
                {
                    Session.Remove(BaggageInsurance_SESSION);
                }
                else
                {
                    Session[BaggageInsurance_SESSION] = value;
                }
            }
        }
        #endregion
        
        #region Baggageinsurance clicking event
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion

        #region Methods

        private void bindSearch()
        {
    
            try
            {
            
                DataTable dt = BaggageInsuranceMaster.GetAdditionalServiceMasterDetails();
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
                if(dt.Rows.Count < 0)
                {
                    lblError.Text = "Record not found!";
                }
            }
          
            catch
            {
                throw;
            }
            
        }
        private DataTable SearchList
        {
            get
            {
                return (DataTable)Session[BaggageInsurance_SEARCH_SESSION];
            }
            set
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["PLAN_ID"] };
                Session[BaggageInsurance_SEARCH_SESSION] = value;
            }
        }
        private void Edit(int id)
        {
            try
            {
                BaggageInsuranceMaster objbaggageInsurance = new BaggageInsuranceMaster(id);
                CurrentObject = objbaggageInsurance;
                txtPlanCode.Text = objbaggageInsurance.PlanCode;
                txtPlanTitle.Text = objbaggageInsurance.PlanTitle;
                txtPlanDescription.Text = objbaggageInsurance.PlanDescription;
                txtPlanMarketingPoints.Text = objbaggageInsurance.PlanMarketingPoints;
                txtPlanAdultPrice.Text = ((decimal)(objbaggageInsurance.PlanAdultPrice)).ToString();

                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch
            {
                throw;
            }
        }
        private void Clear()
        {
            txtPlanCode.Text = string.Empty;
            txtPlanDescription.Text = string.Empty;
            txtPlanTitle.Text = string.Empty;
            txtPlanMarketingPoints.Text = string.Empty;
            txtPlanAdultPrice.Text = string.Empty;
        }
        #endregion

        #region Button clicking events
        private void Save()
        {
            try
            {
                BaggageInsuranceMaster objbaggageInsurance;
                if (CurrentObject == null)
                {
                    objbaggageInsurance = new BaggageInsuranceMaster();
                }
                else
                {
                    objbaggageInsurance = CurrentObject;
                }
                string plancode = string.Empty;
                objbaggageInsurance.PlanCode = txtPlanCode.Text;
                objbaggageInsurance.PlanTitle = txtPlanTitle.Text;
                objbaggageInsurance.PlanDescription = txtPlanDescription.Text;
                objbaggageInsurance.PlanMarketingPoints = txtPlanMarketingPoints.Text;
                objbaggageInsurance.PlanAdultPrice = Decimal.Parse(txtPlanAdultPrice.Text);
                //int planid= objbaggageInsurance.Save();
                objbaggageInsurance.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("BaggageInsuranceMaster", plancode, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
                btnSave.Text = "Save";
                Clear();

            }
            catch
            {
                throw;
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void FilterSearch_Click(object sender, EventArgs e)
        {
            try
            {

                string[,] textboxesNColumns ={{ "HTtxtCode", "PLAN_CODE" },{"HTtxtTitle", "PLAN_TITLE" }};
                CommonGrid g = new CommonGrid();
                g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion

        #region  grideview events
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                FilterSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
       
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                Int32 PId = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(PId);
                this.Master.HideSearch();
               
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion
    }
}
