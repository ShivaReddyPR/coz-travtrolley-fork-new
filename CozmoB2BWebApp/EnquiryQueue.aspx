﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="EnquiryQueueGUI" Title="Enquiry Queue" Codebehind="EnquiryQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--<script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>--%>
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="JSLib/prototype.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<script type="text/javascript" language="javascript">
function init() {

        //    showReturn();
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal1.cfg.setProperty("title", "Select From Date");
        cal1.cfg.setProperty("close", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();

        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
        cal2.cfg.setProperty("title", "Select To Date");
        cal2.selectEvent.subscribe(setDate2);
        cal2.cfg.setProperty("close", true);
        cal2.render();
    }

    function showCalendar1() {
       document.getElementById('container2').style.display = "none";
       document.getElementById('container1').style.display = "block";

    }

    function showCalendar2() {
        document.getElementById('container1').style.display = "none";
        cal1.hide();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');

          //  var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

            //cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            //cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal2.render();
        }
        document.getElementById('container2').style.display = "block";
    }


    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];

        document.getElementById('IShimFrame').style.display = "none";
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
         
        departureDate = cal1.getSelectedDates()[0];
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        //			
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        var time = "";
        var timePM = "";

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");

        document.getElementById("<%=txtFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
 

        //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
        //cal2.render();

        cal1.hide();

    }
    function setDate2() {
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }

        var date2 = cal2.getSelectedDates()[0];

        var depDateArray = date1.split('/');

        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        //var thisTime = this.today.getHours();
        //var thisMinutes = this.today.getMinutes();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }

        document.getElementById("<%=txtTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();


        cal2.hide();
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
     
    </script>
    
   <script type="text/javascript">
         function switchViews(obj,id) {
             document.getElementById('divDesc').style.display = "block";
             var positions = getRelativePositions(document.getElementById(id));
             document.getElementById('divDesc').style.left = (positions[0] - 600) + 'px';
             document.getElementById('divDesc').style.top = (positions[1] + 15) + 'px';
             var remarks = document.getElementById('ctl00_cphTransaction_dlEnquiry_ctl0' + obj + '_hdnRemarks').value;
             document.getElementById('<%= lblRemarks.ClientID %>').innerHTML = remarks;
         }
         function AddRemarks(obj,id) {
             document.getElementById('divRemarks').style.display = "block";
             var positions = getRelativePositions(document.getElementById(id));
             document.getElementById('divRemarks').style.left = '50%';//Code Changed by Lokesh Kaviti to render the div same on all devices.
             document.getElementById('divRemarks').style.top = (positions[1] + 15) + 'px';
             document.getElementById('<%= hdnEnquiryId.ClientID %>').value = obj;
         }

         function HideDesc() {
             document.getElementById('divDesc').style.display = "none";
             document.getElementById('<%= lblRemarks.ClientID %>').value = "";
             return false;
         }
         function validateRemarks() {
             if (getElement('txtRemarks').value == '') addMessage('Remarks cannot be blank!', '');
             if (getMessage() != '') {
                 alert(getMessage()); clearMessage(); return false;
             }
         }
         function HideDelete() {
             document.getElementById('divRemarks').style.display = "none";
             document.getElementById('<%= txtRemarks.ClientID %>').value = "";
             return false;
         }

         function getRelativePositions(obj) {
             var curLeft = 0;
             var curTop = 0;
             if (obj.offsetParent) {
                 do {
                     curLeft += obj.offsetLeft;
                     curTop += obj.offsetTop;
                 } while (obj = obj.offsetParent);

             }
             return [curLeft, curTop];
         }
    </script>


<div class="ns-h3"> Enquire Queue  </div>






 <div>


              <div id="container1" style="position: absolute;top:140px; left:20%; display: none; z-index:9999">
                                            </div>
                                                 <div class="clear" style="margin-left:30px">
                                            <div id="container2" style="position: absolute; top:140px;left:46%; z-index:9999; display: none;">
                                            </div>
                                        </div>
                                        
                                        
                                     
  
    <div class="paramcon " title="Param" id="divParam">
        <div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>
        
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
        
        
        <div class="bg_white bor_gray"> 
        
           <div class="col-md-12 padding-0 marbot_10">                                      

 
    <div class="col-md-2 col-xs-4 marxs_top5"> <asp:Label ID="lblFromDate" Text="From Date:" runat="server"></asp:Label></div>
    <div class="col-md-2 col-xs-8 marxs_top5"> <table> 
        <tr> 
        <td><asp:TextBox ID="txtFrom" CssClass="form-control" runat="server" Width="110"></asp:TextBox> </td>
        
        <td><a href="javascript:void()" onclick="showCalendar1()"><img src="images/call-cozmo.png"  alt="Pick Date" /></a> </td>
        
        </tr>
        
        
        </table> </div>
    <div class="col-md-2 col-xs-4 marxs_top5"><asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label> </div>
    <div class="col-md-2 col-xs-8 marxs_top5">   <table> 
        <tr> 
        <td> <asp:TextBox ID="txtTo" CssClass="form-control" runat="server" Width="110"></asp:TextBox></td>
        
         <td> <a href="javascript:void()" onclick="showCalendar2()"><img src="images/call-cozmo.png"  alt="Pick Date" /></a></td>
</tr>
        
        
        </table></div>
        
    <div class="col-md-2 col-xs-4 marxs_top5"> <asp:Label ID="lblAgent" runat="server" Text="Agent"></asp:Label></div>
    <div class="col-md-2 col-xs-8 marxs_top5"> <asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server"></asp:DropDownList></div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
    
    
    <div class="col-md-12 padding-0 marbot_10">    
    
    
    <div class="col-md-2 col-xs-4 ">  <asp:Label ID="lblProduct" runat="server" Text="Product"></asp:Label></div>
<div class="col-md-2 col-xs-8">  <asp:DropDownList  CssClass="form-control" ID="ddlProduct" runat="server">
            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
            <asp:ListItem Text="Activity" Value="A"></asp:ListItem>
            <asp:ListItem Text="Package" Value="P"></asp:ListItem>
            <asp:ListItem Text="Fixed Departure" Value="F"></asp:ListItem>
            </asp:DropDownList></div>
            
            
<div class="col-md-8 col-xs-12">  <asp:Button runat="server" ID="btnSearch" CssClass="btn but_b pull-right mar_xs_10" Text="Search" OnClick="btnSearch_OnClick"
                         /></div>
                         
                         
                         
                         <div class="clearfix"></div>
    
    </div>
    
    <div class="clearfix"> </div>
    
        </div>
        </asp:Panel>
        
        
        
    


 
        


    </div>




</div>
 <div>


           

   
   
   <div class="martop_14"> 
   <div title="paging">  <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" >First</asp:LinkButton>
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server"/>
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton></div>
   
   
   
   <div> 
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td>
           <div title="Desc" id="divDesc" style="position:absolute;display:none;right:355px;top:250px;">
        
        
        <div style=" position:relative" class="view-remarks"> 
        
        
  <div class="ns-h3">Remarks
  
  
  <a style=" position:absolute; right:10px; top:2px" onclick="return HideDesc();" class="fcol_fff" href=""> X</a>
        
        
        
        
       
        </div>
       
        <div class="view-remarks-inner"> 
        
       
        
        <table border="0" width="200px" height="50px" >
        <tr>
            <td valign="top" align="left"><asp:Label ID="lblRemarks" runat="server" CssClass="label" ></asp:Label></td>
        </tr>
        
        </table>
        
         </div>
         <div class="clear"></div>
        </div>
        
       
        
    </div>
    
    
    
    
    <div title="Remarks" id="divRemarks" style="position:absolute;display:none;">
    
    
        
        <div style=" position:relative" class="view-remarks"> 
        
        <div><b> <asp:Label ID="Label1" Text="Remarks:" runat="server" CssClass="label" ></asp:Label></b>
    
     <asp:Button ID="btnClose" CssClass="close-x" runat="server" Text="X"  style=" position:absolute; right:5px; top:5px;" OnClientClick="return HideDelete();" />
    
    </div>
        
        
        <table border="0" width="200px" height="50px" >
        <tr >
            <td valign="top" align="right"></td>
            <td><asp:TextBox ID="txtRemarks" TextMode="multiline" Height="45" width="100%" runat="server" MaxLength="500" CssClass="inputEnabled" ></asp:TextBox> </td>
        </tr>
        <tr>
        <td style=" padding-top:10px" colspan="2" align="right" valign="top">
                <asp:Button ID="btnOk" runat="server" CssClass="button" Text="Ok"  OnClick="btnOk_Click" OnClientClick="return validateRemarks();" />
                
          </td>
         </tr>
        </table>
        
        
        </div>
        
        
          <div class="clear"></div>
    </div>
    
    
    
    
    
    
     <div id="Div1"  runat="server">
   <asp:HiddenField ID="hdnEnquiryId" runat="server" />
   
   
   <div class="table-responsive bg_white"> 
    <asp:DataList ID="dlEnquiry" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0"
                Width="100%" ForeColor="" ShowHeader="true" DataKeyField="EnquiryId">
                <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White"  HorizontalAlign="Left"/>
      <HeaderTemplate>
   
                                        <tr height="30px">
                                            <td width="180px" class="heading">
                                                Product Type
                                            </td>
                                            <td width="180px" class="heading">
                                                Agent Name
                                            </td>
                                            <td width="240px" class="heading" >
                                               Product Name
                                            </td>
                                             <td width="180px" class="heading" >
                                               Pax Name
                                            </td>
                                            <td width="240px" class="heading" >
                                                Email
                                            </td>
                                            <td width="120px" class="heading" >
                                                Phone
                                            </td>
                                            <td width="100px" class="heading" >
                                                City
                                            </td>
                                             <td width="100px" class="heading">
                                                 Country
                                            </td>
                                             <td width="240px" class="heading">
                                                Enquiry Msg
                                            </td>
                                             <td width="200px" class="heading">
                                                Departure Date
                                            </td>
                                             <td width="150px" class="heading">
                                                Adults
                                            </td>
                                             <td width="100px" class="heading">
                                               Childs
                                            </td>
                                             <td width="100px" class="heading">
                                                Infants
                                            </td>
                                             <td width="200px" class="heading">
                                                Created On
                                            </td>
                                              <td width="200px" class="heading">
                                                Follow Up
                                            </td>
                                               <td width="200px" class="heading">
                                                View FollowUP
                                            </td>
                                        </tr>
                                   
    </HeaderTemplate>
    <ItemTemplate>
  
                                        <tr height="30px">
                                            <td width="180px">
                                             <%#Eval("ProductType")%>
                                            </td>
                                            <td width="180px" align="left">
                                                <%#Eval("agent_name")%>
                                            </td>
                                            <td width="240px" align="left">
                                                <%#Eval("ProductName")%>
                                            </td>
                                             <td width="180px" align="left">
                                                <%#Eval("PaxName")%>
                                            </td>
                                            <td width="240px" align="left">
                                                <%#Eval("Email")%>
                                            </td>
                                            <td width="120px" align="left">
                                                <%#Eval("Phone")%>
                                            </td>
                                             <td width="100px" align="left">
                                                <%#Eval("CityOfResidence")%>
                                            </td>
                                            <td width="100px" align="left">
                                                <%#Eval("CountryOfResidence")%>
                                            </td>
                                            <asp:HiddenField ID="hdnRemarks" runat="server" Value='<%#Eval("EnquiryMsg") %>' />
                                             <td width="240px">
                                               <a href="#" id='ShowRemarks-<%#Eval("EnquiryId") %>'  onclick="javascript:switchViews('<%# Container.ItemIndex +1 %>',this.id);">View Remarks
                </a>
                                            </td>
                                             <td width="200px">
                                                <%#Eval("DepartureDate")%>
                                            </td>
                                            <td width="150px">
                                                <%#Eval("Adult")%>
                                            </td>
                                            <td width="100px">
                                                <%#Eval("Child")%>
                                            </td>
                                               <td width="100px">
                                                <%#Eval("Infant")%>
                                            </td>
                                            <td width="200px">
                                                <%#Eval("CreatedOn")%>
                                            </td>
                                                <td width="200px">
                                               <a id='FollowUp-<%# Eval("EnquiryId")%>' href="#FollowUp-<%# Eval("EnquiryId")%>" onclick="javascript:AddRemarks('<%# Eval("EnquiryId") %>',this.id);">Follow Up</a>
                                            </td>
                                              <td width="200px">
                                               <a style=" cursor:pointer" onClick="javascript:window.open('ViewEnquiry.aspx?EnquireId=<%# Eval("EnquiryId") %>','','location=0,status=0,scrollbars=1,width=600,height=600,top=0,left=0');">View FollowUP</a>                                              </td>
                                        </tr>
                                   
    </ItemTemplate>
     </asp:DataList>
     
     </div>
    
     
     <!--Add DataField For Export Into  Excel -->
     <div>
    <asp:DataGrid ID="dgEnquiryReportList" runat="server" AutoGenerateColumns="false">
    <Columns>
    <asp:BoundColumn HeaderText="Product Type" DataField="ProductType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Agent Name" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Product Name" DataField="ProductName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Pax Name" DataField="PaxName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Email" DataField="Email" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Phone" DataField="Phone" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="City" DataField="CityOfResidence" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Country" DataField="CountryOfResidence" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Enquiry Msg" DataField="EnquiryMsg" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Departure Date" DataField="DepartureDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Adults" DataField="Adult" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Childs" DataField="Child" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Infants" DataField="Infant" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Created On" DataField="CreatedOn" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <%--<asp:BoundColumn HeaderText="Follow Up" DataField="cityRef" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="View FollowUP" DataField="Payableamount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>--%>
   </Columns>
    </asp:DataGrid>
    </div>

      <asp:Label ID="lblMessage" runat="server"></asp:Label>
     </div>
     
     </td>
    
    </tr>
    </table>
    <table>
    <tr>
     <td ><asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                              CssClass="button" style="margin-left:10px; margin-bottom:10px;" /></td>
    </tr>
    </table>
   
   </div>
   
   <div class="clearfix"> </div>
   </div>
   
  

 </div>
   <iframe id="IShimFrame" style="position:absolute; display:none;" frameborder="0"></iframe>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

