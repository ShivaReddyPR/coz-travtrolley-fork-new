﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpCityMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpCityMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script>

        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/expenseMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var countryData = {}; var currencyData = {};       
        var ECT_Id = 0;
        var agentdata = {}; var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* to get screen dropdown list data */
            GetScreenData();

        });

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent, country and currency */
        function GetScreenData() {
            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCityMasterScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind city master agent, Country, Currency */
        function BindScreenData(screenData) {
            agentdata = {};
            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }
            if (!IsEmpty(screenData.dtCountries))
                BindCountryData(screenData.dtCountries);
            if (!IsEmpty(screenData.dtCurrency))
                BindCurrencyData(screenData.dtCurrency);
        }

        /* To bind country data to country control */
        function BindCountryData(cData) {
            countryData = {};
            if (IsEmpty(cData)) {
                $('#ddlCountry').empty();
                $("#ddlCountry").select2("val", '');
                return;
            }
            countryData = cData;
            var options = countryData.length != 1 ? GetddlOption('', '--Select Country--') : '';
            $.each(countryData, function (key, col) {

                options += GetddlOption(col.countrY_CODE, col.countrY_NAME);
            });
            $('#ddlCountry').empty();
            $('#ddlCountry').append(options);
            $("#ddlCountry").select2("val", '');
            if (countryData.length == 1) {
                $("#ddlCountry").select2("val", countryData[0].countrY_CODE);
            }
        }

        /* To bind currency data to currency control */
        function BindCurrencyData(curData) {
            currencydata = {};
            if (IsEmpty(curData)) {
                $('#ddlCurrency').empty();
                $("#ddlCurrency").select2("val", '');
                return;
            }
            currencydata = curData;
            var options = GetddlOption('', '--Select Currency--');
            $.each(currencydata, function (key, col) {

                options += GetddlOption(col.currency_code, col.currency_code);
            });

            $('#ddlCurrency').empty();
            $('#ddlCurrency').append(options);
            $("#ddlCurrency").select2("val", '');
        }

        /* To prepare and get the city entity list for selected rows */
        function GetSelectedRows() {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var expcSelected = [];
            $.each(selectedRows, function (key, col) {

                var cInfo = ecInfoDetails.find(item => item.ecT_ID == col);
                cInfo.ecT_Status = false;
                cInfo.ecT_CreatedBy = apiAgentInfo.LoginUserId;
                expcSelected = expcSelected.concat(cInfo);
            });
            return expcSelected;
        }

        /* To prepare and set city info entity */
        function RefreshGrid(resp) {
            $.each(selectedRows, function (key, col) {

                var ectid = parseInt(col);
                ecInfoDetails = ecInfoDetails.filter(item => (item.ecT_ID) !== ectid);
            });
            if (ecInfoDetails.length == 0) {
                BindCityInfo(ecInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', ecInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To Load expense city info */
        function LoadCitiesInfo() {
            ClearControls();
            var ecT_AgentId = $("#ddlAgentId").val();
            if (ecT_AgentId == "" || ecT_AgentId == "--Select Agent--") {
                toastr.error('Please select agent');
                $('#ddlAgentId').parent().addClass('form-text-error');
                return false;
            }
            var reqData = { AgentInfo: apiAgentInfo, AgentId: ecT_AgentId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCityData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindCityInfo, null, null);
        }

        /* To bind city info to grid */
        function BindCityInfo(ecInfo) {
            if (ecInfo.length == 0) {

                ShowError('no data available for selected agent.');
                ClearInfo();
                return;
            }
            ecInfoDetails = ecInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Code|Name|Country|Currency').split('|');
            gridProperties.displayColumns = ('ecT_Code|ecT_Name|countrY_NAME|ecT_Currency').split('|');
            gridProperties.pKColumnNames = ('ecT_ID').split('|');
            gridProperties.dataEntity = ecInfo;
            gridProperties.divGridId = 'CityList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#CList').show();
        }

        /* To save city info */
        function Save() {
            var ecT_AgentId = $("#ddlAgentId").val();
            var ecT_Code = $("#ECT_Code").val();
            var ecT_Name = $("#ECT_Name").val();
            var ecT_Country = $("#ddlCountry").val();
            var ecT_Currency = $("#ddlCurrency").val();

            if ((ecT_AgentId == "" || ecT_AgentId == "--Select Agent--") || (ecT_Code == "" || ecT_Code == "Enetr Code") || (ecT_Name == "" || ecT_Name == "Enter Name") || (ecT_Country == "" || ecT_Country == "--Select Country--" || ecT_Country == null) || (ecT_Currency == "" || ecT_Currency == "--Select Currency--" || ecT_Currency == null)) {
                if (ecT_AgentId == "" || ecT_AgentId == "--Select Agent--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (ecT_Code == "" || ecT_Code == "Enter Code") {
                    toastr.error('Please Enter Code');
                    $("#ECT_Code").addClass('form-text-error');
                }
                if (ecT_Name == "" || ecT_Name == "Enter Name") {
                    toastr.error('Please Enter Name');
                    $("#ECT_Name").addClass('form-text-error');
                }
                if (ecT_Country == "" || ecT_Country == "--Select Country--" || ecT_Country == null) {
                    toastr.error('Please Select Country');
                    $('#ddlCountry').parent().addClass('form-text-error');
                }
                if (ecT_Currency == "" || ecT_Currency == "--Select Currency--" || ecT_Currency == null) {
                    toastr.error('Please Select Currency');
                    $('#ddlCurrency').parent().addClass('form-text-error');
                }
                return false;
            }

            var cityInfo = { ECT_ID: ECT_Id, ECT_AgentId: ecT_AgentId, ECT_Code: ecT_Code, ECT_Name: ecT_Name, ECT_Country: ecT_Country, ECT_Currency: ecT_Currency, ECT_Status: true, ECT_CreatedBy: apiAgentInfo.LoginUserId, ECT_ModifiedBy: apiAgentInfo.LoginUserId }
            var reqData = { AgentInfo: apiAgentInfo, CityInfo: cityInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveCityInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);
        }

        /* To bind status of city info */
        function StatusConfirm() {
            if (ECT_Id > 0) {
                alert('record updated successfully!');
            }
            else if (ECT_Id == 0) {
                alert('record added successfully!');
            }
            ClearInfo();
            LoadCitiesInfo();
        }

        /* To update selected city info from the grid and update the status in data base */
        function EditCityInfo() {
            var delCInfo = GetSelectedRows();
            if (IsEmpty(delCInfo) || delCInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delCInfo) || delCInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var ecDetails = ecInfoDetails.find(item => item.ecT_ID == selectedRows[0]);
            ECT_Id = ecDetails.ecT_ID;
            $("#ddlAgentId").select2('val', ecDetails.ecT_AgentId);
            $("#ECT_Code").val(ecDetails.ecT_Code);
            $("#ECT_Name").val(ecDetails.ecT_Name);
            $("#ddlCountry").select2('val', ecDetails.ecT_Country);
            $("#ddlCurrency").select2('val', ecDetails.ecT_Currency);
        }

        /* To delete selected rows from the grid and update the status in data base */
        function Delete() {
            var delCInfo = GetSelectedRows();
            if (IsEmpty(delCInfo) || delCInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }
            var reqData = { AgentInfo: apiAgentInfo, delCInfo, Status: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveCityInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To clear city details and hide the div */
        function ClearInfo() {
            selectedRows = [];
            selectedProfile = 0;            
            ECT_Id = 0;
            ClearControls();
            RemoveGrid();
            $('#CList').hide();
        }

        /* To clear controls */
        function ClearControls() {
            $("#ECT_Code").val('');
            $("#ECT_Name").val('');
            $("#ddlCurrency").select2('val', '');
            $("#ddlCountry").select2('val', '');

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#ECT_Code").removeClass('form-text-error');
            $("#ECT_Name").removeClass('form-text-error');
            $("#ddlCurrency").parent().removeClass('form-text-error');
            $("#ddlCountry").parent().removeClass('form-text-error');
        }

        /* textbox allows only alphanumeric and @-_$,.:; */
        function check(e) {
            var keynum;
            var keychar;

            // For Internet Explorer
            if (window.event) {
                keynum = e.keyCode;
            }
            // For Netscape/Firefox/Opera
            else if (e.which) {
                keynum = e.which;
            }
            keychar = String.fromCharCode(keynum);
            //List of special characters you want to restrict
            if (keychar == "'" || keychar == "`" || keychar == "!" || keychar == "#" || keychar == "%" || keychar == "^" || keychar == "*" || keychar == "(" || keychar == ")" || keychar == "+" || keychar == "=" || keychar == "/" || keychar == "~" || keychar == "<" || keychar == ">" || keychar == "|" || keychar == "?" || keychar == "{" || keychar == "}" || keychar == "[" || keychar == "]" || keychar == "¬" || keychar == "£" || keychar == '"' || keychar == "\\") {
                return false;
            } else {
                return true;
            }
        }

        /* textbox allows only alphanumeric */
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
            return ret;
        }

    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">City Master</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showCityList" type="button" data-toggle="collapse" data-target="#CList" aria-expanded="false" aria-controls="CiList" onclick="LoadCitiesInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Code</label><span style="color: red;">*</span>
                            <input type="text" id="ECT_Code" class="form-control" maxlength="10" placeholder="Enter Code" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Name</label><span style="color: red;">*</span>
                            <input type="text" class="form-control" id="ECT_Name" maxlength="100" placeholder="Enter Name" onkeypress="return check(event)" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Country</label><span style="color: red;">*</span>
                            <select name="Country" id="ddlCountry" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Currency</label><span style="color: red;">*</span>
                            <select name="Currency" id="ddlCurrency" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="exp-content-block collapse" id="CList" style="display: none;">
                    <h5 class="mb-3 float-left">All Cities List</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditCityInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="CityList"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
