﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;

public partial class FixedDeparturePassengerListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected string activityImgFolder;
    protected string dateControlId;
    protected void Page_Load(object sender, EventArgs e)
    {
        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        if (!IsPostBack)
        {
            if (Session["FixedDeparture"] != null)
            {
                activity = Session["FixedDeparture"] as Activity;

                lblName.Text = activity.Name;
                DataTable dtTransHeader = activity.TransactionHeader;
                int adults = 0, childs = 0, infants = 0;
                if (dtTransHeader != null && dtTransHeader.Rows.Count > 0)
                {
                    adults = Convert.ToInt32(dtTransHeader.Rows[0]["Adult"]);
                    childs = Convert.ToInt32(dtTransHeader.Rows[0]["Child"]);
                    infants = Convert.ToInt32(dtTransHeader.Rows[0]["Infant"]);
                    hdnTotalPaxCount.Value = Convert.ToString(adults + childs + infants);
                }
                BindPassengers();
            }
            else
            {
                Response.Redirect("FixedDepartureResults.aspx", false);
            }
        }
        else
        {
            BindPassengers();
        }
    }
     

    void BindPassengers()
    {
        //Bind Rooms
        activity = Session["FixedDeparture"] as Activity;
        DataTable dtTransHeader = activity.TransactionHeader;
        DataTable dtTransactionDetails = activity.TransactionDetail;
        for (int i = 0; i < Convert.ToInt32(dtTransHeader.Rows[0]["RoomCount"]); i++)
        {
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + (i + 1) + "'");
            DataTable dt = dtTransactionDetails.Clone();
            dt.Columns.Add("LabelQty", typeof(System.Int32));
            int z=0;
            foreach (DataRow row in rows)
            {
                string find = "PaxType='" + row["PaxType"] + "'";
                DataRow[] foundRows = dt.Select(find);
                if (foundRows.Length == 0)
                {
                    dt.ImportRow(row);
                    dt.Rows[z]["LabelQty"] = 1;
                    z++;
                }
                else
                {
                    int qty = Convert.ToInt32(foundRows[0]["LabelQty"]);
                    qty += 1;
                    foundRows[0]["LabelQty"] = qty;
                }
            }
            HtmlTableRow hRow = new HtmlTableRow();
            HtmlTableCell hCell = new HtmlTableCell();
            hCell.InnerHtml += "<b>" + " ROOM " + (i + 1) + "</b>";
            hCell.Attributes.Add("class", "subgray-header");
            hCell.ColSpan = 8;
            hRow.Cells.Add(hCell);
            tblPassenger.Rows.Add(hRow);
            //hdnAdults.Value = string.Empty;
            //hdnChilds.Value = string.Empty;
            //hdnInfants.Value = string.Empty;
            if (!IsPostBack)
            {
                if (hdnAdults.Value != "")
                {
                    hdnAdults.Value += "," + "1";
                }
                else
                {
                    hdnAdults.Value = "1";
                }
                if (hdnChilds.Value != "")
                {
                    hdnChilds.Value += "," + "1";
                }
                else
                {
                    hdnChilds.Value = "1";
                }
                if (hdnInfants.Value != "")
                {
                    hdnInfants.Value += "," + "1";
                }
                else
                {
                    hdnInfants.Value = "1";
                }
            }

            for (int m = 0; m < dt.Rows.Count; m++)
            {
                if (dt.Rows[m]["PaxType"].ToString().Trim() == "Adult" && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 0)
                {
                    for (int j = 0; j < Convert.ToInt32(dt.Rows[m]["LabelQty"]); j++)
                    {


                        HtmlTable tblAdults = new HtmlTable();
                        tblAdults.ID = "tblR" + (i + 1) + "Adults" + (j + 1);
                        HtmlTableRow tr = new HtmlTableRow();
                        HtmlTableCell tc = new HtmlTableCell();
                        tc.InnerHtml += "<b>" + " Adult " + (j + 1).ToString() + "</b>";
                        tc.Attributes.Add("class", "ns-h3");
                        tc.Width = "500px";
                        tc.ColSpan = 8;

                        tr.Cells.Add(tc);
                        tblAdults.Rows.Add(tr);
                        HtmlTableRow tr1 = new HtmlTableRow();
                        HtmlTableCell prefix = new HtmlTableCell();

                        Label lblPrefix = new Label();
                        lblPrefix.Text = "Title <span style='color:red;'>*</span>:";
                        lblPrefix.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlTitle = new DropDownList();
                        ddlTitle.CssClass = "field120";
                        ddlTitle.Items.Add(new ListItem("Select", "Select"));
                        ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                        ddlTitle.Items.Add(new ListItem("Mrs.","Mrs."));
                        ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
                        ddlTitle.Items.Add(new ListItem("Dr.", "Dr."));
                        ddlTitle.SelectedIndex = 0;
                        ddlTitle.ID = "ddlTitleA" + (i + 1) + (j + 1);
                        ddlTitle.CausesValidation = true;
                        ddlTitle.ValidationGroup = "pax";
                        prefix.Controls.Add(lblPrefix);
                        prefix.Controls.Add(ddlTitle);

                        RequiredFieldValidator prefixvc = new RequiredFieldValidator();
                        prefixvc.ControlToValidate = ddlTitle.ID;
                        prefixvc.ErrorMessage = "Select Title";
                        prefixvc.ValidationGroup = "pax";
                        prefixvc.InitialValue = "Select";
                        prefixvc.SetFocusOnError = true;
                        prefix.Controls.Add(prefixvc);


                        //Email
                        HtmlTableCell email = new HtmlTableCell();
                        Label lblEmail = new Label();
                        if (j == 0 && i==0)
                        {
                            lblEmail.Text = "Email <span style='color:red;'>*</span>:";
                        }
                        else
                        {
                            lblEmail.Text = "Email:";
                        }
                        lblEmail.Width = new Unit(90, UnitType.Pixel);
                        TextBox txtEmail = new TextBox();
                        txtEmail.ID = "txtEmailA" + (i + 1) + (j + 1);
                        txtEmail.Width = new Unit(150, UnitType.Pixel);
                        txtEmail.CssClass = "field120";
                        txtEmail.CausesValidation = true;
                        txtEmail.ValidationGroup = "pax";
                        email.Controls.Add(lblEmail);
                        email.Controls.Add(txtEmail);
                        if (j == 0 && i == 0)
                        {
                            RequiredFieldValidator emailvc = new RequiredFieldValidator();
                            emailvc.ControlToValidate = txtEmail.ID;
                            emailvc.ErrorMessage = "Enter Email";
                            emailvc.InitialValue = "";
                            emailvc.SetFocusOnError = true;
                            emailvc.ValidationGroup = "pax";
                            email.Controls.Add(emailvc);
                        }
                        RegularExpressionValidator emailrv = new RegularExpressionValidator();
                        emailrv.ControlToValidate = txtEmail.ID;
                        emailrv.ErrorMessage = "Invalid Email address";
                        emailrv.SetFocusOnError = true;
                        emailrv.ValidationGroup = "pax";
                        emailrv.ValidationExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*$";
                        email.Controls.Add(emailrv);

                        tr1.Cells.Add(prefix);
                        tr1.Cells.Add(email);

                        HtmlTableRow tr2 = new HtmlTableRow();
                        HtmlTableCell fname = new HtmlTableCell();
                        //First Name

                        Label lblfname = new Label();
                        lblfname.Text = "First Name: <span style='color:red;'>*</span>:";
                        lblfname.Width = new Unit(90, UnitType.Pixel);
                        TextBox txtFirstName = new TextBox();
                        txtFirstName.ID = "txtFirstNameA" + (i + 1) + (j + i);
                        txtFirstName.Width = new Unit(150, UnitType.Pixel);
                        txtFirstName.CausesValidation = true;
                        txtFirstName.MaxLength = 10;
                        txtFirstName.ValidationGroup = "pax";
                        txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtFirstName.Attributes.Add("ondrop", "return false;");
                        txtFirstName.Attributes.Add("onpaste", "return false;");
                        fname.Controls.Add(lblfname);
                        fname.Controls.Add(txtFirstName);

                        RequiredFieldValidator fnamevc = new RequiredFieldValidator();
                        fnamevc.ControlToValidate = txtFirstName.ID;
                        fnamevc.ErrorMessage = "Enter First Name";
                        fnamevc.InitialValue = "";
                        fnamevc.SetFocusOnError = true;
                        fnamevc.ValidationGroup = "pax";
                        fname.Controls.Add(fnamevc);

                        CustomValidator fnameValidator = new CustomValidator();
                        fnameValidator.ControlToValidate = txtFirstName.ID;
                        fnameValidator.ClientValidationFunction = "validateName";
                        fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        fnameValidator.SetFocusOnError = true;
                        fnameValidator.ValidationGroup = "pax";
                        fname.Controls.Add(fnameValidator);

                        HtmlTableCell lname = new HtmlTableCell();

                        //LastName
                        Label lbllname = new Label();
                        lbllname.Text = "LastName: <span style='color:red;'>*</span>:";
                        lbllname.Width = new Unit(90, UnitType.Pixel);

                        TextBox txtLastName = new TextBox();
                        txtLastName.ID = "txtLastNameA" + (i + 1) + (j + i);
                        txtLastName.Width = new Unit(150, UnitType.Pixel);
                        txtLastName.CausesValidation = true;
                        txtLastName.MaxLength = 15;
                        txtLastName.ValidationGroup = "pax";
                        txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtLastName.Attributes.Add("ondrop", "return false;");
                        txtLastName.Attributes.Add("onpaste", "return false;");
                        lname.Controls.Add(lbllname);
                        lname.Controls.Add(txtLastName);


                        RequiredFieldValidator lnamevc = new RequiredFieldValidator();
                        lnamevc.ControlToValidate = txtLastName.ID;
                        lnamevc.ErrorMessage = "Enter Last Name";
                        lnamevc.InitialValue = "";
                        lnamevc.SetFocusOnError = true;
                        lnamevc.ValidationGroup = "pax";
                        lname.Controls.Add(lnamevc);

                        CustomValidator lnameValidator = new CustomValidator();
                        lnameValidator.ControlToValidate = txtLastName.ID;
                        lnameValidator.ClientValidationFunction = "validateName";
                        lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        lnameValidator.SetFocusOnError = true;
                        lnameValidator.ValidationGroup = "pax";
                        lname.Controls.Add(lnameValidator);

                        //DoB
                        HtmlTableRow tr3 = new HtmlTableRow();
                        HtmlTableCell dob = new HtmlTableCell();
                        Label lblDOB = new Label();
                        lblDOB.Text = "DOB <span style='color:red;'>*</span>:";
                        lblDOB.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlDay = new DropDownList();
                        ddlDay.ID = "ddlDayA" + (i + 1) + (j + i);
                        ddlDay.Width = new Unit(50, UnitType.Pixel);
                        BindDates(ddlDay);

                        DropDownList ddlMonth = new DropDownList();
                        ddlMonth.ID = "ddlMonthA" + (i + 1) + (j + i);
                        ddlMonth.Width = new Unit(60, UnitType.Pixel);
                        BindMonths(ddlMonth);

                        DropDownList ddlYear = new DropDownList();
                        ddlYear.ID = "ddlYearA" + (i + 1) + (j + i);
                        ddlYear.Width = new Unit(60, UnitType.Pixel);
                        BindYears(ddlYear);

                        Label lblError = new Label();
                        lblError.ID = "lblErrorA" + (i + 1) + (j + i);
                        lblError.Width = new Unit(150, UnitType.Pixel);

                        dob.Controls.Add(lblDOB);
                        dob.Controls.Add(ddlDay);
                        dob.Controls.Add(ddlMonth);
                        dob.Controls.Add(ddlYear);

                        RequiredFieldValidator dayvc = new RequiredFieldValidator();
                        dayvc.ControlToValidate = ddlDay.ID;
                        dayvc.ErrorMessage = "Select Date";
                        dayvc.InitialValue = "-1";
                        dayvc.SetFocusOnError = true;
                        dayvc.ValidationGroup = "pax";
                        dob.Controls.Add(dayvc);
                        dob.Controls.Add(lblError);

                        RequiredFieldValidator monthvc = new RequiredFieldValidator();
                        monthvc.ControlToValidate = ddlMonth.ID;
                        monthvc.ErrorMessage = "Select Month";
                        monthvc.InitialValue = "-1";
                        monthvc.SetFocusOnError = true;
                        monthvc.ValidationGroup = "pax";
                        dob.Controls.Add(monthvc);

                        RequiredFieldValidator yearvc = new RequiredFieldValidator();
                        yearvc.ControlToValidate = ddlYear.ID;
                        yearvc.ErrorMessage = "Select Year";
                        yearvc.InitialValue = "-1";
                        yearvc.SetFocusOnError = true;
                        yearvc.ValidationGroup = "pax";
                        dob.Controls.Add(yearvc);

                        RangeValidator rvYear = new RangeValidator();
                        rvYear.ControlToValidate = ddlYear.ID;
                        rvYear.SetFocusOnError = true;
                        rvYear.ErrorMessage = "Passenger Age must be in between 30-Days and 75-Years";
                        rvYear.Type = ValidationDataType.Integer;
                        rvYear.ValidationGroup = "pax";
                        rvYear.MinimumValue = DateTime.Now.AddYears(-75).Year.ToString();
                        rvYear.MaximumValue = DateTime.Now.AddYears(0).Year.ToString();

                        //Nationality
                        HtmlTableCell nation = new HtmlTableCell();
                        Label lblNation = new Label();
                        lblNation.Text = "Nationality <span style='color:red;'>*</span>:";
                        lblNation.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlNationality = new DropDownList();
                        ddlNationality.ID = "ddlNationalityA" + (i + 1) + (j + i);
                        ddlNationality.Width = new Unit(150, UnitType.Pixel);
                        ddlNationality.CausesValidation = true;
                        ddlNationality.ValidationGroup = "pax";
                        BindNationality(ddlNationality);
                        nation.Controls.Add(lblNation);
                        nation.Controls.Add(ddlNationality);

                        RequiredFieldValidator nationvc = new RequiredFieldValidator();
                        nationvc.ControlToValidate = ddlNationality.ID;
                        nationvc.ErrorMessage = "Select Nationality";
                        nationvc.InitialValue = "-1";
                        nationvc.SetFocusOnError = true;
                        nationvc.ValidationGroup = "pax";
                        nation.Controls.Add(nationvc);


                        //PassportNo
                        HtmlTableRow tr4 = new HtmlTableRow();
                        HtmlTableCell pass = new HtmlTableCell();
                        Label lblPassportNo = new Label();
                        lblPassportNo.Text = "Passport No: <span style='color:red;'>*</span>:";
                        lblPassportNo.Width = new Unit(90, UnitType.Pixel);

                        TextBox txtPassportNo = new TextBox();
                        txtPassportNo.ID = "txtPassportNoA" + (i + 1) + (j + i);
                        txtPassportNo.Width = new Unit(150, UnitType.Pixel);
                        txtPassportNo.CausesValidation = true;
                        txtPassportNo.MaxLength = 15;
                        txtPassportNo.ValidationGroup = "pax";
                        //txtPassportNo.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtPassportNo.Attributes.Add("ondrop", "return false;");
                        txtPassportNo.Attributes.Add("onpaste", "return false;");
                        pass.Controls.Add(lblPassportNo);
                        pass.Controls.Add(txtPassportNo);


                        RequiredFieldValidator lpassvc = new RequiredFieldValidator();
                        lpassvc.ControlToValidate = txtPassportNo.ID;
                        lpassvc.ErrorMessage = "Enter PassportNo";
                        lpassvc.InitialValue = "";
                        lpassvc.SetFocusOnError = true;
                        lpassvc.ValidationGroup = "pax";
                        pass.Controls.Add(lpassvc);

                        CustomValidator lPassValidator = new CustomValidator();
                        lPassValidator.ControlToValidate = txtPassportNo.ID;
                        lPassValidator.ClientValidationFunction = "validateName";
                        lPassValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        lPassValidator.SetFocusOnError = true;
                        lPassValidator.ValidationGroup = "pax";
                        pass.Controls.Add(lPassValidator);

                        //Passport Exp

                        HtmlTableCell dobPass = new HtmlTableCell();
                        Label lblDOBPass = new Label();
                        lblDOBPass.Text = "Passport Expiry<span style='color:red;'>*</span>:";
                        lblDOBPass.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlDayPass = new DropDownList();
                        ddlDayPass.ID = "ddlDayPassA" + (i + 1) + (j + i);
                        ddlDayPass.Width = new Unit(50, UnitType.Pixel);
                       
                        BindDates(ddlDayPass);

                        DropDownList ddlMonthPass = new DropDownList();
                        ddlMonthPass.ID = "ddlMonthPassA" + (i + 1) + (j + i);
                        ddlMonthPass.Width = new Unit(60, UnitType.Pixel);
                        BindMonths(ddlMonthPass);

                        DropDownList ddlYearPass = new DropDownList();
                        ddlYearPass.ID = "ddlYearPassA" + (i + 1) + (j + i);
                        ddlYearPass.Width = new Unit(60, UnitType.Pixel);
                        BindPassYears(ddlYearPass);

                        Label lblErrorPass = new Label();
                        lblErrorPass.ID = "lblErrorPassA" + (i + 1) + (j + i);
                        lblErrorPass.Width = new Unit(150, UnitType.Pixel);

                        dobPass.Controls.Add(lblDOBPass);
                        dobPass.Controls.Add(ddlDayPass);
                        dobPass.Controls.Add(ddlMonthPass);
                        dobPass.Controls.Add(ddlYearPass);

                        RequiredFieldValidator dayPassvc = new RequiredFieldValidator();
                        dayPassvc.ControlToValidate = ddlDayPass.ID;
                        dayPassvc.ErrorMessage = "Select Date";
                        dayPassvc.InitialValue = "-1";
                        dayPassvc.SetFocusOnError = true;
                        dayPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(dayvc);
                        dobPass.Controls.Add(lblError);

                        RequiredFieldValidator monthPassvc = new RequiredFieldValidator();
                        monthPassvc.ControlToValidate = ddlMonthPass.ID;
                        monthPassvc.ErrorMessage = "Select Month";
                        monthPassvc.InitialValue = "-1";
                        monthPassvc.SetFocusOnError = true;
                        monthPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(monthPassvc);

                        RequiredFieldValidator yearPassvc = new RequiredFieldValidator();
                        yearPassvc.ControlToValidate = ddlYearPass.ID;
                        yearPassvc.ErrorMessage = "Select Year";
                        yearPassvc.InitialValue = "-1";
                        yearPassvc.SetFocusOnError = true;
                        yearPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(yearPassvc);

                        HtmlTableRow tr5 = new HtmlTableRow();
                        HtmlTableCell roomType = new HtmlTableCell();

                        //RoomType
                        Label lblRoomType = new Label();
                        lblRoomType.Text = "RoomType <span style='color:red;'>*</span>:";
                        lblRoomType.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlRoomType = new DropDownList();
                        ddlRoomType.Width = new Unit(150, UnitType.Pixel);
                        ddlRoomType.Items.Add(new ListItem("Select", "Select"));
                        ddlRoomType.Items.Add(new ListItem("Single Adult", "SA"));
                        ddlRoomType.Items.Add(new ListItem("Double Sharing Adults", "DSA"));
                        ddlRoomType.Items.Add(new ListItem("Twin sharing", "TS"));
                        ddlRoomType.Items.Add(new ListItem("Double+ 1 Child extra bed", "DCB"));
                        ddlRoomType.Items.Add(new ListItem("Double + 1 child on existing bed","DCEB"));
                        ddlRoomType.Items.Add(new ListItem("Triple", "T"));
                        ddlRoomType.Items.Add(new ListItem("Family Room", "FR"));
                        ddlRoomType.SelectedIndex = 0;
                        ddlRoomType.ID = "ddlRoomTypeA" + (i + 1) + (j + i);
                        ddlRoomType.CausesValidation = true;
                        ddlRoomType.ValidationGroup = "pax";
                        roomType.Controls.Add(lblRoomType);
                        roomType.Controls.Add(ddlRoomType);

                        RequiredFieldValidator roomTypevc = new RequiredFieldValidator();
                        roomTypevc.ControlToValidate = ddlRoomType.ID;
                        roomTypevc.ErrorMessage = "Select RoomType";
                        roomTypevc.ValidationGroup = "pax";
                        roomTypevc.InitialValue = "Select";
                        roomTypevc.SetFocusOnError = true;
                        roomType.Controls.Add(roomTypevc);



                        //MealChioce
                        HtmlTableCell mealChioce = new HtmlTableCell();
                        Label lblMealChioce = new Label();
                        lblMealChioce.Text = "Meal Chioce <span style='color:red;'>*</span>:";
                        lblMealChioce.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlMealChioce = new DropDownList();
                        ddlMealChioce.Width = new Unit(150, UnitType.Pixel);
                        ddlMealChioce.Items.Add(new ListItem("Select", "Select"));
                        ddlMealChioce.Items.Add(new ListItem("Vegetarian", "Vegetarian"));
                        ddlMealChioce.Items.Add(new ListItem("Asian Vegetarian", "Asian Vegetarian"));
                        ddlMealChioce.Items.Add(new ListItem("Halal", "Halal"));
                        ddlMealChioce.Items.Add(new ListItem("Non-Veg", "Non-Veg"));
                        ddlMealChioce.Items.Add(new ListItem("Jain", "Jain"));
                        ddlMealChioce.Items.Add(new ListItem("Infant Meal", "Infant Meal"));
                        ddlMealChioce.Items.Add(new ListItem("Child Meal Veg", "Child Meal Veg"));
                        ddlMealChioce.Items.Add(new ListItem("Child Meal Non Veg", "Child Meal Non Veg"));
                        ddlMealChioce.SelectedIndex = 0;
                        ddlMealChioce.ID = "ddlMealChioceA" + (i + 1) + (j + i);
                        ddlMealChioce.CausesValidation = true;
                        ddlMealChioce.ValidationGroup = "pax";
                        mealChioce.Controls.Add(lblMealChioce);
                        mealChioce.Controls.Add(ddlMealChioce);

                        RequiredFieldValidator mealChioceevc = new RequiredFieldValidator();
                        mealChioceevc.ControlToValidate = ddlMealChioce.ID;
                        mealChioceevc.ErrorMessage = "Select Meal Chioce";
                        mealChioceevc.ValidationGroup = "pax";
                        mealChioceevc.InitialValue = "Select";
                        mealChioceevc.SetFocusOnError = true;
                        mealChioce.Controls.Add(mealChioceevc);

                        HtmlTableCell AddRemove = new HtmlTableCell();
                        Button btnAddA = new Button();
                        btnAddA.ID = "btnAddA" + (j + 1) + (i + 1);
                        btnAddA.Text = "Add";
                        btnAddA.Width = new Unit(50, UnitType.Pixel);
                        btnAddA.Click += new EventHandler(btnAdd_Click);
                        if (j == 0 && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 1)
                        {
                            AddRemove.Controls.Add(btnAddA);
                        }
                        Button btnRemoveA = new Button();
                        btnRemoveA.ID = "btnRemoveA" + (j + 1) + (i + 1);
                        btnRemoveA.Text = "Remove";
                        btnRemoveA.Click += new EventHandler(btnRemoveA_Click);
                        if (j != 0 && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 1)
                        {
                            AddRemove.Controls.Add(btnRemoveA);
                        }
                        HtmlTableRow tr6 = new HtmlTableRow();
                        HtmlTableCell Note = new HtmlTableCell();
                        Note.ColSpan = 4;
                        Label lblNote = new Label();
                        lblNote.Text = "Please note room types are subject to availability , please request /check with operations";
                        //lblNote.Width = new Unit(90, UnitType.Pixel);
                        lblNote.ForeColor = System.Drawing.Color.Red;
                        Note.Controls.Add(lblNote);
                        if (j != 0)
                        {
                            tblAdults.Visible = false;
                        }
                        else
                        {
                            if (!IsPostBack)
                            {
                                hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) + 1);
                            }
                        }

                        tr3.Cells.Add(dob);
                        tr3.Cells.Add(nation);
                        tr2.Cells.Add(fname);
                        tr2.Cells.Add(lname);
                        tr4.Cells.Add(pass);
                        tr4.Cells.Add(dobPass);
                        tr5.Cells.Add(roomType);
                        tr5.Cells.Add(mealChioce);
                        tr5.Cells.Add(AddRemove);
                        tr6.Cells.Add(Note);
                        tblAdults.Rows.Add(tr1);
                        tblAdults.Rows.Add(tr2);
                        tblAdults.Rows.Add(tr3);
                        tblAdults.Rows.Add(tr4);
                        tblAdults.Rows.Add(tr5);
                        tblAdults.Rows.Add(tr6);

                        HtmlTableCell tcAdults = new HtmlTableCell();
                        tcAdults.ColSpan = 4;
                        tcAdults.Controls.Add(tblAdults);
                        HtmlTableRow trAdults = new HtmlTableRow();
                        trAdults.Cells.Add(tcAdults);
                        tblPassenger.Rows.Add(trAdults);
                    }
                }

                if (dt.Rows[m]["PaxType"].ToString().Trim() == "Child" && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 0)
                {
                    for (int k = 0; k < Convert.ToInt32(dt.Rows[m]["LabelQty"]); k++)
                    {

                        HtmlTable tblChilds = new HtmlTable();
                        tblChilds.ID = "tblR" + (i + 1) + "Childs" + (k + 1);
                        HtmlTableRow tr = new HtmlTableRow();
                        HtmlTableCell tc = new HtmlTableCell();
                        tc.InnerHtml += "<b>" + " Child " + (k + 1).ToString() + "</b>";
                        tc.Attributes.Add("class", "ns-h3");
                        tc.ColSpan = 8;
                        tr.Cells.Add(tc);




                        tblChilds.Rows.Add(tr);
                        HtmlTableRow tr1 = new HtmlTableRow();
                        HtmlTableCell prefix = new HtmlTableCell();

                        Label lblPrefix = new Label();
                        lblPrefix.Text = "Title <span style='color:red;'>*</span>:";
                        lblPrefix.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlTitle = new DropDownList();
                        ddlTitle.CssClass = "field120";
                        ddlTitle.Items.Add(new ListItem("Select", "Select"));
                        ddlTitle.Items.Add(new ListItem("Mstr.", "Mstr."));
                        //ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
                        //ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                        ddlTitle.Items.Add(new ListItem("Dr.", "Dr."));
                        ddlTitle.SelectedIndex = 0;
                        ddlTitle.ID = "ddlTitleC" + (i + 1) + (k + 1);
                        ddlTitle.CausesValidation = true;
                        ddlTitle.ValidationGroup = "pax";
                        prefix.Controls.Add(lblPrefix);
                        prefix.Controls.Add(ddlTitle);

                        RequiredFieldValidator prefixvc = new RequiredFieldValidator();
                        prefixvc.ControlToValidate = ddlTitle.ID;
                        prefixvc.ErrorMessage = "Select Title";
                        prefixvc.ValidationGroup = "pax";
                        prefixvc.InitialValue = "Select";
                        prefixvc.SetFocusOnError = true;
                        prefix.Controls.Add(prefixvc);


                        //Email
                        HtmlTableCell email = new HtmlTableCell();
                        Label lblEmail = new Label();
                        lblEmail.Text = "Email:";
                        lblEmail.Width = new Unit(90, UnitType.Pixel);
                        TextBox txtEmail = new TextBox();
                        txtEmail.ID = "txtEmailC" + (i + 1) + (k + 1);
                        txtEmail.Width = new Unit(150, UnitType.Pixel);
                        txtEmail.CssClass = "field120";
                        txtEmail.CausesValidation = true;
                        txtEmail.ValidationGroup = "pax";
                        email.Controls.Add(lblEmail);
                        email.Controls.Add(txtEmail);

                        //RequiredFieldValidator emailvc = new RequiredFieldValidator();
                        //emailvc.ControlToValidate = txtEmail.ID;
                        //emailvc.ErrorMessage = "Enter Email";
                        //emailvc.InitialValue = "";
                        //emailvc.SetFocusOnError = true;
                        //emailvc.ValidationGroup = "pax";
                        //email.Controls.Add(emailvc);

                        RegularExpressionValidator emailrv = new RegularExpressionValidator();
                        emailrv.ControlToValidate = txtEmail.ID;
                        emailrv.ErrorMessage = "Invalid Email address";
                        emailrv.SetFocusOnError = true;
                        emailrv.ValidationGroup = "pax";
                        emailrv.ValidationExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*$";
                        email.Controls.Add(emailrv);

                        tr1.Cells.Add(prefix);
                        tr1.Cells.Add(email);

                        HtmlTableRow tr2 = new HtmlTableRow();
                        HtmlTableCell fname = new HtmlTableCell();
                        //First Name

                        Label lblfname = new Label();
                        lblfname.Text = "First Name: <span style='color:red;'>*</span>:";
                        lblfname.Width = new Unit(90, UnitType.Pixel);
                        TextBox txtFirstName = new TextBox();
                        txtFirstName.ID = "txtFirstNameC" + (i + 1) + (k + i);
                        txtFirstName.Width = new Unit(150, UnitType.Pixel);
                        txtFirstName.CausesValidation = true;
                        txtFirstName.MaxLength = 10;
                        txtFirstName.ValidationGroup = "pax";
                        txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtFirstName.Attributes.Add("ondrop", "return false;");
                        txtFirstName.Attributes.Add("onpaste", "return false;");
                        fname.Controls.Add(lblfname);
                        fname.Controls.Add(txtFirstName);

                        RequiredFieldValidator fnamevc = new RequiredFieldValidator();
                        fnamevc.ControlToValidate = txtFirstName.ID;
                        fnamevc.ErrorMessage = "Enter First Name";
                        fnamevc.InitialValue = "";
                        fnamevc.SetFocusOnError = true;
                        fnamevc.ValidationGroup = "pax";
                        fname.Controls.Add(fnamevc);

                        CustomValidator fnameValidator = new CustomValidator();
                        fnameValidator.ControlToValidate = txtFirstName.ID;
                        fnameValidator.ClientValidationFunction = "validateName";
                        fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        fnameValidator.SetFocusOnError = true;
                        fnameValidator.ValidationGroup = "pax";
                        fname.Controls.Add(fnameValidator);

                        HtmlTableCell lname = new HtmlTableCell();

                        //LastName
                        Label lbllname = new Label();
                        lbllname.Text = "Last Name: <span style='color:red;'>*</span>:";
                        lbllname.Width = new Unit(90, UnitType.Pixel);

                        TextBox txtLastName = new TextBox();
                        txtLastName.ID = "txtLastNameC" + (i + 1) + (k + i);
                        txtLastName.Width = new Unit(150, UnitType.Pixel);
                        txtLastName.CausesValidation = true;
                        txtLastName.MaxLength = 15;
                        txtLastName.ValidationGroup = "pax";
                        txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtLastName.Attributes.Add("ondrop", "return false;");
                        txtLastName.Attributes.Add("onpaste", "return false;");
                        lname.Controls.Add(lbllname);
                        lname.Controls.Add(txtLastName);


                        RequiredFieldValidator lnamevc = new RequiredFieldValidator();
                        lnamevc.ControlToValidate = txtLastName.ID;
                        lnamevc.ErrorMessage = "Enter Last Name";
                        lnamevc.InitialValue = "";
                        lnamevc.SetFocusOnError = true;
                        lnamevc.ValidationGroup = "pax";
                        lname.Controls.Add(lnamevc);

                        CustomValidator lnameValidator = new CustomValidator();
                        lnameValidator.ControlToValidate = txtLastName.ID;
                        lnameValidator.ClientValidationFunction = "validateName";
                        lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        lnameValidator.SetFocusOnError = true;
                        lnameValidator.ValidationGroup = "pax";
                        lname.Controls.Add(lnameValidator);

                        //DoB
                        HtmlTableRow tr3 = new HtmlTableRow();
                        HtmlTableCell dob = new HtmlTableCell();
                        Label lblDOB = new Label();
                        lblDOB.Text = "DOB <span style='color:red;'>*</span>:";
                        lblDOB.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlDay = new DropDownList();
                        ddlDay.ID = "ddlDayC" + (i + 1) + (k + i);
                        ddlDay.Width = new Unit(50, UnitType.Pixel);
                        
                        BindDates(ddlDay);

                        DropDownList ddlMonth = new DropDownList();
                        ddlMonth.ID = "ddlMonthC" + (i + 1) + (k + i);
                        ddlMonth.Width = new Unit(60, UnitType.Pixel);
                        
                        BindMonths(ddlMonth);

                        DropDownList ddlYear = new DropDownList();
                        ddlYear.ID = "ddlYearC" + (i + 1) + (k + i);
                        ddlYear.Width = new Unit(60, UnitType.Pixel);
                        
                        BindYears(ddlYear);

                        Label lblError = new Label();
                        lblError.ID = "lblErrorC" + (i + 1) + (k + i);
                        lblError.Width = new Unit(150, UnitType.Pixel);

                        dob.Controls.Add(lblDOB);
                        dob.Controls.Add(ddlDay);
                        dob.Controls.Add(ddlMonth);
                        dob.Controls.Add(ddlYear);

                        RequiredFieldValidator dayvc = new RequiredFieldValidator();
                        dayvc.ControlToValidate = ddlDay.ID;
                        dayvc.ErrorMessage = "Select Date";
                        dayvc.InitialValue = "-1";
                        dayvc.SetFocusOnError = true;
                        dayvc.ValidationGroup = "pax";
                        dob.Controls.Add(dayvc);
                        dob.Controls.Add(lblError);

                        RequiredFieldValidator monthvc = new RequiredFieldValidator();
                        monthvc.ControlToValidate = ddlMonth.ID;
                        monthvc.ErrorMessage = "Select Month";
                        monthvc.InitialValue = "-1";
                        monthvc.SetFocusOnError = true;
                        monthvc.ValidationGroup = "pax";
                        dob.Controls.Add(monthvc);

                        RequiredFieldValidator yearvc = new RequiredFieldValidator();
                        yearvc.ControlToValidate = ddlYear.ID;
                        yearvc.ErrorMessage = "Select Year";
                        yearvc.InitialValue = "-1";
                        yearvc.SetFocusOnError = true;
                        yearvc.ValidationGroup = "pax";
                        dob.Controls.Add(yearvc);


                        //Nationality
                        HtmlTableCell nation = new HtmlTableCell();
                        Label lblNation = new Label();
                        lblNation.Text = "Nationality <span style='color:red;'>*</span>:";
                        lblNation.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlNationality = new DropDownList();
                        ddlNationality.ID = "ddlNationalityC" + (i + 1) + (k + i);
                        ddlNationality.Width = new Unit(150, UnitType.Pixel);
                        ddlNationality.CausesValidation = true;
                        ddlNationality.ValidationGroup = "pax";
                        BindNationality(ddlNationality);
                        nation.Controls.Add(lblNation);
                        nation.Controls.Add(ddlNationality);

                        RequiredFieldValidator nationvc = new RequiredFieldValidator();
                        nationvc.ControlToValidate = ddlNationality.ID;
                        nationvc.ErrorMessage = "Select Nationality";
                        nationvc.InitialValue = "-1";
                        nationvc.SetFocusOnError = true;
                        nationvc.ValidationGroup = "pax";
                        nation.Controls.Add(nationvc);


                        //PassportNo
                        HtmlTableRow tr4 = new HtmlTableRow();
                        HtmlTableCell pass = new HtmlTableCell();
                        Label lblPassportNo = new Label();
                        lblPassportNo.Text = "Passport No: <span style='color:red;'>*</span>:";
                        lblPassportNo.Width = new Unit(90, UnitType.Pixel);

                        TextBox txtPassportNo = new TextBox();
                        txtPassportNo.ID = "txtPassportNoC" + (i + 1) + (k + i);
                        txtPassportNo.Width = new Unit(150, UnitType.Pixel);
                        txtPassportNo.CausesValidation = true;
                        txtPassportNo.MaxLength = 15;
                        txtPassportNo.ValidationGroup = "pax";
                        //txtPassportNo.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtPassportNo.Attributes.Add("ondrop", "return false;");
                        txtPassportNo.Attributes.Add("onpaste", "return false;");
                        pass.Controls.Add(lblPassportNo);
                        pass.Controls.Add(txtPassportNo);


                        RequiredFieldValidator lpassvc = new RequiredFieldValidator();
                        lpassvc.ControlToValidate = txtLastName.ID;
                        lpassvc.ErrorMessage = "Enter PassportNo";
                        lpassvc.InitialValue = "";
                        lpassvc.SetFocusOnError = true;
                        lpassvc.ValidationGroup = "pax";
                        pass.Controls.Add(lpassvc);

                        CustomValidator lPassValidator = new CustomValidator();
                        lPassValidator.ControlToValidate = txtLastName.ID;
                        lPassValidator.ClientValidationFunction = "validateName";
                        lPassValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        lPassValidator.SetFocusOnError = true;
                        lPassValidator.ValidationGroup = "pax";
                        pass.Controls.Add(lPassValidator);

                        //Passport Exp

                        HtmlTableCell dobPass = new HtmlTableCell();
                        Label lblDOBPass = new Label();
                        lblDOBPass.Text = "Passport Expiry<span style='color:red;'>*</span>:";
                        lblDOBPass.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlDayPass = new DropDownList();
                        ddlDayPass.ID = "ddlDayPassC" + (i + 1) + (k + i);
                        ddlDayPass.Width = new Unit(50, UnitType.Pixel);
                        
                        BindDates(ddlDayPass);

                        DropDownList ddlMonthPass = new DropDownList();
                        ddlMonthPass.ID = "ddlMonthPassC" + (i + 1) + (k + i);
                        ddlMonthPass.Width = new Unit(60, UnitType.Pixel);
                        
                        BindMonths(ddlMonthPass);

                        DropDownList ddlYearPass = new DropDownList();
                        ddlYearPass.ID = "ddlYearPassC" + (i + 1) + (k + i);
                        ddlYearPass.Width = new Unit(60, UnitType.Pixel);
                        
                        BindPassYears(ddlYearPass);

                        Label lblErrorPass = new Label();
                        lblErrorPass.ID = "lblErrorPassC" + (i + 1) + (k + i);
                        lblErrorPass.Width = new Unit(150, UnitType.Pixel);

                        dobPass.Controls.Add(lblDOBPass);
                        dobPass.Controls.Add(ddlDayPass);
                        dobPass.Controls.Add(ddlMonthPass);
                        dobPass.Controls.Add(ddlYearPass);

                        RequiredFieldValidator dayPassvc = new RequiredFieldValidator();
                        dayPassvc.ControlToValidate = ddlDayPass.ID;
                        dayPassvc.ErrorMessage = "Select Date";
                        dayPassvc.InitialValue = "-1";
                        dayPassvc.SetFocusOnError = true;
                        dayPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(dayvc);
                        dobPass.Controls.Add(lblError);

                        RequiredFieldValidator monthPassvc = new RequiredFieldValidator();
                        monthPassvc.ControlToValidate = ddlMonthPass.ID;
                        monthPassvc.ErrorMessage = "Select Month";
                        monthPassvc.InitialValue = "-1";
                        monthPassvc.SetFocusOnError = true;
                        monthPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(monthPassvc);

                        RequiredFieldValidator yearPassvc = new RequiredFieldValidator();
                        yearPassvc.ControlToValidate = ddlYearPass.ID;
                        yearPassvc.ErrorMessage = "Select Year";
                        yearPassvc.InitialValue = "-1";
                        yearPassvc.SetFocusOnError = true;
                        yearPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(yearPassvc);


                        HtmlTableRow tr5 = new HtmlTableRow();

                        //MealChioce
                        HtmlTableCell mealChioce = new HtmlTableCell();
                        Label lblMealChioce = new Label();
                        lblMealChioce.Text = "Meal Chioce :";
                        lblMealChioce.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlMealChioce = new DropDownList();
                        ddlMealChioce.Width = new Unit(150, UnitType.Pixel);
                        ddlMealChioce.Items.Add(new ListItem("Select", "Select"));
                        ddlMealChioce.Items.Add(new ListItem("Vegetarian", "Vegetarian"));
                        ddlMealChioce.Items.Add(new ListItem("Asian Vegetarian", "Asian Vegetarian"));
                        ddlMealChioce.Items.Add(new ListItem("Halal", "Halal"));
                        ddlMealChioce.Items.Add(new ListItem("Non-Veg", "Non-Veg"));
                        ddlMealChioce.Items.Add(new ListItem("Jain", "Jain"));
                        ddlMealChioce.Items.Add(new ListItem("Infant Meal", "Infant Meal"));
                        ddlMealChioce.Items.Add(new ListItem("Child Meal Veg", "Child Meal Veg"));
                        ddlMealChioce.Items.Add(new ListItem("Child Meal Non Veg", "Child Meal Non Veg"));
                        ddlMealChioce.SelectedIndex = 0;
                        ddlMealChioce.ID = "ddlMealChioceC" + (i + 1) + (k + i);
                        ddlMealChioce.CausesValidation = true;
                        ddlMealChioce.ValidationGroup = "pax";
                        mealChioce.Controls.Add(lblMealChioce);
                        mealChioce.Controls.Add(ddlMealChioce);

                        //RequiredFieldValidator mealChioceevc = new RequiredFieldValidator();
                        //mealChioceevc.ControlToValidate = ddlMealChioce.ID;
                        //mealChioceevc.ErrorMessage = "Select Meal Chioce";
                        //mealChioceevc.ValidationGroup = "pax";
                        //mealChioceevc.InitialValue = "Select";
                        //mealChioceevc.SetFocusOnError = true;
                        //mealChioce.Controls.Add(mealChioceevc);
                        HtmlTableCell design = new HtmlTableCell();

                        HtmlTableCell addRemove = new HtmlTableCell();
                        Button btnAddC = new Button();
                        btnAddC.ID = "btnAddC" + (k + 1) + (i + 1);
                        btnAddC.Text = "Add";
                        btnAddC.Width = new Unit(50, UnitType.Pixel);
                        btnAddC.Click += new EventHandler(btnAddC_Click);
                        if (k == 0 && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 1)
                        {
                            addRemove.Controls.Add(btnAddC);
                        }
                        Button btnRemoveC = new Button();
                        btnRemoveC.ID = "btnRemoveC" + (k + 1) + (i + 1);
                        btnRemoveC.Text = "Remove";
                        btnRemoveC.Click += new EventHandler(btnRemoveC_Click);
                        if (k != 0 && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 1)
                        {
                            addRemove.Controls.Add(btnRemoveC);
                        }

                        if (k != 0)
                        {
                            tblChilds.Visible = false;
                        }
                        else
                        {
                            if (!IsPostBack)
                            {
                                hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) + 1);
                            }
                        }


                        tr3.Cells.Add(dob);
                        tr3.Cells.Add(nation);
                        tr2.Cells.Add(fname);
                        tr2.Cells.Add(lname);
                        tr4.Cells.Add(pass);
                        tr4.Cells.Add(dobPass);
                        
                        tr5.Cells.Add(mealChioce);
                        tr5.Cells.Add(design);
                        tr5.Cells.Add(addRemove);
                        tblChilds.Rows.Add(tr1);
                        tblChilds.Rows.Add(tr2);
                        tblChilds.Rows.Add(tr3);
                        tblChilds.Rows.Add(tr4);
                        tblChilds.Rows.Add(tr5);

                        HtmlTableCell tcChilds = new HtmlTableCell();
                        tcChilds.ColSpan = 4;
                        tcChilds.Controls.Add(tblChilds);
                        HtmlTableRow trChilds = new HtmlTableRow();
                        trChilds.Cells.Add(tcChilds);
                        tblPassenger.Rows.Add(trChilds);
                    }
                }

                if (dt.Rows[m]["PaxType"].ToString().Trim() == "Infant" && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 0)
                {
                    for (int l = 0; l < Convert.ToInt32(dt.Rows[m]["LabelQty"]); l++)
                    {
                        HtmlTable tblInfants = new HtmlTable();
                        tblInfants.ID = "tblR" + (i + 1) + "Infant" + (l + 1);
                        HtmlTableRow tr = new HtmlTableRow();
                        HtmlTableCell tc = new HtmlTableCell();
                        tc.InnerHtml += "<b>" + " Infant " + (l + 1).ToString() + "</b>";
                        tc.Attributes.Add("class", "ns-h3");
                        tc.ColSpan = 8;
                        tr.Cells.Add(tc);



                        tblInfants.Rows.Add(tr);
                        HtmlTableRow tr1 = new HtmlTableRow();
                        HtmlTableCell prefix = new HtmlTableCell();

                        Label lblPrefix = new Label();
                        lblPrefix.Text = "Title <span style='color:red;'>*</span>:";
                        lblPrefix.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlTitle = new DropDownList();
                        ddlTitle.CssClass = "field120";
                        ddlTitle.Items.Add(new ListItem("Select", "Select"));
                        ddlTitle.Items.Add(new ListItem("Mstr.", "Mstr."));
                        //ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                        //ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
                        ddlTitle.Items.Add(new ListItem("Dr.", "Dr."));
                        ddlTitle.SelectedIndex = 0;
                        ddlTitle.ID = "ddlTitleI" + (i + 1) + (l + 1);
                        ddlTitle.CausesValidation = true;
                        ddlTitle.ValidationGroup = "pax";
                        prefix.Controls.Add(lblPrefix);
                        prefix.Controls.Add(ddlTitle);

                        RequiredFieldValidator prefixvc = new RequiredFieldValidator();
                        prefixvc.ControlToValidate = ddlTitle.ID;
                        prefixvc.ErrorMessage = "Select Title";
                        prefixvc.ValidationGroup = "pax";
                        prefixvc.InitialValue = "Select";
                        prefixvc.SetFocusOnError = true;
                        prefix.Controls.Add(prefixvc);


                        //Email
                        HtmlTableCell email = new HtmlTableCell();
                        Label lblEmail = new Label();
                        lblEmail.Text = "Email:";
                        lblEmail.Width = new Unit(90, UnitType.Pixel);
                        TextBox txtEmail = new TextBox();
                        txtEmail.ID = "txtEmailI" + (i + 1) + (l + 1);
                        txtEmail.Width = new Unit(150, UnitType.Pixel);
                        txtEmail.CssClass = "field120";
                        txtEmail.CausesValidation = true;
                        txtEmail.ValidationGroup = "pax";
                        email.Controls.Add(lblEmail);
                        email.Controls.Add(txtEmail);

                        //RequiredFieldValidator emailvc = new RequiredFieldValidator();
                        //emailvc.ControlToValidate = txtEmail.ID;
                        //emailvc.ErrorMessage = "Enter Email";
                        //emailvc.InitialValue = "";
                        //emailvc.SetFocusOnError = true;
                        //emailvc.ValidationGroup = "pax";
                        //email.Controls.Add(emailvc);

                        RegularExpressionValidator emailrv = new RegularExpressionValidator();
                        emailrv.ControlToValidate = txtEmail.ID;
                        emailrv.ErrorMessage = "Invalid Email address";
                        emailrv.SetFocusOnError = true;
                        emailrv.ValidationGroup = "pax";
                        emailrv.ValidationExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*$";
                        email.Controls.Add(emailrv);

                        tr1.Cells.Add(prefix);
                        tr1.Cells.Add(email);

                        HtmlTableRow tr2 = new HtmlTableRow();
                        HtmlTableCell fname = new HtmlTableCell();
                        //First Name

                        Label lblfname = new Label();
                        lblfname.Text = "First Name: <span style='color:red;'>*</span>:";
                        lblfname.Width = new Unit(90, UnitType.Pixel);
                        TextBox txtFirstName = new TextBox();
                        txtFirstName.ID = "txtFirstNameI" + (i + 1) + (l + i);
                        txtFirstName.Width = new Unit(150, UnitType.Pixel);
                        txtFirstName.CausesValidation = true;
                        txtFirstName.MaxLength = 10;
                        txtFirstName.ValidationGroup = "pax";
                        txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtFirstName.Attributes.Add("ondrop", "return false;");
                        txtFirstName.Attributes.Add("onpaste", "return false;");
                        fname.Controls.Add(lblfname);
                        fname.Controls.Add(txtFirstName);

                        RequiredFieldValidator fnamevc = new RequiredFieldValidator();
                        fnamevc.ControlToValidate = txtFirstName.ID;
                        fnamevc.ErrorMessage = "Enter First Name";
                        fnamevc.InitialValue = "";
                        fnamevc.SetFocusOnError = true;
                        fnamevc.ValidationGroup = "pax";
                        fname.Controls.Add(fnamevc);

                        CustomValidator fnameValidator = new CustomValidator();
                        fnameValidator.ControlToValidate = txtFirstName.ID;
                        fnameValidator.ClientValidationFunction = "validateName";
                        fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        fnameValidator.SetFocusOnError = true;
                        fnameValidator.ValidationGroup = "pax";
                        fname.Controls.Add(fnameValidator);

                        HtmlTableCell lname = new HtmlTableCell();

                        //LastName
                        Label lbllname = new Label();
                        lbllname.Text = "Last Name: <span style='color:red;'>*</span>:";
                        lbllname.Width = new Unit(90, UnitType.Pixel);

                        TextBox txtLastName = new TextBox();
                        txtLastName.ID = "txtLastNameI" + (i + 1) + (l + i);
                        txtLastName.Width = new Unit(150, UnitType.Pixel);
                        txtLastName.CausesValidation = true;
                        txtLastName.MaxLength = 15;
                        txtLastName.ValidationGroup = "pax";
                        txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtLastName.Attributes.Add("ondrop", "return false;");
                        txtLastName.Attributes.Add("onpaste", "return false;");
                        lname.Controls.Add(lbllname);
                        lname.Controls.Add(txtLastName);


                        RequiredFieldValidator lnamevc = new RequiredFieldValidator();
                        lnamevc.ControlToValidate = txtLastName.ID;
                        lnamevc.ErrorMessage = "Enter Last Name";
                        lnamevc.InitialValue = "";
                        lnamevc.SetFocusOnError = true;
                        lnamevc.ValidationGroup = "pax";
                        lname.Controls.Add(lnamevc);

                        CustomValidator lnameValidator = new CustomValidator();
                        lnameValidator.ControlToValidate = txtLastName.ID;
                        lnameValidator.ClientValidationFunction = "validateName";
                        lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        lnameValidator.SetFocusOnError = true;
                        lnameValidator.ValidationGroup = "pax";
                        lname.Controls.Add(lnameValidator);

                        //DoB
                        HtmlTableRow tr3 = new HtmlTableRow();
                        HtmlTableCell dob = new HtmlTableCell();
                        Label lblDOB = new Label();
                        lblDOB.Text = "DOB <span style='color:red;'>*</span>:";
                        lblDOB.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlDay = new DropDownList();
                        ddlDay.ID = "ddlDayI" + (i + 1) + (l + i);
                        ddlDay.Width = new Unit(50, UnitType.Pixel);
                        
                        BindDates(ddlDay);

                        DropDownList ddlMonth = new DropDownList();
                        ddlMonth.ID = "ddlMonthI" + (i + 1) + (l + i);
                        ddlMonth.Width = new Unit(60, UnitType.Pixel);
                        
                        BindMonths(ddlMonth);

                        DropDownList ddlYear = new DropDownList();
                        ddlYear.ID = "ddlYearI" + (i + 1) + (l + i);
                        ddlYear.Width = new Unit(60, UnitType.Pixel);
                        
                        BindYears(ddlYear);

                        Label lblError = new Label();
                        lblError.ID = "lblErrorI" + (i + 1) + (l + i);
                        lblError.Width = new Unit(150, UnitType.Pixel);

                        dob.Controls.Add(lblDOB);
                        dob.Controls.Add(ddlDay);
                        dob.Controls.Add(ddlMonth);
                        dob.Controls.Add(ddlYear);

                        RequiredFieldValidator dayvc = new RequiredFieldValidator();
                        dayvc.ControlToValidate = ddlDay.ID;
                        dayvc.ErrorMessage = "Select Date";
                        dayvc.InitialValue = "-1";
                        dayvc.SetFocusOnError = true;
                        dayvc.ValidationGroup = "pax";
                        dob.Controls.Add(dayvc);
                        dob.Controls.Add(lblError);

                        RequiredFieldValidator monthvc = new RequiredFieldValidator();
                        monthvc.ControlToValidate = ddlMonth.ID;
                        monthvc.ErrorMessage = "Select Month";
                        monthvc.InitialValue = "-1";
                        monthvc.SetFocusOnError = true;
                        monthvc.ValidationGroup = "pax";
                        dob.Controls.Add(monthvc);

                        RequiredFieldValidator yearvc = new RequiredFieldValidator();
                        yearvc.ControlToValidate = ddlYear.ID;
                        yearvc.ErrorMessage = "Select Year";
                        yearvc.InitialValue = "-1";
                        yearvc.SetFocusOnError = true;
                        yearvc.ValidationGroup = "pax";
                        dob.Controls.Add(yearvc);

                         

                        //Nationality
                        HtmlTableCell nation = new HtmlTableCell();
                        Label lblNation = new Label();
                        lblNation.Text = "Nationality <span style='color:red;'>*</span>:";
                        lblNation.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlNationality = new DropDownList();
                        ddlNationality.ID = "ddlNationalityI" + (i + 1) + (l + i);
                        ddlNationality.Width = new Unit(150, UnitType.Pixel);
                        ddlNationality.CausesValidation = true;
                        ddlNationality.ValidationGroup = "pax";
                        BindNationality(ddlNationality);
                        nation.Controls.Add(lblNation);
                        nation.Controls.Add(ddlNationality);

                        RequiredFieldValidator nationvc = new RequiredFieldValidator();
                        nationvc.ControlToValidate = ddlNationality.ID;
                        nationvc.ErrorMessage = "Select Nationality";
                        nationvc.InitialValue = "-1";
                        nationvc.SetFocusOnError = true;
                        nationvc.ValidationGroup = "pax";
                        nation.Controls.Add(nationvc);


                        //PassportNo
                        HtmlTableRow tr4 = new HtmlTableRow();
                        HtmlTableCell pass = new HtmlTableCell();
                        Label lblPassportNo = new Label();
                        lblPassportNo.Text = "Passport No: <span style='color:red;'>*</span>:";
                        lblPassportNo.Width = new Unit(90, UnitType.Pixel);

                        TextBox txtPassportNo = new TextBox();
                        txtPassportNo.ID = "txtPassportNoI" + (i + 1) + (l + i);
                        txtPassportNo.Width = new Unit(150, UnitType.Pixel);
                        txtPassportNo.CausesValidation = true;
                        txtPassportNo.MaxLength = 15;
                        txtPassportNo.ValidationGroup = "pax";
                        //txtPassportNo.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                        txtPassportNo.Attributes.Add("ondrop", "return false;");
                        txtPassportNo.Attributes.Add("onpaste", "return false;");
                        pass.Controls.Add(lblPassportNo);
                        pass.Controls.Add(txtPassportNo);


                        RequiredFieldValidator lpassvc = new RequiredFieldValidator();
                        lpassvc.ControlToValidate = txtLastName.ID;
                        lpassvc.ErrorMessage = "Enter PassportNo";
                        lpassvc.InitialValue = "";
                        lpassvc.SetFocusOnError = true;
                        lpassvc.ValidationGroup = "pax";
                        pass.Controls.Add(lpassvc);

                        CustomValidator lPassValidator = new CustomValidator();
                        lPassValidator.ControlToValidate = txtLastName.ID;
                        lPassValidator.ClientValidationFunction = "validateName";
                        lPassValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                        lPassValidator.SetFocusOnError = true;
                        lPassValidator.ValidationGroup = "pax";
                        pass.Controls.Add(lPassValidator);

                        //Passport Exp

                        HtmlTableCell dobPass = new HtmlTableCell();
                        Label lblDOBPass = new Label();
                        lblDOBPass.Text = "Passport Expiry<span style='color:red;'>*</span>:";
                        lblDOBPass.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlDayPass = new DropDownList();
                        ddlDayPass.ID = "ddlDayPassI" + (i + 1) + (l + i);
                        ddlDayPass.Width = new Unit(50, UnitType.Pixel);
                        
                        BindDates(ddlDayPass);

                        DropDownList ddlMonthPass = new DropDownList();
                        ddlMonthPass.ID = "ddlMonthPassI" + (i + 1) + (l + i);
                        ddlMonthPass.Width = new Unit(60, UnitType.Pixel);
                        BindMonths(ddlMonthPass);

                        DropDownList ddlYearPass = new DropDownList();
                        ddlYearPass.ID = "ddlYearPassI" + (i + 1) + (l + i);
                        ddlYearPass.Width = new Unit(60, UnitType.Pixel);
                        BindPassYears(ddlYearPass);

                        Label lblErrorPass = new Label();
                        lblErrorPass.ID = "lblErrorPassI" + (i + 1) + (l + i);
                        lblErrorPass.Width = new Unit(150, UnitType.Pixel);

                        dobPass.Controls.Add(lblDOBPass);
                        dobPass.Controls.Add(ddlDayPass);
                        dobPass.Controls.Add(ddlMonthPass);
                        dobPass.Controls.Add(ddlYearPass);

                        RequiredFieldValidator dayPassvc = new RequiredFieldValidator();
                        dayPassvc.ControlToValidate = ddlDayPass.ID;
                        dayPassvc.ErrorMessage = "Select Date";
                        dayPassvc.InitialValue = "-1";
                        dayPassvc.SetFocusOnError = true;
                        dayPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(dayvc);
                        dobPass.Controls.Add(lblError);

                        RequiredFieldValidator monthPassvc = new RequiredFieldValidator();
                        monthPassvc.ControlToValidate = ddlMonthPass.ID;
                        monthPassvc.ErrorMessage = "Select Month";
                        monthPassvc.InitialValue = "-1";
                        monthPassvc.SetFocusOnError = true;
                        monthPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(monthPassvc);

                        RequiredFieldValidator yearPassvc = new RequiredFieldValidator();
                        yearPassvc.ControlToValidate = ddlYearPass.ID;
                        yearPassvc.ErrorMessage = "Select Year";
                        yearPassvc.InitialValue = "-1";
                        yearPassvc.SetFocusOnError = true;
                        yearPassvc.ValidationGroup = "pax";
                        dobPass.Controls.Add(yearPassvc);


                        HtmlTableRow tr5 = new HtmlTableRow();
                        //MealChioce
                        HtmlTableCell mealChioce = new HtmlTableCell();
                        Label lblMealChioce = new Label();
                        lblMealChioce.Text = "Meal Chioce:";
                        lblMealChioce.Width = new Unit(90, UnitType.Pixel);
                        DropDownList ddlMealChioce = new DropDownList();
                        ddlMealChioce.Width = new Unit(150, UnitType.Pixel);
                        ddlMealChioce.Items.Add(new ListItem("Select", "Select"));
                        ddlMealChioce.Items.Add(new ListItem("Vegetarian", "Vegetarian"));
                        ddlMealChioce.Items.Add(new ListItem("Asian Vegetarian", "Asian Vegetarian"));
                        ddlMealChioce.Items.Add(new ListItem("Halal", "Halal"));
                        ddlMealChioce.Items.Add(new ListItem("Non-Veg", "Non-Veg"));
                        ddlMealChioce.Items.Add(new ListItem("Jain", "Jain"));
                        ddlMealChioce.Items.Add(new ListItem("Infant Meal", "Infant Meal"));
                        ddlMealChioce.Items.Add(new ListItem("Child Meal Veg", "Child Meal Veg"));
                        ddlMealChioce.Items.Add(new ListItem("Child Meal Non Veg", "Child Meal Non Veg"));
                        ddlMealChioce.SelectedIndex = 0;
                        ddlMealChioce.ID = "ddlMealChioceI" + (i + 1) + (l + i);
                        ddlMealChioce.CausesValidation = true;
                        ddlMealChioce.ValidationGroup = "pax";
                        mealChioce.Controls.Add(lblMealChioce);
                        mealChioce.Controls.Add(ddlMealChioce);

                        //RequiredFieldValidator mealChioceevc = new RequiredFieldValidator();
                        //mealChioceevc.ControlToValidate = ddlMealChioce.ID;
                        //mealChioceevc.ErrorMessage = "Select Meal Chioce";
                        //mealChioceevc.ValidationGroup = "pax";
                        //mealChioceevc.InitialValue = "Select";
                        //mealChioceevc.SetFocusOnError = true;
                        //mealChioce.Controls.Add(mealChioceevc);
                        HtmlTableCell design = new HtmlTableCell();

                        HtmlTableCell addRemove = new HtmlTableCell();
                        Button btnAddI = new Button();
                        btnAddI.ID = "btnAddI" + (l + 1) + (i + 1);
                        btnAddI.Text = "Add";
                        btnAddI.Width = new Unit(50, UnitType.Pixel);
                        btnAddI.Click += new EventHandler(btnAddI_Click);
                        if (l == 0 && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 1)
                        {
                            addRemove.Controls.Add(btnAddI);
                        }
                        Button btnRemoveI = new Button();
                        btnRemoveI.ID = "btnRemoveI" + (l + 1) + (i + 1);
                        btnRemoveI.Text = "Remove";
                        btnRemoveI.Click += new EventHandler(btnRemoveI_Click);
                        if (l != 0 && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 1)
                        {
                            addRemove.Controls.Add(btnRemoveI);
                        }


                        if (l != 0)
                        {
                            tblInfants.Visible = false;
                        }
                        else
                        {
                            if (!IsPostBack)
                            {
                                hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) + 1);
                            }
                        }

                        tr3.Cells.Add(dob);
                        tr3.Cells.Add(nation);
                        tr2.Cells.Add(fname);
                        tr2.Cells.Add(lname);
                        tr4.Cells.Add(pass);
                        tr4.Cells.Add(dobPass);
                        tr5.Cells.Add(mealChioce);
                        tr5.Cells.Add(design);
                        tr5.Cells.Add(addRemove);

                        tblInfants.Rows.Add(tr1);
                        tblInfants.Rows.Add(tr2);
                        tblInfants.Rows.Add(tr3);
                        tblInfants.Rows.Add(tr4);
                        tblInfants.Rows.Add(tr5);
                        HtmlTableCell tcAdults = new HtmlTableCell();
                        tcAdults.ColSpan = 4;
                        tcAdults.Controls.Add(tblInfants);
                        HtmlTableRow trInfants = new HtmlTableRow();
                        trInfants.Cells.Add(tcAdults);
                        tblPassenger.Rows.Add(trInfants);
                    }

                }
            }

        }
    }

    void btnRemoveC_Click(object sender, EventArgs e)
    {
        try
        {
            string roomId = ((System.Web.UI.Control)(sender)).ID.Split('C')[1].Substring(1, 1);
            int childs = Convert.ToInt32(hdnChilds.Value.Split(',')[Convert.ToInt32(roomId) - 1]);
            HtmlTable tblR1C1 = tblPassenger.FindControl("tblR" + roomId + "Childs" + (childs)) as HtmlTable;
            if (tblR1C1 != null)
            {
                tblR1C1.Visible = false;
            }

            string pax = hdnChilds.Value;
            string[] roomChild = pax.Split(',');


            string paxChilds = string.Empty;

            for (int i = 0; i < roomChild.Length; i++)
            {
                if ((i + 1).ToString() == roomId)
                {
                    if (paxChilds == string.Empty)
                    {
                        paxChilds = (childs - 1).ToString();
                    }
                    else
                    {
                        paxChilds += "," + (childs - 1).ToString();
                    }
                    hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) - 1);
                }
                else
                {
                    if (paxChilds == string.Empty)
                    {
                        paxChilds = roomChild[i];
                    }
                    else
                    {
                        paxChilds += "," + roomChild[i];
                    }
                }
            }
            hdnChilds.Value = paxChilds;
            activity = Session["FixedDeparture"] as Activity;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + roomId + "'");
            foreach (DataRow dr in rows)
            {
                if (dr["PaxType"].ToString().Trim() == "Child")
                {

                    HtmlTable table = tblPassenger.FindControl("tblR" + roomId + "Childs" + 1) as HtmlTable;
                    Button btnAddC = table.FindControl("btnAddC" + 1 + roomId) as Button;
                    if (btnAddC != null && btnAddC.Style.Value == "display:none;")
                    {
                        btnAddC.Style.Add("display", "block");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void btnRemoveI_Click(object sender, EventArgs e)
    {
        try
        {
            string roomId = ((System.Web.UI.Control)(sender)).ID.Split('I')[1].Substring(1, 1);
            int infants = Convert.ToInt32(hdnInfants.Value.Split(',')[Convert.ToInt32(roomId) - 1]);
            HtmlTable tblR1I1 = tblPassenger.FindControl("tblR" + roomId + "Infant" + (infants)) as HtmlTable;
            if (tblR1I1 != null)
            {
                tblR1I1.Visible = false;
            }

            string pax = hdnInfants.Value;

            string[] roomInfant = pax.Split(',');

            string paxInfants = string.Empty;

            for (int i = 0; i < roomInfant.Length; i++)
            {
                if ((i + 1).ToString() == roomId)
                {
                    if (paxInfants == string.Empty)
                    {
                        paxInfants = (infants - 1).ToString();
                    }
                    else
                    {
                        paxInfants += "," + (infants - 1).ToString();
                    }
                    hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) - 1);
                }
                else
                {
                    if (paxInfants == string.Empty)
                    {
                        paxInfants = roomInfant[i];
                    }
                    else
                    {
                        paxInfants += "," + roomInfant[i];
                    }
                }
            }
            hdnInfants.Value = paxInfants;

            activity = Session["FixedDeparture"] as Activity;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + roomId + "'");
            foreach (DataRow dr in rows)
            {
                if (dr["PaxType"].ToString().Trim() == "Infant")
                {
                    HtmlTable table = tblPassenger.FindControl("tblR" + roomId + "Infant" + 1) as HtmlTable;
                    Button btnAddI = table.FindControl("btnAddI" + 1 + roomId) as Button;
                    if (btnAddI != null && btnAddI.Style.Value == "display:none;")
                    {
                        btnAddI.Style.Add("display", "block");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void btnRemoveA_Click(object sender, EventArgs e)
    {
        try
        {
            activity = Session["FixedDeparture"] as Activity;
            string roomId = ((System.Web.UI.Control)(sender)).ID.Split('A')[1].Substring(1, 1);
            int adults = Convert.ToInt32(hdnAdults.Value.Split(',')[Convert.ToInt32(roomId) - 1]);
            HtmlTable tblR1A1 = tblPassenger.FindControl("tblR" + roomId + "Adults" + (adults)) as HtmlTable;
            if (tblR1A1 != null)
            {
                tblR1A1.Visible = false;
            }
            string pax = hdnAdults.Value;

            string[] roomAdults = pax.Split(',');


            string paxAdults = string.Empty;

            for (int i = 0; i < roomAdults.Length; i++)
            {
                if ((i + 1).ToString() == roomId)
                {
                    if (paxAdults == string.Empty)
                    {
                        paxAdults = (adults - 1).ToString();
                    }
                    else
                    {
                        paxAdults += "," + (adults - 1).ToString();
                    }
                    hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) - 1);
                }
                else
                {
                    if (paxAdults == string.Empty)
                    {
                        paxAdults = roomAdults[i];
                    }
                    else
                    {
                        paxAdults += "," + roomAdults[i];
                    }
                }
            }
            hdnAdults.Value = paxAdults;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + roomId + "'");
            foreach (DataRow dr in rows)
            {
                if (dr["PaxType"].ToString().Trim() == "Adult")
                {
                    
                        HtmlTable table = tblPassenger.FindControl("tblR" + roomId + "Adults" + 1) as HtmlTable;
                        Button btnAdd = table.FindControl("btnAddA" + 1 + roomId) as Button;
                        if (btnAdd != null && btnAdd.Style.Value == "display:none;")
                        {
                            btnAdd.Style.Add("display", "block");
                        }
                    
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    void BindDates(DropDownList ddlDay)
    {
        ddlDay.Items.Add(new ListItem("Day", "-1"));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    void BindMonths(DropDownList ddlMonth)
    {
        ddlMonth.Items.Add(new ListItem("Month", "-1"));
        string[] months = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        for (int i = 1; i <= months.Length; i++)
        {
            ddlMonth.Items.Add(new ListItem(months[i - 1], i.ToString()));
        }
    }

    void BindYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = 1930; i <= DateTime.Now.Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }
    void BindPassYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = DateTime.Now.Year; i <= 2030; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    void BindNationality(DropDownList ddlNationality)
    {
        ListItem item = new ListItem("Select Nationality", "-1");

        ddlNationality.DataSource = CT.Core.Country.GetNationalityList();
        ddlNationality.DataTextField = "key";
        ddlNationality.DataValueField = "value";
        ddlNationality.DataBind();
        ddlNationality.Items.Insert(0, item);
    }

    #region

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            activity = Session["FixedDeparture"] as Activity;
            string roomId = ((System.Web.UI.Control)(sender)).ID.Split('A')[2].Substring(1, 1);
            int adults = Convert.ToInt32(hdnAdults.Value.Split(',')[Convert.ToInt32(roomId) - 1]);
            HtmlTable tblR1A1 = tblPassenger.FindControl("tblR" + roomId + "Adults" + (adults + 1)) as HtmlTable;
            if (tblR1A1 != null)
            {
                tblR1A1.Visible = true;
            }
            string pax = hdnAdults.Value;

            string[] roomAdults = pax.Split(',');
            string paxAdults = string.Empty;  
   
            for (int i = 0; i < roomAdults.Length; i++)
            {
                if ((i + 1).ToString() == roomId)
                {
                    if (paxAdults == string.Empty)
                    {
                        paxAdults = (adults + 1).ToString();
                    }
                    else
                    {
                        paxAdults += "," + (adults + 1).ToString();
                    }
                    hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) + 1);
                }
                else
                {
                    if (paxAdults == string.Empty)
                    {
                        paxAdults = roomAdults[i];
                    }
                    else
                    {
                        paxAdults += "," + roomAdults[i];
                    }
                }
            }
            hdnAdults.Value = paxAdults;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + roomId + "'");
            DataTable dt = dtTransactionDetails.Clone();
            dt.Columns.Add("LabelQty", typeof(System.Int32));
            int z = 0;
            foreach (DataRow row in rows)
            {
                string find = "PaxType='" + row["PaxType"] + "'";
                DataRow[] foundRows = dt.Select(find);
                if (foundRows.Length == 0)
                {
                    dt.ImportRow(row);
                    dt.Rows[z]["LabelQty"] = 1;
                    z++;
                }
                else
                {
                    int qty = Convert.ToInt32(foundRows[0]["LabelQty"]);
                    qty += 1;
                    foundRows[0]["LabelQty"] = qty;
                }
            }


            foreach (DataRow dr in dt.Rows)
            {
                if (dr["PaxType"].ToString().Trim() == "Adult")
                {
                    if ((adults + 1) == (Convert.ToInt32(dr["LabelQty"])))
                    {
                        HtmlTable table = tblPassenger.FindControl("tblR" + roomId + "Adults" + 1) as HtmlTable;
                        Button btnAdd = table.FindControl("btnAddA" + 1 + roomId) as Button;
                        if (btnAdd != null)
                        {
                            btnAdd.Style.Add("display", "none");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void btnAddI_Click(object sender, EventArgs e)
    {
        try
        {
            string roomId = ((System.Web.UI.Control)(sender)).ID.Split('I')[1].Substring(1, 1);
            int infants = Convert.ToInt32(hdnInfants.Value.Split(',')[Convert.ToInt32(roomId) - 1]);
            HtmlTable tblR1I1 = tblPassenger.FindControl("tblR" + roomId + "Infant" + (infants + 1)) as HtmlTable;
            if (tblR1I1 != null)
            {
                tblR1I1.Visible = true;
            }

            string pax = hdnInfants.Value;

            string[] roomInfant = pax.Split(',');

            string paxInfants = string.Empty;

            for (int i = 0; i < roomInfant.Length; i++)
            {
                if ((i + 1).ToString() == roomId)
                {
                    if (paxInfants == string.Empty)
                    {
                        paxInfants = (infants + 1).ToString();
                    }
                    else
                    {
                        paxInfants += "," + (infants + 1).ToString();
                    }
                    hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) + 1);
                }
                else
                {
                    if (paxInfants == string.Empty)
                    {
                        paxInfants = roomInfant[i];
                    }
                    else
                    {
                        paxInfants += "," + roomInfant[i];
                    }
                }
            }
            hdnInfants.Value = paxInfants;
            activity = Session["FixedDeparture"] as Activity;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + roomId + "'");
            DataTable dt = dtTransactionDetails.Clone();
            dt.Columns.Add("LabelQty", typeof(System.Int32));
            int z = 0;
            foreach (DataRow row in rows)
            {
                string find = "PaxType='" + row["PaxType"] + "'";
                DataRow[] foundRows = dt.Select(find);
                if (foundRows.Length == 0)
                {
                    dt.ImportRow(row);
                    dt.Rows[z]["LabelQty"] = 1;
                    z++;
                }
                else
                {
                    int qty = Convert.ToInt32(foundRows[0]["LabelQty"]);
                    qty += 1;
                    foundRows[0]["LabelQty"] = qty;
                }
            }
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["PaxType"].ToString().Trim() == "Infant")
                {
                    if ((infants + 1) == (Convert.ToInt32(dr["LabelQty"])))
                    {
                        HtmlTable table = tblPassenger.FindControl("tblR" + roomId + "Infant" + 1) as HtmlTable;
                        Button btnAddI = table.FindControl("btnAddI" + 1 + roomId) as Button;
                        if (btnAddI != null)
                        {
                            btnAddI.Style.Add("display", "none");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void btnAddC_Click(object sender, EventArgs e)
    {
        try
        {
            string roomId = ((System.Web.UI.Control)(sender)).ID.Split('C')[1].Substring(1, 1);
            int childs = Convert.ToInt32(hdnChilds.Value.Split(',')[Convert.ToInt32(roomId) - 1]);
            HtmlTable tblR1C1 = tblPassenger.FindControl("tblR" + roomId + "Childs" + (childs + 1)) as HtmlTable;
            if (tblR1C1 != null)
            {
                tblR1C1.Visible = true;
            }

            string pax = hdnChilds.Value;
            string[] roomChild = pax.Split(',');

            string paxChilds = string.Empty;

            for (int i = 0; i < roomChild.Length; i++)
            {
                if ((i + 1).ToString() == roomId)
                {
                    if (paxChilds == string.Empty)
                    {
                        paxChilds = (childs + 1).ToString();
                    }
                    else
                    {
                        paxChilds += "," + (childs + 1).ToString();
                    }
                    hdnPaxCount.Value = Convert.ToString(Convert.ToInt32(hdnPaxCount.Value) + 1);
                }
                else
                {
                    if (paxChilds == string.Empty)
                    {
                        paxChilds = roomChild[i];
                    }
                    else
                    {
                        paxChilds += "," + roomChild[i];
                    }
                }
            }
            hdnChilds.Value = paxChilds;
            activity = Session["FixedDeparture"] as Activity;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + roomId + "'");
            DataTable dt = dtTransactionDetails.Clone();
            dt.Columns.Add("LabelQty", typeof(System.Int32));
            int z = 0;
            foreach (DataRow row in rows)
            {
                string find = "PaxType='" + row["PaxType"] + "'";
                DataRow[] foundRows = dt.Select(find);
                if (foundRows.Length == 0)
                {
                    dt.ImportRow(row);
                    dt.Rows[z]["LabelQty"] = 1;
                    z++;
                }
                else
                {
                    int qty = Convert.ToInt32(foundRows[0]["LabelQty"]);
                    qty += 1;
                    foundRows[0]["LabelQty"] = qty;
                }
            }
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["PaxType"].ToString().Trim() == "Child")
                {
                    if ((childs + 1) == (Convert.ToInt32(dr["LabelQty"])))
                    {
                        HtmlTable table = tblPassenger.FindControl("tblR" + roomId + "Childs" + 1) as HtmlTable;

                        Button btnAddC = table.FindControl("btnAddC" + 1  + roomId) as Button;
                        if (btnAddC != null)
                        {
                            btnAddC.Style.Add("display", "none");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion


    protected void imgSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            if (Session["FixedDeparture"] != null)
            {
                activity = Session["FixedDeparture"] as Activity;
            }
            else
            {
                Response.Redirect("FixedDepartureResults.aspx");
            }
           
            DataTable dtTransHeader = activity.TransactionHeader;
            DataTable dtTransactionDetails = activity.TransactionDetail;
            int paxSerialNo = 1;
            for (int i = 0; i < Convert.ToInt32(dtTransHeader.Rows[0]["RoomCount"]); i++)
            {
                DataRow[] rows = dtTransactionDetails.Select("RoomNO='" + (i + 1) + "'");
                DataTable dt = dtTransactionDetails.Clone();
                dt.Columns.Add("LabelQty", typeof(System.Int32));
                int z = 0;
                foreach (DataRow row in rows)
                {
                    string find = "PaxType='" + row["PaxType"] + "'";
                    DataRow[] foundRows = dt.Select(find);
                    if (foundRows.Length == 0)
                    {
                        dt.ImportRow(row);
                        dt.Rows[z]["LabelQty"] = 1;
                        z++;
                    }
                    else
                    {
                        int qty = Convert.ToInt32(foundRows[0]["LabelQty"]);
                        qty += 1;
                        foundRows[0]["LabelQty"] = qty;
                    }
                }
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    if (dt.Rows[m]["PaxType"].ToString().Trim() == "Adult" && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(dt.Rows[m]["LabelQty"]); j++)
                        {
                            HtmlTable tableA = tblPassenger.FindControl("tblR" + (i + 1) + "Adults" + (j + 1)) as HtmlTable;
                            if (tableA.Visible == true)
                            {
                                DropDownList ddlTitleA = tableA.FindControl("ddlTitleA" + (i + 1) + (j + 1)) as DropDownList;
                                TextBox txtEmailA = tableA.FindControl("txtEmailA" + (i + 1) + (j + 1)) as TextBox;
                                TextBox txtFirstNameA = tableA.FindControl("txtFirstNameA" + (i + 1) + (j + i)) as TextBox;
                                TextBox txtLastNameA = tableA.FindControl("txtLastNameA" + (i + 1) + (j + i)) as TextBox;
                                DropDownList ddlDayA = tableA.FindControl("ddlDayA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlMonthA = tableA.FindControl("ddlMonthA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlYearA = tableA.FindControl("ddlYearA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlNationalityA = tableA.FindControl("ddlNationalityA" + (i + 1) + (j + i)) as DropDownList;
                                TextBox txtPassportNoA = tableA.FindControl("txtPassportNoA" + (i + 1) + (j + i)) as TextBox;
                                DropDownList ddlDayPassA = tableA.FindControl("ddlDayPassA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlMonthPassA = tableA.FindControl("ddlMonthPassA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlYearPassA = tableA.FindControl("ddlYearPassA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlRoomTypeA = tableA.FindControl("ddlRoomTypeA" + (i + 1) + (j + i)) as DropDownList;
                                DropDownList ddlMealChioceA = tableA.FindControl("ddlMealChioceA" + (i + 1) + (j + i)) as DropDownList;

                                if (activity.TransactionDetail == null)
                                {
                                    activity.TransactionDetail = ActivityDetails.GetActivityTransactionDetail(1);
                                }
                                if (activity.TransactionDetail.Rows.Count != Convert.ToInt32(hdnTotalPaxCount.Value))
                                {
                                    DataRow row = activity.TransactionDetail.NewRow();
                                    row["FirstName"] = txtFirstNameA.Text;
                                    row["LastName"] = txtLastNameA.Text;
                                    row["Email"] = txtEmailA.Text;
                                    row["Nationality"] = ddlNationalityA.SelectedItem.Text;
                                    row["PaxSerial"] = paxSerialNo;
                                    paxSerialNo++;
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["PassportNo"] = txtPassportNoA.Text.Trim();
                                    row["Title"] = ddlTitleA.SelectedItem.Text;
                                    row["MealChoice"] = ddlMealChioceA.SelectedItem.Text;
                                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYearA.SelectedItem.Value), Convert.ToInt32(ddlMonthA.SelectedItem.Value), Convert.ToInt32(ddlDayA.SelectedItem.Value));
                                    row["DateOfBirth"] = tempDob;
                                    DateTime tempDobPass = new DateTime(Convert.ToInt32(ddlYearPassA.SelectedItem.Value), Convert.ToInt32(ddlMonthPassA.SelectedItem.Value), Convert.ToInt32(ddlDayPassA.SelectedItem.Value));
                                    row["PassportExpDate"] = tempDob;
                                    row["RoomType"] = ddlRoomTypeA.SelectedItem.Text;
                                    row["RoomNo"] = i + 1;
                                    row["PaxType"] = "Adult";
                                    activity.TransactionDetail.Rows.Add(row);
                                }
                                else
                                {
                                    DataRow row = activity.TransactionDetail.Rows[paxSerialNo-1];
                                    row["FirstName"] = txtFirstNameA.Text;
                                    row["LastName"] = txtLastNameA.Text;
                                    row["Email"] = txtEmailA.Text;
                                    row["Nationality"] = ddlNationalityA.SelectedItem.Text;
                                    row["PaxSerial"] = paxSerialNo;
                                    paxSerialNo++;
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["PassportNo"] = txtPassportNoA.Text.Trim();
                                    row["Title"] = ddlTitleA.SelectedItem.Text;
                                    row["MealChoice"] = ddlMealChioceA.SelectedItem.Text;
                                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYearA.SelectedItem.Value), Convert.ToInt32(ddlMonthA.SelectedItem.Value), Convert.ToInt32(ddlDayA.SelectedItem.Value));
                                    row["DateOfBirth"] = tempDob;
                                    DateTime tempDobPass = new DateTime(Convert.ToInt32(ddlYearPassA.SelectedItem.Value), Convert.ToInt32(ddlMonthPassA.SelectedItem.Value), Convert.ToInt32(ddlDayPassA.SelectedItem.Value));
                                    row["PassportExpDate"] = tempDob;
                                    row["RoomType"] = ddlRoomTypeA.SelectedItem.Text;
                                    //row["RoomNo"] = i + 1;
                                    
                                }
                            }
                        }
                    }
                    if (dt.Rows[m]["PaxType"].ToString().Trim() == "Child" && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 0)
                    {
                        for (int k = 0; k < Convert.ToInt32(dt.Rows[m]["LabelQty"]); k++)
                        {
                            HtmlTable tableC = tblPassenger.FindControl("tblR" + (i + 1) + "Childs" + (k + 1)) as HtmlTable;
                            if (tableC.Visible == true)
                            {
                                DropDownList ddlTitleC = tableC.FindControl("ddlTitleC" + (i + 1) + (k + 1)) as DropDownList;
                                TextBox txtEmailC = tableC.FindControl("txtEmailC" + (i + 1) + (k + 1)) as TextBox;
                                TextBox txtFirstNameC = tableC.FindControl("txtFirstNameC" + (i + 1) + (k + i)) as TextBox;
                                TextBox txtLastNameC = tableC.FindControl("txtLastNameC" + (i + 1) + (k + i)) as TextBox;
                                DropDownList ddlDayC = tableC.FindControl("ddlDayC" + (i + 1) + (k + i)) as DropDownList;
                                DropDownList ddlMonthC = tableC.FindControl("ddlMonthC" + (i + 1) + (k + i)) as DropDownList;
                                DropDownList ddlYearC = tableC.FindControl("ddlYearC" + (i + 1) + (k + i)) as DropDownList;
                                DropDownList ddlNationalityC = tableC.FindControl("ddlNationalityC" + (i + 1) + (k + i)) as DropDownList;
                                TextBox txtPassportNoC = tableC.FindControl("txtPassportNoC" + (i + 1) + (k + i)) as TextBox;
                                DropDownList ddlDayPassC = tableC.FindControl("ddlDayPassC" + (i + 1) + (k + i)) as DropDownList;
                                DropDownList ddlMonthPassC = tableC.FindControl("ddlMonthPassC" + (i + 1) + (k + i)) as DropDownList;
                                DropDownList ddlYearPassC = tableC.FindControl("ddlYearPassC" + (i + 1) + (k + i)) as DropDownList;
                                DropDownList ddlMealChioceC = tableC.FindControl("ddlMealChioceC" + (i + 1) + (k + i)) as DropDownList;
                                if (activity.TransactionDetail == null)
                                {
                                    activity.TransactionDetail = ActivityDetails.GetActivityTransactionDetail(1);
                                }
                                if (activity.TransactionDetail.Rows.Count != Convert.ToInt32(hdnTotalPaxCount.Value))
                                {
                                    DataRow row = activity.TransactionDetail.NewRow();
                                    row["FirstName"] = txtFirstNameC.Text;
                                    row["LastName"] = txtLastNameC.Text;
                                    row["Email"] = txtEmailC.Text;
                                    row["Nationality"] = ddlNationalityC.SelectedItem.Text;
                                    row["PaxSerial"] = paxSerialNo;
                                    paxSerialNo++;
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["PassportNo"] = txtPassportNoC.Text.Trim();
                                    row["Title"] = ddlTitleC.SelectedItem.Text;
                                    row["MealChoice"] = ddlMealChioceC.SelectedItem.Text;
                                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYearC.SelectedItem.Value), Convert.ToInt32(ddlMonthC.SelectedItem.Value), Convert.ToInt32(ddlDayC.SelectedItem.Value));
                                    row["DateOfBirth"] = tempDob;
                                    DateTime tempDobPass = new DateTime(Convert.ToInt32(ddlYearPassC.SelectedItem.Value), Convert.ToInt32(ddlMonthPassC.SelectedItem.Value), Convert.ToInt32(ddlDayPassC.SelectedItem.Value));
                                    row["PassportExpDate"] = tempDob;
                                    row["RoomType"] = string.Empty;
                                    row["RoomNo"] = i + 1;
                                    row["PaxType"] = "Child";
                                    activity.TransactionDetail.Rows.Add(row);
                                }
                                else
                                {
                                    DataRow row = activity.TransactionDetail.Rows[paxSerialNo - 1];
                                    row["FirstName"] = txtFirstNameC.Text;
                                    row["LastName"] = txtLastNameC.Text;
                                    row["Email"] = txtEmailC.Text;
                                    row["Nationality"] = ddlNationalityC.SelectedItem.Text;
                                    row["PaxSerial"] = paxSerialNo;
                                    paxSerialNo++;
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["PassportNo"] = txtPassportNoC.Text.Trim();
                                    row["Title"] = ddlTitleC.SelectedItem.Text;
                                    row["MealChoice"] = ddlMealChioceC.SelectedItem.Text;
                                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYearC.SelectedItem.Value), Convert.ToInt32(ddlMonthC.SelectedItem.Value), Convert.ToInt32(ddlDayC.SelectedItem.Value));
                                    row["DateOfBirth"] = tempDob;
                                    DateTime tempDobPass = new DateTime(Convert.ToInt32(ddlYearPassC.SelectedItem.Value), Convert.ToInt32(ddlMonthPassC.SelectedItem.Value), Convert.ToInt32(ddlDayPassC.SelectedItem.Value));
                                    row["PassportExpDate"] = tempDob;
                                    row["RoomType"] = string.Empty;
                                    //row["RoomNo"] = i + 1;
                                }
                            }
                        }
                    }
                    if (dt.Rows[m]["PaxType"].ToString().Trim() == "Infant" && Convert.ToInt32(dt.Rows[m]["LabelQty"]) > 0)
                    {
                        for (int l = 0; l < Convert.ToInt32(dt.Rows[m]["LabelQty"]); l++)
                        {
                            HtmlTable tableI = tblPassenger.FindControl("tblR" + (i + 1) + "Infant" + (l + 1)) as HtmlTable;
                            if (tableI.Visible == true)
                            {
                                DropDownList ddlTitleI = tableI.FindControl("ddlTitleI" + (i + 1) + (l + 1)) as DropDownList;
                                TextBox txtEmailI = tableI.FindControl("txtEmailI" + (i + 1) + (l + 1)) as TextBox;
                                TextBox txtFirstNameI = tableI.FindControl("txtFirstNameI" + (i + 1) + (l + i)) as TextBox;
                                TextBox txtLastNameI = tableI.FindControl("txtLastNameI" + (i + 1) + (l + i)) as TextBox;
                                DropDownList ddlDayI = tableI.FindControl("ddlDayI" + (i + 1) + (l + i)) as DropDownList;
                                DropDownList ddlMonthI = tableI.FindControl("ddlMonthI" + (i + 1) + (l + i)) as DropDownList;
                                DropDownList ddlYearI = tableI.FindControl("ddlYearI" + (i + 1) + (l+ i)) as DropDownList;
                                DropDownList ddlNationalityI = tableI.FindControl("ddlNationalityI" + (i + 1) + (l + i)) as DropDownList;
                                TextBox txtPassportNoI = tableI.FindControl("txtPassportNoI" + (i + 1) + (l + i)) as TextBox;
                                DropDownList ddlDayPassI = tableI.FindControl("ddlDayPassI" + (i + 1) + (l + i)) as DropDownList;
                                DropDownList ddlMonthPassI = tableI.FindControl("ddlMonthPassI" + (i + 1) + (l + i)) as DropDownList;
                                DropDownList ddlYearPassI = tableI.FindControl("ddlYearPassI" + (i + 1) + (l + i)) as DropDownList;
                                DropDownList ddlMealChioceI = tableI.FindControl("ddlMealChioceI" + (i + 1) + (l + i)) as DropDownList;
                                if (activity.TransactionDetail == null)
                                {
                                    activity.TransactionDetail = ActivityDetails.GetActivityTransactionDetail(1);
                                }
                                if (activity.TransactionDetail.Rows.Count != Convert.ToInt32(hdnTotalPaxCount.Value))
                                {
                                    DataRow row = activity.TransactionDetail.NewRow();
                                    row["FirstName"] = txtFirstNameI.Text;
                                    row["LastName"] = txtLastNameI.Text;
                                    row["Email"] = txtEmailI.Text;
                                    row["Nationality"] = ddlNationalityI.SelectedItem.Text;
                                    row["PaxSerial"] = paxSerialNo;
                                    paxSerialNo++;
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["PassportNo"] = txtPassportNoI.Text.Trim();
                                    row["Title"] = ddlTitleI.SelectedItem.Text;
                                    row["MealChoice"] = ddlMealChioceI.SelectedItem.Text;
                                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYearI.SelectedItem.Value), Convert.ToInt32(ddlMonthI.SelectedItem.Value), Convert.ToInt32(ddlDayI.SelectedItem.Value));
                                    row["DateOfBirth"] = tempDob;
                                    DateTime tempDobPass = new DateTime(Convert.ToInt32(ddlYearPassI.SelectedItem.Value), Convert.ToInt32(ddlMonthPassI.SelectedItem.Value), Convert.ToInt32(ddlDayPassI.SelectedItem.Value));
                                    row["PassportExpDate"] = tempDob;
                                    row["RoomType"] = string.Empty;
                                    row["RoomNo"] = i + 1;
                                    row["PaxType"] = "Infant";
                                    activity.TransactionDetail.Rows.Add(row);
                                }
                                else
                                {
                                    DataRow row = activity.TransactionDetail.Rows[paxSerialNo - 1];
                                    row["FirstName"] = txtFirstNameI.Text;
                                    row["LastName"] = txtLastNameI.Text;
                                    row["Email"] = txtEmailI.Text;
                                    row["Nationality"] = ddlNationalityI.SelectedItem.Text;
                                    row["PaxSerial"] = paxSerialNo;
                                    paxSerialNo++;
                                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                                    row["CreatedDate"] = DateTime.Now;
                                    row["PassportNo"] = txtPassportNoI.Text.Trim();
                                    row["Title"] = ddlTitleI.SelectedItem.Text;
                                    row["MealChoice"] = ddlMealChioceI.SelectedItem.Text;
                                    DateTime tempDob = new DateTime(Convert.ToInt32(ddlYearI.SelectedItem.Value), Convert.ToInt32(ddlMonthI.SelectedItem.Value), Convert.ToInt32(ddlDayI.SelectedItem.Value));
                                    row["DateOfBirth"] = tempDob;
                                    DateTime tempDobPass = new DateTime(Convert.ToInt32(ddlYearPassI.SelectedItem.Value), Convert.ToInt32(ddlMonthPassI.SelectedItem.Value), Convert.ToInt32(ddlDayPassI.SelectedItem.Value));
                                    row["PassportExpDate"] = tempDob;
                                    row["RoomType"] = string.Empty;
                                    //row["RoomNo"] = i + 1;
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < 5; i++)
            {
                DataRow flexRow = activity.FlexDetails.NewRow();
                flexRow["ActivityId"] = activity.Id;
                flexRow["flexCreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                flexRow["flexCreatedOn"] = DateTime.Now;
                flexRow["flexId"] = 0;
                switch (i.ToString())
                {

                    case "0":
                        flexRow["flexLabel"] = "Name";
                        flexRow["flexData"] = txtName.Text;
                        break;
                    case "1":
                        flexRow["flexLabel"] = "Contact number in UAE";
                        flexRow["flexData"] = "txtContact";
                        break;
                    case "2":
                        flexRow["flexLabel"] = "Contact number while travelling";
                        flexRow["flexData"] = txtTravel.Text;
                        break;
                    case "3":
                        flexRow["flexLabel"] = "Emergency Number while travelling";
                        flexRow["flexData"] = txtEmTravel.Text; ;
                        break;
                    case "4":
                        flexRow["flexLabel"] = "How did you get to about us";
                        flexRow["flexData"] = txtAbouts.Text;
                        break;
                    case "5":
                        flexRow["flexLabel"] = "Additional info";
                        flexRow["flexData"] = txtAddinfo.Text; ;
                        break;

                }
                activity.FlexDetails.Rows.Add(flexRow);
            }
            Session["FixedDeparture"] = activity;
            Response.Redirect("FixedDepartureConfirmation.aspx", false);


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
