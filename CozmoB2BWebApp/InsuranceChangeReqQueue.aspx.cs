﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.Insurance;
using CT.AccountingEngine;
using ReligareInsurance;
using CT.TicketReceipt.Common;

public partial class InsuranceChangeReqQueueGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    PagedDataSource pagedData = new PagedDataSource();
    bool isShowAll = false;
    protected string pagingEnable = "style='display:block'";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    isShowAll = true;
                    InitializeControls();

                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "change Request page load", "0");
        }
    }
    private void InitializeControls()
    {
        try
        {
            BindAgency();
            Clear();
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "autoCompInitFlightSearch();", "script");
    }
    private void BindAgency()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--Select Agency--", "0"));

            ddlAgency.SelectedIndex = -1;
            if (Settings.LoginInfo.AgentId > 0)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            //ddlAgency.Items.FindByText(Settings.LoginInfo.AgentName).Selected = true;
            if (Settings.LoginInfo.AgentId > 1)
            {
                ddlAgency.Enabled = false;
            }
            else
            {
                ddlAgency.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetInsuredList();
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "change Request calling search event", "0");
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "change Request calling clear event", "0");
        }
    }

    private void GetInsuredList()
    {
        try
        {
            DateTime departureDate, returnDate, purchaseDateFrom, purchaseDateTo;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (txtDeptDate.Text.Trim().Length > 0)
            {
                departureDate = Convert.ToDateTime(txtDeptDate.Text.Trim(), dateFormat);
            }
            else
            {
                departureDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
            }
            if (txtArrDate.Text.Trim().Length > 0)
            {
                returnDate = Convert.ToDateTime(txtArrDate.Text.Trim(), dateFormat);
            }
            else
            {
                returnDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
            }
            if (txtPurchaseDateFrom.Text.Trim().Length > 0)
            {
                purchaseDateFrom = Convert.ToDateTime(txtPurchaseDateFrom.Text.Trim(), dateFormat);
            }
            else
            {
                purchaseDateFrom = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
            }
            if (txtPurchaseDateTo.Text.Trim().Length > 0)
            {
                purchaseDateTo = Convert.ToDateTime(txtPurchaseDateTo.Text.Trim() + " 11:59:59 PM", dateFormat);
            }
            else
            {
                purchaseDateTo = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy 11:59:59 PM"));
            }
            //string status = ddlStatus.SelectedItem.Text;
            string policyNo = txtPolicyNo.Text.Trim();
            string pnrNo = string.Empty;
            string origin = GetCityCode(Origin.Text.Trim());
            string destination = GetCityCode(Destination.Text.Trim());
            int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            // Added By HAri Malla 25-01-2019
            DataSet dsqueue = new DataSet();
            if (ddlInsType.SelectedValue == "RELIGARE")
            {
                DataTable InsQueue = ReligareHeader.GetInsuranceChangeRequestQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID);
                System.Data.DataColumn newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
                newColumn.DefaultValue = "Religare";
                InsQueue.Columns.Add(newColumn);
                dsqueue.Tables.Add(InsQueue.Copy());
            }
            else if (ddlInsType.SelectedValue == "TUNES")
            {
                DataTable InsQueue = InsuranceQueue.GetInsuranceChangeRequestQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID);
                System.Data.DataColumn newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
                newColumn.DefaultValue = "Tunes";
                InsQueue.Columns.Add(newColumn);
                dsqueue.Tables.Add(InsQueue);
            }
            else
            {
                DataTable InsQueue = ReligareHeader.GetInsuranceChangeRequestQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID);
                System.Data.DataColumn newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
                newColumn.DefaultValue = "Religare";
                InsQueue.Columns.Add(newColumn);
                dsqueue.Tables.Add(InsQueue.Copy());
                DataTable InsQueue1 = InsuranceQueue.GetInsuranceChangeRequestQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID);
                newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
                newColumn.DefaultValue = "Tunes";
                InsQueue1.Columns.Add(newColumn);
                dsqueue.Tables.Add(InsQueue1.Copy());
            }
            Session["InsuranceQueue"] = dsqueue;
            dlInsQueue.DataSource = dsqueue;
            dlInsQueue.DataBind();
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + ":GetInsuredList() ", "0");
        }
    }
    private void Clear()
    {
        txtDeptDate.Text = string.Empty;
        txtArrDate.Text = string.Empty;
        txtPurchaseDateFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        txtPurchaseDateTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        txtPNRno.Text = string.Empty;
        txtPolicyNo.Text = string.Empty;
        Origin.Text = string.Empty;
        Destination.Text = string.Empty;
        //ddlAgency.SelectedIndex = 0;
        isShowAll = true;
        Session["InsuranceQueue"] = null;
        GetInsuredList();
    }

    private string GetCityCode(string searchCity)
    {
        string cityCode = "";

        cityCode = searchCity.Split(',')[0];

        if (cityCode.Contains(" "))
        {
            cityCode = cityCode.Split(' ')[0].Split(')')[0].Replace("(", "");
        }
        else
        {
            cityCode = cityCode.Split(')')[0].Replace("(", "");
        }
        return cityCode;
    }
    
    #region Paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    

    void doPaging()
    {
        // changed by hari malla on 26-01-2019 for paging religare and tunes.
        DataSet ds = (DataSet)Session["InsuranceQueue"];
        DataTable dt = new DataTable();
        dt = ds.Tables[0].Copy();
        if (ds.Tables.Count > 1)
        {
            dt.Merge(ds.Tables[1], true, MissingSchemaAction.Ignore);
        }
        pagedData.DataSource = dt.DefaultView;
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlInsQueue.DataSource = pagedData;
        dlInsQueue.DataBind();
        if (dt.Rows.Count <= 0)
        {
            pagingEnable = "style='display:none'";
        }
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
    #endregion
    
    protected void dlInsQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Refund")
        {
            if (e.CommandArgument.ToString().Length > 0)
            {
                int IPHId = Convert.ToInt32(e.CommandArgument);
                Label lblInsType = e.Item.FindControl("lblInsType") as Label; //Added by Hari MAlla on 25-01-2019
                TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                TextBox txtsupplier = e.Item.FindControl("txtsupplier") as TextBox;
                HiddenField hdnPlanId = e.Item.FindControl("hdnPlanId") as HiddenField;
                HiddenField hdnrequestId = e.Item.FindControl("hdnRequestId") as HiddenField;
                int planId = Convert.ToInt32(hdnPlanId.Value);
                InsuranceHeader header = null;
                ReligareHeader religareHeader = null;
                if (lblInsType.Text.ToUpper() == "RELIGARE")
                {
                    religareHeader = new ReligareHeader();
                    religareHeader.Load(IPHId);                    
                }
                else
                {
                    header = new InsuranceHeader();
                    header.RetrieveConfirmedPlan(IPHId);
                }               
                try
                {
                    //Added By Hari Malla on 26-01-2019 for Religare.
                    if (religareHeader != null)
                    {
                        decimal? bookingAmt = Convert.ToDecimal(religareHeader.Premium_Amount);// + religareHeader.Markup +religareHeader.HandlingFee+ religareHeader.GstValue+religareHeader.B2CMarkup;
                        Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                        cancellationData.Add("Status", "Cancelled");
                        AgentMaster agent = new AgentMaster(religareHeader.Agent_id);
                        if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED")
                        {
                            decimal adminFee = Convert.ToDecimal(txtAdminFee.Text);
                            decimal supplierfee = Convert.ToDecimal(txtsupplier.Text);
                            decimal fee = Convert.ToDecimal(txtAdminFee.Text) - Convert.ToDecimal(txtsupplier.Text);
                            if (bookingAmt >= adminFee + supplierfee)
                            {
                                CancellationCharges cancellationCharge = new CancellationCharges();
                                cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                                cancellationCharge.SupplierFee = Convert.ToDecimal(txtsupplier.Text);
                                cancellationCharge.PaymentDetailId = 0;
                                cancellationCharge.ReferenceId = religareHeader.Header_id;
                                cancellationCharge.ProductType = ProductType.Insurance;
                                cancellationCharge.CancelPenalty = Convert.ToDecimal(bookingAmt);
                                cancellationCharge.CreatedBy = (int)Settings.LoginInfo.UserID;
                                cancellationCharge.Save();
                                NarrationBuilder objNarration = new NarrationBuilder();
                                LedgerTransaction ledgerTxn = new LedgerTransaction();
                                ledgerTxn.LedgerId = religareHeader.Agent_id;
                                int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(religareHeader.Header_id, CT.BookingEngine.ProductType.Insurance);
                                ledgerTxn.Amount = -(adminFee + supplierfee);
                                objNarration.PaxName = string.Format("{0} {1}", religareHeader.ReligarePassengers[0].FirstName, religareHeader.ReligarePassengers[0].LastName);
                                objNarration.Remarks = "Religare Insurance Cancellation Charges for " + religareHeader.Policy_no + ".";
                                objNarration.PolicyNo = religareHeader.Policy_no;
                                objNarration.TravelDate = religareHeader.Startdate.ToString();
                                ledgerTxn.Narration = objNarration;
                                ledgerTxn.ReferenceId = religareHeader.Header_id;
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceCancellationCharge;
                                ledgerTxn.Notes = "";
                                ledgerTxn.Date = DateTime.UtcNow;
                                ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                                ledgerTxn.TransType = religareHeader.Trans_type;                                
                                ledgerTxn.Save();
                                LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                                ledgerTxn.TxnId = 0;
                                ledgerTxn.Amount = +(Convert.ToDecimal(religareHeader.Premium_Amount) - (adminFee + supplierfee));
                                ledgerTxn.Date = DateTime.UtcNow;
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceRefund;
                                objNarration.Remarks = "Refunded for Religare Insurace Policy No " + religareHeader.Policy_no + ".";
                                ledgerTxn.Narration = objNarration;
                                ledgerTxn.Save();
                                LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                                if (adminFee+ supplierfee <= bookingAmt)
                                {
                                    bookingAmt -= adminFee+supplierfee;
                                }

                                if (religareHeader.Trans_type != "B2C")
                                {
                                    agent.CreatedBy = Settings.LoginInfo.UserID;
                                    agent.UpdateBalance(Convert.ToDecimal(bookingAmt));

                                    if (religareHeader.Agent_id == Settings.LoginInfo.AgentId)
                                    {
                                        Settings.LoginInfo.AgentBalance += Convert.ToDecimal(bookingAmt);
                                    }
                                }
                                //updateing service request
                                ServiceRequest sr = new ServiceRequest();
                                sr.UpdateServiceRequestAssignment(Convert.ToInt32(hdnrequestId.Value), (int)ServiceRequestStatus.Completed, (int)Settings.LoginInfo.UserID, (int)ServiceRequestStatus.Completed, null);

                                //Updating Plan status 
                                religareHeader.Status = (int)InsuranceBookingStatus.Cancelled;
                                religareHeader.updateStatus();

                                //Sending email.
                                Hashtable table = new Hashtable(3);
                                table.Add("agentName", agent.Name);
                                table.Add("policyName", religareHeader.ProductName);
                                table.Add("policyNo", religareHeader.Policy_no);

                                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                                UserMaster bookedUser = new UserMaster(religareHeader.Created_by);
                                AgentMaster bookedAgency = new AgentMaster(religareHeader.Agent_id);
                                toArray.Add(bookedUser.Email);
                                toArray.Add(bookedAgency.Email1);
                                toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                                toArray.Add(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]);
                                string message = ConfigurationManager.AppSettings["INSURANCE_REFUND"];
                                try
                                {
                                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Insurance) Response for cancellation. Policy No:(" + religareHeader.Policy_no + ")", message, table);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                            else
                            {
                                Utility.StartupScript(this.Page, "message('Admin Fee and Supplier Fee should not be Greater Then Booking Amount.');", "message");
                                throw new Exception("Admin Fee and Supplier Fee should not be Greater Then Booking Amount.");
                            }                         
                        }
                    }
                    if (header != null)
                    {
                        if (header.InsPlans != null && header.InsPlans.Count > 0)
                        {
                            for (int i = 0; i < header.InsPlans.Count; i++)
                            {
                                if (header.InsPlans[i].PlanId == planId) //checking cancel plan
                                {

                                    decimal bookingAmt = Convert.ToDecimal(header.InsPlans[i].NetAmount) + header.InsPlans[i].Markup - header.InsPlans[i].Discount + header.InsPlans[i].B2CMarkup;

                                    Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                                    //Cancel Method
                                    //ZeusInsB2B.Tunes tunesApi = new ZeusInsB2B.Tunes();
                                    /*SourceDetails agentDetails = new SourceDetails();//Added by Lokesh on 9-Jan-18 .Need to pass UserName,Password,Channel Based on AgentDetails.
                                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                    {
                                        agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["TUNES"];
                                    }
                                    else
                                    {
                                        agentDetails = Settings.LoginInfo.AgentSourceCredentials["TUNES"];
                                    }
                                    tunesApi.UserName = agentDetails.UserID;
                                    tunesApi.Password = agentDetails.Password;
                                    tunesApi.Channel = agentDetails.HAP;*/
                                    //cancellationData = tunesApi.CancelPolicy(header.InsPlans[i].ItineraryID, header.PNR, header.InsPlans[i].PolicyNo, header.InsPlans[0].PolicyPurchasedDate);
                                    cancellationData.Add("Status", "Cancelled");
                                    AgentMaster agent = new AgentMaster(header.AgentId);
                                    if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED")
                                    {

                                        //TODO Ziyad  check the cancellation charges from the response and deduct from booking amount.
                                        decimal adminFee = Convert.ToDecimal(txtAdminFee.Text);
                                        decimal supplierfee = Convert.ToDecimal(txtsupplier.Text);

                                        decimal fee = Convert.ToDecimal(txtAdminFee.Text) - Convert.ToDecimal(txtsupplier.Text);
                         
                                        CancellationCharges cancellationCharge = new CancellationCharges();
                                        cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                                        cancellationCharge.SupplierFee = Convert.ToDecimal(txtsupplier.Text); 
                                        cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                                        cancellationCharge.ReferenceId = header.InsPlans[i].PlanId;
                                        cancellationCharge.ProductType = ProductType.Insurance;
                                        cancellationCharge.CancelPenalty = bookingAmt;
                                        cancellationCharge.CreatedBy = (int)Settings.LoginInfo.UserID;
                                        cancellationCharge.Save();

                                        //Ledger transaction
                                        NarrationBuilder objNarration = new NarrationBuilder();
                                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                                        ledgerTxn.LedgerId = (int)header.AgentId;
                                        int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(header.Id, CT.BookingEngine.ProductType.Insurance);                                        
                                        ledgerTxn.Amount = -(adminFee+ supplierfee);
                                        objNarration.PaxName = string.Format("{0} {1}", header.InsPassenger[0].FirstName, header.InsPassenger[0].LastName);
                                        objNarration.Remarks = "Insurance Cancellation Charges for " + header.InsPlans[i].PolicyNo + ".";
                                        objNarration.PolicyNo = header.InsPlans[i].PolicyNo;
                                        objNarration.TravelDate = header.DepartureDateTime;
                                        ledgerTxn.Narration = objNarration;
                                        ledgerTxn.ReferenceId = (int)header.InsPlans[i].PlanId;
                                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceCancellationCharge;
                                        ledgerTxn.Notes = "";
                                        ledgerTxn.Date = DateTime.UtcNow;
                                        ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                                        ledgerTxn.TransType = header.TransType;
                                        ledgerTxn.PaymentMode = (int)header.PaymentMode;
                                        ledgerTxn.Save();
                                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                                        ledgerTxn.TxnId = 0;
                                        ledgerTxn.Amount = +(header.InsPlans[i].PremiumAmount-(adminFee + supplierfee));
                                        ledgerTxn.Date = DateTime.UtcNow;
                                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceRefund;
                                        objNarration.Remarks = "Refunded for Insurace Policy No "+ header.InsPlans[i].PolicyNo+".";
                                        ledgerTxn.Narration = objNarration;
                                        ledgerTxn.PaymentMode = (int)header.PaymentMode;
                                        ledgerTxn.Save();
                                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                                        if (adminFee <= bookingAmt)
                                        {
                                            bookingAmt -= adminFee;
                                        }

                                        if (header.TransType != "B2C")
                                        {
                                            agent.CreatedBy = Settings.LoginInfo.UserID;
                                            agent.UpdateBalance(bookingAmt);

                                            if (header.AgentId == Settings.LoginInfo.AgentId)
                                            {
                                                Settings.LoginInfo.AgentBalance += bookingAmt;
                                            }
                                        }
                                     
                                        //updateing service request
                                        ServiceRequest sr = new ServiceRequest();
                                        sr.UpdateServiceRequestAssignment(Convert.ToInt32(hdnrequestId.Value), (int)ServiceRequestStatus.Completed, (int)Settings.LoginInfo.UserID, (int)ServiceRequestStatus.Completed, null);

                                        //Updating Plan status  
                                        InsurancePlan.Update(header.InsPlans[i].PlanId, (int)InsuranceBookingStatus.Cancelled, ZeusInsB2B.Zeus.ProposalStatus.CANCELLED.ToString(), true);

                                        //Sending email.
                                        Hashtable table = new Hashtable(3);
                                        table.Add("agentName", agent.Name);
                                        table.Add("policyName", header.InsPlans[i].PlanTitle);
                                        table.Add("policyNo", header.InsPlans[i].PolicyNo);
                                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                                        UserMaster bookedUser = new UserMaster(header.CreatedBy);
                                        AgentMaster bookedAgency = new AgentMaster(header.AgentId);
                                        toArray.Add(bookedUser.Email);
                                        toArray.Add(bookedAgency.Email1);
                                        toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                                        toArray.Add(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]);
                                        string message = ConfigurationManager.AppSettings["INSURANCE_REFUND"];
                                        try
                                        {
                                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Insurance) Response for cancellation. Policy No:(" + header.InsPlans[i].PolicyNo + ")", message, table);
                                        }
                                        catch
                                        {

                                        }
                                    }
                                    break;
                                }
                               
                            }
                            //taking updated cancel Count
                            int cancelCount = InsurancePlan.GetCancelPlansCount(header.Id);
                            if (cancelCount == header.InsPlans.Count)
                            {
                                //updateing Header status
                                InsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)InsuranceBookingStatus.Cancelled);
                            }
                            else
                            {
                                InsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)InsuranceBookingStatus.PartialCancelled);
                            }
                        }                        
                    }
                   Response.Redirect("InsuranceChangeReqQueue.aspx", false);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Update Insurance Change Request : " + ex.ToString(), Request["REMOTE_ADDR"]);
                }
            }
        }
    }
 
    protected void dlInsQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                 Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                HtmlTable tblRefund = e.Item.FindControl("tblRefund") as HtmlTable;
                string state = ((DataRowView)e.Item.DataItem).Row["ProposalState"].ToString();
                Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                Label lblAgentCurrency = e.Item.FindControl("lblAgentCurrency") as Label;
                HiddenField hdnPlanId = e.Item.FindControl("hdnPlanId") as HiddenField;
                HiddenField hdnRequestId = e.Item.FindControl("hdnRequestId") as HiddenField;
                decimal totalAmt = 0, markup = 0, b2cMarkup = 0, discount = 0;
                DataRowView view = e.Item.DataItem as DataRowView;
                int agentId = Convert.ToInt32(view["agencyId"]);
                totalAmt = Convert.ToDecimal(view["NetAmount"]);
                markup = Convert.ToDecimal(view["Markup"]);
                b2cMarkup = Convert.ToDecimal(view["B2CMarkup"]);
                discount = Convert.ToDecimal(view["Discount"]);
                hdnPlanId.Value = Convert.ToString(view["PlanId"]);
                hdnRequestId.Value = Convert.ToString(view["requestId"]);
                Label lblsupplierCurrency = e.Item.FindControl("lblsupplierCurrency") as Label;
                AgentMaster agent = new AgentMaster(agentId);                 
                lblPrice.Text = agent.AgentCurrency + " " + (totalAmt + markup - discount + b2cMarkup).ToString("N" + agent.DecimalValue);//add markup and b2c markup in total price, by chandan on 16062016
                
                btnRefund.OnClientClick = "return ValidateFee('" + e.Item.ItemIndex + "');";
                lblStatus.Text = state;
                if (state != "CANCELLED")
                {
                    tblRefund.Visible = true;
                    lblStatus.Text = "REQUESTED FOR CANCEL";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    tblRefund.Visible = false;
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
                lblAgentCurrency.Text = agent.AgentCurrency;
                lblsupplierCurrency.Text = agent.AgentCurrency;
            }
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + ":ins_changeREq_Q:dlInsQueue_ItemDataBound ", "0");
        }
    }
   
}
