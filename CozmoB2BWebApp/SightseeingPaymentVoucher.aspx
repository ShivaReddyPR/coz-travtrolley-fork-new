﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="SightseeingPaymentVoucher" Title="Sightseeing Payment Voucher" Codebehind="SightseeingPaymentVoucher.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<link href="css/menuDefault.css" rel="stylesheet" type="text/css">
    

    <link href="css/accordion_ui.css" type="text/css" rel="stylesheet" />
   <script type="text/javascript">
        function viewVoucher() {
            window.open('PrintSightseeingVoucher.aspx?ConfNo=<%= itinerary.ConfirmationNo %>', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');
            return false;
        }
     
    </script>
    
    
            
            
 <div  class="ns-h3">
 
 <label>     <strong>  Sightseeing Confirmation</strong> </label>
 
       <label class=" pull-right"> Confirmation No:
                <%=itinerary.ConfirmationNo%>lblEmail
                (Reference No:
                <%=itinerary.BookingReference.Replace("|", "")%>|)</label>
           <div class="clearfix"></div> 
        </div>
        
        <div class="bg_white bor_gray padding-5">
        
       
            <div>                                      
    <div class="col-md-6"><asp:Image ID="imgHeaderLogo" runat="server" Visible="false" /><b> <%=itinerary.ItemName %></b> </div>
                                                                
 <div class="col-md-6">Booked on: <span class="spnred"><%=itinerary.CreatedOn.ToString("dd MMM yyyy hh:mm tt") %></span> </div>
    <div class="col-md-12 margin-top-10">Add :-<%=itinerary.Address %> </div>
   <div class="col-md-12 margin-top-10">  Tel :- <b><%=itinerary.TelePhno %></b> </div>
     <div class="col-md-12 margin-top-10"> No. Of People: <span style="font-weight:bold;"><%=itinerary.AdultCount+itinerary.ChildCount %>
                      (<%=itinerary.AdultCount %>
                      Adult(s),
                      <%=itinerary.ChildCount %>
                      Child(ren))</span>
</div>


    <div class="clearfix"></div>
    </div>
    
        
        
        
        </div> 
        
        
        <div class="padding-5">
        
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        
          
          
          
          
          
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
   <tr> 
   
   <td height="14px"> </td>
   </tr>
   
            <tr>
                <td>
                   <h4> Payment Details</h4>
                </td>
            </tr>
            
                <tr>
                    <td>
                     <table class="table902 bg_white" width="100%" border="1" cellspacing="0" cellpadding="0">
                            <tr>
                               
                            <td>
                                <b>Name</b>
                            </td>
                            <td>
                                <b>No. of Guests</b>
                            </td>
                               
                                <td>
                                    <b>Total</b>
                                </td>
                            </tr>
                             <% decimal gtotal = 0,pageMarkup=0;%>
                            <tr>
                            
                                <td>
                                    <%=itinerary.ItemName%>
                                </td>
                                <td>
                                 <%=itinerary.AdultCount+itinerary.ChildCount %>
                      (<%=itinerary.AdultCount %>
                      Adult(s),
                      <%=itinerary.ChildCount %>
                      Child(ren))
                                </td>
                                <td>
                               
                                <%pageMarkup = itinerary.Price.AsvAmount; %>
                                    <%=itinerary.Price.Currency%>
                                    
                                    <%=Math.Ceiling(itinerary.Price.NetFare + itinerary.Price.Markup).ToString("N"+ Settings.LoginInfo.DecimalValue) %>
                                    
                                </td>
                                
                               
                            </tr>
                          
                           <% gtotal += Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.Markup); %>
                            
                            <%if(pageMarkup > 0){ %>
                            
                            <tr>
                                <td>
                                    <b>Addl Markup </b>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <b class="spnred"><%=itinerary.Price.Currency%>
                                        <%=Math.Round(pageMarkup,Settings.LoginInfo.DecimalValue)%></b>
                                </td>
                            </tr>
                            <%} %>
                         <tr >
                                            <td>
                         <%if (Settings.LoginInfo.LocationCountryCode == "IN")
                             { %>
                                         
                                                Total GST :
                                           
                                           
                                           
                                        <%}
    else
    {%> VAT:
                                                <%} %>
                                                 </td>
                                               <td>
                                    &nbsp;
                                </td>
                                              <td>
                                                 <b class="spnred"><%=itinerary.Price.Currency %>
                                                <%=itinerary.Price.OutputVATAmount.ToString("N" + Settings.LoginInfo.DecimalValue)%></b>
                                            </td>
                                        </tr>
                              <tr>
                                <td>
                                    <b>Grand Total </b>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <b class="spnred"><%=itinerary.Price.Currency %>
                                        <%=((Math.Ceiling(gtotal) + pageMarkup)+Math.Ceiling(itinerary.Price.OutputVATAmount)).ToString("N"+Settings.LoginInfo.DecimalValue)%></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                        
                    <% string cancelData = "", amendmentData = ""; 
                                     if (itinerary.Source == SightseeingBookingSource.GTA)
                                    {
                                        //staticInfo = gta.GetSightseeingItemInformation(itinerary.CityCode, itinerary.ItemName, itinerary.ItemCode);
                                        cancelData = itinerary.CancellationPolicy.Split('@')[0];
                                        amendmentData = itinerary.CancellationPolicy.Split('@')[1];
                                       
                                         
                                    }%>
                    
                    
                    
                      <div>
                            
                            <div  class="ns-h3 margin-top-10">Guest Details</div>
                           <div class="bg_white bor_gray padding-5">
                           
                           
                              <table class="table902" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="red_span">
                                                        <b>Lead Guest</b>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Name :<br />
                                                        </b>
                                                    </td>
                                                    <td >
                                                        <%  string paxName = "";
                                                           
                                                                paxName = itinerary.PaxNames[0].Split('|')[0];
                                                            %>
                                                           <%=paxName%><br />
                                                    </td>
                                                </tr>
                                               
                                        <tr>
                                            <td>
                                                <b>E-mail ID:</b>
                                            </td>
                                            <td>
                                                <%=itinerary.Email%>
                                                
                                            </td>
                                        </tr>
                                    </table>
                               
                                </div>
                                </div>
                                
                               
                               
                               
                                  <div  class="ns-h3 margin-top-10">Cancellation &amp; Charges </div>
                           <div class="bg_white bor_gray padding-5">
                           
                           <div>
                                     
                                    <ul>
                                         <%if (string.IsNullOrEmpty(itinerary.CancellationPolicy))
                                            { %>
                                    <%string[] cdata = cancelData.Split('|');
                                        foreach (string data in cdata)
                                        { %>
                                       <li><%=data%></li>                                           
                                          <%}%>
                                       <%} else { %>
                                       <li><%=itinerary.CancellationPolicy%></li> 
                                       <%} %>
                                    </ul>
                                
                            </div>
                           
                           
                           
                           </div>   </div>
                                
                               
                                  <%if (!string.IsNullOrEmpty(amendmentData) || amendmentData!="")
                                       { %>  
                                  <div  class="ns-h3 margin-top-10">Amendment Policies </div>
                           <div class="bg_white bor_gray padding-5">
                           
                           <div>
                                
                                    <%string[] adata = amendmentData.Split('|');%>
                                    <ul>
                                        <%foreach (string data in adata)
                                          {
                                              if (data.Length > 0)
                                              {%>
                                        <li>
                                            <%=data%></li>
                                        <%}
                                 } %>
                                    </ul>
                                
                            </div>
                           
                               
                                </div>
                              
                                    <%} %>
                       
                       
                       
                       
                                    <div  class="ns-h3 margin-top-10">Notes & Additional Information </div> 
                           <div class="bg_white bor_gray padding-5">
                           
                            
                                  <%if (!string.IsNullOrEmpty(itinerary.Note))
                              { %>
                            
                      
                      <div>
                             
                                <ul>
                                
                                <%if(!string.IsNullOrEmpty(itinerary.Note.Split('#')[0])){ %>
                                  <li><%=itinerary.Note.Split('#')[0]%></li><%}
                                  if (!string.IsNullOrEmpty(itinerary.Note.Split('#')[1]))
                                  {
                                  string[] addInfo = itinerary.Note.Split('#')[1].Split('|');
                                  foreach (string ad in addInfo)
                                  {
                                          %>
                                   <li><%=ad%></li>
                                <%}
                                  }%>
                                </ul>
                            </div>
                            
                      <% }%>
                           
                            </div>
                              
                            <div  class="ns-h3 margin-top-10"> PickUp Location & PickUp Time </div>
                                  <div class="bg_white bor_gray padding-5">
                                       <%if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 1)
                              { %>
                            
                      
                      <div>
                             
                                <ul>
                                
                              <%string[] aData = itinerary.DepPointInfo.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                       foreach (string depoint in aData)
                                       {%>
                                                <li>
                                                    <%=depoint%></li>
                                                <%}
                                                %>
                                </ul>
                            </div>
                            
                      <% }%>
                                      </div>
                            
                        <div  class="ns-h3 margin-top-10">Guest PickUp Point </div>
                           <div class="bg_white bor_gray padding-5">
                           
                           <div>
                                     
                                    <ul>
                                         <%if (!string.IsNullOrEmpty(itinerary.PickupPoint))
                                            { %>
                                   
                                      <li><%=itinerary.PickupPoint%></li>
                                       <%} %>
                                    </ul>
                                
                            </div>
                           
                           
                           
                           </div> 
                                
                    </td>
                </tr>
                
                
                
            </table>
          
            <div style=" float:right; padding-top:20px; padding-bottom:10px; text-align:right">
            
            
             <asp:Button ID="ImageButton1" OnClientClick="return viewVoucher();" Text="View Voucher" CssClass="btn but_b" 
                            runat="server" /></div>
             </td>
                </tr>
                
                
                
            </table> 
        </div>
        

   <!-- For Voucher Mail -->
     <div id="printableArea" runat="server" style="display:none; border-style:solid; border-width:1px;">
     
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="21%">
                                                
                                                <div style='margin-top:5px;'><img src="<%=logoPath %>" width='180px' height='60px'/></div>
                                            </td>
                                           </tr>
                                    </tbody>
                                </table>
                            </td>
                          
                        </tr>
                        <tr>
                           <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;" align="center">
                                <label>Sightseeing Voucher</label>
                            </td>
                        </tr>
                         <tr><td height="10"></td></tr>
                        <tr>
                            <td>
                            
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse">
                                    <tbody>
                                   
                                    <tr>
                                    <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;" colspan="2">
                                <label>Voucher Details</label>
                            </td>
                                    </tr>
                                        <tr>
                                            <td>
                                                <strong>Booking Reference No:</strong>
                                               
                                             <asp:Label ID="lblConfirmation" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Printed On: </strong>
                                               
                                             <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Itinerary Number:</strong>
                                                
                                            <asp:Label ID="lblBookingRef" runat="server" Text=""></asp:Label>

                                            </td>
                                            </tr>
                                         <tr>
                                            <td>
                                                <strong>Booked By :</strong>
                                                
                                            <asp:Label ID="lblBookedBy" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Booking Status :</strong>
                                                
                                            <asp:Label ID="lblBookingStatus" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                                <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                           <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>Service Provider Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                    <tbody>
                                        <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Supplier Name :</strong> 
                                                <asp:Label ID="lblSupplierName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                         <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Supplier Address :</strong> 
                                                <asp:Label ID="lblServiceName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Telephone :</strong> 
                                                <asp:Label ID="lblTelphone" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Email :</strong> 
                                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                    </tbody>
                                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                           <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>Passenger Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Passenger Name:</strong> 
                                                <asp:Label ID="lblGuestName" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td width="50%">
                                                <strong>Service Type :</strong> 
                                                <asp:Label ID="lblServiceType" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Supplier Reference :</strong> 
                                                <asp:Label ID="lblSupplierInfo" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Tour Date:</strong> 
                                                <asp:Label ID="lblTransactionDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <strong>Tour Time:</strong> 
                                                <asp:Label ID="lblPickupTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <strong>Tour Type:</strong> 
                                                <asp:Label ID="lblTourName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td>
                                                <strong>Pick up Location:</strong> 
                                                <asp:Label ID="lblPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                        </tr>--%>
                                         <tr>
                                            <td>
                                                <strong>Guest Pickup Point :</strong> 
                                                <asp:Label ID="lblPickingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <%-- <tr>
                                            <td>
                                                <strong>Starting Point:</strong>  
                                                <asp:Label ID="lblStartingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                        </tr>--%>
                                        <%-- <tr>
                                            <td>
                                                <strong>Ending Point:</strong>  
                                                <asp:Label ID="lblEndingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                        </tr>--%>
                                       <%-- <tr>
                                            <td>
                                                <strong>Entrance Fees:</strong>  
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                        </tr>--%>
                                         <tr>
                                            <td>
                                                <strong>Additional Requests:</strong>  
                                                <asp:Label ID="lblAdditional" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                        </tr>
                                       <%-- <tr>
                                          <td height="20" valign="top">
                                                <strong>Tour Language :</strong> 
                                                <asp:Label ID="lblTourLanguage" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Departure Date  :</strong> 
                                                <asp:Label ID="lblTransactionDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                       <%-- <tr>
                                            <td height="20" valign="top">
                                                <strong>No of Adults </strong>: 
                                                <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Duration :</strong> 
                                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <%--<tr>
                                            <td height="20" valign="top">
                                                <strong>No of Children</strong> : 
                                                <asp:Label ID="lblChildCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            
                                        </tr>--%>
                                       <%-- <tr>
                                            <td height="20" valign="top">
                                                <strong>Country :</strong> 
                                                <asp:Label ID="lblCountry" runat="server" Text=""></asp:Label>
                                            </td>
                                            <%-- <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <%--<tr>
                                            <td height="20" valign="top">
                                                <strong>Tour Address :</strong> 
                                                <asp:Label ID="lblTourAddress" runat="server" Text=""></asp:Label>
                                            </td>
                                             <td valign="top" style="height: 20px">
                                                <strong>Tour Tel number :</strong> 
                                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                                            </td>
                                            
                                        </tr>--%>
                                        <%--<tr>
                                           
                                            <td style="height: 20px">
                                                <strong>Entrance Fees :</strong> <%=Settings.LoginInfo.Currency%> 
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Special Name :</strong> 
                                                <asp:Label ID="lblSpecialName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <%-- <tr>
                                        <td>
                                                <strong>Pick up point :</strong> 
                                                <asp:Label ID="lblPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                            <td >
                                                <strong>Pick up Time :</strong> 
                                                <asp:Label ID="lblPickupTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        
                        <%
                            if (itinerary != null)
                            {
                                if (!string.IsNullOrEmpty(itinerary.CancellationPolicy))
                                {
                                    string[] GetData = itinerary.CancellationPolicy.Split('@');
                                    string[] cancelData = GetData[0].Split('|');
                                    string[] amendData = new string[0]; 
                                    if (GetData.Length > 1)
                                    {
                                       amendData= GetData[1].Split('|');
                                    }
                           %>
                        <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>
                                    Cancellation & Charges
                                </label>
                            </td>
                        </tr>
                         <tr>
                            <td  style="padding-top:5px;">
                                <%for (int i = 0; i < cancelData.Length; i++)
                                  { %>
                                    <p  style="padding-left:5px;"> <%=cancelData[i]%></p>
                                    
                                    <%} %>
                                <%if(amendData.Length>0){ %>
                               <h5><span style="padding-left: 5px;">Amendment Policy :</span></h5>
                               <%for (int j = 0; j < amendData.Length; j++)
                                 { %>
                               <p style="padding-left: 5px;">
                                   <%=amendData[j]%></p>
                               <%} %>
                                <%} %>
                           </td>
                       </tr>
                       <%} %>
                       <tr><td><br /></td></tr>
                       <%
                               /*Condition for Notes 12/03/2016*/
                           if (!string.IsNullOrEmpty(itinerary.Note))
                           {
                               string noteInfo = itinerary.Note.Split('#')[0];
                        %>
                        <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>
                                    Notes &amp; Additional Information</label>
                            </td>
                        </tr>
                        <tr>
                        <td  style="padding-top:5px;">
                        <p style="padding-left:5px;"><%=noteInfo%></p>
                        </td>
                        </tr>
                        
                        <%
                               /*condition For Additional Information 12/03/2016*/
                           if (itinerary.Note.Contains("#"))
                           {
                               string[] addInfo = itinerary.Note.Split('#')[1].Split('|');

                               foreach (string ad in addInfo)
                               {
                                          %>
                        <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=ad%></span><br />
                        </td>
                        </tr>
                        <%}
                           }
                           }%>
                         <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                         <%
                           }%>
                            <tr>
                            <td>
                                &nbsp;
                            </td>
                                
                                <%if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 1)
                                          { %>
                                     <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label> Pcikup Location &amp; Pcikup Date Time</label>
                            </td>
                        </tr> 
                                    <%string[] aData = itinerary.DepPointInfo.Split(',');
                                       foreach (string depoint in aData)
                                       {%>
                                                <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=depoint%></span><br />
                        </td>
                        </tr>
                                                <%}
                                                %>
                                         
                                         <%
                                          }%>
                           
                                <td>
                                &nbsp;
                            </td>
                                 <%if (!string.IsNullOrEmpty(itinerary.PickupPoint))
                                { %>
                                 <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label> Guest Pickup Point</label>
                            </td>
                        </tr> 
                              <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=itinerary.PickupPoint%></span><br />
                        </td>
                        </tr>
                                <%} %>
                                <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="40" bgcolor="" align="center">
                                <div style="border: solid 1px #ccc; padding: 10px; text-align: center">
                                    Cozmo Holidays Tel. No. :99999999
                                    <br />
                                    Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:ziyad@cozmotravel.com">
                                        info@cozmotravel.com</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                          <td >
                        <center>
                        <%--<div>Booked and payable by Gullivers Travel Associates <br/> Only Payment For Extras To Be Collected From The Client.</div>--%>
                        </center>
                        </td>
                        </tr>
                    </tbody>
                </table>
            
    </div>
    <!-- For Voucher Supplier Mail -->
    <div id="PrintSupplerEmail" runat="server" style="display:none; border-style:solid; border-width:1px;">
     
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="21%">
                                                
                                                <div style='margin-top:5px;'><img src="<%=logoPath %>" width='180px' height='60px'/></div>
                                            </td>
                                           </tr>
                                    </tbody>
                                </table>
                            </td>
                          
                        </tr>
                        <tr>
                           <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;" align="center">
                                <label>Sightseeing Voucher</label>
                            </td>
                        </tr>
                         <tr><td height="10"></td></tr>
                        <tr>
                            <td>
                            
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse">
                                    <tbody>
                                   
                                    <tr>
                                    <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;" colspan="2">
                                <label>Voucher Details</label>
                            </td>
                                    </tr>
                                        <tr>
                                            <td>
                                                <strong>Booking Reference No:</strong>
                                               
                                             <asp:Label ID="lblSConfirmation" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Printed On: </strong>
                                               
                                             <asp:Label ID="lblSBookingDate" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Itinerary Number:</strong>
                                                
                                            <asp:Label ID="lblSBookingRef" runat="server" Text=""></asp:Label>

                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Booked By: </strong>
                                                
                                            <asp:Label ID="lblSBookedBy" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                         <tr>
                                            <td>
                                                <strong>Booked Status: </strong>
                                                
                                            <asp:Label ID="lblSBookingStatus" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                           <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>Service Provider Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Supplier Name:</strong> 
                                                <asp:Label ID="lblSSupplierName" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Supplier Address :</strong> 
                                                <asp:Label ID="lblSServiceName" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Telephone :</strong> 
                                                <asp:Label ID="lblSTelephone" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                         <%--<tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Email :</strong> 
                                                <asp:Label ID="lblSEmail" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                    </tbody>
                                    </table>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                           <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>Guest Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Passenger Name :</strong> 
                                                <asp:Label ID="lblSGuestName" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td width="50%">
                                                <strong>Service Type :</strong> 
                                                <asp:Label ID="lblSserviceType" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblSCity" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Supplier Reference :</strong> 
                                                <asp:Label ID="lblSSupplierReference" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Tour Date :</strong> 
                                                <asp:Label ID="lblSTransactionDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Tour Time :</strong> 
                                                <asp:Label ID="lblSPickUpTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Tour Type :</strong> 
                                                <asp:Label ID="lblSTourName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                         <%--<tr>
                                            <td>
                                                <strong>Pick up Location :</strong> 
                                                <asp:Label ID="lblSPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <strong>Guest Pickup Point :</strong> 
                                                <asp:Label ID="lblSPickingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td>
                                                <strong>Starting Point :</strong> 
                                                <asp:Label ID="lblSStartingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                         <%--<tr>
                                            <td>
                                                <strong>Ending Point :</strong> 
                                                <asp:Label ID="lblSEndingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <strong>Entrance Fees :</strong> <%=Settings.LoginInfo.Currency%> 
                                                <asp:Label ID="lblSTotal" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Additional Requests :</strong> <%=Settings.LoginInfo.Currency%> 
                                                <asp:Label ID="lblSAdditionalRequests" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td height="20" valign="top">
                                                <strong>No of Adults </strong>: 
                                                <asp:Label ID="lblSAdultCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Duration :</strong> 
                                                <asp:Label ID="lblSDuration" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                       <%-- <tr>
                                            <td height="20" valign="top">
                                                <strong>No of Children</strong> : 
                                                <asp:Label ID="lblSChildCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>SupplierInfo :</strong> 
                                                <asp:Label ID="lblSSupplierInfo" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <%--<tr>
                                            <td height="20" valign="top">
                                                <strong>Country :</strong> 
                                                <asp:Label ID="lblSCountry" runat="server" Text=""></asp:Label>
                                            </td>
                                             <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblSCity" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                       <%-- <tr>
                                            <td height="20" valign="top">
                                                <strong>Tour Address :</strong> 
                                                <asp:Label ID="lblSTourAddress" runat="server" Text=""></asp:Label>
                                            </td>
                                             <td valign="top" style="height: 20px">
                                                <strong>Tour Tel number :</strong> 
                                                <asp:Label ID="lblSPhone" runat="server" Text=""></asp:Label>
                                            </td>
                                            
                                        </tr>--%>
                                        <tr>
                                           
                                            <%--<td style="height: 20px">
                                                <strong>Entrance Fees :</strong> <%=Settings.LoginInfo.Currency%> 
                                                <asp:Label ID="Label16" runat="server" Text=""></asp:Label>
                                            </td>--%>
                                            <%--<td>
                                                <strong>Special Name :</strong> 
                                                <asp:Label ID="lblSSprcialName" runat="server" Text=""></asp:Label>
                                            </td>--%>
                                        </tr>
                                        <%-- <tr>
                                        <td>
                                                <strong>Pick up point :</strong> 
                                                <asp:Label ID="lblSPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                            <td >
                                                <strong>Pick up Time :</strong> 
                                                <asp:Label ID="lblSPickUpTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        --%>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        
                        <%
                            if (itinerary != null)
                            {
                                if (!string.IsNullOrEmpty(itinerary.CancellationPolicy))
                                {
                                    string[] GetData = itinerary.CancellationPolicy.Split('@');
                                    string[] cancelData = GetData[0].Split('|');
                                    string[] amendData = new string[0]; 
                                    if (GetData.Length > 1)
                                    {
                                       amendData= GetData[1].Split('|');
                                    }
                           %>
                        <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>
                                    Cancellation & Charges
                                </label>
                            </td>
                        </tr>
                         <tr>
                            <td  style="padding-top:5px;">
                                <%for (int i = 0; i < cancelData.Length; i++)
                                  { %>
                                    <p  style="padding-left:5px;"> <%=cancelData[i]%></p>
                                    
                                    <%} %>
                                <%if(amendData.Length>0){ %>
                               <h5><span style="padding-left: 5px;">Amendment Policy :</span></h5>
                               <%for (int j = 0; j < amendData.Length; j++)
                                 { %>
                               <p style="padding-left: 5px;">
                                   <%=amendData[j]%></p>
                               <%} %>
                                <%} %>
                           </td>
                       </tr>
                       <%} %>
                       <tr><td><br /></td></tr>
                       <%
                               /*Condition for Notes 12/03/2016*/
                           if (!string.IsNullOrEmpty(itinerary.Note))
                           {
                               string noteInfo = itinerary.Note.Split('#')[0];
                        %>
                        <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label>
                                    Notes &amp; Additional Information</label>
                            </td>
                        </tr>
                        <tr>
                        <td  style="padding-top:5px;">
                        <p style="padding-left:5px;"><%=noteInfo%></p>
                        </td>
                        </tr>
                        
                        <%
                               /*condition For Additional Information 12/03/2016*/
                           if (itinerary.Note.Contains("#"))
                           {
                               string[] addInfo = itinerary.Note.Split('#')[1].Split('|');

                               foreach (string ad in addInfo)
                               {
                                          %>
                        <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=ad%></span><br />
                        </td>
                        </tr>
                        <%}
                           }
                           }%>
                         <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                         <%
                           }%>
                            <tr>
                            <td>
                                &nbsp;
                            </td>
                                <%if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 1)
                                          { %>
                                     <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label> Pcikup Location &amp; Pcikup Date Time</label>
                            </td>
                        </tr> 
                                    <%string[] aData = itinerary.DepPointInfo.Split(',');
                                       foreach (string depoint in aData)
                                       {%>
                                                <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=depoint%></span><br />
                        </td>
                        </tr>
                                                <%}
                                                %>
                                         
                                         <%
                                          }%>
                           
                                <td>
                                &nbsp;
                            </td>
                            <%if (!string.IsNullOrEmpty(itinerary.PickupPoint))
                                { %>
                                 <tr>
                            <td style="background: #6d6b6c; height: 24px; line-height: 22px; font-size: 13px;
                                color: #fff; padding-left: 10px; font-weight: bold;">
                                <label> Guest Pickup Point</label>
                            </td>
                        </tr> 
                              <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=itinerary.PickupPoint%></span><br />
                        </td>
                        </tr>
                                <%} %>
                                <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="40" bgcolor="" align="center">
                                <div style="border: solid 1px #ccc; padding: 10px; text-align: center">
                                    Cozmo Holidays Tel. No. :99999999
                                    <br />
                                    Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:ziyad@cozmotravel.com">
                                        info@cozmotravel.com</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                          <td >
                        <center>
                        <%--<div>Booked and payable by Gullivers Travel Associates <br/> Only Payment For Extras To Be Collected From The Client.</div>--%>
                        </center>
                        </td>
                        </tr>
                    </tbody>
                </table>
            
    </div>
   
   
   
   
    
    
              
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

