﻿using System;
using CT.BookingEngine;
using CT.MetaSearchEngine;

public partial class BaggageAjax : System.Web.UI.Page
{
    protected int resultId = 0;
    protected bool error = false;
    protected bool sessionError = false;
    protected SearchResult[] results = new SearchResult[0];
    protected SearchResult result = new SearchResult();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!sessionError)
        {
            if (Request["resultId"] != null)
            {
                resultId = Convert.ToInt32(Request["resultId"]);
                //resultId -= 1;
            }
            string sessionId = string.Empty;
            if (Request["sessionId"] != null)
            {
                sessionId = Request["sessionId"];
            }
            results = Basket.FlightBookingSession[sessionId].Result;
            //TODO: Add try catch here
            MetaSearchEngine mse = new MetaSearchEngine(sessionId);
            mse.SettingsLoginInfo = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo;
            mse.AppUserId = (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
            mse.SessionId = sessionId;

            try
            {
                if (results[resultId - 1].ResultBookingSource == BookingSource.PKFares && string.IsNullOrEmpty(results[resultId - 1].BaggageIncludedInFare))
                {
                    SearchRequest request = Session["FlightRequest"] as SearchRequest;
                    result = mse.RepricePKFares(resultId, request, "B2B");
                    Session["RepricedResult"] = result;//Update the repriced result to the session
                    Session["Results"] = results;                    
                }
                else
                {
                    result = results[resultId - 1];
                }                
            }
            catch (BookingEngineException)
            {
                error = true;
            }

            
        }
    }
}