﻿using CT.CMS;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class B2BCMSMaster : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Master.PageRole = true;
                if (!IsPostBack)
                {
                    bindAgentDetails();
                    Session["B2BCMSSliderFiles"] = null;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Binding the agent Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Utility.StartupScript(this.Page, "InitDropZone();", "InitDropZone");
        }
        private void bindAgentDetails()
        {
            try
            {
                ddlAgents.Items.Clear();
                DataSet dtlist = B2BCMSDetails.GetAgentsList(1, "AGENT", 1, ListStatus.Short, RecordStatus.Activated);
                DataView dv = dtlist.Tables[0].DefaultView;
                dv.Sort = "agent_name";
                ddlAgents.AppendDataBoundItems = true;
                ddlAgents.DataSource = dtlist.Tables[0];
                ddlAgents.DataValueField = "agent_id";
                ddlAgents.DataTextField = "agent_name";
                ddlAgents.DataBind();
                ddlAgents.Items.Insert(0, new ListItem("-- Select --", "-1"));

                ddlFindAgent.Items.Clear();
                dv = dtlist.Tables[0].DefaultView;
                ddlFindAgent.AppendDataBoundItems = true;
                ddlFindAgent.DataSource = dtlist.Tables[1];
                ddlFindAgent.DataValueField = "agent_id";
                ddlFindAgent.DataTextField = "agent_name";
                ddlFindAgent.DataBind();
                ddlFindAgent.Items.Insert(0, new ListItem("-- Select --", "-1"));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Binding the agent Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        private void bindGrid(DataTable dt)
        {
            try
            {
                DataView dv = dt.DefaultView;
                dv.RowFilter = "CMS_AGENT_ID IN(" + ddlFindAgent.SelectedValue + ") and len(CMS_IMAGE_FILE_PATH) >0";
                dt = dv.ToTable();
                DLCMSMaster.DataSource = dt;
                DLCMSMaster.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CMS MAster Failed to Binding the sliders. Error:" + ex.ToString(), "");
                throw ex;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt =B2BCMSDetails.GetCMSSliders(Convert.ToInt32(ddlFindAgent.SelectedValue));
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlAgents.SelectedItem.Text = ddlFindAgent.SelectedItem.Text;
                    ddlAgents.Enabled = false;
                    bindGrid(dt);
                    txtBankDesc.Text = dt.Rows[0]["CMS_BANK_DETAILS"].ToString();
                    txtSupportDesc.Text = dt.Rows[0]["CMS_SUPPORT_DETAILS"].ToString();
                    hdnCmsId.Value = dt.Rows[0]["CMS_ID"].ToString();
                    divBankDetails.InnerHtml = txtBankDesc.Text;
                    divSupportDetails.InnerHtml = txtSupportDesc.Text;
                    chkBank.Checked = Convert.ToBoolean(dt.Rows[0]["CMS_IS_BANK"]);
                    chkSupport.Checked = Convert.ToBoolean(dt.Rows[0]["CMS_IS_SUPPORT"]);
                    btnSave.Text = "Update";
                    DLCMSMaster.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CMS MAster Failed to feaching the sliders. Error:" + ex.ToString(), "");
            }
        }
        protected void lbtnstatus_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
                string sliderID = ((Label)row.FindControl("lblSLIDER_ID")).Text;
                string cmsID = ((Label)row.FindControl("lblCMS_ID")).Text;
                bool status = ((Label)row.FindControl("lblstatus")).Text == "1" ? false : true;
                B2BCMSSlider.Update_SliderStatus(ref cmsID, ref sliderID, ref status);
                DataTable dt = B2BCMSDetails.GetCMSSliders(Convert.ToInt32(ddlFindAgent.SelectedValue));
                if (dt != null && dt.Rows.Count > 0)
                    bindGrid(dt);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CMS MAster Failed to Update status. Error:" + ex.ToString(), "");
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            try
            {
                // Binding CMS MAster Details             
                string sDefaultPath = @"images/B2BCMS/";

                bool bHasImageURL = ConfigurationManager.AppSettings["B2BCMSImageUrl"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["B2BCMSImageUrl"]);

                string sImageURL = bHasImageURL ? ConfigurationManager.AppSettings["B2BCMSImageUrl"] : Server.MapPath("~/");

                string sImagePath = ConfigurationManager.AppSettings["B2BCMSImagePath"] != null &&
                    !string.IsNullOrEmpty(ConfigurationManager.AppSettings["B2BCMSImagePath"]) ? ConfigurationManager.AppSettings["B2BCMSImagePath"] :
                    sDefaultPath;

                string sFilePath = sImageURL + sImagePath + (ddlAgents.SelectedValue != "-1" ? ddlAgents.SelectedValue : ddlFindAgent.SelectedValue) + @"/";
                if (!Directory.Exists(sFilePath))
                    Directory.CreateDirectory(sFilePath);

                B2BCMSDetails details = new B2BCMSDetails();
                B2BCMSSlider slider = null;
                details.CMS_ID = string.IsNullOrEmpty(hdnCmsId.Value) ? 0 : Convert.ToInt32(hdnCmsId.Value);
                details.CMS_CREATED_BY = (int)Settings.LoginInfo.UserID;
                details.CMS_AGENT_ID = string.IsNullOrEmpty(hdnCmsId.Value) ? ddlAgents.SelectedValue : ddlFindAgent.SelectedValue;
                details.CMS_BANK_DETAILS = txtBankDesc.Text;
                details.CMS_SUPPORT_DETAILS = txtSupportDesc.Text;
                details.CMS_IS_BANK = chkBank.Checked;
                details.CMS_IS_SUPPORT = chkSupport.Checked;
                details.CMS_STATUS = true;
                List<B2BCMSSlider> lisliders = new List<B2BCMSSlider>();
                List<HttpPostedFile> files = (List<HttpPostedFile>)Session["B2BCMSSliderFiles"];
                if (files != null && files.Count > 0)
                {
                    List<string> liFilenames = new List<string>();
                    var imageFileName = JArray.Parse(hdnImageFileNames.Value).Children();
                    foreach (var item in imageFileName)
                    {
                        liFilenames.Add(item.ToString());
                    }
                    for (int i = 0; i < files.Count; i++)
                    {
                        if (liFilenames.Contains(files[i].FileName))
                        {
                            if (File.Exists(sFilePath + files[i].FileName))
                                File.Delete(sFilePath + files[i].FileName);
                            files[i].SaveAs(sFilePath + files[i].FileName);
                            slider = new B2BCMSSlider();
                            slider.CMS_IMAGE_LABEL = files[i].FileName;
                            slider.CMS_FILE_TYPE = Path.GetExtension(sFilePath + files[i].FileName);
                            slider.CMS_ID = details.CMS_ID;
                            slider.SLIDER_CREATED_BY = (int)Settings.LoginInfo.UserID;
                            slider.SLIDER_STATUS = true;
                            slider.CMS_IMAGE_FILE_PATH = (bHasImageURL ? sFilePath : sFilePath.Replace(sImageURL, string.Empty)) + files[i].FileName;
                            lisliders.Add(slider);
                        }
                    }
                }
                details.sliders = lisliders;
                int result = details.save();
                if (result > 0)
                {
                    msg = string.IsNullOrEmpty(hdnCmsId.Value) ? "Successfully Saved Master Details." : "Successfully Updated Master Details.";
                }
                else
                {
                    msg = string.IsNullOrEmpty(hdnCmsId.Value) ? "Failed to Saving Master Details." : "Failed to Updating Master Details.";
                }
                clear();
                Session["B2BCMSSliderFiles"] = null;
                Utility.StartupScript(this.Page, "message('" + msg + "');", "message");
            }
            catch (Exception ex)
            {
                Session["B2BCMSSliderFiles"] = null;
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Save/Update the CMS Master Details" + ex.ToString(), Request["REMOTE_ADDR"]);
                msg = string.IsNullOrEmpty(hdnCmsId.Value) ? "Failed to Saving Master Details." : "Failed to Updating Master Details.";
                Utility.StartupScript(this.Page, "message('" + msg + "');", "message");
            }
        }
        private void clear()
        {
            btnSave.Text = "Save";
            DLCMSMaster.DataSource = new DataTable ();
            DLCMSMaster.DataBind();
            txtBankDesc.Text = string.Empty;
            txtSupportDesc.Text = string.Empty;
            divBankDetails.InnerHtml = string.Empty;
            divSupportDetails.InnerHtml = string.Empty;
            ddlFindAgent.ClearSelection();
            ddlAgents.ClearSelection();
            bindAgentDetails();
            ddlFindAgent.Enabled = true;
            ddlAgents.Enabled = true;            
            chkSupport.Checked = true;            
            chkBank.Checked = true;
			hdnCmsId.Value = string.Empty;
        }

        protected void DLCMSMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable dt = B2BCMSDetails.GetCMSSliders(Convert.ToInt32(ddlFindAgent.SelectedValue));
                if (dt != null && dt.Rows.Count > 0)
                    bindGrid(dt);
                DLCMSMaster.PageIndex = e.NewPageIndex;
                DLCMSMaster.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to GridView Paging" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            clear();
        }
        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string sDefaultPath = @"images/B2BCMS/";

                bool bHasImageURL = ConfigurationManager.AppSettings["B2BCMSImageUrl"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["B2BCMSImageUrl"]);

                string sImageURL = bHasImageURL ? ConfigurationManager.AppSettings["B2BCMSImageUrl"] : Server.MapPath("~/");

                string sImagePath = ConfigurationManager.AppSettings["B2BCMSImagePath"] != null &&
                    !string.IsNullOrEmpty(ConfigurationManager.AppSettings["B2BCMSImagePath"]) ? ConfigurationManager.AppSettings["B2BCMSImagePath"] :
                    sDefaultPath;

                GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
                string sliderID = ((Label)row.FindControl("lblSLIDER_ID")).Text;
                string filepath = ((HiddenField)row.FindControl("hdnFilepath")).Value;
                if (!string.IsNullOrEmpty(sliderID) && !string.IsNullOrEmpty(filepath))
                {
                    int res = B2BCMSSlider.Delete_Slider(Convert.ToInt32(sliderID));
                    if (res > 0 && File.Exists(sImageURL + filepath))
                        File.Delete(sImageURL +filepath);
                    else if(res > 0 && File.Exists(filepath))
                        File.Delete(filepath);
                    DataTable dt = B2BCMSDetails.GetCMSSliders(Convert.ToInt32(ddlFindAgent.SelectedValue));
                    bindGrid(dt);
                }              
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CMS MAster Failed to Update status. Error:" + ex.ToString(), "");
            }
        }

        protected void btnUpdateImage_Click(object sender, EventArgs e)
        {
            try
            {
                string sDefaultPath = @"images/B2BCMS/";

                bool bHasImageURL = ConfigurationManager.AppSettings["B2BCMSImageUrl"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["B2BCMSImageUrl"]);

                string sImageURL = bHasImageURL ? ConfigurationManager.AppSettings["B2BCMSImageUrl"] : Server.MapPath("~/");

                string sImagePath = ConfigurationManager.AppSettings["B2BCMSImagePath"] != null &&
                    !string.IsNullOrEmpty(ConfigurationManager.AppSettings["B2BCMSImagePath"]) ? ConfigurationManager.AppSettings["B2BCMSImagePath"] :
                    sDefaultPath;
                string sFilePath = sImageURL + sImagePath + (ddlAgents.SelectedValue != "-1" ? ddlAgents.SelectedValue : ddlFindAgent.SelectedValue) + @"/";


                GridViewRow row = (GridViewRow)((Button)sender).NamingContainer;
                btnFind_Click(null, null);
                List<HttpPostedFile> files = (List<HttpPostedFile>)Session["B2BCMSSliderFiles"];
                string sliderID = ((Label)row.FindControl("lblSLIDER_ID")).Text;
                string filepath = ((HiddenField)row.FindControl("hdnFilepath")).Value;   
                string orderId= ((HiddenField)row.FindControl("hdnSliderOrder")).Value;
                int isUpdated= int.Parse(((HtmlInputHidden)row.FindControl("isUploaded")).Value);
                
                B2BCMSSlider slider = new B2BCMSSlider();
                slider.SLIDER_ID = int.Parse(sliderID);
                if (isUpdated!=0)
                {
                    if (File.Exists(sImageURL + filepath))
                        File.Delete(sImageURL + filepath);
                    else if (File.Exists(filepath))
                        File.Delete(filepath);
                    files[0].SaveAs(sFilePath + files[0].FileName);
                    slider.CMS_IMAGE_LABEL = files[0].FileName;
                    slider.CMS_FILE_TYPE = Path.GetExtension(sFilePath + files[0].FileName);                    
                    slider.CMS_IMAGE_FILE_PATH = (bHasImageURL ? sFilePath : sFilePath.Replace(sImageURL, string.Empty)) + files[0].FileName;
                    slider.SLIDER_ORDER = int.Parse(orderId);
                }
                else
                {
                    string fileName = ((Label)row.FindControl("lblfname")).Text;
                    slider.CMS_IMAGE_LABEL = fileName;
                    slider.CMS_FILE_TYPE = Path.GetExtension(sFilePath + fileName);                    
                    slider.CMS_IMAGE_FILE_PATH = (bHasImageURL ? sFilePath : sFilePath.Replace(sImageURL, string.Empty)) + fileName;
                    slider.SLIDER_ORDER = int.Parse(orderId);
                }                
                slider.SLIDER_CREATED_BY = (int)Settings.LoginInfo.UserID;
                slider.Update_SliderDetails();
                Session["B2BCMSSliderFiles"] = null;
                btnFind_Click(null, null);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CMS MAster Failed to Update status. Error:" + ex.ToString(), "");
            }
        }
    } 
}
