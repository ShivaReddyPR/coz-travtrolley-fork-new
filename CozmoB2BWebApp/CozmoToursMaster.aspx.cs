﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

public partial class CozmoToursMasterGUI : CT.Core.ParentPage
{
    private string ACTIVITY_TOUR_SESSION = "_ActivityTourDetails";
    
   
        
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
   
    private void InitializePageControls()
    {
        try
        {
            LoadCountries();
            LoadCity();
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void LoadCountries()
    {
        try
        {
            SortedList countryList = Country.GetCountryList();
            ddlCountry.DataSource = countryList;
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            ddlCountry.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlCountry_SelectIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LoadCity();
            ddlCity.Focus();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void LoadCity()
    {
        try
        {
            SortedList dtCities = City.GetCitiesUnique(ddlCountry.SelectedItem.Value);
            ddlCity.DataSource = dtCities;
            ddlCity.DataTextField = "value";
            ddlCity.DataValueField = "key";
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem("--Select City--", "0"));
            ddlCity.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

 protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
 private ActivityTour CurrentObject
 {
     get
     {
         return (ActivityTour)Session[ACTIVITY_TOUR_SESSION];
     }
     set
     {
         if (value == null)
         {
             Session.Remove(ACTIVITY_TOUR_SESSION);
         }
         else
         {
             Session[ACTIVITY_TOUR_SESSION] = value;
         }

     }
 }
 private void Save()
 {
     try
     {
         if (ddlCountry.SelectedIndex > 0 && ddlCity.SelectedIndex > 0 && txtActivity.Text.Length != 0)
         {
             //ActivityTour activityTour = new ActivityTour();
             ActivityTour activityTour;
             if (CurrentObject == null)
             {
                 activityTour = new ActivityTour();
                 activityTour.CreatedBy = (int)Settings.LoginInfo.UserID;
             }
             else
             {
                 activityTour = CurrentObject;
                 activityTour.ModifiedBy = (int)Settings.LoginInfo.UserID;
             }

             activityTour.CountryCode = ddlCountry.SelectedValue;
             activityTour.CityCode = ddlCity.SelectedValue;
             activityTour.ActivityName = txtActivity.Text.Trim();
             //string url = ConfigurationManager.AppSettings["baseUrl"];
             //activityTour.URL = url+"?countryCode="+ddlCountry.SelectedValue+"&cityCode="+ddlCity.SelectedValue+"&activityId="+txtUrl.Text.Trim();
             activityTour.URL = txtUrl.Text.Trim();
             //activityTour.ActivityId = txtUrl.Text.Trim();

             activityTour.Save();
             BindSearch();
             Clear();
         }

     }
     catch(Exception ex)
     {
         throw ex;
     }
 }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void Clear()
    {
        try
        {
            ddlCountry.SelectedIndex = -1;
            ddlCity.SelectedIndex = -1;
            txtActivity.Text = string.Empty;
            txtUrl.Text = string.Empty;
            btnSave.Text = "Save";
            CurrentObject = null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region GridView
    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        BindSearch();
    //    }
    //    catch (Exception ex)
    //    {
    //        //Label lblMasterError = (Label)this.Master.FindControl("lblError");
    //        //lblMasterError.Visible = true;
    //        //lblMasterError.Text = ex.Message;
    //        //Utility.WriteLog(ex, this.Title);
    //    }
    //}
    private void BindSearch()
    {
        try
        {
            DataTable dt = ActivityTour.GetList();
            gvSearch.DataSource = dt;
            gvSearch.DataBind();
            
        }
        catch { throw; }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            int tourId = Convert.ToInt32(gvSearch.SelectedValue);
            Edit(tourId);
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
           // gvSearch.EditIndex = -1;
           // gvSearch.DataBind();
            BindSearch();
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
   
    private void Edit(int id)
    {
        try
        {
            Clear();
            ActivityTour activityTour = new ActivityTour(id);
            CurrentObject = activityTour;
            ddlCountry.SelectedValue = activityTour.CountryCode;
            LoadCity();
            ddlCity.SelectedValue = activityTour.CityCode;
            txtActivity.Text = activityTour.ActivityName;
            txtUrl.Text = activityTour.URL;
            //txtUrl.Text = txtUrl.Text.Trim();
            btnSave.Text = "Update";
            btnCancel.Text = "Cancel";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
