﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CorpGVRequestGUI" Codebehind="CorpGVRequest.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
  <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="JSLib/prototype.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script>
        function Validate() {
            document.getElementById('errMess').style.display = "none";
            if (getElement('ddlTravelReasons').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Travel Reason from the list";
                return false;
            }
            <%if (IsCorporate)
        { %>
            if (getElement('ddlTravelEmployee').value == "-1") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Travel Employee from the list";
                return false;
            }
            <%} %>
            if (getElement('ddlTravelingCountry').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Visa Country from the list";
                return false;
            }
            if (getElement('ddlVisaType').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Visa Type from the list";
                return false;
            }
            var date1 = document.getElementById('<%= txtFrom.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
                return false;
            }
            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check In Date";
                return false;
            }
            var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            if (todaydate.getTime() > cInDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check In Date should be greater than equal to todays date";
                return false;
            }
        }
    </script>
    <script>
        var cal1;
        

        function init() {

            //    showReturn();
            var today = new Date();
            var todays = today.setDate(today.getDate() + <%=agentblockdays%>);          
            // For making dual Calendar use CalendarGroup  for single Month use Calendar
            cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
            //cal1 = new YAHOO.widget.Calendar("cal1", "Outcontainer1");
            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();
        }

        function showCalendar1() {
            init();
            document.getElementById('container1').style.display = "block";
            document.getElementById('Outcontainer1').style.display = "block";
        }
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            departureDate = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%= txtFrom.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal1.hide();
            document.getElementById('Outcontainer1').style.display = "none";
        }
    YAHOO.util.Event.addListener(window, "load", init);
    </script>
     <script>
        function ShowPax() {
            if (document.getElementById('<%=ddlTravelReasons.ClientID%>') != null) {
                var ReasonId = document.getElementById('<%=ddlTravelReasons.ClientID%>').value;
                var res = ReasonId.split('~');
                var reason = "";
                if (res.length > 1) {
                    reason = res[1];
                }
                if (reason.trim() == "E") {
                    document.getElementById('divAdult').style.display = "block";
                    document.getElementById('divChild').style.display = "block";
                    document.getElementById('divInfant').style.display = "block";
                }
                else
                {
                    document.getElementById('divAdult').style.display = "none";
                    document.getElementById('divChild').style.display = "none";
                    document.getElementById('divInfant').style.display = "none";
                }
            }
        }
        
    </script>
    <div class="body_container"> 
        
         
<h4> Quick Access</h4>
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
<div class="bggray bor_gray paddingtop_10 marbot_20"> 
    <div class="col-md-2">
        <div class="form-group">
            <label>Travel Reason </label><sup style="color:Red">*</sup>
            <asp:DropDownList ID="ddlTravelReasons" CssClass="form-control select-element" runat="server" onchange="ShowPax();">
            </asp:DropDownList>
        </div>

    </div>
    <%if (IsCorporate)
        { %>
    <div class="col-md-2">
        <div class="form-group">
            <label>Traveller Name: </label><sup style="color:Red">*</sup>
            <asp:DropDownList ID="ddlTravelEmployee" CssClass="form-control select-element" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTravelEmployee_SelectedIndexChanged">
            </asp:DropDownList>
        </div>

    </div>
    <%} %>
    <div class="col-md-2"> 
<div class="form-group">
<label> Travelling Country:  </label><sup style="color:Red">*</sup>
    <asp:DropDownList ID="ddlTravelingCountry" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTravelingTo_SelectedIndexChanged" AutoPostBack="true"> </asp:DropDownList>
</div>

</div>
    <div class="col-md-2"> 
<div class="form-group">
<label> Visa Type:  </label><sup style="color:Red">*</sup>
    <asp:DropDownList ID="ddlVisaType" runat="server" CssClass="form-control" > </asp:DropDownList>
</div>

</div>
    <div class="col-md-6"> 
<div class="form-group">
<label> Travel Date:  </label><sup style="color:Red">*</sup>
    <table> 
        <tr> 
        <td><asp:TextBox ID="txtFrom" CssClass="form-control" runat="server" Width="110"></asp:TextBox> </td>
        
         <td>
                                                                <a href="javascript:void(null)" onclick="showCalendar1()">
                                          <img id="dateLink1" src="images/call-cozmo.png" />
                                                                    </a>
                                                                </td>
        
        </tr>
        
        
        </table> 

         <div id="Outcontainer1" style="display:none">
            <div id="container1" style="border:0px solid #ccc;"> </div>
        </div>

</div>

</div>
    <div class="clearfix"> </div>
    <%if (IsCorporate)
        { %>
    <div class="col-md-2"> 
<div class="form-group">
<label> Passport Number: </label>
    <asp:TextBox ID="txtPassportNo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
</div>

</div>

<div class="col-md-2"> 
<div class="form-group">
<label> Employee Code: </label>
<asp:TextBox ID="txtEmployeeCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
</div>

</div>
    <%} %>

<%--<div class="clearfix"> </div>--%>

<div class="col-md-1" id="divAdult" style="display:none;"> 
<div class="form-group">
<label>Adult </label>

<div><asp:DropDownList ID="ddlAdults"  CssClass="form-control  custom-select" runat="server">
    <asp:ListItem Text="1" Value="1" Selected="True"></asp:ListItem>
    <asp:ListItem Text="2" Value="2"></asp:ListItem>
    <asp:ListItem Text="3" Value="3"></asp:ListItem>
    <asp:ListItem Text="4" Value="4"></asp:ListItem>
    <asp:ListItem Text="5" Value="5"></asp:ListItem>
    <asp:ListItem Text="6" Value="6"></asp:ListItem>
    <asp:ListItem Text="7" Value="7"></asp:ListItem>
    <asp:ListItem Text="8" Value="8"></asp:ListItem>
    <asp:ListItem Text="9" Value="9"></asp:ListItem>
     </asp:DropDownList></div>

</div>
    </div>

    <div class="col-md-1" id="divChild" style="display:none;">
    <div class="form-group">
<label>Child </label>

        <div>
            <asp:DropDownList ID="ddlChilds" runat="server">
                <asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                <asp:ListItem Text="9" Value="9"></asp:ListItem>
            </asp:DropDownList>

        </div>

</div>
        </div>
    <div class="col-md-1" id="divInfant" style="display:none;">
    <div class="form-group">
<label>Infant </label>

        <div>
            <asp:DropDownList ID="ddlInfants" runat="server">
                <asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                <asp:ListItem Text="9" Value="9"></asp:ListItem>
            </asp:DropDownList>
        </div>

</div>
</div>
    <%--<div class="clearfix"> </div>--%>
    <%--<div class="col-md-1"> 
<div class="form-group">
    </div>
        </div>--%>
    <div class="col-md-1"> 
<div class="form-group">
<label> &nbsp; </label>

<div><asp:Button ID="btnSearch" runat="server" Text="Search" class="btn but_b" OnClick="btnSearch_Click" OnClientClick="return Validate();" />
     </div>
</div>

</div>
<div class="clearfix"> </div>
</div>

<h2 class="text-center"> Most Popular Visas </h2>
<div class="paddingtop_10 popular_list_container"> 


<div class="col-md-2"> 
<div class="visa_product">
<div> <img src="doc/United-Arab-Emirates_flat.png" /> </div>
<div> <strong> Dubai</strong> </div>
<div> <a href="#">LEARN MORE </a> </div>
 </div>
</div>

<div class="col-md-2"> 
<div class="visa_product">
<div> <img src="doc/chinaflag.png" /> </div>
<div> <strong> China </strong> </div>
<div> <a href="#">LEARN MORE </a> </div>
 </div>
</div>

<div class="col-md-2"> 
<div class="visa_product">
<div> <img src="doc/Thailand.png" /> </div>
<div> <strong> Thailand</strong> </div>
<div> <a href="#">LEARN MORE </a> </div>
 </div>
</div>

<div class="col-md-2"> 
<div class="visa_product">
<div> <img src="doc/Turkey.png" /> </div>
<div> <strong> Turkey </strong> </div>
<div> <a href="#">LEARN MORE </a> </div>
 </div>
</div>


<div class="col-md-2"> 
<div class="visa_product">
<div> <img src="doc/Australia.png" /> </div>
<div> <strong> Australia </strong> </div>
<div> <a href="#">LEARN MORE </a> </div>
 </div>
</div>

<div class="col-md-2"> 
<div class="visa_product">
<div> <img src="doc/malysia.png" /> </div>
<div> <strong> Malaysia </strong> </div>
<div> <a href="#">LEARN MORE </a> </div>
 </div>
</div>

<div class="clearfix"> </div>
</div>
    
        
</div>
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

