using System;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class HotelDetailAjax :System.Web.UI.Page
{
    protected HotelRequest request = new HotelRequest();
    protected HotelSearchResult[] hotelInfo = new HotelSearchResult[0];
    protected HotelSource hSource = new HotelSource();
    protected HotelSearchResult result = new HotelSearchResult();
    protected HotelDetails hotelDetails = new HotelDetails();

    int hotelIndex = 0;
    string sessionId = string.Empty;
    protected string siteName = string.Empty;
    protected bool ErrorFlag = false;
    protected string errorMessage = string.Empty;
    protected int imageIndex = 0;
    protected string latitude = string.Empty, longitude = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["cSessionId"] != null)
        {
            sessionId = Session["cSessionId"].ToString();
            Dictionary<string, string> filterCriteria = (Dictionary<string, string>)Session["hFilterCriteria"];
            HotelSearchResult hData = new HotelSearchResult();
            int noOfPages = 1;
            hotelInfo = hData.GetFilteredResult(sessionId, filterCriteria, ref noOfPages);
            if (hotelInfo == null || hotelInfo.Length == 0)
            {
                errorMessage = "Your Session is expired !! Please" + "<a href=\"HotelSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                return;
            }
            request = (HotelRequest)Session["req"];
            hotelIndex = Convert.ToInt32(Request["hotelIndex"]);


            result = hotelInfo[hotelIndex];
            //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "GetHotelDetails Filtering by Index: " + hotelIndex + " HotelName: " + result.HotelName + " BookingSource: " + result.BookingSource, "0");
            try
            {
                if (result.BookingSource == HotelBookingSource.DOTW && result.CityCode.Length > 0)
                {
                    CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
                    //hotelDetails = dotw.GetItemInformation(result.CityCode, result.HotelName, result.HotelCode);
                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["cSessionId"].ToString());
                    //Added by brahmam getting error in metasearch engine
                    mse.SettingsLoginInfo = Settings.LoginInfo;
                    hotelDetails = mse.GetHotelDetails(result.CityCode, result.HotelCode, result.StartDate, result.EndDate, request.PassengerNationality, request.PassengerCountryOfResidence, result.BookingSource);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split(',')[0];
                        longitude = hotelDetails.Map.Split(',')[1];
                    }
                }
                else if (result.BookingSource == HotelBookingSource.HotelConnect)
                {
                    //if (Request["sourceLink"] == "amenities")
                    //{
                    //    HotelConnect.SearchEngine se = new HotelConnect.SearchEngine();
                    //    hotelDetails.HotelFacilities = se.GetHotelFacilities(Convert.ToInt32(result.HotelCode));
                    //}
                    //else if (Request["sourceLink"] == "description")
                    //{
                    //    hotelDetails.Description = result.HotelDescription;
                    //}
                    //else
                    //{
                    //    hotelDetails.Images = new List<string>();
                    //    hotelDetails.Images.AddRange(result.HotelPicture.Split(','));
                    //}
                    hotelDetails.Description = result.HotelDescription;
                    hotelDetails.Address = result.HotelAddress;
                    hotelDetails.Map = result.HotelMap;
                    if (!string.IsNullOrEmpty(hotelDetails.Map) && hotelDetails.Map.Length > 0)
                    {
                        hotelDetails.Map = hotelDetails.Map.Replace("||", ",");
                        latitude = hotelDetails.Map.Split(',')[0];
                        longitude = hotelDetails.Map.Split(',')[1];
                    }
                    CZInventory.SearchEngine se = new CZInventory.SearchEngine();
                    hotelDetails.HotelFacilities = se.GetHotelFacilities(Convert.ToInt32(result.HotelCode));
                    hotelDetails.Images = new List<string>();
                    hotelDetails.Images.AddRange(result.HotelPicture.Split(','));
                }
                else if (result.BookingSource == HotelBookingSource.RezLive)
                {
                    RezLive.XmlHub rezAPI = new RezLive.XmlHub(Session["cSessionId"].ToString());
                    //Modified by brahmam requarding if static data is not there at the time should be download
                    hotelDetails = rezAPI.GetHotelDetailsInformation(result.CityCode, result.HotelName, result.HotelCode);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                    //if (Request["sourceLink"] == "amenities")
                    //{                        
                    //hotelDetails.HotelFacilities = result.HotelFacilities;
                    //}
                    //else if (Request["sourceLink"] == "description")
                    //{
                    //hotelDetails.Description = result.HotelDescription;
                    //}
                    //else
                    //{
                    //hotelDetails.Images = new List<string>();
                    //hotelDetails.Images.AddRange(result.HotelPicture.Split(','));
                    // }
                }
                else if (result.BookingSource == HotelBookingSource.LOH)
                {
                    LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(Session["cSessionId"].ToString());
                    //hotelDetails = jxe.GetHotelDetails(result.HotelCode,);
                    hotelDetails = jxe.GetHotelDetails(result.HotelCode, result.CityCode); //Insted of CityName changed CityCode modified by brahmam
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split(',')[0];
                        longitude = hotelDetails.Map.Split(',')[1];
                        result.HotelAddress = hotelDetails.Address;
                        result.HotelDescription = hotelDetails.Description;

                    }
                }
                else if (result.BookingSource == HotelBookingSource.HotelBeds)   //Added by brahmam 26.09.2014
                {
                    CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                    hBeds.sessionId = Session["cSessionId"].ToString();
                    hotelDetails = hBeds.GetItemInformation(result.CityCode, result.HotelName, result.HotelCode);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
                else if (result.BookingSource == HotelBookingSource.GTA)
                {
                    CT.BookingEngine.GDS.GTA gta = new CT.BookingEngine.GDS.GTA();
                    gta.SessionId = Session["cSessionId"].ToString();
                    hotelDetails = gta.GetItemInformation(result.CityCode, result.HotelName, result.HotelCode);

                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                    //modified by brahmam not required all images saving audit
                    //Audit.Add(EventType.HotelSearch, Severity.Low, 1, hotelDetails.Images.Count.ToString() + "images is downloaded", "");
                }
                else if (result.BookingSource == HotelBookingSource.Miki)
                {

                    CT.BookingEngine.GDS.MikiApi miki = new CT.BookingEngine.GDS.MikiApi(Session["cSessionId"].ToString(),Settings.LoginInfo.LocationCountryCode);
                    hotelDetails = miki.getStaticInfoOfProduct(result.CityCode, result.HotelName, result.HotelCode);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
                else if (result.BookingSource == HotelBookingSource.TBOHotel)
                {
                    TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                    hotelDetails = tboHotel.GetHotelDetails(result.CityCode, result.HotelCode, Convert.ToInt32(result.SequenceNumber), result.PropertyType);
                    //Map code added by brahmam
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
                //Loading static info Added by brahmam 27.06.2016
                else if (result.BookingSource == HotelBookingSource.WST)
                {
                    CT.BookingEngine.GDS.WST wst = new CT.BookingEngine.GDS.WST();
                    hotelDetails = wst.GetItemInformation(result.CityCode, result.HotelName, result.HotelCode);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
                else if (result.BookingSource == HotelBookingSource.JAC) //Added by brahmam downlaoding static data
                {
                    CT.BookingEngine.GDS.JAC jacApi = new CT.BookingEngine.GDS.JAC();
                    jacApi.SessionId = Session["cSessionId"].ToString();
                    hotelDetails = jacApi.GetPropertyDetails(result.CityCode, result.HotelName, result.HotelCode);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
                else if (result.BookingSource == HotelBookingSource.EET) //Added by brahmam downlaoding static data
                {
                    CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET(Session["cSessionId"].ToString());
                    hotelDetails = eetApi.GetHotelDetails(result.HotelCode, result.CityCode); //Insted of CityName changed CityCode modified by brahmam
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split(',')[0];
                        longitude = hotelDetails.Map.Split(',')[1];
                        result.HotelAddress = hotelDetails.Address;
                        result.HotelDescription = hotelDetails.Description;

                    }
                }
                else if (result.BookingSource == HotelBookingSource.Agoda) //Added by brahmam downlaoding static data
                {
                    CT.BookingEngine.GDS.Agoda agodaApi = new CT.BookingEngine.GDS.Agoda();
                    hotelDetails = agodaApi.GetHotelDetailsInformation(result.CityCode, result.HotelName, result.HotelCode);
                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
//modified by brahmam not required all images saving audit
                //Audit.Add(EventType.HotelSearch, Severity.Low, 1, hotelDetails.Images.Count.ToString() + "images is downloaded", "");
                #region GRN Added By Harish 
                else if(result.BookingSource==HotelBookingSource.GRN)
                {                 
                    CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                    grnApi.GrnUserId = Settings.LoginInfo.UserID;
                    hotelDetails = grnApi.GetHotelDetailsInformation(result.CityCode, result.HotelName, result.HotelCode);
                     HotelSearchResult resultObj = null;
                    resultObj = Session["SelectedHotel"] as HotelSearchResult;
                    if(resultObj != null)
                    {
                        hotelDetails.RoomFacilities = new List<string>();                       
                        if (resultObj.RoomDetails[0].Amenities.Count > 0)
                        {
                            string[] roomFacilList = resultObj.RoomDetails[0].Amenities.ToArray();
                            string[] facilities = roomFacilList[0].Split(';');
                            foreach (string roomFacl in facilities)
                            {
                                hotelDetails.RoomFacilities.Add(roomFacl);
                            }
                        }
                    }
                    
                    if (!string.IsNullOrEmpty(hotelDetails.Map) && hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];                        
                    }
                }
                #endregion
                //Added by somasekhar on 30/08/2018 downlaoding static data
                else if (result.BookingSource == HotelBookingSource.Yatra) 
                {
                    CT.BookingEngine.GDS.Yatra yatraApi = new CT.BookingEngine.GDS.Yatra();
                    // We are Calling this method for Hotel Static Data and Room level Amenities time.
                    hotelDetails = yatraApi.GetHotelDetailsInformation(result.CityCode, result.HotelName, result.HotelCode);

                    HotelSearchResult resultObj = null;
                    resultObj = Session["SelectedHotel"] as HotelSearchResult;
                    if (resultObj != null)
                    {       // Here HotelDetail Search  Time we are getting RoomAmenities    
                        // Overridding the Static RoomAmenities , if exist.
                        if (resultObj.RoomDetails[0].Amenities.Count > 0)
                        {
                            hotelDetails.RoomFacilities = resultObj.RoomDetails[0].Amenities ?? new List<string>();                             
                        }
                    }

                    if (hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }

                //modified by brahmam not required all images saving audit
                //Audit.Add(EventType.HotelSearch, Severity.Low, 1, hotelDetails.Images.Count.ToString() + "images is downloaded", "");
                #region OYO Source Added By Somasekhar on 13/12/2018
                else if (result.BookingSource == HotelBookingSource.OYO)
                {
                    CT.BookingEngine.GDS.OYO oyoApi =new  CT.BookingEngine.GDS.OYO();
                    oyoApi.AppUserId = Settings.LoginInfo.UserID;
                    hotelDetails = oyoApi.GetHotelDetailsInformation(result.CityCode, result.HotelName, result.HotelCode);
                    if (!string.IsNullOrEmpty(hotelDetails.Map) && hotelDetails.Map.Length > 0)
                    {
                        latitude = hotelDetails.Map.Split('|')[0];
                        longitude = hotelDetails.Map.Split('|')[1];
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "0");
            }
        }
        else
        {
            errorMessage = "Your Session is expired !! Please" + "<a href=\"Default.aspx\"" + "\">" + " Retry" + "</a>.";
            ErrorFlag = true;
            return;
        }
        //if (Request["Action"].ToString() == "getCancellationDetail")
        //{
        //    try
        //    {
        //        if (Session["cSessionId"] != null)
        //        {
        //            sessionId = Session["cSessionId"].ToString().Trim();
        //        }
        //        else
        //        {
        //            errorMessage = "Your Session is expired !! Please" + "<a href=\"Default.aspx\"" + "\">" + " Retry" + "</a>.";
        //            ErrorFlag = true;
        //            return;
        //        }
                
                
        //    }
        //    catch (Exception ex)
        //    {
        //        errorMessage = "Unable to Load Cancellation details, data not available.";
        //        ErrorFlag = true;
        //        return;
        //    }
        //}
        //else
        //{
        //    try
        //    {
        //        if (Session["cSessionId"] != null)
        //        {
        //            sessionId = Session["cSessionId"].ToString().Trim();
        //        }
        //        else
        //        {
        //            errorMessage = "Your Session is expired !! Please" + "<a href=\"Default.aspx\"" + "\">" + " Retry" + "</a>.";
        //            ErrorFlag = true;
        //            return;
        //        }
        //        hotelIndex = Convert.ToInt32(Request["hotelIndex"]);
                
        //        if (Request["Action"].ToString() == "getRoomDetail")
        //        {
                    
                    
                    
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (Request["oxiAction"] != null && Request["oxiAction"].ToString() == "getRoomDetail")
        //        {
        //            errorMessage = "Unable to Load Room details, data not available.";
        //        }
        //        else
        //        {
        //            errorMessage = "Unable to Load Hotel details, data not available.";
        //        }
        //        ErrorFlag = true;
        //        return;
        //    }
        //}
    }
}
