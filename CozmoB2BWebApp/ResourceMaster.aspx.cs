﻿#region NameSpace Region
using System;
using System.Web.UI.WebControls;
using System.Data;
using CT.Core;
using CT.CMS;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
#endregion

public partial class ResourceMasterUI : CT.Core.ParentPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!IsPostBack)
                {
                    Clear();
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI Page_Load Event " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    #region Resource Manager  Grid Events
    protected void gridViewRM_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gridViewRM.EditIndex = e.NewEditIndex;
            BindResourcesGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : gridViewRM_RowEditing event error.Reason: " + ex.ToString(), "0");
        }
    }
    protected void gridViewRM_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int resourceId = Convert.ToInt32(gridViewRM.DataKeys[e.RowIndex].Values["resource_id"]);
            TextBox txtResKey = (TextBox)gridViewRM.Rows[e.RowIndex].FindControl("txtEditResKey");
            TextBox txtResValue = (TextBox)gridViewRM.Rows[e.RowIndex].FindControl("txtEditResValue");
            TextBox txtResDesc = (TextBox)gridViewRM.Rows[e.RowIndex].FindControl("txtEditResDesc");
            if (txtResKey != null && txtResValue != null && txtResDesc != null && resourceId > 0 && ddlCultureCode.SelectedIndex > 0 && ddlResourceClass.SelectedIndex > 0)
            {
                DataTable dtKeysList = ResourceMaster.GetResourceKeysList(ddlCultureCode.SelectedItem.Value, ddlResourceClass.SelectedItem.Value);
                DataRow[] results = dtKeysList.Select("culture_code = '" + ddlCultureCode.SelectedItem.Value + "' AND resource_class = '" + ddlResourceClass.SelectedItem.Value + "' AND resource_key = '" + txtResKey.Text.Trim() + "' AND resource_id NOT IN ('" + resourceId + "')");

                if (results.Length > 0)
                {
                    Utility.StartupScript(this.Page, "alert('Duplicate  Item. Key Already Exists !')", "");
                    throw new Exception("Key Already Exists !");
                }

                ResourceMaster rm = new ResourceMaster();
                rm.Resource_Id = resourceId;
                rm.Culture_code = ddlCultureCode.SelectedItem.Value;
                rm.Resource_class = ddlResourceClass.SelectedItem.Value;
                rm.Resource_description = txtResDesc.Text.Trim();
                rm.Resource_key = txtResKey.Text.Trim();
                rm.Resource_value = txtResValue.Text.Trim();
                rm.Status = "A";
                rm.save();
                gridViewRM.EditIndex = -1;
                BindResourcesGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "updateNotification();", "SCRIPT");
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : gridViewRM_RowUpdating event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewRM_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gridViewRM.EditIndex = -1;
            BindResourcesGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : gridViewRM_RowCancelingEdit event error.Reason: " + ex.ToString(), "0");

        }
    }
    protected void gridViewRM_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            int resourceId = Convert.ToInt32(gridViewRM.DataKeys[e.RowIndex].Values["resource_id"]);
            HiddenField hdnStatus = (HiddenField)gridViewRM.Rows[e.RowIndex].FindControl("hdnStatus");
            string rowStatus = string.Empty;
            if (hdnStatus != null && hdnStatus.Value == "A")
            {
                rowStatus = "D";
            }
            else if (hdnStatus != null && hdnStatus.Value == "D")
            {
                rowStatus = "A";
            }
            if (!string.IsNullOrEmpty(rowStatus) && resourceId > 0)
            {
                ResourceMaster.DeleteResourceKey(resourceId, rowStatus);
                BindResourcesGrid();
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "deleteAlert();", "SCRIPT");
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CMSMaster page : gridViewTM_RowDeleting event error.Reason: " + ex.ToString(), "0");
        }

    }
    protected void gridViewRM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("AddNew"))
            {
                TextBox txtResKey = (TextBox)gridViewRM.FooterRow.FindControl("txtResKey");
                TextBox txtResValue = (TextBox)gridViewRM.FooterRow.FindControl("txtResValue");
                TextBox txtResDesc = (TextBox)gridViewRM.FooterRow.FindControl("txtResDesc");

                if (txtResKey != null && txtResValue != null && txtResDesc != null && ddlCultureCode.SelectedIndex > 0 && ddlResourceClass.SelectedIndex > 0)
                {
                    DataTable dtKeysList = ResourceMaster.GetResourceKeysList(ddlCultureCode.SelectedItem.Value, ddlResourceClass.SelectedItem.Value);
                    DataRow[] results = dtKeysList.Select("culture_code = '" + ddlCultureCode.SelectedItem.Value + "' AND resource_class = '" + ddlResourceClass.SelectedItem.Value + "' AND resource_key = '" + txtResKey.Text.Trim() + "'");

                    if (results.Length > 0)
                    {
                        Utility.StartupScript(this.Page, "alert('Duplicate  Item. Key Already Exists !')", "");
                        throw new Exception("Key Already Exists !");
                    }
                    ResourceMaster rm = new ResourceMaster();
                    rm.Resource_Id = 0;
                    rm.Culture_code = ddlCultureCode.SelectedItem.Value;
                    rm.Resource_class = ddlResourceClass.SelectedItem.Value;
                    rm.Resource_description = txtResDesc.Text.Trim();
                    rm.Resource_key = txtResKey.Text.Trim();
                    rm.Resource_value = txtResValue.Text.Trim();
                    rm.Status = "A";
                    rm.save();
                    gridViewRM.EditIndex = -1;
                    BindResourcesGrid();
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "saveNotification();", "SCRIPT");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : gridViewRM_RowCommand event error.Reason: " + ex.ToString(), "0");

        }

    }
    protected void gridViewRM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Button btnDel = (Button)e.Row.FindControl("btnDeleteRM");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                Label lblCultureCode = (Label)e.Row.FindControl("lblCultureCode");

                if (btnDel != null && lblStatus != null && drv.Row["STATUS"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["STATUS"])))
                {
                    if (Convert.ToString(drv.Row["STATUS"]) == "A")
                    {
                        btnDel.Text = "DeActivate";
                        btnDel.OnClientClick = "return confirm('Do you want to Deactivate?')";
                        lblStatus.Text = "Active";
                    }
                    else
                    {
                        btnDel.Text = "Activate";
                        btnDel.OnClientClick = "return confirm('Do you want to Activate?')";
                        lblStatus.Text = "Inactive";
                    }
                }
                if (lblCultureCode != null && drv.Row["culture_code"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["culture_code"])))
                {
                    if (Convert.ToString(drv.Row["culture_code"]) == "AR")
                    {
                        lblCultureCode.Text = "ARABIC";
                    }
                    else if (Convert.ToString(drv.Row["culture_code"]) == "EN")
                    {
                        lblCultureCode.Text = "ENGLISH";
                    }
                    else
                    {
                        lblCultureCode.Text = string.Empty;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : gridViewRM_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }
    #endregion

    #region Button Click Events
    public void btnLoad_Click(object sender, EventArgs e)
    {
        try
        {
            BindResourcesGrid();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : btnLoad_Click event error.Reason: " + ex.ToString(), "0");
        }
    }
    public void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("ResourceMaster.aspx");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "ResourceMasterUI page : btnClear_Click event error.Reason: " + ex.ToString(), "0");
        }
    }

    #endregion

    #region Private Methods
    private void BindResourcesGrid()
    {
        if (ddlCultureCode.SelectedIndex > 0 && ddlResourceClass.SelectedIndex > 0)
        {
            DataTable dtDetails = ResourceMaster.GetResourceKeysList(ddlCultureCode.SelectedItem.Value, ddlResourceClass.SelectedItem.Value);
            if (dtDetails != null && dtDetails.Rows.Count > 0)
            {
                gridViewRM.DataSource = dtDetails;
                gridViewRM.DataBind();

            }
            else
            {
                dtDetails.Rows.Add(dtDetails.NewRow());
                gridViewRM.DataSource = dtDetails;
                gridViewRM.DataBind();
                gridViewRM.Rows[0].Visible = false;

            }
            ddlCultureCode.Enabled = false;
            ddlResourceClass.Enabled = false;
            btnLoad.Visible = false;
        }
        else
        {
            ddlCultureCode.Enabled = true;
            ddlResourceClass.Enabled = true;
            btnLoad.Visible = true;
        }
    }
    private void Clear()
    {
        try
        {
            ddlCultureCode.SelectedIndex = 0;
            ddlResourceClass.SelectedIndex = 0;
            btnLoad.Visible = true;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
