﻿using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.Corporate;
using CT.GlobalVisa;
using System.Globalization;
using CT.GlobasVisa;

public partial class CorpGVRequestGUI : CT.Core.ParentPage
{
    GlobalVisaSession visaSession = new GlobalVisaSession();
    protected int agentblockdays = 0;
    protected bool IsCorporate = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;

            if (Settings.LoginInfo == null) //Checking Logininfo
            {
                Response.Redirect("AbandonSession.aspx", false);
                return;
            }
            agentblockdays = AgentMaster.GetAgentBlockDays(Settings.LoginInfo.AgentId);
            /* Here  IsCorporate == "Y"  Means That is CorpPorate Login
               Else Retails Login 
               */
            if (!string.IsNullOrEmpty(Settings.LoginInfo.IsCorporate) && Settings.LoginInfo.IsCorporate == "Y")
            {
                IsCorporate = true;
            }
            if (!IsPostBack)
            {
                Session["GVSession"] = null; //Page load Session clearing
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }

            Utility.StartupScript(this.Page, "ShowPax();", "script");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVRequest page load exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
       
    }
    private void InitializePageControls()
    {
        try
        {            
            DateTime endDate = DateTime.Now.Date;
            endDate= endDate.AddDays(agentblockdays);
            // txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtFrom.Text = endDate.ToString("dd/MM/yyyy");
            BindTravelReasons(); 
            BindTravelingTo();
            // If Retails Login No need to show Employee Details 
            if (IsCorporate)
            {
                BindTravelEmployee();
            }
        }
        catch { throw; }
    }
    private void BindTravelReasons()
    {
        try
        {
            if (IsCorporate)  //If Corporate Only having Multiple Reasons else Only one Reason
            {
                ddlTravelReasons.DataSource = TravelUtility.GetTravelReasonList("G", Settings.LoginInfo.AgentId, ListStatus.Short);//Here Type G Means GlobalVisa
            }
            else
            {
                ddlTravelReasons.DataSource = TravelUtility.GetTravelReasonList("R", Settings.LoginInfo.AgentId, ListStatus.Short);//Here Type R Means Retail
            }
            ddlTravelReasons.DataTextField = "Description";
            ddlTravelReasons.DataValueField = "ReasonId";
            ddlTravelReasons.DataBind();
            ddlTravelReasons.Items.Insert(0, new ListItem("Select Reason", "-1"));
            if (!IsCorporate)
            {
                ddlTravelReasons.SelectedIndex = 1;
            }
        }
        catch (Exception ex) { throw ex; }
    }
    private void BindTravelingTo()
    {
        try
        {
            ddlTravelingCountry.DataSource = Country.GetCountryList();
            ddlTravelingCountry.DataTextField = "Key";
            ddlTravelingCountry.DataValueField = "Value";
            ddlTravelingCountry.DataBind();
            ddlTravelingCountry.Items.Insert(0, new ListItem("Select TravelingTo", "-1"));

        }
        catch (Exception ex) { throw ex; }
    }
    private void BindTravelEmployee()
    {
        try
        {
            ddlTravelEmployee.DataSource = TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short);
            ddlTravelEmployee.DataTextField = "ProfileName";
            ddlTravelEmployee.DataValueField = "Profile";
            ddlTravelEmployee.DataBind();
            if (ddlTravelEmployee.Items.Count > 1)
            {
                ddlTravelEmployee.Items.Insert(0, new ListItem("Select Employee", "-1"));
            }
            else
            {
                //Previously Corpoarte Profiles are combined with ProfileId - Grade 
                //Now Corpoarte Profiles are combined with ProfileId ~ Grade 
                //Reason For Change:Since For Predefined profiles the ProfileId will be -1,-2.
                //-1 ~ -1   Profile Id : -1,Grade :-1,Profile Name : Client
                //-2 ~ -2   Profile Id : -2,Grade :-2,Profile Name : Vendor
                string[] profIdArray = ddlTravelEmployee.SelectedValue.Split('~');
                if (profIdArray.Length > 0) //If only Employee Directly loading passport no and Employee Code
                {
                    int profId = Utility.ToInteger(profIdArray[0]);
                    CorporateProfile cropProfile = new CorporateProfile(profId, Settings.LoginInfo.AgentId);
                    if (cropProfile != null)
                    {
                        if (!string.IsNullOrEmpty(cropProfile.PassportNo))
                        {
                            txtPassportNo.Text = cropProfile.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(cropProfile.EmployeeId))
                        {
                            txtEmployeeCode.Text = cropProfile.EmployeeId;
                        }
                        if (visaSession != null)
                        {
                            visaSession.CorpProfileDetails = cropProfile;
                            Session["GVSession"] = visaSession; //storing profile object in session
                        }

                    }
                }
            }

        }
        catch(Exception ex) { throw ex; }
    }

    protected void ddlTravelEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool _selected = false;
            if (ddlTravelEmployee.SelectedIndex > 0)
            {
                string[] profIdArray = ddlTravelEmployee.SelectedValue.Split('~');
                if (profIdArray.Length > 0)
                {
                    int profId = Utility.ToInteger(profIdArray[0]);
                    CorporateProfile cropProfile = new CorporateProfile(profId, Settings.LoginInfo.AgentId);
                    if (cropProfile != null)
                    {
                        _selected = true;
                        if (!string.IsNullOrEmpty(cropProfile.PassportNo))
                        {
                            txtPassportNo.Text = cropProfile.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(cropProfile.EmployeeId))
                        {
                            txtEmployeeCode.Text = cropProfile.EmployeeId;
                        }
                        if (visaSession != null)
                        {
                            visaSession.CorpProfileDetails = cropProfile;
                            Session["GVSession"] = visaSession;
                        }
                    }
                }
            }
            if(!_selected)
            {
                txtPassportNo.Text = string.Empty;
                txtEmployeeCode.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            txtPassportNo.Text = string.Empty;
            txtEmployeeCode.Text = string.Empty;
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVRequest TravelEmployee SelectedIndexChanged exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["GVSession"] != null || !IsCorporate) //Checking Session
            {
                if (IsCorporate)
                {
                    visaSession = Session["GVSession"] as GlobalVisaSession;
                }
                if (ddlTravelReasons.SelectedIndex > 0)
                {
                    string[] resonId = ddlTravelReasons.SelectedValue.Split('~');
                    visaSession.TravelResionId = Utility.ToInteger(resonId[0]);
                    visaSession.TravelResionName = ddlTravelReasons.SelectedItem.Text;
                    visaSession.ActionStatus = Utility.ToString(resonId[1]);
                    //Default Assign 1 Adult
                    visaSession.Adult = 1;
                    visaSession.Child = 0;
                    visaSession.Infant = 0;
                    if (resonId.Length > 1 && Utility.ToString(resonId[1].Trim()) == "E") //If Action Status E We need to take selected Pax Details
                    {
                        visaSession.Adult = Convert.ToInt32(ddlAdults.SelectedValue);
                        visaSession.Child = Convert.ToInt32(ddlChilds.SelectedValue);
                        visaSession.Infant = Convert.ToInt32(ddlInfants.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlTravelEmployee.SelectedValue))
                {
                    string[] employeeId = ddlTravelEmployee.SelectedValue.Split('-');
                    visaSession.TravelEmployeeId = Utility.ToInteger(employeeId[0]);
                }
                if (ddlTravelingCountry.SelectedIndex > 0)
                {
                    visaSession.TravelToCountryCode = ddlTravelingCountry.SelectedValue;
                    visaSession.TravelToCountryName = ddlTravelingCountry.SelectedItem.Text;
                }
                if (ddlVisaType.SelectedIndex > 0)
                {
                    visaSession.VisaTypeId = Utility.ToInteger(ddlVisaType.SelectedValue);
                    visaSession.VisaTypeName = ddlVisaType.SelectedItem.Text;
                }
                IFormatProvider format = new CultureInfo("en-GB", true);
                visaSession.VisaDate = DateTime.Parse(txtFrom.Text, format);
                Session["GVSession"] = visaSession;
                if (IsCorporate) //if Corporate We need to Show All Corporate Details
                {
                    Response.Redirect("CorpGVViewProfile.aspx", false);
                }
                else
                {
                    Response.Redirect("CorpGVEmployeeDetail.aspx", false);
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVRequest Search Event exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void ddlTravelingTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindVisaType(ddlTravelingCountry.SelectedItem.Value);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVRequest TravelingTo event exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    void BindVisaType(string countryCode)
    {
        try
        {
            ddlVisaType.DataSource = VisaTypeMaster.GetCountryByVisaType(countryCode);
            ddlVisaType.DataTextField = "visa_type_name";
            ddlVisaType.DataValueField = "visa_type_id";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("Select VisaType", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
