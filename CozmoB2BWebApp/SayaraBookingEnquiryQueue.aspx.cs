﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.SCR;
using System.Collections.Generic;
public partial class SayaraBookingEnquiryQueueUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    IntialiseControls();
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue) Page_Load Event " + ex.ToString(), "0");
        }
    }

    private void IntialiseControls()
    {
        try
        {
            dcApprovalFromDate.Value = DateTime.Now.AddMonths(-1);
            dcApprovalToDate.Value = DateTime.Now;

            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
           DateTime startDate = Convert.ToDateTime(dcApprovalFromDate.Value, provider);
            DateTime endDate = Convert.ToDateTime(Convert.ToDateTime(dcApprovalToDate.Value, provider).ToString("dd/MM/yyyy 23:59"), provider);


            bindSearch(startDate, endDate);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindSearch(DateTime fromDate, DateTime toDate)
    {
        try
        {
            string bookingtype = null;
            string vehicletype = null;
            if (ddlBookngType.SelectedValue != "-1")
            {
                bookingtype = ddlBookngType.SelectedValue;
            }
            if (ddlVehicleType.SelectedValue != "-1")
            {
                vehicletype = ddlVehicleType.SelectedValue;
            }
            DataTable dt = BookingEnquiry.GetBookingEnquiryRecords(fromDate, toDate,bookingtype,vehicletype);
            CommonGrid g = new CommonGrid();
            g.BindGrid(gridViewSCR, dt);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #region GridEvents
    protected void gridViewSCR_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gridViewSCR.EditIndex = e.NewEditIndex;
            bindSearch(dcApprovalFromDate.Value, dcApprovalToDate.Value);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue)gridViewSCR_RowEditing Event Error Raised.Reason " + ex.ToString(), "0");
        }
    }
    protected void gridViewSCR_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int enqId = Convert.ToInt32(gridViewSCR.DataKeys[e.RowIndex].Values["EnquiryId"]);
            TextBox txtVMSRef = (TextBox)gridViewSCR.Rows[e.RowIndex].FindControl("editVMSRefNum");
            DropDownList ddlDriver = (DropDownList)gridViewSCR.Rows[e.RowIndex].FindControl("ddlDriver");
            DropDownList ddlVehType = (DropDownList)gridViewSCR.Rows[e.RowIndex].FindControl("ddlVehType");
            DropDownList ddlBookStatus = (DropDownList)gridViewSCR.Rows[e.RowIndex].FindControl("ddlBookingStatus");
            if (enqId > 0)
            {
                BookingEnquiry.UpdatBookingEnqStatus(enqId, txtVMSRef.Text, Convert.ToInt32(ddlDriver.SelectedItem.Value), Convert.ToInt32(ddlVehType.SelectedItem.Value), ddlBookStatus.SelectedItem.Value, Convert.ToInt32(Settings.LoginInfo.UserID), Convert.ToString(ddlVehType.SelectedItem.Text.Split('~')[0]));
                try
                {
                    SendEmail(enqId, ddlBookStatus.SelectedItem.Text);
                }
                catch { }
            }
            gridViewSCR.EditIndex = -1;
            bindSearch(dcApprovalFromDate.Value, dcApprovalToDate.Value);

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue)gridViewSCR_RowUpdating Event Error Raised.Reason " + ex.ToString(), "0");
        }
    }
    protected void gridViewSCR_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gridViewSCR.EditIndex = -1;
            bindSearch(dcApprovalFromDate.Value, dcApprovalToDate.Value);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue)gridViewSCR_RowCancelingEdit Event Error Raised.Reason " + ex.ToString(), "0");
        }
    }


    protected void gridViewSCR_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlDriver = (DropDownList)e.Row.FindControl("ddlBookingStatus");
                ddlDriver.Items.Add(new ListItem("Pending", "P"));
                ddlDriver.Items.Add(new ListItem("Driver and vehicle allocated ", "A"));
                ddlDriver.Items.Add(new ListItem("Completed", "C"));
                ddlDriver.Items.Add(new ListItem("Cancelled by customer", "X"));
                ddlDriver.Items.Add(new ListItem("No Show", "N"));
                ddlDriver.Items.Insert(0, new ListItem("Select", "0"));
            }


            if (e.Row.RowType == DataControlRowType.DataRow && gridViewSCR.EditIndex == e.Row.RowIndex)
            {
                DropDownList ddlDriver = (DropDownList)e.Row.FindControl("ddlDriver");
                ddlDriver.DataSource = GetDriverData();
                ddlDriver.DataTextField = "emp_Name";
                ddlDriver.DataValueField = "emp_Id";
                ddlDriver.DataBind();
                ddlDriver.Items.Insert(0, new ListItem("Select", "0"));
                ddlDriver.Items.FindByValue((e.Row.FindControl("lblDriverAlloc1") as Label).Text).Selected = true;

                DropDownList ddlVehType = (DropDownList)e.Row.FindControl("ddlVehType");
                ddlVehType.DataSource = GetVehicleTypeData();
                ddlVehType.DataTextField = "ve_name";
                ddlVehType.DataValueField = "ve_id";
                ddlVehType.DataBind();
                ddlVehType.Items.Insert(0, new ListItem("Select", "0"));
                ddlVehType.Items.FindByValue((e.Row.FindControl("lblVehType1") as Label).Text).Selected = true;

                DropDownList ddlBookingStatus = (DropDownList)e.Row.FindControl("ddlBookingStatus");
                string bookingStatus = ((Label)e.Row.FindControl("lblBokStatus")).Text;
                ddlBookingStatus.SelectedValue = bookingStatus;

                if (bookingStatus == "P")
                {
                    ddlDriver.Enabled = true;
                    ddlVehType.Enabled = true;
                }
                else
                {
                    ddlDriver.Enabled = false;
                    ddlVehType.Enabled = false;
                }

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue)gridViewSCR_RowDataBound Event Error Raised.Reason " + ex.ToString(), "0");
        }
    }

    private DataTable GetDriverData()
    {
        DataTable dtDrivers = new DataTable();
        try
        {
            dtDrivers = BookingEnquiry.GetDriverDetails(1, "A", "Driver");
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dtDrivers;
    }
    private DataTable GetVehicleTypeData()
    {
        DataTable dtVehType = new DataTable();
        try
        {
            dtVehType = BookingEnquiry.GetVehicleType(1, "A");
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dtVehType;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            bindSearch(Convert.ToDateTime(dcApprovalFromDate.Value, dateFormat), Convert.ToDateTime(dcApprovalToDate.Value, dateFormat));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue)btnSearch_Click Event Error Raised.Reason " + ex.ToString(), "0");
        }
    }
    protected void gridViewSCR_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gridViewSCR.PageIndex = e.NewPageIndex;
            bindSearch(dcApprovalFromDate.Value, dcApprovalToDate.Value);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(SayaraBookingEnquiryQueue)gridViewSCR_PageIndexChanging Event Error Raised.Reason " + ex.ToString(), "0");
        }
    }
    #endregion

    protected void SendEmail(int enqId,string bookingStatus)
    {
        BookingEnquiry enq = new BookingEnquiry(enqId); 
        //string myPageHTML = string.Empty;
        try
        {
            List<string> toArray = new System.Collections.Generic.List<string>();
            if (!string.IsNullOrEmpty(enq.Email))
            {
                toArray.Add(enq.Email);
            }
            string message = "<p>Dear  " + enq.Name + ",</p>";
            message += "<p>We received your payment.</p></br>";
            message += "<p>Booking Status:" + bookingStatus + "</p></br>";
            message += "<p><b>Below are the details submitted by you :</b></p><br/>";
            message += "<p><b>Passenger Name :</b>" + enq.Name + "</br></p>";
            message += "<p><b>Email :</b>" + enq.Email + "</br></p>";
            message += "<p><b>Mobile :</b>" + enq.Mobile + "</br></p>";
            message += "<p><b>Total Number of Passengers :</b>" + enq.PaxCount + "</br></p>";
            message += "<p><b>Pick Up Location :</b>" + GetCityName(enq.PickupLocationId) + "</br></p>";
            message += "<p><b>Drop Off Location :</b>" + GetCityName(enq.DropOffLocationID) + "</br></p>";
            message += "<p><b>Coming From :</b>" + enq.ComingFrom + "</br></p>";
            message += "<p><b>Flight Number :</b>" + enq.FlightNo + "</br></p>";
            message += "<p><b>Pick Up Date Time :</b>" + enq.PickUpDateTime.ToString("dd-MMM-yyyy HH:mm") + "</br></p>";
            message += "<p><b>Arrival Date Time: :</b>" + enq.ArrivalDateTime.ToString("dd-MMM-yyyy HH:mm") + "</br></p>";
            message += "<p><b>Suit Case Count :</b>" + enq.SuitcaseCount + "</br></p>";
            message += "<p><b>Total Amount :</b>" + enq.Currency + " " + enq.TotalAmount + "</br></p>";
            message += "<p><b>Voucher Number: :</b>" + enq.VoucherNo + "</br></p>";
            message += "<p>Regards,</br></p><p><b>Sayara Car Rental </b></p>";
           
            string subject = "Sayara Booking Status Change Notification";
            if (toArray != null && toArray.Count > 0)
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, message, new Hashtable(), ConfigurationManager.AppSettings["SCRBookingEnquiryEmail"]);
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateCreateExpenseReport.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        //return myPageHTML;
    }
    public string GetCityName(int cityId)
    {
        string cityName = string.Empty;
        try
        {
            cityName = BookingEnquiry.GetCityName(cityId);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Book, Severity.High, 0, "Exception in BookingEnquiryQueue Page. Message: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return cityName;
    }


    protected void ddlBookngType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if(ddlBookngType.SelectedValue!= "-1")
            {
                ddlVehicleType.Enabled = true;
            }
            else
            {
                ddlVehicleType.SelectedValue = "-1";
                ddlVehicleType.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Book, Severity.High, 0, "(SayaraBookingEnquiryQueue)ddlBookngType_SelectedIndexChanged Event Error Raised.Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
