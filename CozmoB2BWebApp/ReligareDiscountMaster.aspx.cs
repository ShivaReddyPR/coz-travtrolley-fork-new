﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareDiscountMaster : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                

            }
        }
        #region Methods
        void bindSearch()
        {

            try
            {

                DataTable dt = ReligareDiscount.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
                
            }

            catch(Exception ex)
            {
                throw ex;
            }

        }
        
         void Save()
        {
            try
            {
                ReligareDiscount Rdiscount=new ReligareDiscount();
                
                if (Utility.ToInteger(hdfEMId.Value) > 0)
                {
                    Rdiscount.ID = Utility.ToLong(hdfEMId.Value);
                }
                else
                {
                    Rdiscount.ID = -1;
                }
                
                Rdiscount.DISCOUNT_MEMBER_ID =Utility.ToInteger(txtDiscountNumberId.Text);
                Rdiscount.DISCOUNT_PERCENTAGE =Utility.ToDecimal( txtDiscountPercentage.Text);
                Rdiscount.DISCOUNT_REMARKS = txtDiscountRemarks.Text;
                Rdiscount.CREATED_BY = Settings.LoginInfo.UserID;
                Rdiscount.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfEMId.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
         void Clear()
        {
            txtDiscountNumberId.Text = string.Empty;
            txtDiscountPercentage.Text = string.Empty;
            txtDiscountRemarks.Text = string.Empty;
        }
       
         void Edit(long id)
        {
            try
            {
                ReligareDiscount Rdiscount = new ReligareDiscount(id);
                hdfEMId.Value = Utility.ToString(Rdiscount.ID);
                txtDiscountNumberId.Text =Convert.ToString(Rdiscount.DISCOUNT_MEMBER_ID);
                txtDiscountPercentage.Text =Convert.ToString (Rdiscount.DISCOUNT_PERCENTAGE);
                txtDiscountRemarks.Text = Rdiscount.DISCOUNT_REMARKS;
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {

                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareDiscountMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareDiscountMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareDiscountMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion
        #region gridview events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long discountid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(discountid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareDiscountMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();
                
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareDiscountMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
 
        #endregion

    }
}
