using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;

enum RepriceResult
{
    Success = 1,
    PricingChange = 2,
    OtherError = 3
}

public partial class AutoTicket :CT.Core.ParentPage// System.Web.UI.Page
{
    /// <summary>
    /// Member who is logged in.
    /// </summary>
    private UserMaster loggedMember = new UserMaster();
    /// <summary>
    /// Booking corresponding to the PNR
    /// </summary>
    protected BookingDetail booking;
    /// <summary>
    /// errorMessage generated if any step fails.
    /// </summary>
    protected string errorMessage = string.Empty;
    /// <summary>
    /// IP Address from where the page is being accessed. For Audit.
    /// </summary>
    protected string ipAddr = string.Empty;
    /// <summary>
    /// Default value true for agent log in.
    /// Set to false if Admin is logged in.
    /// </summary>
    protected bool isAgent = true;
    /// <summary>
    /// Contains the name of booking queue page as it is different for agent and admin.
    /// </summary>
    protected string queuePage = string.Empty;
    protected TicketingResponse response;
    Dictionary<string, string> ticketData = new Dictionary<string, string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();   // Checks for login session.
        ErrorView.ActiveViewIndex = 0;
        response = new TicketingResponse();
        MetaSearchEngine mse = new MetaSearchEngine();
        FlightItinerary itinerary = new FlightItinerary();
        AgentMaster agency = new AgentMaster();
        try
        {
            if (Request.QueryString["flightId"] != null) // We can not proceed without pnr.
            {
                itinerary = new FlightItinerary(Convert.ToInt32(Request.QueryString["flightId"]));
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(itinerary.FlightId));
                string corporateCode = string.Empty;
                if (Request.QueryString["corporateCode"] != null)
                {
                    ticketData.Add("corporateCode", Request.QueryString["corporateCode"].ToString());
                }
                if (Request.QueryString["tourCode"] != null)
                {
                    ticketData.Add("tourCode", Request.QueryString["tourCode"].ToString());
                }
                if (Request.QueryString["endorsement"] != null)
                {
                    ticketData.Add("endorsement", Request.QueryString["endorsement"].ToString());
                }
                if (Request.QueryString["remarks"] != null)
                {
                    ticketData.Add("remarks", Request.QueryString["remarks"].ToString());
                }
                loggedMember = new UserMaster(Settings.LoginInfo.UserID);    // picking memberId of the logged in user from Session.
                ipAddr = Request.ServerVariables["REMOTE_ADDR"];
                response = mse.Ticket(itinerary.PNR, loggedMember, ticketData, ipAddr);
            }else
            {
                response.Status = TicketingResponseStatus.OtherError;
                ErrorView.ActiveViewIndex = 1;
            }


            if (response.Status == TicketingResponseStatus.Successful || response.Status == TicketingResponseStatus.TicketedDB)
            {
                BookingUtility.AgencyLccBal.Remove(booking.AgencyId);
                //Dictionary<int, decimal> agencyLccBal = (Dictionary<int, decimal>)Technology.MemCache.Cache.Get("AgencyLccBal");
                //if (agencyLccBal != null)
                //{
                //    agencyLccBal.Remove(booking.AgencyId);
                //    Technology.MemCache.Cache.Replace("AgencyLccBal", (object)agencyLccBal);
                //}
                //decimal amountToCheck = 0;
                //List<BookingDetail> insuranceBooking = BookingDetail.GetChildBookings(booking.BookingId);
                //agency.Load(Convert.ToInt32(booking.AgencyId));
                //for (int i = 0; i < insuranceBooking.Count; i++)
                //{
                //    Product[] product = insuranceBooking[i].ProductsList;
                //    for (int j = 0; j < product.Length; j++)
                //    {
                //        if (product[j].ProductType == ProductType.Insurance)
                //        {
                //            Insurance insurance = new Insurance();
                //            insurance.Load(product[j].ProductId);
                //            mse.CreatePolicy(itinerary, insurance, agency, loggedMember);
                //        }
                //    }
                //}
  /* SAI              decimal ticketAmount = 0;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    if (itinerary.Passenger[0].Price.NetFare > 0)
                    {
                        ticketAmount = pax.Price.NetFare + pax.Price.Tax + pax.Price.Markup;
                    }
                    else
                    {
                        ticketAmount = pax.Price.PublishedFare + pax.Price.Tax;
                    }
                }

                //Updating Agent Balance
                Settings.LoginInfo.AgentBalance -= ticketAmount;
                AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                agent.CreatedBy = Settings.LoginInfo.UserID;
                agent.UpdateBalance(-ticketAmount);         */

                decimal currentAgentBalance = 0;
                AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                agent.CreatedBy = Settings.LoginInfo.UserID;
                currentAgentBalance = agent.UpdateBalance(0);

                Settings.LoginInfo.AgentBalance = currentAgentBalance;
                Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance); 


                Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + booking.BookingId, false);
            }
            else
            {
                if (response.Status == TicketingResponseStatus.InProgress)
                {
                    errorMessage = "Ticketing is already in progress.";
                }
                else if (response.Status == TicketingResponseStatus.NotAllowed)
                {
                    errorMessage = "<span class=\"bold border-bottom\">Auto ticketing is not allowed.</span><br /><br /> ";
                    errorMessage += response.Message + "<br />";
                }
                else if (response.Status == TicketingResponseStatus.NotCreated || response.Status == TicketingResponseStatus.OtherError)
                {
                    errorMessage = "Ticket could not be generated.";
                }
                else if (response.Status == TicketingResponseStatus.NotSaved)
                {
                    errorMessage = "Ticket has been generated but could not be saved in our system.";
                }
                else if (response.Status == TicketingResponseStatus.TicketedWS)
                {
                    errorMessage = "Ticket is already generaged.";
                }
                else if (response.Status == TicketingResponseStatus.PriceChanged)
                {
                    errorMessage = "Pricing has changed for this booking.";
                }
                else if (response.Status == TicketingResponseStatus.NotSaved)
                {
                    errorMessage = "Ticket generated but could not be saved in our system.";
                }
                else
                {
                    errorMessage = "Ticket could not be generated.";
                }
            }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High,(int)Settings.LoginInfo.UserID,ex.Message,"");
        }
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {            
            Response.Redirect("AbandonSession.aspx", true);
        }
        else
        {
            isAgent = (Settings.LoginInfo.AgentId > 0);
            if (isAgent)
            {
                queuePage = "AgentBookingQueue.aspx";
            }
            else
            {
                queuePage = "BookingQueue.aspx";
            }
        }
    }
}
