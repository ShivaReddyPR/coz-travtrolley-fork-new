﻿<%@ Page Title="SectorList Master" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="SectorMaster.aspx.cs" Inherits="CozmoB2BWebApp.SectorMaster" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <style>
        .hide {
            display: none;
        }

        .show {
            display: block;
        }

        .HeaderFreez {
            position: relative;
            top: expression(this.offsetParent.scrollTop);
            z-index: 10;
        }
    </style>
    <asp:HiddenField ID="hdnNewSectors" runat="server" Value="" /> 
    <asp:HiddenField ID="hdnNewOrigin" runat="server" Value="" />
    <div class="body_container">
        <div class="paramcon" title="header">
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblSupplier" Text="Supplier:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlSuppliers" runat="server" Enabled="true"
                        AutoPostBack="false" CssClass="inpuTddlEnabled form-control" >
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblOrigin" Text="Origin:" runat="server"></asp:Label>
                </div>
                <div class="col-md-1">
                    <asp:TextBox ID="txtOrigin" runat="server" Enabled="true" onkeypress="return IsAlpha(event);" onblur="CheckNewOrigin();" MaxLength="3" style="text-transform:uppercase;" CssClass="inputEnabled form-control"></asp:TextBox>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="divSectors" class="paramcon" title="header" runat="server">
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-1">
                    <label id="lblSectors">Sectors : </label>
                </div>
                <div id="TextBoxContainer" class="col-md-12">
                    <!--Dynamic textboxes will be added here -->
                </div>
            </div>
        </div>
        <input id="btnAdd" class="btn but_b" type="button" value="Add Destination" onclick="AddTextBox()" runat="server" />
        <div class="col-md-12 padding-0 marbot_10">
            <label class=" f_R mar-5">
                <asp:Button ID="btnImport" Text="Import From Sector Config" runat="server"
                    CssClass="btn but_b" OnClientClick="" OnClick="btnImport_Click" ></asp:Button>
            <%--    <asp:Button ID="Button1" Text="Import From Sector Config" runat="server"
                    CssClass="btn but_b" OnClientClick="window.open('btnImport_Click')" OnClick="btnImport_Click" ></asp:Button>--%>
            </label>
            <label class=" f_R mar-5">
                <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();"
                    CssClass="btn but_b" OnClick="btnSave_Click"></asp:Button></label>
            <label class=" f_R mar-5">
                <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b" OnClick="btnClear_Click"></asp:Button></label>
            <label class=" f_R mar-5">
                <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b" OnClick="btnSearch_Click"></asp:Button></label>

        </div>
    </div>
    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
    <div class="clearfix"></div>

    <asp:Panel ID="pnlImport" runat="server" Visible="false">
        <div id="popup" style="max-height: 720px; overflow-y: scroll;">
            <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
            <asp:GridView ID="gvImport" Width="100%" runat="server" AllowPaging="false" DataKeyNames="SMID"
                EmptyDataText="No Origins Found!" AutoGenerateColumns="false" GridLines="Both" CssClass="grdTable"
                CellPadding="4" CellSpacing="0" HeaderStyle-CssClass="HeaderFreez"
                OnRowEditing="gvImport_RowEditing" OnRowUpdating="gvImport_RowUpdating" OnRowCancelingEdit="gvImport_RowCancelingEdit">
                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Supplier">
                        <HeaderStyle HorizontalAlign="left" Font-Size="Medium" Font-Bold="true" />
                        <HeaderTemplate>
                            <cc1:Filter ID="GVtxtSupplier" Width="100px" CssClass="inputEnabled" HeaderText="Supplier" OnClick="FilterSupplier_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSupplier" runat="server" Text='<%# Eval("Supplier") %>' ToolTip='<%# Eval("Supplier") %>' Width="150px">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Origin">
                        <HeaderStyle HorizontalAlign="left" Font-Size="Medium" Font-Bold="true" />

                        <ItemTemplate>
                            <asp:Label ID="lblOrigin" runat="server" Text='<%# Eval("Origin") %>' ToolTip='<%# Eval("Origin") %>' Width="150px" Height="20px">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sectors">
                        <HeaderStyle HorizontalAlign="left" Font-Size="Medium" Font-Bold="true" />

                        <ItemTemplate>
                            <asp:Label ID="lblSectors" runat="server" Text='<%# Eval("Sectors") %>' ToolTip='<%# Eval("Sectors") %>' Width="850px">
                            </asp:Label>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSectors" runat="server" Text='<%# Eval("Sectors") %>' ToolTip='<%# Eval("Sectors") %>' Width="850px">
                            </asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update" />
                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel" />
                        </EditItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </div>
        <asp:Button ID="btnUpdate" Text="Save" runat="server" CssClass="btn but_b" Visible="false" OnClick="btnUpdate_Click"></asp:Button>
    </asp:Panel>
    <script type="text/javascript">

        function Save() {
            if (getElement('ddlSuppliers').selectedIndex <= 0) addMessage('Please select Supplier from the list!', '');
            if (getElement('txtOrigin').value == '') addMessage('Origin cannot be blank!', '');
            if (getElement('txtOrigin').length > 0 && getElement('txtOrigin').length < 3) addMessage('Please enter atleast 3 characters for origin!', '');
            textboxes = $('#TextBoxContainer').find("[name='txtSectorDynamic']");
            if (textboxes.length == 0) addMessage('Please add atleast one sector!', '');
            textboxes.each(function () {
                if (this.value.length == 0) {
                    addMessage('Field should not be empty!', '');
                    return false;;
                }
                if (this.value.length > 0 && this.value.length < 3) {
                    addMessage('Please enter atleast 3 characters for sectors!', '');
                    return false;;
                }
            });
            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }
        }

        function IsAlpha(e) {
                    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                    var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
                  
                    return ret;
        }

    </script>
    <script type="text/javascript">
        function GetDynamicTextBox(value) {
            return '<input name = "txtSectorDynamic" class="form-control col-md-1" type="text" maxlength="3" value = "' + value + '"  onkeypress="return IsAlpha(event);" onchange="CheckSector(this);" style="text-transform:uppercase;"/>' +
                '<input type="button" class="form-control col-md-1"  style="width:2%;background-color: aliceblue;" name = "btnRemoveSector" value="x" onclick = "RemoveTextBox(this)" />';

        }
        function AddTextBox() {
            var div = document.createElement('DIV');
            div.innerHTML = GetDynamicTextBox("");
            document.getElementById("TextBoxContainer").appendChild(div);
        }

        function RemoveTextBox(div) {
            var newSectors = ''; //if we close the sector textbox then that sector need to remove from hdnNewSectors
            if (div.parentNode.childNodes[0].value.length==3 && document.getElementById('<%=hdnNewSectors.ClientID%>').value.includes(div.parentNode.childNodes[0].value.toUpperCase()))
            {
                var sectors = document.getElementById('<%=hdnNewSectors.ClientID%>').value.split(',');
                for (var i = 0; i < sectors.length; i++) {
                    if (sectors[i] != div.parentNode.childNodes[0].value.toUpperCase()) {
                        if (newSectors == '')
                            newSectors = sectors[i];
                        else
                            newSectors += "," + sectors[i];
                    }
                }
                document.getElementById('<%=hdnNewSectors.ClientID%>').value = newSectors;
            }
            document.getElementById("TextBoxContainer").removeChild(div.parentNode);
        }

        function RecreateDynamicTextboxes(values) {
            if (values != null) {
                var html = "";
                for (var i = 0; i < values.length; i++) {
                    html += '<div>' + GetDynamicTextBox(values[i]) + "</div>";
                }
                document.getElementById("TextBoxContainer").innerHTML = html;
            }
        }

        function CheckSector(ctrl) {
            if (ctrl.value.length == 3) {  //If we try to add any same sector to existing sector list then displaying Alert sector alreay exist
                if ((document.getElementById("TextBoxContainer").innerHTML.indexOf(ctrl.value.toUpperCase()) != -1  || document.getElementById('<%=hdnNewSectors.ClientID%>').value.includes(ctrl.value.toUpperCase()))&& (document.getElementById('<%=hdnNewOrigin.ClientID%>').value == "NO" || document.getElementById('<%=hdnNewOrigin.ClientID%>').value == "")) {
                    alert(ctrl.value.toUpperCase() + " Sector already exist");
                    ctrl.value = "";
                    ctrl.focus();
                }
                else if (document.getElementById('<%=hdnNewOrigin.ClientID%>').value == "YES") { //If we try to add new sectors with new origin then checking sector is repeted or not.
                    if (document.getElementById('<%=hdnNewSectors.ClientID%>').value == '')
                        document.getElementById('<%=hdnNewSectors.ClientID%>').value = ctrl.value.toUpperCase();
                    else if (document.getElementById('<%=hdnNewSectors.ClientID%>').value != '' && !document.getElementById('<%=hdnNewSectors.ClientID%>').value.includes(ctrl.value.toUpperCase())) {
                        document.getElementById('<%=hdnNewSectors.ClientID%>').value += "," + ctrl.value.toUpperCase();
                    }
                    else if (document.getElementById('<%=hdnNewSectors.ClientID%>').value != '' && document.getElementById('<%=hdnNewSectors.ClientID%>').value.includes(ctrl.value.toUpperCase())) {
                        alert(ctrl.value.toUpperCase() + " Sector already exist");
                        ctrl.value = "";
                        ctrl.focus();
                    }
                }
                UpdateSetors();
            }

        }

        function UpdateSetors() {
            document.getElementById('<%=hdnNewSectors.ClientID%>').value = '';
            for (var i = 0; i < document.getElementById("TextBoxContainer").children.length; i++) {
                if (document.getElementById("TextBoxContainer").children[i] != null && document.getElementById("TextBoxContainer").children[i].childNodes[0].value.toUpperCase() != '')
                    if (document.getElementById('<%=hdnNewSectors.ClientID%>').value == '')
                        document.getElementById('<%=hdnNewSectors.ClientID%>').value = document.getElementById("TextBoxContainer").children[i].childNodes[0].value.toUpperCase();
                    else
                        document.getElementById('<%=hdnNewSectors.ClientID%>').value += "," + document.getElementById("TextBoxContainer").children[i].childNodes[0].value.toUpperCase();

            }
        }
        function CheckNewOrigin() { //Checking the sectors for new origin from db
            var supplier = document.getElementById('<%=ddlSuppliers.ClientID%>').value;
            var origin = document.getElementById('<%=txtOrigin.ClientID%>').value.toUpperCase();
            if (origin.length == 3) {
                $.ajax({
                    url: "SectorMaster.aspx/CheckNewOrigin",
                    type: 'POST',
                    dataType: 'json',
                    data: "{'supplier':'" + supplier + "','origin':'" + origin + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        document.getElementById('<%=hdnNewOrigin.ClientID%>').value = data.d;
                    },
                    error: function (data) {

                    }
                });
            }
        }
        
    </script>
    <script type="text/javascript">
        function ConfirmOnDelete(item) {
            if (confirm("Are you sure to delete: " + item + "?") == true)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="SMID"
        EmptyDataText="No Origins Found!" AutoGenerateColumns="false" PageSize="15" GridLines="none" CssClass="grdTable"
        OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
        OnPageIndexChanging="gvSearch_PageIndexChanging" OnRowDataBound="gvSearch_RowDataBound"  OnRowDeleting="gvSearch_RowDeleting">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkDelete" CommandName="delete" CommandArgument='<%# Eval("SMID") %>'
                        runat="server" OnClientClick="return ConfirmOnDelete('');">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="GVtxtOrigin" Width="150px" CssClass="inputEnabled" HeaderText="Origin" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="GVlblOrigin" runat="server" Text='<%# Eval("Origin") %>' CssClass="label grdof" ToolTip='<%# Eval("Origin") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="GVtxtSupplier" Width="100px" CssClass="inputEnabled" HeaderText="Supplier" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="GVlblSupplier" runat="server" Text='<%# Eval("Supplier") %>' CssClass="label grdof" ToolTip='<%# Eval("Supplier") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="GVtxtCreatedBy" Width="150px" CssClass="inputEnabled" HeaderText="Created By" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="GVlblStatus" runat="server" Text='<%# Eval("CreatedBy") %>' CssClass="label grdof" ToolTip='<%# Eval("CreatedBy") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

