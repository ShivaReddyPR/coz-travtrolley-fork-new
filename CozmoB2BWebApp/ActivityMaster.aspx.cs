﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.ActivityDeals;
using System.IO;
using CT.TicketReceipt.Common;

public partial class ActivityMasterGUI : CT.Core.ParentPage
{
    private string AFC_DETAILS_SESSION = "_ActivityDeatilList";
    private string PRICE_DETAILS_SESSION = "_PriceDetails";
    private string ACF_SESSION = "ACFdetails";

    protected Dictionary<string, string> activityVariables = new Dictionary<string, string>();
    protected Dictionary<string, string[]> activityVariablesArray = new Dictionary<string, string[]>();
    protected string dateBlockStyle = String.Empty;
    protected string[] splitter = { "#&#" };
    protected string pickupLocationDetails = string.Empty;
    //protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                this.Master.PageRole = true;
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.SaveButton);
                long activityId = 0;
                lblError.Visible = false;
                if (Convert.ToBoolean(Request["savePage"]))
                {
                    if (Session["PageSaved"] != null && Convert.ToBoolean(Session["PageSaved"]))
                    {
                        Session["PageSaved"] = null;
                       // Session["PageSaved"] = false;
                       // clear();
                        Response.Redirect("ActivityMaster.aspx", false);
                      

                    }
                }
                else
                {
                    string[] emptyStringArr = new string[1];
                    emptyStringArr[0] = string.Empty;
                    if (!IsPostBack)
                    {
                        activityId = Convert.ToInt32(Request.QueryString["activity"]);
                        //activityId = 28;
                        if (activityId > 0)
                            LoadActivity(activityId);

                        txtDuration.Attributes.Add("readonly", "true");
                        if (activityId <= 0)
                        {
                            InitializePageControls();

                            int inclusionsCount = 0;
                            if (inclusionsCount < 2)
                            {
                                inclusionsCount = 2;
                                activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                            }
                            int exclusionsCount = 0;
                            if (exclusionsCount < 2)
                            {
                                exclusionsCount = 2;
                                activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                            }

                            int CancellPolicyCount = 0;
                            if (CancellPolicyCount < 2)
                            {
                                CancellPolicyCount = 2;
                                activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                            }

                            int ThingsToBringCount = 0;
                            if (ThingsToBringCount < 2)
                            {
                                ThingsToBringCount = 2;
                                activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                            }

                            int UnAvailDatesCount = 0;
                            if (UnAvailDatesCount < 2)
                            {
                                UnAvailDatesCount = 2;
                                activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                            }
                        }
                        else
                        {
                            //Inclusion
                            int inclusionsCount = Convert.ToInt16(hdfinclCounter.Value);
                            emptyStringArr[0] = string.Empty;
                            string inclusions = hdfinclusion.Value;
                            if (inclusionsCount <= 2 && inclusions == "#&#")
                            {
                                inclusionsCount = 2;
                                activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                            }
                            else
                            {
                                activityVariablesArray.Add("inclusionsValue", inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                            }

                            //Exclusion

                            int exclusionsCount = Convert.ToInt16(hdfExclCounter.Value);
                            emptyStringArr[0] = string.Empty;
                            string exclusions = hdfExclusions.Value;
                            if (exclusionsCount <= 2 && exclusions == "#&#")
                            {
                                exclusionsCount = 2;
                                activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                            }
                            else
                            {
                                activityVariablesArray.Add("exclusionsValue", exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                            }


                            //UnavailDate
                            int UnAvailDatesCount = Convert.ToInt16(hdfUnavailDateCounter.Value);
                            emptyStringArr[0] = string.Empty;
                            string unavailDates = hdfUnavailDates.Value;
                            if (UnAvailDatesCount <= 2 && unavailDates == "#&#")
                            {
                                UnAvailDatesCount = 2;
                                activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                            }
                            else
                            {
                                activityVariablesArray.Add("UnAvailDatesValue", unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                            }


                            //CancelPolicy
                            int CancelPolicyCount = Convert.ToInt16(hdfCanPolCounter.Value);
                            emptyStringArr[0] = string.Empty;
                            string cancelPolicy = hdfCanPolicy.Value;
                            if (CancelPolicyCount <= 2 && cancelPolicy == "#&#")
                            {
                                CancelPolicyCount = 2;
                                activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                            }
                            else
                            {
                                activityVariablesArray.Add("CancellPolicyValue", cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                            }



                            //Things to Bring
                            int thingsCount = Convert.ToInt16(hdfThingsCounter.Value);
                            emptyStringArr[0] = string.Empty;
                            string things = hdfThings.Value;
                            if (thingsCount <= 2 && things == "#&#")
                            {
                                thingsCount = 2;
                                activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                            }
                            else
                            {
                                activityVariablesArray.Add("ThingsToBringValue", things.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                            }
                        }

                    }
                    else
                    {
                        activityId = Convert.ToInt32(Request.QueryString["activity"]);
                        BindPostBackControls();
                    }
                    hdfinclusion.Value = "";
                    hdfExclusions.Value = "";
                    hdfCanPolicy.Value = "";
                    hdfThings.Value = "";
                    hdfUnavailDates.Value = "";
                    activityVariables.Add("exclusionsCount", hdfExclCounter.Value);
                    activityVariables.Add("inclusionsCount", hdfinclCounter.Value);
                    activityVariables.Add("CancellPolicyCount", hdfCanPolCounter.Value);
                    activityVariables.Add("ThingsToBringCount", hdfThingsCounter.Value);
                    activityVariables.Add("UnAvailDatesCount", hdfUnavailDateCounter.Value);
                    if (string.IsNullOrEmpty(lblCurrency.Text))
                    {
                        lblCurrency.Text = Settings.LoginInfo.Currency;
                    }
                    if (!string.IsNullOrEmpty(pickupLocationDetails) || ddlTransferInclude.SelectedValue.ToLower() == "yes")
                    {
                        Utility.StartupScript(this.Page, "LoadPickupDetails('" + pickupLocationDetails + "')", "bindData");
                        if (txtPickLocation.Enabled == false)
                        {
                            txtPickLocation.Enabled = true;
                            txtPickUpHr.Enabled = true;
                            txtPickUpMin.Enabled = true;
                        }
                    }
                    
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    private void BindPostBackControls()
    {
        try
        {
            string country = "";
            string city = "";

            country = hdfSelCountry.Value;
            city = hdfSelCity.Value;
            if (!string.IsNullOrEmpty(country))
            {
                if (!string.IsNullOrEmpty(country))
                    CountryText1.SelectedValue = country;

                GetCity(country);
                if (!string.IsNullOrEmpty(city))
                    CityText1.SelectedValue = city;
            }

            string[] emptyStringArr = new string[1];
            emptyStringArr[0] = string.Empty;
            int inclusionsCount = Convert.ToInt16(hdfinclCounter.Value);
            emptyStringArr[0] = string.Empty;

            string inclusions = string.Empty;
            for (int i = 1; i < inclusionsCount; i++)
            {
                inclusions += Request["InclusionsText-" + i];
                inclusions += "#&#";
            }
            if (inclusions == "#&#")
            {
                activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                hdfinclCounter.Value = "2";
            }
            else
            {
                if (inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                    hdfinclCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("inclusionsValue", inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfinclCounter.Value = Convert.ToString(inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }

            //Exclusion
            int exclusionsCount = Convert.ToInt16(hdfExclCounter.Value);
            emptyStringArr[0] = string.Empty;
            string exclusions = string.Empty;
            for (int i = 1; i < exclusionsCount; i++)
            {
                exclusions += Request["ExclusionsText-" + i];
                exclusions += "#&#";
            }
            if (exclusions == "#&#")
            {
                activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                hdfExclCounter.Value = "2";
            }
            else
            {
                if (exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                    hdfExclCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("exclusionsValue", exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfExclCounter.Value = Convert.ToString(exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }

            //UnavailDate
            int UnAvailDatesCount = Convert.ToInt16(hdfUnavailDateCounter.Value);
            emptyStringArr[0] = string.Empty;
            string unavailDates = string.Empty;
            for (int i = 1; i < UnAvailDatesCount; i++)
            {
                unavailDates += Request["txtUnAvailDatesText-" + i];
                unavailDates += "#&#";
            }
            if (unavailDates == "#&#")
            {
                activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                hdfUnavailDateCounter.Value = "2";
            }
            else
            {
                if (unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                    hdfUnavailDateCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("UnAvailDatesValue", unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfUnavailDateCounter.Value = Convert.ToString(unavailDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }


            //CancelPolicy
            int CancelPolicyCount = Convert.ToInt16(hdfCanPolCounter.Value);
            emptyStringArr[0] = string.Empty;
            string cancelPolicy = string.Empty;
            for (int i = 1; i < CancelPolicyCount; i++)
            {
                cancelPolicy += Request["CancellPolicyText-" + i];
                cancelPolicy += "#&#";
            }
            if (cancelPolicy == "#&#")
            {
                activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                hdfCanPolCounter.Value = "2";
            }
            else
            {
                if (cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                    hdfCanPolCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("CancellPolicyValue", cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfCanPolCounter.Value = Convert.ToString(cancelPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }
            }

            //Things to Bring
            int thingsCount = Convert.ToInt16(hdfThingsCounter.Value);
            emptyStringArr[0] = string.Empty;

            string things = string.Empty;
            for (int i = 1; i < thingsCount; i++)
            {
                things += Request["ThingsToBringText-" + i];
                things += "#&#";
            }
            if (things == "#&#")
            {
                activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                hdfThingsCounter.Value = "2";
            }
            else
            {
                if (things.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
                {
                    activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                    hdfThingsCounter.Value = "2";
                }
                else
                {
                    activityVariablesArray.Add("ThingsToBringValue", things.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                    hdfThingsCounter.Value = Convert.ToString(things.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
                }

            }

            //PickUpLocations and Timings
            int pickupCount = Convert.ToInt32(hdnPickupCount.Value);
            emptyStringArr[0] = string.Empty;
            if (pickupCount > 0)
            {
                 pickupLocationDetails = string.Empty;
                for (int p = 1; p <= pickupCount; p++)
                {
                    pickupLocationDetails += Request["txtPickLocation-" + p] + ", " + Request["txtPickUpHr-" + p] + ":" + Request["txtPickUpMin-" + p] + " " + (!string.IsNullOrEmpty(Request["txtPickUpHr-" + p]) && Convert.ToInt32(Request["txtPickUpHr-" + p]) < 12 ? "AM" : "PM") + "|";
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        try
        {
            string rrr = txtDuration.Text;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void InitializePageControls()
    {
        try
        {
            clear();
            GetCountry();
            Getthemes();
            BindAgent();

        }
        catch
        {
            throw;
        }
    }
    private void GetCountry()
    {
        CountryText1.DataSource = Country.GetCountryList();
        CountryText1.DataValueField = "Value";
        CountryText1.DataTextField = "Key";
        CountryText1.DataBind();

    }
    private void GetCity(string countryid)
    {

        CityText1.DataSource = Activity.GetCity(countryid);
        CityText1.DataTextField = "cityName";
        CityText1.DataValueField = "cityCode";
        CityText1.DataBind();

    }
    private void Getthemes()
    {
        try
        {
            chkTheme.DataSource = Activity.GetTheme("A");
            chkTheme.DataValueField = "themeId";
            chkTheme.DataTextField = "themeName";
            chkTheme.DataBind();
        }
        catch
        {
            throw;
        }
    }
    protected void btnUpload1_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage1.HasFile == true)
            {
                if (fuImage1.PostedFile.ContentLength < 614400)// 600 kb
                {
                    string fileExtension = Path.GetExtension(fuImage1.FileName);
                    string filename = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");

                    string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"];
                    if (!Directory.Exists(folderName))
                    {
                        Directory.CreateDirectory(folderName);
                    }
                    if (string.IsNullOrEmpty(folderName))
                    {
                        lblError.Text = "Activity Image path is not configured for the Copy Agent";
                        lblError.Visible = true;
                        return;
                    }

                    //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;
                    string imagePath = folderName + filename + fileExtension;
                    fuImage1.SaveAs(imagePath);
                    //Session["flname"] = flname;
                    lblUpload1.Text = filename + fileExtension;
                    //lblUploadMsg.Text = filename + fileExtension + " Image 1 uploaded.";
                    lbUploadMsg.Text = " Image 1 uploaded.";
                    hdfImage1.Value = imagePath;
                    hdfImage1Extn.Value = fileExtension;
                    hdfUploadYNImage1.Value = "1";
                }
                else
                {
                    lblError.Text = "File size should not be more than 600 KB";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }
            Utility.StartupScript(this.Page, "ShowTab('packageTab3');", "showTab");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page: " + ex.ToString(), "0");
        }

    }
    protected void btnUpload2_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage2.HasFile == true)
            {
                if (fuImage2.PostedFile.ContentLength < 614400)// 600 kb
                {
                    string fileExtension = Path.GetExtension(fuImage2.FileName);
                    string filename = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");

                    string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"];
                    if (!Directory.Exists(folderName))
                    {
                        Directory.CreateDirectory(folderName);
                    }
                    string imagePath = folderName + filename + fileExtension;
                    //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;
                    //string imagePath = Server.MapPath("~/ActivityImages/") + filename + fileExtension;
                    fuImage2.SaveAs(imagePath);
                    lblUplaod2.Text = filename + fileExtension;
                    //lblUpload2Msg.Text = filename + fileExtension + " image 2 uploaded.";
                    //lblUpload2Msg.Text =  " Image 2 uploaded.";
                    lbUpload2Msg.Text = " Image 2 uploaded.";
                    hdfImage2.Value = imagePath;
                    hdfImage2Extn.Value = fileExtension;
                    hdfUploadYNImage2.Value = "1";
                }
                else
                {
                    lblError.Text = "File size should not be more than 600 KB";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }
            Utility.StartupScript(this.Page, "ShowTab('packageTab3');", "showTab");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void btnUpload3_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage3.HasFile == true)
            {
                if (fuImage3.PostedFile.ContentLength < 614400)// 600 kb
                {
                    string fileExtension = Path.GetExtension(fuImage3.FileName);
                    string filename = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");

                    string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"];
                    if (!Directory.Exists(folderName))
                    {
                        Directory.CreateDirectory(folderName);
                    }
                    string imagePath = folderName + filename + fileExtension;
                    //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;
                    fuImage3.SaveAs(imagePath);
                    lblUpload3.Text = filename + fileExtension;
                    //lblUpload3Msg.Text = filename + fileExtension + " image 3 uploaded.";
                    lbUpload3Msg.Text = " Image 3 uploaded.";
                    hdfImage3.Value = imagePath;
                    hdfImage3Extn.Value = fileExtension;
                    hdfUploadYNImage3.Value = "1";
                }
                else
                {
                    lblError.Text = "File size should not be more than 600 KB";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }
            Utility.StartupScript(this.Page, "ShowTab('packageTab3');", "showTab");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void lbUploadMsg_Click(object sender, EventArgs e)
    {
        try
        {
            //string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
            string imgPath = hdfImage1.Value;
            img1PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','1');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
            Utility.StartupScript(this.Page, "ShowTab('packageTab3');", "showTab");
        }
        catch { throw; }

    }
    protected void lbUpload2Msg_Click(object sender, EventArgs e)
    {
        try
        {
            //string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
            string imgPath = hdfImage2.Value;
            img2PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','2');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
            Utility.StartupScript(this.Page, "ShowTab('packageTab3');", "showTab");
        }
        catch { throw; }

    }
    protected void lbUpload3Msg_Click(object sender, EventArgs e)
    {
        try
        {
            //string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesSaveFolder"];
            string imgPath = hdfImage3.Value;
            img3PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','3');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
            Utility.StartupScript(this.Page, "ShowTab('packageTab3');", "showTab");
        }
        catch { throw; }

    }
    private DataTable ActivityDetails
    {
        get
        {
            DataTable dtActivity = null;
            dtActivity = Session[AFC_DETAILS_SESSION] as DataTable;
            return dtActivity;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["flexId"] };
            Session[AFC_DETAILS_SESSION] = value;
        }
    }
    private DataTable PriceDetails
    {
        get
        {
            DataTable dtPrice = null;

            dtPrice = Session[PRICE_DETAILS_SESSION] as DataTable;
            return dtPrice;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["priceId"] };
            Session[PRICE_DETAILS_SESSION] = value;
        }
    }
    private void BindGrid()
    {
        try
        {
            DataTable dtTemp;
            if (ActivityDetails.Rows.Count > 0)
            {
                DataRow[] dr = ActivityDetails.Select("flexid= 0");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                }
                dtTemp = ActivityDetails.Copy();
                dtTemp.AcceptChanges();
                if (dtTemp.Rows.Count == 0)
                {
                    AddNewGridRowForAFD();

                }
                else
                {
                    gvAFCDetails.DataSource = ActivityDetails;
                    gvAFCDetails.DataBind();
                }

                // gvAFCDetails.DataSource = ActivityDetails;
                //gvAFCDetails.DataBind();

            }
            else
            {
                AddNewGridRowForAFD();
                dtTemp = ActivityDetails.Copy();
                dtTemp.AcceptChanges();
            }

            // setRowId();
            try
            {
                if (gvAFCDetails.Rows.Count >= 1 && Convert.ToInt32(dtTemp.Rows[0]["flexid"]) > 0)
                {

                    hdfAFCCount.Value = Convert.ToString(gvAFCDetails.Rows.Count);
                }
            }
            catch
            {
                hdfAFCCount.Value = "0";
            }
            //else
            //{

            //    DataRow dr = ActivityDetails.NewRow();
            //    foreach (DataColumn dc in ActivityDetails.Columns)
            //    {
            //        if (dc.ColumnName == "flexControlDESC") dc.MaxLength = 20;
            //        if (dc.ColumnName == "flexDataTypeDESC") dc.MaxLength = 20;
            //        if (dc.ColumnName == "flexStatusDESC") dc.MaxLength = 20;

            //        if (!dc.AllowDBNull)
            //        {
            //            switch (dc.DataType.Name)
            //            {
            //                case "DateTime":
            //                    dr[dc] = default(DateTime);
            //                    break;
            //                default:
            //                    dr[dc] = 0;
            //                    break;
            //            }
            //        }
            //    }
            //    dr["flexid"] = 0;
            //    ActivityDetails.Rows.Add(dr);
            //    gvAFCDetails.DataSource = ActivityDetails;
            //    gvAFCDetails.DataBind();
            //    gvAFCDetails.Rows[0].Visible = false;
            //}

        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message);
            throw;
        }


    }
    private void AddNewGridRowForAFD()
    {


        DataRow dr = ActivityDetails.NewRow();
        foreach (DataColumn dc in ActivityDetails.Columns)
        {
            if (dc.ColumnName == "flexControlDESC") dc.MaxLength = 20;
            if (dc.ColumnName == "flexDataTypeDESC") dc.MaxLength = 20;
            if (dc.ColumnName == "flexStatusDESC") dc.MaxLength = 20;

            if (!dc.AllowDBNull)
            {
                switch (dc.DataType.Name)
                {
                    case "DateTime":
                        dr[dc] = default(DateTime);
                        break;
                    default:
                        dr[dc] = 0;
                        break;
                }
            }
        }

        dr["flexid"] = 0;
        ActivityDetails.Rows.Add(dr);
        gvAFCDetails.DataSource = ActivityDetails;
        gvAFCDetails.DataBind();
        gvAFCDetails.Rows[0].Visible = false;
    }
    private void BindGridPrice()
    {
        try
        {

            DataTable dtTemp;
            if (PriceDetails.Rows.Count > 0)
            {
                DataRow[] dr = PriceDetails.Select("priceId= 0");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                }
                dtTemp = PriceDetails.Copy();
                dtTemp.AcceptChanges();
                if (dtTemp.Rows.Count == 0)
                {
                    AddNewGridRow();

                }
                else
                {
                    GvPrice.DataSource = PriceDetails;
                    GvPrice.DataBind();
                }
            }
            else
            {
                AddNewGridRow();
                dtTemp = PriceDetails.Copy();
                dtTemp.AcceptChanges();
            }

            setRowId();
            try
            {
                if (GvPrice.Rows.Count >= 1 && Convert.ToInt32(dtTemp.Rows[0]["priceId"]) > 0)
                {
                    hdfPriceCount.Value = Convert.ToString(GvPrice.Rows.Count);
                }
            }
            catch
            {
                hdfPriceCount.Value = "0";
            }


        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message);
            throw;
        }


    }
    private void AddNewGridRow()
    {


        DataRow dr = PriceDetails.NewRow();
        foreach (DataColumn dc in PriceDetails.Columns)
        {
            if (!dc.AllowDBNull)
            {
                switch (dc.DataType.Name)
                {
                    case "DateTime":
                        dr[dc] = default(DateTime);
                        break;
                    default:
                        dr[dc] = 0;
                        break;
                }
            }
        }

        dr["priceId"] = 0;
        PriceDetails.Rows.Add(dr);
        GvPrice.DataSource = PriceDetails;
        GvPrice.DataBind();
        GvPrice.Rows[0].Visible = false;
    }
    private void clear()
    {

        Activity tempAFC = new Activity(-1);
        ActivityDetails = tempAFC.DTAFCDetails;
        PriceDetails = tempAFC.DTPriceDetails;
        //if (hdnClear.Value == "Tour Details")
        //{
            txtActivityName.Text = string.Empty;
            txtStartingFrom.Text = string.Empty;
            txtIntroduction.Text = string.Empty;
            txtOverView.Text = string.Empty;
            txtItinerary1.Text = string.Empty;
            txtItinerary2.Text = string.Empty;
            txtDetails.Text = string.Empty;
            txtSupplierName.Text = string.Empty;
            txtSupplierEmail.Text = string.Empty;
            txtSupplierTelephone.Text = string.Empty;
            txtServiceName.Text = string.Empty;
           // txtMealsInclude.Text = string.Empty;
            //txtTrasferInclude.Text = string.Empty;
            txtPickLocation.Text = string.Empty;
            //txtDropOffLocation.Text = string.Empty;
            txtPickUpHr.Text = string.Empty;
            txtPickUpMin.Text = string.Empty;
            //txtDropUpHr.Text = string.Empty;
            //txtDropUpMin.Text = string.Empty;
            txtStartFromHr.Text = string.Empty;
            txtStartTimeMin.Text = string.Empty;
            txtEndTimeMin.Text = string.Empty;
            txtEndToHr.Text = string.Empty;
            CountryText1.SelectedIndex = 0;
            CityText1.SelectedIndex = 0;
            ddlMealsInclude.SelectedIndex = 0;
            ddlTransferInclude.SelectedIndex = 0;

        //}
        //else if (hdnClear.Value == "Price Details")
        //{
            gvAFCDetails.EditIndex = -1;
        //}
       // else if (hdnClear.Value == "Gallery")
       // {
            fuImage1.Dispose();
            fuImage2.Dispose();
            fuImage3.Dispose();
            lbUploadMsg.Text = string.Empty;
            lbUpload2Msg.Text = string.Empty;
            lbUpload3Msg.Text = string.Empty;
            lblUpload3.Text = string.Empty;
            lblUplaod2.Text = string.Empty;
            lblUpload1.Text = string.Empty;
      //  }
       // else if (hdnClear.Value == "Available Dates")
       // {
            txtFrom.Text = string.Empty;
            txtFromTime.Text = string.Empty;
            txtTo.Text = string.Empty;
            txtToTime.Text = string.Empty;
            Date.Text = string.Empty;
      //  }
       // else if (hdnClear.Value == "Inclusions / Exclusions")
      //  {
            txtUnavailableDays.Text = string.Empty;
            ddlBookingCutOff.SelectedIndex = 0;
            for (int i = 1; i < Convert.ToInt32(hdfExclCounter.Value); i++)
            {
                string txtExclusion = Request["ExclusionsText-" + i];
                if (txtExclusion != null)
                {

                }
            }
        //}
        chkTheme.Dispose();

        ddlDays.SelectedIndex = 0;


        // txtDuration.Text = string.Empty;
        txtUnavailableDays.Text = string.Empty;


        txtStockInHand.Text = string.Empty;

        chkTheme.SelectedIndex = -1;

        lblMessage.Text = string.Empty;
        hdfAFCDetails.Value = "0";
        hdfPrice.Value = "0";
        BindGrid();
        BindGridPrice();
        CurrentObject = null;

        hdfExclCounter.Value = "2";
        hdfinclCounter.Value = "2";
        hdfCanPolCounter.Value = "2";
        hdfThingsCounter.Value = "2";
        hdfUnavailDateCounter.Value = "2";
        //ddlAgent.Enabled = true;
        txtPickUpPoint.Text = string.Empty;
    }

    


    private void LoadActivity(long id)
    {
        gvAFCDetails.EditIndex = -1;
        Activity activity = new Activity(id);
        CurrentObject = activity;
        ActivityDetails = activity.DTAFCDetails;
        PriceDetails = activity.DTPriceDetails;
        txtActivityName.Text = activity.ActivityName;
        txtStartingFrom.Text = Convert.ToString(activity.StartingFrom);
        if (!string.IsNullOrEmpty(activity.Currency))
        {
            lblCurrency.Text = activity.Currency;
        }
        else
        {
            lblCurrency.Text = AgentMaster.GetAgentCurrency(activity.AgencyId);
        }
        Getthemes();

        String[] themes = activity.Theme.Split(',');
        foreach (string theme in themes)
        {
            if (chkTheme.Items.FindByValue(theme) != null)
            {
                chkTheme.Items.FindByValue(theme).Selected = true;
            }
        }

        BindAgent();
        ddlAgent.SelectedValue = Convert.ToString(activity.AgencyId);
        //ddlAgent.Enabled = false;

        string country = activity.Country;
        string city = activity.City;
        GetCountry();
        try
        {
            if (!string.IsNullOrEmpty(country))
            {

                CountryText1.SelectedValue = country;
                hdfSelCountry.Value = country;
            }

            GetCity(country);
            if (!string.IsNullOrEmpty(city))
            {

                CityText1.SelectedValue = city;
                hdfSelCity.Value = city;
            }
        }
        catch (Exception)
        {
        }

        ddlDays.SelectedValue = Convert.ToString(activity.ActivityDays);

        string[] startFrom = activity.StartFrom.ToString("HH:mm").Split(':');
        //string[] start=startFrom.sp
        txtStartFromHr.Text = startFrom[0];
        txtStartTimeMin.Text = startFrom[1];

        string[] endTo = activity.EndTo.ToString("HH:mm").Split(':');
        txtEndToHr.Text = endTo[0];
        txtEndTimeMin.Text = endTo[1];
        txtDuration.Text = activity.ActivityDuration;
        string FromDate = activity.AvailableDateStart.ToString("dd/MM/yyyy");
        //FromDate=FromDate.Replace(
        txtFrom.Text = activity.AvailableDateStart.ToString("dd/MM/yyyy").Replace("-", "/");
        txtFromTime.Text = activity.AvailableDateStart.ToString("hh:mm tt");
        txtTo.Text = activity.AvailableDateEnd.ToString("dd/MM/yyyy").Replace("-", "/");
        txtToTime.Text = activity.AvailableDateEnd.ToString("hh:mm tt");
        txtPickUpHr.Text = string.Empty;
        txtPickUpMin.Text = string.Empty;
        Date.Text = string.Empty;
        //if (activity.PickUpDateTime != DateTime.MinValue)
        //{
        //    string[] pickTime = activity.PickUpDateTime.ToString("HH:mm").Split(':');
        //    txtPickUpHr.Text = pickTime[0];
        //    txtPickUpMin.Text = pickTime[1];
        //}
        txtPickUpHr.Text.Trim();
        txtPickUpMin.Text.Trim();

        if (activity.TransferInclude == "Yes" && !string.IsNullOrEmpty(activity.PickLocation))
        {
            txtPickLocation.Text = null;
            txtPickUpHr.Text = null;
            txtPickUpMin.Text = null;
            string[] pickupDetails = null; 
            string[] pickupLocationTime=activity.PickLocation.Split(new string[] {"#&#"},StringSplitOptions.RemoveEmptyEntries);
            if (pickupLocationTime != null && pickupLocationTime.Length > 0)
            {
                pickupLocationDetails = string.Empty;
                for (int p=0;p<pickupLocationTime.Length;p++)
                {
                    pickupDetails = pickupLocationTime[p].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (p == 0)
                    {
                        if (pickupDetails.Length > 2)
                        {
                            string pickupTime = pickupDetails[pickupDetails.Length - 1];
                            string pickupLocation = string.Empty;
                            for (int m = 0; m < pickupDetails.Length - 1;m++ )
                            {
                                pickupLocation += pickupDetails[m] + " ";
                            }
                            txtPickLocation.Text = pickupLocation;
                            pickupTime.TrimStart();
                            txtPickUpHr.Text = pickupTime.Split(':')[0];
                            txtPickUpMin.Text = pickupTime.Split(new char[] { ':', ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
                        }
                        else if (pickupDetails.Length == 2)
                        {
                            txtPickLocation.Text = pickupDetails[0];
                            if (!string.IsNullOrEmpty(pickupDetails[1].TrimStart()))
                            {
                                string pickupstarttime = pickupDetails[1].TrimStart().Split('-')[0];
                                txtPickUpHr.Text = pickupstarttime.Split(':')[0];
                                txtPickUpMin.Text = pickupstarttime.Split(':')[1];
                                string pickupEndtime = pickupDetails[1].TrimStart().Split('-')[1];
                                txtPickUpHrRange.Text = pickupEndtime.Split(':')[0];
                                txtPickUpMinRange.Text= pickupEndtime.Split(':')[1];
                            }
                            //if (!string.IsNullOrEmpty(pickupDetails[2].TrimStart()))
                            //{
                            //    txtPickUpHrRange.Text = pickupDetails[2].TrimStart().Split(':')[0];
                            //    txtPickUpMinRange.Text = pickupDetails[2].TrimStart().Split(new char[] { ':', ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];
                            //}
                        }
                        else {
                            txtPickLocation.Text = pickupDetails[0];
                        }
                    }
                    else {
                        pickupLocationDetails += pickupLocationTime[p] + '|';
                    }
                    if (activity.PickLocation.Length > 0)
                    {
                        txtPickUpPoint.Text = activity.PickupPoint;
                    }
                }
                txtPickLocation.Enabled = true;
                txtPickUpHr.Enabled = true;
                txtPickUpMin.Enabled = true;
                txtPickUpPoint.Enabled=true;
                txtPickUpMinRange.Enabled = true;
                txtPickUpHrRange.Enabled = true;
                hdnPickupCount.Value = "0";
            }


            //txtDropUpHr.Text = dropTime[0];
            //txtDropUpMin.Text = dropTime[1];
        }
        else
        {
            //txtDropUpHr.Text = string.Empty;
            //txtDropUpMin.Text = string.Empty;
        }
        txtUnavailableDays.Text = string.Empty;
        // txtDuration.Text = string.Empty;
        txtIntroduction.Text = Server.HtmlDecode(activity.Introduction);
        txtOverView.Text = Server.HtmlDecode(activity.Overview);
        txtItinerary1.Text = Server.HtmlDecode(activity.Itinerary1);
        if (activity.ActivityDays == 2)
        {
            txtItinerary2.Visible = true;
            lblItinerary2.Visible = true;
            txtItinerary2.Text = activity.Itinerary2;
        }
        txtDetails.Text = Server.HtmlDecode(activity.Details);
        txtSupplierName.Text = activity.SupplierName;
        txtSupplierEmail.Text = activity.SupplierEmail;
        txtServiceName.Text = activity.SupplierServiceName;
        txtSupplierTelephone.Text = activity.SupplierTelePhone;
        if (activity.MealsIncluded == "Yes")
        {
            ddlMealsInclude.SelectedIndex = 1;
        }
        else
        {
            ddlMealsInclude.SelectedIndex = 2;
        }
        if (activity.TransferInclude == "Yes")
        {
            ddlTransferInclude.SelectedIndex = 1;
        }
        else
        {
            ddlTransferInclude.SelectedIndex = 2;
        }

        //ddlMealsInclude.SelectedItem.Text = activity.MealsIncluded;
        //ddlTransferInclude.SelectedItem.Text = activity.TransferInclude;

        //if (activity.TransferInclude == "Yes")
        //{
        //    txtPickLocation.Enabled = true;
        //    txtPickUpHr.Enabled = true;
        //    txtPickUpMin.Enabled = true;
        //    //txtDropOffLocation.Enabled = true;
        //    //txtDropUpHr.Enabled = true;
        //    //txtDropUpMin.Enabled = true;
        //}
        //else
        //{
        //    txtPickLocation.Enabled = false;
        //    txtPickUpHr.Enabled = false;
        //    txtPickUpMin.Enabled = false;
        //    //txtDropOffLocation.Enabled = false;
        //    //txtDropUpHr.Enabled = false;
        //    //txtDropUpMin.Enabled = false;

        //}

        //txtPickLocation.Text = activity.PickLocation;
        //txtDropOffLocation.Text = activity.DropOffLocation;
        txtUnavailableDays.Text = activity.UnAvailableDays;
        ddlBookingCutOff.SelectedValue = Convert.ToString(activity.BookingCutOff);

        //string
        //string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityEditImagesFolder"];
        string folderName = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"];
        //string imagePath = Server.MapPath("~/" + folderName + "/") + filename + fileExtension;

        lblUpload1.Text = activity.Image1;
        hdfImage1.Value = folderName + lblUpload1.Text;
        hdfImage1Extn.Value = Path.GetExtension(activity.Image1);
        if (!string.IsNullOrEmpty(activity.Image1) && !string.IsNullOrEmpty(hdfImage1Extn.Value))
        {
            lbUploadMsg.Text = " Image 1 uploaded.";
            //lbUploadMsg.Text = " Image 1 uploaded.";
            //lbUploadMsg.OnClientClick = "return showimag1('dUpload1Msg','imgupload1','" + hdfImage1.ClientID + "')";
        }
        hdfUploadYNImage1.Value = "1";


        lblUplaod2.Text = activity.Image2;
        hdfImage2.Value = folderName + lblUplaod2.Text;
        hdfImage2Extn.Value = Path.GetExtension(activity.Image2);
        if (!string.IsNullOrEmpty(activity.Image2) && !string.IsNullOrEmpty(hdfImage2Extn.Value))
        {
            lbUpload2Msg.Text = " Image 2 uploaded.";
            //lbUpload2Msg.Text = " Image 2 uploaded.";
            //lbUpload2Msg.OnClientClick = "return showimag1('dUpload2Msg','imgupload2','" + hdfImage2.ClientID + "')";
        }
        hdfUploadYNImage2.Value = "1";


        lblUpload3.Text = activity.Image3;
        //hdfImage3.Value = "./ActivityImages/" + lblUpload3.Text;
        hdfImage3.Value = folderName + lblUpload3.Text;
        hdfImage3Extn.Value = Path.GetExtension(activity.Image3);
        if (!string.IsNullOrEmpty(activity.Image3) && !string.IsNullOrEmpty(hdfImage3Extn.Value))
        {
            lbUpload3Msg.Text = " Image 3 uploaded.";
            //lbUpload3Msg.Text = " Image 3 uploaded.";
            //lbUpload3Msg.OnClientClick = "return showimag1('dUpload3Msg','imgupload3','" + hdfImage3.ClientID + "')";
        }
        hdfUploadYNImage3.Value = "1";


        txtStockInHand.Text = Convert.ToString(activity.StockInHand);
        if (activity.stockinUsed > 0)
        {
            txtStockUsed.Visible = true;
            txtStockUsed.Text= Convert.ToString(activity.stockinUsed);
        }
        if (activity.Inclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfinclCounter.Value = Convert.ToString(activity.Inclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfinclCounter.Value = "2";
        }
        if (activity.Exclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfExclCounter.Value = Convert.ToString(activity.Exclusion.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfExclCounter.Value = "2";
        }
        if (activity.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfCanPolCounter.Value = Convert.ToString(activity.CancellationPolicy.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfCanPolCounter.Value = "2";
        }

        if (activity.ThingsToBring.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfThingsCounter.Value = Convert.ToString(activity.ThingsToBring.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfThingsCounter.Value = "2";
        }
        if (activity.UnvailableDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
        {
            hdfUnavailDateCounter.Value = Convert.ToString(activity.UnvailableDates.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
        }
        else
        {
            hdfUnavailDateCounter.Value = "2";
        }
        hdfinclusion.Value = activity.Inclusion;
        hdfExclusions.Value = activity.Exclusion;
        hdfCanPolicy.Value = activity.CancellationPolicy;
        hdfThings.Value = activity.ThingsToBring;
        hdfUnavailDates.Value = activity.UnvailableDates;



        lblMessage.Text = string.Empty;
        hdfAFCDetails.Value = "0";
        hdfPrice.Value = "0";
        BindGrid();
        BindGridPrice();
        divStockUsed.Visible = true;
        //txtStockUsed.Text = Convert.ToString(activity.StockUsed);


    }
    private void setRowId()
    {
        try
        {
            if (GvPrice.EditIndex > -1)
            {
            }
            else
            {
                ((TextBox)GvPrice.FooterRow.FindControl("FTtxtAmount")).Text = Convert.ToDecimal(0).ToString();
            }
            txtTotal.Text = Convert.ToString(PriceDetails.Compute("SUM(amount)", ""));


        }
        catch { throw; }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["PageSaved"] == null )//|| (Session["PageSaved"] != null && Convert.ToBoolean(Session["PageSaved"])==false))// since double loading
                Save();

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }
    protected void RenameImage(string activityId, string imagePath, string slNo)
    {
        try
        {

            //string imagePath = hdfImage1.Value;
            string source = imagePath.Substring(0, imagePath.LastIndexOf("\\") + 1);
            if (!string.IsNullOrEmpty(source))
            {
                // string fName = imagePath;

                string oldFile = Path.GetFileName(imagePath);
                string fileExtension = Path.GetExtension(imagePath);
                string dFilePath = string.Empty;
                //dFilePath = source + dFile;
                //FileInfo fi = new FileInfo(dFilePath);
                string newfile = activityId + "_" + slNo + fileExtension;
                if (newfile != oldFile)
                {
                    if (System.IO.File.Exists(source + newfile)) System.IO.File.Delete(source + newfile);
                    System.IO.File.Move(source + oldFile, source + newfile);
                    System.IO.File.Delete(source + oldFile);
                }
            }

        }
        catch { }

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            clear();
            //Response.Redirect("activitymaster.aspx");
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = ex.Message;

        }
    }
    private void Save()
    {
        try
        {
            //Activity tmpActivity = new Activity(-1);


            int themeCount = 0;
            string s1 = string.Empty;
            for (int i = 0; i < chkTheme.Items.Count; i++)
            {
                if (chkTheme.Items[i].Selected)
                {
                    s1 += chkTheme.Items[i].Value + ",";
                    themeCount = themeCount + 1;
                }
            }

            if (themeCount == 0)
            {
                throw new Exception("Atleast one theme should be selected !");
            }

            if (txtIntroduction.Text == "") throw new Exception("Introduction cannot be blank !");
            if (txtOverView.Text == "") throw new Exception("OverView cannot be blank !");
            if (txtItinerary1.Text == "") throw new Exception("Itinerary 1 cannot be blank !");


            if (PriceDetails.Rows.Count <= 1 && Convert.ToInt32(PriceDetails.Rows[0][0]) == 0)
            {
                DataRow[] drPrice = PriceDetails.Select("priceId= 0");
                if (drPrice.Length > 0)
                {
                    throw new Exception("Price details cannot be blank !");
                }
            }


            Activity objActivity;

            if (CurrentObject == null)
            {
                objActivity = new Activity();
                objActivity.Id = -1;
            }
            else
            {
                objActivity = CurrentObject;
            }

            objActivity.ActivityName = txtActivityName.Text;
            if (txtStockInHand.Text.Trim() != "")
            {
                objActivity.StockInHand = Convert.ToInt32(txtStockInHand.Text); ;
            }
            objActivity.ActivityDays = Convert.ToInt32(ddlDays.SelectedItem.Value);
            if (txtStartingFrom.Text.Trim() != "")
            {
                objActivity.StartingFrom = Convert.ToDecimal(txtStartingFrom.Text);
            }


            objActivity.Country = hdfSelCountry.Value;//CountryText1.SelectedItem.Value;
            objActivity.City = hdfSelCity.Value;
            objActivity.Currency = lblCurrency.Text;
            objActivity.Image1 = lblUpload1.Text;
            objActivity.Image2 = lblUplaod2.Text;
            objActivity.Image3 = lblUpload3.Text;
            objActivity.Image1Extn = hdfImage1Extn.Value;
            objActivity.Image2Extn = hdfImage2Extn.Value;
            objActivity.Image3Extn = hdfImage3Extn.Value;

            objActivity.Theme = s1.ToString().TrimEnd(',');
            objActivity.Introduction = Server.HtmlEncode(txtIntroduction.Text);
            objActivity.Overview = Server.HtmlEncode( txtOverView.Text);
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (txtStartFromHr.Text != "" & txtStartTimeMin.Text != "")
            {
                string StartFromDate = (txtStartFromHr.Text + ":" + txtStartTimeMin.Text);
                string tmpStartFromDate = Convert.ToString(DateTime.Now.ToShortDateString());
                StartFromDate = tmpStartFromDate + " " + StartFromDate;
                //objActivity.StartFrom = Convert.ToDateTime(StartFromDate, dateFormat);
                objActivity.StartFrom = Convert.ToDateTime(StartFromDate);
            }
            if (txtEndToHr.Text != "" && txtEndTimeMin.Text != "")
            {
                string EndToDate = (txtEndToHr.Text + ":" + txtEndTimeMin.Text);
                string tmpEndToDate = Convert.ToString(DateTime.Now.ToShortDateString());
                EndToDate = tmpEndToDate + " " + EndToDate;
                //objActivity.EndTo = Convert.ToDateTime(EndToDate,dateFormat);
                objActivity.EndTo = Convert.ToDateTime(EndToDate);
            }
            objActivity.ActivityDuration = txtDuration.Text;
            if (txtFrom.Text != "" && txtFromTime.Text != "")
            {
                string AFrom = (txtFrom.Text + " " + txtFromTime.Text);
                objActivity.AvailableDateStart = Convert.ToDateTime(AFrom, dateFormat);
                //objActivity.AvailableDateStart = Convert.ToDateTime(AFrom);
            }
            if (txtTo.Text != "" && txtToTime.Text != "")
            {
                string ATo = (txtTo.Text + " " + txtToTime.Text);
                objActivity.AvailableDateEnd = Convert.ToDateTime(ATo, dateFormat);
                //objActivity.AvailableDateEnd = Convert.ToDateTime(ATo);
            }

            int unAvailCounter = Convert.ToInt32(Request["UnAvailDatesCurrent"]);
            string unAvailDates = string.Empty;
            for (int i = 1; i < unAvailCounter; i++)
            {
                unAvailDates += Request["txtUnAvailDatesText-" + i];
                unAvailDates += "#&#";
            }
            if (!string.IsNullOrEmpty(unAvailDates))
            {
                string Dates = removeDuplicateDates(unAvailDates);
                if (!string.IsNullOrEmpty(Dates))
                {
                    objActivity.UnvailableDates = Dates;
                    int count = Dates.Replace("#&#", "|").Split('|').Length - 1;
                    if (count == 0)
                    {
                        objActivity.UnAvailableDays = Convert.ToString("1");
                    }
                    else
                    {
                        objActivity.UnAvailableDays = Convert.ToString(count);
                    }
                }
                else
                {
                    objActivity.UnvailableDates = unAvailDates;
                    objActivity.UnAvailableDays = "0";
                }
            }
            else
            {
                objActivity.UnvailableDates = unAvailDates;
                objActivity.UnAvailableDays = "0";
            }

            objActivity.Itinerary1 = Server.HtmlEncode(txtItinerary1.Text);
            objActivity.Itinerary2 = txtItinerary2.Text;
            objActivity.Details = Server.HtmlEncode( txtDetails.Text);
            //objActivity.UnAvailableDays = hdnUnavailableDays.Value;

            objActivity.SupplierName = txtSupplierName.Text.Trim(); ;
            objActivity.SupplierEmail = txtSupplierEmail.Text;
            //Added by Anji
            objActivity.SupplierServiceName = txtServiceName.Text.Trim();
            objActivity.SupplierTelePhone = txtSupplierTelephone.Text.Trim();
            //End
            objActivity.MealsIncluded = ddlMealsInclude.SelectedItem.Text;
            objActivity.TransferInclude = ddlTransferInclude.SelectedItem.Text;

            string PickupLocation = string.Empty;
            string pickuptime = string.Empty;
            if (ddlTransferInclude.SelectedValue.ToString().ToLower() == "yes")
            {
                if (!string.IsNullOrEmpty(txtPickLocation.Text))// && 
                {
                    PickupLocation = txtPickLocation.Text + ", ";
                    if (!string.IsNullOrEmpty(txtPickUpHr.Text) && !string.IsNullOrEmpty(txtPickUpMin.Text))
                    {
                        string hour = Convert.ToInt32(txtPickUpHr.Text) < 10 ? "0" + txtPickUpHr.Text : txtPickUpHr.Text;
                        string min = Convert.ToInt32(txtPickUpMin.Text) < 10 ? "0" + txtPickUpMin.Text : txtPickUpMin.Text;
                        PickupLocation += hour + ":" + min;
                    }
                    else
                    {
                        PickupLocation += string.Empty;
                    }
                    if (!string.IsNullOrEmpty(txtPickUpHrRange.Text) && !string.IsNullOrEmpty(txtPickUpMinRange.Text))
                    {
                        string hour = Convert.ToInt32(txtPickUpHrRange.Text) < 10 ? "0" + txtPickUpHrRange.Text : txtPickUpHrRange.Text;
                        string min = Convert.ToInt32(txtPickUpMinRange.Text) < 10 ? "0" + txtPickUpMinRange.Text : txtPickUpMinRange.Text;
                        PickupLocation += "-" + hour + ":" + min;
                    }
                    else {
                        PickupLocation += string.Empty;
                    }
                }
                else {
                    throw new Exception("Please fill all PickUp Details");
                }
                
                if (!string.IsNullOrEmpty(txtPickUpPoint.Text))
                {
                    objActivity.PickupPoint = txtPickUpPoint.Text;
                }

                int pickupCount = Convert.ToInt32(hdnPickupCount.Value);
                if (pickupCount > 0)
                {
                    for (int pick = 1; pick <= pickupCount; pick++)
                    {
                        //if (pickupCount == 0 || (pickupCount> 0 && pick < pickupCount))
                        {
                            if (!string.IsNullOrEmpty(Request["txtPickLocation-" + pick]))
                            {
                                PickupLocation += "#&#" + Request["txtPickLocation-" + pick] + ", ";
                                if (!string.IsNullOrEmpty(Request["txtPickUpHr-" + pick]) && !string.IsNullOrEmpty(Request["txtPickUpMin-" + pick]))
                                {
                                    pickuptime = string.Empty;
                                    string hour = Convert.ToInt32(Request["txtPickUpHr-" + pick]) < 10 ? "0" + Convert.ToInt32(Request["txtPickUpHr-" + pick]) : Request["txtPickUpHr-" + pick];
                                    string min = Convert.ToInt32(Request["txtPickUpMin-" + pick]) < 10 ? "0" + Convert.ToInt32(Request["txtPickUpMin-" + pick]) : Request["txtPickUpMin-" + pick];

                                    PickupLocation += hour + ":" + min;
                                }
                                else
                                {
                                    PickupLocation += string.Empty;
                                }
                                if (!string.IsNullOrEmpty(Request["txtPickUpHrRange-" + pick]) && !string.IsNullOrEmpty(Request["txtPickUpMinRange-" + pick]))
                                {
                                    pickuptime = string.Empty;
                                    string hour = Convert.ToInt32(Request["txtPickUpHrRange-" + pick]) < 10 ? "0" + Convert.ToInt32(Request["txtPickUpHrRange-" + pick]) : Request["txtPickUpHrRange-" + pick];
                                    string min = Convert.ToInt32(Request["txtPickUpMinRange-" + pick]) < 10 ? "0" + Convert.ToInt32(Request["txtPickUpMinRange-" + pick]) : Request["txtPickUpMinRange-" + pick];

                                    PickupLocation += "-" + hour + ":" + min;
                                }
                                else
                                {
                                    PickupLocation += string.Empty;
                                }
                            }
                            else
                            {
                                throw new Exception("Please fill all PickUp Details");
                            }
                        }
                    }
                }
            }

            objActivity.PickLocation = PickupLocation; 
            int excCounter = Convert.ToInt32(Request["ExclusionsCurrent"]);
            string exclusions = string.Empty;
            for (int i = 1; i < excCounter; i++)
            {
                exclusions += Request["ExclusionsText-" + i];
                exclusions += "#&#";
            }
            objActivity.Exclusion = exclusions;


            int incCounter = Convert.ToInt32(Request["InclusionsCurrent"]);
            string inclusions = string.Empty;
            for (int i = 1; i < incCounter; i++)
            {
                inclusions += Request["InclusionsText-" + i];
                inclusions += "#&#";
            }
            objActivity.Inclusion = inclusions;


            int canPolicyCounter = Convert.ToInt32(Request["CancellPolicyCurrent"]);
            string cancelPolicy = string.Empty;
            for (int i = 1; i < canPolicyCounter; i++)
            {
                cancelPolicy += Request["CancellPolicyText-" + i];
                cancelPolicy += "#&#";
            }
            objActivity.CancellationPolicy = cancelPolicy;


            int ThingsToBringCounter = Convert.ToInt32(Request["ThingsToBringCurrent"]);
            string thingsToBring = string.Empty;
            for (int i = 1; i < ThingsToBringCounter; i++)
            {
                thingsToBring += Request["ThingsToBringText-" + i];
                thingsToBring += "#&#";
            }
            objActivity.ThingsToBring = thingsToBring;

            if (ddlBookingCutOff.SelectedIndex != 0)
            {
                objActivity.BookingCutOff = Convert.ToInt32(ddlBookingCutOff.SelectedItem.Value);
            }
            else
            {
                objActivity.BookingCutOff = 1;
            }
            objActivity.DTAFCDetails = ActivityDetails;
            objActivity.DTPriceDetails = PriceDetails;
            objActivity.CreatedBy = 1;
            objActivity.IsFixedDeparture = "N";
            objActivity.AgencyId = Convert.ToInt32(ddlAgent.SelectedItem.Value); //(int)Session["agencyId"];
            try
            {
                objActivity.SaveActivity();
                string actId = Convert.ToString(objActivity.Id);
                RenameImage(actId, hdfImage1.Value, "1");
                RenameImage(actId, hdfImage2.Value, "2");
                RenameImage(actId, hdfImage3.Value, "3");
                //hdfMode.Value = "New";



                //Response.Redirect("activitymaster.aspx",false);
                clear();
                hdfinclusion.Value = "";
                hdfExclusions.Value = "";
                hdfCanPolicy.Value = "";
                hdfThings.Value = "";
                hdfUnavailDates.Value = "";
                hdfExclCounter.Value = "2";
                hdfinclCounter.Value = "2";
                hdfCanPolCounter.Value = "2";
                hdfThingsCounter.Value = "2";
                hdfUnavailDateCounter.Value = "2";
                activityVariables.Add("exclusionsCount", hdfExclCounter.Value);
                activityVariables.Add("inclusionsCount", hdfinclCounter.Value);
                activityVariables.Add("CancellPolicyCount", hdfCanPolCounter.Value);
                activityVariables.Add("ThingsToBringCount", hdfThingsCounter.Value);
                activityVariables.Add("UnAvailDatesCount", hdfUnavailDateCounter.Value);
                string[] emptyStringArr = new string[1];
                emptyStringArr[0] = string.Empty;
                int inclusionsCount = 0;
                if (inclusionsCount < 2)
                {
                    inclusionsCount = 2;
                    activityVariablesArray.Add("inclusionsValue", emptyStringArr);
                }
                int exclusionsCount = 0;
                if (exclusionsCount < 2)
                {
                    exclusionsCount = 2;
                    activityVariablesArray.Add("exclusionsValue", emptyStringArr);
                }

                int CancellPolicyCount = 0;
                if (CancellPolicyCount < 2)
                {
                    CancellPolicyCount = 2;
                    activityVariablesArray.Add("CancellPolicyValue", emptyStringArr);
                }

                int ThingsToBringCount = 0;
                if (ThingsToBringCount < 2)
                {
                    ThingsToBringCount = 2;
                    activityVariablesArray.Add("ThingsToBringValue", emptyStringArr);
                }

                int UnAvailDatesCount = 0;
                if (UnAvailDatesCount < 2)
                {
                    UnAvailDatesCount = 2;
                    activityVariablesArray.Add("UnAvailDatesValue", emptyStringArr);
                }
                //lblMessage.ForeColor = System.Drawing.Color.Green;
                //lblMessage.Text = "SucessFully Inserted";

                //ClearAllControls();
                Utility.StartupScript(this.Page, "ShowAlertMessage('Tour is Successfully Added');", "SuccessMessage");
                Session["PageSaved"] = true;
                //Response.Redirect("ActivityMaster.aspx", false);
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
                LoadPageControls();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page saving " + ex.Message, "0");
            LoadPageControls();
        }

    }
    private void LoadPageControls()
    {
        try
        {
            activityVariables = new Dictionary<string, string>();
            activityVariablesArray = new Dictionary<string, string[]>();
            BindPostBackControls();
            hdfinclusion.Value = "";
            hdfExclusions.Value = "";
            hdfCanPolicy.Value = "";
            hdfThings.Value = "";
            hdfUnavailDates.Value = "";
            activityVariables.Add("exclusionsCount", hdfExclCounter.Value);
            activityVariables.Add("inclusionsCount", hdfinclCounter.Value);
            activityVariables.Add("CancellPolicyCount", hdfCanPolCounter.Value);
            activityVariables.Add("ThingsToBringCount", hdfThingsCounter.Value);
            activityVariables.Add("UnAvailDatesCount", hdfUnavailDateCounter.Value);
            Utility.StartupScript(this.Page, "LoadPickupDetails('" + pickupLocationDetails + "')", "bindData");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected Activity CurrentObject
    {
        get
        {
            return (Activity)Session[ACF_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ACF_SESSION);
            }
            else
            {
                Session[ACF_SESSION] = value;
            }

        }

    }
    protected void gvAFCDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvAFCDetails.FooterRow;
                DataRow dr = ActivityDetails.NewRow();
                DataTable dtDeleted = ActivityDetails.GetChanges(DataRowState.Deleted);
                long deletedSerial = 0;
                if (dtDeleted != null)
                {
                    dtDeleted.RejectChanges();
                    deletedSerial = Convert.ToInt32(dtDeleted.Compute("MAX(flexId)", ""));
                }
                long serial = Convert.ToInt32(ActivityDetails.Compute("MAX(flexId)", ""));
                serial = (serial >= deletedSerial) ? serial + 1 : deletedSerial + 1;
                dr["flexId"] = serial;
                SetActivityDetails(dr, gvRow, "FT");

                ActivityDetails.Rows.Add(dr);

                BindGrid();

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    private void SetActivityDetails(DataRow dr, GridViewRow gvRow, string mode)
    {
        try
        {

            dr["flexControl"] = ((DropDownList)gvRow.FindControl(mode + "ddlControl")).SelectedItem.Value;
            dr["flexControlDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlControl")).SelectedItem.Text;
            dr["flexLabel"] = ((TextBox)gvRow.FindControl(mode + "txtLabel")).Text;

            dr["flexSqlQuery"] = ((TextBox)gvRow.FindControl(mode + "txtSqlQuery")).Text;
            dr["flexDataType"] = ((DropDownList)gvRow.FindControl(mode + "ddlDatatype")).SelectedItem.Value;
            dr["flexDataTypeDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlDatatype")).SelectedItem.Text;
            dr["flexMandatoryStatus"] = ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Value;
            dr["flexStatusDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Text;
            dr["flexOrder"] = Convert.ToInt32(((TextBox)gvRow.FindControl(mode + "txtOrder")).Text);
            dr["flexCreatedBy"] = Settings.LoginInfo.UserID;
        }
        catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0"); }
    }
    private void SetPriceDetails(DataRow dr, GridViewRow gvRow, string mode)
    {
        try
        {
            //dr["label"] = ((TextBox)gvRow.FindControl(mode + "txtLabel")).Text;
            dr["label"] = ((DropDownList)gvRow.FindControl(mode + "ddlPaxType")).SelectedItem.Value;
            dr["SupplierCost"] = ((TextBox)gvRow.FindControl(mode + "txtSupplierCost")).Text;
            dr["tax"] = ((TextBox)gvRow.FindControl(mode + "txtTax")).Text;
            dr["markup"] = ((TextBox)gvRow.FindControl(mode + "txtMarkup")).Text;
            //dr["amount"] = ((TextBox)gvRow.FindControl(mode + "txtAmount")).Text;
            dr["amount"] = Convert.ToDecimal(dr["SupplierCost"]) + Convert.ToDecimal(dr["tax"]) + Convert.ToDecimal(dr["markup"]);
            dr["PriceDate"] = DateTime.MinValue;
            dr["MinPax"] = ((TextBox)gvRow.FindControl(mode + "txtMinPax")).Text; //Newly Added minPax Added by brahmam
            dr["createdBy"] = Settings.LoginInfo.UserID;

        }
        catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0"); }
    }
    protected void gvAFCDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvAFCDetails.EditIndex = e.NewEditIndex;
            BindGrid();
            long serial = Convert.ToInt32(gvAFCDetails.DataKeys[e.NewEditIndex].Value);
            GridViewRow gvRow = gvAFCDetails.Rows[e.NewEditIndex];
            ((DropDownList)gvRow.FindControl("EITddlControl")).Text = ((HiddenField)gvRow.FindControl("EIThdfControl")).Value;
            ((DropDownList)gvRow.FindControl("EITddlDataType")).Text = ((HiddenField)gvRow.FindControl("EIThdfDataType")).Value;
            ((DropDownList)gvRow.FindControl("EITddlMandatory")).Text = ((HiddenField)gvRow.FindControl("EIThdfStatus")).Value;
            gvAFCDetails.FooterRow.Visible = false;
            hdfAFCDetails.Value = "2";

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void gvAFCDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvAFCDetails.Rows[e.RowIndex];
            long serial = Convert.ToInt32(gvAFCDetails.DataKeys[e.RowIndex].Value);
            DataRow dr = ActivityDetails.Rows.Find(serial);
            dr.BeginEdit();
            SetActivityDetails(dr, gvRow, "EIT");
            dr.EndEdit();
            gvAFCDetails.EditIndex = -1;
            BindGrid();
            hdfAFCDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void gvAFCDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvAFCDetails.Rows[e.RowIndex];
            long serial = Convert.ToInt32(gvAFCDetails.DataKeys[e.RowIndex].Value);
            ActivityDetails.Rows.Find(serial).Delete();
            BindGrid();
            gvAFCDetails.EditIndex = -1;
            hdfAFCDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }

    }
    protected void GvPrice_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = GvPrice.FooterRow;
                DataRow dr = PriceDetails.NewRow();
                //TextBox txtLabel = gvRow.FindControl("FTtxtLabel") as TextBox;
                DropDownList ddlPaxType = (DropDownList)gvRow.FindControl("FTddlPaxType");
                string paxType = ddlPaxType.SelectedItem.Value;
                if (paxType == "0")
                {
                    throw new Exception("Please select PaxType");
                }
                if (PriceDetails.Select(string.Format("Label='{0}'", paxType)).Length < 0)
                {
                    Utility.StartupScript(this.Page, "alert('Label Name Cannot Be Duplicate !')", "");
                    Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
                    throw new Exception("Label Name Cannot Be Duplicate !");
                }

                DataTable dtDeleted = PriceDetails.GetChanges(DataRowState.Deleted);
                long deletedSerial = 0;
                if (dtDeleted != null)
                {
                    dtDeleted.RejectChanges();
                    deletedSerial = Convert.ToInt32(dtDeleted.Compute("MAX(priceId)", ""));
                }
                long serial = Convert.ToInt32(PriceDetails.Compute("MAX(priceId)", ""));
                serial = (serial >= deletedSerial) ? serial + 1 : deletedSerial + 1;
                dr["priceId"] = serial;
                SetPriceDetails(dr, gvRow, "FT");
                PriceDetails.Rows.Add(dr);
                BindGridPrice();
                Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
            Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void GvPrice_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            GvPrice.EditIndex = e.NewEditIndex;

            BindGridPrice();
            long serial = Convert.ToInt32(GvPrice.DataKeys[e.NewEditIndex].Value);
            GridViewRow gvRow = GvPrice.Rows[e.NewEditIndex];
            ((DropDownList)gvRow.FindControl("EITddlPaxType")).SelectedValue = ((HiddenField)gvRow.FindControl("EIThdfPaxType")).Value;
            hdfPrice.Value = "2";
            Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void GvPrice_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = GvPrice.Rows[e.RowIndex];
            long serial = Convert.ToInt32(GvPrice.DataKeys[e.RowIndex].Value);
            DropDownList ddlPaxType = (DropDownList)gvRow.FindControl("EITddlPaxType");
            string paxType = ddlPaxType.SelectedItem.Value;
            if (paxType == "0")
            {
                throw new Exception("Please select PaxType");
            }
            if (PriceDetails.Select(string.Format("Label='{0}'", paxType)).Length < 0)
            {
                Utility.StartupScript(this.Page, "alert('Label Name Cannot Be Duplicate !')", "");
                Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
                throw new Exception("Label Name Cannot Be Duplicate !");
            }
            DataRow dr = PriceDetails.Rows.Find(serial);
            dr.BeginEdit();
            SetPriceDetails(dr, gvRow, "EIT");
            dr.EndEdit();
            GvPrice.EditIndex = -1;
            BindGridPrice();
            hdfPrice.Value = "0";
            Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void GvPrice_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow gvRow = GvPrice.Rows[e.RowIndex];
            long serial = Convert.ToInt32(GvPrice.DataKeys[e.RowIndex].Value);
            PriceDetails.Rows.Find(serial).Delete();
            BindGridPrice();
            GvPrice.EditIndex = -1;
            hdfPrice.Value = "0";
            Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void gvAFCDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvAFCDetails.EditIndex = -1;
            BindGrid();
            hdfAFCDetails.Value = "0";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void GvPrice_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            GvPrice.EditIndex = -1;
            BindGridPrice();
            hdfPrice.Value = "0";
            Utility.StartupScript(this.Page, "ShowTab('packageTab2');", "showTab");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
    protected void ddlDays_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDays.SelectedValue == "1")
        {
            lblItinerary2.Visible = false;
            txtItinerary2.Visible = false;
        }
        else
        {
            lblItinerary2.Visible = true;
            txtItinerary2.Visible = true;
        }

    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity page " + ex.Message, "0");
        }
    }
public string removeDuplicateDates(string dates)
    {
        string uniqueDates = string.Empty;
        string formattedDates =  dates.Replace("#&#","|");
        int duplicateCount = 0;
        string[] newDatesString = formattedDates.Split('|');
        List<string> found = new List<string>();

        foreach (string date in newDatesString)
        {
            if (found.Contains(date))
            {
                duplicateCount++;
            }
            else
            {
                found.Add(date);
            }
        }
        if (found.Count > 0)
        {
            foreach (string newDate in found)
            {
                if (!string.IsNullOrEmpty(newDate))
                {
                    if (uniqueDates.Length > 0)
                    {
                        uniqueDates = uniqueDates + newDate + "#&#";
                    }
                    else
                    {
                        uniqueDates = newDate + "#&#";
                    }
                }
            }
        }
        return uniqueDates;
        
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try {
            lblCurrency.Text = AgentMaster.GetAgentCurrency(Convert.ToInt32(ddlAgent.SelectedValue));
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Activity Master page " + ex.Message, "0");
        }
    }
}
