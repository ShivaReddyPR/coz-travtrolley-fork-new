﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Net;
using CT.AccountingEngine;
using System.Collections.Specialized;
using CCA.Util;
using System.Threading;
using Encryption.AES;
using SafexPay;
using System.Linq;

public partial class PaymentConfirmationBySegments :CT.Core.ParentPage// System.Web.UI.Page
{
    protected SearchResult onwardResult;
    protected SearchResult returnResult;
    int resultId=0;
    string sessionId;
    bool isLCC = false;
    int agencyId = -1;
    protected bool isOnwardBookingSuccess = false, isReturnBookingSuccess = false;
    protected List<Ticket> ticketList;
    protected FlightItinerary onwardFlightItinerary;
    protected FlightItinerary returnFlightItinerary;
    protected string IPAddr = string.Empty;
    protected BookingDetail booking;
    protected string inBaggage = "", outBaggage = "";
    protected Airline airline;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected AgentMaster agency;
    protected decimal charges = 0, totalBookingAmount = 0;
    protected int emailCounter = 1;//Checking for Agent details display in Email body
    protected AutoResetEvent[] BookingEvents = new AutoResetEvent[2];
    protected decimal onOriginalFare = 0m, onCurrentFare = 0m, retOriginalFare = 0m, retCurrentFare = 0m;
    protected LocationMaster location = new LocationMaster();
    //private BookingResponse bookingResponse = new BookingResponse();
    protected Dictionary<string, object> SessionValues = new Dictionary<string, object>();
    protected const string ONWARD_RESULT = "OnwardResult";
    protected const string RETURN_RESULT = "ReturnResult";    
    protected const string ONWARD_PRICE_CHANGED_TBO = "OnwardIsPriceChangedTBO";
    protected const string RETURN_PRICE_CHANGED_TBO = "ReturnIsPriceChangedTBO";
    protected const string ONWARD_FLIGHT_ITINERARY = "OnwardFlightItinerary";
    protected const string RETURN_FLIGHT_ITINERARY = "ReturnFlightItinerary";
    protected const string ONWARD_BOOKING_RESPONSE = "OnwardBookingResponse";
    protected const string RETURN_BOOKING_RESPONSE = "ReturnBookingResponse";
    protected const string ROUTING_TRIP_ID = "RoutingTripId";
    protected const string ONWARD_SSR_PRICE_CHANGE = "OnwardSSRPriceChange";
    protected const string RETURN_SSR_PRICE_CHANGE = "ReturnSSRPriceChange";
    string routingTripId = string.Empty;
    protected bool sendmail;
    protected string[] pageParams = null, ccParams = null;
    protected Dictionary<string, string> PGResponseValues = new Dictionary<string, string>();


    protected void Page_Load(object sender, EventArgs e)
    {
        GetSession();

        pageParams = GenericStatic.GetSetPageParams("PageParams", "", "get").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        ccParams = GenericStatic.GetSetPageParams("CCParams", "", "get").Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries);

        if (pageParams != null && (Session["sessionId"] != null && Session["PaxPageValues"] != null) || ccParams != null) 
        {
            if (Settings.LoginInfo.IsCorporate == "Y")
                btnHold.Text = "Send For Approval";
            try
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agencyId = Settings.LoginInfo.OnBehalfAgentID;
                    agency = new AgentMaster(agencyId);
                    lblAgentBalance.Text = agency.AgentCurrency + " " + agency.UpdateBalance(0).ToString("N" + agency.DecimalValue);
                    location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                }
                else
                {
                    agencyId = Settings.LoginInfo.AgentId;
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                    lblAgentBalance.Text = Settings.LoginInfo.Currency + " " + agency.UpdateBalance(0).ToString("N" + agency.DecimalValue);
                }
                hdnAgencyName.Value = agency.Name;
                hdnAgencyPhone.Value = agency.Phone1;

                //Disable when payment mode is credit limit
                if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit || agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                {
                    ddlPaymentType.Disabled = true;                    
                }
                else if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Card)
                {
                    ddlPaymentType.Disabled = false;
                }
                

                //FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;
                onwardFlightItinerary = SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary;
                sendmail = onwardFlightItinerary.Sendmail;
                returnFlightItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
                sessionId = Session["sessionId"].ToString();
                //if (Request.QueryString["id"] != null)
                //{
                //    resultId = Convert.ToInt32(Request["id"]);
                //}
                //else if (Session["ResultIndex"] != null)
                //{
                //    resultId = Convert.ToInt32(Session["ResultIndex"]);
                //}
                if (Basket.FlightBookingSession.ContainsKey(sessionId))
                {
                    //result = Basket.FlightBookingSession[sessionId].Result[resultId - 1];
                    onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                    returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                    SearchResult[] results = new SearchResult[1];
                    results[0] = onwardResult;
                    SearchResult[] searchResults = new SearchResult[1];
                    searchResults[0] = returnResult;
                    if (!IsPostBack)//Added by Lokesh on 8Feb,2018 to Avoid RoundTrip Databinding
                    {
                        if (SessionValues.ContainsKey(ONWARD_SSR_PRICE_CHANGE) && !string.IsNullOrEmpty(SessionValues[ONWARD_SSR_PRICE_CHANGE].ToString()) && !SessionValues.ContainsKey(RETURN_SSR_PRICE_CHANGE))
                        {
                            string price = SessionValues[ONWARD_SSR_PRICE_CHANGE].ToString();
                            onOriginalFare = Convert.ToDecimal(price.Split('-')[0]);
                            onCurrentFare = Convert.ToDecimal(price.Split('-')[1]);
                        }
                        else if (SessionValues.ContainsKey(RETURN_SSR_PRICE_CHANGE) && !string.IsNullOrEmpty(SessionValues[RETURN_SSR_PRICE_CHANGE].ToString()))
                        {
                            string price = SessionValues[RETURN_SSR_PRICE_CHANGE].ToString();
                            retOriginalFare = Convert.ToDecimal(price.Split('-')[0]);
                            retCurrentFare = Convert.ToDecimal(price.Split('-')[0]);
                        }
                        else if (SessionValues.ContainsKey(ONWARD_SSR_PRICE_CHANGE) && !string.IsNullOrEmpty(SessionValues[ONWARD_SSR_PRICE_CHANGE].ToString()) && SessionValues.ContainsKey(RETURN_SSR_PRICE_CHANGE) && !string.IsNullOrEmpty(SessionValues[RETURN_SSR_PRICE_CHANGE].ToString()))
                        {
                            string price = SessionValues[ONWARD_SSR_PRICE_CHANGE].ToString();
                            onOriginalFare = Convert.ToDecimal(price.Split('-')[0]);
                            onCurrentFare = Convert.ToDecimal(price.Split('-')[1]);
                            price = SessionValues[RETURN_SSR_PRICE_CHANGE].ToString();
                            retOriginalFare = Convert.ToDecimal(price.Split('-')[0]);
                            retCurrentFare = Convert.ToDecimal(price.Split('-')[0]);
                        }
                        //Update the price (shiva 26Mar2018)
                        //Update the price only intially and not on page refresh since we are summing up values and in order to achieve
                        //that AirLocatorCode is checked for null. If updated already AirLocatorCode='G9' will be assigned
                        if (onwardResult.ResultBookingSource == BookingSource.AirArabia && string.IsNullOrEmpty(onwardFlightItinerary.AirLocatorCode))
                        {
                            CalculateBaggageFare(SearchType.OneWay);
                            onwardFlightItinerary.AirLocatorCode = "G9";
                        }
                        if (returnResult != null && returnResult.ResultBookingSource == BookingSource.AirArabia && string.IsNullOrEmpty(returnFlightItinerary.AirLocatorCode))
                        {
                            CalculateBaggageFare(SearchType.Return);
                            returnFlightItinerary.AirLocatorCode = "G9";
                        }

                        List<FlightInfo> Segments = new List<FlightInfo>(onwardResult.Flights[0]);
                        if (onwardResult.Flights.Length > 1)
                            Segments.AddRange(onwardResult.Flights[1]);

                        //dlFlight.DataSource = results;
                        dlFlight.DataSource = Segments;
                        dlFlight.DataBind();
                        if (returnResult != null)
                        {
                            List<FlightInfo> ReturnSegments = new List<FlightInfo>(returnResult.Flights[0]);
                            if (returnResult.Flights.Length > 1)
                                Segments.AddRange(returnResult.Flights[1]);
                            //dlFlightReturn.DataSource = searchResults;
                            dlFlightReturn.DataSource = ReturnSegments;
                            dlFlightReturn.DataBind();
                        }
                    }
                    dlPaxPrice.DataSource = results;
                    dlPaxPrice.DataBind();
                    if (returnResult != null)
                    {
                        dlPaxPriceReturn.DataSource = searchResults;
                        dlPaxPriceReturn.DataBind();
                    }
                }
                if (!IsPostBack)//Added by Lokesh on 8Feb,2018 to Avoid RoundTrip Databinding
                {
                    if (string.IsNullOrEmpty(routingTripId) && string.IsNullOrEmpty(onwardFlightItinerary.RoutingTripId))
                    {
                        routingTripId = GenerateSuperPNR();
                        onwardFlightItinerary.RoutingTripId = routingTripId;
                        if (returnFlightItinerary != null)
                        {
                            returnFlightItinerary.RoutingTripId = routingTripId;
                        }
                        SaveInSession(ONWARD_FLIGHT_ITINERARY, onwardFlightItinerary);
                        SaveInSession(RETURN_FLIGHT_ITINERARY, returnFlightItinerary);                        
                    }
                    
                    dlPassengers.DataSource = onwardFlightItinerary.Passenger;
                    dlPassengers.DataBind();

                    //Added By Somasekhar on 02/07/2018  -- for load dynamically Payment Gateways. Avoid loading PG Sources for credit limit
                    if (agency.PaymentMode != CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                        LoadPaymentGateways();

                    MultiView1.ActiveViewChanged += MultiView1_ActiveViewChanged;
                }
                //Assign payment mode for itineraries 
                if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                {
                    onwardFlightItinerary.PaymentMode = ModeOfPayment.CreditLimit;
                    if (returnFlightItinerary != null && returnFlightItinerary.Passenger != null)
                        returnFlightItinerary.PaymentMode = ModeOfPayment.CreditLimit;
                }
                
                IPAddr = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]; ;
                onwardFlightItinerary.BookUserIP = IPAddr;
                if(returnFlightItinerary!=null)
                    returnFlightItinerary.BookUserIP = IPAddr;
                if (!IsPostBack && onwardResult != null)
                {
                    //Get G9 Baggage fare quote
                    //Code added by Lokesh on 10/07/2016.
                    lblHold.Visible = btnHold.Visible = !onwardResult.IsLCC && (onwardResult.ResultBookingSource == BookingSource.TBOAir || onwardResult.ResultBookingSource == BookingSource.UAPI) && returnResult == null;
                }

                if (ccParams != null && ccParams.Length > 0)
                {
                    foreach (string val in ccParams)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            string key = val.Split('=')[0];
                            string value = val.Split('=')[1];
                            PGResponseValues.Add(key, value);
                        }
                    }
                }

                #region PaymentGateway Response Checking
                if (Request.QueryString["ErrorPG"] != null)//Error returned from Payment Gateway
                {
                    lblError.Text = Request.QueryString["ErrorPG"];
                    MultiView1.ActiveViewIndex = 1;
                }
                else if (PGResponseValues.ContainsKey("encResp"))//Payment returned from CCAvenue
                {
                    string orderId = string.Empty;
                    string paymentId = string.Empty;
                    string statusMessage = string.Empty;

                    string workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(PGResponseValues["encResp"].Replace(" ", "+"), workingKey);
                    Audit.Add(EventType.Book, Severity.Low, 1, "(Flight)CCAvenue response data : " + encResponse, Request["REMOTE_ADDR"]);
                    NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');
                    foreach (string seg in segments)
                    {
                        string[] parts = seg.Split('=');
                        if (parts.Length > 0)
                        {
                            string Key = parts[0].Trim();
                            string Value = parts[1].Trim();
                            Params.Add(Key, Value);
                        }
                    }

                    int paymentInformationId = 0;

                    if (Session["PaymentInformationId"] != null)
                    {
                        paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
                    }

                    if (Params["order_status"] == "Success")
                    {
                        orderId = Params["order_id"];
                        //paymentId = Params["payment_id"];
                        paymentId = Params["tracking_id"];

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = onwardFlightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                            creditCard.UpdatePaymentDetails();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        BookingResponse response = new BookingResponse();
                        statusMessage = "Successful";
                        ddlPaymentType.SelectedIndex = 1;//CreditCard
                        if (!onwardFlightItinerary.IsLCC)
                        {
                            response = (BookingResponse)SessionValues[ONWARD_BOOKING_RESPONSE];
                        }
                        else
                        {
                            try
                            {
                                response = MakeBooking(onwardFlightItinerary);
                                
                            }
                            catch
                            {
                                throw;
                            }
                        }

                        int flightId = FlightItinerary.GetFlightId(response.PNR);
                        onwardFlightItinerary = new FlightItinerary(flightId);

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = onwardFlightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        if (response.Status == BookingResponseStatus.Successful)
                        {
                            GenerateTicket(response, SearchType.OneWay);

                            if (returnFlightItinerary != null)
                            {
                                if (Ticket.GetTicketList(onwardFlightItinerary.FlightId).Count > 0)
                                {
                                    returnFlightItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
                                    BookingResponse returnBookingResponse = MakeBooking(returnFlightItinerary);
                                    SaveInSession(RETURN_BOOKING_RESPONSE, returnBookingResponse);
                                    if (returnBookingResponse.Status == BookingResponseStatus.Successful)
                                    {
                                        GenerateTicket(returnBookingResponse, SearchType.Return);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Params["failure_message"] != null && Params["failure_message"].Length > 0)
                        {
                            statusMessage = Params["failure_message"];
                        }
                        else if (Params["status_message"] != null && Params["status_message"].Length > 0)
                        {
                            statusMessage = Params["status_message"];
                        }
                        else
                        {
                            statusMessage = "Payment declined due to invalid details or due to technical error.";
                        }

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = onwardFlightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        
                        Session["PaxPageValues"] = null;
                        MultiView1.ActiveViewIndex = 1;
                        lblError.Text = statusMessage;
                    }
                }
                //Added by Somasekhar on 27/06/2018 -- for SafexPay Payment Gateway
                #region SafexPay 
                else if (PGResponseValues.ContainsKey("txn_response"))//Payment returned from SafexPay
                {
                    string orderId = string.Empty;
                    string paymentId = string.Empty;
                    string statusMessage = string.Empty;
                    SafexPayService safexPay = new SafexPayService();
                    CryptoClass aes = new CryptoClass();
                    #region response parameters

                    try
                    {
                        aes.enc_txn_response = (!String.IsNullOrEmpty(PGResponseValues["txn_response"])) ? PGResponseValues["txn_response"].Replace(" ", "+") : string.Empty;
                        aes.enc_pg_details = (!String.IsNullOrEmpty(PGResponseValues["pg_details"])) ? PGResponseValues["pg_details"].Replace(" ", "+") : string.Empty;
                        aes.enc_fraud_details = (!String.IsNullOrEmpty(PGResponseValues["fraud_details"])) ? PGResponseValues["fraud_details"].Replace(" ", "+") : string.Empty;
                        aes.enc_other_details = (!String.IsNullOrEmpty(PGResponseValues["other_details"])) ? PGResponseValues["other_details"].Replace(" ", "+") : string.Empty;


                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.StackTrace, Request["REMOTE_ADDR"]);
                    }
                    #endregion
                    #region Txn_Resonse
                    try
                    {

                        string txn_response = aes.decrypt(aes.enc_txn_response, safexPay.txnMerchantKey);
                        string[] txn_response_arr = txn_response.Split('|');

                        safexPay.ag_id = (!String.IsNullOrEmpty(txn_response_arr[0])) ? txn_response_arr[0] : string.Empty;
                        safexPay.me_id = (!String.IsNullOrEmpty(txn_response_arr[1])) ? txn_response_arr[1] : string.Empty;
                        safexPay.order_no = (!String.IsNullOrEmpty(txn_response_arr[2])) ? txn_response_arr[2] : string.Empty;
                        safexPay.Amount = (!String.IsNullOrEmpty(txn_response_arr[3])) ? txn_response_arr[3] : string.Empty;
                        safexPay.Country = (!String.IsNullOrEmpty(txn_response_arr[4])) ? txn_response_arr[4] : string.Empty;
                        safexPay.Currency = (!String.IsNullOrEmpty(txn_response_arr[5])) ? txn_response_arr[5] : string.Empty;
                        safexPay.txn_date = (!String.IsNullOrEmpty(txn_response_arr[6])) ? txn_response_arr[6] : string.Empty;
                        safexPay.txn_time = (!String.IsNullOrEmpty(txn_response_arr[7])) ? txn_response_arr[7] : string.Empty;
                        safexPay.ag_ref = (!String.IsNullOrEmpty(txn_response_arr[8])) ? txn_response_arr[8] : string.Empty;
                        safexPay.pg_ref = (!String.IsNullOrEmpty(txn_response_arr[9])) ? txn_response_arr[9] : string.Empty;
                        safexPay.status = (!String.IsNullOrEmpty(txn_response_arr[10])) ? txn_response_arr[10] : string.Empty;
                        safexPay.res_code = (!String.IsNullOrEmpty(txn_response_arr[11])) ? txn_response_arr[11] : string.Empty;
                        safexPay.res_message = (!String.IsNullOrEmpty(txn_response_arr[12])) ? txn_response_arr[12] : string.Empty;

                        Audit.Add(EventType.Book, Severity.Low, 1, "(Flight)SafexPay response data : " + txn_response, Request["REMOTE_ADDR"]);

                        try
                        {
                            //response Log in decrypt format
                            safexPay.WriteResonse(txn_response);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    #endregion

                    int paymentInformationId = 0;

                    if (Session["PaymentInformationId"] != null)
                    {
                        paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
                    }

                    if (safexPay.status == "Successful")
                    {
                        orderId = safexPay.order_no;
                        //paymentId = Params["payment_id"];
                        paymentId = safexPay.pg_ref;
                        statusMessage = "Successful";

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = onwardFlightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                            creditCard.UpdatePaymentDetails();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        BookingResponse response = new BookingResponse();
                        ddlPaymentType.SelectedIndex = 1;//CreditCard
                        if (!onwardFlightItinerary.IsLCC)
                        {
                            response = (BookingResponse)SessionValues[ONWARD_BOOKING_RESPONSE];                            
                        }
                        else
                        {
                            try
                            {
                                response = MakeBooking(onwardFlightItinerary);                                
                            }
                            catch
                            {
                                throw;
                            }
                        }

                        int flightId = FlightItinerary.GetFlightId(response.PNR);
                        onwardFlightItinerary = new FlightItinerary(flightId);

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = onwardFlightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        if (response.Status == BookingResponseStatus.Successful)
                        {
                            GenerateTicket(response, SearchType.OneWay);

                            if (returnFlightItinerary != null)
                            {
                                if (Ticket.GetTicketList(onwardFlightItinerary.FlightId).Count > 0)
                                {
                                    returnFlightItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
                                    BookingResponse returnBookingResponse = MakeBooking(returnFlightItinerary);
                                    SaveInSession(RETURN_BOOKING_RESPONSE, returnBookingResponse);
                                    if (returnBookingResponse.Status == BookingResponseStatus.Successful)                                    
                                        GenerateTicket(returnBookingResponse, SearchType.Return);                                    
                                }
                            }
                        }
                    }
                    else
                    {
                        statusMessage = safexPay.status;

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = onwardFlightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        lblError.Text = safexPay.status == "Failed" ? safexPay.status : "Payment declined due to technical error or due to invalid details";
                        MultiView1.ActiveViewIndex = 1;                        
                        Session["PaxPageValues"] = null;
                    }
                }
                #endregion 
                #endregion
            }
            catch (Exception ex)
            {
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErrFlight");
                MultiView1.ActiveViewIndex = 1;
                lblError.Text = ex.Message;
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
                Session["FZBaggageDetails"] = null;
                Session["ResultIndex"] = null;//Clear the Result Index session
                Session["BookingResponse"] = null;
                Session["FlightItinerary"] = null;
                RemoveFromSession(ROUTING_TRIP_ID);
                Session["PaxPageValues"] = null;
            }
        }
        else
        {
            Session["PaxPageValues"] = null;
            RemoveFromSession(ROUTING_TRIP_ID);
            Response.Redirect("HotelSearch.aspx?source=Flight", true);
        }       
    }

    private void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErrFlightTest1");
    }

    private void CalculateBaggageFare(SearchType type)
    {
        try
        {
            FlightItinerary itinerary = (type == SearchType.OneWay ? SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary : SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary);
            SearchResult result = (type == SearchType.OneWay ? SessionValues[ONWARD_RESULT] as SearchResult : SessionValues[RETURN_RESULT] as SearchResult);
            SearchRequest request = Session["FlightRequest"] as SearchRequest;
            if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                //Hold booking is not allowed for G9

                string[] RPH = new string[result.Flights.Length];
                List<string> rph = new List<string>();
                for (int i = 0; i < result.Flights.Length; i++)
                {
                    for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        rph.Add(result.Flights[i][j].UapiDepartureTime.ToString() + "|" + result.Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber);
                    }
                }
                RPH = rph.ToArray();
                int arraySize = 0;

                arraySize = (itinerary.Passenger.Length - request.InfantCount) * itinerary.Segments.Length;

                List<string> paxBaggages = new List<string>();
                // int count = 0;

                for (int i = 0; i < (itinerary.Passenger.Length - request.InfantCount); i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        string travelerRPH = "";
                        if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                        {
                            travelerRPH = "A" + (i + 1);
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            travelerRPH = "C" + (i + 1);
                        }

                        string bagCode = itinerary.Passenger[i].BaggageType;
                        string mealCode = itinerary.Passenger[i].MealType;
                        string seatCode = itinerary.Passenger[i].SeatInfo;
                        for (int k = 0; k < result.Flights.Length; k++)
                        {
                            for (int j = 0; j < result.Flights[k].Length; j++)
                            {
                                if (!string.IsNullOrEmpty(bagCode))
                                    paxBaggages.Add(bagCode.Split(',')[k].Trim() + "-BAG" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                if (!string.IsNullOrEmpty(mealCode))
                                    paxBaggages.Add(mealCode.Split(',')[k].Trim() + "-MEAL" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                if (!string.IsNullOrEmpty(seatCode))
                                {
                                    string seatDetail = (seatCode.Split('|').Length > k) ? seatCode.Split('|')[k].Trim() : seatCode;
                                    if (seatDetail.Contains(result.Flights[k][j].Origin.AirportCode + "-" + result.Flights[k][j].Destination.AirportCode))
                                        paxBaggages.Add(seatDetail.Split('(')[0] + "-SEAT" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                }
                            }

                        }
                    }
                }

                CT.BookingEngine.GDS.AirArabia aaObj = new CT.BookingEngine.GDS.AirArabia();
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    aaObj.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                    aaObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                    aaObj.Code = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                    aaObj.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    aaObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    aaObj.UserName = Settings.LoginInfo.AgentSourceCredentials["G9"].UserID;
                    aaObj.Password = Settings.LoginInfo.AgentSourceCredentials["G9"].Password;
                    aaObj.Code = Settings.LoginInfo.AgentSourceCredentials["G9"].HAP;
                    aaObj.AgentCurrency = Settings.LoginInfo.Currency;
                    aaObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                }

                decimal fares = itinerary.Passenger.ToList().Sum(p => p.Price.Markup + p.Price.PublishedFare + p.Price.AsvAmount + p.Price.BaggageCharge + p.Price.MealCharge + p.Price.SeatPrice - p.Price.Discount + p.Price.Tax + p.Price.HandlingFeeAmount);
                if (type == SearchType.OneWay)
                    onOriginalFare = onOriginalFare > 0 ? onOriginalFare : fares;
                else
                    retOriginalFare = retOriginalFare > 0 ? retOriginalFare : fares;

                decimal vatAmount = 0m;

                if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                {
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (location.ID));
                        pax.Price.OutputVATAmount = gstAmount;
                        pax.Price.GSTDetailList = gstTaxList;
                        vatAmount += gstAmount;
                    }
                }
                else if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                {
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (Settings.LoginInfo.LocationID));
                        pax.Price.OutputVATAmount = gstAmount;
                        pax.Price.GSTDetailList = gstTaxList;
                        vatAmount += gstAmount;
                    }
                }
                else
                {
                    if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                    {
                        OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                        decimal markupValue = 0m, handlingFee = 0m, baggageCharge = 0m;
                        markupValue = itinerary.Passenger.ToList().Sum(p => agency.AgentType == (int)AgentType.Agent ? p.Price.Markup + p.Price.AsvAmount : p.Price.Markup);
                        handlingFee = itinerary.Passenger.ToList().Sum(p => p.Price.HandlingFeeAmount);
                        baggageCharge = itinerary.Passenger.ToList().Sum(p => p.Price.BaggageCharge);

                        //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                        vatAmount = outVat.CalculateVatAmount((decimal)(type == SearchType.OneWay ? onOriginalFare : retOriginalFare) - handlingFee, markupValue, agency.DecimalValue);
                    }
                }

                if (type == SearchType.OneWay)
                    onOriginalFare += vatAmount;
                else
                    retOriginalFare += vatAmount;

                CT.BookingEngine.GDS.AirArabia.SessionData data = (CT.BookingEngine.GDS.AirArabia.SessionData)Basket.BookingSession[sessionId][result.Airline + "-" + result.ResultKey];
                string fareXml = "";
                CookieContainer cookie = new CookieContainer();
                aaObj.AppUserId = Settings.LoginInfo.UserID.ToString();
                aaObj.SessionID = Session["sessionId"].ToString();
                //Modified by Lokesh on 04-April-2018
                //Applicable to Only G9 Source
                //If the customer is travelling from India(Pax will have state code and TaxRegNo)
                Dictionary<string, decimal> PaxBagFares = new Dictionary<string, decimal>();

                string[] baggageFare =  aaObj.GetBaggageFare(ref result, request, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref paxBaggages, itinerary.Passenger[0].GSTStateCode, itinerary.Passenger[0].GSTTaxRegNo, ref PaxBagFares);
                MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                mse.SettingsLoginInfo = Settings.LoginInfo;
                PriceAccounts price = result.Price;
                decimal inputvat = 0m;
                mse.CalculateInputVATForFlight(request.Segments[0].Origin, request.Segments[0].Destination, result.ResultBookingSource, ref price, (decimal)result.TotalFare, ref inputvat);
                result.TotalFare += (double)inputvat;
                result.Tax += (double)inputvat;
                int totalpax = request.AdultCount + request.ChildCount + request.InfantCount;


                int count = 0;
                decimal markup = 0, discount = 0;
                //Calculate Markup for the new fare
                for (count = 0; count < result.FareBreakdown.Length; count++)
                {
                    Fare fare = result.FareBreakdown[count];
                    if (count == 0)
                    {
                        fare.TotalFare += (double)(inputvat);
                    }
                    CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(result, count, agencyId, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");
                    markup += (tempPrice.Markup) * fare.PassengerCount;
                    discount += (tempPrice.Discount * fare.PassengerCount);
                    //Assign result price markup
                    result.Price.Markup = tempPrice.Markup;
                    result.Price.MarkupType = tempPrice.MarkupType;
                    result.Price.MarkupValue = tempPrice.MarkupValue;

                    result.Price.Markup = tempPrice.Markup;
                    fare.AgentMarkup = (tempPrice.Markup * fare.PassengerCount);
                    fare.AgentDiscount = (tempPrice.Discount * fare.PassengerCount);
                }
                markup = Math.Round(markup, agency.DecimalValue);
                discount = Math.Round(discount, agency.DecimalValue);

                result.TotalFare += (double)markup;
                result.TotalFare -= (double)discount;

                data.SelectedResultInfo = fareXml;
                // TodO ziya: update itineray according to last price quote
                //Update the price (shiva 26Mar2018)

                //Modified by Lokesh on 5-April-2018
                //Now Update the Price For all Pax Types
                //Since we are reading the AirArabiaBaggagePriceResponse
                if (itinerary != null && itinerary.Passenger.Length > 0)
                {
                    decimal roe = 1;
                    if (aaObj.ExchangeRates.ContainsKey("AED"))
                    {
                        roe = aaObj.ExchangeRates["AED"];
                    }
                    itinerary.AirLocatorCode = "G9";
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (result.FareBreakdown[i] != null && result.FareBreakdown[i].SupplierFare > 0)
                        {
                            itinerary.Passenger[i].Price.SupplierPrice = (decimal)result.FareBreakdown[i].SupplierFare / result.FareBreakdown[i].PassengerCount;
                            itinerary.Passenger[i].Price.PublishedFare = (decimal)result.FareBreakdown[i].BaseFare / result.FareBreakdown[i].PassengerCount;
                            itinerary.Passenger[i].Price.Tax = (decimal)result.FareBreakdown[i].Tax / result.FareBreakdown[i].PassengerCount;
                            //Update Recalculated Markup written by Shiva
                            itinerary.Passenger[i].Price.Markup = result.FareBreakdown[i].AgentMarkup / result.FareBreakdown[i].PassengerCount;
                            //ReAssign Handling Fee calculated
                            itinerary.Passenger[i].Price.HandlingFeeAmount = result.FareBreakdown[i].HandlingFee / result.FareBreakdown[i].PassengerCount;
                            itinerary.Passenger[i].Price.Discount = result.FareBreakdown[i].AgentDiscount / result.FareBreakdown[i].PassengerCount;
                        }
                        //Update the baggage fares assigned from BaggagePriceQuote. In order to show the popup compare previous fare and updated fare
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            string travelerRPH = "";
                            if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                            {
                                travelerRPH = "A" + (i + 1);
                            }
                            else if (itinerary.Passenger[i].Type == PassengerType.Child)
                            {
                                travelerRPH = "C" + (i + 1);
                            }

                            itinerary.Passenger[i].Price.BaggageCharge = (PaxBagFares != null && PaxBagFares.ContainsKey("BAG~" + travelerRPH) ? PaxBagFares["BAG~" + travelerRPH] : 0);
                            itinerary.Passenger[i].Price.MealCharge = (PaxBagFares != null && PaxBagFares.ContainsKey("MEAL~" + travelerRPH) ? PaxBagFares["MEAL~" + travelerRPH] : 0);
                            itinerary.Passenger[i].Price.SeatPrice = (PaxBagFares != null && PaxBagFares.ContainsKey("SEAT~" + travelerRPH) ? PaxBagFares["SEAT~" + travelerRPH] : 0);
                            //Convert into Agent currency
                        }
                    }
                    
                    fares = itinerary.Passenger.ToList().Sum(p => p.Price.Markup + p.Price.PublishedFare + p.Price.AsvAmount + p.Price.BaggageCharge + p.Price.MealCharge + p.Price.SeatPrice - p.Price.Discount + p.Price.Tax + p.Price.HandlingFeeAmount);
                    if (type == SearchType.OneWay)
                        onCurrentFare = fares;
                    else
                        retCurrentFare = fares;

                    vatAmount = 0;

                    if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (location.ID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (Settings.LoginInfo.LocationID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else
                    {
                        if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                        {
                            OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                            decimal markupValue = 0m, handlingFee = 0m, baggageCharge = 0m, mealCharge = 0m, seatCharge = 0m;
                            markupValue = itinerary.Passenger.ToList().Sum(p => agency.AgentType == (int)AgentType.Agent ? p.Price.Markup + p.Price.AsvAmount : p.Price.Markup);
                            handlingFee = itinerary.Passenger.ToList().Sum(p => p.Price.HandlingFeeAmount);
                            baggageCharge = itinerary.Passenger.ToList().Sum(p => p.Price.BaggageCharge);
                            mealCharge = itinerary.Passenger.ToList().Sum(p => p.Price.MealCharge);
                            seatCharge = itinerary.Passenger.ToList().Sum(p => p.Price.SeatPrice);
                            //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                            vatAmount = outVat.CalculateVatAmount((decimal)result.TotalFare + baggageCharge +mealCharge+seatCharge- handlingFee, markupValue, agency.DecimalValue);
                        }
                    }

                    if (type == SearchType.OneWay)
                        onCurrentFare += vatAmount;
                    else
                        retCurrentFare += vatAmount;


                    if (type == SearchType.OneWay)
                        SaveInSession(ONWARD_FLIGHT_ITINERARY, itinerary);
                    else
                        SaveInSession(RETURN_FLIGHT_ITINERARY, itinerary);
                }

                onOriginalFare = Math.Round(onOriginalFare, agency.DecimalValue);
                onCurrentFare = Math.Round(onCurrentFare, agency.DecimalValue);
                retOriginalFare = Math.Round(retOriginalFare, agency.DecimalValue);
                retCurrentFare = Math.Round(retCurrentFare, agency.DecimalValue);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void dlFlight_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                FlightInfo obj = e.Item.DataItem as FlightInfo;

                if (obj != null)
                {
                    Image imgFlightCarrierCode = e.Item.FindControl("imgFlightCarrierCode") as Image;
                    Label lblFlightOriginCode = e.Item.FindControl("lblFlightOriginCode") as Label;
                    Label lblFlightDestinationCode = e.Item.FindControl("lblFlightDestinationCode") as Label;
                    Label lblFlightDuration = e.Item.FindControl("lblFlightDuration") as Label;
                    Label lblOnFlightWayType = e.Item.FindControl("lblOnFlightWayType") as Label;

                    Label lblFlightCode = e.Item.FindControl("lblFlightCode") as Label;
                    Label lblFlightDepartureDate = e.Item.FindControl("lblFlightDepartureDate") as Label;
                    Label lblFlightArrivalDate = e.Item.FindControl("lblFlightArrivalDate") as Label;
                    Label lblOnDefaultBaggage = e.Item.FindControl("lblOnDefaultBaggage") as Label;


                    string rootFolder = Airline.logoDirectory + "/";
                    Airline departingAirline = new Airline();
                    departingAirline.Load(obj.Airline);
                    imgFlightCarrierCode.ImageUrl = rootFolder + departingAirline.LogoFile;
                    lblFlightOriginCode.Text = obj.Origin.CityName;
                    lblFlightDestinationCode.Text = obj.Destination.CityName;
                    lblFlightDuration.Text = obj.Duration.Hours + "hrs " + obj.Duration.Minutes + "mins";

                    switch (obj.Stops)
                    {
                        case 0:
                            lblOnFlightWayType.Text = "Non Stop";
                            break;
                        case 1:
                            lblOnFlightWayType.Text = "Single Stop";
                            break;
                        case 2:
                            lblOnFlightWayType.Text = "Two Stops";
                            break;
                        default:
                            lblOnFlightWayType.Text = "Two+ Stops";
                            break;
                    }
                    lblOnFlightWayType.Text += "<br/>" + obj.SegmentFareType;
                    lblFlightCode.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.FlightNumber;
                    lblFlightDepartureDate.Text = obj.DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                    lblFlightArrivalDate.Text = obj.ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                    lblOnDefaultBaggage.Text = !string.IsNullOrEmpty(obj.DefaultBaggage)? obj.DefaultBaggage:"As Per Airline Policy";

                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void dlPaxPrice_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblBaseFare = e.Item.FindControl("lblBaseFare") as Label;                
                Label lblMarkupTotal = e.Item.FindControl("lblMarkupTotal") as Label;                
                Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
                Label lblBaggagePrice = e.Item.FindControl("lblBaggagePrice") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                Label lblTax = e.Item.FindControl("lblTax") as Label;
                Label lblVATAmount = e.Item.FindControl("lblVATAmount") as Label;
                Label lblDiscount = e.Item.FindControl("lblDiscount") as Label;
                Label lblDiscountAmount = e.Item.FindControl("lblDiscountAmount") as Label;
                Label lblTotalPubFare = e.Item.FindControl("lblTotalPubFare") as Label;
                Label lblK3Tax = e.Item.FindControl("lblK3Tax") as Label;

                SearchResult result = e.Item.DataItem as SearchResult;
                FlightItinerary itinerary = SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary;

                Label lblMeal = e.Item.FindControl("lblMeal") as Label;
                Label lblMealPrice = e.Item.FindControl("lblMealPrice") as Label;

                Label lblSeat = e.Item.FindControl("lblSeat") as Label;
                Label lblSeatPrice = e.Item.FindControl("lblSeatPrice") as Label;

                if (itinerary != null)
                {
                    int adults = 0;
                    decimal baggageCharge = 0, discount = 0, asvAmount = 0, mealCharge = 0, SeatCharge = 0;
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        if (pax.Type == PassengerType.Adult)
                        {
                            adults += 1;
                            
                        }
                        if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                        {
                            baggageCharge += pax.Price.BaggageCharge;
                            mealCharge += pax.Price.MealCharge;
                        }
                        pax.Price.DecimalPoint = agency.DecimalValue;
                        discount += pax.Price.Discount;
                        asvAmount += pax.Price.AsvAmount;
                        SeatCharge += pax.Price.SeatPrice;
                    }
                    
                    baggageCharge = Math.Round(baggageCharge, agency.DecimalValue);
                    mealCharge = Math.Round(mealCharge, agency.DecimalValue);
                    SeatCharge = Math.Round(SeatCharge, agency.DecimalValue);

                    if (itinerary.FlightBookingSource == BookingSource.UAPI || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.AirArabia)
                    {
                        lblMeal.Visible = true;
                        lblMealPrice.Visible = true;
                        lblMealPrice.Text = agency.AgentCurrency + " " + mealCharge.ToString("N" + agency.DecimalValue);
                    }

                    lblSeat.Visible = lblSeatPrice.Visible = true;
                    lblSeatPrice.Text = agency.AgentCurrency + " " + SeatCharge.ToString("N" + agency.DecimalValue);

                    discount = Math.Round(discount, agency.DecimalValue);
                    
                    if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                    {
                        lblBaggage.Visible = true;
                        lblBaggagePrice.Visible = true;
                        lblBaggagePrice.Text = agency.AgentCurrency + " " + baggageCharge.ToString("N" + agency.DecimalValue);
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare) + baggageCharge + mealCharge + SeatCharge).ToString("N" + agency.DecimalValue);//lblTotalPrice.Text;
                        hdnBookingAmount.Value = (Convert.ToDecimal(result.TotalFare) + baggageCharge + mealCharge + SeatCharge).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {                        
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare)).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = (Convert.ToDecimal(result.TotalFare)).ToString("N" + agency.DecimalValue);
                    }
                    decimal vatAmount = 0m;
                    decimal markUp =0m;
                    if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (location.ID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else if(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (Settings.LoginInfo.LocationID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else
                    {
                        if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                        {
                            OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                            decimal markup = 0m, handlingFee = 0m, paxtotalFare = 0m;
                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                              
                                paxtotalFare += Convert.ToDecimal(pax.Price.BaggageCharge.ToString("N" + agency.DecimalValue)) +Convert.ToDecimal(pax.Price.PublishedFare.ToString("N" + agency.DecimalValue)) - Convert.ToDecimal(pax.Price.Discount.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(pax.Price.Tax.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(pax.Price.OtherCharges.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(pax.Price.MealCharge.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(pax.Price.SeatPrice.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(pax.Price.Markup.ToString("N" + agency.DecimalValue));
                                markup += Convert.ToDecimal(pax.Price.Markup.ToString("N" + agency.DecimalValue));
                                handlingFee += Convert.ToDecimal(pax.Price.HandlingFeeAmount.ToString("N" + agency.DecimalValue));
                                if (agency.AgentType == (int)AgentType.Agent)
                                {
                                    markup += Convert.ToDecimal(pax.Price.AsvAmount.ToString("N" + agency.DecimalValue));
                                    paxtotalFare += Convert.ToDecimal(pax.Price.AsvAmount.ToString("N" + agency.DecimalValue));
                                }
                            }
                           
                            //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                            vatAmount = outVat.CalculateVatAmount((decimal)paxtotalFare, markup, agency.DecimalValue);

                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                pax.Price.OutputVATAmount = Convert.ToDecimal(vatAmount.ToString("N" + agency.DecimalValue)) / itinerary.Passenger.Length;
                            }
                        }
                    }
                    
                    lblVATAmount.Text = agency.AgentCurrency + " " + vatAmount.ToString("N" + agency.DecimalValue);
                    totalBookingAmount = Convert.ToDecimal(result.TotalFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(baggageCharge.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(mealCharge.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(SeatCharge.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(vatAmount.ToString("N" + agency.DecimalValue));
                    
                    decimal tboMarkUp = 0, tboDiscount = 0, tboBaseFare = 0, tboTax = 0;
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            if (result.FareBreakdown[i] != null)
                            {
                                tboMarkUp += Convert.ToDecimal(result.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                                tboDiscount += Convert.ToDecimal(result.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue));
                                tboBaseFare += Convert.ToDecimal(result.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(result.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                                tboTax += Convert.ToDecimal(result.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
                            }
                        }
                        tboTax += Convert.ToDecimal(tboMarkUp) + Convert.ToDecimal(result.Price.OtherCharges) + Convert.ToDecimal(result.Price.AdditionalTxnFee) + Convert.ToDecimal(result.Price.SServiceFee) + Convert.ToDecimal(result.Price.TransactionFee);
                    }
                    else
                    {
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            if (result.FareBreakdown[i] != null)
                            {
                                markUp += Math.Round(result.FareBreakdown[i].AgentMarkup,agency.DecimalValue); // *result.FareBreakdown[i].PassengerCount;                            
                            }
                        }
                    }
                    
                    double Tax = result.Tax + (double)markUp;
                    baggageCharge = Convert.ToDecimal(baggageCharge.ToString("N" + agency.DecimalValue));
                    vatAmount = Convert.ToDecimal(vatAmount.ToString("N" + agency.DecimalValue));
                    asvAmount = Convert.ToDecimal(asvAmount.ToString("N" + agency.DecimalValue));
                    double k3Tax = Convert.ToDouble(result.Price.K3Tax.ToString("N"+agency.DecimalValue));
                    lblK3Tax.Text = agency.AgentCurrency + " " + k3Tax.ToString("N"+agency.DecimalValue);
                    Tax = Convert.ToDouble(Tax.ToString("N" + agency.DecimalValue));
                    result.BaseFare = Math.Round(result.BaseFare, agency.DecimalValue);
                    if (itinerary.FlightBookingSource != BookingSource.TBOAir)
                    {
                        lblTax.Text = agency.AgentCurrency + " " + (Tax-k3Tax + (double)(itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                        lblBaseFare.Text = agency.AgentCurrency + " " + ((decimal)result.BaseFare + (decimal)(itinerary.Passenger[0].Price.AsvElement == "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);

                    }
                    else
                    {
                        //For TBOAir BaseFare contains HandlingFee with OtherCharges so take Tax only
                        lblTax.Text = agency.AgentCurrency + " " + (tboTax- (decimal)k3Tax + (itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                        lblBaseFare.Text = agency.AgentCurrency + " " + (tboBaseFare + (itinerary.Passenger[0].Price.AsvElement == "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);

                    }

                    decimal dcAsvAmount = Settings.LoginInfo.IsOnBehalfOfAgent ? asvAmount : 0;

                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        //lblTotalPrice.Text = lblTotal.Text;
                        //Add Page Level Markup (Addl Markup/asvAmount) to the Total displayed
                        totalBookingAmount = ((Convert.ToDecimal(tboBaseFare) + tboTax + baggageCharge + vatAmount + dcAsvAmount - discount));
                        lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + asvAmount - discount)).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        totalBookingAmount = ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + mealCharge + SeatCharge + vatAmount + dcAsvAmount - discount));
                        //lblTotal.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare) + asvAmount).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + asvAmount - discount)).ToString("N" + agency.DecimalValue);

                    }

                    if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                    {
                        lblMarkupTotal.Text = agency.AgentCurrency + " " + Math.Ceiling(Convert.ToDecimal(tboBaseFare) + (decimal)tboTax+ baggageCharge +  + asvAmount + vatAmount - discount).ToString("N" + agency.DecimalValue);
                        lblTotalPubFare.Text = agency.AgentCurrency + " " + Math.Ceiling(Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge+ asvAmount + vatAmount).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        lblMarkupTotal.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.BaseFare) + (decimal)Tax + asvAmount + baggageCharge + mealCharge + SeatCharge + vatAmount- discount).ToString("N" + agency.DecimalValue);
                        lblTotalPubFare.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.BaseFare) + (decimal)Tax + asvAmount + baggageCharge + mealCharge + SeatCharge + vatAmount + discount).ToString("N" + agency.DecimalValue);
                    }
                    
                    if(discount > 0)
                    {
                        lblDiscount.Visible = true;
                        lblDiscountAmount.Visible = true;
                        lblDiscountAmount.Text = "(-)" + agency.AgentCurrency + " " + discount.ToString("N" + agency.DecimalValue);
                    }                     
                }
                //lblTxnAmount.Text = agency.AgentCurrency + " " + totalBookingAmount.ToString("N" + agency.DecimalValue);//Added by Suresh for Adding Vat amount to Total amount in Oneway in Combination.
                lblTxnAmount.Text = lblMarkupTotal.Text;
                hdnBookingAmount.Value = totalBookingAmount.ToString("N" + agency.DecimalValue);
                //Added by lokesh on 4Jan2019
                if (itinerary != null && itinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    FlightItinerary returnItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
                    if(returnItinerary != null && returnItinerary.FlightBookingSource != BookingSource.TBOAir)
                    {
                        totalBookingAmount = Math.Ceiling(totalBookingAmount);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
        finally
        {
            //hdnBookingAmount.Value = lblTxnAmount.Text.Split(' ')[1];
        }
    }

    private BookingResponse MakeBooking(FlightItinerary itinerary)
    {
        if (ddlPaymentType.SelectedIndex == 1 || Session["ResultIndex"] != null)
        {
            itinerary.PaymentMode = ModeOfPayment.CreditCard;
            itinerary.CcPayment = new CCPayment(); // in order to avoid error while saving Itinerary
        }
        
        SearchRequest request = null;
        if (Session["FlightRequest"] != null)
        {
            request = Session["FlightRequest"] as SearchRequest;
        }
        

        string TripId = DateTime.Now.ToString("yyyyMMddHmmssFFF");//Format is Year:9999 Month:12 Date:31 Hour:23 Minutes:59 Second:59 Millisecond:999
        if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
        {            
            itinerary.TripId = TripId;//Set the tripId for Corporate Booking
        }
        
        MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
        mse.SettingsLoginInfo = Settings.LoginInfo;        
        
        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agencyId = Settings.LoginInfo.OnBehalfAgentID;
        }
        else
        {
            agencyId = Settings.LoginInfo.AgentId;
        }
        itinerary.AirLocatorCode = null;//Clear G9 value from saving into DB assigned for checking baggage price update
        BookingResponse bookingResponse = new BookingResponse();
        AgentMaster agent = new AgentMaster(agencyId);        
        decimal currentAgentBalance = agent.UpdateBalance(0);
        lblAgentBalance.Text = agency.AgentCurrency + " " + currentAgentBalance.ToString("N" + agency.DecimalValue);
        if (ddlPaymentType.Value == "Card" || currentAgentBalance >= Convert.ToDecimal(lblTxnAmount.Text.Replace(agency.AgentCurrency, "").Trim()) || hdfAction.Value == "Hold")
        {
            //If TBO Air and non-LCC then allow only ticketing - added by shiva 19-aug-2016
            //if ((itinerary.FlightBookingSource == BookingSource.TBOAir && !itinerary.IsLCC))// not LCC and TBO Air GDS
            //{
            //    if (hdfAction.Value == "Ticket" || itinerary.IsLCC)
            //    {
            //        if ((itinerary.FlightBookingSource == BookingSource.TBOAir && itinerary.IsDomestic && onwardResult.FareType == "Normal") || itinerary.IsLCC)
            //        {
            //            bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ticketed, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            //        }
            //        else
            //        {
            //            bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ready, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            //        }
            //    }
            //    else if (hdfAction.Value == "Hold" || !itinerary.IsLCC)
            //    {
            //        bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Hold, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            //    }
            //}
            //else // LCC
            //{
            //    bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ticketed, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            //}

            if (itinerary.IsLCC)
            {
                bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ticketed, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            }
            else
            {
                bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ready, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            }

            if (bookingResponse.Status == BookingResponseStatus.Assignseatfailed && !string.IsNullOrEmpty(bookingResponse.Error))
            {
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "SeatAssignAlert('" + bookingResponse.Error + "')", "seatsalertret");
                bookingResponse.Status = BookingResponseStatus.Successful;
            }

            if (bookingResponse.Status != BookingResponseStatus.Successful)
            {
                //In case of booking failure clear session 
                if (!string.IsNullOrEmpty(itinerary.TripId) || !string.IsNullOrEmpty(itinerary.RoutingTripId))
                {
                    mse.ClearSession();// If selected itinerary is failing, then stopping optional itinerary bookings for corporate
                }

                lblError.Text = bookingResponse.Error;
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErrFlightTest1");
                MultiView1.ActiveViewIndex = 1;

            }
            else if (bookingResponse.Status == BookingResponseStatus.Successful)
            {
                if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
                {
                    Dictionary<FlightItinerary, BookingResponse> BookingResponses = new Dictionary<FlightItinerary, BookingResponse>();
                    BookingResponses.Add(itinerary, bookingResponse);
                    Session["CorpBookingResponses"] = BookingResponses;
                    Session["TripId"] = itinerary.TripId;

                }
                //If Ticketing is not allowed then clear the results from the Basket
                //which we normally do for normal bookings. 
                if (!agent.AgentTicketingAllowed && Settings.LoginInfo.IsCorporate != "Y")
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "ticket not allowed", Request["REMOTE_ADDR"]);
                    mse.ClearSession();
                }
            }
        }
        else
        {
            lblBalanceError.Text = "Insufficient Credit Balance ! Please contact Admin";
        }
        return bookingResponse;
    }
  
    protected void imgBtnPayment_Click(object sender, EventArgs e)
    {
        try
        {
            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
            returnResult = SessionValues[RETURN_RESULT] as SearchResult;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////// Agent Currency, Rate of Exchange & Fare Check up ///////////////////////////////////////
            decimal rateOfExchange = 0; decimal roe = 0;
            FlightItinerary itinerary = SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary;
            

            decimal baseFare = 0, tax = 0, resultBaseFare = 0, resultTax = 0;
            if (onwardResult.ResultBookingSource != BookingSource.TBOAir)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[onwardResult.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                else
                {
                    rateOfExchange = Settings.LoginInfo.AgentExchangeRates[onwardResult.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                StaticData sd = new StaticData();
                sd.BaseCurrency = agency.AgentCurrency;
                roe = sd.CurrencyROE[onwardResult.Price.SupplierCurrency];

                SearchRequest request = null;
                int paxCount = 0;
                if (Session["FlightRequest"] != null)
                {
                    request = Session["FlightRequest"] as SearchRequest;
                    paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                }

                if (onwardResult.Price.MarkupType == "P")
                {
                    if (agency.AgentAirMarkupType == "BF")
                    {
                        baseFare = ((decimal)onwardResult.Price.PublishedFare) * roe;
                        baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)onwardResult.Price.PublishedFare) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                    }
                    else if (agency.AgentAirMarkupType == "TX")
                    {
                        baseFare = ((decimal)onwardResult.Price.Tax / roe) * roe;
                        baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)onwardResult.Price.Tax / rateOfExchange) * roe;
                        resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                    }
                    else
                    {
                        baseFare = ((decimal)onwardResult.Price.SupplierPrice) * roe;
                        baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)onwardResult.Price.SupplierPrice) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                    }
                }
                else
                {
                    baseFare = (onwardResult.Price.SupplierPrice * roe) + (onwardResult.Price.MarkupValue * paxCount);
                    resultBaseFare = (onwardResult.Price.SupplierPrice * rateOfExchange) + (onwardResult.Price.MarkupValue * paxCount);
                }
            }
            else
            {
                
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (agency.AgentCurrency == onwardResult.Currency)
            {
                if (rateOfExchange == roe && Math.Round(resultBaseFare, agency.DecimalValue) >= Math.Round(baseFare, agency.DecimalValue))
                {
                    MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
                    mse.SettingsLoginInfo = Settings.LoginInfo;

                    if (onwardResult.IsLCC)
                    {
                        /**************************************************
                        *               Credit Card Payment
                        * *************************************************/
                        if (ddlPaymentType.Value == "Card")
                        {
                            //Added by Somasekhar on 27/06/2018 -- For SafexPay
                            itinerary.PaymentMode = ModeOfPayment.CreditCard;
                            Session["FlightItinerary"] = itinerary;
                            string pgName = rblAgentPG.SelectedItem.Text;
                            GenericStatic.GetSetPageParams("PageParams", pageParams[0] + "," + pgName, "set");
                            Response.Redirect("PaymentProcessing.aspx", false);
                            //if (pgName == "SafexPay")
                            //{
                            //    Response.Redirect("PaymentProcessing.aspx?paymentGateway=SafexPay&id=" + Request.QueryString["id"], true);
                            //}
                            ////=====================================================
                            //else if (pgName == "CCAvenue")
                            //{
                            //    Response.Redirect("PaymentProcessing.aspx?paymentGateway=CCAvenue&id=" + Request.QueryString["id"], true);
                            //}
                        }
                    }
                    else//Update payment mode for GDS too
                    {
                        if (ddlPaymentType.Value == "Card")
                        {
                            itinerary.PaymentMode = ModeOfPayment.CreditCard;
                            Session["FlightItinerary"] = itinerary;
                        }
                    }

                    //If both onward and return flight itineraries are special round trips
                    //Then Assign all the return itinerary components and form a single itinerary
                    //Do no allow the return itinerary for booking and ticketing
                    //code added by lokesh on 11April2019
                    if(onwardFlightItinerary != null && returnFlightItinerary != null && onwardFlightItinerary.IsSpecialRoundTrip && returnFlightItinerary.IsSpecialRoundTrip)
                    {
                        BuildSpecialRoundTripItinerary();
                    }
                    if (!onwardResult.IsLCC || (onwardResult.IsLCC && ddlPaymentType.Value != "Card"))
                    {
                        BookingResponse onwardBookingResponse = MakeBooking(onwardFlightItinerary); //sai
                        BookingResponse returnBookingResponse = new BookingResponse();

                        sessionId = Session["sessionId"].ToString();

                        if (onwardBookingResponse.Status == BookingResponseStatus.Successful)
                        {
                            Session["SeatError"] = onwardBookingResponse.Error;
                            SaveInSession(ONWARD_BOOKING_RESPONSE, onwardBookingResponse);
                            //For GDS Booking after reservation we need to proceed for payment
                            if (!onwardResult.IsLCC)
                            {
                                /*************************************************
                                 *          Credit Card Payment
                                 * *************************************************/
                                if (ddlPaymentType.Value == "Card")
                                {
                                    //Added by Somasekhar on 27/06/2018 -- For SafexPay
                                    string pgName = rblAgentPG.SelectedItem.Text;
                                    GenericStatic.GetSetPageParams("PageParams", pageParams[0] + "," + pgName, "set");
                                    Response.Redirect("PaymentProcessing.aspx", false);
                                }
                            }
                            if (ddlPaymentType.Value != "Card")
                            {
                                GenerateTicket(onwardBookingResponse, SearchType.OneWay);
                                RemoveFromSession(ROUTING_TRIP_ID);
                                if (returnFlightItinerary != null)//For One-Way there will be no return return Flight Itinerary object
                                {
                                    if (Ticket.GetTicketList(onwardFlightItinerary.FlightId).Count > 0 && (!onwardFlightItinerary.IsSpecialRoundTrip) && (!returnFlightItinerary.IsSpecialRoundTrip))
                                    {
                                        try
                                        {
                                            returnFlightItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
                                            returnBookingResponse = MakeBooking(returnFlightItinerary);

                                            if (returnBookingResponse.Status == BookingResponseStatus.Successful)
                                            {
                                                Session["SeatError"] = Session["SeatError"] != null && !string.IsNullOrEmpty((string)Session["SeatError"]) ?
                                                    (string)Session["SeatError"] + "@" + returnBookingResponse.Error : returnBookingResponse.Error;
                                                SaveInSession(RETURN_BOOKING_RESPONSE, returnBookingResponse);
                                                GenerateTicket(returnBookingResponse, SearchType.Return);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new Exception("Failed to book for " + returnFlightItinerary.Segments[0].Origin.AirportCode + "-" + returnFlightItinerary.Segments[returnFlightItinerary.Segments.Length - 1].Destination.AirportCode + ". Reason: " + ex.Message, ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
                }
            }
            else
            {
                lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
            }
        }
        catch (System.Threading.ThreadAbortException ex)
        {            
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, " imgBtnPayment_Click"+ex.ToString(), Request["REMOTE_ADDR"]);
            
        }
        catch (Exception ex)
        {
            
            MultiView1.ActiveViewIndex = 1;
            lblError.Text = ex.Message;            
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
            RemoveFromSession(ROUTING_TRIP_ID);
            Response.Redirect("ErrorPage.aspx?Err=" + lblError.Text, false);
        }
    }

    private void GenerateTicket(BookingResponse bookingResponse, SearchType searchType)
    {
        MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
        mse.SettingsLoginInfo = Settings.LoginInfo;      

        Dictionary<string, string> ticketData = new Dictionary<string, string>();

        ticketData.Add("corporateCode", "1");
        ticketData.Add("tourCode", "1");
        ticketData.Add("endorsement", "1");
        ticketData.Add("remarks", "1");

        //result = Basket.FlightBookingSession[sessionId].Result[resultId - 1];  //the given key was not present in dictionary when UAPI //sai
        //Added by Ravi. To continue on ward booking if return booking fails in the Normal Return combination

        TicketingResponse ticketingResponse;
        if (onwardFlightItinerary.IsLCC && searchType == SearchType.OneWay)
        {
            ticketingResponse = new TicketingResponse();
            ticketingResponse.Status = TicketingResponseStatus.Successful;

            isOnwardBookingSuccess = (!string.IsNullOrEmpty(onwardFlightItinerary.PNR) ? true : false);            
        }
        else if (returnFlightItinerary != null && returnFlightItinerary.IsLCC && searchType == SearchType.Return)
        {
            ticketingResponse = new TicketingResponse();
            ticketingResponse.Status = TicketingResponseStatus.Successful;
            isReturnBookingSuccess = (!string.IsNullOrEmpty(returnFlightItinerary.PNR) ? true : false);
        }
        else
        {
            UserMaster member = new UserMaster(Settings.LoginInfo.UserID);
            ticketingResponse = mse.Ticket(bookingResponse.PNR, member, ticketData, IPAddr);

            //added sai for Sending Email when status is in Ready or Inprogress 
            FlightItinerary itineraryDB = new FlightItinerary(FlightItinerary.GetFlightId(bookingResponse.PNR));
            BookingDetail booking = new BookingDetail(itineraryDB.BookingId);

            if (booking.Status == BookingStatus.Ready || booking.Status == BookingStatus.InProgress)
            {
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
               
                string message = "Booking is in " + booking.Status.ToString().ToUpper() + " state for the following PNR:(" + onwardFlightItinerary.PNR + ").Reason:" + ticketingResponse.Message + "\nAgencyId is " + onwardFlightItinerary.AgencyId.ToString() + "\n Logged in Member is " + member.FirstName + "(" + (int)member.ID + ", " + member.LoginName + ")\n";
                
                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Ticketing Failed. There was a problem communicating to the Reservation System.", message);

                Response.Redirect("ErrorPage.aspx?Err=" + ticketingResponse.Message, false);
            }
        }

        if (ticketingResponse.Status == TicketingResponseStatus.Successful)
        {
            int flightId = FlightItinerary.GetProductIdByBookingId(bookingResponse.BookingId, ProductType.Flight); //Changed Avoding getting flight throught pnr(Flight Inventory)
            if (searchType == SearchType.OneWay)
            {
                onwardFlightItinerary = new FlightItinerary(flightId);
                SaveInSession(ONWARD_FLIGHT_ITINERARY, onwardFlightItinerary);
                isOnwardBookingSuccess = true;
            }
            else
            {
                returnFlightItinerary = new FlightItinerary(flightId);
                SaveInSession(RETURN_FLIGHT_ITINERARY, returnFlightItinerary);
                isReturnBookingSuccess = true;
            }
            
            try
            {
                if (searchType == SearchType.OneWay)
                {
                    //Check whether tickets are saved or not
                    List<Ticket> tickets = new List<Ticket>();
                    tickets = Ticket.GetTicketList(onwardFlightItinerary.FlightId);
                    if (tickets != null && tickets.Count > 0)
                    {
                        Invoice invoice = new Invoice();
                        int invoiceNumber = Invoice.isInvoiceGenerated(tickets[0].TicketId, ProductType.Flight);

                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(onwardFlightItinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                            if (invoiceNumber > 0)
                            {
                                invoice.Load(invoiceNumber);
                                invoice.Status = InvoiceStatus.Paid;
                                invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                invoice.UpdateInvoice();
                            }
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, onwardFlightItinerary.CreatedBy, "(Invoice)Invoice not created due to Tickets not found for PNR: " + onwardFlightItinerary.PNR, Request.ServerVariables["REMOTE_ADDR"]);
                        //TODO: Send email to Cozmo
                        Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed to generate due to no tickets for PNR " + onwardFlightItinerary.PNR, "<p>Dear Team,<br/>Invoice generation failed due to Tickets were not saved or found for PNR: " + onwardFlightItinerary.PNR + "</p><br/><p>Regards,<br/>Cozmo B2B Auto Emailer</p>");
                    }
                }
                else
                {
                    //Check whether tickets are saved or not
                    List<Ticket> tickets = new List<Ticket>();
                    tickets = Ticket.GetTicketList(returnFlightItinerary.FlightId);
                    if (tickets != null && tickets.Count > 0)
                    {
                        Invoice invoice = new Invoice();
                        int invoiceNumber = Invoice.isInvoiceGenerated(tickets[0].TicketId, ProductType.Flight);

                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(returnFlightItinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                            if (invoiceNumber > 0)
                            {
                                invoice.Load(invoiceNumber);
                                invoice.Status = InvoiceStatus.Paid;
                                invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                invoice.UpdateInvoice();
                            }
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, returnFlightItinerary.CreatedBy, "(Invoice)Invoice not created due to Tickets not found for PNR: " + returnFlightItinerary.PNR, Request.ServerVariables["REMOTE_ADDR"]);
                        //TODO: Send email to Cozmo
                        Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed to generate due to no tickets for PNR " + returnFlightItinerary.PNR, "<p>Dear Team,<br/>Invoice generation failed due to Tickets were not saved or found for PNR: " + returnFlightItinerary.PNR + "</p><br/><p>Regards,<br/>Cozmo B2B Auto Emailer</p>");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(Invoice)"+ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                //TODO: Send email to Cozmo
                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed to generate due to error for PNR " + onwardFlightItinerary.PNR, "<p>Dear Team,<br/>Invoice generation failed due to Error while generation of Invoice for PNR: " + onwardFlightItinerary.PNR + ".<br/> Error details:" + ex.ToString() + " </p><br/><p>Regards,<br/>Cozmo B2B Auto Emailer</p>");
            }
            AgentMaster agent = new AgentMaster(agencyId);
            agent.CreatedBy = Settings.LoginInfo.UserID;

            //If Payment Mode = On Account deduct agent balance, if Credit Card do not deduct balance
            //if (Session["ResultIndex"] == null)
            if (onwardFlightItinerary.PaymentMode != ModeOfPayment.CreditCard && searchType == SearchType.OneWay)
            {
                decimal currentAgentBalance = 0;
               
                if (onwardFlightItinerary.FlightBookingSource != BookingSource.TBOAir)
                {
                    currentAgentBalance = agent.UpdateBalance(0);
                }
                else //For TBO we are ceiling the Total Amount and need to deduct the same from Agent balance
                {
                    //For all other sources we are deducting the balance from Add_Ticket procedure passenger wise i.e
                    //if 6 pax are there six times we are deducting the balance with pax wise total amount. So in case
                    //of TBO Air if we do pax wise ceiling TotalAmount will be higher than the original amount.

                    decimal ticketAmount = 0;
                    foreach (FlightPassenger pax in onwardFlightItinerary.Passenger)
                    {
                        ticketAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.OtherCharges + pax.Price.BaggageCharge + 
                            pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee + pax.Price.OutputVATAmount + 
                            pax.Price.HandlingFeeAmount - pax.Price.Discount + pax.Price.MealCharge + pax.Price.SeatPrice +
                            (Settings.LoginInfo.IsOnBehalfOfAgent ? pax.Price.AsvAmount : 0);
                    }
                    if (onwardFlightItinerary.PaymentMode == ModeOfPayment.Credit)
                    {
                        currentAgentBalance = agent.UpdateBalance(-Math.Ceiling(ticketAmount));
                    }
                    else
                    {
                        currentAgentBalance = agent.UpdateBalance(0);
                    }
                }
                //if (Session["BookingAgencyID"] == null)
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    Settings.LoginInfo.AgentBalance = currentAgentBalance;
                    Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                    lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
                }
            }

            if (returnFlightItinerary != null && returnFlightItinerary.PaymentMode != ModeOfPayment.CreditCard && searchType == SearchType.Return)
            {
                decimal currentAgentBalance = 0;
                
                if (returnFlightItinerary.FlightBookingSource != BookingSource.TBOAir)
                {
                    currentAgentBalance = agent.UpdateBalance(0);
                }
                else //For TBO we are ceiling the Total Amount and need to deduct the same from Agent balance
                {
                    //For all other sources we are deducting the balance from Add_Ticket procedure passenger wise i.e
                    //if 6 pax are there six times we are deducting the balance with pax wise total amount. So in case
                    //of TBO Air if we do pax wise ceiling TotalAmount will be higher than the original amount.

                    decimal ticketAmount = 0;
                    foreach (FlightPassenger pax in returnFlightItinerary.Passenger)
                    {
                        ticketAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.OtherCharges + pax.Price.BaggageCharge + 
                            pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee + pax.Price.OutputVATAmount + 
                            pax.Price.HandlingFeeAmount - pax.Price.Discount + pax.Price.MealCharge + pax.Price.SeatPrice +
                            (Settings.LoginInfo.IsOnBehalfOfAgent ? pax.Price.AsvAmount : 0);
                    }
                    if (returnFlightItinerary.PaymentMode == ModeOfPayment.Credit)
                    {
                        currentAgentBalance = agent.UpdateBalance(-Math.Ceiling(ticketAmount));
                    }
                    else
                    {
                        currentAgentBalance = agent.UpdateBalance(0);
                    }
                }
                //if (Session["BookingAgencyID"] == null)
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    Settings.LoginInfo.AgentBalance = currentAgentBalance;
                    Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                    lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
                }
            }
            Session["FZBaggageDetails"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;

            //display ticket
            //if b2b2b agent then save ticketed booking todo ziyad
            if (onwardFlightItinerary.FlightId > 0 && searchType == SearchType.OneWay)
                SendFlightTicketEmail(onwardFlightItinerary, searchType);

            if (returnFlightItinerary != null && returnFlightItinerary.FlightId > 0 && searchType == SearchType.Return)
                SendFlightTicketEmail(returnFlightItinerary, searchType);

            if ((searchType == SearchType.OneWay && returnFlightItinerary == null) || searchType == SearchType.Return || (returnFlightItinerary != null && returnFlightItinerary.IsSpecialRoundTrip))
            {
                RemoveFromSession(ONWARD_BOOKING_RESPONSE);
                RemoveFromSession(RETURN_BOOKING_RESPONSE);
                RemoveFromSession(ONWARD_PRICE_CHANGED_TBO);
                RemoveFromSession(RETURN_PRICE_CHANGED_TBO);
                RemoveFromSession(ONWARD_RESULT);
                RemoveFromSession(RETURN_RESULT);
                RemoveFromSession(ROUTING_TRIP_ID);
                mse.ClearSession();//Clear the Booking Session
                Session["PaxPageValues"] = null;//Clear the Dictionary Session
                Session["PageParams"] = null;
                Session["CCParams"] = null;

                Response.Redirect("ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId, false);
            }
        }
        else
        {
            Session["FZBaggageDetails"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;
            RemoveFromSession(ONWARD_BOOKING_RESPONSE);
            RemoveFromSession(RETURN_BOOKING_RESPONSE);
            RemoveFromSession(ONWARD_PRICE_CHANGED_TBO);
            RemoveFromSession(RETURN_PRICE_CHANGED_TBO);
            RemoveFromSession(ONWARD_RESULT);
            RemoveFromSession(RETURN_RESULT);
            RemoveFromSession(ROUTING_TRIP_ID);
            mse.ClearSession();//Clear the Booking Session
            Session["PaxPageValues"] = null;//Clear the Dictionary Session
            Session["PageParams"] = null;
            Session["CCParams"] = null;
            Response.Redirect("ErrorPage.aspx?Err=" + ticketingResponse.Message, false);

            if (ticketingResponse.Status == TicketingResponseStatus.NotSaved)
            {
                if (returnFlightItinerary != null && returnFlightItinerary.PNR != null)
                {
                    FailedBooking fb = FailedBooking.Load(returnFlightItinerary.PNR);
                    int id = fb.FailedBookingId;
                    MultiView1.ActiveViewIndex = 1;
                    lblError.Text = "There may be an error occured while booking. However PNR is created (" + returnFlightItinerary.PNR + ").<br /> Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance." + ticketingResponse.Message;
                    //if b2b2b agent then save Hold Booking todo ziyad
                    //Redirect to agent ticket page
                }
                else
                {
                    FailedBooking fb = FailedBooking.Load(onwardFlightItinerary.PNR);
                    int id = fb.FailedBookingId;
                    MultiView1.ActiveViewIndex = 1;
                    lblError.Text = "There may be an error occured while booking. However PNR is created (" + onwardFlightItinerary.PNR + ").<br /> Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance." + ticketingResponse.Message;
                    //if b2b2b agent then save Hold Booking todo ziyad
                    //Redirect to agent ticket page
                }
                Response.Redirect("ErrorPage.aspx?Err=" + lblError.Text, false);
            }
            else
            {   
                RemoveFromSession(ROUTING_TRIP_ID);
                mse.ClearSession();//Clear the Booking Session
                Session["PaxPageValues"] = null;//Clear the Dictionary Session
                Session["PageParams"] = null;
                Session["CCParams"] = null;
                MultiView1.ActiveViewIndex = 1;

                if (returnFlightItinerary != null && returnFlightItinerary.PNR != null)
                {
                    if (!string.IsNullOrEmpty(ticketingResponse.Message))
                    {
                        lblError.Text = "There may be an error occured while booking. However PNR is created (" + returnFlightItinerary.PNR + "). Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance." + ticketingResponse.Message;
                    }
                    else
                    {
                        lblError.Text = "There may be an error occured while booking. However PNR is created (" + returnFlightItinerary.PNR + "). Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance.";
                    }
                }
                else if (onwardFlightItinerary != null && onwardFlightItinerary.PNR != null)
                {
                    if (!string.IsNullOrEmpty(ticketingResponse.Message))
                    {
                        lblError.Text = "There may be an error occured while booking. However PNR is created (" + onwardFlightItinerary.PNR + "). Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance." + ticketingResponse.Message;
                    }
                    else
                    {
                        lblError.Text = "There may be an error occured while booking. However PNR is created (" + onwardFlightItinerary.PNR + "). Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance.";
                    }
                }
                else
                {
                    lblError.Text = "There may be an error occured while booking. Please contact Cozmo for more details " + ticketingResponse.Message;
                }

                Response.Redirect("ErrorPage.aspx?Err=" + lblError.Text, false);
            }
        }
    }

    protected void btnHold_Click(object sender, EventArgs e)
    {
        BookingResponse bookingResponse = new BookingResponse();
        try
        {
                     
            
            FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////// Agent Currency, Rate of Exchange & Fare Check up ///////////////////////////////////////
            decimal rateOfExchange = 0; decimal roe = 0;            

            decimal baseFare = 0, tax = 0, resultBaseFare = 0, resultTax = 0;
            if (onwardResult.ResultBookingSource != BookingSource.TBOAir)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[onwardResult.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                else
                {
                    rateOfExchange = Settings.LoginInfo.AgentExchangeRates[onwardResult.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                StaticData sd = new StaticData();
                sd.BaseCurrency = agency.AgentCurrency;
                roe = sd.CurrencyROE[onwardResult.Price.SupplierCurrency];


                //for (int i = 0; i < result.FareBreakdown.Length; i++)
                //{
                //    baseFare += ((((decimal)result.FareBreakdown[i].BaseFare / rateOfExchange) * roe) * result.FareBreakdown[i].PassengerCount);
                //    tax += ((((decimal)result.FareBreakdown[i].Tax / rateOfExchange) * roe) * result.FareBreakdown[i].PassengerCount);
                //    resultBaseFare += (decimal)result.FareBreakdown[i].BaseFare;
                //    resultTax += result.FareBreakdown[i].Tax;
                //}
                SearchRequest request = null;
                int paxCount = 0;
                if (Session["FlightRequest"] != null)
                {
                    request = Session["FlightRequest"] as SearchRequest;
                    paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                }

                if (onwardResult.Price.MarkupType == "P")
                {
                    if (agency.AgentAirMarkupType == "BF")
                    {
                        baseFare = ((decimal)onwardResult.Price.PublishedFare) * roe;
                        baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)onwardResult.Price.PublishedFare) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                    }
                    else if (agency.AgentAirMarkupType == "TX")
                    {
                        baseFare = ((decimal)onwardResult.Price.Tax / roe) * roe;
                        baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)onwardResult.Price.Tax / rateOfExchange) * roe;
                        resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                    }
                    else
                    {
                        baseFare = ((decimal)onwardResult.Price.SupplierPrice) * roe;
                        baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)onwardResult.Price.SupplierPrice) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                    }
                }
                else
                {
                    baseFare = (onwardResult.Price.SupplierPrice * roe) + (onwardResult.Price.MarkupValue * paxCount);
                    resultBaseFare = (onwardResult.Price.SupplierPrice * rateOfExchange) + (onwardResult.Price.MarkupValue * paxCount);
                }
            }
            else
            {
                
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (agency.AgentCurrency == onwardResult.Currency)
            {
                if (rateOfExchange == roe && Math.Round(resultBaseFare, agency.DecimalValue) >= Math.Round(baseFare, agency.DecimalValue))
                {
                    bookingResponse = MakeBooking(onwardFlightItinerary);
                    
                    //Dictionary<string, string> ticketData = new Dictionary<string, string>();

                    //ticketData.Add("corporateCode", "1");
                    //ticketData.Add("tourCode", "1");
                    //ticketData.Add("endorsement", "1");
                    //ticketData.Add("remarks", "1");                    

                    //sessionId = Session["sessionId"].ToString();
                    //resultId = Convert.ToInt32(Request["id"]);                    

                    //if (bookingResponse.Status == BookingResponseStatus.Successful)
                    //{
                    //    Response.Redirect("ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId, false);
                    //}
                    //else if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                    //{
                    //    Response.Redirect("ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId, false);
                    //}
                }
                else
                {
                    lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
                }
            }
            else
            {
                lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
            }

        }
        catch (Exception ex)
        {
            MultiView1.ActiveViewIndex = 1;
            lblError.Text = ex.Message;
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
        finally
        {
            Session["FZBaggageDetails"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;
        }
        if (bookingResponse.Status == BookingResponseStatus.Successful)
        {
            //Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + Request.QueryString["id"]);
            Session["SeatError"] = bookingResponse.Error;
            Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingResponse.BookingId, false);
        }
    }

    protected void BindTicketInfo(FlightItinerary itinerary)
    {
        try
        {
            agency = new AgentMaster(itinerary.AgencyId);
            booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(itinerary.FlightId));
            if (booking.Status == BookingStatus.Ticketed)
            {
                ticketList = Ticket.GetTicketList(itinerary.FlightId);
            }

            if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai)
            {
                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                }
            }
            airline = new Airline();
            airline.Load(itinerary.ValidatingAirlineCode);

            if (airline.LogoFile.Trim().Length != 0)
            {
                string[] fileExtension = airline.LogoFile.Split('.');
                AirlineLogoPath = AirlineLogoPath + itinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected string SendFlightTicketEmail(FlightItinerary itinerary, SearchType searchType)
    {
        string myPageHTML = string.Empty;
        try
        {
            isReturnBookingSuccess = true;
            BindTicketInfo(itinerary);
            string logoPath = "";
            int agentId = 0;

            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId;
            }

            string serverPath = "";

            if (Request.Url.Port > 0)
            {
                serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath = Request.Url.Scheme+"://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }

            if (agentId > 1)
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
            }
            
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            if (searchType == SearchType.OneWay)
                EmailDiv.RenderControl(htw);
            else
                ReturnEmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
            myPageHTML = myPageHTML.Replace("none", "block");

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            toArray.Add(itinerary.Passenger[0].Email);

            string subject = "Ticket Confirmation - " + itinerary.PNR;
            AgentMaster agency = new AgentMaster(itinerary.AgencyId);
            string bccEmails = string.Empty;
            if (!string.IsNullOrEmpty(agency.Email1))
            {
                bccEmails = agency.Email1;
            }
            if (!string.IsNullOrEmpty(agency.Email2))
            {
                bccEmails = bccEmails + "," + agency.Email2;
            }
            //if (ViewState["MailSent"] == null)//If booking in not corporate then send email
            if(sendmail==true)
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), bccEmails);
                ViewState["MailSent"] = true;
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Flight E-ticket Email PNR " + itinerary.PNR + ": reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return myPageHTML;
    }


    //Added by lokesh on 4-July-2018
    /// <summary>
    /// This method returns the baggage for GDS suppliers from table BKE_Segment_PTC_Detail
    /// </summary>
    /// <param name="flightId"></param>
    /// <returns></returns>
    protected string GetBaggageForGDS(int flightId,PassengerType paxType)
    {
        string gdsBaggage = string.Empty;
        try
        {
           List<SegmentPTCDetail> segmentPTCDetails = new List<SegmentPTCDetail>();
           List<SegmentPTCDetail> paxTypeSegmentPTCDetails = new List<SegmentPTCDetail>();
            string paxCode = string.Empty;
            switch (paxType)
            {
                case PassengerType.Adult:
                    paxCode = "ADT";
                    break;
                case PassengerType.Child:                  
                     paxCode = "CNN";
                    break;
                case PassengerType.Infant:
                    paxCode = "INF";
                    break;
            }

           segmentPTCDetails =  SegmentPTCDetail.GetSegmentPTCDetail(flightId);
            paxTypeSegmentPTCDetails = segmentPTCDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.PaxType.ToLower().Equals(paxCode.ToLower()); });
            if (paxTypeSegmentPTCDetails != null && paxTypeSegmentPTCDetails.Count >0)
            {
                for(int i=0;i< paxTypeSegmentPTCDetails.Count;i++)
                {
                    if(!string.IsNullOrEmpty(paxTypeSegmentPTCDetails[i].Baggage))
                    {
                        if (string.IsNullOrEmpty(gdsBaggage))
                        {
                            gdsBaggage = !paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("piece") ? (paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("kg") ? paxTypeSegmentPTCDetails[i].Baggage : paxTypeSegmentPTCDetails[i].Baggage + "Kg") : paxTypeSegmentPTCDetails[i].Baggage;
                        }
                        else
                        {
                            gdsBaggage += "," + (!paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("piece") ? (paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("kg") ? paxTypeSegmentPTCDetails[i].Baggage : paxTypeSegmentPTCDetails[i].Baggage + "Kg") : paxTypeSegmentPTCDetails[i].Baggage);
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to read baggage" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        if(string.IsNullOrEmpty(gdsBaggage) && paxType != PassengerType.Infant)
        {
            gdsBaggage = "Airline Norms";
        }
        return gdsBaggage;
    }
    //Added by Sonmasekhar -- for loading Payment gateways
    private void LoadPaymentGateways()
    {
        try
        {
            DataTable dtPaymentGateways = AgentMaster.GetAgentPaymentgateways(agencyId, (int)ProductType.Flight);

            if (dtPaymentGateways.Rows.Count > 0)
            {
                //agentPGCount = dtPaymentGateways.Rows.Count;
                foreach (DataRow dr in dtPaymentGateways.Rows)
                {
                    rblAgentPG.Items.Add(new ListItem(Convert.ToString(dr["name"]), Convert.ToString(dr["pgcharge"])));
                    
                }
                rblAgentPG.ClearSelection();
                if (rblAgentPG.Items.Count > 0)
                {   
                    rblAgentPG.Items[0].Selected = true;
                    charges = Convert.ToDecimal(rblAgentPG.Items[0].Value);
                }

            }


        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to load Agent PaymentGateways " + ". reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void SaveInSession(string key, object data)
    {
        if (Session["PaxPageValues"] != null)
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
            if (!SessionValues.ContainsKey(key))
            {
                SessionValues.Add(key, data);
            }
            else
            {
                SessionValues[key] = data;
            }
            Session["PaxPageValues"] = SessionValues;
        }
        else
        {
            if (!SessionValues.ContainsKey(key))
            {
                SessionValues.Add(key, data);
            }
            else
            {
                SessionValues[key] = data;
            }

            Session["PaxPageValues"] = SessionValues;
        }
    }

    protected void dlFlightReturn_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                FlightInfo obj = e.Item.DataItem as FlightInfo;

                if (obj != null)
                {
                    Image imgFlightCarrierCode = e.Item.FindControl("imgFlightCarrierCode") as Image;
                    Label lblFlightOriginCode = e.Item.FindControl("lblFlightOriginCode") as Label;
                    Label lblFlightDestinationCode = e.Item.FindControl("lblFlightDestinationCode") as Label;
                    Label lblFlightDuration = e.Item.FindControl("lblFlightDuration") as Label;
                    Label lblOnFlightWayType = e.Item.FindControl("lblOnFlightWayType") as Label;

                    Label lblFlightCode = e.Item.FindControl("lblFlightCode") as Label;
                    Label lblFlightDepartureDate = e.Item.FindControl("lblFlightDepartureDate") as Label;
                    Label lblFlightArrivalDate = e.Item.FindControl("lblFlightArrivalDate") as Label;
                    Label lblRetDefaultBaggage = e.Item.FindControl("lblRetDefaultBaggage") as Label;


                    string rootFolder = Airline.logoDirectory + "/";
                    Airline departingAirline = new Airline();
                    departingAirline.Load(obj.Airline);
                    imgFlightCarrierCode.ImageUrl = rootFolder + departingAirline.LogoFile;
                    lblFlightOriginCode.Text = obj.Origin.CityName;
                    lblFlightDestinationCode.Text = obj.Destination.CityName;
                    lblFlightDuration.Text = obj.Duration.Hours + "hrs " + obj.Duration.Minutes + "mins";

                    switch (obj.Stops)
                    {
                        case 0:
                            lblOnFlightWayType.Text = "Non Stop";
                            break;
                        case 1:
                            lblOnFlightWayType.Text = "Single Stop";
                            break;
                        case 2:
                            lblOnFlightWayType.Text = "Two Stops";
                            break;
                        default:
                            lblOnFlightWayType.Text = "Two+ Stops";
                            break;
                    }
                    lblOnFlightWayType.Text += "<br/>" + obj.SegmentFareType;
                    lblFlightCode.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.FlightNumber;
                    lblFlightDepartureDate.Text = obj.DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                    lblFlightArrivalDate.Text = obj.ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                    lblRetDefaultBaggage.Text = !string.IsNullOrEmpty(obj.DefaultBaggage) ? obj.DefaultBaggage : "As Per Airline Policy";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void dlPaxPriceReturn_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblBaseFare = e.Item.FindControl("lblBaseFare") as Label;
                Label lblMarkupTotal = e.Item.FindControl("lblMarkupTotal") as Label;
                Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
                Label lblBaggagePrice = e.Item.FindControl("lblBaggagePrice") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                Label lblTax = e.Item.FindControl("lblTax") as Label;
                Label lblVATAmount = e.Item.FindControl("lblVATAmount") as Label;
                Label lblDiscount = e.Item.FindControl("lblDiscount") as Label;
                Label lblDiscountAmount = e.Item.FindControl("lblDiscountAmount") as Label;
                Label lblTotalPubFare = e.Item.FindControl("lblTotalPubFare") as Label;
                Label lblK3Tax = e.Item.FindControl("lblK3Tax") as Label;

                SearchResult result = e.Item.DataItem as SearchResult;
                FlightItinerary itinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;

                Label lblMeal = e.Item.FindControl("lblMeal") as Label;
                Label lblMealPrice = e.Item.FindControl("lblMealPrice") as Label;

                Label lblSeat = e.Item.FindControl("lblSeat") as Label;
                Label lblSeatPrice = e.Item.FindControl("lblSeatPrice") as Label;

                if (itinerary != null)
                {
                    int adults = 0;
                    decimal baggageCharge = 0, discount = 0, asvAmount = 0, mealCharge = 0, SeatCharge = 0;
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        if (pax.Type == PassengerType.Adult)
                        {
                            adults += 1;

                        }
                        if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                        {
                            baggageCharge += pax.Price.BaggageCharge;
                            mealCharge += pax.Price.MealCharge;
                        }
                        pax.Price.DecimalPoint = agency.DecimalValue;
                        discount += pax.Price.Discount;
                        asvAmount += pax.Price.AsvAmount;
                        SeatCharge += pax.Price.SeatPrice;
                    }

                    if (itinerary.FlightBookingSource == BookingSource.UAPI || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.AirArabia)
                    {
                        lblMeal.Visible = true;
                        lblMealPrice.Visible = true;
                        lblMealPrice.Text = agency.AgentCurrency + " " + mealCharge.ToString("N" + agency.DecimalValue);
                    }

                    lblSeat.Visible = lblSeatPrice.Visible = true;
                    lblSeatPrice.Text = agency.AgentCurrency + " " + SeatCharge.ToString("N" + agency.DecimalValue);

                    discount = Math.Round(discount, agency.DecimalValue);
                    baggageCharge = Math.Round(baggageCharge, agency.DecimalValue);
                    mealCharge = Math.Round(mealCharge, agency.DecimalValue);
                    SeatCharge = Math.Round(SeatCharge, agency.DecimalValue);

                    if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                    {
                        lblBaggage.Visible = true;
                        lblBaggagePrice.Visible = true;
                        lblBaggagePrice.Text = agency.AgentCurrency + " " + baggageCharge.ToString("N" + agency.DecimalValue);
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare) + baggageCharge).ToString("N" + agency.DecimalValue);//lblTotalPrice.Text;
                        hdnBookingAmount.Value = (Convert.ToDecimal(result.TotalFare) + baggageCharge).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare)).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = (Convert.ToDecimal(result.TotalFare)).ToString("N" + agency.DecimalValue);
                    }
                    decimal vatAmount = 0m;
                    
                    if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (location.ID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (Settings.LoginInfo.LocationID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else
                    {
                        if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                        {
                            OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                            decimal markup = 0m, handlingFee = 0m, paxtotalFare =0m;
                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                paxtotalFare += pax.Price.BaggageCharge + pax.Price.PublishedFare - pax.Price.Discount + pax.Price.Tax + pax.Price.OtherCharges + pax.Price.MealCharge + pax.Price.SeatPrice + pax.Price.Markup;
                                markup += pax.Price.Markup;
                                handlingFee += pax.Price.HandlingFeeAmount;
                                if (agency.AgentType == (int)AgentType.Agent)
                                {
                                    markup += pax.Price.AsvAmount;
                                    paxtotalFare += pax.Price.AsvAmount;
                                }
                            }
                            //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                            vatAmount = outVat.CalculateVatAmount((decimal)paxtotalFare, markup, agency.DecimalValue);

                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                pax.Price.OutputVATAmount = vatAmount / itinerary.Passenger.Length;
                            }
                        }
                    }
                    lblVATAmount.Text = agency.AgentCurrency + " " + vatAmount.ToString("N" + agency.DecimalValue);
                    decimal markUP = 0;
                    decimal tboMarkUp = 0, tboDiscount = 0, tboBaseFare = 0, tboTax = 0;
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            if (result.FareBreakdown[i] != null)
                            {
                                tboMarkUp += Convert.ToDecimal(result.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;  
                                tboDiscount += Convert.ToDecimal(result.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue));
                                tboBaseFare += Convert.ToDecimal(result.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(result.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                                tboTax += Convert.ToDecimal(result.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
                            }
                        }

                        tboTax += (Convert.ToDecimal(tboMarkUp.ToString("N" + agency.DecimalValue)) + (result.Price.OtherCharges) + result.Price.AdditionalTxnFee + returnResult.Price.SServiceFee + returnResult.Price.TransactionFee);
                    }
                    else
                    {
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            if (result.FareBreakdown[i] != null)
                            {
                                markUP += Convert.ToDecimal(result.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;                            
                            }
                        }
                    }
                    
                    double Tax = result.Tax + (double)markUP;
                    
                    
                    vatAmount = Convert.ToDecimal(vatAmount.ToString("N" + agency.DecimalValue));
                    asvAmount = Convert.ToDecimal(asvAmount.ToString("N" + agency.DecimalValue));
                    Tax = Convert.ToDouble(Tax.ToString("N" + agency.DecimalValue));
                    double k3Tax = Convert.ToDouble(result.Price.K3Tax.ToString("N"+agency.DecimalValue));
                    lblK3Tax.Text= agency.AgentCurrency + " " +k3Tax.ToString("N" + agency.DecimalValue);
                    result.BaseFare= Convert.ToDouble(result.BaseFare.ToString("N" + agency.DecimalValue));
                    totalBookingAmount= Convert.ToDecimal(totalBookingAmount.ToString("N" + agency.DecimalValue));
 
                    if (itinerary.FlightBookingSource != BookingSource.TBOAir)
                    {
                        lblBaseFare.Text = agency.AgentCurrency + " " + (result.BaseFare + (double)(itinerary.Passenger[0].Price.AsvElement == "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                        lblTax.Text = agency.AgentCurrency + " " + (Tax- k3Tax + (double)(itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        //For TBOAir BaseFare contains HandlingFee with OtherCharges so take Tax only
                        //lblTax.Text = agency.AgentCurrency + " " + ((result.Tax) + (double)(markUp + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + (itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0))).ToString("N" + agency.DecimalValue);
                        lblTax.Text = agency.AgentCurrency + " " + (tboTax-(decimal)k3Tax + (itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                        lblBaseFare.Text = agency.AgentCurrency + " " + (tboBaseFare + (itinerary.Passenger[0].Price.AsvElement == "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);

                    }

                    /* To display asv amount in total fare to be deducted from agent balance */
                    decimal dcAsvAmount = Settings.LoginInfo.IsOnBehalfOfAgent ? asvAmount : 0;

                    //Added By Suresh V for One- One search just ceiling Amount to be booked if the booking source is TBOAir.
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir && onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        //lblTotalPrice.Text = lblTotal.Text;
                        //Add Page Level Markup (Addl Markup/asvAmount) to the Total displayed
                        lblTxnAmount.Text = agency.AgentCurrency + " " + Math.Ceiling(Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge +  vatAmount + dcAsvAmount - discount + Math.Ceiling(totalBookingAmount)).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = Math.Ceiling((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge + vatAmount + dcAsvAmount - discount) + Math.Ceiling(totalBookingAmount)).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + asvAmount - discount)).ToString("N" + agency.DecimalValue);

                    }
                    else if (onwardFlightItinerary.FlightBookingSource != BookingSource.TBOAir && itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(totalBookingAmount) + Math.Ceiling((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge +  vatAmount + dcAsvAmount - discount))).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = (Convert.ToDecimal(totalBookingAmount) + Math.Ceiling((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge + vatAmount + dcAsvAmount - discount))).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + asvAmount - discount)).ToString("N" + agency.DecimalValue);

                    }
                    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir && itinerary.FlightBookingSource != BookingSource.TBOAir)
                    {
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Math.Ceiling(totalBookingAmount) + (Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + mealCharge + SeatCharge + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + Convert.ToDecimal(vatAmount) + dcAsvAmount - Convert.ToDecimal(discount))).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = (Math.Ceiling(totalBookingAmount) + (Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + mealCharge + SeatCharge + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + Convert.ToDecimal(vatAmount) + dcAsvAmount - Convert.ToDecimal(discount))).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + asvAmount - discount)).ToString("N" + agency.DecimalValue);

                    }
                    else 
                    {
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(totalBookingAmount) + ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + mealCharge + SeatCharge + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + Convert.ToDecimal(vatAmount) + dcAsvAmount - Convert.ToDecimal(discount)))).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = (Convert.ToDecimal(totalBookingAmount) + ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + mealCharge + SeatCharge + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + Convert.ToDecimal(vatAmount) + dcAsvAmount - Convert.ToDecimal(discount)))).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + asvAmount - discount)).ToString("N" + agency.DecimalValue);

                    }



                    if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                    {
                        lblMarkupTotal.Text = agency.AgentCurrency + " " + Math.Ceiling((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge ) + asvAmount + vatAmount - discount).ToString("N" + agency.DecimalValue);
                        lblTotalPubFare.Text = agency.AgentCurrency + " " + Math.Ceiling((Convert.ToDecimal(tboBaseFare) + (decimal)tboTax + baggageCharge) + asvAmount + vatAmount).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        lblMarkupTotal.Text = agency.AgentCurrency + " " + ((decimal)(result.BaseFare) + (decimal)Tax + asvAmount + baggageCharge + mealCharge + SeatCharge + vatAmount-discount).ToString("N" + agency.DecimalValue);
                        lblTotalPubFare.Text = agency.AgentCurrency + " " + ((decimal)(result.BaseFare) + (decimal)Tax + asvAmount + baggageCharge + mealCharge + SeatCharge + vatAmount + discount).ToString("N" + agency.DecimalValue);
                    }
                    if (discount > 0)
                    {
                        lblDiscount.Visible = true;
                        lblDiscountAmount.Visible = true;
                        lblDiscountAmount.Text = "(-)" + agency.AgentCurrency + " " + discount.ToString("N" + agency.DecimalValue);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
        finally
        {
            //hdnBookingAmount.Value = lblTxnAmount.Text.Split(' ')[1];
        }
    }

    void RemoveFromSession(string key)
    {
        if (Session["PaxPageValues"] != null)
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
            if (SessionValues.ContainsKey(key))
            {
                SessionValues[key] = null;
            }
            Session["PaxPageValues"] = SessionValues;
        }
    }

    protected Dictionary<string, object> GetSession()
    {
        if (Session["PaxPageValues"] != null)
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
        }

        return SessionValues;
    }

    protected string GenerateSuperPNR()
    {
        string pnr = FlightItinerary.GenerateRoutingTripPNR();

        return pnr.ToUpper();
    }

    /// <summary>
    /// Assign the return itinerary components to the onward itinerary.
    /// If booking source is Indigo
    /// If the itineraries are of special round trip fares.
    /// </summary>
    private void BuildSpecialRoundTripItinerary()
    {
        try
        {
            onwardFlightItinerary = SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary;
            returnFlightItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
            if (onwardFlightItinerary != null && returnFlightItinerary != null && onwardFlightItinerary.IsSpecialRoundTrip && returnFlightItinerary.IsSpecialRoundTrip)
            {
                //1.Ticket Advisory //Result Fare Sell Key
                onwardFlightItinerary.TicketAdvisory += "|" + returnFlightItinerary.TicketAdvisory;

                //2.SpecialRequest //Result Journey Sell Key
                onwardFlightItinerary.SpecialRequest += "|" + returnFlightItinerary.SpecialRequest;

                //3.Assign the Segments
                List<FlightInfo> flightInfos = new List<FlightInfo>();
                foreach (FlightInfo segment in onwardFlightItinerary.Segments)
                {
                    flightInfos.Add(segment);
                }
                foreach (FlightInfo segment in returnFlightItinerary.Segments)
                {
                    flightInfos.Add(segment);
                }
                onwardFlightItinerary.Segments = flightInfos.ToArray();

                //4.Assign the pax price components
                for(int i=0;i< onwardFlightItinerary.Passenger.Length;i++)
                {
                    //==========Price Components================

                    //1.InputVATAmount
                    onwardFlightItinerary.Passenger[i].Price.InputVATAmount += returnFlightItinerary.Passenger[i].Price.InputVATAmount;

                    //1.1 OutputVATAmount
                    onwardFlightItinerary.Passenger[i].Price.OutputVATAmount += returnFlightItinerary.Passenger[i].Price.OutputVATAmount;

                    //2.Published Fare
                    onwardFlightItinerary.Passenger[i].Price.PublishedFare += returnFlightItinerary.Passenger[i].Price.PublishedFare;

                    //3.Supplier Price
                    onwardFlightItinerary.Passenger[i].Price.SupplierPrice += returnFlightItinerary.Passenger[i].Price.SupplierPrice;

                    //4.Markup
                    onwardFlightItinerary.Passenger[i].Price.Markup += returnFlightItinerary.Passenger[i].Price.Markup;

                    //5.Tax
                    onwardFlightItinerary.Passenger[i].Price.Tax += returnFlightItinerary.Passenger[i].Price.Tax;

                    //6.Discount
                    onwardFlightItinerary.Passenger[i].Price.Discount += returnFlightItinerary.Passenger[i].Price.Discount;

                    //7.Handling Fee Amount;
                    onwardFlightItinerary.Passenger[i].Price.HandlingFeeAmount += returnFlightItinerary.Passenger[i].Price.HandlingFeeAmount;

                    //8.WhiteLabelDiscount;
                    onwardFlightItinerary.Passenger[i].Price.WhiteLabelDiscount += returnFlightItinerary.Passenger[i].Price.WhiteLabelDiscount;

                    //9.Transaction Fee
                    onwardFlightItinerary.Passenger[i].Price.TransactionFee += returnFlightItinerary.Passenger[i].Price.TransactionFee;

                    //10.Additional Transaction Fee
                    onwardFlightItinerary.Passenger[i].Price.AdditionalTxnFee += returnFlightItinerary.Passenger[i].Price.AdditionalTxnFee;

                    //11.AsvAmount
                    onwardFlightItinerary.Passenger[i].Price.AsvAmount += returnFlightItinerary.Passenger[i].Price.AsvAmount;

                    //12.Dynamic Markup
                    onwardFlightItinerary.Passenger[i].Price.DynamicMarkup += returnFlightItinerary.Passenger[i].Price.DynamicMarkup;

                    //13.Baggage Charge and Baggage Codes
                    onwardFlightItinerary.Passenger[i].BaggageCode += "," + returnFlightItinerary.Passenger[i].BaggageCode;
                    onwardFlightItinerary.Passenger[i].Price.BaggageCharge += returnFlightItinerary.Passenger[i].Price.BaggageCharge;
                    onwardFlightItinerary.Passenger[i].BaggageType += "," + returnFlightItinerary.Passenger[i].BaggageType;

                    //14.Meal Charge and Meal Codes
                    onwardFlightItinerary.Passenger[i].MealType += "," + returnFlightItinerary.Passenger[i].MealType;
                    onwardFlightItinerary.Passenger[i].Price.MealCharge += returnFlightItinerary.Passenger[i].Price.MealCharge;
                    onwardFlightItinerary.Passenger[i].MealDesc += returnFlightItinerary.Passenger[i].MealDesc;

                    //15.Seat Charge and Seat Codes
                    if (onwardFlightItinerary.Passenger[i].liPaxSeatInfo == null && returnFlightItinerary.Passenger[i].liPaxSeatInfo != null)
                        onwardFlightItinerary.Passenger[i].liPaxSeatInfo = new List<PaxSeatInfo>();
                    if (returnFlightItinerary.Passenger[i].liPaxSeatInfo != null)
                        onwardFlightItinerary.Passenger[i].liPaxSeatInfo.AddRange(returnFlightItinerary.Passenger[i].liPaxSeatInfo);
                    onwardFlightItinerary.Passenger[i].Price.SeatPrice += returnFlightItinerary.Passenger[i].Price.SeatPrice;

                    //16.Assigning Pax WiseTax Break Up components.
                    if (returnFlightItinerary.Passenger[i].TaxBreakup != null && returnFlightItinerary.Passenger[i].TaxBreakup.Count > 0)
                    {

                        List<KeyValuePair<string, decimal>> newTaxComponents = new List<KeyValuePair<string, decimal>>();
                        newTaxComponents.AddRange(onwardFlightItinerary.Passenger[i].TaxBreakup);
                        newTaxComponents.AddRange(returnFlightItinerary.Passenger[i].TaxBreakup);
                        onwardFlightItinerary.Passenger[i].TaxBreakup = newTaxComponents;
                       
                    }

                }

                //5.Assign itinerary pending amount due
                onwardFlightItinerary.ItineraryAmountDue = returnFlightItinerary.ItineraryAmountDue;
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
}
