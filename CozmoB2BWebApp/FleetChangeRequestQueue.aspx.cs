﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Configuration;
using CT.Core;
using CT.MetaSearchEngine;
using CT.AccountingEngine;

public partial class FleetChangeRequestQueue : CT.Core.ParentPage
{
    protected int pageNo;
    protected List<int> sourcesList = new List<int>();
    protected List<int> selectedSources = new List<int>();
    protected string fleetCancel = string.Empty;
    protected bool isAdmin = false;
    protected string loginFilter = null;
    UserMaster loggedMember = null;
    protected int recordsPerPage = 10;
    private int loggedMemberId;
    protected int noOfPages;
    protected int queueCount = 0;
    protected string show, fleetFilter, paxFilterUI;
    string requestTypeCSV = null;
    protected List<int> requestTypes = new List<int>();
    string serviceRequestTypeCSV = null;
    protected bool isAssignedRequest;
    protected ServiceRequest[] serviceRequest = new ServiceRequest[0];
    protected List<int> requestStatuses = new List<int>();
    bool? isDomestic = null;
    protected List<ChangeRequestQueue> data = new List<ChangeRequestQueue>();
    protected string agentFilter = string.Empty;
    string agencyTypeCSV = null;
    protected string paxFilter = null;
    protected string pnrFilter = null;
    protected string agencyIdCSV = null;
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected List<FleetItinerary> itinerary = new List<FleetItinerary>();
    protected string requestSourceIdCSV = null;
    string bookingSourceCSV = null;
    protected string ticketFilter = null;
    protected string pagingScript = string.Empty;
    int currentPageNo = 1;
    protected List<fleetAgentBookingBO> filteredResults;
    protected int maxResult, minResult;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            if (Settings.LoginInfo != null)
            {

                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                    selectedSources = sourcesList;
                    fleetCancel = "FleetCancel";
                }

                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 0)
                {
                    isAdmin = true;
                }
                else
                {
                    loginFilter = Convert.ToString(Session["loginName"]);
                }
                if (Settings.LoginInfo.AgentId != 1)
                {
                    ddlTransType.Enabled = false;
                }
                else
                {
                    ddlTransType.Enabled = true;
                }
                recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    sourcesList = FleetItinerary.LoadSourceId();
                    selectedSources = sourcesList;

                    txtCheckIn.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtCheckOut.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    hdfParam.Value = "0";

                    ddlAgents.DataSource = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                    ddlAgents.DataTextField = "agent_name";
                    ddlAgents.DataValueField = "agent_id";
                    ddlAgents.DataBind();

                    Array Sources = Enum.GetValues(typeof(CarBookingSource));
                    foreach (CarBookingSource source in Sources)
                    {
                        if (source == CarBookingSource.Sayara)
                        {
                            ListItem item = new ListItem(Enum.GetName(typeof(CarBookingSource), source), ((int)source).ToString());
                            ddlSource.Items.Add(item);
                        }
                    }

                    ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
                    ddlLocations.DataTextField = "location_name";
                    ddlLocations.DataValueField = "location_id";
                    ddlLocations.DataBind();

                    Array Statuses = Enum.GetValues(typeof(CarBookingStatus));
                    foreach (CarBookingStatus status in Statuses)
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(CarBookingStatus), status), ((int)status).ToString());
                        if (item.Text == "Confirmed")
                        {
                            ddlBookingStatus.Items.Insert(2, new ListItem("Vouchered", "1"));
                        }
                        else
                        {
                            ddlBookingStatus.Items.Add(item);
                        }
                    }
                    ddlAgents.SelectedIndex = -1;
                    if (Settings.LoginInfo.AgentId > 0)
                    {
                        ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    }
                    if (Settings.LoginInfo.AgentId > 1)
                    {
                        ddlAgents.Enabled = false;
                    }
                    LoadChangeQueues();

                }
                else
                {
                    if (ddlTransType.SelectedItem.Value == "B2B")
                    {
                        SetFilters();
                    }
                    else  //B2C paging
                    {
                        if (PageB2CNoString.Value.Length > 0 && Session["bookingQueueRes"] != null && hdnTransTypeSelected.Value == "0")
                        {
                            currentPageNo = Convert.ToInt16(PageB2CNoString.Value);
                            List<fleetAgentBookingBO> bookingQueueRes = Session["bookingQueueRes"] as List<fleetAgentBookingBO>;
                            filteredResults = DoPaging(bookingQueueRes);
                            if (filteredResults != null && filteredResults.Count > 0)
                            {
                                dlSCRBookingQueue.DataSource = filteredResults; //B2C Bookings
                                dlSCRBookingQueue.DataBind();
                            }
                        }
                    }
                }
                loggedMember = new UserMaster(Convert.ToInt32(Session["memberId"]));
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.FleetCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void LoadChangeQueues()
    {
        try
        {
            if (requestTypeCSV == null)
            {
                requestTypes.Add((int)RequestType.FleetCancel);
                fleetCancel = "fleetCancel";
                requestTypeCSV = "8,9";
            }
            if (serviceRequestTypeCSV == null)
            {
                if (isAssignedRequest)
                {
                    requestStatuses.Add((int)ServiceRequestStatus.Unassigned);
                    requestStatuses.Add((int)ServiceRequestStatus.Assigned);
                    requestStatuses.Add((int)ServiceRequestStatus.Acknowledged);
                    requestStatuses.Add((int)ServiceRequestStatus.Closed);
                    requestStatuses.Add((int)ServiceRequestStatus.Pending);
                    requestStatuses.Add((int)ServiceRequestStatus.Rejected);
                    requestStatuses.Add((int)ServiceRequestStatus.Completed);
                    serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                }
            }
            isDomestic = true;

            string endDate = txtCheckOut.Text;
            endDate += " 23:59:59";
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            data = CT.Core.Queue.GetFleetChangeRequestData(((pageNo - 1) * recordsPerPage) + 1, (pageNo * recordsPerPage), requestTypeCSV, serviceRequestTypeCSV, agencyTypeCSV, bookingSourceCSV, fleetFilter, agencyIdCSV, paxFilter, loginFilter, pnrFilter, ticketFilter, isDomestic, ref queueCount, requestSourceIdCSV, Convert.ToDateTime(txtCheckIn.Text, dateFormat), Convert.ToDateTime(endDate, dateFormat));
            CT.Core.Queue[] queues = new CT.Core.Queue[data.Count];
            for (int j = 0; j < data.Count; j++)
            {
                queues[j] = new CT.Core.Queue();
                queues[j].QueueId = data[j].QueueId;
                queues[j].ItemId = data[j].ItemId;
            }
            AssignToVariables(queues);

            string url = "";
            if (queueCount > 0)
            {
                if ((queueCount % recordsPerPage) > 0)
                {
                    noOfPages = (queueCount / recordsPerPage) + 1;
                }
                else
                {
                    noOfPages = (queueCount / recordsPerPage);
                }
            }
            else
            {
                noOfPages = 0;
            }

            if (noOfPages > 0)
            {
                //For Paging
                show = MetaSearchEngine.PagingJavascript(noOfPages, url, Convert.ToInt32(PageNoString.Value));
            }

            if (fleetFilter == null)
            {
                fleetFilter = "";
            }
            if (agencyIdCSV == null)
            {
                agentFilter = "";
            }
            if (paxFilter == null)
            {
                paxFilter = "";
            }
            if (pnrFilter == null)
            {
                pnrFilter = "";
            }
            if (loginFilter == null)
            {
                loginFilter = "";
            }
        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.FleetCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Change REquest Queue: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    private void AssignToVariables(CT.Core.Queue[] relevantQueues)
    {
        try
        {
            ArrayList listOfItemId = new ArrayList();
            int numBooking = 0;
            foreach (CT.Core.Queue queue in relevantQueues)
            {
                ServiceRequest sr = new ServiceRequest(queue.ItemId);
                listOfItemId.Add(queue.ItemId);
                numBooking++;
            }
            bookingDetail = new BookingDetail[numBooking];

            serviceRequest = new ServiceRequest[numBooking];
            for (int count = 0; count < listOfItemId.Count; count++) //@@@@ From here
            {
                ServiceRequest sRequest = new ServiceRequest(Convert.ToInt32(listOfItemId[count]));
                serviceRequest[count] = sRequest;
                BookingDetail bDetail = null;
                try
                {
                    bDetail = new BookingDetail(serviceRequest[count].BookingId);
                    bookingDetail[count] = bDetail;
                }
                catch { }
                Product[] products = BookingDetail.GetProductsLine(serviceRequest[count].BookingId);
                AgentMaster agency = new AgentMaster(bDetail.AgencyId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i] != null)
                    {
                        string productType = Enum.Parse(typeof(ProductType), products[i].ProductTypeId.ToString()).ToString();
                        switch (productType)
                        {

                            case "Car": FleetItinerary itineary = new FleetItinerary();
                                itineary.Load(products[i].ProductId);
                                itineary.ProductId = products[i].ProductId;
                                itineary.ProductType = products[i].ProductType;
                                itineary.ProductTypeId = products[i].ProductTypeId;
                                itineary.Remarks = sRequest.Data; //Remarks will be add here for only showing purpose
                                itinerary.Add(itineary);

                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            Session["ServiceRequests"] = serviceRequest;
            dlChangeRequests.DataSource = itinerary;  //Binding B2B Bookings
            dlChangeRequests.DataBind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.ChangeRequest, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    private void SetFilters()
    {


        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAssignedRequest = true;
            isAdmin = true;
        }

        agencyTypeCSV = "1";


        if (requestTypeCSV == null)
        {
            requestTypeCSV = "8,9";
        }
        fleetCancel = Request["FleetCancel"];

        switch (ddlBookingStatus.SelectedItem.Text)
        {
            case "Unassigned":
                serviceRequestTypeCSV = "1";
                break;
            case "Assigned":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "2";
                }
                else
                {
                    serviceRequestTypeCSV += ",2";
                }
                break;
            case "Acknowledged":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "3";
                }
                else
                {
                    serviceRequestTypeCSV += ",3";
                }
                break;
            case "Completed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "4";
                }
                else
                {
                    serviceRequestTypeCSV += ",4";
                }
                break;
            case "Rejected":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "5";
                }
                else
                {
                    serviceRequestTypeCSV += ",5";
                }
                break;
            case "Closed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "6";
                }
                else
                {
                    serviceRequestTypeCSV += ",6";
                }
                break;
            case "Pending":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "7";
                }
                else
                {
                    serviceRequestTypeCSV += ",7";
                }
                break;
            case "All":
                serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                break;
        }


        selectedSources = new List<int>();
        sourcesList = FleetItinerary.LoadSourceId();
        bookingSourceCSV = null;


        if (ddlSource.SelectedItem.Text == "All")
        {
            for (int i = 1; i <= sourcesList.Count; i++)
            {

                selectedSources.Add(i);
                if (bookingSourceCSV != null)
                {
                    bookingSourceCSV += "," + i.ToString();
                }
                else
                {
                    bookingSourceCSV = i.ToString();
                }

            }
        }
        else
        {
            bookingSourceCSV = Convert.ToString(ddlSource.SelectedItem.Value);
        }

        if (!string.IsNullOrEmpty(txtFleetName.Text))
        {
            fleetFilter = txtFleetName.Text.Replace("'", "''").Trim();
        }
        if (!string.IsNullOrEmpty(txtAgentLoginName.Text.Trim()))
        {
            loginFilter = txtAgentLoginName.Text.Replace("'", "''").Trim();
        }
        if (!string.IsNullOrEmpty(txtPaxName.Text))
        {
            paxFilterUI = txtPaxName.Text;
            paxFilter = txtPaxName.Text.Replace("'", "''").Trim();
            paxFilter = paxFilter.Replace(" ", "");
        }
        if (!string.IsNullOrEmpty(txtConfirmNo.Text))
        {
            pnrFilter = txtConfirmNo.Text.Replace("'", "''").Trim();
        }

        if (ddlAgents.SelectedItem.Text != "All")
        {
            agencyIdCSV = ddlAgents.SelectedItem.Text.Replace("'", "''").Trim();
        }
        requestSourceIdCSV = "2,3";


        if (agencyIdCSV == null)
        {
            agentFilter = "";
            agencyIdCSV = "";
        }


    }
    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtLocations = LocationMaster.GetList(Utility.ToInteger(ddlAgents.SelectedItem.Value), ListStatus.Short, RecordStatus.Activated, string.Empty);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
        hdfParam.Value = "0";
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlTransType.SelectedItem.Value == "B2B")
        {
            lblMessage.Text = "";
            tblB2B.Visible = true;
            tblB2C.Visible = false;
            LoadChangeQueues();
        }
        else if (ddlTransType.SelectedItem.Value == "B2C") //Here B2C means Sayara Bookings
        {
            tblB2B.Visible = false;
            tblB2C.Visible = true;
            LoadQueue();
        }
    }
    protected void dlChangeRequests_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Refund")
            {
                TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
                Label lblError = e.Item.FindControl("lblError") as Label;
                Label lblRemarks = e.Item.FindControl("lblRemarks") as Label;
                lblMessage.Text = "";

                if (txtAdminFee.Text.Trim().Length > 0 && txtSupplierFee.Text.Trim().Length > 0)
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(FleetItinerary.GetFleetId(e.CommandArgument.ToString()), ProductType.Car));
                    FleetItinerary itinerary = new FleetItinerary();
                    FleetPassenger fPax = new FleetPassenger();
                    AgentMaster agent = new AgentMaster(bookingDetail.AgencyId);
                    Product[] products = BookingDetail.GetProductsLine(bookingDetail.BookingId);
                    decimal bookingAmt = 0;
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Car)
                        {
                            itinerary.Load(products[i].ProductId);
                            itinerary.PassengerInfo = fPax.Load(itinerary.FleetId);
                            break;
                        }
                    }

                    decimal discount = 0;

                    if (itinerary.Price.NetFare > 0)
                    {
                        bookingAmt += (itinerary.Price.NetFare + itinerary.Price.Markup + itinerary.Price.Tax); 
                    }
                    discount += itinerary.Price.Discount;


                    bookingAmt -= discount;
                    //Use ceiling instead of Round, Changed on 05082016
                    bookingAmt = Math.Ceiling(bookingAmt);
                    Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                    if (itinerary.Source == CarBookingSource.Sayara)
                    {
                        Sayara.SayaraApi sayaraApi = new Sayara.SayaraApi();
                        itinerary.Remarks = lblRemarks.Text.Trim();
                        cancellationData = sayaraApi.CancelFleetBooking(itinerary);
                    }
                    if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED")
                    {
                        lblError.Text = "";
                        itinerary.BookingStatus = CarBookingStatus.Cancelled;
                        if (itinerary.Source == CarBookingSource.Sayara)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }

                        itinerary.UpdateBookingStatus();

                        serviceRequest = Session["ServiceRequests"] as ServiceRequest[];

                        loggedMemberId = (int)Settings.LoginInfo.UserID;
                        ServiceRequest sr = new ServiceRequest();
                        CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                        cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                        cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                        cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                        cancellationCharge.ReferenceId = itinerary.FleetId;
                        decimal exchangeRate = 0;
                        if (cancellationData["Currency"] != agent.AgentCurrency)
                        {
                            StaticData staticInfo = new StaticData();
                            staticInfo.BaseCurrency = agent.AgentCurrency;

                            Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                            exchangeRate = rateOfExList[cancellationData["Currency"]];
                            cancellationCharge.CancelPenalty = (Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate);
                        }
                        else
                        {
                            cancellationCharge.CancelPenalty = Convert.ToDecimal(cancellationData["Amount"]);
                        }
                        cancellationCharge.CreatedBy = loggedMemberId;
                        cancellationCharge.ProductType = ProductType.Car;
                        cancellationCharge.Save();

                        if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0)
                        {
                            try
                            {

                                //decimal exchangeRate = 0;
                                if (cancellationData["Currency"] != agent.AgentCurrency)
                                {
                                    StaticData staticInfo = new StaticData();
                                    staticInfo.BaseCurrency = agent.AgentCurrency;
                                    Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                    exchangeRate = rateOfExList[cancellationData["Currency"]];
                                }
                                if (exchangeRate <= 0)
                                {
                                    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]);
                                }
                                else
                                {
                                    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate;
                                }

                            }
                            catch { }
                        }

                        // Admin & Supplier Fee save in Leadger
                        int invoiceNumber = 0;
                        decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                        //decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                        decimal adminFee = 0;
                        if (adminChar < bookingAmt)
                        {
                            adminFee = adminChar;
                        }
                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                        NarrationBuilder objNarration = new NarrationBuilder();
                        invoiceNumber = Invoice.isInvoiceGenerated(itinerary.FleetId, ProductType.Car);
                        //ledgerTxn = new LedgerTransaction();
                        objNarration.PaxName = itinerary.PassengerInfo[0].FirstName + " " + itinerary.PassengerInfo[0].LastName;
                        objNarration.DocNo = invoiceNumber.ToString();
                        objNarration.FleetConfirmationNo = itinerary.BookingRefNo;
                        objNarration.TravelDate = itinerary.FromDate.ToShortDateString();
                        objNarration.Remarks = "Fleet Cancellation Charges";
                        //Ledger
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = -adminFee;
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.FleetId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CarCancellationCharge;
                        ledgerTxn.Notes = "Confirmation NO :" + itinerary.BookingRefNo; ;
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                        //save Refund amount
                        ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = bookingAmt;
                        objNarration.PaxName = itinerary.PassengerInfo[0].FirstName + " " + itinerary.PassengerInfo[0].LastName;
                        objNarration.Remarks = "Refunded for Voucher No -" + itinerary.BookingRefNo; ;
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.FleetId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CarRefund;
                        ledgerTxn.Notes = "Fleet Voucher Refunded";
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);


                        if (adminFee <= bookingAmt)
                        {
                            bookingAmt -= adminFee;
                        }

                        if (itinerary.TransType != "B2C")
                        {
                            //Settings.LoginInfo.AgentBalance += bookingAmt;
                            agent.CreatedBy = Settings.LoginInfo.UserID;
                            agent.UpdateBalance(bookingAmt);

                            if (bookingDetail.AgencyId == Settings.LoginInfo.AgentId)
                            {
                                Settings.LoginInfo.AgentBalance += bookingAmt;
                            }
                        }


                        //Update Queue Status
                        CT.Core.Queue.SetStatus(QueueType.Request, serviceRequest[e.Item.ItemIndex].RequestId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                        sr.UpdateServiceRequestAssignment(serviceRequest[e.Item.ItemIndex].RequestId, (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);

                        //Sending Email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agent.Name);
                        table.Add("itemName", itinerary.FleetName);
                        table.Add("confirmationNo", itinerary.BookingRefNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);

                        string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["FLEET_CANCEL_MAIL"]).Split(';');
                        foreach (string cnMail in cancelMails)
                        {
                            toArray.Add(cnMail);
                        }

                        string message = ConfigurationManager.AppSettings["FLEET_REFUND"];
                        try
                        {
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Fleet)Response for cancellation. Confirmation No:(" + itinerary.BookingRefNo + ")", message, table);
                        }
                        catch { }
                    }
                    else
                    {
                        lblMessage.Text = "Failed Cancellation from " + itinerary.Source.ToString();
                    }
                    LoadChangeQueues();
                }
                else
                {
                    lblError.Text = "Please enter Admin & Supplier Fee";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, "0");
        }
    }
    protected void dlChangeRequests_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                FleetItinerary itinerary = e.Item.DataItem as FleetItinerary;

                if (itinerary != null)
                {
                    Label lblSupplier = e.Item.FindControl("lblSupplier") as Label;
                    Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                    Label lblGuests = e.Item.FindControl("lblGuests") as Label;
                    Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                    Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                    TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    txtAdminFee.Attributes.Add("onblur", "Validate(" + e.Item.ItemIndex + ")");
                    Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                    Label lblAgent = e.Item.FindControl("lblAgent") as Label;
                    btnRefund.OnClientClick = "return Validate('" + e.Item.ItemIndex + "');";

                    Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                    Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                    Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                    Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                    Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;
                    Label lblCreatedBy = e.Item.FindControl("lblCreatedBy") as Label;
                    Label lblAdminCurrency = e.Item.FindControl("lblAdminCurrency") as Label;
                    Label lblSupplierCurrency = e.Item.FindControl("lblSupplierCurrency") as Label;

                    decimal totalPrice = 0;
                    if (itinerary != null)
                    {
                        FleetPassenger fPax = new FleetPassenger();
                        itinerary.PassengerInfo = fPax.Load(itinerary.FleetId);
                        if (itinerary.Price.AccPriceType == PriceType.PublishedFare)
                        {
                            totalPrice = itinerary.Price.PublishedFare + itinerary.Price.Tax - itinerary.Price.Discount;
                        }
                        else
                        {
                            totalPrice = itinerary.Price.NetFare + itinerary.Price.Tax + itinerary.Price.Markup + itinerary.Price.B2CMarkup - itinerary.Price.Discount;
                        }

                        lblPrice.Text = (itinerary.Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Price.Currency) : "AED ") + " " + Math.Ceiling(totalPrice).ToString("N" + itinerary.Price.DecimalPoint);
                        lblAdminCurrency.Text = itinerary.Price.Currency;
                        lblSupplierCurrency.Text = itinerary.Price.Currency;


                        lblPaxName.Text = itinerary.PassengerInfo[0].FirstName == null ? "" : itinerary.PassengerInfo[0].FirstName + " " + itinerary.PassengerInfo[0].LastName;

                    }
                    lblGuests.Text = "1";

                    lblSupplier.Text = (itinerary.Source == null ? "" : itinerary.Source.ToString());
                    lblStatus.Text = itinerary.BookingStatus.ToString();
                    try
                    {
                        //---------------------------Retrieve Agent-------------------------------------------
                        int bkgId = BookingDetail.GetBookingIdByProductId(itinerary.ProductId, itinerary.ProductType);
                        BookingDetail bkgDetail = new BookingDetail(bkgId);
                        AgentMaster agent = new AgentMaster(bkgDetail.AgencyId);
                        lblAgent.Text = agent.Name;
                        lblAgencyBalance.Text = agent.CurrentBalance.ToString("0.000");
                        lblAgencyEmail.Text = agent.Email1;
                        lblAgencyName.Text = agent.Name;
                        lblAgencyPhone1.Text = agent.Phone1;
                        lblAgencyPhone2.Text = agent.Phone2;
                        //------------------------------------------------------------------------------------
                    }
                    catch { }
                    if (DateTime.Now.CompareTo(itinerary.FromDate) > 0)
                    {

                        txtAdminFee.Visible = false;
                        btnRefund.Visible = false;
                    }
                    else
                    {

                        if (itinerary.FromDate.Subtract(DateTime.Now).Days >= 0 && itinerary.BookingStatus != CarBookingStatus.Cancelled)
                        {
                            txtAdminFee.Visible = true;
                            btnRefund.Visible = true;
                        }
                        else
                        {
                            txtAdminFee.Visible = false;
                            btnRefund.Visible = false;
                        }
                    }

                    if (lblPaxName.Text == "")
                    {
                        e.Item.Visible = false;
                    }
                    try
                    {
                        UserMaster createdBy = new UserMaster(Convert.ToInt64(itinerary.CreatedBy));
                        lblCreatedBy.Text = createdBy.FirstName + " " + createdBy.LastName;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
    }
    protected void ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
    {
        tblB2B.Visible = false;
        //tblB2C.Visible = false;
        if (ddlTransType.SelectedItem.Value == "B2B")
        {
            VisableControls(true);
            lblPaymentStatus.Visible = false;
            ddlPaymentStatus.Visible = false;
            lblB2CBookingStatus.Visible = false;
            ddlB2CBookingStatus.Visible = false;
            lblB2CConfirmNo.Visible = false;
            txtB2CConfirmNo.Visible = false;
        }
        else if (ddlTransType.SelectedItem.Value == "B2C")
        {
            VisableControls(false);
            lblPaymentStatus.Visible = true;
            ddlPaymentStatus.Visible = true;
            lblB2CBookingStatus.Visible = true;
            ddlB2CBookingStatus.Visible = true;
            lblB2CConfirmNo.Visible = true;
            txtB2CConfirmNo.Visible = true;
        }
        hdfParam.Value = "0";
    }
    private void VisableControls(bool value)
    {
        lblSource.Visible = value;
        ddlSource.Visible = value;
        ddlAgents.Visible = value;
        lblAgent.Visible = value;
        lblLocation.Visible = value;
        ddlLocations.Visible = value;
        lblFleetName.Visible = value;
        txtFleetName.Visible = value;
        lblPaxName.Visible = value;
        txtPaxName.Visible = value;
        lblConfirmNo.Visible = value;
        txtConfirmNo.Visible = value;
        lblBookingStatus.Visible = value;
        ddlBookingStatus.Visible = value;
        lblAgentName.Visible = value;
        txtAgentLoginName.Visible = value;
    }

    private void LoadQueue()
    {
        try
        {

            DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
            string bkRefNum = string.Empty;
            string bkStatus = string.Empty;
            string paymentStatus = string.Empty;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            if (txtCheckIn.Text != string.Empty)
            {
                try
                {
                    startDate = Convert.ToDateTime(txtCheckIn.Text, provider);
                }
                catch { }
            }
            else
            {
                startDate = DateTime.Now;
            }

            if (txtCheckOut.Text != string.Empty)
            {
                try
                {
                    endDate = Convert.ToDateTime(Convert.ToDateTime(txtCheckOut.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                catch { }
            }
            else
            {
                endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
            }
            if (txtConfirmNo.Text != string.Empty)
            {
                bkRefNum = txtConfirmNo.Text;
            }
            if (ddlBookingStatus.SelectedIndex > 0)
            {
                bkStatus = ddlBookingStatus.SelectedItem.Value;
            }
            if (ddlPaymentStatus.SelectedIndex > 0)
            {
                paymentStatus = ddlPaymentStatus.SelectedItem.Value;
            }
            Sayara.SayaraApi sayaraApi = new Sayara.SayaraApi();
            List<fleetAgentBookingBO> bookingQueueRes = sayaraApi.GetB2CChangeRequestQueue(startDate, endDate, bkRefNum, bkStatus, paymentStatus);
            Session["bookingQueueRes"] = bookingQueueRes;
            filteredResults = DoPaging(bookingQueueRes);
            if (filteredResults != null && filteredResults.Count > 0)
            {
                dlSCRBookingQueue.DataSource = filteredResults;
                dlSCRBookingQueue.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="resultDisplay"></param>
    /// <returns></returns>
    private List<fleetAgentBookingBO> DoPaging(List<fleetAgentBookingBO> resultDisplay)
    {
        List<fleetAgentBookingBO> results = new List<fleetAgentBookingBO>();
        try
        {
            if (resultDisplay != null && resultDisplay.Count > 0)
            {

                int numberOfPages = 0;
                if (resultDisplay.Count % recordsPerPage > 0)
                {
                    numberOfPages = (resultDisplay.Count / recordsPerPage) + 1;
                }
                else
                {
                    numberOfPages = resultDisplay.Count / recordsPerPage;
                }

                string urlForPaging = string.Empty;
                pagingScript = PagingJavascript(numberOfPages, urlForPaging, currentPageNo);

                if (resultDisplay.Count > 0)
                {
                    if ((currentPageNo * recordsPerPage) <= resultDisplay.Count)
                    {
                        for (int i = (currentPageNo * recordsPerPage) - recordsPerPage; i < (currentPageNo * recordsPerPage); i++)
                        {
                            if (i < resultDisplay.Count)
                            {
                                results.Add(resultDisplay[i]);
                            }
                        }
                    }
                    else
                    {
                        for (int i = (currentPageNo * recordsPerPage) - recordsPerPage; i < (currentPageNo * recordsPerPage); i++)
                        {
                            if (i < resultDisplay.Count)
                            {
                                results.Add(resultDisplay[i]);
                            }
                        }
                    }
                }

                maxResult = results.Count * currentPageNo;
                if (currentPageNo * recordsPerPage < maxResult)
                {
                    maxResult = currentPageNo * recordsPerPage;//filteredResults.Length;
                }

                minResult = ((currentPageNo * recordsPerPage) - recordsPerPage);
            }
            return results;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    /// </summary>
    /// <param name="noOfPages"></param>
    /// <param name="url"></param>
    /// <param name="pageNo"></param>
    /// <returns></returns>
    public static string PagingJavascript(int noOfPages, string url, int pageNo)
    {
        string show = string.Empty;
        string previous = string.Empty;
        string first = string.Empty;
        string last = string.Empty;
        string next = string.Empty;
        int showperPage = 2;

        // records per page from configuration       
        showperPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["showPerPage"]);

        showperPage = showperPage - 1;
        int showPageNos;
        if ((pageNo + showperPage) <= noOfPages)
        {
            showPageNos = pageNo + showperPage;
        }
        else
        {
            showPageNos = noOfPages;
        }

        if (pageNo != 1)
        {
            previous = "<a href=\"" + "javascript:ShowB2CPage(" + (pageNo - 1) + ")\"> Previous </a>| ";
            first = "<a href=\"" + "javascript:ShowB2CPage(1)\"> First </a>| ";
        }

        if (pageNo != noOfPages)
        {
            next = "<a href=\"" + "javascript:ShowB2CPage(" + (pageNo + 1) + ")\"> Next </a>";
            last = "| <a href=\"" + "javascript:ShowB2CPage(" + noOfPages + ")\"> Last </a>";
        }
        show = first + previous;
        for (int k = pageNo; k <= showPageNos; k++)
        {
            if (k == pageNo)
            {
                show += "<b><a href=\"" + "javascript:ShowB2CPage(" + k + ")\">" + k + "</a></b> | ";
            }
            else
            {
                show += "<a href=\"" + "javascript:ShowB2CPage(" + k + ")\">" + k + "</a> | ";
            }
        }
        show = show + next + last;

        return show;
    }
    protected void dlSCRBookingQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                fleetAgentBookingBO queeuDetail = e.Item.DataItem as fleetAgentBookingBO;


                if (queeuDetail != null)
                {

                    Label lblBookingStatus = e.Item.FindControl("lblBookingStatus") as Label;

                    Label lblBookingDate = e.Item.FindControl("lblBookingDate") as Label;
                    Label lblFromLoc = e.Item.FindControl("lblFromLoc") as Label;
                    Label lblToLoc = e.Item.FindControl("lblToLoc") as Label;

                    Label lblBookingReference = e.Item.FindControl("lblBookingReference") as Label;
                    Label lblBookingNetValue = e.Item.FindControl("lblBookingNetValue") as Label;
                    Label lblPaymentStatus = e.Item.FindControl("lblPaymentStatus") as Label;


                    Label lblBookedBy = e.Item.FindControl("lblBookedBy") as Label;
                    Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;

                    Label lblRemarks = e.Item.FindControl("lblRemarks") as Label;

                    Label lblToLocation = e.Item.FindControl("lblToLocation") as Label;


                    Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                    btnRefund.OnClientClick = "return ValidateB2C('" + e.Item.ItemIndex + "');";

                    lblBookingStatus.Text = queeuDetail.BookingStatus;
                    lblBookingDate.Text = queeuDetail.BookingDate.ToString();
                    lblFromLoc.Text = queeuDetail.FromLocation;
                    lblToLoc.Text = queeuDetail.ToLocation;
                    lblBookingReference.Text = queeuDetail.BookingRef;
                    lblBookingNetValue.Text = Convert.ToString(queeuDetail.BookingNetAmount);
                    lblPaymentStatus.Text = queeuDetail.PayStatus;
                    lblBookedBy.Text = queeuDetail.FirstName + "" + queeuDetail.LastName;
                    lblPaxName.Text = queeuDetail.FirstName + "" + queeuDetail.LastName;
                    lblRemarks.Text = queeuDetail.Remarks;

                    if (string.IsNullOrEmpty(lblToLoc.Text))
                    {
                        lblToLoc.Visible = false;
                        lblToLocation.Visible = false;
                    }
                    else
                    {
                        lblToLoc.Visible = true;
                        lblToLocation.Visible = true;
                    }

                    if (queeuDetail.ServiceRequestStatus == "CancellationRequest" || queeuDetail.ServiceRequestStatus == "ModificationRequest")
                    {
                        btnRefund.Visible = true;
                    }
                    else
                    {
                        btnRefund.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
        }
    }

    protected void dlSCRBookingQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {

            TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
            TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
            if (e.CommandName == "Refund")
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { '|' });
                decimal adminFee = Convert.ToDecimal(txtAdminFee.Text);
                decimal suppplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                int bookingId = Convert.ToInt32(commandArgs[0]);
                int clientId = Convert.ToInt32(commandArgs[3]);
                int serviceRequestId = Convert.ToInt32(commandArgs[1]);
                string requestStatus = string.Empty;
                if (commandArgs[2] == "CancellationRequest")
                {
                    requestStatus = "Cancel";
                }
                if (commandArgs[2] == "ModificationRequest")
                {
                    requestStatus = "Modified";
                }
                 Sayara.SayaraApi sayaraApi = new Sayara.SayaraApi();
                 bool saved = sayaraApi.SaveServiceRequestDetails(adminFee, suppplierFee, bookingId, clientId, serviceRequestId, requestStatus);
                 if (saved)
                 {
                     LoadQueue();
                 }

            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
        }
    }
}
