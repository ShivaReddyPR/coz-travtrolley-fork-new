﻿using System;
using System.Data;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class FixedDepartureCreditNoteGUI : System.Web.UI.Page
{
    protected DataTable passengers;
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
    protected RegCity agencyCity = new RegCity();
    protected UserMaster loggedMember = new UserMaster();
    protected int activityId;
    protected int agencyId;
    protected int cityId = 0;
    protected string remarks = string.Empty;
    protected string pnr;
    protected DateTime traveldate = new DateTime();
    protected Activity activity = new Activity();
    protected bool isServiceAgency;
    protected bool paymentDoneAgainstInvoice = false;
    protected string supplierName = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        int invoiceNumber = 0;
        if (Request["agencyId"] != null)
        {
            agencyId = Convert.ToInt32(Request["agencyId"]);
            agency = new AgentMaster(agencyId);
        }
        else
        {
            throw new ArgumentException("All the required values are not available in Request");
        }

        if (Request["bookingId"] != null && Request["agencyId"] != null)
        {
            activityId = Convert.ToInt32(Request["bookingId"]);
            agencyId = Convert.ToInt32(Request["agencyId"]);
        }
        else
        {
            invoiceNumber = Convert.ToInt32(Request["invoiceNumber"]);
        }
        activity.GetActivityForQueue(activityId);
        pnr = activity.TransactionHeader.Rows[0]["TripId"].ToString();

        DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
        DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

        if (cities != null && cities.Length > 0)
        {
            cityId = Convert.ToInt32(cities[0]["city_id"]);
        }
        // Reading agency information.
        agency = new AgentMaster(agencyId);
        isServiceAgency = true;// agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
        if (agency.City.Length == 0)
        {
            agencyCity.CityName = agency.City;
        }
        else
        {
            agencyCity = RegCity.GetCity(cityId);
        }
        // Formatting agency address for display.
        agencyAddress = agency.Address;
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        if (agency.Address != null && agency.Address.Length > 0)
        {
            agencyAddress += agency.Address;
        }
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        // Getting ticket list from DB


        traveldate = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]);
        passengers = activity.TransactionDetail;
        // Generating invoice.
        if (passengers.Rows.Count > 0)
        {
            if (invoiceNumber == 0)
            {
                invoiceNumber = Invoice.isInvoiceGenerated((int)activity.Id, ProductType.FixedDeparture);
            }
        }
        if (invoiceNumber > 0)
        {
            invoice = new Invoice();
            invoice.Load(invoiceNumber);
            supplierName = Invoice.GetSupplierByInvoiceNumber(invoiceNumber);
        }
        else
        {
            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice((int)activity.Id, string.Empty, (int)loggedMember.ID, ProductType.FixedDeparture, 1);
            invoice.Load(invoiceNumber);
        }
        paymentDoneAgainstInvoice = Invoice.IsPaymentDoneAgainstInvoice(invoiceNumber);
        LocationMaster location = new LocationMaster(Convert.ToInt32(activity.TransactionHeader.Rows[0]["LocationId"]));
        lblLocation.Text = location.Name;
        decimal TotalPremiumAmt = 0;
        decimal Markup = 0;
        decimal NetAmount = 0;
        for (int i = 0; i < activity.TransactionPrice.Rows.Count; i++)
        {
            NetAmount += Convert.ToDecimal(activity.TransactionPrice.Rows[i]["Amount"]) * Convert.ToInt32(activity.TransactionPrice.Rows[i]["LabelQty"]);
            Markup += Convert.ToDecimal(activity.TransactionPrice.Rows[i]["Markup"]) * Convert.ToInt32(activity.TransactionPrice.Rows[i]["LabelQty"]);
        }
        TotalPremiumAmt = Math.Ceiling(NetAmount + Markup);

        lblNetValue.Text = TotalPremiumAmt.ToString("N" + agency.DecimalValue);
       
        CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
        cancellationCharge.Load(activityId, ProductType.FixedDeparture);
        lblCancelAmountValue.Text = agency.AgentCurrency + " " + (cancellationCharge.AdminFee).ToString("N" + agency.DecimalValue);
        lblRefunded.Text = (TotalPremiumAmt - cancellationCharge.AdminFee).ToString("N" + agency.DecimalValue);
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
