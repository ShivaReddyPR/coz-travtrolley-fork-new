﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="DisplayPNRInformationGUI" Title="PNR Information" Codebehind="DisplayPNRInformation.aspx.cs" %>

<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">
    function ShowPreloader() {
        document.getElementById('MainDiv').style.display = "none";
        document.getElementById('PreLoader').style.display = "block";        
        return true;
    }
</script>
<%--<form id="form1" action="DisplayPNRInformation.aspx" runat="server">--%>
<div id="MainDiv">
<% if (itinerary != null && !string.IsNullOrEmpty(itinerary.PNR))
   { %>
   
<div>


    <div class="col-md-12 padding-0 marbot_10">                              

         
    <div class="col-md-4"><div>
            <span>PNR: <b> 
                <% = itinerary.PNR%>
            </b></span>
        </div> </div>
    
    
    <div class="col-md-4">  Booking on behalf of
            <b> <%= agency.Name%></b> </div>
            
            
    <div class="col-md-4"> 
     Origin ( <%= itinerary.Segments[0].Origin.CityName%> - <%= itinerary.Segments[0].Origin.CityCode%> ) To Destination (<%=  itinerary.Destination%>)
    
    </div>



    <div class="clearfix"></div>
    </div>
    
   
 
    <div class="col-md-12 padding-0 marbot_10">      
    
    <div class="col-md-8"> 
    
    
   <div  class="bg_white"> 
     
 <div class="ns-h3">Flight Details </div>  
<div class="table-responsive"> <table class="table">
                                        <tr>
                                            <th>
                                                Flight#</th>
                                            <th>
                                                Origin</th>
                                            <th>
                                                Destination</th>
                                            <th>
                                                Class</th>
                                            <th>
                                                Dep Date Time</th>
                                            <th>
                                                Arr Date Time</th> 
                                        </tr>
                                        <%  for (int i = 0; i < itinerary.Segments.Length; i++)
                                            {
                                                if (itinerary.Segments[i] != null)
                                                { 
                                                 %>
                                        <tr>
                                            <td>
                                                <%= itinerary.Segments[i].Airline%>
                                                -
                                                <%= itinerary.Segments[i].FlightNumber%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].Origin.AirportCode%>
                                                ,
                                                <%= itinerary.Segments[i].Origin.CityName%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].Destination.AirportCode%>
                                                ,
                                                <%= itinerary.Segments[i].Destination.CityName%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].BookingClass%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].DepartureTime.ToString("dd/MM/yyyy - hh:mm tt")%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].ArrivalTime.ToString("dd/MM/yyyy - hh:mm tt")%>
                                            </td>                                           
                                        </tr>
                                        <%  }
                                            } %>
                                    </table></div>
 
 
 <div class="clearfix"> </div>
 
 </div>
    
    </div>
    
    
   <div class="col-md-4"> 
   
   
   
   <div>
<div>
<table style="width:100%;">
<tr style="height:20px;">
<td style="width:33%; margin-left:5px;"></td>
<td style="width:33%; margin-right:5px; display:none;"><span class="fright bold"> Published</span></td>
<td style="width:33%; margin-right:5px;"><span style="display:none;" class="fright bold"> Offered</span></td>
</tr>
<%if (fareBreakDown != null && fareBreakDown.Length > 0)
  { %>
 <%int decimalPoint = 0;decimal outputVat = 0, baggageCharge = 0;

     if (Settings.LoginInfo.IsOnBehalfOfAgent)
     {
         decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
     }
     else
     {
         decimalPoint = Settings.LoginInfo.DecimalValue;
     }
     foreach(FlightPassenger pax in itinerary.Passenger)
     {
         outputVat += pax.Price.OutputVATAmount;
         baggageCharge += pax.Price.BaggageCharge;
     }
     %> 
<%  for (int i = 0; i < fareBreakDown.Length; i++)
    {
        double temp = 0;
        if (fareBreakDown[i] != null)
        {
                                %>
                                <tr style="height:20px;">
                                <td style="width:60%;">Fare:
                               <%=fareBreakDown[i].PassengerType%> X <%=fareBreakDown[i].PassengerCount %>
                                </td>
                                <td style="width:10%;">
                                <%=agency.AgentCurrency%> 
                                </td>
                                <td style="width:30%;">
                                <span class="fright">
                                
                                <% = Convert.ToDouble(fareBreakDown[i].BaseFare + Convert.ToDouble(fareBreakDown[i].AirlineTransFee) + Convert.ToDouble(fareBreakDown[i].HandlingFee)).ToString("N" + decimalPoint)%>
                                </span>
                                </td>
                                <td style="width:33%;">
                                <span class="fright" style="display:none;">
                                <%=agency.AgentCurrency%>
                                <% temp = fareBreakDown[i].SellingFare + Convert.ToDouble(fareBreakDown[i].AirlineTransFee) - ((fareBreakDown[i].TotalFare - fareBreakDown[i].BaseFare) / fareBreakDown[i].PassengerCount); %>
                                            <% = temp.ToString("N" + decimalPoint)%>
                                            </span>
                                </td>
                                </tr>
                                    <tr style="height:20px;">
                                    <td>Tax: </td>
                                    <td>
                                    <%=agency.AgentCurrency%>
                                    </td>
                                    <td>
                                    <span class="fright">
                                    
                                    <% temp = fareBreakDown[i].TotalFare - fareBreakDown[i].BaseFare; %>
                                            <% = temp.ToString("N" + decimalPoint)%> 
                                            </span>
                                            </td>
                                            
                                    <td>
                                    <span class="fright" style="display:none;">
                                    <%=agency.AgentCurrency%>
                                    <% temp = ((fareBreakDown[i].TotalFare - fareBreakDown[i].BaseFare) / fareBreakDown[i].PassengerCount); %>
                                            <% = temp.ToString("N" + decimalPoint)%>
                                            </span>
                                            </td>
                                    </tr>        
    
                                     <% 
    if (fareBreakDown[i].AdditionalTxnFee > 0)
    { %>
                                        
                                       <tr style="height:20px; display:none;">
                                       <td>S.Fee</td>
                                        <td>
                                    <%=agency.AgentCurrency%>
                                    </td>
                                       <td>
                                       <span class="fright">
                                       <%=agency.AgentCurrency%>
                                       <%= (fareBreakDown[i].AdditionalTxnFee / fareBreakDown[i].PassengerCount).ToString("N" + decimalPoint)%>
                                       </span>
                                       </td>
                                       <td>
                                       <span class="fright" style="display:none;">
                                       0.00</span></td>
                                       </tr>
                                    <%} %>
                                    <tr style="height:20px;display:none;">
                                    <td>O/C</td>
                                     <td>
                                    <%=agency.AgentCurrency%>
                                    </td>
                                    <td>
                                    <span class="fright">
                                    <%=agency.AgentCurrency%>
                                    <% = otherCharges[i].ToString("N" + decimalPoint)%></span></td>
                                    <td>
                                    <span class="fright" style="display:none;">
                                    <%=agency.AgentCurrency%>
                                    <% = otherCharges[i].ToString("N" + decimalPoint)%></span></td>
                                    </tr>
                                    <tr style="height:20px;">
                                    <td><span class="fleft bold">Total</span> </td>
                                     <td>
                                     <span class="fleft bold">
                                    <%=agency.AgentCurrency%></span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = Convert.ToDouble(fareBreakDown[i].TotalFare + Convert.ToDouble(fareBreakDown[i].AirlineTransFee) + Convert.ToDouble(fareBreakDown[i].AdditionalTxnFee) + Convert.ToDouble(otherCharges[i])).ToString("N" + decimalPoint)%>
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold" style="display:none;">
                                    <%=agency.AgentCurrency%>
                                    <% temp = fareBreakDown[i].SellingFare + Convert.ToDouble(fareBreakDown[i].AirlineTransFee) + Convert.ToDouble(otherCharges[i]); %>
                                            <% = temp.ToString("N" + decimalPoint)%>
                                            </span>
                                    </td>
                                    </tr>
                                   
                                
                                <%      }
    }
     %>
                           
</table>
</div>
<div style="background-color:#E0E0F8; border:dotted 1px grey; margin-left: 10px; margin-top: 10px; margin-right: 10px; margin-bottom:10px;">
    <div style="margin-left: 20px; margin-top: 15px; margin-right: 20px; width: 200px;">
        <table style="width: 100%;">
            <tr>
                <td style="width: 55%;">
                </td>
                <td style="width: 45%;">
                </td>
            </tr>
            
            <%  for (int i = 0; i < fareBreakDown.Length; i++)
                {
                    if (fareBreakDown[i] != null)
                    {
                                        %>
                                        <tr style="height:20px;">
                                        <td>
                                        <span class="fleft">
                                        <% = fareBreakDown[i].PassengerType.ToString()%>
                                                x
                                                <% = fareBreakDown[i].PassengerCount%>
                                                </span>
                                        </td>
                                    
                                        <td>
                                        <%=agency.AgentCurrency %>
                                        </td>
                                        <td>
                                        <span class="fright">
                                        
                                        <% = Convert.ToDouble(fareBreakDown[i].TotalFare + Convert.ToDouble(fareBreakDown[i].AdditionalTxnFee) + Convert.ToDouble(fareBreakDown[i].AirlineTransFee) + Convert.ToDouble(otherCharges[i] * fareBreakDown[i].PassengerCount)).ToString("N" + decimalPoint)%>
                                        </span>
                                        </td>
                                        </tr>
                                            
                                        <%      }
                }%>
  
                                    <tr style="height:20px; display:none;" >
                                    <td>
                                    <span class="fleft bold">
                                    Total
                                    </span>
                                    </td>
                                     <td>
                                     <span class="fleft bold">
                                        <%=agency.AgentCurrency %></span>
                                        </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = totalPublished.ToString("N" + decimalPoint)%>
                                    </span>
                                    </td>
                                    </tr>
                                    <tr style="height:20px; display:none;">
                                    <td>
                                    <span class="fleft bold">
                                    Comm. Earned
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = handlingCharge.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </span>
                                    </td>
                                    </tr>
                                     <%if (additionalTxnFee > 0)
                                          { %>
                                          <tr style="height:20px; display:none;">
                                          <td>
                                          <span class="fleft bold">
                                          Service Fee
                                          </span>
                                          </td>
                                          <td>
                                          <span class="fright bold">
                                          <% = additionalTxnFee.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                          </span>
                                          </td>
                                          </tr>
                                        <% } %>
                                   <tr style="height:20px; display:none;">
                                    <td>
                                    <span class="fleft bold">
                                    Comm. Reversed
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = reverseHandlingCharge.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </span>
                                    </td>
                                    </tr>
                                     <tr style="height:20px; display:none;">
                                    <td>
                                    <span class="fleft bold">
                                    TDS
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = tdsHc.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </span>
                                    </td>
                                    </tr>
                                    
                                    <tr style="height:20px; display:none;">
                                    <td>
                                    <span class="fleft bold">
                                    Service Tax
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = serviceTax.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </span>
                                    </td>
                                    </tr>
                                    
                                    <%if (totalTransFee > 0)
                                      { %>
                                              
                                              <tr style="height:20px;  display:none;">
                                              <td>
                                              <span class="fleft bold">
                                              Tra Fee
                                              </span>
                                              </td>
                                              <td>
                                              <span class="fright bold">
                                              <% = totalTransFee.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                              </span>
                                              </td>
                                              </tr>
                                            
                                            <%} %>
                                    <tr style="height:20px; display:none;">
                                    <td>
                                    <span class="fleft bold">
                                    PLB Earned
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = addHandlingCharge.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </span>
                                    </td>
                                    </tr>
                                    <tr style="height:20px; display:none;">
                                    <td>
                                    <span class="fleft bold">
                                    TDS on PLB
                                    </span>
                                    </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = tdsAddHandlingCharge.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    </span>
                                    </td>
                                    </tr>
            <%if(itinerary.IsLCC) { %>
            <tr style="height: 20px;">
                <td>
                    <span class="fleft bold">Baggage
                    </span>
                </td>
                <td>
                    <span class="fleft bold">
                        <%=agency.AgentCurrency %></span>
                </td>
                <td>
                    <span class="fright bold">
                        <% = baggageCharge.ToString("N" + decimalPoint)%>
                    </span>
                </td>
            </tr>
            <%} %>
            <tr style="height: 20px;">
                <td>
                    <span class="fleft bold">VAT
                    </span>
                </td>
                <td>
                    <span class="fleft bold">
                        <%=agency.AgentCurrency %></span>
                </td>
                <td>
                    <span class="fright bold">
                        <% = outputVat.ToString("N" + decimalPoint)%>
                    </span>
                </td>
            </tr>
                                    <tr style="height:20px;">
                                    <td>
                                    <span class="fleft bold">
                                    Total
                                    </span>
                                    </td>
                                     <td>
                                     <span class="fleft bold">
                                        <%=agency.AgentCurrency %></span>
                                        </td>
                                    <td>
                                    <span class="fright bold">
                                    <% = totalAgentprice.ToString("N" + decimalPoint)%>
                                    </span>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>&nbsp;</td>
                                    </tr>
                                 <%   }   %>
                                    <%else if (ShowErrorMessage.Length > 0)
    {
       %>
       
     <%Response.Redirect("DisplayPNR.aspx?Error=" + ShowErrorMessage, false); %>
       
       <%} %>
        </table>
    </div>
    </div>
</div>
   
   </div>
    
    
     <div class="clearfix"> </div>
    </div>


         
   
  
 
 
      <div class="clearfix"> </div>
 
</div>

  <div> 

<div class=" col-md-6">

  <div class="bg_white bor_gray"> 
                          <div class="ns-h3">
                                Passengers</div>
                            
                                <% for (int i = 0; i < itinerary.Passenger.Length; i++)
                                   { %>
                          
                           <div class=" pad_10">
                         
                           <label id="Title<%= i %>" ><%= itinerary.Passenger[i].Title%> </label>
                           <label id="FirstName<%= i %>" ><%= itinerary.Passenger[i].FirstName%></label>
                           <label id="LastName" ><%= itinerary.Passenger[i].LastName%></label>
                           <span style="margin-left:5px;">  <% = itinerary.Passenger[i].Type.ToString()%> </span>
                            </div>
                            
                                <% } %>
                               
                       <div class="clearfix"></div>
                            
                            </div>
                            
                            
                        </div>


 <div class="col-md-6" >
 <div class="bg_white bor_gray"> 
    <div class="ns-h3">Select Mode of Payment </div>
        <table width="100%">
            <tr>
                <td align="right">
                    Payment mode:
                </td>
                <td>
                    <asp:DropDownList ID="ddlPaymentType" runat="server" Width="100px">
                        <asp:ListItem Selected="True" Text="On Account" Value="Credit"></asp:ListItem>
                        <asp:ListItem Text="Credit Card" Value="Card"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td colspan="2">
            &nbsp;
            </td>
            </tr>
            
        </table>
    </div>
    </div>
<br />
<div class="col-md-6"> <asp:Button CssClass="btn but_b pull-right" ID="btnSave" runat="server" Text="Save Booking"
                    OnClick="btnSave_Click" OnClientClick="return ShowPreloader();" />
                    <asp:Button ID="btnHold" CssClass="btn but_b pull-right" runat="server" Text="Hold" Visible="false" OnClick="btnHold_Click" OnClientClick="return ShowPreloader();"/></div>
 
 
 <div class="clearfix"></div>

</div>

<% }else { %>
    Itinerary details not found
    <%} %>
</div>
<%--</form>--%>
<div id="PreLoader" style="display:none;top:0px;margin-top:0px">
        <div class="loadingDiv" style="top:230px;">
            <div id="loadingDivision">
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
            
                <strong>Awaiting confirmation</strong>
               
                </div>
            <div style="font-size: 21px; color: #3060a0">
                       
                 <strong style="font-size: 14px; color: black" >do not click the refresh or back button as the transaction may be interrupted or terminated.</strong>
            
                    </div>
        </div>
    </div>
    <div id='EmailDiv' runat='server' style='width: 100%; display: none;'>
        <%//if (isBookingSuccess)
            {
                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                if (ticket != null && ticket.Length > 0)
                {
                    ptcDetails = ticket[0].PtcDetail;
                }
                else// For Hold Bookings
                {
                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId);
                }
                AgentMaster agency = new AgentMaster(itinerary.AgencyId);
        %>
        <%//Show the Agent details for First Email only. In case of Corporate booking second & third 
              //emails need not show Agent details in the email body
              //if (emailCounter <= 1)
                { %>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                    <asp:Image ID='imgLogo' runat='server' />
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 70%;"></td>
                <td align="right">
                    <label style="padding-right: 20%"><strong>Agent Name:</strong></label></td>
                <td align="left">
                    <label style="padding-right: 10%"><strong><%=agency.Name%></strong></label></td>
            </tr>
            <tr>
                <td style="width: 70%;"></td>
                <td align="right">
                    <label style="padding-right: 20%"><strong>Phone No:</strong></label></td>
                <td align="left">
                    <label style="padding-right: 10%"><strong><%=agency.Phone1%></strong></label></td>
            </tr>
        </table>
        <% }
            for (int count = 0; count < itinerary.Segments.Length; count++)
            {
                int paxIndex = 0;
                if (Request["paxId"] != null)
                {
                    paxIndex = Convert.ToInt32(Request["paxId"]);
                }

                List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                if (ptcDetails != null)
                {
                    ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == itinerary.Segments[count].SegmentId; });
                }
        %>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    <div style='border: solid 1px #000'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td width='25%' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>Flight :
                                            <%=itinerary.Segments[count].Origin.CityName%>
                                            to
                                            <%=itinerary.Segments[count].Destination.CityName%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>RESERVATION</strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>
                                            <%=itinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'><strong>Airline Ref : 
                                                <%=(itinerary.Segments[count].AirlinePNR == null || itinerary.Segments[count].AirlinePNR == "" ? (itinerary.PNR.Split('|').Length > 1 ? itinerary.PNR.Split('|')[itinerary.Segments[count].Group] : itinerary.PNR) : (itinerary.Segments[count].AirlinePNR.Split('|').Length > 1 ? itinerary.Segments[count].AirlinePNR.Split('|')[itinerary.Segments[count].Group] : itinerary.Segments[count].AirlinePNR)) %>
                                                </strong></font>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd; margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='padding-left: 20px'>
                                                <strong>Passenger Name's :</strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if(ticket != null && ticket.Length > 0){ %>
                                            <strong>Ticket No </strong>
                                            <%}else { // For Hold Bookings%>
                                            <strong>PNR </strong>
                                            <%} %>

                                        </td>
                                        <td style='width: 25%; float: left; height: 20px; line-height: 20px;'>
                                            <strong>Baggage </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd; margin-top: 2px; height: 20px; line-height: 20px;'>
                                    <tr>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <%if (ticket != null && ticket.Length > 0)
                                                  { %>
                                                <strong>Ticket Date:</strong>
                                                <%=ticket[0].IssueDate.ToString("dd/MM/yyyy")%>
                                                <%}
                                                  else
                                                  {//for Corporate HOLD Booking %>
                                                <strong>Booking Date:</strong>
                                                <%=itinerary.CreatedOn.ToString("dd/MM/yyyy")%>
                                                <%} %>
                                            </label>
                                        </td>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <strong>Trip ID :</strong>
                                                <%=booking.BookingId%></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%for (int j = 0; j < itinerary.Passenger.Length; j++)
              { %>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd; margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='margin-left: 20px; padding: 2px; background: #EBBAD9;'>
                                                <strong>
                                                    <%=itinerary.Passenger[j].Title + " " + itinerary.Passenger[j].FirstName + " " + itinerary.Passenger[j].LastName%>
                                                </strong>
                                            </label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if(ticket != null && ticket.Length                                          > 0){ %>
                                            <%=(ticket[j].TicketNumber.Split('|').Length > 1 ? ticket[j].TicketNumber.Split('|')[itinerary.Segments[count].Group] : ticket[j].TicketNumber) %>
                                            <%}else{ //for Corporate HOLD Booking%>
                                            <%=(itinerary.PNR.Split('|').Length > 1 ? itinerary.PNR.Split('|')[itinerary.Segments[count].Group] : itinerary.PNR)%>
                                            <%} %>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px; text-align: right;'>

                                            <%if (itinerary.FlightBookingSource == BookingSource.UAPI || (itinerary.FlightBookingSource == BookingSource.TBOAir && !(itinerary.IsLCC)))
                                                   {
                                                       if (Request["bkg"] == null || Request["bkg"] != null)
                                                       {
                                                           if (ptcDetail.Count > 0)
                                                           {
                                                               string baggage = "";
                                                               foreach (SegmentPTCDetail ptc in ptcDetail)
                                                               {
                                                                   switch (itinerary.Passenger[j].Type)
                                                                   {
                                                                       case PassengerType.Adult:
                                                                           if (ptc.PaxType == "ADT")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                       case PassengerType.Child:
                                                                           if (ptc.PaxType == "CNN")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                       case PassengerType.Infant:
                                                                           if (ptc.PaxType == "INF")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                   }
                                                               }

                                                               if (!string.IsNullOrEmpty(baggage) && baggage.Length > 0 && !baggage.Contains("Bag") && !baggage.ToLower().Contains("piece") && !baggage.ToLower().Contains("unit") && !baggage.ToLower().Contains("units"))
                                                               {%>
                                            <%=(baggage.ToLower().Contains("kg") ? baggage : baggage + " Kg")%>

                                            <%}
                                                               else
                                                               {%>
                                            <%=(string.IsNullOrEmpty(baggage) ? "Airline Norms" : baggage)%>
                                            <%}
                                                        }
                                                        else if (itinerary.Passenger[j].Type != PassengerType.Infant)
                                                        {%>
                                            Airline Norms
                                                        <%}
                                                        //End PtcDetail.Count
                                                    }//End Request("bkg"]
                                                }
                                                else if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.PKFares|| itinerary.FlightBookingSource == BookingSource.SpiceJetCorp|| itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                                                {
                                                    if (itinerary.Passenger[j].Type != PassengerType.Infant)
                                                    { %>
                                            <%=itinerary.Passenger[j].BaggageCode.Split(',')[itinerary.Segments[count].Group]%>
                                            <%}
                                                   }
                                                       else if (itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                                                   {%>

                                            <%=itinerary.Passenger[j].BaggageCode.Split(',')[itinerary.Segments[count].Group]%>

                                            <%}                                                  
                                                   
                                                   else if (itinerary.FlightBookingSource == BookingSource.TBOAir && (itinerary.IsLCC))
                                                   {
                                                       if (itinerary.Passenger[j].Type != PassengerType.Infant)
                                                       {
                                                           string strBaggage = string.Empty;
                                                           if (itinerary.Passenger[j].BaggageCode != string.Empty && itinerary.Passenger[j].BaggageCode.Split(',').Length > 1)
                                                           {
                                                               strBaggage = itinerary.Passenger[j].BaggageCode.Split(',')[count];
                                                           }
                                                           else if (itinerary.Passenger[j].BaggageCode.Split(',').Length <= 1)
                                                           {
                                                               strBaggage = itinerary.Passenger[j].BaggageCode;
                                                           }
                                                           else
                                                           {
                                                               strBaggage = "Airline Norms";
                                                           }
                                            %>
                                            <%=strBaggage%>
                                            <%}
                                                   } %>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd; margin-top: 2px; height: 20px; line-height: 20px'>
                                    <tr>
                                        <td style='width: 50%'>
                                            <label style='padding-left: 20px'>
                                                <strong>PNR No : </strong>
                                                <%=(itinerary.PNR.Split('|').Length > 1 ? itinerary.PNR.Split('|')[itinerary.Segments[count].Group] : itinerary.PNR)%></label>
                                        </td>
                                        <%if(!string.IsNullOrEmpty(itinerary.TripId)){ %>
                                        <td style='width: 50%; text-align: right'>
                                            <strong>Corporate Booking Code</strong>
                                            <%=itinerary.TripId %>
                                        </td>
                                        <%} %>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left'>
                                <label style='padding-left: 20px'>
                                    Date:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Status:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 40%; float: left;'>
                                <label style='padding-left: 20px; color: #08bd48;'>
                                    <strong>
                                        <%=booking.Status.ToString()%></strong></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Departs:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].DepartureTime.ToString("hh:mm tt")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Arrives:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].ArrivalTime.ToString("hh:mm tt")%>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airline:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(itinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Flight:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Airline + " " + itinerary.Segments[count].FlightNumber%>
                                    <%try
                                       {  //Loading Operating Airline and showing
                                           if (!itinerary.IsLCC && itinerary.Segments[count].OperatingCarrier != null && itinerary.Segments[count].OperatingCarrier.Length > 0)
                                           {
                                               string opCarrier = itinerary.Segments[count].OperatingCarrier;
                                               CT.Core.Airline opAirline = new CT.Core.Airline();
                                               if (opCarrier.Split('|').Length > 1)
                                               {
                                                   opAirline.Load(opCarrier.Split('|')[0]);
                                               }
                                               else
                                               {
                                                   opAirline.Load(opCarrier.Substring(0, 2));
                                               } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                       }
                                       catch { } %>
                                </label>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    From:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Origin.CityName + ", " + itinerary.Segments[count].Origin.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Origin.AirportName + ", " + itinerary.Segments[count].DepTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    To:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Destination.CityName + ", " + itinerary.Segments[count].Destination.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Destination.AirportName + ", " + itinerary.Segments[count].ArrTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Class:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].CabinClass%></label>
                                <%if (itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl|| itinerary.FlightBookingSource == BookingSource.SpiceJetCorp|| itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                                      {%>
                                <label style='padding-left: 20px'>
                                    <label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 25%;'>
                                        Fare Type :
                                    </label>
                                    <%=itinerary.Segments[count].SegmentFareType%></label>
                                <%} %>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Duration:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Duration%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <%} %>
        </table>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0;

                        if (ticket != null && ticket.Length > 500)
                        {
                            for (int k = 0; k < ticket.Length; k++)
                            {
                                AirFare += ticket[k].Price.PublishedFare;
                                Taxes += ticket[k].Price.Tax + ticket[k].Price.Markup;
                                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticket[k].Price.AdditionalTxnFee + ticket[k].Price.OtherCharges + ticket[k].Price.SServiceFee + ticket[k].Price.TransactionFee;
                                }
                                Baggage += ticket[k].Price.BaggageCharge;
                                MarkUp += ticket[k].Price.Markup;
                                Discount += ticket[k].Price.Discount;
                                AsvAmount += ticket[k].Price.AsvAmount;
                                outputVAT += ticket[k].Price.OutputVATAmount;
                            }
                            if (ticket[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticket[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < itinerary.Passenger.Length; k++)
                            {
                                AirFare += itinerary.Passenger[k].Price.PublishedFare;
                                Taxes += itinerary.Passenger[k].Price.Tax + itinerary.Passenger[k].Price.Markup;
                                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += itinerary.Passenger[k].Price.AdditionalTxnFee + itinerary.Passenger[k].Price.OtherCharges + itinerary.Passenger[k].Price.SServiceFee + itinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += itinerary.Passenger[k].Price.BaggageCharge;
                                MarkUp += itinerary.Passenger[k].Price.Markup;
                                Discount += itinerary.Passenger[k].Price.Discount;
                                AsvAmount += itinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += itinerary.Passenger[k].Price.OutputVATAmount;
                                if (itinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (itinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                    %>
                    <% if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId<=1)
                              {
                                  
                    %>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'></td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Taxes.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp )
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>VAT</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=outputVAT.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>:
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                    <%if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                       { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
    else
    { %>
                                                                    <%=((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <%} %>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <%} %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

