<%@ Page Language="C#" AutoEventWireup="true" Inherits="CreateHotelInvoiceGUI" Codebehind="CreateHotelInvoice.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Invoice</title>
    
    

    
    <link rel="stylesheet" href="css/main-style.css" />
    <script type="text/javascript">
var Ajax;

if (window.XMLHttpRequest) {
    Ajax = new XMLHttpRequest();
}
else {
    Ajax = new ActiveXObject('Microsoft.XMLHTTP');
}
    function printPage() {
        document.getElementById('btnPrint').style.display = "none";
        window.print();
//        var divToPrint = document.getElementById('middle-container');
//           var popupWin = window.open('', 'HotelInvoice', 'width=850,height=500');
//           popupWin.document.open();
//           popupWin.document.write('<html><head><style media=\"screen\">.hotlinvoice td { line-height:22px; font-size:13px; }.hotlinvoice h3 {  background:url(images/cozmovisa-sprite.jpg) repeat-x; height:20px;margin-top:10px; color:#fff; padding-left:10px; }</style></head><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
//            popupWin.document.close(); 
        setTimeout('showButtons()', 1000);
    }
    function showButtons() {
        document.getElementById('btnPrint').style.display = "block";
    }

    /* function HideEmailDiv() {
    document.getElementById('emailSent').style.display='none';
    document.getElementById('emailBlock').style.display = 'none';
    }

    function ShowEmailDivD() {
    document.getElementById("addressBox").value='';
    document.getElementById('messageText').innerHTML = '';
    document.getElementById('emailBlock').style.display = 'block';
    document.getElementById('addressBox').focus();
    }
    
    function SendMail()
    {
    var ValidEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z][a-zA-Z]+)$/;
    var addressList=document.getElementById('addressBox').value;  
    var hotelId=<%#itinerary.HotelId %>  ;
    //if((addressList)=='')
    //{
    //document.getElementById('emailSent').style.display='block';
    //document.getElementById('messageText').innerHTML='Please fill the email address';
    //return;
    //}
    //if(!ValidEmail.test((addressList)))
    //{
    //document.getElementById('emailSent').style.display='block';
    //document.getElementById('messageText').innerHTML='Please fill correct email address';
    //return;
    //}
    //var invoiceNumber=document.getElementById('invoiceNo').value;
    //var paramList ='invoiceNumber=' + invoiceNumber;
    //paramList +=  "&addressList=" + addressList;
    //paramList+="&hotelId="+hotelId;
    //paramList += "&isInvoice=true";
    //var url = "EmailHotelItineary.aspx";
    
    //Ajax.onreadystatechange = DisplayMessage;
    //Ajax.open('POST', url);
    //Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    //Ajax.send(paramList);
    //}
    //function DisplayMessage()
    //{  
    //if (Ajax.readyState == 4) {
    //if (Ajax.status == 200) {
    //HideEmailDiv();
    //document.getElementById('emailSent').style.display='block';
    //document.getElementById('messageText').innerHTML='Email Sent Successfully';
    //}
    //}
    //}*/

    </script>
    
          <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="<%=Request.Url.Scheme%>://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="<%=Request.Url.Scheme%>://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

       <!-- manual css -->
   <link href="css/override.css" rel="stylesheet">

<!-- Bootstrap Core JavaScript -->
 <script src="Scripts/bootstrap.min.js"></script>





</head>
<body>
<div id="">
    <form id="form1" runat="server">
    
<div>
<div style="position:absolute; right:700px; top:530px; display:none"  id="emailBlock">
                    
                    
                    <div style="border: solid 4px #ccc; width:200px" class="ShowMsgDiv"> 
                    <div class="showMsgHeading"> Enter Email Address <a style=" float:right; padding-right:10px" class="closex" href="javascript:HideEmailDiv()">X</a></div>
                    
                           <div style=" padding:10px">
                                    <input style=" width:100%; border: solid 1px #7F9DB9" id="addressBox" name="" type="text" /></div>
                               <div style=" padding-left:10px; padding-bottom:10px">
                                    <input onclick="SendMail()" type="button" value="Send mail" />
                                    
                                    </div>
                                    
                                    
                                    
                    </div>
                    

</div>

</div>
<div>

<input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 








<table id="tblInvoice"  width="100%" border="0" cellspacing="0" cellpadding="0">
  
  <tr> 
   <td colspan="2">
   &nbsp;
   
   </td>
  </tr>
  
  <tr>
    <td colspan="2">

    
    
    
    
    <div class="col-md-4"> <asp:Image ID="imgLogo"  runat="server" AlternateText="AgentLogo" ImageUrl="" style="max-width: 159px;max-height: 51px;" /></div>
    <div class="col-md-4"> <center> <h1 style=" font-size:26px">Invoice</h1>
        <div class="small">
             <%if (location.CountryCode == "IN")
                 { %>
             GSTIN:100019554300003
             <%}
    else {%>
             TRN:100019554300003
             <%} %>
        </div>
                           </center>  </div>
    <div class="col-md-4"> 
    
    <label class=" pull-right"> <input style=" width:80px"  id="btnPrint" onclick="return printPage();" type="button"
                        value="Print Invoice" /></label>
    
    </div>
    
    
    



    </td>
  </tr>
 
 
  <tr>
    <td colspan="2">
    
     <div class="col-md-6"> 
                                <table class="hotlinvoice" width="100%">
                                    <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Client Details</h3>                                            </td>
                                        </tr>
                                     <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Name: </b></td>
                                            <td style="vertical-align: top;"> <% = agency.Name %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Code: </b></td>
                                            <td style="vertical-align: top;"> <% = agency.Code %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Address: </b></td>
                                            <td style="vertical-align: top;"><% = agencyAddress %></td>
                                        </tr>
                                            <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>TelePhone No: </b></td>
                                                <td style="vertical-align: top;">
                                                    <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                        {%>
                                                    Phone:
                                                    <% = agency.Phone1%>
                                                    <%  }
                                                        else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                        { %>
                                                    Phone:
                                                    <% = agency.Phone2%>
                                                    <%  } %>
                                                    <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                        { %>
                                                    Fax:
                                                    <% = agency.Fax %>
                                                    <%  } %>                                                </td>
                                        </tr>
                                        <%if (location.CountryCode == "IN")
                                            { %>
                                        <tr>
                                            <td style="vertical-align: middle; width: 120px;"><b>GST Number: </b></td>

                                            <td style="vertical-align: middle;"><% = location.GstNumber %></td>
                                        </tr>
                                        <%}
                                        else if (agency.Telex != null && agency.Telex.Length > 0)
                                        {%>
                                        <tr>
                                            <td style="vertical-align: middle; width: 120px;"><b>TRN Number: </b></td>

                                            <td style="vertical-align: middle;"><% = agency.Telex %></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>       
                                
                                
                                </div>                    
                          
                          <div class="col-md-6"> 
                                <table class="hotlinvoice" width="100%" align="right" style="font-size: 12px;font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px; font-family: 'Arial';">Invoice Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Invoice No: </b></td>
                                            <td style="vertical-align: top;"><%=invoice.CompleteInvoiceNumber%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Invoice Date: </b></td>
                                            <td style="vertical-align: top;"><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Confirmation No: </b></td>
                                            <td style="vertical-align: top;"><% = confNo %></td>
                                        </tr>
                                            <tr>
                                             <%  
                                                 int bookingID = 0;
                                                 bookingID = BookingDetail.GetBookingIdByProductId(hotelId, ProductType.Hotel); 
                                            %>
                                            <td style="vertical-align: top;width: 120px;"><b>CozmoVocher No: </b></td>
                                            <td style="vertical-align: top;">HTL-CT-<%=bookingID%></td>
                                        </tr>
                                    </tbody>
                                </table>   
                                
                                  </div>              
    
    
    
    </td>
                  
    
     </tr>
  
  
   <tr>
   <td colspan="2">
    
      <div class="col-md-6"> 
    <table width="100%" class="hotlinvoice"  style="font-size: 12px;font-family: 'Arial'">
                              <tbody>
                                <tr>
                                  <td colspan="4" style="vertical-align: top;">
                                  
                                  <h3 style="font-size: 16px;font-family: 'Arial';">Booking Details</h3></td>
                                </tr>
                                
                                <tr>
                                  <td style="vertical-align: top;"><b>Booking Date: </b></td>
                                  <td style="vertical-align: top;"> <%=itinerary.CreatedOn.ToString("dd MMM yy") %></td>
                                </tr>
                             
                                <tr>
                                  <td style="vertical-align: top;"><b>Check In: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.StartDate.ToString("dd MMM yy") %></td>
                                 
                                  <td style="vertical-align: top;"><b>Check Out: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.EndDate.ToString("dd MMM yy") %></td>
                                </tr>
                                
                                <tr>
                                  <%  
                                                 System.TimeSpan diffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
                                            %>
                                  <td style="vertical-align: top;"><b>No. of Night(s): </b></td>
                                  <td style="vertical-align: top;"><%= diffResult.Days%></td>
                                  <td style="vertical-align: top;"><b>No. of Room(s): </b></td>
                                  <td style="vertical-align: top;"><% = itinerary.NoOfRooms%></td>
                                </tr>
                                
                                
                              </tbody>
                            </table>
      
      
      </div>
      
            <div class="col-md-6"> <table class="hotlinvoice" width="100%" align="right" style="font-size: 12px;font-family: 'Arial'">
      <tbody>
                                        <tr>
                                            <td style="vertical-align: top;" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Hotel Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Hotel Name: </b></td>
                                            <td style="vertical-align: top;"> <%=itinerary.HotelName%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Address1: </b></td>
                                            <td style="vertical-align: top;"> <%=itinerary.HotelAddress1%></td>
                                        </tr>
                                           <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>City: </b></td>
                                            <td style="vertical-align: top;">  <%=itinerary.CityRef %></td>
                                        </tr>
                                  </tbody>
                                </table></div>
            
    
      
       </td>
  </tr>
  
  
  
  <tr>
   <td colspan="2">
   
            <div class="col-md-6">  <table class="hotlinvoice" width="100%" style="font-size: 12px; font-family: 'Arial'">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4">
                                                                <h3 style="font-size: 16px; font-family: 'Arial';">
                                                                    Lead Passenger Details</h3>
                                                            </td>
                                                            </tr>
                                        
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pax Name: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Firstname %> <%= itinerary.HotelPassenger.Lastname %>
                                            </td>
                                             </tr>
                                             <%if(agency.AddnlPaxDetails=="Y" )
                                             {%>
                                       <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Department: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Department%> 
                                            </td>
                                             </tr>
                                           
                                              <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Division: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Division%> 
                                            </td>
                                             </tr>
                                             <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Employee ID: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.EmployeeID%> 
                                            </td>
                                             </tr>
                                              <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Employee Name: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Employee%> 
                                            </td>
                                             </tr>
                                              <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Purpose of Travel: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Purpose%> 
                                            </td>
                                             </tr>
                                             <%} %>
                                             </tbody>
                                             </table>
                                             </div>
                                             
                                              <div class="col-md-6">  
                                             <table class="hotlinvoice" width="100%" style="font-size: 12px; font-family: 'Arial'">
                            <tbody>
                              <% decimal totalPrice = 0;
                                  decimal handlingCharge = 0;
                                  decimal serviceTax = 0;
                                  decimal tdsCommission = 0;
                                  decimal tdsPLB = 0;
                                  decimal plb = 0;
                                  decimal transactionFee = 0;
                                  PriceAccounts price = new PriceAccounts();
                                  price = hRoomList[0].Price;
                                  decimal rateofExchange = price.RateOfExchange;
                                  decimal markup = 0, addlMarkup = 0, discount = 0, b2cMarkup = 0;
                                  decimal outPutVat = 0, inPutVat = 0;
                                  string symbol = Util.GetCurrencySymbol(price.Currency);
                                  decimal CGST = 0, SGST = 0, IGST = 0;
                                  int CGSTPer = 0, IGSTPer = 0, SGSTPer = 0;
                                  if (hRoomList[0].Price.AccPriceType == PriceType.PublishedFare)
                                  {
                                      for (int k = 0; k < hRoomList.Length; k++)
                                      {
                                          totalPrice = totalPrice + (hRoomList[k].Price.PublishedFare + hRoomList[k].Price.OtherCharges);
                                          markup += hRoomList[k].Price.Markup;
                                          b2cMarkup += hRoomList[k].Price.B2CMarkup;
                                          //handlingCharge = handlingCharge + (hRoomList[k].Price.AgentCommission);
                                          //serviceTax = serviceTax + (hRoomList[k].Price.SeviceTax);
                                          //tdsCommission = tdsCommission + hRoomList[k].Price.TdsCommission;
                                          //plb = plb + hRoomList[k].Price.AgentPLB;
                                          //tdsPLB = tdsPLB + hRoomList[k].Price.TDSPLB;
                                          //transactionFee = transactionFee + hRoomList[k].Price.TransactionFee;

                                      }
                                  }
                                  else if (hRoomList[0].Price.AccPriceType == PriceType.NetFare)
                                  {
                                      for (int k = 0; k < hRoomList.Length; k++)
                                      {
                                          totalPrice = totalPrice + (hRoomList[k].Price.NetFare + hRoomList[k].Price.OtherCharges );
                                          discount += hRoomList[k].Price.Discount;
                                          markup += hRoomList[k].Price.Markup;
                                          addlMarkup = hRoomList[0].Price.AsvAmount;
                                          b2cMarkup += hRoomList[k].Price.B2CMarkup;
                                          if (location.CountryCode == "IN") //If Booking Login Country IN is there Need to load GST
                                          {
                                              List<GSTTaxDetail> taxDetList = GSTTaxDetail.LoadGSTDetailsByPriceId(hRoomList[k].Price.PriceId);
                                              if (taxDetList != null && taxDetList.Count > 0)
                                              {
                                                  CGST += taxDetList.Where(p => p.TaxCode == "CGST").Sum(p => p.TaxAmount);
                                                  SGST += taxDetList.Where(p => p.TaxCode == "SGST").Sum(p => p.TaxAmount);
                                                  IGST += taxDetList.Where(p => p.TaxCode == "IGST").Sum(p => p.TaxAmount);
                                                  GSTTaxDetail objTaxDet = taxDetList.Where(p => p.TaxCode == "CGST").FirstOrDefault();
                                                  if (objTaxDet != null)
                                                  {
                                                      CGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                                  }
                                                  objTaxDet = taxDetList.Where(p => p.TaxCode == "SGST").FirstOrDefault();
                                                  if (objTaxDet != null)
                                                  {
                                                      SGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                                  }
                                                  objTaxDet = taxDetList.Where(p => p.TaxCode == "IGST").FirstOrDefault();
                                                  if (objTaxDet != null)
                                                  {
                                                      IGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                                  }
                                              }
                                          }
                                          else
                                          {
                                              outPutVat += hRoomList[k].Price.OutputVATAmount;
                                              inPutVat += hRoomList[k].Price.InputVATAmount;
                                          }
                                          //serviceTax = serviceTax + (hRoomList[k].Price.SeviceTax);
                                          //tdsCommission = tdsCommission + hRoomList[k].Price.TdsCommission;
                                          //plb = plb + hRoomList[k].Price.AgentPLB;
                                          //tdsPLB = tdsPLB + hRoomList[k].Price.TDSPLB;
                                          //transactionFee = transactionFee + hRoomList[k].Price.TransactionFee;
                                      }

                                      if(agencyType==AgentType.BaseAgent || agencyType==AgentType.Agent)
                                      {
                                          totalPrice -= inPutVat;
                                      }
                                      else
                                      {
                                          inPutVat = 0;
                                      }
                                  }
                                  //if (Settings.LoginInfo.AgentId > 1)
                                  //{
                                  //    totalPrice = totalPrice + markup;
                                  //}

                                  %>
                              <tr>
                                <td style="vertical-align: top; width:50%;" colspan="2"><h3 style="font-size: 16px;font-family: 'Arial';">Rate Details</h3></td>
                              </tr>                                                      
                                  <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Net Amount:</b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency  %>
                                        <% if (agencyId <= 1)
                                            {%>
                                           <%=((totalPrice)).ToString("N" + agency.DecimalValue)%>
                                           <%}
                                               else {%>
                                           <%=(Math.Ceiling(totalPrice + markup)).ToString("N" + agency.DecimalValue)%>
                                           <%} %>
                                        
                                    </td>
                                </tr>                                                                               
                                <tr>
                                <%if (agencyId == 1)
                                    { %>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Markup: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency %>
                                        <%=((markup) ).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                    <%} %>
                                </tr>
                                <%if (discount > 0)
                                  { %>
                                <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Discount:</b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency%>
                                        <%=((discount) * rateofExchange).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                </tr>
                                <%} %>
                                <%if (addlMarkup > 0)
                                  {%>
                                <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Addl Markup: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency  %>
                                        <%=(addlMarkup).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                </tr>
                                <%} %>
                                 <%if ((agencyType == AgentType.BaseAgent || agencyType == AgentType.Agent) && location.CountryCode !="IN"){%>
                                <tr>
                                    <td style="vertical-align: top;width: 120px;"><b>In Vat: </b></td>  
                                     <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(inPutVat).ToString("N"+agency.DecimalValue)%></td>                                 
                                </tr>                               
                                <% }%>                             
                                 <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Gross Amount:  </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(Math.Ceiling((totalPrice + markup+inPutVat) - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee - discount + addlMarkup)).ToString("N" + agency.DecimalValue)%></td>
                              </tr>                            
                               <%if (location.CountryCode != "IN")
                                       {
                                           if (agencyType == AgentType.BaseAgent || agencyType == AgentType.Agent)
                                           {%>
                                  <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Out Vat: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(outPutVat).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                                <%else
                                       {%>
                                  <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Vat: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(outPutVat).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%}
                                    }
                                    else { %> 
                                <%if(IGST > 0) { %>    
                                    <tr>
                                <td style="vertical-align: top;width: 120px;"><b>IGST(<%=IGSTPer %>) %: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(IGST).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                                   <%if(CGST > 0) { %>    
                                    <tr>
                                <td style="vertical-align: top;width: 120px;"><b>CGST(<%=CGSTPer %>) %: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(CGST).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                                   <%if(SGST > 0) { %>    
                                    <tr>
                                <td style="vertical-align: top;width: 120px;"><b>SGST(<%=SGSTPer %>) %: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(SGST).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                                <%} %>                     
                              <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Total Amount: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency%> <%=Math.Ceiling(Math.Ceiling(totalPrice + markup+inPutVat) + serviceTax + tdsCommission + transactionFee - discount + addlMarkup + Math.Ceiling(outPutVat)+Math.Ceiling(IGST+CGST+SGST)).ToString("N"+agency.DecimalValue)%></td>
                              </tr>                      
                            </tbody>
                          </table>
                          </div>
                          </td>
                          </tr>
                                    </tbody>
                                </table>
                                
                                
                                </td>
                                </tr>
                                <tr>
   
    
    
    <td colspan="2">
    
    
    <div class="col-md-12"> 
    
    <table border="0" class="hotlinvoice" width="30%" style="font-family: 'Arial'">
<tr>
                 <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                 <td style="vertical-align: top; width: 100px;">
                     <b>invoiced By: </b>
                 </td>
                 <td style="vertical-align: top;"  align="left">
                     Cozmo Travel
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Created By: </b>
                 </td>
                 <td style="vertical-align: top;" align="left">
                     <%= member.FirstName + " " + member.LastName%>
                 </td>
             </tr>
             <tr>
             <td style="vertical-align: top; width: 100px;">
             <b>Location: </b>
             </td>
             <td style="vertical-align: top;" align="left">
             <%=location.Name %>
             </td>
             </tr>
        <%--     <tr>
                 <td>
                     <%if (Request.QueryString["hotelId"] == null)
                       { %>
                     <a href="InvoiceHistory.aspx">Back</a>
                     <%}
                       else
                       { %>
                     <a href="HotelBookingQueue.aspx">Back</a>
                     <%} %>
                 </td>
             </tr>--%>
             <tr>
             <td style="width:130px">
             <div class="width-100 fleft text-right" style="display:none"><img src="images/Email1.gif"  />
           <a href="javascript:ShowEmailDivD()">Email Invoice</a>
                                     
        
                       
          </div>
          </td>
          <td style="width:160px">
          <span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;"></span> 
              </td>
                    <td>
                        <div class="email-message-parent" style="display:none;" id="emailSent">
                    <span  style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                    </span>
                    
           </div>
             </td>
             </tr>
             </table>
    
    </div>
    
    
     </td>
     <td></td>
     </tr>
         </table>
    
</div>
    </form>
    </div>
</body>
</html>
