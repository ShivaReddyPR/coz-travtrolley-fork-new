<%@ Page Language="C#" AutoEventWireup="true" Inherits="HotelDetailAjax" Codebehind="HotelDetailAjax.aspx.cs" %>

<%@ Import Namespace ="System.Collections.Generic" %>
<script type="text/javascript">
    document.write('<style>.noscript { display: none; }</style>');
</script>

<%if (Request["Action"].ToString() == "getHotelDetail")
  {%>



<div class="padding0"> 

<div class="window_title"> <%=result.HotelName%> </div>




<div> 
    <div class="container_iframe">
            <ul class="tabs">
                <!-- <li class="active">Overview</li> -->
                <%--<li   id="liOverview" class="active" ><a href="javaScript:ShowDiv('Overview','Hot_Amenities')">Overview</a></li>--%>
                <li id="liHot_Desc"><a href="javaScript:ShowDiv('Hot_Desc','Overview')">Description</a></li>
              
                <li id="liPhotos"><a href="javaScript:ShowDiv('Photos','Overview')">Photos</a></li>
                
                <li id="liHot_Amenities"><a href="javaScript:ShowDiv('Hot_Amenities','Overview')">Amenities</a></li>
                
                <li id="liHLocation" onclick="javascript:initialize()"><a href="javaScript:ShowDiv('HLocation','Overview')" >Map</a></li>
            </ul>
          </div>
         
         
         
         <div class="hotel_inner-sc">
         
      
          
             <div style="padding: 4px 11px 16px 11px; display: none" id="Hot_Desc">
                 <%if (hotelDetails.Description != null || hotelDetails.Address != null)
                   { %>
                 <p>
                     <b>Address</b></p>
                 <p class="fleft">
                     <b>
                         <%=hotelDetails.Address%>
                     </b>
                 </p>
                 <br />
                 <br />
                 <%if (hotelDetails.Description != null)
                   { %>
                 <p class="fleft">
                     <%if (result.BookingSource == CT.BookingEngine.HotelBookingSource.LOH || result.BookingSource == CT.BookingEngine.HotelBookingSource.EET)
                       { %>
                     <%=HttpUtility.HtmlDecode(hotelDetails.Description)%>
                     <%}
                       else
                       { %>
                     <%=hotelDetails.Description%>
                 </p>
                 <%}
                   }
                   } %>
             </div>
             <div style="padding: 4px 11px 0px 11px; display: none" id="Photos">
                 <div>
                     <div class="col-md-8">
                         <b id="ImageHeader"></b>
                     </div>
                     <%--<div class="list_rght1">
Gallery
</div>--%>
                     <div class="clear">
                     </div>
                 </div>
                 <div>
                     <div class="col-md-8">
                         <%string Path = "";
                           if (result.BookingSource == CT.BookingEngine.HotelBookingSource.DOTW)
                           {
                               Path = CT.Configuration.ConfigurationSystem.DOTWConfig["imgPathForServer"].ToString();
                           }
                           else if (result.BookingSource == CT.BookingEngine.HotelBookingSource.HotelConnect)
                           {
                               Path = CT.Configuration.ConfigurationSystem.HotelConnectConfig["imgPathForServer"].ToString();
                           }
                           else if (result.BookingSource == CT.BookingEngine.HotelBookingSource.HotelBeds)
                           {
                               Path = CT.Configuration.ConfigurationSystem.HotelBedsConfig["imgPathForServer"].ToString();
                           }
                           else if (result.BookingSource == CT.BookingEngine.HotelBookingSource.GTA)
                           {
                               Path = CT.Configuration.ConfigurationSystem.GTAConfig["imgPathForServer"].ToString();
                           }
                           //else if (result.BookingSource == CT.BookingEngine.HotelBookingSource.Miki)
                           //{
                           //    Path = CT.Configuration.ConfigurationSystem.GTAConfig["imgPathForServer"].ToString();
                           //}
                         %>
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                             <tr>
                                 <td>
                                     <div class="result_hot_slide">
                                         <a style="cursor: pointer; position: absolute; top: 40%; left: 10px; z-index: 999"
                                             onclick="javascript:PrevImage('<%=((hotelDetails.Images != null) ? hotelDetails.Images.Count : 0) %>')">
                                             <img src="images/back-1.png" width="37" height="37" /></a><a style="cursor: pointer;
                                                 position: absolute; top: 40%; right: 7px; z-index: 999" onclick="javascript:NextImage('<%=(hotelDetails.Images != null ? hotelDetails.Images.Count : 0) %>')">
                                                 <img src="images/next-1.png" width="37" height="37" /></a>
                                         <% if (hotelDetails.Images != null && hotelDetails.Images.Count > 0)
                                            {
                                                if (result.BookingSource == CT.BookingEngine.HotelBookingSource.DOTW || result.BookingSource == CT.BookingEngine.HotelBookingSource.GTA || result.BookingSource == CT.BookingEngine.HotelBookingSource.HotelBeds)
                                                {%>
                                         <%for (int i = 0; i < hotelDetails.Images.Count; i++)
                                           {
                                               if (hotelDetails.Images[i].Contains("http"))
                                                   Path = "";
                                               else
                                                   if (result.BookingSource == CT.BookingEngine.HotelBookingSource.DOTW)
                                                   {
                                                       Path = CT.Configuration.ConfigurationSystem.DOTWConfig["imgPathForServer"].ToString();
                                                   }

                                               if (i == 0)
                                               {%>
                                         <span style="" id="image-<%=i%>">
                                             <img class="slidbigpic" src="<%=Path + hotelDetails.Images[i] %>" alt="hotel image" /><br />
                                             <br />
                                         </span>
                                         <%}
              else
              {%>
                                         <span style="display: none;" id="image-<%=i%>">
                                             <img class="slidbigpic" src="<%=Path + hotelDetails.Images[i] %>" alt="hotel image" /><br />
                                             <br />
                                         </span>
                                         <%}%>
                                         <%} %>
                                         <% }
                else if (result.BookingSource != CT.BookingEngine.HotelBookingSource.DOTW && result.BookingSource != CT.BookingEngine.HotelBookingSource.GTA)
                {%>
                                         <%for (int i = 0; i < hotelDetails.Images.Count; i++)
                                           { %>
                                         <%if (i == 0)
                                           {%>
                                         <span style="" id="image-<%=i%>">
                                             <img class="slidbigpic" src="<%=Path + hotelDetails.Images[i] %>" alt="hotel image" /><br />
                                             <br />
                                         </span>
                                         <%}
                                           else
                                           {%>
                                         <span style="display: none;" id="image-<%=i%>">
                                             <img class="slidbigpic" src="<%=Path + hotelDetails.Images[i] %>" alt="hotel image" /><br />
                                             <br />
                                         </span>
                                         <%}%>
                                         <%} %>
                                         <%}
                else
                {%>
                                         <span style="float: left; margin: 10px 0 0 65px;" id="Span1">Images Not Available<br />
                                             <br />
                                         </span>
                                         <%}
            }%></div>
                                 </td>
                             </tr>
                             <%if (result.BookingSource == CT.BookingEngine.HotelBookingSource.GTA)
                               { %>
                             <tr>
                                 <td>
                                     Images provided by VFM Leonardo Inc.
                                 </td>
                             </tr>
                             <%} %>
                         </table>
                     </div>
                     <div class="list_rght">
                         <%if (hotelDetails.Images != null && hotelDetails.Images.Count > 0)
                           { %>
                         <ul>
                             <%for (int i = 0; i < hotelDetails.Images.Count; i++)
                               { %>
                             <li><a href="#" onclick="ShowGalleryImage('<%= hotelDetails.Images.Count %>','<%=i %>');">
                                 <img width="100px" height="75px" src="<%=Path + hotelDetails.Images[i] %>" alt="hotel image" /></a></li>
                             <%} %>
                         </ul>
                         <%}
                           else
                           {%>
                         <span style="float: left; margin: 10px 0 0 65px;" id="Span1">Images Not Available<br />
                             <br />
                         </span>
                         <%} %>
                     </div>
                     <div class="clearfix">
                     </div>
                 </div>
                 <div class="clear">
                 </div>
             </div>
             <div style="padding: 4px 11px 0px 11px; display: none" id="Hot_Amenities">
                 <strong style="font-size: 13px">Hotel In and Around</strong>
                 <ul style="padding: 5px 20px 0px 15px;">
                     <%if (hotelDetails.Attractions != null && hotelDetails.Attractions.Count > 0)
                       {%>
                     <%foreach (KeyValuePair<string, string> pair in hotelDetails.Attractions)
                       { %>
                     <li>
                         <%=pair.Key%>:
                         <%=pair.Value%></li>
                     <%}
                      }
                       else
                       {%>
                     <li><b>Hotel Attraction Details not available</b></li>
                     <%}%>
                 </ul>
                 <strong style="font-size: 13px">
                     <br />
                     Hotel Facilities</strong>
                 <ul style="padding: 5px 20px 0px 15px;">
                     <%if (hotelDetails.HotelFacilities != null && hotelDetails.HotelFacilities.Count > 0)
                       {%>
                     <%for (int i = 0; i < hotelDetails.HotelFacilities.Count; i++)
                       { %>
                     <li>
                         <%=hotelDetails.HotelFacilities[i]%>
                     </li>
                     <%}
                      }
                       else
                       {%>
                     <li>Room Facility Details not available</li>
                     <%} %>
                     <%if (result.BookingSource == CT.BookingEngine.HotelBookingSource.HotelBeds)
                       {%>
                     <strong style="font-size: 15px; color: Red">
                         <br />
                         (*) Some services shall be paid at the establishment</strong>
                     <%} %>
                 </ul>
                 <div class="clear">
                 </div>
             </div>
              <div id="HLocation" style="padding: 4px 11px 0px 11px;">
             <%if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
               { %>
            
            <input type="hidden" id="latitude"  value='<%=latitude %>'/>
            <input type="hidden" id="longitude" value='<%=longitude %>' />
            <div id="googleMap" style="width: 890px; height: 380px;"></div>
          <%}
               else
               {%>
               <label>No Map Found</label>
               <%} %>
            </div>
            
        </div>


<div class="clearfix"> </div>

</div>


<div class="clearfix"> </div>




</div>



    <%}  %>
     
    <%if (hotelDetails.RoomFacilities != null && hotelDetails.RoomFacilities.Count > 0 && Request["room_amenities"] != null)
      { %>
    <div class='showMsgHeading'>
        Room Amenities</div>
      <div style="overflow:auto;height:170px;">
    
    <ul style="padding: 5px 20px 0px 15px;">
        <%if (hotelDetails.RoomFacilities != null && hotelDetails.RoomFacilities.Count > 0)
          {%>
        <%for (int i = 0; i < hotelDetails.RoomFacilities.Count; i++)
          { %>
        <li>
            <%=hotelDetails.RoomFacilities[i]%></li>
        <%}
                }
          else
          {%>
        <li>Room Facility Details not available</li>
        <%} %>
        <%if (result.BookingSource == CT.BookingEngine.HotelBookingSource.HotelBeds)
                      {%>
                     <strong style="font-size: 15px;color:Red">
                    <br />
                    (*) Some services shall be paid at the establishment</strong>
                    <%} %>
    </ul>
    </div>
    <%}else if(Request["room_amenities"] != null){ %>
    <div class='showMsgHeading'>
        Room Amenities</div>
        <ul><li>No Details Found</li></ul>
    <%} %>
  
