<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="AgentPaymentDetailsUI" Title="Agent Payment Details" Codebehind="AgentPaymentDetails.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--<div class="grdScrlTrans" style="margin-top:-2px;height:520px;border:solid0px;text-align:center" >--%>
<table width="100%"  border="0">
<tr height="30px">
<td> 

<div class="body_container"> 
          
    
     
    
       <div class="paramcon" title="header">    
       
        <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"> 

    <asp:RadioButton ID="rbtnCredit" CssClass="custom-radio2-table" runat="server" Text="Credit" Checked="true" onchange= "return showDropdown('Clear');" GroupName="Payment" />
    
    
    </div>
    <div class="col-md-2"> <div id="creditDiv">
                        <asp:DropDownList ID="ddlCreditItems" CssClass="form-control" runat="server" >
                        <asp:ListItem Text="TOP UP" Value= "ADD" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="CR-TRANSFER" Value= "CR-TRA" ></asp:ListItem>
                        <asp:ListItem Text="CREDIT NOTE" Value= "CR-NOTE" ></asp:ListItem>
                        </asp:DropDownList>
                        </div></div>
    <div class="col-md-2"> <asp:RadioButton CssClass="custom-radio2-table" ID="rbtnDebit" runat="server" Text="Debit" onchange= "return showDropdown('Clear');" GroupName="Payment" /></div>
     <div class="col-md-2"><div id="debitDiv">
                        <asp:DropDownList ID="ddlDebitItems" CssClass="form-control" runat="server" >
                        <asp:ListItem Text="OFFLINE BOOKING OTHERS" Value= "DEB-OTH" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="OFFLINE BOOKING AIR" Value= "DEB-AIR" ></asp:ListItem>
                        <asp:ListItem Text="OFFLINE BOOKING HOTEL" Value= "DEB-HTL" ></asp:ListItem>
                        <asp:ListItem Text="CREDIT REVERSAL" Value= "DEB-REV" ></asp:ListItem>
                        <asp:ListItem Text="DEB-TRANSFER" Value= "DEB-TRA" ></asp:ListItem>
                        </asp:DropDownList>
                        </div> </div>
 


    <div class="clearfix"></div>
    </div>
       
    <asp:HiddenField id="hdfDetailRowId" runat="server"></asp:HiddenField>
   
    
    <div class="col-md-12 padding-0 marbot_10">                               
       
    <div class="col-md-2"><asp:Label ID="lblReceiptNo" Text="Receipt No:" runat="server" ></asp:Label> </div>
    
    <div class="col-md-2"> <asp:TextBox  ID="txtReceiptNo" runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox></div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblDate" Text="Date:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"> <uc1:DateControl ID="dcDocDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="false"  HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>
     
     
     
    <div class="col-md-2"> <asp:Label ID="lblAgent" Text="Agent:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:DropDownList ID="ddlAgent"  runat="server" Enabled="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged"  AutoPostBack="true" CssClass="inputddlEnabled form-control"></asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"> <asp:Label ID="lblPayMode" Text="Pay Mode:" runat="server"></asp:Label></div>
    
    
    <div class="col-md-2"><asp:DropDownList ID="ddlPayMode"  runat="server" Enabled="true" CssClass="inputddlEnabled form-control">
    </asp:DropDownList> </div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblAmount" Text="Amount:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"> <asp:TextBox ID="txtAmount"  runat="server" Enabled="true" CssClass="inputEnabled form-control" onpaste="return false;" ondrop="return false;" onkeypress="return isNumber(event)" onfocus="Check(this.id);" onBlur="Set(this.id);" ></asp:TextBox></div>
     
     
    <div class="col-md-2"><asp:Label ID="lblCurrency" Text="Currency:" runat="server"></asp:Label> </div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlCurrency"  runat="server" Enabled="true" CssClass="inputDdlEnabled form-control"></asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"> <asp:Label ID="lblChequeNo" Text="Cheque #:" runat="server"></asp:Label></div>
    
    <div class="col-md-2"> <asp:TextBox ID="txtChequeNo"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    
    
    <div class="col-md-2"><asp:Label ID="lblChequeDate" Text="Cheque Date:" runat="server"></asp:Label> </div>
    
    
     <div class="col-md-2"> <uc1:DateControl ID="dcChequeDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>
     
     
    <div class="col-md-2"> <asp:Label ID="lblBankName" Text="Bank:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtBankName"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"> <asp:Label ID="lblBankBranch" Text="Branch:" runat="server"></asp:Label></div>
    
    
    <div class="col-md-2"> <asp:TextBox ID="txtBankBranch"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    
    <div class="col-md-2"> <asp:Label ID="lblBankAc" Text="Bank Account:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtBankAccount"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>
     
     
    <div class="col-md-2"><asp:Label ID="lblPaySlip" Text="Upload Pay Slip:" runat="server"></asp:Label> </div>
    
    
     <div class="col-md-2"><CT:DocumentManager ID="docPaySlip" runat="server" DefaultImageUrl="~/images/common/no_preview.png"
                    DocumentImageUrl="~/images/common/Preview.png"   ShowActualImage="false"  SessionName="crenterlic"
                    DefaultToolTip=""  /> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"><asp:Label ID="lblRemarks" Text="Remarks:" runat="server"></asp:Label> </div>
    <div class="col-md-10"> 
    <asp:TextBox ID="txtRemarks"  runat="server" TextMode="MultiLine" Height="60px"  Enabled="true" CssClass="inputEnabled" ></asp:TextBox>
    
    </div>



    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-12"> 
         <label style=" padding-right:5px" class="f_R"><asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();"  CssClass=" btn but_b" OnClick ="btnSave_Click" ></asp:Button></label>
                     <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server"  CssClass="btn but_b"  OnClick="btnClear_Click"></asp:Button></label>
                    <label style=" padding-right:5px" class="f_R">  <asp:Button ID="btnSearch" Text="Search" runat="server"  CssClass="btn but_b"  OnClick="btnSearch_Click"></asp:Button></label>
    
    </div>



    <div class="clearfix"></div>
    </div>
    
    
    
    
    
         

     </div>
    
    
    <div class="clearfix"></div>
</div>

</td>
</tr>
       
  
            
    </table>    
  <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<script type="text/javascript">

    function Save() {

        if (getElement('ddlAgent').selectedIndex <= '0') addMessage('Please select Agent from the list!', '');
        if (getElement('ddlPayMode').selectedIndex <= '0') addMessage('Please select Payment mode from the list!', '');
        if (getElement('ddlCurrency').selectedIndex <= '0') addMessage('Please select Currency from the list!', '');
        // if(getElement('txtMapLocation').value=='' ) addMessage('Mapping Location cannot be blank!','');
        if (getElement('txtAmount').value == "0.00" || getElement('txtAmount').value == '') addMessage('Amount cannot be zero/blank!', '');


        if (getElement('ddlPayMode').value == 'CHEQUE' || getElement('ddlPayMode').value == 'DRAFT' || getElement('ddlPayMode').value == 'TRANSFER') {

            if (getElement('txtChequeNo').value == '') addMessage('Cheque No cannot be blank!', '');
            var chequeDate = GetDateTimeObject('ctl00_cphTransaction_dcChequeDate');

            if (chequeDate == null) addMessage('Please select Cheque Date !', '');
            if (getElement('txtBankName').value == '') addMessage('Bank cannot be blank!', '');
            if (getElement('txtBankBranch').value == '') addMessage('Branch cannot be blank!', '');
            if (getElement('txtBankAccount').value == '') addMessage('Account cannot be blank!', '');
        }
        if (getMessage() != '') {
            //alert(getMessage()); 
            alert(getMessage()); 
            clearMessage(); return false;
        }
        getElement('btnSave').style.display = "none";
    }

  function Check(id) {
      var val = document.getElementById(id).value;
      if (val == '0') {
          document.getElementById(id).value = '';
      }
  }
  function Set(id) {
      var val = document.getElementById(id).value;
      if (val == '' || val == '0') {
          document.getElementById(id).value = '0.00';
      }
  }
  function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 46 || charCode > 57)) {
          return false;
      }
      return true;
  }

  function showDropdown(action) {
      if (document.getElementById('<%=rbtnCredit.ClientID %>').checked) {
          document.getElementById('creditDiv').style.display = "block";
          document.getElementById('debitDiv').style.display = "none";
          if (action == "Clear") {
              document.getElementById('<%=ddlCreditItems.ClientID %>').selectedIndex = 0;
          }
      }
      else if (document.getElementById('<%=rbtnDebit.ClientID %>').checked) {
          document.getElementById('creditDiv').style.display = "none";
          document.getElementById('debitDiv').style.display = "block";
          if (action == "Clear") {
              document.getElementById('<%=ddlDebitItems.ClientID %>').selectedIndex = 0;
          }
      }
      return false;
  }
  function clear() {
      document.getElementById('<%=rbtnDebit.ClientID %>').checked = false;
      document.getElementById('<%=rbtnCredit.ClientID %>').checked = true;
      document.getElementById('creditDiv').style.display = "block";
      document.getElementById('debitDiv').style.display = "none";
      document.getElementById('<%=ddlCreditItems.ClientID %>').selectedIndex = 0;
  }
//   function InterfaceYN()
//   {
//       alert(getElement('ctl00_cphTransaction_rdbInterface').value);
//       alert(getElement('rdbInterface').id);
//        //if(getElement('rdbInterface').selectedValue=="N" 
//        //{
//             alert('Hi');
//       // document.getElementById('divInterface').style.display='block';
//        //}
//       // else
//        //{
//        //document.getElementById('divInterface').style.display='none';
//       // }
//     
//   }

//Documents
</script>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="AP_ID" 
    EmptyDataText="No Location List!" AutoGenerateColumns="false" PageSize="10" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
           
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtName"  Width="100px" CssClass="inputEnabled" HeaderText="Agent" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("agent_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtPayMode" Width="100px" HeaderText="Payment Mode" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblPayMode" runat="server" Text='<%# Eval("ap_payment_mode_name") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_payment_mode_name") %>' Width="150px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>     
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtReceiptNo"  Width="150px" CssClass="inputEnabled" HeaderText="Receipt #" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptNo" runat="server" Text='<%# Eval("ap_reciept_no") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_reciept_no") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>   
    
    <asp:TemplateField>
    <HeaderStyle VerticalAlign="top" />
    <HeaderTemplate>
    <table>    
    <tr><td >
    </td></tr>
    <tr><td >
    </td>
    <td>
    <uc1:DateControl ID="HTtxtDocDate" runat="server" DateOnly="true" />
    </td>
    <td>
    <asp:ImageButton ID="HTbtnDocDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="FilterSearch_Click" />
    </td>
    <%--<td>
    <asp:ImageButton ID="HTbtnChequeReturnDateAdvance"  runat="server" ImageUrl="~/Images/wg_filterAdvance.GIF"  ImageAlign="AbsMiddle"  OnClientClick="return validateAdvanceFilter(this.id,'A');" Alt="Advance Filter" /></td>--%>
    </tr></table>
    <label style="width:140px" class="filterHeaderText">&nbsp;Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptDate" Width="130px" runat="server" Text='<%# IDDateTimeFormat(Eval("ap_receipt_date")) %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_receipt_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>     
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtAmount"  Width="150px" CssClass="inputEnabled" HeaderText="Amount" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblAmount" runat="server" Text='<%# Eval("ap_amount") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_amount") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>        
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtChequeNo"  Width="150px" CssClass="inputEnabled" HeaderText="Cheque #" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblChequeNo" runat="server" Text='<%# Eval("ap_cheque_no") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_cheque_no") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtBankName"  Width="150px" CssClass="inputEnabled" HeaderText="Bank" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblBankName" runat="server" Text='<%# Eval("ap_bank_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_bank_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
   
   <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtBankBranch"  Width="150px" CssClass="inputEnabled" HeaderText="Branch" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblBankBranch" runat="server" Text='<%# Eval("ap_bank_branch") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_bank_branch") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtBankAc"  Width="150px" CssClass="inputEnabled" HeaderText="Account" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblBankAc" runat="server" Text='<%# Eval("ap_bank_account") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_bank_account") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    
    </asp:TemplateField>
    
    </Columns>           
    </asp:GridView>

</asp:Content>





