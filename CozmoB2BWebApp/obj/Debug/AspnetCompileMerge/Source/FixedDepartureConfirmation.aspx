﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDepartureConfirmationGUI" Title="FixedDeparture Confirmation" Codebehind="FixedDepartureConfirmation.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<!-- ----------------------For Calender Control--------------------------- -->
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

<!-- ----------------------End Calender Control--------------------------- -->
<script type="text/javascript">
    if (history.length > 1) {
        history.forward(1);
    }
    var cal1;
    var cal2;
    function init() {
        var dt = new Date();
        cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
        cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("close", true);
        cal1.cfg.setProperty("iframe", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();

        cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
        cal2.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
        cal2.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
        cal2.cfg.setProperty("close", true);
        cal2.cfg.setProperty("iframe", true);
        cal2.selectEvent.subscribe(setDate2);
        cal2.render();
    }

    function showCalendar1() {

        document.getElementById('container1').style.display = "block";
        document.getElementById('Outcontainer1').style.display = "block";
    }
    function showCalendar2() {

        document.getElementById('container2').style.display = "block";
        document.getElementById('Outcontainer2').style.display = "block";
    }

    var departureDate = new Date();

    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMessHotel').style.visibility = "visible";
            document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=txtDepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        cal1.hide();
        document.getElementById('Outcontainer1').style.display = "none";
    }


    function setDate2() {
        var date1 = cal2.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMessHotel').style.visibility = "visible";
            document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=txtVisaDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        cal2.hide();
        document.getElementById('Outcontainer2').style.display = "none";
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
    function validate() {
        if (document.getElementById('<%=txtCash.ClientID %>').value == "") {
            document.getElementById('<%=txtCash.ClientID %>').value = "0";
        }
        if (document.getElementById('<%=txtCredit.ClientID %>').value == "") {
            document.getElementById('<%=txtCredit.ClientID %>').value = "0";
        }
        if (document.getElementById('<%=txtCard.ClientID %>').value == "") {
            document.getElementById('<%=txtCard.ClientID %>').value = "0";
        }
//        if (('<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId %>') == '1') {
            var cash = eval(document.getElementById('<%=txtCash.ClientID %>').value);
            var credit = eval(document.getElementById('<%=txtCredit.ClientID %>').value);
            var card = eval(document.getElementById('<%=txtCard.ClientID %>').value);
            cash = Math.ceil(cash);
            card = Math.ceil(card);
            credit = Math.ceil(credit);
            if (document.getElementById('tblCash').style.display == "block" && document.getElementById('tblCredit').style.display == "block" && document.getElementById('tblCard').style.display == "block") {
                if (cash == Math.ceil(0) && card == Math.ceil(0) && credit == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            } else if (document.getElementById('tblCash').style.display == "block" && document.getElementById('tblCredit').style.display == "block") {
                if (cash == Math.ceil(0) && credit == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            } else if (document.getElementById('tblCash').style.display == "block" && document.getElementById('tblCard').style.display == "block") {
                if (cash == Math.ceil(0) && card == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            else if (document.getElementById('tblCash').style.display == "block") {
                if (cash == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            else if (document.getElementById('tblCredit').style.display == "block") {
                if (credit == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            else if (document.getElementById('tblCard').style.display == "block") {
                if (card == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            if (!CalculateBalance()) {
                return false;
            }
            if (document.getElementById('tblNextPayment').style.display == "block") {
                if (document.getElementById('ctl00_cphTransaction_txtDepDate').value == "DD/MM/YYYY" || document.getElementById('ctl00_cphTransaction_txtDepDate').value == "") {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Next Payment Date";
                    return false;
                }
            }
            if (getElement('ddlDiscount').selectedIndex > 0) {
                if (document.getElementById('ctl00_cphTransaction_txtPlease').value == "") {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter specify";
                    return false;
                }

            }
            if (getElement('txtReceiptNo').value == "") {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "please enter ReceiptNo";
                return false;

            }
           
            if (eval(getElement('lblPayableCharge').innerHTML.split(' ')[1]) < eval(getElement('hdnTotalPaxCount').value) * 500) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "The minimum payment should be done at least 500 AED * No of pax";
                return false;
            }
            
            if (document.getElementById('rules').checked == false) {
                document.getElementById('checkRules').style.display = "block";
                return false;
            }
            else {
                document.getElementById('<%=hdnPaymentDate.ClientID %>').value = document.getElementById('<%=txtDepDate.ClientID %>').value;
                document.getElementById('checkRules').style.display = "none";
                return true;
            }
            

//        }

        }


        function CalculateDiscount() {
            var currency = '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>';
            var code = ('<%=code%>');
            var total = eval('<%=total%>');
            var discount = eval('<%=discount%>');
            if (document.getElementById('<%=txtCode.ClientID %>').value == code && code.length > 0) {
                if (total > discount) {
                    var totalAmount = eval(total - discount);
                }
                document.getElementById('<%=lblDiscount.ClientID %>').innerHTML = currency + " " + Math.ceil(discount).toFixed(2);
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = currency + " " + Math.ceil(totalAmount).toFixed(2);
                document.getElementById('<%=hdnTotal.ClientID %>').value = Math.ceil(totalAmount).toFixed(2);
                document.getElementById('<%=lblCodeError.ClientID %>').innerHTML = "";
            }
            else {
                document.getElementById('<%=lblDiscount.ClientID %>').innerHTML = Math.ceil("0").toFixed(2);
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = currency + " " + Math.ceil(total).toFixed(2);
                document.getElementById('<%=hdnTotal.ClientID %>').value = Math.ceil(total).toFixed(2);
                if (document.getElementById('<%=txtCode.ClientID %>').value != "") {
                    document.getElementById('<%=lblCodeError.ClientID %>').innerHTML = "Not Valid Code";
                }
                else {
                    document.getElementById('<%=lblCodeError.ClientID %>').innerHTML = "";
                }
            }
        }

        function ShowHideRemarks() {
            var currency = '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>';
            var total = eval('<%=total%>');
            var discount = eval('<%=discount%>');
            if (getElement('ddlDiscount').selectedIndex > 0) {
                document.getElementById('tblDiscount').style.display = "block";
            }
            else {
                document.getElementById('tblDiscount').style.display = "none";
                document.getElementById('<%=lblDiscount.ClientID %>').innerHTML = Math.ceil("0").toFixed(2);
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = currency + " " + Math.ceil(total).toFixed(2);
                document.getElementById('<%=hdnTotal.ClientID %>').value = Math.ceil(total).toFixed(2);
                document.getElementById('<%=lblCodeError.ClientID %>').innerHTML = "";
                document.getElementById('<%=txtCode.ClientID %>').value = "";
                CalculateBalance();
            }
        }
    

        function CalculateBalance() {
            var currency = '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>';
            var cash = eval(document.getElementById('<%=txtCash.ClientID %>').value);
            var credit = eval(document.getElementById('<%=txtCredit.ClientID %>').value);
            var card = eval(document.getElementById('<%=txtCard.ClientID %>').value);
            //var total = eval('<%=total%>');
            var total = eval(document.getElementById('<%=hdnTotal.ClientID %>').value);
            var amountPaid = eval(0);
            var charge = eval(document.getElementById('<%=ddlCreditCard.ClientID %>').value);
            if (document.getElementById('<%=txtCash.ClientID %>').value == "") {
                cash = eval(0);
            }
            if (document.getElementById('<%=txtCredit.ClientID %>').value == "") {
                credit = eval(0);
            }
            if (document.getElementById('<%=txtCard.ClientID %>').value == "") {
                card = eval(0);
            }
            cash = Math.ceil(cash);
            card = Math.ceil(card);
            credit = Math.ceil(credit);
            total = Math.ceil(total);

           // card += (card * charge / 100);
            card = Math.ceil(card);
            document.getElementById('CardAmount').innerHTML = Math.ceil(eval(card * charge / 100)).toFixed(2);
            document.getElementById('TotalCardAmount').innerHTML = Math.ceil(eval(card + (card * charge / 100))).toFixed(2);

            if (document.getElementById('tblCash').style.display == "block" && eval(cash) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                return false;
            }
            else if (document.getElementById('tblCredit').style.display == "block" && eval(cash + credit) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                return false;
            }
            else if (document.getElementById('tblCard').style.display == "block" && eval(cash + card) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                return false;
            }
            else if (document.getElementById('tblCredit').style.display == "block" && document.getElementById('tblCard').style.display == "block" && eval(cash + credit + card) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                return false;
            }
            else {

                document.getElementById('errorMsg').style.display = "none";
                document.getElementById('errorMsg').innerHTML = "";
                amountPaid = cash;
                if (document.getElementById('tblCredit').style.display == "block") {
                    total -= credit;
                    amountPaid += credit;
                }
                if (document.getElementById('tblCard').style.display == "block") {
                    total -= Math.ceil(card);
                    amountPaid += Math.ceil(card);

                }
                if (document.getElementById('tblNextPayment').style.display = "block") {
                    document.getElementById('tblNextPayment').style.display = "none"
                    //document.getElementById('<%=txtDepDate.ClientID %>').value = "";
                }

                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency+" "+ Math.ceil(eval(total - cash)).toFixed(2);
                document.getElementById('<%=lblAmountPaid.ClientID %>').innerHTML = currency + " " + Math.ceil(amountPaid).toFixed(2);
                document.getElementById('<%=lblPayableCharge.ClientID %>').innerHTML = currency + " " + Math.ceil(amountPaid + eval(card * charge / 100)).toFixed(2);

                if (eval(cash) < eval(total) || eval(cash + credit) < eval(total) || eval(cash + card) < eval(total) || eval(cash + credit + card) < eval(total)) {
                    document.getElementById('tblNextPayment').style.display = "block";
                }
                else {
                    document.getElementById('<%=txtDepDate.ClientID %>').value = "";
                }
                return true;
            }
        }

        function ShowHideCreditCard() {
            var ddl = document.getElementById('<%=ddlSettlementMode.ClientID %>');
            var currency = '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>';
           // var total = eval('<%=total%>');
            var total = eval(document.getElementById('<%=hdnTotal.ClientID %>').value);
            var charge = eval(document.getElementById('<%=ddlCreditCard.ClientID %>').value);

            if (ddl.options[ddl.selectedIndex].text.indexOf("Card") >= 0) {
                if (ddl.options[ddl.selectedIndex].text == "Card") {
                    document.getElementById('tblCash').style.display = "none";
                    document.getElementById('tblCredit').style.display = "none";
                    document.getElementById('<%=txtCash.ClientID %>').value = "0";
                    document.getElementById('<%=txtCredit.ClientID %>').value = "0";
                }
                else {
                    document.getElementById('tblCash').style.display = "block";
                    document.getElementById('tblCredit').style.display = "none";
                }
                document.getElementById('tblCreditCard').style.display = "block";
                document.getElementById('tblCard').style.display = "block";
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total)).toFixed(2);
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total)).toFixed(2);
            }
            else {
                document.getElementById('tblCreditCard').style.display = "none"
                document.getElementById('tblCard').style.display = "none";
                document.getElementById('tblCredit').style.display = "none";
                document.getElementById('<%=txtCard.ClientID %>').value = "0";
            }

            if (ddl.options[ddl.selectedIndex].text.indexOf("Credit") >= 0) {
                if (ddl.options[ddl.selectedIndex].text == "Credit") {
                    document.getElementById('tblCredit').style.display = "block";
                    document.getElementById('tblCreditCard').style.display = "none"
                    document.getElementById('tblCard').style.display = "none";
                    document.getElementById('tblCash').style.display = "none";
                    document.getElementById('<%=txtCard.ClientID %>').value = "0";
                }
                else {
                    document.getElementById('tblCredit').style.display = "none";
                }
                document.getElementById('tblCredit').style.display = "block";                
            }
            if (ddl.options[ddl.selectedIndex].text.indexOf("Cash") >= 0) {
                if (ddl.options[ddl.selectedIndex].text == "Cash") {
                    document.getElementById('tblCash').style.display = "block";
                    document.getElementById('tblCredit').style.display = "none";
                    document.getElementById('tblCreditCard').style.display = "none"
                    document.getElementById('tblCard').style.display = "none";
                    document.getElementById('<%=txtCard.ClientID %>').value = "0";
                }
                else {
                    document.getElementById('tblCash').style.display = "none";
                }
                document.getElementById('tblCash').style.display = "block";
            }
//            else {
//                document.getElementById('tblCredit').style.display = "none"
//            }
            CalculateBalance();
        }

        function ApplyCreditCardCharge() {
            var ddl = document.getElementById('<%=ddlCreditCard.ClientID %>');
            //var total = eval('<%=total%>');
            var total = eval(document.getElementById('<%=hdnTotal.ClientID %>').value);
            var card = eval(document.getElementById('<%=txtCard.ClientID %>').value);
            if (ddl.selectedIndex > 0) {
                var charge = eval(ddl.value);
                //total += card * charge / 100;
                document.getElementById('<%=hdnCardType.ClientID %>').value = ddl.options[ddl.selectedIndex].text;
                document.getElementById('<%=lblCharge.ClientID %>').innerHTML = parseFloat(charge).toFixed(2);
                document.getElementById('CardAmount').innerHTML = Math.ceil((card * charge / 100)).toFixed(2);
                document.getElementById('TotalCardAmount').innerHTML = Math.ceil(eval(card + (card * charge / 100))).toFixed(2);
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = Math.ceil(total).toFixed(2);
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = Math.ceil(total).toFixed(2);
            }
            CalculateBalance();
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 46) {
                return true;
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
 
 
 <style> 
 
 input {
    border: 1px solid #7f9db9;
    font-size: 12px;
    height: 18px;
    
}


 
 </style>
 
   

        <div style=" padding-top:10px">
            <div class="ns-h3"><asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></div>
            <asp:HiddenField ID="hdnTotalPaxCount" runat="server" Value="" />
            <asp:HiddenField ID="hdnCardType" runat="server" Value=""/>
            <asp:HiddenField ID="hdnPaymentDate" runat="server" Value=""/>
            <asp:HiddenField ID="hdnTotal" runat="server" Value="0" />
            <div class="wraps0ppp">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div style="width: 100%" class="hotels_gr1">
                                <span class="hotel_view">
                                    <asp:Image ID="imgActivity" runat="server" />
                                </span>
                                <h3>
                                    <asp:Label ID="lblTitle1" runat="server" Text=""></asp:Label></h3>
                                <p>
                                    <asp:Label ID="lblOverview" runat="server" Text=""></asp:Label></p>
                                <h2 style="padding-top: 10px">
                                    Total :<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></h2>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear">
            </div>
        </div>
        <div style="padding: 10px 0px 10px 0px">
            <div style="width: 50%; float: left" class="Holiday_Packages">
               
               
               <div class="ns-h3">
                Passenger Details
                </div>
                <div class="wrap_auto" style="overflow-y: scroll; min-height:300px">
                    <asp:DataList ID="dlPaxList" runat="server" Width="100%">
                        <ItemTemplate>
                            <div style="height: 40px;">
                                <h3 style="color: ">
                                </h3>
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            Pax Name :
                                        </td>
                                        <td align="left">
                                    <%#Eval("FirstName") %>
                                    <%#Eval("LastName") %> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            E-mail:
                                        </td>
                                        <td>
                                            <%#Eval("Email") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="b_bot">
                                        </td>
                                    </tr>
                                </table>
                                <span class="b_bot"></span>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                    <div class="clear">
                    </div>
                </div>
                <div class="fff_round">
                    <div class="fff_round_1">
                    </div>
                    <div class="fff_round_2">
                    </div>
                    <div class="fff_round_3">
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div style="width: 45%; float: right" class="Honeymoon_Packages">
                
                      
               <div class="ns-h3">
                Payment Details
                </div>
                <div id="errorMsg" style="display:none;color:Red">
                </div>
                
                <div style="min-height: 300px;" class="wrap_auto">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td>
                       <%-- <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.MemberType == CT.TicketReceipt.BusinessLayer.MemberType.ADMIN)
                          {%>--%>
                        <table >
                        <tr>
                         <td colspan="2">
                         <table>
                                    <tr>
                            <td height="22">
                                Settlement Mode :
                            </td>
                            <td width="60%">
                                <asp:DropDownList CssClass="inputDdlEnabled" Width="120px"  ID="ddlSettlementMode" runat="server" onchange="ShowHideCreditCard()">
                                </asp:DropDownList>
                            </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblCreditCard" style="display: none">
                                    <tr>
                                        <td height="22">
                                            Credit Card Type :
                                        </td>
                                        <td style="width:70%">
                                            <asp:DropDownList CssClass="inputDdlEnabled" Width="120px" ID="ddlCreditCard" runat="server" onchange="ApplyCreditCardCharge()">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="22">
                                            Card Charge (%):
                                        </td>
                                        <td style="width:70%">
                                            <asp:Label ID="lblCharge" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="2">
                         <table>
                                    <tr>
                                <td height="22"  style="width: 29%">
                                    Discount:
                                </td>
                                <td width="60%">
                                   <asp:DropDownList ID="ddlDiscount" runat="server" CssClass="inputDdlEnabled" Width="120px" onchange="ShowHideRemarks()">
                                   <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                                   <asp:ListItem Value="Promotion" Text="Promotion"></asp:ListItem>
                                   <asp:ListItem Value="TravelAgent" Text="Travel Agent"></asp:ListItem>
                                   <asp:ListItem Value="EmployeeDiscount" Text="Employee Discount"></asp:ListItem>
                                   <asp:ListItem Value="GroupDiscount" Text="Group Discount"></asp:ListItem>
                                   </asp:DropDownList>
                                </td>
                                </tr>
                                </table>
                                </td>
                            </tr>
                            <tr>
                    <td colspan="3">
                        <table id="tblDiscount" style="display: none; width: 100%">
                            <tr>
                                 <td height="22"  style="width: 29%">
                                   Please Specify :
                                </td>
                                <td width="60%">
                                     <asp:TextBox ID="txtPlease" runat="server" CssClass="inp_00" Width="100px"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                             <td colspan="2">
                             <table width="100%">
                             <tr>
                              <td height="22" style="width: 29%">
                                   Code :
                                </td>
                                <td style="width:60%">
                                     <asp:TextBox ID="txtCode" runat="server" CssClass="inp_00" onblur="CalculateDiscount()"></asp:TextBox>
                                      <asp:Label ID="lblCodeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </td>
                             </tr>
                             </table>
                             </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                        <tr>
                        <td colspan="2">
                         <table>
                                    <tr>
                            <td height="22">
                                Total Amount :
                            </td>
                            <td>
                                <asp:Label ID="lblTotalAmount" runat="server" Text=""></asp:Label>
                            </td>
                            </tr>
                            </table>
                            </td>
                        </tr>
                         
                <tr>
                <td colspan="2">
                    <table id="tblCash" style="display: block; width: 100%">
                        <tr>
                            <td height="22" style="width: 60%">
                                Cash :
                            </td>
                            <td >
                                <asp:TextBox ID="txtCash" runat="server" onkeypress="return isNumber(event)" onkeyup="CalculateBalance()"
                                    onblur="CalculateBalance()" Text="0" Width="60px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="tblCredit" style="display: none; width: 100%">
                            <tr>
                                <td height="22" style="width: 60%">
                                    Credit :
                                </td>
                                <td >
                                    <asp:TextBox ID="txtCredit" runat="server" Text="0" onkeypress="return isNumber(event)" onkeyup="CalculateBalance()" onblur="CalculateBalance()" Width="60px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="tblCard" style="display: none; width: 100%">
                            <tr>
                                <td height="22" style="width:33.5%">
                                    Card :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCard" runat="server" Text="0" onkeypress="return isNumber(event)" onkeyup="CalculateBalance()" onblur="CalculateBalance()" Width="60px"></asp:TextBox>
                                    Charge on Card: <span id="CardAmount"></span>
                                </td>
                            </tr>
                            <tr>
                            <td height="22">
                            Total Charge : 
                            </td>
                            <td >
                            <span id="TotalCardAmount"></span>
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                        
                        <tr>
                    <td colspan="2">
                        <table id="tblNextPayment" style="display: none; width: 100%">
                            <tr>
                                 <td height="22" style="width:29%">
                                   NextPay Date:
                                </td>
                                <td style="width:21%">
                                     <asp:TextBox ID="txtDepDate" runat="server" CssClass="inp_00" Text="DD/MM/YYYY" ReadOnly="true"></asp:TextBox>
                                </td>
                               
                                <td style="width:50%"> <a href="javascript:void(null)" onclick="showCalendar1()" >
                                    <img src="images/call-cozmo.png"/></a></td>
                               
                            </tr>
                        </table>
                    </td>
                </tr>
                           
                <tr>
                 <td colspan="2">
                         <table>
                                    <tr>
                                <td height="22"  style="width: 29%">
                                    Receipt Number:
                                </td>
                                <td width="60%">
                                    <asp:TextBox ID="txtReceiptNo" runat="server" CssClass="inp_00"></asp:TextBox>
                                </td>
                                </tr>
                                </table>
                                </td>
                            </tr>
                
                            <tr>
                                <td colspan="3">
                                    <table id="Table1">
                                        <tr>
                                            <td height="22" style="width: 25%">
                                                Visa Date:
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtVisaDate" runat="server" CssClass="inp_00" Text="DD/MM/YYYY"
                                                    ></asp:TextBox>
                                            </td>
                                            <td style="width: 100%">
                                            <a href="javascript:void(null)" onclick="showCalendar2()">
                                                    <img src="images/call-cozmo.png" /></a>
                                                
                                            </td>
                                            <td>
                                    
                                </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                             <td colspan="2">
                         <table>
                                    <tr>
                            <td height="22" style="width: 15%"></td>
                            <td height="22"><asp:CheckBox ID="chkvisa" runat="server" Text="&nbsp;&nbsp;(Please Check the box to send Email)" Width="300px" Font-Bold="true" /></td>
                           </tr>
                           </table></td>
                            </tr>
                            <tr>
                            <td></td>
                                
                            </tr>
                        <tr>
                            <td colspan="3">
                                <hr class="b_bot_1 " />
                            </td>
                        </tr>
                        </table>
                       <%-- <%} %>--%>
                        </td>
                        </tr>
                        <tr>
                            <td >
                                <div style="font-size: 11px">
                                    Fixed Departure changes may incur penalities and/or increased fares.<br />
                                    Fixed Departure is non-transferable.
                                </div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <label class="mychk1">
                                    <input type="checkbox" name="rules" id="rules" />
                                    <b>I have read and accept the <a style="font-style:italic" href="FixDepTerms.aspx" target=_blank> rules & restrictions</a></b></label>
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="fnt11_gray">(please check the
                                    box to continue) </span>
                                <div id="checkRules" class="error_msg" style="display: none">
                                    <div id="errorMessage" class="error_heading">
                                        Please check rules &amp; restrictions box to confirm booking.
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <hr class="b_bot_1 " />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <table width="100%">
                                    <tbody>
                                        <%--<tr style="background: #ccc" class="nA-h4">
                                            <td>
                                                Available Balance :
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblAgentBalance" runat="server" Font-Bold="true"></asp:Label>                                                
                                            </td>
                                        </tr>--%>
                                         <tr class="nA-h4">
                                            <td>
                                                Total Amount to be Paid :
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblPayable" runat="server" Font-Bold="true"></asp:Label>
                                                <%--<span style="font-weight:bold;" id="ctl00_cphTransaction_lblAmountPaid">AED 124.78</span>--%>
                                            </td>
                                        </tr>
                                        <tr class="nA-h4">
                                            <td>
                                                Discount :
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblDiscount" runat="server" Text="" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="nA-h4">
                                            <td>
                                                Amount Paid :
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblAmountPaid" runat="server" Font-Bold="true"></asp:Label>
                                                <%--<span style="font-weight:bold;" id="ctl00_cphTransaction_lblAmountPaid">AED 124.78</span>--%>
                                            </td>
                                        </tr>
                                        <tr class="nA-h4">
                                            <td>
                                                Amount Received (Incl. Charges):
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblPayableCharge" runat="server" Font-Bold="true"></asp:Label>
                                                <%--<span style="font-weight:bold;" id="ctl00_cphTransaction_lblAmountPaid">AED 124.78</span>--%>
                                            </td>
                                        </tr>
                                        <tr class="nA-h4">
                                            <td>
                                                Balance Amount :
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblBalance" runat="server" Text="" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                <span style="font-weight: bold; color: Red" id="ctl00_cphTransaction_lblBalanceEtrror">
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<a class="blue_button" onclick="PaymentRedirection('<%=( activity.TransactionHeader.Rows[0]["TotalPrice"].ToString() )%>');">
                                    <span>Make Payment</span></a>--%>
                                <asp:Button ID="btnConfirm" CssClass="button-new" runat="server" Text="Confirm" OnClientClick="return validate();"
                                    OnClick="btnConfirm_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label Style="color: Red" ID="lblBalanceEtrror" Font-Bold="true" runat="server"
                                    Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="fff_round">
                    <div class="fff_round_1">
                    </div>
                    <div class="fff_round_2">
                    </div>
                    <div class="fff_round_3">
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
         <div id="Outcontainer1" style="position:absolute;display: none; right:60px; top:164px;  border:solid 0px #ccc;  z-index:200;  " >

    <a onclick="document.getElementById('Outcontainer1').style.display = 'none'">
        <img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />

</a>

<div id="container1" style="border:1px solid #ccc;">
</div>

</div>
 <div id="Outcontainer2" style="position:absolute;display: none; right:60px; top:164px;  border:solid 0px #ccc;  z-index:200;  " >

    <a onclick="document.getElementById('Outcontainer2').style.display = 'none'">
        <img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />

</a>

<div id="container2" style="border:1px solid #ccc;">
</div>

</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

