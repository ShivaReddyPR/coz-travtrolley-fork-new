﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ApiHotelResults.aspx.cs" Inherits="CozmoB2BWebApp.ApiHotelResults" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
        <!-- manual css -->      
        <!-- New BE js -->


        <!-- end new Js -->

        <!-- end -->

        <!-- jQuery -->
        <!-- Bootstrap Core JavaScript -->
       
        <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
        <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
        <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
        <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
        <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />

        <!-- Required styles for Hotel Listing-->
        <link href="build/css/royalslider.css" rel="stylesheet" type="text/css" />
        <link href="build/css/rs-default.css" rel="stylesheet" type="text/css" />
        <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
        <%--<script src="<%=Request.Url.Scheme%>://maps.googleapis.com/maps/api/js?key=AIzaSyB_RGvSBnxHX_IPvIrlEg1TGy3YaIwvwMk" type="text/javascript"></script>--%>

   <script src="<%=Request.Url.Scheme%>://maps.googleapis.com/maps/api/js?key=AIzaSyC0mr4cq7HrZgJb9TZMLYWk-yh-_Ahg9Ro&libraries=places&callback=initAutocomplete" async defer></script>
    <style>
        .error {
            border: 1px solid #D8000C;
        }    
    </style>
    <script type="text/javascript">

        var CorpInfo;

        /* To bind corporate ddl fields */
        function BindCorpInfo()
        {
            CorpInfo = JSON.parse(document.getElementById('<%=hdnCorpInfo.ClientID %>').value);
            var sReason = '<option value="">Select Reason for Travel</option>';

            $.each(CorpInfo.Table1, function (key, col) {

                sReason += '<option value="' + col.ReasonId + '">' + col.Description + '</option>';
            });

            document.getElementById('ddlTravelReasons').innerHTML = sReason;

            var sTraveler = '<option value="">Select Traveller</option>';

            $.each(CorpInfo.Table2, function (key, col) {

                sTraveler += '<option value="' + col.Profile + '">' + col.ProfileName + '</option>';
            });

            document.getElementById('ddlTravelers').innerHTML = sTraveler;

            Setddlval('ddlTravelReasons', request.Corptravelreason, '');
            Setddlval('ddlTravelers', request.Corptraveler, '')
        }

        /* To set bootstrap drop down value based on display text/stored value */
        function Setddlval(id, val, type) {

            if (type == 'text') 
                $("#s2id_" + id).select2('val', $("#" + id + " option").filter(function () { return this.text == val.trim(); }).val());
            else
                $("#s2id_" + id).select2('val', val.trim());

            if (document.getElementById(id).value == '')
                $("#s2id_" + id).select2('val', $("#" + id + " option:first").val());
        }

    </script>
        <script type="text/javascript">
            var Dataofobject;
            var SearchList;
            var SessionId;
            var request;
            var TotalRecords;
            var FilterList = [];
            var HotelNames = [];
            var ModifyCity = [];
            var NoofPages; var PageIndex = 1; var sort = [];
            var decimalvalue;
            var Modify = false;//To handle slider loading for no result page
            var Rating = 0;
            var AgentType;
            var ActiveSource = 0;
            //Scroll Binding Data
            $(window).scroll(function () {
                //var hT = $('#Count').offset().top;
                //var hH = $('#Count').outerHeight();
                //var wH = $(window).height();
                //var wS = $(this).scrollTop();
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
                    //$(window).unbind('scroll');
                    //if (wS > (hT + hH - wH)) {               
                    var Results
                     if (sort.length != 0) {
                        Results = sort;
                    }
                    else if (FilterList.length != 0) {
                        Results = FilterList;
                    }
                    else {
                        Results = SearchList;
                    }
                    PageIndex++;
                    if (PageIndex <= NoofPages)
                        LoadHotels(Results, PageIndex);
                }
            });
            //page load
            $(document).ready(function () {
                //$(".body_container").addClass("loading-progress");
                request = JSON.parse(document.getElementById('<%=hdnobj.ClientID %>').value);
                AgentType = JSON.parse(document.getElementById('<%=hdnAgentType.ClientID %>').value);
                Rating = request.MinRating;
                if (Rating > 0) {
                    $("#rating-" + Rating).prop("checked", true);
                     $("#rating-" + Rating).prop("disabled", true);
                    $('#spanhover').removeClass('star-cb-group withhover');
                      $('#spanhover').addClass('star-cb-group');
                   $(".Allrating").hide();
                    $('.star-rating :input').attr('disabled', true);
                }
                LoadModifySearch();//loading all data and assign for modify search
                SearchHotels();//search Hotels via API

                $('input[type=radio][name=rating]').change(function () {
                    $('#Chkrating').prop("checked", false);
                    $(this).prop("checked", true);

                    AllSort();
                });

                //autocomplete for HotelName and Modify city
                Hotelautocomplete();
                $("#txtCity").autocomplete({
                    source: ModifyCity,
                    minLength: 3,
                    open: function (event, ui) {
                        $("#txtCity").autocomplete("widget").css("width", "243px");
                    }
                });
                $('#Chkrating').change(function () {
                    if ($(this).is(":checked")) {
                        $("#rating-5").prop("checked", true);
                        AllSort();
                    }
                    else {
                        $("#rating-0").prop("checked", true);
                        AllSort();
                    }
                });
                $("#btnFilterLocation").click(function () {
                    AllSort();
                });

                if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '')
                    BindCorpInfo();
            });
             function Hotelautocomplete() {//Hotel Names AutoComplete in filters
                      $("#txtHotelNameSearch").autocomplete({
                    source: HotelNames,
                    minLength: 3,
                    open: function (event, ui) {
                        $("#txtHotelNameSearch").autocomplete("widget").css("width", "285px");
                    },
                    select: function (event, ui) {
                        $("#txtHotelNameSearch").val(ui.item.value);
                        AllSort();
                    }
                });
                }
            function ConvertDate(selector) { // To matach with Main search Date Format
                var from = selector.split("/");
                var Date = from[1] + "/" +from[0]  + "/" + from[2];
                return Date;
}
            //modify Search and updating request obj Session  through web Method and 
            //again search for hotels with modified request
            function ModifySearch() {
                Modify = true;
                var StartDate = ConvertDate($('#txtFromDate').val());
                var EndDate = ConvertDate($('#txtToDate').val());
                var Nationality = $('#ModifyNationality').val();
                var ddlTravReason = $('#ddlTravelReasons').val();
                var ddlTraveler = $('#ddlTravelers').val();
                var rooms = $('#roomCount').val();
                var ModifyHotelName = $("#txtHotelName").val();
                var CityName; var CountryName; var Longitude; var Latitude; var Radius; var Supplierid = []; var MinRating; var MaxRating;

                if ($("#modifyrating").val() == 0) {
                    MinRating = 0;
                    MaxRating = 5;
                }
                else {
                     MinRating =$("#modifyrating").val();
                    MaxRating =$("#modifyrating").val();
                }
                if (request.Longtitude == 0 && request.Latitude == 0) {
                    var City = $('#txtCity').val();
                     CityName = City.split(',')[0];
                     CountryName = City.split(',')[1];
                }
                else {
                        Longitude = $('#modifyLongtitude').val();
                     Latitude = $('#modifyLatitude').val();
                    Radius = ($('#Radius').val() * 1000);
                    CountryName = $("#PointOfInterest").val();
                }
             
                var paxCount = 0; var AdultCount = 0; var ChildCOunt = 0; var MultipleRooms = false; var guestInfo = [];

                if (StartDate == "" || StartDate == null) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Start Date";
                    return false;
                }
                else if (EndDate == "" || EndDate == null) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select End Date";
                    return false;
                }
                else if (!checkRooms()) {
                    return false;
                }
                else if (!hotelSourceValidate()) {
                    return false;
                }
                   else  if (CityName == "" || CityName == null || CityName.length <= "3") {
                    if (request.Longtitude == 0 && request.Latitude == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select City Name";
                        return false;
                    }
                    else {
                        if ((Longitude == "" && Latitude == "") || $("#PointOfInterest").val() == "") {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please Select POI";
                            return false;
                        }
                        else if( $("#Radius").val()<=0){
                             document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Radius";
                    return false;
                        }
                          else {
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";
                }
                    }
                       
                }
                else if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '' && ddlTravReason == '') {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Travel Reason";
                    return false;               
                }
                else if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '' && ddlTraveler == '') {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Traveller";
                    return false;               
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";
                }
                //Starts Loader
                $('#LocationFilter').val("");
                $('#txtHotelNameSearch').val("");
                $("#rating-0").prop("checked", true);
                $("input[name='rating']:checked").val(0);
                $("#Chkrating").prop("checked", false);
                $('#list-group').children().remove();
                $("#sortbyPrice").select2("val", "Lowest");
                $("#Noresults").hide();
                FilterList = []; sort = [];
                $(".body_container").addClass("loading-progress");
                $('#StaticView').children().css('display', 'block');
                NProgress.start();
                PageIndex = 1;
                
                if (rooms > 1) {
                    MultipleRooms = true;
                }
                for (var i = 0; i < rooms; i++) {
                    var noOfAdults = $("#adtRoom-" + (i + 1)).val();
                    paxCount += parseInt(noOfAdults);
                    AdultCount += parseInt(noOfAdults);
                    if ($('#chdRoom-' + (i + 1)).val() > 0) {
                        var noOfChild = $('#chdRoom-' + (i + 1)).val();
                        paxCount += parseInt(noOfChild);
                        ChildCOunt += parseInt(noOfChild);
                        var ChildAge = [];
                        for (var j = 1; j <= noOfChild; j++) {
                            numChild = $("#ChildBlock-" + (i + 1) + "-ChildAge-" + j).val();
                            ChildAge.push(numChild);
                        }

                    }
                    else {
                        var noOfChild = 0;
                        var ChildAge = [];
                    }
                    guestInfo.push({ ChildAge, noOfAdults, noOfChild });
                }
                if (AgentType==3 || AgentType==4) {
                    Supplierid = null;
                }
                else {
                    if (document.getElementById('chklistitem0').checked == true) {
                        Supplierid = null;
                    }
                    else {
                        var $chkbox =  $('#tblSources tr').find('input[type="checkbox"]');
                    if ($chkbox.length) {
                        for (var i = 1; i < $chkbox.length; i++) {
                         if(  document.getElementById('chklistitem'+i).checked == true)
                            Supplierid.push($chkbox[i].value);
                        }
                    }
                    } 
                }
                if (request.Longtitude == 0 && request.Latitude == 0) {
                    Object.assign(request, {
                        Corptravelreason: ddlTravReason,
                        Corptraveler: ddlTraveler,
                        PassengerNationality: Nationality,
                        StartDate: StartDate,
                        EndDate: EndDate,
                        CityName: CityName,
                        CountryName: CountryName,
                        NoOfRooms: rooms,
                        IsMultiRoom: MultipleRooms,
                        RoomGuest: guestInfo,
                        MaxRating: MaxRating,
                        MinRating:MinRating,
                        HotelName:ModifyHotelName ,
                        SupplierIds:Supplierid
                    });
                }
                else {
                    Object.assign(request, {
                        Corptravelreason: ddlTravReason,
                        Corptraveler: ddlTraveler,
                        PassengerNationality: Nationality,
                        StartDate: StartDate,
                         EndDate: EndDate,
                         CountryName: CountryName,
                        Longtitude: Longitude,
                         Latitude: Latitude,
                        Radius:Radius,
                        NoOfRooms: rooms,
                        IsMultiRoom: MultipleRooms,
                        RoomGuest: guestInfo,
                         MaxRating: MaxRating,
                          MinRating:MinRating,
                         HotelName: ModifyHotelName,
                        SupplierIds:Supplierid
                    });
                }
               Rating = request.MinRating;
                if (Rating > 0) {
                    $("#rating-" + Rating).prop("checked", true);
                    $("#rating-" + Rating).prop("disabled", true);
                    $('#spanhover').removeClass('star-cb-group withhover');
                    $('#spanhover').addClass('star-cb-group');
                    $(".Allrating").hide();
                    $('.star-rating :input').attr('disabled', true);
                }
                else {
                    $("#rating-" + Rating).prop("checked", true);
                    $("#rating-" + Rating).prop("disabled", false);
                    $('#spanhover').addClass('star-cb-group withhover');
                    //$('#spanhover').removeClass('star-cb-group');
                    $(".Allrating").show();
                    $('.star-rating :input').attr('disabled', false);
                }
                document.getElementById('<%=hdnobj.ClientID %>').value = request;
                var Jsonrequeststring = JSON.stringify(request);
                  $.ajax({
                    type: "POST",
                    url: "ApiHotelResults.aspx/ModifySearch", 
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: "{'data':'" + Jsonrequeststring + "' }",
                    success: function (data) {
                    },
                    error: function (d) {
                    }
                });
                    //$("#htlPriceRangeSlider").slider("destroy");
                 if ($('#htlPriceRangeSlider').text().length != 0) {
                   $("#htlPriceRangeSlider").slider("destroy");
               }
                $('#list-group').children().remove();
                HotelNames = [];
                SearchHotels();
            }

            //Check for Room(s) Details (i.e Adult,Childs and Ages are empty or not)
             function checkRooms() {
            var submit = true;
            var rooms = document.getElementById('roomCount');
            var room = parseInt(rooms.options[rooms.selectedIndex].value);
            if (room > 0) {
                for (var k = 1; k <= room; k++) {
                    if (submit == false) {
                        break;
                    }
                    if (document.getElementById("adtRoom-" + k).value == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Enter Room " + k + "Details!!";
                        submit = false;
                    }

                    if (document.getElementById("chdRoom-" + k).value > 0) {
                        for (var j = 1; j <= document.getElementById("chdRoom-" + k).value; j++) {
                            var str = "ChildBlock-" + k + "-ChildAge-" + j;
                            //                            str += parseInt(k);
                            //                            str += parseInt(j);
                            if (document.getElementById(str).value < 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter Child-" + (parseInt(j)) + "  Age Details in Room " + (parseInt(k)) + "!!";
                                submit = false;
                                break;
                            }

                        } //for inner for
                    } //end if
                }  //end for
            }
            else {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 room";
                submit = false;
            }

            return submit;
        }


            function LoadModifySearch() {
                var options = '';
                $.ajax({
                    type: "POST",
                    url: "ApiHotelResults.aspx/LoadModifyData", //It calls our web method  
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $.each(data.d, function (index, item) {
                            options += "<option value='" + index + "'>" + item + "</option>";
                        });
                        $('#ModifyNationality').append(options);
                        $("#ModifyNationality").select2("val", request.PassengerNationality);

                    },
                    error: function (d) {
                    }
                });
                if (request.Latitude == 0 && request.Longtitude == 0) {
                    $("#PointOfInterest").hide();
                    $("#divRadius").hide();
                    LoadModifyCity();
                }
                else {
                    $("#txtCity").hide();
                    $('#cityIcon').attr('class', 'icon icon-map-marked-alt');
                     $('#modifyLongtitude').val(request.Longtitude);
                    $('#modifyLatitude').val(request.Latitude);
                       $("#PointOfInterest").val(request.CountryName);
                $("#Radius").val((request.Radius/1000));
                }
               
                //Assign values to Modify search control
                //Selected Rooms adults and childs
                  PersonInfo = request.RoomGuest;
                var htlAdultCount = 0; var htlChildCount = 0;
                $('#roomCount').val(PersonInfo.length);
                for (var i = 0; i < PersonInfo.length; i++) {
                    htlAdultCount += PersonInfo[i].noOfAdults;
                    htlChildCount += PersonInfo[i].noOfChild;
                    document.getElementById('room-' + (i + 1)).style.display = 'flex';
                    $("#adtRoom-" + (i + 1)).val(PersonInfo[i].noOfAdults);
                    $('#chdRoom-' + (i + 1)).val(PersonInfo[i].noOfChild);
                    for (var j = 1; j <= PersonInfo[i].noOfChild; j++) {
                        document.getElementById('ChildBlock-' + (i + 1)).style.display = 'block';
                        document.getElementById('ChildBlock-' + (i + 1) + '-Child-' + j).style.display = 'block';
                        document.getElementById('ChildBlock-' + (i + 1) + '-ChildAge-' + j).value = PersonInfo[i].childAge[j - 1];
                        document.getElementById('PrevChildCount-' + (i + 1)).value = PersonInfo[i].noOfChild;
                    }
                }
                var HotelTotalRooms = 'Room ' + PersonInfo.length;

                var HotelTotalPax = htlAdultCount;
                HotelTotalPax += ' Adult ';
                if (htlChildCount > 0) {
                    HotelTotalPax += ', ' + htlChildCount + ' Child';
                }
                $('#htlTotalRooms').html(HotelTotalRooms);
                $('#htlTotalPax').html(HotelTotalPax);
                document.getElementById('PrevNoOfRooms').value = PersonInfo.length;
                //Date Controls
                FromDate = new Date(request.StartDate);
                $("#txtFromDate").datepicker(
                    {
                        minDate: 0,
                        numberOfMonths: [1, 2],
                        dateFormat: 'dd/mm/yy',
                        onSelect: function (dateText, inst) {
                            var selectedDate = new Date(ConvertDate(dateText));
                            selectedDate.setDate(selectedDate.getDate() + 1);
                            $("#txtToDate").datepicker("option", "minDate", selectedDate);
                            var fromDate = new Date($("#txtFromDate").datepicker("getDate"));
                            var toDate = new Date($("#txtToDate").datepicker("getDate"));
                            if (fromDate > toDate) {
                                $("#txtToDate").datepicker("setDate", selectedDate);
                                // $("#txtToDate").val(selectedDate);

                            }

                        },
                        onchange: function (dateText, inst) {


                        }
                    }
                ).datepicker("setDate", FromDate);
                FromDate.setDate(FromDate.getDate() + 1);
                $("#txtToDate").datepicker(
                    {
                        numberOfMonths: [1, 2],
                        minDate: FromDate,
                         dateFormat: 'dd/mm/yy'
                    }).datepicker("setDate", new Date(request.EndDate));

                $("#txtHotelName").val(request.HotelName);
                    $("#modifyrating").select2("val", request.MinRating);
                if (AgentType == 1 || AgentType == 2) {// For Base and Agent
                    $("#divSupplier").show();
                    BindSuppliers();
                }
            }
            function BindSuppliers() {
            $.ajax({
                    type: "POST",
                    url: "ApiHotelResults.aspx/BindSuppliers", //It calls our web method  
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var items = JSON.parse(data.d);
                    ActiveSource = items.length;
           printSuppliers(items);
                    },
                    error: function (d) {
                    }
                });
            }
            function printSuppliers(data) {
        var table = $('<table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" border="0" cellspacing="0" cellpadding="0"></table>');
                var counter = 1;
                 table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                    type: 'checkbox', name: 'chklistitem', value: "0", id: 'chklistitem0',onclick:'selectHotelSources()'
                })).append(
                $('<label>').attr({
                    for: 'chklistitem0'
                }).text("ALL"))));
                $(data).each(function () {
                         table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                    type: 'checkbox', name: 'chklistitem', value: this.gimmonixSuppId, id: 'chklistitem' + counter,onclick:'setCheck("chklistitem' + counter+'")'
                })).append(
                $('<label>').attr({
                    for: 'chklistitem' + counter++
                }).text(this.Name))));
                      });
               
 
            $('#dvCheckBoxListControl').append(table);
            checkSupplier(data);
            } 
            function checkSupplier(data) {
                var SupplierId = request.SupplierIds;
                if (SupplierId == null) {
                        var $chkbox =  $('#tblSources tr').find('input[type="checkbox"]');
                    if ($chkbox.length) {
                        $chkbox.each(function (i) {
                            $(this).prop("checked", true);
                        });
                    }
                }
                else {
              $('#tblSources tr').each(function(i) {
    var $chkbox = $(this).find('input[type="checkbox"]');
                  if ($chkbox.length) {
                       for(var i=0; i<SupplierId.length; i++){
                           var name = SupplierId[i];
                           var Value = $chkbox[0].value;
    if(name == Value){
      $chkbox.prop("checked", true);
      break;
    }
  }
    }
});
                }
   
            }
            function selectHotelSources() {
            var selected;
            if (document.getElementById('chklistitem0') != null) {
                if (document.getElementById('chklistitem0').checked) {
                    for (i = 1; i <= ActiveSource; i++) {
                        document.getElementById('chklistitem' + i).checked = true;
                    }
                }
                else {
                    for (i = 1; i <= ActiveSource; i++) {
                        document.getElementById('chklistitem' + i).checked = false;
                    }
                }
            }


        }


            function hotelSourceValidate() {
                if (AgentType == 1 || AgentType == 2) {
                    var counter = 0;
                    for (i = 1; i <= ActiveSource; i++) {
                        if (document.getElementById('chklistitem' + i) != null && document.getElementById('chklistitem' + i).checked) {
                            counter++;
                        }
                    }
                    if (AgentType == 3 || AgentType == 4) {
                        counter = 1;
                    }



                    if (counter == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
                        return false;
                    }
                    return true;
                }
                else {
                     return true;
                }
           
        }

 function setCheck(ctrl) {
            if (document.getElementById(ctrl).checked == false) {
                document.getElementById('chklistitem0').checked = false;
            }
        }


            function LoadModifyCity() {
                 $.ajax({
                    type: "POST",
                    url: "ApiHotelResults.aspx/LoadCity", //It calls our web method  
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $.each(data.d, function (index, item) {
                                ModifyCity.push(item);
                            //options += "<option value='" + index + "'>" + item + "</option>";
                        });
                        //$('#ModifyNationality').append(options);
                        //$('#ModifyNationality').val(request.PassengerNationality);
                       

                    },
                    error: function (d) {
                    }
                });
                $('#txtCity').val(request.CityName + "," + request.CountryName);
            }
            function DynamicAPIURL() {
                var url;
               
                var Secure ='<%=Request.IsSecureConnection%>';
                if (Secure == 'True') {
                    url = '<%=Request.Url.Scheme%>'+"://";
            }
            else {
                    url = '<%=Request.Url.Scheme%>'+"://";
                }

             url=url+'<%=Request.Url.Host%>' + "/HotelWebApi";

                return url;
            }
            //Search Hotels via API
            function SearchHotels() {
                var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
                var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
                var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
                var data = { request: request, AgentId: AgentId, UserId: UserId, BehalfLocation: BehalfLocation };
                var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
                
                if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }
                $.ajax({
                    url: apiUrl+'/api/Hotel/GetSearchResult',
                    type: 'POST',
                    ContentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        Dataofobject = JSON.parse(data);
                        SearchList = Dataofobject.Result;
                        SessionId = Dataofobject.SessionId;
                        decimalvalue = Dataofobject.decimalValue;
                        if (SearchList.length == 0) {
                            $('#StaticView').children().css('display', 'none');
                             $('#Heading').hide();
                            $("#Noresults").show();
                    $(".body_container").removeClass("loading-progress");

                        }
                        else {
                            BindResults();
                        }
                        
                                              
                    },
                    complete: function () {
                        NProgress.done();
                    },
                    error: function (xhr, textStatus, errorThrown) {
                       // alert('Error in Operation');
                    }
                });
            }
            function BindResults() {
                  TotalRecords = SearchList.length;        
                if(TotalRecords!=0) {
                    for (var i = 0; i < SearchList.length; i++) {
                        HotelNames.push(SearchList[i].HotelName);
                    }
                    Hotelautocomplete();
                    NoofPages = Math.ceil(TotalRecords / 10);//To display intial number of hotels
                    $('#price-from-range').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount));  //.toFixed(decimalvalue)
                    var last = SearchList[SearchList.length - 1];
                    $('#price-to-range').text(Math.ceil(last.TotalPrice -last.Price.Discount ) );
                    $('#fromrange').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount) );
                    $('#torange').text(Math.ceil(last.TotalPrice -last.Price.Discount) ); 

                    $('#fromrangeCurrency').text(SearchList[0].Currency);
                    $('#torangeCurrency').text(SearchList[0].Currency);                    

                    priceSlider();
                    Modify = false;
                    document.getElementById('<%=hdnHotelResults.ClientID %>').value = SearchList;
                    LoadHotels(SearchList, 1);
                    $(".body_container").removeClass("loading-progress");
                }
            }
            //Slider LOading
            function LoadSlider() {
                var uiMoreButtonClass = $('.ui-list-btn');
                $('.results-list-wrapper').on('click', '.ui-list-btn', function (e) {
                    console.log('TEST')
                    $('.ui-collapsible-panel').hide();
                    $(this).closest('li.item').find('.ui-collapsible-panel').addClass('animated fadeIn').show();
                    uiMoreButtonClass.closest('li.active').removeClass('active');
                    $(this).closest('li').addClass('active');
                    var elemIndex = $(this).attr('href');
                    $('.hotel-listing-tab a[href="' + elemIndex + '"]').tab('show');
                    $('.royalSlider').royalSlider('updateSliderSize', true);
                })
                $('.hotel-listing-tab a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
                    uiMoreButtonClass.closest('li.active').removeClass('active');
                    var elemID = $(this).attr('href');
                    $("a[href='" + elemID + "']").closest('li').addClass('active');
                    $('.royalSlider').royalSlider('updateSliderSize', true);
                })
                //Close Expanded Panel
                $('.ui-close-button').on('click', function () {
                    $(this).closest('.ui-collapsible-panel').hide();
                    $('.ui-list-features').find('li.active').removeClass('active')
                })
                $('.royalSlider').each(function () {
                    var sliderEl = $(this);
                    var sliderOptions = {
                        controlNavigation: 'thumbnails',
                        thumbs: {
                            orientation: 'vertical',
                            paddingBottom: 4,
                            appendSpan: true
                        },
                        transitionType: 'fade',
                        autoScaleSlider: true,
                        autoScaleSliderWidth: 960,
                        autoScaleSliderHeight: 530,
                        loop: true,
                        arrowsNav: true,
                        keyboardNavEnabled: true,
                        imageScaleMode: 'fill'
                    };
                    sliderEl.royalSlider(sliderOptions);
                });

            }
            function priceSlider() {
                var sliderMin =  $('#fromrange').text();
                var sliderMax = $('#torange').text();
            
                $("#htlPriceRangeSlider").slider({
                    range: true,
                    min: Number(sliderMin),
                    max: Number(sliderMax),
                    values: [sliderMin, sliderMax],
                    slide: function (event, ui) {
                        var fromRange = ui.values[0],
                            toRange = ui.values[1];
                        $('#price-from-range').text(fromRange);
                        $('#price-to-range').text(toRange);
                    },
                    change: function (event, ui) {
                        
                        if (Modify == false) {
                            AllSort();
                        }
                        
                    }

                });
                $('#price-from-range').text(sliderMin);
                $('#price-to-range').text(sliderMax);
            }

            //Binding Hotels
            function LoadHotels(HotelList, page) {
                if (HotelList.length == 0) {
                    $('#TotalCount').text('');
                      $('#h2HotelinCity').text('');
                      $('#Heading').show();
                    $('#TotalCount').text(HotelList.length + ' found');
                    if (request.Longitude != 0 && request.Latitude != 0) {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CountryName + '</strong>');
                    } else {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CityName + '</strong>');
                    }
                
                    //alert("No Results Found");
                }
                else {
                    var showFrom = ((Math.ceil(page) - 1) * 10);
                    var showTo = Math.ceil(showFrom) + 9;
                    //document.getElementById("Heading").style.display = "block";
                  $('#TotalCount').text('');
                      $('#h2HotelinCity').text('');
                      $('#Heading').show();
                    $('#TotalCount').text(HotelList.length + ' found');
                     if (request.Longitude != 0 && request.Latitude != 0) {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CountryName + '</strong>');
                    } else {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CityName + '</strong>');
                    }
                    var orgqueueTemplate = $('#templateView').html();
                    var Count = 0;
                    for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {
                        if (i < HotelList.length) {
                            var rating;
                            if (HotelList[i].Rating == 1) {
                                rating = 'one-star-hotel';
                            }
                            else if (HotelList[i].Rating == 2) {
                                rating = 'two-star-hotel';
                            }
                            else if (HotelList[i].Rating == 3) {
                                rating = 'three-star-hotel';
                            }
                            else if (HotelList[i].Rating == 4) {
                                rating = 'four-star-hotel';
                            }
                            else if (HotelList[i].Rating == 5) {
                                rating = 'five-star-hotel';
                            }
                            $('#list-group').append('<li id="List' + i + '" class="item">' + orgqueueTemplate + '</li>');
                            $('#h4HotelName').attr('id', 'h4HotelName-' + i);
                            $('#h4HotelName-' + i).text(HotelList[i].HotelName);
                            $('#pAddress').attr('id', 'pAddress-' + i);
                            $('#pAddress-' + i).text(HotelList[i].HotelAddress);
                            $('#divrating').attr('id', 'divrating-' + i);
                            $('#divrating-' + i).addClass(rating);
                            $('#spanRating').attr('id', 'spanRating-' + i);
                            $('#spanRating-' + i).text(HotelList[i].Rating);
                            $('#HotelImage').attr('id', 'HotelImage-' + i);
                            if (HotelList[i].HotelPicture != null && HotelList[i].HotelPicture!='')
                                $('#HotelImage-' + i).attr("src", HotelList[i].HotelPicture);
                            else 
                            $('#HotelImage-' + i).attr("src", '/images/HotelNA.jpg');
                            $('#Discount').attr('id', 'Discount-' + i);
                            var discountAmount = parseFloat(HotelList[i].TotalPrice) - parseFloat(HotelList[i].Price.Discount);
                            $('#Discount-' + i).html('<em class="currency">' + HotelList[i].Currency + ' </em>' + ' ' + Math.ceil(discountAmount).toFixed(decimalvalue));
                            $('#TotalPrice').attr('id', 'TotalPrice-' + i);
                            $('#TotalPrice-' + i).html('<del><em class="currency">' + HotelList[i].Currency + ' </em>' + ' ' + Math.ceil(HotelList[i].TotalPrice).toFixed(decimalvalue)) + '</del>';
                            if (discountAmount.toFixed(decimalvalue) == HotelList[i].TotalPrice.toFixed(decimalvalue)) {
                                $('#TotalPrice-' + i).children().hide();
                            }
                            else {
                                $('#TotalPrice-' + i).children().show();
                            }
                            $('#btnSelectRoom').attr('id', 'btnSelectRoom-' + i);
                            $('#btnSelectRoom-' + i).attr("onClick", "SelectRoom('" + HotelList[i].HotelCode + "','" + SessionId + "')");
                            $('#HotelDetailsview').attr('id', 'HotelDetailsview-' + i);
                            $('#pills-gallery-tab').attr('id', 'pills-gallery-tab-' + i);
                            $('#pills-location-tab').attr('id', 'pills-location-tab-' + i);
                            $('#pills-overview-tab').attr('id', 'pills-overview-tab-' + i);
                            $('#btnimage').attr('id', 'btnimage-' + i);

                            //$('#btnimage-' + i).attr("onClick", "GetHotelDetails('"+HotelList[i].HotelCode+ "','" + SessionId + "')");
                            $('#btnimage-' + i).prop("href", "#pills-gallery-tab-" + i);
                            $('#btnlocation').attr('id', 'btnlocation-' + i);
                            $('#btnlocation-' + i).prop("href", "#pills-location-tab-" + i);
                            $('#btnDes').attr('id', 'btnDes-' + i);
                            $('#btnDes-' + i).prop("href", "#pills-overview-tab-" + i);
                            $('#btnDetaliGallery').attr('id', 'btnDetaliGallery-' + i);
                            $('#btnDetalilocation').attr('id', 'btnDetalilocation-' + i);
                            $('#btnDetaliOverview').attr('id', 'btnDetaliOverview-' + i);
                            $('#btnDetaliGallery-' + i).prop("href", "#pills-gallery-tab-" + i);
                            $('#btnDetalilocation-' + i).prop("href", "#pills-location-tab-" + i);
                            $('#btnDetaliOverview-' + i).prop("href", "#pills-overview-tab-" + i);
                           $('#HotelDes').attr('id', 'HotelDes-' + i);
                            var images = "";
                            if (HotelList[i].HotelImages != null) {
                                $('#List' + i).find('#Gallery').attr('id', 'Gallery-' + i);
                                for (var j = 0; j < HotelList[i].HotelImages.length; j++) {
                                    images += '<a class="rsImg" data-rsbigimg="build" href="' + HotelList[i].HotelImages[j] + '" data-rsw="700" data-rsh="300"><img width="96" height="72" class="rsTmb" src="' + HotelList[i].HotelImages[j] + '" /></a>';
                                }
                               $('#Gallery-' + i).html(images);
                            }
                          
                            if (HotelList[i].HotelFacilities != null) {
                                for (var j = 0; j < HotelList[i].HotelFacilities.length; j++) {
                                     $('#List' + i).find('#Amenities').attr('id', 'Amenities-' + i);
                                    var amenity = HotelList[i].HotelFacilities[j].trim().toLowerCase();
                                    amenity = amenity.replace(" ", "-");
                                    $('#Amenities-' + i).append(' <li><span class="icon icon-' + amenity + '"></span><span class="text">' + HotelList[i].HotelFacilities[j] + '</span></li>');;
                                }
                            }
                            $('#HotelDes-' + i).append('<p>' + HotelList[i].HotelDescription + '</p>');
                            if (HotelList[i].HotelMap != null) {
                                 $('#List' + i).find('#map_canvas').attr('id', 'map_canvas-' + i);
                                initialize(HotelList[i].HotelMap, i);
                            }

                        }
                    }
                    LoadSlider();
                }
                $('#StaticView').children().css('display', 'none');
            }
            //when we click on SelectRoom it redirect to ApiRoomDetails page
            function SelectRoom(hotelCode, sessionId) {
                window.location.href = "ApiRoomDetails.aspx?hotelCode=" + hotelCode + "&sessionId=" + sessionId;
            }
            //Filters
            function AllFilters() {
                if ($('#txtHotelNameSearch').val() != "" && $("input[name='rating']:checked").val() != 0 && $('#LocationFilter').val() != "") {
                    var Location = new RegExp($('#LocationFilter').val(), 'gi');
                    FilterList = SearchList.filter(function (item) {
                        return item.HotelName == $('#txtHotelNameSearch').val() && item.Rating == $("input[name='rating']:checked").val() && item.HotelAddress.match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
               else if ( $("input[name='rating']:checked").val() != 0 && $('#LocationFilter').val() != "") {
                    var Location = new RegExp($('#LocationFilter').val(), 'gi');
                    FilterList = SearchList.filter(function (item) {
                        return  item.Rating == $("input[name='rating']:checked").val() && item.HotelAddress.match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else if ($('#txtHotelNameSearch').val() != "" && $("input[name='rating']:checked").val() != 0) {
                    var Location = new RegExp($('#LocationFilter').val(), 'gi');
                    FilterList = SearchList.filter(function (item) {
                        return item.HotelName == $('#txtHotelNameSearch').val() && item.Rating == $("input[name='rating']:checked").val() && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else if ($('#txtHotelNameSearch').val() != "" && $('#LocationFilter').val() != "") {
                    var Location = new RegExp($('#LocationFilter').val(), 'gi');
                    FilterList = SearchList.filter(function (item) {
                        return item.HotelName == $('#txtHotelNameSearch').val() && item.HotelAddress.match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else if ($('#txtHotelNameSearch').val() != "") {
                    FilterList = SearchList.filter(function (item) {
                        return item.HotelName == $('#txtHotelNameSearch').val() && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice -item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else if ($('#LocationFilter').val() != "") {
                   
                    var Location = new RegExp($('#LocationFilter').val().toLowerCase(), 'gi');
                    FilterList = SearchList.filter(function (item) {
                        return item.HotelAddress.toLowerCase().match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else if ($("input[name='rating']:checked").val() != 0) {
                    FilterList = SearchList.filter(function (item) {
                        return item.Rating == $("input[name='rating']:checked").val() && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });

                }
                else if ($('#price-from-range').text() != "") {
                    FilterList = SearchList.filter(function (item) {
                        return (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else {
                    FilterList = SearchList;
                }
            }
            //Sort by Price
            function AllSort() {
                sort = [];
                AllFilters();
                if ($('#sortbyPrice').val() == "Highest") {

                    sort = Object.assign([], FilterList);
                    sort = sort.reverse();
                    $('#list-group').children().remove();
                    PageIndex = 1;
                    NoofPages = Math.ceil(sort.length / 10);
                    LoadHotels(sort, 1);
                }
                else {
                    $('#list-group').children().remove();
                    PageIndex = 1;
                    NoofPages = Math.ceil(FilterList.length / 10);
                    LoadHotels(FilterList, 1);
                }
            }
            //clear Filters
            function ClearFilters() {    
                $('#LocationFilter').val("");
                $('#txtHotelNameSearch').val("");
                $("#rating-0").prop("checked", true);
				 $("input[name='rating']:checked").val(0);
                 $("#Chkrating").prop("checked", false);
                $('#list-group').children().remove();
                $("#sortbyPrice").select2("val", "Lowest");
                if (Rating > 0) {
                    $("#rating-" + Rating).prop("checked", true);
                    $("#rating-" + Rating).prop("disabled", true);

                    $(".Allrating").hide();
                    $('.star-rating :input').attr('disabled', true);
                }
                 $('#price-from-range').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount));  //.toFixed(decimalvalue)
                    var last = SearchList[SearchList.length - 1];
                    $('#price-to-range').text(Math.ceil(last.TotalPrice - last.Price.Discount));
                    $('#fromrange').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount));
                    $('#torange').text(Math.ceil(last.TotalPrice - last.Price.Discount));

                    $('#fromrangeCurrency').text(SearchList[0].Currency);
                    $('#torangeCurrency').text(SearchList[0].Currency);
                AllSort();
                // $("#htlPriceRangeSlider").slider("destroy");
                if ($('#htlPriceRangeSlider').text().length != 0) {
                    $("#htlPriceRangeSlider").slider("destroy");
                }
                priceSlider();
            }
            //map
            function initialize(Map, i) {
                var data = Map.split('||');
                myCenter = new google.maps.LatLng(data[0], data[1]);
                var mapProp = {
                    center: myCenter,
                    zoom: 18,
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true,
                    rotateControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map_canvas-" + i), mapProp);


                marker = new google.maps.Marker({
                    position: myCenter,
                    animation: google.maps.Animation.BOUNCE
                });

                marker.setMap(map);
            }
            <%--function GetHotelDetails(HotelCode, SessionId) {
                 var Data = SearchList.filter(function (item) {
                        return item.HotelCode ==HotelCode ;
                    });
                var Results = Data[0];
                  var data = { Results: Results, SessionId: SessionId,request:request };
            var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
                  $.ajax({
                 url: apiUrl+'/api/Hotel/GetHotelDetails',
                 type: 'POST',
                 ContentType: 'application/json; charset=utf-8',
                 dataType: 'json',
                 data: data,
                 success: function (data) {
                     SearchList = data;
                     TotalRecords = SearchList.length;
                     document.getElementById('<%=hdnHotelResults.ClientID %>').value = SearchList;
                        LoadHotels(SearchList);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Error in Operation');
                    }
                });
            }--%>

            $(function () {

                $('#normalSearchFields').on('click', function () {
                    var htlHddDiv = $('#advancedSearchFields');
                    if (htlHddDiv.css('display') == 'none') {
                        htlHddDiv.fadeIn();
                    } 

                    if ($('#divCorpFields').css('display') == 'none' && document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '') {
                        $('#divCorpFields').fadeIn();
                    }                     
                })
            })
            //POINT OF INTEREST
            //PointOfInterst
        var autocomplete;
        var geolocation;
  function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
 autocomplete = new google.maps.places.Autocomplete(
                  /** @type {HTMLInputElement} */(document.getElementById('PointOfInterest')),
                  { types: ['geocode'] });
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
                     var place = autocomplete.getPlace();
          $('#modifyLongtitude').val(place.geometry.location.lng());
           $('#modifyLatitude').val(place.geometry.location.lat());

              });
}

        function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
     geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
        };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
            }

</script>

       <%-- for Adult Child script--%>

        <script>
           
            function ShowRoomDetails() {

                //var f=document.getElementById;

                var rooms = document.getElementById('roomCount');
                if (rooms != null) {
                    var count = eval(rooms.options[rooms.selectedIndex].value);
                    if (document.getElementById('PrevNoOfRooms') != null) {
                        var prevCount = eval(document.getElementById('PrevNoOfRooms').value);

                        if (count > prevCount) {
                            for (var i = (prevCount + 1); i <= count; i++) {

                                document.getElementById('room-' + i).style.display = 'flex';
                                document.getElementById('adtRoom-' + i).value = '1';
                                document.getElementById('chdRoom-' + i).value = '0';
                                document.getElementById('PrevChildCount-' + i).value = '0';

                            }

                        }
                        else if (count < prevCount) {
                            //alert('count' +count+',prev '+prevCount);
                            //alert(document.getElementById('PrevNoOfRooms').value);

                            for (var i = prevCount; i > count; i--) {
                                document.getElementById('room-' + i).style.display = 'none';
                                document.getElementById('adtRoom-' + i).value = '1';

                                var childcount = document.getElementById('chdRoom-' + i).value
                                document.getElementById('ChildBlock-' + i).style.display = 'none';
                                for (var j = 1; j <= childcount; j++) {
                                    document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).value = '-1';
                                    document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).style.display = 'none';
                                    document.getElementById('ChildBlock-' + i + '-Child-' + j).style.display = 'none';
                                }
                                document.getElementById('chdRoom-' + i).value = '0';
                            }
                        }
                    }
                    document.getElementById('PrevNoOfRooms').value = count;
                }
            }
            /*function to show number of childrens    */



            function ShowChildAge(number) {
                var childCount = eval(document.getElementById('chdRoom-' + number).value);
                var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);
                if (eval(document.getElementById('chdRoom-1').value) > 0 || eval(document.getElementById('chdRoom-2').value) > 0 || eval(document.getElementById('chdRoom-3').value) > 0 || eval(document.getElementById('chdRoom-4').value) > 0) {
                    document.getElementById('childDetails').style.display = 'block';
                }
                else {
                    document.getElementById('childDetails').style.display = 'none';
                }
                if (childCount > PrevChildCount) {
                    document.getElementById('ChildBlock-' + number).style.display = 'block';
                    for (var i = (PrevChildCount + 1); i <= childCount; i++) {
                        document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).style.display = 'block';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                        $('#ChildBlock-' + number + '-ChildAge-' + i).select2('destroy');
                    }
                }
                else if (childCount < PrevChildCount) {
                    if (childCount == 0) {
                        document.getElementById('ChildBlock-' + number).style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-1').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-2').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-3').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-4').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-5').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-6').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-3').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-4').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-5').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-6').style.display = 'none';

                    }







                    else {
                        for (var i = PrevChildCount; i > childCount; i--) {
                            if (i != 0) {
                                document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                                document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                            }
                        }
                    }
                }
                document.getElementById('PrevChildCount-' + number).value = childCount;
            }
            function childInfo(id) {
                var index = parseInt(id);
                var se = $("selectChild-" + index);
                if (parseInt(se.value) > 0) {


                    var str = "";

                    str += "  <div style='float:left; width:200px;' id='roomChild" + index + "'>";
                    for (var i = 0; i < se.value; i++) {

                        str += "<span style='float:left; width:95px; padding-bottom:3px;'>";
                        str += "  <label style='float:left; width:40px; padding-top:4px; font-size:11px;'> Child " + (parseInt(i) + 1) + "</label>";
                        str += "         <em style='float:left;'>";
                        str += "            <select style='width:50px;font-size:10px;font-family:Arial' class='room_guests' name='child" + index + (parseInt(i) + 1) + "' id='child" + index + (parseInt(i) + 1) + "'>";
                        str += "                    <option selected='selected' value='-1'>Age?</option>";

                        str += "                    <option value='0'><1</option>";
                        str += "                      <option value='1' >1</option>";
                        str += "                      <option value='2'>2</option>";
                        str += "                    <option value='3'>3</option>";
                        str += "                        <option value='4'>4</option>";
                        str += "                        <option value='5'>5</option>";
                        str += "                        <option value='6'>6</option>";
                        str += "                     <option value='7'>7</option> ";
                        str += "                     <option value='8'>8</option>";
                        str += "                      <option value='9'>9</option>"
                        str += "                      <option value='10'>10</option>";
                        str += "                      <option value='11'>11</option>";
                        str += "                   <option value='12'>12</option>";
                        str += "                    <option value='13'>13</option> ";
                        str += "                      <option value='14'>14</option>";
                        str += "                      <option value='15'>15</option>";
                        str += "                     <option value='16'>16</option> ";
                        str += "                    <option value='17'>17</option>";
                        str += "                      <option value='18'>18</option>";

                        str += "               </select>";
                        str += "       </em>";
                        str += "        </span>";
                    }
                    str += "  </div>";

                    $("childInfo-" + index).innerHTML = str;
                    $("childInfo-" + index).style.display = "block";
                }
                else {
                    $("roomChild" + index).style.display = "none";

                }


            }
            function roomInfo(id) {

                if (parseInt(id) == 0) {
                    var se = $('roomCount');
                }
                else {
                    var se = $('norooms');
                }
                if (parseInt(se.value) > 1) {
                    $('guestsInfo').style.display = "none";
                }
                else {
                    $('guestsInfo').style.display = "none";
                    return;

                }
                var str = "";
                for (var i = 2; i <= se.value; i++) {
                    str += "   <div style='float:left; width:220px;' id='noOfPax-" + i + "'>";
                    str += "<b style='float:left; padding:8px 5px 0 0;'>Room " + parseInt(i) + "</b>";
                    str += "      <div class='no_of_guests'>";
                    str += "     <select class='guests'  name='adultCount-" + parseInt(i) + "' id='adultCount-" + parseInt(i) + "'>";
                    str += "               <option selected='selected' value='1' >1</option>";
                    str += "               <option value='2' >2</option>";
                    str += "               <option value='3'>3</option>";
                    str += "               <option value='4'>4</option>";
                    str += "        </select>";
                    str += "       <span>Adults</span>";
                    str += "       <b>(18+ yrs)</b>";
                    str += "   </div>";
                    str += "   <div class='no_of_guests' >";
                    str += "       <select class='guests' id='selectChild-" + parseInt(i) + "' name='childCount-" + parseInt(i) + "' onchange='javascript:childInfo(" + parseInt(i) + ")'>";
                    str += "               <option selected='selected' value='0'>none</option>";
                    str += "               <option value='1'>1</option>";
                    str += "               <option value='2'>2</option>";
                    str += "           </select>";
                    str += "       <span>Children</span>";
                    str += "       <b>(Till 18 yrs)</b>";
                    str += "   </div>";
                    str += "   </div>";
                    str += "<div id='childInfo-" + i + "' style='display:none'></div>"
                }
                $('guestsInfo').innerHTML = str;
                $('guestsInfo').style.display = "block";

            }
            </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.hotel-pax-dropdown select').change(function () {
                    hotelPaxCount();
                });
				//Main Loader Starts
				NProgress.start();
            });
            function hotelPaxCount() {
                var roomCount = $('#PrevNoOfRooms').val();
                var htlAdultCount = 0,
                    htlChildCount = 0;
                $('#hotelRoomSelectDropDown select.adult-pax').each(function (index) {
                    count = index + 1;
                    if (count > roomCount) {
                        return false;
                    }
                    htlAdultCount += parseInt($(this).val());
                })
                $('#hotelRoomSelectDropDown select.child-pax').each(function (index) {
                    count = index + 1;
                    if (count > roomCount) {
                        return false;
                    }
                    htlChildCount += parseInt($(this).val());
                });
				var HotelTotalRooms = 'Room ' + roomCount;
				
                var HotelTotalPax = htlAdultCount;
                HotelTotalPax += ' Adult ';
                if (htlChildCount > 0) {
                    HotelTotalPax += ', ' + htlChildCount + ' Child';
                }
                $('#htlTotalRooms').html(HotelTotalRooms);
                $('#htlTotalPax').html(HotelTotalPax);
            }
            hotelPaxCount();

        </script>
        <input type="hidden" id="hdnobj" runat="server" />
        <input type="hidden" id="hdnUserId" runat="server" />
        <input type="hidden" id="hdnAgentId" runat="server" />
        <input type="hidden" id="hdnBehalfLocation" runat="server" />
        <input type="hidden" id="hdnHotelResults" runat="server" />
        <input type="hidden" id="hdnGuestInfo" runat="server" />
     <input type="hidden" id="hdnAgentType" runat="server" />
        <input type="hidden" id="hdnCorpInfo" runat="server" />
        <div class="body_container ui-listing-page loading-progress">
            <!--Modify Panel-->
            <div class="ui-modify-wrapper">
                  <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" class="alert-messages alert-caution">
                    </div>
                </div>

                <div class="ui-hotel-modify-search">



                    <div class="row custom-gutter">
                        <div class="col-md-10">
                            <div class="row custom-gutter">
                                <div class="col-12" id="normalSearchFields">
                                    <div class="row custom-gutter">
                                        <div class="col-md-6 col-lg-6 col-xl-5 mb-2 mb-xl-0">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-location" id="cityIcon"></span>
                                                </div>
                                                <input class="form-control" type="text"  id="txtCity">
                                                  <input type="text" class="form-control" placeholder="Point of Interest" id="PointOfInterest" onFocus="geolocate()" name="location"/>
                                               

                                                 <input type="hidden" id="modifyLongtitude" />
                                                 <input type="hidden" id="modifyLatitude" />

                                                <div class="inputTypeAddkm-wrap ml-2 float-left" id="divRadius" style="width: 120px;">                                              
                                                     <input type="number" class="form-control small inputTypeAddkm" id="Radius"  value="0"/>                                                                           
                                                </div>  

                                            </div>
                                        </div>
                                    
                                <div class="col-md-3 col-lg-3 col-xl-2 mb-2 mb-xl-0">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="icon icon-calendar"></span>
                                        </div>

                                        <input class="form-control" type="text" id="txtFromDate">
                                    </div>

                                </div>

                                <div class="col-md-3 col-lg-3 col-xl-2 mb-2 mb-xl-0">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="icon icon-calendar"></span>
                                        </div>

                                        <input class="form-control" type="text" id="txtToDate">
                                    </div>

                                </div>
                                <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="icon icon-language"></span>
                                        </div>

                                        <select class="form-control" id="ModifyNationality">
                                        </select>
                                    </div>
                                </div>
                          
                                    </div>
                                </div>

                                <div class="col-12" id="advancedSearchFields" style="display:none">
                                    <div class="row custom-gutter">
                                        <div class="col-md-6 col-xl-3 mb-2 mb-md-0 mt-md-2">
                                            <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#hotelRoomSelectDropDown">
                                        <div class="icon-holder">
 	                                         <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text">                                        	
											<em id="htlTotalRooms" class="room-label">Room 1</em>
											<em class="pax-label" id="htlTotalPax"> 1 Adult</em>    
                                        </span>           
                                    </a>
                                    <div class="dropdown-content d-none p-4 hotel-pax-dropdown" id="hotelRoomSelectDropDown">     
                                        <div class="row custom-gutter">
                                            <div class="col-4">
                                                ROOM 
                                            </div>
                                            <div class="col-6">
                                                  <input type="hidden" value="1" name="PrevNoOfRooms" id="PrevNoOfRooms" />
                                                  <div class="form-control-holder">
                                                     <select class="form-control small no-select2 htl-room-select" id="roomCount" name="roomCount" onchange="ShowRoomDetails();">
                                                        <option selected="selected">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <%--<option value="5">5</option>
                                                        <option value="6">6</option>--%>
                                                    </select>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-1">                                           
                                            <div class="col-12"> <label class="room-label">Room 1</label></div>
                                            <div class="col-4">
                                                 <div class="form-group">  
                                                       <label>Adults</label> 
                                                    <select class="form-control no-select2 adult-pax" name="adtRoom-1" id="adtRoom-1">
                                                                        <option selected="selected" value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                    </select>
                                                                    <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1" />  
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                 <div class="form-group">                                                     
                                                     <label> Child(2-12 yrs)</label> 
                                                     <select onchange="javascript:ShowChildAge('1')" name="chdRoom-1" 
                                                            id="chdRoom-1" class="form-control no-select2  child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                 </div>
                                                
                                            </div>
                                            <div class="col-12" id="ChildBlock-1" name="ChildBlock-1" style="display: none; width: 100%;">
                                                 <div class="row no-gutters child-age-wrapper">                                                     
                                                    <div class="col-4" id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display: none;">
                                                        <div class="form-group">                                                     
                                                             <label>Child 1</label> 
                                                             <select class="form-control no-select2" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option> 
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="col-4" id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display: none;">
                                                        <div class="form-group">                                                     
                                                             <label>Child 2</label> 
                                                             <select class="form-control no-select2" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 3</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 4</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display: none;" >
                                                        <div class="form-group">
                                                            <label>Child 5</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-6" name="ChildBlock-1-Child-6" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 6</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-6" name="ChildBlock-1-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="col-12" id="childDetails" style="display: none;">
                                                         <!--<p>Child Details</p>-->
                                                     </div>



                                                 </div>
                                            </div>


                                        </div>     
                                        
                                        <div class="row custom-gutter room-wrapper" id="room-2" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 2</label></div>
                                            <div class="col-4">
                                                 <div class="form-group">  
                                                       <label>Adults</label> 
                                                         <select name="adtRoom-2" id="adtRoom-2" class="form-control no-select2 adult-pax">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                        </select>
                                                       <input type="hidden" value="0" id="PrevChildCount-2" name="PrevChildCount-2" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                      <label> Child (2-12 yrs)</label> 
                                                      <select onchange="javascript:ShowChildAge('2')" name="chdRoom-2" id="chdRoom-2" class="form-control no-select2  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                     </select>     
                                                </div>
                                            </div>
                                            <div class="col-12"  id="ChildBlock-2" name="ChildBlock-2" style="display: none;">
                                              <div class="row no-gutters child-age-wrapper">  
                                                <div class="col-4"  id="ChildBlock-2-Child-1" name="ChildBlock-2-Child-1">
                                                   <div class="form-group">  
                                                    <label>Child 1</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-1" name="ChildBlock-2-ChildAge-1">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-2" name="ChildBlock-2-Child-2" style="display: none;">
                                                   <div class="form-group">  
                                                    <label>Child 2</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-2" name="ChildBlock-2-ChildAge-2">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-3" name="ChildBlock-2-Child-3" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 3</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-3" name="ChildBlock-2-ChildAge-3">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-4" name="ChildBlock-2-Child-4" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 4</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-4" name="ChildBlock-2-ChildAge-4">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-5" name="ChildBlock-2-Child-5" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 5</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-5" name="ChildBlock-2-ChildAge-5">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-6" name="ChildBlock-2-Child-6" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 6</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-6" name="ChildBlock-2-ChildAge-6">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-3" style="display: none;">  
                                                <div class="col-12"> <label class="room-label">Room 3</label></div>
                                                    <div class="col-4" >
                                                        <div class="form-group">
                                                            <label>Adults</label> 
                                                            <select name="adtRoom-3" id="adtRoom-3" class="form-control no-select2 adult-pax">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                             </select>
                                                             <input type="hidden" value="0" id="PrevChildCount-3" name="PrevChildCount-3" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4" >
                                                        <div class="form-group">
                                                            <label> Children (2-12 yrs)</label> 
                                                            <select onchange="javascript:ShowChildAge('3')" name="chdRoom-3" id="chdRoom-3" class="form-control  no-select2  child-pax">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12"  id="ChildBlock-3" name="ChildBlock-3" style="display: none;">
                                                        <div class="row no-gutters child-age-wrapper">     
                                                                <div class="col-4"  id="ChildBlock-3-Child-1" name="ChildBlock-3-Child-1" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 1</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-1" name="ChildBlock-3-ChildAge-1">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-2" name="ChildBlock-3-Child-2" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 2</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-2" name="ChildBlock-3-ChildAge-2"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-3" name="ChildBlock-3-Child-3" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 3</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-3" name="ChildBlock-3-ChildAge-3"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-4" name="ChildBlock-3-Child-4" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 4</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-4" name="ChildBlock-3-ChildAge-4"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-5" name="ChildBlock-3-Child-5" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 5</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-5" name="ChildBlock-3-ChildAge-5"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-6" name="ChildBlock-3-Child-6" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 6</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-6" name="ChildBlock-3-ChildAge-6"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>      
                                                        </div>
                                                    </div>

                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-4" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 4</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                          <select name="adtRoom-4" id="adtRoom-4" class="form-control no-select2 adult-pax">
                                                            <option selected="selected" value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                        <input type="hidden" value="0" id="PrevChildCount-4" name="PrevChildCount-4" /> 
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>  Child (2-12 yrs)</label> 
                                                    <select onchange="javascript:ShowChildAge('4')" name="chdRoom-4" id="chdRoom-4" class="form-control no-select2 child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>  

                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-4" name="ChildBlock-4" style="display: none;">
                                                <div class="row no-gutters child-age-wrapper">   
                                                    <div class="col-4" id="ChildBlock-4-Child-1" name="ChildBlock-4-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-1" name="ChildBlock-4-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-2" name="ChildBlock-4-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-2" name="ChildBlock-4-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-3" name="ChildBlock-4-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-3" name="ChildBlock-4-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-4" name="ChildBlock-4-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-4" name="ChildBlock-4-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-5" name="ChildBlock-4-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-5" name="ChildBlock-4-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-6" name="ChildBlock-4-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-6" name="ChildBlock-4-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-5" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 5</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                       <select name="adtRoom-5" id="adtRoom-5" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-5" name="PrevChildCount-5" />    
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                     <select onchange="javascript:ShowChildAge('5')" name="chdRoom-5" id="chdRoom-5" class="form-control  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-5" name="ChildBlock-5" style="display: none;">                                             
                                                    <div class="row no-gutters child-age-wrapper">
                                                        <div class="col-4" id="ChildBlock-5-Child-1" name="ChildBlock-5-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-1" name="ChildBlock-5-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-2" name="ChildBlock-5-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-2" name="ChildBlock-5-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-3" name="ChildBlock-5-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-3" name="ChildBlock-5-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-4" name="ChildBlock-5-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-4" name="ChildBlock-5-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-5" name="ChildBlock-5-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-5" name="ChildBlock-5-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-6" name="ChildBlock-5-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                          <label>Child 6</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-6" name="ChildBlock-5-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>  
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-6" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 6</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                     <select name="adtRoom-6" id="adtRoom-6" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-6" name="PrevChildCount-6" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                          <select onchange="javascript:ShowChildAge('6')" name="chdRoom-6" id="chdRoom-6" class="form-control child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>  
                                                </div>
                                            </div>                                            
                                            <div class="col-12" id="ChildBlock-6" name="ChildBlock-6" style="display: none;">
                                                 <div class="row no-gutters child-age-wrapper">
                                                    <div class="col-4" id="ChildBlock-6-Child-1" name="ChildBlock-6-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 16</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-1" name="ChildBlock-6-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-2" name="ChildBlock-6-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 26</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-2" name="ChildBlock-6-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-3" name="ChildBlock-6-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 36</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-3" name="ChildBlock-6-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-4" name="ChildBlock-6-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 46</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-4" name="ChildBlock-6-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-5" name="ChildBlock-6-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 56</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-5" name="ChildBlock-6-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-6" name="ChildBlock-6-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-6" name="ChildBlock-6-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                                                                                             
                              
  
                                  
                                                </div>
                                          </div>
                                        </div>

                                         <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-md-2">

                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-star"></span>
                                                </div>

                                                <select  id="modifyrating" Class="form-control">
                                                    <option  value="0">Show All</option>
                                                    <option Value="1">1 rating</option>
                                                    <option Value="2">2 rating</option>
                                                    <option Value="3">3 rating</option>
                                                    <option Value="4">4 rating</option>
                                                    <option Value="5">5 rating</option>
                                                </select> 
                                            </div>

                                    
                                         </div>
                                         <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-0 mt-md-2" id="divSupplier" style="display:none;">
                                  <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class=" form-control-element with-custom-dropdown" data-dropdown="#HtlSupplierDropdown">
                                        <div class="form-control-holder mb-0 ">
                                            <div class="icon-holder">                                       
                                                 <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text mt-2" id="htlSelectedSuppliers">Search Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="HtlSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Search Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12" id="dvCheckBoxListControl">                               
<%--                                                <table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" border="0" cellspacing="0" cellpadding="0"></table>  --%>
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                              </div>
                                         </div>
                                         <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-md-2">
                                             <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-bed"></span>
                                                </div>

                                                <input type="text" placeholder="Search By Hotel Name" Class="form-control" id="txtHotelName" /> 
                                            </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-12" id="divCorpFields" style="display:none">
                                    <div class="row custom-gutter">
                                        <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                            <div class="input-group">
                                                <select class="form-control" id="ddlTravelReasons"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                            <div class="input-group">
                                                <select class="form-control" id="ddlTravelers"></select>
                                            </div>
                                        </div>
                                    </div>                                                                    
                                </div>


                            </div>
                        </div>
                        <div class="col-md-2 d-flex align-content-center align-items-center justify-content-center">
                            <input type="button" class="btn btn-primary font-weight-bold w-100" style="height: 38px;" value="MODIFY" id="btnModify" onclick="ModifySearch();"/>
                        </div>
                    </div>



                </div>
            </div>
            <div class="results-list-wrapper pt-3">
                <div class="row">
<!--
                    <div class="col-12 listing-progress">	
				<h6 class="text-center">Please wait while we search for the best hotels</h6>				
				<div class="progress">							
					<div class="determinate" style="width: 70%"></div>
				 </div>					
			</div>
-->
                    <div class="col-12 col-lg-3 d-none d-lg-block" id="filter-panel">
                        <div class="row">
                            <div class="col-md-12 filter-text">
                                <span>FILTER </span><a id="ClearFilter" href="javascript:void(0);" onclick="ClearFilters();" >Clear all</a>
                            </div>
                        </div>
                        <div class="ui-left-panel">

                            <div class="row">

                                <div class="col-md-12 mb-1 left-panel-heading">HOTEL NAME </div>

                                <div class="col-md-12">

                                    <div class="input-group">

                                        <input class="form-control  ui-serch-by-hotel" placeholder="Search by Hotel Name" id="txtHotelNameSearch" onkeydown="return (event.keyCode!=13);">
                                       <%-- <span class="input-group-append">                
			 <button  class="btn btn-outline-primary border-left-0 border" type="button" id="btnFilterHotelName">                 
				<span class="icon icon-search"></span> </button>
							</span>--%>
                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">STAR RATING </div>

                                <div class="col-md-12 filter-category">
									<div class="custom-checkbox-style">
										<input type="checkbox" id="Chkrating" name="rating" value="0" class="Allrating" />
										<label for="rating-0" class="Allrating">All Stars</label>
									</div>
                                    <div class="star-rating">
                                        <fieldset>
                                            <span id="spanhover" class="star-cb-group withhover">
                                                <input type="radio" id="rating-5" name="rating" value="5" /><label for="rating-5" class="lblratingclass-5">5</label>
                                                <input type="radio" id="rating-4" name="rating" value="4" /><label for="rating-4" class="lblratingclass-4">4</label>
                                                <input type="radio" id="rating-3" name="rating" value="3" /><label for="rating-3" class="lblratingclass-3">3</label>
                                                <input type="radio" id="rating-2" name="rating" value="2" /><label for="rating-2" class="lblratingclass-2">2</label>
                                                <input type="radio" id="rating-1" name="rating" value="1" /><label for="rating-1" class="lblratingclass-2">1</label> 
                          <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" checked="checked"/><label for="rating-0">0</label>     												
                                            </span>
                                        </fieldset>
                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">PRICE </div>

                                <div class="col-md-12">
                                    <div class="filter-item price-slider">

                                        <div class="price-slider-text">
                                            <div class="float-left">
                                                <small id="fromrangeCurrency"></small> 
                                                <span class="range" id="price-from-range"></span>
                                                <span style="display:none;" id="fromrange"></span>
                                            </div>
                                            <div class="float-right">

                                                <small id="torangeCurrency"></small>   
                                                <span class="range" id="price-to-range"></span>
                                                 <span style="display:none;" id="torange"></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="htlPriceRangeSlider" ></div>

                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">LOCATION </div>

                                <div class="col-md-12">
                                     <div class="input-group">
                                <input type="text" id="LocationFilter" class="form-control custom-select" placeholder="Enter Location" onkeydown="return (event.keyCode!=13);"/>
                                     <span class="input-group-append">                
			 <button  class="btn btn-outline-primary border-left-0 border" type="button" id="btnFilterLocation">                 
				<span class="icon icon-search"></span> </button>
							</span>
</div>
                                </div>



                                <div class="col-md-12 filter-category" style="display:none">

                                    <div class="custom-checkbox-style mt-4">
                                        <input type="checkbox" />
                                        <label class="lbl-select-all">HOTEL CHAIN </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Accor - Le Club Accor </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Choice - Choice Privileges </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Hyatt - World of Hyatt </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Jumeirah Hotel and Resorts </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Hilton - Hilton Honors </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Emaar Hospitality Group </label>
                                    </div>

                                </div>


                                <div class="col-md-12 filter-category"  style="display:none">
                                    <div class="custom-checkbox-style mt-4">
                                        <input type="checkbox" />
                                        <label class="lbl-select-all">PROPERTY TYPE </label>
                                    </div>
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" />
                                        <label>Hotels </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" />
                                        <label>B&B and Inns </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" />
                                        <label>Specialty Lodging </label>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <button type="button" id="filterDoneMobile" class="pl-3 d-block d-lg-none btn-block btn-primary btn mt-3 font-weight-bold">
                            APPLY
                        </button>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="ui-result-header">
                            <div class="float-left mb-3" id="Heading" style="display:none;">
                                <h2 class="float-left" id="h2HotelinCity"></h2>
                                <span class="primary-bgcolor ui-bg-highlighted float-left" id="TotalCount"></span>
                            </div>
                            <div class="float-right mb-3 d-block d-lg-none">

                                <a href="javascript:void(0);" id="filterBtnMobile" class="mt-1 ml-2 btn btn-outline-secondary">
                                    <span class="icon icon-filter"></span>
                                </a>
                            </div>

                            <div class="float-left float-sm-right mb-3 mb-md-0">

                                <div class="ui-list-control-btn-group">
                                    <ul>
                                        <li class="normal-text">SORT BY</li>
                                        <li>
                                            <div class="btn-group" id="sortDropdown">
                                                <select id="sortbyPrice" class="form-control" onchange="AllSort();">
                                                    <option value="Lowest">Lowest Price</option>
                                                    <option value="Highest">Highest Price</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li style="display: none;">
                                            <a href="javascript:void(0);">
                                                <img src="build/img/map-view.svg" alt="Hotel Listing Map View">
                                                MAP VIEW
                                            </a>
                                        </li>
                                        <li class="active" style="display: none;"><a href="javascript:void(0);">
                                            <span class="icon icon-list-view"></span>
                                            LIST VIEW</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>



                        <div class="clearfix"></div>

           	<ul class="ui-listing-wrapper list-unstyled" id="StaticView">
							<li class="item">
								        <div>
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <%--<img src="123" alt="Hotel Name" class=" h-100">--%>
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div ></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"</span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
							</li>	
				
															<li class="item">
								        <div >
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div ></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"></span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0" >SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
							</li>		
                   							<li class="item">		
                                                    <div >
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div ></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"></span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
							</li>	
															<li class="item">
   <div >
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"></span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
								       
							</li>	
				
						</ul>
                         <div class="inn_wrap01" id="Noresults" style=" display:none;">
                    <div class="ns-h3">
                        No Hotels found
                    </div>
                    <div style="text-align: center; font-weight: bold">
                        search Again
                    </div>
                    <div class="clear">
                    </div>
                </div>

                        <ul class="ui-listing-wrapper list-unstyled" id="list-group">
                        </ul>

                    </div>
                </div>
            </div>
        </div>
            
         <div id="templateView" style="display:none;">
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100" id="HotelImage">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column">
                    <h4 id="h4HotelName"></h4>
                    <p class="address mb-0" id="pAddress"></p>
                    <div class="mb-auto mt-2">
                        <div id="divrating" class="star-rating-list float-left"></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" id="spanRating"></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" id="btnimage">
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false" id="btnlocation">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false" id="btnDes">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator">
                    <div class="ui-list-price text-center">
                        <span class="price d-inline-block d-sm-block strikeout" id="TotalPrice"></span>
                      <span class="price d-inline-block d-sm-block " id="Discount"></span>
                        
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0" id="btnSelectRoom">SELECT ROOM</button>
                    </div>
                </div>
            </div>
            <div class="ui-collapsible-panel exapanded-view" id="HotelDetailsview">
                <a href="javascript:void(0);" class="ui-close-button"><span class="icon icon-close"></span></a>
                <div class="ui-list-collapse-content" id="hotel-listing-collapse-1">
                    <div class="card card-body">
                        <ul class="nav nav-pills mb-3 hotel-listing-tab" id="hotel-listing-tab-2" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link active" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-2" aria-selected="true" id="btnDetaliGallery">Photo Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-2" aria-selected="false" id="btnDetalilocation">Location</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-2" aria-selected="false" id="btnDetaliOverview">Overview</a>
                            </li>
                        </ul>
                        <div class="tab-content hotel-listing-tabContent" id="hotel-listing-tabContent-2">
                            <div class="tab-pane fade show active in" id="pills-gallery-tab" role="tabpanel" aria-labelledby="pills-gallery-tab">

                                <!--Gallery-->
                                <div class="royalSlider rsUni" id="Gallery">

                                </div>
                                 
                            </div>
                            <div class="tab-pane fade" id="pills-location-tab" role="tabpanel" aria-labelledby="pills-location-tab-2">
                               
                                <div id="map_canvas" style="width:100%;height:380px;"></div>

                            </div>
                            <div class="tab-pane fade" id="pills-overview-tab" role="tabpanel" aria-labelledby="pills-overview-tab-2">

                                <h6 class="pl-3 mt-3 mb-0 font-weight-bold">HOTEL AMENITIES</h6>
                                <ul class="amenities-list" id="Amenities">
                                </ul>
                                <div class="clearfix"></div>
                                <h6 class="my-3 px-3 font-weight-bold">HOTEL OVERVIEW</h6>
                                <div class="hotel-description px-3" id="HotelDes">
                                  
                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <script src="build/js/jquery.royalslider.min.js"></script>
    <script src="build/js/jquery-ui.js"></script>
    <script src="build/js/hotel-listing.js"></script> 
    <script src="build/js/nprogress.js"></script>
        </span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
