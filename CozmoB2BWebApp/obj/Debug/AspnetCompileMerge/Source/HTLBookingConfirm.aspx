﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="HTLBookingConfirmUI" Title="Hotel Booking Confirmation" Codebehind="HTLBookingConfirm.aspx.cs" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">


<script type="text/javascript">
    if (history.length > 1) {
        history.forward(1);
    }
    function validateTerms() {
        if (document.getElementById('chkTerms').checked == true) {
            document.getElementById('validate').style.display = "none";
            <%if (!Settings.LoginInfo.AgentBlock) //checking Agent able to book or not Added By brahmam
    {%>
            if (document.getElementById('<%=hdnWarning.ClientID %>').value.length <= 0 && document.getElementById('<%=ddlPayment.ClientID %>').value != "Card") {
                document.getElementById('MainDiv').style.display = "none";
                document.getElementById('PreLoader').style.display = "block";
            }
            history.forward(1);
            return true;
            <%}
    else
    {%>
            alert('Please contact Administrator'); //We are Showing Alert Message
            return false;
            <%}%>
        }
        else {
            document.getElementById('validate').style.display = "block";
            document.getElementById('MainDiv').style.display = "block";
            document.getElementById('PreLoader').style.display = "none";
            return false;
        }
    }
    function CardChargeEnable() {
        if (document.getElementById('<%=ddlPayment.ClientID %>').value == "Card") {
            document.getElementById('divCardCharge').style.display = "block";
            document.getElementById('lblCardCharges').innerHTML = parseFloat(eval('<%=charges %>')).toFixed(eval('<%=decimalPoint %>')); ;
        }
        else {
            document.getElementById('divCardCharge').style.display = "none";
            document.getElementById('lblCardCharges').value = "";
        }
    }
</script>

    <asp:HiddenField ID="hdnWarning" runat="server" />
    <asp:HiddenField ID="hdncardCharge" runat="server" Value="0" />
    <div id="MainDiv">
        <div>
            <div>
                <div class=" col-md-3 padding-0 bg_white">
                    <div class="ns-h3">
                        Pricing Details
                    </div>
                    <div>
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="59%">
                                    Hotel Cost
                                </td>
                                <td width="41%">
                                    <%=(itinerary.Currency)%>
                                    <%=Math.Round(total, decimalPoint).ToString("N" + decimalPoint)%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    (Inclusive of all Taxes &amp; Fees)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Total</strong>
                                </td>
                                <td>
                                    <strong>
                                        <%=(itinerary.Currency)%>
                                        <%=Math.Round(total, decimalPoint).ToString("N" + decimalPoint)%></strong>
                                </td>
                            </tr>
                            <%if (discount > 0)
                              { %>
                            <tr>
                                <td>
                                    <strong class="spnred">Discount</strong>
                                </td>
                                <td>
                                    <strong>
                                        <%=(itinerary.Currency)%>
                                        <span style="text-align: right; width: 100%;">
                                            <%=(discount).ToString("N" + decimalPoint)%></span></strong>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td>
                                    <strong class="spnred">Total Inc. Markup</strong>
                                </td>
                                <td>
                                    <strong>
                                        <%=(itinerary.Currency)%>
                                        <%=(markup > 0 ? markup : total).ToString("N" + decimalPoint)%></strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="ns-h3">
                        Hotel Review</div>
                    <div>
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="42%">
                                    <strong>Hotel name :</strong>
                                </td>
                                <td width="58%">
                                    <%=itinerary.HotelName.Split('|')[0]%>
                                </td>
                            </tr>
                            <%--<tr>
                            <td colspan="2">
                                (Room Only)
                            </td>
                        </tr>--%>
                            <tr>
                                <td>
                                    <strong>City Name : </strong>
                                    <br />
                                </td>
                                <td>
                                    <%=itinerary.CityRef %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Check in:
                                        <br />
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.StartDate.ToString("dd-MMM-yyyy") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Check out:
                                        <br />
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.EndDate.ToString("dd-MMM-yyyy")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Nights: </strong>
                                </td>
                                <td>
                                    <%=itinerary.EndDate.Subtract(itinerary.StartDate).Days %>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="ns-h3">
                        Room Details</div>
                    <div>
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <%int adults = 0, childs = 0; %>
                            <%for (int i = 0; i < itinerary.Roomtype.Length; i++) %>
                            <%{
                                  adults += itinerary.Roomtype[i].AdultCount;
                                  childs += itinerary.Roomtype[i].ChildCount;
                              } %>
                            <tr>
                                <td width="42%">
                                    <strong>Rooms :</strong>
                                </td>
                                <td width="58%">
                                    <%=itinerary.Roomtype.Length %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Passengers :<br />
                                    </strong>
                                </td>
                                <td>
                                    Adult
                                    <%=adults%>, Child
                                    <%=childs %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Price :</strong>
                                </td>
                                <td>
                                    <strong>
                                        <%=(itinerary.Currency)%>
                                        <%=Math.Ceiling(total - discount).ToString("N" + decimalPoint.ToString())%></strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class=" col-md-9">
                    <div class="ns-h3">
                        Payment Confirmation</div>
                    <div class="bg_white bor_gray marbot_20" id="">
                        <div class="wrap10">
                            <div class="pad-10-LR">
                                <asp:MultiView ID="PaymentMultiView" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="NormalView" runat="server">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                                <div>
                                                                    <table class="table902" width="99%" border="0" cellspacing="0" cellpadding="0">
                                                                        <%foreach (HotelRoom room in itinerary.Roomtype)
                                                                            {%>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <strong>
                                                                    <%= room.RoomName +"-"+room.MealPlanDesc %></strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="red_span" colspan="2">
                                                                <strong>Lead Guest</strong>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <% foreach (HotelPassenger pax in room.PassenegerInfo)
    {
        if (pax.LeadPassenger)
        {%>
                                                        <tr>
                                                            <td width="34%">
                                                                <strong>Name :<br />
                                                                </strong>
                                                            </td>
                                                            <td width="66%">
                                                                <%  string paxName = "";
    paxName = pax.Firstname + " " + pax.Lastname;
                                                                %>
                                                                <%=pax.Title %>.
                                                                <%=paxName%><br />
                                                            </td>
                                                        </tr>
                                                        <%-- <tr>
                                                    <td>
                                                        <strong>Address :
                                                            <br />
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <%=itinerary.HotelPassenger.Addressline1%> <%=itinerary.HotelPassenger.Addressline2%><br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Mobile no:<br />
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <%=itinerary.HotelPassenger.Phoneno%><br />
                                                    </td>
                                                </tr>--%>
                                                        <tr>
                                                            <td>
                                                                <strong>E-mail ID:</strong>
                                                            </td>
                                                            <td>
                                                                <%=pax.Email%>
                                                            </td>
                                                        </tr>
                                                        <%}
    }%>
                                                        <%if (room.PassenegerInfo.Count > 1)
    { %>
                                                        <tr>
                                                            <td colspan="2" class="red_span">
                                                                <strong>Other Guests</strong>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%foreach (HotelPassenger pass in room.PassenegerInfo)
    {
        if (!pass.LeadPassenger)
        {%>
                                                        <tr>
                                                            <td>
                                                                <%=pass.Title%>.
                                                                <%=pass.Firstname%>
                                                                <%=pass.Lastname%>
                                                            </td>
                                                        </tr>
                                                        <%}
        }
    }%>
                                                    </table>
                            </div>
                            </td> </tr> </table> </td> </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <% 

                                    %>
                                    <table class="table902" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <%if (itinerary.Source == HotelBookingSource.DOTW || itinerary.Source == HotelBookingSource.HotelBeds || itinerary.Source == HotelBookingSource.RezLive || itinerary.Source == HotelBookingSource.Miki || itinerary.Source == HotelBookingSource.TBOHotel || itinerary.Source == HotelBookingSource.WST  || itinerary.Source == HotelBookingSource.OYO )
    { %>
                                                <b>Hotel Norms</b><br />
                                                <%string[] notes = remarks.Split('|'); %>
                                                <ul>
                                                    <%foreach (string note in notes)
    { %>
                                                    <li>
                                                        <%=note%></li>
                                                    <%} %>
                                                </ul>
                                                <%}
                                                    else if (itinerary.Source == HotelBookingSource.GRN)
    {
        if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
        { %>
                                                <b>Hotel Norms</b><br />
                                                <%string[] notes = itinerary.HotelPolicyDetails.Split('|'); %>
                                                <ul>
                                                    <%foreach (string note in notes)
    { %>
                                                    <li>
                                                        <%=note%></li>
                                                    <%} %>
                                                </ul>
                                                <%}
    }
    else if (itinerary.Source == HotelBookingSource.Agoda)
    {
        if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
        { %>
                                                <b>Hotel Norms</b><br />
                                                <ul>
    
                                                    <li>
                                                        <%=itinerary.HotelPolicyDetails%></li>
                                                    
                                                </ul>
                                                <%}
    } %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Cancellation &amp; Charges</b><br />
                                                <%string[] cdata = cancelData.Split('|');%>
                                                <ul>
                                                    <%foreach (string data in cdata)
    { %>
                                                    <li>
                                                        <%=data %></li>
                                                    <%} %>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="color: Red; font-size: 14px">
                                        <%=warningMsg %>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td height="21">
                                    <hr class="b_bot_1 " />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right">
                                    <table width="100%">
                                        <tr class="nA-h4">
                                            <td>
                                                Available Balance :
                                            </td>
                                            <td style="font-weight: bold">
                                                <%=agency.CurrentBalance.ToString("N" + decimalPoint)%>
                                            </td>
                                        </tr>
                                        <tr class="nA-h4">
                                            <td>
                                                <%if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                    { %>
                                                Total GST :
                                        <%}else if(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode=="IN")
                                            {%>
                                                Total GST :
                                            <%}
                                            else
                                            {%>
                                                Vat :
                                        <%}%>
                                            </td>
                                            <td style="font-weight: bold">
                                                <%=outPutVat.ToString("N" + decimalPoint)%>
                                            </td>
                                        </tr>
                                        <tr class="nA-h4">
                                            <td>
                                                Amount to be Booked :
                                            </td>
                                            <td style="font-weight: bold">
                                                <%=(Math.Ceiling(total) + Math.Ceiling(outPutVat)).ToString("N" + decimalPoint)%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="21">
                                    <hr class="b_bot_1 ">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table class="table902" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="97%" align="left">
                                                <label>
                                                    <input type="checkbox" name="chkTerms" id="chkTerms"  />
                                                    I have reviewed and agreed on the rates and commision offered for this booking
                                                </label>
                                                <div id="validate" style="display: none; color: Red">
                                                    <span>Please check the Terms checkbox to continue</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="2" align="left">
                             <label>
                             <asp:DropDownList ID="ddlPayment" runat="server" CssClass="form-control" Enabled="false" onChange="CardChargeEnable();">
                             <asp:ListItem Text="OnAccount" Value="Credit" Selected="True"></asp:ListItem>
                             <asp:ListItem Text="Credit Card" Value="Card"></asp:ListItem>
                             </asp:DropDownList>
                           
                             </label>
                             <div style="display:none;color:Red;font-weight:bold;" id="divCardCharge">Credit Card Charges
                            <label id="lblCardCharges" style="color:Red;font-weight:bold;"></label> % applicable on Total Amount
                            </div>
                            </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <label style="padding-right: 10px">
                                    <%if (!string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                                      { %>
                                        <asp:Button ID="imgMakePayment" runat="server" CssClass="btn but_b" Text="Confirm"
                                            OnClick="imgMakePayment_Click" OnClientClick="return validateTerms();" />
                                            <%} %>
                                    </label>
                                </td>
                            </tr>
                            </table> </asp:View>
                            <asp:View ID="ErrorView" runat="server">
                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                                <br />
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/HotelSearch.aspx?source=Hotel">Go to Search Page</asp:HyperLink>
                                <br />
                                <%--<asp:ImageButton ID="ImageButton1" runat="server"  
                            ImageUrl="~/images/continue.jpg" onclick="ImageButton1_Click"/>--%>
                            </asp:View>
                            </asp:MultiView>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
            </div>
            </div>
            <div class="clearfix">
        </div>
        </div>
            <div class="col-md-12">
                <div id="PreLoader" style="display: none">
                    <div class="loadingDiv">
                        <div>
                            <img src="images/preloader11.gif" /></div>
                        <div style="font-size: 16px; color: #999999">
                            <strong>Awaiting confirmation from</strong>
                        </div>
                        <div class="primary-color" style="font-size: 21px">
                            <strong><span id="searchCity">
                                <%=itinerary.HotelName %></span> </strong>
                            <br />
                        </div>

                        <div> 
                        
                        
                            <strong style="font-size: 14px; color: black">do not click the refresh or back button
                                as the transaction may be interrupted or terminated.</strong>
                        </div>
                    </div>
                    <div class="parameterDiv">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%">
                                    <label class="primary-color">
                                        <strong>Check In:</strong></label>
                                    <span id="fromDate">
                                        <%=itinerary.StartDate.ToString("dd/MM/yyyy") %></span>
                                </td>
                                <td width="50%">
                                    <label class="primary-color">
                                        <strong>Check out:</strong></label>
                                    <span id="toDate">
                                        <%=itinerary.EndDate.ToString("dd/MM/yyyy") %></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        <%--</div>
    </div>
    --%>
    
    
   
    
    
   
   
</asp:Content>
