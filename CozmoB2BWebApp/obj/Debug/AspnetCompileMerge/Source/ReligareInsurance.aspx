﻿<%@ Page Title="Religare Insurance" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareInsurance.aspx.cs" Inherits="CozmoB2BWebApp.ReligareInsurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modall" style="display: none">
                <br />
                <center>
                <div class="center"> 
                     <b style="text-align:center;color:red;">Loading. Please wait.</b><br /><br />
                    <img src="images/ajaxLoader.gif" alt="Loading" height="100" width="100" />
                </div>
                    </center>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="body_container" style="margin-top: 20px;">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <span>Select Agent Type</span>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:RadioButton ID="rbself" runat="server" Text="Self" AutoPostBack="true" Checked="true" GroupName="agents" OnCheckedChanged="rbself_CheckedChanged" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:RadioButton ID="rbb2b" runat="server" Text="Agent" AutoPostBack="true" GroupName="agents" OnCheckedChanged="rbself_CheckedChanged" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" runat="server" id="divagent" visible="false">
                            <span>Select Agent</span>
                            <asp:DropDownList ID="ddlagents" runat="server" CssClass="form-control" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlagents_SelectedIndexChanged">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorddlagents" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" runat="server" id="divAgentLoc" visible="false">
                            <span>Select Agent Location</span>
                            <asp:DropDownList ID="ddlAgentLoc" runat="server" CssClass="form-control" Visible="false">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorddllocation" style="color: red;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:Panel ID="pnlRadioButtons" runat="server">
                                <asp:Label ID="lblInsType" runat="server" Style="float: left;" Text="Select Product Type"></asp:Label>
                                &nbsp;
                               <%-- <div id="divInsTypes" runat="server" style="margin-top: 5px;">
                                </div> --%>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Travelling To</span>
                            <asp:DropDownList ID="ddlTravelling" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTravelling_SelectedIndexChanged">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorTravelling" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Trip Type</span>
                            <asp:DropDownList ID="ddlTripType" CssClass="form-control" Style="background-color: #fff;" runat="server">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                <asp:ListItem Value="SINGLE">Single</asp:ListItem>
                                <asp:ListItem Value="MULTI">Multi</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorTripType" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3" runat="server" id="divstartdate">
                        <div class="form-group">
                            <span>Start Date</span>
                            <div class="input-group date txtstartDate">
                                <asp:TextBox ID="txtstart" runat="server" Style="display: none;" />
                                <%-- onchange="javascript:DateDifference()"--%>
                                <asp:TextBox ID="txtStartDate" ReadOnly="true" runat="server" onchange="javascript:DateDifference()" TabIndex="1" CssClass="form-control" Style="background-color: #fff;" />
                                <asp:Label runat="server" class="input-group-addon" ID="spstartdate"><i class="glyphicon glyphicon-calendar"></i></asp:Label>
                            </div>
                            <span id="errorStartDate" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <%--runat="server" id="divEnddate"--%>
                        <div class="form-group">
                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" />
                            <asp:DropDownList ID="ddlTerm" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged" TabIndex="2" Visible="false">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                            </asp:DropDownList>
                            <div class="input-group date txtstartDate" id="divenddate" runat="server">

                                <asp:TextBox ID="txtend" runat="server" Style="display: none;" />
                                <asp:TextBox ID="txtEndDate" onchange="javascript:DateDifference()" ReadOnly="true" TabIndex="2" runat="server" CssClass="form-control" Style="background-color: #fff;" />
                                <asp:Label class="input-group-addon" ID="spanaddon" runat="server"><i class="glyphicon glyphicon-calendar"></i></asp:Label>
                            </div>
                            <span id="errorEndDate" style="color: red;"></span>
                        </div>
                    </div>


                    <div class="col-md-3" id="divterm" runat="server">
                        <div class="form-group">
                            <asp:Label ID="lbldays" runat="server" Text="Days" />
                            <asp:TextBox ID="txtDays" runat="server" onchange="javascript:AddDays()" TabIndex="3" onKeyPress="return isNumber(event)" CssClass="form-control" Style="background-color: #fff;" />

                            <asp:DropDownList ID="ddltripPeriod" CssClass="form-control" runat="server" TabIndex="3" Visible="false">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                <asp:ListItem Value="45">45</asp:ListItem>
                                <asp:ListItem Value="60">60</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorDays" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>PED</span>
                            <asp:DropDownList ID="ddlPED" TabIndex="4" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlPED_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                <asp:ListItem Value="0">Yes</asp:ListItem>
                                <asp:ListItem Value="1">No</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorPED" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Travellers</span>
                            <asp:DropDownList ID="ddlTravellers" CssClass="form-control" TabIndex="5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTravellers_SelectedIndexChanged">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorTravellers" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Sum Insured</span>
                            <asp:DropDownList ID="ddlSumIns" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSumIns_SelectedIndexChanged">
                                <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                            </asp:DropDownList>
                            <span id="errorSumIns" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Table ID="pnltravelers" class="table-style-form" Style="width: 100%; border: 0" runat="server">
                            </asp:Table>
                            <span id="errorpnltravelers" style="color: red; float: right; margin-right: 50px;"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3" runat="server" id="divpremium" visible="false">
                        <div class="form-group">
                            <span>Your Premium is</span>
                            <asp:Label ID="lblPremium" runat="server" Text="0" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <asp:Button ID="btnsave" CssClass="btn but_b" Style="float: right;" OnClick="btnsave_Click" runat="server" Text="Continue" OnClientClick="javascript:return insuranceValidation();" />
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="transparentCover"></div>
            <div class="loading">
                <b>Loading. Please wait.</b><br />
                <br />
                <img src="images/ajaxLoader.gif" alt="Loading" height="100" width="100" />
               <img src="http://www.mytreedb.com/uploads/mytreedb/loader/ajax_loader_blue_512.gif" alt="" height="100" width="100" /> 
         </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
    <script src="Temp/jquery-ui-1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="Temp/jquery-ui-1.12.1/jquery-ui.css">
    <script type="text/javascript"> 
        function pageLoad() {
            var dtToday = new Date();
            var month = dtToday.getMonth();
            var day = dtToday.getDate();
            var year = dtToday.getFullYear();
            if (month < 10)
                month = '0' + month.toString();
            if (day < 10)
                day = '0' + day.toString();

            var maxDate = day + '/' + month + '/' + year;
            $("#<%=txtStartDate.ClientID%>").datepicker({
                changeYear: true,
                changeMonth: true,
                minDate: maxDate,
                yearRange: "0:+1",
                dateFormat: "dd/M/yy",
            }).val();
            $("#<%=txtEndDate.ClientID%>").datepicker({
                changeYear: true,
                changeMonth: true,
                minDate: maxDate,
                yearRange: "0:+2",
                dateFormat: "dd/M/yy",
            }).val();
            $("#<%=txtStartDate.ClientID%>").datepicker("option", "minDate", new Date(year, month, day, 1));
            $("#<%=txtEndDate.ClientID%>").datepicker("option", "minDate", new Date(year, month, day, 1));
        }

        function AddDays() {
            debugger;
            var days = $("#<%=txtDays.ClientID%>").val();
            var startDate = $("#<%=txtStartDate.ClientID%>").datepicker('getDate');
            var endDate = $("#<%=txtEndDate.ClientID%>").datepicker('getDate');
            var date = new Date(startDate);
            var day = 0;
            var diff = Math.floor((endDate.getTime() - startDate.getTime()) / 86400000); // ms per day

            if (days > diff) {
                day = parseInt(date.getDate()) + parseInt(days - 1);
                date.setDate(day);
                formatDate(date);
                $("#<%=txtEndDate.ClientID%>").val(formatDate(date));
                $("#<%=txtend.ClientID%>").val(formatDate(date));
            }
            else {
                day = parseInt(date.getDate()) + parseInt(days - 1);
                date.setDate();
                $("#<%=txtEndDate.ClientID%>").val(formatDate(date));
                $("#<%=txtend.ClientID%>").val(formatDate(date));
            }

           <%-- if (diff >= 180) {
                $("#<%=txtEndDate.ClientID%>").val('');
                $("#<%=txtend.ClientID%>").val('');
                $("#<%=txtDays.ClientID%>").val('');
                alert('Maximum Plan apply Lessthan 180 Days.');
            }--%>
        }

        function formatDate(date) {
            var monthNames = ["Jan", "Feb", "Mar", "April", "May", "Jun", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            var index = parseInt(monthIndex) + 1;
            if (day <= 9)
                day = '0' + day;
            if (index <= 9)
                index = '0' + index;

            return day + '/' + monthNames[monthIndex] + '/' + year;
            //   return index+ '/' + day+ '/' + year;
        }
        function DateDifference() {
            var startDate = $("#<%=txtStartDate.ClientID%>").datepicker('getDate');
            var endDate = $("#<%=txtEndDate.ClientID%>").datepicker('getDate');
            $("#<%=txtStartDate.ClientID%>").datepicker('hide');
            $("#<%=txtEndDate.ClientID%>").datepicker('hide');
            var edate = $("#<%=txtEndDate.ClientID%>").val();
            var diff = 0;
            if (startDate != null && endDate != null && edate != undefined) {
                $("#<%=txtstart.ClientID%>").val($("#<%=txtStartDate.ClientID%>").val());
                $("#<%=txtend.ClientID%>").val($("#<%=txtEndDate.ClientID%>").val());
                diff = Math.floor((endDate.getTime() - startDate.getTime()) / 86400000); // ms per day
                if (diff > 0) {
                    $("#<%=txtDays.ClientID%>").val(diff + 1);
                }
                else {
                    alert('StartDate and EndDate must be difference two.');
                    $("#<%=txtEndDate.ClientID%>").val('');
                    $("#<%=txtDays.ClientID%>").val('');
                    $("#<%=txtend.ClientID%>").val('');
                }
            }
            //  $("#<%=txtEndDate.ClientID%>").focus();
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $("#<%=txtEndDate.ClientID%>").change(function () {
                $("#<%=ddlPED.ClientID%>").focus();
            });
            $("#<%=txtStartDate.ClientID%>").change(function () {
                $("#<%=txtstart.ClientID%>").val($("#<%=txtStartDate.ClientID%>").val());
                //hiding dates
                var dtToday = new Date($("#<%=txtStartDate.ClientID%>").val());
                var montharr = { '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec' };
                var month = dtToday.getMonth();
                var day = dtToday.getDate();
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();
                var maxDate = day + '/' + month + '/' + year;
                $("#<%=txtEndDate.ClientID%>").datepicker({
                    changeYear: true,
                    changeMonth: true,
                    minDate: maxDate,
                    yearRange: "0:+5",
                    dateFormat: "dd/M/yy",
                }).val();
                $("#<%=txtEndDate.ClientID%>").datepicker("option", "minDate", new Date(year, month, day, 1));

                var ddlTripType = $("#<%=ddlTripType.ClientID%>").val();
                if (ddlTripType == "MULTI") {
                    var date1 = $("#<%=txtStartDate.ClientID%>").datepicker('getDate');
                    var date2 = $("#<%=txtStartDate.ClientID%>").datepicker('getDate');
                    date2.setDate(date2.getDate() - 1);
                    date2.setFullYear(date2.getFullYear() + 1);
                    $("#<%=txtEndDate.ClientID%>").datepicker('setDate', date2);
                    var millisecondsPerDay = 1000 * 60 * 60 * 24;
                    var millisBetween = date2.getTime() - date1.getTime();
                    var days = millisBetween / millisecondsPerDay;
                    //Checking Lead Year or not
                    var year = date2.getFullYear();

                    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) && date1.getMonth() > 0)
                        $("#<%=txtDays.ClientID%>").val(Math.floor(366));
                    else
                        $("#<%=txtDays.ClientID%>").val(Math.floor(365));

                    $("#<%=txtstart.ClientID%>").val($("#<%=txtStartDate.ClientID%>").val());
                    $("#<%=txtend.ClientID%>").val($("#<%=txtEndDate.ClientID%>").val());
                }
                $("#<%=txtStartDate.ClientID%>").focus();
            });
            $("#<%=spanaddon.ClientID%>").click(function () {
                $("#<%=txtEndDate.ClientID%>").datepicker('show');
            });
            $("#<%=spstartdate.ClientID%>").click(function () {
                $("#<%=txtStartDate.ClientID%>").datepicker('show');
            });          
        }
        $(document).ready(function () {
            $("#<%=txtEndDate.ClientID%>").change(function () {
                $("#<%=ddlPED.ClientID%>").focus();
            });


            $("#<%=txtStartDate.ClientID%>").change(function () {
                $("#<%=txtstart.ClientID%>").val($("#<%=txtStartDate.ClientID%>").val());
                var ddlTripType = $("#<%=ddlTripType.ClientID%>").val();
                //hiding dates
                var dtToday = new Date($("#<%=txtStartDate.ClientID%>").val());
                var montharr = { '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec' };
                var month = dtToday.getMonth();
                var day = dtToday.getDate();
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();
                var maxDate = day + '/' + month + '/' + year;
                $("#<%=txtEndDate.ClientID%>").datepicker({
                    changeYear: true,
                    changeMonth: true,
                    minDate: maxDate,
                    yearRange: "0:+5",
                    dateFormat: "dd/M/yy",
                }).val();
                $("#<%=txtEndDate.ClientID%>").datepicker("option", "minDate", new Date(year, month, day, 1));

                if (ddlTripType == "MULTI") {
                    var date1 = $("#<%=txtStartDate.ClientID%>").datepicker('getDate');
                    var date2 = $("#<%=txtStartDate.ClientID%>").datepicker('getDate');
                    date2.setDate(date2.getDate() - 1);
                    date2.setFullYear(date2.getFullYear() + 1);
                    $("#<%=txtEndDate.ClientID%>").datepicker('setDate', date2);
                    var millisecondsPerDay = 1000 * 60 * 60 * 24;
                    var millisBetween = date2.getTime() - date1.getTime();
                    var days = millisBetween / millisecondsPerDay;
                    var year = date1.getFullYear();
                    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
                        $("#<%=txtDays.ClientID%>").val(Math.floor(366));
                    else
                        $("#<%=txtDays.ClientID%>").val(Math.floor(365));

                    $("#<%=txtDays.ClientID%>").val(Math.floor(days - 1));
                    $("#<%=txtstart.ClientID%>").val($("#<%=txtStartDate.ClientID%>").val());
                    $("#<%=txtend.ClientID%>").val($("#<%=txtEndDate.ClientID%>").val());
                }
            });
            $("#<%=spanaddon.ClientID%>").click(function () {
                $("#<%=txtEndDate.ClientID%>").datepicker('show');
            });
            $("#<%=spstartdate.ClientID%>").click(function () {
                $("#<%=txtStartDate.ClientID%>").datepicker('show');
            });
          
        });
    </script>
    <script type="text/javascript">
        function insuranceValidation() {
            var isvalid = true;

            var isChecked = $("#ctl00_cphTransaction_rbb2b").is(":checked");
            if (isChecked) {
                var ddlagents = $("#<%=ddlagents.ClientID%>").val();
                if (ddlagents == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlagents").focus();
                    $("#s2id_ctl00_cphTransaction_ddlagents").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlagents").innerText = "Please select Agent.";
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlagents").css("border-color", "");
                    $('#errorddlagents').val('');
                }
            }


            var ddlTravelling = $("#<%=ddlTravelling.ClientID%>").val();
            if (ddlTravelling == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlTravelling").focus();
                $("#s2id_ctl00_cphTransaction_ddlTravelling").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorTravelling").innerText = "Please select Travelling.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlTravelling").css("border-color", "");
                $('#errorTravelling').val('');
            }
            var ddlPED = $("#<%=ddlPED.ClientID%>").val();
            if (ddlPED == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlPED").focus();
                $("#s2id_ctl00_cphTransaction_ddlPED").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorPED").innerText = "Please select PED.";
            } else {
                $("#s2id_ctl00_cphTransaction_ddlPED").css("border-color", "");
                $('#errorPED').val('');
            }

            var ddlTravellers = $("#<%=ddlTravellers.ClientID%>").val();
            if (ddlTravellers == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlTravellers").focus();
                $("#s2id_ctl00_cphTransaction_ddlTravellers").css("border-color", "Red");
                document.getElementById("errorTravellers").innerText = "Please select Travellers.";
                isvalid = false;
            } else {
                $("#s2id_ctl00_cphTransaction_ddlTravellers").css("border-color", "");
                $('#errorTravellers').val('');
            }

            var txtStartDate = $("#<%=txtStartDate.ClientID%>").val();
            if (txtStartDate == "") {
                $("#<%=txtStartDate.ClientID%>").focus();
                $("#<%=txtStartDate.ClientID%>").css("border-color", "Red");
                $("#<%=txtStartDate.ClientID%>").datepicker("hide");
                document.getElementById("errorStartDate").innerText = "Please select Start Date.";
                isvalid = false;
            } else {
                $("#<%=txtStartDate.ClientID%>").css("border-color", "");
                $('#errorStartDate').val('');
            }

            var txtEndDate = $("#<%=txtEndDate.ClientID%>").val();
            if (txtEndDate == "") {
                $("#<%=txtEndDate.ClientID%>").focus();
                $("#<%=txtEndDate.ClientID%>").css("border-color", "Red");
                $("#<%=txtEndDate.ClientID%>").datepicker("hide");
                document.getElementById("errorEndDate").innerText = "Please select End Date.";
                isvalid = false;
            } else {
                $("#<%=txtEndDate.ClientID%>").css("border-color", "");
                $('#errorEndDate').val('');
            }

            var txtDays = $("#<%=txtDays.ClientID%>").val();
            if (txtDays == "") {
                $("#<%=txtDays.ClientID%>").focus();
                $("#<%=txtDays.ClientID%>").css("border-color", "Red");
                document.getElementById("errorDays").innerText = "Please enter the days.";
                isvalid = false;
            } else {
                $("#<%=txtDays.ClientID%>").css("border-color", "");
                $('#errorDays').val('');
            }

            var ddlTripType = $("#<%=ddlTripType.ClientID%>").val();
            if (ddlTripType == "-1") {
                $("#ctl00_cphTransaction_ddlTripType").focus();
                $("#ctl00_cphTransaction_ddlTripType").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorTripType").innerText = "Please select Trip Type.";
            } else {
                $("#ctl00_cphTransaction_ddlTripType").css("border-color", "");
                $('#errorTripType').val('');
            }

            var ddlSumIns = $("#<%=ddlSumIns.ClientID%>").val();
            if (ddlSumIns == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlSumIns").focus();
                $("#s2id_ctl00_cphTransaction_ddlSumIns").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorSumIns").innerText = "Please select Sum Insured.";
            } else {
                $("#s2id_ctl00_cphTransaction_ddlSumIns").css("border-color", "");
                $('#errorSumIns').val('');
            }


            var travellerCount = $("#<%=ddlTravellers.ClientID%>").val();
            if (travellerCount != "-1") {
                for (var i = 1; i <= travellerCount; i++) {
                    var travellerAge = $("#ctl00_cphTransaction_ddlTravellerAge" + i).val();
                    if (travellerAge == "-1") {
                        $("#s2id_ctl00_cphTransaction_ddlTravellerAge" + i).focus();
                        $("#s2id_ctl00_cphTransaction_ddlTravellerAge" + i).css("border-color", "Red");
                        document.getElementById("errorpnltravelers").innerText = "Please select Traveller Age."
                        isvalid = false;
                    }
                    else {
                        $("#s2id_ctl00_cphTransaction_ddlTravellerAge" + i).css("border-color", "");
                        $('#errorpnltravelers').val('');
                    }
                }
            }
            var checked = $('#ctl00_cphTransaction_rbInsType1').is(":checked");
            if (ddlTripType == "MULTI" || checked) {
                var ddltripPeriod = $("#<%=ddltripPeriod.ClientID%>").val();
                if (ddltripPeriod == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddltripPeriod").focus();
                    $("#s2id_ctl00_cphTransaction_ddltripPeriod").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorDays").innerText = "Please select the trip Period.";
                } else {
                    $("#s2id_ctl00_cphTransaction_ddltripPeriod").css("border-color", "");
                    $('#errorDays').val('');
                }
            }
            if (checked) {
                var ddltripPeriod = $("#<%=ddltripPeriod.ClientID%>").val();
                if (ddltripPeriod == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddltripPeriod").focus();
                    $("#s2id_ctl00_cphTransaction_ddltripPeriod").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorDays").innerText = "Please select the Trip Period.";
                } else {
                    $("#s2id_ctl00_cphTransaction_ddltripPeriod").css("border-color", "");
                    $('#errorDays').val('');
                }

                var ddlTerm = $("#<%=ddlTerm.ClientID%>").val();
                if (ddlTerm == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlTerm").focus();
                    $("#s2id_ctl00_cphTransaction_ddlTerm").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorEndDate").innerText = "Please select the Term In Months.";
                } else {
                    $("#s2id_ctl00_cphTransaction_ddlTerm").css("border-color", "");
                    $('#errorEndDate').val('');
                }
            }

            return isvalid;
        }
        //function ShowProgress() {
        //    setTimeout(function () {
        //        var modal = $('<div />');
        //        modal.addClass("modal");
        //        $('body').append(modal);
        //        var loading = $(".loading");
        //        loading.show();
        //        $(".transparentCover").show();
        //        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        //        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        //        loading.css({ top: top, left: left });
        //    }, 200);

        //} 

    </script>

    <style type="text/css">
        .modall {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=20);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .center {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 200px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        .ui-accordion .ui-accordion-content {
            height: auto !important;
        }

        .transparentCover {
            display: none;
            position: absolute;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            background: #dbdee9;
            opacity: 0.4;
            filter: alpha(opacity=40);
            z-index: 100;
        }

        .loading {
            display: none;
            position: fixed;
            left: 50%;
            top: 50%;
            background-color: white;
            height: 200px;
            width: 200px;
            text-align: center;
            /* background:url('http://gocozmo.com/images/preloaderFlight.gif') no-repeat center 50%;
            background: url('http://www.mytreedb.com/uploads/mytreedb/loader/ajax_loader_blue_512.gif') no-repeat center 50%;*/
            z-index: 100;
        }
        /**/
        .InsuranceType {
            margin-left: 15px;
        }
    </style>
</asp:Content>
