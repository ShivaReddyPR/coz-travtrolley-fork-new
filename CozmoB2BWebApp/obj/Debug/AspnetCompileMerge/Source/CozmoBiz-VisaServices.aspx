﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
<title>Visa Services  </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="images/favicon.ico">    	


 <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />

<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />

<link href="css/Default.css" rel="stylesheet" type="text/css" />

<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />

 <link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
        
    
          <!-- Bootstrap Core CSS -->
   <link href="css/bootstrap.min.css" rel="stylesheet" />

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />
    
    <link href="yui/build/fonts/fonts-min.css" rel="stylesheet" type="text/css" />
    
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="BookingPanelStyle.css" />
    
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
    
    <link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
    
   <link href="css/toastr.css" rel="stylesheet" type="text/css" />
  


    
<link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />


    
  
</head>
<body>






<div class="topHeader2"> 

<div class="container"> 
<div class="row"> 

<div class="col-md-12"> 
<img src="images/logoc.jpg">   

</div> 

          







</div>
</div>


</div>




<div class="heading_bizz"> 

<div class="container"> 
<h2> Visa Services</h2>

</div>

</div>

          
<form runat="server" id="form1"> 


<div class="container"> 

<div class="innerPage2">

<div class="row"> 

<div class="col-12"> 

<p>

Sudden or planned international trip? Need an expert guide to navigate the complicated process of applying for visas in your city? Look no further than CCTM. We have 15 branches across India with dedicated staff that have a thorough understanding of visa requirements for 100s of countries and established relationships with all major embassies. <br /> <br /> 

Whether you are applying for Schengen or the US visa, travelling to Azerbaijan for a conference, or going on vacation, our custom-built portal helps reduce filing time and increase approval rates, even for express-requests, so you can rest assured that your visa will come through in time. Rely on us to get your documentation right the first time and take the stress out of filing paperwork.  

</p>



</div>



</div>







</div>


</div>




</form>













<div class="footer_sml_bizz"> 
 
 
  Copyright 2019 Cozmo Travel World, India. All Rights Reserved. 


 
 
 </div>





    <script type="text/javascript" src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/Scripts/Menu/query2.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    
    
     <script type="text/javascript" src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/build/js/main.min.js"></script>
     
	 
	 
	 <script src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/scripts/select2.min.js" type="text/javascript"></script>
     
     
     
     
     <script type="text/javascript">
         $(document).ready(function () {

             $('select').select2();


         });
    
</script>




<script>


    //on scroll up/down show/hide top icon

    $('body').prepend('<a href="#" class="back-to-top"><i class="icon-expand_less"></i></a>');

    var amountScrolled = 300;

    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('slow');
        } else {
            $('a.back-to-top').fadeOut('slow');
        }
    });


    //on click scroll to top 

    $('a.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });


</script>




<script>



    $('#Products2,#Partners2').click(function () {
        var Linkid = $(this).attr('href'),
                            Linkid = Linkid.replace('#', '');
        $('html, body').animate({
            scrollTop: $('[name="' + Linkid + '"]').offset().top
        }, 500);


        //        alert("Hello! I am an alert box!");   



    })

       

</script>



 
    <link href="build/css/owl-carousel/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href="build/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
 

</body>
</html>





 


<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>


