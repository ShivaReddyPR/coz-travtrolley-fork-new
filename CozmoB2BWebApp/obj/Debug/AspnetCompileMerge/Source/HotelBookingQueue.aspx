﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="HotelBookingQueue" Title="Hotel Booking Queue" Codebehind="HotelBookingQueue.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="js/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    
     <link href="css/style.css" rel="stylesheet" type="text/css" /> <!--Added by chandan on  13062016 -->

  <%--  <script src="Scripts/Queue.js" type="text/javascript"></script>--%>


    <script type="text/javascript">

        function ViewDocs(id) {
            var wnd = window.open('HotelInvoiceDocuments.aspx?id=' + id, '_blank', 'width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no');
        }
        
        var Ajax;

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function ViewPaymentInfo(id, bookingId) {
            pblockId = id;
            var url = "PaymentInfoAjax.aspx";
            var paramList = 'isPaymentInfo=true';
            paramList += '&bookingId=' + bookingId;
            paramList += '&blockId=' + pblockId;
            document.getElementById('PaymentInfo-'+pblockId).style.display = "block";
            document.getElementById('PaymentInfo-'+ pblockId).innerHTML = "Loading...";

            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                    var numberOfRecord=eval('<%=recordsPerPage%>');
                    for (var i = 0; i < numberOfRecord; i++) {
                    if(document.getElementById('PaymentInfo-' + i)!= null)
                        document.getElementById('PaymentInfo-' + i).style.display = "none";
                        
                    }
                    document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
                        document.getElementById('PaymentInfo-' + pblockId).innerHTML = Ajax.responseText;
                       
                    }
                }
            }
        }
        
        
        
        function ShowPage(pageNo) {
            document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=btnSubmit.ClientID %>').click();
        }
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById('DisplayAgent' + id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        //        function submitForm() {
        //            
        //        }

//        function CancelAmendBooking(cnf) {

//            //document.getElementById('<%=hdnBooking.ClientID %>').value = document.getElementById('booking' + cnf).value + '|' + document.getElementById('txtRemarks').value;
//            hidestuff('DisplayGrid2');
//        }

        function CancelAmendBooking(cnf) {
            var val = document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_ddlBooking').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_errRemarks').innerHTML = "Please Select Request";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
                //document.getElementById('errMess1').style.display = "block";
                document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            else {
                //hidestuff('DisplayGrid2');
               // document.getElementById('errMess1').style.display = "none";
                document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_errRemarks').innerHTML = "";
                return true;
            }
        }
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            init();
              $('container2').context.styleSheets[0].display = "none";
              $('container1').context.styleSheets[0].display = "block";
              cal1.show();
              cal2.hide();
            document.getElementById('container1').style.display = "block";
             
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //    if (difference < 1) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 )";
            //        return false;
            //    }
//            if (difference == 0) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
//                return false;
//            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);



        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }

        function ViewInvoice(hotelId, agentId, confNo) {
            window.open("CreateHotelInvoice.aspx?hotelId=" + hotelId + "&agencyId=" + agentId + "&ConfNo=" + confNo + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            return false;
        }

        function ViewVoucher(confNo) {
            window.open("printHotelVoucher.aspx?ConfNo=" + confNo + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            return false;
        }
    </script>


    <asp:HiddenField ID="hdnBooking" runat="server" />
    <asp:HiddenField ID="PageNoString" runat="server" Value="1" />
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; z-index:9999; top: 120px; left:8%; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute;  z-index:9999; top: 120px; left:40%; display: none;">
        </div>
    </div>
    <div>
 <asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
          <tr>
              <td style="width: 700px" align="left">
                  <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                      onclick="return ShowHide('divParam');">Hide Parameter</a>
              </td>
          </tr>
      </table>
      <div title="Param" id="divParam">
       
       
       
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
     
    <div class="paramcon"> 
       
              <div class="margin-top-5"> 


<div class="col-md-2"> From Date:  </div> 
<div class="col-md-2"> 

<table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="CheckIn" runat="server" CssClass="form-control" Width="120px" ></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showCal1()">
                                                    <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

   </div> 

<div class="col-md-2">  To Date: </div> 
<div class="col-md-2"> 

<table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="CheckOut" CssClass="form-control" runat="server"  Width="120px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showCal2()">
                                                    <img id="Img1" src="images/call-cozmo.png" alt="Pick Date"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

 </div> 

<div class="col-md-2"> Booking Status:</div> 
<div class="col-md-2"> 
   <asp:DropDownList ID="ddlBookingStatus" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList>

 </div> 


<div class="clearfix"> </div>

 </div>
 
    <div class="margin-top-5"> 


<div class="col-md-2"> <asp:Label ID="lblSource" runat="server" Text="Source:"></asp:Label>  </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlSource" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList>   </div> 

<div class="col-md-2">  Agent: </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlAgents" CssClass="form-control" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAgents_SelectedIndexChanged">
                                        <%--<asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>--%>
                                    </asp:DropDownList> </div> 

<div class="col-md-2"> <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label> </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlB2BAgent"  runat="server" CssClass="inputDdlEnabled form-control"
                                        OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList> </div> 
<div class="clearfix"> </div>
 </div>  
 
 
 <div class="margin-top-5"> 



<div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label>  </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlB2B2BAgent"  runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>   </div> 

<div class="col-md-2">  Location: </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlLocations" CssClass="form-control" runat="server" AppendDataBoundItems="true" Enabled="false" >
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList> </div> 

<div class="col-md-2"> Hotel: </div> 
<div class="col-md-2"> <asp:TextBox ID="txtHotelName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox></div> 
<div class="clearfix"> </div>
 </div>
 
 
 <div class="margin-top-5"> 


<div class="col-md-2">  Pax Name: </div> 
<div class="col-md-2"> <asp:TextBox ID="txtPaxName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>   </div> 

<div class="col-md-2"> Confirmation No. </div> 
<div class="col-md-2"> <asp:TextBox ID="txtConfirmNo"  runat="server" CssClass="inputEnabled form-control"></asp:TextBox> </div> 

<div class="col-md-2"> <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label> </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlTransType" CssClass="form-control" runat="server" Visible="false">
                                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                                    <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                                    <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                                </asp:DropDownList> </div> 
<div class="clearfix"> </div>
 </div>
  
 
 <div class="margin-top-5"> 
 
 <div class="col-md-12"><asp:Button CssClass="btn but_b pull-right" ID="btnSubmit" runat="server" Text="Search"
                                            OnClick="btnSubmit_Click" /> </div>
 
     <div class="clearfix"> </div> 
 </div>
      
      <div class="clearfix"> </div> 
       
    </div>    
       
    
        </asp:Panel>
       
       
       
        </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="right">
                    <%=show %>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <div style=" border: solid 0px red">
                        <asp:DataList ID="dlBookings" runat="server" Width="100%" OnItemDataBound="dlBookings_ItemDataBound"
                            OnItemCommand="dlBookings_ItemCommand">
                            <ItemStyle VerticalAlign="Top" />
                            <ItemTemplate>
                                
                                
                                 
                                <div style="display: none" id="DisplayAgent<%#Container.ItemIndex %>" class="div-TravelAgent">
                                    <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href="#"
                                        onclick="hidestuff('DisplayAgent<%#Container.ItemIndex %>');">
                                        <img src="images/close1.png" /></a></span>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="25">
                                                <b>
                                                    <asp:Label ID="lblAgencyName" runat="server" Text=""></asp:Label></b>
                                            </td>
                                            <td>
                                                Credit Balance: <strong>
                                                    <asp:Label ID="lblAgencyBalance" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                Mob : <strong>
                                                    <asp:Label ID="lblAgencyPhone1" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                            <td>
                                                Phone : <strong>
                                                    <asp:Label ID="lblAgencyPhone2" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                Email : <strong>
                                                    <asp:Label ID="lblAgencyEmail" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                            <td>
                                                <a href="#">View Full Profile</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                
                         
                     <div style=" background:#fff; border: solid 1px #ccc; padding: 10px 0px 10px 0px; margin-bottom:10px;"> 
                                
                                
                   <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">  Agent : <a href="#" onclick="showStuff(<%#Container.ItemIndex %>);"><strong>
                                                        <asp:Label ID="lblAgentName" runat="server" Text=""></asp:Label></strong></a></div>
                                <div class="col-md-4"> Status : <b style="color: #009933">
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                </b></div>
                                 <div class="col-md-4">Confirmation No :<b>
                                                    <%#Eval("ConfirmationNo") %></b> </div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                                               
                   <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">
                                     <% if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                             { %>
                                Supplier :<b style="color: #999999">
                                                    <asp:Label ID="lblSupplier" runat="server"></asp:Label></b>
                                                    
                                            <%}
                                             else
                                             { %>        
                                                    
                                                    <%} %>
                                                     </div>
                                                     
                                <div class="col-md-4"> Last Cancellation Date : <b>
                                                    <%# Convert.ToDateTime(Eval("LastCancellationDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                 <div class="col-md-4"> Supplier Ref :<b>
                                                    <%#Eval("BookingRefNo") %>
                                                </b> </div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                               
                                
                  <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">Hotel :<b>
                                                    <%#Eval("HotelName") %></b> </div>
                                <div class="col-md-4"> No. Of Rooms : <b>
                                                    <%#Eval("NoOfRooms") %></b></div>
                                 <div class="col-md-4"> Total Price : <b>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("TotalPrice") %>'></asp:Label></b></div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                    <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4"> Booked On :<b>
                                                    <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                <div class="col-md-4"> No. Of Guests : <b>
                                                    <asp:Label ID="lblGuests" runat="server" Text='<%#Eval("Guests") %>'></asp:Label></b></div>
                                 <div class="col-md-4"> Pax Name : <b>
                                                    <asp:Label ID="lblPaxName" runat="server" Text='<%#Eval("PaxName") %>'></asp:Label></b></div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                     <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">Check In :<b>
                                                    <%#Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                <div class="col-md-4"> Check Out : <b>
                                                    <%#Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b></div>
                                 <div class="col-md-4">  Boooked By : <asp:Label ID="lblBookedBy" runat="server" Text=""></asp:Label></div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                                         
                       <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">  Location : <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label></div>
                                <div class="col-md-4"> 
                                                        <asp:Label ID="lblBooking" BackColor="#9AF1A2" ForeColor="Black" runat="server" Font-Italic="True"
                                                    Font-Names="Arial" Font-Size="10pt"></asp:Label>
                                                        </div>
                                 <div class="col-md-4">
                                 
                                 
                                 <label class=" pull-left"><asp:Button CssClass="button margin-right-5" ID="btnInvoice" CommandName="Invoice" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                        runat="server" Text="View Invoice"  Visible="true" /> </label
                                 
                                  <label class=" pull-left"><asp:Button ID="btnOpen" CssClass="button" CommandName="Voucher" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                                runat="server" Text="View Voucher"  Visible="true" /> </label>
                                 
                                 
                                                        
                                                        
                                 <div class="clearfix"></div>
                                 
                                  </div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                         <%if (agent != null && agent.RequiredItinerary)
                             { %>
                         <div class="col-md-12">
                             <a href="#" onclick='ViewDocs(<%#Eval("HotelId") %>)'>View Documents</a>
                             
                         </div>
                         
                              <%} %>  
                        <div class="col-md-12 margin-top-10"> 
                                
                                         
    
                                    
                                    
                                 
                                        <table id="tblBooking" width="100%" runat="server">
                                                    <tr>
                                                       <td>
                                                       
                                                       
                                                       
                                                   <div> 
                                
                                <div class="col-md-3 marbot_10 right-align">Request Change : </div>
                                <div class="col-md-3 marbot_10"><asp:DropDownList CssClass="form-control" ID="ddlBooking" runat="server">
                                                            <asp:ListItem Selected ="True" Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Cancel Booking" Value="Cancel Booking"></asp:ListItem>
                                                                <asp:ListItem Text="Amend Booking" Value="Amend Booking"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            </br>
                                                            <b id="errRemarks" style="color:Red" runat="server"></b>
                                                             </div>
                                 <div class="col-md-3 marbot_10"> 
                                 
                                 <asp:TextBox CssClass="form-control" ID="txtRemarks" TextMode="MultiLine" Rows="1" Text="Enter Remarks here"
                                                                runat="server" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"></asp:TextBox>
                                      
                                      
                                      
                                      </div>
                                  
                                  
                                  
                                    <div class="col-md-3 marbot_10">  <asp:Button CssClass="button" ID="btnBooking" runat="server" CommandName="Booking" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                                Text="Submit" Visible="false" /> </div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>           
                                                       
                                                       
                                                       
                                                       
                                                       
                                                       
                                                       </td>
                                                    </tr>
                                                     
                                                </table>
                                                <table>
                                 <tr>
                                    <td align="left" colspan="2" style="padding-left: 20px">
                                        <div style="position: relative">
                                            <asp:LinkButton ID="lnkPayment" runat="server" Visible="false" Text="Payment Information"
                                                href="#"></asp:LinkButton>
                                            <div id="PaymentInfo-<%#Container.ItemIndex %>" class="visa_PaymentInfo_pop" style="position: absolute;
                                                display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </table>
                                 <div class="clearfix"> </div>
                                
                                </div>
                         
                         <div class="clearfix"></div>
                         
                                
                             </div>       
                                
                                
                                
                               
                                  
                                
                                
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </td>
            </tr>
            <tr>
                 <td align="right">
                    <%=show %>
                </td>
            </tr>
            
        </table>
    </div>
    
     
    
    <input type="hidden" name="filter" value="true" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
