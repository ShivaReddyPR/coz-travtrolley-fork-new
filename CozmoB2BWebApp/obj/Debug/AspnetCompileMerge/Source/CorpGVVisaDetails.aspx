﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorpGVVisaDetailsGUI" Codebehind="CorpGVVisaDetails.aspx.cs" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="CT.GlobalVisa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    )<style> 

.tab-content { max-height:300px; overflow-y:scroll;  }

</style>
    <%if (string.IsNullOrEmpty(errorMessage))
        { %>
<div class="body_container"> 
<%if (passengerList != null && passengerList.Count > 0)
    { %>
<div class="badge_custom"> 
<span class="badge badge-default">  <%=passengerList[0].Fpax_name %></span>

<span class="badge badge-info"><%=passIsseCountry %> </span>

<span class="badge badge-primary"> <%=passengerList[0].Fpax_passport_no %> </span>
<br /> <br /> 

</div>
    <%} %>



<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#VisaRequirements">Visa Requirements </a></li>
    <li><a data-toggle="tab" href="#TermsandConditions">Terms and Conditions</a></li>
  </ul>
 
  <div class="tab-content tabouter">



    <div id="VisaRequirements" class="tab-pane fade in active">
        <%if (gvRequire != null)
            {
                %>
      
          <%if (!string.IsNullOrEmpty(gvRequire.Header))
              { %>
         <h3><%=gvRequire.Header %></h3>
        <%} %>
      

      <p> 

    <ul class="list_custom">

           <%if (!string.IsNullOrEmpty(gvRequire.Description))
               { %>
        <%=gvRequire.Description %>
        <%}%>

        </ul>
</p>
        <%} %>

    </div>
        <div id="TermsandConditions" class="tab-pane fade">
            <%if (gvTerms != null)
                {
                    %>
             <%if (!string.IsNullOrEmpty(gvTerms.Header))
                 { %>
      <h3><%=gvTerms.Header%></h3>
            <%} %>
      <p>
          <%if (!string.IsNullOrEmpty(gvTerms.Description))
              { %><%=gvTerms.Description %>
          <%} %>
      </p>
            <%} %>
    </div>


  </div>
<div class="row"> 
<div class="col-md-12">
<br /> 

<%--<div class="badge_custom font_med">   <span > <img height="25px" src="doc/china-flag.jpg" /> China Visa</span> for holder living in 
<span >  United Arab Emirates</span> </div>--%>

<div class="well bg_white martop_14 select_visa_for"> 

<div class="row">



<div class="col-md-4"> 
      Applying for:  
      
      <b>
     <%if (visaSession != null)
         { %>
    <%=visaSession.TravelToCountryName %>
    <%} %></b>
    </div>

<div class="col-md-4"> 
for citizens of: <b><%=passIsseCountry %></b>
    </div>


<div class="col-md-4"> 
living in: <b><%=residenceCountry %></b>
    </div>



 </div>

</div>

<style> .tooltip { position:absolute; z-index:9999 } </style>


<h4><%if (visaSession != null)
        { %>
     <%=visaSession.TravelToCountryName %> <%=visaSession.VisaTypeName %> Fees 
    <%} %>
</h4>

<div class="table-responsive">


<table class="table table-striped custom-checkbox-table">




    <tbody>
        <tr>
            <td align="left"><strong>Visa Category</strong></td>
            <td align="left"><strong>Processing  </strong></td>
            <td align="left"><strong>Visa fee </strong></td>
            <td align="left"><strong>Appointment Fee </strong></td>
            <td align="left"><strong>Total Cost </strong></td>
        </tr>
        <asp:HiddenField ID="hdnSelectedFeeId" runat="server" value="0" />
        <asp:HiddenField ID="hdnSelectedHandlingFeeId" runat="server" value="0" />
        <%if (gvFeeList != null && gvFeeList.Count > 0)
            {%>
             <input type="hidden" id="hdnCount" value="<%=gvFeeList.Count %>" />
                <%for (int i = 0; i < gvFeeList.Count; i++)
                    {
                        if (gvFeeList[i] != null)
                        {%>
        <tr>

            <td align="left">

                <input type="radio" id="rbFee<%=i%>" name="Fee" />
                <input type="hidden" id="hdnFee<%=i%>" value="<%=gvFeeList[i].TransactionId%>" />
                <%=gvFeeList[i].VisaCategory %>
                 

            </td>
            <td align="left"><%=gvFeeList[i].ProcessingDays%> business days   </td>

            <%  int adultCount = 0;
                int childCount = 0;
                int infantCount = 0;
                decimal TotalAdultsCharge = 0;
                decimal TotalChildCharge = 0;
                decimal TotalInfantCharge = 0;

                //decimal adSvcTax = 0;
                //decimal chSvcTax = 0;
                //decimal inSvcTax = 0;
                //decimal totalSvcTax = 0;
                decimal adadd1charge = 0;
                decimal chadd1charge = 0;
                decimal inadd1charge = 0;
                decimal adHandlingFee = 0;
                decimal chHandlingFee = 0;
                decimal inHandlingFee = 0;
                decimal totalCharge = 0;
                decimal totaladd1charge = 0;
                decimal totalhandlingfee = 0;
                if (visaSession != null)
                {
                    adultCount = visaSession.Adult;
                    childCount = visaSession.Child;
                    infantCount = visaSession.Infant;
                }

                TotalAdultsCharge = Utility.ToDecimal(gvFeeList[i].TotalChargeAdult) * adultCount;
                TotalChildCharge = Utility.ToDecimal(gvFeeList[i].TotalChargeChild) * childCount;
                TotalInfantCharge = Utility.ToDecimal(gvFeeList[i].TotalChargeInfant) * infantCount;
                //adSvcTax = Utility.ToDecimal(gvFeeList[i].AdultService) * adultCount;
                //chSvcTax = Utility.ToDecimal(gvFeeList[i].ChildService) * childCount;
                //inSvcTax = Utility.ToDecimal(gvFeeList[i].InfantService) * infantCount;
                //totalSvcTax = (adSvcTax + chSvcTax + inSvcTax);
                adadd1charge = Utility.ToDecimal(gvFeeList[i].AdultAdd1Fee) * adultCount;
                chadd1charge = Utility.ToDecimal(gvFeeList[i].ChildAdd1Fee) * childCount;
                inadd1charge = Utility.ToDecimal(gvFeeList[i].InfantAdd1Fee) * infantCount;
                for (int j = i; j < gvHandlingFee.Count; j++)
                {
                  %>  <input type="hidden" id="hdnHandlingFee<%=i%>" value="<%=gvHandlingFee[j].TransactionId%>" /><%
                    decimal HandlingFee = gvHandlingFee[j].HandlingFee;
                    adHandlingFee = Utility.ToDecimal(gvHandlingFee[j].HandlingFee) * adultCount;
                    chHandlingFee = Utility.ToDecimal(gvHandlingFee[j].HandlingFee) * childCount;
                    inHandlingFee = Utility.ToDecimal(gvHandlingFee[j].HandlingFee) * infantCount;
                    break;
                }
                totaladd1charge = (adadd1charge + chadd1charge + inadd1charge);
                totalhandlingfee = (adHandlingFee+chHandlingFee+chHandlingFee);
                totalCharge = (TotalAdultsCharge + TotalChildCharge + TotalInfantCharge +totalhandlingfee);
            %>
            <td align="left"><%=Formatter.ToCurrency(totalCharge - totaladd1charge-totalhandlingfee) %> <span class="text-danger"></span></td>
            <td align="left"><%=Formatter.ToCurrency(totaladd1charge +totalhandlingfee) %> <span class="text-danger"></span></td>

            <td align="left"><%=Formatter.ToCurrency(totalCharge) %><span class="text-danger"></span></td>

        </tr>
        <%}
                }
            }%>
    </tbody>
  </table>
</div>

 </div>

  <%if (gvFeeList != null && gvFeeList.Count > 0)
      {%>
 <div class="col-md-12">
     <div class="martop_14"><asp:Label CssClass="pull-right" runat="server" ID="lblOnAccount" Text="" ></asp:Label>
     <div class="clearfix"> </div>
     </div>

<div ><asp:Button ID="btnApply"  runat="server" class="btn but_b pull-right" Text="Apply"  OnClick="btnApply_Click" OnClientClick="return Validate();"/> <div class="clearfix"> </div> </div>
 <div class="clearfix"> </div>
 </div>
     <%} %>

 <div class="clearfix"> </div>
 


</div>


</div>
    <%}
        else
        {%>
    <div class="body_container">
       <div><%=errorMessage %></div>
    </div>
        <%} %>



 <script>
     function Validate() {
         var isValied = false;
         if (document.getElementById('hdnCount') != null) {
             var totCount = eval(document.getElementById('hdnCount').value);
             for (i = 0; i < totCount; i++) {
                 if (document.getElementById('rbFee' + i).checked) {
                     document.getElementById('<%=hdnSelectedFeeId.ClientID%>').value = document.getElementById('hdnFee' + i).value;
                     document.getElementById('<%=hdnSelectedHandlingFeeId.ClientID%>').value = document.getElementById('hdnHandlingFee' + i).value;
                     isValied = true;
                     break;
                 }
             }
         }
         if (!isValied) {
             alert("Please select At least One Visa Category");
         }
         return isValied;
     }
     $(document).ready(function () {
         $('[data-toggle="tooltip"]').tooltip();
     });
</script>

<%--AWESOME FONTS--%>
<link rel="stylesheet" href="css/fontawesome/css/font-awesome.min.css">
     <div id='EmailDivApprover' runat='server' style='width: 100%; display: none;'>
    <%if(detail != null) %>
    <%{ %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            </head>
            <body>
            
            <table>
            <tr>
            <td>
            Dear
       <%if (!string.IsNullOrEmpty(approverNames) && approverNames.Length > 0) %>
                                              <%{ %>
                                              
                          <%=approverNames %>
                                              <%} %>
            </td>
            </tr>
            </table>
        <p>Find below Visa details for Employee :
                                              
                                              </p>
                                              
             <table>
             <tr>
             <td>
             Doc No : <%=detail.DocNumber %>
             </td>
             </tr>
             <tr>
             <td>
             Date : <%=detail.DocDate %>
             </td>
             </tr>
             
             <tr>
             <td>
             Passport Type : <%=detail.PassportType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Type Of Visa : <%=detail.SelVisaType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Country : <%=detail.SelCountry %>
             </td>
             </tr>
             
             <tr>
             <td>
             Nationality : <%=detail.SelNationality %>
             </td>
             </tr>
             
             <tr>
             <td>
             Residence : <%=detail.SelResidence %>
             </td>
             </tr>
              <tr>
             <td>
             Visa Category : <%=detail.VisaCategory %>
             </td>
             </tr>
             
             </table>
             Thanks & Regards,
             Global Visa Sales Team.
            </body>
            </html>
    <%} %>
    
    </div>
    
    
      <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
    <%if(detail != null) %>
    <%{ %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
        <title></title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            
            </head>
            <body>
            
            <table>
            <tr>
            <td>
            Dear Employee,
            </td>
            </tr>
            
            <tr>
            <td>
            <p>
  Below are the visa details which are the sent by  <% = approverNames  %></p>
            </td>
            </tr>   
            </table>                          
             <table>
             <tr>
             <td>
             Doc No : <%=detail.DocNumber %>
             </td>
             </tr>
             <tr>
             <td>
             Date : <%=detail.DocDate %>
             </td>
             </tr>
             
             <tr>
             <td>
             Passport Type : <%=detail.PassportType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Type Of Visa : <%=detail.SelVisaType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Country : <%=detail.SelCountry %>
             </td>
             </tr>
             
             <tr>
             <td>
             Nationality : <%=detail.SelNationality %>
             </td>
             </tr>
             
             <tr>
             <td>
             Residence : <%=detail.SelResidence %>
             </td>
             </tr>
              <tr>
             <td>
             Visa Category : <%=detail.VisaCategory %>
             </td>
             </tr>
             </table>
             Thanks & Regards,
             Global Visa Sales Team.
            </body>
            </html>
    <%} %>
    
    </div>


        <style> 
    
    .tabouter { border-left: solid 1px #ddd;  border-right: solid 1px #ddd;  border-bottom: solid 1px #ddd; padding: 0px 0px 10px 20px; background:#fff;}
    
    </style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

