﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="GVHandlingFeeMasterUI" Title="Global Visa HandlingFee Master" Codebehind="GVHandlingFeeMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
        <style>
            .body_container{
                height:700px;
            }
        </style>

    
<div class="body_container"> 

    <div class="row"> 
        <div class="col-md-12"> 
            <center>
        <table cellpadding="0"  cellspacing="2">
            <tr>
                <td style="height: 10px">
                </td>
            </tr>
            <tr>
                <td>
                    <table class="trpad" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <%--<td align="right" height="23">
                                <asp:Label ID="lblAgentId" runat="server" Text="Agent"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAgent" CssClass="inputDdlEnabled form-control" runat="server" Width="154px" >
                                </asp:DropDownList>
                            </td>--%>
                            <td align="right" height="23">
                                <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label><span style="color:red">*:</span>
                            </td>
                            
                            <td>
                                <asp:DropDownList ID="ddlCountry" CssClass="inputDdlEnabled form-control" runat="server" Width="154px" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"  AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlNationality" CssClass="inputDdlEnabled form-control" runat="server" Width="154px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr >
                           <td align="right" height="23">
                                <asp:Label ID="lblResidence" runat="server" Text="Residence"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlResidence" CssClass="inputDdlEnabled form-control" runat="server" Width="154px">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblVisaCategory" runat="server" Text="Visa Category"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVisaCategory" CssClass="inputDdlEnabled form-control" runat="server"
                                    Width="154px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 20px">
                </td>
            </tr>
            <tr> 
            <td> 
            <table> 
            <tr> 
            <td valign="top">
                        <div class="pad20">
            <table id="tblVisaTypes" runat="server"></table>
        </div>
             </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <div style=" text-align:right" class="pad20">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b" OnClientClick="return Save();" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnCancel" runat="server" Text="Clear" CssClass="btn but_b" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b" OnClick="btnSearch_Click"/>
                            <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="btn but_b" Visible="false"  />
                            <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
                            <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
                        </div>
                    </center>
                </td>
            </tr>
        </table>
                </center>
    </div>
    </div></div>

    <script type="text/javascript">
        function Save() {



            //if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please Select Agent from the List!', '');
            if (getElement('ddlCountry').selectedIndex <= 0) addMessage('Please Select Country from the List!', '');
            if (getElement('ddlNationality').selectedIndex <= 0) addMessage('Please Select Nationality from the List!', '');
            if (getElement('ddlResidence').selectedIndex <= 0) addMessage('Please Select Residence from the List!', '');
            if (getElement('ddlVisaCategory').selectedIndex <= 0) addMessage('Please Select Visa Category from the List!', '');
            
          


            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
                dtvisatypes
            }

        }

        function restrictNumeric(fieldId, kind) {
            try {
                return (maskNumeric(fieldId, (kind == '3' || kind == '4' ? 'false' : 'true'), (kind == '1' || kind == '3' ? 'true' : 'false')))
            }
            catch (err) {
                showError(err, 'Validation', 'restrictNumeric'); return (false)
            }
        }
        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0.00') {
                document.getElementById(id).value = '';
            }
        }

        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0.00' || val == 'NaN') {
                document.getElementById(id).value = '0.00';
            }
        }
        <%--function searchValidation()
        {
            debugger;
            if (document.getElementById('<%=ddlAgent.ClientID %>').value == "-1") {
                document.getElementById('<%=lblErrorMsg.ClientID %>').style.display = "block";
                document.getElementById('<%=lblErrorMsg.ClientID %>').innerHTML = "Please Select the Agent";
                alert("Please Select the Agent");
                return false;
            }
            else {
                document.getElementById('<%=lblErrorMsg.ClientID %>').style.display = "none";
                document.getElementById('<%=lblErrorMsg.ClientID %>').innerHTML = "";
                return true;
            }
        }--%>
    </script>
</asp:Content>
    <asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="hand_id"
        EmptyDataText="No Requirements List!" AutoGenerateColumns="false" PageSize="10"
        GridLines="none" CssClass="" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
        CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="" ShowSelectButton="True" />
            <%--<asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtAgent" Width="150px" CssClass="inputEnabled" HeaderText="Agent"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>--%>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <%--<ItemTemplate>
                    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("hand_agent_Name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("hand_agent_Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtCountry" Width="150px" CssClass="inputEnabled" HeaderText="Country"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("hand_country_Name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("hand_country_Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtNationality" Width="150px" CssClass="inputEnabled" HeaderText="Nationality"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("hand_nationality_Name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("hand_nationality_Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTxtVisaType" Width="100px" CssClass="inputEnabled" HeaderText="Visa Type"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblVisaType" runat="server" Text='<%# Eval("hand_visa_type_Name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("hand_visa_type_Name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtResidence" Width="100px" CssClass="inputEnabled" HeaderText="Residence"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITresiName" runat="server" Text='<%# Eval("hand_residence_Name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("hand_residence_Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtVisaCategory" Width="100px" HeaderText="Visa Category" CssClass="inputEnabled"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblVisaCategory" runat="server" Text='<%# Eval("charge_visa_category_Name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("charge_visa_category_Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


