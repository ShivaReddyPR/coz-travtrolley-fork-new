﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ExportExcelHotelAcctListGUI" Codebehind="ExportExcelHotelAcctList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Account Report</title>
</head>
<body>
    <form id="form1" runat="server">
       <div>
    <asp:DataGrid ID="dgHotelAcctReportList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="Agent Code" DataField="agent_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Agent Name" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Booking Date" DataField="createdOn" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Hotel Ref" DataField="bookingID" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Supplier" DataField="hotelSource" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Sup Comfirm" DataField="confirmationNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Passenger Name" DataField="PassengerName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Adult Count" DataField="AdultCount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Child Count" DataField="ChildCount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Check In" DataField="checkInDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Check Out" DataField="checkOutDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Hotel" DataField="hotelName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Room Type" DataField="RoomType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="City" DataField="cityRef" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Payable Amount" DataField="Payableamount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice Amount" DataField="TotalAmount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="profit" DataField="profit" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Discount" DataField="Discount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="AgentSF" DataField="AgentSF" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice No" DataField="InvoiceNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Status" DataField="status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="AcctStatus" DataField="AccountedStatus" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
    </asp:DataGrid>
    </div>
    </form>
</body>
</html>
