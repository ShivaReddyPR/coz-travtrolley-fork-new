﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="B2BGVIframe.aspx.cs" Inherits="CozmoB2BWebApp.B2BGVIframe" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
   <div style="overflow-x:auto;overflow-y:auto;"> 
       <iframe style="width: 1490px;" height="650" id="GViframeVisa" src="<%=System.Configuration.ConfigurationManager.AppSettings["GVIframeUrl"].ToString() %>?b2bagentId=<%=b2bagentId %>&userId=<%=userId %>&userLoginName=<%=userName%>&product=<%=product%>&password=<%=password%>">
        </iframe>
       </div>
</asp:Content>
 
