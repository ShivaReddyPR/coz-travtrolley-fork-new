﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDepartureResultsGUI" Title="Fixed Departure Results" Codebehind="FixedDepartureResults.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     <script type="text/javascript" src="js/VisaDefault.js" ></script>
      <script type="text/javascript" src="js/VisaPassengerDetail.js" ></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<script type="text/javascript" language="javascript">
    var cal1;
    var cal2;
    function init() {

        //showReturn();
        var dt = new Date();
        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("title", "Select your desired checkin date:");
        cal1.cfg.setProperty("close", true);
        cal1.cfg.setProperty("iframe", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();
        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
        cal2.cfg.setProperty("title", "Select your desired checkout date:");
        cal2.selectEvent.subscribe(setDate2);
        cal2.cfg.setProperty("close", true);
        cal2.render();
        //load();
    }

    function showCalendar1() {
        cal2.hide();
        document.getElementById('container1').style.display = "block";

        //          var bodyRect = document.body.getBoundingClientRect();
        //          var elemRect = document.getElementById('container1').getBoundingClientRect();
        //          var offset = elemRect.top - bodyRect.top;

        //          document.getElementById('Outcontainer1').style.top = eval(offset-300 );

        document.getElementById('Outcontainer1').style.display = "block";
        document.getElementById('Outcontainer2').style.display = "none";
    }

    var departureDate = new Date();
    function showCalendar2() {
        cal1.hide();
        var date1 = document.getElementById("<%=checkInDate.ClientID%>").value;

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');
            //              var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);
            var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());
            cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal2.render();
        }
        document.getElementById('container2').style.display = "block";


        document.getElementById('Outcontainer2').style.display = "block";
        document.getElementById('Outcontainer1').style.display = "none";
    }
    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMess').style.visibility = "visible";
            document.getElementById('errMess').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById("<%=checkInDate.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
        cal1.hide();
        document.getElementById('Outcontainer1').style.display = "none";
    }


    function setDate2() {
        var date1 = document.getElementById("<%=checkInDate.ClientID%>").value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.visibility = "visible";
            document.getElementById('errMess').innerHTML = "First select checkin date.";
            return false;
        }

        var date2 = cal2.getSelectedDates()[0];
        var depDateArray = date1.split('/');

        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.visibility = "visible";
            document.getElementById('errMess').innerHTML = " Invalid checkin date";
            return false;
        }

        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();

        if (difference < 0) {
            document.getElementById('errMess').style.visibility = "visible";
            document.getElementById('errMess').innerHTML = "Checkout date should be greater than or equal to checkin date (" + date1 + ")";
            return false;
        }

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }

        document.getElementById("<%=checkOutDate.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
        cal2.hide();
        document.getElementById('Outcontainer2').style.display = "none";

    }
    YAHOO.util.Event.addListener(window, "load", init);


    function checkTourDates() {
        document.getElementById('errMess').style.display = "none";
        var date1 = document.getElementById("<%=checkInDate.ClientID%>").value;
        var date2 = document.getElementById("<%=checkOutDate.ClientID%>").value;
        var depDateArray = date1.split('/');
        var retDateArray = date2.split('/');
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
        if (date1 == "DD/MM/YYYY" || date1 == "") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Select Valid Departure Date";
            return false;
        }


        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = " Invalid departure date";
            return false;
        }

        if (date2 == "DD/MM/YYYY" || date2 == "") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Select Valid Return Date";
            return false;
        }


        // checking if date2 is valid	
        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = " Invalid return date";
            return false;
        }
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
        var difference = returndate.getTime() - depdate.getTime();

        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Return date should be greater than or equal to departure date";
            return false;
        }

        return true;
    }


    function TourSearch() {
        if (validate()) {
            document.getElementById('<%=hdfTourValue.ClientID %>').value = 1;
            document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
            document.forms[0].submit();
        }
        else
            return false;
    }

    function validate() {
        if (!checkTourDates()) {
            return false;
        }
        return true;
    }
    </script>
    <script type="text/javascript">

        function SortRegionJS(id) {
            document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
            document.getElementById('<%=hdfRegion.ClientID %>').value = id;
            document.forms[0].submit();
        }
        function SortThemeJS(id) {
            document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
            document.getElementById('<%=hdfTheme.ClientID %>').value = id;
            document.getElementById('lnk' + id).style.background = "#EAF7FD";
            document.forms[0].submit();
        }
        function SortCountryJS(id) {
            document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
            document.getElementById('<%=hdfCountry.ClientID %>').value = id;
            document.forms[0].submit();
        }

        function clearFilters() {
            document.getElementById('<%=hdfRegion.ClientID %>').value = "";
            document.getElementById('<%=hdfTheme.ClientID %>').value = "";
            document.getElementById('<%=hdfCountry.ClientID %>').value = "";
            document.getElementById('<%=hdfClearFilters.ClientID %>').value = "yes";
            document.forms[0].submit();
        }
             
    </script>

   
    <div>
        <%--<form id="form2" action="ActivityDetails.aspx" method="post">
        <input type="hidden" id="resultIndex" name="resultIndex" value="<%=id %>" />
        </form>--%>
    </div>
    <div style="height:700px">
        <%--<form id="form1" action="ActivityResults.aspx" method="post" runat="server">--%>
         <asp:HiddenField ID="hdfID" runat="server" />
         <asp:HiddenField ID="hdfRegion" runat="server" />
        
        <asp:HiddenField ID="hdfTheme" runat="server" />
        
        <asp:HiddenField ID="hdfCountry" runat="server" />
        <asp:HiddenField ID="hdfClearFilters" runat="server" Value="no" />
        <asp:HiddenField ID="hdfTourValue" runat="server"  Value="0"/>
<div class="error_msg" style="display:none;" id="errMess"> </div>
<div class="hotel_container">
                    <div id="narrow-search">
                        <div class="ns-h3">
                            Narrow Result</div>
                        <div class="borwrap">
                        <div style="text-align:center">
                            <a href="#" onclick="clearFilters()">Clear Filter</a>
                            </div>
                            <div class="block_997">
                                
                                <div class="roundheading">Select Region /Country</div>
                                
                                
                                <div class="Box">
                                    <asp:TreeView ID="TreeView1" runat="server" ShowLines="True">
                                    </asp:TreeView>
                                </div>
                                <div class="shd_round">
                                    <div class="shd_round_1">
                                    </div>
                                    <div class="shd_round_2">
                                    </div>
                                    <div class="shd_round_3">
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="block_997">
                                
                                 <div class="roundheading">Categories </div>
                                
                                
                                <div class="excursion-wrap">
                                    <ul>
                                        <%for (int i = 0; i < ThemeList.Rows.Count; i++)
                                          { %>
                                        <li id='lnk<% =ThemeList.Rows[i]["ThemeId"].ToString()%>'><a  href="#" onclick="javascript:SortThemeJS('<% =ThemeList.Rows[i]["ThemeId"].ToString()%>')">
                                            <% =ThemeList.Rows[i]["ThemeName"].ToString()%></a></li>
                                        <%--<asp:LinkButton ID="<% =CityList[i]%>" runat="server" Text="- <% =CityList[i]%>" OnClick="<% =CityList[i]%>_Click"></asp:LinkButton></li>--%>
                                        <%} %>
                                    </ul>
                                </div>
    <div class="shd_round">
        <div class="shd_round_1">
        </div>
        <div class="shd_round_2">
        </div>
        <div class="shd_round_3">
        </div>
        <div class="clear">
        </div>
    </div>
    </div>
    <div class="block_997"  style="display:none;">
      <div class="roundheading">Tour Date</div>
      
        <div class="excursion-wrap">
<table>
<tr>
<td><asp:Label ID="lblFromDate" runat="server" Text="From Date:"></asp:Label></td>
        <td width="14%">
                                        <asp:TextBox ID="checkInDate" runat="server" Text="DD/MM/YYYY" CssClass="inp_00"/>
                                    </td>
                   <td width="2%" valign="bottom">
                                        <a href="javascript:void(null)" onclick="showCalendar1()">
                                            <img src="images/call-cozmo.png" alt="" />
                                        </a>
                                    </td>
                                 </tr>

                                 <tr>
                                 <td><asp:Label ID="lblToDate" runat="server" Text="To Date:"></asp:Label></td>

                                                    <td>
                                                    <asp:TextBox ID="checkOutDate" runat="server" Text="DD/MM/YYYY" CssClass="inp_00"/>
                                        <%--<input class="inp_00" type="text" value="DD/MM/YYYY" name="checkOutDate" id="checkOutDate" runat"server" />--%>
                                    </td>
      <td width="2%" valign="bottom">
                                        <a href="javascript:void(null)" onclick="showCalendar2()">
                                            <img src="images/call-cozmo.png" alt="" />
                                        </a>
                                    </td>
                                 </tr>
                                                                       <div id="Outcontainer1" style="position: absolute; display: none;left: 0px;
                z-index: 300; ">
                <a onclick="document.getElementById('Outcontainer1').style.display = 'none'">
                    <img style="cursor: pointer; position: absolute; z-index: 300; top: -12px; right: -10px;"
                        src="images/cross_icon.png" />
                </a>
             <div id="container1" style="border: 1px solid #ccc;">
                </div>
            </div>
                                      <div id="Outcontainer2" style="position: absolute; display: none; left: 120px;
                z-index: 300;">
                <a onclick="document.getElementById('Outcontainer2').style.display = 'none'">
                    <img style="cursor: pointer; position: absolute; z-index: 300; top: -12px; right: -10px;"
                        src="images/cross_icon.png" />
                </a>
                <div id="container2" style="border: 1px solid #ccc;">
                </div>
            </div>
                             <td height="14" colspan="2">
                             <%-- <center>
                                 <asp:HyperLink ID="HyperLink1"  runat="server"><a  Class="book-now-button" href="FixedDepartureDetails.aspx?id=<%# Eval("activityId") %>">
                                 
                                 
                                 Book Now</a></asp:HyperLink>
                                   </center>--%>
                            <a class="book-now-button" href="#" onclick="return TourSearch();"><span>Search</span></a>
                        </td>
</table>
</div>
   <%--     <div class="activity-duration">
            <ul>
                <%for (int i = 0; i < Duration.Count; i++)
                  { %>
                <li><a href="#" onclick="javascript:SortDurationJS('<% =Duration[i]%>')">-
                    <% =Duration[i]%>
                    </a></li>
                <%} %>
            </ul>
        </div>--%>
        <div class="shd_round">
            <div class="shd_round_1">
            </div>
            <div class="shd_round_2">
            </div>
            <div class="shd_round_3">
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    </div>
    
    </div>
    <div id="results-search">
        <div class="listing_search">
            <div class="pagination_coz">
                <div class="pagination_l">
                </div>
                <div class="pagination_m">
                    <div class=" page_l">
                        Sort by: <span class="label24">
                            <asp:DropDownList ID="ddlSort" CssClass="inp_auto" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged1">
                                <asp:ListItem Value="price" Selected="True" Text="Lowest Price"></asp:ListItem>
                                <asp:ListItem Value="alphabetical" Text="Alphabetical order"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblActivityName" Text='<%# Eval("activityName") %>' runat="server"></asp:Label>
                            
                        </span>
                    </div>
                    <div class=" page_r">
                    Page:  
                        
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click"  CssClass="PagerStyle" >First</asp:LinkButton>
                       
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" CssClass="PagerStyle"  />
                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" CssClass="PagerStyle" />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" CssClass="PagerStyle">Last</asp:LinkButton>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="pagination_l2">
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div>
        
        <div>
            <asp:DataList ID="DLActivities" runat="server" CellPadding="4" class="hotel_mblock"
                DataKeyField="activityId" Width="750px" 
                onitemdatabound="DLActivities_ItemDataBound">
                <ItemTemplate>
                    <div class="hotel_mblock">
                        <div class="hotels_grid ">
                            <div class="hotels_gr1">
                                <span class="hotel_view">
                                    <img src='<%=activityImgFolder %><%# Eval("imagePath1") %>' style="width:145px;height:120px" alt=''/>
                                    <%--<img src='<%=Request.Url.Scheme%>://10.10.1.119/cozmoweb/activityimages/1_1.gif' style="width:145px;height:120px" alt=''/>--%>
                                    <%--<img src='ActivityImages/2_1.gif' alt=''/>--%>
                                    
                                    
                                    
                                </span><h3>
                                    <%# Eval("activityName") %> </h3>
                                    <%--<h3><%=activityImgFolder %><%# Eval("imagePath1") %></h3>--%>
                                    
                                <p>
                                
                                 <%--<asp:HiddenField ID="hdfId" runat="server" Value="" />--%>
                                     <strong>Region :</strong>
                                     <asp:Label ID="lblRegion" runat="server" ></asp:Label>
                                    <br />
                                    <strong>Country :</strong>
                                    <asp:Label ID="lblCountry" runat="server" Font-Bold="true" ></asp:Label>
                                    <br>
                                    </p>
                                <p>
                                <%--<%# Eval("introduction") %>'>--%>
                                    <%--<asp:Label ID="Label1" runat="server" CssClass="grdof" Width="100ppx" Text='<%# Eval("introduction") %>'></asp:Label>--%>
                                    <asp:Label ID="Label2" runat="server" Text='<%# restrictLength(Eval("introduction")) %>'></asp:Label>
                                    
                                </p>
                                <hr />
                                <p>Category: 
                                    <asp:Label ID="lblCategory" runat="server" Font-Bold="true" ></asp:Label></p>
                            </div>
                            <div class="hotels_grr">
                                <p class="lowest_price">
                                    Starting From</p>
                                <p class="best_price">
                                    <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency%>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#CTCurrencyFormat(Eval("Amount")) %>'></asp:Label></p>
                                <span class="spccc">per person</span>
                                <div style=" padding-top:16px">
                                 <center>
                                 <asp:HyperLink ID="HyperLink1"  runat="server"><a  Class="book-now-button" href="FixedDepartureDetails.aspx?id=<%# Eval("activityId") %>">
                                 
                                 
                                Details</a></asp:HyperLink>
                                   </center>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    </div>
 <div class="clear"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

