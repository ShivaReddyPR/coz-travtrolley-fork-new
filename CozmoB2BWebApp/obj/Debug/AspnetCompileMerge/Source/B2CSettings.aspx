﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="B2CSettings" Title="B2C Settings" Codebehind="B2CSettings.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
    <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
  
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<link rel="stylesheet" href="css/ajax_tab_css.css">
 <script type="text/javascript">

     function SelectAllMenus() {
         var chkList = document.getElementById("<%=chkListB2CMenu.ClientID %>");

         if (document.getElementById("ctl00_cphTransaction_chkListB2CMenu_0").checked) {
             for (var i = 1; i < chkList.rows.length; i++) {
                 document.getElementById("ctl00_cphTransaction_chkListB2CMenu_" + i).checked = true;
             }
             document.getElementById("ctl00_cphTransaction_chkListB2CMenu_0").innerHTML = "UnSelect All";
         }
         else {
             for (var i = 1; i < chkList.rows.length; i++) {
                 document.getElementById("ctl00_cphTransaction_chkListB2CMenu_" + i).checked = false;
             }
             document.getElementById("ctl00_cphTransaction_chkListB2CMenu_0").innerHTML = "Select All";
         }
     }

     function ShowHideSelection() {
         var chkList = document.getElementById("<%=chkListB2CMenu.ClientID %>");
         for (var i = 1; i < chkList.rows.length; i++) {
             document.getElementById("ctl00_cphTransaction_chkListB2CMenu_0").checked = false;
         }
     }
 
     function setLoginSuffix(id) {
         var ddlAgent = document.getElementById(id);
         if (ddlAgent.selectedIndex > 0 && getElement('hdfMode').value == '0') {
             var suffix = ddlAgent.options[ddlAgent.selectedIndex].value;
             getElement('txtLoginSuffix').value = suffix;
         }
     }
     function CheckPassword() {

         if (getElement('txtPassword').value != getElement('txtConfirmPassword').value) addMessage('Password Mismatch!', '');
         if (getMessage() != '') {
             alert(getMessage()); clearMessage(); return false;
         }
     }


     function Save() {
         // ShowMessageDialog('testing', 'testing details','Severe')

         if (getElement('ddlParentAgent').selectedIndex <= 0) addMessage('Please select Agent from the List!', '');
         if (getElement('txtFirstName').value == '') addMessage('First Name cannot be blank!', '');
         if (getElement('txtLastName').value == '') addMessage('Last Name cannot be blank!', '');
         if (getElement('txtEmailID').value == '') addMessage('Email Id cannot be blank!', '');
         else if (!checkEmail(getElement('txtEmailID').value)) addMessage('Email is not valid!', '');
         if (getElement('txtLoginName').value == '') addMessage('Login Name cannot be blank!', '');
         if (getElement('hdfMode').value == '0') {
             if (getElement('txtPassword').value == '') addMessage('Password Name cannot be blank!', '');
             if (getElement('txtConfirmPassword').value == '') addMessage('Confirm Password Name cannot be blank!', '');
         }
         if (getElement('ddlLocation').selectedIndex <= 0) addMessage('Please select Location from the List!', '');
         if (getElement('txtWebAddress').value == '') addMessage('WebSite Address cannot be blank!', '');
         if (getElement('txtWebTitle').value == '') addMessage('WebSite Title cannot be blank!', '');
         if (getMessage() != '') {
             alert(getMessage()); clearMessage(); return false;
         }
     }



     function ShowDiv(name) {
         if (name == "Settings") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             getElement('lnkSettings').className = "active";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "";
             return false;
         }
         if (name == "Air") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CAirSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "active";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "";
             return false;
         }
         if (name == "Hotel") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CHotelSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "active";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "";
             return false;
         }
         if (name == "Insurance") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CInsuranceSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "active";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "";
             return false;
         }
         if (name == "Activity") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CActivitySettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "active";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "";
         }
         if (name == "FixDep") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CFixedDepartureSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "active";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "";
         }
         if (name == "Visa") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CVisaSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkVisa').className = "active";
             getElement('lnkTransfer').className = "";
         }
         if (name == "Sightseeing") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Hotel').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Transfer').style.dispaly = "none";
             window.location = 'B2CSightseeingSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "active";
             getElement('lnkTransfer').className = "";
         }
         if (name == "Transfer") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Settings').style.display = "none";
             document.getElementById('Air').style.display = "none";
             document.getElementById('Insurance').style.display = "none";
             document.getElementById('Activity').style.display = "none";
             document.getElementById('FixDep').style.display = "none";
             document.getElementById('Visa').style.dispaly = "none";
             document.getElementById('Hotel').style.dispaly = "none";
             document.getElementById('Sightseeing').style.dispaly = "none";
             window.location = 'B2CTransferSettings.aspx?memberId=' + getElement('hdnUserId').value + '&agentId=' + getElement('hdnAgentId').value;
             getElement('lnkSettings').className = "";
             getElement('lnkAir').className = "";
             getElement('lnkHotel').className = "";
             getElement('lnkInsurance').className = "";
             getElement('lnkActivity').className = "";
             getElement('lnkFixDep').className = "";
             getElement('lnkVisa').className = "";
             getElement('lnkSightseeing').className = "";
             getElement('lnkTransfer').className = "active";
             return false;
         }
     }

 </script>
 <%--<script type="text/javascript">


     $(function() {
         $('.tabs').tabs()
     });

     $('.tabs').bind('change', function(e) {
         var nowtab = e.target // activated tab
         var divid = $(nowtab).attr('href').substr(1);
         if (divid == "ajax") {
             $.getJSON('<%=Request.Url.Scheme%>://1.upstatic.sinaapp.com/api.php').success(function(data) {
                 $("#" + divid).text(data.msg);
             });
         }


     });
  
</script>--%>
 
 <div class="body_container">
 <div class="col-md-12"> <div class="setting_links"> 
  
  <ul>
                                    <li><asp:LinkButton ID="lnkSettings" runat="server" href="#" OnClientClick="return ShowDiv('Settings')" class="active">Settings</asp:LinkButton></li>
                                    <li><asp:LinkButton ID="lnkAir" runat="server" href="#" OnClientClick="return ShowDiv('Air')" style="display:none;">Air White Label</asp:LinkButton></li>
                                     <li><asp:LinkButton ID="lnkHotel" runat="server" href="#" OnClientClick="return ShowDiv('Hotel')" style="display:none;">Hotel White Label</asp:LinkButton></li>
                                     <li><asp:LinkButton ID="lnkInsurance" runat="server" href="#" OnClientClick="return ShowDiv('Insurance')" style="display:none;">Insurance White Label</asp:LinkButton></li>
                                      <li><asp:LinkButton ID="lnkActivity" runat="server" href="#" OnClientClick="return ShowDiv('Activity')" style="display:none;">Activity White Label</asp:LinkButton></li>
                                      <li><asp:LinkButton ID="lnkFixDep" runat="server" href="#" OnClientClick="return ShowDiv('FixDep')" style="display:none;">FixedDeparture White Label</asp:LinkButton></li>
                                      <li><asp:LinkButton ID="lnkVisa" runat="server" href="#" OnClientClick="return ShowDiv('Visa')" style="display:none;">Visa White Label</asp:LinkButton></li>
                                      <li><asp:LinkButton ID="lnkSightseeing" runat="server" href="#" OnClientClick="return ShowDiv('Sightseeing')" style="display:none;">Sightseeing White Label</asp:LinkButton></li>
                                      <li><asp:LinkButton ID="lnkTransfer" runat="server" href="#" OnClientClick="return ShowDiv('Transfer')" style="display:none;">Transfer White Label</asp:LinkButton></li>
                                </ul>
  
  </div>  </div>
        
        
        
   
                         <div class="">
                                <div class="active"  id="Settings">
                                <asp:HiddenField ID="hdnAgentId" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnUserId" runat="server" Value="0" />
                                <asp:HiddenField runat="server" id="hdfMode" value="0" ></asp:HiddenField> 
             
             
             <div class="paramcon">             
                               
                              <div class="col-md-12 marbot_10">          <h4>  Agency Details</h4></div>
                              
                              
                             <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblParentAgent" runat="server" Text="Parent Agent:"></asp:Label> </div>
 
    <div class="col-md-2"> <asp:DropDownList ID="ddlParentAgent" CssClass="inputDdlEnabled form-control" runat="server"
                                Width="153px" OnSelectedIndexChanged="ddlParentAgent_SelectedIndexChanged"  AutoPostBack="true" onchange="setLoginSuffix(this.id);">
                            </asp:DropDownList></div>
                            
                            
                            
    <div class="col-md-2"> <asp:Label ID="lblSubAgent" runat="server" Text="Sub Agent:" Enabled="true"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlSubAgent" CssClass="inputDdlEnabled form-control" runat="server"
                                Width="153px" AutoPostBack="true" OnSelectedIndexChanged="ddlSubAgent_SelectedIndexChanged" Enabled="true">
                            </asp:DropDownList> </div>
     <div class="col-md-2"><asp:Label ID="lblAgentType" runat="server" Text="Agent Type:"></asp:Label> </div>
         <div class="col-md-2"><asp:TextBox ID="txtAgentType" Enabled="false" CssClass="inputDisabled form-control" runat="server"></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblCountry" runat="server" Text="Country:"></asp:Label></div>
    <div class="col-md-2"> <asp:TextBox ID="txtCountry" Enabled="false" CssClass="inputDisabled form-control" runat="server"></asp:TextBox></div>
    
    
    <div class="col-md-2"><asp:Label ID="lblCurrency" runat="server" Text="Currency:"></asp:Label> </div>
     <div class="col-md-2"> <asp:TextBox ID="txtCurrency" Enabled="false" CssClass="inputDisabled form-control" runat="server"></asp:TextBox></div>
     <div class="col-md-2"> <asp:Label ID="lblPhoneNo" runat="server" Text="Phone Number:"></asp:Label></div>
         <div class="col-md-2"><asp:TextBox ID="txtPhoneNo" Enabled="false" CssClass="inputDisabled form-control" runat="server"></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>


       
           
           
        <div class="col-md-12 marbot_10">            <h4> B2C Details</h4></div>
           
                <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblFirstName" runat="server" Text="First Name:"></asp:Label></div>
    <div class="col-md-2"><asp:TextBox ID="txtFirstName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </div>
    
    <div class="col-md-2"> <asp:Label ID="lblLastName" runat="server" Text="Last Name:"></asp:Label></div>
    
     <div class="col-md-2"><asp:TextBox ID="txtLastName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </div>
     
     
     <div class="col-md-2"> <asp:Label ID="lblEmailId" runat="server" Text="Email ID:"></asp:Label></div>
     
         <div class="col-md-2"> <asp:TextBox ID="txtEmailID" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></div>

<div class="clearfix"></div>
    </div>
                    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblLoginName" runat="server" Text="Login Name:"></asp:Label> </div>
 
 
    <div class="col-md-2">
    
    
         <table width="100%"> 
        <tr> 
        <td> <asp:TextBox ID="txtLoginName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></td>
        
         <td><asp:TextBox ID="txtLoginSuffix" Enabled="false" CssClass="inputDisabled form-control" runat="server" Width="30px"></asp:TextBox> </td>
</tr>
        
        
        </table>
        
        
    
    
    
     </div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtPassword" CssClass="inputEnabled form-control" EnableViewState="true" runat="server"  TextMode="password"></asp:TextBox> </div>
     
     <div class="col-md-2"><asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password:"></asp:Label> </div>
     
         <div class="col-md-2"><asp:TextBox ID="txtConfirmPassword" CssClass="inputEnabled form-control" Onchange="return CheckPassword();" runat="server" TextMode="password"></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>


       
       
       
       
       
            <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblLocation" runat="server" Text="Location:"></asp:Label> </div>
    <div class="col-md-2"><asp:DropDownList ID="ddlLocation"  CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList> </div>
    
 
     <div class="col-md-2"> <asp:Label ID="lblWebAddress" runat="server" Text="WebSite Address:"></asp:Label></div>
     
         <div class="col-md-2"> <asp:TextBox ID="txtWebAddress" runat="server" CssClass="inputEnabled form-control"></asp:TextBox></div>
         
         
         
              <div class="col-md-2"><asp:Label ID="lblWebTitle" runat="server" Text="WebSite Title:"></asp:Label> </div>
              
         <div class="col-md-2"><asp:TextBox ID="txtWebTitle"  runat="server" CssClass="inputEnabled form-control"></asp:TextBox> </div>
         

<div class="clearfix"></div>
    </div>


              <div class="col-md-12 padding-0 marbot_10">   
      
      
      
         <div class="col-md-2"><asp:Label ID="lblAddress" runat="server" Text="Address:"></asp:Label> </div>
         
     <div class="col-md-6"><asp:TextBox ID="txtAddress"  runat="server" TextMode="multiline" Height="45" CssClass="inputEnabled" Width="100%" ></asp:TextBox> </div>
     
     
<div class="col-md-4"> <table>
    <tr>
    <td> Upload Logo:</td>
    
    
    
    <td>
    <CT:DocumentManager ID="agentImage" runat="server" DefaultImageUrl="~/images/common/no_preview.png"
                    DocumentImageUrl="~/images/common/Preview.png"   ShowActualImage="false"  SessionName="crenterlic"
                    DefaultToolTip=""  />
        </td>
    </tr>
    </table></div>
     
     
     
      <div class="clearfix"></div>
    </div>    
             
                      
                      
                      
      </div>      

        
        
        
              
      <div class="col-md-12 padding-0 marbot_10"> 
                                           
    <div class="col-md-6"> 
    
    
<div>     <asp:CheckBox ID="chkVoucherLogo" runat="server" Text="Display Voucher Logo" Font-Bold="true" /></div>
    
    <div>  <div style="height:250px; width:100%; margin-top:-0.5px; border:solid 1px" class="grdScrlTrans"   >
             <asp:GridView ID="gvUserRoleDetails" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="Role_id" 
            EmptyDataText="No Role Details!" AutoGenerateColumns="false" PageSize="10" GridLines="none"  CssClass="grdTable"
            CellPadding="1" CellSpacing="0"
            OnPageIndexChanging="gvUserRoleDetails_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black">Select</label>
    <asp:CheckBox runat="server" id="HTchkSelectAll" AutoPostbACK="true" OnCheckedChanged="HTchkSelectAll_CheckedChanged"  ></asp:CheckBox>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" AutoPostbACK="true"   OnCheckedChanged="ITchkSelect_CheckedChanged" CssClass="InputEnabled" Checked='<%# Eval("urd_status").ToString()=="A"%>' ></asp:CheckBox>
    </ItemTemplate>    
    </asp:TemplateField>   
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    
    <label style=" float:left"><cc1:Filter ID="HTtxtRoleName" Width="150px" HeaderText="Role Name" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />   </label>              
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblRolName" runat="server" Text='<%# Eval("Role_name") %>' CssClass="label grdof" ToolTip='<%# Eval("Role_name") %>' Width="150px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>  
    </Columns>           
    </asp:GridView>
                </div></div>
    
    
    
    </div>
    <div class="col-md-6"> 
    <div> <asp:CheckBox ID="chkInvoiceLogo" runat="server" Text="Display Voucher Logo" Font-Bold="true"/></div>
    
    
    <div> 
    
    <div style="height:250px;width:100%; margin-top:-0.5px;border:solid 1px" class="grdScrlTrans">
                    <asp:Label ID="Label1" runat="server" Text="Enable / Disable B2C Menus"></asp:Label>
                    <asp:CheckBoxList ID="chkListB2CMenu" runat="server">
                    </asp:CheckBoxList>
                </div>
    
    </div>
    
    
    </div>


    <div class="clearfix"></div>
    </div>
    
                  
                      
                      
                      
        
        
              
      <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-6"> </div>
    <div class="col-md-6"> <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
    
             <label class=" f_R mar-5"> <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
             
             
                    <label class=" f_R mar-5"> <asp:Button ID="btnCancel" Text="Clear" runat="server" CssClass="btn but_b"  OnClick="btnCancel_Click"></asp:Button></label>
    
    </div>


    <div class="clearfix"></div>
    </div>
    
    
                  
                      
                               
                               
                                
                                </div>
                                <div id="Air"></div>
                                 <div id="Hotel"></div>
                                 <div id="Insurance"></div>
                                 <div id="Activity"></div>
                                  <div id="FixDep"></div>
                                   <div id="Visa"></div>
                                    <div id="Sightseeing"></div>
                                    <div id="Transfer"></div>
                                </div>
                          
                          
   <div class="clearfix"></div>    
                          
 </div>
               
                
            
          
     

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

