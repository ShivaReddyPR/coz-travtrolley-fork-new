﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddVisaType" Title="Cozmo Travels" ValidateRequest="false" Codebehind="AddVisaType.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<link rel="stylesheet" type="text/css" href="css/style.css" />



 <div> 
    <div class="col-md-6"> <h4> <asp:Label runat="server" ID="lblStatus" Text="Add Visa Type"></asp:Label></h4> </div>
    
    <div class="col-md-6">
    
    <asp:HyperLink ID="HyperLink1" class="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaTypeList.aspx">Go to Visa Type List</asp:HyperLink>
   

 </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
    <div class="paramcon"> 
    
            <div> 
                <div class="col-md-2">
                    <label>
                        Visa Type<sup style="color: Red">*</sup></label>
                </div>
    
    <div class="col-md-2"> <p>
                    
                    <span>
                        <asp:TextBox CssClass="form-control" ID="txtVisaTypeName" runat="server"></asp:TextBox>&nbsp;</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server"
                        ErrorMessage="Please fill visa type " ControlToValidate="txtVisaTypeName"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Please enter maximum 50 character"
                        ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtVisaTypeName" Display="Dynamic"></asp:RegularExpressionValidator></p></div>     
           
                <div class="col-md-2">
                    <label>
                        Arabic Visa Type<sup style="color: Red">*</sup></label>
                </div>

                <div class="col-md-2">
                    <p>

                        <span>
                            <asp:TextBox CssClass="form-control" ID="txtVisaTypeNameAR" runat="server" onkeypress="return CheckArabicCharactersOnly(event);"></asp:TextBox>&nbsp;</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server"
                            ErrorMessage="Please fill visa type Arabic" ControlToValidate="txtVisaTypeNameAR"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Please enter maximum 50 character"
                            ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtVisaTypeNameAR" Display="Dynamic"></asp:RegularExpressionValidator>
                    </p>
                </div>
    
    <div class="col-md-2"><label>
                        Country<sup style="color: Red">*</sup></label> </div>     
    
    <div class="col-md-2"> <p>
                    
                    <span>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select country "
                        ControlToValidate="ddlCountry" InitialValue="-1"></asp:RequiredFieldValidator>
                </p></div> 
    <div class="clearfix"> </div> 
    </div>
           <div class="margin-bottom-10"> 
               <div class="col-md-2"> <label>
                        Processing Days<sup style="color: Red">*</sup></label></div>
    
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtProcessingDays"
                            MaxLength="4"></asp:TextBox>
                    </span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ControlToValidate="txtProcessingDays" ErrorMessage="Please fill numeric value "
                        ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please fill processing days "
                        ControlToValidate="txtProcessingDays" Display="Dynamic"></asp:RequiredFieldValidator>
                </p> </div>    
    <div class="col-md-2"> <label>
                        Validity Days</label></div>
    <%--<sup style="color: Red">*</sup>--%>
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtvalidityDays"
                            MaxLength="4"></asp:TextBox>
                    </span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                        ControlToValidate="txtvalidityDays" ErrorMessage="Please fill numeric value "
                        ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please fill validity  days "
                        ControlToValidate="txtvalidityDays" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                </p> </div>    
    <div class="col-md-2"> <label>
                        Living Validity</label></div>
    <%--<sup style="color: Red">*</sup>--%>
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtLivingValidity"
                            MaxLength="4"></asp:TextBox>
                    </span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                        ControlToValidate="txtLivingValidity" ErrorMessage="Please fill numeric value "
                        ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please fill living validity  days "
                        ControlToValidate="txtLivingValidity" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                </p> </div>     
    
        
    
    <div class="clearfix"> </div> 
    </div>
    
    <div class="margin-bottom-10">

        <div class="col-md-2"><p><label>
                        Visa Icon</label></p> </div>
    <%--<sup style="color: Red">*</sup>--%>
    <div class="col-md-2"> <p>
                    
                    <span>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                        <asp:FileUpload Style="width: 160px" CssClass="myuploadcontrol" ID="visaIconFileUpload"
                            runat="server" />
                        </ContentTemplate>
                        <Triggers>
                        <asp:PostBackTrigger ControlID="btnSave" />
                        </Triggers>
                        </asp:UpdatePanel>
                        
                    </span>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please select visa icon. "
                        ControlToValidate="visaIconFileUpload" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="visaIconFileUpload"
                        Display="Dynamic" ErrorMessage="Only .jpg or .gif files are allowed!" ValidationExpression="^(.*?)\.((jpg)|(JPG)|(gif)|(JPEG)|(jpeg)|(GIF))$"></asp:RegularExpressionValidator>
                </p></div>  
            <div class="col-md-2">
                <p>
                <label>
                    Agent<sup style="color: Red">*</sup></label>
                    </p>
            </div>
            <div class="col-md-2">
                <p>
                        <asp:DropDownList ID="ddlAgent" runat="server" Width="100%" CssClass="form-control" Height="120px">
                        </asp:DropDownList>
                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Please select Agent from List"
                        ValidationExpression="[\S\s]{0,600}" ControlToValidate="ddlAgent" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please select Agent "
                        ControlToValidate="ddlAgent" InitialValue="-1"></asp:RequiredFieldValidator>
                    </p>
            </div>
            <div class="clearfix"></div>
        </div>
    
        <div class="margin-bottom-10">
            <div class="col-md-2">
                <label>
                    Description</label>
            </div>

            <div class="col-md-6">
                <p style="width: 100% !important">

                    <span>
                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="100%"
                            CssClass="inp_border18" Height="120px"></asp:TextBox>
                    </span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Please enter maximum 600 character"
                        ValidationExpression="[\S\s]{0,600}" ControlToValidate="txtDescription" Display="Dynamic"></asp:RegularExpressionValidator>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="margin-bottom-10">
            <div class="col-md-2">
                <label>
                    Arabic Description</label>
            </div>

            <div class="col-md-6">
                <p style="width: 100% !important">

                    <span>
                        <asp:TextBox ID="txtDescriptionAR" runat="server" TextMode="MultiLine" Width="100%" onkeypress="return CheckArabicCharactersOnly(event);"
                            CssClass="inp_border18" Height="120px"></asp:TextBox>
                    </span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Please enter maximum 600 character"
                        ValidationExpression="[\S\s]{0,600}" ControlToValidate="txtDescriptionAR" Display="Dynamic"></asp:RegularExpressionValidator>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
    
             <div class="margin-bottom-10"> 
    <div class="col-md-2"> </div>
    
    <div class="col-md-2"><asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b"
                                OnClick="btnSave_Click" /> </div>    

    
    <div class="clearfix"> </div> 
    </div>
    
    
    
    </div>
     <script  type="text/javascript">
// Allow Arabic Characters only
function CheckArabicCharactersOnly(e) {
var unicode = e.charCode ? e.charCode : e.keyCode
if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
if (unicode == 32)
return true;
else {
if ((unicode < 48 || unicode > 57) && (unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabic
return false; //disable key press
}
}
}
</script>

    
</asp:Content>
