﻿<%@ Page Title="B2C Hotel Pending Booking Details" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="B2CHotelPendingQueue.aspx.cs" Inherits="CozmoB2BWebApp.B2CHotelPendingQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
       <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <iframe id="IShimFrame" style="position: absolute; display: none;"></iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
        </div>
    </div>
         <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
    <div class="body_container" style="margin-top: 10px; min-height: 100%;">
        <div class="row">
           <div class="col-md-3">
                        <span>From Date</span>
                        <div class="form-group">
                            <div class="input-group date fromDate">
                                <asp:TextBox ID="txtFromDate" runat="server" placeholder="DD/MM/YYYY" CssClass="inputEnabled form-control"></asp:TextBox>
                                <asp:Label runat="server" class="input-group-addon" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </asp:Label>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-3">
                        <span>To Date</span>
                        <div class="form-group">
                            <div class="input-group date toDate">
                                <asp:TextBox ID="txtToDate" runat="server"  placeholder="DD/MM/YYYY" CssClass="form-control" ></asp:TextBox>
                                <asp:Label runat="server" class="input-group-addon" onclick="showCal2()">
                                        <img id="dateLink2" src="images/call-cozmo.png" alt="Pick Date" /> 
                                   </asp:Label>
                            </div>
                        </div>                         
                    </div>
            <div class="col-md-3">
                <span>Payment Source</span>
                <div class="form-group">
                    <asp:DropDownList ID="ddlpaymentSource" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <span>Booking Status</span>
                <div class="form-group">
                    <asp:DropDownList ID="ddlbookingStatus" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <span>Select Agent</span>
                <div class="form-group">
                    <asp:DropDownList ID="ddlAgents" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <span>Payment Id</span>
                <div class="form-group">
                    <asp:TextBox ID="txtPaymentId" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <span>Order Id</span>
                <div class="form-group">
                    <asp:TextBox ID="txtOrderId" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <span>Hotel Name</span>
                <div class="form-group">
                    <asp:TextBox ID="txtHotelName" runat="server" CssClass="form-control" />
                </div>
            </div>
        </div>
        <div class="row" style="float: right; margin-right: 10px;">
            <asp:Button ID="btnSearch" runat="server" class="btn btn-info btn-primary" Text="Search" OnClick="btnSearch_Click" />
        </div>
        <br />
    </div>
    <div class="container" style="text-align: center; overflow: auto; border-radius: 5px;">
        <asp:GridView ID="GridPendingQueue" ShowHeaderWhenEmpty="true" runat="server" CellPadding="4" CssClass="table"
            EmptyDataText="Hotel pending Queue details not found." AllowPaging="True" Style="margin-top: 10px;"
            AutoGenerateColumns="false" PageSize="10" OnPageIndexChanging="GridPendingQueue_PageIndexChanging">
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <HeaderStyle CssClass="ns-h3" BackColor="red" Font-Bold="True" ForeColor="White" />
            <PagerStyle CssClass="pagination-ys" HorizontalAlign="Left" />
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
            <Columns>
                <asp:TemplateField HeaderText="SL.No">
                    <ItemTemplate>&nbsp;&nbsp;<%#Container.DataItemIndex+1 %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Passerger Details">
                    <ItemTemplate>
                        <asp:Button ID="btnViewPax" runat="server" CssClass="btn btn-info btn-primary" OnClientClick="showmodal(this.id);" Text="View" />
                        <asp:Label ID="lblPassengerInfo" Style="display: none;" runat="server" Text='<%# Eval("passengerInfo")%>' />

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment Source">
                    <ItemTemplate>
                        <asp:Label ID="lblpaymentSource" Style="display: none" runat="server" Text='<%# Eval("paymentGatewaySourceId")%>' />
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("PaymentGateway")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment Id">
                    <ItemTemplate>
                        <asp:Label ID="lblpaymentId" CssClass="label grdof" runat="server" Text='<%# Eval("paymentId")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Order Id">

                    <ItemTemplate>
                        <asp:Label ID="lblorderId" CssClass="label grdof" runat="server" Text='<%# Eval("orderId")%>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Booking Date">
                    <ItemTemplate>
                        <asp:Label ID="lnblCreatedOn" CssClass="label grdof" runat="server" Style="width: 100%;" Text='<%# Eval("createdOn","{0:yyyy-MM-dd HH:mm}") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Check In Date">
                    <ItemTemplate>
                        <asp:Label ID="lbldeptDate" runat="server" CssClass="label grdof" Text='<%# Eval("checkInDate","{0:yyyy-MM-dd HH:mm}") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Check out Date">
                    <ItemTemplate>
                        <asp:Label ID="lblReturnDate" CssClass="label grdof" runat="server" Text='<%# Eval("checkOutDate","{0:yyyy-MM-dd HH:mm}") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                   <asp:TemplateField HeaderText="Hotel Source">
                    <ItemTemplate>
                        <asp:Label ID="lblHotelSource" runat="server" CssClass="label grdof" Text='<%# Eval("hotelsource")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hotel Name">
                    <ItemTemplate>
                        <asp:Label ID="lblhotelName" runat="server" CssClass="label grdof" Text='<%# Eval("hotelName")%>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="No of Rooms">
                    <ItemTemplate>
                        <asp:Label ID="lblrooms" runat="server" CssClass="label grdof" Text='<%# Eval("noOfRooms")%>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="city ref">
                    <ItemTemplate>
                        <asp:Label ID="lblcity" runat="server" CssClass="label grdof" Text='<%# Eval("cityRef")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="city code">
                    <ItemTemplate>
                        <asp:Label ID="lblcityCode" runat="server" CssClass="label grdof" Text='<%# Eval("cityCode")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="country">
                    <ItemTemplate>
                        <asp:Label ID="lblCountry" runat="server" CssClass="label grdof" Text='<%# Eval("country")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hotel Address">
                    <ItemTemplate>
                        <asp:Label ID="lblHotelAddress" runat="server" CssClass="label grdof" Text='<%# Eval("address1")+","+Eval("address2")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
              
                <asp:TemplateField HeaderText="Booking Status">
                    <ItemTemplate>
                        <asp:Label ID="lblBookingStatus" CssClass="label grdof" runat="server" Text='<%# Eval("Booking_status")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment Status">
                    <ItemTemplate>
                        <asp:Label ID="lblPaymentStatus" CssClass="label grdof" runat="server" Text='<%# Eval("payment_status")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Amount">
                    <ItemTemplate>
                        <asp:Label ID="lblPremoumAmount" runat="server" CssClass="label grdof" Text='<%# Eval("paymentAmount")%>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label ID="lblEmail" runat="server" CssClass="label grdof" Text='<%# Eval("email")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone">
                    <ItemTemplate>
                        <asp:Label ID="lblPhone" runat="server" CssClass="label grdof" Text='<%# Eval("phone")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Agent">
                    <ItemTemplate>
                        <asp:Label ID="lblAgentName" runat="server" CssClass="label grdof" Text='<%# Eval("agent_name")%>' />
                        <asp:Label ID="lblDecimalValue" Style="display: none;" runat="server" Text='<%# Eval("agent_decimal")%>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="IP Address">
                    <ItemTemplate>
                        <asp:Label ID="lblipAdd" runat="server" CssClass="label grdof" Text='<%# Eval("ipAddress")  %>   ' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remarks">
                    <ItemTemplate>
                        <asp:Label ID="lblRemarks" CssClass="label grdof" runat="server" Text='<%# Eval("Remark")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
            </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalpassengerInfo" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="color: white">
                    <asp:Label ID="lbltitle" Style="color: white; text-align: center; margin-top: 15px" runat="server" Text="Passenger Details"></asp:Label>
                    <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body" id="paxbody">
                    <div  class="row">
                       <div class="col-md-4"> <span style="font-weight: bold;">Email  :</span>&nbsp;&nbsp;<span id="paxEmail"></span></div> 
                       <div class="col-md-4">  <span style="font-weight: bold;">Phone :</span>&nbsp;&nbsp;<span id="paxPhone"></span></div> 
                       <div class="col-md-4">  <span style="font-weight: bold;">city :</span>&nbsp;&nbsp;<span id="paxcity"></span></div>  
                       <div class="col-md-4">  <span style="font-weight: bold;">state :</span>&nbsp;&nbsp;<span id="paxstate"></span></div>
                       <div class="col-md-4">  <span style="font-weight: bold;">country :</span>&nbsp;&nbsp;<span id="paxcountry"></span></div>
                     </div>
                    <h1 class="ns-h3" style="border-radius: 5px; margin-top: 20px;">Passenger Details</h1>
                    <table id="tablepassenger" class="table table-hover">
                    </table> 
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnclose" runat="server" data-dismiss="modal" CssClass="btn btn-info btn-primary" Text="Close" Style="border-radius: 5px" />
                </div>
            </div>
        </div>
    </div>  
      <script>
        var cal1;
        var cal2;
        function init() {
            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select From date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select To date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }
        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
             //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear(); cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }
            var date2 = cal2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        function CheckValidDate(Day, Mn, Yr) {
            var DateVal = Mn + "/" + Day + "/" + Yr;
            var dt = new Date(DateVal);

            if (dt.getDate() != Day) {
                return false;
            }
            else if (dt.getMonth() != Mn - 1) {
                //this is for the purpose JavaScript starts the month from 0
                return false;
            }
            else if (dt.getFullYear() != Yr) {
                return false;
            }
            return (true);
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  
    <script type="text/javascript">         
        function showmodal(id) {
            id = id.replace("btnViewPax", "lblPassengerInfo");
            var paxinfo = $('#' + id).html();
            var i = 1;
            var name,  paxtype, age, RoomIndex;
            $("#tablepassenger tr").remove();
            var th = "<tr><th>SL.No</th> <th>Name</th>  <th>Pax Type</th><th>Age</th><th>Room Index</th></tr>";
            $('#tablepassenger').append(th);
            $(paxinfo).find('WSGuest').each(function () {
                name = "<td>" + $(this).find('Title').text() + " " + $(this).find('FirstName').text() + "" + $(this).find('LastName').text() + "</td>";
                paxtype = "<td>" + $(this).find('GuestType').text() + "</td>";
                RoomIndex = "<td>" + $(this).find('RoomIndex').text() + "</td>";
                age = "<td>" + $(this).find('Age').text() + "</td>";
                $('#tablepassenger').append("<tr><td>" + i + "</td>" + name + paxtype + age + RoomIndex + "</tr>");
                i++;
                var email = $(this).find('Email').text();
                var phone = $(this).find('Phoneno').text();
                var City = $(this).find('City').text();
                var State = $(this).find('State').text();
                var Country = $(this).find('Country').text();   
                if (email != '' && phone != '') {
                    $('#paxEmail').text(email);
                    $('#paxPhone').text(phone);
                    $('#paxcity').text(City);
                    $('#paxstate').text(State);
                    $('#paxcountry').text(Country);
                }
            });
            var rowCount = $('#tablepassenger tr').length;
            if (rowCount > 1) {
                $('#modalpassengerInfo').modal('show');
            }
        }
        function formatDate(date) {
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = date.getDate();
            if (day < 10)
                day = '0' + day;
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            return year + '/' + monthNames[monthIndex] + '/' + day;
        }        
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
               args.set_errorHandled(true);
            }
        }
    </script>
    <style type="text/css">
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
            background-color: #6d6b6c;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    font-weight: bold;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }

        #GridPendingQueue thead tr th {
            text-align: center !important;
            width: auto;
        }

        #GridPendingQueue tbody tr td {
            width: 100%;
        }

        #tablepassenger tr th {
            font-weight: bold;
        }

        .modal-content {
            width: 150%;
            overflow-y: auto;
        }

        .modal-body {
            height: auto;
            overflow: auto;
        }
    </style>
</asp:Content>
