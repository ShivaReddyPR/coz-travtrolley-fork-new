<%@ Control Language="C#" AutoEventWireup="true" Inherits="DateControl" Codebehind="DateControl.ascx.cs" %>
<div class="form-groupp date-control-input">    
    <asp:TextBox ID="Date" cssDisabled="inputDisabled" cssEnabled="inputEnabled" CssClass="inputEnabled width75 form-control date"  autocomplete="off"   runat="server" MaxLength="10"></asp:TextBox>
    <asp:TextBox ID="Time" cssDisabled="inputDisabled" cssEnabled="inputEnabled" CssClass="inputEnabled form-control time"  autocomplete="off"  runat="server" MaxLength="5"></asp:TextBox>
    <div class="clearfix"></div>
</div> 
<script>
 function dateControl(){
  $('.date-control-input').each(function () {
        if ($(this).find('.form-control').hasClass('time') && $(this).find('.form-control').hasClass('date')  ) {
            $(this).addClass('dateNtime');
        } else if($(this).find('.form-control').hasClass('time')) {
            $(this).addClass('timeOnly');
        } else if($(this).find('.form-control').hasClass('date')) {
            $(this).addClass('dateOnly');
        }
    })
 }
  $(document).ready(function(){
     dateControl();
  })
   var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
          dateControl();
        });

</script>
