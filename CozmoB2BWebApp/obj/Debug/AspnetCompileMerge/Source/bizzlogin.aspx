﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bizzlogin.aspx.cs" Inherits="CozmoB2BWebApp.bizzlogin" %>

<!DOCTYPE html>
<html>
<head runat="server">
<title> Corporate Travel   </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="images/favicon.ico">    	


 <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />

<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />

<link href="css/Default.css" rel="stylesheet" type="text/css" />

<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />

 <link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
        
    
          <!-- Bootstrap Core CSS -->
   <link href="css/bootstrap.min.css" rel="stylesheet" />

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />
    
    <link href="yui/build/fonts/fonts-min.css" rel="stylesheet" type="text/css" />
    
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="BookingPanelStyle.css" />
    
    <link href="https//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
    
    <link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
    
   <link href="css/toastr.css" rel="stylesheet" type="text/css" />
  


    
<link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />


    
  
</head>
<body>


<style> 
.theme-border-color, .pagination.tt-paging b a, .btn-primary:hover, .form-control:focus, .select2-drop-active, .form-control.select2-container.select2-dropdown-open, .btn-primary, .but_d, .qq-upload-button {
    border-color: #ccc !important;
}




</style>





<div class="topHeader2"> 

<div class="container"> 
<div class="row"> 

<div class="col-xs-6"> 
<img src="images/logoc.jpg" class="img-responsive">   

</div> 
<div class="col-xs-6"> 
<img src="images/bizzlogo.jpg"  class="img-responsive float-right" style="display:none">   

</div> 
          







</div>
</div>

</div>
           <form runat="server" id="form1"> 
    <asp:ScriptManager ID="ScriptManager" runat="server">
</asp:ScriptManager>
         <asp:UpdatePanel ID="upnlLogin" runat="server" UpdateMode="conditional">
        <ContentTemplate>
<div class="bgRegister2"> 

<div class="container"> 


<div class="row">

<div class="col-md-7">



<div class="title"> 





<h1> Business Travel, Personalized, Simplified & Delivered  </h1>

</div>










  </div>

<div class="col-md-5">

<div class="createYourAccount2"> 







<div class="row"> 

<div class="col-md-12"> 



<div class="col-12 mb-5"> 


<div class="row"> 


<div class="col-md-6 col-6 text-right"> 



<span class="logocctm"> <img src="images/cctmlogoc.jpg" /> </span>

 </div>


<div class="col-md-6 col-6 text-left">


<div class="linkChanger">  </div>

  </div>
</div>

</div>





<div id="loginTrav">

<div class="col-12"> 


<div class="form-control-holder">
<div class="icon-holder">
<span class="icon-user"></span>
 </div>
         <asp:TextBox ID="txtLoginName" class="form-control" autocomplete="off" placeholder="User Name" runat="server"></asp:TextBox>                                                            
</div>                
                          

</div>

<div class="col-12"> 


<div class="form-control-holder">
<div class="icon-holder">
<span class="icon-password"></span>
 </div>
     <asp:TextBox ID="txtPassword" class="form-control"  autocomplete="off" placeholder="Password"  runat="server" TextMode="password"></asp:TextBox>                                                     
</div>                
                          

</div>



<style>

    .icon-password:before {
    content: '\ea35';
}



.footerReg { margin-top:0px; }

</style>
<div class="col-12"> 
  <span> 
        <asp:Label ID="lblForgotPwd" runat="server" Text="" style="color:Red; font-size:14px;"></asp:Label>
        <asp:Label style="color:Red; font-size:14px;" runat="server" ID="lblError" CssClass="lblError"></asp:Label></span>
<div class="form-control-holder">
 <asp:Button ID="btnLogin" class="btn btn-lg btn-block" runat="server" Text="SUBMIT"  OnClick="btnLogin_Click" OnClientClick=" Validate()"/>
</div> 
 </div>


 </div>

<div id="normalTrav" class="col-12 mt-4 normalTrav"> 
 
<a href="Register.aspx" target="_blank"> <strong> Register </strong> </a> &nbsp;  |   &nbsp;     <a id="forgotpassword" href="#" onclick="return ShowPopUp(this.id);" > Forgot password?</a>
 
 </div>



 <!--<div class="col-12 mt-2"> 


<div class="form-control-holder">
<div class="icon-holder">
<span class="icon-language"></span>
 </div>
                                    
<select class="form-control">

 <option>Select Country</option>
 
  <option>United Arab Emirates</option>
  
    <option>India</option>
</select> 
                                    
                                    
                                       
</div>                
                          

</div> -->

</div>



<div class="clearfix"> </div>

</div>


</div>

  </div>


</div>


</div>


</div>
       <div  class="modal" id="forgotPwd">
           
			
			
			<div class="modal-content forgot_div">
      <em class="close close_popup" style=" position:absolute;  right:10px; top:10px; cursor:pointer">
					  
						<i><img align="right" alt="Close" onclick="HidePopUp()" src="images/close-itimes.gif"></i>
					</em>

					
					
           
    <div><center> <h4>Forgot password? </h4></center> </div>
                                        
      <div>  
      
      <asp:TextBox ID="txtEmailId" CssClass="form-control margin_top10" placeholder="Enter Your Email Address" runat="server" CausesValidation="True" ></asp:TextBox>
      
      
      
       </div>
            
         <div class="martop_14">
                
               
                
                
                
                <asp:Button ID="btnGetPassword" CssClass="chosencol btn" runat="server" Text="Get Password" OnClientClick="return Validate()"
                            OnClick="btnGetPassword_Click"  />
                          
                          
                           <b style="display: none; color:Red" id="err"></b>
                
               
                
                </div>   
           
           
            
           
    </div>


		
					
					         

              
                   
               
           </div>     
    </ContentTemplate>
        </asp:UpdatePanel>
    </form>

<div class="RegisterWrapper">

<div class="container"> 

<div class="row"> 

<div class="col-md-12"> 

<div class="headingRegister2"> 


<h2> Our Services </h2>
<div class="saprater"> </div>

</div>


</div>

</div>


<div class="row mt-5"> 


<div class="col-md-4"> 


<a href="CozmoBiz-AirTickets.aspx" class="productWrap"> 

<div class="productIcons"> <span> <label> <img src="build/img/img2/airplane.svg" />  </label>  </span> </div>

<h3> Air Tickets</h3>
<p>To ensure the quality and effectiveness of every business.  </p>




 </a>


  </div>

<div class="col-md-4"> 


<a href="CozmoBiz-VisaServices.aspx" class="productWrap"> 
<div class="productIcons"> <span> <label> <img src="build/img/img2/passport.svg" />  </label>  </span> </div>

<h3> Visa Services</h3>
<p>To ensure the quality and effectiveness of every business.  </p>


</a>



 </div>

<div class="col-md-4"> 


<a href="CozmoBiz-Hotels.aspx" class="productWrap"> 
<div class="productIcons"> <span> <label> <img src="build/img/img2/hotel.svg" />  </label>  </span> </div>

<h3>Hotels</h3>
<p>To ensure the quality and effectiveness of every business.  </p>
</a>



 </div>

<div class="col-md-4"> 

<a href="CozmoBiz-Forex.aspx" class="productWrap"> 
<div class="productIcons"> <span> <label> <img src="build/img/img2/currency.svg" />  </label>  </span> </div>

<h3>Forex</h3>
<p>To ensure the quality and effectiveness of every business.  </p>
</a>



 </div>

<div class="col-md-4"> 

<a href="CozmoBiz-TravelInsurance.aspx" class="productWrap"> 
<div class="productIcons"> <span> <label> <img src="build/img/img2/baggage.svg" />  </label>  </span> </div>

<h3> Travel Insurance</h3>
<p>To ensure the quality and effectiveness of every business.  </p>
</a>


 </div>

<div class="col-md-4"> 

<a href="CozmoBiz-CozmoMICE.aspx" class="productWrap"> 
<div class="productIcons"> <span> <label> <img src="build/img/img2/team.svg" />  </label>  </span> </div>

<h3> Mice</h3>
<p>To ensure the quality and effectiveness of every business.  </p>

</a>



 </div>



</div>




</div>


</div>



<div style=" display:none" class="TestimonialWrapper">
<div class="container"> 

<div class="row"> 

<div class="col-md-12"> 

<div class="headingRegister2"> 


<h2>Customer Speaks </h2>
<div class="saprater"> </div>

</div>


</div>

</div>

<div class="row"> 

<div class="col-md-4"> 

<div class="clientBox"> 

<div> 

<span class="s1"> <img class="clientImage" src="images/cc2.jpg" />  </span>


<span class="s2">  <img class="clientComma" src="build/img/inverted-commas.svg" />  </span>


<div class="clearfix"> </div>

</div>
<div class="clientSpeaks"> 
<p> It is not every day that you come across a passionate and trustworthy financial advisor. 
John Doe is true professional who does his work really well. Thanks John! 
</p>
</div>
<div class="clientName"> Johnson.M.J. </div>

<div class="clientCompany"> <small> Manager at ABC Travel</small> </div> </div>

</div>


<div class="col-md-4"> 

<div class="clientBox"> 

<div> 

<span class="s1"> <img class="clientImage" src="images/cc1.jpg" />  </span>


<span class="s2">  <img class="clientComma" src="build/img/inverted-commas.svg" />  </span>


<div class="clearfix"> </div>

</div>
<div class="clientSpeaks"> 
<p> In just 2 very short meetings with John he managed to find the solution personally catered to my situation and expectations. Punctual, well informed and very reliable.
</p>
</div>
<div class="clientName">  Carol Harper  </div>

<div class="clientCompany"> <small> Director at ABC Travel</small> </div> </div>

</div>

<div class="col-md-4"> 

<div class="clientBox"> 

<div> 

<span class="s1"> <img class="clientImage" src="images/cc3.jpg" />  </span>


<span class="s2">  <img class="clientComma" src="build/img/inverted-commas.svg" />  </span>


<div class="clearfix"> </div>

</div>
<div class="clientSpeaks"> 
<p> A very professional financial advisor, who is true to his word. John has demonstrated a high amount of integrity in the time I have known him, and he is fast to respond to my concerns.
</p>
</div>
<div class="clientName">  Snow.J.R.  </div>

<div class="clientCompany"> <small> Managing Director of ABC Travel</small> </div> </div>

</div>


</div>


</div>

 </div>




<div class="footer_sml_bizz"> 
 
 
  Copyright 2019  Cozmo Travel LLC, UAE . All Rights Reserved. 


 
 
 </div>

   <div  style="display: none"> 

      <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 150px">
            </td>
            <td style="width: 150px">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label Visible="false" ID="lblCompanyName" runat="server" Text="Company:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" Visible="false" ID="ddlCompany" CssClass="inputDdlEnabled"
                    Width="154px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="height: 15px">
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
            </td>
        </tr>
    </table>
  
  
  </div> 



    <script type="text/javascript" src="<%=Request.Url.Scheme%>://travtrolley.com/Scripts/Menu/query2.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    
    
     <script type="text/javascript" src="<%=Request.Url.Scheme%>://travtrolley.com/build/js/main.min.js"></script>
     
	 
	 
	 <script src="<%=Request.Url.Scheme%>://travtrolley.com/scripts/select2.min.js" type="text/javascript"></script>
     
     
     
     
     <script type="text/javascript">
         $(document).ready(function () {

          $('select').select2();


         });
    
</script>




<script>


    //on scroll up/down show/hide top icon

    $('body').prepend('<a href="#" class="back-to-top"><i class="icon-expand_less"></i></a>');

    var amountScrolled = 300;

    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('slow');
        } else {
            $('a.back-to-top').fadeOut('slow');
        }
    });


    //on click scroll to top 

    $('a.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });


</script>




<script>



    $('#Products2,#Partners2').click(function () {
                        var Linkid = $(this).attr('href'),
                            Linkid = Linkid.replace('#', '');
                        $('html, body').animate({
                            scrollTop: $('[name="' + Linkid + '"]').offset().top
                        }, 500);


                        //        alert("Hello! I am an alert box!");   
      


    })

       

</script>



 
    <link href="build/css/owl-carousel/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href="build/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
 

</body>
</html>


    <script language="javascript" type="text/javascript">

        function setFocus() {
            document.getElementById('ctl00_cphTransaction_txtLoginName').focus();
        }
        setTimeout('setFocus()', 1000);

        function ShowPopUp(id) {
            document.getElementById('<%=txtEmailId.ClientID %>').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('forgotPwd').style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('forgotPwd').style.left = (positions[0] + 130) + 'px';
            //            document.getElementById('forgotPwd').style.top = (positions[1] - 200) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('forgotPwd').style.display = "none";
        }
        function getRelativePositions(obj) {
            var curLeft = 0;
            var curTop = 0;
            if (obj.offsetParent) {
                do {
                    curLeft += obj.offsetLeft;
                    curTop += obj.offsetTop;
                } while (obj = obj.offsetParent);

            }
            return [curLeft, curTop];
        }
        function Validate() {

            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
            
       
     </script>


 


<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>


