﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ActivityCategoriesListGUI" Title="Activity Categories list" Codebehind="ActivityCategoriesList.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<!-- ----------------------For Calender Control--------------------------- -->
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<!-- ----------------------End Calender Control--------------------------- -->
<script type="text/javascript">

    var cal1;
    var cal2;
    function init() {
        var dt = new Date();
        cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
        cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + (dt.getDate() + 1) + "/" + dt.getUTCFullYear()); //Stopping same day selection in Calendar by Shiva 15 Oct 2016
        cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
        //        cal1.cfg.setProperty("title", "Select your desired checkin date:");
        cal1.cfg.setProperty("close", true);
        cal1.cfg.setProperty("iframe", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();
    }

    function showCalendar1() {

        document.getElementById('container1').style.display = "block";
        document.getElementById('Outcontainer1').style.display = "block";
    }

    var departureDate = new Date();

    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMessHotel').style.visibility = "visible";
            document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=txtDepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        cal1.hide();
        document.getElementById('Outcontainer1').style.display = "none";
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
    
    

    
    </script>
    <script type="text/javascript">
//Validate the qty entered against the pax count
        function ValidateQty() {
            var adultCount = 0;
            var rows = document.getElementById('<%=hdnRows.ClientID %>').value;
            for (var i = 1; i <= rows; i++) {
                var txtQty = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_ddlQty').value;
                var minPax = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_hdnMinPax').value;
                var PaxType = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_lblFlexLabel').innerHTML;
                //if (PaxType == 'Adult') {
                if (txtQty < minPax) {
                    if (window.navigator.appCodeName != "Mozilla") {
                        document.getElementById('errMessHotel').style.display = "block";
                        document.getElementById('errMessHotel').innerText = PaxType + " Minimum " + minPax + " Pax is required";
                    }
                    else {
                        document.getElementById('errMessHotel').style.display = "block";
                        document.getElementById('errMessHotel').textContent = PaxType + " Minimum " + minPax + " Pax is required";
                    }
                    return false;
                }
                adultCount = parseInt(adultCount) + parseInt(txtQty);
            }
            var date = document.getElementById('<%=txtDepDate.ClientID %>').value;
            if (date.length <= 0 || date == 'DD/MM/YYYY') {
                if (window.navigator.appCodeName != "Mozilla") {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').innerText = "Enter Valid Date.";
                }
                else {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').textContent = "Enter Valid Date.";
                }
                return false;
            }
            document.getElementById('<%=hdnAdult.ClientID %>').value = adultCount;
            return true
        }
    function CalculateTotal1(id) {
        var x = id;
    }
    function CalculateTotal() {
        var decimalpoint = eval('<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue %>');
        var rows = eval(document.getElementById('<%=hdnRows.ClientID %>').value);
        var sum = eval(0);
        for (var i = 1; i <= rows; i++) {
            var txtQty = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_ddlQty').value;
            var price = eval(0);
            if (window.navigator.appCodeName != "Mozilla") {
                price = eval(document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_lblPrice').innerText.replace(',', ''));
            }
            else {
                price = eval(document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_lblPrice').textContent.replace(',', ''));
            }
            var total = txtQty * price;

            sum += total;
            if (window.navigator.appCodeName != "Mozilla") {
                document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_lblTotal').innerText = parseFloat(total).toFixed(decimalpoint);
                document.getElementById('lblSumTotal').innerHTML = parseFloat(sum).toFixed(decimalpoint);
            }
            else {
                document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + i + '_lblTotal').textContent = parseFloat(total).toFixed(decimalpoint);
                document.getElementById('lblSumTotal').textContent = parseFloat(sum).toFixed(decimalpoint);
            }
        }
    }
    
    
    function Check(id) {
        var val = document.getElementById(id).value;
        if (val == '0') {
            document.getElementById(id).value = '';
        }
    }
    function Set(id) {
        var val = document.getElementById(id).value;
        if (val == '' || val == '0') {
            document.getElementById(id).value = '0';
        }
    }
 
</script>
    <%--<form id="form1" runat="server">--%>
    <asp:HiddenField ID="hdnRows" runat="server" />
    <asp:HiddenField ID="hdnPaxCount" runat="server" />
   <asp:HiddenField ID="hdnAdult" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChild" runat="server"   Value="0"/>
    <asp:HiddenField ID="hdnInfant" runat="server"  Value="0"/>
    
        <div>
            <div class="ns-h3">
                    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></div>
            <div class="wraps0ppp" style="position:relative">

                  <div id="Outcontainer1" style="position:absolute;display: none;border:solid 0px #ccc;  z-index:200;width: 100%;left: 0; top: 46px; " >
                    <div id="container1" style="left:25%;">
                    </div>
                  </div>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width:400px;">
                                        <label style="font-size: 13px">
                                            Availability for <strong>
                                                <asp:Label ID="lblAvail" runat="server" Text=""></asp:Label></strong> on <strong>
                                                    <asp:Label ID="lblDate" runat="server" Text=""></asp:Label></strong></label>
                                    </td>
                                    <td height="20" valign="top" align="center" style="width:400px;">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Excursion Date :
                                                    </td>
                                                    <td>
                                                        <asp:TextBox Width="90px" ID="txtDepDate" runat="server" CssClass="form-control"
                                                            Text="DD/MM/YYYY"></asp:TextBox>
                                                    </td>
                                                    <td valign="bottom">
                                                        <a href="javascript:void(null)" onclick="showCalendar1()" />
                                                        <img src="images/call-cozmo.png"></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right" style="width:200px;"> 
                                        <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                            <tr>
                                                <td width="11%">
                                                    <asp:Image ID="Image1" ImageUrl="images/error.gif" Width="29" Height="24" Visible="false"
                                                        runat="server" />
                                                </td>
                                                <td width="89%">
                                                    <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="#9a0406" Text="Selected tour is not available on the specified dates. Please change your search
                                                                            parameters."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                           
                        </td>
                       
                    </tr>
                    
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <h4>
                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                :
                                <asp:Label ID="lblDays" runat="server" Text=""></asp:Label>
                                &nbsp;&nbsp;
                                <%if (activity.TransactionHeader != null && activity.TransactionHeader.Rows.Count > 0)
                                  { %>
                            (Adults:<%=activity.TransactionHeader.Rows[0]["Adult"]%>,
                            Childs:<%=activity.TransactionHeader.Rows[0]["Child"]%>,
                            Infants:<%=activity.TransactionHeader.Rows[0]["Infant"]%>)
                            <%} %>
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <div style=" font-size:16px; font-weight:bold">
                                Booking for
                                <asp:Label ID="lblActivity" runat="server" Text=""></asp:Label>
                                
                                </div>
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                         <div class="error_msg" style="display:none;width:100%;text-align:center;" id="errMessHotel"> </div>
                            <asp:DataList ID="DataList1" runat="server" Width="100%" OnItemDataBound="DataList1_ItemDataBound" DataKeyField="priceId">
                            <HeaderTemplate>
                            <table class="qty-tble" width="100%" border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td class="bggray">
                                        <strong> Categories</strong>
                                    </td>
                                    <td class="bggray">
                                       <strong>  Per Person</strong>
                                    </td>
                                     <td class="bggray">
                                        <label style="padding-right: 20px">
                                            
                                            <strong> Min Pax</strong>
                                            
                                            
                                            
                                            </label>
                                    </td>
                                     <td class="bggray">
                                    Total
                                    </td>
                                </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <table class="qty-tble" width="100%" border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td style="width:200px">
                                        <asp:Label ID="lblFlexLabel" runat="server" Text='<%#Eval("Label") %>'></asp:Label>                                        
                                    </td>
                                    <td style="width:180px">
                                        <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>&nbsp;<asp:Label ID="lblPrice" runat="server" Text='<%#CTCurrencyFormat(Eval("Price")) %>'></asp:Label></td>
                                    <td style="width:180px">
                                   
                                        <label>    
                                        <strong>X</strong> 
                                        <asp:DropDownList ID="ddlQty" CssClass="dropdown" runat="server" onchange="CalculateTotal();" Width="50px"></asp:DropDownList>                                                                               
                                            <%--<asp:TextBox ID="txtQty" CssClass="qnttxt" Text='<%#Eval("MinPax") %>' runat="server" onkeypress="return isNumber(event)" onkeyup="CalculateTotal();" onfocus="Check(this.id);" onBlur="Set(this.id);"></asp:TextBox>--%>
                                            <asp:HiddenField ID="hdnMinPax" runat="server" Value='<%#Eval("MinPax") %>' />
                                            </label>
                                    </td>
                                    <td style="width:120px">
                                        <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>&nbsp;<asp:Label ID="lblTotal" runat="server" Text='<%#CTCurrencyFormat(Eval("Amount")) %>'></asp:Label></td>
                                </tr>
                                
                            </table>
                            </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                     <tr height="30px">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="65%" align="right">
                                        <b> <label style="font-size:small;">Total: </label></b>
                                    </td>
                                    <td align="right" width="18%" style="font-size:small;"><b><%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>&nbsp;</b> </td>
                                    <td width="14%" align="left">
                                        <b><label id="lblSumTotal" style="font-size:small;"><%=Convert.ToDecimal(0).ToString("N"+CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue) %></label></b>
                                    </td>
                                    
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    
                </table>
            </div>
            
            
            <div style="padding-right: 10px" class=" f_R">
                                <%--<asp:ImageButton ID="imgSubmit" runat="server" ImageUrl="~/images/submit-gif.gif" OnClick="imgSubmit_Click" OnClientClick="return ValidateQty();" />--%>
                                
                                <asp:LinkButton ID="imgSubmit" runat="server" Font-Bold="true" CssClass="btn but_b"  Text="Submit" OnClick="imgSubmit_Click" OnClientClick="return ValidateQty();" />
                                
                                </div>
            <div class="clear"></div>
            
        </div>
       <script type="text/javascript">
          function isNumber(evt) {
              evt = (evt) ? evt : window.event;
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                  return false;
              }
              return true;
          }
      </script>
     
    <%--</form>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

