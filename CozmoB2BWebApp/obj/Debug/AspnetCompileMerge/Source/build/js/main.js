$(function(){
		
//	$('.main-navigation li').on('mouseover',function(){
//		$(this).find('>ul').show();	
//		$(this).find('>ul').addClass('bouncceIn ancimated')
//	}).on('mouseout',function(){
//		$(this).find('>ul').hide();				
//	})
//	
	  
 var screenWidth = window.innerWidth,
	 bodyHeight  = $('body').innerHeight();

	$('ul.dropdown_menu>li>ul').addClass('first-level');
	$('ul.dropdown_menu>li>ul.first-level ul').addClass('second-level');

	if(screenWidth < 992){			
		var menuBtn = $('ul.dropdown_menu>li a');			
		menuBtn.on('click',function(){			
		  if($(this).hasClass('hover')){			    
			 $(this).next('.first-level').hide().css('visibility', 'hidden').removeClass('activeMenu');  
			 $(this).next('.second-level').hide().css('visibility', 'hidden').removeClass('activeMenu');	
			 $(this).removeClass('hover'); 			  
		  }else{  			  
			 $(this).next('.first-level').show().css('visibility', 'visible').addClass('activeMenu');
			 $(this).next('.second-level').show().css('visibility', 'visible').addClass('activeMenu');	
			 $(this).addClass('hover');  			
		  }
		});  		
	}else{		
		 $("ul.dropdown_menu li").hover(function(){
			$(this).addClass("hover");
			$('ul:first',this).css('visibility', 'visible').addClass('activeMenu');
		}, function(){    
			$(this).removeClass("hover");
			$('ul:first',this).css('visibility', 'hidden').removeClass('activeMenu');
		});  		
	}
	if(bodyHeight < 770){	
		$('.main-navigation').addClass('adjust-menuheight');
	} 
    
    $("ul.dropdown_menu li ul li:has(ul)").find("a:first").addClass('hasdropdown');
    
//	var myAccountMenu = $('.my-account-menu').clone(true,true);
//	myAccountMenu.insertBefore('.dropdown_menu');
	
   
	
	if(screenWidth < 768){
		$('select').select2('destroy');
	}
	
	$('.swap-airports').on('click',function(){		
		var org = $('.origin-airport').val(),
			dest = $('.dest-airport').val();	
		$('.origin-airport').val(dest);
		$('.dest-airport').val(org);
		
		
	}) 
	
	
	 
	//BS4  - converting col-xs to col
	//$('[class^="col-xs"]').each(function(){
		//console.log('asdas : ' + $(this).attr('class'))
		//var currentClass = $(this).attr('class');
		//console.log(currentClass);
		//var replacedClass = currentClass.addClass('col');
	//	$(this).addClass('col');
		//console.log(replacedClass);
	//})

	
	//Custom DropDown
	var dropDownBtn = $('[data-dropdown]')

	$('[data-dropdown]').on('click',function(event){
		 event.preventDefault();
		var ddContent = $(this).data('dropdown');	
		$(ddContent).addClass('fadeIn animated faster');
		$(ddContent).removeClass('d-none');
	})
	$('.dd-done-btn').click(function(e){		  
		e.preventDefault();
		var ddContent = $(this).closest('.dropdown-content');
		ddContent.addClass('d-none');
		ddContent.removeClass('fadeIn animated faster');
	})
	
	var mainTitle = $('.title-style span').text().trim().length
	if(!mainTitle){
		$('.title-style').hide();
	}
    
//  $('#ctl00_cphTransaction_chkSuppliers input').change(function(){	 
//	
//	  $('#ctl00_cphTransaction_chkSuppliers input:checked').each(function(){
//		var content =    $(this).next('label').text();
//		  $('#selectedSuppliers').append(content);
//		 console.log('item' + $(this).next('label').text());  
//	  })
//  })
	  
//    $('#ctl00_cphTransaction_chkSuppliers input').each(function(){
//		$('')
//	})
})

$(document).mouseup(function (e) {
    var ddContent = $('.dropdown-content');

       if (!ddContent.is(e.target) && ddContent.has(e.target).length === 0) {
          ddContent.addClass('d-none');
		  ddContent.removeClass('fadeIn animated faster');
       }

});

//Remove on red border from input fields
$(document).on('blur','.form-text-error',function(){
	$(this).removeClass('form-text-error');		
})
$(document).on('blur','.form-border-color',function(){
	$(this).removeClass('form-border-color');
})
