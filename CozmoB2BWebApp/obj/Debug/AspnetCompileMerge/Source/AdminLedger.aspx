﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AdminLeadger" Title="Ledger Transaction" Codebehind="AdminLedger.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.AccountingEngine" %>
<%@ Import Namespace="CT.GlobalVisa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="js/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />

    <script type="text/javascript">
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            init();
              $('container2').context.styleSheets[0].display = "none";
              $('container1').context.styleSheets[0].display = "block";
              cal1.show();
              cal2.hide();
            document.getElementById('container1').style.display = "block";
             
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
             
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);



        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
    </script>
 
 <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left:10%; display: none; z-index:9999;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left:43%; z-index:9999; display: none;">
        </div>
    </div>
    <div>
    <asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
          <tr>
              <td style="width: 700px" align="left">
                  <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                      onclick="return ShowHide('divParam');">Hide Parameter</a>
              </td>
          </tr>
      </table>
      <div class="paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
        
        
        
              <div class="col-md-12 padding-0 marbot_10">                                    

  
    <div class="col-md-2">  From Date:</div>
    <div class="col-md-2"> <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="CheckIn" runat="server" Width="110px" CssClass="form-control"></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showCal1()">
                                                    <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table></div>
  
    <div class="col-md-2"> To Date:</div>
    <div class="col-md-2"> <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="CheckOut" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <a href="javascript:void(null)" onclick="showCal2()">
                                                    <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table></div>
  
    <div class="col-md-2">  Agent:</div>
    <div class="col-md-2"> 
    <asp:DropDownList ID="ddlAgents" CssClass="form-control" runat="server"  AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlAgents_SelectionChanged">
                                    </asp:DropDownList>
    
    </div>

    <div class="clearfix"></div>
    </div>
    
    
          <div class="col-md-12 padding-0 marbot_10">                                    

  
    <div class="col-md-2"><asp:Label ID="lblB2BAgent" runat="server" Visible="true" Text="B2BAgent:"></asp:Label> </div>
    <div class="col-md-2"><asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control" 
                        OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList> </div>
  
    <div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" runat="server" Visible="true" Text="B2B2BAgent:"></asp:Label></div>
    <div class="col-md-2"> <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control">
                    </asp:DropDownList></div>
  
    <div class="col-md-2"><asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label> </div>
    
    <div class="col-md-2"><asp:DropDownList ID="ddlTransType" runat="server" CssClass="form-control" Visible="false">
                                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                                    <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                                    <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                                </asp:DropDownList> </div>

    <div class="clearfix"></div>
    </div>
    
    
          <div class="col-md-12 padding-0 marbot_10">                                    
<div class="col-md-2"><asp:Label ID="lblPaymentType" runat="server" Text="Account Type:"></asp:Label> </div>
    
    <div class="col-md-2"><asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="OnAccount" Text="OnAccount" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Card" Text="Card"></asp:ListItem>
                                </asp:DropDownList> </div>
  <asp:Button CssClass="btn but_b pull-right" ID="btnSubmit" runat="server" Text="Search" OnClick="btnSubmit_Click" />
<div class="clearfix"></div>
    </div>
    
    
    
        
        
        
        
        
        </asp:Panel>
        </div>
        
        <div id="ExportDiv" runat="server">
         <div class="table-responsive col-md-12 padding-0 margin-top-10"> 
            <table class="table bg_white" border="1">
                  <tr class="themecol1">   
                   
                            <td class="padding-left-10 bold">
                             <span style="color:White;">   Agent Name </span></td>                                
                            <td class="bold  padding-left-10 "> 
                               <span style="color:White;">   Ref #</span></td>
                            <td class="bold  padding-left-10 "> 
                               <span style="color:White;">   Particulars</span></td>                            
                            <td class="bold left  text-right padding-right-10 ">
                               <span style="color:White;">   Debit</span></td>
                            <td class="bold left  text-right padding-right-10 ">
                               <span style="color:White;">   Credit</span></td>
                        </tr>
                        <%try
                            {
                                csvLedger.Append("Date,Ref# ,Particulars,Debit,Credit\n\n"); %> <%--running total Removed brahmam--%>
                        <%if (ledgerAdmin.Count > 0)
                            {
                                int agencyId = ledgerAdmin[0].AgencyId;
                                DateTime date = ledgerAdmin[0].CreatedOn;
                                decimal totalDebit = 0;
                                decimal totalCredit = 0;
                                decimal balanceTillDate = 0;
                                string invoiceNumber = string.Empty;
                                int referenceId = 0;
                                decimal ledgerTotal = 0;
                                string tempinvoiceNumber = string.Empty;
                                int ticketCounter = 1;
                                string counterString = string.Empty;
                                int decimalValue = 2;
                                 
                                for (int i = 0; i < ledgerAdmin.Count; i++)
                                {
                                    string debit = string.Empty;
                                    string credit = string.Empty;
                                    ticketCounter = ledgerAdmin[i].PaxCount;
                                    ledgerTotal = ledgerAdmin[i].Amount;
                                    decimalValue = ledgerAdmin[i].AgentDecimal;
                                    counterString = string.Empty;
                                    invoiceNumber = string.Empty;
                                    invoiceNumber = ledgerAdmin[i].InvoiceNumber;
                                    referenceId =Convert.ToInt32(ledgerAdmin[i].ReferenceType);
                                    
                                    
                                    //Flight Tickets combined
                                    if (ledgerAdmin[i].ReferenceType == ReferenceType.TicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.OfflineTicket || ledgerAdmin[i].ReferenceType == ReferenceType.InvoiceEdited || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.TicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.TicketVoid || ledgerAdmin[i].ReferenceType == ReferenceType.TicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketModification || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketVoid)
                                    {
                                        ticketCounter = ledgerAdmin.Where(p => p.InvoiceNumber ==  invoiceNumber && (int)p.ReferenceType==referenceId).Count();
                                        ledgerTotal = ledgerAdmin.Where(p => p.InvoiceNumber == invoiceNumber && (int)p.ReferenceType==referenceId).Sum(s => s.Amount);
                                    }
                                    if (ledgerAdmin[i].ReferenceType == ReferenceType.HotelBooked  || ledgerAdmin[i].ReferenceType == ReferenceType.TicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.OfflineTicket || ledgerAdmin[i].ReferenceType == ReferenceType.InsuranceCreated || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.TicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.TicketVoid || ledgerAdmin[i].ReferenceType == ReferenceType.TicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketModification || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketVoid)
                                    {
                                        counterString = " X " + ticketCounter.ToString();
                                    }
                                    int sourceId = 0;
                                    if (ledgerAdmin[i].ReferenceType == ReferenceType.TicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.OfflineTicket || ledgerAdmin[i].ReferenceType == ReferenceType.TicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.TicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.TicketVoid || ledgerAdmin[i].ReferenceType == ReferenceType.TicketModification || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketModification || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketVoid)
                                    {
                                        sourceId = ledgerAdmin[i].FlightSource;
                                    }
                                    if (ledgerTotal > 0)
                                    {
                                        if (sourceId == (int)BookingSource.TBOAir)
                                        {
                                            credit = Math.Ceiling(ledgerTotal).ToString("N" + decimalValue);
                                        }
                                        else
                                        {
                                            credit = ledgerTotal.ToString("N" + decimalValue);
                                        }
                                        debit = "-";
                                    }
                                    else
                                    {
                                        if (sourceId == (int)BookingSource.TBOAir)
                                        {
                                            debit = Math.Ceiling(-ledgerTotal).ToString("N" + decimalValue);
                                        }
                                        else
                                        {
                                            debit = (-ledgerTotal).ToString("N" + decimalValue);
                                        }
                                        credit = "-";
                                    }
                                    if (agencyId != ledgerAdmin[i].AgencyId || i == 0)
                                    {
                                        agencyId = ledgerAdmin[i].AgencyId;
                                      %>
                                      <%if (i != 0)
                                          {%>
                                          <tr class="height-25 width-100 light-gray-back">
                                        <td colspan="3" class="bold padding-right-10  ">
                                            Total</td>
                                        <td class="bold padding-right-10  ">
                                            <%=totalDebit.ToString("N" + decimalValue)%>
                                        </td>
                                        <td class="bold padding-right-10  ">
                                            <%=totalCredit.ToString("N" + decimalValue)%>
                                        </td>
                           </tr> 
                           <%csvLedger.Append(",,Total," + totalDebit.ToString("N" + decimalValue) + "," + totalCredit.ToString("N" + decimalValue) + "\n\n"); %>
                            <tr class="height-25 wid th-100 light-gray-back">
                            <td colspan="4" class="bold " style="border-right:0px">
                                Closing Balance: &nbsp;
                                   <%csvLedger.Append("\n,,Closing Balance:,"); %></td>
                                    
                            <td class="bold padding-right-10  " style="border-left:0px">
                                 <%=(-(balanceTillDate + totalDebit - totalCredit)).ToString("N" + decimalValue)%>
                                 <%csvLedger.Append("," + (-(balanceTillDate + totalDebit - totalCredit)).ToString("N" + decimalValue)); %>
                            </td>
                        </tr>
                           <%
                               }
                               totalCredit = 0;
                               totalDebit = 0; %>
                             <tr>
                             <td><%=ledgerAdmin[i].AgentName%></td>
                                <td colspan="2" class="bold padding-left-10 " style="border-right:0px;">
                                Opening Balance (All values are in <%=ledgerAdmin[i].AgentCurrency %>)
                                </td>
                                  <%csvLedger.Append("\n" + ledgerAdmin[i].AgentName + "  "); %>
                                <%csvLedger.Append(",,Opening Balance,"); %>
                                <td class="bold padding-right-10" style="border-left:0px;">
                                <%balanceTillDate =ledgerAdmin[i].BalanceTillDate;
                                    if (ddlPaymentType.SelectedItem.Value == "OnAccount")
                                    {
                                        balanceTillDate += -ledgerAdmin[i].AgentCurrentBalance;
                                    }
                                    if (balanceTillDate < 0)
                                    { %>
                                <%=(-balanceTillDate).ToString("N" + decimalValue)%>
                                <%
                                        csvLedger.Append((-balanceTillDate).ToString("N" + decimalValue).Replace(",", "") + ",0\n\n");
                                    }
                                    else
                                    {%>
                                <img alt="Spacer" src="Images/spacer.gif" />
                                <%} %></td>
                                <td class="bold padding-right-10 "  >
                                <%
                                    if (balanceTillDate >= 0)
                                    { %>
                                <%=balanceTillDate.ToString("N" + decimalValue)%>
                                <%
                                        csvLedger.Append("0," + balanceTillDate.ToString("N" + decimalValue).Replace(",", "") + "\n\n");
                                    }
                                    else
                                    {%>
                            <img alt="Spacer" src="Images/spacer.gif" />
                            <%} %>
                        </td>
                    </tr>
                    <%}
                    %>
                    <%  else
                        {
                        } %>
                    <%
                        if (ledgerAdmin[i].TransType == "B2B")
                        {
                            if (ledgerTotal > 0)
                            {
                                totalCredit += Math.Round(ledgerTotal, decimalValue);
                            }
                            else
                            {
                                totalDebit += -Math.Round(ledgerTotal, decimalValue);
                            }
                        }

                                        %>
                                        <tr class="height-25 width-100">
                                <td class="padding-left-10 bold locked-pnr-booking-width  ">
                                    <%=UTCtoISTtimeZoneConverter(ledgerAdmin[i].CreatedOn.ToString()).ToString("dd-MM-yyyy")%>
                        <%csvLedger.Append(UTCtoISTtimeZoneConverter(ledgerAdmin[i].CreatedOn.ToString()).ToString("dd-MM-yyyy") + ","); %>
                                </td>  
                                <td class="date-of-birth width-140  padding-left-10 ">
                                <%if (ledgerAdmin[i].ReferenceType == ReferenceType.PaymentRecieved || ledgerAdmin[i].ReferenceType == ReferenceType.PaymentRecieved || ledgerAdmin[i].ReferenceType == ReferenceType.PaymentReversed || ledgerAdmin[i].ReferenceType == ReferenceType.HotelBooked || ledgerAdmin[i].ReferenceType == ReferenceType.HotelCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.HotelRefund || ledgerAdmin[i].ReferenceType == ReferenceType.TicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.TicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.TicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.TicketVoid || ledgerAdmin[i].ReferenceType == ReferenceType.TicketModification || ledgerAdmin[i].ReferenceType == ReferenceType.SightSeeingRefund || ledgerAdmin[i].ReferenceType == ReferenceType.SightseeingCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.SightseeingBooked || ledgerAdmin[i].ReferenceType == ReferenceType.CardHotelRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardHotelCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCreated || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketModification || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CardTicketVoid || ledgerAdmin[i].ReferenceType == ReferenceType.CarBooked || ledgerAdmin[i].ReferenceType == ReferenceType.CarRefund || ledgerAdmin[i].ReferenceType == ReferenceType.CarCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.InsuranceCancellationCharge || ledgerAdmin[i].ReferenceType == ReferenceType.InsuranceRefund || ledgerAdmin[i].ReferenceType == ReferenceType.PackageBooked || ledgerAdmin[i].ReferenceType == ReferenceType.PackageCancellationCharges || ledgerAdmin[i].ReferenceType == ReferenceType.PackageRefund || ledgerAdmin[i].ReferenceType == ReferenceType.GlobalVisaBooked)
                                    {
                                        csvLedger.Append(invoiceNumber + ",");
                                      %>
                                    <%=invoiceNumber%>
                                   
                                 <%}
                                     else
                                     {
                                         if (ledgerAdmin[i].Narration.DocNo != "")%>
                                             <%{%>
                                                 <%=ledgerAdmin[i].Narration.DocNo%>
                                                 <%csvLedger.Append(ledgerAdmin[i].Narration.DocNo.Replace(',', ' ') + ","); %>
                                             <%}
                                                 else%>
                                         <%if (ledgerAdmin[i].CheckNo != "0")
                                             {%>
                                            <%=ledgerAdmin[i].CheckNo%>
                                            <%csvLedger.Append(ledgerAdmin[i].CheckNo + ","); %>
                                          <%}%>
                                         <%else
                                             { %>
                                                -
                                                <%csvLedger.Append("-,"); %>
                                             <%} %>
                                    <%} %>
                                </td>
                                <td class="width-800  " style="padding-left:10px;">
                                   <%if (ledgerAdmin[i].ReferenceType == ReferenceType.PaymentReversed)
                                       {%>
                                     
                                      <b><%=ledgerAdmin[i].Narration.Remarks%>: </b><%=ledgerAdmin[i].Notes%><%=ledgerAdmin[i].Narration.PaxName%>                       
                                    <%
                                            csvLedger.Append(ledgerAdmin[i].Narration.Remarks.Replace(',', ' ').Replace('\r', ' ').Replace('\n', ' ') + ": " + ledgerAdmin[i].Notes.Replace(',', ' ').Replace('\r', ' ').Replace('\n', ' ') + ledgerAdmin[i].Narration.PaxName.Replace(',', ' ') + ",");
                                        }%>
                                    <%else
                                        {%>
                                     <b><%=ledgerAdmin[i].Narration.Remarks.Replace('@', '|')%>: </b>
                                        <%=ledgerAdmin[i].Narration.PaxName%> <%=counterString%> <%=ledgerAdmin[i].Notes%>
                <%
                        csvLedger.Append(ledgerAdmin[i].Narration.Remarks.Replace(',', ' ').Replace('\r', ' ').Replace('\n', ' ') + ": " + ledgerAdmin[i].Narration.PaxName.Replace(',', ' ') + counterString + ledgerAdmin[i].Notes.Replace(',', ' ').Replace('\r', ' ').Replace('\n', ' ') + ",");

                    }%></td>                  
                                <td class="padding-right-10">
                                    <%=debit%></td>                                    
                                <td class="padding-right-10 ">
                                    <%=credit%> </td> 
                            </tr>
                             <% 
                                 decimal runningBalance = 0;
                                 runningBalance = balanceTillDate + totalDebit - totalCredit;
                                 csvLedger.Append(debit.Replace('-', '0').Replace(",", "") + "," + credit.Replace('-', '0').Replace(",", "") + "\n"); //Running Total Commented
                                                                                                                                                      //csvLedger.Append(debit.Replace('-', '0').Replace(",", "") + "," + credit.Replace('-', '0').Replace(",", "") + "," + (ledgerAdmin[i].RunningTotal + tempZiya).ToString("N" + decimalValue).Replace('-', '0').Replace(",", "") + "\n");
                            %>
                                    
                        <%if (ledgerAdmin[i].Narration.Sector != "") %>
                        <%{ %>
                        <tr class="height-25">
                            <td class="padding-left-10 bold"></td>
                            <td class="bold  padding-left-10"></td>
                            <td class=" padding-left-10">
                        <b><%=ledgerAdmin[i].Narration.Sector%> </b>  TICKET NO.: <%=ledgerAdmin[i].Narration.TicketNo%></td>                          
                        <%csvLedger.Append(",," + ledgerAdmin[i].Narration.Sector + "  TICKET NO.: " + ledgerAdmin[i].Narration.TicketNo + ",,\n"); %>                              
                            <td class="bold left  text-right padding-right-10"></td>
                            <td class="bold left  text-right padding-right-10"></td>
                            <%--<td class="bold left  text-right padding-right-10 " width="10%"></td>--%>
                        </tr>
                        <%}
                            else if (ledgerAdmin[i].Narration.HotelConfirmationNo != "") %>
                        <%{ %>
                        <tr class="height-25">
                            <td class="padding-left-10 bold"></td>
                            <td class="bold  padding-left-10"></td>
                            <td class=" padding-left-10">
                        <b><%=ledgerAdmin[i].Narration.DestinationCity%> </b>  CONFIRMATION NO.: <%=ledgerAdmin[i].Narration.HotelConfirmationNo.Replace('@', '|') %></td>                         
                        <%csvLedger.Append(",," + ledgerAdmin[i].Narration.DestinationCity + "  CONFIRMATION NO.: " + ledgerAdmin[i].Narration.HotelConfirmationNo.Replace('@', '|') + ",,\n");%>
                            <td class="bold left  text-right padding-right-10"></td>
                            <td class="bold left  text-right padding-right-10"></td>
                        </tr>
                        <%}
                            else if (ledgerAdmin[i].Narration.PolicyNo != "") %>
                        <%{ %>
                        <tr class="height-25">
                            <td class="padding-left-10 bold"></td>
                            <td class="bold  padding-left-10"></td>
                            <td class=" padding-left-10">
                        <b>POLICY NO.: <%=ledgerAdmin[i].Narration.PolicyNo%></b></td>
                        <%csvLedger.Append(",,POLICY NO.: " + ledgerAdmin[i].Narration.PolicyNo + ",,\n");%>
                            <td class="bold left  text-right padding-right-10"></td>
                            <td class="bold left  text-right padding-right-10"></td>
                        </tr>
                        <%} %>
                        <%if (ledgerAdmin[i].Narration.TravelDate != "") %>
                        <%{
                                string[] travelDate = (ledgerAdmin[i].Narration.TravelDate.Contains("/") ? ledgerAdmin[i].Narration.TravelDate.Split('/') : ledgerAdmin[i].Narration.TravelDate.Split('-')); %><tr class="height-25">
                            <td class="padding-left-10 bold"></td>
                            <td class="bold  padding-left-10"></td>
                            <td class=" padding-left-10"><%if (ledgerAdmin[i].Narration.FlightNo != "")
                                                                          { %> 
                        TRAVEL DATE : <%=travelDate[1] + "/" + travelDate[0] + "/" + travelDate[2]%>  
                                                                                   BY : <%=ledgerAdmin[i].Narration.FlightNo%>
                    <%csvLedger.Append(",,TRAVEL DATE : " + travelDate[1] + "/" + travelDate[0] + "/" + travelDate[2] + "  BY : " + ledgerAdmin[i].Narration.FlightNo + ",,\n");%>
                    <%}
                        else
                        {
                            if (ledgerAdmin[i].ReferenceType != ReferenceType.MobileRecharge)
                            {%>
                                                                           <% }
                                                                               else
                                                                               {%>
                          RECHARGE DATE : <%=travelDate[1] + "/" + travelDate[0] + "/" + travelDate[2]%>
                    <%csvLedger.Append(",, RECHARGE DATE : " + travelDate[1] + "/" + travelDate[0] + "/" + travelDate[2] + ",,\n");%>
                    <%}
                        }%></td>       
              
                            <td class="bold left  text-right padding-right-10"></td>
                            <td class="bold left  text-right padding-right-10"></td>
                        </tr>
                        <%} %>
                        <%if (ledgerAdmin[i].Narration.ChequeNo != "") %>
                        <%{ %>
                        <tr class="height-25">
                            <td class="padding-left-10 bold"></td>
                            <td class="bold  padding-left-10"></td>
                            <td class=" padding-left-10">
                        <b><%=ledgerAdmin[i].Narration.PaymentMode%> No.: </b> <%=ledgerAdmin[i].Narration.ChequeNo%></td>  
                 <%csvLedger.Append(",," + ledgerAdmin[i].Narration.PaymentMode + " No.: " + ledgerAdmin[i].Narration.ChequeNo + ",,\n");%>
                            <td class="bold left  text-right padding-right-10"></td>
                        </tr>
                        <%} %>
                        <tr class="height-25">
                            <td class="padding-left-10 bold"></td>
                            <td class="bold  padding-left-10"></td>
                            <td class=" padding-left-10"></td>
                            <td class="bold left  text-right padding-right-10"></td>
                            <td class="bold left  text-right padding-right-10"></td>
                        </tr>
                        <%csvLedger.Append("\n"); %>
                        <%
                                if (ledgerAdmin[i].ReferenceType == ReferenceType.HotelBooked)
                                {
                                    i = i++;
                                }
                                else
                                {
                                    i = i + ticketCounter - 1;
                                }
                            } %>
                        <tr class="height-25 wid th-100 light-gray-back">
                            <td colspan="3" class="bold " style="border-right:0px">
                                Total &nbsp;
                                 <%csvLedger.Append("\n,,Total:,"); %></td>

                            <td class="bold padding-right-10  " style="border-left:0px">
                                <%=totalDebit.ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                 <%csvLedger.Append((totalDebit).ToString("N" + Settings.LoginInfo.DecimalValue).Replace(",", "")); %>
                            </td>
                            <td class="bold padding-right-10  ">
                                <%=totalCredit.ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                   <%csvLedger.Append("," + (totalCredit).ToString("N" + Settings.LoginInfo.DecimalValue).Replace(",", "")); %>
                            </td>
                           
                        </tr>
                        <tr class="height-25 wid th-100 light-gray-back">
                            <td colspan="4" class="bold " style="border-right:0px">
                                Closing Balance: &nbsp;
                                   <%csvLedger.Append("\n,,Closing Balance:,"); %></td>
                                    
                            <td class="bold padding-right-10  " style="border-left:0px">
                                 <%=(-(balanceTillDate + totalDebit - totalCredit)).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                 <%csvLedger.Append("," + (-(balanceTillDate + totalDebit - totalCredit)).ToString("N" + Settings.LoginInfo.DecimalValue).Replace(",", "")); %>
                            </td>
                        </tr>
                        <%}%>
                        <% Session["csvLedger"] = csvLedger;
                            }
                            catch (Exception ex)
                            {
                                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                            }%>
                    </table>
        
        <div class="clearfix"> </div>       
 </div>
               
                </div>
                 
                 <div>    <asp:Button runat="server" ID="ExportToExcelButton" OnClick="ExportToExcelButton_Click"
            Text="Export To Excel" /></div>  
          
        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

