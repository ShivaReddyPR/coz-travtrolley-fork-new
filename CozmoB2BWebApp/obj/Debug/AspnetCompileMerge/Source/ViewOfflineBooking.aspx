﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ViewOfflineBooking.aspx.cs" Inherits="CozmoB2BWebApp.ViewOfflineBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<style>@media only screen {
  html {
    min-height: 100%;
    background: #f3f3f3;
  }
}

@media only screen and (max-width: 716px) {
  .small-float-center {
    margin: 0 auto !important;
    float: none !important;
    text-align: center !important;
  }
}

@media only screen and (max-width: 716px) {
  table.body img {
    width: auto;
    height: auto;
  }

  table.body center {
    min-width: 0 !important;
  }

  table.body .emailcontainer {
    width: 95% !important;
  }

  table.body .columns {
    height: auto !important;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding-left: 16px !important;
    padding-right: 16px !important;
  }

  table.body .columns .columns {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  table.body .collapse .columns {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  th.small-6 {
    display: inline-block !important;
    width: 50% !important;
  }

  th.small-12 {
    display: inline-block !important;
    width: 100% !important;
  }

  .columns th.small-12 {
    display: block !important;
    width: 100% !important;
  }

  table.menu {
    width: 100% !important;
  }

  table.menu td,
  table.menu th {
    width: auto !important;
    display: inline-block !important;
  }

  table.menu.vertical td,
  table.menu.vertical th {
    display: block !important;
  }

  table.menu[align="center"] {
    width: auto !important;
  }
}</style>
    
<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        sRefKey = document.getElementById('<%=hdnRefNo.ClientID %>').value != '' ? document.getElementById('<%=hdnRefNo.ClientID %>').value : '';        
        GetDetails();
    });

    function GetDetails() {

        try {
            
            details = AjaxCall('ViewOfflineBooking.aspx/GetBookingDetails', "{'sRefKey':'" + sRefKey + "'}");
            if (details.length > 1) {

                FltDtls = JSON.parse(details[0]);
                HtlDtls = JSON.parse(details[1]);
                if (FltDtls.Table.length > 0)
                    BindSegmentInfo();
                if (HtlDtls.Table.length > 0)
                    BindHotelInfo(HtlDtls.Table[0]);

                BindHDRInfo(FltDtls.Table.length > 0 ? FltDtls.Table[0]: HtlDtls.Table[0]);
                BindPaxInfo((FltDtls.Table.length > 0 ? FltDtls.Table2 : HtlDtls.Table1), (FltDtls.Table.length > 0 ? FltDtls.Table3 : HtlDtls.Table2));
                document.getElementById('tblFltInfo').style.display = FltDtls.Table.length > 0 ? 'table' : 'none';
                document.getElementById('tblHotelInfo').style.display = HtlDtls.Table.length > 0 ? 'table' : 'none';
            }    
        }
        catch(exception)
        {
            Showalert('divAlert', 'Alert', 'Failed to load the details, please contact cozmo.', '');
        }    
    }

    function BindHDRInfo(data) {

        document.getElementById('spnTripreason').innerHTML = data.TravelReason;
        document.getElementById('spnBookRefNo').innerHTML = data.BookingRefNo;
        document.getElementById('spnBookdt').innerHTML = data.CreatedDate;
        document.getElementById('spncuser').innerHTML = document.getElementById('spnBookBy').innerHTML = data.CreatedName;
        document.getElementById('ancBookEmail').innerHTML = data.AgentEmail;
        //document.getElementById('ancBookEmail').href = 'mailto:' + FltDtls.Table[0].AgentEmail;
        $("ancBookEmail").prop("href", "mailto:"  + data.AgentEmail)
    }

    function BindHotelInfo(data) {

        document.getElementById('spnHotcidt').innerHTML = data.ohbciday + ', ' + data.ohbcidate;
        document.getElementById('spnHotCity').innerHTML = data.ohbcity;
        document.getElementById('spnHotcodt').innerHTML = data.ohbcoday + ', ' + data.ohbcodate;
        document.getElementById('spnHotcntry').innerHTML = data.ohbcountry;
        document.getElementById('spnHotapname').innerHTML = data.ohbapname;
        document.getElementById('spnHotspreq').innerHTML = data.ohbspreq;
        document.getElementById('spnHotadinfo').innerHTML = data.ohbadinfo;
    }

    function BindSegmentInfo() {

        var segmnts = ''; 
        $.each(FltDtls.Table1, function (key, col) {

            if (key == 0) {

                document.getElementById('spnFltDepDt').innerHTML = col.ofbdepday + ', ' + col.ofbdepdate;
                document.getElementById('spnFltcabcls').innerHTML = col.ofbcabinclass;
                document.getElementById('spnFltdepap').innerHTML = col.ofbdepairport + (col.ofbdeptime != null && col.ofbdeptime != '' ? ', ' + col.ofbdeptime : '');
                document.getElementById('spnFltarrap').innerHTML = col.ofbarrairport;
            }
            else {

                segmnts += Seghtml.replace(/@ofbdepdays, @ofbdepdates/g, col.ofbdepday + ', ' + col.ofbdepdate).replace(/@ofbcabinclasses/g, col.ofbcabinclass)
                    .replace(/@ofbdepairports, @ofbdeptimes/g, col.ofbdepairport + (col.ofbdeptime != null && col.ofbdeptime != '' ? ', ' + col.ofbdeptime : ''))
                    .replace(/@ofbarrairports/g, col.ofbarrairport);
            }
        });
        document.getElementById('divSegments').innerHTML = segmnts != '' ? segmnts : '';
    }

    function BindPaxInfo(paxdtls, flexdtls) {

        var paxinfo = ''; 
        $.each(paxdtls, function (key, col) {
            
            paxinfo += Paxhtml.replace(/@opinames/g, col.opiname).replace(/@opidobs/g, col.opidob).replace(/@opippnos/g, col.opippno)
                .replace(/@opippexps/g, col.opippexp).replace(/@opipois/g, col.opipoi).replace(/@opiemails/g, col.opiemail).replace(/@opiaddresses/g, col.opiaddress);            
        });
        document.getElementById('divPaxInfo').innerHTML = paxinfo != '' ? paxinfo : '';
        BindFlexInfo(flexdtls);
    }

    function BindFlexInfo(flexdtls) {

        var flexdata = '';
        if (flexdtls.length > 0) {

            $.each(flexdtls, function (key, col) {

                flexdata += Flexdtlhtml.replace(/@ofbflexname/g, col.FlexLabel).replace(/@ofbflexval/g, col.FlexData);
            });            
        }        
        document.getElementById('divFlexInfo').innerHTML = flexdata != '' ? Flexhdrhtml + flexdata : '';
    }

    /* Global variables */
    var sRefKey = ''; var details; var FltDtls; var HtlDtls;

    var Seghtml = '<table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; ' +
        'padding: 0; text-align: left; vertical-align: top">' +
        '<tbody><tr style="padding: 0; text-align: left; vertical-align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; ' +
        'border - collapse: collapse !important; color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; ' +
        'line - height: 1.3; margin: 0; padding: 2px 7px; text - align: left; vertical - align: top; word - wrap: break-word"> <strong style="font-weight: 600"> ' +
        '@ofbdepdays, @ofbdepdates </strong> </td> </tr> <tr style="padding: 0; text-align: left; vertical-align: top"> <td colspan="2" style="-moz-hyphens: auto; ' +
        '- webkit - hyphens: auto; Margin: 0; border - collapse: collapse!important; color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; ' +
        'font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; text - align: left; vertical - align: top; word - wrap: break-word"> ' +
        '<hr size="1" style="margin: 5px 0;border-color: #ccc"> </td> </tr> </tbody> </table> <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" ' +
        'style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top" > <tbody> <tr style="padding: 0; text-align: left; ' +
        'vertical - align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; ' +
        'font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; text - align: left;' +
        'vertical - align: top; word - wrap: break-word">  <strong style="font - weight: 600">Cabin Type: @ofbcabinclasses</strong> </td> </tr> <tr style="padding: 0; ' +
        'text - align: left; vertical - align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; ' +
        'font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; text - align: left; ' +
        'vertical - align: top; word - wrap: break-word">  Departure <strong style="font - weight: 600">@ofbdepairports, @ofbdeptimes</strong> </td> </tr>' +
        '<tr style="padding: 0; text-align: left; vertical-align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; ' +
        'color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; ' +
        'text - align: left; vertical - align: top; word - wrap: break-word">  Arrival <strong style="font - weight: 600">@ofbarrairports</strong> </td> </tr> ' +
        '<tr style="padding: 0; text-align: left; vertical-align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; ' +
        'color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; ' +
        'text - align: left; vertical - align: top; word - wrap: break-word"><strong style="font - weight: 600"></strong>  </td> </tr> ' +
        '<tr style="padding: 0; text-align: left; vertical-align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; ' +
        'color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; ' +
        'text - align: left; vertical - align: top; word - wrap: break-word"><strong style="font - weight: 600"></strong> </td> </tr> ' +
        '<tr style="padding: 0; text-align: left; vertical-align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; ' +
        'color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; ' +
        'text - align: left; vertical - align: top; word - wrap: break-word"><strong style="font - weight: 600"></strong>  </td> </tr> ' +
        '<tr style="padding: 0; text-align: left; vertical-align: top"> <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; ' +
        'color: #0a0a0a; font - family: Arial, sans - serif; font - size: 12px; font - weight: normal; hyphens: auto; line - height: 1.3; margin: 0; padding: 2px 7px; ' +
        'text - align: left; vertical - align: top; word - wrap: break-word"><strong style="font - weight: 600"></strong>  </td></tr> </tbody></table>';

    var Paxhtml = '<table width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">' +
            	'<tbody>' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">' +
            				'<table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">' +
            					'<tbody>' +
            						'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            							'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600">Traveler: @opinames</strong>' +
            							'</td>' +
            						'</tr>' +
            					'</tbody>' +
            				'</table>' +
            			'</td>' +
            		'</tr>' +
            	'</tbody>' +
            '</table>' +
            '<table class="table-content-area" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">' +
            	'<tbody>' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Date Of Birth: <strong>@opidobs</strong>' +
            			'</td>' +
            		'</tr>	' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Passport No: <strong>@opippnos</strong>' +
            			'</td>' +
            		'</tr>	' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Passport Expiry: <strong>@opippexps</strong>' +
            			'</td>' +
            		'</tr>	' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Place of Issue: <strong>@opipois</strong>' +
            			'</td>' +
            		'</tr>	' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Email: <strong>@opiemails</strong>' +
            			'</td>' +
            		'</tr>	' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family:  Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Address: <strong>@opiaddresses</strong>' +
            			'</td>' +
            		'</tr>' +
            	'</tbody>' +
        '</table>';

    var Flexhdrhtml = '<table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">' +
            	'<tbody>' +
            		'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">' +
            				'<table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">' +
            					'<tbody>' +
            						'<tr style="padding: 0; text-align: left; vertical-align: top">' +
            							'<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600">Free Data Fields:</strong>' +
            							'</td>' +
            						'</tr>' +
            					'</tbody>' +
            				'</table>' +
            			'</td>' +
            		'</tr>' +
            	'</tbody>' +
            '</table>';

    var Flexdtlhtml = '<table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">' +
            	'<tbody>' +
            		'<tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">' +
            			'<td style="width:50%;-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  @ofbflexname: </td>' +
            			'<td style="width:50%;-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  @ofbflexval </td>' +
            		'</tr>' +
            	'</tbody>' +
            '</table>';

</script>
    <a href="OfflineBookingQueue.aspx">&lt;&lt; Back To Offline Booking Queue</a>
    <div style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background: #fff !important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important">
    <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #fff !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
      <tr style="padding: 0; text-align: left; vertical-align: top">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
          <center data-parsed="" style="min-width: 700px; width: 100%">
            <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" align="center" class="float-center">
            
            <table align="center" class="emailcontainer bc-bookinginfo-wrapper float-center" style="Margin: 0 auto; background: #f7f7f7; border: solid #d0cfcf 1px; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 700px"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            
            <table class="main-header" width="100%" cellspacing="0" cellpadding="4" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600">Trip Details (Booked by <span id="spncuser">@createduser</span>)</strong></td>
            		</tr>
            	</tbody>
            </table>
            <table id="tblFltInfo" width="100%" cellspacing="0" cellpadding="4" border="0" class="table-bg-color" style="background-color: #f1f7fb; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            				<table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            					<tbody>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td height="12" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
            								<span class="image-label" style="background-color: #656565; color: #fff; font-size: 11px; font-weight: bold; padding: 1px 6px; text-transform: uppercase">Air Transport</span>
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td height="5" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  
										  <strong style="font-weight: 600">
											<span id="spnFltDepDt">@ofbdepday, @ofbdepdate</span>
										  </strong>                              
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
            								<hr size="1" style="margin: 5px 0;border-color: #ccc">
            							</td>
            						</tr>
            					</tbody>
            				</table>
            				<table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            					<tbody>
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Cabin Type: <strong style="font-weight: 600"><span id="spnFltcabcls">@ofbcabinclass</span></strong>  
										</td>
									</tr>
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Departure <strong style="font-weight: 600"><span id="spnFltdepap">@ofbdepairport, @ofbdeptime</span></strong>  
										</td>
									</tr>
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Arrival <strong style="font-weight: 600"><span id="spnFltarrap">@ofbarrairport</span></strong>  
										</td>
									</tr>
									<!-- Empty rows to increase the gap between segments -->
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600"></strong>  
										</td>
									</tr>
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600"></strong>  
										</td>
									</tr>
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600"></strong>  
										</td>
									</tr>
									<tr style="padding: 0; text-align: left; vertical-align: top">
										<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600"></strong>  
										</td>
									</tr>
            					</tbody>
            				</table>
            				<div id="divSegments">@Segments</div>
            			</td>
            		</tr>		
            	</tbody>
            </table>
            <table id="tblHotelInfo" width="100%" cellspacing="0" cellpadding="4" border="0" class="table-bg-color" style="background-color: #f1f7fb; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            				<table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            					<tbody>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td height="12" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
            								<span class="image-label" style="background-color: #656565; color: #fff; font-size: 11px; font-weight: bold; padding: 1px 6px; text-transform: uppercase">Hotel Accomodation</span>
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td height="5" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  
                            <strong style="font-weight: 600">
                              <span id="spnHotcidt">@ohbciday, @ohbcidate </span>
                            </strong>
                            
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
            								<hr size="1" style="margin: 5px 0;border-color: #ccc">
            							</td>
            						</tr>
            					</tbody>
            				</table>
            				<table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            					<tbody>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  City: <strong style="font-weight: 600"><span id="spnHotCity">@ohbcity</span></strong>  
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  until: <strong style="font-weight: 600"><span id="spnHotcodt">@ohbcoday, @ohbcodate </span></strong>  
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Country: <strong style="font-weight: 600"><span id="spnHotcntry">@ohbcountry</span></strong>  
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Apartment Name: <strong style="font-weight: 600"><span id="spnHotapname">@ohbapname</span></strong>  
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Special Requests: <strong style="font-weight: 600"><span id="spnHotspreq">@ohbspreq</span></strong>  
            							</td>
            						</tr>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">  Additional Information: <strong style="font-weight: 600"><span id="spnHotadinfo">@ohbadinfo</span></strong>  
            							</td>
            						</tr>
            					</tbody>
            				</table>
            			</td>
            		</tr>
            		
            	</tbody>
            </table>
            <%--<table width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            				<table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            					<tbody>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600">Traveler: @opiname</strong>
            							</td>
            						</tr>
            					</tbody>
            				</table>
            			</td>
            		</tr>
            	</tbody>
            </table>
            <table class="table-content-area" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Date Of Birth: <strong>@opidob</strong>
            			</td>
            		</tr>	
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Passport No: <strong>@opippno</strong>
            			</td>
            		</tr>	
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Passport Expiry: <strong>@opippexp</strong>
            			</td>
            		</tr>	
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Place of Issue: <strong>@opipoi</strong>
            			</td>
            		</tr>	
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Email: <strong>@opiemail</strong>
            			</td>
            		</tr>	
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">Address: <strong>@opiaddress</strong>
            			</td>
            		</tr>
            	</tbody>
            </table>--%>
			<div id="divPaxInfo">@opiadditionalpax</div>
            <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            				<table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            					<tbody>
            						<tr style="padding: 0; text-align: left; vertical-align: top">
            							<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word"><strong style="font-weight: 600">Please enter Trip Information:</strong>
            							</td>
            						</tr>
            					</tbody>
            				</table>
            			</td>
            		</tr>
            		<tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
            			<td width="50%" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  Trip Purpose:  
            			</td>
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  <span id="spnTripreason">@ofbpurpose</span>
            			</td>
            		</tr>
            	</tbody>
            </table>
            <div id="divFlexInfo">@obFlexheader @obFlexdata </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="border-collapse: collapse; border-spacing: 0; border-top: 1px solid #909090; margin-top: 5px; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word"></td>
            		</tr>
            	</tbody>
            </table>
            <table class="table-content-area" width="100%" cellspacing="4" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
            	<tbody>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td width="50%" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  
                              Request Code: <span id="spnBookRefNo">@bookrefno</span>
                              
            			</td>
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  
                              Request Date: <span id="spnBookdt">@bookdate</span>
                              
            			</td>
            		</tr>
            		<tr style="padding: 0; text-align: left; vertical-align: top">
            			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  
                            Ordered by: <span id="spnBookBy">@bookby</span>
                            
            			</td>
            			<td colspan="3" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 3px 8px; text-align: left; vertical-align: top; word-wrap: break-word">  
                            eMail: <a id="ancBookEmail" href="mailto:@bookemail" target="_blank" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">@bookemail  
            				</a>
            				  
            			</td>
            		</tr>
            	</tbody>
            </table>
            </td></tr></tbody></table>
          </center>
        </td>
      </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </div>
<asp:HiddenField ID="hdnRefNo" runat="server" />
<div id="divAlert" class="modal fade" role="dialog"></div>
</asp:Content>
