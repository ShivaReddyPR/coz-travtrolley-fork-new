﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"   Inherits="ActivityResultsGUI" Title="Activity Results" CodeBehind="ActivityResults.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
  

        <script type="text/javascript">
            function SortCityJS(id) {
                document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
                document.getElementById('<%=hdfCity.ClientID %>').value = id;
                document.forms[0].submit();
            }
            function SortThemeJS(id) {
                document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
                document.getElementById('<%=hdfTheme.ClientID %>').value = id;
                document.getElementById('lnk' + id).style.background = "#EAF7FD";
                document.forms[0].submit();
            }
            function SortDurationJS(id) {
                document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
                document.getElementById('<%=hdfDuration.ClientID %>').value = id;
                document.forms[0].submit();
            }
            function SortCountryJS(id) {
                document.getElementById('<%=hdfClearFilters.ClientID %>').value = "no";
                document.getElementById('<%=hdfCountry.ClientID %>').value = id;
                document.forms[0].submit();
            }

            function clearFilters() {
                document.getElementById('<%=hdfCity.ClientID %>').value = "";
                document.getElementById('<%=hdfTheme.ClientID %>').value = "";
                document.getElementById('<%=hdfDuration.ClientID %>').value = "";
                document.getElementById('<%=hdfCountry.ClientID %>').value = "";
                document.getElementById('<%=hdfClearFilters.ClientID %>').value = "yes";
                document.forms[0].submit();
            }
        </script>

   
    <div>
        <%--<form id="form2" action="ActivityDetails.aspx" method="post">
        <input type="hidden" id="resultIndex" name="resultIndex" value="<%=id %>" />
        </form>--%>
    </div>
    <div>
        <%--<form id="form1" action="ActivityResults.aspx" method="post" runat="server">--%>
         <asp:HiddenField ID="hdfID" runat="server" />
        <asp:HiddenField ID="hdfCity" runat="server" />
        <asp:HiddenField ID="hdfTheme" runat="server" />
        <asp:HiddenField ID="hdfDuration" runat="server" />
        <asp:HiddenField ID="hdfCountry" runat="server" />
        <asp:HiddenField ID="hdfClearFilters" runat="server" Value="no" />
         </div>
       
       
        <div>
           

<div class="col-md-2 padding-0 hidden-xs">

 <div id="">
                        <div class="ns-h3">Narrow Result</div>
                        <div class="bor_gray bg_white">
                     
                        <div class="pad_10"><a href="#" onclick="clearFilters()">Clear Filter</a></div>
                        
                         
                                
                             <div class="activity_heading"> Select Country /City </div>
                                
                                <div>
                                    <asp:TreeView ID="TreeView1" runat="server" ShowLines="True">
                                    </asp:TreeView>
                             
                                </div>
                                
                                
                        
                          <div class="activity_heading"> Categories </div>
                                
                                
                                
                                
                                <div class="pad_10">
                                    <ul>
                                        <%for (int i = 0; i < ThemeList.Rows.Count; i++)
                                          { %>
                                        <li id='lnk<% =ThemeList.Rows[i]["ThemeId"].ToString()%>'><a  href="#" onclick="javascript:SortThemeJS('<% =ThemeList.Rows[i]["ThemeId"].ToString()%>')">
                                            <% =ThemeList.Rows[i]["ThemeName"].ToString()%></a></li>
                                        <%--<asp:LinkButton ID="<% =CityList[i]%>" runat="server" Text="- <% =CityList[i]%>" OnClick="<% =CityList[i]%>_Click"></asp:LinkButton></li>--%>
                                        <%} %>
                                    </ul>
                                </div>
    

    <div>
    
    <div class="activity_heading">Activity Duration </div>
      
        
       <div class="pad_10">
            <ul>
                <%for (int i = 0; i < Duration.Count; i++)
                  { %>
                <li><a href="#" onclick="javascript:SortDurationJS('<% =Duration[i]%>')">-
                    <% =Duration[i]%>
                    </a></li>
                <%} %>
            </ul>
        </div>
        
    </div>
    </div>
    
    </div>
    
     <div class="clear"></div>
    </div>
 
 
<div class="col-md-10 pad_right0"> 


        <div style=" display:block" class="listing_search">
            <div class="pagination_coz">
                <div class="pagination_l">
                </div>
                <div class="pagination_m">
                    <div class=" page_l">
                        Sort by: <span class="label24">
                            <asp:DropDownList ID="ddlSort" CssClass="inp_auto" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged1">
                                <asp:ListItem Value="price" Selected="True" Text="Lowest Price"></asp:ListItem>
                                <asp:ListItem Value="alphabetical" Text="Alphabetical order"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblActivityName" Text='<%# Eval("activityName") %>' runat="server"></asp:Label>
                            
                        </span>
                    </div>
                    <div class=" page_r">
                    Page:  
                        
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click"  CssClass="PagerStyle" >First</asp:LinkButton>
                       
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" CssClass="PagerStyle"  />
                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" CssClass="PagerStyle" />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" CssClass="PagerStyle">Last</asp:LinkButton>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="pagination_l2">
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
      
        
     <div class="col-md-12 padding-0">
           
           
  
           
           
          
            <asp:DataList ID="DLActivities" runat="server" CellPadding="4" class=""
                DataKeyField="activityId" Width="100%" 
                onitemdatabound="DLActivities_ItemDataBound">
                <ItemTemplate>
                
                
       
                
              <div class=" bor_gray bg_white pad_10 marbot_10"> 
                
                <div class="col-md-12 pad0">
                <div class="col-md-2  pad0"> 
                
          
                
                <img class=" img-responsive" src='<%=activityImgFolder %><%# Eval("imagePath1") %>' />
                
                
                
               
                </div> 
                <div class="col-md-8"> 
                <div> 
                <h4>   <%# Eval("activityName") %></h4>
                
                </div>
                
                <div> <asp:Label ID="Label2" runat="server" Text='<%# restrictLength(Eval("introduction")) %>'></asp:Label></div>
                
                <div> 
                
                <p>
                                
                               
                                    <strong>Location :</strong>
                                    <asp:Label ID="lblCity" runat="server" Text='<%# Eval("cityName") %>'></asp:Label><span>,</span>
                                    <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("countryName") %>'></asp:Label>
                                    <br>
                                    <strong>Duration :</strong><asp:Label ID="lblDuration" runat="server" Text='<%# Eval("durationHours") %>'></asp:Label>Hours
                                    </p>
                </div>
                
                <div> Category: <asp:Label ID="lblCategory" runat="server" Font-Bold="true" ></asp:Label></div>
                
                
                </div> 
                
                <div class="col-md-2"> 
                
                
              
                                <div class="lowest_price">  Starting From</div>
                                
                                <div class="best_price">
                                    <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency%>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#CTCurrencyFormat(Eval("Amount")) %>'></asp:Label></div>
                                    
                                    
                                <div> per person</div>
                                
                                
                                
                                <div> <a href="ActivityDetails.aspx?id=<%# Eval("activityId") %>" class="but but_b "> Book Now</a></div>
                                
                           
                 <div class="clearfix"></div>
                </div> 
                
               <div class="clearfix"></div>
                
                
                 </div>
                 
               
                    <div class="clearfix"> </div>
                    
                    </div>
                    
                    
                </ItemTemplate>
            </asp:DataList>
           
            

           
           
            </div>
        
    

    
    
     <div class="clear"></div>
    
    </div>

 
 <div class="clear"></div>
                    
   
    </div>
 

    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

