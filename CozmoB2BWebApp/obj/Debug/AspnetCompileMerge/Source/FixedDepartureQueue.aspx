﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDepartureQueueGUI" Title="FixedDeparture Queue" Codebehind="FixedDepartureQueue.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<style>
  p    {color:Red}
</style>
<link href="css-holiday/main-style.css" rel="stylesheet" type="text/css" />
<%--<link href="css/main-style.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
   
    <script type="text/javascript" src="ash.js"></script>
     <link rel="stylesheet" href="css/style.css">
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />

<div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
            </div>
<div class="clear" style="margin-left: 30px">
            <div id="container3" style="position: absolute; top:120px; left: 97px; display: none;">
            </div>
        </div>
         <div class="clear" style="margin-left: 30px">
            <div id="container4" style="position: absolute; top:120px; left: 377px; display: none;">
            </div>
        </div>
  <div style=" padding-top:10px">
  <div title="Param" style=" border: solid 1px #ccc; padding:5px;">
        
        <table width="100%" cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:75px"></td>
                <td style="width:175px"></td>
                <td style="width:75px"></td>
                <td style="width:175px"></td>
                <td style="width:75px"></td>
                <td style="width:100px"></td>
                <td style="width:75px"></td>
                <td style="width:75px"></td>
            </tr> 
           
            <tr height="30px">
                 <td align="right"><asp:Label ID="lblFromDate" Text="From Date:" runat="server" CssClass="label" ></asp:Label></td>
                  <td>          <p class="fleft car-search">
                                <span class="fleft margin-top-3 margin-left-5">
                                <asp:TextBox ID="txtFrom" CssClass="auto-list" runat="server" Width="80"></asp:TextBox>
                                 </span>
                                 <a href="javascript:void(null)" onclick="showCalendar3()">
                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                            margin: 5px" />
                    </a>
</p>
                                 </td>
                                
                 <td align="right"><asp:Label ID="lblToDate" Text="To Date:" runat="server" CssClass="label" ></asp:Label></td>
                 <td> <p class="fleft car-search">
            <span class="fleft margin-top-3 margin-left-5">
                  <asp:TextBox ID="txtTo" CssClass="auto-list" runat="server" Width="80"></asp:TextBox>
            </span>
            <a href="javascript:void(null)" onclick="showCalendar4()">
                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                            margin: 5px" />
                    </a>
</p>
            </td> 
              <td align="left"><asp:Label ID="lblTripId" Text="Trip Id:" runat="server" CssClass="label" ></asp:Label></td>
              <td> <asp:TextBox ID="txtTripId" CssClass="auto-list" runat="server" Width="80"></asp:TextBox></td>
         
                 
            </tr>
            
            
            <tr height="30px">
                  <td align="right"><asp:Label ID="lblPaxName" Text="Pax Name:" runat="server" CssClass="label" ></asp:Label></td>
              <td>&nbsp;<asp:TextBox ID="txtPaxName" CssClass="auto-list" runat="server" Width="80"></asp:TextBox></td>
                                
                  <td align="right"><asp:Label ID="lblPrice" Text="Status:" runat="server" CssClass="label" ></asp:Label></td>
              <td>&nbsp;<asp:DropDownList ID="ddlStatus" runat="server" CssClass="inputDdlEnabled" Width="120px">
              <asp:ListItem Text="Select" Value="-1" Selected="True"></asp:ListItem>
              <asp:ListItem Text="Quoted" Value="Q"></asp:ListItem>
              <asp:ListItem Text="Confirmed" Value="C"></asp:ListItem>
              <asp:ListItem Text="InProgress" Value="I"></asp:ListItem>
              <asp:ListItem Text="Quoted Cancel" Value="X"></asp:ListItem>
              <asp:ListItem Text="Requested For Cancel" Value="Y"></asp:ListItem>
              <asp:ListItem Text="Cancelled" Value="Z"></asp:ListItem>
              </asp:DropDownList></td>
              
             <td align="left">
                    &nbsp; Agency: 
                </td>
                <td>&nbsp;<asp:DropDownList ID="ddlAgency" runat="server" CssClass="inputDdlEnabled"></asp:DropDownList>
                </td>
         
                 
                 
            </tr>
            <tr height="30px">
                
                <td colspan="6"></td>
                <td align="left"><asp:Button runat="server" ID="btnSearch" Text="Search" 
                         CssClass="button" OnClick="btnSearch_Click"  /></td>
                         <td align="left">
                         <asp:Button runat="server" ID="btnClear" Text="Clear" Width="50px"  
                         CssClass="button" OnClick="btnClear_Click"  />
                         </td>
            </tr>
        </table>
           </div>
           <div style=" height:40px">
           
           <div class=" pager" style="height:25px;">
                        Page:
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First" CssClass="PagerStyle"></asp:LinkButton>
                        
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" CssClass="PagerStyle"/>
                        <asp:Label ID="lblCurrentPage" runat="server" CssClass="PagerStyle"></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" CssClass="PagerStyle" />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last" CssClass="PagerStyle"></asp:LinkButton>
                    </div>
           </div>
           
                    
             <div style="width: 970px;  color: #000000; background-color: #FFFFFF;">
                 <asp:DataList ID="dlActivityQueue" runat="server" CellPadding="4"
                     DataKeyField="ATHId" Width="850px" onitemdatabound="dlActivityQueue_ItemDataBound" 
                     onitemcommand="dlActivityQueue_ItemCommand">
                     <ItemTemplate>
                     <div id="Result" style="width: 950px;"> 
                        
                         <table class="tbl" style="background-color: #EEF0FC;" width="99%">
                             <tr style="height: 5px;">
                             <td style="width: 2%"></td>
                                 <td style="width: 35%">
                                 </td>
                                 <td style="width: 30%">
                                 </td>
                                 <td style="width: 23%">
                                 </td>
                                 <td style="width: 9%">
                                 </td>
                             </tr>
                             <tr style="height: 25px;">
                             <td></td>
                                 <td colspan="1">
                                 <div>
                                 <p>
                                     <span style="color:#2B6BB5; font-size:small;font-weight: bold;">
                                         <%# Eval("activityName")%></span>
                                      <span>(<%# Eval("agent_name")%>)</span></p></div>
                                 </td>
                                 <td> <span class="auto" style="width: 150px;">
                                  <asp:Label style="font-weight:bolder;font-style:italic" ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                 <asp:HiddenField ID="hdnQuoted" runat="server" Value='<%#Eval("QuotedStatus") %>' />
                                  </span>
                                 </td>
                                 <td>
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("TripId")%></span>
                                            
                                 
                                 </td>
                                 <td></td>
                             </tr>
                             <tr style="height: 25px;">
                             <td></td>
                                 <td>
                                 <table>
                                         <tr>
                                             <td style="width:80px">
                                                 <span style="font-size: 10px;">Booked:</span>
                                             </td>
                                             <td>
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("TransactionDate")%></span>
                                             </td>
                                         </tr>
                                     </table>
                                     
                                 </td>
                                 
                                 <td>
                                 <table>
                                 <tr>
                                 <td style="width:40px"><asp:Label ID="lblStatusText" runat="server" Text="Status:" Visible="false"></asp:Label>
                                 <asp:HiddenField ID="hdnStatusValue" runat="server" Value='<%# Eval("PaymentStatus")%>' />
                                 </td>
                                 <td style="font-size: 12px; font-weight: bold;"><asp:Label ID="lblStatusValue" runat="server" Text='<%#(Convert.ToInt32(Eval("PaymentStatus")) == 1 ? "<p>Partial Payment</p>" : "Fully Paid")%>' Visible="false"></asp:Label>
                                  <asp:Label ID="lblQuotedStatus" runat="server" Visible="false"></asp:Label></td>
                                 </tr>
                                 </table>
                                 </td>
                            <%-- <td> Status:<span class="auto" style="width: 150px;"><%#(Convert.ToInt32(Eval("PaymentStatus")) == 1 ? "<p>Partial Payment</p>" : "Full payment")%></span></td>--%>
                                 <td>
                                     <span class="fleft font-14" style="color: Blue;">
                                         <%#Eval("LeadPaxName")%>
                                     </span>
                                 </td>
                                 <td align="center">
                                 <table id="tblOpen" runat="server" visible="false">
                                 <tr>
                                 <td>
                                 <span>
                                         <input id="Open" class="button" onclick='location.href=&#039;ViewBookingForFixedDeparture.aspx?bookingId=<%# Eval("ATHId") %>&#039;'
                                             type="button" value="Open" />
                                     </span>
                                     <span>
                                         <asp:Button CssClass="button-small" ID="btnInvoice"  runat="server" Text="View Invoice" Visible="true"  />
                                         <asp:Button CssClass="button-small" ID="btnCreditNote"  runat="server" Text="View Credit Note" Visible="false" />
                                     </span>
                                 </td>
                                 </tr>
                                 </table>
                                     <table id="tblContinue" runat="server" visible="false">
                                     <tr>
                                     <td>
                                     <span>
                                     <asp:Button ID="btnContinue" runat="server"  CommandName="Continue" CommandArgument='<%# Eval("ATHId") %>' Text="Continue" CssClass="button" />
                                     </span>
                                     </td>
                                     </tr>
                                     </table>
                                 </td>
                             </tr>
                             <tr style="height: 25px;">
                             <td></td>
                                 <td>
                                     <table>
                                         <tr>
                                             <td style="width:80px">
                                                 <span style="font-size: 10px;">Tour Dt:</span>
                                             </td>
                                             <td>
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("Booking")%></span>
                                             </td>
                                         </tr>
                                     </table>
                                 </td>
                                 <td>
                                     <asp:Button ID="btnCancel" runat="server" class="button" Text="Cancel" Visible="false"  CommandName="Cancel" CommandArgument='<%# Eval("ATHId") %>'/>
                                 </td>
                                 <td>
                                     <span style="font-size: large; font-weight: bold;" class="font-12">Price:
                                         <%#CTCurrencyFormat(Convert.ToDecimal(Eval("TotalPrice")) - Convert.ToDecimal(Eval("Discount")), Eval("AgencyId"))%>
                                         </span>
                                 </td>
                                 <td>
                                 </td>
                             </tr>
                              <tr>
                                 <td colspan="6">
                                     <b id="errRemarks" style="color: Red" runat="server"></b>
                                     <table style="margin-top: 10px" id="tblChangeRequest" visible="false" width="100%"
                                         border='0' cellspacing='0' cellpadding='2' runat="server">
                                         <tr>
                                             <td align="right">
                                                 Request Change :
                                             </td>
                                             <td align="left">
                                                 <asp:DropDownList ID="ddlChangeRequestType" runat="server">
                                                     <asp:ListItem Selected="True" Text="Select" Value="Select"></asp:ListItem>
                                                     <asp:ListItem Text="Cancel Request" Value="Cancel Request"></asp:ListItem>
                                                 </asp:DropDownList>
                                             </td>
                                             <td align="left">
                                                 <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Rows="2" Text="Enter Remarks here"
                                                     runat="server" Width="200px" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"
                                                     onblur="this.style.color='#000000'; if( this.value=='' ) { this.value='Enter Remarks here'; }"></asp:TextBox>
                                                 &nbsp;&nbsp;
                                                 <asp:Button ID="btnRequest" runat="server" CommandName="FdChangeRequest" CommandArgument='<%#Eval("ATHId") %>'
                                                     Text="Submit Request" Font-Bold="True" /> 
                                             </td>
                                             <td align="right">
                                             </td>
                                         </tr>
                                     </table>
                                 </td>
                             </tr>
                         </table>
                       
                        </div>
                     </ItemTemplate>
                 </asp:DataList>
             </div>
    </div>
    <script type="text/javascript">
        
        var cal3;
        var cal4;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
           
            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("title", "Purchase From Date");
            cal3.selectEvent.subscribe(setDate3);
            cal3.cfg.setProperty("close", true);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("title", "Purchase To Date");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        
        function showCalendar3() {
            
            $('container4').context.styleSheets[0].display = "none";
            cal4.hide();
            cal3.show();
           
            document.getElementById('container3').style.display = "block";
        }
        var departureDate = new Date();
        function showCalendar4() {
            $('container3').context.styleSheets[0].display = "none";
            cal3.hide();
            cal4.show();
            var date3 = document.getElementById('<%= txtFrom.ClientID%>').value;
            
            if (date3.length != 0 && date3 != "DD/MM/YYYY") {
                var depDateArray = date3.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal4.render();
            }
            document.getElementById('container4').style.display = "block";
        }

        function setDate3() {
           

            var date3 = cal3.getSelectedDates()[0];

            var month = date3.getMonth() + 1;
            var day = date3.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtFrom.ClientID %>').value = day + "/" + month + "/" + date3.getFullYear();
            cal3.hide();
        }

        function setDate4() {
            var date4 = cal4.getSelectedDates()[0];

            var date3 = document.getElementById('<%=txtFrom.ClientID %>').value;
            if (date3.length == 0 || date3 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select Purchase from date.";
                return false;
            }

            var depDateArray = date3.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Purchase From Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date4.getFullYear(), date4.getMonth(), date4.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Purchase To date should be greater than Purchase from date (" + date3 + ")";
                return false;
            }
           
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date4.getMonth() + 1;
            var day = date4.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtTo.ClientID %>').value = day + "/" + month + "/" + date4.getFullYear();
            cal4.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init);
        YAHOO.util.Event.addListener(this, "click", init);

        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0.00') {
                document.getElementById(id).value = '';
            }
        }
        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0.00') {
                document.getElementById(id).value = '0.00';
            }
        }


        function CancelInsPlan(index) {
            var val = document.getElementById('ctl00_cphTransaction_dlActivityQueue_ctl0' + index + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlActivityQueue_ctl0' + index + '_ddlChangeRequestType').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlActivityQueue_ctl0' + index + '_errRemarks').innerHTML = "Please Select Request type!";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
            document.getElementById('ctl00_cphTransaction_dlActivityQueue_ctl0' + index + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            else {
                document.getElementById('ctl00_cphTransaction_dlActivityQueue_ctl0' + index + '_errRemarks').innerHTML = "";
                return true;
            }
        }

        function ViewInvoice(fixID, AgentId) {
            window.open("FixedDepartureInvoice.aspx?bookingId=" + fixID + "&agencyId=" + AgentId + "&fromAgent=true", 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            return false;
        }
        function ViewCreditNote(fixID, AgentId) {
            window.open("FixedDepartureCreditNote.aspx?bookingId=" + fixID + "&agencyId=" + AgentId + "&fromAgent=true", 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            return false;
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

