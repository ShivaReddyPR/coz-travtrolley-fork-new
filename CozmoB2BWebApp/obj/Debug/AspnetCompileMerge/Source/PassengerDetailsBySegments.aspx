﻿<%@ Page Title="Passenger Details" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="PassengerDetailsBySegments.aspx.cs" Inherits="PassengerDetailsBySegments" %>

<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link rel="stylesheet" href="css/accordian.css" />
    <link rel="stylesheet" type="text/css" href="css/seat-charts.css" />

    <style>
        
        a {
            color: #b71a4c;
        }

        .disabled {
            color: #ccc;
            pointer-events:none;
        }

        .front-indicator {
            
            background-color: #8f858a;
            color: #fff;
            text-align:center;
            padding: 3px;
            border-radius:5px;
            margin:auto;
            display:inline-block;
            width:140px; margin-bottom:10px;
        }

        .wrapper {
            width: 100%;
            text-align: center;
            margin-top: 150px;
        }



        .booking-details {
            float: left;
            text-align: left;
            margin-left: 35px;
            font-size: 12px;
            position: relative;
            height: 401px;
        }

            .booking-details h2 {
                margin: 25px 0 20px 0;
                font-size: 17px;
            }

            .booking-details h3 {
                margin: 5px 5px 0 0;
                font-size: 14px;
            }

        div.seatCharts-cell {
            color: #182C4E;
            height: 30px;
            width: 30px;
            line-height: 30px;
        }

        div.seatCharts-seat {
            color: #FFFFFF;
            cursor: pointer;
        }

        div.seatCharts-row {
            height: 35px;
            width: 450px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
        }

        div.seatCharts-seat.available {
            background-color: #5ec034;
        }

            div.seatCharts-seat.available.first-class {
                /* 	background: url(vip.png); */
                background-color: #3a78c3;
            }

        div.seatCharts-seat.focused {
            background-color: #76B474;
        }

        div.seatCharts-seat.selected {
            background-color: #E6CAC4;
        }

        div.seatCharts-seat.unavailable {
            background-color: #6d6b6c;
        }

        div.seatCharts-container {
            /*border-right: 1px dotted #adadad;
            width: 260px;*/
            padding: 0px 10px 20px 10px;
            float: left;
        }

        div.seatCharts-legend {
            padding-left: 0px;
            position: absolute;
            bottom: 16px;
        }

        ul.seatCharts-legendList {
            padding-left: 0px;
        }

        span.seatCharts-legendDescription {
            margin-left: 5px;
            line-height: 30px;
        }

        .checkout-button {
            display: block;
            margin: 10px 0;
            font-size: 14px;
        }

        #selected-seats {
            max-height: 90px;
            overflow-y: scroll;
            overflow-x: none;
            width: 170px;
        }

        .ui-accordion .ui-accordion-header {  
            padding: 9px 10px 9px 10px;
            font-size: 17px;
            font-weight: 700;
            position:relative;

            color:#fff;
    
        }
  
        .custom-arrow-down,.custom-arrow-up {  
            width:23px; 
            height:23px;
            display:block; 
            position:absolute;
            right:10px;
            top: 13px;
            background:none!important;
            text-indent: inherit !important;
        }
        
        .custom-arrow-up:before,.custom-arrow-down:before{
            font-family: 'Glyphicons Halflings';
            color:#fff;
        }
        
        .custom-arrow-up:before{
                content: "\e113";
        }
        
        .custom-arrow-down:before {
                content: "\e114";
        }
        
        .ui-accordion .ui-accordion-header{
            background-color: #6d6b6c;
        }
        
        .ui-accordion .ui-accordion-header.ui-accordion-header-active{
            background-color: #5ec034;
        }
   
        .ui-accordion .ui-accordion-content {
            padding: 0px 10px 0px 10px;   
            overflow: hidden;
        }
        
  

        .pax_grid {
	        border-bottom: solid 1px #ccc;
	        padding-bottom: 10px;
	        padding-top: 10px; width:100%;
        }


        .pax_grid:last-child {
          border-bottom: solid 1px #fff;
        }


        .pax_grid .pax_name {
	        font-size: 12px;
	        text-transform: uppercase;
	        font-weight: 700
        }

        .pax_grid .selected_seat,
        .pax_grid .No_selected_seat {
	        font-size: 11px;
	        display: inline-block;
	        width: 42px;
	        height: 26px;
	        line-height: 26px;
	        text-align: center;
	        border-radius: 4px;
        }

        .light_wrapper .selected,
        .light_wrapper .available,
        .light_wrapper .occupied {
	        display: inline-block;
	        width: 20px;
	        height: 20px;
	        text-align: center;
	        border-radius: 4px;
	        padding-left: 20px;
	        line-height: 20px;
	        font-size: 12px;
        }

        .light_wrapper .selected {
	        background-color: #e6cac4;
        }

        .light_wrapper .available {
	        background-color: #5ec034;
        }

        .light_wrapper .occupied {
	        background-color: #6d6b6c;
        }

        .light_wrapper {
	        padding: 10px;
	        background-color: #fff;
	        border: solid 1px #ccc;
        }

        .pax_grid .selected_seat,
        .pax_grid .No_selected_seat {
	        border: solid 1px #dadada;
        }

        .pax_grid .No_selected_seat {
	        background-color: #fff;
	
	
        }

        .pax_grid .selected_seat {
	        background-color: #f6f6f6;
	        position:absolute; top:-4px;
        }

        .pax_grid .currency {
	        color: Gray;
	        font-size: 11px;
        }


        .pax_grid .price { font-size:12px; font-weight:700 }


        .scroll_skeleton {
	        /*max-width: 270px;*/
	        max-height: 530px;
	        text-align: center;
	        margin-top: 20px;
	        margin-left: auto;
	        margin-right: auto;
	        overflow-y: scroll;
	        overflow-x: hidden;
	        scrollbar-color: #c0c0c4 #f0f0f0;
            scrollbar-width: thin;
    
        }

        .term_conditions {
	        font-size: 11px; margin-top:14px;
        }

        .term_conditions a {
	        color: Red
        }

        .btn_reset {
	        border: solid 2px #666666;
	        margin-top: 20px;
        }

        @media(max-width:992px) {
	        .wrapper {
		        width: 100%;
	        }
        }

        .selected_seat, .No_selected_seat   {
        font-size:11px; display:inline-block; 
         width:26px; height:26px; line-height:26px; 
         text-align:center; border-radius:4px; 
         }	
        .selected_seat, .No_selected_seat {   border:solid 1px #dadada;  }

        @media (max-width: 767px) {
            .pax_grid .price {
                font-size: 12px;
            }

            .pax_grid .pax_name {
                font-size: 12px;
            }
        }

        .baggage-wrapper /* Added this style for displaying baggage onward and return */ {
            /*width: 100%;*/
        }

        .subgray-header {
            height: auto;
            line-height: 20px;
            padding: 5px 10px 5px 10px;
            margin-bottom: 15px;
        }
        .custom-checkbox-style input[type=checkbox]+label:before {
			line-height: 1.2;
		}

	   .chkbox-addpasngr{
			position:absolute; 
			right:10px; 
			top:-20px; 
			display:inline-block;
	   }
		.passenger-label{
			line-height: 2;
		}
    </style>

    <script src="Scripts/jsBE/Utils.js"></script>
    <script src="Scripts/Common/common.js"></script>
    <script src="scripts/accordian.js"></script>
    <script src="scripts/seat-charts.js"></script>
    <script type="text/javascript">

        function Showseatalert(msg) {

            /* To disable bootstrap modal close on escape button and pop up outside click */

            $("#Seatsalert").modal({
                backdrop: 'static',
                keyboard: false
            });

            CalculateBaggagePrice();

            /* To identify error message journey type and update continue button label */
            document.getElementById('<%=btnContinue.ClientID %>').value = msg.indexOf("O|") != -1 ? 'Skip onward rejected seats and continue with booking' : 'Continue';
            msg = msg.replace('O|', '');

            document.getElementById('divSeatsalertText').innerHTML = msg.replace(/@/g, '</br>');
            $('#Seatsalert').modal('show');
        }

        function Cancel(id) {
           $('#'+id).modal('hide');
           window.location.href = 'HotelSearch.aspx?source=Flight';
        }
        
        function Continue(id) {
            var seatalertbtn = document.getElementById('SeatsalertContinue');
            $(seatalertbtn).prop("disabled", true);

            $('#' + id).modal('hide');
            showpopup('Failed');

            /* To expand ADD API's box for both lead and additional passengers */
            var ancAPI = document.getElementById('ancAPI');
            if (ancAPI.attributes["aria-expanded"].value == 'false')
                ancAPI.click();
            for (var paxind = 0; paxind < PaxNames.length; paxind++) {
                ancAPI = null;
                ancAPI = document.getElementById('ancAPI' + paxind);
                if (ancAPI != null && ancAPI.attributes["aria-expanded"].value == 'false')
                   ancAPI.click();
            }

            $(seatalertbtn).prop("disabled", false);
            return false;
        }

        function Saveseatinfo() {

            var counter = 0;
            for (var i = 1; i <= SeatData.SegmentSeat.length; i++) {

                var id = document.getElementById('Seg' + i.toString());
                var segname = id.title;
                for (var c = 1; c <= PaxNames.length; c++) {
                    var seatrow = document.getElementById('Seg' + i.toString() + 'paxrow' + c.toString());
                    var seatid = document.getElementById('Seg' + i.toString() + 'pax' + c.toString() + 'seatno');
                    var priceid = document.getElementById('Seg' + i.toString() + 'pax' + c.toString() + 'price');
                    paxseatdtls[counter].Segment = segname;
                    paxseatdtls[counter].PaxNo = c - 1;
                    paxseatdtls[counter].SeatNo = seatid.innerText;
                    //paxseatdtls[counter].SeatNo = c < 3 && i == 2 && SeatsarrayMain[c] != null ? SeatsarrayMain[c] : seatid.innerText;
                    paxseatdtls[counter].Price = seatid.innerText == '' ? '0' : priceid.innerText.substring(priceid.innerText.indexOf(" ") + 1, priceid.innerText.length);
                    if (seatrow.classList.contains('disabled'))
                        paxseatdtls[counter].SeatStatus = 'S'
                    else
                        paxseatdtls[counter].SeatStatus = paxseatdtls[counter].SeatNo == '' ? '' : 'A';
                    counter++;
                }
            }
            $('#<% =txtPaxSeatInfo.ClientID %>').val(JSON.stringify(paxseatdtls)); 
            $('#myModal').modal('hide');
            return false;
        }

        function showpopup(source) {
                        
            var seatbtn = document.getElementById('btnSeatselect');
            $(seatbtn).prop("disabled", true);
            $('.modal').css('overflow-y', 'auto');                         

            if (source == 'Failed')
                BindSegMents(SeatData);
            else {
                if (!validate()) {
                    $(seatbtn).prop("disabled", false);
                    return false;
                }
            }

            if (firstflag) {
                agentdecimal = eval('<%=agency.DecimalValue %>');
                sCurrency = '<%=agency.AgentCurrency %>';
                var paxseatobj = document.getElementById('<%=hdnPaxSeatInfo.ClientID %>').value;
                paxseatdtls = JSON.parse(paxseatobj);
                GetSeats('', '');
            }
            if (!firstflag)
                $('#myModal').modal('show');

            $(seatbtn).prop("disabled", false);
        }

        function ResetSeatMap() {

            var seatbtn = document.getElementById('btnSeatreset');
            $(seatbtn).addClass("disabled");

            paxseatinfo = document.getElementById('<%=txtPaxSeatInfo.ClientID %>').value;
            
            if (paxseatinfo != ''){

                paxseatdtls = JSON.parse(paxseatinfo);
                for (var c = 0; c < paxseatdtls.length; c++)
                {
                    if (paxseatdtls[c].SeatStatus != 'S') {                        
                        paxseatdtls[c].SeatNo = '';
                        paxseatdtls[c].Price = '0';
                        paxseatdtls[c].SeatStatus = '';
                    }
                }
                $('#<% =txtPaxSeatInfo.ClientID %>').val(JSON.stringify(paxseatdtls)); 
            }            
            
            resetflag = firstflag = true;
            GetSeats('', '');

            $(seatbtn).removeClass("disabled");

        }

        function BindSegMents(segmentdata) {

            var paxseatobj = document.getElementById('<%=txtPaxSeatInfo.ClientID %>').value;
            if (paxseatobj)
                paxseatdtls = JSON.parse(paxseatobj);
            Selseats = [];
            sSuccessSegments = '';
            var tmpseg = ''; var activesegid = '';
            $('#Segments').children().remove();
            $("#Segments").append('<div id="accordion">');
            
            $.each(segmentdata.SegmentSeat, function (key, value) {

                var TotalPrice = 0;                
                Selseats[key] = '';
                var paxseatassignedcnt = 0; 
                sFailureSegments = '';
                $("#accordion").append('<h4 id="' + value.SegmntID + '" title="' + value.Origin + '-' + value.Destination + '">' +
                    value.Origin + ' - ' + value.Destination + '</h3>');
                $("#accordion").append('<div id= "' + value.SegmntID + 'paxdtls"></div>');
                $('#' + value.SegmntID + 'paxdtls').append(
                    '<div id="divpaxinfo"><table width="100%" id= "' + value.SegmntID + 'tblpaxdtls">' +
                    //'<tr> <td>Passenger Name</td> <td> Price </td> <td> SeatNo </td> </tr>' +
                    '</table ></div>');

                if (PaxNames.length > 0) {

                    for (var i = 1; i <= PaxNames.length; i++) {

                        var seatno = '', sprice = '', seatstatus = '';

                        if (paxseatobj) {

                            $.each(paxseatdtls, function (cnt, seat) {

                                if (!(resetflag == true && seat.SeatStatus == 'A')) {

                                    if (seat.PaxNo == i - 1 && seat.Segment == value.Origin + '-' + value.Destination && seat.SeatNo != '' && seat.SeatNo != 'NoSeat') {
                                        seatno = seat.SeatNo; sprice = seat.Price; TotalPrice += eval(seat.Price);
                                        Selseats[key] = Selseats[key] != null && Selseats[key] != 'undefined' ? Selseats[key] + seatno + '-' + seat.SeatStatus + '|' : seatno + '-' + seat.SeatStatus + '|';
                                    }
                                    seatstatus = seat.PaxNo == i - 1 && seat.Segment == value.Origin + '-' + value.Destination ? seat.SeatStatus : seatstatus;
                                }
                            });

                        }

                        tmpseg = seatstatus == 'F' && tmpseg == '' ? value.SegmntID : tmpseg;
                        activesegid = seatstatus == 'F' && activesegid == '' ? key : activesegid;
                        var seatprice = seatno != '' ? sCurrency + ' ' + sprice : '';
                        var rowclass = seatstatus == 'S' ? 'row disabled' : 'row';
                        paxseatassignedcnt = seatstatus == 'S' ? paxseatassignedcnt + 1 : paxseatassignedcnt;

                        var pname = PaxNames[i - 1].slice(0, -2);
                        pname = pname.replace(/-/g, ' ');
                        $('#' + value.SegmntID + 'tblpaxdtls').append(
                            '<div class="pax_grid"><div class="'+ rowclass +'" id="' + value.SegmntID + 'paxrow' + i.toString() + '" >' +
                            '<div class="col-md-7 col-xs-7"><span  id="' + value.SegmntID + 'pax' + i.toString() + 'name" class="pax_name">' + pname + '</span></div>' +
                            '<div class="col-md-2 col-xs-2"><span id="' + value.SegmntID + 'pax' + i.toString() + 'seatno" class="selected_seat cancel-cart-item">' + seatno + '</span ></div >' +                        
                            '<div class="col-md-3 col-xs-3"><span class="price getprice" id="' + value.SegmntID + 'pax' + i.toString() +   'price">'+ seatprice +'</span></div>'+
                        '</div></div>'
                        );
                    }

                    var TotPrice = Selseats[key] != null && Selseats[key] != '' ? sCurrency + ' ' + parseFloat(eval(TotalPrice)).toFixed(agentdecimal) : '';

                    $('#' + value.SegmntID + 'tblpaxdtls').append(
                        '<div class="pax_grid"><div class="row">'+
                        '<div class="col-sm-6"><span class="pax_name">Total Seat Fare:</span></div>' + 
                        //'<div class="col-md-2 col-xs-2"><span id="' + value.SegmntID + 'pax' + i.toString() + 'seatno"class="selected_seat cancel-cart-item"></span></div>' +                        
                        '<div class="col-sm-6"><span class="price" id="' + value.SegmntID + '_Total">' + TotPrice + '</span></div>'+
                        '</div></div>'
                    );
                }


                if (paxseatassignedcnt == PaxNames.length)
                    sSuccessSegments = sSuccessSegments == '' ? value.SegmntID : sSuccessSegments + '|' + value.SegmntID;
            });

            var asgid = tmpseg == '' ? 0 : eval(tmpseg.substring(4, 3)) - 1;

            $(function () {

                var icons = {
                    header: "custom-arrow-down",
                    activeHeader: "custom-arrow-up"
                };

                $("#accordion").accordion({
                    icons: icons,
                    active:asgid,
                    heightStyle: "content"
                });

                $("#toggle").button().on("click", function () {
                    if ($("#accordion").accordion("option", "icons")) {
                        $("#accordion").accordion("option", "icons", null);
                    } else {
                        $("#accordion").accordion("option", "icons", icons);
                    }
                });
            });
            resetflag = false;
            tmpseg = tmpseg == '' ? document.getElementById('Seg1') : document.getElementById(tmpseg);
            LoadSegmentSeats(tmpseg);
        }

        function Getemptyseats(no) {
            var emptyseat = '';
            for (var i = 0; i < no; i++) {
                emptyseat += '|_';
            }
            return emptyseat;
        }

        /* Added .1 sec timer to show processing div on segment seats loading */
        function LoadSegmentSeats(event) {
            document.getElementById('ctl00_upProgress').style.display = 'block';
            setTimeout(function(){ LoadSegmentSeat(event); document.getElementById('ctl00_upProgress').style.display ='none'; }, 100);            
        }

        function LoadSegmentSeat(event) {

            $('#seatslayout').children().remove();
            //$("#seatslayout").append('<div class="container"></div>');
            //$(".container").append('<h1>Please select seats</h1>');
            $("#seatslayout").append('<div id="seat-map"></div>');
            $("#seat-map").append('<div class="front-indicator">Front</div>');

            var header = document.getElementById(event.id);
            currsegid = event.id;
            var segid = event.id.substring(4, 3);
            Seatsarray = []; SeatsarrayMain = [];
            var currentsegment = '';
            var columnlength = 0, rowno = 0, prevrow = 1, prevcolumnlength = 0; coldiff = 0;
            var seatrows = ''; seatype = ''; prevseattype = ''; lastselseatid = '';

            if (firstflag || sSuccessSegments.indexOf(currsegid) != -1 || sFailureSegments.indexOf(currsegid) != -1) {
                $.each(SeatData.SegmentSeat, function (key, value) {
                    if (value.Origin + '-' + value.Destination == header.title) {
                        currentsegment = value;
                    }
                });
            }

            $('#seatslayout').removeClass('disabled');
            if (sSuccessSegments != '' && sSuccessSegments.indexOf(currsegid) != -1)
                $('#seatslayout').addClass('disabled');

            if (firstflag && sFailureSegments == '')
                sFailureSegments = currsegid;

            if (currentsegment == '' || currentsegment.SeatInfoDetails == null) {

                var journy = header.title.split('-');
                GetSeats(journy[0], journy[1]);
                sFailureSegments = sFailureSegments == '' ? currsegid : sFailureSegments + '|' + currsegid;
                $.each(SeatData.SegmentSeat, function (key, value) {
                    if (value.Origin + '-' + value.Destination == header.title) {
                        currentsegment = value;
                    }
                });
            }

            if (currentsegment == '' || currentsegment.SeatInfoDetails == null)
                return false;

            $.each(currentsegment.SeatInfoDetails, function (key, value) {
                var seatind = '';
                if (value.RowNo == prevrow) {
                    columnlength++;
                    seatind = value.SeatNo.substring(value.SeatNo.length, value.SeatNo.length - 1);                    
                    if (prevseattype != null && value.SeatType != null && prevseattype != '' && value.SeatType != '') {
                        seatrows += prevseattype == value.SeatType ? 'N|' : '';
                        columnlength = prevseattype == value.SeatType ? Math.ceil(columnlength) + 1 : columnlength;
                    }
                    seatrows += seatind + '|';
                }
                else {
                    if (columnlength > prevcolumnlength) {
                        Seatsarray = [];
                        prevcolumnlength = columnlength;
                        Seatsarray.push(seatrows.slice(0, -1));
                    }
                    
                    columnlength = 1;
                    seatind = value.SeatNo.substring(value.SeatNo.length, value.SeatNo.length - 1);
                    seatrows = seatind + '|';
                }
                prevseattype = value.SeatType;
                prevrow = value.RowNo;
                if (value.AvailablityType != 'Open')
                    SeatsarrayMain.push(value.SeatNo);
            });

            var maxcollength = Seatsarray[0];

            var id = currsegid.replace('Seg', '');
            var $total = $('#' + currsegid + '_Total'),
                sc = $('#seat-map').seatCharts({
                    map: Selseats[id - 1] != null && Selseats[id - 1] != 'undefined' ? Selseats[id - 1] : '',
                    naming: {
                        segmntdtls: currentsegment,
                        columns: maxcollength,
                        rows: prevrow - 1,
                        top: false,
                        left: false,
                        getLabel: function (character, row, column) {
                            return firstSeatLabel++;
                        },
                    },
                    legend: {
                        node: $('#legend'),
                        items: [
                            ['f', 'available', 'First Class'],
                            ['e', 'available', 'Economy Class'],
                            ['f', 'unavailable', 'Already Booked']
                        ]
                    },
                    click: function () {
                        var segid = this.settings.id.substring(4, 3);
                        if (this.status() == 'available') {
                            //sCurrency = this.settings.CurrencyCode;
                            
                            if (Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' && Selseats[segid - 1] != '' && Selseats[segid - 1].slice(0, -1).split('|').length == PaxNames.length)
                                return 'available';

                            AssignSeat(this.settings.label, this.settings.Price, segid, 'Add', this.settings.CurrencyCode);

                            var totalprice = recalculateTotal(sc);
                            $total.text(Math.ceil(totalprice) > 0 ? this.settings.CurrencyCode + ' ' + totalprice.toLocaleString() : '');

                            return 'selected';
                        } else if (this.status() == 'selected') {

                            var tmpSelseats = '|'+ Selseats[segid - 1];
                            if (Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' && tmpSelseats.indexOf('|' + this.settings.label + '-S|') != -1) {
                                alert('Cannot modify the reserved seat');
                                return 'selected';
                            }

                            var segid = this.settings.id.substring(4, 3);

                            //remove the item from our cart
                            AssignSeat(this.settings.label, this.settings.Price, segid, 'Remove', this.settings.CurrencyCode);
                            
                            var totalprice = recalculateTotal(sc);
                            $total.text(Math.ceil(totalprice) > 0 ? this.settings.CurrencyCode + ' ' + totalprice.toLocaleString() : '');

                            //seat has been vacated
                            return 'available';
                        } else if (this.status() == 'unavailable') {
                            //seat has been already booked
                            return 'unavailable';
                        } else {
                            return this.style();
                        }
                    }
                });

            //this will handle seat no against passeger click event
            $('#' + currsegid + 'tblpaxdtls').on('click', '.cancel-cart-item', function () {
                lastselseatid = this;
                var seatno = this.innerText;
                if (lastselseatid.innerText != '') {
                    sc.get(currsegid + '_' + seatno).click();
                    var changeseatstatus = new Array();
                    changeseatstatus[0] = currsegid + '_' + seatno;
                    sc.get(changeseatstatus).status('available');
                    var seatid = document.getElementById(currsegid + '_' + seatno);
                    var clss = seatid.className;
                    seatid.setAttribute('class', clss.replace('selected', 'available'));
                }
            });

            //This will change the status of previously selected seats
            var disableseats = Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' ? Selseats[segid - 1].slice(0, -1).split('|') : '';
            if (disableseats != '') {
                for (var c = 0; c < disableseats.length; c++) {
                    disableseats[c] = currsegid + '_' + disableseats[c].slice(0, -2);
                }
                sc.get(disableseats).status('selected');
            }
        }

        var firstSeatLabel = 1;
        function recalculateTotal(sc) {
            var total = 0;

            //To find every selected seat and sum its price
            var cersegseats = $('#' + currsegid + 'tblpaxdtls .getprice');
            $.each(cersegseats, function (i, el) {
                var price = el.innerText.substring(el.innerText.indexOf(" ") + 1, el.innerText.length);
                if (price != '' && Math.ceil(price) > 0) {
                    total += parseFloat(eval(price));
                }
            });

            //sc.find('selected').each(function () {
            //    total += this.settings.Price;
            //});

            return total;
        }

        var SeatData;
        var paxseatdtls;
        var currsegid;
        var totselseats = 0;
        var Selseats = [];
        var Seatsarray = []; SeatsarrayMain = [];
        var paxcount = 0;
        var lastselseatid = '', paxseatinfo = '';
        var PaxNames = [];
        var firstflag = true, resetflag = false;
        var sCurrency = '';
        var sSuccessSegments = '';
        var agentdecimal = 2;
        var sFailureSegments = '';

        //Based on this flag the user selected meal values will be assigned to the hidden field value.
        //Once all the page validations are successful then we will assign this flag to true.
        var resetLCCMealFlag = false;

        function changeseat(event) {
            lastselseatid = event;
            var deselseatno = event.innerText;
            AssignSeat(event.innerText, event.id.substring(4, 3), 'Remove');
            var seatid = document.getElementById('Seg' + event.id.substring(4, 3) + '_' + deselseatno);
            seatid.setAttribute('class', 'seatCharts-seat seatCharts-cell available');
            seatid.setAttribute('aria-checked', false);
        }

        function AssignSeat(selseat, price, segid, Action, currency) {

            var tmpSelseats = '|'+ Selseats[segid - 1];
            Selseats[segid - 1] = '';
            var seatalloted = false;

            if (lastselseatid != '' && Action == 'Add') {
                lastselseatid.innerText = selseat; seatalloted = true;
                document.getElementById(lastselseatid.id.replace('seatno', 'price')).innerText = currency + ' ' + price;
                lastselseatid = '';
            }

            for (var c = 1; c <= PaxNames.length; c++) {

                var id = document.getElementById('Seg' + segid + 'pax' + c.toString() + 'seatno');
                var priceid = document.getElementById('Seg' + segid + 'pax' + c.toString() + 'price');

                if ((id.innerText == 'SeatNo' || id.innerText == '') && Action == 'Add' && !seatalloted) {
                    id.innerText = selseat; seatalloted = true; priceid.innerText = currency + ' ' + price;
                }

                if (id.innerText == selseat && Action == 'Remove') {
                    id.innerText = ''; priceid.innerText = '';
                }

                if (id.innerText != 'SeatNo' && id.innerText != '') {
                    var seatno = tmpSelseats.indexOf('|' + id.innerText + '-S|') != -1 ? id.innerText + '-S|' : id.innerText + '-A|';
                    Selseats[segid - 1] = Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' ? Selseats[segid - 1] + seatno : seatno ;
                }
            }
        }

        function GetSeats(origin, destination) {

            $.ajax({
                type: "POST",
                url: "PassengerDetailsBySegments.aspx/GetSeatMap",
                contentType: "application/json; charset=utf-8",
                data: "{'sOrigin':'" + origin + "','sDestination':'" + destination + "','sPaxnames':'" + PaxNames.toString() + "'}",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d == '') {
                        
                        if (firstflag) {
                            $('#myModal').modal('show');
                            firstflag = false;
                        }
                        
                        $('#seatslayout').children().remove();
                        $("#seatslayout").append('<div id="seat-map"></div>');
                        $("#seat-map").append('<div class="front-indicator"><strong>No Seats Available</strong></div>');                      
                        return;
                    }
                    SeatData = JSON.parse(data.d);
                    if (origin == '' && destination == '' && data.d != '') {
                        BindSegMents(SeatData);
                        firstflag = false;
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });

            return SeatData;
        }

    </script>

    <script type="text/javascript">
        function validate() {
            var isValid = true;
            var count = '<%=(paxCount-1) %>';
            var paxdtls = '';
            PaxNames = [];
            var depDate = '<%=onwardResult.Flights[0][0].DepartureTime.ToString("dd/MM/yyyy")%>';
            var travelDate = depDate.split('/');
            var sysdate = new Date(travelDate[2], eval(travelDate[1]) - 1, travelDate[0]);
            var date = new Date(eval(document.getElementById('<%=ddlYear.ClientID %>').value), eval(document.getElementById('<%=ddlMonth.ClientID %>').value) - 1, eval(document.getElementById('<%=ddlDay.ClientID %>').value));
            var expDate = new Date(eval(document.getElementById('<%=ddlPEYear.ClientID %>').value), eval(document.getElementById('<%=ddlPEMonth.ClientID %>').value) - 1, eval(document.getElementById('<%=ddlPEDay.ClientID %>').value));
            var one_day = 1000 * 60 * 60 * 24;

            document.getElementById('title').style.display = 'none';
            document.getElementById('fname').style.display = 'none';
            document.getElementById('lname').style.display = 'none';
            <%if (GenderRequired)
        { %>
            document.getElementById('gender').style.display = 'none';
            <%} %>
            <%if (DOBRequired)
        { %>
            document.getElementById('dobDay').style.display = 'none';
            document.getElementById('dobMonth').style.display = 'none';
            document.getElementById('dobYear').style.display = 'none';
            <%} %>
            <%if (PassportNoRequired)
        { %>
            document.getElementById('passport').style.display = 'none';
            <%} %>
            <%if (PassportExpiryRequired)
        { %>
            document.getElementById('peDay').style.display = 'none';
            document.getElementById('peMonth').style.display = 'none';
            document.getElementById('peYear').style.display = 'none';
            <%} %>
            document.getElementById('mobileCountryCode').style.display = 'none';
            document.getElementById('mobile').style.display = 'none';
            <%if (NationalityRequired)
        { %>
            document.getElementById('nationality').style.display = 'none';
            <%} %>
            <%if (CountryRequired)
        { %>
            document.getElementById('country').style.display = 'none';
            <%} %>
            <%if (EmailRequired)
        { %>
            document.getElementById('email').style.display = 'none';
            <%} %>
            <%if (AddressRequired)
        { %>
            document.getElementById('address').style.display = 'none';
            <%} %>


            if (document.getElementById('<%=ddlTitle.ClientID %>').value == -1) {
                document.getElementById('title').style.display = 'block';
                document.getElementById('title').innerHTML = "Please Select Title";
                isValid = false;
            }
            else
                paxdtls = document.getElementById('<%=ddlTitle.ClientID %>').value + ' ';
            if (document.getElementById('<%=txtPaxFName.ClientID %>').value.trim().length <= 0) {
                document.getElementById('fname').style.display = 'block';
                document.getElementById('fname').innerHTML = "Please enter Passenger First Name";
                isValid = false;
            }
            else
                paxdtls = paxdtls + '-' + document.getElementById('<%=txtPaxFName.ClientID %>').value + ' ';

            if (document.getElementById('<%=txtPaxLName.ClientID %>').value.trim().length <= 0) {
                document.getElementById('lname').style.display = 'block';
                document.getElementById('lname').innerHTML = "Please enter Passenger Last Name";
                isValid = false;
            }
            else
                paxdtls = paxdtls + '-' + document.getElementById('<%=txtPaxLName.ClientID %>').value + '-A';

            if (document.getElementById('<%=txtPaxLName.ClientID %>').value.trim().length < 2) {
                document.getElementById('lname').style.display = 'block';
                document.getElementById('lname').innerHTML = "Passenger Last Name Length should be greater than one";
                isValid = false;
            }

            PaxNames.push(paxdtls); paxdtls = '';

            <%if (GenderRequired)
        { %>
            if (document.getElementById('<%=ddlGender.ClientID %>').value == "-1") {
                document.getElementById('gender').style.display = 'block';
                document.getElementById('gender').innerHTML = "Please select Gender";
                isValid = false;
            }
            <%} %>
            <%if (DOBRequired)
        { %>
            if (document.getElementById('<%=ddlDay.ClientID %>').value == "-1") {
                document.getElementById('dobDay').style.display = 'block';
                document.getElementById('dobDay').innerHTML = "Please select Birth Day";
                isValid = false;
            }
            if (document.getElementById('<%=ddlMonth.ClientID %>').value == "-1") {
                document.getElementById('dobMonth').style.display = 'block';
                document.getElementById('dobMonth').innerHTML = "Please select Birth Month";
                isValid = false;
            }
            if (document.getElementById('<%=ddlYear.ClientID %>').value == "-1") {
                document.getElementById('dobYear').style.display = 'block';
                document.getElementById('dobYear').innerHTML = "Please select Birth Year";
                isValid = false;
            }
            if (date >= sysdate) {
                document.getElementById('dobYear').style.display = 'block';
                document.getElementById('dobYear').innerHTML = "Date of Birth should be earlier than Departure Date";
                isValid = false;
            }
            if ((Math.ceil(sysdate.getTime() - date.getTime()) / one_day / 365) <= 12) {
                document.getElementById('dobYear').style.display = 'block';
                document.getElementById('dobYear').innerHTML = "Date of Birth should be greater than 12 years";
                isValid = false;
            }
            <%} %>
            <%if (PassportNoRequired)
        { %>
            if (document.getElementById('<%=txtPassportNo.ClientID %>').value.trim().length <= 0) {
                document.getElementById('passport').style.display = 'block';
                document.getElementById('passport').innerHTML = "Please enter Passport Number";
                isValid = false;
            }
            <%} %>
            <%if (PassportExpiryRequired)
        { %>
            if (document.getElementById('<%=ddlPEDay.ClientID %>').value == "-1") {
                document.getElementById('peDay').style.display = 'block';
                document.getElementById('peDay').innerHTML = "Please select Expiry Day";
                isValid = false;
            }
            if (document.getElementById('<%=ddlPEMonth.ClientID %>').value == "-1") {
                document.getElementById('peMonth').style.display = 'block';
                document.getElementById('peMonth').innerHTML = "Please select Expiry Month";
                isValid = false;
            }
            if (document.getElementById('<%=ddlPEYear.ClientID %>').value == "-1") {
                document.getElementById('peYear').style.display = 'block';
                document.getElementById('peYear').innerHTML = "Please select Expiry Year";
                isValid = false;
            }
            if (expDate <= sysdate) {
                document.getElementById('peYear').style.display = 'block';
                document.getElementById('peYear').innerHTML = "Passport expiry date should be later than departure date";
                isValid = false;
            } else if (Math.ceil((expDate.getTime() - sysdate.getTime()) / one_day) < 180) {
                document.getElementById('peYear').style.display = 'block';
                document.getElementById('peYear').innerHTML = "Passport should be valid for 6 months from the departure date";
                isValid = false;
            }
            <%} %>
            if (document.getElementById('<%=txtMobileCountryCode.ClientID %>').value.trim().length <= 0) {
                document.getElementById('mobileCountryCode').style.display = 'block';
                document.getElementById('mobileCountryCode').innerHTML = 'Please enter country code !!';
                isValid = false;
            }
            if (document.getElementById('<%=txtMobileNo.ClientID %>').value.trim().length <= 0) {
                document.getElementById('mobile').style.display = 'block';
                document.getElementById('mobile').innerHTML = "Please enter Mobile Number";
                isValid = false;
            }
//            if (document.getElementById('<%=txtMobileNo.ClientID %>').value.length <= 0) {
//                document.getElementById('mobile').style.display = 'block';
//                document.getElementById('mobile').innerHTML = 'Please enter valid phone number !!';
//                isValid = false;
//            }
//            if (!document.getElementById('<%=txtMobileNo.ClientID %>').value.indexOf("-") < 0) {
//                document.getElementById('mobile').style.display = 'block';
//                document.getElementById('mobile').innerHTML = 'Please enter country code for phone number !!';
//                isValid = false;
//            }
            <%if (NationalityRequired)
        { %>
            if (document.getElementById('<%=ddlNationality.ClientID %>').value == "-1") {
                document.getElementById('nationality').style.display = 'block';
                document.getElementById('nationality').innerHTML = "Please select Nationality";
                isValid = false;
            }
            <%} %>
            <%if (CountryRequired)
        { %>
            if (document.getElementById('<%=ddlCountry.ClientID %>').value == "-1") {
                document.getElementById('country').style.display = 'block';
                document.getElementById('country').innerHTML = "Please select Country";
                isValid = false;
            }
            <%} %>

            <%if (EmailRequired)
        { %>
            if (document.getElementById('<%=txtEmail.ClientID %>').value.trim().length <= 0) {
                document.getElementById('email').style.display = 'block';
                document.getElementById('email').innerHTML = "Please enter Email Address";
                isValid = false;
            }//checkEmail function is referred in Common.js added above
            else if (!checkEmail(document.getElementById('<%=txtEmail.ClientID %>').value.trim())) {
                document.getElementById('email').style.display = 'block';
                document.getElementById('email').innerHTML = "Please enter valid Email Address";
                isValid = false;
            }
            <%} %>
            <%if (AddressRequired)
        { %>
            if (document.getElementById('<%=txtAddress1.ClientID %>').value.trim().length <= 0) {
                document.getElementById('address').style.display = 'block';
                document.getElementById('address').innerHTML = "Please enter Address";
                isValid = false;
            }
            <%} %>

            //Added by lokesh on 24/08/2018
            //If the lead passenger supplies the GST number then need to supply company name also,it is mandatory
            //If the source is SpiceJet and origin country is india only
            <%if (onwardResult != null && onwardResult.Flights[0][0].Origin.CountryCode == "IN" && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp ||onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))
        {%>
            if (document.getElementById('<%=txtTaxRegNo_SG_GST.ClientID%>') != null && document.getElementById('<%=txtTaxRegNo_SG_GST.ClientID%>').value.trim().length > 0 && document.getElementById('<%=txtCompanyName_SG_GST.ClientID%>') != null && document.getElementById('<%=txtCompanyName_SG_GST.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstCompanyName_SG').style.display = 'block';
                document.getElementById('gstCompanyName_SG').innerHTML = "Please enter GST Company Name";
                isValid = false;
            }
         <%} %>

            <%if (returnResult != null && returnResult.Flights[0][0].Origin.CountryCode == "IN" && (returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))
        {%>
            if (document.getElementById('<%=txtTaxRegNo_SG_GST.ClientID%>') != null && document.getElementById('<%=txtTaxRegNo_SG_GST.ClientID%>').value.trim().length > 0 && document.getElementById('<%=txtCompanyName_SG_GST.ClientID%>') != null && document.getElementById('<%=txtCompanyName_SG_GST.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstCompanyName_SG').style.display = 'block';
                document.getElementById('gstCompanyName_SG').innerHTML = "Please enter GST Company Name";
                isValid = false;
            }
         <%} %>

              <%if (onwardResult != null && returnResult != null && (((onwardResult.IsGSTMandatory && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir) || (returnResult.IsGSTMandatory && returnResult.ResultBookingSource == BookingSource.TBOAir)) || ((onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI) && requiredGSTForUAPI)))
        {%>
            if (document.getElementById('<%=txtGSTCompanyAddress.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstAddress').style.display = 'block';
                document.getElementById('gstAddress').innerHTML = "Please enter GST Company Address";
                isValid = false;
            }
                <%if (onwardResult.IsGSTMandatory || returnResult.IsGSTMandatory)
        {%>
            if (document.getElementById('<%=txtGSTCompanyName.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstName').style.display = 'block';
                document.getElementById('gstName').innerHTML = "Please enter GST Company Name";
                isValid = false;
            }
                <%}%>
            if (document.getElementById('<%=txtGSTContactNumber.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstContact').style.display = 'block';
                document.getElementById('gstContact').innerHTML = "Please enter GST Company Contact Number";
                isValid = false;
            }
            if (document.getElementById('<%=txtGSTNumber.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstNumber').style.display = 'block';
                document.getElementById('gstNumber').innerHTML = "Please enter GST Registration Number";
                isValid = false;
            }
            if (document.getElementById('<%=txtGSTCompanyEmail.ClientID%>').value.trim().length <= 0) {
                document.getElementById('gstEmail').style.display = 'block';
                document.getElementById('gstEmail').innerHTML = "Please enter GST Company Email";
                isValid = false;
            }
            <%}%>


            if (eval(count) > 0) {
                for (var i = 0; i < eval(count); i++) {
                    var paxType = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblPaxType').innerHTML;
                    var dob = "dob" + i;
                    var pspExpDate = "expDate" + i;
                    var dob = new Date(eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYear').value), eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonth').value) - 1, eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDay').value));
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxtitle').style.display = 'none';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxfirstname').style.display = 'none';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxlastname').style.display = 'none';
                    <%if (GenderRequired)
        { %>
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxgender').style.display = 'none';
                    <%} %>
                    <%if (DOBRequired)
        { %>
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxday').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxmonth').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'none';
                    <%} %>
                    <%if (PassportNoRequired)
        { %>
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpassportno').style.display = 'none';
                    <%} %>
                    <%if (PassportExpiryRequired)
        { %>
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeday').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeMonth').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'none';
                    <%} %>
                    <%if (CountryRequired)
        { %>
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxcountry').style.display = 'none';
                    <%} %>

                    <%if (NationalityRequired)
        { %>
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxNationality').style.display = 'none';
                    <%} %>

                    <%if (PassportExpiryRequired)
        { %>
                //var dob = "dob" + i;
                // var pspExpDate = "expDate" + i;
                //var paxType = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblPaxType').innerHTML;
                // var dob = new Date(eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYear').value), eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonth').value) - 1, eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDay').value));
                var pspExpDate = new Date(eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEYear').value), eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEMonth').value) - 1, eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEDay').value));
                var currentDate = document.getElementById('<%=hdfCurrentDate.ClientID %>').value;

                if (currentDate != null && currentDate != "") {
                    var array = currentDate.split(',');
                    var today = new Date(parseInt(array[2]), parseInt(array[1]) - 1, parseInt(array[0]));
                    var adultAge = new Date(parseInt(array[2]) - 13, parseInt(array[1]) - 1, parseInt(array[0]));
                    var childAge = new Date(parseInt(array[2]) - 12, parseInt(array[1]) - 1, parseInt(array[0]));
                    var infantAge = new Date(parseInt(array[2]) - 2, parseInt(array[1]) - 1, parseInt(array[0]));
                }
                    <%} %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPaxTitle').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxtitle').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxtitle').innerHTML = "Please Select Title";
                        isValid = false;
                    }
                    else
                        paxdtls = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPaxTitle').value + ' ';
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxFName').value.trim().length <= 0) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxfirstname').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxfirstname').innerHTML = "Please enter First Name";
                        isValid = false;
                    }
                    else
                        paxdtls = paxdtls + '-' + document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxFName').value + ' ';
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxLName').value.trim().length <= 0) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxlastname').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxlastname').innerHTML = "Please enter Last Name";
                        isValid = false;
                    }
                    else
                        paxdtls = paxdtls + '-' + document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxLName').value;

                    if (paxType != "Infant") { paxdtls = paxType == 'Adult' ? paxdtls + '-A' : paxdtls + '-C'; PaxNames.push(paxdtls); } paxdtls = '';

                    <%if (GenderRequired)
        { %>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlGender').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxgender').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxgender').innerHTML = "Please select Gender";
                    isValid = false;
                }
                    <%} %>
                    <%if (DOBRequired)
        { %>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDay').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxday').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxday').innerHTML = "Please select Day";
                    isValid = false;
                }
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonth').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxmonth').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxmonth').innerHTML = "Please select Month";
                    isValid = false;
                }
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYear').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Please select Year";
                    isValid = false;
                }
                if (paxType == "Adult") {
                    if ((Math.ceil(sysdate.getTime() - dob.getTime()) / one_day / 365) <= 12) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Adult Age should be greater than 12 years.";
                        isValid = false;
                    }
                }
                if (paxType == "Child") {
                    if ((Math.ceil(sysdate.getTime() - dob.getTime()) / one_day / 365) < 2 || (Math.ceil(sysdate.getTime() - dob.getTime()) / one_day / 365) > 12) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Child Age should be between (2-12) years.";
                        isValid = false;
                    }
                }
                if (paxType == "Infant") {
                    if ((Math.ceil(sysdate.getTime() - dob.getTime()) / one_day / 365) <= 0 || (Math.ceil(sysdate.getTime() - dob.getTime()) / one_day / 365) > 2) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Infants Age should be less than 2 year";
                        isValid = false;
                    }
                }
                    <%} %>
                    <%if (PassportNoRequired)
        { %>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPassportNo').value.trim().length <= 0) {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpassportno').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpassportno').innerHTML = "Please enter Passport number";
                    isValid = false;
                }
                    <%} %>
                    <%if (PassportExpiryRequired)
        { %>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEDay').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeday').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeday').innerHTML = "Please select Expiry Day";
                    isValid = false;
                }
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEMonth').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeMonth').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeMonth').innerHTML = "Please select Expiry Month";
                    isValid = false;
                }
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEYear').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').innerHTML = "Please select Expiry Year";
                    isValid = false;
                }
                if (pspExpDate <= sysdate) {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').innerHTML = "Passport expiry date should be later than departure date";
                    isValid = false;
                }
                else if (Math.ceil((pspExpDate.getTime() - sysdate.getTime()) / one_day) < 180) {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').innerHTML = "Passport should be valid for 6 months from departure date";
                    isValid = false;
                }
                    <%} %>
                    <%if (CountryRequired)
        { %>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlCountry').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxcountry').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxcountry').innerHTML = "Please select Country";
                    isValid = false;
                }
                    <%} %>

                    <%if (NationalityRequired)
        { %>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value == "-1") {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxNationality').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxNationality').innerHTML = "Please select Nationality";
                    isValid = false;
                }
                    <%} %>
                }
            }
            var n = 0;
            //Modified by Lokesh : On 15May2017
            if (getElement('hdnFlexCount') != null) {

                for (var n = 0; n < parseInt(getElement('hdnFlexCount').value); n++) {
                    if (getElement('lblError' + n) != null) {
                        getElement('lblError' + n).innerHTML = '';
                    }
                    switch (getElement('hdnFlexControl' + n).value) {

                        case 'T':
                            if (getElement('hdnFlexMandatory' + n).value == "Y") {
                                if (getElement('txtFlex' + n).value == '') {
                                    getElement('lblError' + n).innerHTML = 'Please Enter ' + getElement('hdnFlexLabel' + n).value;
                                    isValid = false;
                                }
                            }
                            break;
                        case 'D':
                            if (getElement('hdnFlexMandatory' + n).value == "Y") {
                                if (getElement('ddlDay' + n).value == "-1" && getElement('ddlMonth' + n).value == "-1" && getElement('ddlYear' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Day,Month,Year';
                                    isValid = false;
                                }
                                else if (getElement('ddlDay' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Day';
                                    isValid = false;
                                }
                                else if (getElement('ddlMonth' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Month';
                                    isValid = false;
                                }
                                else if (getElement('ddlYear' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Year';
                                    isValid = false;
                                }
                            }
                            break;
                        case 'L':
                            if (getElement('hdnFlexMandatory' + n).value == "Y") {
                                if (getElement('ddlFlex' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please Select ' + getElement('hdnFlexLabel' + n).value;
                                    isValid = false;
                                }
                            }
                            break;
                    }
                }
            }



            <%if (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
        { %>
            //Set the selected index of baggage for Air Arabia.
            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            var ddlOut = document.getElementById('<%=ddlOnwardBaggage.ClientID %>');
            var ddlIn = document.getElementById('<%=ddlInwardBaggage.ClientID %>');
            //document.getElementById('<%=hdnOutBagSelection.ClientID %>').value = ddlOut.selectedIndex;            
            document.getElementById('<%=hdnOutBagSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlOnwardBaggage').select2("data").element[0].index;
            if (ddlIn != null) {
                //document.getElementById('<%=hdnInBagSelection.ClientID %>').value = ddlIn.selectedIndex;
                document.getElementById('<%=hdnInBagSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlInwardBaggage').select2("data").element[0].index;
            }
            for (i = 0; i < paxCount - 1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage');
                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage');

                document.getElementById('<%=hdnOutBagSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage').select2("data").element[0].index;
                if (inb != null) {
                    document.getElementById('<%=hdnInBagSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage').select2("data").element[0].index;
                }
            }
            <%} %>



     //For SpiceJet For corporate fares meal is mandatory
     //Added by lokesh on 4th oct,2018.
<%if (mealRequiredSG_Corporate)
        { %>

            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            if (document.getElementById('<%=ddlOnwardMeal.ClientID %>') != null &&
                document.getElementById('<%=ddlOnwardMeal.ClientID %>').value == "0") {
                if (document.getElementById('errOnwardMeal') != null) {
                    document.getElementById('errOnwardMeal').style.display = 'block';
                    document.getElementById('errOnwardMeal').innerHTML = "Please select Meal";
                }
                isValid = false;
            }
            if (document.getElementById('<%=ddlInwardMeal.ClientID %>') != null &&
                document.getElementById('<%=ddlInwardMeal.ClientID %>').value == "0") {
                if (document.getElementById('errInwardMeal') != null) {
                    document.getElementById('errInwardMeal').style.display = 'block';
                    document.getElementById('errInwardMeal').innerHTML = "Please select Meal";
                }
                isValid = false;
            }
            for (i = 0; i < paxCount - 1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');
                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');

                if (out != null && out.value == "0") {
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal') != null) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').innerHTML = "Please select Meal";
                    }
                    isValid = false;
                }
                if (inb != null && inb.value == "0") {
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal') != null) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').innerHTML = "Please select Meal";
                    }
                    isValid = false;
                }
            }
            <%} %>
if (!resetLCCMealFlag) {
 //For SpiceJet regarding meal selection addded the below logic
<%if (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp) || (returnResult != null && (returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) || (onwardResult!=null && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)))
        { %>

            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            var ddlOut = document.getElementById('<%=ddlOnwardMeal.ClientID %>');
            var ddlIn = document.getElementById('<%=ddlInwardMeal.ClientID %>');
            if (ddlOut != null) {
                document.getElementById('<%=hdnOutMealSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlOnwardMeal').select2("data").element[0].index;
            }
            else {
                document.getElementById('<%=hdnOutMealSelection.ClientID %>').value = "0";
            }
            if (ddlIn != null) {
                document.getElementById('<%=hdnInMealSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlInwardMeal').select2("data").element[0].index;
            }
            else {
                document.getElementById('<%=hdnInMealSelection.ClientID %>').value = "0";
            }
            for (i = 0; i < paxCount - 1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');
                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');
                if (out != null) {
                    document.getElementById('<%=hdnOutMealSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal').select2("data").element[0].index;
                }
                else {
                    document.getElementById('<%=hdnOutMealSelection.ClientID %>').value += "," + "0";
                }
                if (inb != null) {
                    document.getElementById('<%=hdnInMealSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal').select2("data").element[0].index;
                }
                else {
                    document.getElementById('<%=hdnInMealSelection.ClientID %>').value += "," + "0";
                }
            }
            <%} %>
}


         //Added by Lokesh on 9-sept-2017 to validate the pax additional service nationality   
     //If the onwardResult booking source is flight inventory only then validate the pax additional services
        <%if (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory)
        { %>

            var validateAddServices = validatePaxAddServices(count);
            if (validateAddServices == false) {
                document.getElementById('paxAddSerNotification').style.display = 'block';
                isValid = false;
            }
            else {
                document.getElementById('paxAddSerNotification').style.display = 'none';
            }
            <%} %>


            //Added by Lokesh on 27-Mar-2018 to Validate the GSTIN Details from the customer.


            <%if (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
        {%>
            var country = document.getElementById('<%=ddlCountry.ClientID%>');
            var selectedCountryText;
            selectedCountryText = country.options[country.selectedIndex].text;

            var originCountry = '<%=onwardResult.Flights[0][0].Origin.CountryCode%>'
            if (selectedCountryText == 'India' || originCountry == 'IN') {
                if (document.getElementById('pnlGSTDetails').style.display = 'block' && document.getElementById('<%=ddlGSTStateCode.ClientID %>').value == "-1") {
                    document.getElementById('valGSTStateCode').style.display = 'block';
                    document.getElementById('valGSTStateCode').innerText = "Please Select State Code.";
                    isValid = false;
                }
            }

            <%}%>
            if (isValid) {
                if (document.getElementById('<%=ddlGSTStateCode.ClientID %>') != null) {
                    document.getElementById('<%=hdnSelStateId.ClientID %>').value = document.getElementById('<%=ddlGSTStateCode.ClientID %>').value;
                }
            }

            //alert(isValid);
            if (isValid == true) {
                //RecalculateCost();
                //Set the flag to true,so incase of seat failure cases also the previously selected values in the hidden field will not be re-assigned.
                resetLCCMealFlag = true;
                return true;
            }
            else {
                return false;
            }
        }

        function validatePaxAddServices(Count) {

            var paxCount = eval(Count) + 1;
            var valid = true;
            var paxNationalityInvalid = 0;
            var nat = document.getElementById("<%=hdnAddNationlities.ClientID%>").value;
            var paxNat = [];
            for (var i = 0; i < paxCount; i++) {


                if (i == 0) { //Lead Pax Nationality

                    if (document.getElementById('ctl00_cphTransaction_ddlNationality').value != "-1") {

                        paxNat.push(document.getElementById('ctl00_cphTransaction_ddlNationality').value);

                    }

                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality') != null) {

                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value != "-1") {


                            paxNat.push(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value);
                        }

                    }



                }

                else { //Additional Passengers Nationality

                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality') != null) {

                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value != "-1") {


                            paxNat.push(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value);
                        }

                    }



                }




            }//EOF for loop

            if (paxNat.length > 0 && nat.length > 0) {
                for (var p = 0; p < paxNat.length; p++) {
                    if (nat.indexOf(paxNat[p]) == -1) { ++paxNationalityInvalid; }

                }

            }

            if (paxNationalityInvalid > 0) {
                valid = false;
            }
            else {

                valid = true;
            }
            return valid;

        }



        function cancelNotification() {
            document.getElementById('paxAddSerNotification').style.display = 'none';
        }

       function CheckVal(){
         var isvalid=true;
          document.getElementById('<%=txtMarkup.ClientID%>').value="0";
          if(document.getElementById('<%=rbtnPercent.ClientID%>').checked==true)
          {  
             document.getElementById('<%=txtMarkup.ClientID%>').maxLength="2";
          }
          if(document.getElementById('<%=rbtnFixed.ClientID%>').checked==true)
          {  
             document.getElementById('<%=txtMarkup.ClientID%>').maxLength="4";
          }
          return isvalid;
       }

        function CalculateBaggagePrice() {
            var source = '<%=onwardResult.ResultBookingSource.ToString() %>';
            var returnSource = '<%=(returnResult !=null ? returnResult.ResultBookingSource.ToString():"") %>';
            var decimalpoint = eval('<%=agency.DecimalValue %>');
            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            var totalFare = eval(0);
            var baggageprice = eval(0);
            var mealPrice = eval(0);
            var onPrice = eval(0);
            var retPrice = eval(0);

            var onwardMeals = eval(0);
            var returnMeals = eval(0);

            var ddlOut = document.getElementById('<%=ddlOnwardBaggage.ClientID %>');
            var ddlIn = document.getElementById('<%=ddlInwardBaggage.ClientID %>');
            var lblout = document.getElementById('<%=lblOutPrice.ClientID %>');
            var lblin = document.getElementById('<%=lblInPrice.ClientID %>');

            var ddlOutMeal = document.getElementById('<%=ddlOnwardMeal.ClientID %>');
            var ddlInMeal = document.getElementById('<%=ddlInwardMeal.ClientID %>');
            var lbloutMeal = document.getElementById('<%=lblOutMealPrice.ClientID %>');
            var lblinMeal = document.getElementById('<%=lblInMealPrice.ClientID %>');




            var onTotalPrice = eval('<%=onwardTotal.ToString().Replace(",", "") %>');
            var inTotalPrice = eval('<%=returnTotal.ToString().Replace(",", "") %>');
            var onPubPrice = eval('<%=onwardPubFare.ToString().Replace(",", "")%>');
            var inPubPrice = eval('<%=returnPubFare.ToString().Replace(",", "")%>');
            var totalPub = eval('<%=totalPubFare.ToString().Replace(",", "")%>');

            if (ddlOut != null && ddlOut.value != "No Bag") {
                totalFare += eval(ddlOut.value);
                baggageprice += eval(ddlOut.value);
                onPrice += eval(ddlOut.value);
                if (lblout != null) {
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblOutPrice.ClientID %>').value = parseFloat(eval(ddlOut.value)).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblOutPrice.ClientID %>').value = parseFloat(eval(ddlOut.value)).toFixed(decimalpoint);
                    }
                    document.getElementById('ctl00_cphTransaction_lblOnwardBaggage').value = parseFloat(onPrice).toFixed(decimalpoint);
                }
            }

            if (ddlOutMeal != null && ddlOutMeal.value != "No Meal") {
                totalFare += eval(ddlOutMeal.value);
                mealPrice += eval(ddlOutMeal.value);
                onwardMeals += eval(ddlOutMeal.value);
                if (lbloutMeal != null) {
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblOutMealPrice.ClientID %>').value = parseFloat(eval(ddlOutMeal.value)).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblOutMealPrice.ClientID %>').value = parseFloat(eval(ddlOutMeal.value)).toFixed(decimalpoint);
                    }
                    if (document.getElementById('<%=lblOnwardMeal.ClientID%>') != null) {
                        document.getElementById('<%=lblOnwardMeal.ClientID%>').value = parseFloat(onwardMeals).toFixed(decimalpoint);
                    }
                }
            }



            if (ddlIn != null && ddlIn.value != "No Bag") {
                totalFare += eval(ddlIn.value);
                baggageprice += eval(ddlIn.value);
                retPrice += eval(ddlIn.value);
                if (lblin != null) {
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblInPrice.ClientID %>').value = parseFloat(eval(ddlIn.value)).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblInPrice.ClientID %>').value = parseFloat(eval(ddlIn.value)).toFixed(decimalpoint);
                    }
                    document.getElementById('ctl00_cphTransaction_lblReturnBaggage').value = parseFloat(retPrice).toFixed(decimalpoint);
                }
            }

            if (ddlInMeal != null && ddlInMeal.value != "No Meal") {
                totalFare += eval(ddlInMeal.value);
                mealPrice += eval(ddlInMeal.value);
                returnMeals += eval(ddlInMeal.value);
                if (lblinMeal != null) {
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblInMealPrice.ClientID %>').value = parseFloat(eval(ddlInMeal.value)).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblInMealPrice.ClientID %>').value = parseFloat(eval(ddlInMeal.value)).toFixed(decimalpoint);
                    }

                    if (document.getElementById('<%=lblReturnMeal.ClientID%>') != null) {
                        document.getElementById('<%=lblReturnMeal.ClientID%>').value = parseFloat(returnMeals).toFixed(decimalpoint);
                    }
                }
            }

            for (i = 0; i < paxCount - 1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage');

                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage');


                var outMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');

                var inbMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');


                var lblout = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice');
                var lblin = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice');

                var lbloutMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice');
                var lblinMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice');

                if (out != null && out.value != "No Bag") {
                    totalFare += eval(out.value);
                    baggageprice += eval(out.value);
                    onPrice += eval(out.value);
                    if (lblout != null) {
                        if (window.navigator.appCodeName == "Mozilla") {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice').value = parseFloat(eval(out.value)).toFixed(decimalpoint);
                            }
                        }
                        else {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice').value = parseFloat(eval(out.value)).toFixed(decimalpoint);
                            }
                        }
                        if (document.getElementById('ctl00_cphTransaction_lblOnwardBaggage') != null) {
                            document.getElementById('ctl00_cphTransaction_lblOnwardBaggage').value = parseFloat(onPrice).toFixed(decimalpoint);
                        }
                    }
                }

                if (outMeal != null && outMeal.value != "No Meal") {
                    totalFare += eval(outMeal.value);
                    mealPrice += eval(outMeal.value);
                    onwardMeals += eval(outMeal.value);
                    if (lbloutMeal != null) {
                        if (window.navigator.appCodeName == "Mozilla") {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').value = parseFloat(eval(outMeal.value)).toFixed(decimalpoint);
                            }
                        }
                        else {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').value = parseFloat(eval(outMeal.value)).toFixed(decimalpoint);
                            }
                        }
                        if (document.getElementById('<%=lblOnwardMeal.ClientID%>') != null) {
                            document.getElementById('<%=lblOnwardMeal.ClientID%>').value = parseFloat(onwardMeals).toFixed(decimalpoint);
                        }
                    }
                }

                if (inb != null && inb.value != "No Bag") {
                    totalFare += eval(inb.value);
                    baggageprice += eval(inb.value);
                    retPrice += eval(inb.value);
                    if (lblin != null) {
                        if (window.navigator.appCodeName == "Mozilla") {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice').value = parseFloat(eval(inb.value)).toFixed(decimalpoint);
                            }
                        }
                        else {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice').value = parseFloat(eval(inb.value)).toFixed(decimalpoint);
                            }
                        
                        }
                        if (document.getElementById('ctl00_cphTransaction_lblReturnBaggage') != null) {
                            document.getElementById('ctl00_cphTransaction_lblReturnBaggage').value = parseFloat(retPrice).toFixed(decimalpoint);
                        }

                    }
                }

                if (inbMeal != null && inbMeal.value != "No Meal") {
                    totalFare += eval(inbMeal.value);
                    mealPrice += eval(inbMeal.value);
                    returnMeals += eval(inbMeal.value);
                    if (lblinMeal != null) {
                        if (window.navigator.appCodeName == "Mozilla") {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').value = parseFloat(eval(inbMeal.value)).toFixed(decimalpoint);
                            }
                        }
                        else {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice') != null) {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').value = parseFloat(eval(inbMeal.value)).toFixed(decimalpoint);
                            }
                        }

                        if (document.getElementById('<%=lblReturnMeal.ClientID%>') != null) {
                            document.getElementById('<%=lblReturnMeal.ClientID%>').value = parseFloat(returnMeals).toFixed(decimalpoint);
                        }
                    }
                }
            }


            var onwardDiscount = eval(0);
            if (document.getElementById('ctl00_cphTransaction_lblOnwardDiscount')) {
                onwardDiscount = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardDiscount').value.replace(',', '').replace('(-)', '')));
            }
            var returnDiscount = eval(0);
            if (document.getElementById('ctl00_cphTransaction_lblReturnDiscount')) {
                returnDiscount = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblReturnDiscount').value.replace(',', '').replace('(-)', '')));
            }
            //Page level markup defined
            if (document.getElementById('ctl00_cphTransaction_hdfAsvAmount').value != null && document.getElementById('ctl00_cphTransaction_hdfAsvAmount').value != "0") {

                var onwardTotal = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', '')));
                var returnTotal = 0;

                if (document.getElementById('ctl00_cphTransaction_lblReturnTotal') != null) {
                  returnTotal = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblReturnTotal').value.replace(',', '')));
                }
                //Onward
                if (source == 'TBOAir') {

                    document.getElementById('<%=lblOnwardGTotal.ClientID %>').value = Math.ceil(eval(onwardTotal) + eval(onPrice)).toFixed(decimalpoint);
                    document.getElementById('<%=lblOnwardPubFare.ClientID %>').value = Math.ceil(eval(onwardTotal) + eval(onwardDiscount) + eval(onPrice)).toFixed(decimalpoint);
                }
                else {//Other Sources
                    document.getElementById('<%=lblOnwardGTotal.ClientID %>').value = parseFloat(eval(onwardTotal) + eval(onPrice) + eval(onwardMeals)).toFixed(decimalpoint);
                    document.getElementById('<%=lblOnwardPubFare.ClientID %>').value = parseFloat(eval(onwardTotal) + eval(onwardDiscount) + eval(onPrice) + eval(onwardMeals)).toFixed(decimalpoint);
                }
                //Return
                if (returnSource != '') {
                    if (returnSource == 'TBOAir') {

                        document.getElementById('<%=lblReturnGTotal.ClientID %>').value = Math.ceil(eval(returnTotal) + eval(retPrice)).toFixed(decimalpoint);
                        document.getElementById('<%=lblReturnPubFare.ClientID %>').value = Math.ceil(eval(returnTotal) + eval(returnDiscount) + eval(retPrice)).toFixed(decimalpoint);
                    }
                    else {//Other Sources
                        document.getElementById('<%=lblReturnGTotal.ClientID %>').value = parseFloat(eval(returnTotal) + eval(retPrice) + eval(returnMeals)).toFixed(decimalpoint);
                        document.getElementById('<%=lblReturnPubFare.ClientID %>').value = parseFloat(eval(returnTotal) + eval(retPrice) + eval(returnMeals) + eval(returnDiscount)).toFixed(decimalpoint);
                    }
                }
            }
            else {//No Page Level Markup 

                //Onward
                if (source == 'TBOAir') {

                    document.getElementById('<%=lblOnwardGTotal.ClientID %>').value = Math.ceil(parseFloat(eval(onTotalPrice) + eval(onPrice))).toFixed(decimalpoint);
                    document.getElementById('<%=lblOnwardPubFare.ClientID %>').value = Math.ceil(parseFloat(eval(onPubPrice) + eval(onPrice))).toFixed(decimalpoint);
                }
                else {//Other Sources
                    document.getElementById('<%=lblOnwardGTotal.ClientID %>').value = parseFloat(eval(onTotalPrice) + eval(onPrice) + eval(onwardMeals)).toFixed(decimalpoint);
                    document.getElementById('<%=lblOnwardPubFare.ClientID %>').value = parseFloat(eval(onPubPrice) + eval(onwardDiscount) + eval(onwardMeals) + eval(onPrice)).toFixed(decimalpoint);
                }
                //Return
                if (returnSource != '') {
                    if (returnSource == 'TBOAir') {
                        document.getElementById('<%=lblReturnGTotal.ClientID %>').value = Math.ceil(parseFloat(eval(inTotalPrice) + eval(retPrice))).toFixed(decimalpoint);
                        document.getElementById('<%=lblReturnPubFare.ClientID %>').value = Math.ceil(parseFloat(eval(inPubPrice) + eval(retPrice))).toFixed(decimalpoint);
                    }
                    else {//Other Sources
                        document.getElementById('<%=lblReturnGTotal.ClientID %>').value = parseFloat(eval(inTotalPrice) + eval(retPrice) + eval(returnMeals)).toFixed(decimalpoint);
                        document.getElementById('<%=lblReturnPubFare.ClientID %>').value = parseFloat(eval(inPubPrice) + eval(returnMeals) + eval(returnDiscount) + eval(retPrice)).toFixed(decimalpoint);
                    }
                }
            }
            if (document.getElementById('ctl00_cphTransaction_lblReturnPubFare') != null) {
                document.getElementById('<%=lblTotalPubFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardPubFare').value.replace(',', '')) + eval(document.getElementById('ctl00_cphTransaction_lblReturnPubFare').value.replace(',', ''))).toFixed(decimalpoint);
            }
            else {
                document.getElementById('<%=lblTotalPubFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardPubFare').value.replace(',', ''))).toFixed(decimalpoint);
            }
            var airPrice = eval(0);
            airPrice = document.getElementById('<%=lblTotalFare.ClientID %>').value;
            airPrice = airPrice.replace(',', '');
            if (source != '' && returnSource != '') {//Round Trip
                if (source == "TBOAir" && returnSource == "TBOAir") 
                {
                    document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" + Math.ceil(parseFloat((eval(airPrice) + totalFare))).toFixed(decimalpoint);
                }

                else {
                    document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" + parseFloat(eval(eval(airPrice) + totalFare)).toFixed(decimalpoint);
                }
            }
            else {//ONEWAY

                if (source == "TBOAir") 
                {
                    document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" + Math.ceil(parseFloat((eval(airPrice) + totalFare))).toFixed(decimalpoint);
                }

                else {
                    document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" + parseFloat(eval(eval(airPrice) + totalFare)).toFixed(decimalpoint);
                }

            }
            if (document.getElementById('<%=lblBaggageAmt.ClientID %>') != null) {
                document.getElementById('<%=lblBaggageAmt.ClientID %>').value = parseFloat(baggageprice).toFixed(decimalpoint);
            }
            if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
                document.getElementById('<%=lblMealAmt.ClientID %>').value = parseFloat(mealPrice).toFixed(decimalpoint);
            }

        }
        function TitleChanged(title, i) {

            var ddlTitle, ddlGender;
            ddlTitle = document.getElementById(title);
            if (i >= 0) {
                ddlGender = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlGender');
            }
            else {
                ddlGender = document.getElementById('<%=ddlGender.ClientID %>');
            }
            if (ddlTitle.selectedIndex == 1) {
                ddlGender.selectedIndex = 1;
            }
            else if (ddlTitle.selectedIndex == 2) {
                ddlGender.selectedIndex = 2;
            }
            else if (ddlTitle.selectedIndex == 3 || ddlTitle.selectedIndex == 0) {
                ddlGender.selectedIndex = 0;
            }

        }

        function RecalculateCost() {
            <%
        decimal onmarkUp = 0, ondiscount = 0, onbaseFare = 0, ontax = 0, onk3tax=0;
        if (onwardResult != null)
        {
            for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
            {
                onmarkUp += Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                ondiscount += Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                onbaseFare += Convert.ToDecimal(onwardResult.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(onwardResult.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                ontax += Convert.ToDecimal(onwardResult.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
            }
            onk3tax= Convert.ToDecimal(onwardResult.Price.K3Tax.ToString("N" + agency.DecimalValue));
            
        }

        decimal retmarkUp = 0, retdiscount = 0, retbasefare = 0, rettax = 0;
        if (returnResult != null)
        {
            for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
            {
                retmarkUp += Convert.ToDecimal(returnResult.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                retdiscount += Convert.ToDecimal(returnResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                retbasefare += Convert.ToDecimal(returnResult.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(returnResult.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                rettax += Convert.ToDecimal(returnResult.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
            }
        }

            %>
            var source = '<%=onwardResult.ResultBookingSource.ToString() %>';
            var returnSource = '<%=(returnResult !=null ? returnResult.ResultBookingSource.ToString():"") %>';
            var asvAmount = 0;
            var decimalpoint = eval('<%=agency.DecimalValue %>');
            var onWardBaseFare = 0;
            var returnBaseFare = 0;
             <%if (onwardResult != null && returnResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir)
        {%> 
            onWardBaseFare = eval('<%=Math.Ceiling(Math.Round(onbaseFare, agency.DecimalValue)) %>');
            returnBaseFare = eval('<%=Math.Round(retbasefare, agency.DecimalValue) %>');
           <%}%>
            <%else if (onwardResult != null && returnResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir && returnResult.ResultBookingSource != CT.BookingEngine.BookingSource.TBOAir)
        {%>
            onWardBaseFare = eval('<%=Math.Ceiling(Math.Round(onbaseFare, agency.DecimalValue)) %>');
            returnBaseFare = eval('<%=Math.Round(retbasefare, agency.DecimalValue) %>');
           <%}%>
            <%else if (onwardResult != null && returnResult != null && onwardResult.ResultBookingSource != CT.BookingEngine.BookingSource.TBOAir && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir)
        {%>
            onWardBaseFare = eval('<%=Math.Round(onbaseFare, agency.DecimalValue) %>');
            returnBaseFare = eval('<%=Math.Ceiling(Math.Round(retbasefare, agency.DecimalValue)) %>');
           <%}%>
            <%else%>
            <%{%>

            onWardBaseFare = eval('<%=Math.Round(onbaseFare, agency.DecimalValue) %>');
            returnBaseFare = eval('<%=Math.Round(retbasefare, agency.DecimalValue) %>');
            <%}%>

            var baseFare = onWardBaseFare + returnBaseFare;

          <%--  //var baseFare = (eval('<%=Math.Ceiling(Math.Round(onbaseFare, agency.DecimalValue) + Math.Round(retbasefare, agency.DecimalValue)) %>'));
          --%>  
            var Tax = (eval('<%=(Math.Round((ontax + onwardResult.Price.OtherCharges + onwardResult.Price.AdditionalTxnFee + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee + onmarkUp), agency.DecimalValue) + Math.Round((rettax + (returnResult != null? returnResult.Price.OtherCharges:0) + (returnResult != null?returnResult.Price.AdditionalTxnFee:0) + (returnResult != null? returnResult.Price.SServiceFee:0) + (returnResult != null ?returnResult.Price.TransactionFee:0) + retmarkUp), agency.DecimalValue)) %>'));
            var onTax = parseFloat(eval('<%=Math.Round((ontax + (onwardResult.Price.OtherCharges + onwardResult.Price.AdditionalTxnFee + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee + onmarkUp)), agency.DecimalValue) %>')).toFixed(decimalpoint);
            var onFare = parseFloat(eval('<%=Math.Round(onbaseFare, agency.DecimalValue)%>')).toFixed(decimalpoint);
            var onBaggage = eval(document.getElementById('ctl00_cphTransaction_lblOnwardBaggage').value.replace(',', ''));
            onFare = parseFloat(eval(onFare)).toFixed(decimalpoint);
            var onk3Tax=parseFloat(eval(<%=onk3tax%>));
            var onMeal = 0;
            if (source != 'TBOAir' && source != 'UAPI' && document.getElementById('ctl00_cphTransaction_lblOnwardMeal') != null)
                onMeal = document.getElementById('ctl00_cphTransaction_lblOnwardMeal').value != '' ? eval(document.getElementById('ctl00_cphTransaction_lblOnwardMeal').value.replace(',', '')) : onMeal;

            if (document.getElementById('<%=rbtnFare.ClientID %>').checked == true) {
                document.getElementById('<%=hdfAsvElement.ClientID %>').value = "BF";
                var markupvalue = document.getElementById('<%=txtMarkup.ClientID %>').value;
                var markup = 0;
                if (eval(markupvalue)==0)
                {
                    document.getElementById('<%=txtMarkup.ClientID %>').value = 0;
                }
                else if (markupvalue.startsWith("0"))
                {
                    markup = eval(document.getElementById('<%=txtMarkup.ClientID %>').value.replace(/\b0+/g, ""));
                    document.getElementById('<%=txtMarkup.ClientID %>').value = markup;
                }
                else
                {
                    markup = eval(document.getElementById('<%=txtMarkup.ClientID %>').value);
                }
                if (markup == null || markup == undefined) markup = 0;
                if (baseFare > 0) {
                    baseFare=parseFloat(baseFare.toFixed(decimalpoint));

                    if (document.getElementById('<%=rbtnPercent.ClientID %>').checked == true) {
                        asvAmount += (baseFare * (markup / 100));
                        baseFare = baseFare + (baseFare * (markup / 100));
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "P";
                        asvAmount=parseFloat(asvAmount.toFixed(decimalpoint));
                        onFare = eval(onFare) + eval(asvAmount);
                    }
                    else if (document.getElementById('<%=rbtnFixed.ClientID %>').checked == true) {
                        asvAmount += markup;
                        baseFare = eval(baseFare) + eval(markup);
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "F";
                        onFare = eval(onFare) + eval(markup);
                    }
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblBaseFare.ClientID %>').value = parseFloat(baseFare).toFixed(decimalpoint);
                        document.getElementById('ctl00_cphTransaction_lblOnwardBaseFare').value = parseFloat(onFare).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblBaseFare.ClientID %>').value = parseFloat(baseFare).toFixed(decimalpoint);
                        document.getElementById('ctl00_cphTransaction_lblOnwardBaseFare').value = parseFloat(onFare).toFixed(decimalpoint);
                    }
                    document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax).toFixed(decimalpoint);
                    document.getElementById('ctl00_cphTransaction_lblOnwardTax').value = parseFloat(onTax - onk3Tax).toFixed(decimalpoint);
                }
            }
            else if (document.getElementById('<%=rbtnTax.ClientID %>').checked == true) {
                document.getElementById('<%=hdfAsvElement.ClientID %>').value = "TF";
                var markupvalue = document.getElementById('<%=txtMarkup.ClientID %>').value;
                var markup = 0;
                if (eval(markupvalue)==0)
                {
                    document.getElementById('<%=txtMarkup.ClientID %>').value = 0;
                }
                else if (markupvalue.startsWith("0"))
                {
                    markup = eval(document.getElementById('<%=txtMarkup.ClientID %>').value.replace(/\b0+/g, ""));
                    document.getElementById('<%=txtMarkup.ClientID %>').value = markup;
                }
                else
                {
                    markup = eval(document.getElementById('<%=txtMarkup.ClientID %>').value);
                }
                if (Tax > 0) {
                    if (document.getElementById('<%=rbtnPercent.ClientID %>').checked == true) {
                        asvAmount += (Tax * (markup / 100));
                        Tax = Tax + (Tax * (markup / 100));
                        asvAmount=parseFloat(asvAmount.toFixed(decimalpoint));
                        onTax = eval(onTax) + eval(asvAmount);
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "P";
                    }
                    else if (document.getElementById('<%=rbtnFixed.ClientID %>').checked == true) {
                        asvAmount += markup;
                        Tax = eval(Tax) + eval(markup);                      
                        onTax = eval(onTax) + eval(markup);
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "F";
                    }
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax - onk3Tax).toFixed(decimalpoint);
                        document.getElementById('ctl00_cphTransaction_lblOnwardTax').value = parseFloat(onTax - onk3Tax).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax - onk3Tax).toFixed(decimalpoint);
                        document.getElementById('ctl00_cphTransaction_lblOnwardTax').value = parseFloat(onTax-onk3Tax).toFixed(decimalpoint);
                    }
                    document.getElementById('<%=lblBaseFare.ClientID %>').value = parseFloat(baseFare).toFixed(decimalpoint);
                    document.getElementById('ctl00_cphTransaction_lblOnwardBaseFare').value = parseFloat(onFare).toFixed(decimalpoint);
                }
            }
            document.getElementById('<%=hdfAsvAmount.ClientID %>').value = asvAmount;
            //Re-Calculate Total Fare
            var fare = baseFare; //eval(document.getElementById('<%=lblBaseFare.ClientID %>').value);
            var Taxes = Tax; //eval(document.getElementById('<%=lblTax.ClientID %>').value);
            var baggage = eval(document.getElementById('<%=lblBaggageAmt.ClientID %>').value.replace(',', ''));
            var meal = eval(0);
            if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
                meal = document.getElementById('<%=lblMealAmt.ClientID %>').value != '' ? eval(document.getElementById('<%=lblMealAmt.ClientID %>').value.replace(',', '')) : meal;
            }
            var discount = eval(0);
            if (document.getElementById('<%=lblDiscount.ClientID %>') != null) {
                discount = eval(document.getElementById('<%=lblDiscount.ClientID %>').value.replace('(-)', '').replace(',', ''));
            }
            var total = eval(0);
            if (baggage > 0) {
                total = eval(fare + Taxes + baggage) - discount;
            }
            else {
                total = eval(fare + Taxes) - discount;
            }
            if (meal > 0) {
                if (baggage > 0) {
                    total = eval(fare + Taxes + baggage + meal) - discount;
                }
                else {
                    total = eval(fare + Taxes + meal) - discount;
                }
            }
            onFare= parseFloat(eval(onFare)).toFixed(decimalpoint);
            onTax=parseFloat(eval(onTax)).toFixed(decimalpoint);
            
            var discount=parseFloat(eval(<%=ondiscount%>).toFixed(decimalpoint));
            if (source == 'TBOAir') {
                
                document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value = parseFloat(eval(onFare) + eval(onTax) - eval(discount)).toFixed(decimalpoint);
                document.getElementById('ctl00_cphTransaction_lblOnwardGTotal').value = Math.ceil(eval(onFare) + eval(onTax) + eval(onBaggage) - eval(discount).toFixed(decimalpoint)).toFixed(decimalpoint);
                document.getElementById('ctl00_cphTransaction_lblOnwardPubFare').value = Math.ceil(parseFloat(eval(onFare) + eval(onTax) + eval(onBaggage))).toFixed(decimalpoint);
            }
            else {
               
                document.getElementById('ctl00_cphTransaction_lblOnwardPubFare').value = parseFloat(eval(onFare) + eval(onTax) + eval(onBaggage) + eval(onMeal)).toFixed(decimalpoint);
                document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value = parseFloat(eval(onFare) + eval(onTax) - eval(discount)).toFixed(decimalpoint);
                 document.getElementById('ctl00_cphTransaction_lblOnwardGTotal').value = parseFloat(eval(onFare) + eval(onTax) + eval(onBaggage) + eval(onMeal) - eval(discount)).toFixed(decimalpoint);
            }
 
            if (source != '' && returnSource != '') {//For Round Trip
                if (source == 'TBOAir' && returnSource == 'TBOAir') {
                    document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(Math.ceil(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', ''))) + Math.ceil(eval(document.getElementById('ctl00_cphTransaction_lblReturnTotal').value.replace(',', '')))).toFixed(decimalpoint);
                }
                else if (source == 'TBOAir' && returnSource != 'TBOAir') {
                    document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(Math.ceil(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', ''))) + eval(document.getElementById('ctl00_cphTransaction_lblReturnTotal').value.replace(',', ''))).toFixed(decimalpoint);
                }
                else if (source != 'TBOAir' && returnSource == 'TBOAir') {
                    document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', '')) + Math.ceil(eval(document.getElementById('ctl00_cphTransaction_lblReturnTotal').value.replace(',', '')))).toFixed(decimalpoint);
                }
                else {
                    document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', '')) + eval(document.getElementById('ctl00_cphTransaction_lblReturnTotal').value.replace(',', ''))).toFixed(decimalpoint);
                }

                document.getElementById('<%=lblTotalPrice.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardGTotal').value.replace(',', '')) + eval(document.getElementById('ctl00_cphTransaction_lblReturnGTotal').value.replace(',', ''))).toFixed(decimalpoint);
                document.getElementById('<%=lblTotalPubFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardPubFare').value.replace(',', '')) + eval(document.getElementById('ctl00_cphTransaction_lblReturnPubFare').value.replace(',', ''))).toFixed(decimalpoint);

            }
            else {//ONEWAY

                if (source == 'TBOAir') {
                    document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(Math.ceil(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', '')))).toFixed(decimalpoint);
                }
                else {
                    document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardTotal').value.replace(',', ''))).toFixed(decimalpoint);
                }
                document.getElementById('<%=lblTotalPrice.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardGTotal').value.replace(',', ''))).toFixed(decimalpoint);
                document.getElementById('<%=lblTotalPubFare.ClientID %>').value = parseFloat(eval(document.getElementById('ctl00_cphTransaction_lblOnwardPubFare').value.replace(',', ''))).toFixed(decimalpoint);
            }
            
            return false;
        }
        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0') {
                document.getElementById(id).value = '';
            }
        }
        function checkingNumber(id) {
            var clipboardData, pastedData;
            clipboardData = id.clipboardData || window.clipboardData;//clipboardData is the cache memory of browser
            pastedData = clipboardData.getData('Text');

            for (var i = 0; i < pastedData.length; i++) {
                var charCode = pastedData.charCodeAt(i);
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    document.getElementById('<%=lblTotalPrice.ClientID %>').value = "";
                alert("Please enter valid phone number");
                return false;
            }
        }
        if (pastedData.length > 10) {
            document.getElementById('<%=lblTotalPrice.ClientID %>').value = "";
                alert("Please enter valid phone number");
                return false;
            }
            else {
                return true;
            }
        }
        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0') {
                document.getElementById(id).value = '0';
            }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

    <%--<script type="text/javascript">
        function WindowLoad() {

            var paxCount = eval('<%=request.AdultCount + request.ChildCount + request.InfantCount %>');
            if (document.getElementById('<%= lblOutPrice.ClientID %>')) document.getElementById('<%= lblOutPrice.ClientID %>').disabled = true;
            if (document.getElementById('<%= lblOutMealPrice.ClientID %>')) document.getElementById('<%= lblOutMealPrice.ClientID %>').disabled = true;


           <%if (request.Type == CT.BookingEngine.SearchType.Return)

        {%>
            if (document.getElementById('<%= lblInPrice.ClientID %>')) document.getElementById('<%= lblInPrice.ClientID %>').disabled = true;
            if (document.getElementById('<%= lblInMealPrice.ClientID %>')) document.getElementById('<%= lblInMealPrice.ClientID %>').disabled = true;
           <%} %>

            document.getElementById('<%= lblBaggageAmt.ClientID %>').disabled = true;
            if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
                document.getElementById('<%= lblMealAmt.ClientID %>').disabled = true;
            }
            document.getElementById('<%= lblTotalPrice.ClientID %>').disabled = true;
            document.getElementById('<%= lblBaseFare.ClientID %>').disabled = true;
            document.getElementById('<%= lblTax.ClientID %>').disabled = true;
            document.getElementById('<%= lblTotalFare.ClientID %>').disabled = true;
            for (i = 0; i < paxCount - 1; i++) {
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice').disabled = true;
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').disabled = true;
                <%if (request.Type == CT.BookingEngine.SearchType.Return)

        {%>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice').disabled = true;
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').disabled = true;
                <%} %> 

            }
            CalculateBaggagePrice();
        }
        window.onload = WindowLoad;
    </script>--%>

    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
            return ret;
        }
        function PriceContinue() {
            //document.getElementById('FareDiff').style.display = 'none';
            $('#FareDiff').modal('hide')
            document.getElementById('<%=imgContinue.ClientID %>').style.display = 'block';
            document.getElementById('priceChange').style.display = 'none';
            document.getElementById('<%=hdfOnwardFareAction.ClientID %>').value = "Update";
            document.getElementById('<%=hdfReturnFareAction.ClientID %>').value = "Update";
            document.forms[0].submit();
        }

        function PriceCancel() {
            //document.getElementById('FareDiff').style.display = 'none';
            $('#FareDiff').modal('hide')
            window.location.href = 'HotelSearch.aspx?source=Flight';
        }

        //This function will execute when there are any mandatory fields in AddMoreDetails Section.
        //Mandatory Fields may be Gender,Passport Expiry ,Passport Number,Nationality,Country or Address Fields.
        function expandMoreDetails() {
            $('.advPax-collapse-btn').trigger('click');
        }



        $(function () {
            //            var BaggageInputCtrl = $('.baggage-input-ctrl');

            //            $('.baggage-wrapper').hide();

            //            BaggageInputCtrl.each(function () {
            //                var $this = $(this);
            //                
            //                if ($(this).length) {
            //                    $this.closest('.baggage-wrapper').show();
            //                }
            //            })   
            //            


            //var collapseBtn = $('.advPax-collapse-btn[data-toggle="collapse"]'),
            //    hash = 0;
            //collapseBtn.each(function () {
            //    $(this).attr('href', '#paggengerDetailsCollapse-' + (++hash));
            //})
            //hash = 0;
            //$('.advPx-collapseContent').each(function () {
            //    $(this).attr('id', 'paggengerDetailsCollapse-' + ((++hash)))
            //})

            //Call the below function if there are any mandatory fields in AddMoreDetails Section.
           //Mandatory Fields may be Gender,Passport Expiry ,Passport Number,Nationality,Country or Address Fields.

            <%if (GenderRequired || PassportNoRequired || PassportExpiryRequired || NationalityRequired || CountryRequired || AddressRequired)
        {%>

            expandMoreDetails(); //This function will automatically expand the add more details section if there are any mandatory fields for better user experience.                    
            <%} %>
            //If isGSTMandatory returned in FareQuote response for TBO then allow GST fields mandatory
           
        <%if ((onwardResult != null && (onwardResult.IsGSTMandatory && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI ) && requiredGSTForUAPI) || (returnResult != null && (returnResult.IsGSTMandatory && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI ) && requiredGSTForUAPI))
        { %>
            document.getElementById('divGSTFields').style.display = 'block';
         <%}%>
        })


    </script>


    <script type="text/javascript">
        //Added by Lokesh on 28Mar2018 To get the GST State Id's
        //This is only for G9 source .
        //Only For Lead Pax.
        //If the customer is travelling from India only.
        $(document).ready(function () {
             <%if (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia && onwardResult.Flights[0][0].Origin.CountryCode == "IN") %>
           <% {%>
            GetStatesInfo();
            document.getElementById('pnlGSTDetails').style.display = 'block';
            document.getElementById('<%=lblG9BaggageAlertLeadPax.ClientID%>').style.display = 'block';
            <% }%>
            <%else%>
           <% {%>
            document.getElementById('pnlGSTDetails').style.display = 'none';
            var G9Baggage = document.getElementById('<%=lblG9BaggageAlertLeadPax.ClientID%>');
            if (G9Baggage != null)
                document.getElementById('<%=lblG9BaggageAlertLeadPax.ClientID%>').style.display = 'none';
            <% }%>

            <%if (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) %>
            <% {%>
            if (document.getElementById('pnlGSTDetails').style.display == 'none') {
                var country = document.getElementById('ctl00_cphTransaction_ddlCountry');
                var selectedCountryText = country.options[country.selectedIndex].text;
                if (selectedCountryText == 'India') {
                    document.getElementById('pnlGSTDetails').style.display = 'block';
                    GetStatesInfo();
                }
                else {
                    document.getElementById('pnlGSTDetails').style.display = 'none';
                }
            }
            <% }%>
            if (getElement('hdnFlexCount').value > '0') {
                document.getElementById('divFlex').style.display = 'block';
                <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate !="Y") %>
            <% {%>
                if (document.getElementById('divFlexAd0') != null) {
                    var flexcnt = '<%=(paxCount-1) %>';
                    for (var i = 0; i < flexcnt; i++) {
                        document.getElementById('divFlexAd' + i).style.display = 'block';
                    }
                }
                  <% }%>
            }
        })

        /* To prevent double click of controls in the page */

        document.addEventListener( 'dblclick', function(event) {  
            event.preventDefault();  
            event.stopPropagation(); 
          },  true //capturing phase!!
        );

    </script>

    <script type="text/javascript">
        //Added by Lokesh on 28Mar2018 To get the GST State Id's
        //This is only for G9 source .
        //Only For Lead Pax.
        //If the customer is travelling from India only.
        var Ajax; //New Ajax object.
        var gstStateId;
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function GetStatesInfo() {
            gstStateId = 'ctl00_cphTransaction_ddlGSTStateCode';
            var paramList = 'requestSource=getStates&id=' + gstStateId + '&countryCode=IN';
            var url = "CityAjax.aspx";
            Ajax.onreadystatechange = BindGSTStateId;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        //Added by lokesh on 6-April-2018
        //This function displays the GST Input fields
        //If the lead pax country of residence is india.
        //If the onwardResult booking source is G9 Only
        function ShowHideGSTDetails(id) {
             <%if (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) %>
           <% {%>
            var country = document.getElementById(id);
            var selectedCountryText = country.options[country.selectedIndex].text;
            if (selectedCountryText == 'India') {
                document.getElementById('pnlGSTDetails').style.display = 'block';
                GetStatesInfo();
            }
            else {
                document.getElementById('pnlGSTDetails').style.display = 'none';
                document.getElementById('<%=ddlGSTStateCode.ClientID %>').selectedIndex = -1;
                document.getElementById('<%=hdnSelStateId.ClientID %>').value = '';

            }

            <% }%>

            <%else%>
            <% {%>
            document.getElementById('pnlGSTDetails').style.display = 'none';
            <% }%>
        }

        //Ajax call which brings complete state id's
        function BindGSTStateId(response) {
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
                gstStateId = Ajax.responseText.split('#')[0];
                var ddl = document.getElementById(gstStateId);
                if (ddl != null) {
                    //ddl.options.length = 0;
                    //var el = document.createElement("option");
                    //el.textContent = "Select State Id";
                    //el.value = "-1";
                    //ddl.add(el, 0);
                    var values = Ajax.responseText.split('#')[1].split(',');
                    for (var i = 0; i < values.length; i++) {
                        var opt = values[i];
                        if (opt.length > 0 && opt.indexOf('|') > 0) {
                            var el = document.createElement("option");
                            el.textContent = opt.split('|')[0];
                            el.value = opt.split('|')[1];
                            ddl.appendChild(el);
                        }
                    }
                }
            }
        }
    </script>

   <!--Search Passenger-->
   <script>

       /* Function to bind the search passenger filters html and to show the pop up */
       function ShowSearhPassenger(paxindx) {

           /* To disable bootstrap modal close on escape button and pop up outside click */

           $("#SearchPassenger").modal({
                backdrop: 'static',
                keyboard: false
           });

           /* If previously opened pop up and the current pop up button click belongs to the same pax, 
            * it will avoid binding the html once again and just open the existing pop up */
           if (cindex == paxindx && document.getElementById('txtSearchEmpId') != null)
               $('#SearchPassenger').modal('show');
           else {

               cindex = paxindx;
               $('#SearchFilters').children().remove();
               $("#PassengerDetailsList").children().remove();
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmpId" class="form-control" placeholder="Please enter employee id" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchFirstName" class="form-control" placeholder="Please enter first name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchLastName" class="form-control" placeholder="Please enter last name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmail" class="form-control" placeholder="Please enter email" /></div>');

               /* If the current pop up was clicked earlier and has any filter options, same filters will assign to the filter controls and, 
                * will perform the search for the same filters */

               if (SearchFilter[cindex] != null && SearchFilter[cindex] != '') {
                   var Filters = SearchFilter[paxindx].split('|');
                   document.getElementById('txtSearchEmpId').value = Filters[0];
                   document.getElementById('txtSearchFirstName').value = Filters[1];
                   document.getElementById('txtSearchLastName').value = Filters[2];
                   document.getElementById('txtSearchEmail').value = Filters[3];
                   SearchPassenger();
               }

               $('#SearchPassenger').modal('show');
           }
       }


       var SearchFilter = new Array(10);
       var selctedprofiles = new Array(10);
       var cindex;
       var PaxProfilesData;
       var PaxProfilesFlexData;
       var AirlineCode = '';

       /* Added .1 sec timer to show processing div on search passengers results loading */
       function SearchPassenger() {
           document.getElementById('ctl00_upProgress').style.display = 'block';
           setTimeout(function(){ SearchPassengers(); document.getElementById('ctl00_upProgress').style.display ='none'; }, 100);            
       }

       /* Function to search passengers list with the given filters in the pop up */
       function SearchPassengers() {

           $("#PassengerDetailsList").children().remove();
           var EmployeeId = document.getElementById('txtSearchEmpId').value, FstName = document.getElementById('txtSearchFirstName').value,
               LstName = document.getElementById('txtSearchLastName').value; Email = document.getElementById('txtSearchEmail').value;
           AirlineCode = '<%=onwardResult.Flights[0][0].Airline%>';

           if (EmployeeId == '' && FstName == '' && LstName == '' && Email == '') {

               SearchFilter[cindex] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter any filter option with 3 characters</div>");
               return false;
           }

           if ((EmployeeId != '' && EmployeeId.length < 3) || (FstName != '' && FstName.length < 3) || (LstName != '' && LstName.length < 3) || (Email != '' && Email.length < 3)) {
               SearchFilter[cindex] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter minimum 3 characters in the filter</div>");
               return false;
           }

           SearchFilter[cindex] = EmployeeId + '|' + FstName + '|' + LstName + '|' + Email + '|' + AirlineCode;

           $.ajax({
                type: "POST",
                url: "PassengerDetails.aspx/GetPaxProfiles",
                contentType: "application/json; charset=utf-8",
                data: "{'sFilters':'" + SearchFilter[cindex] + "'}",
                dataType: "json",
                async: false,
                success: function (data) {
                    ShowPassengerDetail(data.d);
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
           
       }

       /* Function to bind passenger search results list to grid */
       function ShowPassengerDetail(results) {

           if (results == '' || results == null || results.length == 0 || results == 'undefined') {
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }
           else {
               PaxProfilesData = JSON.parse(results[0]);
               PaxProfilesFlexData = JSON.parse(results[1]);
           }

           $("#PassengerDetailsList").append('<table id="tblPaxDetails" class="table b2b-corp-table"><tr><th><span>Select</span></th><th><span>Employee Code</span></th><th><span>First Name</span></th><th><span>Last Name</span></th><th><span>Email</span></th><th><span>Passport No</span></th></tr></table>');
           var hasdata = false;
           $.each(PaxProfilesData, function (key, prfdata) {

               if (selctedprofiles.indexOf(prfdata.ProfileId) == -1) {
                   hasdata = true;
                   $("#tblPaxDetails").append('<tr>' +
                       
                       '<td> <span class="glyphicon glyphicon-plus-sign" id="prof_' + prfdata.ProfileId + '" onclick="SelectPaxData(this,' + prfdata.ProfileId + ')" /> </td>' +
                       '<td class="text-left"> <span>' + prfdata.EmployeeId + '</span></td> <td class="text-left"><span>' + prfdata.FName + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.LName + '</span></td> <td class="text-left"><span>' + prfdata.Email + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.PassportNo + '</span></td>' +
                       '</tr >');
               }
           });

           if (!hasdata) {
               $("#PassengerDetailsList").children().remove();
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }

       }

       /* Function to assign flex field controls data */
       function AssignFlexData(cntrltype, cntrlid, flexData) {

            if (cntrltype == "T")
                document.getElementById("ctl00_cphTransaction_txtFlex" + cntrlid).value = flexData;
            else if (cntrltype == "L") {
                flexData = flexData == '' ? '-1' : flexData;
                $("#s2id_ctl00_cphTransaction_ddlFlex" + cntrlid).select2("val", flexData);
            }
            else{
                var d = flexData.split(' ')[0];
                var date = d.split('/');
                if (date.length == 3) {
                    $("#s2id_ctl00_cphTransaction_ddlMonth" + cntrlid).select2("val", date[0]);
                    $("#s2id_ctl00_cphTransaction_ddlDay" + cntrlid).select2("val", date[1]);
                    $("#s2id_ctl00_cphTransaction_ddlYear" + cntrlid).select2("val", date[2]);
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlMonth" + cntrlid).select2("val", '-1');
                    $("#s2id_ctl00_cphTransaction_ddlDay" + cntrlid).select2("val", '-1');
                    $("#s2id_ctl00_cphTransaction_ddlYear" + cntrlid).select2("val", '-1');
                }
            }
       }

       /* Function to assign flex field controls data for additional pax */
       function AssignFlexDataAdPax(cntrltype, cntrlid, flexData, paxid) {

            if (cntrltype == "T")
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxid + '_txtFlexAd' + cntrlid).value = flexData;
            else if (cntrltype == "L") {
                flexData = flexData == '' ? '-1' : flexData;
                $('#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxid + '_ddlFlexAd' + cntrlid).select2("val", flexData);
            }
            else{
                var d = flexData.split(' ')[0];
                var date = d.split('/');
                if (date.length == 3) {
                    $("#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0" + paxid + "_ddlMonthAd" + cntrlid).select2("val", date[0]);
                    $("#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0" + paxid + "_ddlDayAd" + cntrlid).select2("val", date[1]);
                    $("#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0" + paxid + "_ddlYearAd" + cntrlid).select2("val", date[2]);
                }
                else {
                    $("#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0" + paxid + "_ddlMonthAd" + cntrlid).select2("val", '-1');
                    $("#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0" + paxid + "_ddlDayAd" + cntrlid).select2("val", '-1');
                    $("#s2id_ctl00_cphTransaction_dlAdditionalPax_ctl0" + paxid + "_ddlYearAd" + cntrlid).select2("val", '-1');
                }
            }
       }

       /* Function to assign the selected profile data to screen passenger details window */
       function SelectPaxData(event, PrfId) {

           var selpaxdtls = '', tempvar = '', drpdndefval = '-1';//, titles = '|Mr|Ms|Mrs|Dr|';
           var isAdPax = cindex > 0;
           var paxindx = cindex - 1;
           selctedprofiles[cindex] = PrfId;

           $.each(PaxProfilesData, function (key, prfdata) {

               if (prfdata.ProfileId == PrfId)
                   selpaxdtls = prfdata;
           });

           if (isAdPax) {

               tempvar = (selpaxdtls.Title == null || selpaxdtls.Title == '') ? '-1' : selpaxdtls.Title;
               $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPaxTitle').select2("val", tempvar);

               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtPaxFName').value = selpaxdtls.FName;
               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtPaxLName').value = selpaxdtls.LName;

               if (selpaxdtls.DOBDate != null && selpaxdtls.DOBDate != '') {
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlDay').select2("val", selpaxdtls.DOBDate);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlMonth').select2("val", selpaxdtls.DOBMonth);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlYear').select2("val", selpaxdtls.DOBYear);
               }
               else {
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlDay').select2("val", drpdndefval);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlMonth').select2("val", drpdndefval);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlYear').select2("val", drpdndefval);
               }

               tempvar = (selpaxdtls.Gender == 'M' || selpaxdtls.Gender == '1') ? '1' : (selpaxdtls.Gender == 'F' || selpaxdtls.Gender == '2') ? '2' : '-1';
               $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlGender').select2("val", tempvar);
               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtPassportNo').value = selpaxdtls.PassportNo;

               if (selpaxdtls.PexpDate != null && selpaxdtls.PexpDate != '') {
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEDay').select2("val", selpaxdtls.PexpDate);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEMonth').select2("val", selpaxdtls.PexpMonth);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEYear').select2("val", selpaxdtls.PexpYear);
               }
               else {
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEDay').select2("val", drpdndefval);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEMonth').select2("val", drpdndefval);
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEYear').select2("val", drpdndefval);
               }

               tempvar = selpaxdtls.PassportCOI != null && selpaxdtls.PassportCOI != '' ? selpaxdtls.PassportCOI : '-1';
               $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlCountry').select2("val", tempvar);

               tempvar = selpaxdtls.NationalityCode != null && selpaxdtls.NationalityCode != '' ? selpaxdtls.NationalityCode : '-1';
               $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlNationality').select2("val", tempvar);

               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtAirline').value = selpaxdtls.FFNum == null ? '' : AirlineCode;
               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtFlight').value = selpaxdtls.FFNum;

               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtSeatPref').value = selpaxdtls.SeatPreference;
               document.getElementById('divSeatPref' + paxindx).style.display =
                   (selpaxdtls.SeatPreference != null && selpaxdtls.SeatPreference != '') ? 'block' : 'none';

               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtMealPref').value = selpaxdtls.MealRequest;
               document.getElementById('divMealPref' + paxindx).style.display =
                   (selpaxdtls.MealRequest != null && selpaxdtls.MealRequest != '') ? 'block' : 'none';

               document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_hdnProfileId').value = PrfId;

               var flexCount = document.getElementById('<%=hdnFlexCount.ClientID%>').value;

               for (var p = 0; p < flexCount; p++) {

                   var flxdata = '';
                   var flexfld = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_hdnFlexIdAd' + p);
                   var FlexControl = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_hdnFlexControlAd' + p).value;

                   $.each(PaxProfilesFlexData, function (key, prfflxdata) {
                       if (prfflxdata.ProfileId == PrfId && flexfld.value == prfflxdata.FlexId)
                           flxdata = prfflxdata.FlexData;
                   });

                   if (flexfld != null && flxdata != '' && flxdata != null) {
                        AssignFlexDataAdPax(FlexControl, p, flxdata, paxindx);
                   }
                   else {                        
                        AssignFlexDataAdPax(FlexControl, p, '', paxindx);
                   }

               }               

               $('label[for="ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_chkAddPax"]').html('Update Passenger');

               var ancAPI = document.getElementById('ancAPI' + paxindx);
               if (ancAPI.attributes["aria-expanded"].value == 'false')
                   ancAPI.click();

           }
           else {

               tempvar = (selpaxdtls.Title == null || selpaxdtls.Title == '') ? '-1' : selpaxdtls.Title;
               $('#s2id_ctl00_cphTransaction_ddlTitle').select2('val', tempvar);

               document.getElementById('ctl00_cphTransaction_txtPaxFName').value = selpaxdtls.FName;
               document.getElementById('ctl00_cphTransaction_txtPaxLName').value = selpaxdtls.LName;
               document.getElementById('ctl00_cphTransaction_txtEmail').value = selpaxdtls.Email;

               if (selpaxdtls.Mobilephone != null && selpaxdtls.Mobilephone != '') {
                   var mobile = selpaxdtls.Mobilephone.split('-');

                   document.getElementById('ctl00_cphTransaction_txtMobileCountryCode').value = mobile.length > 1 ? mobile[0] : '';
                   document.getElementById('ctl00_cphTransaction_txtMobileNo').value = mobile.length > 1 ? mobile[1] : mobile[0];
               }

               if (selpaxdtls.DOBDate != null && selpaxdtls.DOBDate != '') {
                   $('#s2id_ctl00_cphTransaction_ddlDay').select2('val', selpaxdtls.DOBDate);
                   $('#s2id_ctl00_cphTransaction_ddlMonth').select2('val', selpaxdtls.DOBMonth);
                   $('#s2id_ctl00_cphTransaction_ddlYear').select2('val', selpaxdtls.DOBYear);
               }
               else {
                   $('#s2id_ctl00_cphTransaction_ddlDay').select2('val', drpdndefval);
                   $('#s2id_ctl00_cphTransaction_ddlMonth').select2('val', drpdndefval);
                   $('#s2id_ctl00_cphTransaction_ddlYear').select2('val', drpdndefval);
               }

               tempvar = (selpaxdtls.Gender == 'M' || selpaxdtls.Gender == '1') ? '1' : (selpaxdtls.Gender == 'F' || selpaxdtls.Gender == '2') ? '2' : '-1';
               $('#s2id_ctl00_cphTransaction_ddlGender').select2('val', tempvar);
               document.getElementById('ctl00_cphTransaction_txtPassportNo').value = selpaxdtls.PassportNo;

               if (selpaxdtls.PexpDate != null && selpaxdtls.PexpDate != '') {
                   $('#s2id_ctl00_cphTransaction_ddlPEDay').select2('val', selpaxdtls.PexpDate);
                   $('#s2id_ctl00_cphTransaction_ddlPEMonth').select2('val', selpaxdtls.PexpMonth);
                   $('#s2id_ctl00_cphTransaction_ddlPEYear').select2('val', selpaxdtls.PexpYear);
               }
               else {
                   $('#s2id_ctl00_cphTransaction_ddlPEDay').select2('val', drpdndefval);
                   $('#s2id_ctl00_cphTransaction_ddlPEMonth').select2('val', drpdndefval);
                   $('#s2id_ctl00_cphTransaction_ddlPEYear').select2('val', drpdndefval);
               }

               tempvar = selpaxdtls.PassportCOI != null && selpaxdtls.PassportCOI != '' ? selpaxdtls.PassportCOI : '-1';
               $('#s2id_ctl00_cphTransaction_ddlCountry').select2('val', tempvar);

               tempvar = selpaxdtls.NationalityCode != null && selpaxdtls.NationalityCode != '' ? selpaxdtls.NationalityCode : '-1';
               $('#s2id_ctl00_cphTransaction_ddlNationality').select2('val', selpaxdtls.NationalityCode);

               document.getElementById('ctl00_cphTransaction_txtAddress1').value = selpaxdtls.Address1;
               document.getElementById('ctl00_cphTransaction_txtAddress2').value = selpaxdtls.Address2;
               document.getElementById('ctl00_cphTransaction_txtAirline').value = selpaxdtls.FFNum == null ? '' : AirlineCode;
               document.getElementById('ctl00_cphTransaction_txtFlight').value = selpaxdtls.FFNum;

               document.getElementById('ctl00_cphTransaction_txtSeatPref').value = selpaxdtls.SeatPreference;
               document.getElementById('divSeatPref').style.display =
                   (selpaxdtls.SeatPreference != null && selpaxdtls.SeatPreference != '') ? 'block' : 'none';

               document.getElementById('ctl00_cphTransaction_txtMealPref').value = selpaxdtls.MealRequest;
               document.getElementById('divMealPref').style.display =
                   (selpaxdtls.MealRequest != null && selpaxdtls.MealRequest != '') ? 'block' : 'none';

               document.getElementById('ctl00_cphTransaction_hdnProfileId').value = PrfId;

               var flexCount = document.getElementById('<%=hdnFlexCount.ClientID%>').value;

               for (var p = 0; p < flexCount; p++) {

                   var flxdata = '';
                   var flexfld = document.getElementById('ctl00_cphTransaction_hdnFlexId' + p);
                   var FlexControl = document.getElementById('ctl00_cphTransaction_hdnFlexControl' + p).value;

                   $.each(PaxProfilesFlexData, function (key, prfflxdata) {
                       if (prfflxdata.ProfileId == PrfId && flexfld.value == prfflxdata.FlexId)
                           flxdata = prfflxdata.FlexData;
                   });

                   if (flexfld != null && flxdata != '' && flxdata != null) {
                        AssignFlexData(FlexControl, flexfld.id.replace('ctl00_cphTransaction_hdnFlexId', ''), flxdata);
                   }
                   else {                        
                        AssignFlexData(FlexControl, flexfld.id.replace('ctl00_cphTransaction_hdnFlexId', ''), '');
                   }

               }

               $('label[for="ctl00_cphTransaction_chkAddPax"]').html('Update Passenger');
               var ancAPI = document.getElementById('ancAPI');
               if (ancAPI.attributes["aria-expanded"].value == 'false')
                   ancAPI.click();
           }           
           $('#SearchPassenger').modal('hide');
       }

   </script>
    
    <asp:HiddenField ID="hdfAsvAmount" runat="server" Value="0" />
    <asp:HiddenField ID="hdfAsvType" runat="server" />
    <asp:HiddenField ID="hdfAsvElement" runat="server" />
    <asp:HiddenField ID="hdfCurrentDate" runat="server" />
    <asp:HiddenField ID="hdfTaxMarkUp" runat="server" />
    <asp:HiddenField ID="hdfOnwardOtherCharges" runat="server" Value="0" />
    <asp:HiddenField ID="hdfReturnOtherCharges" runat="server" Value="0" />
    <asp:HiddenField ID="hdfOnwardFareAction" runat="server" Value="Get" />
    <asp:HiddenField ID="hdfReturnFareAction" runat="server" Value="Get" />
    <asp:HiddenField ID="hdnSelStateId" runat="server" />
    <asp:HiddenField ID="hdnOutMealSelection" runat="server" Value="0" />
    <asp:HiddenField ID="hdnInMealSelection" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPaxSeatInfo" runat="server" />
    <div>
        <div class="paddingbot_10">
            <asp:Label ID="lblTimeChanged" runat="server" Text="Note: Itinerary details have been changed."
                ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
        </div>
        <div>
            <div class="col-md-3 padding-0 bg_white">
                <div class="ns-h3">
                    Onward Flight summary
                </div>
                <table class="table901" id="tblOnwardFlightDetails" runat="server" width="100%" border="0"
                    cellspacing="0" cellpadding="0">
                </table>
                <%if (returnResult != null && returnResult.Flights.Length >= 1)
                    { %>
                <div class="ns-h3">
                    Return Flight summary
                </div>
                <table class="table901" id="tblReturnFlightDetails" runat="server" width="100%" border="0"
                    cellspacing="0" cellpadding="0">
                </table>
                <%} %>

                <div>
                    <div class="ns-h3">
                        Onward Flight
                    </div>
                    <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="gray-smlheading">
                                <b>Product</b>
                            </td>
                            <td class="gray-smlheading">
                                <b>Price</b>
                            </td>
                        </tr>
                        <tr>
                            <td>AirFare                                
                            </td>
                            <td valign="bottom">
                                <%=agency.AgentCurrency %>
                                <asp:TextBox ID="lblOnwardBaseFare" CssClass="txt24" Text="0.00" Enabled="false" BackColor="White" ForeColor="Black"
                                    BorderColor="White" Style="text-align: right" BorderStyle="None" BorderWidth="0"
                                    runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Taxes&Fees
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardTax" Text="0.00" CssClass="txt24" Enabled="false" BackColor="White"
                                    ForeColor="Black" BorderColor="White" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                         <tr>
                            <td>K3 Tax
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardK3Tax" CssClass="txt24" BackColor="White"
                                    ForeColor="Black" BorderColor="White" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%if (Discount > 0)
                        { %>
                        <tr>
                            <td>Discount
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardDiscount" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td>Total                                
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardTotal" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Baggage
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardBaggage" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>

                        <%if (onwardResult != null && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp|| onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) %>
                        <%{ %>
                        <tr>
                            <td>Meal
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardMeal" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%} %>

                        <tr>
                            <td>
                                <strong>Total Offer Fare</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardGTotal" Text="0.00" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                </strong>
                            </td>
                        </tr>
                        <tr class="d-none">
                            <td>
                                <strong>Total Pub Fare</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblOnwardPubFare" Text="0.00" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                </strong>
                            </td>
                        </tr>
                    </table>

                    <%if(returnResult != null) %>
                    <%{ %>
                    <div class="ns-h3">
                        Return Flight
                    </div>
                    <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="gray-smlheading">
                                <b>Product</b>
                            </td>
                            <td class="gray-smlheading">
                                <b>Price</b>
                            </td>
                        </tr>
                        <tr>
                            <td>AirFare                                
                            </td>
                            <td valign="bottom">
                                <%=agency.AgentCurrency %>
                                <asp:TextBox ID="lblReturnBaseFare" CssClass="txt24" Text="0.00" Enabled="false" BackColor="White" ForeColor="Black"
                                    BorderColor="White" Style="text-align: right" BorderStyle="None" BorderWidth="0"
                                    runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Taxes&Fees
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnTax" Text="0.00" CssClass="txt24" Enabled="false" BackColor="White"
                                    ForeColor="Black" BorderColor="White" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>K3 Tax
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnK3Tax" CssClass="txt24" BackColor="White"
                                    ForeColor="Black" BorderColor="White" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%if (Discount > 0)
                        { %>
                        <tr>
                            <td>Discount
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnDiscount" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td>Total                                
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnTotal" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Baggage
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnBaggage" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <%if (returnResult != null && (returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) %>
                        <%{ %>
                        <tr>
                            <td>Meal
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnMeal" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <%} %>

                        <tr>
                            <td>
                                <strong>Total Offer Fare</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnGTotal" Text="0.00" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server"></asp:TextBox>
                                </strong>
                            </td>
                        </tr>
                        <tr class="d-none">
                            <td>
                                <strong>Total Pub Fare</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblReturnPubFare" Text="0.00" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server"></asp:TextBox>
                                </strong>
                            </td>
                        </tr>
                    </table>
                    <%} %>

                    <div class="ns-h3">
                        Booking Review
                    </div>
                    <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="gray-smlheading">
                                <b>Product</b>
                            </td>
                            <td class="gray-smlheading">
                                <b>Price</b>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>AirFare                                
                            </td>
                            <td valign="bottom">
                                <%=agency.AgentCurrency %>
                                <asp:TextBox ID="lblBaseFare" CssClass="txt24" Enabled="false" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" Style="text-align: right" BorderStyle="None" BorderWidth="0"
                                    runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>Taxes&Fees
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblTax" CssClass="txt24" Text="0.00" Enabled="false" BackColor="White"
                                    ForeColor="Black" BorderColor="White" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%if (Discount > 0)
                        { %>
                        <tr style="display: none">
                            <td>Discount
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblDiscount" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td>Total                                
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblTotalFare" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Baggage
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblBaggageAmt" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>

                        <%--<%if (onwardResult != null && returnResult != null && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir)) %>
                        <%{ %>--%>
                        <tr>
                            <td>Meal
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblMealAmt" Text="0.00" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>

                            </td>
                        </tr>
                      <%--  <%} %>--%>

                        <tr>
                            <td>
                                <strong>Total Offer Fare</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblTotalPrice" Text="0.00" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                </strong>
                            </td>
                        </tr>
                        <tr class="d-none">
                            <td>
                                <strong>Total Pub Fare</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblTotalPubFare" Text="0.00" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                </strong>
                            </td>
                        </tr>
                    </table>
                </div>
                <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate != "Y" && agency.AgentParantId != 2125)
                    { %>
                <div class="ns-h3">
                    Markup
                </div>
                <div style="padding: 5px;">
                    <table width="100%" border="0">
                        <tr>
                            <td style="padding-right: 2px;" valign="top">Add Markup on
                                <%--<asp:CheckBox ID="chkFare" runat="server" Text="Fare" />
                                        <asp:CheckBox ID="ChkTax" runat="server" Text="Tax" />--%>
                                <asp:RadioButton ID="rbtnFare" runat="server" Text="Fare" GroupName="markup" Checked="true" />
                                <asp:RadioButton ID="rbtnTax" runat="server" Text="Tax" GroupName="markup" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="70%">
                                    <tr>
                                        <td style="padding-right: 2px;">
                                            <strong>Add Markup</strong>
                                        </td>
                                        <td style="padding-right: 2px;">
                                            <label>
                                                <asp:TextBox ID="txtMarkup" runat="server" CssClass="form-control" MaxLength="2" Width="70px" onpaste="return false;"
                                                    ondrop="return false;" onkeypress="return isNumber(event)"  onfocus="Check(this.id);"
                                                    onBlur="Set(this.id);" Text="0"></asp:TextBox>
                                            </label>
                                        </td>
                                        <td>
                                            <label>
                                                <asp:RadioButton ID="rbtnPercent" runat="server" Text="P" GroupName="cost" Checked="true" onchange="return CheckVal();"/>
                                            </label>
                                        </td>
                                        <td align="right">
                                            <label>
                                                <asp:RadioButton ID="rbtnFixed" runat="server" Text="F" GroupName="cost" onchange="return CheckVal();"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class=" pad_14">
                                    <asp:Button ID="imgBtnCost" runat="server" CssClass="button-normal" Text="Recalculate Cost"
                                        OnClientClick="return RecalculateCost();" />
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <%} %>
            </div>
            <div class="col-md-9">
                <div class="bg_white" style="border: solid 1px #ccc; border-top: 0px; margin-bottom: 10px; padding-bottom: 10px;">
                    <div class="ns-h3">
                        Please enter Passenger(s) Details
                    </div>
                    <div class="subgray-header">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12">
                                <b class="passenger-label">Passenger 1 - Adult</b>
                                
                                <a id="ancAPI" class="btn btn-primary advPax-collapse-btn float-right" role="button" data-toggle="collapse" href="#paggengerDetailsCollapse-1"
                                    aria-expanded="false" aria-controls="collapseExample">Add API's </a>
                                <button type="button" class="btn btn-link float-right" onclick="ShowSearhPassenger(0)">Search Passenger <span class="glyphicon glyphicon-search"></span></button>
                                 <asp:CheckBox runat="server" ID="chksendmail" Checked="true" Text="send Email" CssClass="float-right custom-checkbox-table" Style="display:inline-block;"   />

                            </div>
                        </div>



                    </div>
                    <div class="pax-details-wrap">
                        <div class="col-xs-12 col-lg-12">
                            <asp:CheckBox runat="server" ID="chkAddPax" CssClass="custom-checkbox-style dark chkbox-addpasngr" Text="Add Passenger"></asp:CheckBox>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-3 col-lg-2">
                                        <label>
                                            <strong>Title<span class="red_span">*</span></strong>
                                        </label>
                                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control" onchange="TitleChanged(this.id,'-1');">
                                            <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                                            <asp:ListItem Value="Mr">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Ms">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
                                            <asp:ListItem Value="Dr">Dr.</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                            <strong>First Name<span class="red_span">*</span></strong></label><b class="red_span"
                                                style="display: none" id="title"></b>
                                        <asp:TextBox ID="txtPaxFName" runat="server" CssClass="form-control" onkeypress="return IsAlphaNumeric(event);"
                                            ondrop="return false;" onpaste="return true;"></asp:TextBox>
                                        <b class="red_span" style="display: none" id="fname"></b>
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                            <strong>Last Name<span class="red_span">*</span></strong></label>
                                        <asp:TextBox ID="txtPaxLName" runat="server" CssClass="form-control" onkeypress="return IsAlphaNumeric(event);"
                                            ondrop="return false;" onpaste="return true;"></asp:TextBox>
                                        <b class="red_span" style="display: none" id="lname"></b>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Email
                                                <%if (EmailRequired)
                                                    { %><span class="red_span">*</span><%} %></strong></label>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" CausesValidation="True"
                                            ValidationGroup="email"></asp:TextBox>
                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email format"
                                            ControlToValidate="txtEmail" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="email" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                        <b class="red_span" style="display: none" id="email"></b>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                            <strong>Mobile <span class="red_span">*</span></strong>
                                        </label>
                                        <div class="input-group tel-input-addon">
                                            <div class="input-group-addon">
                                                <asp:TextBox ID="txtMobileCountryCode" runat="server" CssClass="form-control pull-left"
                                                    onkeypress="return isNumber(event)" MaxLength="3"></asp:TextBox>
                                            </div>
                                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control pull-left" onkeypress="return isNumber(event)"
                                                onpaste="return checkingNumber(this.id);"></asp:TextBox>
                                            <b class="red_span" style="display: none" id="mobileCountryCode"></b><b class="red_span"
                                                style="display: none" id="mobile"></b>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <strong>D.O.B.
                                                <%if (DOBRequired)
                                                    { %><span class="red_span">*</span><%} %>
                                            </strong>
                                        </label>
                                        <div class="row no-gutter">
                                            <div class="col-xs-4">
                                                <asp:DropDownList ID="ddlDay" runat="server" AppendDataBoundItems="True" CssClass=" form-control pull-left">
                                                    <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4">
                                                <asp:DropDownList ID="ddlMonth" runat="server" AppendDataBoundItems="True" CssClass=" form-control pull-left">
                                                    <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                    <asp:ListItem Value="01">Jan</asp:ListItem>
                                                    <asp:ListItem Value="02">Feb</asp:ListItem>
                                                    <asp:ListItem Value="03">Mar</asp:ListItem>
                                                    <asp:ListItem Value="04">Apr</asp:ListItem>
                                                    <asp:ListItem Value="05">May</asp:ListItem>
                                                    <asp:ListItem Value="06">Jun</asp:ListItem>
                                                    <asp:ListItem Value="07">Jul</asp:ListItem>
                                                    <asp:ListItem Value="08">Aug</asp:ListItem>
                                                    <asp:ListItem Value="09">Sep</asp:ListItem>
                                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4">
                                                <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="True" CssClass=" form-control pull-left">
                                                    <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <b class="red_span" style="display: none" id="dobDay"></b><b class="red_span" style="display: none"
                                                id="dobMonth"></b><b class="red_span" style="display: none" id="dobYear"></b>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Destination Phone</strong>
                                        </label>
                                        <div class="input-group tel-input-addon">
                                            <div class="input-group-addon">
                                                <asp:TextBox ID="txtDestCntCode" runat="server" CssClass="form-control pull-left"
                                                    onkeypress="return isNumber(event)" MaxLength="3"></asp:TextBox>
                                            </div>
                                            <asp:TextBox ID="txtDestPhoneNo" runat="server" CssClass="form-control pull-left" onkeypress="return isNumber(event)"
                                                onpaste="return checkingNumber(this.id);"></asp:TextBox>

                                        </div>
                                    </div>






                                </div>
                                <!--Added by lokesh on 27-Mar-2018 -->
                                <!--Applicable to only AirArabiaSource and also for only Lead PAX -->
                                <!-- Fields should be displayed only for the customers travelling from INDIA" -->
                                <!--Capture GSTIN  number from customer  -->


                                <div class="form-group" style="display: none;" id="pnlGSTDetails">
                                    <div class="col-md-3">
                                        <label>
                                            <strong>State Code<span class="red_span">*</span>
                                            </strong>
                                        </label>
                                        <div class="row no-gutter">
                                            <div class="col-xs-6">
                                                <asp:DropDownList runat="server" ID="ddlGSTStateCode">
                                                    <asp:ListItem Selected="True" Value="-1">Select State Code</asp:ListItem>

                                                </asp:DropDownList>
                                                <span class="red_span" id="valGSTStateCode" style="display: none;"></span>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Tax Registration Number
                                            </strong>
                                        </label>
                                        <asp:TextBox MaxLength="50" CssClass="form-control pull-left" runat="server" ID="txtGSTRegNum"></asp:TextBox>
                                        <span class="red_span" id="valGSTRegNum" style="display: none;"></span>

                                        <div class="clearfix">
                                        </div>
                                    </div>

                                </div>

                                <!-- Added by lokesh on 21-Aug-2018-->
                                <!-- Applicable to only spice jet source-->
                                <!--Fields should be displayed only for the customers travelling from INDIA-->
                                <!--Capture TaxRegNumber number from customer  -->


                                  <%if ((requiredGSTForSG_6E && onwardResult != null && onwardResult.Flights[0][0].Origin.CountryCode == "IN"  && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) || (requiredGSTForSG_6E && returnResult != null && returnResult.Flights[0][0].Origin.CountryCode == "IN"  && (returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))) %>
                                <%{ %>
                                <div class="form-group" id="pnlGSTDetails_SG">

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Tax Registration Number
                                            </strong>
                                        </label>
                                        <asp:TextBox MaxLength="15" CssClass="form-control pull-left" runat="server" ID="txtTaxRegNo_SG_GST"></asp:TextBox>


                                        <div class="clearfix">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Company Name
                                            </strong>
                                        </label>
                                        <asp:TextBox CssClass="form-control pull-left" runat="server" ID="txtCompanyName_SG_GST"></asp:TextBox>
                                        <span class="red_span" id="gstCompanyName_SG" style="display: none;"></span>
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Official Email
                                            </strong>
                                        </label>
                                        <asp:TextBox CssClass="form-control pull-left" runat="server" ID="txtGSTOfficialEmail"></asp:TextBox>
                                        <span class="red_span" id="gstOfficialEmail_SG" style="display: none;"></span>
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                </div>
                                <%} %>

                                <!--GST input fields for TBOAir added by shiva-->
                                <div class="form-group" style="display: none" id="divGSTFields">
                                    <div class="col-md-4">
                                        <label>
                                            <strong>GST Company Address
                                                 <%if (onwardResult.IsGSTMandatory || (returnResult != null && returnResult.IsGSTMandatory) || requiredGSTForUAPI)
                                                     { %>
                                                <span class="red_span">*</span>
                                                <%} %>
                                            </strong>
                                        </label>
                                        <asp:TextBox ID="txtGSTCompanyAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                        <span class="red_span" id="gstAddress" style="display: none;"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <strong>GST Company Contact Number
                                                 <%if (onwardResult.IsGSTMandatory || (returnResult != null && returnResult.IsGSTMandatory) || requiredGSTForUAPI)
                                                     { %>
                                                <span class="red_span">*</span>
                                                <%} %>
                                            </strong>
                                        </label>
                                        <asp:TextBox ID="txtGSTContactNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                        <span class="red_span" id="gstContact" style="display: none;"></span>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label>
                                            <strong>GST Company Name
                                                 <%if (requiredGSTForUAPI || (onwardResult.IsGSTMandatory) || (returnResult != null && returnResult.IsGSTMandatory))
                                                     { %>
                                                <span class="red_span">*</span>
                                                <%} %>
                                            </strong>
                                        </label>
                                        <asp:TextBox ID="txtGSTCompanyName" runat="server" CssClass="form-control"></asp:TextBox>
                                        <span class="red_span" id="gstName" style="display: none;"></span>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label>
                                            <strong>GST Number
                                                 <%if (onwardResult.IsGSTMandatory || (returnResult != null && returnResult.IsGSTMandatory) || requiredGSTForUAPI)
                                                     { %>
                                                <span class="red_span">*</span>
                                                <%} %>
                                            </strong>
                                        </label>
                                        <asp:TextBox ID="txtGSTNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                        <span class="red_span" id="gstNumber" style="display: none;"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <strong>GST Company Email
                                                 <%if (onwardResult.IsGSTMandatory || (returnResult != null && returnResult.IsGSTMandatory) || requiredGSTForUAPI)
                                                     { %>
                                                <span class="red_span">*</span>
                                                <%} %>
                                            </strong>
                                        </label>
                                        <asp:TextBox ID="txtGSTCompanyEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        <span class="red_span" id="gstEmail" style="display: none;"></span>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdnFlexCount" runat="server" Value="0" />
                                <div class="form-group baggage-wrapper col-xs-12 col-lg-12" runat="server" id="lblOnBagWrapper" visible="false">
                               

                                           
                                
                                    <!-- Added by lokesh on 29-Mar-2018 for G9 Source only if the customer is travelling from India -->

                                    <asp:Label runat="server" CssClass="red_span" Style="display: none" ID="lblG9BaggageAlertLeadPax">(Excluding SGST & GST Tax Components)</asp:Label>

                           
                                    

                                         
                                     

                               <div class="row row-5px">


                                   <div class="col-12">
                                     <div class="row">
                                          
                                        <div class="col-12" id="lccLeadPaxBaggagelbl" runat="server">
                                            <span style="color: black; font-weight: bold; ">
                                                 <asp:Label ID="lblBaggage" runat="server" Text="" Visible="false"></asp:Label>
                                             <%if (onwardResult != null && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))%>
                                        <%{ %>
                                       Select Baggage
                                        <%} %>
                                                           </span></div>

              


                                        <div class="col-md-6" runat="server" id="leadPaxOnBagDiv" >
                                         

                                         <div>   Onward: <asp:Label ID="lblOnBag" runat="server" CssClass="sm-text-baggage d-inline" Text="" Visible="false"></asp:Label>  </div>
                                            <div class="input-group">
                                              
                                                <asp:DropDownList ID="ddlOnwardBaggage" Style="margin-bottom: 0;" CssClass="form-control pull-left baggage-input-ctrl"
                                                    runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                </asp:DropDownList>



                                                <div class="input-group-text">
                                                <asp:TextBox ID="lblOutPrice" Enabled="false" Text="0.00" style="width:50px" runat="server"></asp:TextBox>

                                                </div>
                                                <asp:HiddenField ID="hdnOutBagSelection" runat="server" Value="0" />
                                             
                                                    
                                           
                                            </div>
                                            

                                            
                                        </div>

                                  

                                        <div class="col-md-6" runat="server" id="leadPaxRetBagDiv">


                                             <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                    { %>
                                                <div>  Return:   <asp:Label ID="lblInBag" runat="server" CssClass="sm-text-baggage d-inline" Text="" Visible="false"></asp:Label> </div>
                                                   
                                               
                                                <%} %>


                                           <div class="input-group">

                                           

                                                <asp:DropDownList ID="ddlInwardBaggage" Style="margin-bottom: 0;" CssClass="form-control pull-left "
                                                    runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                </asp:DropDownList>


                                                <div class="input-group-text">

                                                <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                    { %>
                                       
                                                    <asp:TextBox ID="lblInPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>

                                                <%} %>
                                     

                                                </div>
                                            </div>

                                               <asp:HiddenField ID="hdnInBagSelection" runat="server" Value="0" />
                                                


                                           
                                        </div>


                                      </div>
                                   </div>
                                   
                                  
                                  
                                  
                                   <div class="col-12">
                                       <div class="row">

                               
                                        <div id="leadPaxMealLblOnw" runat="server" class="col-12 mt-3"><span style="color: black; font-weight: bold; ">Select Meal</span></div>

                               <%if (onwardResult != null && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) %>
                                        <%{ %>

                                    

                                        <div class="col-md-6" runat="server" id="leadPaxOnMealDiv">


                                              <div>  Onward Meal: </div>
                                                   
                                                


                                            
                                            <div class="input-group">
                                       
                                                <asp:DropDownList ID="ddlOnwardMeal" Style="margin-bottom: 0;" CssClass="form-control pull-left baggage-input-ctrl"
                                                    runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                </asp:DropDownList>
                                               

                                                <div class="input-group-text">

                                             
                                                <asp:TextBox ID="lblOutMealPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>
                                              

                                              </div>

                                            </div>



                                             <b class="red_span" style="display: none" id="B1"></b>
                                            <asp:Label ID="lblOnMeal" runat="server" CssClass="sm-text-baggage" Text="" Visible="false"></asp:Label>
                                        </div>
                                        <%} %>
                                        <%if (returnResult != null && (returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) %>
                                        <%{ %>
                                        <div class="col-md-6" runat="server" id="leadPaxRetMealDiv">
                                            
                                            


                                                   <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                    { %>
                                                <div>  Return Meal:  </div>
                                                   
                                               
                                                <%} %>





                                                <b class="red_span" style="display: none" id="B2"></b>
                                            <div class="input-group">

                                    
                                                <asp:DropDownList ID="ddlInwardMeal" Style="margin-bottom: 0;" CssClass="form-control pull-left "
                                                    runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                </asp:DropDownList>
                                                

                                                <div class="input-group-text">
                                                <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                    { %>
                                             
                                                    <asp:TextBox ID="lblInMealPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>





                                             
                                                <%} %>


                                                </div>
                                               
                                            </div>

                                             <asp:HiddenField ID="hdnFlexCount1" runat="server" Value="0" />
                                            <asp:Label ID="lblInMeal" runat="server" CssClass="sm-text-baggage" Text="" Visible="false"></asp:Label>


                                        </div>
                                        <%} %>
                                          </div>
                                   </div>


                               </div>

               
                                </div>

                                <div class="clear"></div>


                                <div class="collapse advPx-collapseContent" id="paggengerDetailsCollapse-1">
                                    <div class="well">
                                        <div class="form-group">
                                            <div class="col-md-3 col-lg-2">
                                                <label>
                                                    <strong>Gender
                                                        <%if (GenderRequired)
                                                            { %><span class="red_span">*</span><%} %></strong></label>
                                                <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True" Value="-1">Gender</asp:ListItem>
                                                    <asp:ListItem Value="1">Male</asp:ListItem>
                                                    <asp:ListItem Value="2">Female</asp:ListItem>
                                                </asp:DropDownList>
                                                <b class="red_span" style="display: none" id="gender"></b>
                                            </div>
                                            <div class="col-md-3">
                                                <label>
                                                    <strong>Passport No
                                                        <%if (PassportNoRequired)
                                                            { %>
                                                        <span class="red_span">*</span><%} %></strong></label>
                                                <asp:TextBox ID="txtPassportNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                <b class="red_span" style="display: none" id="passport"></b>
                                            </div>
                                            <div class="col-md-5">
                                                <label>
                                                    <strong>Passport Exp
                                                        <%if (PassportExpiryRequired)
                                                            { %><span class="red_span">*</span><%} %></strong></label>
                                                <div class="row no-gutter">
                                                    <div class="col-xs-4">
                                                        <asp:DropDownList ID="ddlPEDay" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                            <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <asp:DropDownList ID="ddlPEMonth" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                            <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                            <asp:ListItem Value="01">Jan</asp:ListItem>
                                                            <asp:ListItem Value="02">Feb</asp:ListItem>
                                                            <asp:ListItem Value="03">Mar</asp:ListItem>
                                                            <asp:ListItem Value="04">Apr</asp:ListItem>
                                                            <asp:ListItem Value="05">May</asp:ListItem>
                                                            <asp:ListItem Value="06">Jun</asp:ListItem>
                                                            <asp:ListItem Value="07">Jul</asp:ListItem>
                                                            <asp:ListItem Value="08">Aug</asp:ListItem>
                                                            <asp:ListItem Value="09">Sep</asp:ListItem>
                                                            <asp:ListItem Value="10">Oct</asp:ListItem>
                                                            <asp:ListItem Value="11">Nov</asp:ListItem>
                                                            <asp:ListItem Value="12">Dec</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <asp:DropDownList ID="ddlPEYear" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                            <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <b class="red_span" style="display: none" id="peDay"></b><b class="red_span" style="display: none"
                                                        id="peMonth"></b><b class="red_span" style="display: none" id="peYear"></b>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>
                                                    <strong>Country
                                                        <%if (CountryRequired)
                                                            { %>
                                                        <span class="red_span">*</span><%} %></strong></label>
                                                <!-- Modified by Lokesh on 6-APril-2018-->
                                                <!-- If the country of residence is India-->
                                                <!-- If the onwardResult booking source is AirArabia-->
                                                <!-- Then GST State Code is mandatory-->
                                                <asp:DropDownList ID="ddlCountry" runat="server" AppendDataBoundItems="True" CssClass="form-control" onchange="ShowHideGSTDetails(this.id)">
                                                    <asp:ListItem Value="-1">Select Country</asp:ListItem>
                                                </asp:DropDownList>
                                                <b class="red_span" style="display: none" id="country"></b>
                                            </div>
                                            <div class="col-md-3">
                                                <label>
                                                    <strong>Nationality
                                                        <%if (NationalityRequired)
                                                            { %><span class="red_span">*</span><%} %></strong></label>
                                                <asp:DropDownList ID="ddlNationality" runat="server" AppendDataBoundItems="True"
                                                    CssClass="form-control">
                                                    <asp:ListItem Selected="True" Value="-1">Select Nationality</asp:ListItem>
                                                </asp:DropDownList>
                                                <b class="red_span" style="display: none" id="nationality"></b>
                                            </div>
                                            <div class="col-md-3" >
                                                <label>
                                                    <strong>Frequent Flyer </strong>
                                                </label>
                                                <div class="input-group tel-input-addon">
                                                    <div class="input-group-addon">
                                                        <asp:TextBox ID="txtAirline" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                    </div>
                                                    <asp:TextBox ID="txtFlight" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                    <asp:HiddenField runat="server" ID="hdnProfileId" Value=""/>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label>
                                                    <strong>Address
                                                        <%if (AddressRequired)
                                                            { %>
                                                        <span class="red_span">*</span><%} %></strong></label>
                                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                                                <b class="red_span" style="display: none" id="address"></b>
                                            </div>
                                            <div class="col-md-4">
                                                <label>
                                                    &nbsp;</label>
                                                <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div id="divSeatPref" style="display:none" class="col-md-4">
                                                <label>
                                                    <strong>Seat Preference</strong></label>
                                                <asp:TextBox ID="txtSeatPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                <b class="red_span" style="display: none" id="address"></b>
                                            </div>
                                            <div id="divMealPref" style="display:none" class="col-md-4">
                                                <label><strong>
                                                    Meal Preference</strong></label>
                                                <asp:TextBox ID="txtMealPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>






                                        <table style="display: none">
                                            <tr>
                                                <td height="27">
                                                    <strong>Alternate No.</strong>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAlternateNo" runat="server" CssClass="form-control" onkeypress="return isNumber(event)"
                                                        Visible="false"></asp:TextBox>
                                                    &nbsp;
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>

                                        <div class="clearfix">
                                        </div>
                                        <div class="form-group baggage-wrapper" id="divFlex" style="display:none;"><div class="row padding-0 marbot_10" id="tblFlexFields" runat="server">
                                            <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                        </div></div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrap10">
                        <asp:DataList ID="dlAdditionalPax" runat="server" Width="100%" OnItemDataBound="dlAddtionalPax_ItemDataBound">
                            <ItemTemplate>


                                <div class="subgray-header">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-12">
                                            <strong  class="passenger-label">
                                                <asp:Label ID="lblPaxType" runat="server" Text='<%#Eval("Type") %>'></asp:Label></strong>
                                            
                                            <a id="ancAPI<%#Container.ItemIndex%>" class="btn btn-primary collapse-btn advPax-collapse-btn  float-right" role="button" data-toggle="collapse" href="#adpaggengerDetailsCollapse-<%#Container.ItemIndex%>"
                                                aria-expanded="false" aria-controls="collapseExample">ADD API's </a>
                                            <button type="button" class="btn btn-link float-right" onclick="ShowSearhPassenger(<%#Container.ItemIndex+1%>)">Search Passenger <span class="glyphicon glyphicon-search"></span></button>
                                        </div>
                                    </div>

                                </div>


                                <div class="pax-details-wrap">
                                    <div class="col-xs-12 col-lg-12">
                                        <asp:CheckBox runat="server" ID="chkAddPax" CssClass="custom-checkbox-style dark chkbox-addpasngr" Text="Add Passenger"></asp:CheckBox>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-md-3 col-lg-2">
                                                    <label>
                                                        <strong>Title <span class="red_span">*</span></strong>
                                                    </label>
                                                    <asp:DropDownList CssClass="form-control" ID="ddlPaxTitle" runat="server">
                                                        <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                                                        <asp:ListItem Value="Mr" Text="Mr."></asp:ListItem>
                                                        <asp:ListItem Value="Ms" Text="Ms."></asp:ListItem>
                                                        <asp:ListItem Value="Dr" Text="Dr."></asp:ListItem>
                                                        <asp:ListItem Value="MSTR" Text="Master"></asp:ListItem>
                                                        <asp:ListItem Value="CHD" Text="Child"></asp:ListItem>
                                                        <asp:ListItem Value="INF" Text="Infant"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <b class="red_span" style="display: none" id="errPaxtitle" runat="server"></b>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        <strong>First Name <span class="red_span">*</span></strong></label>
                                                    <asp:TextBox ID="txtPaxFName" CssClass="form-control" runat="server" CausesValidation="true"
                                                        ValidationGroup="email" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"
                                                        onpaste="return true;"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="errPaxfirstname" runat="server"></b>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        <strong>Last Name <span class="red_span">*</span></strong></label>
                                                    <asp:TextBox ID="txtPaxLName" CssClass="form-control" runat="server" CausesValidation="true"
                                                        ValidationGroup="email" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"
                                                        onpaste="return true;"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="errPaxlastname" runat="server"></b>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>
                                                        <strong>D.O.B.
                                                            <%if (DOBRequired)
                                                                { %><span class="red_span">*</span><%} %>
                                                        </strong>
                                                    </label>
                                                    <div class="row no-gutter">
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlDay" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlMonth" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                                <asp:ListItem Value="01">Jan</asp:ListItem>
                                                                <asp:ListItem Value="02">Feb</asp:ListItem>
                                                                <asp:ListItem Value="03">Mar</asp:ListItem>
                                                                <asp:ListItem Value="04">Apr</asp:ListItem>
                                                                <asp:ListItem Value="05">May</asp:ListItem>
                                                                <asp:ListItem Value="06">Jun</asp:ListItem>
                                                                <asp:ListItem Value="07">Jul</asp:ListItem>
                                                                <asp:ListItem Value="08">Aug</asp:ListItem>
                                                                <asp:ListItem Value="09">Sep</asp:ListItem>
                                                                <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                <asp:ListItem Value="12">Dec</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <b class="red_span" style="display: none" id="errPaxday" runat="server"></b><b class="red_span"
                                                            style="display: none" id="errPaxmonth" runat="server"></b><b class="red_span" style="display: none"
                                                                id="errPaxyear" runat="server"></b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group baggage-wrapper col-xs-12 col-lg-12" runat="server" id="lblOnBagWrapperAddPax" visible="false">
                                              

                                               <asp:Label runat="server" CssClass="red_span" ID="lblG9BaggageAlertAddPax">(Excluding CGST and GST Tax Components)</asp:Label>
                                                
                                               
                                               
                                               
                                                <div class="row">
                                                 



                                                 
                                                    <div class="col-12">
                                                       
                                                        <div class="row">
                                                         
                                                             

                                                             <div class="col-md-12">
                                                             <asp:Label ID="lblBaggage" style="color: black; font-weight: bold; " runat="server" Text="Select Baggage " Visible="false"></asp:Label> </div>

                                                             <div class="col-md-6" id="addPaxOnBag" runat="server">


                                                                <div>   Onward:  <asp:Label ID="lblOnBag" CssClass="sm-text-baggage d-inline" runat="server" Text="" Visible="false"></asp:Label> </div>
                                                                       

                                                               
                                                                <div class="input-group">

                                                                 
                                                                    <asp:DropDownList CssClass="form-control pull-left baggage-input-ctrl" ID="ddlOnwardBaggage"
                                                                        Style="margin-bottom: 0;" runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                                    </asp:DropDownList>                                                         
                                                                    
                                                                    
                                                                    <div class="input-group-text">
                                                                     <asp:TextBox ID="lblOutPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>
                                                                    </div>


               
                                                                                                                      
                                                                </div>



                                                                  
                                                             </div>



                                                             <div class="col-md-6" id="addPaxRetBag" runat="server">
                                                              


                                                                       <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                                        { %>

                                                                    <div>  Return: <asp:Label ID="lblInBag" runat="server" Text="" CssClass="sm-text-baggage d-inline" Visible="false"></asp:Label>  </div>

                                                                    <%} %>




                                                              
                                                                <div class="input-group">
                                                           



                                                                    <asp:DropDownList ID="ddlInwardBaggage" runat="server" Style="margin-bottom: 0;"
                                                                        CssClass="form-control pull-left" Visible="false" onchange="CalculateBaggagePrice()">
                                                                    </asp:DropDownList>


                                                                    <div class="input-group-text">

                                                                    <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                                        { %>
                                                            
                                                                    <asp:TextBox ID="lblInPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>
                                                            
                                                                    <%} %>
                                                                </div>


                                                                 </div>

                                                                
                                                            </div>


                                                        </div>
                                                    </div>
                                                   
                                                   
                                                   
                                                    <div class="col-12">
                                                       
                                                        <div class="row">
                                                      
                                                          <div class="col-12 mt-3" id="addPaxlblMeal" runat="server"><span style="color: black; font-weight: bold; ">Select Meal</span></div>
                                                          
                                                         
                                                         
                                                          <div class="col-md-6" id="addPaxOnwardMeal" runat="server">


                                                             <div>  Onward Meal: </div>
                                                                       
                                                                    

                                                                <div class="input-group">
                                                                 
                                                                    <asp:DropDownList CssClass="form-control pull-left baggage-input-ctrl" ID="ddlOnwardMeal"
                                                                        Style="margin-bottom: 0;" runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                                    </asp:DropDownList>
                                                                   
                                                           
                                                           <div class="input-group-text">
                                                                    <asp:TextBox ID="lblOutMealPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>
                                                              </div>
                                                                </div>



                                                                 <b class="red_span" style="display: none" runat="server" id="errOnwardMeal"></b>
                                                                <asp:Label ID="lblOnMeal" CssClass="sm-text-baggage" runat="server" Text="" Visible="false"></asp:Label>
                                                            </div>

                                                          
                                                           <div class="col-md-6" id="addPaxReturnMeal" runat="server">
                                                                 
                                                                 
                                                                       <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                                            { %>

                                                                        <div> Return Meal: </div>
                                                          
                                                                        <%} %>





                                                                    <div class="input-group">
                                                                       
                                                         

                                                                        <asp:DropDownList ID="ddlInwardMeal" runat="server" Style="margin-bottom: 0;"
                                                                            CssClass="form-control pull-left" Visible="false" onchange="CalculateBaggagePrice()">
                                                                        </asp:DropDownList>
                                                                       
                                                                       
                                                                        


                                                                        <div class="input-group-text">

                                                                        <%if (request.Type == CT.BookingEngine.SearchType.Return)
                                                                            { %>
                                                       
                                                                        <asp:TextBox ID="lblInMealPrice" Text="0.00" Enabled="false" style="width:50px" runat="server"></asp:TextBox>
                                                          
                                                                        <%} %>

                                                                        </div>
                                                                    </div>
                                                       
                                                        <asp:Label ID="lblInMeal" runat="server" Text="" CssClass="sm-text-baggage" Visible="false"></asp:Label>
                                                        <b class="red_span" style="display: none" runat="server" id="errInwardMeal"></b>
                                                                </div>



                                                        </div>

                                                    </div>


                                                   



                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="collapse advPx-collapseContent" id="adpaggengerDetailsCollapse-<%#Container.ItemIndex%>">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <div class="col-md-3 col-lg-2">
                                                            <label>
                                                                <strong>Gender
                                                                    <%if (GenderRequired)
                                                                        { %><span class="red_span">*</span><%} %></strong>
                                                            </label>
                                                            <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control" CausesValidation="true"
                                                                ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Gender</asp:ListItem>
                                                                <asp:ListItem Value="1">Male</asp:ListItem>
                                                                <asp:ListItem Value="2">Female</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <b class="red_span" style="display: none" id="errPaxgender" runat="server"></b>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                <strong>Passport No
                                                                    <%if (PassportNoRequired)
                                                                        { %><span class="red_span">*</span><%} %></strong></label>
                                                            <asp:TextBox ID="txtPassportNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <b class="red_span" style="display: none" id="errPaxpassportno" runat="server"></b>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <label>
                                                                <strong>Passport Exp
                                                                    <%if (PassportExpiryRequired)
                                                                        { %><span class="red_span">*</span><%} %></strong>
                                                            </label>
                                                            <div class="row no-gutter">
                                                                <div class="col-xs-4">
                                                                    <asp:DropDownList ID="ddlPEDay" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                        <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-xs-4">
                                                                    <asp:DropDownList ID="ddlPEMonth" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                        <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                                        <asp:ListItem Value="01">Jan</asp:ListItem>
                                                                        <asp:ListItem Value="02">Feb</asp:ListItem>
                                                                        <asp:ListItem Value="03">Mar</asp:ListItem>
                                                                        <asp:ListItem Value="04">Apr</asp:ListItem>
                                                                        <asp:ListItem Value="05">May</asp:ListItem>
                                                                        <asp:ListItem Value="06">Jun</asp:ListItem>
                                                                        <asp:ListItem Value="07">Jul</asp:ListItem>
                                                                        <asp:ListItem Value="08">Aug</asp:ListItem>
                                                                        <asp:ListItem Value="09">Sep</asp:ListItem>
                                                                        <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                        <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                        <asp:ListItem Value="12">Dec</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-xs-4">
                                                                    <asp:DropDownList ID="ddlPEYear" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                        <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <b class="red_span" style="display: none" id="errPaxpeday" runat="server"></b><b
                                                                    class="red_span" style="display: none" id="errPaxpeMonth" runat="server"></b>
                                                                <b class="red_span" style="display: none" id="errPaxpeyear" runat="server"></b>
                                                            </div>

                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                <strong>Country
                                                                    <%if (CountryRequired)
                                                                        { %>
                                                                    <span class="red_span">*</span><%} %></strong></label>
                                                            <asp:DropDownList ID="ddlCountry" runat="server" AppendDataBoundItems="True" CssClass="form-control"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Select Country</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <b class="red_span" style="display: none" id="errPaxcountry" runat="server"></b>
                                                        </div>
                                                        <!--Lokesh 10May2017 : Added Nationality for additional pax-->
                                                        <div class="col-md-3">
                                                            <label>
                                                                <strong>Nationality
                                                                    <%if (NationalityRequired)
                                                                        { %>
                                                                    <span class="red_span">*</span><%} %></strong></label>
                                                            <asp:DropDownList ID="ddlNationality" runat="server" AppendDataBoundItems="True"
                                                                CssClass="form-control" CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Value="-1">Select Nationality</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <b class="red_span" style="display: none" id="errPaxNationality" runat="server"></b>
                                                        </div>
                                                        <!-- Praveen 18April2019 : Added FF Number for additional pax -->
                                                        <div class="col-md-3" >
                                                            <label>
                                                                <strong>Frequent Flyer </strong>
                                                            </label>
                                                            <div class="input-group tel-input-addon">
                                                                <div class="input-group-addon" >
                                                                    <asp:TextBox ID="txtAirline" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                                </div>
                                                                <asp:TextBox ID="txtFlight" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div id="divSeatPref<%#Container.ItemIndex%>" style="display:none" class="col-md-3">
                                                            <label>
                                                                <strong>Seat Preference</strong></label>
                                                            <asp:TextBox ID="txtSeatPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            <b class="red_span" style="display: none" id="B3" runat="server"></b>
                                                        </div>
                                                        <div id="divMealPref<%#Container.ItemIndex%>" style="display:none" class="col-md-3">
                                                            <label>
                                                                <strong>Meal Preference</strong></label>
                                                            <asp:TextBox ID="txtMealPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            <b class="red_span" style="display: none" id="B4" runat="server"></b>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdnProfileId" Value=""/>
                                                        <div class="clearfix">
                                                        </div>
                                                        <div class="form-group baggage-wrapper"  id="divFlexAd<%#Container.ItemIndex%>" style="display:none" ><div class="row padding-0 marbot_10" id="tblFlexFieldsAd" style="padding:15px" runat="server">
                                                        </div></div>
                                                        <div class="clearfix">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <div>
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                 <%if ((onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet)
                       || (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp)
                       || (onwardResult != null && (onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) 
                       || (returnResult != null && (returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))
                       || (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo)
                       || (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp) 
                       || (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI) || (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI))
                     { %>
                <div>
                    <a id="btnSeatselect" class="btn btn-info btn-lg" data-toggle="modal" onclick="showpopup(''); return false;">Select seats</a>
                </div>
                <%} %>
                <div>
                    <label style="padding-right: 10px; float: right; text-align: right">
                        <asp:Button ID="imgContinue" CssClass="button-normal" Text="Continue" runat="server"
                            CausesValidation="true" OnClientClick="return validate();" OnClick="imgContinue_Click"
                            ValidationGroup="email" />
                    </label>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <% if (hdfOnwardFareAction.Value != "Updated" && hdfReturnFareAction.Value != "Updated")
        { %>

    <%if (errorMessage.Length == 0)
        {
            double original = Convert.ToDouble((originalBaseFare + originalTax).ToString("N" + agency.DecimalValue));
            double repriced = Convert.ToDouble((repricedBaseFare + repricedTax).ToString("N" + agency.DecimalValue));

            if (original != repriced && repriced > 0)
            {%>
    <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="FareRuleHead">Price for the Itinerary has been changed!</h4>
                </div>
                <div class="modal-body">
                    <div id="FareRuleBody" style="text-align: center">
                        <div>
                            <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">Price has been changed for Itinerary</span>
                        </div>
                        <div>
                            <div class="col-md-6">
                                <b>Original Fare :</b>
                                <label>
                                    <%=(originalBaseFare + originalTax).ToString("N" + agency.DecimalValue)%>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <b>Changed Fare :</b>
                                <label style="color: Red">
                                    <%=(repricedBaseFare + repricedTax).ToString("N" + agency.DecimalValue)%>
                                </label>
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="padtop_4">
                            <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="PriceContinue()" />
                        </div>
                        <div class="padtop_4">
                            <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                                style="width: 150px;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //document.getElementById('FareDiff').style.display = 'block';
        $('#FareDiff').modal('show')
        document.getElementById('<%=imgContinue.ClientID %>').style.display = 'none';
        document.getElementById('priceChange').style.display = 'block';
    </script>
    <%}
        }
        else
        {%>

    <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <center>
                        <h2>
                        <%=errorMessage%></h2>
                        <br />
                        <br />
                        <input type="button" id="btnCancel" value="Search Again" onclick="PriceCancel()" />
                    </center>
                </div>
            </div>
        </div>
    </div>

    <%}
        }%>


    <asp:HiddenField runat="server" ID="hdnAddNationlities" />


    <div id="paxAddSerNotification" style="display: none;" class="price_change_block">
        <div class="head_bg">
            <h5>
                <center>
                        Notification!</center>
            </h5>
        </div>
        <div class="body" style="overflow: auto; width: 100%; text-align: center">
            <div>
                <span style="color: Red; font-size: 14px; font-weight: bold;">Selected Nationality is
                        not authorised to book this flight!</span>
            </div>
            <div>
                <div class="clearfix">
                </div>
            </div>
            <div class="padtop_4">
                <input type="button" id="btnAddSerCancel" class="btn but_b" value="Close" onclick="cancelNotification()"
                    style="width: 150px;" />
            </div>
        </div>
    </div>



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn btn-default" onclick="Saveseatinfo()" >Save seats</button>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="Segments"></div>
                                </div>
                                <div class="col-md-12 term_conditions"> 
                                    To ensure the safety of passengers, including yours, adults traveling with Infants, pregnant ladies, those who require wheelchair and who require safety assistance in case of emergency evacuation cannot be assigned emergency exit seats, including 1st row on few aircraft.
                                    Emergency exit rows and 1st row of some aircraft cannot be assigned to adults traveling with Infants, pregnant ladies, those who require wheelchair and who require safety assistance in case of emergency evacuation. For further details, please read the <a href="#">T&C</a>.
                                </div>
                                <div class="col-md-12">
                                    <a id="btnSeatreset" onclick="return ResetSeatMap(); return false;" class="btn btn_reset"> RESET </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <div class="light_wrapper">
                                        <div class="col-md-4 col-xs-4"> <span class="selected"> &nbsp;Selected </span>  </div>
                                        <div class="col-md-4 col-xs-4"> <span class="available"> &nbsp;Available </span>   </div>
                                        <div class="col-md-4 col-xs-4"> <span class="occupied"> &nbsp;Occupied </span>  </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="scroll_skeleton"> 
                                        <div id="seatslayout"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="Saveseatinfo()" >Save seats</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <asp:TextBox runat="server" style="display:none" ID="txtPaxSeatInfo" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade pymt-modal" data-backdrop="static" id="Seatsalert" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width:500px;">
              <div class="modal-header">
                  <h4 class="modal-title" id="SeatsalertLabel">Seat assignment alert</h4>
              </div>
              <div class="modal-body">
                <div id="divSeatsalertText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;"></div>                  
                <div class="padtop_4">
                    <a id="SeatsalertContinue" class="btn but_b" onclick="Continue('Seatsalert')">Please select different seats</a></div><div></div>
                    <asp:Button ID="btnContinue" CssClass="btn but_b" Text="Continue" runat="server" style="margin-top: 10px;"
                            CausesValidation="true" OnClientClick="return validate();" OnClick="imgContinue_Click"
                            ValidationGroup="email" />
                </div>
              </div>
            </div>
          </div>
        <!-- Search Passenger Modal -->
    <div id="SearchPassenger" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header pt-3 pb-2">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search Passenger</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                    <div id="SearchFilters" class="row custom-gutter"></div>
                    </div>
                </div>
                <div class="modal-footer py-2"> <button type="button" class="button" onclick="SearchPassenger(); return false">Search</button> </div>
                <div id="PassengerDetailsList" class="px-3 table table-responsive" ></div>
            </div>
        </div>
    </div>
    <!--Search Passenger Modal End-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
