﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Title="" CodeBehind="BookOffline.aspx.cs" Inherits="CozmoB2BWebApp.BookOffline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    
<link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <style>
    .red { color:red; }
    .search_container a, a.search_container  { color:#fff; }    
    .btn-link,.btn-link:hover{
    	display:inline-block; background-color:gray; color:#fff; 
    	border-radius:50%; border: solid 1px #fff;  
 		padding: 0px 2px 1px  2px;   	
    }
	.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover{
		border: 1px solid #60c231;
		background: #60c231;
	}

    .disabled {
        color: #ccc;
        pointer-events:none;
    }
    </style>

<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="yui/build/event/event-min.js"></script>
<script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
<script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
<script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
<script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
<script src="yui/build/container/container-min.js" type="text/javascript"></script>

<script type="text/javascript">

    var specialKeys = new Array();

    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right

    function IsAlphaNumeric(e) {
        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        return ((keyCode >= 46 && keyCode <= 58) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
            || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
    }

    function IsAlpha(e) {
        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        return ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
            || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
    }

    function IsNumeric(e) {
        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        return ((keyCode >= 46 && keyCode <= 58) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
    }

    function ValidateEmail(event) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(event.value)) {
            ShowError('Invalid email id'); event.value = ''; event.focus();
        }
    }

    function ShowAlert(id, title, message, closefn) {

        $('#' + id).children().remove();
        $('#' + id).append('<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"> ' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">' + title + '</h4> </div> ' +
            '<div class="modal-body"> <div class="form-group"> </div><div class="row custom-gutter"> <strong>' + message + '</strong></div></div>' +
            '<div class="modal-footer"> <button type="button" class="button" onclick="' + closefn + '">OK</button> </div>' +
            '</div></div>');
        $('#' + id).modal('show');
    }

    function Refresh() {
        window.location = 'BookOffline.aspx';
    }

    function Validate() {
                
        focusoncntrl = true;
        
        DelErrorClass();
        if ($('input[id=chkCopyToHotel]').is(':checked')) {

            if (ValidateFlight() && !ValidateHotel()) {

                $("[id*=rbtnPrdct] label:contains('Hotel')").closest("td").find("input").attr("checked", "checked");
                Prdctchange(document.getElementById('<%=rbtnPrdct.ClientID %>').id);
            }
        }
        else if ($("[id*=ctl00_cphTransaction_rbtnPrdct] input:checked").val() == 'A')
            ValidateFlight();
        else 
            ValidateHotel();

        ValidateCommon();
        return focusoncntrl;
    }

    function ValidateFlight() {

        Journeyval = $("[id*=ctl00_cphTransaction_rbtnJrnyType] input:checked").val();
        if (Journeyval == 'M' && sMultiCityCnt > 0) {

            for (var c = 1; c <= sMultiCityCnt; c++) {

                validmandatory('txtOrigin' + c, 'textbox', '', 'Please enter segment - ' + c + ' Origin', false);
                validmandatory('txtDestination' + c, 'textbox', '', 'Please enter segment - ' + c + ' Destination', false);
                validmandatory('DepDate' + c, 'textbox', '', 'Please enter segment - ' + c + ' Departure date', false);
                //validmandatory('txtAirline' + c, 'textbox', '', 'Please enter segment - ' + c + ' Airline', false);
                //validmandatory('txtFlightNo' + c, 'textbox', '', 'Please enter segment - ' + c + ' Flight no', false);
                //validmandatory('ddlcabinclass' + c, 'textbox', '', 'Please select segment - ' + c + ' cabin class', false);
                ValAirports('txtOrigin' + c, 'txtDestination' + c, 'Origin and Destination airports should not be same for segment-' + c);
            }
        }
        else {

            validmandatory('txtOrigin', 'textbox', '', 'Please enter Origin', true);
            validmandatory('txtDestination', 'textbox', '', 'Please enter Destination', true);
            validmandatory('DepDate', 'textbox', '', 'Please enter Departure date', true);
            //validmandatory('txtPreferredAirline', 'textbox', '', 'Please enter Airline', true);
            //validmandatory('txtFlightNo', 'textbox', '', 'Please enter Flight no', true);
            //validmandatory('ddlOnwardcabin', 'dropdown', '', 'Please select cabin class', true);
            ValAirports(document.getElementById('<%=txtOrigin.ClientID %>').id, document.getElementById('<%=txtDestination.ClientID %>').id,
                'Origin and Destination airports should not be same');

            if (Journeyval == 'R') {

                validmandatory('ReturnDateTxt', 'textbox', '', 'Please enter return journey date', true);
                //validmandatory('txtPreferredAirlineRet', 'textbox', '', 'Please enter return Airline', true);
                //validmandatory('txtFlightNoRet', 'textbox', '', 'Please enter return Flight no', true);
                //validmandatory('ddlReturncabin', 'dropdown', '', 'Please select return cabin class', true);
            }
                
        }

        if (FlexInfo != null && sFlexcnt > 0) {
            ValidateFlex('1');
        }

        return focusoncntrl;
    }

    function ValAirports(Fromid, Toid, errmsg) {

        if (document.getElementById(Fromid).value == document.getElementById(Toid).value) {
            focusoncntrl = false;            
            ShowError(errmsg);
        }
    }

    function ValidateHotel() {

        validmandatory('txtHotelCity', 'textbox', '', 'Please enter hotel city', false);
        validmandatory('ddlHotelCountry', 'dropdown', '--Select--', 'Please enter hotel country', true);
        validmandatory('txthotelChkInDate', 'textbox', '', 'Please enter hotel check in date', false);
        validmandatory('txthotelChkOutDate', 'textbox', '', 'Please enter hotel check out date', false);

        if (FlexInfo != null && sFlexcnt > 0) {
            ValidateFlex('2');
        }

        return focusoncntrl;
    }

    /* To validate flex information for both flight and hotel */
    function ValidateFlex(Prdct) {

        var prdmsg = Prdct == '1' ? 'Flight' : 'Hotel';
        $.each(FlexInfo, function (key, col) {

            if (col.FlexProductId == Prdct && col.flexMandatoryStatus == 'Y' && $('#divFlex' + key) != null &&
                document.getElementById('divFlex' + key).style.display != 'none') {
                                
                if (col.flexControl == 'T')
                    validmandatory('txtFlex' + key, 'textbox', '', 'Please enter ' + prdmsg + ' ' + col.flexLabel, false);
                else if (col.flexControl == 'L' && $('#ddlFlex' + key).children('option').length > 1)
                    validmandatory('ddlFlex' + key, 'dropdown', '--Select--', 'Please select ' + prdmsg + ' ' + col.flexLabel, false);
                else if (col.flexControl == 'D') 
                    validdateddl('Flex' + key, true, 'Please select ' + prdmsg + ' ' + col.flexLabel);
            }                
        }); 
    }

    /* To validate common info for both hotel and flight booking */
    function ValidateCommon() {

        for (var c = 1; c <= sPaxCnt; c++) {

            validmandatory('txtLastName' + c, 'textbox', '', 'Please enter passenger - ' + c + ' Last Name', false);
            validmandatory('txtFirstName' + c, 'textbox', '', 'Please enter passenger - ' + c + ' First Name', false);
            validmandatory('ddlTitle' + c, 'dropdown', '--Select Title--', 'Please enter passenger - ' + c + ' Title', false);
            if (c == 1)
                validmandatory('txtEmail' + c, 'textbox', '', 'Please enter passenger - ' + c + ' email', false);
            validdateddl('DOB' + c, false, 'Please select passenger ' + c + ' date of birth');
            validdateddl('PassportExpiry' + c, false, 'Please select passenger ' + c + ' passport expiry');
        }

        if (document.getElementById('divTTP').style.display != 'none') {
            validmandatory('ddlFlightTravelReasons', 'dropdown', '--Select Reason--', 'Please select travel type', true);
            validmandatory('ddlFlightEmployee', 'dropdown', '--Select Traveller--', 'Please select traveler', true);
        }

        if (document.getElementById('divReference').style.display != 'none') {
            if (!validmandatory('txtReference', 'textbox', '', 'Please enter reference no', false))
                document.getElementById('txtReference').focus();
        }

        if (!($('input[id=chkConfirm]').is(':checked')) && focusoncntrl) {
            ShowError('Please accept term and conditions');
            focusoncntrl = false;
        }
        return focusoncntrl;
    }

    /* To validate date related drop downs */
    function validdateddl(dtField, ismand, message) {

        if (!ismand)
            ismand = (document.getElementById('ddldt' + dtField).value != '' || document.getElementById('ddlmn' + dtField).value != '' ||
                document.getElementById('ddlyr' + dtField).value != '');

        if (ismand) {

            validmandatory('ddldt' + dtField, 'dropdown', 'Day', message + ' Date', false);
            validmandatory('ddlmn' + dtField, 'dropdown', 'Month', message + ' Month', false);
            validmandatory('ddlyr' + dtField, 'dropdown', 'Year', message + ' Year', false);
        }
    }

    /* To delete error class for all controls */
    function DelErrorClass() {

        var errmsgs = $('.form-text-error');
        if (errmsgs != null && errmsgs.length > 0) {

            $.each(errmsgs, function (key, cntrl) {

                $('#' + cntrl.id).removeClass('form-text-error');
            });
        }
    }

    function validmandatory(cntrlid, cntrltype, defaultval, errmsg, issrvrcntrl) {

        var valid = true;

        cntrlid = (issrvrcntrl && cntrlid.indexOf('ctl00_cphTransaction_') < 0) ? 'ctl00_cphTransaction_' + cntrlid : cntrlid;
        //cntrltype = issrvrcntrl ? cntrltype : 'textbox'; 

        if (cntrltype == 'dropdown' && document.getElementById('s2id_' + cntrlid).style.display != 'none') {

            var ddlvalue = document.getElementById('s2id_' + cntrlid).innerText.trim();
            if (ddlvalue == defaultval || ddlvalue == '') {
                $('#s2id_' + cntrlid).parent().addClass('form-text-error');
                if (focusoncntrl)
                    document.getElementById('s2id_' + cntrlid).focus();
                valid = false;
                if (errmsg != null && errmsg != '')
                    ShowError(errmsg);
            }
            else {
                $('#s2id_' + cntrlid).parent().removeClass('form-text-error');
            }
        }

        if (cntrltype == 'textbox' && document.getElementById(cntrlid).style.display != 'none') {
            var txtvalue = document.getElementById(cntrlid).value.trim();
            if (txtvalue == '' || txtvalue == defaultval || txtvalue == errmsg) {
                $('#' + cntrlid).addClass('form-text-error');
                if (focusoncntrl)
                    document.getElementById(cntrlid).focus();
                valid = false;
                if (errmsg != null && errmsg != '')
                    ShowError(errmsg);
            }
            else {
                $('#' + cntrlid).removeClass('form-text-error');
            }
        }
        focusoncntrl = focusoncntrl ? valid : focusoncntrl;
        
        return valid;
    }

    function clearcntrls(cntrltype, cntrlid, sectorid, defaultval) {

        if (cntrltype == 'textbox') {
            document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorid).value = defaultval;
        }

        if (cntrltype == 'dropdown') {

            $('#s2id_ctl00_cphTransaction_' + cntrlid + sectorid).select2('val', defaultval);
            document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorid).innerHTML = defaultval;
        }
    }

    /* To display error message in JS */
    function ShowError(errmsg) {

        var errmsgs = $('div.toast-message'); var display = true;
        if (errmsgs != null && errmsgs.length > 0) {

            $.each(errmsgs, function (key, msgs) {

                if (errmsg == msgs.innerText) {
                    display = false;
                    return false;
                }
            });
        }
        if (display)
            toastr.error(errmsg);
    }

</script>
<script>

    /* To display yahoo calender */
    var calcntrlid, calender, maxdtcntrlid, sdtvldmsg;
    function ShowYahooCalender(cntnr, cntrl, ismindate, mindateid, mindtmsg, maxdateid, dtvldmsg) {
        
        if (!$(cntnr) || !$(cntrl) || cntnr == null || cntrl == null || cntnr == 'undefined' || cntrl == 'undefined')
            return;

        if (mindateid != null && mindateid != '' && document.getElementById(mindateid) != null && document.getElementById(mindateid).value == '') {
            ShowError(mindtmsg); return;
        }            

        calcntrlid = cntrl.id;
        maxdtcntrlid = maxdateid; sdtvldmsg = dtvldmsg;

        if (calender != 'undefined' && calender != null)
            calender.hide();
                
        var today = new Date();
        calender = new YAHOO.widget.CalendarGroup("calender", cntnr.id);

        if (ismindate) {

            if (mindateid != null && mindateid != '' && document.getElementById(mindateid) != null && document.getElementById(mindateid).value != '') {
                var datemin = document.getElementById(mindateid).value.split('/');
                calender.cfg.setProperty("minDate", datemin[1] + '/' + datemin[0] + '/' + datemin[2]);
            }
            else
                calender.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        }
        
        calender.cfg.setProperty("close", true);
        calender.selectEvent.subscribe(setDate);
        calender.render();
        document.getElementById(cntnr.id).style.display = "block";
    }

    /* To set the selected date from yahoo calender to text box */
    function setDate() {

        var date1 = calender.getSelectedDates()[0];

        if (date1 != null && date1 != '' && date1 != 'undefined') {

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }

            if (maxdtcntrlid != null && document.getElementById(maxdtcntrlid) != null && document.getElementById(maxdtcntrlid).value != '' &&
                !(Validatedate(day + "/" + (month) + "/" + date1.getFullYear(), document.getElementById(maxdtcntrlid).value))) {
                ShowError(sdtvldmsg); return;
            }

            document.getElementById(calcntrlid).value = day + "/" + (month) + "/" + date1.getFullYear();
            calender.hide();
        }
        
    }

    /* To validate dates */
    function Validatedate(mindate, maxdate) {

        var dt1 = mindate.split('/'); var dt2 = maxdate.split('/');
        mindate = new Date(dt1[1] + '/' + dt1[0] + '/' + dt1[2]); maxdate = new Date(dt2[1] + '/' + dt2[0] + '/' + dt2[2]);
        return (mindate <= maxdate);
    }

    /* To initialize yahoo calender */
    YAHOO.util.Event.addListener(window, "load", ShowYahooCalender);

    /* To show calender for dynamic fields for multi city journey */
    function DisplayCalender(cntnr, cntrl, ismindate, ismincntrl) {

        var previd = Math.ceil(cntrl.id.replace('DepDate', '')) - 1;
        var prevsegid = !ismincntrl ? null : 'DepDate' + previd;
        
        ShowYahooCalender(cntnr, cntrl, ismindate, prevsegid, 'Please select previous segment date', null, null);
    }
    
</script>
    
<script type="text/javascript">

    $(document).ready(function () {

        AddNewPax(document.getElementById('ddlPax'));
        if (document.getElementById('<%=txtFlexInfo.ClientID %>').value != '')
            BindFlexFields();
        if (document.getElementById('<%=txtCorpProfile.ClientID %>').value != '')
            BindCorpProfile(JSON.parse(document.getElementById('<%=txtCorpProfile.ClientID %>').value));
        sUserId = document.getElementById('<%=hdnUserId.ClientID %>').value;
        document.getElementById('divTTP').style.display = document.getElementById('divProfiles').style.display =
            document.getElementById('<%=hdnIsCorp.ClientID %>').value == 'Y' ? 'block' : 'none';
    });

    function FormatDate(dtcntrl, seprator, format, inctime) {

        var formatdt = '';
        var dateval = document.getElementById(dtcntrl).value.split(seprator);
        if (format == 'yyyy-mm-dd')
            formatdt = dateval[2] + '-' + dateval[1] + '-' + dateval[0] + (inctime ? 'T00:00:00' : '');
        return formatdt;
    }

    function SaveHotel() {
                
        HItinerary.RequestType = document.getElementById('ddlReqType').value;        
        HItinerary.ConfirmationNo = document.getElementById('txtReference').value;
        HItinerary.cityRef = document.getElementById('txtHotelCity').value.split(',')[0];
        HItinerary.CountryCode = document.getElementById('<%=ddlHotelCountry.ClientID %>').value;
        HItinerary.StartDate = FormatDate('txthotelChkInDate', '/', 'yyyy-mm-dd', true);
        HItinerary.EndDate = FormatDate('txthotelChkOutDate', '/', 'yyyy-mm-dd', true);
        HItinerary.AppartmentName = document.getElementById('txtHoltelApName').value;
        HItinerary.SpecialRequest = document.getElementById('txtSSR').value;
        HItinerary.Additionalinfo = document.getElementById('txtAddInfo').value;

        var sRooms = []; sRooms.push('0');
        var sRoomsPax = []; sRoomsPax.push('0');

        for (var p = 1; p <= sPaxCnt; p++) {

            var rno = document.getElementById('ddlRoomNo' + p).value;
            if (jQuery.inArray(rno, sRooms) == -1)
                sRooms.push(rno);
            var ptype = $('#divpaxhdr' + p).text().indexOf('Adult') > -1 ? 'A' : 'C';
            if (sRoomsPax[rno] == null || sRoomsPax[rno] == 'undefined')
                sRoomsPax.push(ptype);
            else
                sRoomsPax[rno] = sRoomsPax[rno] + '-' + ptype;
        }

        for (var p = 1; p < sRooms.length; p++) {

            HItinerary.Roomtype[p - 1].RoomTypeCode = sRooms[p];            
            HItinerary.Roomtype[p - 1].NoOfUnits = sRoomsPax[p].split('-').length;
            HItinerary.Roomtype[p - 1].AdultCount = sRoomsPax[p].split('A').length - 1;
            HItinerary.Roomtype[p - 1].ChildCount = sRoomsPax[p].split('C').length - 1;
            HItinerary.Roomtype[p - 1].PassenegerInfo = SavePax(HItinerary.Roomtype[p].PassenegerInfo, sRooms[p]);
        }
        HItinerary.NoOfRooms = sRooms.length - 1;
    }

    function SaveFlight() {

        Journeyval = $("[id*=ctl00_cphTransaction_rbtnJrnyType] input:checked").val();

        FItinerary.RequestType = document.getElementById('ddlReqType').value;
        FItinerary.PNR = document.getElementById('txtReference').value;
        var datetime;

        if (Journeyval == 'M' && sMultiCityCnt > 0) {

            FItinerary.Origin = document.getElementById('txtOrigin1').value.split(')')[0].replace('(', '');
            FItinerary.destination = document.getElementById('txtDestination' + sMultiCityCnt).value.split(')')[0].replace('(', '');
            FItinerary.AirlineCode = document.getElementById('txtAirline1').value.split(',')[0];
            FItinerary.TravelDate = FormatDate('DepDate1', '/', 'yyyy-mm-dd', true);

            for (var i = 1; i <= sMultiCityCnt; i++) {

                FItinerary.Segments[i - 1].Origin = document.getElementById('txtOrigin' + i).value.split(')')[0].replace('(', '');
                FItinerary.Segments[i - 1].Destination = document.getElementById('txtDestination' + i).value.split(')')[0].replace('(', '');       
                FItinerary.Segments[i - 1].DepartureTime = FItinerary.Segments[i - 1].ArrivalTime = FormatDate('DepDate' + i, '/', 'yyyy-mm-dd', true);
                FItinerary.Segments[i - 1].Airline = document.getElementById('txtAirline' + i).value.split(',')[0];
                FItinerary.Segments[i - 1].CabinClass = document.getElementById('ddlcabinclass' + i).value;
                FItinerary.Segments[i - 1].FlightNumber = document.getElementById('txtFlightNo' + i).value;
                FItinerary.Segments[i - 1].CreatedBy = sUserId;
                FItinerary.Segments[i - 1].BookingClass = 'E'; 
                FItinerary.Segments[i - 1].Preftimespan = document.getElementById('ddlPrefTime' + i).value;
            }
        }
        else {
            
            FItinerary.Origin = document.getElementById('<%=txtOrigin.ClientID %>').value.split(')')[0].replace('(', '');
            FItinerary.destination = document.getElementById('<%=txtDestination.ClientID %>').value.split(')')[0].replace('(', '') ;
            FItinerary.AirlineCode = document.getElementById('<%=txtPreferredAirline.ClientID %>').value.split(',')[0];

            FItinerary.Segments[0].Origin = document.getElementById('<%=txtOrigin.ClientID %>').value.split(')')[0].replace('(', '');
            FItinerary.Segments[0].Destination = document.getElementById('<%=txtDestination.ClientID %>').value.split(')')[0].replace('(', '') ;
            FItinerary.Segments[0].DepartureTime = FItinerary.TravelDate = FItinerary.Segments[0].ArrivalTime =
                FormatDate(document.getElementById('<%=DepDate.ClientID %>').id, '/', 'yyyy-mm-dd', true);
            FItinerary.Segments[0].Airline = document.getElementById('<%=txtPreferredAirline.ClientID %>').value.split(',')[0];
            FItinerary.Segments[0].FlightNumber = document.getElementById('<%=txtFlightNo.ClientID %>').value;
            FItinerary.Segments[0].CabinClass = document.getElementById('<%=ddlOnwardcabin.ClientID %>').value;
            FItinerary.Segments[0].CreatedBy = sUserId;
            FItinerary.Segments[0].BookingClass = 'E'; 
            FItinerary.Segments[0].Preftimespan = document.getElementById('ddlOnwPrefTime').value;

            if (Journeyval == 'R') {

                FItinerary.Segments[1].Origin = document.getElementById('<%=txtDestination.ClientID %>').value.split(')')[0].replace('(', '');
                FItinerary.Segments[1].Destination = document.getElementById('<%=txtOrigin.ClientID %>').value.split(')')[0].replace('(', '');
                FItinerary.Segments[1].DepartureTime = FItinerary.Segments[1].ArrivalTime =
                    FormatDate(document.getElementById('<%=ReturnDateTxt.ClientID %>').id, '/', 'yyyy-mm-dd', true);
                FItinerary.Segments[1].Airline = document.getElementById('<%=txtPreferredAirlineRet.ClientID %>').value.split(',')[0];
                FItinerary.Segments[1].FlightNumber = document.getElementById('<%=txtFlightNoRet.ClientID %>').value;
                FItinerary.Segments[1].CabinClass = document.getElementById('<%=ddlReturncabin.ClientID %>').value;
                FItinerary.Segments[1].CreatedBy = sUserId;
                FItinerary.Segments[1].BookingClass = 'E';
                FItinerary.Segments[1].Preftimespan = document.getElementById('ddlRetPrefTime').value;
            }
        }
        FItinerary.Passenger = SavePax(FItinerary.Passenger, '0');
    }

    function SavePax(paxobj, roomno) {

        for (var i = 1; i <= sPaxCnt; i++) {

            if (document.getElementById('ddlRoomNo' + i).value == roomno || roomno == '0') {
                
                var PaxType = $('#divpaxhdr' + i).text();   

                paxobj[i - 1].type = PaxType.indexOf('Adult') > -1 ? '1' : PaxType.indexOf('Child') > -1 ? '2' : '3';
                paxobj[i - 1].Title = document.getElementById('ddlTitle' + i).value;
                paxobj[i - 1].FirstName = document.getElementById('txtFirstName' + i).value;
                paxobj[i - 1].LastName = document.getElementById('txtLastName' + i).value;
                paxobj[i - 1].PassportNo = document.getElementById('txtPassportNo' + i).value;
                paxobj[i - 1].Country = document.getElementById('txtPlaceOfIssue' + i).value;
                paxobj[i - 1].Email = document.getElementById('txtEmail' + i).value;
                paxobj[i - 1].AddressLine1 = document.getElementById('txtAddress' + i).value;
                paxobj[i - 1].CellPhone = '00-0000000000';
                if (i == 1 && document.getElementById('<%=ddlFlightEmployee.ClientID %>') != null && document.getElementById('<%=ddlFlightEmployee.ClientID %>').value != '')
                    paxobj[i - 1].CorpProfileId = document.getElementById('<%=ddlFlightEmployee.ClientID %>').value.split('-')[0];
                if (document.getElementById('ddlyrDOB' + i).value != '')
                    paxobj[i - 1].DateOfBirth = document.getElementById('ddlyrDOB' + i).value + '-' + document.getElementById('ddlmnDOB' + i).value
                        + '-' + document.getElementById('ddldtDOB' + i).value + 'T00:00:00';
                if (document.getElementById('ddlyrPassportExpiry' + i).value != '')
                    paxobj[i - 1].PassportExpiry = document.getElementById('ddlyrPassportExpiry' + i).value + '-' +
                        document.getElementById('ddlmnPassportExpiry' + i).value + '-' + document.getElementById('ddldtPassportExpiry' + i).value + 'T00:00:00';       
                paxobj[i - 1].CreatedBy = sUserId;
                if (FlexInfo != null) 
                    paxobj[i - 1].FlexDetailsList = SaveFlexInfo(paxobj[i - 1].FlexDetailsList);
            }
        }
        return paxobj;
    }

    function SaveFlexInfo(FlexDetailsList) {

        $.each(FlexInfo, function (key, col) {                   

            if (col.FlexProductId == Prdct || Prdct == '0') {
                
                var flexdat = '';
                if (col.flexControl == 'T')
                        flexdat = document.getElementById('txtFlex' + key).value;
                    else if (col.flexControl == 'L' && document.getElementById('ddlFlex' + key).value != '' && document.getElementById('ddlFlex' + key).value != '-1')
                        flexdat = document.getElementById('ddlFlex' + key).selectedOptions[0].text;
                    else if (col.flexControl == 'D') {
                        flexdat = document.getElementById('ddldtFlex' + key).value + '/' +
                            document.getElementById('ddlmnFlex' + key).value + '/' + document.getElementById('ddlyrFlex' + key).value;
                }

                if (flexdat != null && flexdat != '' && flexdat.replace('//', '') != '') {

                    FlexDetailsList[key].FlexLabel = col.flexLabel;
                    FlexDetailsList[key].FlexId = col.flexId;
                    FlexDetailsList[key].ProductID = Prdct;
                    FlexDetailsList[key].FlexGDSprefix = col.flexGDSprefix;                        
                    FlexDetailsList[key].FlexData = flexdat;
                }       
            }                
        }); 

        return FlexDetailsList;
    }

    function SaveBooking() {

        try {

            if (!Validate())
                return false;

            document.getElementById('ctl00_upProgress').style.display = 'block';
            setTimeout(function () {

                //if (Validate()) {
            
                    FItinerary = JSON.parse(document.getElementById('<%=txtOffFltInfo.ClientID %>').value);            
                    HItinerary = JSON.parse(document.getElementById('<%=txtOffHotelInfo.ClientID %>').value);

                    HItinerary.CreatedBy = FItinerary.CreatedBy = sUserId;
                    if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null && document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').value != '')
                        HItinerary.CorpTravelReasonId = FItinerary.CorpTravelReasonId = document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').value.split('~')[0];
                    Prdct = $('input[id=chkCopyToHotel]').is(':checked') ? '0' : $("[id*=ctl00_cphTransaction_rbtnPrdct] input:checked").val() == 'A' ? '1' : '2';
            
                    if (Prdct == '1') {
                        SaveFlight();
                    }                
                    else if (Prdct == '2')
                        SaveHotel();
                    else {
                        SaveFlight();  SaveHotel(); 
                    }

                    var ddldata = AjaxCall('BookOffline.aspx/SaveOfflineBooking',
                        "{'sFltitinerary':'" + ((Prdct == '1' || Prdct == '0') ? JSON.stringify(FItinerary) : '') +
                        "', 'sHotelitinerary':'" + ((Prdct == '2' || Prdct == '0') ? JSON.stringify(HItinerary) : '') + "'}");
                    ShowAlert('divAlert', 'Alert', 'Reference no - ' + ddldata, 'Refresh();');
                //}
            
                document.getElementById('ctl00_upProgress').style.display = 'none';
                //window.location = 'BookOffline.aspx';
            }, 100);                  
        }
        catch(exception)
        {
            alert(exception);
            ShowAlert('divAlert', 'Alert', 'Failed to save the booking, please contact admin', '');
        }

        return false;
    }

    /* To get data, month and year individually */
    function GetDateCmp(dateval, datecmp, seprator) {

        var arrdt = dateval.split(seprator);
        return arrdt[0] != '0001' ? (datecmp == 'D' ? arrdt[2] : datecmp == 'M' ? arrdt[1] : arrdt[0]) : '';
    }

    /* To Bind profile data for both flight and hotel */
    function BindCorpProfile(CorpInfo) {

        try {
            
            if (CorpInfo != null && CorpInfo != '') {

                $("#s2id_ddlTitle1").select2("val", CorpInfo.Title);
                document.getElementById('txtFirstName1').value = CorpInfo.SurName;
                document.getElementById('txtLastName1').value = CorpInfo.Name;
                document.getElementById('txtPassportNo1').value = CorpInfo.PassportNo;
                document.getElementById('txtPlaceOfIssue1').value = CorpInfo.PlaceOfIssue;
                document.getElementById('txtEmail1').value = CorpInfo.Email;
                document.getElementById('txtAddress1').value = CorpInfo.Address1;

                $("#s2id_ddldtDOB1").select2("val", CorpInfo.DateOfBirth != null ? GetDateCmp(CorpInfo.DateOfBirth.split('T')[0], 'D', '-') : '');
                $("#s2id_ddlmnDOB1").select2("val", CorpInfo.DateOfBirth != null ? GetDateCmp(CorpInfo.DateOfBirth.split('T')[0], 'M', '-') : '');
                $("#s2id_ddlyrDOB1").select2("val", CorpInfo.DateOfBirth != null ? GetDateCmp(CorpInfo.DateOfBirth.split('T')[0], 'Y', '-') : '');

                $("#s2id_ddldtPassportExpiry1").select2("val", CorpInfo.DateOfExpiry != null ? GetDateCmp(CorpInfo.DateOfExpiry.split('T')[0], 'D', '-') : '');
                $("#s2id_ddlmnPassportExpiry1").select2("val", CorpInfo.DateOfExpiry != null ? GetDateCmp(CorpInfo.DateOfExpiry.split('T')[0], 'M', '-') : '');
                $("#s2id_ddlyrPassportExpiry1").select2("val", CorpInfo.DateOfExpiry != null ? GetDateCmp(CorpInfo.DateOfExpiry.split('T')[0], 'Y', '-') : '');

                if (FlexInfo != null) {

                    $.each(FlexInfo, function (key, col) {

                        var flexData = '';
                        if (col.FlexProductId == '1' || col.FlexProductId == '2') {

                            if (CorpInfo.DtProfileFlex != null) {

                                $.each(CorpInfo.DtProfileFlex, function (flexkey, flexcol) {
                                    if (flexcol.flexLabel == col.flexLabel && flexcol.FlexProductId == col.FlexProductId)
                                        flexData = flexcol.Detail_FlexData;
                                });
                            }
                            AssignFlexData(col.flexControl, key, flexData);
                        }
                    });
                }
            }
            else {

                $("#s2id_ddlTitle1").select2("val", '');
                document.getElementById('txtPassportNo1').value = document.getElementById('txtLastName1').value = document.getElementById('txtFirstName1').value = '';
                document.getElementById('txtAddress1').value = document.getElementById('txtEmail1').value = document.getElementById('txtPlaceOfIssue1').value = '';

                $("#s2id_ddldtDOB1").select2("val", '');
                $("#s2id_ddlmnDOB1").select2("val", '');
                $("#s2id_ddlyrDOB1").select2("val", '');

                $("#s2id_ddldtPassportExpiry1").select2("val", '');
                $("#s2id_ddlmnPassportExpiry1").select2("val", '');
                $("#s2id_ddlyrPassportExpiry1").select2("val", '');

                if (FlexInfo != null) {

                    $.each(FlexInfo, function (key, col) {

                        if (col.FlexProductId == '1' || col.FlexProductId == '2') 
                            AssignFlexData(col.flexControl, key, '');
                    });
                }
            }
        }
        catch(exception)
        {

        }
    }

    /* To assign flex field controls data */
    function AssignFlexData(cntrltype, cntrlid, flexData) {

        if (cntrltype == "T")
            document.getElementById("txtFlex" + cntrlid).value = flexData;
        else if (cntrltype == "L" && flexData != null && flexData != '') {
            
            $("#s2id_ddlFlex" + cntrlid).select2("val", flexData);

            if (document.getElementById('ddlFlex' + cntrlid).value == '')
                $("#s2id_ddlFlex" + cntrlid).select2('val', $("#ddlFlex" + cntrlid + " option:first").val());
        }
        else{
            var d = flexData.split(' ')[0];
            var date = d.split('/');
            if (date.length == 3) {
                date[0] = date[0].length == 1 ? '0' + date[0] : date[0];
                date[1] = date[1].length == 1 ? '0' + date[1] : date[1];
                $("#s2id_ddldtFlex" + cntrlid).select2("val", date[0]);
                $("#s2id_ddlmnFlex" + cntrlid).select2("val", date[1]);
                $("#s2id_ddlyrFlex" + cntrlid).select2("val", date[2]);
            }
            else {
                $("#s2id_ddldtFlex" + cntrlid).select2("val", '');
                $("#s2id_ddlmnFlex" + cntrlid).select2("val", '');
                $("#s2id_ddlyrFlex" + cntrlid).select2("val", '');
            }
        }
    }

    /* To get traveller profile data */
    function GetProfileInfo(event) {

        document.getElementById('ctl00_upProgress').style.display = 'block';

        setTimeout(function () {
            var CorpInfo = null;
            if (event.value != '' && event.value != '--Select Traveller--') {

                var CorpInfo = JSON.parse(AjaxCall('BookOffline.aspx/GetCorpProfData', "{'sProfileId':'" + event.value + "'}"));
                if (CorpInfo == '' || CorpInfo == null)
                    ShowError('Error while getting profile info, please check the logs');
            }
            BindCorpProfile(CorpInfo);
            document.getElementById('ctl00_upProgress').style.display = 'none';
        }, 100);       
    }

    function EnableTravelTrip(selval) {

        if (selval == null || selval == '' || selval == '--Select Reason--' || selval == '-1')
            return;

        var reason = selval.split('~')[0];
        $.each(FlexInfo, function (key, col) {
            
            col.FlexProductId = (col.FlexProductId == null || col.FlexProductId == '' || col.FlexProductId == 'undefined') ? '1' : col.FlexProductId;

            if (col.FlexProductId == '1' || col.FlexProductId == '2') {

                var IsBind = true; var lables = '|';
                for (var r = 0; r < Flexconfigs.length; r++) {

                    lables += Flexconfigs[r].split(',')[1].toUpperCase() + '|';
                    if (IsBind && reason != Flexconfigs[r].split(',')[0] && col.flexLabel.toUpperCase() == Flexconfigs[r].split(',')[1].toUpperCase())
                        IsBind = false;
                }

                if (lables.indexOf('|' + col.flexLabel.toUpperCase() + '|') != -1)
                    document.getElementById('divFlex' + key).style.display = IsBind ? 'block' : 'none';
            }
        });
    }

    function ShowMoreFlex(Addflexid) {
        //Addflexid.style.display = Addflexid.style.display == 'none' ? 'block' : 'none';
    }

    /* To Bind Flex fields for both flight and hotel */
    function BindFlexFields() {

        FlexInfo = JSON.parse(document.getElementById('<%=txtFlexInfo.ClientID %>').value);
        Flexconfigs = document.getElementById('<%=hdnFlexConfig.ClientID %>').value == '' ? '' : document.getElementById('<%=hdnFlexConfig.ClientID %>').value.split('|');

        if (FlexInfo != null) {

            if (Flexconfigs != null && Flexconfigs != 'undefined' && Flexconfigs != '' && Flexconfigs.length > 0)
                $('#' + document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').id).on('change', function (e) { EnableTravelTrip(e.val); });

            var flexids = [];
            var fltcnt = 0, hltcnt = 0;var currentyear = new Date().getFullYear();
            sFlexcnt = FlexInfo.length;

            $.each(FlexInfo, function (key, col) {

                col.FlexProductId = (col.FlexProductId == null || col.FlexProductId == '' || col.FlexProductId == 'undefined') ? '1' : col.FlexProductId;
                if (col.FlexProductId == '1' || col.FlexProductId == '2'){

                    flexids.push('|' + col.flexId + col.flexControl + '|');

                    var dataevnt = col.flexDataType == 'T' ? '' :
                        col.flexDataType == 'N' ? 'return IsNumeric(event)' : 'return IsAlphaNumeric(event)';
                    var cntrlhtml = '';
                    if (col.flexControl == 'T')
                        cntrlhtml = AddTextbox(col.flexLabel, 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), dataevnt, '50', '', col.flexDisable);
                    if (col.flexControl == 'D')
                        cntrlhtml = AddCalenderddl(col.flexLabel, 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), (currentyear-100), (currentyear+100), col.flexDisable);
                    if (col.flexControl == 'L')
                        cntrlhtml = AddFlexddl(col.flexLabel, 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), (col.flexSqlQuery.indexOf('@value') != '-1' ? '' : col.flexSqlQuery), col.flexDisable);

                    var isDependent = false; var dcntid = '';

                    if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

                        dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
                            'ddl' + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');

                        if (document.getElementById(dcntid) == null)
                            return;

                        var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        var ocflex = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' : 'ddl';

                        isDependent = document.getElementById(dcntid).value == '' || document.getElementById(dcntid).value == '-1';             
                        
                        $('#' + dcntid).on('change', function (e) { BindFlexdynamicddl((ocflex == 'txt' ? e.target.value : e.val), curflex + 'Flex' + key, col.flexSqlQuery, curflex); });
                    }
                    
                    cntrlhtml = '<div ' + (isDependent ? 'style="display:none"' : '') + ' class="col-md-3 mb-3 ' + (col.flexDisable ? 'disabled' : '') + '" '
                        + 'id="divFlex' + key + '">' + cntrlhtml + '</div>';

                    if (col.FlexProductId == '1') {

                        if (fltcnt >= 10) {
                            if (fltcnt == 10)
                                $('#FooterFlexFields').append('<div id="FltAddFlex" class="col-12 collapse">' + cntrlhtml + '</div><div class="col-12"><a class="btn btn-primary btn-sm advPax-collapse-btn float-left ml-0 mt-3 mb-3 px-3 py-0 text-center collapsed" onclick="ShowMoreFlex(FltAddFlex)"  data-toggle="collapse" href="#FltAddFlex" role="button" aria-expanded="false" aria-controls="FltAddFlex"> More.. </a></div>');
                            else
                                $('#FltAddFlex').append(cntrlhtml);
                        }
                        else
                            $('#' + (col.flexplacehold == 'HDR' ? 'HeaderFlexFields' : 'FooterFlexFields')).append(cntrlhtml);
                        fltcnt++;
                    }
                        
                    if (col.FlexProductId == '2') {

                        if (hltcnt >= 10) {
                            if (hltcnt == 10)
                                $('#FooterFlexFieldsHotel').append('<div id="HotAddFlex" class="col-12 collapse">' + cntrlhtml + '</div><div class="col-12"><a class="btn btn-primary btn-sm advPax-collapse-btn float-left ml-0 mt-3 mb-3 px-3 py-0 text-center collapsed" onclick="ShowMoreFlex(HotAddFlex)" data-toggle="collapse" href="#HotAddFlex" role="button" aria-expanded="false" aria-controls="HotAddFlex"> More.. </a>');
                            else
                                $('#HotAddFlex').append(cntrlhtml);
                        }
                        else
                            $('#' + (col.flexplacehold == 'HDR' ? 'HeaderFlexFieldsHotel' : 'FooterFlexFieldsHotel')).append(cntrlhtml);
                        hltcnt++;
                    }                        
                }                
            });            
            $('select').select2();
        }
    }

    /* To bind flex field drop down list dynamically based on dependent flex field selected value */
    function BindFlexdynamicddl(selval, ddlid, ddlquery, cntrl) {

        try {

            ddlquery = (ddlquery == null || ddlquery == 'undefined' || ddlquery == '') ? '' : ddlquery.replace(/@value/g, selval);
            if (cntrl == 'ddl' && ddlquery != '')
                document.getElementById(ddlid).innerHTML = GetFlexddlData(ddlquery, cntrl);
            else
                document.getElementById(ddlid).value = ddlquery != '' ? GetFlexddlData(ddlquery, cntrl) : selval;
            document.getElementById(ddlid.replace(cntrl, 'div')).style.display = (selval != '' && selval != '-1') ? 'block' : 'none';
            Setddlval(ddlid, '-1', '');
        }
        catch (exception) {
            var exp = exception;
        }
    }

    /* To get data for flex field drop down list based on flex query */
    function GetFlexddlData(ddlquery, cntrl) {

        try {

            ddlquery = ddlquery.replace(/'/g, '^');        
            var ddldata = (ddlquery == '' || ddlquery == 'undefined' || ddlquery == null) ? '' :
                JSON.parse(AjaxCall('BookOffline.aspx/GetFlexddlData', "{'sddlQuery':'" + ddlquery + "'}"));
            if (ddldata != '') {

                var col = [];

                for (var key in ddldata[0]) {
                    col.push(key);
                }

                var ddlhtml = '';

                if (cntrl == 'ddl') {
                        
                    for (var d = 0; d < ddldata.length; d++) {

                        ddlhtml += '<option value="' + ddldata[d][col[0]] + '">' + ddldata[d][col[1]] + '</option>';
                    }
                }
                return cntrl == 'txt' ? ddldata[0][col[0]] : '<option selected="selected" value="">--Select--</option>' + ddlhtml;
            }
            else
                return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
        }
        catch (exception) {
            return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
        }
    }

    /* To Bind Flex field drop down list based on flex query */
    function AddFlexddl(lable, id, val, Isreq, ddlquery, IsDisable) {
                
        return AddMandatory(Isreq) + '<label>' + lable + ': </label> <select id="ddl' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
            (ddlquery == '' ? '<option selected="selected" value="">--Select--</option>' : GetFlexddlData(ddlquery, 'ddl')) + '</select>';
    }

    /* To call ajax web methods */
    function AjaxCall(Ajaxurl, Inputdata) {

        var obj = '';
        
        $.ajax({
            type: "POST",
            url: Ajaxurl,
            contentType: "application/json; charset=utf-8",
            data: Inputdata,// "{'sOrigin':'" + origin + "','sDestination':'" + destination + "','sPaxnames':'" + PaxNames.toString() + "'}",
            dataType: "json",
            async: false,
            success: function (data) {                    
                obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
            },
            error: function (error) {
                console.log(JSON.stringify(error));
            }
        });

        return obj;
    }

    /* Declare Global variables */
    var sMultiCityCnt = 0; sPaxCnt = 0; sAdultCnt = 0; sChildCnt = 0; sInfCnt = 0; sFlexcnt = 0;
    var Journeyval = ''; Prdct = ''; sUserId = ''; FlexConfigs = ''; var focusoncntrl = true;
    var FlexInfo, BookingInfo, FItinerary, HItinerary;

    /* To enable related view for flight and hotel based on radio button selection */
    function Prdctchange(id) {

        var val = $("[id*=" + id + "] input:checked").val();
        document.getElementById('divFlight').style.display = val == 'A' ? 'block' : 'none';
        document.getElementById('divHotel').style.display = val == 'H' ? 'block' : 'none';
        document.getElementById('HeaderFlexFields').style.display = val == 'A' ? 'block' : 'none';
        document.getElementById('FooterFlexFields').style.display = val == 'A' ? 'block' : 'none';
        document.getElementById('HeaderFlexFieldsHotel').style.display = val == 'H' ? 'block' : 'none';
        document.getElementById('FooterFlexFieldsHotel').style.display = val == 'H' ? 'block' : 'none';
        EnableHotelInfo();
    }

    /* To enable related view for flight based on journey type radio button selection and add dynamic html in case of multi city option */
    function Journeychange(id) {

        Journeyval = $("[id*=" + id + "] input:checked").val();
        document.getElementById('OnwRet').style.display = Journeyval != 'M' ? 'block' : 'none';
        document.getElementById('Return').style.display = Journeyval == 'R' ? 'block' : 'none';
        if (Journeyval == 'M' && $('#MultiRows').children().length == 0) { AddMultiCityRow(); AddMultiCityRow(); }
        document.getElementById('MultiCity').style.display = Journeyval == 'M' ? 'block' : 'none';
    }

    /* To Add new row html elements for multi city */
    function AddMultiCityRow() {

        sMultiCityCnt++;

        $('#MultiRows').append('<div id="MRow' + sMultiCityCnt + '" class="row"></div>');
        $('#MRow' + sMultiCityCnt).append('<div class="col-md-2 mb-2"> <div><span class="red">*</span>Departure </div> ' +
            '<input id="txtOrigin' + sMultiCityCnt + '" type="text" Placeholder="Please enter origin" class="form-control" onkeypress="autoCompAirportInit(origcontainer' + sMultiCityCnt + ', this)" />' + 
            '<div style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 9999;"  id="origcontainer' + sMultiCityCnt + '"></div>' +
            '</div>');

        $('#MRow' + sMultiCityCnt).append('<div class="col-md-2 mb-2"> <div><span class="red">*</span>Arrival </div> ' +
            '<input id="txtDestination' + sMultiCityCnt + '" type="text" Placeholder="Please enter destination" class="form-control" onkeypress="autoCompAirportInit(destcontainer' + sMultiCityCnt + ', this)" />' + 
            '<div style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 9999;"  id="destcontainer' + sMultiCityCnt + '"></div>' +
            '</div>');

        var closebtn = sMultiCityCnt > 2 ? '<a id="del' + sMultiCityCnt + '" style="right: -10px;top:20px;cursor: pointer;" class="remove-button float-left" onclick="DelMultiCityRow(this.id)"> <span class="icon-close"></span>  </a>' : '';

        var focevent = '', onclickevent = ''; 
        if (sMultiCityCnt == 1) {
            focevent = 'onfocus="DisplayCalender(calcontnr' + sMultiCityCnt + ', this, true, null)"';
            onclickevent = 'onclick="DisplayCalender(calcontnr' + sMultiCityCnt + ', DepDate' + sMultiCityCnt + ', true, null)"';
        }
        else {
            focevent = 'onfocus="DisplayCalender(calcontnr' + sMultiCityCnt + ', this, true, true)"';
            onclickevent = 'onclick="DisplayCalender(calcontnr' + sMultiCityCnt + ', DepDate' + sMultiCityCnt + ', true, true)"';
        }

        $('#MRow' + sMultiCityCnt).append('<div class="col-md-2 mb-2"> <div><span class="red">*</span>Departure date</div> <div> ' +
            '<table class="float-left"><tbody><tr><td><input data-calendar-contentid="#calcontnr' + sMultiCityCnt + '" id="DepDate' + sMultiCityCnt + '" type="text" ' + focevent + ' class="form-control"  Placeholder="Please enter departure date" /></td><td>' + AddPrefTimeddl('PrefTime' + sMultiCityCnt) + '</td></tr></tbody></table>' +
            '<div id="calcontnr' + sMultiCityCnt + '" style="display: none;"> </div></div >');

        $('#MRow' + sMultiCityCnt).append('<div class="col-md-2 mb-2"> <div>Airline </div> ' +
            '<input id="txtAirline' + sMultiCityCnt + '" type="text" Placeholder="Please enter airline" class="form-control" onkeypress="autoCompAirlinesInit(alcontainer' + sMultiCityCnt + ', this)" />' + 
            '<div style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 9999;"  id="alcontainer' + sMultiCityCnt + '"></div>' +
            '</div>');

        $('#MRow' + sMultiCityCnt).append('<div class="col-md-1 mb-2"> <div>Flight No </div> ' +
            '<input id="txtFlightNo' + sMultiCityCnt + '" type="text" Placeholder="flight no" class="form-control" /></div>');

        $('#MRow' + sMultiCityCnt).append('<div class="col-md-2 mb-2"> <div>Cabin class </div> ' +
            '<select id="ddlcabinclass' + sMultiCityCnt + '" class="form-control"> </select> ' + closebtn + ' </div>');

        document.getElementById('ddlcabinclass' + sMultiCityCnt).innerHTML = document.getElementById('<%=ddlOnwardcabin.ClientID %>').innerHTML;

        document.getElementById('add_more_city_button').style.display = sMultiCityCnt < 6 ? 'block' : 'none';
        $('select').select2();
    }

    /* To remove multi city row on row level delete button click */
    function DelMultiCityRow(id) {
        var rowid = id.substring(4, 3);
        if ((sMultiCityCnt - rowid) == 0) {

             $('#MRow' + rowid).remove();
        }        
        else {

            $('#MRow' + rowid).remove();
            for (var i = 0; i < sMultiCityCnt - rowid; i++) {
                var newid = rowid + i;
                var preid = rowid + 1;
                $('#MRow' + preid).attr('id', 'MRow' + newid);   
                $('#Origin' + preid).attr('id', 'Origin' + newid);   
                $('#Destination' + preid).attr('id', 'Destination' + newid);   
                $('#DelMultiCityRow' + preid).attr('id', 'DelMultiCityRow' + newid);   
                $('#DepDate' + preid).attr('id', 'DepDate' + newid);   
                $('#origcontainer' + preid).attr('id', 'origcontainer' + newid);   
                $('#destcontainer' + preid).attr('id', 'destcontainer' + newid);   
                $('#calcontnr' + preid).attr('id', 'calcontnr' + newid);   
                $('#txtAirline' + preid).attr('id', 'txtAirline' + newid);   
                $('#alcontainer' + preid).attr('id', 'alcontainer' + newid);   
                $('#txtFlightNo' + preid).attr('id', 'txtFlightNo' + newid);   
                $('#ddlcabinclass' + preid).attr('id', 'ddlcabinclass' + newid); 
                $('#ddlPrefTime' + preid).attr('id', 'ddlPrefTime' + newid);
            }
        }
        sMultiCityCnt--;
        document.getElementById('add_more_city_button').style.display = sMultiCityCnt < 6 ? 'block' : 'none';
    }
        
    /* To add hotel rooms drop down */
    function AddPrefTimeddl(id) {        
        return '<select id="ddl' + id + '" class="form-control small ml-2">' +
            '<option selected="selected" value="">Any time</option> <option value="Morning">Morning</option>' +
            '<option value="Afternoon">Afternoon</option> <option value="Evening">Evening</option> <option value="Night">Night</option> </select>' ;
    }

    /* To add new passenger html fields */
    function AddNewPax(PaxType) {

        sPaxCnt++;

        sAdultCnt = PaxType.value == 'A' ? sAdultCnt + 1 : sAdultCnt;
        sChildCnt = PaxType.value == 'C' ? sChildCnt + 1 : sChildCnt;
        sInfCnt = PaxType.value == 'I' ? sInfCnt + 1 : sInfCnt;
        $('#spTotpax').text(sPaxCnt); $('#spadult').text(sAdultCnt); $('#spchild').text(sChildCnt); $('#spinf').text(sInfCnt);

        var Titlehtml = PaxType.value == 'A' ? AddAPaxTitleddl('Title', 'Title' + sPaxCnt, '', true) : AddCPaxTitleddl('Title', 'Title' + sPaxCnt, '', true);
        PaxType = PaxType.value == 'A' ? 'Adult' : PaxType.value == 'C' ? 'Child' : 'Infant';    
        var delhtml = sPaxCnt < 2 ? '' : '<a id="delpax' + sPaxCnt + '" class="float-right remove-button mr-2" style="right: 0;top: 5px;cursor:pointer" onclick="DelPax(this.id)"> <span class="icon-close"></span>   </a>'

        var currentyear = new Date().getFullYear();

        $('#divPassengers').append('<div id="divpax' + sPaxCnt + '"></div>');
        $('#divpax' + sPaxCnt).append('<div class="ns-h3 position-relative"> <span id="divpaxhdr' + sPaxCnt + '"><strong>Passenger-' + sPaxCnt + ' (' + PaxType + ')</span></strong>  ' + delhtml +
            '</div><div class="bg_white p-3"> <div class="row">' +
            '<div class="col-md-3 mb-3">' + Titlehtml + '</div>' +
            '<div class="col-md-3 mb-3">' + AddTextbox('First Name', 'FirstName' + sPaxCnt, '', true, 'return IsAlpha(event)', '50', '', false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddTextbox('Last Name', 'LastName' + sPaxCnt, '', true, 'return IsAlpha(event)', '50', '', false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddCalenderddl('DOB', 'DOB' + sPaxCnt, '', false, (currentyear-100), currentyear, false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddTextbox('Passport No', 'PassportNo' + sPaxCnt, '', false, 'return IsAlphaNumeric(event)', '50', '', false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddCalenderddl('Passport Expiry', 'PassportExpiry' + sPaxCnt, '', false, (currentyear-50), (currentyear+50), false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddTextbox('Place Of Issue', 'PlaceOfIssue' + sPaxCnt, '', false, 'return IsAlpha(event)', '50', '', false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddTextbox('Email', 'Email' + sPaxCnt, '', (sPaxCnt == 1), '', '100', 'ValidateEmail(this);', false) + '</div>' +
            '<div class="col-md-3 mb-3">' + AddTextbox('Address', 'Address' + sPaxCnt, '', false, '', '100', '', false) + '</div>' +
            '<div id="divPaxRoom' + sPaxCnt + '" class="col-md-3 mb-3">' + AddPaxRoomddl('Room No', 'RoomNo' + sPaxCnt, '', true) + '</div>' +
            '</div> </div> </div>');
        document.getElementById('divAddPax').style.display = sPaxCnt < 8 ? 'block' : 'none';      
        EnableHotelInfo();      
        $('select').select2();
    }

    function EnableHotelInfo() {

        var val = $("[id*=_rbtnPrdct] input:checked").val();
        for (var r = 1; r <= sPaxCnt; r++) {
            document.getElementById('divPaxRoom' + r).style.display = val == 'H' ? 'none' : 'none';
        }
        if (val == 'H') {

            var cityname = '';                
            Journeyval = $("[id*=_rbtnJrnyType] input:checked").val();
            if (Journeyval == 'O' || Journeyval == 'R') {

                document.getElementById('txthotelChkInDate').value = document.getElementById('<%=DepDate.ClientID %>').value != '' ?
                    document.getElementById('<%=DepDate.ClientID %>').value : document.getElementById('txthotelChkInDate').value;
                document.getElementById('txthotelChkOutDate').value = Journeyval == 'R' && document.getElementById('<%=ReturnDateTxt.ClientID %>').value != '' ?
                    document.getElementById('<%=ReturnDateTxt.ClientID %>').value : document.getElementById('txthotelChkOutDate').value;

                if (document.getElementById('<%=txtDestination.ClientID %>').value != '')
                    cityname = document.getElementById('<%=txtDestination.ClientID %>').value.split(',')[0].split(')')[1] + ',' +
                        document.getElementById('<%=txtDestination.ClientID %>').value.split(',')[1].split('-')[0];
                document.getElementById('txtHotelCity').value = cityname != '' ? cityname : document.getElementById('txtHotelCity').value;
                if (cityname.split(',').length > 1)
                    Setddlval(document.getElementById('<%=ddlHotelCountry.ClientID %>').id, cityname.split(',')[1], 'text');
            }
            else {

                document.getElementById('txthotelChkInDate').value = document.getElementById('DepDate' + sMultiCityCnt).value != '' ?
                    document.getElementById('DepDate' + sMultiCityCnt).value : '';                

                if (document.getElementById('txtDestination' + sMultiCityCnt).value != '')
                    cityname = document.getElementById('txtDestination' + sMultiCityCnt).value.split(',')[0].split(')')[1] + ',' +
                        document.getElementById('txtDestination' + sMultiCityCnt).value.split(',')[1].split('-')[0];
                document.getElementById('txtHotelCity').value = cityname != '' ? cityname : document.getElementById('txtHotelCity').value;
                if (cityname.split(',').length > 1)
                    Setddlval(document.getElementById('<%=ddlHotelCountry.ClientID %>').id, cityname.split(',')[1], 'text');
            }
        }
    }

    function Setddlval(id, val, type) {

        if (type == 'text') 
            $("#s2id_" + id).select2('val', $("#" + id + " option").filter(function () { return this.text == val.trim(); }).val());
        else
            $("#s2id_" + id).select2('val', val.trim());

        if (document.getElementById(id).value == '')
            $("#s2id_" + id).select2('val', $("#" + id + " option:first").val());
    }

    /* To add mandatory aestrick span */
    function AddMandatory(isreq) {
        return isreq ? '<span class="red">*</span>' : '';
    }

    /* To add hotel rooms drop down */
    function AddPaxRoomddl(lable, id, val, Isreq) {        
        return AddMandatory(Isreq) + '<label>' + lable + ': </label> <select id="ddl' + id + '" class="form-control">' +
            '<option selected="selected" value="1">1</option> <option value="2">2</option>' +
            '<option value="3">3</option> <option value="4">4</option> </select>' ;
    }

    /* To add dynamic text box field */
    function AddTextbox(lable, id, val, Isreq, onkeypress, maxlen, onchange, IsDisable) {        
        return AddMandatory(Isreq) + '<label>' + lable + ': </label> <input id="txt' + id + '" type="text" onkeypress="' + onkeypress +
            '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control" maxlength="' + maxlen + '" onchange="' + onchange + '" value="' + val + '">';
    }

    /* To add passenger fields adult title drop down */
    function AddAPaxTitleddl(lable, id, val, Isreq) {        
        return AddMandatory(Isreq) + '<label>' + lable + ': </label> <select id="ddl' + id + '" class="form-control">' +
            '<option selected="selected" value="">--Select Title--</option> <option value="Mr">Mr.</option>' +
            '<option value="Mrs">Mrs.</option> <option value="Ms">Ms.</option> <option value="Dr">Dr.</option> </select>' ;
    }

    /* To add passenger fields child title drop down */
    function AddCPaxTitleddl(lable, id, val, Isreq) {        
        return AddMandatory(Isreq) + '<label>' + lable + ': </label> <select id="ddl' + id + '" class="form-control">' +
            '<option selected="selected" value="">Select Title</option> <option value="Mr">Mr.</option>' +
            '<option value="MSTR">Master</option> <option value="CHD">Child</option> <option value="INF">Infant</option> </select>' ;
    }

    /* To add calender drop downs */
    function AddCalenderddl(lable, id, val, Isreq, startyear, endyear, IsDisable) {        
        return AddMandatory(Isreq) + '<label>' + lable + ': </label> <div style="margin-left: -15px;">' + AddDateddl(id, val, IsDisable) + AddMonthddl(id, val, IsDisable) + AddYearddl(id, val, startyear, endyear, IsDisable) + '</div>';
    }

    /* To add passenger fields calender date drop down */
    function AddDateddl(id, val, IsDisable) {     
        var dates = '';
        for (var d = 1; d <= 31; d++) {
            var dtval = d < 10 ? '0' + d : d;
            dates += '<option value="' + dtval + '">' + dtval + '</option>';
        }
        return '<div class="col-md-4"> <select id="ddldt' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
            '<option selected="selected" value="">Day</option>' + dates + '</select> </div>';
    }

    /* To add passenger fields calender month drop down */
    function AddMonthddl(id, val, IsDisable) {        
        return '<div class="col-md-4"> <select id="ddlmn' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
            '<option selected="selected" value="">Month</option> <option value="01">Jan</option>' +
            '<option value="02">Feb</option> <option value="03">Mar</option> <option value="04">Apr</option>' +
            '<option value="05">May</option> <option value="06">Jun</option> <option value="07">Jul</option>' +
            '<option value="08">Aug</option> <option value="09">Sep</option> <option value="10">Oct</option>' +
            '<option value="11">Nov</option> <option value="12">Dec</option> </select> </div>' ;
    }

    /* To add passenger fields calender year drop down */
    function AddYearddl(id, val, start, end, IsDisable) {     
                
        var years = '';
        for (var y = start; y <= end; y++) {
            years += '<option value="' + y + '">' + y + '</option>';
        }
        return '<div class="col-md-4"> <select id="ddlyr' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
            '<option selected="selected" value="">Year</option>' + years + '</select> </div>';
    }

    /* To remove passenger on respective delete button click */
    function DelPax(id) {

        var rowid = id.substring(7, 6);

        var PaxType = $('#divpaxhdr' + rowid).text();   
        sAdultCnt = PaxType.indexOf('Adult') > -1 ? sAdultCnt - 1 : sAdultCnt;
        sChildCnt = PaxType.indexOf('Child') > -1 ? sChildCnt - 1 : sChildCnt;
        sInfCnt = PaxType.indexOf('Infant') > -1 ? sInfCnt - 1 : sInfCnt;

        $('#divpax' + rowid).remove();
        if ((sPaxCnt - rowid) > 0) {

            for (var i = 0; i < sPaxCnt - rowid; i++) {

                var newid = Math.ceil(rowid) + i;
                var preid = Math.ceil(newid) + 1;

                var paxhdr = $('#divpaxhdr' + preid).text();    
                paxhdr = paxhdr.replace('Passenger-' + preid, 'Passenger-' + newid);
                $('#divpaxhdr' + preid).text(paxhdr);

                $('#divpax' + preid).attr('id', 'divpax' + newid);   
                $('#divpaxhdr' + preid).attr('id', 'divpaxhdr' + newid);   
                $('#divPaxRoom' + preid).attr('id', 'divPaxRoom' + newid);   
                $('#ddlRoomNo' + preid).attr('id', 'ddlRoomNo' + newid);   
                $('#ddlTitle' + preid).attr('id', 'ddlTitle' + newid);   
                $('#delpax' + preid).attr('id', 'delpax' + newid);   
                $('#txtFirstName' + preid).attr('id', 'txtFirstName' + newid);   
                $('#txtLastName' + preid).attr('id', 'txtLastName' + newid);   
                $('#ddldtDOB' + preid).attr('id', 'ddldtDOB' + newid);   
                $('#ddldtDOB' + preid).attr('id', 'ddlmnDOB' + newid);   
                $('#ddldtDOB' + preid).attr('id', 'ddlyrDOB' + newid);   
                $('#txtPassportNo' + preid).attr('id', 'txtPassportNo' + newid);   
                $('#txtPassportExpiry' + preid).attr('id', 'txtPassportExpiry' + newid);   
                $('#txtPlaceOfIssue' + preid).attr('id', 'txtPlaceOfIssue' + newid);   
                $('#txtEmail' + preid).attr('id', 'txtEmail' + newid);   
            }
        }
        sPaxCnt--;
       
        $('#spTotpax').text(sPaxCnt); $('#spadult').text(sAdultCnt); $('#spchild').text(sChildCnt); $('#spinf').text(sInfCnt);
        document.getElementById('divAddPax').style.display = sPaxCnt < 8 ? 'block' : 'none';
    }

</script>

<h3> Offline Booking</h3>
<div class="search_container p-3">
    <div class="col-md-12">
        <asp:RadioButtonList id="rbtnPrdct" runat="server" style="width:250px" CssClass="mt-3 mb-3 custom-radio-table" onchange="Prdctchange(this.id)" RepeatDirection="Horizontal">
            <asp:ListItem Text="Air" Selected="True" Value="A"></asp:ListItem>
            <asp:ListItem Text="Hotel" Value="H"></asp:ListItem>
        </asp:RadioButtonList>        
    </div>
	<div>
		<div class="col-md-2 mb-3">
			<label> Booking</label>   
			<select id="ddlReqType" onchange="document.getElementById('divReference').style.display = this.value == 'N' ? 'none' : 'block'" class="form-control">
				<option selected="selected" value="N">New </option>
				<option value="C">Change</option>
			</select>
		</div>
		<div id="divReference" style="display:none" class="col-md-2 mb-3">
			<label> <span class="red">*</span>Ref. Number</label>  
			<input ID="txtReference" class="form-control" type="text" placeholder="Please enter Reference number"/>                  
		</div>
		<div id="divTTP" style="display:none" class="col-md-2 mb-3">
			<label> <span class="red">*</span>Travel Type</label>  
			<asp:DropDownList ID="ddlFlightTravelReasons" CssClass="form-control select-element" runat="server"> </asp:DropDownList>                 
		</div>
		<div id="divProfiles" style="display:none" class="col-md-2 mb-3">
			<label> <span class="red">*</span>Traveler</label>  
			<asp:DropDownList ID="ddlFlightEmployee" onchange="GetProfileInfo(this)" CssClass="form-control select-element" runat="server"> </asp:DropDownList>                 
		</div>

		<div id="HeaderFlexFields"></div><div id="HeaderFlexFieldsHotel" style="display:none"></div>
	</div>
	<div id="divFlight" class="col-md-12">            
	
		<asp:RadioButtonList id="rbtnJrnyType" runat="server" style="width:350px" CssClass="mt-3 custom-radio-table" onchange="Journeychange(this.id)" RepeatDirection="Horizontal">
			<asp:ListItem Text="One Way" Selected="True" Value="O"></asp:ListItem>
			<asp:ListItem Text="Round Trip" Value="R"></asp:ListItem>
			<asp:ListItem Text="Multi City" Value="M"></asp:ListItem>
		</asp:RadioButtonList>
		       
		<div id="OnwRet"><div class="row">     
			<div class="col-md-6 mb-3">  
				<div>  <span class="red">*</span>From</div>                             
				<asp:TextBox ID="txtOrigin" Placeholder="Please enter origin"
					CssClass="form-control" runat="server" onkeypress="autoCompAirportInit(statescontainer,this)"></asp:TextBox>
				<div style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 9999;" id="statescontainer"></div>
			</div> 
			<div class="col-md-6 mb-3">  
				<div>  <span class="red">*</span>To</div>                      
				<asp:TextBox ID="txtDestination" Placeholder="Please enter destination" CssClass="form-control" runat="server" onkeypress="autoCompAirportInit(statescontainer1,this)"></asp:TextBox>
				<div style="width: 250px;line-height:30px; color:#000; z-index: 9999;" id="statescontainer1"> </div>
			</div> 

			<div class="col-12 marbot_10">
				<div class="row">
					<div class="col-md-3">
						<div> <span class="red">*</span>Departure Date </div>
						<div id="depdtcntnr" style="display: none;"> </div>
						<div>
							<table>
								<tbody>
									<tr>
										<td>
											<asp:TextBox ID="DepDate" style="width:180px;" Placeholder="Please enter departure date" data-calendar-contentID="#depdtcntnr" 
								                CssClass="form-control" runat="server"
								                onfocus="ShowYahooCalender(depdtcntnr, this, true, null, '', $('[id$=_ReturnDateTxt]').attr('id'), 'Departure date should be less than return date')"></asp:TextBox>          
										</td>
										<%--<td>
											<a href="javascript:void(null)" onclick="ShowYahooCalender(depdtcntnr, document.getElementById('<%=DepDate.ClientID %>'), true, null, '', $('[id$=_ReturnDateTxt]').attr('id'), 'Departure date should be less than return date')">
												<img id="Img4" src="images/call-cozmo.png" alt="Pick Date">
											</a>
										</td>--%>
										<td>
                                            <select ID="ddlOnwPrefTime" class="form-control small ml-2">
                                                <option selected="selected" value="">Any time</option>
                                                <option Value="Morning">Morning</option>
                                                <option Value="Afternoon">Afternoon</option>
                                                <option Value="Evening">Evening</option>
                                                <option Value="Night">Night</option>
                                            </select> 
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="col-md-3">
						<div>  Airline Preference </div>                
						<asp:TextBox ID="txtPreferredAirline" CssClass="form-control" runat="server" Placeholder="Please enter preferred airline" 
							onkeypress="autoCompAirlinesInit(airlinecontainer, this)"></asp:TextBox>
						<div id="airlinecontainer"  style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;" ></div>
						<asp:HiddenField ID="airlineCode" runat="server" />
						<asp:HiddenField ID="airlineName" runat="server" />  
					</div>
					<div class="col-md-2">
						<div>  Flight No </div>
						<div> <asp:TextBox CssClass="form-control" Placeholder="Please enter flight no" ID="txtFlightNo" runat="server"></asp:TextBox></div>
					</div>
					<div class="col-md-3">
						<div>  Cabin Class </div>
							<asp:DropDownList ID="ddlOnwardcabin" Width="150px" runat="server">								
								<asp:ListItem Text="Economy" Value="2"></asp:ListItem>
								<asp:ListItem Text="Business" Value="4"></asp:ListItem>
							</asp:DropDownList>
					</div>
				</div>
			</div>              

		
			<div class="col-12 marbot_10"> 
				<div id="Return" style="display:none" class="row">
					<div class="col-md-3"> 
						<div>  <span class="red">*</span>Return</div>
						<div>						
						    <table>
								<tbody>
									<tr>
										<td>
											<asp:TextBox ID="ReturnDateTxt" Placeholder="Please enter return date" CssClass="form-control" onfocus="ShowYahooCalender(retcntnr, this, true, $('[id$=_DepDate]').attr('id'), 'Please select departure date', null, '')" data-calendar-contentID="#retcntnr" runat="server" ></asp:TextBox>   
											<div id="retcntnr" style="position: absolute; top: 179px; left: 8px; display: none;"></div>      
										</td>
										<%--<td>
											<a href="javascript:void(null)" onclick="ShowYahooCalender(retcntnr, document.getElementById('<%=ReturnDateTxt.ClientID %>'), true, $('[id$=_DepDate]').attr('id'), 'Please select departure date', null, '')">
												<img id="Img4" src="images/call-cozmo.png" alt="Pick Date">
											</a>
										</td>--%>
										<td>
                                            <select ID="ddlRetPrefTime" class="form-control small ml-2">
                                                <option selected="selected" value="">Any time</option>
                                                <option Value="Morning">Morning</option>
                                                <option Value="Afternoon">Afternoon</option>
                                                <option Value="Evening">Evening</option>
                                                <option Value="Night">Night</option>
                                            </select> 
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-3"> 
						<div>  Airline Preference </div>
						<asp:TextBox ID="txtPreferredAirlineRet" CssClass="form-control" runat="server"  Placeholder="Please enter preferred airline"                        
							onkeypress="autoCompAirlinesInit(airlinecontainer1, this)"></asp:TextBox>
						<div  id="airlinecontainer1"  style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;"></div>
						<asp:HiddenField ID="airlineCodeRet" runat="server" />
						<asp:HiddenField ID="airlineNameRet" runat="server" />                                
					</div>
					<div class="col-md-2">                     
						<div>  Flight No </div>                        
						<asp:TextBox CssClass="form-control" ID="txtFlightNoRet" runat="server" Placeholder="Please enter flight no"></asp:TextBox>
					</div>
					<div class="col-md-3">
						<div>  Cabin Class </div>
							<asp:DropDownList ID="ddlReturncabin" Width="150px" runat="server">
								<asp:ListItem Text="Economy" Value="2"></asp:ListItem>
								<asp:ListItem Text="Business" Value="4"></asp:ListItem>
							</asp:DropDownList>
					</div>
				</div>
			</div>
		</div></div>
		<div id="MultiCity" style="display:none" > 
			<div id="MultiRows"></div>
			<div class="row"> 
				<div class="col-md-4">  
					<a id="add_more_city_button" style="color: #fff;" class="mb-3" onclick="AddMultiCityRow()"> <span class="icon-add"></span> Add More Cities  </a>
				</div>
			</div>
			<%--<div class="row"> 
				<div class="col-md-4">
					<div>  Airline Preference </div>                
					<asp:TextBox ID="txtMPreferredAirline" CssClass="form-control" runat="server" Placeholder="Please enter preferred airline" 
							onkeypress="autoCompAirlinesInit(airlinecontainer2, this)"></asp:TextBox>
					<div  id="airlinecontainer2"  style="width:300px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;" ></div>
					<asp:HiddenField ID="HiddenField1" runat="server" />
					<asp:HiddenField ID="HiddenField2" runat="server" />  
				</div>
				<div class="col-md-4">
					<div>  Cabin Class </div>
					<asp:DropDownList ID="ddlcabinclass" runat="server" Width="150px">
						<asp:ListItem Text="All" Value="1"></asp:ListItem>
						<asp:ListItem Text="Economy" Value="2"></asp:ListItem>
						<asp:ListItem Text="Premium Economy" Value="3"></asp:ListItem>
						<asp:ListItem Text="Business" Value="4"></asp:ListItem>
						<asp:ListItem Text="First" Value="6"></asp:ListItem>
					</asp:DropDownList>
				</div>
			</div>--%>
		</div>
	</div>
	<div id="divHotel" style="display:none" class="p-4">       
	   <div class="row"> 
			<div class="col-md-4 mb-3">
				<label><span class="red">*</span>City</label>   
				<input id="txtHotelCity" type="text" onkeypress="autoCompHotelCity(hotctycont,this,document.getElementById('<%=ddlHotelCountry.ClientID %>').id)" class="form-control" placeholder="Please enter City" > 
				<div id="hotctycont" style="display: none;"> </div> 
			</div>
			<div class="col-md-4 mb-3">
				<label><span class="red">*</span>Country</label>   
				<asp:DropDownList ID="ddlHotelCountry" CssClass="form-control select-element" runat="server"></asp:DropDownList>   
			</div>
			<div class="col-md-4 mb-3">
				<label><span class="red">*</span>Check In Date</label>    
				<div> 
					<input type="text" id="txthotelChkInDate" class="form-control" data-calendar-contentID="#hotchkincal" 
						onfocus="ShowYahooCalender(hotchkincal, this, true, null, '', $('[id$=txthotelChkOutDate]').attr('id'), 'Check in date should be less than check out date')" />
				</div>
				<div id="hotchkincal" style="display: none;"> </div> 
			</div>
			<div class="col-md-4 mb-3">
				<label><span class="red">*</span>Check Out Date</label>   
				<div>
					<input type="text" id="txthotelChkOutDate" data-calendar-contentID="#hotchkoutcal" class="form-control" 
						onfocus="ShowYahooCalender(hotchkoutcal, this, true, $('[id$=txthotelChkInDate]').attr('id'), 'Please select check in date', null, '')" />
				</div>          
				<div id="hotchkoutcal" style="display: none;"> </div> 
			</div>
			<%--<div class="col-md-4 mb-3">
				<label>Number Of Guests</label>   
				<select id="ddlPaxCount" class="form-control">
					<option selected="selected" value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</div> --%>       
			<div class="col-md-4 mb-3">
				<label>Apartment Name (If Known)</label>   
				<input type="text" id="txtHoltelApName" class="form-control" placeholder="Apartment Name (If Known)" > 
			</div>
			<div class="col-md-4 mb-3">
				<label>Special Requests</label>   
				<input type="text" id="txtSSR" class="form-control" placeholder="Special Requests" > 
			</div>
			<div class="col-md-4 mb-3">
				<label> Additional Information</label>      
				<input type="text" id="txtAddInfo" class="form-control" placeholder="Additional Information" >   
			</div>   
	   </div>
	</div>

	<div class="col-md-2 custom-checkbox-style">
		<input type="checkbox" name="checkbox" onchange="if(this.checked) EnableHotelInfo();" id="chkCopyToHotel" value="value">
		<label for="chkCopyToHotel" style="line-height: normal;">Require Hotel ?</label>
	</div>
	
	<div class="clearfix"></div>
</div>
<%--<div class="row mt-4"> 
    <div class="col-md-4 mb-3">
        <label> Booker's Name:</label>   
        <input type="text" Placeholder="Please enter booker name" class="form-control"> 
    </div>   
    <div class="col-md-2 mb-3">
        <label>Contact No:</label>   
        <input type="text" Placeholder="Please enter contact no" class="form-control"> 
    </div>      
    <div class="col-md-2 mb-3">
        <label>Email:</label>   
        <input type="text" Placeholder="Please enter email" class="form-control" > 
    </div>      
</div>--%>
         
<div class="bggray margin-top-10 row"> <strong> Passenger Details</strong> </div>
<div id="divPassengers"></div>
        
<div id="divAddPax" class="bg_white margin-top-10">                  
    <span ID="lblPax" style="width:100px;">Add More Pax:</span>
    <select ID="ddlPax" style="width:100px;">
        <option Value="A">Adult</option>
        <option Value="C">Child</option>
        <option Value="I">Infant</option>
    </select>
    <a ID="lnkAdd" onclick="AddNewPax(document.getElementById('ddlPax')); return false;"><img ID="imgAdd" src="images/Addplus.jpg" style="width:18px" /></a>
    <span class="ml-4">Currently Added (<span id="spTotpax">0</span>) | <span id="spadult">0</span>-Adult(s) | <span id="spchild">0</span>-Child(s) | <span id="spinf">0</span>-Infant(s)</span>
</div>        


<div class="ns-h3 margin-top-10 pl-3"> <strong> UDID</strong> </div>
<div class="bg_white p-3"> 
    <div class="row" id="FooterFlexFields"> </div> 
    <div class="row" id="FooterFlexFieldsHotel" style="display:none"> </div> 
</div> 

        
<div class="ns-h3 margin-top-10">Please note below points</div>
<div class="bg_white pad_10 small">         
    <span class="mb-2 d-block"><span style='color:red;'>*</span>&nbsp;Kindly Check the spelling & reconfirm the passenger names(s) before you book.<br></span>
    <span class="mb-2 d-block"><span style='color:red;'>*</span>&nbsp;Ticket Name Changes are not permitted once issued<br></span>
    <span class="mb-2 d-block">The above mentioned purchases are subject to cancellation,date change fee and once purchased tickets are non-transferable and 
    name changes are not permitted. For further details,read the over view of all the Restriction,Penalties & Cancellation Charges.
    </span>        
    <div class="row"> 
        <div class="col-md-8 mt-3"> 
            <div class="custom-checkbox-style dark">
                <input type="checkbox" name="checkbox" id="chkConfirm" value="value">
                <label for="chkConfirm" style="line-height: normal;"> I confirm the required authorization has been agreed by my line manager to travel. Please note:  Failure to obtain the required approval may result in disciplinary action and/or dismissal. </label>
            </div>
        </div>
        <div class="col-md-4 mt-3"> 
            <asp:Button ID="btnContinue" runat="server" Text="SUBMIT" CssClass="but but_b pull-right" OnClientClick="return SaveBooking(); return false;" /> <%--OnClick="btnContinue_Click" --%>
        </div>
    </div>                              
</div>
<div id="divAlert" class="modal fade" role="dialog"></div>
<asp:TextBox ID="txtFlexInfo" runat="server" style="display:none"></asp:TextBox>
<asp:TextBox ID="txtOffFltInfo" runat="server" style="display:none"></asp:TextBox>
<asp:TextBox ID="txtOffHotelInfo" runat="server" style="display:none"></asp:TextBox>
<asp:TextBox ID="txtCorpProfile" runat="server" style="display:none"></asp:TextBox>
<asp:TextBox ID="txtCountries" runat="server" style="display:none"></asp:TextBox>
<asp:HiddenField ID="hdnUserId" runat="server" /><asp:HiddenField ID="hdnIsCorp" runat="server" /><asp:HiddenField ID="hdnFlexConfig" runat="server" />
</asp:Content>


