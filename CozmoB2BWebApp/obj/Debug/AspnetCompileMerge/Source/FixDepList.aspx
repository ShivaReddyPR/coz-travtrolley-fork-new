﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixDepListGUI" Title="Fixed Departure" Codebehind="FixDepList.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%--<%@ Import Namespace="CT.HolidayDeals" %>--%>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--<form id="form1" runat="server">--%>


<div style=" margin-top:10px;" class="ns-h3">Fixed Departure</div>
<div style="float:left; width:930px; border-left:1px solid #000; border-right:1px solid #000; border-bottom:1px solid #000;">

<div style="float:left; width:930px; border-left:1px solid #000; border-right:1px solid #000; border-bottom:1px solid #000;">

      
        <asp:DataList ID="DataList1" runat="server"
BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 

Font-Bold="True" Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1"
RepeatDirection="Horizontal" Width="463px" OnSelectedIndexChanged="DataList1_SelectedIndexChanged">
<HeaderTemplate>
<p style="float:left; background: #418ce3; width:930px; border-left:1px solid #000; line-height:18px;  font-size:12px; color:White; border-right:1px solid #000; border-top:1px solid #000;">
    <span style="float:left; font-weight:bold; width:270px; text-align:left; ">Tour Name</span>
    <span style="float:left; font-weight:bold; width:150px; text-align:center; ">StockIn Hand</span>
    <span style="float:left; font-weight:bold; width:250px; text-align:center; ">Starting From</span>
    <span style="float:left; font-weight:bold; width:100px; text-align:center; ">Edit</span>
    
</p>

 </HeaderTemplate>
<itemtemplate>
  <table class="style1">                   
                    <tr>
                       
                        <td style="text-align: left;width:300px;height:20px">
                            <asp:Label id="lblActivity" class="fleft" runat="server"  Text='<%# Eval("activityName") %>' >    </asp:Label>
                         </td>
                            <td style="width:200px"> 
                            <asp:Label id="Label1" style="text-align:center" runat="server"  Text='<%# Eval("stockInHand") %>' >    </asp:Label>
                            
                         </td>
                            <td style="text-align: left;width:200px">
                            <asp:Label id="Label2" runat="server"  Text='<%# Eval("activityStartingFrom") %>' >    </asp:Label>
                         </td>
                         <td align="right">
                            <asp:LinkButton style="color:blue" CommandName="Select" runat="server" Text="   Edit" id="btnEdit"></asp:LinkButton>
                            <asp:HiddenField id="IThdfActivityId" runat="server" Value='<%# Eval("activityId") %>' />
                        </td>                   
                      
                   
                        <td class="style2">
                             </td>
                        <td style="text-align: left">
                             </td>
                    </tr>
                    <tr>
                        <td class="style2" colspan="2">
                           
                        </td>
                    </tr>
                </table>
            </itemtemplate>
</asp:DataList>
</div>

</div>  

<%--</form>--%>
</asp:Content>

