﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ActivityListGUI" Title="Activity List" Codebehind="ActivityList.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">


<div class="body_container"> 

<div class="ns-h3">Activities</div>
<div class="bor_gray bg_white pad_10"> 


<div> <table>
<tr>   
   <td height="23px"> <asp:Label ID="lblAgent" runat="server" Text="Agent:"> </asp:Label><sup>*</sup> </td>
   
   <td><asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" Width="200px" ></asp:DropDownList></td>   
   
   
   <td>  <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="button" OnClick="btnSearch_Click" >
                                            </asp:Button></td>
   </tr></table></div>






</div>




<div class=" text-right pad10"> 

<a href="CreateTours.aspx" class="btn-primary btn"><span class="glyphicon glyphicon-plus-sign"></span> Create New </a>



<a href="#" class="btn-success btn"><span class="glyphicon glyphicon-ok-sign"></span> Publish </a>
<a href="#" class="btn-warning btn"><span class="glyphicon glyphicon-remove-sign"></span> UnPublish </a>



<a href="#" class="btn-danger btn"><span class="glyphicon glyphicon-trash"></span> Delete </a>


</div>


   <div> 
   
   
   
   <asp:DataList  ID="DataList1" runat="server" CssClass="table table-bordered b2b-corp-table" 

BorderStyle="None" BorderWidth="0px" CellPadding="0" CellSpacing="0"   RepeatDirection="Horizontal" Width="100%" OnItemCommand="DataList1_ItemCommand">
<HeaderTemplate>

  <tr>
    <th>Activity</th>
    <th>StockIn Hand</th>
    <th>Starting From</th>
    <th>Edit</th>
      <th>Status</th>
  </tr>


 </HeaderTemplate>
<itemtemplate>




  <tr>
    <td><asp:Label id="lblActivity" class="" runat="server"  Text='<%# Eval("activityName") %>' >    </asp:Label></td>
    <td><asp:Label id="Label1" style="text-align:center" runat="server"  Text='<%# Eval("stockInHand") %>' >    </asp:Label></td>
    <td><asp:Label id="Label2" runat="server"  Text='<%# Eval("activityStartingFrom") %>' >    </asp:Label></td>
    
    <td><asp:LinkButton CommandName="Edit" runat="server" id="btnEdit"><span class="glyphicon glyphicon-pencil text-success"></span></asp:LinkButton>
                          
                          
                          
                            <asp:HiddenField id="IThdfActivityId" runat="server" Value='<%# Eval("activityId") %>' />
                            
                            
                            </td>


      <td>
      
      <%--<asp:LinkButton  style="color:blue" CommandName="StatusUpdate" runat="server" Text='<%# Eval("activityStatus").ToString() == "A" ? "InActive?" : "Active?" %>' id="lnkStatus" OnClientClick='<%# string.Format("return statusPopUp(\"{0}\");",Eval("activityStatus")) %>'><span class="glyphicon glyphicon-minus-sign"></span></asp:LinkButton>--%>
                           
<asp:LinkButton  CommandName="StatusUpdate" runat="server" id="lnkStatus" OnClientClick='<%# string.Format("return statusPopUp(\"{0}\");",Eval("activityStatus")) %>'><span class="glyphicon glyphicon-trash text-danger"></span></asp:LinkButton>         
                            <asp:HiddenField ID="IThdfStatus" runat="server" Value='<%# Eval("activityStatus") %>' />
                            
                            
                            
                            </td>
  </tr> 
   
   
            </itemtemplate>
</asp:DataList>
   </div>








</div>



<script>
    function statusPopUp(status) {
        var msg = "";
        if (status == 'A') {
            var msg = confirm("Are you sure want to DeActive?");
        }
        else {
            var msg = confirm("Are you sure want to Active?");
        }
        if (msg == true) {
            return true;
        }
        else {
            return false;
        }
        return true;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

