﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
<title>Travel Insurance</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="images/favicon.ico">    	


 <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />

<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />

<link href="css/Default.css" rel="stylesheet" type="text/css" />

<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />

 <link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
        
    
          <!-- Bootstrap Core CSS -->
   <link href="css/bootstrap.min.css" rel="stylesheet" />

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />
    
    <link href="yui/build/fonts/fonts-min.css" rel="stylesheet" type="text/css" />
    
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="BookingPanelStyle.css" />
    
    <link href="<%=Request.Url.Scheme%>://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
    
    <link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
    
   <link href="css/toastr.css" rel="stylesheet" type="text/css" />
  


    
<link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />


    
  
</head>
<body>






<div class="topHeader2"> 

<div class="container"> 
<div class="row"> 

<div class="col-md-12"> 
<img src="images/logoc.jpg">   

</div> 

          







</div>
</div>


</div>




<div class="heading_bizz"> 

<div class="container"> 
<h2> Travel Insurance </h2>

</div>

</div>

          
<form runat="server" id="form1"> 


<div class="container"> 

<div class="innerPage2">

<div class="row"> 

<div class="col-12"> 

<p>

Losing your bags enroute to your destination, or suffering other travel-related mishaps is an unfortunate reality for customers who live out of a suitcase. Travel insurance is a must for peace of mind and hassle-free travel. CCTM is well-placed to improve customer safety with timely alerts, and we are one of the few distributors of Blue Ribbon bags, in addition to providing standard travel insurance services.<br /> <br /> 

Blue Ribbon bags help track  and expedite the return of misplaced baggage for 96 hours after your flight lands. This service covers every flight, on every airline, everywhere in the world. Get continuous status updates via email and get a guaranteed payment for bags lost, without having to furnish proof of baggage contents. 

</p>



</div>



</div>







</div>


</div>




</form>













<div class="footer_sml_bizz"> 
 
 
  Copyright 2019 Cozmo Travel World, India. All Rights Reserved. 


 
 
 </div>





    <script type="text/javascript" src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/Scripts/Menu/query2.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    
    
     <script type="text/javascript" src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/build/js/main.min.js"></script>
     
	 
	 
	 <script src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/scripts/select2.min.js" type="text/javascript"></script>
     
     
     
     
     <script type="text/javascript">
         $(document).ready(function () {

             $('select').select2();


         });
    
</script>




<script>


    //on scroll up/down show/hide top icon

    $('body').prepend('<a href="#" class="back-to-top"><i class="icon-expand_less"></i></a>');

    var amountScrolled = 300;

    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('slow');
        } else {
            $('a.back-to-top').fadeOut('slow');
        }
    });


    //on click scroll to top 

    $('a.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });


</script>




<script>



    $('#Products2,#Partners2').click(function () {
        var Linkid = $(this).attr('href'),
                            Linkid = Linkid.replace('#', '');
        $('html, body').animate({
            scrollTop: $('[name="' + Linkid + '"]').offset().top
        }, 500);


        //        alert("Hello! I am an alert box!");   



    })

       

</script>



 
    <link href="build/css/owl-carousel/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href="build/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
 

</body>
</html>





 


<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>


