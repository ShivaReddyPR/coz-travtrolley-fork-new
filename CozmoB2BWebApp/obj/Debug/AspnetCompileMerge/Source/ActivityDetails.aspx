﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ActivityDetailsGUI" Title="Activity Detail" Codebehind="ActivityDetails.aspx.cs" %>
 <%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">


<link href="css/ModalPop.css"  rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="Scripts/jsBE/ModalPop.js"></script>
   <script type="text/javascript" src="Scripts/jsBE/prototype.js"></script>

  
  
  <script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<link rel="stylesheet" href="css/ajax_tab_css.css">
   
  
    
     <script type="text/javascript">


         $(function() {
             $('.tabs').tabs()
         });

         $('.tabs').bind('change', function(e) {
             var nowtab = e.target // activated tab
             var divid = $(nowtab).attr('href').substr(1);
             if (divid == "ajax") {
                 $.getJSON('<%=Request.Url.Scheme%>://1.upstatic.sinaapp.com/api.php').success(function(data) {
                     $("#" + divid).text(data.msg);
                 });
             }


         });
  
</script>

  
  
   
   
<link rel="stylesheet" type="text/css" href="css/fancybox2.css" media="screen" />


    <script type="text/javascript">
    
        function ShowDiv(name) {
            if (name == "home") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Inclusions") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Itinerary") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "RoomRates") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Terms") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                return false;
            }
        }

//        $(function() {
//            $('.tabs').tabs()
//        });

//        $('.tabs').bind('change', function(e) {
//            var nowtab = e.target // activated tab
//            var divid = $(nowtab).attr('href').substr(1);
//            if (divid == "ajax") {
//                $.getJSON('<%=Request.Url.Scheme%>'+'://1.upstatic.sinaapp.com/api.php').success(function(data) {
//                    $("#" + divid).text(data.msg);
//                });
//            }


//        });
//  
    </script>

   
    
<script type="text/javascript" language="javascript">

    // Regular expression for Not a number;
    var IsNaN = /\D/;
    function SendMailTemp() {
        var phoneNo = document.getElementById('phoneNo').value;
        var packageName = document.getElementById('dealName').value;
        var paramList = "";
        paramList += 'dealId=' + $('dealId').value;
        paramList += '&agencyId=' + $('agencyId').value;
        paramList += '&phoneNo=' + phoneNo;
        paramList += '&packageName=' + packageName;
        //paramList += '&nights=' + $('nights').value;
        //paramList += '&isInternational=' + $('isInternational').value;
        if (Trim(phoneNo) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = 'Please enter phone no.';
            return false;
        }
        if (isNaN(phoneNo)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = 'Please enter only number';
            return false;
        }
        paramList += '&submitEnquiry1=true';
        var url = "EmailAjax.aspx";
        new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EmailResponse });
    }
    function EmailResponse(response) {
        if (response.responseText == "true") {
            document.getElementById('message').style.display = "block";
            document.getElementById('message').style.top = document.getElementById('message').style.top - 85 + "px";
            document.getElementById('message').style.left = document.getElementById('message').style.left + 5 + "px";
            document.getElementById('errMess').style.display = "none";
            document.getElementById('phoneNo').value = "enter phone number";
            document.getElementById('phoneField').style.display = "none";
        }
    }
    function ResetForm() {
        document.getElementById('phone').value = "";
        document.getElementById('email').value = "";
        document.getElementById('city').value = "";
        document.getElementById('country').value = "";
        document.getElementById('description').value = ""
        document.getElementById('paxName').value = "";
    }


    function SubmitEnquiry() {
        var phoneNo = document.getElementById('phone').value;
        var activityName = document.getElementById('dealName').value;
        var email = document.getElementById('email').value;
        var productType = document.getElementById('productType').value;
        var city = document.getElementById('city').value;
        var country = document.getElementById('country').value;
        var paxName = document.getElementById('paxName').value;
        var description = document.getElementById('description').value;
        var dealId = document.getElementById('dealId').value;
        var agencyId = document.getElementById('agencyId').value;
        var noofpax = document.getElementById('noofpax').value;
        //var paxName = document.getElementById('paxName').value;

        var paramList = "";
        paramList += '&dealId=' + dealId;
        paramList += '&agencyId=' + agencyId;
        paramList += '&description=' + description;
        paramList += '&productType=' + "A";
        paramList += '&phoneNo=' + phoneNo;
        paramList += '&paxName=' + paxName;
        paramList += '&EmailId=' + email;
        paramList += '&City=' + city;
        paramList += '&Country=' + country;
       // paramList += '&nights=' + $('nights').value;
        paramList += '&activityName=' + activityName;
        paramList += '&Noofpax=' + noofpax;
        if (noofpax <= 0) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please select no of passengers';
            return false;
        }
        if (Trim(paxName) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your name';
            return false;
        }
        if (Trim(email) == "") {
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your email';
            return false;
        }
        if (!ValidEmail.test(email)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter valid email';
            return false;
        }
        if (Trim(phoneNo) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter phone number';
            return false;
        }
        if (isNaN(phoneNo)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter correct phone number';
            return false;
        }
        if (Trim(country) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your country';
            return false;
        }
        if (Trim(city) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your city';
            return false;
        }
        if (Trim(description) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter some description / comment  of your enquiry';
            return false;
        }
        paramList += '&submitActivityEnquiry=true';
        var url = "EmailAjax.aspx";
        new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EnquiryRes });
    }
    function EnquiryRes(response) {
        if (response.responseText == "true") {
            document.getElementById('inline1').style.display = "none";
            document.getElementById('err').style.display = "none";
            document.getElementById('message').style.display = "block";
            alert('Enquiry Submitted Successfully');
		HideEmailPopup();
            ResetForm();
        }
    }
	function ShowEmailPopup() {

	    //        ModalPop.Container = $$$('emailEnquiry');
	    ModalPop.Container = document.getElementById('emailEnquiry');
        ModalPop.Show("<div>ytytyy</div>");

    }

    function HideEmailPopup() {
        ModalPop.Hide();
    }
    
    function ShowPopUP() {
        document.getElementById('popup').style.display = "block";
        document.getElementById('message').style.display = "none";
    }
    function HidePopUP() {
        document.getElementById('popup').style.display = "none";
    }
    function HidePopUp() {

        ModalPop.Hide();
    }  
    function markout(textBox, txt) {
        if (textBox.value == "") {
            textBox.value = txt;
        }
    }
    function markin(textBox, txt) {
        if (textBox.value == txt) {
            textBox.value = "";
        }
    }
    </script>
     <script type="text/javascript">
         function isNumber(evt) {
             evt = (evt) ? evt : window.event;
             var charCode = (evt.which) ? evt.which : evt.keyCode;
             if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                 return false;
             }
             return true;
         }
      </script>





  
    
   <%-- <form id="form1" runat="server" method="post">--%>
    
    <% if (activity != null)
       {%>
     <input type="hidden" id="agencyId" name="agencyId" visible="false" value="<%=Settings.LoginInfo.AgentId %>" />
     
    <input type="hidden" id="dealId"   name="dealId" value="<%=activity.Id %>" />
    <input type="hidden" id="productType"   name="productType" value="A" />
    <input type="hidden" id="dealName" name="dealName" value="<%=activity.Name %>" />
    <%} %>
       
        <div>
        
          <div class="ns-h3">
                
                    <asp:Label ID="lblActivity" runat="server" Text="" ></asp:Label>
                </div>
                
                
                
                
                
            
            <div class="wraps0ppp">
            
            
            
                 <div class="col-md-8 gap_pck pad_left0"> 
                 
                 
                 <asp:Image ID="imgActivity" runat="server" Width="624px" Height="270px" />
                 
                 
                 
                  
              
               
               </div> 
            
            
            
            <div class="col-md-4" id="BookingDetails">
                                          
                                          
                                          
                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        
                                                           
                                                            <tr>
                                                                <td align="center">
                                                              <div class="lowest_price"> Starting From</div>
                                                                
                                                                <div class="best_price">     <%=Settings.LoginInfo.Currency %>
                                                                        <asp:Label ID="lblAmount" runat="server" Text=""></asp:Label></div>   
                                                                   
                                                                   
                                                                    <div> <span class="spccc">per person</span></div>
                                                                  
                                                                  
                                                                 
                                                                </td>
                                                            </tr>
                                                            
                                                            
                                                                  <tr> 
             
             <td style=" border-bottom: solid 1px #ccc; height:10px;" colspan="2"> </td>
             </tr>
                                                                 
           
                                                     
                                                            <tr style="display:none;">
                                                            
                                                                <td align="center">
                                                                    <table  border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="right">
                                                                                <img src="images/submit_enquiry_icon.jpg" width="26">
                                                                            </td>
                                                                            <td  align="left">
                                                                                
                                                                                
                                                                                 <a class="nor_lnk" href="#" onclick="ShowEmailPopup()">Submit Enquiry </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                
                                                            </tr>
                                                          
                                                             <tr> 
             
             <td style=" border-bottom: solid 1px #ccc; colspan="2"> </td>
             </tr>
                                                            
                                                            
                                                            <tr>
                                                                <td align="center">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="right">
                                                                                <img src="images/print_icon.jpg" width="26">
                                                                            </td>
                                                                            <td align="left">
                                                                                <a style=" cursor:pointer" class="nor_lnk" onClick="javascript:window.open('PrintActivity.aspx','','location=0,status=0,scrollbars=1,width=1000,height=600');">Print this Activity </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr> 
             
             <td style=" border-bottom: solid 1px #ccc; " colspan="2"> </td>
             </tr>
                                                        </table>
                                                        
                                                        
                                            
                                             
                                             
                                   
                                   <div class="pad20"> 
                                   
                                   
                                   <center> <a Class="btn but_b" href="ActivityCategoriesList.aspx"> Book Now</a></center>
                                   
                                   </div>          
                                             
                                                
                                                
                                           
                                        </div>
              
         
                            <div class="clear">
                            </div>
            </div>
           
        </div>
        
        
        
      
        
        <div class="clear">
        </div>
     
     
     
     
     <div class="col-md-12 padding-0 margin_top20 marbot_20"> 

 

    
    <!--easy reponsive tabs-->
    <script src="Scripts/easyResponsiveTabs.js"></script>
    
    <link rel="stylesheet" type="text/css" href="css/easy-responsive-tabs.css" />
  
    
     	<!--Plug-in Initialisation-->
	<script type="text/javascript">
	    $(document).ready(function() {
	        //Horizontal Tab
	        $('#parentHorizontalTab').easyResponsiveTabs({
	            type: 'default', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            fit: true, // 100% fit in a container
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });

	        // Child Tab
	        $('#ChildVerticalTab_1').easyResponsiveTabs({
	            type: 'vertical',
	            width: 'auto',
	            fit: true,
	            tabidentify: 'ver_1', // The tab groups identifier
	            activetab_bg: '#fff', // background color for active tabs in this group
	            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
	            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
	            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
	        });

	        //Vertical Tab
	        $('#parentVerticalTab').easyResponsiveTabs({
	            type: 'vertical', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            fit: true, // 100% fit in a container
	            closed: 'accordion', // Start closed if in accordion view
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo2');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });
	    });
</script>

<div id="parentVerticalTab">
            <ul class="resp-tabs-list hor_1">
                <li>Overview</li>
                <li>Inclusions & Exclusions</li>
                
                <li style="display:none;">Itinerary</li>
                
                <li>Price</li> 
             
                         
                <li>Terms & Conditions</li> 
                                
            </ul>
           
           
            <div class="resp-tabs-container hor_1">
                
              

<div  id="Overview">
 
 
 
<%=(activity != null ? activity.Overview : "") %>
</div>


 <div id="InclusionsExclusions">
 


                            

                                        <h4>Inclusions:</h4>
                                        <ul></ul>
                                        <%if (activity != null)
                                          { %>
                                            <%foreach (string item in activity.Inclusions)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%} %>
                                      
                                       
                                     <h4>Exclusions:</h4>
                                            <%foreach (string item in activity.Exclusions)
                                              { %>
                                            <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%}
                                          }%>
                       
 
 
 </div>
 
  <div id="Div2" style="display:none;">
  
                <div> 
                                            <%=(activity != null ? activity.Itinerary1 : "") %></div>
                                       
                                       
                                       
                                       <div>  
                                            <%=(activity != null ? activity.Itinerary2 : "") %>
                                        <%=(activity != null ? activity.Details : "") %></div>
                                           
                                           
                                           <div style=" padding-top:20px">  <h4>Transfer included</h4>
                                           
                                           <div> <li style=" list-style-type:circle; margin-left:20px;"> <%=(activity != null ? activity.TransferIncluded : "") %></li></div>
                                           
                                           </div>
                                            
                                            
                                            <div style=" padding-top:20px"><h4>Meals Included</h4> </div>
                                            <ul>
                                            <%if(activity != null && activity.MealsIncluded != null){ %>
                                            <%string[] mealItems = activity.MealsIncluded.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                            <%foreach (string meal in mealItems)
                                              { %>
                                              
                                              
                                              <div> <li style=" list-style-type:circle; margin-left:20px;"><%=meal %></li></div>
                                            <%} %>
                                            <%} %>
                                            </ul>

  
  
   </div>
  
   
        <div id="Term-condition">  
        
                <label style="width:500px">
                                    <h4>Price</h4>
                                            <asp:DataList ID="dlRoomRates" runat="server" Width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                                    
                                                    <%--<tr>
                                                    <td><strong>Label</strong></td>
                                                    <td><strong>Supplier Cost</strong></td>
                                                    <td><strong>Tax</strong></td>
                                                    <td><strong>Markup</strong></td>
                                                    <td><strong>Total</strong></td>
                                                    </tr>--%>
                                                        <tr>
                                                           <td height="25px" width="50%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                            <%--<td>
                                                                <%#Eval("SupplierCost") %>
                                                            </td>
                                                            <td>
                                                                <%#Eval("Tax") %>
                                                            </td>
                                                            <td>
                                                                <%#Eval("Markup") %>
                                                            </td>--%>
                                                            <td  width="70%">
                                                            <%=Settings.LoginInfo.Currency %>
                                                                <%#CTCurrencyFormat(Eval("Total"))%>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                            </label>
        
        
        </div>
    
    
    <div id="Div3">
                       <h4>Things to bring</h4>
                                        <ul>
                                        <%if (activity != null && activity.ThingsToBring != null)
                                          { %>
                                        <%string[] things = activity.ThingsToBring.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string thing in things)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=thing %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                                        <br />
                                        <h4>Cancellation Policy</h4>
                                     <ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                                        
                                        <h4>Important Details</h4>
                                         <%=(activity != null ? activity.Details : "") %>






                   
                
 </div>
            
            
        </div>

</div> 
    
    <%--</form>--%>
    
    </div>
     
         <div id="emailEnquiry" class="signin_box_class"  style="z-index:9999; display: none; ">
         
         
        
        <div class="summer-promotion-window">
                
                
                
                
                   <div class="col-md-12"> <em class="close" style=" position:absolute; z-index:9999;  right:10px; cursor:pointer">
					  
						<i><img alt="Close" onclick="HideEmailPopup()" src="images/close-itimes.gif"></i>
					</em><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><h2 style="font-family:Calibri; font-size:22px">Why Wait &amp; Watch</h2></td>
              </tr>
              
              
              <tr>
                                    <td height="20" valign="top">
                                        <strong>Grab your opportunity Immediately</strong>
                                    </td>
                                </tr>
              
            </table></div>
            
            
                
                
                
                
                <div class="col-md-12 marbot_10"> 
                                    <label style="font-size: 11px">
                                            Leave an enquiry below and member of our team will get back to you soon.</label>
                                            
                                             <span style=" color:Red" class="red padding-left-125" id="err"></span> 
                                    
                                    </div> 
                                    
                                    
                                   
                                    
                                    <div> 
                                    
                                    <div class="col-md-6 marbot_10"> 
                                    <div>  <strong>Number of Pax</strong></div>
                                    <div><select class="form-control" id="noofpax" name="noofpax">
                                                <option value="0">-Select-</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                </select> </div>
                                    
                                    
                                    </div>
                                     <div class="col-md-6 marbot_10"> 
                                     
                                     <div> <strong>Name</strong> (required)</div>
                                     
                                     <div><input class="form-control"  type="text" id="paxName" name="paxName" class="send-enq" /> </div>
                                     
                                     
                                     </div>
                                    
                                    </div>
                                    
                                    
                                    
                                    
                                      <div> 
                                    <div class="col-md-6 marbot_10"> 
                                    <div> <strong>E-mail Id</strong> (required)</div>
                                    <div> <input class="form-control" type="text" id="email" name="email" class="send-enq" /></div>
                                    
                                     </div>
                                    
                                     <div class="col-md-6 marbot_10">  
                                     <div>  <strong>Phone Number</strong> (required)</div>
                                     
                                     <div> <input class="form-control"  type="text" id="phone" onkeypress="return isNumber(event)" name="phone" class="send-enq" /></div>
                                     
                                     
                                     </div>
                                     
                                     
                                     </div>
                                  
                                    
                                    
                                    
                                          <div> 
                                    <div class="col-md-6 marbot_10">
                                    <div> <strong>Country of Residence</strong> (required)</div>
                                    
                                    <div> <input class="form-control" type="text" id="country" name="country" class="send-enq" /></div>
                                    
                                      </div>
                                    
                                     <div class="col-md-6 marbot_10">  
                                     <div> <strong>City of Residence</strong> (required)</div>
                                     
                                     <div> <input class="form-control" type="text" id="city" name="city" class="send-enq" /></div>
                                     
                                     </div>
                                     
                                     
                                     </div>
                                     
                                     
                                            <div> 
                                    <div class="col-md-12">
                                    
                                    <div>  <strong>Please enter a brief description of your need.</strong></div>
                                    
                                    <div class="marbot_10"> <textarea class="form-control" rows="3" cols="100"
                                          
                                          
                                                                id="description" name="description" class="send-enq"></textarea></div>
                                                                
                                                                
                                        
                                        <div  class="marbot_10">
                                        
                                        
                                        
                       <input type="submit" class="but but_b pull-right btn-xs-block" onclick="return SubmitEnquiry();" value="Submit">
 
 
                                        
                                  <%--      
                                         <a class="button-new pull-right" onclick="SubmitEnquiry()">
                                        <strong> Submit</strong>--%>
                                        
                                        </a></div>                      
                                             <dfn id="errMess"></dfn>                   
                                    
                                     </div>
                                    
                                    
                                    
                                    
                                    </div>
                                    
                                    
                                    
                                  <div class="marbot_10">    <i id="message" style="display: none;"><del>
                                    <img src="Images/greenmark.gif" alt="mark" /></del> <dfn>Our Travel Expert will call
                                        you shortly</dfn> </i>   </div>
                       
                       <div class="clearfix"> </div>             
                
                </div>
                
                
                
           
         
         </div>
     
        <div id="inline1" style="display: none;">
       
            
          
          
        </div>
       <%-- 
         <%} %>
            <%} %>--%>
       
    </div>
   
   <%--</form>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

