<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddVisaFaq" Title="Visa Faq" ValidateRequest="false" Codebehind="AddVisaFaq.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server" >
   <link rel="stylesheet" type="text/css" href="css/style.css" />
   <div class="b2b_visa_form">
                        <div class="cal_head">
                           <% if (UpdateFaq > 0)
                           {%>
                               <span><b>Update visa faq</b></span>

                          <% }
                           else
                           {  %>
                               <span><b>Add visa faq</b></span>
                          <% }
                               %>
                               <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/VisaFaqList.aspx">Go to Visa Faq List</asp:HyperLink>
                    </div>
                     <div class="login">
                            <div class="parent">
                            <p>
                                <label>
                                    Agent<sup>*</sup>
                                </label>
                                <span>
                                    <asp:DropDownList ID="ddlAgent" CssClass="inp_border18" Width="162" runat="server" >
                                    </asp:DropDownList></span><asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select a Agent "
                                     ControlToValidate="ddlAgent"  InitialValue="Select Agent"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <label>
                                    Question<sup style="color:Red">*</sup></label>
                                <span>
                                    <asp:TextBox ID="txtQuestion" CssClass="inp_border18"  runat="server" Width="400px"></asp:TextBox>&nbsp;</span>
                                    
                                    
                                    
                                <asp:RequiredFieldValidator ID="rfvTxtQuestion" runat="server" ErrorMessage="Please fill question"
                                    ControlToValidate="txtQuestion"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter maximum 2000 character"
                         ValidationExpression="[\S\s]{0,2000}" ControlToValidate="txtQuestion" Display="Dynamic"></asp:RegularExpressionValidator>
                                      </p>
                            <p>
                                <label>
                                    Answer<sup style="color:Red">*</sup></label>
                                    <span>
                                    <asp:TextBox id="txtAnswer" name="txtAnswer" TextMode="MultiLine"  CssClass="inp_border18" Width="400px" Height="200px" runat="server"></asp:TextBox></span>
                                    <asp:RequiredFieldValidator ID="rfvTxtAnswer" runat="server" ErrorMessage="Please fill answer"
                                    ControlToValidate="txtAnswer"></asp:RequiredFieldValidator>
                            </p>
                            <p><dfn>
                                <label>&nbsp;</label>
                                <span>
                                    <asp:Button ID="btnSave" runat="server" Text=" Save " CssClass="clik input_buttonv"
                                        OnClick="btnSave_Click" /></span>
                                </dfn>
                            </p>
                        </div>
                    </div>
                </div>
         
</asp:Content>

