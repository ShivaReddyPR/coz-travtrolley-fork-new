<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateExpenseApprovalsUI"
    Title="Expense Approvals" Codebehind="CorporateExpenseApprovals.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <h2>
                Expense Approvals
            </h2>
            <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
            <div class="tab-content responsive">
                <div style="background: #fff; padding: 10px;">
                    <div>
                        <h4>
                            <strong>Expense list</strong>
                        </h4>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcApprovalFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcApprovalToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClick="btnSearchApprovals_Click" CssClass="btn btn-primary btn-blue"
                                runat="server" ID="btnSearchApprovals" Text="Search" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
               <div class="table table-responsive mb-0">
                  <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="Id"
                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="left" />
                        <ItemTemplate>
                            <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("Id") %>'>
                            </asp:HiddenField>
                             <asp:HiddenField ID="hdfExpDetailId" runat="server" Value='<%# Bind("EXPDETAIL_ID") %>'>
                            </asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="center" />
                        <HeaderTemplate>
                            <label style="color: Black">
                                Select</label>
                            <asp:CheckBox runat="server" ID="HTchkSelectAll" AutoPostBack="true" OnCheckedChanged="ITchkSelect_CheckedChanged">
                            </asp:CheckBox>
                        </HeaderTemplate>
                        <ItemStyle />
                        <ItemTemplate>
                            <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEEID" CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEENAME" CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF." CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblEXPREF" runat="server" Text='<%# Eval("EXPREF") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("EXPREF") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY." CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXPMODE") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("EXPMODE") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtEXPTYPE" Width="100px" HeaderText="EXPENSE TYPE." CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblEXPTYPE" runat="server" Text='<%# Eval("EXPTYPE") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("EXPTYPE") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderTemplate>
                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="AMOUNT" CssClass="form-control"
                                OnClick="FilterSearch_Click" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("AMOUNT") %>' CssClass="label grdof"
                                ToolTip='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# Eval("EXPDETAIL_ID", "~/CorporateViewExpenseDetails.aspx?ExpId={0}") %>'
                                Text='View Details' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                </div>
        </div>
        <div class="pad_10">
            <asp:Button CssClass="btn btn-primary btn-blue" OnClick="btnApprove_Click" runat="server"
                ID="btnApprove" Text="Approve" />
            <asp:Button OnClick="btnReject_Click" Style='background: #999;' CssClass="btn but_d btn_xs_block cursor_point"
                runat="server" ID="btnReject" Text="Reject" />
            <div class="clearfix">
            </div>
        </div>
    </div>
    <!-- Email DIV-->
    <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
      <%if(detail != null) %>
    <%{ %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        </head>
        <body>
            <div style="margin: 0; background: #f3f3f3!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; min-width: 100%;
                padding: 0; text-align: left; width: 100%!important">
                <span style="color: #f3f3f3; display: none!important; font-size: 1px; line-height: 1px;
                    max-height: 0; max-width: 0; overflow: hidden"></span>
                <table style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                    border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                    font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                    vertical-align: top; width: 100%">
                    <tr style="padding: 0; text-align: left; vertical-align: top">
                        <td align="center" valign="top" style="margin: 0; border-collapse: collapse!important;
                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                            line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                            word-wrap: break-word">
                            <center style="min-width: 580px; width: 100%">
                                <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                    float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                    vertical-align: top; width: 580px" align="center">
                                    <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                            <td style="margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                                                vertical-align: top; word-wrap: break-word">
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <img src="<%=Request.Url.Scheme%>://www.travtrolley.com/images/logo.jpg" style="clear: both; display: block;
                                                                                max-width: 100%; outline: 0; text-decoration: none; width: auto">
                                                                        </th>
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                            width: 0">
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                                <%if (detail != null) %>                                        
                          <%{  %>
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left; word-wrap: normal">
                                                                                Dear <% =detail.EmpName %>,</h1>
                                                                            <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left">
                                                                                Below are the expenses which are  <% =_approvalStatus %> by  <% = approverNames  %></p>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <%} %>
                                                                            
                                                                            
                                                                            
                                                                            <table style="background: #fff; border: 1px solid #167f92; border-collapse: collapse;
                                                                                border-radius: 10px; border-spacing: 0; color: #024457; font-size: 11px; margin: 1em 0;
                                                                                padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                                                <tr style="background-color: #eaf3f3; border: 1px solid #d9e4e6; padding: 0; text-align: left;
                                                                                    vertical-align: top">
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Date
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Ref
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Category
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Type
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Amount
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Status
                                                                                    </th>
                                                                                </tr>
                                                                                
                                                                     <%if (detail != null) %>                                        
                          <%{  %>
                                   
                                                                                
                                                                                
                                                                                <tr style="border: 1px solid #d9e4e6; padding: 0; text-align: left; vertical-align: top">
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% = Convert.ToString(detail.Date).Split(' ')[0] %>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        
                                                                                        <% = detail.DocNo%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% = detail.ExpCategoryText %>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% = detail.ExpTypeText%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <%= detail.Amount%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <span style="color: green"><% =_approvalStatus %></span>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <% } %>  
                            
                                
                                                                               
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                                                position: relative; text-align: left; vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                            font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                                            padding-bottom: 16px; padding-left: 0!important; padding-right: 0!important;
                                                                                            text-align: left; width: 100%">
                                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                                vertical-align: top; width: 100%">
                                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                                                        <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                            padding: 0; text-align: left">
            <%if(detail != null) %>  
            <%{ %>                       
                                                                                                            Regards<br>
                                                                                                            <strong><% =detail.AgencyName %></strong></p>
                                                      <%} %>
                                                                                                    </th>
                                                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                                                        width: 0">
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </th>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </table>
                <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                </div>
            </div>
        </body>
        </html>
        
        <%} %>
    </div>
    
    
    
     <!-- Email DIV-->
    <div id='EmailDivApprover' runat='server' style='width: 100%; display: none;'>
    <%if(detail != null) %>
    <%{ %>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            <title>Expense Approval Request</title>
            <style>
                @media only screen
                {
                    html
                    {
                        min-height: 100%;
                        background: #f3f3f3;
                    }
                }
                @media only screen and (max-width:596px)
                {
                    table.body img
                    {
                        width: auto;
                        height: auto;
                    }
                    table.body center
                    {
                        min-width: 0 !important;
                    }
                    table.body .container
                    {
                        width: 95% !important;
                    }
                    table.body .columns
                    {
                        height: auto !important;
                        -moz-box-sizing: border-box;
                        -webkit-box-sizing: border-box;
                        box-sizing: border-box;
                        padding-left: 16px !important;
                        padding-right: 16px !important;
                    }
                    table.body .columns .columns
                    {
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                    }
                    th.small-6
                    {
                        display: inline-block !important;
                        width: 50% !important;
                    }
                    th.small-12
                    {
                        display: inline-block !important;
                        width: 100% !important;
                    }
                    .columns th.small-12
                    {
                        display: block !important;
                        width: 100% !important;
                    }
                }
            </style>
        </head>
        <body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box;
            -webkit-text-size-adjust: 100%; margin: 0; background: #f3f3f3!important; box-sizing: border-box;
            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
            line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100%!important">
            <span class="preheader" style="color: #f3f3f3; display: none!important; font-size: 1px;
                line-height: 1px; max-height: 0; max-width: 0; mso-hide: all!important; opacity: 0;
                overflow: hidden; visibility: hidden"></span>
            <table class="body" style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                vertical-align: top; width: 100%">
                <tr style="padding: 0; text-align: left; vertical-align: top">
                    <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto;
                        margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                        font-size: 16px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                        padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                        <center data-parsed="" style="min-width: 580px; width: 100%">
                            <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                vertical-align: top; width: 580px" align="center" class="container float-center">
                                <tbody>
                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                            hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                                            word-wrap: break-word">
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word">
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px">
                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%">
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                        <img src="<%=Request.Url.Scheme%>://www.travtrolley.com/images/logo.jpg" style="-ms-interpolation-mode: bicubic;
                                                                            clear: both; display: block; max-width: 100%; outline: 0; text-decoration: none;
                                                                            width: auto">
                                                                    </th>
                                                                    <th class="expander" style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                        font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                        text-align: left; visibility: hidden; width: 0">
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word">
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <%if (detail != null) %>
                                            <%{  %>
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px">
                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%">
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                        <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left; word-wrap: normal">
                                                                            Dear
                                                                       <%if (!string.IsNullOrEmpty(approverNames) && approverNames.Length > 0) %>
                                              <%{ %>
                                              
                          <%=approverNames %>
                                              <%} %>
                                                           
                                                           
                                                           </h1>
                                                                        <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left">
                                                                            Find below expense claim details for Employee ID :
                                                                            <% =detail.EmpId%>, Employee Name :
                                                                            <% =detail.EmpName%></p>
                                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table class="approver-table" style="background: #FFF; border: 1px solid #167F92;
                                                                            border-collapse: collapse; border-radius: 10px; border-spacing: 0; color: #024457;
                                                                            font-size: 11px; margin: 1em 0; padding: 0; text-align: left; vertical-align: top;
                                                                            width: 100%">
                                                                            <tr style="background-color: #EAF3F3; border: 1px solid #D9E4E6; padding: 0; text-align: left;
                                                                                vertical-align: top">
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Date
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Ref
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Category
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Type
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Amount
                                                                                </th>
                                                                                <th style="Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left">Action</th>
                                                                                
                                                                                
                                                                            </tr>
                                                                            <%if (detail != null) %>
                                                                            <%{  %>
                                                                            
                                                                            <tr style="border: 1px solid #D9E4E6; padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = Convert.ToString(detail.Date).Split(' ')[0]%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = detail.DocNo%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% =detail.ExpCategoryText%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = detail.ExpTypeText%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = detail.Amount%>
                                                                                </td>
                                                                                <td style="Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word">
    <a href="<% =ConfigurationManager.AppSettings["CORPORATE_ROOT_URL"]%>/hn_CorporateApprovals.ashx?status=<% =HttpContext.Current.Server.UrlEncode("A")%>&approverId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(approverId))%>&expDetailId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(detail.ExpDetailId))%>" style="Margin:0;color:green;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:underline">Approve</a> | <a href="<% =ConfigurationManager.AppSettings["CORPORATE_ROOT_URL"]%>/hn_CorporateApprovals.ashx?status=<% =HttpContext.Current.Server.UrlEncode("R")%>&approverId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(approverId))%>&expDetailId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(detail.ExpDetailId))%>" style="Margin:0;color:red;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:underline">Reject</a></td>                                       </tr>
                                                                            <% } %>
                                                                           
                                                                        </table>
                                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <%if (detail != null) %>
                                                                        <%{  %>
                                                                        <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                                            padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0!important;
                                                                                        padding-right: 0!important; text-align: left; width: 100%">
                                                                                        <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                            vertical-align: top; width: 100%">
                                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                                <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                    font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                                                    <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                        font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                        padding: 0; text-align: left">
                                                                                                        Regards<br>
                                                                                                                                                                                                       <strong><% = detail.AgencyName%></strong></p>
                                                                                                </th>
                                                                                                <th class="expander" style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                    font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                                                    text-align: left; visibility: hidden; width: 0">
                                                                                                </th>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <% } %>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>
            <!-- prevent Gmail on iOS font size manipulation -->
            <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
        </body>
        </html>
        
        <%} %>
    </div>
    
   
    
    
</asp:Content>
