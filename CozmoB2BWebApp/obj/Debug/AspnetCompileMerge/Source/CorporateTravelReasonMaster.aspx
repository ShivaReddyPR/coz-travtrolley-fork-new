﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TransactionVisaTitle.master" Inherits="CorporateTravelReasonMasterGUI" Title="Corporate TravelReason" Codebehind="CorporateTravelReasonMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <asp:HiddenField ID="hdnrRequireId" Value="0" runat="server" />
     <div class="body_container">    
    <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblAgent" runat="server" Text="Agent:"></asp:Label></div>
   
        <div class="col-md-2"> <asp:DropDownList ID="ddlAgent" runat="server" class="form-control" ></asp:DropDownList></div>
<div class="clearfix"></div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="LabeCode" runat="server" Text="Code:"></asp:Label></div>
   
        <div class="col-md-2"> <asp:TextBox ID="txtCode" runat="server" MaxLength="15"  CssClass="inputEnabled form-control" onkeypress="return IsAlphaNumeric(event);"></asp:TextBox></div>
<div class="clearfix"></div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblDescription" runat="server" Text="Description:"></asp:Label></div>
   
        <div class="col-md-2"> <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="inputEnabled form-control" ></asp:TextBox></div>
<div class="clearfix"></div>
        </div>
     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblType" runat="server" Text="Type:"></asp:Label></div>
   
        <div class="col-md-2"> <asp:DropDownList ID="ddlType" runat="server" class="form-control" >
             <asp:ListItem Text="--Select Type--" Value="-1"></asp:ListItem>
             <asp:ListItem Text="Division" Value="A"></asp:ListItem>
                <asp:ListItem Text="Grade" Value="B"></asp:ListItem>
                <asp:ListItem Text="Designation" Value="C"></asp:ListItem>                
                <asp:ListItem Text="Department" Value="D"></asp:ListItem>
                               </asp:DropDownList></div>
<div class="clearfix"></div>
        </div>
          <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblActionStatus" runat="server" Text="ActionStatus:"></asp:Label></div>
   
        <div class="col-md-2"> <asp:DropDownList ID="ddlActionStatus" runat="server" class="form-control" >
              <asp:ListItem Text="--Select ActionStatus--" Value="-1"></asp:ListItem>
            <asp:ListItem Text="Pending" Value="P"></asp:ListItem>
                <asp:ListItem Text="Approved" Value="A"></asp:ListItem>
                <asp:ListItem Text="Rejected" Value="R"></asp:ListItem>                
               
                               </asp:DropDownList></div>
<div class="clearfix"></div>
        </div>
         <div style="padding-left:230px">
             <asp:Button ID="btnSave" Text="Save" runat="server" 
                              CssClass="but but_b" OnClientClick="return Save();" OnClick="btnSave_Click" ></asp:Button>
                              
                    <asp:Button ID="btnCancel" Text="Clear" runat="server" CssClass="but but_b" OnClick="btnCancel_Click"  ></asp:Button>
                    
                    <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="but but_b" OnClick="btnSearch_Click" ></asp:Button>
              <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
         </div>
         </div>
     <script type="text/javascript">
                function Save() {
                    if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please select Agent from the list!', '');
                    if (getElement('txtCode').value == '') addMessage('Code cannnot be blank!', '');
                    if (getElement('txtDescription').value == '') addMessage('Description cannnot be blank!', '');
                    if (getElement('ddlType').selectedIndex <= 0) addMessage('Please select Type from the list!', '');                   
                    if (getElement('ddlActionStatus').selectedIndex <= 0) addMessage('Please select ActionStatus from the list!', '');                   
                    if (getMessage() != '') {
                        alert(getMessage()); clearMessage();
                        return false;
                    }
                }
                var specialKeys = new Array();
                specialKeys.push(8); //Backspace
                specialKeys.push(9); //Tab
                specialKeys.push(46); //Delete
                specialKeys.push(36); //Home
                specialKeys.push(35); //End
                specialKeys.push(37); //Left
                specialKeys.push(39); //Right
                function IsAlphaNumeric(e) {
                    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                    var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
                    return ret;
                }
         </script>
 </asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="REASONID"
        EmptyDataText="No Data Found!" AutoGenerateColumns="false" PageSize="10" GridLines="none" CssClass="grdTable"  CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
       >
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                
                <HeaderTemplate>
                  <asp:Label ID="lblhdrAgent" runat="server" Text="Agent" CssClass="label "></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblAgent" runat="server" Text='<%# Eval("AGENT_NAME") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblhdrCode" runat="server" Text="Code" CssClass="label grdof"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblCode" runat="server" Text='<%# Eval("CODE") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate>
                    <asp:Label ID="lblhdrType" runat="server" Text="Type" CssClass="label grdof"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("TYPE") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblhdrActionStatus" runat="server" Text="ActionStatus" CssClass="label grdof"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblActionStatus" runat="server" Text='<%# Eval("ACTIONSTATUS") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           </Columns>
           </asp:GridView>
</asp:content>
