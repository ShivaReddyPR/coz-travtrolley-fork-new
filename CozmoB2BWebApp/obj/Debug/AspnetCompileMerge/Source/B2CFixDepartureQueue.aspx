﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="B2CFixDepartureQueueGUI" Title="B2C FixedDeparture" Codebehind="B2CFixDepartureQueue.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
     <script type="text/javascript" src="ash.js"></script>
     <link rel="stylesheet" href="css/style.css" />
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
            </div>
<div class="clear" style="margin-left: 30px">
            <div id="container3" style="position: absolute; top:120px; left: 97px; display: none;">
            </div>
        </div>
         <div class="clear" style="margin-left: 30px">
            <div id="container4" style="position: absolute; top:120px; left: 377px; display: none;">
            </div>
        </div>
        <div style=" padding-top:10px">
  <div title="Param" style=" border: solid 1px #ccc; padding:5px;">
        
        <table width="100%" cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:75px"></td>
                <td style="width:175px"></td>
                <td style="width:75px"></td>
                <td style="width:175px"></td>
                <td style="width:75px"></td>
                <td style="width:100px"></td>
                <td style="width:75px"></td>
                <td style="width:75px"></td>
            </tr> 
           
            <tr height="30px">
                 <td align="right"><asp:Label ID="lblFromDate" Text="From Date:" runat="server" CssClass="label" ></asp:Label></td>
                  <td>          <p class="fleft car-search">
                                <span class="fleft margin-top-3 margin-left-5">
                                <asp:TextBox ID="txtFrom" CssClass="auto-list" runat="server" Width="80"></asp:TextBox>
                                 </span>
                                 <a href="javascript:void(null)" onclick="showCalendar3()">
                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                            margin: 5px" />
                    </a>
</p>
                                 </td>
                                
                 <td align="right"><asp:Label ID="lblToDate" Text="To Date:" runat="server" CssClass="label" ></asp:Label></td>
                 <td> <p class="fleft car-search">
            <span class="fleft margin-top-3 margin-left-5">
                  <asp:TextBox ID="txtTo" CssClass="auto-list" runat="server" Width="80"></asp:TextBox>
            </span>
            <a href="javascript:void(null)" onclick="showCalendar4()">
                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                            margin: 5px" />
                    </a>
</p>
            </td> 
              <td align="left"><asp:Label ID="lblTripId" Text="Trip Id:" runat="server" CssClass="label" ></asp:Label></td>
              <td> <asp:TextBox ID="txtTripId" CssClass="auto-list" runat="server" Width="80"></asp:TextBox></td>
         
                 
            </tr>
            
            
            <tr height="30px">
                  <td align="right"><asp:Label ID="lblPaxName" Text="Pax Name:" runat="server" CssClass="label" ></asp:Label></td>
              <td>&nbsp;<asp:TextBox ID="txtPaxName" CssClass="auto-list" runat="server" Width="80"></asp:TextBox></td>
                                
                  <td align="right"><asp:Label ID="lblPrice" Text="Price:" runat="server" CssClass="label" ></asp:Label></td>
              <td>&nbsp;<asp:TextBox ID="txtPrice" CssClass="auto-list" runat="server" Width="80" onkeypress="return restrictNumeric(this.id,'0');" onfocus="Check(this.id);" onBlur="Set(this.id);"></asp:TextBox></td>
              
             <td align="left">
                    &nbsp; Agency: 
                </td>
                <td>&nbsp;<asp:DropDownList ID="ddlAgency" runat="server" CssClass="inputDdlEnabled"></asp:DropDownList>
                </td>
         
                 
                 
            </tr>
            <tr height="30px">
                
                <td colspan="6"></td>
                <td align="left"><asp:Button runat="server" ID="btnSearch" Text="Search" 
                         CssClass="button" OnClick="btnSearch_Click"  /></td>
                         <td align="left">
                         <asp:Button runat="server" ID="btnClear" Text="Clear" Width="50px"  
                         CssClass="button" OnClick="btnClear_Click"  />
                         </td>
            </tr>
        </table>
           </div>
                     <div style=" height:40px">
           
           <div class="pager" style="height:25px;">
                        Page:
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First" CssClass="PagerStyle"></asp:LinkButton>
                        
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" CssClass="PagerStyle"/>
                        <asp:Label ID="lblCurrentPage" runat="server" CssClass="PagerStyle"></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" CssClass="PagerStyle" />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last" CssClass="PagerStyle"></asp:LinkButton>
                    </div>
           </div>
           <div style="width: 970px;  color: #000000; background-color: #FFFFFF;">
                 <asp:DataList ID="dlActivityQueue" runat="server" CellPadding="4"
                     DataKeyField="ATHId">
                     <ItemTemplate>
                      <div id="Result" style="width: 950px;"> 
                        
                         <table class="tbl" style="background-color: #EEF0FC;" width="99%" >
                             <tr style="height: 5px;">
                                 <td style="width: 45%">
                                 </td>
                                 <td style="width: 25%">
                                 </td>
                                 <td style="width: 25%">
                                 </td>
                                 <td style="width: 9%">
                                 </td>
                             </tr>
                             <tr style="height: 25px;">
                           <%--  <td></td>--%>
                                 <td colspan="1">
                                 <div>
                                 <p>
                                     <span style="color:#2B6BB5; font-size:small;font-weight: bold;">
                                         <%# Eval("activityName")%></span>
                                      <span>(<%# Eval("agent_name")%>)</span></p></div>
                                 </td>
                                 <td> Status:<b><%# Eval("STATUS")%> </b></td>
                                 <td>
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("TripId")%></span>
                                            
                                 
                                 </td>
                                 <td></td>
                             </tr>
                             <tr style="height: 25px;">
                             <%--<td></td>--%>
                                 <td>
                                 <table>
                                         <tr>
                                             <td style="width:80px">
                                                 <span style="font-size: 10px;">Booked:</span>
                                             </td>
                                             <td>
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("TransactionDate")%></span>
                                             </td>
                                         </tr>
                                     </table>
                                     
                                 </td>
                                 <td></td>
                                 <td>
                                     <span class="fleft font-14" style="color: Blue;">
                                         <%#Eval("LeadPaxName")%>
                                     </span>
                                 </td>
                                 <td align="center">
                                     <span>
                                         <input id="Open" class="button" onclick='location.href=&#039;ViewBookingForActivity.aspx?bookingId=<%# Eval("ATHId") %>&#039;'
                                             type="button" value="Open" />
                                     </span>
                                       <span>
                                         <input id="viewInvoice" class="button"  onclick=window.open("FixedDepartureInvoice.aspx?bookingId=<%# Eval("ATHId") %>&agencyId=<%# Eval("AgencyId") %>","Invoice","width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                                             type="button" value="View Invoice" />
                                     </span>
                                 </td>
                             </tr>
                             <tr style="height: 25px;">
                            <%-- <td></td>--%>
                                 <td>
                                     <table>
                                         <tr>
                                             <td style="width:80px">
                                                 <span style="font-size: 10px;">Tour Dt:</span>
                                             </td>
                                             <td>
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("Booking")%></span>
                                             </td>
                                         </tr>
                                     </table>
                                 </td>
                                 <td>
                                     
                                 </td>
                                 <td>
                                     <span style="font-size: large; font-weight: bold;" class="font-12">Price:
                                         <%#CTCurrencyFormat(Eval("TotalPrice"), Eval("AgencyId"))%>
                                         </span>
                                 </td>
                                 <td>
                                 </td>
                             </tr>
                         </table>
                       
                        </div>
                     </ItemTemplate>
                 </asp:DataList>
             </div>
           </div>
           <script type="text/javascript">

               var cal3;
               var cal4;
               function init() {

                   //    showReturn();
                   var today = new Date();
                   // For making dual Calendar use CalendarGroup  for single Month use Calendar     

                   cal3 = new YAHOO.widget.Calendar("cal3", "container3");
                   cal3.cfg.setProperty("title", "Purchase From Date");
                   cal3.selectEvent.subscribe(setDate3);
                   cal3.cfg.setProperty("close", true);
                   cal3.render();

                   cal4 = new YAHOO.widget.Calendar("cal4", "container4");
                   cal4.cfg.setProperty("title", "Purchase To Date");
                   cal4.selectEvent.subscribe(setDate4);
                   cal4.cfg.setProperty("close", true);
                   cal4.render();
               }

               function showCalendar3() {

                   //$('container4').context.styleSheets[0].display = "none";
                   cal4.hide();
                   cal3.show();

                   document.getElementById('container3').style.display = "block";
               }
               var departureDate = new Date();
               function showCalendar4() {
                   //$('container3').context.styleSheets[0].display = "none";
                   cal3.hide();
                   cal4.show();
                   var date3 = document.getElementById('<%= txtFrom.ClientID%>').value;

                   if (date3.length != 0 && date3 != "DD/MM/YYYY") {
                       var depDateArray = date3.split('/');

                       var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                       cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                       cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                       cal4.render();
                   }
                   document.getElementById('container4').style.display = "block";
               }

               function setDate3() {


                   var date3 = cal3.getSelectedDates()[0];

                   var month = date3.getMonth() + 1;
                   var day = date3.getDate();

                   if (month.toString().length == 1) {
                       month = "0" + month;
                   }

                   if (day.toString().length == 1) {
                       day = "0" + day;
                   }

                   document.getElementById('<%=txtFrom.ClientID %>').value = day + "/" + month + "/" + date3.getFullYear();
                   cal3.hide();
               }

               function setDate4() {
                   var date4 = cal4.getSelectedDates()[0];

                   var date3 = document.getElementById('<%=txtFrom.ClientID %>').value;
                   if (date3.length == 0 || date3 == "DD/MM/YYYY") {
                       document.getElementById('errMess').style.display = "block";
                       document.getElementById('errorMessage').innerHTML = "First select Purchase from date.";
                       return false;
                   }

                   var depDateArray = date3.split('/');

                   // checking if date1 is valid		    
                   if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                       document.getElementById('errMess').style.display = "block";
                       document.getElementById('errorMessage').innerHTML = " Invalid Purchase From Date";
                       return false;
                   }
                   document.getElementById('errMess').style.display = "none";
                   document.getElementById('errorMessage').innerHTML = "";

                   // Note: Date()	for javascript take months from 0 to 11
                   var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                   var returndate = new Date(date4.getFullYear(), date4.getMonth(), date4.getDate());
                   var difference = returndate.getTime() - depdate.getTime();

                   if (difference < 0) {
                       document.getElementById('errMess').style.display = "block";
                       document.getElementById('errorMessage').innerHTML = "Purchase To date should be greater than Purchase from date (" + date3 + ")";
                       return false;
                   }

                   document.getElementById('errMess').style.display = "none";
                   document.getElementById('errorMessage').innerHTML = "";

                   var month = date4.getMonth() + 1;
                   var day = date4.getDate();

                   if (month.toString().length == 1) {
                       month = "0" + month;
                   }

                   if (day.toString().length == 1) {
                       day = "0" + day;
                   }

                   document.getElementById('<%=txtTo.ClientID %>').value = day + "/" + month + "/" + date4.getFullYear();
                   cal4.hide();
               }

               YAHOO.util.Event.addListener(window, "load", init);
               YAHOO.util.Event.addListener(this, "click", init);

               function Check(id) {
                   var val = document.getElementById(id).value;
                   if (val == '0.00') {
                       document.getElementById(id).value = '';
                   }
               }
               function Set(id) {
                   var val = document.getElementById(id).value;
                   if (val == '' || val == '0.00') {
                       document.getElementById(id).value = '0.00';
                   }
               }
               function restrictNumeric(fieldId, kind) {
                   try {
                       return (maskNumeric(fieldId, (kind == '3' || kind == '4' ? 'false' : 'true'), (kind == '1' || kind == '3' ? 'true' : 'false')))
                   }
                   catch (err) {
                       showError(err, 'Validation', 'restrictNumeric'); return (false)
                   }
               }
               function maskNumeric(fieldId, ignoreNegative, IgnoreDecimal) {
                   var key; var keychar
                   if (ignoreNegative == null) ignoreNegative = 'true'
                   if (IgnoreDecimal == null) IgnoreDecimal = 'true'
                   if (window.event) {
                       if (navigator.appName.substring(0, 1) == 'M') key = window.event.keyCode
                       else key = window.event.charCode
                   }
                   else if (event) key = event.which
                   else return true
                   keychar = String.fromCharCode(key)
                   if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) return true
                   var strSet = "0123456789" + (ignoreNegative == 'true' ? '' : '-') + (IgnoreDecimal == 'true' ? '' : '.')
                   if ((strSet.indexOf(keychar) > -1)) {
                       var inputbox = document.getElementById(fieldId)
                       if (ignoreNegative == 'false' && key == 45) {
                           if (inputbox.value.indexOf('-') == -1) inputbox.value = '-' + inputbox.value
                           return (false)
                       }
                       if (IgnoreDecimal == 'false' && inputbox.value.indexOf('.') > -1 && key == 46) return (false)
                       return true
                   }
                   return (false)
               }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

