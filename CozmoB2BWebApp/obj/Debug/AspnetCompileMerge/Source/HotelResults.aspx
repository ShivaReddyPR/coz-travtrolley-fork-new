﻿
<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" Inherits="HotelResults" Title="Hotel Search Results" Codebehind="HotelResults.aspx.cs" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

<script type="text/javascript" src="Scripts/jsBE/SearchResult.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/ModalPop.js"></script>

    <%--<script src="Scripts/ModalPop.js" type="text/javascript"></script>--%>

    <script type="text/javascript" src="Scripts/jsBE/Utils.js" language="javascript"></script>

    <link href="css/ModalPop.css" rel="stylesheet" type="text/css" />
    <link href="css/BookingStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/popup_box.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/ajax_tab_css.css">
 
<script src="<%=Request.Url.Scheme%>"://maps.googleapis.com/maps/api/js?key=AIzaSyB_RGvSBnxHX_IPvIrlEg1TGy3YaIwvwMk&sensor=true" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/Jquery/popup_window.js"></script>
    <%--<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>--%>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
   
    <%--<script src="Scripts/Jquery/fadeslideshow.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
    //Map resoluations purpose Added by brahmam 24.10.2016
        HTMLElement.prototype._getBoundingClientRect = HTMLElement.prototype.getBoundingClientRect;
        HTMLElement.prototype.getBoundingClientRect = function() {
            try {
                return this._getBoundingClientRect();
            } catch (e) {
                return { top: this.offsetTop, left: this.offsetLeft };
            }
        }
//        if (document.getElementById('latitude') != null && document.getElementById('longitude') != null) {
//            var myCenter = new google.maps.LatLng(document.getElementById('latitude').value, document.getElementById('longitude').value);
//            var marker;
        function initialize() {
            if (document.getElementById('latitude') != null && document.getElementById('longitude') != null) {
                myCenter = new google.maps.LatLng(document.getElementById('latitude').value, document.getElementById('longitude').value);
                var mapProp = {
                    center: myCenter,
                    zoom: 18,
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true,
                    rotateControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);


                marker = new google.maps.Marker({
                    position: myCenter,
                    animation: google.maps.Animation.BOUNCE
                });

                marker.setMap(map);

                //            var infowindow = new google.maps.InfoWindow({
                //                content: "Hotel Name"
                //            });

                //            infowindow.open(map, marker);
            }
        }
        //}
        //google.maps.event.addDomListener(window, 'load', initialize);
        
        var Ajax;
        var hotelIndex = "";
        var CONTEXT_HOTELDETAIL = ''

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        function GetHotelDetail(index, hotelCode, cityCode, hotelName, isDom, sessionId, sourceLink) {
            hotelIndex = index;
            CONTEXT_HOTELDETAIL = sourceLink;
            var paramList = 'Action=getHotelDetail' + '&hotelIndex=' + hotelIndex + '&sessionId=' + sessionId + '&hotelCode=' + hotelCode + '&cityCode=' + cityCode + '&hotelName=' + hotelName + '&isDom=' + isDom + '&sourceLink=' + sourceLink;
            var url = "HotelDetailAjax.aspx";
            ModalPop.ControlBox.style.display = "none";
            var modalMessage = "<div><img alt=\"Loading...\" height=\"100\" width=\"100\" src=\"Images/ajax-loader.gif\" style=\"margin: 10% 40% ;\" /><h3 class=\"primary-color\"> <center> Loading Hotel Details...</center></h3></div>";
            ModalPop.Show({ x: 0  }, modalMessage);
            
            //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowHotelDetail }
            Ajax.onreadystatechange = ShowHotelDetail;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
          
        }
        function ShowHotelDetail(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        var text = Ajax.responseText;
                        ModalPop.ControlBox.style.display = "none";
                        var modalMessage = text;
                        modalMessage += "<br/><div class=\"close_fcozmo\" onclick=\"javascript:HideWindowPopUp(0)\"><a class=\"xclo\">X</a> </div>";
                        ModalPop.Show({ x: 0 }, modalMessage);
                        if (CONTEXT_HOTELDETAIL == 'photos') {
                            ShowDiv('Photos', 'Overview');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'description') {
                            ShowDiv('Hot_Desc', 'Overview');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'amenities') {
                            ShowDiv('Hot_Amenities', 'Overview');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'map') {
                            ShowDiv('HLocation', 'overview');
                        }
                    }
                }
            }
        }
        function HideWindowPopUp(check) {
            flag = check;
            ModalPop.Hide();
        }

        function ShowDiv(showDivId, hideDivId) {

            //          document.getElementById('Overview').style.display = 'none';
            //          document.getElementById('liOverview').className = '';

            document.getElementById('Hot_Desc').style.display = 'none';
            document.getElementById('liHot_Desc').className = '';


            document.getElementById('Photos').style.display = 'none';
            document.getElementById('liPhotos').className = '';


            document.getElementById('Hot_Amenities').style.display = 'none';
            document.getElementById('liHot_Amenities').className = '';

            document.getElementById('HLocation').style.display = 'none';
            document.getElementById('liHLocation').className = '';
            
            document.getElementById(showDivId).style.display = 'block';

            document.getElementById("li" + showDivId).className = 'active';
            document.getElementById("li" + showDivId).click();

        }

        function NextImage(imageArrayLength) {
            for (var i = 0; i < imageArrayLength; i++) {
                if (i < (imageArrayLength - 1)) {
                    if (document.getElementById('image-' + i).style.display == 'block' || document.getElementById('image-' + i).style.display == '') {
                        document.getElementById('image-' + i).style.display = 'none';
                        if (document.getElementById('image-' + eval(i + 1))) {
                            document.getElementById('image-' + eval(i + 1)).style.display = 'block';
                            //document.getElementById('ImageHeader').innerHTML = 'Hotel Image Number ' + eval(i + 1 + 1);
                            break;
                        }
                    }
                }
            }
        }
        function PrevImage(imageArrayLength) {
            for (var i = 0; i < imageArrayLength; i++) {
                if (i > 0) {
                    if (document.getElementById('image-' + i).style.display == 'block' || document.getElementById('image-' + i).style.display == '') {
                        document.getElementById('image-' + i).style.display = 'none';
                        if (document.getElementById('image-' + eval(i - 1))) {
                            document.getElementById('image-' + eval(i - 1)).style.display = 'block';
                            // document.getElementById('ImageHeader').innerHTML = 'Hotel Image Number ' + eval(i - 1 + 1);
                            break;
                        }
                    }
                }
            }
        }
        function ShowGalleryImage(imageArrayLength, imageIndex) {
            for (var i = 0; i < imageArrayLength; i++) {
                document.getElementById('image-' + i).style.display = 'none';
            }
            document.getElementById('image-' + imageIndex).style.display = 'block';
            //document.getElementById('ImageHeader').innerHTML = 'Hotel Image Number ' + eval(imageIndex+1);
        }
        function NextRoomImage(imageArrayLength) {
            for (var i = 0; i < imageArrayLength; i++) {
                if (i < (imageArrayLength - 1)) {
                    if (document.getElementById('RoomImage-' + i).style.display == 'block' || document.getElementById('RoomImage-' + i).style.display == '') {
                        document.getElementById('RoomImage-' + i).style.display = 'none';
                        if (document.getElementById('RoomImage-' + eval(i + 1))) {
                            document.getElementById('RoomImage-' + eval(i + 1)).style.display = 'block';
                            document.getElementById('RoomImageHeader').innerHTML = 'Room Image Number ' + eval(i + 1 + 1);
                            break;
                        }
                    }
                }
            }
        }
        function PrevRoomImage(imageArrayLength) {
            for (var i = 0; i < imageArrayLength; i++) {
                if (i > 0) {
                    if (document.getElementById('RoomImage-' + i).style.display == 'block' || document.getElementById('RoomImage-' + i).style.display == '') {
                        document.getElementById('RoomImage-' + i).style.display = 'none';
                        if (document.getElementById('RoomImage-' + eval(i - 1))) {
                            document.getElementById('RoomImage-' + eval(i - 1)).style.display = 'block';
                            document.getElementById('RoomImageHeader').innerHTML = 'Room Image Number ' + eval(i - 1 + 1); break;
                        }
                    }
                }
            }
        }
    
    
    </script>

    <script type="text/javascript">

        function SetModifyValues() {
            var rooms = eval(document.getElementById('hRooms').value);
            var adults = document.getElementById('<%=hdnAdults.ClientID %>').value;
            var childs = document.getElementById('<%=hdnChilds.ClientID %>').value;

            var roomAdults = adults.split(',');
            var roomChilds = childs.split(',');
            var childAges = document.getElementById('<%=hdnChildAges.ClientID %>').value.split(',');

        document.getElementById('NoOfRooms').value = rooms;

        ShowRoomDetails();

            document.getElementById('rating').value = document.getElementById('<%=hdnRating.ClientID %>').value;
            var age = eval(0);
            for (i = 1; i <= rooms; i++) {
                for (j = 0; j < roomAdults.length; j++) {
                    if (roomAdults[j].split('-')[0] == i) {
                        document.getElementById('adtRoom-' + i).value = roomAdults[j].split('-')[1];
                    }
                }
                for (k = 0; k < roomChilds.length; k++) {
                    if (roomChilds[k].split('-')[0] == i) {
                        document.getElementById('chdRoom-' + i).value = roomChilds[k].split('-')[1];
                        ShowChildAge(k + 1);
                        var chd = eval(roomChilds[k].split('-')[1]);
                        for (l = 0; l < chd; l++) {
                            document.getElementById('ChildBlock-' + (k + 1) + '-ChildAge-' + (l + 1)).value = childAges[age];
                            age++;
                        }
                    }
                }
            }

        }
        var hSource;
        var init = 'true';
        
        function SelectRoomType(index, x, y, roomType, price, isTourico) {
            var differentRoomType = false;
            if (isTourico == 'True') {
                var roomCode = new Array();
                for (var i = 0; i < eval(document.getElementById('hRooms').value); i++) {
                    if (i != x) {
                        roomCode = document.getElementById('rCode' + index + "-" + i).value.split('|');


                        if (roomType.split('|')[0] != roomCode[0]) {
                            differentRoomType = true;
                            break;
                        }
                    }
                }
            }


            if (document.getElementById('RoomChoice' + x + "Type" + y)) {
                document.getElementById('RoomChoice' + x + "Type" + y).checked = true;
                //document.getElementById('RoomChoice' + x + "Type" + y).checked = 'checked';
            }
            //SameRooms contains request object roomguest indexes with same occupancy having adults count and index i.e. 2-0,2-3,3-1,3-3 etc
            //roomType is truncated with adult count of the room with '#' i.e. roomTypeCode#2
            var sameRooms = "";
            if (document.getElementById('hdnSameRooms') != null && document.getElementById('hdnSameRooms').value.indexOf(',') > 0) {
                sameRooms = document.getElementById('hdnSameRooms').value.split(',');
            }
            
            if (document.getElementById('priceTag' + index + "-" + x) != null) {

                document.getElementById('rCode' + index + "-" + x).value = roomType.split('#')[0];
                if (sameRooms != null) {
                    for (var i = 0; i < sameRooms.length; i++) {
                        if (sameRooms[i].split('-')[0] == roomType.split('#')[1]) {
                            document.getElementById('rCode' + index + "-" + sameRooms[i].split('-')[1]).value = roomType.split('#')[0];
                        }
                    }
                }

                document.getElementById('priceTag' + index + "-" + x).value = eval(price);
            }
            var totalPrice = eval(0);
            var rooms = eval(document.getElementById('hRooms').value);
            //var roomDetails = eval(document.getElementById('rDetails' + index).value);
            var c = eval(y);
            var b = eval(x);
            for (var i = 0; i < rooms; i++) {

                var source = document.getElementById('rSource' + index + "-" + i);

                if (source != null) {// not DOTW(DOTW will be null)
                    hSource = source;
                    //removed by brahmam Reg MultiSelection
//                    if (source.value == "RezLive") {
//                        if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {
//                            if (document.getElementById('RoomChoice' + i + "Type" + y) != null) {
//                                document.getElementById('RoomChoice' + i + "Type" + y).checked = true;
//                                totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + y).value);
//                            }
//                        }
//                    }
                    //Added By brahmam 26.09.2014
                     if (source.value == "HotelBeds" && rooms == 1) {
                        if (init == 'true') {
                            if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {
                                totalPrice += eval(document.getElementById('priceTag' + index + "-" + i).value);
                            }
                        }
                        else {

                            totalPrice = eval(price);
                        }
                    }
                    //Added by brahmam TBOHotel 
                    else if (source.value == "TBOHotel") {
                        var roomComb = document.getElementById('hdnroomComb').value;
                        //FixedCombination means Unique Combination
                        if (roomComb == "FixedCombination") {
                            if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {

                                if (rooms > 1) {
                                    if (x > 0 && i == 0) {
                                        totalPrice = eval(price);
                                    }
                                    if (x == 1) {
                                        if (i == 0) {
                                            b--;
                                            c--;
                                        }
                                        else {
                                            b++;
                                            c++;
                                        }
                                    }
                                    else if (x == 2) {
                                        b--;
                                        c--;
                                    }
                                    else if (x == 3) {
                                        if (i <= 2) {
                                            b--;
                                            c--;
                                        }
                                        else {
                                            b++;
                                            c++;
                                        }
                                    }
                                    if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null) {
                                        document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                        if (init == 'true') {
                                            totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                        }
                                        else {
                                            totalPrice = price;
                                        }
                                    }
                                }
                                else {
                                    if (document.getElementById('RoomChoice' + i + "Type" + (y)) != null) {
                                        document.getElementById('RoomChoice' + i + "Type" + (y)).checked = true;
                                        if (init == 'true') {
                                            totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + (y)).value);
                                        }
                                        else {
                                            totalPrice = price;
                                        }

                                    }
                                }
                            }
                        }
                        //OpenCombination means NON Unique Combination
                        else if (roomComb == "OpenCombination") {
                            if (init == 'true') {
                                if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {
                                    if (document.getElementById('priceTag' + index + "-" + i) != null) {
                                        totalPrice += eval(document.getElementById('priceTag' + index + "-" + i).value);
                                    }
                                }
                            }
                            else {
                                totalPrice = eval(price);
                            }
                        }
                    }
                    //Source Rezlive Added by brahmam MultiSelection purpose 18.09.2015
                         //Condition added for Yartra Source --  source.value=="Yatra" , added by somasekhar on 03/09/2018
                         //Condition added for OYO Source  --  source.value=="OYO" , added by somasekhar on 13/12/2018
                    else if (source.value == "LOH" || source.value == "GTA" || source.value == "RezLive" || source.value == "EET" || source.value == "Agoda" || source.value=="Yatra" || source.value=="OYO") { 
                        if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {

                            if (rooms > 1) {
                                //if (x > 0 && i == 0) {
                                //    totalPrice = eval(price);
                                //}
                                if (x == 1) {
                                    if (i == 0) {
                                        b--;
                                        c--;
                                    }
                                    else {
                                        b++;
                                        c++;
                                    }
                                }
                                else if (x == 2) {
                                    if (i <= 1) {
                                        b--;
                                        c--;
                                    }
                                    else if (i == 2) {
                                        b = (b + i);
                                        c = (c + i);
                                    }
                                    else {
                                        b++;
                                        c++;
                                    }
                                }
                                else if (x == 3) {
                                    if (i <= 2) {
                                        b--;
                                        c--;
                                    }
                                    else if (i == 3) {
                                        b = (b + i);
                                        c = (c + i);
                                    }
                                }
                                if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null) {
                                    document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                    if (init == 'true') {
                                        totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                    }
                                    else {
                                        totalPrice = price;
                                    }
                                }
                            }
                            else {
                                if (document.getElementById('RoomChoice' + i + "Type" + (y)) != null) {
                                    document.getElementById('RoomChoice' + i + "Type" + (y)).checked = true;
                                    if (init == 'true') {
                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + (y)).value);
                                    }
                                    else {
                                        totalPrice = price;
                                    }

                                }
                            }
                        }
                    } // Room selection purpose WST,JAC Source Added by brahmam 20.09.2016
                    else if (source.value == "Miki" || source.value == "WST" || source.value == "JAC") {
                        if (init == 'true') {
                            if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {
                                if (document.getElementById('priceTag' + index + "-" + i) != null) {
                                    totalPrice += eval(document.getElementById('priceTag' + index + "-" + i).value);
                                }
                            }
                        }
                        else {

                            totalPrice = eval(price);
                        }
                    }
                   //added By harish
                    else if (source.value == "GRN") {                      
                        var groupcode = document.getElementById('rGroupCode' + index + x + "-" + y).value;
                        var price = eval(document.getElementById('rPrice' + index + x + "-" + y).value)                       
                        var roomType = document.getElementById('hdnroomType' + index + x + "-" + y).value;                        
                        var noOfTypes = document.getElementById('hdnroomTypesCount').value;       
                      
  var rateKey = document.getElementById('rRateKey' + index + x + "-" + y).value;                   
                        if (roomType == "Bundeled") {
                            if (x == 0)
                            {
                                for (var k = b; k < rooms ; k++) {
                                    completed = true;
                                    for (var j = c; j <= noOfTypes; j++) {
                                        if (completed) {
                                            if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + j).value && price == eval(document.getElementById('rPrice' + index + k + "-" + j).value)) {
                                                    document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                    if (init == 'true') {
                                                        totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                        b++
                                                        break;
                                                    }
                                                    else {
                                                        totalPrice = price;
                                                    }
                                                }

                                            }
                                            else if (j == noOfTypes) {
                                               
                                                for (var l = c; l >= 0 ; l--) {
                                                    if (document.getElementById('RoomChoice' + (k) + "Type" + (l)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + l).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + l).value && price == eval(document.getElementById('rPrice' + index + k + "-" + l).value)) {
                                                            document.getElementById('RoomChoice' + (k) + "Type" + (l)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + l).value);
                                                                b++;
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        } else {
                                            break;
                                        }

                                    }


                                }
                            }                                
                            else if(x==1)
                            {
                                var c = eval(y);
                                var b = eval(x);
                                    if(i==0)
                                    {                                   
                                        for(var k=b-1;k>=0;k--)
                                        {
                                            for(var j=c;j>=0;j--)
                                            {
                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + j).value && price == eval(document.getElementById('rPrice' + index + k + "-" + j).value)) {
                                                        document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                            break;                                                       
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }
                                                    else if (j == 0) {
                                                        var c = eval(y);
                                                        for (var p = c; p <= noOfTypes ; p++) {
                                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (p)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + p).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + p).value && price == eval(document.getElementById('rPrice' + index + i + "-" + p).value)) {
                                                                    document.getElementById('RoomChoice' + (i) + "Type" + (p)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + p).value);
                                                                        b++;
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }

                                                }
                                                else if (j == 0) {
                                                    var c = eval(y);
                                                    for (var p = c; p <= noOfTypes ; p++) {
                                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (p)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + p).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + p).value && price == eval(document.getElementById('rPrice' + index + i + "-" + p).value)) {
                                                                document.getElementById('RoomChoice' + (i) + "Type" + (p)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + p).value);
                                                                    b++;
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var c = eval(y);
                                        completed = true;
                                        for (var f = c; f <= noOfTypes; f++) {
                                            if(completed)
                                            {
                                                if (document.getElementById('RoomChoice' + (i) + "Type" + (f)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + f).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + f).value && price == eval(document.getElementById('rPrice' + index + i + "-" + f).value)) {
                                                        document.getElementById('RoomChoice' + (i) + "Type" + (f)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + f).value);
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }
                                                    else if (f == noOfTypes) {
                                                        var c = eval(y);
                                                        for (var o = c; o >= 0 ; o--) {
                                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (o)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + o).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + o).value && price == eval(document.getElementById('rPrice' + index + i + "-" + o).value)) {
                                                                    document.getElementById('RoomChoice' + (i) + "Type" + (o)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + o).value);
                                                                        b++;
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }

                                                } else if (f == noOfTypes) {
                                                    var c = eval(y);
                                                    for (var o = c; o >= 0 ; o--) {
                                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (o)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + o).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + o).value && price == eval(document.getElementById('rPrice' + index + i + "-" + o).value)) {
                                                                document.getElementById('RoomChoice' + (i) + "Type" + (o)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + o).value);
                                                                    b++;
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }else
                                            {
                                                break;
                                            }
                                        }
                                    
                                    }
                                }
                            else if(x==2)
                            {
                                var c = eval(y);
                                var b = eval(x);
                                completed = true;
                                    var groupcode = document.getElementById('rGroupCode' + index + x + "-" + y).value;
                                    if (i == 0) {
                                        for (var k = b - 2; k >= 0; k--) {
                                            for (var j = c ; j >= 0; j--) {
                                                if(completed)
                                                {
                                                    if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + j).value && price == eval(document.getElementById('rPrice' + index + k + "-" + j).value)) {
                                                            document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }
                                                        else if (j == 0 && totalPrice == 0) {
                                                            var c = eval(y);
                                                            for (var m = c; m <= noOfTypes ; m++) {
                                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + m).value && price == eval(document.getElementById('rPrice' + index + k + "-" + m).value)) {
                                                                        document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                        if (init == 'true') {
                                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                            completed = false;
                                                                            break;
                                                                        }
                                                                        else {
                                                                            totalPrice = price;
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }

                                                    }
                                                    else if (j == 0 && totalPrice == 0) {
                                                        var c = eval(y);
                                                        for (var m = c; m <= noOfTypes ; m++) {
                                                            if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + m).value && price == eval(document.getElementById('rPrice' + index + k + "-" + m).value)) {
                                                                    document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (i == 1) {
                                   
                                        for (var j = noOfTypes; j >= 0; j--) {
                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (j)) != null) {
                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + j).value && price == eval(document.getElementById('rPrice' + index + i + "-" + j).value)) {
                                                    document.getElementById('RoomChoice' + (i) + "Type" + (j)).checked = true;
                                                    if (init == 'true') {
                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + j).value);
                                                        break;
                                                    }
                                                    else {
                                                        totalPrice = price;
                                                    }
                                                }

                                            }                                                                                
                                        }
                                    
                                    }
                                    else if (i > 1) {
                                        var c = eval(y);
                                        for (var j = c; j <= noOfTypes; j++) {
                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (j)) != null) {
                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + j).value && price == eval(document.getElementById('rPrice' + index + i + "-" + j).value)) {
                                                    document.getElementById('RoomChoice' + (i) + "Type" + (j)).checked = true;
                                                    if (init == 'true') {
                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + j).value);
                                                        c++;
                                                        break;
                                                    }
                                                    else {
                                                        totalPrice = price;
                                                    }
                                                }
                                                else if (j == noOfTypes) {
                                                    var c = eval(y);
                                                    for (var m = c; m >= 0 ; m--) {
                                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + m).value && price == eval(document.getElementById('rPrice' + index + i + "-" + m).value)) {
                                                                document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }

                                            }
                                            else if (j == noOfTypes) {
                                                var c = eval(y);
                                                for (var m = c; m >= 0 ; m--) {
                                                    if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + m).value && price == eval(document.getElementById('rPrice' + index + i + "-" + m).value)) {
                                                            document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        totalPrice = eval(price);
                                    }
                                }
                            else
                            {
                                var c = eval(y);
                                var b = eval(x);
                                completed = true;
                                var groupcode = document.getElementById('rGroupCode' + index + x + "-" + y).value;
                                if (i == 0) {
                                    for (var k = b - 3; k >= 0; k--) {
                                        for (var j = c ; j >= 0; j--) {
                                            if (completed) {
                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + j).value && price == eval(document.getElementById('rPrice' + index + k + "-" + j).value)) {
                                                        document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }
                                                    else if (j == 0 && totalPrice == 0) {
                                                        var c = eval(y);
                                                        for (var m = c; m <= noOfTypes ; m++) {
                                                            if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + m).value && price == eval(document.getElementById('rPrice' + index + k + "-" + m).value)) {
                                                                    document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }

                                                }
                                                else if (j == 0 && totalPrice == 0) {
                                                    var c = eval(y);
                                                    for (var m = c; m <= noOfTypes ; m++) {
                                                        if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + m).value && price == eval(document.getElementById('rPrice' + index + k + "-" + m).value)) {
                                                                document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            else {
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ((i == 1)||(i==2)) {
                                    for (var j = noOfTypes; j >= 0; j--) {
                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (j)) != null) {
                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + j).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + j).value && price == eval(document.getElementById('rPrice' + index + i + "-" + j).value)) {
                                                document.getElementById('RoomChoice' + (i) + "Type" + (j)).checked = true;
                                                if (init == 'true') {
                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + j).value);
                                                    break;
                                                }
                                                else {
                                                    totalPrice = price;
                                                }
                                            }

                                        }


                                    }

                                }
                                else if (i > 2) {
                                    var c = eval(y);
                                    for (var l = c; l <= noOfTypes; l++) {
                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (l)) != null) {
                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + l).value && rateKey == document.getElementById('rRateKey' + index + i + "-" + l).value && price == eval(document.getElementById('rPrice' + index + i + "-" + l).value)) {
                                                document.getElementById('RoomChoice' + (i) + "Type" + (l)).checked = true;
                                                if (init == 'true') {
                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + l).value);
                                                    break;
                                                }
                                                else {
                                                    totalPrice = price;
                                                }
                                            }
                                            else if (l == noOfTypes) {
                                                var c = eval(y);
                                                for (var m = c; m >= 0 ; m--) {
                                                    if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + m).value && price == eval(document.getElementById('rPrice' + index + k + "-" + m).value)) {
                                                            document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }
                                        else if (l == noOfTypes) {
                                            var c = eval(y);
                                            for (var m = c; m >= 0 ; m--) {
                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value && rateKey == document.getElementById('rRateKey' + index + k + "-" + m).value && price == eval(document.getElementById('rPrice' + index + k + "-" + m).value)) {
                                                        document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                            completed = false;
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                     
                                    }

                                }
                                else {
                                    totalPrice = eval(price);
                                }
                            }
                        }
                            //For NonBundeled groupcode will be same in all selected rooms                       
                        else if (roomType == "NonBundeled") {
                            var completed = true;                           
                            var groupcode = document.getElementById('rGroupCode' + index + x + "-" + y).value;
                            if(x==0)
                            {                               
                                for (var k = b; k < rooms ; k++) {
                                    completed = true;
                                    for (var j = c; j <= noOfTypes; j++) {
                                        if (completed)
                                        {
                                            if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value) {
                                                    document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                    if (init == 'true') {
                                                        totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                        b++;
                                                        break;
                                                    }
                                                    else {
                                                        totalPrice = price;
                                                    }
                                                }
                                                else if (j == noOfTypes) {
                                                    var c = eval(y);
                                                    for (var j = c; j >= 0 ; j--) {
                                                        if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value) {
                                                                document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                                    b++;
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }

                                            }
                                            else if (j == noOfTypes) {
                                                var c = eval(y);
                                                for (var j = c; j >= 0 ; j--) {
                                                    if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value) {
                                                            document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                                b++;
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        } else {
                                            break;
                                        }
                                           
                                        }                                 
                                
                            }
                            }
                            else if(x==1)
                            {                                                                                       
                                if(i==0)
                                {
                                    var c = eval(y);
                                    var b = eval(x);
                                    for(var k=b-1;k>=0;k--)
                                    {                                     
                                        for(var j=c;j>=0;j--)
                                        {
                                            if (completed) {
                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value) {
                                                        document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }
                                                   else if (j == 0) {
                                                        var c = eval(y);
                                                        for (var m = c; m <= noOfTypes ; m++) {
                                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                                    document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }

                                                }

                                               else if (j == 0) {
                                                    var c = eval(y);                                                  
                                                    for (var m = c; m <= noOfTypes ; m++) {
                                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                                document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                   
                                        }
                                       
                                    }                                
                                else
                                {
                                    var c = eval(y);
                                    for (var j = c; j <= noOfTypes; j++) {
                                        if(completed)
                                        {                                           
                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (j)) != null) {
                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + j).value) {
                                                    document.getElementById('RoomChoice' + (i) + "Type" + (j)).checked = true;
                                                    if (init == 'true') {
                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + j).value);
                                                        break;
                                                    }
                                                    else {
                                                        totalPrice = price;
                                                    }
                                                }
                                                else if (j == noOfTypes) {
                                                    var c = eval(y);
                                                    for (var m = c; m >= 0 ; m--) {
                                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                                document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }

                                            } else if (j == noOfTypes) {
                                                var c = eval(y);
                                                for (var m = c; m >= 0 ; m--) {
                                                    if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                            document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }else
                                        {
                                            break;
                                        }
                                        }
                                    
                                }
                            }
                            else if(x==2)
                            {
                                var c = eval(y);
                                var b = eval(x);
                                var groupcode = document.getElementById('rGroupCode' + index + x + "-" + y).value;
                                if (i == 0) {

                                    for (var k = b - 2; k >= 0; k--) {
                                        for (var j = c ; j >= 0; j--) {
                                            if(completed)
                                            {
                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value) {
                                                        document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }
                                                    else if (j == 0 && totalPrice == 0) {

                                                        var c = eval(y);
                                                        for (var m = c; m <= noOfTypes ; m++) {
                                                            if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value) {
                                                                    document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }

                                                }
                                                else if (j == 0 && totalPrice == 0) {

                                                    var c = eval(y);
                                                    for (var m = c; m <= noOfTypes ; m++) {
                                                        if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value) {
                                                                document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (i == 1) {
                                        for (var j = noOfTypes; j >= 0; j--) {
                                            if (document.getElementById('RoomChoice' + (i) + "Type" + (j)) != null) {
                                                if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + j).value) {
                                                    document.getElementById('RoomChoice' + (i) + "Type" + (j)).checked = true;
                                                    if (init == 'true') {
                                                        totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + j).value);
                                                        break;
                                                    }
                                                    else {
                                                        totalPrice = price;
                                                    }
                                                }

                                            }
                                           
                                            
                                        }
                                    
                                }
                                else if (i > 1) {
                                    var c = eval(y);
                                    for (var m = c; m <= noOfTypes; m++) {
                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                if (init == 'true') {
                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                    c++;
                                                    break;
                                                }
                                                else {
                                                    totalPrice = price;
                                                }
                                            }
                                            else if (m == noOfTypes) {

                                                var c = eval(y);
                                                for (var m = c; m <= noOfTypes ; m--) {
                                                    if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                            document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }
                                        else if (m == noOfTypes) {

                                            var c = eval(y);
                                            for (var m = c; m <= noOfTypes ; m--) {
                                                if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                        document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                            completed = false;
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    

                                }
                                else
                                {
                                    totalPrice = eval(price);
                                }
                            }
                            else
                            {
                                var c = eval(y);
                                var b = eval(x);
                                var groupcode = document.getElementById('rGroupCode' + index + x + "-" + y).value;
                                if (i == 0) {
                                    for (var k = b - 3; k >= 0; k--) {
                                        for (var j = c ; j >= 0; j--) {
                                            if (completed) {
                                                if (document.getElementById('RoomChoice' + (k) + "Type" + (j)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + j).value) {
                                                        document.getElementById('RoomChoice' + (k) + "Type" + (j)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + j).value);
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }
                                                    else if (j == 0 && totalPrice == 0) {
                                                        var c = eval(y);
                                                        for (var m = c; m <= noOfTypes ; m++) {
                                                            if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                                if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value) {
                                                                    document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                    if (init == 'true') {
                                                                        totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                        completed = false;
                                                                        break;
                                                                    }
                                                                    else {
                                                                        totalPrice = price;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }

                                                }
                                                else if (j == 0 && totalPrice == 0) {
                                                    var c = eval(y);
                                                    for (var m = c; m <= noOfTypes ; m++) {
                                                        if (document.getElementById('RoomChoice' + (k) + "Type" + (m)) != null) {
                                                            if (groupcode == document.getElementById('rGroupCode' + index + k + "-" + m).value) {
                                                                document.getElementById('RoomChoice' + (k) + "Type" + (m)).checked = true;
                                                                if (init == 'true') {
                                                                    totalPrice += eval(document.getElementById('rPrice' + index + k + "-" + m).value);
                                                                    completed = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    totalPrice = price;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            else {
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ((i == 1)||(i==2)) {
                                    for (var m = noOfTypes; m >= 0; m--) {
                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                if (init == 'true') {
                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                    break;
                                                }
                                                else {
                                                    totalPrice = price;
                                                }
                                            }

                                        }


                                    }

                                }
                                else if (i > 2) {
                                    var c = eval(y);
                                    for (var m = c; m <= noOfTypes; m++) {
                                        if (document.getElementById('RoomChoice' + (i) + "Type" + (m)) != null) {
                                            if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + m).value) {
                                                document.getElementById('RoomChoice' + (i) + "Type" + (m)).checked = true;
                                                if (init == 'true') {
                                                    totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + m).value);
                                                    break;
                                                }
                                                else {
                                                    totalPrice = price;
                                                }
                                            }
                                            else if (m == noOfTypes) {

                                                var c = eval(y);
                                                for (var p = c; p >= 0 ; p--) {
                                                    if (document.getElementById('RoomChoice' + (i) + "Type" + (p)) != null) {
                                                        if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + p).value) {
                                                            document.getElementById('RoomChoice' + (i) + "Type" + (p)).checked = true;
                                                            if (init == 'true') {
                                                                totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + p).value);
                                                                completed = false;
                                                                break;
                                                            }
                                                            else {
                                                                totalPrice = price;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                        else if (m == noOfTypes) {

                                            var c = eval(y);
                                            for (var p = c; p >= 0 ; p--) {
                                                if (document.getElementById('RoomChoice' + (i) + "Type" + (p)) != null) {
                                                    if (groupcode == document.getElementById('rGroupCode' + index + i + "-" + p).value) {
                                                        document.getElementById('RoomChoice' + (i) + "Type" + (p)).checked = true;
                                                        if (init == 'true') {
                                                            totalPrice += eval(document.getElementById('rPrice' + index + i + "-" + p).value);
                                                            completed = false;
                                                            break;
                                                        }
                                                        else {
                                                            totalPrice = price;
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }


                                }
                                else {
                                    totalPrice = eval(price);
                                }
                            }
                        }
                    }

                }
                else if (source == null) {
                    if (init == 'true') {
                        if (document.getElementById('RoomChoice' + x + "Type" + y).checked) {
                            if (document.getElementById('priceTag' + index + "-" + i) != null) {
                                totalPrice += eval(document.getElementById('priceTag' + index + "-" + i).value);
                            }
                        }
                        //                        else {
                        //                            totalPrice += eval(document.getElementById('priceTag' + index + "-" + i).value);
                        //                        }
                    }
                    else {
                        //document.getElementById('NewRoomChoice' + x + "Type" + y).checked = true;
                        totalPrice = eval(price);
                    }
                }
                if (x < 1) {
                    b++;
                    c++;
                }
            }
            var source = document.getElementById('rSource' + index + "-" + 0);
            if (source != null && source.value == "HotelBeds" && rooms > 1) {
                var roomsSelected = eval(0);
                b = eval(0);
                c = eval(0);
                //document.getElementById('hdnDiffRoomsSel').value = "";
                //RoomSeries also contains all room indexes with adult count i.e. 2-0,2-1,2-2,3-0,3-1,3-3
                var roomSeries = document.getElementById('hdnRoomSeries').value.split('|');
                var sameRooms;
                if (document.getElementById('hdnSameRooms').value.indexOf(',') > 0) {
                    sameRooms = document.getElementById('hdnSameRooms').value.split(',');
                }
                else {
                    if (sameRooms != null && sameRooms.length == 1) {
                        sameRooms = sameRooms.splice(0, 0);
                    }
                }
                //DiffRooms contains room indexes of non-unique room indexes with adult count i.e. 1-0,2-0,3-0
                var diffRooms = document.getElementById('hdnDiffRooms').value.trim().split(',');
                
                //When unique room selection is made
                if (sameRooms.length > 0) {
                    for (var j = 0; j < sameRooms.length; j++) {
                        if (sameRooms[j].length > 0) {
                            var series = roomSeries[sameRooms[j].split('-')[1]].split(',');
                            for (var i = 0; i < series.length; i++) {
                                if (roomType.split('#')[1] == series[i].split('-')[0]) {
                                    if (roomSeries[x].split(',')[i].split('-')[1] == y && diffRooms.indexOf(x) == -1) {
                                        b = sameRooms[j].split('-')[1];
                                        c = series[i].split('-')[1];

                                        if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null) {
                                            document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                            if (init == 'true') {
                                                totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                                roomsSelected++;
                                            }
                                            else {
                                                totalPrice = price;
                                            }
                                        }
                                        break;
                                    }
                                    else if (diffRooms.indexOf(x) >= 0) {//retrieving previous selection for same rooms
                                        b = sameRooms[j].split('-')[1];
                                        c = series[i].split('-')[1];
                                        if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null && document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked) {
                                            //document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                            if (init == 'true') {
                                                totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                                roomsSelected++;
                                            }
                                            else {
                                                totalPrice = price;
                                            }
                                        }
                                        
                                    }
                                }
                                else if (diffRooms.indexOf(x) >= 0) {
                                b = sameRooms[j].split('-')[1];
                                    c = series[i].split('-')[1];

                                    if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null && document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked) {
                                        if (init == 'true') {
                                            totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                            roomsSelected++;                                          
                                        }
                                        else {
                                            totalPrice = price;
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
                //When atleast one single unique room combination is there and non-unique room combination is there
                var added = false;
                if (diffRooms.length > 0 && sameRooms.length > 0) {
                    for (var j = 0; j < diffRooms.length; j++) {
                        if (diffRooms[j].length > 0) {
                            var series = roomSeries[diffRooms[j]].split(',');
                            if (series.indexOf(y) >= 0) {
                                document.getElementById('hdnDiffRoomsSel').value = "";
                            }

                            for (var i = 0; i < series.length; i++) {
                                b = diffRooms[j];
                                c = series[i].split('-')[1];
                                //if (series[i].split('-')[1] == y) {
                                if (diffRooms.indexOf(x) >= 0 && series[i].split('-')[1] == y) {

                                    if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null ) {
                                        document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                        //store the selection
                                        if (document.getElementById('hdnDiffRoomsSel').value.length > 0) {
                                            document.getElementById('hdnDiffRoomsSel').value += "," + b + "-" + c;
                                        }
                                        else {
                                            document.getElementById('hdnDiffRoomsSel').value = b + "-" + c;
                                        }
                                        //                                        if (diffRooms[j].indexOf(b) < 0) {
                                        //                                            document.getElementById('hdnDiffRoomsSel').value = b + "-" + c;
                                        //                                        } else {
                                        //                                            document.getElementById('hdnDiffRoomsSel').value = diffRooms[eval(j) + 1] + "-" + eval(eval(c) + 1);
                                        //                                        }

                                        if (init == 'true') {
                                            totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                            roomsSelected++;
                                        }
                                        else {
                                            totalPrice = price;
                                        }
                                    }
                                    break;
                                }
                                else if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null && document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked) {
                                    //document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                    //store the selection
                                    if (document.getElementById('hdnDiffRoomsSel').value.length > 0) {
                                        document.getElementById('hdnDiffRoomsSel').value += "," + b + "-" + c;
                                    }
                                    else {
                                        document.getElementById('hdnDiffRoomsSel').value = b + "-" + c;
                                    }
                                    //                                        if (diffRooms[j].indexOf(b) < 0) {
                                    //                                            document.getElementById('hdnDiffRoomsSel').value = b + "-" + c;
                                    //                                        } else {
                                    //                                            document.getElementById('hdnDiffRoomsSel').value = diffRooms[eval(j) + 1] + "-" + eval(eval(c) + 1);
                                    //                                        }

                                    if (init == 'true') {
                                        totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                        roomsSelected++;
                                    }
                                    else {
                                        totalPrice = price;
                                    }
                                    break;
                                }
                                else if (roomSeries[diffRooms[j]].indexOf(y) == -1) {
                                    if (document.getElementById('hdnDiffRoomsSel').value.length > 0) {
                                        b = document.getElementById('hdnDiffRoomsSel').value.split('-')[0];
                                        c = document.getElementById('hdnDiffRoomsSel').value.split('-')[1];
                                    } else {
                                        b = diffRooms[j];
                                        c = series[i].split('-')[1];
                                    }

                                    if (roomsSelected <= rooms) {
                                        if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null) {

                                            document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;

                                            if (init == 'true') {
                                                totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                                roomsSelected++;
                                            }
                                            else {
                                                totalPrice = price;
                                            }
                                            added = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else { //When all are unique combinations no non-unique combinations are there i.e. 4 rooms 2-0,2-2,3-0,3-2
                            for (var j = 0; j < sameRooms.length; j++) {
                                if (sameRooms[j].length > 0) {
                                    var series = roomSeries[sameRooms[j].split('-')[1]].split(',');
                                    for (var i = 0; i < series.length; i++) {
                                        if (roomType.split('#')[1] != series[i].split('-')[0]) {
                                            //if (roomSeries[x].split(',')[i].split('-')[1] == y && diffRooms.indexOf(x) == -1) {
                                            b = sameRooms[j].split('-')[1];
                                            c = series[i].split('-')[1];

                                            if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null && document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked) {

                                                if (init == 'true') {
                                                    totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                                }
                                                else {
                                                    totalPrice = price;
                                                }
                                                break;
                                            }

                                            //}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //Normal room selection
                if (totalPrice == 0) {
                    for (var i = 0; i < rooms; i++) {
                        var series = roomSeries[i].split(',');
                        for (var j = 0; j < series.length; j++) {
                            b = i;
                            c = series[j].split('-')[1];
                            if (x == i && y == c) {
                                if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null) {
                                    document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked = true;
                                    if (init == 'true') {
                                        totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                    }
                                    else {
                                        totalPrice = price;
                                    }
                                }
                                break;
                            } 
                            else {
                                if (document.getElementById('RoomChoice' + (b) + "Type" + (c)) != null && document.getElementById('RoomChoice' + (b) + "Type" + (c)).checked) {

                                    if (init == 'true') {
                                        totalPrice += eval(document.getElementById('rPrice' + index + b + "-" + c).value);
                                    }
                                    else {
                                        totalPrice = price;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            
            var discount = eval(0);
            var discType;
//            if (document.getElementById('rSource' + index + "-" + x) == null) {
//                if (document.getElementById('<%=hDOTWDiscountType.ClientID %>').value == "F") { // unnecessary code commented by ziya
//                    discount = eval(document.getElementById('<%=hDOTWDiscount.ClientID %>').value);
//                }
//                else if (document.getElementById('<%=hDOTWDiscountType.ClientID %>').value == "P") {
//                    discount = eval(totalPrice) * (eval(document.getElementById('<%=hDOTWDiscount.ClientID %>').value) / 100);
//                }
//                discType = document.getElementById('<%=hDOTWDiscountType.ClientID %>').value;
//            }
//            else if (document.getElementById('rSource' + index + "-" + x).value == "RezLive") {
//                if (document.getElementById('<%=hRezDiscountType.ClientID %>').value == "F") {
//                    discount = eval(document.getElementById('<%=hRezDiscount.ClientID %>').value);
//                }
//                else if (document.getElementById('<%=hRezDiscountType.ClientID %>').value == "P") {
//                    discount = eval(totalPrice) * (eval(document.getElementById('<%=hRezDiscount.ClientID %>').value) / 100);
//                }
//                discType = document.getElementById('<%=hRezDiscountType.ClientID %>').value;
//            }
//            else if (document.getElementById('rSource' + index + "-" + x).value == "LOH") {
//                if (document.getElementById('<%=hLOHDiscountType.ClientID %>').value == "F") {
//                    discount = eval(document.getElementById('<%=hLOHDiscount.ClientID %>').value);
//                }
//                else if (document.getElementById('<%=hLOHDiscountType.ClientID %>').value == "P") {
//                    discount = (eval(totalPrice)) * (eval(document.getElementById('<%=hLOHDiscount.ClientID %>').value) / 100);
//                }
//                discType = document.getElementById('<%=hLOHDiscountType.ClientID %>').value;
//            }
            
            var priceString = "";
            if (init == 'true') {
                priceString = parseFloat(Math.ceil((eval(Math.ceil(eval(totalPrice)) - discount)))).toFixed(2);
            }
            else {
                priceString = parseFloat(Math.ceil(eval(totalPrice))).toFixed(eval('<%=decimalValue %>'));
            }
            
            if (init == 'true') {
                document.getElementById('TotalSelectedFare' + index).innerHTML = parseFloat(Math.ceil(eval(totalPrice))).toFixed(eval('<%=decimalValue %>'));
            }
            else {
                document.getElementById('TotalSelectedFare' + index).innerHTML = parseFloat(Math.ceil(eval(totalPrice))).toFixed(eval('<%=decimalValue %>'));
            }
            if (discount > 0) {
                document.getElementById('DiscountFare' + index).innerHTML = parseFloat(discount).toFixed(eval('<%=decimalValue %>'));
            }
            if (init == 'true') {
                document.getElementById('TotalFare' + index).innerHTML = parseFloat(Math.ceil((eval(Math.ceil(eval(totalPrice)) - discount)))).toFixed(eval('<%=decimalValue %>'));
            }
            else {

                document.getElementById('TotalFare' + index).innerHTML = parseFloat(Math.ceil((eval(Math.ceil(eval(totalPrice)) - discount)))).toFixed(eval('<%=decimalValue %>')); ;


            }
            document.getElementById('selRoomIndex').value = index + "," + x + "," + y;
            document.getElementById('selTotalFare').value = parseFloat(Math.ceil(eval(totalPrice))).toFixed(eval('<%=decimalValue %>'));
            document.getElementById('roomSelected').value = "true";

        }

        var resultIndex;
        function GetRooms(index, totFare, hotelCode, hotelName) {
           //clicking two times roomdetails need to hide first roomdetails new Added by brahmam 25.08.2016
            if (document.getElementById('maindiv-' + resultIndex) != null) {
                document.getElementById('maindiv-' + resultIndex).innerHTML = '';
                document.getElementById('maindiv-' + resultIndex).style.display = "none";
            }
            resultIndex = index;
            var url = 'RoomDetailsAjax.aspx';
            var paramList = 'index=' + index + '&totFare=' + totFare + '&hotelCode=' + hotelCode;
            document.getElementById('maindiv-' + resultIndex).className = "modalShadow2";
             document.getElementById('maindiv-' + resultIndex).style.display = "block";
//            document.getElementById('maindiv-' + resultIndex).style.left = "250px";
//            document.getElementById('maindiv-' + resultIndex).style.top = "50px";
//            document.getElementById('maindiv-' + resultIndex).style.height = "520px";
//            document.getElementById('maindiv-' + resultIndex).style.position= "absolute";
             document.getElementById('maindiv-' + resultIndex).innerHTML = "<div style='color:#999999; text-align:center;font-size:16px;top:200px;'></br></br></br></br></br></br></br><img src='images/preloader11.gif' /></br>Please wait while loading room details for</br></br></br><b class='primary-color' style='font-size:21px;'>" + hotelName + "</b></div>";
            Ajax.onreadystatechange = ShowRooms;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function ShowRooms() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        var text = Ajax.responseText;
                        document.getElementById('maindiv-' + resultIndex).innerHTML = text;
                        document.getElementById('maindiv-' + resultIndex).className = "modalShadow2";
                        document.getElementById('maindiv-' + resultIndex).style.display = "block";
//                        document.getElementById('maindiv-' + resultIndex).style.left = "200px";
//                        document.getElementById('maindiv-' + resultIndex).style.top = "80px";
//                        document.getElementById('maindiv-' + resultIndex).style.height = "520px";
                        document.getElementById('selRoomIndex').value = resultIndex + "," + 0 + "," + 0;
                        document.getElementById('selTotalFare').value = document.getElementById('TotalSelectedFare' + resultIndex).innerHTML;
                        init = "true";
                        
                        //SetPageLoaded();
                        //ShowModalPopUp(resultIndex);
                    }
                }
            }
        }
        

        var pageLoaded = true;
        function SetPageLoaded() {
            pageLoaded = true;
        }
        window.onload = SetPageLoaded;
        function ShowModalPopUp(id) {

            if (pageLoaded) {
                ModalPop.ControlBox.style.display = "none";
                var modalMessage = document.getElementById('maindiv-' + id).innerHTML;
                modalMessage = modalMessage.replace(/RoomChoice/gi, "NewRoomChoice");
                modalMessage = modalMessage.replace(/rCode/gi, "rCodeNew");
                modalMessage = modalMessage.replace(/priceTag/gi, "priceTagNew");
                modalMessage = modalMessage.replace(/TotalSelectedFare/gi, "TotalSelectedFareNew");
                if (modalMessage.indexOf("DiscountFare") > 0) {
                    modalMessage = modalMessage.replace(/DiscountFare/gi, "DiscountFareNew");
                }
                modalMessage = modalMessage.replace(/TotalFare/gi, "TotalFareNew");
                modalMessage = modalMessage.replace(/roomSelectionError/gi, "roomSelectionErrorNew");
                modalMessage = modalMessage.replace(/continueButton/gi, "continueButtonNew");
                //modalMessage = modalMessage.replace(/fareP/gi, "FareNew");
                //modalMessage = modalMessage.replace(/cancelP/gi, "CancelNew");
                //modalMessage = modalMessage.replace(/showRoomDetP/gi, "ShowRoomDetNew");
                ModalPop.Show({ x: 850, y: 520 }, modalMessage);
                if (document.getElementById('roomSelected').value == "false") {
                    document.getElementById('selRoomIndex').value = id + "," + 0 + "," + 0;
                    document.getElementById('selTotalFare').value = document.getElementById('TotalSelectedFareNew' + id).innerHTML;
                }
            }
            init = 'true';
        }
        function HideMRPopUp(index) {
            if (index) {
                for (var x = 0; x < eval(document.getElementById('hRooms').value); x++) {
                    if (document.getElementById('rCode' + index + "-" + x) != null) {
                        document.getElementById('rCode' + index + "-" + x).value = document.getElementById('rCodeDefault' + index + "-" + x).value;
                    }
                }
            }
            if (document.getElementById('roomSelected') != null) {
                document.getElementById('roomSelected').value = "false";
            }
            //ModalPop.Hide();
            document.getElementById('maindiv-' + resultIndex).innerHTML = '';
            document.getElementById('maindiv-' + resultIndex).style.display = "none";
        }
        var findex;
       // var c = eval(0);
       // var b = eval(0);
        var amenities = '';
        
        function ShowFare(id, index, x, y) {
            
            findex = index;

           
            
            if (document.getElementById(id).style.display != "block") {                
                document.getElementById(id).style.display = "block";                
            }
            else {
                document.getElementById(id).style.display = "none";
            }
            //ShowModalPopUp(findex);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            //ModalPop2.ControlBox.style.display = "none";
            //var modalMessage = "<div style='text-align:left;'>" + document.getElementById(id).innerHTML + "</div>";
            //ModalPop2.Show({ x: 550, y: 300 }, modalMessage);
        }

        function hideFare(id) {
            
            document.getElementById(id).style.display = 'none';
            //ModalPop2.Hide();
            amenities = '';
            //ShowModalPopUp(findex);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            init = 'true';
            if (document.getElementById('divMeal' + rtcode) != null) {
                document.getElementById('divMeal' + rtcode).innerHTML = "";
            }
        }
        var aindex;
        var cid;
        function ShowAmenities(id, index, hotelCode,x,y) {
            aindex = index;
            cid = id;
           
            
            document.getElementById(id).style.display = 'block';
            document.getElementById(id).innerHTML = 'Loading...';
            var paramList = 'Action=getRoomDetail' + '&hotelIndex=' + index + '&hotelCode=' + hotelCode + '&room_amenities=show';
            var url = "HotelDetailAjax.aspx"; 
            Ajax.onreadystatechange = ShowRoomDetail;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            //ModalPop2.ControlBox.style.display = "none";
            //var modalMessage = "<div style='text-align:left;overflow:auto;height:280px'>" + document.getElementById(id).innerHTML + "</div>";
            //ModalPop2.Show({ x: 550, y: 300 }, modalMessage);
            //ShowModalPopUp(aindex);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            
        }

        function ShowRoomDetail() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        var text = Ajax.responseText;
                        document.getElementById(cid).innerHTML = "<a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"hideAmenities('" + cid + "')\">X</a>" + text;
                    }
                }
            }
            //ShowModalPopUp(aindex);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            //init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            init = 'true';
        }

        function ShowRoomAmenities(id, index, x, y) {
            aindex = index;
            cid = id;
            
            document.getElementById(id).style.display = 'block';
            //ShowModalPopUp(aindex);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');            
            
        }

        function hideAmenities(id) {
            document.getElementById(id).style.display = 'none';
            //ModalPop2.Hide();
            //ShowModalPopUp(aindex);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            init = 'true';
        }      
        
               
        
        
        function ShowCancellationDetails(id) {
            if (document.getElementById(id).style.display != "block") {
                document.getElementById(id).style.display = "block";
            }
            else {
                document.getElementById(id).style.display = "none";
            }
        }
        var divIds = '';
        function ShowRoomDetails(hotelCode, cityCode, rtc, source, hotelName, divId) {
            
            rCode = rtc;
            divIds = divId;
            if (document.getElementById(divIds).style.display != "block") {
                var paramList = 'hotelCode=' + hotelCode + '&cityCode=' + cityCode + '&rCode=' + rtc + '&source=' + source + '&hotelName=' + hotelName + '&isCloseRequired=false';
                var url = "RoomDetailsAjax.aspx";
                new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowRoomDetailsComplete });
            }
            else {
                document.getElementById(divIds).style.display = "none";
            }
        }
        function ShowRoomDetailsComplete(response) {
            if (document.getElementById(divIds).style.display != "block") {
                document.getElementById(divIds).innerHTML = response.responseText;
                document.getElementById(divIds).style.display = "block";
            }
            else {
                document.getElementById(divIds).style.display = "none";
            }
        }

        var Ajax;

        var rtcode;
        
//        function CancelRoom(hotelSource, hotelCode, rpt, rtc, price, cityCode, hotelName) {
//            rCode = rtc;
//            var paramList = 'hotelSource=' + hotelSource + '&hotelCode=' + hotelCode + '&ratePlan=' + rpt + '&rCode=' + rtc + '&price=' + price + '&cityCode=' + cityCode + '&hotelName=' + hotelName;
//            var url = "CancelHotelDetailAjax.aspx";
//            document.getElementById("room" + rCode).style.display = "block";
//            document.getElementById("room" + rCode).innerHTML = "Loading...";
//            if (window.XMLHttpRequest) {
//                Ajax = new window.XMLHttpRequest();
//            }
//            else {
//                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
//            }
//            Ajax.onreadystatechange = SetRoom;
//            Ajax.open('POST', url);
//            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//            Ajax.send(paramList);
//        }
//        function SetRoom(response) {
//            if (Ajax.readyState == 4) {
//                if (Ajax.status == 200) {
//                    if (Ajax.responseText.length > 0) {
//                        var text = Ajax.responseText;
//                        document.getElementById("room" + rCode).innerHTML = text;
//                        document.getElementById("room" + rCode).style.display = "block";
//                    }
//                }
//            }
        //        }
        var ind;
        var rtIndex;
        function GetRoomInfo(hotelSource, hotelCode, rtc, price, cityCode, hotelName, index, y) {
            rtcode = rtc;
            ind = index;
            rtIndex = y;
            var paramList = 'hotelSource=' + hotelSource + '&hotelCode=' + hotelCode + '&rCode=' + rtc + '&price=' + price + '&cityCode=' + cityCode + '&hotelName=' + hotelName;
            var url = "CancelHotelDetailAjax.aspx";
//            document.getElementById('divMeal' + rtcode).style.display = "block";
//            document.getElementById('divMeal' + rtcode).innerHTML = "Loading...";
            //ShowModalPopUp(index);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            if (window.XMLHttpRequest) {
                Ajax = new window.XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }
            Ajax.onreadystatechange = SetRoomInfo;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }
        function SetRoomInfo(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var text = Ajax.responseText;
                        
                        var index = eval(text.indexOf('~'));
                        if (index > 0) {
                            var amenity = text.substring(index + 1, text.indexOf('@'));
                            if (amenity.substring(0, amenity.indexOf('-')) == rtcode) {
                                amenity = amenity.replace(rtcode + '-', '');
                                var vals = amenity.split('-');
                                var para = document.createElement("p");
                                var bold = document.createElement("b");
                                for (var i = 0; i < vals.length; i++) {
                                    bold.appendChild(document.createTextNode(vals[i]));
                                    bold.appendChild(document.createElement("br"));
                                }
                                para.appendChild(bold);
                                para.style.color = "red";
                                document.getElementById('divMeal' + rtcode).appendChild(para);                                
                            }
                        }
                        ShowFare('fareP' + ind + rtIndex, ind, ind, rtIndex);
                        //ShowModalPopUp(ind);
                    }                    
                }
            }           
        }
        
        
        function GetRoomCancelInfo(hotelSource, hotelCode,  rtc, price, cityCode, hotelName, index) {
            rtcode = rtc;
            ind = index;
            var rtCode = rtc.replace('+', '%2B'); //Added by chandan on @@@ 02082016

            var paramList = 'hotelSource=' + hotelSource + '&hotelCode=' + hotelCode + '&rCode=' + rtCode + '&price=' + price + '&cityCode=' + cityCode + '&hotelName=' + hotelName;
            var url = "CancelHotelDetailAjax.aspx";
            document.getElementById("room" + rtcode).style.display = "block";
            document.getElementById("room" + rtcode).innerHTML = "Loading...";
            //ModalPop2.ControlBox.style.display = "none";
            //var modalMessage =  document.getElementById("room" + rCode).innerHTML ;

            //ModalPop2.Show({ x: 350, y: 200 }, modalMessage);

            //ShowModalPopUp(index);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            
            if (window.XMLHttpRequest) {
                Ajax = new window.XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }
            Ajax.onreadystatechange = SetRoomCancelInfo;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);            
        }
        function SetRoomCancelInfo(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var text = Ajax.responseText;
                        
                        document.getElementById("room" + rtcode).style.display = "block";
                        document.getElementById("room" + rtcode).innerHTML = text;
                        //ShowModalPopUp(ind);
                    }
                }
            }
            
            
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            init = "true";
        }
        function hideCP(id) {
            var div = document.getElementById(id);

            div.style.display = 'none';
            //ModalPop2.Hide();
            //ShowModalPopUp(ind);
            var itemIndex = document.getElementById('selRoomIndex').value.split(',')[0];
            var roomIndex = document.getElementById('selRoomIndex').value.split(',')[1];
            var resultIndex = document.getElementById('selRoomIndex').value.split(',')[2];
            init = 'false';
            //SelectRoomType(itemIndex, roomIndex, resultIndex, document.getElementById('rCode' + itemIndex + "-" + roomIndex).value, document.getElementById('selTotalFare').value, 'false');
            //document.getElementById('TotalSelectedFare' + itemIndex).innerHTML = document.getElementById('selTotalFare').value;
            init = 'true';
        }
        function show(id) {

        var div = document.getElementById(id);

            div.style.visibility = 'visible';
        }
        function hide(id) {
            var div = document.getElementById(id);

            div.style.visibility = 'hidden';
        }
        function searchFilter() {
            document.getElementById('<%=ApplyFilter.ClientID %>').value = "true";
            document.getElementById('<%=hdnSubmit.ClientID %>').value = "";
            document.forms[0].submit();
            return false;            
        }

        function ClearFilters() {
            document.getElementById('<%=ApplyFilter.ClientID %>').value = "false";
            document.forms[0].submit();
            return false;
        }

        function ShowPage(pageNo) {
            //$('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=Change.ClientID %>').value = "true";
            document.getElementById('<%=hdnSubmit.ClientID %>').value = "";
            $(document.forms[0]).submit();
        }
        function submitForm(index, y, hcode) {            
            var rooms = eval(document.getElementById('hRooms').value);
            var discount;
            var cotsPopupCount = 0;
            document.getElementById('continueButton' + index).innerHTML = '<b>Please wait...</b>';
            if (rooms <= 1) {

                document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = "";
                for (i = 0; i < rooms; i++) {
                    for (j = 0; j < y; j++) {
                        var radio = document.getElementById('RoomChoice' + i + 'Type' + j);
                        var source = document.getElementById('rSource' + index + '-' + i);
                        var price = document.getElementById('priceTag' + index + '-' + i).value;

                        if (radio != null && (radio.checked)) {
                            if (source != null && source.value == "RezLive") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                                discount = document.getElementById('<%=hRezDiscountType.ClientID %>').value;
                            }
                            else if (source != null && source.value == "LOH") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                                discount = document.getElementById('<%=hLOHDiscountType.ClientID %>').value;
                            }
                            //Added by brahmam 26.09.2014
                            else if (source != null && source.value == "HotelBeds") {
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + document.getElementById('rCode' + index + '-' + i).value;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = document.getElementById('rCode' + index + '-' + i).value;
                                    }
                                }
                                discount = document.getElementById('<%=hHBedsDiscountType.ClientID %>').value;

                            }
                            else if (source != null && source.value == "TBOHotel") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            //Added by brahmam 17.11.2014
                            else if (source != null && source.value == "GTA") {
                            if (cotsPopupCount == 0) {
                                var noOfCots = document.getElementById('NoOfCots' + index + i + '-' + j).value;
                                var SharingBedding = document.getElementById('SharingBedding' + index + i + '-' + j).value;
                                var x = true;
                                if ((parseInt(noOfCots) > 0) && SharingBedding == "True") {
                                    x = confirm("At this hotel additional bed is not provided in the room,child will need to share the existing bedding,if you want to guarantee a separate bed,please click on the Cancel button and adjust your search to a Triple room,or click OK to Continue. \n\n Please note:Cots(s) will be requested at the hotel,however cots are not guaranted and are subject to availability at check-in Also,maximum age for cot is 2 years old.? ");
                                }
                                else if (parseInt(noOfCots) > 0) {
                                    x = confirm("Please note:Cots(s) will be requested at the hotel,however cots are not guaranted and are subject to availability at check-in Also,maximum age for cot is 2 years old.?");
                                }
                                else if (SharingBedding == "True") {
                                    x = confirm("At this hotel additional bed is not provided in the room,child will need to share the existing bedding,if you want to guarantee a separate bed,please click on the Cancel button and adjust your search to a Triple room,or click OK to Continue.?");
                                }
                                if (!x) {
                                    document.getElementById('continueButton' + index).innerHTML = '<b>Book Now</b>';
                                    return false;
                                }
                                cotsPopupCount = 1;
                            }
                                
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            else if (source != null && source.value == "Miki") {
                                
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            //added by brahmam for get roomindex of selected item
                            else if (source != null && source.value == "WST") {

                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            //added by brahmam for get roomindex of selected item
                            else if (source != null && source.value == "JAC") {

                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            else if (source != null && source.value == "EET") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                                //Added by brahmam For Agoda Purpose
                            else if (source != null && source.value == "Agoda") {

                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                                // Added by somasekhar on 03/09/2018 for Yatra
                                 else if (source != null && source.value == "Yatra") {
                              // debugger;
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            //
 else if (source != null && source.value == "GRN") {

                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                                  // Added by somasekhar on 13/12/2018 for OYO
                                 else if (source != null && source.value == "OYO") {                               
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            //
                            else {
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + document.getElementById('rCode' + index + '-' + i).value;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = document.getElementById('rCode' + index + '-' + i).value;
                                    }
                                }
                                discount = document.getElementById('<%=hDOTWDiscountType.ClientID %>').value;
                            }
                        }
                    }                    
                }

                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value == "" && document.getElementById('rCode0-0') != null && document.getElementById('rCode0-1') != null) {
                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = document.getElementById('rCode0-0').value + ',' + document.getElementById('rCode0-1').value;
                }

                //var selrooms = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.split(',');

                document.getElementById('<%=hdnSubmit.ClientID %>').value = "Book";
                document.getElementById('<%=hotelCode.ClientID %>').value = index;
                document.getElementById('<%=roomCode.ClientID %>').value = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value;
                document.getElementById('<%=hdnDiscountType.ClientID %>').value = discount;

                document.forms[0].submit();

            }
            else if (rooms > 1) {
            document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = "";
                for (i = 0; i < rooms; i++) {
                    for (j = 0; j < y; j++) {
                        var radio = document.getElementById('RoomChoice' + i + 'Type' + j);
                        var source = document.getElementById('rSource' + index + '-' + i);
                        var price = document.getElementById('priceTag' + index + '-' + i).value;

                        if (radio != null && (radio.checked)) {
                            if (source != null && source.value == "RezLive") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                                discount = document.getElementById('<%=hRezDiscountType.ClientID %>').value;
                            }
                            else if (source != null && source.value == "LOH") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                                discount = document.getElementById('<%=hLOHDiscountType.ClientID %>').value;
                            }
                            //Added by brahmam 26.09.2014
                            else if (source != null && source.value == "HotelBeds") {
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + document.getElementById('rCode' + index + '-' + i).value;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = document.getElementById('rCode' + index + '-' + i).value;
                                    }
                                }
                                discount = document.getElementById('<%=hHBedsDiscountType.ClientID %>').value;
                            }
                            else if (source != null && source.value == "TBOHotel") {
                                price = document.getElementById('rPrice' + index + i + '-' + j).value;
                                if (document.getElementById('rCode' + index + '-' + i) != null) {
                                    if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                    }
                                }
                            }
                            //Added by brahmam 17.11.2014
                            else if (source != null && source.value == "GTA") {
                            if (cotsPopupCount == 0) {
                                var noOfCots = document.getElementById('NoOfCots' + index + i + '-' + j).value;
                                var SharingBedding = document.getElementById('SharingBedding' + index + i + '-' + j).value;
                                var x = true;
                                if ((parseInt(noOfCots) > 0) && SharingBedding == "True") {
                                    x = confirm("At this hotel additional bed is not provided in the room,child will need to share the existing bedding,if you want to guarantee a separate bed,please click on the Cancel button and adjust your search to a Triple room,or click OK to Continue. \n\n Please note:Cots(s) will be requested at the hotel,however cots are not guaranted and are subject to availability at check-in Also,maximum age for cot is 2 years old.? ");
                                }
                                else if (parseInt(noOfCots) > 0) {
                                    x = confirm("Please note:Cots(s) will be requested at the hotel,however cots are not guaranted and are subject to availability at check-in Also,maximum age for cot is 2 years old.?");
                                }
                                else if (SharingBedding == "True") {
                                    x = confirm("At this hotel additional bed is not provided in the room,child will need to share the existing bedding,if you want to guarantee a separate bed,please click on the Cancel button and adjust your search to a Triple room,or click OK to Continue.?");
                                }
                                if (!x) {
                                    document.getElementById('continueButton' + index).innerHTML = '<b>Book Now</b>';
                                    return false;
                                }
                                cotsPopupCount = 1;
                            }
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;
                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }

                        //added by chandan for get roomcode of selected items
                        else if (source != null && source.value == "Miki") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;

                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
                        //added by brahmam for get multiroomindex of selected item
                        else if (source != null && source.value == "WST") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;

                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
                        //added by brahmam for get multiroomindex of selected item
                        else if (source != null && source.value == "JAC") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;

                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
                        else if (source != null && source.value == "EET") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;
                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
                            //Added By Agoda purpose 08.02.2018
                        else if (source != null && source.value == "Agoda") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;

                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                            }
                                //Added by somasekhar on 03/09/2018 
                                else if (source != null && source.value == "Yatra") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;

                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
 //Added By Harish,GRN purpose 03-Sep-2018
                        else if (source != null && source.value == "GRN") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;

                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
                                   //Added by somasekhar on 13/12/2018 For OYO source
                                else if (source != null && source.value == "OYO") {
                            price = document.getElementById('rPrice' + index + i + '-' + j).value;
                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + j;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = j;
                                }
                            }
                        }
                                //=============================================
                        else {
                            if (document.getElementById('rCode' + index + '-' + i) != null) {
                                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.length > 0) {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value += ',' + document.getElementById('rCode' + index + '-' + i).value;
                                }
                                else {
                                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = document.getElementById('rCode' + index + '-' + i).value;
                                }
                            }
                            discount = document.getElementById('<%=hDOTWDiscountType.ClientID %>').value;
                        }
                    }
                }
            }
    
                if (document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value == "") {
                    document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value = document.getElementById('rCode0-0').value + ',' + document.getElementById('rCode0-1').value;
                }
                

                //var selrooms = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value.split(',');

                document.getElementById('<%=hdnSubmit.ClientID %>').value = "Book";
                document.getElementById('<%=hotelCode.ClientID %>').value = index;
                document.getElementById('<%=roomCode.ClientID %>').value = document.getElementById('ctl00_cphTransaction_DataList1_ctl0' + index + '_hSelRooms').value;

                //                if (selrooms.length > rooms) {
                //                    alert("You need to select only " + eval(document.getElementById('hRooms').value) + " rooms for booking");
                //                    return false;
                //                }
                //                 if (selrooms.length < rooms) {
                //                    alert("You need to select atleast " + eval(document.getElementById('hRooms').value) + " rooms for booking");
                //                    return false;
                //                }
                {
                    window.location.href = 'HotelReview.aspx?rCode=' + document.getElementById('<%=roomCode.ClientID %>').value + '&hCode=' + index + '&dt=' + discount;
                }
            }
            

        }
        function selectAll() {
            var all = document.getElementById('<%=chkAll.ClientID %>').checked;
            if (all) {
                document.getElementById('<%=chkOne.ClientID %>').checked = true;
                document.getElementById('<%=chkTwo.ClientID %>').checked = true;
                document.getElementById('<%=chkThree.ClientID %>').checked = true;
                document.getElementById('<%=chkFour.ClientID %>').checked = true;
                document.getElementById('<%=chkFive.ClientID %>').checked = true;
            } else {
            document.getElementById('<%=chkOne.ClientID %>').checked = false;
            document.getElementById('<%=chkTwo.ClientID %>').checked = false;
            document.getElementById('<%=chkThree.ClientID %>').checked = false;
            document.getElementById('<%=chkFour.ClientID %>').checked = false;
            document.getElementById('<%=chkFive.ClientID %>').checked = false;
            }
        }

        function check(control) {
            if (!control.checked) {
                document.getElementById('<%=chkAll.ClientID %>').checked = false;
            }
            
        }

                   
    </script>

    <script type="text/javascript">
        function IntDom(showDivId, id) {

            document.getElementById(showDivId).style.display = "block";
            var el = document.getElementById(id);
            document.getElementById(id).focus();
        }
        function markout(textBox, txt) {
            if (textBox.value == "") {
                textBox.value = txt;
            }
        }
        function markin(textBox, txt) {
            if (textBox.value == txt) {
                textBox.value = "";
            }
        }
        function getFile(url, passData) {
            if (window.XMLHttpRequest) {
                AJAX = new XMLHttpRequest();
            }
            else {
                AJAX = new ActiveXObject("Microsoft.XMLHTTP");
            }
            if (AJAX) {
                AJAX.open("POST", url, false);
                AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                AJAX.send(passData);
                return AJAX.responseText;
            }
            else {
                return false;
            }
        }
        function getStates_Hotel(sQuery) {

            var paramList = 'searchKey=' + sQuery;

            if (document.getElementById('domesticHotel').checked == true) {

                paramList += '&requestSource=' + "HotelSearchDomestic";
            }
            else {
                paramList += '&requestSource=' + "HotelSearchInternational";
            }
            var url = "CityAjax.aspx";
            var arrayStates = "";
            var faltoo = getFile(url, paramList);
            arrayStates = faltoo.split('/');
            if (arrayStates[0] != "") {
                for (var i = 0; i < arrayStates.length; i++) {
                    if (arrayStates[i].split(',')[2].length > 0) {
                        arrayStates[i] = [arrayStates[i].split(',')[1] + ', ' + arrayStates[i].split(',')[2] + ', ' + arrayStates[i].split(',')[3], arrayStates[i]];
                    }
                    else {
                        arrayStates[i] = [arrayStates[i].split(',')[1] + ', ' + arrayStates[i].split(',')[3], arrayStates[i]];
                    }
                }

                return arrayStates;
            }
            else return (false);
        }

        //for added helper in source destination lookup

        function autoCompInit() {
            // Instantiate third data source        
            oACDS3 = new YAHOO.widget.DS_JSFunction(getStates_Hotel); // temporarily autosearch deactivated
            // Instantiate third auto complete        
            oAutoComp3 = new YAHOO.widget.AutoComplete('city', 'statescontainer3', oACDS3);
            oAutoComp3.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp3.queryDelay = 0;
            oAutoComp3.minQueryLength = 3;
            oAutoComp3.useIFrame = true;
            oAutoComp3.useShadow = true;

            oAutoComp3.formatResult = function(oResultItem, sQuery) {
                document.getElementById('statescontainer3').style.display = "block";
                var toShow = oResultItem[1].split(',');
                var sMarkup;
                if (toShow[2].length > 0) {
                    sMarkup = toShow[1] + ',' + toShow[2] + ',' + toShow[3];
                }
                else {
                    sMarkup = toShow[1] + ',' + toShow[3];
                }
                //var aMarkup = ["<li>", sMarkup, "</li>"]; 
                var aMarkup = [sMarkup];
                return (aMarkup.join(""));
            };
            oAutoComp3.itemSelectEvent.subscribe(itemSelectHandler3);
        }
        var itemSelectHandler3 = function(sType2, aArgs2) {

            YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
            var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
            var city = oMyAcInstance2[1].split(',');
            document.getElementById('CityCode').value = city[0];
            document.getElementById('CountryName').value = city[3];
            document.getElementById('city').value = city[1] + ',' + city[3];
            document.getElementById('statescontainer3').style.display = "none";
            var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
            var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
        };
        YAHOO.util.Event.addListener(this, 'load', autoCompInit); //temporarily commented
    </script>

   


    <!--[if IE]>
	<style type="text/css">
	a.button_or span {
		line-height:31px;
	}	    
    .ie_r1 {  margin-top:16px;  }  
  .wrap ul li a   { color:#2fb5ea; text-decoration:none; padding-right:9px; background:url(images/marker.png) no-repeat right 4px;      }
.wrap ul li a:hover   {  background:url(images/marker.png) no-repeat right -13px; text-decoration:none; padding-right:11px;      }  
    		</style>
	<![endif]-->

    <script type="text/javascript">
//        $(document).ready(function() {
//            $('.button_or1').showHide({
//                speed: 300,  // speed you want the toggle to happen	
//                easing: '',  // the animation effect you want. Remove this line if you dont want an effect and if you haven't included jQuery UI
//                changeText: 0, // if you dont want the button text to change, set this to 0
//                showText: 'Modify Search', // the button text to show when a div is closed
//                hideText: 'Modify Search' // the button text to show when a div is open
//                //alert(document.getElementById('btnSearch'));			 
//            });
//        });
//        
//        $(document).ready(function() {
//            $(".button_or1").click(function() {
//                $(".opendv").toggleClass("closedv");
//            });
//        });
    </script>

    <script src="Scripts/Jquery/showHide.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ShowChildAge(number) {
            var childCount = eval(document.getElementById('chdRoom-' + number).value);
            var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);
            if (eval(document.getElementById('chdRoom-1').value) > 0 || eval(document.getElementById('chdRoom-2').value) > 0 || eval(document.getElementById('chdRoom-3').value) > 0 || eval(document.getElementById('chdRoom-4').value) > 0) {
                document.getElementById('childDetails').style.display = 'block';
            }
            else {
                document.getElementById('childDetails').style.display = 'none';
            }
            if (childCount > PrevChildCount) {
                document.getElementById('ChildBlock-' + number).style.display = 'block';
                for (var i = (PrevChildCount + 1); i <= childCount; i++) {
                    document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                }
            }
            else if (childCount < PrevChildCount) {
                if (childCount == 0) {
                    document.getElementById('ChildBlock-' + number).style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-1').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-2').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                }
                else {
                    for (var i = PrevChildCount; i > childCount; i--) {
                        if (i != 0) {
                            document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                            document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                        }
                    }
                }
            }
            document.getElementById('PrevChildCount-' + number).value = childCount;
        }

        function ShowRoomDetails() {
            //var f=document.getElementById;
            var count = eval(document.getElementById('NoOfRooms').value);
            var prevCount = eval(document.getElementById('PrevNoOfRooms').value);

            if (count > prevCount) {
                for (var i = (prevCount + 1); i <= count; i++) {

                    document.getElementById('room-' + i).style.display = 'block';
                    document.getElementById('adtRoom-' + i).value = '1';
                    document.getElementById('chdRoom-' + i).value = '0';
                    document.getElementById('PrevChildCount-' + i).value = '0';
                }
            }
            else if (count < prevCount) {
                //alert('count' +count+',prev '+prevCount);
                //alert(document.getElementById('PrevNoOfRooms').value);

                for (var i = prevCount; i > count; i--) {
                    document.getElementById('room-' + i).style.display = 'none';
                    document.getElementById('adtRoom-' + i).value = '1';
                    document.getElementById('chdRoom-' + i).value = '0';
                    /*document.getElementById('PrevChildCount-' + i).value = '0';
                    document.getElementById('ChildBlock-' + i).style.display = 'none';
                    document.getElementById('ChildBlock-' + i + '-ChildAge-1').value = '-1';
                    document.getElementById('ChildBlock-' + i + '-ChildAge-2').value = '-1';*/

                }
            }
            document.getElementById('PrevNoOfRooms').value = count;
        }

        

        function HotelSearch() {
            if (CheckCity() && checkDates() && checkRooms()) {
                $('SearchResultLoad').context.styleSheets[0].display = "block";
                document.getElementById('<%=hdnSubmit.ClientID %>').value = "search"
                return true;
            }
            else {
                return false;
            }
        }

        function CheckCity() {
            var submit = true;
            var city = document.getElementById('city');

            if (city.value != "Enter city name") {
                submit = true;
                //document.getElementById('searchCity').innerHTML = city.value;
            }
            else {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Enter a City";
                submit = false;
            }
            return submit;
        }

        function checkDates() {
            document.getElementById('errMess').style.display = "none";


            var date1 = document.getElementById('<%= CheckIn.ClientID %>').value;
            var date2 = document.getElementById('<%= CheckOut.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
                return false;
            }
            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check In Date";
                return false;
            }
            var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            if (todaydate.getTime() > cInDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check In Date should be greater than equal to todays date";
                return false;
            }

            if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check Out Date";
                return false;
            }
            var retDateArray = date2.split('/');

            // checking if date2 is valid	
            if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check Out Date";
                return false;
            }
            var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            if (todaydate.getTime() > cOutDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check Out Date should be greater than equal to todays date";
                return false;
            }

            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckOut date should be greater than  or equal to CheckIn date";
                return false;
            }

            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckIn date and CheckOut date could not be same";
                return false;
            }


            return true;
        }

        function checkRooms() {
            var submit = true;
            var rooms = document.getElementById('NoOfRooms');
            var room = parseInt(rooms.value);

            for (var k = 1; k <= room; k++) {
                if (submit == false) {
                    break;
                }
                if (document.getElementById("adtRoom-" + k).value == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Room " + k + "Details!!";
                    submit = false;
                }

                if (document.getElementById("chdRoom-" + k).value > 0) {
                    for (var j = 1; j <= document.getElementById("chdRoom-" + k).value; j++) {
                        var str = "ChildBlock-" + k + "-ChildAge-" + j;
                        //                            str += parseInt(k);
                        //                            str += parseInt(j);
                        if (document.getElementById(str).value < 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please Enter Child-" + (parseInt(j)) + "  Age Details in Room " + (parseInt(k)) + "!!";
                            submit = false;
                            break;
                        }

                    } //for inner for
                } //end if
            }  //end for

            return submit;
        }

        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCalendar1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
        }
        var departureDate = new Date();
        function showCalendar2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();
//When We Select Departure Date Return date Calenadr Shoulb Be Opened Automatically
            showCalendar2();

        }
        function setDate2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 1) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                return false;
            }
            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);

        function IntDom(showDivId, id) {

            document.getElementById(showDivId).style.display = "block";
            var el = document.getElementById(id);
            document.getElementById(id).focus();
        }
        function markout(textBox, txt) {
            if (textBox.value == "") {
                textBox.value = txt;
            }
        }
        function markin(textBox, txt) {
            if (textBox.value == txt) {
                textBox.value = "";
            }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 46)) {
                return false;
            }
            return true;
        }
    </script>
    <script type="text/javascript">
        function selectBox(id) {

            var checkBoxId = id;
            var getResultID = checkBoxId.split('-');
            var pageno=<%=pageNo%>;
            var records=<%=pageNo-1%>*<%=recordsPerPage%>; 
            if(pageno!=1)
            {
                var resultId = eval(getResultID[1]) +eval(records) ;
            }
            else{
                var resultId = getResultID[1] ;
            }
           
            var hdnArray = '';
            var hdnField = document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail');

            if (document.getElementById(checkBoxId).checked) {
                if (hdnField.value != "") {
                    hdnArray = hdnField.value.split(',');

                    if (hdnArray.indexOf('' + resultId) == -1) hdnField.value += ',' + resultId;

                }
                else {
                    hdnField.value = resultId;
                }
            }
            else {
                var getNewValues = '';
                var getUnselectValue = hdnField.value.split(',');

                if (getUnselectValue.length > 0) {

                    for (var k = 0; k < getUnselectValue.length; k++) {
                        if (getUnselectValue[k] == resultId)
                            getUnselectValue[k] = "";
                        else {
                            if (getNewValues != '')
                                getNewValues += ',' + getUnselectValue[k];
                            else
                                getNewValues = '' + getUnselectValue[k];
                        }
                    }
                }

                chkAll.checked = false;//Uncheck the Select All checkbox

                document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
                if (getNewValues != '') {
                    document.getElementById('<%=hdnCheckboxEmail.ClientID %>').value = getNewValues;
                }

            }
        }
        function SelectResult() {
            if (document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value.length > 0) {
                HideEmailDiv('downloadBlock');
                window.location.href = "ExcelDownload.aspx?ids=" + document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value + "&markup=" + (document.getElementById('txtDownloadMarkup').value.trim().length > 0 ? document.getElementById('txtDownloadMarkup').value : "0");
                document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
            }
            else {
                document.getElementById('emailStatus').innerHTML = 'Please select at least one Itinerary';
                document.getElementById('emailStatus').style.display = 'block';
            }
        }
        
    </script>
  <%--  <script type="text/javascript" src="Scripts/Jquery/ddaccordion.js"></script>

    <script type="text/javascript" src="Scripts/Jquery/accordion_ui.js"></script>

    <!-- FANCY BOX START -->

    <script type="text/javascript" src="Scripts/Jquery/jquery.fancybox.js?v=2.1.3"></script>

    <link rel="stylesheet" type="text/css" href="Scripts/Jquery/jquery.fancybox.css?v=2.1.2" media="screen" />

    <script type="text/javascript">
        $(document).ready(function() {
            /*
            *  Simple image gallery. Uses default settings
            */

            $('.fancybox').fancybox();

            /*
            *  Different effects
            */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,

                openEffect: 'none',

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect: 'elastic',
                openSpeed: 150,

                closeEffect: 'elastic',
                closeSpeed: 150,

                closeClick: true,

                helpers: {
                    overlay: null
                }
            });

            /*
            *  Button helper. Disable animations, hide close button, change title type and content
            */

            $('.fancybox-buttons').fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    buttons: {}
                },

                afterLoad: function() {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
            *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
            */

            $('.fancybox-thumbs').fancybox({
                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,
                arrows: false,
                nextClick: true,

                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });

            /*
            *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
				    openEffect: 'none',
				    closeEffect: 'none',
				    prevEffect: 'none',
				    nextEffect: 'none',

				    arrows: false,
				    helpers: {
				        media: {},
				        buttons: {}
				    }
				});

            /*
            *  Open manually
            */

            $("#fancybox-manual-a").click(function() {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function() {
                $.fancybox.open({
                    href: 'iframe.html',
                    type: 'iframe',
                    padding: 5
                });
            });

            $("#fancybox-manual-c").click(function() {
                $.fancybox.open([
					{
					    href: '1_b.jpg',
					    title: 'My title'
					}, {
					    href: '2_b.jpg',
					    title: '2nd title'
					}, {
					    href: '3_b.jpg'
					}
				], {
				    helpers: {
				        thumbs: {
				            width: 75,
				            height: 50
				        }
				    }
				});
            });


        });
    </script>--%>

    <!-- FANCY BOX CLOSED -->
    <%
        if (request == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    %>
    
    <input type="hidden" id="selRoomIndex" name="selRoomIndex" />
    <input type="hidden" id="selTotalFare" name="selTotalFare" />
    <input type="hidden" id="roomSelected" name="roomSelected" value="false" />
    <asp:HiddenField ID="hDOTWDiscount" runat="server" Value=""/>
    <asp:HiddenField ID="hDOTWDiscountType" runat="server" Value=""/>
    <asp:HiddenField ID="hRezDiscount" runat="server" Value="0"/>
    <asp:HiddenField ID="hRezDiscountType" runat="server" Value="" />
    <asp:HiddenField ID="hLOHDiscount" runat="server" Value="0"/>
    <asp:HiddenField ID="hLOHDiscountType" runat="server" Value="" />
    <asp:HiddenField ID="hHBedsDiscount" runat="server" Value="0"/>
    <asp:HiddenField ID="hHBedsDiscountType" runat="server" Value="" />

       <asp:HiddenField ID="hdnCheckboxEmail" runat="server" Value="" />
        
        <div style=" border: solid 1px #ccc" class="search_rpt1">
           

                     <span style="display: none;"><dfn>
                <input type="radio" id="domesticHotel" name="hotelSearchArea" onclick="javascript:IntDomChecked()"
                    checked="checked" value="1" />
            </dfn><em>Domestic Search</em> </span><span style="display: none;"><dfn>
                <input type="radio" id="intlHotel" name="hotelSearchArea" onclick="javascript:IntDomChecked()"
                    value="0" />
            </dfn><em>International</em> </span>
            <asp:HiddenField ID="hdnSubmit" runat="server" />
            <asp:HiddenField ID="ApplyFilter" runat="server" Value="" />
            <asp:HiddenField ID="PageNoString" runat="server" />
            <asp:HiddenField ID="Change" runat="server" Value="false" />
            <input type="hidden" id="hRooms" name ="hRooms" value="<%=request.NoOfRooms %>" />
            <asp:HiddenField ID="hotelCode" runat="server" />
            <asp:HiddenField ID="roomCode" runat="server" />
            <asp:HiddenField ID="ratePlanCode" runat="server"  />
            <asp:HiddenField ID="hdnAdults" runat="server"  />
            <asp:HiddenField ID="hdnChilds" runat="server"  />
            <asp:HiddenField ID="hdnChildAges" runat="server"  />
            <asp:HiddenField ID="hdnRating" runat="server"  />
            <asp:HiddenField ID="hdnDiscountType" runat="server"  />
            <%--<div class="L34_l">
                <a href="" title="Modify Search" class="button_or1" rel="#slidingDiv" onclick="SetModifyValues();return false;"><span class="opendv">
                    Modify Search</span></a>
            <%--</div>--%>





          
           
            <div class="col-md-2 col-xs-12 martop_xs10 line_right">
                <span class="fnt16"><b>
                    <%if (chkAll.Checked && txtHotelName.Text.Length <= 0)
                      { %>
                    <%=(Session["ResultCount"] != null ? Session["ResultCount"].ToString() : "0")%></b></span>
                Hotels Found in :<br />
                <%}
                      else if (!chkAll.Checked || txtHotelName.Text.Length > 0)
                      {%>
                <%=(Session["ResultCount"] != null ? Session["ResultCount"].ToString() : "0")%></b></span> Hotels Found in : <span class="hidden-xs"> <br /></span>
                <%} %>
                <span class="spnred ">
                <%=request.CityName %>
                </span>
            </div>
           <div class="col-md-2 col-xs-6 martop_xs10 line_right">
                <span class="spnred">Check-in</span><br />
                <span class="fnt11">
                    <%=request.StartDate.ToString("dd, MMM yyyy") %></span> <span class="fnt11_gray">(<%=request.StartDate.ToString("ddd") %>)</span> 
            </div>
            <div class="col-md-2 col-xs-6 martop_xs10 line_right">
                <span class="spnred">Check-Out</span><br />
                <span class="fnt11">
                    <%=request.EndDate.ToString("dd, MMM yyyy") %></span> <span class="fnt11_gray">(<%=request.EndDate.ToString("ddd") %>)</span>
            </div>
            <div class="col-md-2 hidden-xs line_right">
                <span class="spnred">No. of Rooms</span><br />
               <span class="fnt11">
                    <%=(request.NoOfRooms > 0 ? request.NoOfRooms : 1)%></span>
            </div>
           <div class="col-md-2 hidden-xs line_right">
                <span class="spnred">No. of People</span>
                <br />
                <span class="fnt11">
                    <%int adults = 0, childs = 0; foreach (RoomGuestData room in request.RoomGuest)
                      {
                          adults += room.noOfAdults;
                          childs += room.noOfChild;
                      }  %>
                      <%=adults %>
                    (Adult(s))
                    <% =childs  %>
                    (Child(s))
                    </span>
            </div>
           
             <div class="col-md-2 col-xs-12 text-align-center martop_xs10">
              <asp:LinkButton ID="lnkModifySearch" CssClass="but but_b" PostBackUrl="~/HotelSearch.aspx?source=Hotel" runat="server">Modify Search</asp:LinkButton>
               </div>
  
            
            
          
           
     
           
           
            <div id="statescontainer3" style="width: 290px; position: absolute; left: 25px;
                top: 200px; display: none; z-index: 103;">
            </div>
            <div id="errMess" class="error_module" style="display: none;">
                <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                </div>
            </div>
            <div class="clear" style="margin-left: 25px">
                <div id="container1" style="position: absolute; top: 340px; left: 142px; display: none;">
                </div>
            </div>
            <div class="clear" style="margin-left: 30px">
                <div id="container2" style="position: absolute; top: 340px; left: 312px; display: none;">
                </div>
            </div>
            <div id="slidingDiv">
                <div class="L35">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="32%">
                                            Choose Destinations
                                        </td>
                                        <td width="34%" rowspan="2" valign="top">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="26%" valign="bottom">
                                                                    Check In
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:TextBox ID="CheckIn" runat="server" Width="120px" CssClass="auto-list"></asp:TextBox>
                                                                </td>
                                                                <td width="74%" height="23" valign="bottom">
                                                                    <a href="javascript:void(null)" onclick="showCalendar1()">
                                                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                                                                            margin: 5px" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="26%" valign="bottom">
                                                                    Check Out
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:TextBox ID="CheckOut" runat="server" CssClass="auto-list" Width="120px"></asp:TextBox>
                                                                </td>
                                                                <td width="74%" height="23" valign="bottom">
                                                                    <a href="javascript:void(null)" onclick="showCalendar2()">
                                                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                                                                            margin: 5px" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <%--<asp:DropDownList ID="ddlCity" runat="server" AppendDataBoundItems="True" 
                                                Width="100px">
                                                <asp:ListItem Selected="True">Select City</asp:ListItem>
                                            </asp:DropDownList>--%>
                                            <label>
                                                <input style="width: 90%" class="inp_22_1" type="text" id="city" name="city" value="<%=cityName%>"
                                                    onblur="markout(this, '<%=cityName%>')" onfocus="markin(this, '<%=cityName%>')"
                                                    onclick="IntDom('statescontainer3' , 'city')" />
                                                <input type="hidden" id="CityCode" name="CityCode" value="" />
                                                <input type="hidden" id="CountryName" name="CountryName" value="" />
                                                <input type="hidden" name="destinationCity" id="destinationCity" />
                                                <input type="hidden" name="isDomesticHotel" id="isDomesticHotel" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                    <tr>
                                        <td height="21" colspan="3">
                                            <hr class="b_bot_1 " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="27%">
                                            Star Rating
                                        </td>
                                        <td width="44%">
                                            Hotel Name
                                        </td>
                                        <td width="29%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="rating" id="rating" style="width: 95px" class="inp_auto">
                                                <option value="0">Show All</option>
                                                <option value="1">1 rating</option>
                                                <option value="2">2 rating</option>
                                                <option value="3">3 rating</option>
                                                <option value="4">4 rating</option>
                                                <option value="5">5 rating</option>
                                                
                                            </select>
                                        </td>
                                        <td>                                            
                                            <asp:TextBox ID="txtHotel" CssClass="inp_22_2" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label>
                                                No. of room</label>
                                            <input type="hidden" value="1" name="PrevNoOfRooms" id="PrevNoOfRooms" />
                                            <select id="NoOfRooms" name="NoOfRooms" onchange="javascript:ShowRoomDetails()" class="wid55">
                                                <option selected="selected" value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <%-- <option value="5">5</option>
                                                <option value="6">6</option>--%>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="L36">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div id="room-1">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="42%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="43%" class="f_size10">
                                                            &nbsp;
                                                        </td>
                                                        <td width="57%" class="f_size10">
                                                            Adults
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Room 1 : </strong>
                                                        </td>
                                                        <td>
                                                            <select name="adtRoom-1" id="adtRoom-1" class="wid55">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                            </select>
                                                            <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="58%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="80%" class="f_size10">
                                                            Children (2-11 yrs)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select onchange="javascript:ShowChildAge('1')" name="chdRoom-1" id="chdRoom-1" class="wid55">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="ChildBlock-1" name="ChildBlock-1" style="display: none;">
                                                    <div id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 1</span><br />
                                                        <select class="wid55" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 2</span><br />
                                                        <select class="wid55" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 3</span><br />
                                                        <select class="wid55" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 4</span><br />
                                                        <select class="wid55" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 5</span><br />
                                                        <select class="wid55" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-1-Child-6" name="ChildBlock-1-Child-6" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 6</span><br />
                                                        <select class="wid55" id="ChildBlock-1-ChildAge-6" name="ChildBlock-1-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21" colspan="2">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="room-2" style="display: none;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="42%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="43%" class="f_size10">
                                                            &nbsp;
                                                        </td>
                                                        <td width="57%" class="f_size10">
                                                            Adults
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Room 2 : </strong>
                                                        </td>
                                                        <td>
                                                            <select name="adtRoom-2" id="adtRoom-2" class="wid55">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                            </select><input type="hidden" value="0" id="PrevChildCount-2" name="PrevChildCount-2" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="58%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="80%" class="f_size10">
                                                            Children (2-11 yrs)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select onchange="javascript:ShowChildAge('2')" name="chdRoom-2" id="chdRoom-2" class="wid55">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="ChildBlock-2" name="ChildBlock-2" style="display: none;">
                                                    <div id="ChildBlock-2-Child-1" name="ChildBlock-2-Child-1" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 1</span><br />
                                                        <select class="wid55" id="ChildBlock-2-ChildAge-1" name="ChildBlock-2-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-2-Child-2" name="ChildBlock-2-Child-2" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 2</span><br />
                                                        <select class="wid55" id="ChildBlock-2-ChildAge-2" name="ChildBlock-2-ChildAge-2">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-2-Child-3" name="ChildBlock-2-Child-3" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 3</span><br />
                                                        <select class="wid55" id="ChildBlock-2-ChildAge-3" name="ChildBlock-2-ChildAge-3">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-2-Child-4" name="ChildBlock-2-Child-4" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 4</span><br />
                                                        <select class="wid55" id="ChildBlock-2-ChildAge-4" name="ChildBlock-2-ChildAge-4">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-2-Child-5" name="ChildBlock-2-Child-5" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 5</span><br />
                                                        <select class="wid55" id="ChildBlock-2-ChildAge-5" name="ChildBlock-2-ChildAge-5">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-2-Child-6" name="ChildBlock-2-Child-6" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 6</span><br />
                                                        <select class="wid55" id="ChildBlock-2-ChildAge-6" name="ChildBlock-2-ChildAge-6">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21" colspan="2">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div  id="room-3" style="display: none;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="42%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="43%" class="f_size10">
                                                            &nbsp;
                                                        </td>
                                                        <td width="57%" class="f_size10">
                                                            Adults
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Room 3 : </strong>
                                                        </td>
                                                        <td>
                                                            <select name="adtRoom-3" id="adtRoom-3" class="wid55">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                            </select>
                                                            <input type="hidden" value="0" id="PrevChildCount-3" name="PrevChildCount-3" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="58%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="80%" class="f_size10">
                                                            Children (2-11 yrs)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select onchange="javascript:ShowChildAge('3')" name="chdRoom-3" id="chdRoom-3" class="wid55">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="ChildBlock-3" name="ChildBlock-3" style="display: none;">
                                                    <div id="ChildBlock-3-Child-1" name="ChildBlock-3-Child-1" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 1</span><br />
                                                        <select class="wid55" id="ChildBlock-3-ChildAge-1" name="ChildBlock-3-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-3-Child-2" name="ChildBlock-3-Child-2" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 2</span><br />
                                                        <select class="wid55" id="ChildBlock-3-ChildAge-2" name="ChildBlock-3-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-3-Child-3" name="ChildBlock-3-Child-3" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 3</span><br />
                                                        <select class="wid55" id="ChildBlock-3-ChildAge-3" name="ChildBlock-3-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-3-Child-4" name="ChildBlock-3-Child-4" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 4</span><br />
                                                        <select class="wid55" id="ChildBlock-3-ChildAge-4" name="ChildBlock-3-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-3-Child-5" name="ChildBlock-3-Child-5" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 5</span><br />
                                                        <select class="wid55" id="ChildBlock-3-ChildAge-5" name="ChildBlock-3-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-3-Child-6" name="ChildBlock-3-Child-6" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 6</span><br />
                                                        <select class="wid55" id="ChildBlock-3-ChildAge-6" name="ChildBlock-3-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21" colspan="2">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="room-4" style="display: none;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="42%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="43%" class="f_size10">
                                                            &nbsp;
                                                        </td>
                                                        <td width="57%" class="f_size10">
                                                            Adults
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Room 4 : </strong>
                                                        </td>
                                                        <td>
                                                            <select name="adtRoom-4" id="adtRoom-4" class="wid55">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                            </select>
                                                            <input type="hidden" value="0" id="PrevChildCount-4" name="PrevChildCount-4" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="58%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="80%" class="f_size10">
                                                            Children (2-11 yrs)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select onchange="javascript:ShowChildAge('4')" name="chdRoom-4" id="chdRoom-4" class="wid55">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="ChildBlock-4" name="ChildBlock-4" style="display: none;">
                                                    <div id="ChildBlock-4-Child-1" name="ChildBlock-4-Child-1" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 1</span><br />
                                                        <select class="wid55" id="ChildBlock-4-ChildAge-1" name="ChildBlock-4-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-4-Child-2" name="ChildBlock-4-Child-2" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 2</span><br />
                                                        <select class="wid55" id="ChildBlock-4-ChildAge-2" name="ChildBlock-4-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-4-Child-3" name="ChildBlock-4-Child-3" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 3</span><br />
                                                        <select class="wid55" id="ChildBlock-4-ChildAge-3" name="ChildBlock-4-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-4-Child-4" name="ChildBlock-4-Child-4" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 4</span><br />
                                                        <select class="wid55" id="ChildBlock-4-ChildAge-4" name="ChildBlock-4-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-4-Child-5" name="ChildBlock-4-Child-5" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 5</span><br />
                                                        <select class="wid55" id="ChildBlock-4-ChildAge-5" name="ChildBlock-4-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-4-Child-6" name="ChildBlock-4-Child-6" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 6</span><br />
                                                        <select class="wid55" id="ChildBlock-4-ChildAge-6" name="ChildBlock-4-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21" colspan="2">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="room-5" style="display: none;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="42%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="43%" class="f_size10">
                                                            &nbsp;
                                                        </td>
                                                        <td width="57%" class="f_size10">
                                                            Adults
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Room 5 : </strong>
                                                        </td>
                                                        <td>
                                                            <select name="adtRoom-5" id="adtRoom-5" class="wid55">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                            <input type="hidden" value="0" id="PrevChildCount-5" name="PrevChildCount-5" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="58%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="80%" class="f_size10">
                                                            Children (2-11 yrs)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select onchange="javascript:ShowChildAge('5')" name="chdRoom-5" id="chdRoom-5" class="wid55">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="ChildBlock-5" name="ChildBlock-5" style="display: none;">
                                                    <div id="ChildBlock-5-Child-1" name="ChildBlock-5-Child-1" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 1</span><br />
                                                        <select class="wid55" id="ChildBlock-5-ChildAge-1" name="ChildBlock-5-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-5-Child-2" name="ChildBlock-5-Child-2" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 2</span><br />
                                                        <select class="wid55" id="ChildBlock-5-ChildAge-2" name="ChildBlock-5-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-5-Child-3" name="ChildBlock-5-Child-3" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 3</span><br />
                                                        <select class="wid55" id="ChildBlock-5-ChildAge-3" name="ChildBlock-5-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-5-Child-4" name="ChildBlock-5-Child-4" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 4</span><br />
                                                        <select class="wid55" id="ChildBlock-5-ChildAge-4" name="ChildBlock-5-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-5-Child-5" name="ChildBlock-5-Child-5" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 5</span><br />
                                                        <select class="wid55" id="ChildBlock-5-ChildAge-5" name="ChildBlock-5-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-5-Child-6" name="ChildBlock-5-Child-6" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 6</span><br />
                                                        <select class="wid55" id="ChildBlock-5-ChildAge-6" name="ChildBlock-5-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21" colspan="2">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="room-6" style="display: none;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="42%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="43%" class="f_size10">
                                                            &nbsp;
                                                        </td>
                                                        <td width="57%" class="f_size10">
                                                            Adults
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Room 6 : </strong>
                                                        </td>
                                                        <td>
                                                            <select name="adtRoom-6" id="adtRoom-6" class="wid55">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                            <input type="hidden" value="0" id="PrevChildCount-6" name="PrevChildCount-6" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="58%">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="80%" class="f_size10">
                                                            Children (2-11 yrs)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select onchange="javascript:ShowChildAge('6')" name="chdRoom-6" id="chdRoom-6" class="wid55">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="ChildBlock-6" name="ChildBlock-6" style="display: none;">
                                                    <div id="ChildBlock-6-Child-1" name="ChildBlock-6-Child-1" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 1</span><br />
                                                        <select class="wid55" id="ChildBlock-6-ChildAge-1" name="ChildBlock-6-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-6-Child-2" name="ChildBlock-6-Child-2" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 2</span><br />
                                                        <select class="wid55" id="ChildBlock-6-ChildAge-2" name="ChildBlock-6-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-6-Child-3" name="ChildBlock-6-Child-3" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 3</span><br />
                                                        <select class="wid55" id="ChildBlock-6-ChildAge-3" name="ChildBlock-6-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-6-Child-4" name="ChildBlock-6-Child-4" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 4</span><br />
                                                        <select class="wid55" id="ChildBlock-6-ChildAge-4" name="ChildBlock-6-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-6-Child-5" name="ChildBlock-6-Child-5" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 5</span><br />
                                                        <select class="wid55" id="ChildBlock-6-ChildAge-5" name="ChildBlock-6-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                    <div id="ChildBlock-6-Child-6" name="ChildBlock-5-Child-6" style="display: none;
                                                        float: left; padding-right: 4px;">
                                                        <span class="f_size10">Child 6</span><br />
                                                        <select class="wid55" id="ChildBlock-6-ChildAge-6" name="ChildBlock-6-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="21" colspan="2">
                                                <hr class="b_bot_1 " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="childDetails" style="display: none;">
                                    <!--  <p>Child Details</p>-->
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<input type="image" name="imageField" id="imageField" src="images/hotel_search_butt.jpg" onclick="return HotelSearch();">--%>
                                <asp:Button ID="imgModifySearch" runat="server"  CssClass=" button-normal" Text="Search for Hotels"  OnClientClick="return HotelSearch();" OnClick="imgModifySearch_Click" />
                            </td>
                    </table>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        
        
        
       
        
        <div class="col-md-12 padding-0"> 
        
        <div class="col-md-3 padding-0 hidden-xs"> 
        
         <div><a  href="#" onclick="ClearFilters()">Clear All Filters</a></div>
         
         <div class="ns-h3">  Filter Results    </div>
         
                 <div class="padding-5 bg_white">   <asp:TextBox ID="txtHotelName" runat="server" Text="Search By Hotel Name" onblur="if(this.value=='') this.value='Search By Hotel Name'"
                                        onfocus="if(this.value =='Search By Hotel Name' ) this.value=''" CssClass="form-control"></asp:TextBox></div>
                                         <div class="padding-5 bg_white">
                  <asp:DropDownList ID="ddlLocation" CssClass="form-control" runat="server" AppendDataBoundItems="true" Visible="false" >
                                    <asp:ListItem Selected="True" Text="Search By Location" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                   </div>
                                     <div class="padding-5 bg_white">
                                     <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" Text="Search By Location" onblur="if(this.value=='') this.value='Search By Location'"
                                        onfocus="if(this.value =='Search By Location' ) this.value=''" Visible="false"></asp:TextBox>
                                         </div>
         
                <div class="ns-h3">   Star Rating   </div>
                
                
                 <div class="padding-5 bg_white"> <table class="pdr_2px" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="20">
                                    <label>
                                        <asp:CheckBox ID="chkFive" runat="server"  GroupName="Rating" onclick="check(this)"/>
                                    </label>
                                </td>
                                <td>
                                    <div class="five-star-hotel">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkFour" runat="server" GroupName="Rating" onclick="check(this)"/>
                                </td>
                                <td>
                                    <div class="four-star-hotel">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkThree" runat="server" GroupName="Rating" onclick="check(this)"/>
                                </td>
                                <td>
                                    <div class="three-star-hotel">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkTwo" runat="server" GroupName="Rating" onclick="check(this)"/>
                                </td>
                                <td>
                                    <div class="two-star-hotel">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkOne" runat="server" GroupName="Rating" onclick="check(this)"/>
                                </td>
                                <td>
                                    <div class="one-star-hotel">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkAll" runat="server"  onclick="selectAll()"/>
                                </td>
                                <td>
                                    All
                                </td>
                            </tr>
                        </table></div>
            
                 
                                          <div class="ns-h3">  Sort By   </div>
                  <div class="padding-5 bg_white"> <asp:DropDownList CssClass="form-control" ID="ddlOrderBy" runat="server">
                                <asp:ListItem Value="0" Selected="True">Lowest Price</asp:ListItem>
                                <asp:ListItem Value="1">Highest Price</asp:ListItem>
                            </asp:DropDownList></div>
                            
                            <div class="col-md-12 pad_10"> <asp:Button CssClass=" btn but_b" runat="server" ID="ImageButton1" Text="Go"  
                                            OnClientClick="return searchFilter();" /></div>
                            
                
         
        
        </div>
       
       
         <div style=" padding-right:0px;" class="col-md-9"> 
         
         
          <div id="">
                <%if ((hotelSearchResult != null && hotelSearchResult.Length > 0))
                  { %>
                <div class="pagination">

                    <div style="width: auto">
                        <label>
                            <%=showPageLink%></label> </div>
                </div>
              <div class="col-md-12 mb-1">             
           <ul class="pagination pull-right mt-2 ml-4">                             
               <%if (agencydetails.RequiredItinerary)
    { %>
               <li><a id="DownloadLink" style="display: block;font-weight:bold;" href="javascript:ShowEmailDiv('Download')"><i class="fa fa-envelope"></i> Download Itineraries</a></li>
               <%}%>
    
           </ul>                      
       <div class="clearfix"></div>
       
        </div>
                <% }
                  else
                  { %>
                <div class="inn_wrap01">
                    <div class="ns-h3">
                        No Hotels found
                    </div>
                    <div style="text-align: center; font-weight: bold">
                        <%=errorMessage %>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="fff_round">
                    <div class="fff_round_1">
                    </div>
                    <div class="fff_round_2">
                    </div>
                    <div class="fff_round_3">
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <%} %>
             
             
                <asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound"
                    Width="100%"  >
                    <ItemTemplate>
                        <!-- ----------------------------- Modified to display Fare Breakup --------------------- -->
                        <div style="position: relative; display:none">
                            <div style="width: 450px; left: 210px; position: absolute; top: 0px; background: #ffffff;
                                float: left; border: 1px solid #3665A2; visibility: hidden;" class="fare_summary_p"
                                id='fare_summary_<%#Eval("HotelCode") %>'>
                                <asp:Label ID="lblFare" runat="server" Text=""></asp:Label>
                                <!-- ----------------------------- Modified to display Fare Breakup --------------------- -->
                            </div>
                        </div>
                    <div>
                           <div class="email-itenary" id="downloadBlock">
                                            <div class="fleft search-child-block" style="background-color: Red;">
                                                <div class="display-none yellow-back center" id="error">
                                                </div>
                                            </div>
                                             <div class="showMsgHeading">
                                                Download Selected Iitneraries <a class="closex" href="#" onclick="HideEmailDiv('downloadBlock');" style="position: absolute;
                                                    right: 5px; top: 3px;">X</a>
                                            </div>
                                            <div class="pad_10">
                                                <div id="itineraryCount1">
                                                    Selected 2 Itineraries for Download</div>
                                              
                                                <%if (Settings.LoginInfo.IsCorporate!="Y")
                                                    { %>

                                                <div>
                                                    Add Markup:</div>
                                                <div>
                                                    <input class="form-control" type="text" id="txtDownloadMarkup" onkeypress="return isNumber(event);"
                                                        name="txtDownloadMarkup" value="0" maxlength="5"  /></div>
                                                <%} %>
                                                <div class="fleft center width-100 margin-top-5">
                                                    <p id="emailStatus" style="color: #18407B; display: none;">
                                                    </p>
                                                </div>
                                                <div>
                                                    <input class="btn but_b" type="button" value="Download" onclick="SelectResult()" />                                                    
                                                </div>                                              
                                            </div>
                                            <div class=" clear">
                                            </div>
                                        </div>
                    </div>
                    
                    
                    
                    <div class="col-md-12 padding-0"> 
                    
                    
                    <div class="hotel_listing">
                   
                   <div class="col-md-2"> 
                   <asp:Image Width="100%" Height="100px" ID="Image1" runat="server" ImageUrl='<%# Bind("HotelPicture") %>' />
                   
                    </div>
                   
                     <div class="col-md-7"> 
                     <div> <h4> <asp:Label ID="Label1" runat="server" Text='<%# Bind("HotelName") %>'></asp:Label></h4> </div>
                     
                     <div> <asp:Label ID="lblDescription" runat="server" ></asp:Label></div>
                     
                   
                     
                     <div>
                     
                     <div class="hotel_window_links">
                                        <%--<span style="color: #999999"><%#Eval("BookingSource") %></span> ---%>
                                        <a class="photo_ico" href="javascript:GetHotelDetail('<%#Container.ItemIndex %>','<%#Eval("HotelCode") %>','<%#Eval("CityCode") %>','<%#Eval("HotelName").ToString().Replace("'", "\\'") %>','<%=request.IsDomestic %>','<%=Session["sSessionId"] %>','photos')">
                                            Photos</a>  <a class="description_ico" href="javascript:GetHotelDetail('<%#Container.ItemIndex %>','<%#Eval("HotelCode") %>','<%#Eval("CityCode") %>','<%#Eval("HotelName").ToString().Replace("'", "\\'") %>','<%=request.IsDomestic %>','<%=Session["sSessionId"] %>','description')">
                                                Description</a>  <a class="amenities_ico" href="javascript:GetHotelDetail('<%#Container.ItemIndex %>','<%#Eval("HotelCode") %>','<%#Eval("CityCode") %>','<%#Eval("HotelName").ToString().Replace("'", "\\'") %>','<%=request.IsDomestic %>','<%=Session["sSessionId"] %>','amenities')">
                                                    Amenities</a> 
                                                    <a class="map_ico" href="javascript:GetHotelDetail('<%#Container.ItemIndex %>','<%#Eval("HotelCode") %>','<%#Eval("CityCode") %>','<%#Eval("HotelName").ToString().Replace("'", "\\'") %>','<%=request.IsDomestic %>','<%=Session["sSessionId"] %>','map')">View Map</a>
                                        <%--<a class="fancybox" href='#' onclick="ShowHotelDetails('inline1<%#Eval("HotelCode") %>','<%#Eval("HotelCode") %>','photos_album_')">Photos</a>--%>
                                        <%--- <a class="fancybox" href='#' onclick="ShowHotelDetails('inline1<%#Eval("HotelCode") %>','<%#Eval("HotelCode") %>','home')">Description</a> - <a class="fancybox" href='#' onclick="ShowHotelDetails('inline1<%#Eval("HotelCode") %>','<%#Eval("HotelCode") %>','aminities_')">
                                            Amenities</a>--%>
                                            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.Agent)
                                              { %>
                                           Source: 
                                        <asp:Label CssClass="hotelsource" ID="Label2" runat="server" Text='<%#Eval("BookingSource") %>'></asp:Label>
                                        <%} %>
                                    </div>
                      
                       <input  type='hidden' id='hSelRooms' runat="server" value=""  />
                     
                      </div>
                      
                        <div> <asp:Label ID="lblPROMO" runat="server" Text="" ></asp:Label></div>
                     
                    
                     
                      </div>
                     
                      
                      
                       <div class="col-md-3 col-xs-12"> 
                     
                       <div class="col-xs-6 col-md-12 p-0"> 
                       
                        <%--<asp:Label CssClass="lowest_price" ID="Label3" runat="server" Text="From"></asp:Label>--%>
                        
                        <asp:Label ID="lblRating" runat="server" Text=""></asp:Label>
                                        <asp:Label CssClass="best_price" ID="lblLowestPrice" runat="server" Text=""></asp:Label>
                                        
                                        
                                        <asp:Label ID="lblFareSummary" runat="server" Text=""></asp:Label>
                                        
                                        
                                        
                                        
                                        <asp:Label runat="server" ID='lblDeal'><img src="images/hotel-deal.gif" alt="Deal"/> </asp:Label>
                                        </div>
                       
                          <div class="col-xs-6 col-md-12 p-0"> 
                       
                      
                                       
                                        <asp:Button ID="imgBtnBookNow" ToolTip="Actual price may vary upon room selection" CssClass=" btn but_b" 
                                            runat="server" Text="Choose Room"   />
                                           
                                            
                                            </div>
                                            
                                            
                                            
                       
                        </div>
                       <div class="clearfix"> </div>
                      </div>
                      
                    
                    <div class="clearfix"> </div>
                    
                    </div>
                    
                    
                    
                    
                        <div id="mainDiv" title="" runat="server">
                 
                         <asp:Label ID="lblMultiRoomSelect" runat="server" Text=""></asp:Label>
                          
                            <table id="tblRoomDetails" runat="server" width='100%' border='1' cellpadding='0' cellspacing='0' bordercolor='#CCCCCC' class='table_loop_1' >
                            <tr>
                            <th width="30%">Room Type</th>
                            <th width="30%">Room Details</th>
                            <th width="40%">Total</th>
                            <%--<th>E-mail</th>--%>
                            </tr>
                                
                            </table>
                        </div>
                        
                        
                        
                    </ItemTemplate>
                </asp:DataList>
                <div class="pagination">
                <div>
                        <span>
                            <%=showPageLink%></span><span style="clear: both">&nbsp;</span></div>
                </div>
            </div>
         
         
         </div>
         <div class="clearfix"> </div> 
        </div>
        
        
     
        
        
        

   
    
</asp:Content>
