﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="BaggageInsurance.aspx.cs" Inherits="CozmoB2BWebApp.BaggageInsuranceGUI" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style type="text/css">
        .insurance-card {
            text-align: center;
            cursor: pointer;
            margin-bottom: 30px;
        }

            .insurance-card .front, .insurance-card .back {
                background-color: #ECDE24;
                border: 1px solid #ECDE24;
                padding: 20px;
                min-height: 100%;
                border-radius: 26px;
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-align: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column;
                box-shadow: 0 6px 25px rgba(0,0,0,.12);
                min-height: 200px;
            }

            .insurance-card .front {
                background-color: #fff;
                border: 1px solid #fff;
            }

            .insurance-card.active .front {
                background-color: #1c498a;
                color: #fff;
            }

                .insurance-card.active .front h4, .insurance-card.active .plan-amount {
                    color: #fff;
                }

                .insurance-card.active .front .select-btn {
                    backface-visibility: hidden;
                    background-color: #ccc !important;
                    color: #000000 !important;
                }

            .insurance-card .back {
            }

            .insurance-card .custom-radio2-table input[type="radio"] + label:before {
                width: 35px;
                height: 35px;
                line-height: 35px;
                font-size: 15px;
            }

            .insurance-card .custom-radio2-table input[type="radio"]:checked + label:after {
                width: 15px;
                height: 15px;
                background-color: #1c498a;
                left: 10px;
                top: 9px;
            }

            .insurance-card h4 {
                font-size: 14px;
                color: #1c498a;
                font-weight: bold;
            }

            .insurance-card .plan-amount {
                font-size: 32px;
                font-weight: bold;
                color: #000;
                position: relative;
            }

                .insurance-card .plan-amount .small {
                    font-size: 12px;
                    position: absolute;
                    left: -27px;
                    bottom: 6px;
                    color: #a9a9a9;
                }

            .insurance-card ul {
                font-size: 13px;
                list-style: none;
                padding: 0;
            }

                .insurance-card ul li {
                    position: relative;
                    padding: 6px 0;
                }

                    .insurance-card ul li:after {
                        content: '';
                        height: 1px;
                        width: 14px;
                        left: 0;
                        right: 0;
                        position: absolute;
                        bottom: 0;
                        background-color: #000;
                        margin: 0 auto;
                    }

                    .insurance-card ul li:last-child:after {
                        display: none;
                    }

        .radio-wrap {
            padding-top: 15px;
        }

        .select-btn {
            margin-top: 20px;
        }

        .pax-info, .select-insurance-wrap {
            background-color: #ececec;
            padding: 20px 12px;
            margin: 0 3px;
        }

        .custom-radio2-table input[type="radio"]:checked + label:after {
            background-color: #1c498a;
        }
    </style>
    <div class="cz-container">



        <script src="<%=Request.Url.Scheme%>://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"> </script>
        <script type="text/javascript">
            function Validate() {

                var isValid = true;

                document.getElementById('emailErr').style.display = 'none';
                document.getElementById('phnoErr').style.display = 'none';
                document.getElementById('paxError').style.display = 'none';

                if (document.getElementById('<%=txtemail.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('emailErr').style.display = 'block';
                    document.getElementById('emailErr').innerHTML = "Please Enter EmailID ";
                    return isValid = false;
                }
                if (document.getElementById('<%=txtPhoneNo.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('phnoErr').style.display = 'block';
                    document.getElementById('phnoErr').innerHTML = "Please Enter Phone Number ";
                    return isValid = false;
                }
                if (document.getElementById('<%=txtPhoneNo.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('phnoErr').style.display = 'block';
                    document.getElementById('phnoErr').innerHTML = "Please Enter Phone Number ";
                    return isValid = false;
                }
                if (document.getElementById('<%=txtemail.ClientID%>').value.trim().length > 0) {
                    isValid = ValidateEmail(document.getElementById('<%=txtemail.ClientID%>').value);
                    if (isValid == false)
                        return isValid = false;
                }
                if (document.getElementById('<%=txtPhoneNo.ClientID%>').value.trim().length > 0) {
                    isValid = ValidatePhoneNumber(document.getElementById('<%=txtPhoneNo.ClientID%>').value)
                        if (isValid == false)
                            return isValid = false;
                }
                var paxCount = document.getElementById('<%=ddlNoOfPassengers.ClientID%>').value;
                if (isValid) {

                    for (var i = 1; i <= paxCount; i++) {
                        if (document.getElementById('ctl00_cphTransaction_txtFirstName' + i).value.trim().length <= 0) {
                            document.getElementById('paxError').style.display = 'block';
                            document.getElementById('paxError').innerHTML = "Please Enter FirstName";
                            return isValid = false;
                        }
                        if (document.getElementById('ctl00_cphTransaction_txtLastName' + i).value.trim().length <= 0) {
                            document.getElementById('paxError').style.display = 'block';
                            document.getElementById('paxError').innerHTML = "Please Enter LastName";
                            return isValid = false;
                        }
                        if (document.getElementById('ctl00_cphTransaction_txtPNRNumber' + i).value.trim().length <= 0) {
                            document.getElementById('paxError').style.display = 'block';
                            document.getElementById('paxError').innerHTML = "Please Enter PNR Number";
                            return isValid = false;
                        }
                        if (document.getElementById('ctl00_cphTransaction_txtTicketNumber' + i).value.trim().length <= 0) {
                            document.getElementById('paxError').style.display = 'block';
                            document.getElementById('paxError').innerHTML = "Please Enter Ticket Number";
                            return isValid = false;
                        }

                    }
                }
                if (isValid)
                {
                    for (var i = 1; i <= paxCount; i++) {
                        if ((i + 1) <= paxCount) {
                            if (document.getElementById('ctl00_cphTransaction_txtTicketNumber' + i).value == document.getElementById('ctl00_cphTransaction_txtTicketNumber' + (i + 1)).value) {
                                document.getElementById('paxError').style.display = 'block';
                                document.getElementById('paxError').innerHTML = "Ticket Number should not be same.";
                                return isValid = false;
                            }
                        }
                    }
                }

                if (isValid) {
                    for (var i = 1; i <= paxCount; i++) {
                        if (document.getElementById('ctl00_cphTransaction_txtPNRNumber' + i).value.trim().length <6) {
                                document.getElementById('paxError').style.display = 'block';
                                document.getElementById('paxError').innerHTML = "PNR Number should be minimum 6 charectors.";
                                return isValid = false;
                            }
                    }
                }
                if (isValid) {
                    for (var i = 1; i <= paxCount; i++) {
                        if (document.getElementById('ctl00_cphTransaction_txtTicketNumber' + i).value.trim().length < 6) {
                            document.getElementById('paxError').style.display = 'block';
                            document.getElementById('paxError').innerHTML = "Ticket Number should be minimum 6 charectors.";
                            return isValid = false;
                        }
                    }
                }

                if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAirAgents.ClientID %>').value == "0") {
                    alert('Please select the Agent');
                    var ddl = document.getElementById('<%=ddlAirAgentsLocations.ClientID%>');
                        $("#<%=ddlAirAgentsLocations.ClientID%>").select2().attr('text', '');
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);
                    return isValid = false;
                }

                if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                && (document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "-1" || document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "0")) {
                        alert('Please select the Location');

                        return isValid = false;
                    }


                    if (isValid) {
                        isValid = validatePlan();
                    }

                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAirAgents.ClientID %>').value != "-1"
                        && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value != "-1") {
                        document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value;

                    }


                    return isValid;
                }
                function getLocation() {
                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                       && document.getElementById('<%=ddlAirAgents.ClientID %>').value != "-1"
                        && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value != "-1") {
                        document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value;

                    }
                }

                function ValidatePhoneNumber(phoneNumner) {
                    var isValid = true;
                    var phonenoreg = /^\d{15}$/;
                    if ((phoneNumner.match(phonenoreg))) {
                        document.getElementById('phnoErr').style.display = 'none';
                        isValid = true;
                    }
                    else {
                        document.getElementById('phnoErr').style.display = 'block';
                        document.getElementById('phnoErr').innerHTML = "Please Enter Phone Number";
                        isValid = false;
                    }
                    return isValid;

                }

                function disablefield() {
                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {

                        document.getElementById('wrapper1').style.display = "block";
                        document.getElementById('wrapper2').style.display = "block";

                    }
                    else {
                        document.getElementById('wrapper1').style.display = "none";
                        document.getElementById('wrapper2').style.display = "none";


                    }
                    <%--if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                        document.getElementById('wrapper').style.display = "block";

                    }
                    else {
                        document.getElementById('wrapper').style.display = "none";

                    }--%>
                }

            function validatePlan() {
                var isValid = false;
                var count = document.getElementById('<%=hdnPlanCount.ClientID%>').value;
                for (var i = 1; i <= count; i++) {  // i=1 planId=1 in DB
                    var rbn = document.getElementById('rbnPlanID_' + i);
                    if (rbn != null && rbn.checked == true) {
                        return isValid = true;
                    }
                }
                if (isValid == false) {
                    alert("Please select the Plan");
                    return isValid = false;
                }
            }

            function isAlpha(e) {
                var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
                //autoCompInit1();
                return ret;
            }

            function ValidateEmail(emailID) {
                var isValid = true;
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (emailID.match(mailformat)) {
                    document.getElementById('emailErr').style.display = 'none';
                    isValid = true;
                }
                else {
                    document.getElementById('emailErr').style.display = 'block';
                    document.getElementById('emailErr').innerHTML = "You have entered an invalid email address"
                    isValid = false;
                }
                return isValid;
            }
            function allowNumerics(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                  && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
            function isAlphaNumeric(e) {

                var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11);
                return ret;
            }
            function StoreSelectedPlanId(e) {
                document.getElementById('<%=hdnPlanId.ClientID%>').value = e.split('_')[1];
            }
           

            function InitCards() {
                var id = document.getElementById('ctl00_cphTransaction_hdnPlanId').value;
                $("#card-" + id).addClass('active');
                $(".insurance-card").flip({ trigger: 'hover' });
                $("#rbnPlanID_" + id).prop('checked', true);
                $("#card-" + id).find('.select-btn').text('Selected');
                                      
            }

            var Ajax;
            function LoadAgentLocations(id, type) {

                var ddl = document.getElementById('<%=ddlAirAgentsLocations.ClientID%>');
                $("#<%=ddlAirAgentsLocations.ClientID%>").empty();
                    $("#<%=ddlAirAgentsLocations.ClientID%>").select2().attr('text', '');
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);

                    if(document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                       && document.getElementById('<%=ddlAirAgents.ClientID %>').value != "0" && 
                        (document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "-1" || 
                        document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "0"))
                    {
                        document.getElementById('<%=hdnAgentLocation.ClientID%>').value = "0";
                        document.forms[0].submit();
                    }    

                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                       && document.getElementById('<%=ddlAirAgents.ClientID %>').value == "0") {
                       
                        if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Location";
                            el.value = "-1";
                            ddl.add(el, 0);
                            var values = ""
                            var el = document.createElement("option");
                            el.textContent = "";
                            el.value = "";
                            ddl.appendChild(el);
                        }
                        document.getElementById('<%=hdnagentIdDeselected.ClientID%>').value = "AgentDeselected";
                        document.forms[0].submit();
                        $("#<%=ddlAirAgentsLocations.ClientID%>").empty();
                    }
                    
                    else {
                        var location = 'ctl00_cphTransaction_ddlAirAgentsLocations';
                        if (type == 'H') {
                            location = 'ctl00_cphTransaction_ddlHotelAgentLocations';
                        }
                        var sel = document.getElementById('<%=ddlAirAgents.ClientID %>').value;

                        var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + sel + '&id=' + location;;
                        var url = "CityAjax.aspx";

                        if (window.XMLHttpRequest) {
                            Ajax = new XMLHttpRequest();
                        }
                        else {
                            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        Ajax.onreadystatechange = BindLocationsList;
                        Ajax.open('POST', url);
                        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                        Ajax.send(paramList);
                    }

                }
                function BindLocationsList(response) {
                    if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
                        var ddl = document.getElementById(Ajax.responseText.split('#')[0]);
                        if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Location";
                            el.value = "-1";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');
                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }
                        }
                    }
                }


        </script>
        <asp:HiddenField ID="hdnPlanId" runat="server" Value="0" />
        <asp:HiddenField ID="hdnPlanCount" runat="server" Value="0" />
        <asp:HiddenField ID="hdnPNRNumber" runat="server" Value="" />
        <asp:HiddenField ID="hdnAgentLocation" runat="server" Value="0" />
        <asp:HiddenField ID="hdnNoofPaxValue" runat="server" Value="0" />
        <asp:HiddenField ID="hdnagentIdDeselected" runat="server" Value="" />

        <div class="body_container">
            <div id="insurance-1">
                <h3 class="py-4">Select Insurance Plan</h3>
                <div class="select-insurance-wrap">
                   
                            <div class="row">
                        <%for (int i = 0; i < baggageInsuranceMasterList.Count; i++)
                            {
                                CT.BookingEngine.BaggageInsuranceMaster objBaggageInsurace = baggageInsuranceMasterList[i];
                        %>
                        <div class="col-md-3">
                            <div id="card-<%=i+1%>" class="insurance-card">
                                <div class="front">
                                    <h4><%=objBaggageInsurace.PlanTitle%></h4>

                                    <div class="plan-amount"><span class="small"><%=objBaggageInsurace.PaxCurrency %></span> <%=(objBaggageInsurace.PlanAdultPrice+ objBaggageInsurace.Markup+objBaggageInsurace.GSTAmount+objBaggageInsurace.InputVAT+objBaggageInsurace.OutputVAT-objBaggageInsurace.Discount).ToString("N"+ decimalPoints) %> </div>
                                    <%=objBaggageInsurace.PlanDescription %>
                                    <button class="btn btn-primary select-btn">Select</button>
                                </div>
                                <div class="back">
                                    <ul>
                                        <% string[] marketingpts = objBaggageInsurace.PlanMarketingPoints.Split('/');
                                            foreach (var item in marketingpts)
                                            {%>
                                        <li><%=item %></li>
                                        <% }
                                        %>
                                    </ul>
                                    <div class="radio-wrap custom-radio2-table">
                                        <input type="radio" id="rbnPlanID_<%=i+1%>" name="insuranceplan" class="insurance-radio" onchange="StoreSelectedPlanId(event);">
                                        <label></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%  }%>
                    </div>
                     
                    



                    <div class="row">

                        <div class="col-md-8"></div>

                        <div class="col-md-4">
                            <div class="row custom-radio2-table">

                                <div class="col-md-6 col-xs-6">
                                    <asp:RadioButton ID="rbtnSelf" runat="server" GroupName="book" onchange="disablefield();" AutoPostBack="true" OnCheckedChanged="rbtnSelf_CheckedChanged"
                                        Font-Bold="True" Text="Self" Checked="true" />
                                </div>
                                <div class="col-md-6 col-xs-6">
                                   
                                            <asp:RadioButton ID="rbtnAgent" runat="server" GroupName="book" onchange="disablefield();" AutoPostBack="true" OnCheckedChanged="rbtnAgent_CheckedChanged"
                                        Font-Bold="True" Text="Agency" />
                                       
                                    
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">


                                        <asp:Label ID="lblNoofPassengers" runat="server" Text="No Of Passengers"></asp:Label>
                                        <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>--%>
                                        <asp:DropDownList ID="ddlNoOfPassengers" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlNoOfPassengers_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                            <asp:ListItem Value="9" Text="9"></asp:ListItem>

                                        </asp:DropDownList>
                                        <%--</ContentTemplate>--%>
                                        <%-- </asp:UpdatePanel>--%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblNoofSectors" runat="server" Text="No Of Sectors"></asp:Label>
                                        <asp:DropDownList ID="ddlNoOfSectors" runat="server" CssClass="form-control">
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-6" id="wrapper1" style="display: none">
                                    <div class="form-group">
                                        <asp:Label ID="ddlAgents" runat="server" Text="Agents"></asp:Label>
                                        <asp:DropDownList runat="server" ID="ddlAirAgents" CssClass="form-control" onchange="LoadAgentLocations(this.id,'F')"></asp:DropDownList>
                                    </div>
                                </div>
                                <%--  <div class="col-md-6"><div id="wrapper1" style="display: none" >--%>
                                <%-- <asp:Label ID="ddlAgents" runat="server" Text="Agents"></asp:Label>
                                  
                                             <asp:DropDownList ID="ddlAirAgents"  AppendDataBoundItems="true"  CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAirAgents_SelectedIndexChanged" >
                                                            </asp:DropDownList>--%>
                                <div class="col-md-6">
                                    <div id="wrapper2" style="display: none">

                                        <asp:Label ID="Label1" runat="server" Text="Location"></asp:Label>

                                        <asp:DropDownList ID="ddlAirAgentsLocations" CssClass="form-control" runat="server" AutoPostBack="true" onchange="getLocation()">
                                        </asp:DropDownList>


                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblEmailID" runat="server" Text="Email ID"></asp:Label>
                                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                                        <b class="red_span" style="display: none" id="emailErr"></b>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPhoneNumber" runat="server" Text="Phone Number"></asp:Label>
                                        <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="form-control" onkeypress="return allowNumerics(event);" MaxLength="15"></asp:TextBox>
                                        <b class="red_span" style="display: none" id="phnoErr"></b>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <%--    <div class="col-md-6"><div id="wrapper3" style="display: none" >

                                   
                                                   
                                                        </div> </div>--%>



                        <%--<div class="col-md-3" id="divLocations" visible="false" runat="server">
                                        <div class="form-group">
                                            <asp:Label ID="lblLocations" runat="server" Text="Locations"></asp:Label>
                                            <asp:DropDownList ID="ddlAirAgents" AppendDataBoundItems="true" CssClass="form-control" runat="server" onchange="LoadAgentLocations(this.id,'F')">
                                            </asp:DropDownList>
                                        </div>

                                    </div>--%>
                    </div>


                </div>

            </div>

            <div id="insurance-2">
                <h3 class="py-4 mt-3">Passenger Information</h3>
                <div class="pax-info">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tblPaxDetails" runat="server">
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <b class="red_span py-3" style="display: none" id="paxError"></b>
                    <asp:Label ID="lblResult" runat="server" Style="display: none; color: green; font-family: Arial; font-size: large;" Text=""></asp:Label>

                    <div class="text-right">

                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn_custom cursor_point" Text="Save" CausesValidation="true" OnClientClick="return Validate();" OnClick="btnSave_Click" />
                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary btn_custom" Text="Clear" OnClick="btnClear_Click" />

                    </div>


                </div>



            </div>
        </div>

        <div style="display: none" id="footer">
            <span class="frright01">
                <div id="ctl00_upnlGrandTotal">
                </div>
            </span>
            <span class="frright">Powered by <a href="#">
                <img src="images/czit.png" alt="#" /></a></span>

        </div>



        <script>
            $(document).ready(function () {

                $(".insurance-card").flip({ trigger: 'hover' });
                $('body').on('click', '.insurance-card', function () {
                    $('.insurance-card').removeClass('active');
                    $(this).addClass('active');
                    $(this).find('.insurance-radio').prop("checked", true);
                    var radioID = $(this).find('.insurance-radio').attr('id');
                    StoreSelectedPlanId(radioID);
                    $('.select-btn').text('Select')
                    $(this).find('.select-btn').text('Selected')
                })

            })


        </script>

        <script type="text/javascript">
            disablefield();

        </script>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server"></asp:Content>
