<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Title="Corporate Trip Summary" Inherits="CorporateTripSummaryUI" Codebehind="CorporateTripSummary.aspx.cs" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="CT.Corporate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    

    <%if (listFlightItinerary != null && listFlightItinerary.Count > 0 && listFlightItinerary[0] != null) %>
    <%{
            flightItinerary = listFlightItinerary[0];
    %>



    <div class="body_container">

        


        <div class="tab-content responsive">
            <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #fff !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
        <tr style="padding: 0; text-align: left; vertical-align: top">
            <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                <center data-parsed="" style="min-width: 700px; width: 100%">
                <%if(Request.IsSecureConnection){%> <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" align="center" class="float-center" /><%}%>
                    <%else {%> <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" align="center" class="float-center" /><%}%>
                    <h2>Trip Summary</h2>
                    <table align="center" class="container bc-bookinginfo-wrapper float-center" style="Margin: 0 auto; background: #f7f7f7; border: solid #d0cfcf 1px; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 700px">
                        <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">

                                    <table class="main-header" width="100%" cellspacing="0" cellpadding="4" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <strong style="font-weight: 600">Traveler:</strong>
                                                </td>
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <%=flightItinerary.CreatedOn.ToString("dd MMM yyyy") %> Trip <%=status+" - "%>  <%=detail.EmpName+" - "%> <%=flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName %>

                                                </td>
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <%if (Ticket.GetTicketList(flightItinerary.FlightId).Count > 0)
                                                                                 { %> <a href="ETicket.aspx?FlightId=<%=flightItinerary.FlightId %>" target="_blank">View Tickets</a><%} %>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table width="100%" cellspacing="0" cellpadding="4" border="0" class="table-bg-color" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>


                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Trip Reference:</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=flightItinerary.TripId %>
                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Trip Request for <%=detail.EmpName %></strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%if (status == "Approved")
                                                                        { %>
                                                                    Approved <strong style="color: green; font-weight: 600">&#10003;</strong> 
                                                                    <%}
    else if (status == "Rejected")
    { %>
                                                                    Rejected <strong style="color: red; font-weight: 600">&#10005;</strong>
                                                                    <%}
    else if (status == "Pending")
    {
        if (Ticket.GetTicketList(flightItinerary.FlightId).Count > 0)
        {%>
                                                                    Approved <strong style="color: green; font-weight: 600">&#10003;</strong> 
                                                                    <%}
    else
    {%> 
                                                                    Pending <strong style="color: gray; font-weight: 600">&#10071;</strong>
                                                                    <%}
    }
    else
    { %>         
                                                                    Ticketed <strong style="color: green; font-weight: 600">&#2713;</strong> 
                                                                    <%} %>
                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Employee ID</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=detail.EmpId %>
                                                                </td>
                                                            </tr>
                                                            <%if(status == "Rejected"){ %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Rejection Reason:</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=CorpProfileApproval.GetRejectionReason(flightItinerary.FlightId) %>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Approval Deadline</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=DateTime.Now.AddHours(Convert.ToDouble(deadline)).ToString("dd-MMM-yyyy hh:mm:ss tt") %>
                                                                </td>
                                                            </tr>
                                                            <%for (int i = 0; i < detail.ProfileApproversList.Select(p => new { Hierarchy = p.Hierarchy }).Distinct().ToList().Count; i++)
                                                                { %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Approver <%=i+1 %></strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=detail.ProfileApproversList[i].ApproverEmail%>
                                                                </td>
                                                            </tr>
                                                            <%}
                                                                string fallBackApprover = string.Empty;
                                                                List<CorpProfileApproval> fallBackApprovers = detail.ProfileApproversList.FindAll(a => a.Hierarchy==detail.ProfileApproversList[0].Hierarchy);
                                                                if (fallBackApprovers.Count > 1)
                                                                {
                                                                    fallBackApprover = fallBackApprovers[1].ApproverEmail;
                                                                    %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Fall Back Approver</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=fallBackApprover %>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <hr size="1" />
                                                                </td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <span class="image-label" style="background-color: #656565; color: #fff; font-size: 11px; font-weight: bold; padding: 1px 6px; text-transform: uppercase">Trip Information</span>
                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td height="5" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Reason for Travel:
                                                                        <%
                                                                            FlightPolicy flightPolicy = new FlightPolicy();
                                                                            flightPolicy.Flightid = flightItinerary.FlightId;

                                                                            flightPolicy.GetPolicyByFlightId();
                                                                            flightPolicies.Add(flightPolicy);                                                                          
                                                                            
                                                                            CorporateTravelReason travelReason = new CorporateTravelReason();
                                                                            if (flightPolicies.Count > 0)
                                                                                travelReason = new CorporateTravelReason(flightPolicies[0].TravelReasonId); %>
                                                                    </strong><%=string.IsNullOrEmpty(travelReason.Description) ? "" : travelReason.Description %>

                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Company:
                                                                    </strong><%=agency.Name %>

                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Policy Compliance:
                                                                    </strong><%=flightPolicies.Count > 0 ? flightPolicies[0].IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + flightPolicies[0].PolicyBreakingRules :"" %>

                                                                </td>
                                                            </tr>
                                                            <%CorporateTravelReason policyReason = new CorporateTravelReason();
                                                                if (flightPolicies.Count > 0) policyReason = new CorporateTravelReason(flightPolicies[0].PolicyReasonId);
                                                                if (flightPolicies.Count > 0 && !flightPolicies[0].IsUnderPolicy)
                                                                { %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Violation Reason:                                                                        
                                                                    </strong> <%= !string.IsNullOrEmpty(policyReason.Description) ? policyReason.Description : string.Empty %>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                        </tbody>
                                                    </table>
                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                    <table class="flight-table" style="border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Airline</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Departure</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Arrival</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Stops</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Duration</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Price</th>
                                                        </tr>
                                                        <% string currency = flightItinerary.Passenger[0].Price.Currency;
                                                            int decimalPoint = flightItinerary.Passenger[0].Price.DecimalPoint;
                                                            List<string> meals = new List<string>();
                                                            List<decimal> mealPrices = new List<decimal>();
                                                            List<string> baggages = new List<string>();
                                                            List<decimal> baggagePrices = new List<decimal>();
                                                            List<string> seats = new List<string>();
                                                            List<decimal> seatPrices = new List<decimal>();
                                                            int segments = flightItinerary.Segments.Length;
                                                            segments += 5;//For SSR values

                                                            flightItinerary.Passenger.ToList().ForEach(p =>
                                                            {
                                                                string meal = string.Empty, seat = string.Empty, bag = string.Empty;
                                                                for (int i = 0; i < flightItinerary.Segments.Length; i++)
                                                                {
                                                                    meal += meal.Length > 0 ? ", No Meal" : " No Meal";
                                                                    seat += seat.Length > 0 ? ", No Seat" : " No Seat";
                                                                    bag += bag.Length > 0 ? ", No Bag" : " No Bag";
                                                                }
                                                                meals.Add(string.IsNullOrEmpty(p.MealDesc) ? meal : p.MealDesc);
                                                                mealPrices.Add(p.Price.MealCharge);
                                                                baggages.Add(string.IsNullOrEmpty(p.BaggageCode) ? bag : p.BaggageCode);
                                                                baggagePrices.Add(p.Price.BaggageCharge);
                                                                seats.Add(string.IsNullOrEmpty(p.SeatInfo) ? seat : p.SeatInfo);
                                                                seatPrices.Add(p.Price.SeatPrice);
                                                            });
                                                            for (int i=0; i<flightItinerary.Segments.Length; i++)
                                                            {
                                                                FlightInfo flight = flightItinerary.Segments[i];%>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                <%Airline airline = new Airline(); airline.Load(flight.Airline); %><%=airline.AirlineName %><br /><%=flight.Airline %> <%=flight.FlightNumber %>
                                                            </td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                <strong><%=flight.Origin.CityName %></strong><br /><%=flight.DepartureTime.ToString("dd MMM yyyy") %><br /><%=flight.DepartureTime.ToString("(ddd), hh:mm tt") %>,<br />Airport:<%=flight.Origin.AirportName %>, Terminal: <%=flight.DepTerminal %>
                                                            </td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                <strong><%=flight.Destination.CityName %></strong><br /><%=flight.ArrivalTime.ToString("dd MMM yyyy") %><br /><%=flight.ArrivalTime.ToString("(ddd), hh:mm tt") %>,<br />Airport:<%=flight.Destination.AirportName %>, Terminal: <%=flight.ArrTerminal %>
                                                            </td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word"><%=flight.Stops == 0 ? "Non Stop" : flight.Stops == 1 ? "One Stop" : "Two Stops" %></td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word"><%=flight.Duration %></td>
                                                            <%if(i==0){ %>
                                                            <td rowspan="<%=segments %>" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; border-left: 1px solid #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: center; vertical-align: middle; word-wrap: break-word">
                                                                <strong><%=currency %> <%=flightItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup + p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                                                                p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount).ToString("N"+ decimalPoint) %></strong></td>
                                                            <%} %>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top;width:100%">
                                                                        <tbody>
                                                                            
                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <%if (mealPrices.Sum() > 0)
                                                                                        {
                                                                                            for (int j = 0; j < meals.Count; j++)
                                                                                            { %>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Meal:
                                                                                    </strong> <%=meals[j] %> [<%=currency %> <%=mealPrices[j].ToString("N" + decimalPoint) %>]
                                                                                </td>
                                                                                <%}
                                                                                        } %>
                                                                            </tr>
                                                                            
                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <%if (seatPrices.Sum() > 0)
                                                                                                               {
                                                                                                                   for (int k = 0; k < seats.Count; k++)
                                                                                                                   { %>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Seat:
                                                                                    </strong> <%=seats[k] %>  [<%=currency %> <%=seatPrices[k].ToString("N" + decimalPoint) %>]
                                                                                </td>
                                                                                <%}
                                                                                                               } %>
                                                                            </tr>
                                                                            
                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <%if (baggagePrices.Sum() > 0)
                                                                                        {
                                                                                            for (int l = 0; l < baggages.Count; l++)
                                                                                            { %>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Additional Baggage
                                                                                    </strong> <%=baggages[l] %>   [<%=currency %> <%=baggagePrices[l].ToString("N" + decimalPoint) %>]
                                                                                </td>
                                                                                <%}
                                                                                        } %>
                                                                            </tr>

                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="2px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 2px; font-weight: normal; hyphens: auto; line-height: 2px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                                                </td>
                                                                            </tr>

                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                        </tr>
                                                        <%} %>
                                                    </table>
                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">

                                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                        <tbody>

                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: bottom; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Total:
                                                                                    </strong>
                                                                                    <strong style="font-size: 14px; font-weight: 600">  <%=currency %>  <%=flightItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup + p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                                                                p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount).ToString("N"+ decimalPoint) %></strong>

                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>





                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <%if(flightItinerary.Passenger[0].FlexDetailsList != null){ %>
                                    <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Reporting Fields:</strong>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <%foreach (FlightFlexDetails flex in flightItinerary.Passenger[0].FlexDetailsList){ %>
                                            <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <%=flex.FlexLabel %>:
                                                </td>
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <%=flex.FlexData %>
                                                </td>
                                            </tr>
                                            <%} %>
                                        </tbody>
                                    </table>
                                    <%} %>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </td>
        </tr>
    </table>
        </div>
    </div>


    <div class="clearfix"></div>
    <script type="text/javascript">
        if (document.getElementById('ctl00_lnkgoogleapi') != null) {
            $('#ctl00_lnkgoogleapi').attr("href", '<%=Request.Url.Scheme%>://ctb2bstage.cozmotravel.com');
        }
        
    </script>


    <!-- EOF: body_container DIV-->
    <%}
        %>






    
    <div id='EmailDiv' runat='server' style='width: 100%; display: none;'>
        <%try
          {
              if (isBookingSuccess)
              {
                  List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                  if (ticketList != null && ticketList.Count > 0)
                  {
                      ptcDetails = ticketList[0].PtcDetail;
                  }
                  else// For Hold Bookings
                  {
                      ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
                  }

        %>
        <%//Show the Agent details for First Email only. In case of Corporate booking second & third 
            //emails need not show Agent details in the email body
            if (emailCounter <= 1)
            { %>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                    <asp:Image ID='imgLogo' runat='server' />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 70%;">
                </td>
                <td align="right">
                    <label style="padding-right: 20%">
                        <strong>Agent Name:</strong></label>
                </td>
                <td align="left">
                    <label style="padding-right: 10%">
                        <strong>
                            <%=agency.Name%></strong></label>
                </td>
            </tr>
            <tr>
                <td style="width: 70%;">
                </td>
                <td align="right">
                    <label style="padding-right: 20%">
                        <strong>Phone No:</strong></label>
                </td>
                <td align="left">
                    <label style="padding-right: 10%">
                        <strong>
                            <%=agency.Phone1%></strong></label>
                </td>
            </tr>
        </table>
        <% }
            for (int count = 0; count < flightItinerary.Segments.Length; count++)
            {
                //int paxIndex = 0;


                List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
        %>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    <div style='border: solid 1px #000'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td width='25%' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'><strong>Flight :
                                        <%=flightItinerary.Segments[count].Origin.CityName%>
                                        to
                                        <%=flightItinerary.Segments[count].Destination.CityName%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'><strong>RESERVATION</strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'><strong>
                                        <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'><strong>Airline Ref :
                                        <%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : (flightItinerary.Segments[count].AirlinePNR.Split('|').Length > 1 ? flightItinerary.Segments[count].AirlinePNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.Segments[count].AirlinePNR))%>
                                    </strong></font>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd;
                                    margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='padding-left: 20px'>
                                                <strong>Passenger Name's :</strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if (ticketList != null && ticketList.Count > 0)
                                              { %>
                                            <strong>Ticket No </strong>
                                            <%}
                                              else
                                              { // For Hold Bookings%>
                                            <strong>PNR </strong>
                                            <%} %>
                                        </td>
                                        <td style='width: 25%; float: left; height: 20px; line-height: 20px;'>
                                            <strong>Baggage </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd;
                                    margin-top: 2px; height: 20px; line-height: 20px;'>
                                    <tr>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <%if (ticketList != null && ticketList.Count > 0)
                                                  { %>
                                                <strong>Ticket Date:</strong>
                                                <%=ticketList[0].IssueDate.ToString("dd/MM/yyyy")%>
                                                <%}
                                                  else
                                                  {//for Corporate HOLD Booking %>
                                                <strong>Booking Date:</strong>
                                                <%=flightItinerary.CreatedOn.ToString("dd/MM/yyyy")%>
                                                <%} %>
                                            </label>
                                        </td>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <strong>Reference No :</strong>
                                                <%=booking.BookingId%></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
              { %>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd;
                                    margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='margin-left: 20px; padding: 2px; background: #EBBAD9;'>
                                                <strong>
                                                    <%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%>
                                                </strong>
                                            </label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if (ticketList != null && ticketList.Count > 0)
                                              { %>
                                            <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber)%>
                                            <%}
                                              else
                                              { //for Corporate HOLD Booking%>
                                            <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%>
                                            <%} %>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px; text-align: right;'>
                                            <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
                                              {

                                                  if (ptcDetail.Count > 0)
                                                  {
                                                      string baggage = "";
                                                      foreach (SegmentPTCDetail ptc in ptcDetail)
                                                      {
                                                          switch (flightItinerary.Passenger[j].Type)
                                                          {
                                                              case PassengerType.Adult:
                                                                  if (ptc.PaxType == "ADT")
                                                                  {
                                                                      baggage = ptc.Baggage;
                                                                  }
                                                                  break;
                                                              case PassengerType.Child:
                                                                  if (ptc.PaxType == "CNN")
                                                                  {
                                                                      baggage = ptc.Baggage;
                                                                  }
                                                                  break;
                                                              case PassengerType.Infant:
                                                                  if (ptc.PaxType == "INF")
                                                                  {
                                                                      baggage = ptc.Baggage;
                                                                  }
                                                                  break;
                                                          }
                                                      }

                                                      if (baggage.Length > 0 && !baggage.Contains("Bag") && !baggage.ToLower().Contains("piece") && !baggage.ToLower().Contains("unit") && !baggage.ToLower().Contains("units"))
                                                      {%>
                                            <%=(baggage.ToLower().Contains("kg") ? baggage : baggage + " Kg")%>
                                            <%}
                                                           else
                                                           {%>
                                            <%=baggage%>
                                            <%}
                                                       }//End PtcDetail.Count

                                                   }
                                              else if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet ||  flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                              {
                                                  if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                  { %>
                                            <%=flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group]%>
                                            <%}
                                                   }
                                              else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                              {
                                                  if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                  {%>
                                            <%=flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group]%>
                                            <%}
                                                   }
                                              else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
                                              {
                                                  if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                  {
                                                      string strBaggage = string.Empty;
                                                      if (flightItinerary.Passenger[j].BaggageCode != string.Empty && flightItinerary.Passenger[j].BaggageCode.Split(',').Length > 1)
                                                      {
                                                          strBaggage = flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group];
                                                      }
                                                      else if (flightItinerary.Passenger[j].BaggageCode.Split(',').Length <= 1)
                                                      {
                                                          strBaggage = flightItinerary.Passenger[j].BaggageCode;
                                                      }
                                                      else
                                                      {
                                                          strBaggage = "Airline Norms";
                                                      }
                                            %>
                                            <%=strBaggage%>
                                            <%}
                                                   } %>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd;
                                    margin-top: 2px; height: 20px; line-height: 20px'>
                                    <tr>
                                        <td style='width: 50%'>
                                            <label style='padding-left: 20px'>
                                                <strong>PNR No : </strong>
                                                <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%></label>
                                        </td>
                                        <%if (!string.IsNullOrEmpty(flightItinerary.TripId))
                                          { %>
                                        <td style='width: 50%; text-align: right'>
                                            <strong>Corporate Booking Code</strong>
                                            <%=flightItinerary.TripId%>
                                        </td>
                                        <%} %>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left'>
                                <label style='padding-left: 20px'>
                                    Date:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Status:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 40%; float: left;'>
                                <label style='padding-left: 20px; color: #08bd48;'>
                                    <strong>
                                        <%=booking.Status.ToString()%></strong></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Departs:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("hh:mm tt")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Arrives:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].ArrivalTime.ToString("hh:mm tt")%>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airline:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(flightItinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Flight:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Airline + " " + flightItinerary.Segments[count].FlightNumber%>
                                    <%try
                                      {  //Loading Operating Airline and showing
                                          if ((!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)||(flightItinerary.FlightBookingSource == BookingSource.Indigo && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0 && flightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                          {
                                              string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                              CT.Core.Airline opAirline = new CT.Core.Airline();
                                              if (opCarrier.Split('|').Length > 1)
                                              {
                                                  opAirline.Load(opCarrier.Split('|')[0]);
                                              }
                                              else
                                              {
                                                  opAirline.Load(opCarrier.Substring(0, 2));
                                              } %>
                                    (Operated by
                                    <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%>
                                    <%=opAirline.AirlineName.ToUpper()%>)
                                    <%}
                                       }
                                      catch { } %>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    From:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Origin.CityName + ", " + flightItinerary.Segments[count].Origin.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Origin.AirportName + ", " + flightItinerary.Segments[count].DepTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    To:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Destination.CityName + ", " + flightItinerary.Segments[count].Destination.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Destination.AirportName + ", " + flightItinerary.Segments[count].ArrTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Class:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].CabinClass%></label>
                                <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                  {%>
                                <label style='padding-left: 20px'>
                                    <label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                        width: 25%;'>
                                        Fare Type :
                                    </label>
                                    <%=flightItinerary.Segments[count].SegmentFareType%></label>
                                <%} %>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Duration:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Duration%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <%} %>
        </table>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0,
                            SeatPref = 0, MealPrice = 0;

                      if (ticketList != null && ticketList.Count > 0)
                      {
                          for (int k = 0; k < ticketList.Count; k++)
                          {
                              AirFare += ticketList[k].Price.PublishedFare;
                              Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                              if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                              {
                                  Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                              }
                              Baggage += ticketList[k].Price.BaggageCharge;
                              MealPrice += ticketList[k].Price.MealCharge;
                              SeatPref += ticketList[k].Price.SeatPrice;
                              MarkUp += ticketList[k].Price.Markup;
                              Discount += ticketList[k].Price.Discount;
                              AsvAmount += ticketList[k].Price.AsvAmount;
                          }
                          if (ticketList[0].Price.AsvElement == "BF")
                          {
                              AirFare += AsvAmount;
                          }
                          else if (ticketList[0].Price.AsvElement == "TF")
                          {
                              Taxes += AsvAmount;
                          }
                      }
                      else
                      {
                          for (int k = 0; k < flightItinerary.Passenger.Length; k++)
                          {
                              AirFare += flightItinerary.Passenger[k].Price.PublishedFare;
                              Taxes += flightItinerary.Passenger[k].Price.Tax + flightItinerary.Passenger[k].Price.Markup;
                              if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                              {
                                  Taxes += flightItinerary.Passenger[k].Price.AdditionalTxnFee + flightItinerary.Passenger[k].Price.OtherCharges + flightItinerary.Passenger[k].Price.SServiceFee + flightItinerary.Passenger[k].Price.TransactionFee;
                              }
                              Baggage += flightItinerary.Passenger[k].Price.BaggageCharge;
                              MealPrice += flightItinerary.Passenger[k].Price.MealCharge;
                              SeatPref += flightItinerary.Passenger[k].Price.SeatPrice;
                              MarkUp += flightItinerary.Passenger[k].Price.Markup;
                              Discount += flightItinerary.Passenger[k].Price.Discount;
                              AsvAmount += flightItinerary.Passenger[k].Price.AsvAmount;
                              if (flightItinerary.Passenger[k].Price.AsvElement == "BF")
                              {
                                  AirFare += AsvAmount;
                              }
                              else if (flightItinerary.Passenger[k].Price.AsvElement == "TF")
                              {
                                  Taxes += AsvAmount;
                              }
                          }
                      }
                    %>
                    <% if (agencyId <= 1)
                       {
                                  
                    %>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'>
                                            </td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Taxes.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || flightItinerary.IsLCC || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr><tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Meal Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=MealPrice.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr><tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Seat Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=SeatPref.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                    <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                      { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage + MealPrice + SeatPref) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                    <%=((AirFare + Taxes + Baggage + MealPrice + SeatPref) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%} %>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <%}
          }
          catch { } %>
    </div>
    <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
        <%if (flightItinerary != null) %>
        <%{%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        </head>
        <body>
            <div style="margin: 0; background: #f3f3f3!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; min-width: 100%;
                padding: 0; text-align: left; width: 100%!important">
                <span style="color: #f3f3f3; display: none!important; font-size: 1px; line-height: 1px;
                    max-height: 0; max-width: 0; overflow: hidden"></span>
                <table style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                    border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                    font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                    vertical-align: top; width: 100%">
                    <tr style="padding: 0; text-align: left; vertical-align: top">
                        <td align="center" valign="top" style="margin: 0; border-collapse: collapse!important;
                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                            line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                            word-wrap: break-word">
                            <center style="min-width: 580px; width: 100%">
                                <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                    float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                    vertical-align: top; width: 700px" align="center">
                                    <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                            <td style="margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                                                vertical-align: top; word-wrap: break-word">
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <img src="<% =agentImageSource%>" style="clear: both; display: block; max-width: 100%;
                                                                                outline: 0; text-decoration: none; width: auto">
                                                                        </th>
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                            width: 0">
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">

                                                                             <%if (!string.IsNullOrEmpty(emailTo) && emailTo == "E") %>
                                                                                <%{ %>
                                                                            <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left; word-wrap: normal">
                                                                             

                                                                                Dear
                                                                                <%=empName%>
                                                                                ,Employee Id :<% =empId%>
                                                                                ,</h1>

                                                                            
                                                                            <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left">
                                                                                Following are your trip details</p>
                                                                            <%} %>


                                                                            <%if (!string.IsNullOrEmpty(emailTo) && emailTo == "A") %>
                                                                                <%{ %>
                                                                            <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left; word-wrap: normal">
                                                                             

                                                                                Dear
                                                                                <% =approverNames%> Below are the trip details for <%=empName %>
                                                                                ,Employee Id :<% =empId%>
                                                                                ,</h1>
                                                                            <%} %>


                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table style="background: #fff; border-collapse: collapse; border-radius: 10px; border-spacing: 0;
                                                                                color: #024457; font-size: 11px; margin: 1em 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tr style="border: 1px solid #666; padding: 0; text-align: left; vertical-align: top">
                                                                                    <th width="20%" style="margin: 0; background-color: #666; border: 1px solid #666;
                                                                                        color: #fff; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Airline
                                                                                    </th>
                                                                                    <th width="20%" style="margin: 0; background-color: #666; border: 1px solid #666;
                                                                                        color: #fff; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Departure
                                                                                    </th>
                                                                                    <th width="20%" style="margin: 0; background-color: #666; border: 1px solid #666;
                                                                                        color: #fff; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Arrival
                                                                                    </th>
                                                                                    <th width="10%" style="margin: 0; background-color: #666; border: 1px solid #666;
                                                                                        color: #fff; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Stops
                                                                                    </th>
                                                                                    <th width="10%" style="margin: 0; background-color: #666; border: 1px solid #666;
                                                                                        color: #fff; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Duration
                                                                                    </th>
                                                                                    <th width="20%" style="margin: 0; background-color: #666; border: 1px solid #666;
                                                                                        color: #fff; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Price
                                                                                    </th>
                                                                                </tr>
                                                                                
                                                                                <%for (int count = 0; count < flightItinerary.Segments.Length; count++) %>
                                                                                        <%{
                                                                                              string rootFolder = Request.Url.Scheme +"://ctb2bstage.cozmotravel.com/" + Airline.logoDirectory + "/";
                                                                                              airline.Load(flightItinerary.Segments[count].Airline);
                                                                                              string imgsrc = rootFolder + airline.LogoFile;
                                                                                        %>
                                                                                <tr style="border: 1px solid #666; padding: 0; text-align: left; vertical-align: top">
                                                                                    <td colspan="6" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        
                                                                                        <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                            vertical-align: top; width: 100%">
                                                                                            <tr style="background-color: transparent; border: 0; padding: 0; text-align: left;
                                                                                                vertical-align: top">
                                                                                                <td width="20%" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                                    margin: 0; padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                                    <img src="<%=imgsrc%>" alt="" style="clear: both; display: block; max-width: 100%;
                                                                                                        outline: 0; text-decoration: none; width: auto" />
                                                                                                    <%=airline.AirlineName%><br>
                                                                                                    <%=flightItinerary.Segments[count].Airline + " " + flightItinerary.Segments[count].FlightNumber%>
                                                                                                </td>
                                                                                                <td width="20%" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                                    margin: 0; padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                                    <strong>
                                                                                                        <%=flightItinerary.Segments[count].Origin.CityName%>
                                                                                                    </strong>
                                                                                                    <br>
                                                                                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%>,<%=flightItinerary.Segments[count].DepartureTime.ToString("hh:mm tt")%>,
                                                                                                    <br>
                                                                                                    <%if (!string.IsNullOrEmpty(flightItinerary.Segments[count].DepTerminal) && flightItinerary.Segments[count].DepTerminal.Trim().Length > 0)
                                                                                                      { %>
                                                                                                    <%= "Airport:" + flightItinerary.Segments[count].Origin.AirportName + ", Terminal: " + flightItinerary.Segments[count].DepTerminal%>
                                                                                                    <%}
                                                                                                      else
                                                                                                      { %>
                                                                                                    <%="Airport:" + flightItinerary.Segments[count].Origin.AirportName%>
                                                                                                    <%} %>
                                                                                                </td>
                                                                                                <td width="20%" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                                    margin: 0; padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                                    <strong>
                                                                                                        <%=flightItinerary.Segments[count].Destination.CityName%>
                                                                                                    </strong>
                                                                                                    <br>
                                                                                                    <%=flightItinerary.Segments[count].ArrivalTime.ToString("MMM dd yyyy (ddd)")%>,<%=flightItinerary.Segments[count].ArrivalTime.ToString("hh:mm tt")%>,
                                                                                                    <br>
                                                                                                    <%if (!string.IsNullOrEmpty(flightItinerary.Segments[count].ArrTerminal) && flightItinerary.Segments[count].ArrTerminal.Trim().Length > 0)
                                                                                                      { %>
                                                                                                    <%= "Airport:" + flightItinerary.Segments[count].Destination.AirportName + ", Terminal: " + flightItinerary.Segments[count].ArrTerminal%>
                                                                                                    <%}
                                                                                                      else
                                                                                                      { %>
                                                                                                    <%="Airport:" + flightItinerary.Segments[count].Destination.AirportName%>
                                                                                                    <%} %>
                                                                                                </td>
                                                                                                <td width="10%" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                                    margin: 0; padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                                    Non Stop<br>
                                                                                                </td>
                                                                                                <td width="10%" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                                    margin: 0; padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                      
                                                                                                        <%=flightItinerary.Segments[count].Duration%></strong>
                                                                                                </td>
                                                                                                <%if (count == 0) %>
                                                                                                <%{ %>
                                                <td width="20%" align="center" valign="middle" style="margin: 0; border-collapse: collapse!important;
                                                                                                    border-left: 1px solid #d9e4e6; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                    font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 7px 7px;
                                                                                                    text-align: center; vertical-align: middle; word-wrap: break-word">
                                                                                                    <strong>
                                                                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                                                                          { %>
                                                                                                        <%=agency.AgentCurrency%>
                                                                                                        <%=Math.Ceiling((AirFare + Taxes + Baggage + MealPrice + SeatPref) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                                                        <%}
                                                                                                          else
                                                                                                          { %>
                                                                                                        <%=agency.AgentCurrency%>
                                                                                                        <%=((AirFare + Taxes + Baggage + MealPrice + SeatPref) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                                                        <%} %></strong><br />
                                                                                                </td>
                                                                                                <%} %>
                                                                                                
                                                                                                <%else %>
                                                                                                <%{ %>
                                                                                                <td width="20%" align="center" valign="middle" style="margin: 0; border-collapse: collapse!important;
                                                                                                    border-left: 1px solid #d9e4e6; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                    font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 7px 7px;
                                                                                                    text-align: center; vertical-align: middle; word-wrap: break-word">
                                       </td>
                                                                                                
                                                                                                
                                                                                                <%} %>
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                            </tr>
                                                                                           
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                 <%} %>
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </table>
                <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                </div>
            </div>
        </body>
        </html>
        <%} %>
    </div>
  
  
  
  
  
  <style> 
  
  a.flight-sort-btn:hover { color:inherit }
  
  </style>
    
  
</asp:Content>
