﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="OfflineBookingQueue.aspx.cs" Inherits="CozmoB2BWebApp.OfflineBookingQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

<link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="scripts/paginathing.js" type="text/javascript"></script>
<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
<script src="scripts/jquery-ui.js"></script>

<script type="text/javascript">
        
    /* Global variables */
    var Recordsperpage = 10; Pagenodisplay = 5, TotalRecords = 0;
    var LoginAgentDetails; var SearchList;

    $(document).ready(function () {
            
        LoginAgentDetails = JSON.parse(document.getElementById('<%=hdfAgent.ClientID %>').value);
        BindAllControls();

        var FromDate = new Date(Date.now);
        $("#txtFromDate").datepicker({
            dateFormat: 'dd-mm-yy',
            onSelect: function (dateText, inst) {
                    
                var selectedDate = new Date(dateText);
                selectedDate.setDate(selectedDate.getDate() + 1);
                $("#txtToDate").datepicker("option", "minDate", selectedDate);
                var fromDate = new Date($("#txtFromDate").datepicker("getDate"));
                var toDate = new Date($("#txtToDate").datepicker("getDate"));
                if (fromDate > toDate) {
                    $("#txtToDate").datepicker("setDate", selectedDate);
                }
            },
            onchange: function (dateText, inst) { }
        }).datepicker("setDate", FromDate);

        FromDate.setDate(FromDate.getDate() + 1);
        $("#txtToDate").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: FromDate,
            maxDate: 0
        }).datepicker("setDate", new Date(FromDate));   
        Search();
    });

    /* To show and hide filter options on the screen */
    function ShowHide(divParam) {

        var display = document.getElementById(divParam).style.display;
        document.getElementById(divParam).style.display = display == 'block' ? 'none' : 'block';
        document.getElementById('ancParam').innerHTML = display == 'block' ? 'Show Param' : 'Hide Param';
        return false;
    }

    /* To Bind Filter Drop downs */
    function BindAllControls() {

        var SearchList = JSON.parse(AjaxCall('OfflineBookingQueue.aspx/BindPageLoadControls', ''));

        var options = "";

        /* AgentDropDown */
        for (var i = 0; i < SearchList.Table.length; i++) {
            options += '<option value="' + SearchList.Table[i]['AGENT_ID'] + '">' + SearchList.Table[i]['AGENT_NAME'] + '</option>';
        }
        $('#ddlAgency').empty();
        $('#ddlAgency').append(options); 
        options = "";

        /* B2Bdropdown */
        for (var i = 0; i < SearchList.Table1.length; i++) {
            options += '<option value="' + SearchList.Table1[i]['AGENT_ID'] + '">' + SearchList.Table1[i]['AGENT_NAME'] + '</option>';
        }
        $('#ddlB2BAgent').empty();
        $('#ddlB2BAgent').append(options);
        $("#ddlB2BAgent").select2("val", LoginAgentDetails.AgentId);
        options = "";

        /* B2B2BdropDown */
        for (var i = 0; i < SearchList.Table2.length; i++) {
            options += '<option value="' + SearchList.Table2[i]['AGENT_ID'] + '">' + SearchList.Table2[i]['AGENT_NAME'] + '</option>';
        }
        $('#ddlB2B2BAgent').empty();
        $('#ddlB2B2BAgent').append(options);
         $("#ddlB2B2BAgent").select2("val", LoginAgentDetails.AgentId);
        options = "";

        /* location dropdown */
        for (var i = 0; i < SearchList.Table3.length; i++) {
            options += '<option value="' + SearchList.Table3[i]['LOCATION_ID'] + '">' + SearchList.Table3[i]['LOCATION_NAME'] + '</option>';
        }
        $('#ddlLocation').empty();
        $('#ddlLocation').append(options);
        $("#ddlLocation").prepend("<option value='0' >---ALL---</option>");
        $("#ddlLocation").select2().select2('val', LoginAgentDetails.LocationId);
        options = "";

        if (LoginAgentDetails.AgentType == "BaseAgent" || LoginAgentDetails.AgentType == "Agent") {
            if (LoginAgentDetails.AgentType == "Agent") {
                $("#ddlAgency").select2("val", LoginAgentDetails.AgentId);
                $("#ddlAgency").prop("disabled", true);
            }
            $("#ddlAgency").select2("val", LoginAgentDetails.AgentId);
              $("#ddlAgency").prepend("<option value='0' >---ALL---</option>");
            $("#ddlB2BAgent").prepend("<option value='0' >---ALL---</option>");
            $("#ddlB2BAgent").prepend("<option value='-1' >---Select---</option>");
            $("#ddlB2BAgent").select2("val", "-1");
            $("#ddlB2B2BAgent").prepend("<option value='0' >---ALL---</option>");
            $("#ddlB2B2BAgent").prepend("<option value='-1' >---Select---</option>");
            $("#ddlB2B2BAgent").select2("val", "-1");
        }
        else if ((LoginAgentDetails.AgentType == "B2B")) {
            $("#ddlB2B2BAgent").prepend("<option value='-1' >---Select---</option>");
            $("#ddlB2B2BAgent").prepend("<option value='0' >---ALL---</option>");
            $("#ddlB2B2BAgent").select2("val", "-1");
            $('#ddlAgency').select2('val', $('#ddlAgency option:eq(0)').val());
            $("#ddlAgency").prop("disabled", true);
            $("#ddlB2BAgent").prop("disabled", true);

        }
        else if (LoginAgentDetails.AgentType == "B2B2B") {
            $('#ddlB2BAgent').select2('val', $('#ddlB2BAgent option:eq(0)').val());
             $('#ddlAgency').select2('val', $('#ddlAgency option:eq(0)').val());
            $("#ddlAgency").prop("disabled", true);
            $("#ddlB2BAgent").prop("disabled", true);
            $("#ddlB2B2BAgent").prop("disabled", true);
        }
        if (LoginAgentDetails.MemberType == "ADMIN" || LoginAgentDetails.MemberType == "SUPER") {
            $("#ddlLocation").prop("disabled", false);
        }
        else {
            $("#ddlLocation").prop("disabled", true);
        }
        if ($("#ddlB2BAgent").val() < 0) {
            $("#ddlB2B2BAgent").prop("disabled", true);
        }
    }

    /* To Bind Agent Drop downs */
    function BindAgents(AgentId, AgentType) {
            
        var Agent_id = "";
        var type = "";
        var B2BAgent_id = $('#ddlB2BAgent').val();
        var B2B2BAgent_id = $('#ddlB2B2BAgent').val();
        if (AgentType == "B2B") {
            Agent_id = $('#ddlAgency').val();
            if ($('#ddlB2BAgent').val() < 0) {
                $('#ddlB2B2BAgent').prop('disabled', true);
            }
            if (Agent_id == 0) {
                type = "BASE";
            }
        }
        else if (AgentType == "B2B2B") {
            Agent_id = $('#ddlB2BAgent').val();
            if (Agent_id >= "0") {
                $('#ddlB2B2BAgent').prop('disabled', false);
            }
            else {
                $('#ddlB2B2BAgent').prop('disabled', true);
            }
            if (Agent_id == " 0") {

                if ($('#ddlAgency').val() > "1") {
                    type = "AGENT";/*AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations*/
                    Agent_id = $('#ddlAgency').val();
                }
                else {
                    type = "B2B"; /*B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations*/
                    Agent_id = $('#ddlAgency').val();
                }
            }
            else {
                if (Agent_id == "-1") {
                    Agent_id = $('#ddlAgency').val();
                    if ($('#ddlAgency').val() == " 0") {
                        type = "BASE";/* BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations*/
                    }
                }
            }
            if ($('#ddlAgency').val() == " 0") {
                if (Agent_id == " 0") {
                    type = "BASEB2B"; /*BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations*/
                }
            }
        }
        if (AgentType == "") {
            Agent_id = $('#ddlB2B2BAgent').val();
            if (Agent_id == "0") {
                type = "B2B2B"; /*B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations*/
                Agent_id = $('#ddlB2BAgent').val();
                if (Agent_id == "0") {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (Agent_id == "-1") {
                Agent_id = $('#ddlB2BAgent').val();
                if (Agent_id == "0") {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    Agent_id = $('#ddlAgency').val();
                }
            }
            if ($('#ddlAgency').val() == "0") {
                if ($('#ddlB2BAgent').val() == " 0") {
                    if ($('#ddlB2B2BAgent').val() == "0") {
                        type = "";
                    }
                }
            }
            if ($('#ddlAgency').val() != "0") {
                if ($('#ddlB2BAgent').val() == "0") {
                    if (('#ddlB2B2BAgent').val() == "0") {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        Agent_id = $('#ddlAgencyD').val();
                    }
                }
            }
        }

        if (AgentType != "") {
            var options = "";
            options += "<option value='-1'>--Select--</option><option value='0'>--All--</option>";

            var data = AjaxCall('OfflineBookingQueue.aspx/BindAllAgentTypes', "{'agentId':'" + AgentId + "','AgentType':'" + AgentType + "'}");

            $.each(data, function (item) {
                options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
            });

            if (AgentType == "B2B") {
                $('#ddlB2BAgent').empty();
                $('#ddlB2BAgent').append(options);
                $('#ddlB2BAgent').select2('val', '-1');
            }
            else if (AgentType == "B2B2B") {
                $('#ddlB2B2BAgent').empty();
                $('#ddlB2B2BAgent').append(options);
                $('#ddlB2B2BAgent').select2('val', '-1');
            }
        }
        BindLocation(Agent_id, type);
    }

    /* To Bind Agent Locations Drop down */
    function BindLocation(Agent_id, Type) {
        var options = "";
        options += "<option value='0'>--All--</option>";

        var data = AjaxCall('OfflineBookingQueue.aspx/BindLocation', "{'agentId':'" + Agent_id + "','type':'" + Type + "'}");

        $.each(data, function (item) {
            options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
        });

        $('#ddlLocation').empty();
        $('#ddlLocation').append(options);
        $('#ddlLocation').select2('val', '0');
    }

    /* To search data based on filter options */
    function Search() {

        $("#ctl00_upProgress").show();

        //AgentFilter Starts
        var AgentFilter = $('#ddlAgency').val();
        var agentType = "";
        if (AgentFilter == "0") {
            agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
            if ($('#ddlB2BAgent').val() == "0") {
                agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
            }
            if ($('#ddlB2B2BAgent').val() == "0") {
                agentType = ""; // null Means binding in list all BOOKINGS
            }
        }
        if (AgentFilter > "0" && $('#ddlB2BAgent').val() != "-1") {
            if (AgentFilter > 1) {
                if ($('#ddlB2BAgent').val() == "0") {

                    agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                }
                else {
                    AgentFilter = $('#ddlB2BAgent').val();
                }
            }
            else {
                if ($('#ddlB2BAgent').val() == "0") {

                    agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                }
                AgentFilter = $('#ddlB2BAgent').val();
            }
        }
        if (AgentFilter > "0" && $('#ddlB2B2BAgent').val() != "-1") {
            if ($('#ddlB2B2BAgent').val() == "0") {
                agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
            }
            else {
                AgentFilter = $('#ddlB2B2BAgent').val();
            }
        }
        if ($('#ddlAgency').val() != "0") {
            if ($('#ddlB2BAgent').val() == "0") {
                if ($('#ddlB2B2BAgent').val() == "0") {
                    agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                    AgentFilter = $('#ddlAgency').val();
                }
            }
        }
        //AgentFilter End
        
        var obj = {};
        obj.FromDate = $('#txtFromDate').val();
        obj.ToDate = $('#txtToDate').val();
        obj.LocationId = $('#ddlLocation').val();
        obj.AgentFilter = AgentFilter;
        obj.agentType = agentType;
        obj.productId = $('#ddlProduct').val();

        SearchList = JSON.parse(AjaxCall('OfflineBookingQueue.aspx/Search', JSON.stringify(obj)));

        TotalRecords = $('#ddlProduct').val() == "1" ? SearchList.Table.length : SearchList.length;                    
        $('.panel-footer').remove();

        if (TotalRecords > 0) {

            $('#NoRecords').hide();
            if (TotalRecords > Recordsperpage) {
                
                $('#list-group').paginathing({
                    limitPagination: (TotalRecords / Recordsperpage) > Pagenodisplay ? Pagenodisplay : Math.ceil(TotalRecords / Recordsperpage),
                    containerClass: 'panel-footer',
                    pageNumbers: true,
                    prevNext: (TotalRecords / Recordsperpage) > Pagenodisplay,
                    firstLast: (TotalRecords / Recordsperpage) > Pagenodisplay,
                    totalRecords: TotalRecords,
                    perPage:Recordsperpage
                });          
            }            
            LoadData(1);
            ShowHide('divParam');
        }
        else {
            $('#list-group').children().remove();
            $('#HeaderPagination').children().remove();
            $('#NoRecords').show();
        }

        $("#ctl00_upProgress").hide();
    }

    /* To load selected page data for both flight and hotel */
    function LoadData(page) {

        $('#list-group').children().remove();            
        var showFrom = ((Math.ceil(page) - 1) * Recordsperpage);
        var showTo = Math.ceil(showFrom) + (Math.ceil(Recordsperpage) - 1);
        showTo = showTo >= TotalRecords ? (Math.ceil(TotalRecords) - 1) : showTo;

        if ($('#ddlProduct').val() == "1") 
            BindFlightData(showFrom, showTo);            
        else 
            BindHotelData(showFrom, showTo);
    }

    /* To bind flight search results to flight html variables */
    function BindFlightData(showFrom, showTo) {

        var orgqueueTemplate = $('#divFlightHTML').html();

        for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {

            var List = SearchList.Table1.filter(function (item) {
                return (item.BookingRefNo == SearchList.Table[i].BookingRefNo);
            });

            if (List != null && List.length > 0) {

                $('#list-group').append('<li id="List' + i + '">' + orgqueueTemplate + '</li>');
                var resn = SearchList.Table[i].TravelReason != null && SearchList.Table[i].TravelReason != '' ? SearchList.Table[i].TravelReason + ',' : '';
                AssignData('FLBookedAgent', i, 'Trip ' + (i + 1) + '. ' + resn + ' Supplier(Booked By ' +
                    SearchList.Table[i].agencyId + ')');

                AssignData('FLBookDate', i, FormatDate(List[0].createdOn));
                AssignData('FLBookingRefNo', i, 'Booking Code :' + SearchList.Table[i].BookingRefNo);
                AssignData('FLleadpax', i, SearchList.Table[i].leadpaxName);

                AssignData('Flightopen', i, '');
                $('#Flightopen-' + i).append('<a id="Open-' + i + '" class="btn but_b pull-right" href="ViewOfflineBooking.aspx?ref=' +
                    SearchList.Table[i].OFLFLTId + '& reftype=1">View Details</a>');

                BindFlightSegments(List, i);
            }
        }
    }

    /* To bind flight segments data to segment html variables */
    function BindFlightSegments(List, i) {
        
        var templateSegment = $('#divSegmentHTML').html();
        $('#list-group').find('#list-group-Segments').attr('id', 'list-group-Segments-' + i);

        for (j = 0; j < List.length; j++) {

            $('#list-group').find('#list-group-Segments-' + i).append('<li id="ListTypes' + i + j + '">' + templateSegment + '</li>');
            $('#FLimgAirLines').attr('id', 'FLimgAirLines-' + i + j);
            $('#FLairLinesName').attr('id', 'FLairLinesName-' + i + j);

            if (List[j].airlineCode != null && List[j].airlineCode.split(",").length > 1) {

                $('#FLimgAirLines-' + i + j).attr("src", 'Images/AirlineLogo/' + List[j].airlineCode.split(",")[1]);                                
                $('#FLairLinesName-' + i + j).text(List[j].airlineCode.split(",")[0] + '-' + List[j].flightNum);
            }

            AssignData('FLDepDate', '' + i + j, FormatDate(List[j].depDateTime) + ' ' + List[j].depAirportCode);
            AssignData('FLDepCity', '' + i + j, List[j].depAirportName);
            AssignData('FLArrDate', '' + i + j, FormatDate(List[j].arrDateTime) + ' ' + List[j].arrAirportCode);
            AssignData('FLArrCity', '' + i + j, List[j].arrAirportName);
        }
    }

    /* To bind hotel search results to hotel html variables */
    function BindHotelData(showFrom, showTo) {

        var orgqueueTemplate = $('#divHotelHTML').html();

        for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {

            $('#list-group').append('<li id="List' + i + '">' + orgqueueTemplate + '</li>');

            AssignData('HTLBookedAgent', i, 'Booked By ' + SearchList[i].agencyId);
            AssignData('HTLBookingRefNo', i, 'Booking Code :' + SearchList[i].BookingRefNo);
            AssignData('HTLleadpax', i, SearchList[i].leadpaxName);
            AssignData('HotelName', i, (SearchList[i].hotelName == null ? '' : SearchList[i].hotelName));
            AssignData('HTLconfirmationNo', i, (SearchList[i].confirmationNo != null ? SearchList[i].confirmationNo : ''));
            AssignData('HTLLocation', i, SearchList[i].LocationName);
            AssignData('HTLnoofRooms', i, SearchList[i].noOfRooms);
            AssignData('HTLcheckInDate', i, FormatDate(SearchList[i].checkInDate));
            AssignData('HTLcheckOutDate', i, FormatDate(SearchList[i].checkOutDate));
            AssignData('HTLBookedOn', i, FormatDate(SearchList[i].createdOn));

            $('#Hotelopen').attr('id', 'Hotelopen-' + i);
			$('#Hotelopen-' + i).append('<a id="Open-' + i + '" class="btn but_b pull-right" href="ViewOfflineBooking.aspx?ref=' + SearchList[i].HTLOFLId + '&reftype=2">View Details</a>');
        }
    }

    /* To show grid selected page as active and load the page data */
    function showselpage(event, pageno) {
        
        event.parentNode.className= 'page active';
        LoadData(pageno);
        //BindHeaderPagination();
    }

    /* To bind pagination tab on top of the grid */
    function BindHeaderPagination() {

        $('#HeaderPagination').children().remove();
        $('#HeaderPagination').append($('.panel-footer')[0] != null ? $('.panel-footer')[0].innerHTML : '');
        var nodes = $('#HeaderPagination')[0].childNodes[1].childNodes;
        $.each(nodes, function (key, node) {
            if (node.childNodes[0].attributes.length > 0)
                node.childNodes[0].attributes[0].nodeValue = 'Hdrpagingclick(this,' + key + ')';
        });
    }

    /* To call actual pagination click from header pagination container */
    function Hdrpagingclick(event, id) {

        var linodes = $('.panel-footer')[0].childNodes[0].childNodes;
        var ancclickfn = '', liclickfn = '';
        $.each(linodes, function (nodid, linode) {
            
            if (nodid == id) { ancclickfn = linode.childNodes[0]; liclickfn = linode; }                
        });
        ancclickfn.onclick();
        liclickfn.click();
    }

    /* Common function to format the date in ddMMMyyyy */
    function FormatDate(date) {

        var formdate = new Date(date);
        return formdate.toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' }).replace(/ /g, ' ');
    }

    /* Common function to set dynaic id's and data to grid variables */
    function AssignData(Mainid, childid, data) {

        $('#' + Mainid).attr('id', Mainid + '-' + childid);
        $('#' + Mainid + '-' + childid).text(data);
    }

</script>

<asp:HiddenField runat="server" ID="hdfAgent" Value="" />
<a style="cursor: default; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam" onclick="return ShowHide('divParam');">Hide Param</a>

<%--Filter optoins HTML --%>
<div title="Param" id="divParam" style="display:block">
    <div class="paramcon">
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2"> From Date: </div>
            <div class="col-md-2"> <table> <tr> <td> <input type="text" id="txtFromDate" class="inputEnabled form-control" /> </td> </tr> </table> </div>
            <div class="col-md-2"> To Date: </div>
            <div class="col-md-2"> <table> <tr> <td> <input type="text" id="txtToDate" class="inputEnabled form-control" /> </td> </tr> </table> </div>
            <div class="col-md-2"> Agent: </div> 
            <div class="col-md-2"> <select class="form-control" id="ddlAgency" onchange="javascript:BindAgents(this.value,'B2B');"></select> </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2"> Client (P): </div> 
            <div class="col-md-2"> <select class="form-control" id="ddlB2BAgent" onchange="javascript:BindAgents(this.value,'B2B2B');"></select> </div>
            <div class="col-md-2"> Client (C): </div>
            <div class="col-md-2"> <select class="form-control" id="ddlB2B2BAgent" onchange="javascript:BindAgents(this.value,'');"></select> </div>
            <div class="col-md-2"> Location: </div> <div class="col-md-2"> <select class="form-control" id="ddlLocation"></select> </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2" style="display: none;"> Booking Status: </div>
            <div class="col-md-2" style="display: none;"> <select class="form-control" id="ddlBookingStatus"></select> </div>
            <div class="col-md-2"> Product : </div>
            <div class="col-md-2"> <select class="form-control" id="ddlProduct"> <option value="1">Flight</option> <option value="2">Hotel</option> </select> </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-12 padding-0 marbot_10"> <input type="button" id="btnSearch" class="btn but_b pull-right" value="Search" onclick="Search();" /> </div>
    </div>
</div>

<div> <label id="NoRecords"><b>No Records Found!</b></label> </div>
<div id="HeaderPagination" style="float:right" class="pagmargin"> </div>
<div class="row"> <div class="col-md-12"> <ul id="list-group"> </ul> </div> </div>

<%--Flight Results View HTML--%>
<div id="divFlightHTML" style="display: none;">        
    <div>  <label class="font-weight-bold" id="FLBookedAgent"> </label> </div> 
    <div class="tbl queue-design">
        <div class="row">
            <div class="col-md-1 col-sm-1 col-2"> <img class="img-responsive aeroplane-icon" src="images/aeroplane.svg" />   </div>
            <div class="col-md-11 col-sm-11 col-10"> 
            <!--header-->
                <div class="row">
                    <%--<div class="col-md-6"> <label class="booking-da font-weight-bold" id="FLDateFromandto"></label> </div>--%>
                    <div class="col-md-6"> <label> <small class="text-muted">Booked:</small> <label class="font-weight-bold" id="FLBookDate"></label> </label> </div>
                    <div class="col-md-6 text-md-right"> <small> <label id="FLBookingRefNo"></label>  </small>  </div>
                    <div class="col-md-12"> <label> <small class="text-muted">Booked for:</small> <label class="font-weight-bold" id="FLleadpax"></label> </label> </div>
                </div>     
                <div class="row"> 
                    <div class="col-md-12"> 
                        <ul class="col-md-12 padding-0 marbot_10" id="list-group-Segments"></ul>
                        <div class="float-lg-right" id="Flightopen"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>          
</div>
<%--Flight Segment Results View HTML--%>
<div id="divSegmentHTML" style="display: none;">
    <div class="row mt-2 mb-4"> 
        <div class="col-md-12"> 
            <img class="float-left mr-2 mb-1" id="FLimgAirLines" src=""/> 
            <div> 
                <label class="font-weight-bold departure" id="FLDepDate"> </label> 
                <label class="destination text-small" id="FLDepCity"></label>  
                <label class="font-weight-bold arrival" id="FLArrDate"></label> 
                <label class="destination text-small" id="FLArrCity"></label>  
            </div>
            <div> <small> <label class="mr-md-4" id="FLairLinesName"></label> </small> </div>
        </div>
    </div>
</div>  
<%--Hotel Results View HTML--%>
<div id="divHotelHTML" style="display: none;">    
    <div> <label class="font-weight-bold" id="HTLBookedAgent"> </label>  </div>
    <div class="tbl queue-design-hotel">                                
        <div class="row">
            <div class="col-md-1 col-sm-1 col-2"> <img class="img-responsive bed-icon" src="images/sleeping-bed.svg" />   </div>
            <div class="col-md-11 col-sm-11 col-10"> 
                <div class="row">
                    <div class="col-md-4"> <label> <small class="text-muted">Booked for </small> <label class="font-weight-bold" id="HTLleadpax"></label> </label> </div>
                    <div class="col-md-4"> <small> <label id="HTLconfirmationNo"></label></small> </div>
                    <div class="col-md-4 text-md-right"> <small>  <label id="HTLBookingRefNo"></label> </small>   </div>
                </div>
                <div class="row mt-2"> 
                    <div class="col-md-4"> Hotel Name: <label class="font-weight-bold" id="HotelName"></label> </div>  
                    <div class="col-md-4"> CheckIn Date : <label class="font-weight-bold" id="HTLcheckInDate"></label>  </div> 
                    <div class="col-md-4"> CheckOut  Date : <label class="font-weight-bold" id="HTLcheckOutDate"></label>  </div>
                </div>
                <div class="row mt-3"> 
                    <div class="col-md-4"> Location : <label class="font-weight-bold" id="HTLLocation"></label> </div> 
                    <div class="col-md-4"> Noof Rooms :<label class="font-weight-bold" id="HTLnoofRooms"></label>  </div>
                    <div class="col-md-4">Booked On :  <label class="font-weight-bold" id="HTLBookedOn"></label>  </div>
                </div>
            </div>
        </div>
        <div class="row mt-2"> <div class="col-md-12"> <div class="float-lg-right" id="Hotelopen">  </div> </div> </div>        
    </div>
</div>
      
<style> 

    /*firoz 04 july 2019*/
    .queue-design img  { height:40px;  }
    .queue-design label { margin:0px;  }
    .queue-design .departure,.queue-design .arrival,.queue-design .booking-da  { font-size:16px;   }
    .queue-design .departure,.queue-design .arrival,.queue-design .destination  { width:21%;     }
    .queue-design .aeroplane-icon{ filter: invert(48%) sepia(79%) saturate(0%) hue-rotate(86deg) brightness(118%) contrast(119%);  }
    .queue-design-hotel .bed-icon{ filter: invert(48%) sepia(79%) saturate(0%) hue-rotate(86deg) brightness(118%) contrast(119%);  }
    .queue-design .text-small  { font-size:85%; }
    @media only screen and (max-width: 600px) { .queue-design .departure,.queue-design .arrival,.queue-design .destination  { float:left;    } }

    .pagmargin {

        padding: 10px 15px;
        background-color: #f5f5f5;
        border-top: 1px solid #ddd;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
 
</style>  
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
