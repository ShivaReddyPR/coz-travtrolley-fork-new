<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateGlobalVisaApprovalsQueueUI"
    Title="Corporate Global Visa Approvals Queue.aspx " Codebehind="CorporateGlobalVisaApprovalsQueue.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        function Validate() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById('ctl00_cphTransaction_dcReimFromDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select from date .";
            }
            else if (document.getElementById('ctl00_cphTransaction_dcReimToDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select to date.";
            }

            else {
                valid = true;
            }

            return valid;
        }
    </script>

    <h5>
        Corporate Global Visa Approval Queue</h5>
    <br />
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>
                    From Date <span class="fcol_red">*</span></label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <uc1:DateControl ID="dcReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                    </uc1:DateControl>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>
                    To Date <span class="fcol_red">*</span></label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <uc1:DateControl ID="dcReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>
                    Select Employee</label>
                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-2">
            <label class="center-block">
                &nbsp;</label>
            <asp:Button OnClientClick="return Validate();" runat="server" ID="btnSearch" OnClick="btnSearch_Click"
                CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <asp:GridView ID="gvVisaSales" Width="100%" runat="server" AllowPaging="true" DataKeyNames="fvs_id"
                    EmptyDataText="No Visa Sales List!" AutoGenerateColumns="false" PageSize="16"
                    GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvVisaSales_PageIndexChanging" OnRowDataBound="gvVisaSales_RowDataBound">
                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("Id") %>'>
                                </asp:HiddenField>
                                <asp:HiddenField ID="hdfExpDetailId" runat="server" Value='<%# Bind("EXPDETAIL_ID") %>'>
                                </asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="center" />
                            <HeaderTemplate>
                                <label style="color: Black">
                                    Select</label>
                                <asp:CheckBox runat="server" ID="HTchkSelectAll" AutoPostBack="true" OnCheckedChanged="ITchkSelect_CheckedChanged">
                                </asp:CheckBox>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="left" />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Doc.No</label>
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:LinkButton ID="ITlblDocNo" runat="server" Text='<%# Eval("fvs_doc_no") %>' ToolTip='<%# Eval("fvs_doc_no") %>'
                                    Width="80px" Style="color: Red" Font-Size="8pt"></asp:LinkButton>
                                <asp:HiddenField ID="IThdnfvsId" runat="server" Value='<%# Bind("fvs_id") %>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle VerticalAlign="top" />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Date</label>
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="ITlblDocDate" Width="125px" runat="server" Text='<%# IDDateTimeFormat(Eval("fvs_doc_date")) %>'
                                    CssClass="grdof" ToolTip='<%# Eval("fvs_doc_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label style="color: Black; font-weight: bold" class="width80">
                                    PSP Type</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblPSPType" runat="server" Text='<%# Eval("fvs_passport_type_name") %>'
                                    CssClass="grdof" ToolTip='<%# Eval("fvs_passport_type_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label style="color: Black; font-weight: bold" class="width80">
                                    Docket No</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblDocketNo" runat="server" Text='<%# Eval("fvs_docket_no") %>'
                                    CssClass="grdof width100" ToolTip='<%# Eval("fvs_docket_no") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <label style="color: Black; font-weight: bold" class="width80">
                                    Type Of Visa</label>
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="ITlblVisaType" runat="server" Text='<%# Eval("fvs_visa_type_name") %>'
                                    CssClass="grdof" ToolTip='<%# Eval("fvs_visa_type_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Country</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("countryName") %>' CssClass="grdof"
                                    ToolTip='<%# Eval("countryName") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Nationality</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("nationality") %>'
                                    CssClass="grdof" ToolTip='<%# Eval("nationality") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Residence</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblResidence" runat="server" Text='<%# Eval("FVS_RESIDENCE_NAME") %>'
                                    CssClass="grdof" ToolTip='<%# Eval("FVS_RESIDENCE_NAME") %>' Width="120px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
                <HeaderStyle />
                <HeaderTemplate>
                    <label class="filterHeaderText">
                        City Name</label>
               
                </HeaderTemplate>
                <ItemStyle />
                <ItemTemplate>
                    <asp:Label ID="ITlblCity" runat="server" Text='<%#Eval("city_name") %>' CssClass="grdof"
                        ToolTip='<%#Eval("city_name") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Visa Category</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblVisaCategory" runat="server" Text='<%# Eval("fvs_visa_category_name") %>'
                                    CssClass="grdof" ToolTip='<%# Eval("fvs_visa_category_name") %>' Width="120px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Visitor</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblVisitor" runat="server" Text='<%# Eval("PAX_NAME") %>' CssClass="grdof"
                                    ToolTip='<%# Eval("PAX_NAME") %>' Width="120px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField Visible="false">
                <HeaderStyle />
                <HeaderTemplate>
                    <label class="filterHeaderText">
                        App Center</label>
                   
                </HeaderTemplate>
                <ItemStyle />
                <ItemTemplate>
                    <asp:Label ID="ITlblAppCenter" runat="server" Text='<%# Eval("CENTER_NAME") %>' CssClass="grdof"
                        ToolTip='<%# Eval("CENTER_NAME") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderStyle VerticalAlign="top" />
                            <HeaderTemplate>
                                <label style="width: 70px; text-align: left; float: left" class="filterHeaderText">
                                    &nbsp;Tent Travel Date</label>
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="ITlblTravelDate" Width="125px" runat="server" Text='<%# IDDateTimeFormat(Eval("fvs_tent_travel_date")) %>'
                                    CssClass="grdof" ToolTip='<%# Eval("fvs_tent_travel_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                <HeaderStyle />
                <HeaderTemplate>
                    <label class="filterHeaderText">
                        Visa Status</label>
                </HeaderTemplate>
                <ItemStyle />
                <ItemTemplate>
                    <asp:Label ID="ITlblVisaStatus" runat="server" 
                        CssClass="grdof"  Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                        
                        
                        
                        <asp:TemplateField>
                            <HeaderStyle />
                            <HeaderTemplate>
                                <label class="filterHeaderText">
                                    Created By</label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblCreatedName" runat="server" Text='<%# Eval("FVS_CREATED_NAME") %>'
                                    CssClass="grdof" ToolTip='<%# Eval("FVS_CREATED_NAME") %>' Width="120px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
                <HeaderStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <asp:Label ID="HTlblStatus" CssClass="filterHeaderText" runat="server" Width="125px"
                        Text="Approve Status" ForeColor="Black"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:HiddenField ID="IThdnapproveStatus" runat="server" Value='<%# Bind("FVS_APPROVE_STATUS") %>'>
                    </asp:HiddenField>
                    <asp:DropDownList ID="ITddlStatus" runat="server" Enabled="false" Width="85px" Height="20px">
                        <asp:ListItem Value="Y" Text="Approved"></asp:ListItem>
                        <asp:ListItem Value="N" Text="Not Approved"></asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        
    </div>
    
    
    <div class="row">
    <div class="pad_10 col-md-12">
            <asp:Button CssClass="btn btn-primary btn-blue" OnClick="btnApprove_Click" runat="server"
                ID="btnApprove" Text="Approve" />
            <asp:Button OnClick="btnReject_Click" Style='background: #999;' CssClass="btn but_d btn_xs_block cursor_point"
                runat="server" ID="btnReject" Text="Reject" />
            <div class="clearfix">
            </div>
        </div>
    </div>
    
    
    <div id='EmailDivApprover' runat='server' style='width: 100%; display: none;'>
    <%if(detail != null) %>
    <%{ %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            </head>
            <body>
            
            <table>
            <tr>
            <td>
            Dear
       <%if (!string.IsNullOrEmpty(approverNames) && approverNames.Length > 0) %>
                                              <%{ %>
                                              
                          <%=approverNames %>
                                              <%} %>
            </td>
            </tr>
            </table>
        <p>Find below Visa details for Employee :
                                              
                                              </p>
                                              
             <table>
             <tr>
             <td>
             Doc No : <%=detail.DocNumber %>
             </td>
             </tr>
             <tr>
             <td>
             Date : <%=detail.DocDate %>
             </td>
             </tr>
             
             <tr>
             <td>
             Passport Type : <%=detail.PassportType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Type Of Visa : <%=detail.SelVisaType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Country : <%=detail.SelCountry %>
             </td>
             </tr>
             
             <tr>
             <td>
             Nationality : <%=detail.SelNationality %>
             </td>
             </tr>
             
             <tr>
             <td>
             Residence : <%=detail.SelResidence %>
             </td>
             </tr>
              <tr>
             <td>
             Visa Category : <%=detail.VisaCategory %>
             </td>
             </tr>
             
             </table>
             Thanks & Regards,
             Global Visa Sales Team.
            </body>
            </html>
    <%} %>
    
    </div>
    
    
      <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
    <%if(detail != null) %>
    <%{ %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
        <title></title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            
            </head>
            <body>
            
            <table>
            <tr>
            <td>
            Dear Employee,
            </td>
            </tr>
            
            <tr>
            <td>
            <p>
  Below are the visa details which are  <% =_approvalStatus %> by  <% = approverNames  %></p>
            </td>
            </tr>   
            </table>                          
             <table>
             <tr>
             <td>
             Doc No : <%=detail.DocNumber %>
             </td>
             </tr>
             <tr>
             <td>
             Date : <%=detail.DocDate %>
             </td>
             </tr>
             
             <tr>
             <td>
             Passport Type : <%=detail.PassportType %>
             </td>
             </tr>
             
             <tr>
             <td>
             Type Of Visa : <%=detail.SelVisaType  %>
             </td>
             </tr>
             
             <tr>
             <td>
             Country : <%=detail.SelCountry %>
             </td>
             </tr>
             
             <tr>
             <td>
             Nationality : <%=detail.SelNationality %>
             </td>
             </tr>
             
             <tr>
             <td>
             Residence : <%=detail.SelResidence %>
             </td>
             </tr>
              <tr>
             <td>
             Visa Category : <%=detail.VisaCategory%>
             </td>
             </tr>
             </table>
             Thanks & Regards,
             Global Visa Sales Team.
            </body>
            </html>
    <%} %>
    
    </div>
</asp:Content>
