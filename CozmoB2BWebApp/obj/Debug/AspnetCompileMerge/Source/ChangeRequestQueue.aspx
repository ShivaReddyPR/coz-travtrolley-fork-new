﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ChangeRequestQueueUI" Title="Change Request Queue" Codebehind="ChangeRequestQueue.aspx.cs" %>
    <%@ MasterType VirtualPath="~/TransactionBE.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="js/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />

    <script type="text/javascript">
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById('DisplayAgent'+id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        function submitForm() {

        }

        function ShowPage(pageNo) {

            document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=btnSubmit.ClientID %>').click();
            //document.forms[0].submit();
        }

        function CancelAmendBooking(cnf) {
            hidestuff('DisplayGrid2');
            document.getElementById('<%=hdnBooking.ClientID %>').value = cnf + '|' + document.getElementById('booking' + cnf).value + '|' + document.getElementById('txtRemarks').value;
            //document.forms[0].submit();
        }


        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtCheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtCheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtCheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //    if (difference < 1) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //        return false;
            //    }
            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtCheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);



        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }

        function Validate(index) {
            var adminFee = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_txtAdminFee').value;
            var supplierFee = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_txtSupplierFee').value;
            var total = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_lblPrice').innerText;
            var totalAmount = total.split(" ");
            if (parseFloat(adminFee)+parseFloat(supplierFee) <0 || (parseFloat(adminFee)+parseFloat(supplierFee)) > parseInt(totalAmount[1])) {
                alert("Admin Fee and Supplier Fee should not be Greater Then Booking Amount");
                return false;
            }
         }
    </script>


    <asp:HiddenField ID="hdnBooking" runat="server" />
    <asp:HiddenField ID="PageNoString" runat="server" Value="1"/>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute;  z-index:9999; top: 120px; left:8%; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; z-index:9999; top: 120px; left:40%; display: none;">
        </div>
    </div>
     <asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
          <tr>
              <td style="width: 700px" align="left">
                  <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                      onclick="return ShowHide('divParam');">Hide Parameter</a>
              </td>
          </tr>
      </table>
       <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
       
        
            <div class="paramcon"> 
                
                <div class="margin-top-5"> 
                                
                                <div class="col-md-2"> From Date:</div>
                                <div class="col-md-2"> <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCheckIn" runat="server" Width="120px" CssClass="form-control"></asp:TextBox>
                                        </td>
                                        <td>
                                            <a href="javascript:void(null)" onclick="showCal1()">
                                                <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                            </a>
                                        </td>
                                    </tr>
                                </table></div>
                                 <div class="col-md-2">  To Date: </div>
                                 <div class="col-md-2"><table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCheckOut" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                                        </td>
                                        <td align="left">
                                            <a href="javascript:void(null)" onclick="showCal2()">
                                                <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                            </a>
                                        </td>
                                    </tr>
                                </table> </div>
                                <div class="col-md-2">  Agent:</div>
                                 <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlAgents" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAgents_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                </asp:DropDownList></div>
                         
                         
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                                
                                
                                      <div class="margin-top-5"> 
                                
                                <div class="col-md-2">  Location:</div>
                                <div class="col-md-2">
                                
                                <asp:DropDownList ID="ddlLocations" CssClass="form-control" runat="server" AppendDataBoundItems="true" Enabled="false">
                                    <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                </asp:DropDownList> </div>
                                
                                
                                 <div class="col-md-2"> Booking Status: </div>
                                 <div class="col-md-2">
                                 
                                  <asp:DropDownList CssClass="form-control" ID="ddlBookingStatus" runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                </asp:DropDownList>
                                
                                
                                </div>
                                <div class="col-md-2"> Source: </div>
                                 <div class="col-md-2">
                                 
                                 <asp:DropDownList CssClass="form-control" ID="ddlSource" runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                </asp:DropDownList>
                                
                                
                                 </div>
                         
                         
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                              
                              
                 <div class="margin-top-5"> 
                                
                                <div class="col-md-2">  Hotel:</div>
                                <div class="col-md-2"> <asp:TextBox ID="txtHotelName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox></div>
                                 <div class="col-md-2"> Pax Name:</div>
                                 <div class="col-md-2"><asp:TextBox ID="txtPaxName" runat="server" 
                                 CssClass="inputEnabled form-control"></asp:TextBox> </div>
                                <div class="col-md-2">  Confirmation No.</div>
                                 <div class="col-md-2"> <asp:TextBox ID="txtConfirmNo" runat="server" CssClass="inputEnabled form-control"></asp:TextBox></div>
                         
                         
                         
                                <div class="clearfix"> </div>
                                </div>
                              
                                          
               
               
               
                       <div class="margin-top-5"> 
                                
                                <div class="col-md-2"><asp:Label ID="Label1" runat="server" Text="Login Name"></asp:Label> </div>
                                <div class="col-md-2"> <asp:TextBox CssClass="form-control"  ID="txtAgentLoginName" runat="server"></asp:TextBox></div>
                                 <div class="col-md-2"> </div>
                                 <div class="col-md-2"> </div>
                                <div class="col-md-2"> </div>
                                 <div class="col-md-2"> <asp:Button CssClass="btn but_b pull-right" ID="btnSubmit" runat="server" OnClientClick="return submitForm();" Text="Search"
                                    OnClick="btnSubmit_Click" /></div>
                         
                         
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                                                 
                                
                  </div>              
        
        
          
        </asp:Panel>
        </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
        </td></tr>
        <tr>
            <td align="right">
                <div style="text-align:right"><%=show %></div>
            </td>
        </tr>
        
        <tr>
            <td>
                <div style="width:100%; overflow: auto;">
                    
                    <asp:DataList ID="dlChangeRequests" runat="server" Width="100%" Height="300px" 
                        OnItemCommand="dlChangeRequests_ItemCommand" 
                        onitemdatabound="dlChangeRequests_ItemDataBound">
                        <ItemStyle VerticalAlign="Top" />
                      
                      
                        <ItemTemplate>
                        
                        
                         <div style="display: none" id="DisplayAgent<%#Container.ItemIndex %>" class="div-TravelAgent">
                <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href="#"
                    onclick="hidestuff('DisplayAgent<%#Container.ItemIndex %>');">
                    <img src="images/close1.png" /></a></span>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="25">
                            <b>
                                <asp:Label ID="lblAgencyName" runat="server" Text=""></asp:Label></b>
                        </td>
                        <td>
                            Credit Balance: <strong>
                                <asp:Label ID="lblAgencyBalance" runat="server" Text=""></asp:Label></strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="25">
                            Mob : <strong>
                                <asp:Label ID="lblAgencyPhone1" runat="server" Text=""></asp:Label></strong>
                        </td>
                        <td>
                            Phone : <strong>
                                <asp:Label ID="lblAgencyPhone2" runat="server" Text=""></asp:Label></strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="25">
                            Email : <strong>
                                <asp:Label ID="lblAgencyEmail" runat="server" Text=""></asp:Label></strong>
                        </td>
                        <td>
                            <a href="#">View Full Profile</a>
                        </td>
                    </tr>
                </table>
                
                
            </div>
                           
                           
                           
                           
                           <div style=" background:#fff; border: solid 1px #ccc; padding: 10px 0px 10px 0px; margin-bottom:10px;"> 
                           
                           
                                <div class="col-md-12 marbot_10"> 
                                
                                <div class="col-md-4"> Agent : <a href="#" onclick="showStuff(<%#Container.ItemIndex %>)"><b>
                                           <asp:Label ID="lblAgent" runat="server" Text=""></asp:Label></b></a></div>
                                <div class="col-md-4"> Status : <b style="color: #009933">
                                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                        </b></div>
                                <div class="col-md-4">  Confirmation No : <b><%#Eval("ConfirmationNo") %></b></div>
                                
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                                 <div class="col-md-12 marbot_10"> 
                                
                                <div class="col-md-4">   Supplier :<b style="color: #999999">
                                            <asp:Label ID="lblSupplier" runat="server" Text=""></asp:Label></b> </div>
                                <div class="col-md-4">Last Cancellation Date : <b>
                                            <%#Convert.ToDateTime(Eval("LastCancellationDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b></div>
                                <div class="col-md-4"> Supplier Ref : <b>
                                            <%#Eval("BookingRefNo") %>
                                            </b></div>
                                
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                                 <div class="col-md-12 marbot_10"> 
                                
                                <div class="col-md-4"> Hotel :<b>
                                            <%#Eval("HotelName") %></b> </div>
                                <div class="col-md-4">  No. Of Rooms :<%#Eval("NoOfRooms") %> </div>
                                <div class="col-md-4"> Total Price : <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label></div>
                                <div class="col-md-4" style="display:none"> <asp:Label ID="lblBookingPrice" runat="server" Text=""></asp:Label></div>
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                                <div class="col-md-12 marbot_10"> 
                                
                                <div class="col-md-4"> Booked On :<b>
                                            <%#Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy HH:mm:ss") %></b> </div>
                                <div class="col-md-4"> No. Of Guests : <asp:Label ID="lblGuests" runat="server" Text=""></asp:Label></div>
                                <div class="col-md-4">Pax Name : <b>
                                            <asp:Label ID="lblPaxName" runat="server" Text=""></asp:Label></b></div>
                                
                                <div class="clearfix"> </div>
                                </div>
                                
                               
                                <div class="col-md-12 marbot_10"> 
                                
                                <div class="col-md-4"> Check In :<b>
                                                <%#Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                <div class="col-md-4"> Check Out : <%#Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></div>
                                <div class="col-md-4">Created By:  <asp:Label ID="lblCreatedBy" runat="server" Text=""></asp:Label></div>
                                
                                <div class="clearfix"> </div>
                                </div>
                                
                                 
                                <div class="col-md-12 marbot_10"> 
                                
                                
                               
                                
                                
                                
                                <div class="col-md-4"><asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label> 
                                            
                                            
                                            <asp:Label ID="Label1" runat="server" Text="Admin Fee"></asp:Label>
                                            
                                  
                                                     <asp:Label ID="lblAdminCurrency" runat="server" Text=""> </asp:Label> <asp:TextBox ID="txtAdminFee" runat="server" Width="80px" Text="0" onkeypress="return restrictNumeric(this.id,'0');"></asp:TextBox>
                                                     
                                                     
                                
                                 </div>
                                 
                                 
                                <div class="col-md-4"><asp:Label ID="Label2" runat="server" Text="Supplier Fee"></asp:Label>
                                <asp:Label ID="lblSupplierCurrency" runat="server" Text=""> </asp:Label> <asp:TextBox ID="txtSupplierFee" runat="server" Width="80px" Text="0"></asp:TextBox>
                                <label class=" pull-right">  <asp:Button ID="btnOpen" CssClass="button-small margin-right-5" runat="server" Text="Open" Font-Bold="True" Visible="false"/>
                                
                                </label>
                                
                                <label class=" pull-right">    <asp:Button ID="btnRefund" runat="server" Font-Bold="True" Text="Refund" CommandName="Refund" CommandArgument='<%#Eval("ConfirmationNo") %>' CssClass="button"/>
                                </label>
                                 </div>
                                <div class="col-md-12 marbot_10">
                                 <div class="col-md-10"> 
                                
                                
                                <asp:Label ID="lblReson" runat="server" Text="Reason:"></asp:Label>
                                
                                <asp:Label ID="lblRemarks" runat="server" BackColor="#CCCCCC" Font-Italic="True"
                                            Font-Names="Arial" Font-Size="10pt" Text='<%#Eval("SpecialRequest") %>'></asp:Label></div>
                                
                                
                                
                                
                                
                                </div>
                                
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                                
                                
                                
                           
                           <div class="clearfix"> </div>
                           
                           </div>
                           
                            <%--<table width="100%" border="0" cellpadding="2" cellspacing="0" class="GridRepeat07"
                                style="border: thin solid #000000;">
                                <tr>
                                    <td height="10" colspan="6" align="right">
                                    </td>
                                </tr>
                      
                                
                                
                                
                                <tr>
                                   
                                    <td align="left" colspan="6">
                                        
                                    </td>
                                     
                                </tr>
                                
                                <tr>
                                    <td colspan="2" align="right">
                                        
                                    </td>
                                    <td align="left" class="style3">
                                        
                   
                                 
                                 
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td class="style4">
                                        
                                    </td>
                                    <td align="right">
                                       
                                        
                                    </td>
                                </tr>
                            </table>--%>
                        </ItemTemplate>
                        
                        
                        
                    </asp:DataList>
                </div>
            </td>
        </tr>
        <tr>
             <td align="right">
                <div style="text-align:right"><%=show %></div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
