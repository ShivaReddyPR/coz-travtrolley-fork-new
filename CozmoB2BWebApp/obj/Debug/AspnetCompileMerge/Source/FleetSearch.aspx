﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FleetSearch" Title="Fleet Search" Codebehind="FleetSearch.aspx.cs" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

        <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
        <link rel="stylesheet" href="css/select2.css" type="text/css" />

        <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
        <script type="text/javascript" src="yui/build/event/event-min.js"></script>
        <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
        <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
        <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
        <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
        <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
        <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
        <script src="yui/build/container/container-min.js" type="text/javascript"></script>
        <script src="Scripts/select2.min.js" type="text/javascript"></script>

        <script>
            var cal1;
            var cal2;

            function init() {

                //    showReturn();
                var today = new Date();
                // For making dual Calendar use CalendarGroup  for single Month use Calendar
                cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
                //cal1 = new YAHOO.widget.Calendar("cal1", "Outcontainer1");
                cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                //            cal1.cfg.setProperty("title", "Select CheckIn date");
                cal1.cfg.setProperty("close", true);
                cal1.selectEvent.subscribe(setDate1);
                cal1.render();

                cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
                //            cal2.cfg.setProperty("title", "Select CheckOut date");
                cal2.selectEvent.subscribe(setDate2);
                cal2.cfg.setProperty("close", true);
                cal2.render();
            }

            function showCalendar1() {
                init();
                if (cal2 != null) cal2.hide();
                document.getElementById('container1').style.display = "block";
                document.getElementById('Outcontainer1').style.display = "block";
                document.getElementById('Outcontainer2').style.display = "none";
            }
            var departureDate = new Date();

            function showCalendar2() {
                $('container1').context.styleSheets[0].display = "none";
                document.getElementById('Outcontainer1').style.display = "none";
                document.getElementById('Outcontainer2').style.display = "block";
                if (cal1 != null) cal1.hide();
                // setting Calender2 min date acoording to calendar1 selected date
                var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
                //var date1=new Date(tempDate.getDate()+1);

                if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                    var depDateArray = date1.split('/');

                    var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                    cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                    cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    cal2.render();
                }
                document.getElementById('container2').style.display = "block";
            }

            function setDate1() {
                var date1 = cal1.getSelectedDates()[0];

                $('IShimFrame').context.styleSheets[0].display = "none";
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();

                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());

                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                    return false;
                }
                departureDate = cal1.getSelectedDates()[0];
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                //			
                var month = date1.getMonth() + 1;
                var day = date1.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
                cal1.hide();
                document.getElementById('Outcontainer1').style.display = "none";

            }

            function setDate2() {
                var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
                if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select pickup date.";
                    return false;
                }

                var date2 = cal2.getSelectedDates()[0];

                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid pickup Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
                var difference = returndate.getTime() - depdate.getTime();

                //            if (difference < 1) {
                //                document.getElementById('errMess').style.display = "block";
                //                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                //                return false;
                //            }
                //            if (difference == 0) {
                //                document.getElementById('errMess').style.display = "block";
                //                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                //                return false;
                //            }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date2.getMonth() + 1;
                var day = date2.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
                cal2.hide();
                document.getElementById('Outcontainer2').style.display = "none";
            }
            YAHOO.util.Event.addListener(window, "load", init);


            function foucusOut(textBox, txt) {
                if (textBox.value == "") {
                    textBox.value = txt;
                }
            }

            function focusIn(textBox, txt) {
                if (textBox.value == txt) {
                    textBox.value = "";
                }
            }
            function selectReturnLocation() {
                if (document.getElementById('ctl00_cphTransaction_chkRetunLocation').checked) {
                    document.getElementById('diffrentLocation').style.display = "block";
                } else {
                    document.getElementById('diffrentLocation').style.display = "none";
                }

            }

            

            function validateSearch() {
                if (document.getElementById('<%=ddlPickupCity.ClientID%>').value == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select pickup City.";
                    return false;
                }
                if (document.getElementById('<%=ddlPickupLocation.ClientID%>').value == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select pickup Location.";
                    return false;
                }
                if (document.getElementById('ctl00_cphTransaction_chkRetunLocation').checked) {
                    if (document.getElementById('<%=ddlReturnCity.ClientID%>').value == "0") {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Return City.";
                        return false;
                    }
                    if (document.getElementById('<%=ddlReturnLocation.ClientID%>').value == "0") {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Return Location.";
                        return false;
                    }
                }
                var date1 = document.getElementById('<%= CheckIn.ClientID %>').value;
                var date2 = document.getElementById('<%= CheckOut.ClientID %>').value;
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Pickup Date";
                    return false;
                }
                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Pickup Date";
                    return false;
                }
                var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                if (todaydate.getTime() > cInDate.getTime()) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Pickup Date should be greater than equal to Todays Date";
                    return false;
                }

                if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Drop Date";
                    return false;
                }
                var retDateArray = date2.split('/');

                // checking if date2 is valid	
                if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Pickup Date";
                    return false;
                }
                var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                if (todaydate.getTime() > cOutDate.getTime()) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Drop Date should be greater than equal to todays date";
                    return false;
                }
                var pickupTime = document.getElementById('<%=ddlPickuptime.ClientID%>').value;
                var returnTime = document.getElementById('<%=ddlReturntime.ClientID%>').value;
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0], pickupTime.split(':')[0], pickupTime.split(':')[1], 0, 0);
                var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0], returnTime.split(':')[0], returnTime.split(':')[1], 0, 0);
                var difference = returndate.getTime() - depdate.getTime();

                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Drop Date Time should be greater than  or equal to Pickup Date Time";
                    return false;
                }

                var timeDiffVal = eval('<%=System.Configuration.ConfigurationManager.AppSettings["BookingTimeDiff"]%>');
                //Multiply will milliseconds per hour
                var timeDiff = 3600000 * timeDiffVal;
                if (difference < timeDiff) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Pickup Date Time & Return Date Time should have atleast 2 hours diffrence";
                    return false;
                }
                return true;
            }

            $(document).ready(function() {
                $(".custom-select2").select2();
            })
        </script>

        <div id="Fllet_main_inner" class="search_container">
            <div class="clear" style="margin-left: 25px">
                <div id="fcontainer1" style="position: absolute; top: 149px; left: 30%; display: none;
                    z-index: 9999;">
                </div>
            </div>
            <div id="Outcontainer1" style="position: absolute; top: 153px; left: 25%; display: none;
                z-index: 300">
                <div id="container1" style="border: 0px solid #ccc;">
                </div>
            </div>
            <div id="Outcontainer2" style="position: absolute; top: 153px; left: 49%; display: none;
                z-index: 300">
                <div id="container2" style="border: 0px solid #ccc;">
                </div>
            </div>
            <%--city,location ,FromDate and ToDate--%>
            <div class="col-md-12 margin-top-10">
                <div class="col-md-12 marbot_10">
                    <h3>
                        Sayara Limousine & Car Rental Services</h3>
                </div>
                <div class="col-md-12" id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" style="color: Red;" class="padding-4 yellow-back width-300 center margin-top-4">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-6">
                    <!--Start:Hidden Content-->
                    <%--  <div class="form-group">
                            <div id="statescontainer3" style="width: 300px; line-height: 30px; color: #000; left: 0;
                                position: absolute; display: none; z-index: 9999;">
                            </div>
                            <label>PickUp City</label>
                            <input class="form-control" type="text" id="city" name="city" value="<%=cityName%>" onblur="foucusOut(this, '<%=cityName%>')" onfocus="focusIn(this, '<%=cityName%>')" onclick="ShowContainer('statescontainer3' , 'city')" />
                            <input type="hidden" id="CityCode" name="CityCode" />
                        </div>--%>
                    <!--End:Hidden Content-->
                    <div class="form-group">
                        <label for="ddlPickupCity">
                            Select <strong>Pickup</strong> city</label>
                        <asp:DropDownList ID="ddlPickupCity" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                            OnSelectedIndexChanged="ddlPickupCity_SelectedIndexChanged" class="form-control form-valid custom-select2">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <%--<select class="form-control custom-select2" id="InputPickupCity">
                                    <option value="0" selected="selected">PICKUP CITY</option>
                                    <option value="1">ABU DHABI</option>
                                    <option value="2">AJMAN</option>
                                    <option value="3">AL AIN</option>
                                    <option value="4">DUBAI</option>
                                    <option value="5">FUJAIRAH</option>
                                    <option value="6">RAS AL KHAIMA</option>
                                    <option value="7">SHARJAH</option>
                                  </select>--%>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-6">
                    <!--Start:Hidden Content-->
                    <%-- <div class="form-group">
                            <div id="statescontainer4" style="width: 300px; line-height: 30px; color: #000; left: 0;
                    position: absolute; display: none; z-index: 9999;">
                            </div>

                            <label>
                    PickUp Location
                </label>
                            <input class="form-control" type="text" id="pickupLocation" name="pickupLocation" value="<%=locationName%>" onblur="foucusOut(this, '<%=locationName%>')" onfocus="focusIn(this, '<%=locationName%>')" onclick="ShowPickupLocation('statescontainer4' , 'pickupLocation')"
                            />
                            <input type="hidden" id="PickupLocationCode" name="PickupLocationCode" />


                        </div>--%>
                    <!--End:Hidden Content-->
                    <div class="form-group">
                        <label for="InputPickupLocation">
                            Select <strong>Pickup</strong> location</label>
                        <asp:DropDownList ID="ddlPickupLocation" runat="server" class="form-control custom-select2">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <%--   <select class="form-control custom-select2" id="InputPickupLocation" readonly>
                                    <option value="0" selected="selected">LOCATION</option>
                                    <option value="1">ABU DHABI</option>
                                    <option value="2">AJMAN</option>
                                    <option value="3">AL AIN</option>
                                    <option value="4">DUBAI</option>
                                    <option value="5">FUJAIRAH</option>
                                    <option value="6">RAS AL KHAIMA</option>
                                    <option value="7">SHARJAH</option>
                                  </select>--%>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2">
                                    <label>
                                        Pickup Date</label>
                                </td>
                                <td style="padding-left: 15px;">
                                    <label>
                                        Time</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox Class="form-control" ID="CheckIn" runat="server" Width="110px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCalendar1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" />
                                    </a>
                                </td>
                                <td style="padding-left: 15px;">
                                    <asp:DropDownList ID="ddlPickuptime" runat="server" class="form-control custom-select2">
                                        <asp:ListItem Value="00:30">00:30</asp:ListItem>
                                        <asp:ListItem Value="01:00">01:00</asp:ListItem>
                                        <asp:ListItem Value="01:30">01:30</asp:ListItem>
                                        <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                        <asp:ListItem Value="02:30">02:30</asp:ListItem>
                                        <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                        <asp:ListItem Value="03:30">03:30</asp:ListItem>
                                        <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                        <asp:ListItem Value="04:30">04:30</asp:ListItem>
                                        <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                        <asp:ListItem Value="05:30">05:30</asp:ListItem>
                                        <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                        <asp:ListItem Value="06:30">06:30</asp:ListItem>
                                        <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                        <asp:ListItem Value="07:30">07:30</asp:ListItem>
                                        <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                        <asp:ListItem Value="08:30">08:30</asp:ListItem>
                                        <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                        <asp:ListItem Value="09:30">09:30</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="10:00">10:00</asp:ListItem>
                                        <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                        <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                        <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                        <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                        <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                        <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                        <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                        <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                        <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                        <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                        <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                        <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                        <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                        <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                        <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                        <asp:ListItem Value="18:30">18:30</asp:ListItem>
                                        <asp:ListItem Value="19:00">19:00</asp:ListItem>
                                        <asp:ListItem Value="19:30">19:30</asp:ListItem>
                                        <asp:ListItem Value="20:00">20:00</asp:ListItem>
                                        <asp:ListItem Value="20:30">20:30</asp:ListItem>
                                        <asp:ListItem Value="21:00">21:00</asp:ListItem>
                                        <asp:ListItem Value="21:30">21:30</asp:ListItem>
                                        <asp:ListItem Value="22:00">22:00</asp:ListItem>
                                        <asp:ListItem Value="22:30">22:30</asp:ListItem>
                                        <asp:ListItem Value="23:00">23:00</asp:ListItem>
                                        <asp:ListItem Value="23:30">23:30</asp:ListItem>
                                        <asp:ListItem Value="00:00">00:00</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2">
                                    <label>
                                        Drop Date</label>
                                </td>
                                <td style="padding-left: 15px;">
                                    <label>
                                        Time</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox Class="form-control" ID="CheckOut" runat="server" Width="110px"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <a href="javascript:void(null)" onclick="showCalendar2()">
                                        <img id="Img1" src="images/call-cozmo.png" />
                                    </a>
                                </td>
                                <td style="padding-left: 15px;">
                                    <asp:DropDownList ID="ddlReturntime" runat="server" class="form-control custom-select2">
                                        <asp:ListItem Value="00:30">00:30</asp:ListItem>
                                        <asp:ListItem Value="01:00">01:00</asp:ListItem>
                                        <asp:ListItem Value="01:30">01:30</asp:ListItem>
                                        <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                        <asp:ListItem Value="02:30">02:30</asp:ListItem>
                                        <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                        <asp:ListItem Value="03:30">03:30</asp:ListItem>
                                        <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                        <asp:ListItem Value="04:30">04:30</asp:ListItem>
                                        <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                        <asp:ListItem Value="05:30">05:30</asp:ListItem>
                                        <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                        <asp:ListItem Value="06:30">06:30</asp:ListItem>
                                        <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                        <asp:ListItem Value="07:30">07:30</asp:ListItem>
                                        <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                        <asp:ListItem Value="08:30">08:30</asp:ListItem>
                                        <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                        <asp:ListItem Value="09:30">09:30</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="10:00">10:00</asp:ListItem>
                                        <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                        <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                        <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                        <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                        <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                        <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                        <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                        <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                        <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                        <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                        <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                        <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                        <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                        <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                        <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                        <asp:ListItem Value="18:30">18:30</asp:ListItem>
                                        <asp:ListItem Value="19:00">19:00</asp:ListItem>
                                        <asp:ListItem Value="19:30">19:30</asp:ListItem>
                                        <asp:ListItem Value="20:00">20:00</asp:ListItem>
                                        <asp:ListItem Value="20:30">20:30</asp:ListItem>
                                        <asp:ListItem Value="21:00">21:00</asp:ListItem>
                                        <asp:ListItem Value="21:30">21:30</asp:ListItem>
                                        <asp:ListItem Value="22:00">22:00</asp:ListItem>
                                        <asp:ListItem Value="22:30">22:30</asp:ListItem>
                                        <asp:ListItem Value="23:00">23:00</asp:ListItem>
                                        <asp:ListItem Value="23:30">23:30</asp:ListItem>
                                        <asp:ListItem Value="00:00">00:00</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12 marbot_10">
                <div id="diffrentLocation" style="display: none;">
                    <div class="col-md-3 col-xs-6">
                        <%-- <div class="form-group">
                                <div id="statescontainer5" style="width: 300px; line-height: 30px; color: #000; left: 0;
                position: absolute; display: none; z-index: 9999;">
                                </div>
                                <label>
                Return City</label>

                                <input class="form-control" type="text" id="returnCity" name="returnCity" value="<%=cityName%>" onblur="foucusOut(this, '<%=cityName%>')" onfocus="focusIn(this, '<%=cityName%>')" onclick="ShowReturnCity('statescontainer5' , 'returnCity')" />
                                <input type="hidden" id="returnCityCode" name="returnCityCode" />


                            </div>--%>
                        <div class="form-group">
                            <label for="ddlReturnCity">
                                Select <strong>Return</strong> city</label>
                            <asp:DropDownList ID="ddlReturnCity" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                OnSelectedIndexChanged="ddlReturnCity_SelectedIndexChanged" class="form-control form-valid custom-select2">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <%--<select class="form-control custom-select2" id="InputPickupCity">
                                    <option value="0" selected="selected">PICKUP CITY</option>
                                    <option value="1">ABU DHABI</option>
                                    <option value="2">AJMAN</option>
                                    <option value="3">AL AIN</option>
                                    <option value="4">DUBAI</option>
                                    <option value="5">FUJAIRAH</option>
                                    <option value="6">RAS AL KHAIMA</option>
                                    <option value="7">SHARJAH</option>
                                  </select>--%>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label for="ddlReturnLocation">
                                Select <strong>Return</strong> location</label>
                            <asp:DropDownList ID="ddlReturnLocation" runat="server" class="form-control custom-select2">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <%--   <select class="form-control custom-select2" id="InputPickupLocation" readonly>
                                    <option value="0" selected="selected">LOCATION</option>
                                    <option value="1">ABU DHABI</option>
                                    <option value="2">AJMAN</option>
                                    <option value="3">AL AIN</option>
                                    <option value="4">DUBAI</option>
                                    <option value="5">FUJAIRAH</option>
                                    <option value="6">RAS AL KHAIMA</option>
                                    <option value="7">SHARJAH</option>
                                  </select>--%>
                        </div>
                        <%--           <div class="form-group">
                                <div id="statescontainer6" style="width: 300px; line-height: 30px; color: #000; left: 0;
                position: absolute; display: none; z-index: 9999;">
                                </div>
                                <label>
                Return Location</label>

                                <input class="form-control" type="text" id="returnLocation" name="returnLocation" value="<%=locationName%>" onblur="foucusOut(this, '<%=locationName%>')" onfocus="focusIn(this, '<%=locationName%>')" onclick="ShowReturnLocation('statescontainer6' , 'returnLocation')"
                                />
                                <input type="hidden" id="ReturnLocationCode" name="ReturnLocationCode" />
                            </div>--%>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>
                            Select car type
                        </label>
                        <asp:DropDownList ID="ddlCarType" runat="server" CssClass="form-control custom-select2">
                            <asp:ListItem Text="CAR TYPE" Value="0"></asp:ListItem>
                            <asp:ListItem Text="CARS" Value="CARS"></asp:ListItem>
                            <asp:ListItem Text="SUV" Value="SUV"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="checkbox2" style="margin-top: 30px;">
                        <label>
                            <asp:CheckBox ID="chkRetunLocation" runat="server" onclick="selectReturnLocation();" />
                            Return car to a different location
                        </label>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 martop_xs10" style="padding-right: 20px;">
                <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="but but_b pull-right"
                    OnClientClick="return validateSearch();" OnClick="btnSearch_Click" />
            </div>
            <div class="clearfix">
            </div>
            <div>
                &nbsp;</div>
        </div>
        <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
        </iframe>
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
    </asp:Content>
