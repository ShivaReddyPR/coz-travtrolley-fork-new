﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ActivityQueueGUI" Title="Activity Queue" Codebehind="ActivityQueue.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<link href="css-holiday/main-style.css" rel="stylesheet" type="text/css" />
<%--<link href="css/main-style.css" rel="stylesheet" type="text/css" />
--%>
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
   
    <script type="text/javascript" src="ash.js"></script>
     <link rel="stylesheet" href="css/style.css">
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />

<div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
            </div>
<div class="clear" style="margin-left: 30px">
            <div id="container3" style="position: absolute; top:120px; left:20%; z-index:9999; display: none;">
            </div>
        </div>
         <div class="clear" style="margin-left: 30px">
            <div id="container4" style="position: absolute; top:120px; left:40%; z-index:9999; display: none;">
            </div>
        </div>
  <div class="body_container">
  <div class="paramcon" title="Param">
        
        
        
        <div class="marbot_10"> 
        <div class="col-md-2">  <asp:Label ID="lblFromDate" Text="From Date:" runat="server"></asp:Label> </div>
        <div class="col-md-2"> 
        
       <table>
       <tr> 
       <td> <asp:TextBox ID="txtFrom" CssClass="form-control" runat="server" Width="100"></asp:TextBox></td>
       
        <td> <a href="javascript:void(null)" onclick="showCalendar3()">
                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                    </a></td>
       </tr>
       
       
       </table>
        
        
        </div>
        <div class="col-md-2"> <asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label></div>
        
        
        
        <div class="col-md-2">
        
       
       <table> 
       
       <tr>
       
        <td> <asp:TextBox ID="txtTo" CssClass="form-control" runat="server" Width="100"></asp:TextBox> </td>
        
         <td> <a href="javascript:void(null)" onclick="showCalendar4()">
                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                    </a> </td>
        
          </tr>
       
       
       </table>
         



</div>

        <div class="col-md-2"><asp:Label ID="lblTripId" Text="Trip Id:" runat="server"></asp:Label> </div>
        
         <div class="col-md-2"><asp:TextBox ID="txtTripId" runat="server" CssClass="form-control"></asp:TextBox> </div>
         
         <div class="clearfix"> </div>
        
        </div>
        
        
        
         <div class=" marbot_10"> 
        <div class="col-md-2"> <asp:Label ID="lblPaxName" Text="Pax Name:" runat="server" ></asp:Label> </div>
        
        
        <div class="col-md-2"> <asp:TextBox ID="txtPaxName" CssClass="form-control" runat="server"></asp:TextBox> </div>
        
        
        
        <div class="col-md-2"><asp:Label ID="lblPrice" Text="Price:" runat="server" ></asp:Label> </div>
        
        <div class="col-md-2"><asp:TextBox ID="txtPrice" CssClass="form-control" runat="server" onkeypress="return restrictNumeric(this.id,'0');" onfocus="Check(this.id);" onBlur="Set(this.id);"></asp:TextBox> </div>
        
        
        
        <div class="col-md-2"> Agency: </div>
        
         <div class="col-md-2"><asp:DropDownList ID="ddlAgency" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> </div>
         
         <div class="clearfix"> </div>
        
        </div>
        
        
        
         <div class=" marbot_10"> 
        <div class="col-md-2">  <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server" ></asp:Label> </div>
        
        <div class="col-md-2"> <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="form-control"  OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
        
        
        <div class="col-md-2"><asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label> </div>
        
        
        <div class="col-md-2"> <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control"></asp:DropDownList></div>
        
        <div class="col-md-2"> <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label></div>
        
        
        
         <div class="col-md-2"> <asp:DropDownList ID="ddlTransType" runat="server" CssClass="form-control" Visible="false">
                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="B2B" Text="B2B"></asp:ListItem>
                           <asp:ListItem  Value="B2C" Text="B2C"></asp:ListItem>
                    </asp:DropDownList></div>
         
         <div class="clearfix"> </div>
        
        </div>
        
        
        
        
         <div class=" marbot_10"> 
        
        <div class="col-md-12">
        
        
          <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button pull-right" OnClick="btnSearch_Click"  />
                         
          <asp:Button runat="server" ID="btnClear" Text="Clear" CssClass="button pull-right" OnClick="btnClear_Click"  />
        
        </div>
        
        </div>
        
        
        
        
        
        
        
           </div>
           
           
           
  <div class="col-md-12">
                     
                     
                     
                     <div class=" martop_10">  <label class="pull-right">
                       
                       <label>  Page:</label>
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First"></asp:LinkButton>
                        
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last"></asp:LinkButton>
                            
                             </label></div>
                    </div>
           
           
           
         
                    
              <div>
                 <asp:DataList ID="dlActivityQueue" runat="server" CellPadding="4"
                     DataKeyField="ATHId" Width="100%">
                     <ItemTemplate>
                     <div id="Result" style="width:100%"> 
                        
                         <table class=" bg_white bor_gray pad_10 marbot_10" width="100%">
                             <tr style="height: 5px;">
                             <td style="width: 2%"></td>
                                 <td style="width: 52%">
                                 </td>
                                 <td style="width: 15%">
                                 </td>
                                 <td style="width: 23%">
                                 </td>
                                 <td style="width: 9%">
                                 </td>
                             </tr>
                             <tr style="height: 25px;">
                             <td></td>
                                 <td colspan="1">
                                 <div>
                                 <p>
                                     <span style="font-weight: bold;">
                                         <%# Eval("activityName")%></span>
                                      <span>(<%# Eval("agent_name")%>)</span></p></div>
                                 </td>
                                 <td> <span class="auto" style="width: 100px;"><%# Eval("STATUS")%> </span></td>
                                 <td>
                                                 <span style="; font-weight: bold;">
                                                     <%# Eval("TripId")%></span>
                                            
                                 
                                 </td>
                                 <td> <label> 
                                     <span>
                                         <input id="Open" class="button" onclick='location.href=&#039;ViewBookingForActivity.aspx?bookingId=<%# Eval("ATHId") %>&#039;'
                                             type="button" value="Open" />
                                     </span>
                                     
                                     </label></td>
                             </tr>
                             <tr style="height: 25px;">
                             <td></td>
                                 <td>
                                 <table>
                                         <tr>
                                             <td style="width:80px">
                                                 <span style="">Booked:</span>
                                             </td>
                                             <td>
                                                 <span style="; font-weight: bold;">
                                                     <%# Convert.ToDateTime(Eval("TransactionDate")).ToString("dd-MMM-yyyy HH:MM")%></span>
                                             </td>
                                         </tr>
                                     </table>
                                     
                                 </td>
                                 <td>
                                     
                                 </td>
                                 <td>
                                     <span>
                                         <%#Eval("LeadPaxName")%>
                                     </span>
                                 </td>
                                 <td>
                                    
                                    
                                    
                                     <label> 
                                    
                                     <span>
                                         <input id="viewInvoice" class="button"  onclick=window.open("ActivityInvoice.aspx?bookingId=<%# Eval("ATHId") %>&agencyId=<%# Eval("AgencyId") %>","Invoice","width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                                             type="button" value="View Invoice" />
                                             
                                             
                                     </span>
                                     
                                     </label>
                                     
                                     
                                 </td>
                             </tr>
                             <tr style="height: 25px;">
                             <td></td>
                                 <td>
                                     <table>
                                         <tr>
                                             <td style="width:80px">
                                                 <span style="">Tour Dt:</span>
                                             </td>
                                             <td>
                                                 <span style="; font-weight: bold;">
                                                     <%# Convert.ToDateTime(Eval("Booking")).ToString("dd-MMM-yyyy HH:MM")%></span>
                                             </td>
                                         </tr>
                                     </table>
                                 </td>
                                 <td>
                                     
                                 </td>
                                 <td>
                                     <span style="font-weight: bold;">Price:
                                         <%#CTCurrencyFormat(Eval("TotalPrice"), Eval("AgencyId"))%>
                                         </span>
                                 </td>
                                 <td>
                                 </td>
                             </tr>
                             
                             <tr>
                             <td height="10" colspan="5"></td>
                                 
                             </tr>
                         </table>
                       
                        </div>
                     </ItemTemplate>
                 </asp:DataList>
             </div>
             
             
            
            
    </div>
    
    
   
    <script type="text/javascript">
        
        var cal3;
        var cal4;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
           
            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("title", "Purchase From Date");
            cal3.selectEvent.subscribe(setDate3);
            cal3.cfg.setProperty("close", true);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("title", "Purchase To Date");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        
        function showCalendar3() {
            
            $('container4').context.styleSheets[0].display = "none";
            cal4.hide();
            cal3.show();
           
            document.getElementById('container3').style.display = "block";
        }
        var departureDate = new Date();
        function showCalendar4() {
            $('container3').context.styleSheets[0].display = "none";
            cal3.hide();
            cal4.show();
            var date3 = document.getElementById('<%= txtFrom.ClientID%>').value;
            
            if (date3.length != 0 && date3 != "DD/MM/YYYY") {
                var depDateArray = date3.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal4.render();
            }
            document.getElementById('container4').style.display = "block";
        }

        function setDate3() {
           

            var date3 = cal3.getSelectedDates()[0];

            var month = date3.getMonth() + 1;
            var day = date3.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtFrom.ClientID %>').value = day + "/" + month + "/" + date3.getFullYear();
            cal3.hide();
        }

        function setDate4() {
            var date4 = cal4.getSelectedDates()[0];

            var date3 = document.getElementById('<%=txtFrom.ClientID %>').value;
            if (date3.length == 0 || date3 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select Purchase from date.";
                return false;
            }

            var depDateArray = date3.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Purchase From Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date4.getFullYear(), date4.getMonth(), date4.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Purchase To date should be greater than Purchase from date (" + date3 + ")";
                return false;
            }
           
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date4.getMonth() + 1;
            var day = date4.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtTo.ClientID %>').value = day + "/" + month + "/" + date4.getFullYear();
            cal4.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init);
        YAHOO.util.Event.addListener(this, "click", init);

        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0.00') {
                document.getElementById(id).value = '';
            }
        }
        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0.00') {
                document.getElementById(id).value = '0.00';
            }
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

