﻿<%@ Page Title="Proposer Details" Language="C#" EnableEventValidation="false" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareProposerDetails.aspx.cs" Inherits="CozmoB2BWebApp.ReligareProposerDetails" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div class="body_container">
        <div class="" style="margin-top: 10px;">
            <table class="table" id="tblQuotation">
                <tr>
                    <td>
                        <h5>Quotation
                        </h5>
                    </td>
                    <td></td>
                    <td>
                        <asp:Label ID="lblagentBal" Style="color: red" runat="server" Text="0.00" /></td>
                    <td style="text-align: right">
                        <h5>
                            <span>Premium :</span>
                            <asp:Label ID="lblPremium" Style="color: Red" runat="server" Text="0.00" />
                        </h5>
                    </td>
                </tr>
                <tr>

                    <td><span>Start Date:-</span>&nbsp;<asp:Label ID="lblStartDate" runat="server" Text="0.00" />
                    </td>
                    <td><span>End  Date:-</span>&nbsp;<asp:Label ID="lblEndDate" runat="server" Text="0.00" />
                    </td>
                    <td><span>Travelling To:-</span>&nbsp;<asp:Label ID="lblTravellingTo" runat="server" Text="0.00" />
                    </td>
                    <td><span>PED:-</span>&nbsp;<asp:Label ID="lblPED" runat="server" Text="0.00" />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit Quotation" class="btn btn-primary float-right" OnClick="btnEdit_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modall">
                    <br />
                    <center>
                <div class="center">
                     <b style="text-align:center;color:red;">Loading. Please wait.</b><br /><br />
                    <img src="images/ajaxLoader.gif" alt="Loading" height="100" width="100" />
                </div>
                    </center>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:TextBox ID="txtInsureDOB" runat="server" Style="display: none" />
                <asp:HiddenField ID="hdnproductType" runat="server" />
                <asp:HiddenField ID="hdnNoofPax" runat="server" />
                <div class="container" style="margin-top: 0px;">
                    <div class="ns-h3" style="border-radius: 5px;">
                        <%--width: 1150px;--%>
                        <h5>Proposer Details <span id='spanproposerdetails' style="float: right; margin-right: 15px;" class="more-less-head glyphicon glyphicon-plus glyphicon-minus"></span></h5>
                    </div>
                    <div id="divproposerdetails">
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Nationality</span>
                                    <asp:DropDownList ID="ddlNationality" TabIndex="0" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlNationality_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                    </asp:DropDownList>
                                    <span id="errorNationality" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Passport No</span>
                                    <asp:TextBox ID="txtPassportNo" runat="server" MaxLength="8" onKeyPress="return isChar(event)" TabIndex="1" CssClass="form-control" />
                                    <span id="errorPassportNo" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Title</span>
                                    <asp:DropDownList ID="ddlTitle" CssClass="form-control" TabIndex="2" runat="server">
                                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                        <asp:ListItem Value="Mr">Mr</asp:ListItem>
                                        <asp:ListItem Value="Ms">Ms</asp:ListItem>
                                    </asp:DropDownList>
                                    <span id="errorddlTitle" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>First Name</span>
                                    <asp:TextBox ID="txtFirstName" TabIndex="3" runat="server" onKeyPress="return validChar(event)" CssClass="form-control" />
                                    <span id="errorFirstName" style="color: red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Last Name</span>
                                    <asp:TextBox ID="txtLastName" runat="server" TabIndex="4" onKeyPress="return validChar(event)" CssClass="form-control" />
                                    <span id="errortxtLastName" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>DOB</span>
                                    <div class="input-group date txtstartDate">
                                        <asp:TextBox ID="txtpropdob" runat="server" Style="display: none;" />
                                        <asp:TextBox ID="txtdob" runat="server" TabIndex="5" CssClass="form-control" ReadOnly="true" Style="background-color: #fff;" onchange="proposerDOB();" />
                                        <asp:Label runat="server" class="input-group-addon" ID="lbldob"><i class="glyphicon glyphicon-calendar"></i></asp:Label>
                                    </div>
                                    <span id="errortxtdob" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Address Line 1</span>
                                    <asp:TextBox ID="txtAddressLine1" runat="server" TabIndex="6" MaxLength="60" CssClass="form-control" />
                                    <span id="errorAddressLine1" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Address Line 2</span>
                                    <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="60" TabIndex="7" CssClass="form-control" />
                                    <span id="errorAddressLine2" style="color: red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Email</span>
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="55" CssClass="form-control" TabIndex="8" />
                                    <span id="errortxtEmail" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Mobile No</span>
                                    <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="10" TabIndex="9" onKeyPress="return isNumber(event)" CssClass="form-control" />
                                    <span id="errortxtMobileNo" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Pincode</span>
                                    <asp:TextBox ID="txtcity" runat="server" Style="display: none" />
                                    <asp:TextBox ID="txtPincode" runat="server" CssClass="form-control" TabIndex="10" MaxLength="6" onKeyPress="return isNumber(event)" />
                                    <span id="errortxtPincode" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>City</span>
                                    <asp:DropDownList ID="ddlCity" CssClass="form-control" TabIndex="11" runat="server">
                                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>                                        
                                    </asp:DropDownList>
                                    <span id="errorddlCity" style="color: red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>State</span>
                                    <asp:TextBox ID="txtState" Style="background-color: #fff;" ReadOnly="true" runat="server" CssClass="form-control" />
                                    <span id="errortxtState" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Nominee Name</span>
                                    <asp:TextBox ID="txtNomineeName" runat="server" TabIndex="12" CssClass="form-control" />
                                    <span id="errortxtNomineeName" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Nominee Relation</span>
                                    <asp:DropDownList ID="ddlNominee" CssClass="form-control" TabIndex="13" runat="server">
                                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                    </asp:DropDownList>
                                    <span id="errorddlNominee" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span>Purpose of Visit</span>
                                    <asp:DropDownList ID="ddlPurposeVisit" CssClass="form-control" TabIndex="14" runat="server">
                                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                        <asp:ListItem Value="BT">Business</asp:ListItem>
                                        <asp:ListItem Value="PL">Pleasure</asp:ListItem>
                                    </asp:DropDownList>
                                    <span id="errorddlPurposeVisit" style="color: red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblresidenceProof" runat="server" Text="Residence Proof" />
                                    <asp:DropDownList ID="ddlresidenceProof" CssClass="form-control" TabIndex="15" runat="server">
                                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                        <asp:ListItem Value="1">Govt issued ID</asp:ListItem>
                                        <asp:ListItem Value="2">OCI card</asp:ListItem>
                                        <asp:ListItem Value="3">Company ID</asp:ListItem>
                                    </asp:DropDownList>
                                    <span id="errorddlresidenceProof" style="color: red;"></span>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblProof" runat="server" Text="Proof Details" />
                                    <asp:TextBox ID="txtProof" runat="server" CssClass="form-control" TabIndex="16" />
                                    <span id="errortxtProof" style="color: red;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container" style="margin-top: 10px;">
                    <div class="ns-h3" style="margin-right: 10px; border-radius: 5px;">
                        <h5>Insured Details <span id='spanInsureddetails' style="float: right; margin-right: 15px;" class="more-less-head glyphicon glyphicon-plus glyphicon-minus"></span>
                        </h5>
                    </div>
                    <asp:Table ID="tblInsuredDetails" class="col-sm-12" runat="server" Style="height: auto; margin-top: 10px;">
                    </asp:Table>
                </div>
                <div class="container" style="margin-top: 10px;" id="divEducationDetails">
                    <div class="ns-h3" style="margin-right: 10px; border-radius: 5px;">
                        <h5>Education Details <span id='spanEducationDetails' style="float: right; margin-right: 15px;" class="more-less-head glyphicon glyphicon-plus glyphicon-minus"></span>
                        </h5>
                    </div>
                    <asp:Table ID="tblEducationDetails" class="col-sm-12" runat="server" Style="height: auto; margin-top: 10px;">
                    </asp:Table>


                </div>
                <div class="container" style="margin-top: 10px;">
                    <div class="container" style="margin-top: 10px;" id="divOptionalCovers">
                        <div class="card" style="background-color: white">
                            <div style="height: 20px; border-radius: 5px;" id="optionalcovers">
                                <h5 class="ns-h3" style="float: left; border-radius: 7px;"><i class="fa fa-table m-r-10"></i>Optional Covers <span id="spanoptionalquestionary" style="margin-left: 10px;" class="more-less-head glyphicon glyphicon-plus glyphicon-minus"></span>&nbsp;</h5>
                            </div>
                            <div class="card-body">
                                <asp:Table ID="tblOptionalCovers" Style="text-align: left;" runat="server" CssClass="table">
                                </asp:Table>
                            </div>
                        </div>
                    </div>
                    <div class="container" style="margin-top: 10px;">
                        <div class="card" style="background-color: white">
                            <div style="height: 20px; border-radius: 5px;">
                                <h5 class="ns-h3" style="float: left; border-radius: 7px;"><i class="fa fa-table m-r-10"></i>Health Questionnaire <span id="spanquestionary" style="margin-left: 10px;" class="more-less-head glyphicon glyphicon-plus glyphicon-minus"></span>&nbsp;</h5>
                            </div>

                            <div class="card-body" id="divquestionary">
                                <asp:Table ID="tblQuestionire" Style="text-align: left;" runat="server" CssClass="table">
                                </asp:Table>
                                <div style="text-align: center; color: red;">
                                    <span><b>If you need to add the PED details please change the your ped selection through Edit Quotation.</b></span>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 20px;">
                            <asp:CheckBox ID="chkTermsCondition" runat="server" Checked="true" />
                            I hereby agree to the <a id="terms">terms & conditions<span id="errorMsg" style="color: red">*</span></a> of the purchase of this policy. 
               <span id="errorterms" style="color: red"></span>
                            <asp:HiddenField ID="hdnstatus" runat="server" />
                        </div>
                        <div class="row">
                            <asp:CheckBox ID="chknationality" runat="server" />
                            Trip start from India only.  
                        </div>

                        <div class="row" style="display: none;" id="divopcovers">
                            <asp:CheckBox ID="chkOptionalCovers" runat="server" />
                            I here by declare that all the optional covers opted by me except for Adventure Sports are University Mandatory.<span style="color: red">*
                        </div>
                        <br />
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1" style="float: right; margin-right: 30px;">
                                    <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" Style="float: right; border-radius: 5px; margin-right: 10px;" CssClass="btn btn-primary active" OnClientClick="javascript:return proposerValidation();" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </ContentTemplate>

        </asp:UpdatePanel>
        <div id="dialogterms" title="Terms and conditions of use" style="display: none;">
            <p>The following terms and conditions of use shall govern your use of the website www.religarehealthinsurance.com and all other URLs forming part thereof (hereinafter referred to as "the Sites"?). The Sites are owned and managed by Religare Health Insurance Company Limited incorporated under the Indian Companies Act, 1956 (hereinafter referred to as "us or we" in the first person and or as "Religare Health Insurance"? wherever the context so requires). These terms and conditions of use form the entirety of any express or implied contract that may or may be deemed to exist between us and you.</p>
            <br />
            <b>Cookies</b><br />
            <p>Religare Health Insurance uses the technology known as "cookies" to track usage patterns, traffic trends and user behaviour, as well as to record other information from the website. For certain services provided on this website, cookies allow Religare Health Insurance and/or its group companies/affiliates to save information locally so that you will not have to re-enter it the next time you visit. Many content adjustments and customer service improvements are made based on the data derived from cookies. The information we collect from cookies will not be used to create profiles of users and will only be used in aggregate form.</p>
            <p>The User may set his/her/its browser to refuse cookies. If the User so chooses, the User may still gain access to most of the Website, but the User may not be able to conduct certain types of transactions (such as shopping) or take advantage of some of the interactive elements offered.</p>
            <p>If the User uses any of the sharing features that may be offered by this Site, the User's friend's email address will not be retained on Religare Health Insurance Website or used in any way by Religare Health Insurance or its group companies/affiliates.</p>
        </div>
        <div id="dialogq1" title="Terms and conditions of PED" style="display: none;">
            <p>The following terms and conditions of use shall govern your use of the website www.religarehealthinsurance.com and all other URLs forming part thereof (hereinafter referred to as "the Sites"?). The Sites are owned and managed by Religare Health Insurance Company Limited incorporated under the Indian Companies Act, 1956 (hereinafter referred to as "us or we" in the first person and or as "Religare Health Insurance"? wherever the context so requires). These terms and conditions of use form the entirety of any express or implied contract that may or may be deemed to exist between us and you.</p>
        </div>
        <div id="dialogq2" title="Terms and conditions of Travel Policy" style="display: none;">
            <p>The following terms and conditions of use shall govern your use of the website www.religarehealthinsurance.com and all other URLs forming part thereof (hereinafter referred to as "the Sites"?). The Sites are owned and managed by Religare Health Insurance Company Limited incorporated under the Indian Companies Act, 1956 (hereinafter referred to as "us or we" in the first person and or as "Religare Health Insurance"? wherever the context so requires). These terms and conditions of use form the entirety of any express or implied contract that may or may be deemed to exist between us and you.</p>
        </div>
        <div id="dialogq3" title="Terms and conditions of Treatment Details" style="display: none;">
            <p>The following terms and conditions of use shall govern your use of the website www.religarehealthinsurance.com and all other URLs forming part thereof (hereinafter referred to as "the Sites"?). The Sites are owned and managed by Religare Health Insurance Company Limited incorporated under the Indian Companies Act, 1956 (hereinafter referred to as "us or we" in the first person and or as "Religare Health Insurance"? wherever the context so requires). These terms and conditions of use form the entirety of any express or implied contract that may or may be deemed to exist between us and you.</p>
        </div>
    </div>

    <%--script code--%>
    <%--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--%>
     <link rel="stylesheet" href="Temp/jquery-ui-1.12.1/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <%--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--%>
     <script src="Temp/jquery-ui-1.12.1/jquery-ui.js"></script>


    <script type="text/javascript"> 
        // Validating the characters.
        function validChar(evt) {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
            if (charCode == 32)
                return true;
            if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                return false;
            }

            return true;
        }

        function isChar(evt) {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
            if (charCode > 31 && (charCode < 65 || charCode > 90) &&
                (charCode < 97 || charCode > 122) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        // Validating the Number.
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        // Inialize the Date picker controls and key press events.
        function pageLoad() {
            var dtToday = new Date();
            var month = dtToday.getMonth();
            var day = dtToday.getDate();
            var year = dtToday.getFullYear() - 18;
            if (month < 10)
                month = '0' + month.toString();
            if (day < 10)
                day = '0' + day.toString();

            var maxDate = day + '/' + month + '/' + year;
            $("#<%=txtdob.ClientID%>").datepicker({
                changeYear: true,
                changeMonth: true,
                maxDate: maxDate,
                yearRange: year - 80 + ":" + year,
                dateFormat: "dd/M/yy",
            }).val();

            $("#<%=txtdob.ClientID%>").datepicker("option", "maxDate", new Date(year, month, day, 1));

            var pscount = $("#<%=hdnNoofPax.ClientID%>").val();
            var hdnproductType = $("#ctl00_cphTransaction_hdnproductType").val();
            for (var i = 0; i < pscount; i++) {
                var divdata = "<div class='input-group date txtDate'>" + $("#ctl00_cphTransaction_tdDOB" + i).html() + " <span id=spanIdob" + i + " class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span></div>";
                $("#ctl00_cphTransaction_tdDOB" + i).html(divdata);
                if (i == 0 && hdnproductType == "2") {
                    var divdata = "<div class='input-group date txtDate'>" + $("#ctl00_cphTransaction_SPDsponsorDetails-dateofBirth").html() + " <span id=spanESdob" + i + " class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span></div>";
                    $("#ctl00_cphTransaction_SPDsponsorDetails-dateofBirth").html(divdata);
                }
            }
            for (var i = 0; i < pscount; i++) {
                var relation = $("#ctl00_cphTransaction_ddlInsRelation" + i).val();
               var dob = $("#ctl00_cphTransaction_txtInsureDOB" + i).val() ;
                if (relation == "SELF") {
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker('disable');
                    $("#spanIdob" + i).hide();
                    $("#ctl00_cphTransaction_txtInsDOB" + i).css('width', '200%');                     
                }
                else if (relation == 'UDTR' || relation == 'SONM') {
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker({
                        changeYear: true,
                        changeMonth: true,
                        maxDate: maxDate,
                        yearRange: year - 20 + ":" + year + 17,
                        dateFormat: "dd/M/yy",
                    }).val();
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker("option", "maxDate", new Date(year + 17, month, day, 1));
                }
                else {                 
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker({
                        changeYear: true,
                        changeMonth: true,
                        maxDate: maxDate,
                        yearRange: year - 80 + ":" + year,
                        dateFormat: "dd/M/yy",
                    }).val();
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker("option", "maxDate", new Date(year, month, day, 1));                   
                }
                 $("#ctl00_cphTransaction_txtInsDOB" + i).val(dob);
            }
            $("#ctl00_cphTransaction_txtESDob").datepicker({
                changeYear: true,
                changeMonth: true,
                maxDate: maxDate,
                yearRange: year - 80 + ":" + year,
                dateFormat: "dd/M/yy",
            }).val();
            $("#ctl00_cphTransaction_txtESDob").datepicker("option", "maxDate", new Date(year, month, day, 1));

            $(".input-group-addon").click(function () {
                var id = this.id;
                var i = 0;
                if (id.startsWith("spanIdob")) {
                    i = this.id.replace("spanIdob", "");
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker("show");
                }
                else if (id == "spanESdob0")
                    $("#ctl00_cphTransaction_txtESDob").datepicker("show");
                else if (id == "ctl00_cphTransaction_lbldob")
                    $("#<%=txtdob.ClientID%>").datepicker('show');
            });
            for (var i = 0; i < 6; i++) {
                $('#ctl00_cphTransaction_txtInsPassportNo' + i).attr('onKeyPress', "return isChar(this.event);");//onKeyPress="return isChar(event)"
            }
        }
        //VAlidating the passport Numbers        
        function passportNo(id) {
            var regsaid = /[a-zA-Z]{1}[0-9]{7}/;
            var passport = $("#" + id).val();
            var i = id.replace('ctl00_cphTransaction_txtInsPassportNo', '')
            if (regsaid.test(passport) == false) {
                document.getElementById("errortxtInsPassportNo" + i).innerText = "Please Enter Valid Passport No.";
                $("#" + id).css("border-color", "Red");
            }
            else {
                $("#" + id).css("border-color", "");
                $("#errortxtInsPassportNo" + i).val('');
            }
        }

        $(document).ready(function () {
            //Hiding the optional cover when Explore product.
            var rowCount = $('#ctl00_cphTransaction_tblOptionalCovers tr').length;
            if (rowCount < 1) {
                $('#divOptionalCovers').hide();
                $('#divopcovers').hide();
            }
            //Hiding the Education when Explore product.
            rowCount = $('#ctl00_cphTransaction_tblEducationDetails tr').length;
            if (rowCount < 1) {
                $('#divEducationDetails').hide();
                $('#divopcovers').hide();
            }
            //Hiding the questionary.
            var status = $('#ctl00_cphTransaction_hdnstatus').val();//postback
            if (status != "postback") {
                $('.trQuestions').hide();
            }
            var i = 0;
            var qids = [];
            $("#ctl00_cphTransaction_tblQuestionire tr").each(function () {
                var cls = $(this).attr('class');
                var value = $(this).attr('class').replace("Main", "");
                var ped = $("#ctl00_cphTransaction_lblPED").text().toUpperCase();
                if (cls.search('Main') > 0) {
                    var spandata = "&nbsp;<span id='" + value + "' class='more-less-head glyphicon glyphicon-question-sign'></span>";
                    if ($("#" + value).length == 0)
                        $(this).find('td:first').append(spandata);
                    qids[i] = value;
                    i++;

                    if (ped.toUpperCase() != "YES") {
                        var id = value.replace('tr', '_rb_') + "Yes";
                        $("#ctl00_cphTransaction" + id).attr('disabled', true);
                        $("#divquestionary").attr('disabled', true);
                    }
                }

                var status = true;
                var checked_Radio = $(this).find('span[class=radioButton]');
                // var checked_Radio = $('#ctl00_cphTransaction_tblQuestionire tbody tr span[class=radioButton]');
                if (checked_Radio.length > 0) {
                    for (var i = 0; i < checked_Radio.length; i++) {
                        if (checked_Radio[i].innerText.toUpperCase() == 'YES') {
                            var id = checked_Radio[i].firstChild.id;
                            var isChecked = $('#' + id).is(":checked");
                            if (!isChecked)
                                status = false;

                            if (!status) {
                                value == 'trpedYesNo' ? $(".trQuestions").hide() : $("." + value).hide();
                            }

                        }
                    }
                }
            });
            $('span').click(function () {
                for (var i = 0; i < qids.length; i++) {
                    if ($(this).attr('id') == qids[i]) {
                        $('#dialogq' + parseInt(i + 1)).dialog({
                            draggable: false,
                            maxWidth: 600,
                            maxHeight: 300,
                            width: 600,
                            height: 300,
                        });
                    }
                }
            });
            $('#terms').click(function () {
                $("#dialogterms").dialog({
                    draggable: false,
                    maxWidth: 600,
                    maxHeight: 600,
                    width: 600,
                    height: 600,
                });

            });
            $('#quesTravel').mouseover(function () {
                $('#dialogq1').dialog({
                    draggable: false,
                    maxHeight: 250,
                    width: 600,
                    height: 250,
                });
            });
            $('#spanquestionary').click(function () {
                var data = $('#spanquestionary').val();
                if (data == "") {
                    $('#spanquestionary').val('plus');
                    $('.card-body').hide();
                    $('#spanquestionary').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanquestionary').val('');
                    $('.card-body').show();
                    $('#spanquestionary').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanInsureddetails').click(function () {
                var data = $('#spanInsureddetails').val();
                if (data == "") {
                    $('#spanInsureddetails').val('plus');
                    $('#ctl00_cphTransaction_tblInsuredDetails').hide();
                    $('#spanInsureddetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanInsureddetails').val('');
                    $('#ctl00_cphTransaction_tblInsuredDetails').show();
                    $('#spanInsureddetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });

            $('#spanEducationDetails').click(function () {
                var data = $('#spanEducationDetails').val();
                if (data == "") {
                    $('#spanEducationDetails').val('plus');
                    $('#ctl00_cphTransaction_tblEducationDetails').hide();
                    $('#spanEducationDetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanEducationDetails').val('');
                    $('#ctl00_cphTransaction_tblEducationDetails').show();
                    $('#spanEducationDetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });

            $('#spanoptionalquestionary').click(function () {
                var data = $('#spanoptionalquestionary').val();
                if (data == "") {
                    $('#spanoptionalquestionary').val('plus');
                    $('#ctl00_cphTransaction_tblOptionalCovers').hide();
                    $('#spanoptionalquestionary').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanoptionalquestionary').val('');
                    $('#ctl00_cphTransaction_tblOptionalCovers').show();
                    $('#spanoptionalquestionary').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });

            $('#spanproposerdetails').click(function () {
                var data = $('#spanproposerdetails').val();
                if (data == "") {
                    $('#spanproposerdetails').val('plus');
                    $('#divproposerdetails').hide();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanproposerdetails').val('');
                    $('#divproposerdetails').show();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });

            $("#ctl00_cphTransaction_txtPassportNo").change(function () {
                var regsaid = /[a-zA-Z]{1}[0-9]{7}/;
                var passport = $("#<%=txtPassportNo.ClientID%>").val();
                if (regsaid.test(passport) == false) {
                    document.getElementById("errorPassportNo").innerText = "Please Enter Valid Passport No.";
                    $("#<%=txtPassportNo.ClientID%>").css("border-color", "Red");
                    $("#<%=txtPassportNo.ClientID%>").val('');
                }
                else {
                    $("#<%=txtPassportNo.ClientID%>").css("border-color", "");
                    $('#errorPassportNo').val('');
                }
            });
            $("#<%=ddlCity.ClientID%>").change(function () {
                $("#<%=txtcity.ClientID%>").val($("#<%=ddlCity.ClientID%>").val());
            });
            //Ajax call for searching the cities using pincodes.
            $("#<%=txtPincode.ClientID%>").change(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                $(".transparentCover").show();
                var options = "";
                $("#<%=ddlCity.ClientID%>").empty();
                options += '<option value=' + -1 + '>-- Select --</option>'
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReligareProposerDetails.aspx/GetCityCodes",
                    data: "{'pincode':'" + $("#<%=txtPincode.ClientID%>").val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (index, item) {
                            if (index == 0)
                                $("#<%=txtState.ClientID%>").val(item);
                            else
                                options += '<option value=' + item + '>' + item + '</option>'
                        });
                        $("#<%=ddlCity.ClientID%>").append(options);
                        $('#' + getElement('ddlCity').id).select2('val', '-1');
                        $(".loading").hide();
                        $(".transparentCover").hide();
                    },
                    error: function (result) {
                        alert("Cities Not Found Searching criteria.");
                        $(".loading").hide();
                        $(".transparentCover").hide();
                    }
                });
            });
            //show the datepickers 
            $(".input-group-addon").click(function () {
                var id = this.id;
                var i = 0;
                if (id.startsWith("spanIdob")) {
                    i = this.id.replace("spanIdob", "");
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker("show");
                }
                else if (id == "spanESdob0")
                    $("#ctl00_cphTransaction_txtESDob").datepicker("show");
                else if (id == "ctl00_cphTransaction_lbldob")
                    $("#<%=txtdob.ClientID%>").datepicker('show');
            });
        });

        //Enable the Events when postback.
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {

            //show the datepickers.
            $(".input-group-addon").click(function () {
                var id = this.id;
                var i = 0;
                if (id.startsWith("spanIdob")) {
                    i = this.id.replace("spanIdob", "");
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker("show");
                }
                else if (id == "spanESdob0")
                    $("#ctl00_cphTransaction_txtESDob").datepicker("show");
                else if (id == "ctl00_cphTransaction_lbldob")
                    $("#<%=txtdob.ClientID%>").datepicker('show');
            });
            //Hiding the optional cover table and div.
            var rowCount = $('#ctl00_cphTransaction_tblOptionalCovers tr').length;
            if (rowCount < 1) {
                $('#divOptionalCovers').hide();
                $('#divopcovers').hide();
            }

            //Hiding the Education Details table and div.
            rowCount = $('#ctl00_cphTransaction_tblEducationDetails tr').length;
            if (rowCount < 1) {
                $('#divEducationDetails').hide();
                $('#divopcovers').hide();
            }
            // assigning the selected city value into textbox control.
            $("#<%=ddlCity.ClientID%>").change(function () {
                $("#<%=txtcity.ClientID%>").val($("#<%=ddlCity.ClientID%>").val());
            });
            //Ajax call for searching the cities using pincodes.
            $("#<%=txtPincode.ClientID%>").change(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                $(".transparentCover").show();
                var options = "";
                $("#<%=ddlCity.ClientID%>").empty();
                options += '<option value=' + -1 + '>-- Select --</option>'
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReligareProposerDetails.aspx/GetCityCodes",
                    data: "{'pincode':'" + $("#<%=txtPincode.ClientID%>").val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (index, item) {
                            if (index == 0)
                                $("#<%=txtState.ClientID%>").val(item);
                            else
                                options += '<option value=' + item + '>' + item + '</option>'
                        });
                        $("#<%=ddlCity.ClientID%>").append(options);
                        $('#' + getElement('ddlCity').id).select2('val', '-1');
                        $(".loading").hide();
                        $(".transparentCover").hide();
                    },
                    error: function (result) {
                        alert("Cities Not Found Searching criteria.");
                        $(".loading").hide();
                        $(".transparentCover").hide();
                    }
                });
            });
            //hiding the question when page load.
            var status = $('#ctl00_cphTransaction_hdnstatus').val();//postback
            if (status != "postback")
                $('.trQuestions').hide();
            var i = 0;
            var qids = [];
            $("#ctl00_cphTransaction_tblQuestionire tr").each(function () {
                var cls = $(this).attr('class');
                var value = $(this).attr('class').replace("Main", "");
                if (cls.search('Main') > 0) {
                    var spandata = "&nbsp;<span id='" + value + "' class='more-less-head glyphicon glyphicon-question-sign'></span>";
                    if ($("#" + value).length == 0) {
                        $(this).find('td:first').append(spandata);
                    }
                    qids[i] = value;
                    i++;
                    var ped = $("#ctl00_cphTransaction_lblPED").text().toUpperCase();
                    if (ped.toUpperCase() != "YES") {
                        var id = value.replace('tr', '_rb_') + "Yes";
                        $("#ctl00_cphTransaction" + id).attr('disabled', true);
                        $("#divquestionary").attr('disabled', true);
                    }
                }
                if (status != "postback") {
                    $("." + value).hide();
                }
            });
            $('span').click(function () {
                for (var i = 0; i < qids.length; i++) {
                    if ($(this).attr('id') == qids[i]) {
                        $('#dialogq' + parseInt(i + 1)).dialog({
                            draggable: false,
                            maxWidth: 600,
                            maxHeight: 300,
                            width: 600,
                            height: 300,
                        });
                    }
                }
            });
            $('#terms').click(function () {
                $("#dialogterms").dialog({
                    draggable: false,
                    maxWidth: 600,
                    maxHeight: 600,
                    width: 600,
                    height: 600,
                });
            });
            $('#quesTravel').mouseover(function () {
                $('#dialogq1').dialog({
                    draggable: false,
                    maxWidth: 600,
                    maxHeight: 250,
                    width: 600,
                    height: 250,
                });
            });
            $('#spanquestionary').click(function () {
                var data = $('#spanquestionary').val();
                if (data == "") {
                    $('#spanquestionary').val('plus');
                    $('.card-body').hide();
                    $('#spanquestionary').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanquestionary').val('');
                    $('.card-body').show();
                    $('#spanquestionary').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanInsureddetails').click(function () {
                var data = $('#spanInsureddetails').val();
                if (data == "") {
                    $('#spanInsureddetails').val('plus');
                    $('#ctl00_cphTransaction_tblInsuredDetails').hide();
                    $('#spanInsureddetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanInsureddetails').val('');
                    $('#ctl00_cphTransaction_tblInsuredDetails').show();
                    $('#spanInsureddetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanproposerdetails').click(function () {
                var data = $('#spanproposerdetails').val();
                if (data == "") {
                    $('#spanproposerdetails').val('plus');
                    $('#divproposerdetails').hide();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanproposerdetails').val('');
                    $('#divproposerdetails').show();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanproposerdetails').click(function () {
                var data = $('#spanproposerdetails').val();
                if (data == "") {
                    $('#spanproposerdetails').val('plus');
                    $('#divproposerdetails').hide();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanproposerdetails').val('');
                    $('#divproposerdetails').show();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanEducationDetails').click(function () {
                var data = $('#spanEducationDetails').val();
                if (data == "") {
                    $('#spanEducationDetails').val('plus');
                    $('#ctl00_cphTransaction_tblEducationDetails').hide();
                    $('#spanEducationDetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanEducationDetails').val('');
                    $('#ctl00_cphTransaction_tblEducationDetails').show();
                    $('#spanEducationDetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanoptionalquestionary').click(function () {
                var data = $('#spanoptionalquestionary').val();
                if (data == "") {
                    $('#spanoptionalquestionary').val('plus');
                    $('#ctl00_cphTransaction_tblOptionalCovers').hide();
                    $('#spanoptionalquestionary').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanoptionalquestionary').val('');
                    $('#ctl00_cphTransaction_tblOptionalCovers').show();
                    $('#spanoptionalquestionary').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
            $('#spanproposerdetails').click(function () {
                var data = $('#spanproposerdetails').val();
                if (data == "") {
                    $('#spanproposerdetails').val('plus');
                    $('#divproposerdetails').hide();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus");
                }
                else {
                    $('#spanproposerdetails').val('');
                    $('#divproposerdetails').show();
                    $('#spanproposerdetails').attr("class", "more-less-head glyphicon glyphicon-plus glyphicon-minus");
                }
            });
        }
        var parameter = Sys.WebForms.PageRequestManager.getInstance();
        parameter.add_endRequest(function () {
            sample();
        });
        $("#divquestionary").attr('disabled', 'disabled');
        function sample() {
            $("#ctl00_cphTransaction_tblQuestionire tr").each(function () {

                var td = $(this).find('td:nth-child(2)');
                var value = $(this).attr('class').replace("Main", "");
                if (value.startsWith('trT00')) {
                    var a = $("#ctl00_cphTransaction_rb_" + value.replace('tr', '') + 'Yes');
                    var isChecked = $('#ctl00_cphTransaction_rb_' + value.replace('tr', '') + 'Yes').is(":checked");
                    if (isChecked) {
                        $('.' + value).show();
                    }
                    else {
                        $('.' + value).hide();
                    }
                }
            });
            var isChecked = $("#ctl00_cphTransaction_rb_pedYesNoYes").is(":checked");
            if (isChecked) {
                $('.trQuestions').show();
            }
            else {
                $('.trQuestions').hide();
            }
        }
        //Enable/Disable the Questionary.
        function enableQuestionary(id) {
            var d = "";
            var pscount = $("#<%=hdnNoofPax.ClientID%>").val();
            if (id.startsWith("ddl") || id.startsWith("cb") || id.startsWith("rb"))
                d = "ctl00_cphTransaction_" + id;
            else
                d = id.id;
            var rstatus = 0;
            for (var i = 0; i < pscount; i++) {
                if (d == "ctl00_cphTransaction_ddl_pedYesNo_" + i || d == "ctl00_cphTransaction_rb_pedYesNoYes" || d == "ctl00_cphTransaction_rb_pedYesNoNo") {
                    rstatus = $("#ctl00_cphTransaction_rb_pedYesNoYes").is(":checked");
                    status = $("#ctl00_cphTransaction_ddl_pedYesNo_" + i).val();
                    var checked_checkboxes = $("[class*=cb" + i + "] input");
                    if (rstatus && status == "1") {
                        for (var i = 0; i < checked_checkboxes.length; i++) {
                            var cbid = checked_checkboxes[i].id;
                            $('#' + cbid).prop("disabled", false);
                        }
                    }
                    else {
                        for (var j = 0; j < checked_checkboxes.length; j++) {
                            var cbid = checked_checkboxes[j].id;
                            $('#' + cbid).prop("disabled", true);
                            $('#' + cbid).prop("checked", false);
                        }
                        $('#ctl00_cphTransaction_txt_otherDiseasesDescription_' + i).val('');
                        $('#ctl00_cphTransaction_txt_otherDiseasesDescription_' + i).prop('disabled', true);
                    }
                }
                else if (d == "ctl00_cphTransaction_ddl_T001_" + i || d == "ctl00_cphTransaction_rb_T001Yes" || d == "ctl00_cphTransaction_rb_T001No") {
                    rstatus = $("#ctl00_cphTransaction_rb_T001Yes").is(":checked");
                    status = $("#ctl00_cphTransaction_ddl_T001_" + i).val();
                    if ($("#ctl00_cphTransaction_txt_T001_" + i).prop('disabled') && status == "1" && rstatus)
                        $('#ctl00_cphTransaction_txt_T001_' + i).prop('disabled', false);
                    else {
                        $('#ctl00_cphTransaction_txt_T001_' + i).prop('disabled', true);
                        $('#ctl00_cphTransaction_txt_T001_' + i).val('');
                    }
                }
                else if (d == "ctl00_cphTransaction_ddl_T002_" + i || d == "ctl00_cphTransaction_rb_T002Yes" || d == "ctl00_cphTransaction_rb_T002No") {
                    rstatus = $("#ctl00_cphTransaction_rb_T002Yes").is(":checked");
                    status = $("#ctl00_cphTransaction_ddl_T002_" + i).val();
                    if ($("#ctl00_cphTransaction_txt_T002_" + i).prop('disabled') && status == "1" && rstatus)
                        $('#ctl00_cphTransaction_txt_T002_' + i).prop('disabled', false);
                    else {
                        $('#ctl00_cphTransaction_txt_T002_' + i).val('');
                        $('#ctl00_cphTransaction_txt_T002_' + i).prop('disabled', true);
                    }
                }
                else if (d == "ctl00_cphTransaction_cb_210_" + i) {
                    rstatus = $("#ctl00_cphTransaction_rb_pedYesNoYes").is(":checked");
                    status = $("#ctl00_cphTransaction_ddl_pedYesNo_" + i).val();
                    if ($("#ctl00_cphTransaction_txt_otherDiseasesDescription_" + i).prop('disabled') && status == "1" && rstatus)
                        $('#ctl00_cphTransaction_txt_otherDiseasesDescription_' + i).prop('disabled', false);
                    else {
                        $('#ctl00_cphTransaction_txt_otherDiseasesDescription_' + i).val('');
                        $('#ctl00_cphTransaction_txt_otherDiseasesDescription_' + i).prop('disabled', true);
                    }
                }
            }
            sample();
        }
        function proposerDOB() {
            $('#ctl00_cphTransaction_txtpropdob').val($('#ctl00_cphTransaction_txtdob').val());
        }
        function insuredDob(id) {
            var i = id.replace("txtInsDOB", "");
            $('#ctl00_cphTransaction_txtInsureDOB' + i).val($('#ctl00_cphTransaction_' + id).val());
        }
       
    </script>
    <script type="text/javascript">
        //Validating the proposer,Insured,Educational and optional covers.
        function proposerValidation() {
            var isvalid = true;
            var pscount = $("#<%=hdnNoofPax.ClientID%>").val();
            var hdnproductType = $("#ctl00_cphTransaction_hdnproductType").val();
            var regsaid = /[a-zA-Z]{1}[0-9]{7}/;

            //Proposer Details
            var ddlNationality = $("#<%=ddlNationality.ClientID%>").val();
            if (ddlNationality == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlNationality").focus();
                $("#s2id_ctl00_cphTransaction_ddlNationality").css("border-color", "Red");
                document.getElementById("errorNationality").innerText = "Please select Nationality.";
                isvalid = false;
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlNationality").css("border-color", "");
                $('#errorNationality').val('');
            }
            var txtPassportNo = $("#<%=txtPassportNo.ClientID%>").val();
            if (txtPassportNo == "") {
                $("#<%=txtPassportNo.ClientID%>").focus();
                $("#<%=txtPassportNo.ClientID%>").css("border-color", "Red");
                document.getElementById("errorPassportNo").innerText = "Please Enter Passport No.";
                isvalid = false;
            }
            else {
                if (!(txtPassportNo.match(regsaid))) {
                    $("#<%=txtPassportNo.ClientID%>").focus();
                    $("#<%=txtPassportNo.ClientID%>").css("border-color", "Red");
                    document.getElementById("errorPassportNo").innerText = "Please Enter the valid Passport No.";
                    isvalid = false;
                }
                else {
                    $("#<%=txtPassportNo.ClientID%>").css("border-color", "");
                    document.getElementById("errorPassportNo").innerText = "";
                }
            }

            var ddlTitle = $("#<%=ddlTitle.ClientID%>").val();
            if (ddlTitle == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlTitle").focus();
                $("#s2id_ctl00_cphTransaction_ddlTitle").css("border-color", "Red");
                document.getElementById("errorddlTitle").innerText = "Please select Title.";
                isvalid = false;
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlTitle").css("border-color", "");
                $('#errorddlTitle').val('');
            }

            var txtFirstName = $("#<%=txtFirstName.ClientID%>").val();
            if (txtFirstName == "") {
                $("#<%=txtFirstName.ClientID%>").focus();
                $("#<%=txtFirstName.ClientID%>").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorFirstName").innerText = "Please Enter First Name.";
            } else {
                $("#<%=txtFirstName.ClientID%>").css("border-color", "");
                $('#errorFirstName').val('');
            }

            var txtLastName = $("#<%=txtLastName.ClientID%>").val();
            if (txtLastName == "") {
                $("#<%=txtLastName.ClientID%>").focus();
                $("#<%=txtLastName.ClientID%>").css("border-color", "Red");
                document.getElementById("errortxtLastName").innerText = "Please Enter Last Name.";
                isvalid = false;
            } else {
                $("#<%=txtLastName.ClientID%>").css("border-color", "");
                $('#errortxtLastName').val('');
            }

            var txtdob = $("#<%=txtdob.ClientID%>").val();
            if (txtdob == "") {
                $("#<%=txtdob.ClientID%>").focus();
                $("#<%=txtdob.ClientID%>").css("border-color", "Red");
                document.getElementById("errortxtdob").innerText = "Please Enter Date of Birth.";
                isvalid = false;
            } else {
                $("#<%=txtdob.ClientID%>").css("border-color", "");
                $('#errortxtdob').val('');
            }

            var txtAddressLine1 = $("#<%=txtAddressLine1.ClientID%>").val();
            if (txtAddressLine1 == "") {
                $("#<%=txtAddressLine1.ClientID%>").focus();
                $("#<%=txtAddressLine1.ClientID%>").css("border-color", "Red");
                document.getElementById("errorAddressLine1").innerText = "Please Enter Address Line 1";
                isvalid = false;
            } else {
                $("#<%=txtAddressLine1.ClientID%>").css("border-color", "");
                $('#errorAddressLine1').val('');
            }

            var txtAddressLine2 = $("#<%=txtAddressLine2.ClientID%>").val();
            if (txtAddressLine2 == "") {
                $("#<%=txtAddressLine2.ClientID%>").focus();
                $("#<%=txtAddressLine2.ClientID%>").css("border-color", "Red");
                document.getElementById("errorAddressLine2").innerText = "Please Enter Address Line 2.";
                isvalid = false;
            } else {
                $("#<%=txtAddressLine2.ClientID%>").css("border-color", "");
                $('#errorAddressLine2').val('');
            }
            var txtEmail = $("#<%=txtEmail.ClientID%>").val();
            if (txtEmail == "") {
                $("#<%=txtEmail.ClientID%>").focus();
                $("#<%=txtEmail.ClientID%>").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errortxtEmail").innerText = "Please Enter Email.";
            } else {
                var mailformat = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                if (!(txtEmail.match(mailformat))) {
                    $("#<%=txtEmail.ClientID%>").focus();
                    $("#<%=txtEmail.ClientID%>").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errortxtEmail").innerText = "Please Enter Valid Email.";
                } else {
                    $("#<%=txtEmail.ClientID%>").css("border-color", "");
                    document.getElementById("errortxtEmail").innerText = "";
                }
            }
            var txtMobileNo = $("#<%=txtMobileNo.ClientID%>").val();
            if (txtMobileNo == "" || txtMobileNo.length < 10 || parseInt(txtMobileNo[0]) < 6) {
                $("#<%=txtMobileNo.ClientID%>").focus();
                $("#<%=txtMobileNo.ClientID%>").css("border-color", "Red");
                document.getElementById("errortxtMobileNo").innerText = "Please Enter Mobile No.";
                if (txtMobileNo.length < 10 || parseInt(txtMobileNo[0]) < 6)
                    document.getElementById("errortxtMobileNo").innerText = "Please Enter valid Mobile No.";
                isvalid = false;
            } else {
                $("#<%=txtMobileNo.ClientID%>").css("border-color", "");
                document.getElementById("errortxtMobileNo").innerText = "";
            }

            var txtPincode = $("#<%=txtPincode.ClientID%>").val();
            if (txtPincode == "") {
                $("#<%=txtPincode.ClientID%>").focus();
                $("#<%=txtPincode.ClientID%>").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errortxtPincode").innerText = "Please Enter PinCode.";
            } else {
                $("#<%=txtPincode.ClientID%>").css("border-color", "");
                $('#errortxtPincode').val('');
            }
            var ddlCity = $("#<%=ddlCity.ClientID%>").val();
            if (ddlCity == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlCity").focus();
                $("#s2id_ctl00_cphTransaction_ddlCity").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorddlCity").innerText = "Please select City.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlCity").css("border-color", "");
                $('#errorddlCity').val('');
            }
            var txtState = $("#<%=txtState.ClientID%>").val();
            if (txtState == "") {
                $("#<%=txtState.ClientID%>").focus();
                $("#<%=txtState.ClientID%>").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errortxtState").innerText = "Please Enter State.";
            } else {
                $("#<%=txtState.ClientID%>").css("border-color", "");
                $('#errortxtState').val('');
            }

            var txtNomineeName = $("#<%=txtNomineeName.ClientID%>").val();
            if (txtNomineeName == "") {
                $("#<%=txtNomineeName.ClientID%>").focus();
                $("#<%=txtNomineeName.ClientID%>").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errortxtNomineeName").innerText = "Please Enter Nominee Name.";
            } else {
                $("#<%=txtNomineeName.ClientID%>").css("border-color", "");
                $('#errortxtNomineeName').val('');
            }

            var ddlNominee = $("#<%=ddlNominee.ClientID%>").val();
            if (ddlNominee == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlNominee").focus();
                $("#s2id_ctl00_cphTransaction_ddlNominee").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorddlNominee").innerText = "Please select the Nominee Relation.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlNominee").css("border-color", "");
                $('#errorddlNominee').val('');
            }

            var ddlPurposeVisit = $("#<%=ddlPurposeVisit.ClientID%>").val();
            if (ddlPurposeVisit == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlPurposeVisit").focus();
                $("#s2id_ctl00_cphTransaction_ddlPurposeVisit").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorddlPurposeVisit").innerText = "Please select purpose of visit.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlPurposeVisit").css("border-color", "");
                $('#errorddlPurposeVisit').val('');
            }
            //validating the residence and proof details while nationality is Indian.
            if (ddlNationality != "IN") {
                var ddlresidenceProof = $("#<%=ddlresidenceProof.ClientID%>").val();
                if (ddlresidenceProof == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlresidenceProof").focus();
                    $("#s2id_ctl00_cphTransaction_ddlresidenceProof").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlresidenceProof").innerText = "Please select Residence Proof.";
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlresidenceProof").css("border-color", "");
                    $('#errorddlresidenceProof').val('');
                }

                var txtProof = $("#<%=txtProof.ClientID%>").val();
                if (txtProof == "") {
                    $("#<%=txtProof.ClientID%>").focus();
                    $("#<%=txtProof.ClientID%>").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errortxtProof").innerText = "Please Enter Proof details.";
                } else {
                    $("#<%=txtProof.ClientID%>").css("border-color", "");
                    $('#errortxtProof').val('');
                }
            }

            //Insured Details
            var passportNo = [];
            var self = [];
            for (var i = 0; i < pscount; i++) {
                var ddlRelation = $("#ctl00_cphTransaction_ddlInsRelation" + i).val();
                if (ddlRelation == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlInsRelation" + i).focus();
                    $("#s2id_ctl00_cphTransaction_ddlInsRelation" + i).css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlInsRelation" + i).innerText = "Please Select Relation.";
                } else {
                    $("#s2id_ctl00_cphTransaction_ddlInsRelation" + i).css("border-color", "");
                    $('#errorddlInsRelation' + i).val('');
                    self[i] = ddlRelation;
                }

                var ddlInsNationality = $("#ctl00_cphTransaction_ddlInsNationality" + i).val();
                if (ddlInsNationality == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlInsNationality" + i).focus();
                    $("#s2id_ctl00_cphTransaction_ddlInsNationality" + i).css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlInsNationality" + i).innerText = "Please Select Nationality.";
                } else {
                    $("#s2id_ctl00_cphTransaction_ddlInsNationality" + i).css("border-color", "");
                    $('#errorddlInsNationality' + i).val('');
                }

                var ddlInsTitle = $("#ctl00_cphTransaction_ddlInsTitle" + i).val();
                if (ddlInsTitle == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlInsTitle" + i).focus();
                    $("#s2id_ctl00_cphTransaction_ddlInsTitle" + i).css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlInsTitle" + i).innerText = "Please Select Title.";
                } else {
                    $("#s2id_ctl00_cphTransaction_ddlInsTitle" + i).css("border-color", "");
                    $('#errorddlInsTitle' + i).val('');
                }

                var txtInsPassportNo = $("#ctl00_cphTransaction_txtInsPassportNo" + i).val();
                if (txtInsPassportNo == "") {
                    $("#ctl00_cphTransaction_txtInsPassportNo" + i).focus();
                    $("#ctl00_cphTransaction_txtInsPassportNo" + i).css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errortxtInsPassportNo" + i).innerText = "Please Enter Passport No.";
                } else {
                    if (!(txtInsPassportNo.match(regsaid))) {
                        $("#ctl00_cphTransaction_txtInsPassportNo" + i).focus();
                        $("#ctl00_cphTransaction_txtInsPassportNo" + i).css("border-color", "Red");
                        document.getElementById("errortxtInsPassportNo" + i).innerText = "Please Enter the valid Passport No.";
                        isvalid = false;
                    }
                    else {
                        $("#ctl00_cphTransaction_txtInsPassportNo" + i).css("border-color", "");
                        document.getElementById("errortxtInsPassportNo" + i).innerText = "";
                        passportNo[i] = txtInsPassportNo;
                    }
                }

                var txtInsFirstName = $("#ctl00_cphTransaction_txtInsFirstName" + i).val();
                if (txtInsFirstName == "") {
                    $("#ctl00_cphTransaction_txtInsFirstName" + i).focus();
                    $("#ctl00_cphTransaction_txtInsFirstName" + i).css("border-color", "Red");
                    document.getElementById("errortxtInsFirstName" + i).innerText = "Please Enter First Name.";
                    isvalid = false;
                } else {
                    $("#ctl00_cphTransaction_txtInsFirstName" + i).css("border-color", "");
                    $('#errortxtInsFirstName' + i).val('');
                }

                var txtInsLastName = $("#ctl00_cphTransaction_txtInsLastName" + i).val();
                if (txtInsLastName == "") {
                    $("#ctl00_cphTransaction_txtInsLastName" + i).focus();
                    $("#ctl00_cphTransaction_txtInsLastName" + i).css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errortxtInsLastName" + i).innerText = "Please Enter Last Name.";
                } else {
                    $("#ctl00_cphTransaction_txtInsLastName" + i).css("border-color", "");
                    $('#errortxtInsLastName' + i).val('');
                }

                var txtInsDOB = $("#ctl00_cphTransaction_txtInsDOB" + i).val();
                if (txtInsDOB == "") {
                    $("#ctl00_cphTransaction_txtInsDOB" + i).focus();
                    $("#ctl00_cphTransaction_txtInsDOB" + i).css("border-color", "Red");
                    document.getElementById("errortxtInsDOB" + i).innerText = "Please select Date of Birth.";
                    $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker('hide');
                    isvalid = false;
                } else {                   
                    var date = new Date(txtInsDOB);
                    var dtToday = new Date();
                    var diff = Math.floor((dtToday - date) / 86400000);
                    var years = diff / 365.25;
                    if ((years < 12 || years > 40) && hdnproductType == 2) {
                        $("#ctl00_cphTransaction_txtInsDOB" + i).focus();
                        $("#ctl00_cphTransaction_txtInsDOB" + i).css("border-color", "Red");
                        document.getElementById("errortxtInsDOB" + i).innerText = "Age should be 12 to 40 years.";
                        $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker('hide');
                        isvalid = false;
                    }
                    else {
                        $("#ctl00_cphTransaction_txtInsDOB" + i).css("border-color", "");
                        document.getElementById("errortxtInsDOB" + i).innerText = "";
                    }
                    $("#ctl00_cphTransaction_txtInsProof" + i).val(txtInsDOB);
                   <%--  if ((ddlRelation == "SONM" || ddlRelation == "UDTR")) {
                        var proposerdob = new Date($("#<%=txtdob.ClientID%>").val());
                        var proposerdiff = Math.floor((date - proposerdob) / 86400000);
                        var year = proposerdiff / 365.25;
                        if (year < 18) {
                            alert(diff + ',' + years);
                            $("#ctl00_cphTransaction_txtInsDOB" + i).focus();
                            $("#ctl00_cphTransaction_txtInsDOB" + i).css("border-color", "Red");
                            document.getElementById("errortxtInsDOB" + i).innerText = "Age should be 18 years insured and proposer.";
                            $("#ctl00_cphTransaction_txtInsDOB" + i).datepicker('hide');
                            isvalid = false;
                        }
                        else {
                            $("#ctl00_cphTransaction_txtInsDOB" + i).css("border-color", "");
                            document.getElementById("errortxtInsDOB" + i).innerText = "";
                        }
                    }
                    else {
                        $("#ctl00_cphTransaction_txtInsDOB" + i).css("border-color", "");
                        document.getElementById("errortxtInsDOB" + i).innerText = "";
                    }--%>
                }
                if (ddlInsNationality != "IN") {
                    var ddlInsResidence = $("#ctl00_cphTransaction_ddlInsResidence" + i).val();

                    if (ddlInsResidence == "-1") {
                        $("#s2id_ctl00_cphTransaction_ddlInsResidence" + i).focus();
                        $("#s2id_ctl00_cphTransaction_ddlInsResidence" + i).css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errorddlInsResidence" + i).innerText = "Please Select Residence.";
                    } else {
                        $("#s2id_ctl00_cphTransaction_ddlInsResidence" + i).css("border-color", "");
                        $('#errorddlInsResidence' + i).val('');
                    }

                    var txtInsProof = $("#ctl00_cphTransaction_txtInsProof" + i).val();
                    if (txtInsProof == "") {
                        $("#ctl00_cphTransaction_txtInsProof" + i).focus();
                        $("#ctl00_cphTransaction_txtInsProof" + i).css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtInsProof" + i).innerText = "Please Enter Proof.";
                    } else {
                        $("#ctl00_cphTransaction_txtInsProof" + i).css("border-color", "");
                        $('#errortxtInsProof' + i).val('');
                    }

                }
                var isDisabled = $("#ctl00_cphTransaction_txt_T001_" + i).prop('disabled');
                if (!isDisabled) {
                    var txtDiseaseDesc = $("#ctl00_cphTransaction_txt_T001_" + i).val();
                    if (txtDiseaseDesc == "") {
                        $("#ctl00_cphTransaction_txt_T001_" + i).focus();
                        $("#ctl00_cphTransaction_txt_T001_" + i).css("border-color", "Red");
                        isvalid = false;
                    } else { $("#ctl00_cphTransaction_txt_T001_" + i).css("border-color", ""); }
                }

                if (hdnproductType != 2) {
                    var isDisabled = $("#ctl00_cphTransaction_txt_T002_" + i).prop('disabled');
                    if (!isDisabled) {
                        var txtDiseaseDesc = $("#ctl00_cphTransaction_txt_T002_" + i).val();
                        if (txtDiseaseDesc == "") {
                            $("#ctl00_cphTransaction_txt_T002_" + i).focus();
                            $("#ctl00_cphTransaction_txt_T002_" + i).css("border-color", "Red");
                            isvalid = false;
                        } else {
                            $("#ctl00_cphTransaction_txt_T002_" + i).css("border-color", "");
                        }
                    }
                }
                // Checking other disease description.
                var isChecked = $("#ctl00_cphTransaction_cb_210_" + i).is(":checked");
                if (isChecked) {
                    var DiseaseDesc = $("#ctl00_cphTransaction_txt_otherDiseasesDescription_" + i).val();
                    if (DiseaseDesc == "") {
                        $("#ctl00_cphTransaction_txt_otherDiseasesDescription_" + i).focus();
                        $("#ctl00_cphTransaction_txt_otherDiseasesDescription_" + i).css("border-color", "Red");
                        isvalid = false;
                    } else {
                        $("#ctl00_cphTransaction_txt_otherDiseasesDescription_" + i).css("border-color", "");
                    }
                }
            }
            // checking terms and conditions
            var isChecked = $("#ctl00_cphTransaction_chkTermsCondition").is(":checked");
            if (!(isChecked)) {
                isvalid = false;
                document.getElementById("errorterms").innerText = "Please check terms and conditions.";
            } else {
                $('#errorterms').val('');
            }
            // Educational Details For Student Explorer Product.    
            if (hdnproductType == "2") {
                var rowCount = $('#ctl00_cphTransaction_tblEducationDetails tr').length;
                if (rowCount > 1) {
                    var txtEIName = $("#ctl00_cphTransaction_txtEIName").val();
                    if (txtEIName == "") {
                        $("#ctl00_cphTransaction_txtEIName").focus();
                        $("#ctl00_cphTransaction_txtEIName").css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtEIName").innerText = 'please enter the Institute Name';
                    } else {
                        $("#ctl00_cphTransaction_txtEIName").css("border-color", "");
                        $('#errortxtEIName').val('');
                    }
                    var txtEIName = $("#ctl00_cphTransaction_txtECourse").val();
                    if (txtEIName == "") {
                        $("#ctl00_cphTransaction_txtECourse").focus();
                        $("#ctl00_cphTransaction_txtECourse").css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtECourse").innerText = 'please enter the Course Name';
                    } else {
                        $("#ctl00_cphTransaction_txtECourse").css("border-color", "");
                        $('#errortxtECourse').val('');
                    }
                    var txtEIAddress = $("#ctl00_cphTransaction_txtEIAddress").val();
                    if (txtEIAddress == "") {
                        $("#ctl00_cphTransaction_txtEIAddress").focus();
                        $("#ctl00_cphTransaction_txtEIAddress").css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtEIAddress").innerText = 'please enter the Institute Address';
                    } else {
                        $("#ctl00_cphTransaction_txtEIAddress").css("border-color", "");
                        $('#errortxtEIAddress').val('');
                    }
                    var txtEICountry = $("#ctl00_cphTransaction_txtEICountry").val();
                    if (txtEICountry == "") {
                        $("#ctl00_cphTransaction_txtEICountry").focus();
                        $("#ctl00_cphTransaction_txtEICountry").css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtEICountry").innerText = 'please enter the Country';
                    } else {
                        $("#ctl00_cphTransaction_txtEICountry").css("border-color", "");
                        $('#errortxtEICountry').val('');
                    }
                    var txtESponser = $("#ctl00_cphTransaction_txtESponser").val();
                    if (txtESponser == "") {
                        $("#ctl00_cphTransaction_txtESponser").focus();
                        $("#ctl00_cphTransaction_txtESponser").css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtESponser").innerText = 'please enter the Sponser Name';
                    } else {
                        $("#ctl00_cphTransaction_txtESponser").css("border-color", "");
                        $('#errortxtESponser').val('');
                    }
                    var txtESDob = $("#ctl00_cphTransaction_txtESDob").val();
                    if (txtESDob == "") {
                        $("#ctl00_cphTransaction_txtESDob").focus();
                        $("#ctl00_cphTransaction_txtESDob").css("border-color", "Red");
                        isvalid = false;
                        document.getElementById("errortxtESDob").innerText = 'please select the Sponser DOB';
                    } else {
                        $("#ctl00_cphTransaction_txtESDob").css("border-color", "");
                        $('#errortxtESDob').val('');
                    }
                    var rel = $("#ctl00_cphTransaction_ddlERelation").val();
                    if (rel == '-1') {
                        document.getElementById("errorddlERelation").innerText = 'please select the Sponser Relation';
                        $("#s2id_ctl00_cphTransaction_ddlERelation").focus();
                        $("#s2id_ctl00_cphTransaction_ddlERelation").css("border-color", "Red");
                        isvalid = false;
                    }
                    else {
                        $('#errorddlERelation').val('');
                        $("#s2id_ctl00_cphTransaction_ddlERelation").css("border-color", "");
                    }
                }
            }
            var j = 0;
            //Checking the Unique passport No
            var proposerpassport = document.getElementById("errorPassportNo").innerText;
            if (proposerpassport == "") {
                 for (var i = 0; i < pscount; i++) {
                    if (passportNo[0] == passportNo[i + 1]) {
                        isvalid = false;
                        document.getElementById('errortxtInsPassportNo' + i).innerText = "please Enter unique passport Number.";
                    } 
                }
            }
            return isvalid;
        }

    </script>
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            background-color: #f1f1f1;
        }

        .select2-container form-control {
            background-color: red;
        }

        .ns-h3 {
            border-radius: 7px;
        }
    </style>
    <style type="text/css">
        .ui-accordion .ui-accordion-content {
            height: auto !important;
        }
    </style>
</asp:Content>
