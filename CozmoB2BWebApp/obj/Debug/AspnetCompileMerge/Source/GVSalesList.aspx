﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="GVSalesList" Codebehind="GVSalesList.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style>
        .gvDtlRow {
            font-weight: normal !important;
            font-size: 12px;
        }

        .gvDtlAlternateRow {
            font-weight: normal !important;
            font-size: 12px;
        }
    </style>


    <div class="body_container">
        <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>



        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td height="30px"><a style="cursor: Hand; font-weight: bold; color: Black;" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a></td>
            </tr>
        </table>

        <div title="Param" id="divParam">
            <asp:Panel runat="server" ID="pnlParam" Visible="true">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 125px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 125px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 125px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr class="trpad paramcon">
                        <td align="right">
                            <asp:Label ID="lblFromDate" Text="From Date:" runat="server"></asp:Label>
                        </td>
                        <td>
                            <uc1:DateControl ID="dcFromDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label>
                        </td>
                        <td>
                            <uc1:DateControl ID="dcToDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblCountry" runat="server" Text="Country To Visit:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCountry" CssClass="inputEnabled form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="trpad paramcon">
                        <td align="right">
                            <asp:Label ID="lblNationality" runat="server" Text="Nationality:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlNationality" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblVisaType" runat="server" Text="Visa Type:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlVisaType" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblDispStatus" runat="server" Text="Dispatch Status:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDispStatus" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="trpad paramcon">

                        <td align="right">
                            <asp:Label ID="lblAgent" runat="server" Text="Agent:"></asp:Label>
                        </td>
                        <td class="paramcon">
                            <asp:DropDownList ID="ddlAgent" AutoPostBack="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblB2BAgent" runat="server" Text="B2BAgent:"></asp:Label>
                        </td>
                        <td class="paramcon">
                            <asp:DropDownList ID="ddlB2BAgent" AutoPostBack="true" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblB2B2BAgent" runat="server" Text="B2B2BAgent:"></asp:Label>
                        </td>
                        <td class="paramcon">
                            <asp:DropDownList ID="ddlB2B2BAgent" AutoPostBack="true" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="trpad">

                        <td align="right">
                            <asp:Label ID="lblLocation" runat="server" Text="Location:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLocation" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblConsultant" runat="server" Text="Consultant:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlConsultant" CssClass="inputEnabled form-control" runat="server"></asp:DropDownList>
                        </td>
                        <td></td>
                        <td style="padding-left: 10px;" align="right">
                            <asp:Button runat="server" ID="btnSearch" Text="Search" OnClientClick="return ValidateParam();" CssClass="btn but_b" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>

            </asp:Panel>


        </div>

        <div class="grdScrlTrans" style="margin-top: -2px; width: 100%">


            <%--<div id="divSearchbar" class="content" style="height:456px;width:100%;" >--%>

            <table id="tabSearch" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:GridView ID="gvVisaSales" Width="100%" runat="server" AllowPaging="true" DataKeyNames="FPAX_ID"
                            EmptyDataText="No Visa Sales List!" AutoGenerateColumns="false" PageSize="25" GridLines="none" CssClass=""
                            OnSelectedIndexChanged="gvVisaSales_SelectedIndexChanged" CellPadding="4" CellSpacing="0" PagerSettings-Mode="NumericFirstLast"
                            OnPageIndexChanging="gvVisaSales_PageIndexChanging">

                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                            <Columns>

                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label" ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                


                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtDocNo" Width="70px" HeaderText="File #" CssClass="inputEnabled width70" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblDocNo" runat="server" Text='<%# Eval("fvs_doc_no") %>' CssClass="grdof width80" ToolTip='<%# Eval("fvs_doc_no") %>' Width="80px"></asp:Label>
                                        <asp:HiddenField ID="IThdfVSId" runat="server" Value='<%# Bind("fvs_id") %>'></asp:HiddenField>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                  <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtVisitorName" HeaderText="Visitor Name" Width="100px" CssClass="inputEnabled  width100" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblVisitorName" runat="server" Text='<%# Eval("FPAX_NAME") %>' CssClass="grdof width120" ToolTip='<%# Eval("FPAX_NAME") %>' Width="140px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                 <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HttxtCountry" HeaderText="Country" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("COUNTRY_NAME") %>' CssClass="grdof width100" ToolTip='<%# Eval("COUNTRY_NAME") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtTracking" HeaderText="Application #" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <%--<asp:TextBox ID="ITtxtTrackingNo" runat="server" Width="150px" MaxLength="250" CssClass="InputEnabled" ></asp:TextBox>--%>
                                        <asp:Label ID="ITlblTrackingNo" runat="server" Text='<%# Eval("fpax_tracking_no") %>' CssClass="grdof width80" ToolTip='<%# Eval("fpax_tracking_no") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <ItemStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtDispatchStatus" CssClass="inputEnabled width80" HeaderText="Dis.Status" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblDispatchStatus" runat="server" Text='<%# Eval("fpax_dispatch_status") %>' CssClass="grdof width100" ToolTip='<%# Eval("fpax_dispatch_status") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtDispRefNo" Width="80px" CssClass="inputEnabled width80" HeaderText="Disp.Ref #" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblDispRefNo" runat="server" Text='<%# Eval("FPAX_DOC_NO") %>' CssClass="grdof width90" ToolTip='<%# Eval("FPAX_DOC_NO") %>' Width="90px"></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle VerticalAlign="top" />
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <uc1:DateControl ID="HTtxtVSDate" runat="server" DateOnly="true" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="HTbtnVStDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                                </td>
                                                <%--<td>
    <asp:ImageButton ID="HTbtnChequeReturnDateAdvance"  runat="server" ImageUrl="~/Images/wg_filterAdvance.GIF"  ImageAlign="AbsMiddle"  OnClientClick="return validateAdvanceFilter(this.id,'A');" Alt="Advance Filter" /></td>--%>
                                            </tr>
                                        </table>
                                        <label style="width: 140px" class="filterHeaderText">&nbsp;Date</label>
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblReceiptDate" Width="130px" runat="server" Text='<%# IDDateTimeFormat(Eval("fvs_doc_date")) %>' CssClass="grdof" ToolTip='<%# Eval("fvs_doc_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField>
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtAppliedThru" Width="100px" CssClass="inputEnabled  width100 " HeaderText="Applied Thru" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblAppliedThru" runat="server" Text='<%# Eval("CENTER_NAME") %>' CssClass="grdof width120" ToolTip='<%# Eval("CENTER_NAME") %>' Width="125px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtVSVisaType" HeaderText="Type Of Visa" CssClass="inputEnabled  width120" Width="120px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblVisaType" runat="server" Text='<%# Eval("fvs_visa_type_name") %>' CssClass="grdof width120" ToolTip='<%# Eval("fvs_visa_type_name") %>' Width="125px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtDocketNo" HeaderText="Docket #" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblDocketNo" runat="server" Text='<%# Eval("FVS_DOCKET_NO") %>' CssClass="grdof width100" ToolTip='<%# Eval("FVS_DOCKET_NO") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField>
                                    <HeaderStyle Width="100px" />
                                    <HeaderTemplate>
                                        <label style="width: 140px" class="filterHeaderText">Rcpt Print</label>

                                        <%--<cc2:Filter ID="HTtxtStatus" TextBoxWidth="60px" OnClick="Filter_Click" runat="server" FilterDataType=number />                 
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="ITlnkPrint" runat="Server" OnClick="ITlnkPrint_Click" Text="Rcpt Print" Width="70px"></asp:LinkButton>
                                        <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("receipt_status_name") %>' ToolTip='<%# Eval("receipt_status_name") %>' Width="60px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <%--<asp:TemplateField>
                                    <HeaderStyle Width="130px" />
                                    <HeaderTemplate>
                                        <label style="width: 140px" class="filterHeaderText">Add. Charges</label>

                                        <%--<cc2:Filter ID="HTtxtStatus" TextBoxWidth="60px" OnClick="Filter_Click" runat="server" FilterDataType=number />                
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="ITlnkAddChrgPrint" Enabled='<%#CT.TicketReceipt.Common.Utility.ToInteger(Eval("FPAX_ADD_CHARGES_ID"))>0?true:false%>' runat="Server" OnClick="ITlnkAddChrgPrint_Click" Text="Add. Charge Print" Width="90px"></asp:LinkButton>
                                        <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("receipt_status_name") %>' ToolTip='<%# Eval("receipt_status_name") %>' Width="60px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <%--     <asp:TemplateField>
    <HeaderStyle  width="100px"/>
    <HeaderTemplate>
    <label style="width:140px" class="filterHeaderText">App Print</label>    
    </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <asp:LinkButton ID="ITlnkAppPrint" runat="Server" onClick="ITlnkAppPrint_Click" Text="App Print" width="70px"  ></asp:LinkButton>
    </ItemTemplate>    
    </asp:TemplateField> --%>
                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HttxtPSPType" HeaderText="PSP Type" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblPSPType" runat="server" Text='<%# Eval("fpax_passport_type") %>' CssClass="grdof width100" ToolTip='<%# Eval("fpax_passport_type") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               

                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HttxtNationality" HeaderText="Nationality" CssClass="inputEnabled  width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("NATIONALITY_NAME") %>' CssClass="grdof width100" ToolTip='<%# Eval("NATIONALITY_NAME") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HttxtResidence" HeaderText="Residence" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblResidence" runat="server" Text='<%# Eval("FVS_RESIDENCE_NAME") %>' CssClass="grdof width120" ToolTip='<%# Eval("FVS_RESIDENCE_NAME") %>' Width="120px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                              

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtPaxType" HeaderText="Pax Type" Width="100px" CssClass="inputEnabled  width100" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblPaxType" runat="server" Text='<%# Eval("FPAX_PAX_TYPE") %>' CssClass="grdof width120" ToolTip='<%# Eval("FPAX_PAX_TYPE") %>' Width="125px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtProfession" HeaderText="Profession" Width="100px" CssClass="inputEnabled width100" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblProfession" runat="server" Text='<%# Eval("FPAX_PROFESSION") %>' CssClass="grdof width120" ToolTip='<%# Eval("FPAX_PROFESSION") %>' Width="125px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtPassportNo" HeaderText="Passport #" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblPassport" runat="server" Text='<%# Eval("fpax_passport_no") %>' CssClass="grdof width100" ToolTip='<%# Eval("fpax_passport_no") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtPhone" HeaderText="Pax Phone" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblPhone" runat="server" Text='<%# Eval("fpax_Phone") %>' CssClass="grdof width100" ToolTip='<%# Eval("fpax_Phone") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtWaiveOff" HeaderText="Waive Off" CssClass="inputEnabled width80" Width="80px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblWaiveOff" runat="server" Text='<%# Eval("fvs_waiveoff_status_name") %>' CssClass="grdof width100" ToolTip='<%# Eval("fvs_waiveoff_status") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtTotalVisaFee" HeaderText="Total Cost" CssClass="inputEnabled width80" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblTotalVisaFee" Style="margin-right: 5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_total_visa_fee")) %>' CssClass="grdof width80" ToolTip='<%# Eval("fvs_total_visa_fee") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtCurrency" HeaderText="Currency" Width="50px" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblCurrenecy" runat="server" Text='<%# Eval("fvs_currency_code") %>' CssClass="grdof" ToolTip='<%# Eval("fvs_currency_code") %>' Width="50px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtModeDesc" HeaderText="Coll.Mode" CssClass="inputEnabled  width60" Width="60px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblHTtxtModeDesc" Style="margin-right: 5px" runat="server" Text='<%# Eval("fvs_settlement_mode_desc") %>' CssClass="grdof width120" ToolTip='<%# Eval("fvs_settlement_mode_desc") %>' Width="120px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtCash" HeaderText="CASH" Width="60px" CssClass="inputEnabled  width60" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblCash" Style="margin-right: 5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_cash_local_amount")) %>' CssClass="grdof width100" ToolTip='<%# Eval("fvs_cash_local_amount") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtCredit" HeaderText="Credit" Width="60px" CssClass="inputEnabled  width60" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblCredit" Style="margin-right: 5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_credit_local_amount")) %>' CssClass="grdof width100" ToolTip='<%# Eval("fvs_credit_local_amount") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtCard" HeaderText="Card" Width="60px" CssClass="inputEnabled  width60" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblCard" Style="margin-right: 5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_card_local_amount")) %>' CssClass="grdof width100" ToolTip='<%# Eval("fvs_card_local_amount") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtEmployee" HeaderText="Employee" CssClass="inputEnabled  width60" Width="60px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblEmployee" Style="margin-right: 5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_employee_local_amount")) %>' CssClass="grdof width100" ToolTip='<%# Eval("fvs_employee_local_amount") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtOthers" HeaderText="Others" Width="60px" CssClass="inputEnabled  width60" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblOthers" Style="margin-right: 5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_others_local_amount")) %>' CssClass="grdof width100" ToolTip='<%# Eval("fvs_others_local_amount") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtModeRemraks" HeaderText="Mode Remarks" CssClass="inputEnabled  width90" Width="90px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblModeRemraks" Style="margin-right: 5px" runat="server" Text='<%# Eval("fvs_mode_remarks") %>' CssClass="grdof width120" ToolTip='<%# Eval("fvs_mode_remarks") %>' Width="120px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtUser" HeaderText="User" Width="120px" CssClass="inputEnabled  width120" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblUSer" runat="server" Text='<%# Eval("fVS_CREATED_NAME") %>' CssClass="grdof width120" ToolTip='<%# Eval("fVS_CREATED_NAME") %>' Width="120px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <%-- <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtTravelDate" HeaderText="Date&nbsp;of&nbsp;Travel" CssClass="inputEnabled" Width="80px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblTravelDate" runat="server" Text='<%# IDDateFormat(Eval("RECEIPT_TRAVEL_DATE")) %>' CssClass="label grdof"  ToolTip='<%# IDDateFormat(Eval("RECEIPT_TRAVEL_DATE"))  %>' Width="90px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> --%>






                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtLocation" HeaderText="Location" CssClass="inputEnabled  width120" Width="120px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("fvs_location_name") %>' CssClass="grdof width120" ToolTip='<%# Eval("fvs_location_name") %>' Width="120px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle />
                                    <HeaderTemplate>

                                        <cc2:Filter ID="HTtxtAgent" HeaderText="Agent" CssClass="inputEnabled  width120" Width="120px" OnClick="Filter_Click" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("fvs_agent_name") %>' CssClass="grdof width120" ToolTip='<%# Eval("fvs_agent_name") %>' Width="120px"></asp:Label>
                                        <asp:HiddenField ID="IThdfAgentId" runat="server" Value='<%# Bind("fvs_agent_id") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <%--<asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate>
    
    <cc2:Filter ID="HTtxtRemarks" HeaderText="Remarks" CssClass="inputEnabled" Width="150px" OnClick="Filter_Click" runat="server" FilterDataType="number" />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("fvs_remarks") %>' CssClass="label grdof"  ToolTip='<%# Eval("fvs_remarks") %>' Width="150px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
                                --%>





                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="center" />
                                    <HeaderTemplate>
                                        <cc2:Filter ID="HTtxtStatus" Width="100px" HeaderText="Status" CssClass="inputEnabled width100" OnClick="Filter_Click" runat="server" FilterDataType="number" />
                                    </HeaderTemplate>
                                    <ItemStyle />
                                    <ItemTemplate>
                                        <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("FVS_STATUS_NAME") %>' ToolTip='<%# Eval("FVS_STATUS_NAME") %>' Width="60px"></asp:Label>--%>
                                        <asp:LinkButton ID="ITlnkCancel" Text='<%# Eval("FVS_STATUS_NAME") %>' CssClass=" fcol_black" Visible='<%# (bool)Eval("fpax_dispatch_status").ToString().ToLower().Equals("dispatched") ? false:true  %>' Enabled='<%# (bool)Eval("fvs_status").Equals("A")?true:false %>' OnClientClick="return confirm('Are you sure to cancel?')" runat="server" Width="150px" CommandName="Select" CausesValidation="True"></asp:LinkButton>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr>
                                <td width="80px">
                                    <asp:Button OnClick="btnExport_Click" runat="server" Visible="true" ID="btnExport" Text="Export To Excel" CssClass="button" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>



        </div>


        <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
        <asp:Label Style="color: #dd1f10" ID="lblError" runat="server"></asp:Label>
        <div>
            <asp:DataGrid ID="dgVisaSalesList" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundColumn HeaderText="Date" DataField="fvs_doc_date" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="File" DataField="fvs_doc_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Application" DataField="fpax_tracking_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Dispatch Status" DataField="fpax_dispatch_status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Dispatch Ref" DataField="FPAX_DOC_NO" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Type of Visa" DataField="fvs_visa_type_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Docket" DataField="FVS_DOCKET_NO" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="PSPType" DataField="fpax_passport_type" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Country" DataField="COUNTRY_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Nationality" DataField="NATIONALITY_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Residence" DataField="FVS_RESIDENCE_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Visitor Name" DataField="FPAX_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Pax Type" DataField="FPAX_PAX_TYPE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Profession" DataField="FPAX_PROFESSION" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Passport" DataField="fpax_passport_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Pax Phone" DataField="fpax_Phone" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Total cost" DataField="fvs_total_visa_fee" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Currency" DataField="fvs_currency_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Coll.Mode" DataField="fvs_settlement_mode_desc" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Cash" DataField="fvs_cash_local_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Credit" DataField="fvs_credit_local_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Card" DataField="fvs_card_local_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Employee" DataField="fvs_employee_local_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Others" DataField="fvs_others_local_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Mode Remarks" DataField="fvs_mode_remarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="User" DataField="fVS_CREATED_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Location " DataField="fvs_location_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Agent" DataField="fvs_agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Status" DataField="FVS_STATUS_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                    
                </Columns>
            </asp:DataGrid>

        </div>
        <script type="text/javascript">

            function ShowHide(div) {
                if (getElement('hdfParam').value == '1') {
                    //alert(document.getElementById(div));
                    //alert(document.getElementById(div).innerHtml);
                    //getElement(div).value;
                    document.getElementById('ancParam').innerHTML = 'Show Param'
                    document.getElementById(div).style.display = 'none';
                    getElement('hdfParam').value = '0';
                }
                else {
                    document.getElementById('ancParam').innerHTML = 'Hide Param'
                    document.getElementById('ancParam').value = 'Hide Param'
                    document.getElementById(div).style.display = 'block';
                    getElement('hdfParam').value = '1';
                }
            }
            function ValidateParam() {
                clearMessage();
                var fromDate = GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
                var fromTime = getElement('dcFromDate_Time');
                var toDate = GetDateTimeObject('ctl00_cphTransaction_dcToDate');
                var toTime = getElement('dcToDate_Time');
                if (fromDate == null) addMessage('Please select From Date !', '');
                //alert(fromTime);
                if (fromTime.value == '') addMessage('Please select From Time!', '');
                if (toDate == null) addMessage('Please select To Date !', '');
                if (toTime.value == '') addMessage('Please select To Time!', '');
                if ((fromDate != null && toDate != null) && fromDate > toDate) addMessage('From Date should not be later than To Date!', '');
                if (getMessage() != '') {
                    alert(getMessage()); clearMessage(); return false;
                }
            }

            function validateAdvanceFilter(id, mode) {

                if (mode == 'A') {

                    getElement('pnlAdvanceFilterDocDate').style.display = "block";
                    getElement('divAdvanceFilterDocDate').style.display = "block";
                    var positions = getRelativePositions(document.getElementById(id));
                    getElement('divAdvanceFilterDocDate').style.left = positions[0] + 'px';
                    getElement('divAdvanceFilterDocDate').style.top = (positions[1] + 20) + 'px';

                }
                else {
                    getElement('pnlAdvanceFilterDocDate').style.display = "none";
                    getElement('divAdvanceFilterDocDate').style.display = "none";
                    getElement('dcDocDateTo_Date').value = '';
                    getElement('dcDocDateFrom_Date').value = '';

                }
                return false;

            }
            function getRelativePositions(obj) {
                var curLeft = 0;
                var curTop = 0;
                if (obj.offsetParent) {
                    do {
                        curLeft += obj.offsetLeft;
                        curTop += obj.offsetTop;
                    } while (obj = obj.offsetParent);

                }
                return [curLeft, curTop];
            }
            function getElement(id) {
                return document.getElementById('ctl00_cphTransaction_' + id);
            }

            function ValidateDate(id) {
                var fromDate = GetDateObject('ctl00_cphTransaction_dcDocDateFrom');
                var toDate = GetDateObject('ctl00_cphTransaction_dcDocDateTo');
                if (fromDate != null && toDate != null && (fromDate > toDate)) addMessage('To-Date should be later than or equal to From-Date !', '');
                if (getMessage() != '') {
                    alert(getMessage()); clearMessage(); return false;
                }
            }


        </script>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

