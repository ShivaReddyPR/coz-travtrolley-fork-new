﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="BaggageInsuranceMaster.aspx.cs" Inherits="CozmoB2BWebApp.BaggageInsuranceMasterGUI" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div>

        
        <div class="col-md-12"> 

            <div class="col-md-12">
                <%--<h3>BaggageInsurance Master</h3>--%>
            </div> <br />
            <div class="col-md-6" style="text-align: right" >
                <asp:Label ID="lblPlanCode" runat="server" CssClass="label" Width="120px" Text="Plan Code:"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtPlanCode" runat="server" MaxLength="100" ></asp:TextBox> <br />
            </div>
            <div class="col-md-6" style="text-align: right">
                <asp:Label ID="lblPlanTitle" runat="server" CssClass="label" Width="120px" Text="Plan Title:"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtPlanTitle" runat="server" Width="100px" MaxLength="100" ></asp:TextBox><br />
            </div>

            <div class="col-md-6" style="text-align: right">
                <asp:Label ID="lblPlanDescription" runat="server" CssClass="label" Width="120px" Text="Plan Description:"></asp:Label>
            </div>
            <div class="col-md-3">
                
                <asp:TextBox ID="txtPlanDescription" runat="server" TextMode="MultiLine" Columns="50" Rows="5" MaxLength="200" ></asp:TextBox>  <br />
            </div>
             <div class="col-md-6" style="text-align: right">
                <asp:Label ID="lblPlanMarketingPoints" runat="server" CssClass="label" Width="120px" Text="Plan Marketing Points:"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtPlanMarketingPoints" runat="server" TextMode="MultiLine" Columns="50" Rows="5"  ></asp:TextBox> <br />
            </div>
            <div class="col-md-6" style="text-align: right">
                <asp:Label ID="lblPlanAdultPrice" runat="server" CssClass="label" Width="120px" Text="Plan Adult Price:"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtPlanAdultPrice" runat="server" Width="100px"  TextMode="Number" MaxLength="8"  ></asp:TextBox> <br />
            </div>
    

            </div>
        <div class="col-md-10" style="text-align:center">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="but but_b" OnClick="btnSave_Click"
                OnClientClick="return Save();" />
            <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b" OnClick="btnSearch_Click"></asp:Button>
            <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b" OnClick="btnClear_Click"></asp:Button>
        </div>
        <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
        
   
</div>

    <script type="text/javascript">
        function Save() {
        if (getElement('txtPlanCode').value == '') addMessage('Insurance Code cannot be blank!', '');
        if (getElement('txtPlanTitle').value == '') addMessage('Insurance Title cannot be blank!', '');
        if (getElement('txtPlanDescription').value == '') addMessage('Insurance Description cannot be blank!', '');
        if (getElement('txtPlanMarketingPoints').value == '') addMessage('Insurance MarketingPoints cannot be blank!', '');
        if (getElement('txtPlanAdultPrice').value=='') addMessage('Insurance AdultPrice cannot be blank!', '')
        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
    </script>
   
</asp:Content>
 <asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 
      
     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="PLAN_ID"
         emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="10" GridLines="None"
         CssClass="grdTable"  OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
         OnPageIndexChanging="gvSearch_PageIndexChanging" >

         <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
         </HeaderStyle>
         <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
         <AlternatingRowStyle CssClass="gvDtlAlternateRow" /> 
         <Columns>
             <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
             <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtCode" Width="70px" HeaderText="Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />  
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("PLAN_CODE") %>' CssClass="label grdof" ToolTip='<%# Eval("PLAN_CODE") %>' Width="70px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>  
             <asp:TemplateField>
                 <HeaderStyle HorizontalAlign="Left" />
                 <HeaderTemplate>
                     <cc1:Filter ID="HTtxtTitle" Width="70px" HeaderText="Title" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" ></cc1:Filter>
                 </HeaderTemplate>
                 <ItemTemplate>
                     <asp:Label ID="ITlblTitle" runat="server" Text='<%# Eval("PLAN_TITLE") %>' CssClass="label grdof" ToolTip='<%# Eval("PLAN_TITLE") %>' Width="100px"></asp:Label>               
                         </ItemTemplate>
             </asp:TemplateField> 
         </Columns>
     </asp:GridView>
</asp:Content>



