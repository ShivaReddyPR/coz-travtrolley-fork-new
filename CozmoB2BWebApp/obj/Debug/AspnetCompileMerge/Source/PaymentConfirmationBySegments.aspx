﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="PaymentConfirmationBySegments" Title="Payment Confirmation" Codebehind="PaymentConfirmationBySegments.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script src="Scripts/jsBE/SearchResult.js" type="text/javascript"></script>
    <script type="text/javascript">
        function SeatAssignAlert(msg) {

            document.getElementById('divSeatsalertText').innerHTML = msg.replace(/@/g, '</br>');
            $('#Seatsalert').modal('show');
        }
        function Cancel(id) {
           $('#'+id).modal('hide');
        }
        function validate() {
            if (document.getElementById('<%=chkRules.ClientID %>').checked == true) {
                $('.modal').modal('hide');
                document.getElementById('rules').style.display = 'none';
                <%if (!Settings.LoginInfo.AgentBlock) //checking Agent able to book or not Added By brahmam
    {%>
                document.getElementById('<%=hdfAction.ClientID %>').value = "Ticket";
                document.getElementById('PreLoader').style.display = "block";
                document.getElementById('MainDiv').style.display = "none";
                document.getElementById('divPrg').style.display = "none";
                return true;
                <%}
    else
    {%>
                alert('Please contact Administrator'); //We are Showing Alert Message
                return false;
                <%}%>
            }
            else {
                document.getElementById('rules').style.display = 'block';
                document.getElementById('PreLoader').style.display = "none";   
                return false;
            }
        }
        function setAction() {
            if (validate()) {
                document.getElementById('<%=hdfAction.ClientID %>').value = "Hold";
                if (document.getElementById('<%=ddlPaymentType.ClientID %>').selectedIndex == 1) {//Show preloader for CreditCard also
                    document.getElementById('PreLoader').style.display = "block";
                    document.getElementById('MainDiv').style.display = "none";
                    document.getElementById('divPrg').style.display = "none";
                }
                return true;
            }
            else {
                return false;
            }
        }
        function PriceContinue() {
            //document.getElementById('FareDiff').style.display = 'none';
            $('#FareDiff').modal('hide')
            document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'block';

        }

        function Deselect() { //Added by Suresh
            var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
                var inputs = radioButtons.getElementsByTagName("input");
            var bookingAmount = eval(document.getElementById("ctl00_cphTransaction_hdnBookingAmount").defaultValue.replace(',',''));
                var currentCode = document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML.split(' ')[0];
            for (var x = 0; x < inputs.length; x++) {
                if (inputs[x].checked) {
                    inputs[x].checked = false;
                    document.getElementById("ctl00_cphTransaction_lblTxnAmount").innerHTML
                        = currentCode + " " + parseFloat(bookingAmount).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
                }
            }
        } //===============================================

        
        function CardChargeEnable() {

            //Deselect(); //Added by Suresh
             <%--var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
            var inputs = radioButtons.getElementsByTagName("input");
             var selected;
            for (var x = 0; x < inputs.length; x++) {
                if (inputs[x].checked) {

                }
            }--%>
 
            FillCharges();//==================================
            if (document.getElementById('ctl00_cphTransaction_ddlPaymentType').value == "Card") {
                document.getElementById('divCardCharge').style.display = "block";
                document.getElementById('lblCardCharges').innerHTML = parseFloat(eval('<%=charges %>')).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});

                //Added by somasekhar on 02/07/2018                
                document.getElementById('divAgentPG').style.display = "block";
            }
            else {
                document.getElementById('divCardCharge').style.display = "none";
                //document.getElementById('lblCardCharges').innerHTML = "";

                //Added by somasekhar on 02/07/2018                
                document.getElementById('divAgentPG').style.display = "none";
                var bookingAmount = eval(document.getElementById("ctl00_cphTransaction_hdnBookingAmount").defaultValue.replace(',',''));
                var currentCode = document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML.split(' ')[0];
                
                    document.getElementById("ctl00_cphTransaction_lblTxnAmount").innerHTML
                        = currentCode + " " + parseFloat(bookingAmount).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
            }
        }
        function BagPriceContinue() {
           // document.getElementById('BagFareDiff').style.display = 'none';
            $('#BagFareDiff').modal('hide');
        }

        function PriceCancel() {
           // document.getElementById('BagFareDiff').style.display = 'none';
            $('#BagFareDiff').modal('hide');
            window.location.href = 'HotelSearch.aspx?source=Flight';
        }
        function FillCharges() {  // Added by Suresh
 
            var bookingAmount = document.getElementById("<%=hdnBookingAmount.ClientID%>").defaultValue;
            var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
            var inputs = radioButtons.getElementsByTagName("input");

            var selected;
            for (var x = 0; x < inputs.length; x++) {
                var totalamount = bookingAmount.replace(/\,/g, '');
                if (inputs[x].checked) {
                    var charge = inputs[x].value;
                    //document.getElementById('divCardCharge').style.display = "block";
                    document.getElementById('lblCardCharges').innerHTML = parseFloat(charge).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
                    var currentCode = document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML.split(' ')[0];
                    if (charge != 0) {
                        var amount = parseFloat(totalamount) + (parseFloat(totalamount) * (parseFloat(charge) / 100));
                        document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML
                            = currentCode + " " + amount.toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});

                    }
                    else {
                        document.getElementById("ctl00_cphTransaction_lblTxnAmount").innerHTML
                            = currentCode + " " + parseFloat(totalamount).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
                    }


                }
            }
            if (selected) {
                alert(selected.value);
            }
        }//==============================
    </script>

<asp:HiddenField ID="hdnAgencyName" runat="server" />
<asp:HiddenField ID="hdnAgencyPhone" runat="server" />

    

    <div class="modal fade pymt-modal" data-backdrop="static" id="Seatsalert" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width:500px;">
              <div class="modal-header">
                  <h4 class="modal-title" id="SeatsalertLabel">Seat assignment alert</h4>
              </div>
              <div class="modal-body">
                <div id="divSeatsalertText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;"></div>                  
                <div class="padtop_4">
                    <input type="button" id="SeatsalertContinue" class="btn but_b" onclick="Cancel('Seatsalert')" />
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close mt-0" style="color:#fff;opacity:.8;" data-dismiss="modal" aria-label="Close"" id="btnok" onclick="PriceContinue()"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Booking Alert</h4>
              </div>
              <div class="modal-body">
                  
 <div id="divFareDiffText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;">Your booking Airline is Low Cost Carrier, for any cancellations, respective Airline cancellation charges will be applied</div>

<%--<div style=" padding-bottom:10px;"> <center> <input type="button value="OK" class="button-normal"/></center> </div>--%>
                     </div>
            </div>
          </div>
        </div>



    <asp:HiddenField ID="hdfStop" runat="server" Value="0" />
    <asp:HiddenField ID="hdfAction" runat="server" />
    <asp:HiddenField ID="hdnBookingAmount" runat="server" />


    <div id="MainDiv">
    <div class="ns-h3">Payment Confirmation</div>
        <div>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="PaymentView" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:DataList ID="dlFlight" runat="server" OnItemDataBound="dlFlight_ItemDataBound"
                                    Width="100%" Caption="Onward Flight">
                                    <ItemTemplate>
                                        <div class="bg_white marbot_20 pad_10">
                                            <table class="flight_details_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="imgOnCarrier1" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnOrigin" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDestination" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDuration" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblOnAircraft" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnRefundable" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblOnCarrier" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepAirport" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnDepTerminal" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrAirport" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnArrTerminal" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnWayType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblOnFlightCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepartureDate" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrivalDate" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="OnStopDep1" runat="server" visible="false">
                                                    <td>&nbsp;<asp:Image ID="imgOnCarrier2" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnOrigin1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDestination1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDuration1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblOnAircraft1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnRefundable1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopArr1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnCarrier1" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnDepTerminal1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnArrTerminal1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnWaytype1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopTime1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnFlightCode1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepartureDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrivalDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="OnStopDep2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Image ID="imgOnCarrier3" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnOrigin2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDestination2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDuration2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnAircraft2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnRefundable2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopArr2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnCarrier2" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnDepTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnArrTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnWaytype2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopTime2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnFlightCode2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepartureDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrivalDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="14px" colspan="5">
                                                        <hr class="b_bot_1 " />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="imgRetCarrier1" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetOrigin1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDestination1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDuration1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblRetAircraft1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetRefundable1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblRetCarrier1" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetDepTerminal1" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetArrTerminal1" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetWayType1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblRetFlightCode1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepartureDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrivalDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="RetStopArr1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Image ID="imgRetCarrier2" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetOrigin2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDestination2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDuration2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblRetAircraft2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetRefundable2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopDep1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetCarrier2" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetDepTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetArrTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetWaytype2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopTime1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetFlightCode2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepartureDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrivalDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="RetStopArr2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Image ID="imgRetCarrier3" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetOrigin3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDestination3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDuration3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblRetAircraft3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetRefundable3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopDep2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetCarrier3" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepAirport3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetDepTerminal3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrAirport3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetArrTerminal3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetWaytype3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopTime2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetFlightCode3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepartureDate3" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrivalDate3" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>    

                                <asp:DataList ID="dlFlightReturn" runat="server" OnItemDataBound="dlFlightReturn_ItemDataBound"
                                    Width="100%" Caption="Return Flight">
                                    <ItemTemplate>
                                        <div class="bg_white marbot_20 pad_10">
                                            <table class="flight_details_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="imgOnCarrier1" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnOrigin" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDestination" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDuration" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblOnAircraft" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnRefundable" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblOnCarrier" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepAirport" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnDepTerminal" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrAirport" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnArrTerminal" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnWayType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblOnFlightCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepartureDate" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrivalDate" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="OnStopDep1" runat="server" visible="false">
                                                    <td>&nbsp;<asp:Image ID="imgOnCarrier2" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnOrigin1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDestination1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDuration1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblOnAircraft1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnRefundable1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopArr1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnCarrier1" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnDepTerminal1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnArrTerminal1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnWaytype1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopTime1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnFlightCode1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepartureDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrivalDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="OnStopDep2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Image ID="imgOnCarrier3" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnOrigin2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDestination2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDuration2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnAircraft2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnRefundable2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopArr2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnCarrier2" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnDepTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblOnArrTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnWaytype2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="OnStopTime2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblOnFlightCode2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnDepartureDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOnArrivalDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="14px" colspan="5">
                                                        <hr class="b_bot_1 " />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="imgRetCarrier1" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetOrigin1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDestination1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDuration1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblRetAircraft1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetRefundable1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblRetCarrier1" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetDepTerminal1" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetArrTerminal1" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetWayType1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblRetFlightCode1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepartureDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrivalDate1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="RetStopArr1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Image ID="imgRetCarrier2" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetOrigin2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDestination2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDuration2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblRetAircraft2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetRefundable2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopDep1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetCarrier2" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetDepTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetArrTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetWaytype2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopTime1" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetFlightCode2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepartureDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrivalDate2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="RetStopArr2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Image ID="imgRetCarrier3" runat="server" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetOrigin3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDestination3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDuration3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td rowspan="3">
                                                        <asp:Label ID="lblRetAircraft3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetRefundable3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopDep2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetCarrier3" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepAirport3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetDepTerminal3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrAirport3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        <asp:Label ID="lblRetArrTerminal3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetWaytype3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="RetStopTime2" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblRetFlightCode3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetDepartureDate3" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRetArrivalDate3" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataList ID="dlPaxPrice" runat="server" Width="100%" OnItemDataBound="dlPaxPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="bg_white">
                                            <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="55%" class="gray-smlheading">
                                                        <strong>Onward Fare</strong>
                                                    </td>
                                                    <td class="gray-smlheading" width="35%" align="right"><strong>Price</strong></td>
                                                    <td width="10%" class="gray-smlheading" align="right"></td>
                                                </tr>
                                                <tr>
                                                    <td>Total AirFare
                                                                    <%--<asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>--%>
                                                    </td>

                                                    <td align="right">
                                                        <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                        <%--Text='<%#Eval("BaseFare","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>K3 Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblK3Tax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblDiscount" runat="server" Text="Discount" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblDiscountAmount" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Total
                                                                    <%--<asp:Label ID="lblTotalAdults" runat="server" Text=""></asp:Label>--%>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblBaggage" runat="server" Text="Baggage " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblBaggagePrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>

                                                <%if (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) %>
                                                <%{ %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMeal" runat="server" Text="Meal " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblMealPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <%} %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblSeat" runat="server" Text="Seats" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblSeatPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                            { %>
                                                                    Total GST
                                                                    <%}
                                                                        else if (!CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                        {%>
                                                                                Total GST
                                                                        <%}
                                                                            else
                                                                            { %>
                                                                    VAT 
                                                                    <%} %>
                                                    </td>
                                                    <td align="right">

                                                        <asp:Label ID="lblVATAmount" runat="server"></asp:Label>


                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong class="spnred">Grand Total Offer Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblMarkupTotal" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr class="d-none">
                                                    <td>
                                                        <strong class="spnred">Grand Total Pub Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblTotalPubFare" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>

                                <asp:DataList ID="dlPaxPriceReturn" runat="server" Width="100%" OnItemDataBound="dlPaxPriceReturn_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="bg_white">
                                            <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="55%" class="gray-smlheading">
                                                        <strong>Return Fare</strong>
                                                    </td>
                                                    <td class="gray-smlheading" width="35%" align="right"><strong>Price</strong></td>
                                                    <td width="10%" class="gray-smlheading" align="right"></td>
                                                </tr>
                                                <tr>
                                                    <td>Total AirFare
                                                                    <%--<asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>--%>
                                                    </td>

                                                    <td align="right">
                                                        <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                        <%--Text='<%#Eval("BaseFare","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>K3 Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblK3Tax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblDiscount" runat="server" Text="Discount" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblDiscountAmount" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Total
                                                                    <%--<asp:Label ID="lblTotalAdults" runat="server" Text=""></asp:Label>--%>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblBaggage" runat="server" Text="Baggage " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblBaggagePrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>

                                                <%if (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp) %>
                                                <%{ %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMeal" runat="server" Text="Meal " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblMealPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <%} %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblSeat" runat="server" Text="Seats" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblSeatPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                            { %>
                                                                    Total GST
                                                                    <%}
                                                                        else if (!CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                        {%>
                                                                                Total GST
                                                                        <%}
                                                                            else
                                                                            { %>
                                                                    VAT 
                                                                    <%} %>
                                                    </td>
                                                    <td align="right">

                                                        <asp:Label ID="lblVATAmount" runat="server"></asp:Label>


                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong class="spnred">Grand Total Offer Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblMarkupTotal" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr  class="d-none">
                                                    <td>
                                                        <strong class="spnred">Grand Total Pub Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblTotalPubFare" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="col-md-6 pad_left0">
                                                <div class="ns-h3">
                                                    <strong>Passenger Details</strong>
                                                    <%if(onwardResult.IsLCC || (returnResult!= null ?returnResult.IsLCC:false))
                                                              %>
                                                    <%{ %>
                                                    <a style="color: #fff; padding-right: 10px; font-weight: normal;display:none;" class="pull-right" href="javascript:history.go(-1);">Edit Pax</a>
                                                    <%} %>
                                                </div>
                                                <div class="bg_white bor_gray pad_10 payment_confirm_pax">
                                                    <asp:DataList ID="dlPassengers" runat="server" Width="100%">
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="20">
                                                                        <strong>
                                                                            <asp:Label ID="lblAdultName" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label></strong>
                                                                        (<%#Eval("Type") %> )<br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Passport #:
                                                                                    <asp:Label ID="lblPassportNumber" runat="server" Text='<%#Eval("PassportNo") %>'></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Phone:
                                                                                    <asp:Label ID="lblPhone" runat="server" Text='<%#Eval("CellPhone") %>'></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Address :
                                                                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("AddressLine1") %>'></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Email:
                                                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Seat Preference:
                                                                                    <asp:Label ID="lblSelectedSeats" runat="server" Text='<%#Eval("SeatInfo") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <hr class="b_bot_1 my-1"></hr>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pad_right0">
                                                <div class="ns-h3"><strong>Payment Details</strong> </div>
                                                <div class="modal fade in farerule-modal-style" data-backdrop="static" id="FareRuleBlock" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close mt-0" style="color: #fff; opacity: .8;" onclick="FareRuleHide()"><span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title" id="FareRuleHead"><span id="FareRuleHeadTitle"></span></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="bg_white" id="FareRuleBody" style="padding: 10px; width: 100%; font-size: 13px; height: 92%; overflow: auto;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bg_white bor_gray pad_10">
                                                    <div>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                            <tr>
                                                                <td>
                                                                    <div style="padding: 0px 10px 0px 10px">

                                                                        <ul style="margin-left: 10px;">
                                                                            <li>Ticket changes may incur penalties and/or increased fares.</li>
                                                                            <li>Tickets are nontransferable and name changes are not allowed.</li>
                                                                            <!--<li>Read an overview of all the<a href="#"> rules &amp; restrictions</a> applicable to 
                                                                        this fare.</li>-->
                                                                            <%if (onwardResult != null && Session["sessionId"] != null)
                                                                            { %>
                                                                            <li>Read the complete<a href="Javascript:FareRule(<%=onwardResult.ResultId %>,'<%=Session["sessionId"].ToString() %>')"> penalty rules for changes and cancellations</a>
                                                                                applicable to this fare.</li>
                                                                            <%} %>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10px">
                                                                    <hr class="b_bot_1 my-1">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table style="margin-left: 10px;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="4%">
                                                                                <span style="font-size: 11px">
                                                                                    <asp:CheckBox ID="chkRules" runat="server" />
                                                                                </span>
                                                                            </td>
                                                                            <td width="96%">
                                                                                <strong>I have read and accept the rules &amp; restrictions.</strong>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="width: 100%">
                                                                                <div id="rules" style="color: Red; display: none; width: 100%">
                                                                                    Please check rules and restrictions to continue.
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lblHold" runat="server" style="padding-left: 10px;" visible="false">
                                                                                    Want to hold your ticket till you pay?</label>
                                                                            </td>
                                                                            <td align="center">
                                                                                <label>
                                                                                    <asp:Button CssClass="btn but but_b" ID="btnHold" runat="server" Text="Hold" OnClientClick="return setAction();" OnClick="btnHold_Click" Visible="false" />
                                                                                    <%--TBO Hold - Modal Button (Hidden)--%>
                                                                                    <a href="javascript:void(0);" type="button" class="btn but_b" style="display: none" data-target="#TBOConfirm" id="TBOHoldConfirmBtn">Hold</a>

                                                                                </label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10px">
                                                                    <hr class="b_bot_1  my-1">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label style="padding-left: 10px;">
                                                                        Select Payment Mode :
                                                                <select style="width: 140px;" class="" id="ddlPaymentType" runat="server" onchange="CardChargeEnable()">
                                                                    <option value="Credit">On Account</option>
                                                                    <option value="Card">Credit Card</option>
                                                                </select>
                                                                    </label>
                                                                    <%-- Added by Somasekhar on 02/07/2018 -- Agent PG Options--%>
                                                                    <div id="divAgentPG" style="display: none; margin-bottom: 10px">
                                                                        Payment Gateways
                                                                <asp:RadioButtonList ID="rblAgentPG" runat="server" onchange=" return FillCharges()">
                                                                    <%--  OnSelectedIndexChanged="rblAgentPG_SelectedIndexChanged" AutoPostBack="true" onchange=" return CardChargeEnable()">--%>
                                                                </asp:RadioButtonList>
                                                                        <div id="pgValidation" style="color: red; display: none; width: 100%;">
                                                                            Please select one of the payment gateways.
                                                                        </div>
                                                                    </div>
                                                                    <%--   ==========================--%>

                                                                    <div style="display: none; color: Red; font-weight: bold;" id="divCardCharge">
                                                                        Credit Card Charges
                                                                <label id="lblCardCharges" style="color: Red; font-weight: bold;">
                                                                </label>
                                                                        % applicable on Total Amount
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10px">
                                                                    <hr class="b_bot_1  my-1">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div style="padding: 5px 0px 10px 0px">
                                                                        <table width="100%">
                                                                            <tbody>
                                                                                <tr class="nA-h4">
                                                                                    <td align="right">Available Balance :
                                                                                    </td>
                                                                                    <td align="right" style="font-weight: bold; padding-right: 10px">
                                                                                        <asp:Label ID="lblAgentBalance" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>


                                                                                <tr class="nA-h4">
                                                                                    <td align="right">Amount to be Booked :
                                                                                    </td>
                                                                                    <td align="right" style="font-weight: bold; padding-right: 10px">
                                                                                        <asp:Label ID="lblTxnAmount" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td align="left" colspan="2">
                                                                                        <asp:Label Style="color: Red" ID="lblBalanceError" Font-Bold="true" runat="server"
                                                                                            Text=""></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>

                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <label style="padding-right: 10px">
                                                                        <%--<asp:ImageButton ID="imgBtnPayment" runat="server" ImageUrl="images/confirm-button.jpg"
                                                                    OnClientClick="return validate();" OnClick="imgBtnPayment_Click" />--%>
                                                                        <%if (agency != null && agency.AgentTicketingAllowed)
                                                                        {  %>
                                                                        <%--TBO Confirm - Modal Button (Hidden)--%>
                                                                        <a href="javascript:void(0);" type="button" class="btn but_b" style="display: none" data-target="#TBOConfirm" id="TBOConfirmBtn">Book Now 
                                                                        </a>

                                                                        <asp:Button ID="imgBtnPayment" runat="server" CssClass=" btn but_b" Text="Confirm" OnClientClick="return validate();" OnClick="imgBtnPayment_Click" />

                                                                        <%  } %>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                    </table>
                    <%//if (!btnHold.Visible)
                        try
                        {
                            if (onwardFlightItinerary.IsLCC || (returnFlightItinerary != null?returnFlightItinerary.IsLCC:false))
                            {

                    %>
                    <script type="text/javascript">
                        //document.getElementById('FareDiff').style.display = 'block';
                        $('#FareDiff').modal('show')
                        document.getElementById('divFareDiffText').innerHTML = 'Your booking Airline is Low Cost Carrier, for any cancellations, respective Airline cancellation charges will be applied';
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';

                    </script>
                    <%}
                        else if (onwardResult.ResultBookingSource == BookingSource.TBOAir)
                        { %>
                    <script type="text/javascript">
                        //document.getElementById('FareDiff').style.display = 'block';
                        $('#FareDiff').modal('show')
                        document.getElementById('divFareDiffText').innerHTML = 'For cancellation/void of this ticket please contact on the same day of ticket issuance before GST 21:30, any requests after the said time/date will not be entertained.';
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';

                    </script>
                    <%}
                        // Added by lokesh on 7-June-2018 
                        // Need to display the below alert for PK Source 
                        else if (onwardResult.ResultBookingSource == BookingSource.PKFares)
                        { %>
                    <script type="text/javascript">
                        //document.getElementById('FareDiff').style.display = 'block';
                        $('#FareDiff').modal('show')
                        document.getElementById('divFareDiffText').innerHTML = 'Unless ticket number is shown on the ticket copy.Ticket copy should not be handed over to customer.';
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';

                    </script>
                    <%} %>

                    <%if (onwardResult.ResultBookingSource == BookingSource.AirArabia && currentFare > originalFare)
                        {
                    %>

                    <div class="modal fade pymt-modal" data-backdrop="static" id="BagFareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="FareRuleHead">Baggage Price for the Itinerary has been revised!</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="" id="FareRuleBody" style="text-align: center;">
                                        <div>
                                            <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">Price has been changed for this Itinerary</span>
                                        </div>
                                        <div>
                                            <div class="col-md-6">

                                                <label>
                                                </label>
                                            </div>
                                            <div class="col-md-6">

                                                <label style="color: Red">
                                                </label>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="padtop_4">
                                            <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="BagPriceContinue()" />
                                        </div>
                                        <div class="padtop_4">
                                            <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                                                style="width: 150px;" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                    <script type="text/javascript">
                        //document.getElementById('BagFareDiff').style.display = 'block';
                        $('#BagFareDiff').modal('show');
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';
                    </script>
                    <%}
                    }
                    catch { } %>
                </asp:View>
                <asp:View ID="ErrorView" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/HotelSearch.aspx?source=Flight">Go to Search Page</asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>

        </div>
    </div>

          <!-- For TBO Validation -->                                       
            <div class="modal fade" tabindex="-1" role="dialog" id="TBOConfirm">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        VOID RESTRICTIONS APPLY. THIS TICKET CANNOT BE VOID
                    </div>
                    <div class="modal-footer text-center">
                        <a class="btn btn-primary" href="" data-dismiss="modal">Cancel </a>
                    </div>
                </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
	    <!-- END  TBO Validation -->
   
   
   
   
   
   
   
    <!-- Modified by Lokesh & Firoz on 18-June-2018-->
    <!-- New Design for E-ticket in Email-->

    <div id='EmailDiv' runat='server' style='width: 600px; display: none;'>
        <%if (onwardFlightItinerary.FlightId > 0)
          {
              List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

              if (ticketList != null && ticketList.Count > 0)
              {
                  ptcDetails = ticketList[0].PtcDetail;
              }
              else// For Hold Bookings
              {
                  ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(onwardFlightItinerary.FlightId);
              }
              //Show the Agent details for First Email only. In case of Corporate booking second & third 
              //emails need not show Agent details in the email body
              %>      
              <%if (emailCounter <= 1)
                { %>
      
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><h2>e-Ticket</h2></td>
                </tr>
              
              <tr>
                <td>                      
                                                <!--Always show booking date only --> 
                    <%if (onwardFlightItinerary != null && onwardFlightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=onwardFlightItinerary.CreatedOn.ToString("ddd") + "," + onwardFlightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>
                                                                                            
                                                                                            </td>
                </tr>
              </table></td>
            <td align="right" valign="top">
            

            <asp:Image Width='159px' Height='51px' ID='imgLogo' runat='server'  />

      <%-- <img src="http://cozmotravel.com/assets/images/main-logo.svg" alt="#" height="70">--%>
            
            
            </td>
            </tr>
          <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>
          <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>

          <tr>
                                                      
                                                        <td colspan="2" align="right">

                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td rowspan="3" valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h2>Reservation Details </h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><strong>PNR No:  <%=onwardFlightItinerary.PNR%></strong></td>
                                                                                    </tr>                                                                                    
                                                                                    <tr>
                                                                                        <td><strong>Routing Trip Id:  <%=onwardFlightItinerary.RoutingTripId%></strong></td>
                                                                                    </tr>                                                                                    
                                                                                </tbody>
                                                                            </table>
                                                                            <h2>&nbsp;</h2>
                                                                        </td>
                                                                        <td align="right">
                                                                            <table id="tblPrintEmailActions" width="200" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Agent Name: <%=hdnAgencyName.Value %></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Phone: <%=hdnAgencyPhone.Value %></strong></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                   
          </table>

      
      
     
        <% }
              
              
              
           for (int count = 0; count < onwardFlightItinerary.Segments.Length; count++)
           {
               int paxIndex = 0;
               if (Request["paxId"] != null)
               {
                   paxIndex = Convert.ToInt32(Request["paxId"]);
               }

               List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
               ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == onwardFlightItinerary.Segments[count].SegmentId; });
               %>
     
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            
            

            
 
          
         
<tr> 

<td height="20"> </td>


</tr>



          

<tr> 
<td> 
    <%if(count == 0) %>
    <%{ %>
<table width="100%" border="1" cellspacing="0" cellpadding="0">


     <%--passanger header row  starts --%>
     <tr>
         
        <td align="center" valign="middle"><strong>Passenger Name</strong></td>
        <td align="center" valign="middle"><strong>E-Ticket Number</strong></td>
        <td align="center" valign="middle"><strong>Baggage</strong></td>
         <%if (onwardFlightItinerary != null && (onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp)) %>
         <%{ %>
          <td align="center" valign="middle"><strong>Meal</strong></td>
         <%} %>
         <td align="center" valign="middle"><strong>Seats</strong></td>
         <td align="center" valign="middle"><strong>Airline Ref.</strong></td>
          
        </tr> 

    <%--passanger row starts--%>
   
   
   
    <%for (int j = 0; j < onwardFlightItinerary.Passenger.Length; j++)
        { %> 
      <tr>
        <td align="center" valign="top"> <strong>
                                                    <%=onwardFlightItinerary.Passenger[j].Title + " " + onwardFlightItinerary.Passenger[j].FirstName + " " + onwardFlightItinerary.Passenger[j].LastName%>
                                                    </strong> </td>
        <td align="center" valign="top"> 
        
             <%if (ticketList != null && ticketList.Count > 0) { %>
                                           <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[onwardFlightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                            <%} else { //for Corporate HOLD Booking%>
                                        <%=(onwardFlightItinerary.PNR.Split('|').Length > 1 ? onwardFlightItinerary.PNR.Split('|')[onwardFlightItinerary.Segments[count].Group] : onwardFlightItinerary.PNR)%>
                                            <%} %>
        
        </td>
        <td align="center" valign="top">
        
                     <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.Amadeus || onwardFlightItinerary.FlightBookingSource == BookingSource.UAPI || (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir && !(onwardFlightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(onwardFlightItinerary.FlightId,onwardFlightItinerary.Passenger[j].Type)%>
    <%}
    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia || onwardFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.PKFares || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp ||  onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp)
    {
        if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : onwardFlightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : onwardFlightItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.Amadeus && onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : onwardFlightItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir && (onwardFlightItinerary.IsLCC))
    {
        if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = onwardFlightItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %>
         </td>

           <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                        <td>
                                                             <%if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = onwardFlightItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                        <%string SeatInfo = string.Empty;%>

                                                        <td align="center" valign="top">
                                                             <%if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    SeatInfo = onwardFlightItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=SeatInfo%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    SeatInfo = "NoSeat"; %>

                                                                 <%=SeatInfo%>
                                                              
                                                            <%} %>
                                                        </td>


        <td align="center" valign="top">
        
        
       <%=(onwardFlightItinerary.Segments[count].AirlinePNR == null || onwardFlightItinerary.Segments[count].AirlinePNR == "" ? (onwardFlightItinerary.PNR.Split('|').Length > 1 ? onwardFlightItinerary.PNR.Split('|')[onwardFlightItinerary.Segments[count].Group] : onwardFlightItinerary.PNR) : onwardFlightItinerary.Segments[count].AirlinePNR)%>
            <%if (!string.IsNullOrEmpty(onwardFlightItinerary.TripId)) { %>
                                        <td style='width:50%;text-align:right'>                                        
                                       
                                       Corporate Booking Code- <%=onwardFlightItinerary.TripId %>
                                        </td>
                                        <%} %>
        
        </td>
        </tr>
    
    
    <%} %>

    
      </table>
<%} %>
</td>
</tr>

 


           
<%--passanger row ends--%>




<tr> 
  
  <td height="20"> </td> 
  
</tr>






<tr> 


<td>

<table width="100%" border="1" style="border-collapse: collapse;" cellspacing="0" cellpadding="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="border-right: solid 1px #ccc;" valign="top"><table style="margin:auto" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding-top:5px" align="center">

                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(onwardFlightItinerary.Segments[count].Airline);
                            booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(onwardFlightItinerary.FlightId));%>
                                   <img src="http://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="#" />
                                    <%}
                                      catch { } %>
                  
                  </td>
                </tr>
              <tr>
                <td align="center"> <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(onwardFlightItinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label> </td>
                </tr>
              <tr>
                <td align="center"><strong> <label style='padding-left: 20px'>
                                    <%=onwardFlightItinerary.Segments[count].Airline + " " + onwardFlightItinerary.Segments[count].FlightNumber%>
                                     <%try
                                       {  //Loading Operating Airline and showing
                                           if (!onwardFlightItinerary.IsLCC && onwardFlightItinerary.Segments[count].OperatingCarrier != null && onwardFlightItinerary.Segments[count].OperatingCarrier.Length > 0)
                                           {
                                               string opCarrier = onwardFlightItinerary.Segments[count].OperatingCarrier;
                                               CT.Core.Airline opAirline = new CT.Core.Airline();
                                               if (opCarrier.Split('|').Length > 1)
                                               {
                                                   opAirline.Load(opCarrier.Split('|')[0]);
                                               }
                                               else
                                               {
                                                   opAirline.Load(opCarrier.Substring(0, 2));
                                               } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                       }
                                       catch { } %>
                                </label></strong></td>
                </tr>
              </table></td>
            <td align="center" valign="top"><table style="margin:auto" width="94%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><i> Departure </i></td>
                    </tr>
                  <tr>
                    <td><strong><%=onwardFlightItinerary.Segments[count].Origin.CityName + ", " + onwardFlightItinerary.Segments[count].Origin.CountryName%></strong></td>
                    </tr>
                  <tr>
                    <td>
                    
                    <%=onwardFlightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%>
                    
                    
                     <%=onwardFlightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%> 
                     
                     
                     </td>
                    </tr>
                  <tr>
                    <td><%=onwardFlightItinerary.Segments[count].Origin.AirportName + ",Terminal-" + onwardFlightItinerary.Segments[count].DepTerminal%> </td>
                  </tr>
                  
                  </table>
                  </td>
                <td width="20"></td>
                <td>
                
               
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><i> Arrival </i></td>
                    </tr>
                  <tr>
                    <td><strong><%=onwardFlightItinerary.Segments[count].Destination.CityName + ", " + onwardFlightItinerary.Segments[count].Destination.CountryName%></strong></td>
                    </tr>
                  <tr>
                    <td> 

                

                    
                    <%=onwardFlightItinerary.Segments[count].ArrivalTime.ToString("MMM dd yyyy (ddd)")%>
                    <%=onwardFlightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%>
                    
                    
                     </td>

                    </tr>
                  <tr>
                    <td><%=onwardFlightItinerary.Segments[count].Destination.AirportName + ",Terminal-" + onwardFlightItinerary.Segments[count].ArrTerminal%> </td>
                  </tr>
                  
                  </table>
                  
                  
                  </td>
                </tr>
              </table></td>
            <td style="border-left: solid 1px #ccc;" valign="top">
            
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">
                    <%if (onwardFlightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (onwardFlightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (onwardFlightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (onwardFlightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %></td>
                </tr>
              <tr>
                <td align="center"> <%=onwardFlightItinerary.Segments[count].Duration%> </td>
                </tr>
              <tr>
                <td align="center">
                    <%if (!string.IsNullOrEmpty(onwardFlightItinerary.Segments[count].CabinClass)) %>
                    <%{ %>
                    Class: <%=onwardFlightItinerary.Segments[count].CabinClass%>
                    <%} %>
                </td>
                </tr>
              <tr>
                <td style="color:#66ca1d" align="center"><%=booking.Status.ToString()%></td>
                </tr>

                
<%--                                    <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                                      {%>                                    
                                      <label style='padding-left: 20px'><label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                        width: 25%; '>Fare Type : </label> <%=onwardFlightItinerary.Segments[count].SegmentFareType%></label>                                                                             
                                      <%} %>--%>





              </table>
              
              
              
              
              
              
              </td>
            </tr>
          </table></td>
      </tr>
      </table>


 </td>

</tr>


           
            
            
            
            
            
            
            
            
            <%} %>
        </table>



       
        <table style='font-size: 12px;' width='600px' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0,Meal=0,SeatPrice=0,k3Tax=0;

                        if (ticketList != null && ticketList.Count > 0)
                        {
                            List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                            ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                              switch(onwardFlightItinerary.FlightBookingSource)
                                  {
                                     case BookingSource.GoAir:
                                     case BookingSource.GoAirCorp:
                                          k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT")? t.TaxValue : 0);
                                          break;
                                    case BookingSource.UAPI:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                    case BookingSource.SpiceJet:
                                    case BookingSource.SpiceJetCorp:
                                    case BookingSource.Indigo:
                                    case BookingSource.IndigoCorp:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                         break;
                                    default:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                 }
                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                                }
                                Baggage += ticketList[k].Price.BaggageCharge;
                                Meal += ticketList[k].Price.MealCharge;
                                SeatPrice += ticketList[k].Price.SeatPrice;
                                MarkUp += ticketList[k].Price.Markup;
                                Discount += ticketList[k].Price.Discount;
                                AsvAmount += ticketList[k].Price.AsvAmount;
                                outputVAT += ticketList[k].Price.OutputVATAmount;
                            }
                            if (ticketList[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticketList[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < onwardFlightItinerary.Passenger.Length; k++)
                            {
                                AirFare += onwardFlightItinerary.Passenger[k].Price.PublishedFare + onwardFlightItinerary.Passenger[k].Price.HandlingFeeAmount;
                                Taxes += onwardFlightItinerary.Passenger[k].Price.Tax + onwardFlightItinerary.Passenger[k].Price.Markup;
                                if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += onwardFlightItinerary.Passenger[k].Price.AdditionalTxnFee + onwardFlightItinerary.Passenger[k].Price.OtherCharges + onwardFlightItinerary.Passenger[k].Price.SServiceFee + onwardFlightItinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += onwardFlightItinerary.Passenger[k].Price.BaggageCharge;
                                Meal += onwardFlightItinerary.Passenger[k].Price.MealCharge;
                                SeatPrice += onwardFlightItinerary.Passenger[k].Price.SeatPrice;
                                MarkUp += onwardFlightItinerary.Passenger[k].Price.Markup;
                                Discount += onwardFlightItinerary.Passenger[k].Price.Discount;
                                AsvAmount += onwardFlightItinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += onwardFlightItinerary.Passenger[k].Price.OutputVATAmount;
                                if (onwardFlightItinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (onwardFlightItinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                    %>
                     <% if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId<=1)
                              {
                                  
                                  %>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'>
                                            </td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=(Taxes-k3Tax).ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>K3 Tax</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=k3Tax.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia || onwardFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp && Baggage > 0 || onwardFlightItinerary.IsLCC )
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>

                                                        <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{ %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Meal Price</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Meal.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Seat Price</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=SeatPrice.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                  <%if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                            { %>
                                                                    Total GST
                                                                    <%}
                                                                        else if (!CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                        {%>
                                                                                Total GST
                                                                        <%}
                                                                            else
                                                                            { %>
                                                                    VAT 
                                                                    <%} %>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=outputVAT.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                   <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                       { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
    else
    { %>
                                                                     <%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                     <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                      <%} %>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>

        <%} %>

       

       
       
        <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (onwardFlightItinerary != null && onwardFlightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary and Unless ticket number is shown on the ticket copy.Ticket copy should not be handed over to customer.</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>
   
   
    </div>
  
  
    <div id='ReturnEmailDiv' runat='server' style='width: 600px; display: none;'>
        <%if (returnFlightItinerary != null && returnFlightItinerary.FlightId > 0)
            {
                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                if (ticketList != null && ticketList.Count > 0)
                {
                    ptcDetails = ticketList[0].PtcDetail;
                }
                else// For Hold Bookings
                {
                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(returnFlightItinerary.FlightId);
                }
                //Show the Agent details for First Email only. In case of Corporate booking second & third 
                //emails need not show Agent details in the email body
        %>
        <%if (emailCounter <= 1)
            { %>


        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <h2>e-Ticket</h2>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <!--Always show booking date only -->
                                <%if (returnFlightItinerary != null && returnFlightItinerary.CreatedOn != DateTime.MinValue) %>
                                <%{ %>
                                              Booking Date:  <%=returnFlightItinerary.CreatedOn.ToString("ddd") + "," + returnFlightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                                <%} %>
                                                                                            
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" valign="top">
                    <asp:Image Width='159px' Height='51px' ID='Image1' runat='server' />                 
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>

                <td colspan="2" align="right">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td rowspan="3" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h2>Reservation Details </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><strong>PNR No:  <%=returnFlightItinerary.PNR%></strong></td>
                                            </tr>
                                             <tr>
                                                <td><strong>Routing Trip Id:  <%=returnFlightItinerary.RoutingTripId%></strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h2>&nbsp;</h2>
                                </td>
                                <td align="right">
                                    <table id="tblPrintEmailActions" width="200" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>Agent Name: <%=hdnAgencyName.Value %></strong></td>
                            </tr>
                            <tr>
                                <td align="right"><strong>Phone: <%=hdnAgencyPhone.Value %></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

        </table>
        <% }
            for (int count = 0; count < returnFlightItinerary.Segments.Length; count++)
            {
                int paxIndex = 0;
                if (Request["paxId"] != null)
                {
                    paxIndex = Convert.ToInt32(Request["paxId"]);
                }

                List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == returnFlightItinerary.Segments[count].SegmentId; });
        %>

        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td>
                    <%if (count == 0) %>
                    <%{ %>
                    <table width="100%" border="1" cellspacing="0" cellpadding="0">
                        <%--passanger header row  starts --%>
                        <tr>
                            <td align="center" valign="middle"><strong>Passenger Name</strong></td>
                            <td align="center" valign="middle"><strong>E-Ticket Number</strong></td>
                            <td align="center" valign="middle"><strong>Baggage</strong></td>
                            <%if (returnFlightItinerary != null && (returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp)) %>
                            <%{ %>
                            <td align="center" valign="middle"><strong>Meal</strong></td>
                            <%} %>
                            <td align="center" valign="middle"><strong>Seats</strong></td>
                            <td align="center" valign="middle"><strong>Airline Ref.</strong></td>

                        </tr>
                        <%--passanger row starts--%>
                        <%for (int j = 0; j < returnFlightItinerary.Passenger.Length; j++)
                            { %>
                        <tr>
                            <td align="center" valign="top"><strong>
                                <%=returnFlightItinerary.Passenger[j].Title + " " + returnFlightItinerary.Passenger[j].FirstName + " " + returnFlightItinerary.Passenger[j].LastName%>
                            </strong></td>
                            <td align="center" valign="top">

                                <%if (ticketList != null && ticketList.Count > 0)
                                    { %>
                                <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[returnFlightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                <%}
                                else
                                { //for Corporate HOLD Booking%>
                                <%=(returnFlightItinerary.PNR.Split('|').Length > 1 ? returnFlightItinerary.PNR.Split('|')[returnFlightItinerary.Segments[count].Group] : returnFlightItinerary.PNR)%>
                                <%} %>
        
                            </td>
                            <td align="center" valign="top">

                                <%if (returnFlightItinerary.FlightBookingSource == BookingSource.Amadeus || returnFlightItinerary.FlightBookingSource == BookingSource.UAPI || (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir && !(returnFlightItinerary.IsLCC)))
                                    {%>
                                <%=GetBaggageForGDS(returnFlightItinerary.FlightId,returnFlightItinerary.Passenger[j].Type)%>
                                <%}
                                    else if (returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia || returnFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.PKFares || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                                    {
                                        if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
                                        { %>
                                <%=(string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : returnFlightItinerary.Passenger[j].BaggageCode)%>
                                <%}
                                    }
                                    else if (returnFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
                                    {%>

                                <%=(string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : returnFlightItinerary.Passenger[j].BaggageCode)%>

                                <%}
                                    //Added by lokesh on 22-06-2018
                                    //For Amadeus Air Source
                                    //Display baggage information based on the pax type.
                                    else if (returnFlightItinerary.FlightBookingSource == BookingSource.Amadeus && returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
                                    {%>
                                <%=(string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : returnFlightItinerary.Passenger[j].BaggageCode)%>
                                <%}

                                    else if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir && (returnFlightItinerary.IsLCC))
                                    {
                                        if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
                                        {
                                            string strBaggage = string.Empty;

                                            if (!string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode))
                                            {
                                                strBaggage = returnFlightItinerary.Passenger[j].BaggageCode;
                                            }
                                            else
                                            {
                                                strBaggage = "Airline Norms";
                                            }
                                %>
                                <%=strBaggage%>
                                <%}
                                } %>
                            </td>

                            <%if (returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                            <%{
                                    string MealDesc = string.Empty;
                            %>
                            <td>
                                <%if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].MealDesc)) %>
                                <%{
                                        MealDesc = returnFlightItinerary.Passenger[j].MealDesc;
                                %>
                                <%=MealDesc%>
                                <%} %>
                                <%else %>
                                <%{
                                        MealDesc = "No Meals"; %>

                                <%=MealDesc%>

                                <%} %>
                            </td>
                            <%} %>
                            
                            <%string SeatInfo = string.Empty;%>

                            <td align="center" valign="top">
                                    <%if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].Seat.Code)) %>
                                <%{
                                        SeatInfo = returnFlightItinerary.Passenger[j].Seat.Code;
                                        %>
                                        <%=SeatInfo%>
                                <%} %>
                                <%else %>
                                <%{
                                        SeatInfo = "NoSeat"; %>

                                        <%=SeatInfo%>
                                                              
                                <%} %>
                            </td>

                            <td align="center" valign="top">


                                <%=(returnFlightItinerary.Segments[count].AirlinePNR == null || returnFlightItinerary.Segments[count].AirlinePNR == "" ? (returnFlightItinerary.PNR.Split('|').Length > 1 ? returnFlightItinerary.PNR.Split('|')[returnFlightItinerary.Segments[count].Group] : returnFlightItinerary.PNR) : returnFlightItinerary.Segments[count].AirlinePNR)%>
                                <%if (!string.IsNullOrEmpty(returnFlightItinerary.TripId))
                                    { %>
                                <td style='width: 50%; text-align: right'>Corporate Booking Code- <%=returnFlightItinerary.TripId %>
                                </td>
                                <%} %>
        
                            </td>
                        </tr>


                        <%} %>
                    </table>
                    <%} %>
                </td>
            </tr>
            <%--passanger row ends--%>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" style="border-collapse: collapse;" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="border-right: solid 1px #ccc;" valign="top">
                                            <table style="margin: auto" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="padding-top: 5px" align="center">

                                                        <%try
                                                            {
                                                                CT.Core.Airline airline = new CT.Core.Airline();
                                                                airline.Load(returnFlightItinerary.Segments[count].Airline);
                                                                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(returnFlightItinerary.FlightId));%>
                                                        <img src="http://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="#" />
                                                        <%}
                                                            catch { } %>
                  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <label style='padding-left: 20px'>
                                                            <%try
                                                                {
                                                                    CT.Core.Airline airline = new CT.Core.Airline();
                                                                    airline.Load(returnFlightItinerary.Segments[count].Airline);%>
                                                            <%=airline.AirlineName%>
                                                            <%}
                                                                catch { } %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center"><strong>
                                                        <label style='padding-left: 20px'>
                                                            <%=returnFlightItinerary.Segments[count].Airline + " " + returnFlightItinerary.Segments[count].FlightNumber%>
                                                            <%try
                                                                {  //Loading Operating Airline and showing
                                                                    if (!returnFlightItinerary.IsLCC && returnFlightItinerary.Segments[count].OperatingCarrier != null && returnFlightItinerary.Segments[count].OperatingCarrier.Length > 0)
                                                                    {
                                                                        string opCarrier = returnFlightItinerary.Segments[count].OperatingCarrier;
                                                                        CT.Core.Airline opAirline = new CT.Core.Airline();
                                                                        if (opCarrier.Split('|').Length > 1)
                                                                        {
                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                        }
                                                                        else
                                                                        {
                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                        } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %>
                                                        </label>
                                                    </strong></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="center" valign="top">
                                            <table style="margin: auto" width="94%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td><i>Departure </i></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong><%=returnFlightItinerary.Segments[count].Origin.CityName + ", " + returnFlightItinerary.Segments[count].Origin.CountryName%></strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <%=returnFlightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%>
                                                                    <%=returnFlightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%>                    
                     
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><%=returnFlightItinerary.Segments[count].Origin.AirportName + ",Terminal-" + returnFlightItinerary.Segments[count].DepTerminal%> </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td>


                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td><i>Arrival </i></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong><%=returnFlightItinerary.Segments[count].Destination.CityName + ", " + returnFlightItinerary.Segments[count].Destination.CountryName%></strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <%=returnFlightItinerary.Segments[count].ArrivalTime.ToString("MMM dd yyyy (ddd)")%>
                                                                    <%=returnFlightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%>     
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><%=returnFlightItinerary.Segments[count].Destination.AirportName + ",Terminal-" + returnFlightItinerary.Segments[count].ArrTerminal%> </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="border-left: solid 1px #ccc;" valign="top">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="center">
                                                        <%if (returnFlightItinerary.Segments[count].Stops == 0) %>
                                                        <%{ %>
                                                        <strong>Non-Stop </strong>
                                                        <%} %>
                                                        <%else if (returnFlightItinerary.Segments[count].Stops == 1) %>
                                                        <%{ %>
                                                        <strong>Single Stop </strong>
                                                        <%} %>
                                                        <%else if (returnFlightItinerary.Segments[count].Stops == 2) %>
                                                        <%{ %>
                                                        <strong>Two Stops </strong>
                                                        <%} %>
                                                        <%else if (returnFlightItinerary.Segments[count].Stops > 2) %>
                                                        <%{ %>
                                                        <strong>Two+ Stops </strong>
                                                        <%} %></td>
                                                </tr>
                                                <tr>
                                                    <td align="center"><%=returnFlightItinerary.Segments[count].Duration%> </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <%if (!string.IsNullOrEmpty(returnFlightItinerary.Segments[count].CabinClass)) %>
                                                        <%{ %>
                    Class: <%=returnFlightItinerary.Segments[count].CabinClass%>
                                                        <%} %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color: #66ca1d" align="center"><%=booking.Status.ToString()%></td>
                                                </tr>


                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
        </table>
        <table style='font-size: 12px;' width='600px' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0, Meal = 0, SeatPrice = 0,k3Tax=0;

                        if (ticketList != null && ticketList.Count > 0)
                        {
                             List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                             ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                                switch(returnFlightItinerary.FlightBookingSource)
                                  {
                                     case BookingSource.GoAir:
                                     case BookingSource.GoAirCorp:
                                          k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT")? t.TaxValue : 0);
                                          break;
                                    case BookingSource.UAPI:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                    case BookingSource.SpiceJet:
                                    case BookingSource.SpiceJetCorp:
                                    case BookingSource.Indigo:
                                    case BookingSource.IndigoCorp:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                         break;
                                    default:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                 }

                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                                }
                                Baggage += ticketList[k].Price.BaggageCharge;
                                Meal += ticketList[k].Price.MealCharge;
                                SeatPrice += ticketList[k].Price.SeatPrice;
                                MarkUp += ticketList[k].Price.Markup;
                                Discount += ticketList[k].Price.Discount;
                                AsvAmount += ticketList[k].Price.AsvAmount;
                                outputVAT += ticketList[k].Price.OutputVATAmount;
                            }
                            if (ticketList[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticketList[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < returnFlightItinerary.Passenger.Length; k++)
                            {
                                AirFare += returnFlightItinerary.Passenger[k].Price.PublishedFare + returnFlightItinerary.Passenger[k].Price.HandlingFeeAmount;
                                Taxes += returnFlightItinerary.Passenger[k].Price.Tax + returnFlightItinerary.Passenger[k].Price.Markup;
                                if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += returnFlightItinerary.Passenger[k].Price.AdditionalTxnFee + returnFlightItinerary.Passenger[k].Price.OtherCharges + returnFlightItinerary.Passenger[k].Price.SServiceFee + returnFlightItinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += returnFlightItinerary.Passenger[k].Price.BaggageCharge;
                                Meal += returnFlightItinerary.Passenger[k].Price.MealCharge;
                                SeatPrice += returnFlightItinerary.Passenger[k].Price.SeatPrice;
                                MarkUp += returnFlightItinerary.Passenger[k].Price.Markup;
                                Discount += returnFlightItinerary.Passenger[k].Price.Discount;
                                AsvAmount += returnFlightItinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += returnFlightItinerary.Passenger[k].Price.OutputVATAmount;
                                if (returnFlightItinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (returnFlightItinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                    %>
                    <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId <= 1)
                        {

                    %>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'></td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=(Taxes-k3Tax).ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>K3 Tax</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=k3Tax.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia || returnFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp && Baggage > 0 || returnFlightItinerary.IsLCC)
                                                            { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>

                                                        <%if (returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{ %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Meal Price</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Meal.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>

                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Seat Price</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=SeatPrice.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (Discount > 0)
                                                            { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <%if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                            { %>
                                                                    Total GST
                                                                    <%}
                                                                        else if (!CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                        {%>
                                                                                Total GST
                                                                        <%}
                                                                            else
                                                                            { %>
                                                                    VAT 
                                                                    <%} %>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=outputVAT.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: right!important;'>:
                                                            </td>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                    <%if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                        { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
                                                                    else
                                                                    { %>
                                                                    <%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <%} %>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
        </table>

        <%} %>
        <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
        <%if (returnFlightItinerary != null && returnFlightItinerary.FlightBookingSource == BookingSource.PKFares) %>
        <%{ %>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                    <span style="color: red">Note: This is just an itinerary and Unless ticket number is shown on the ticket copy.Ticket copy should not be handed over to customer.</span>
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <%} %>
    </div>
    <!--Email sending code for Corporate Profile -->
















   
    <div id="PreLoader" style="display:none">
        <div class="loadingDiv" style="top:230px;">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">            
                <strong>Awaiting confirmation</strong>               
                </div>
            <div style="font-size: 21px; color: #3060a0">                       
                 <strong style="font-size: 14px; color: black" >do not click the refresh or back button as the transaction may be interrupted or terminated.</strong>
            
                    </div>
        </div>
       <%--<div class="parameterDiv">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%">
                        <label class="parameterLabel">
                            <strong>Check In:</strong></label>
                        <span id="fromDate"><%=itinerary.StartDate.ToString("dd/MM/yyyy") %></span>
                    </td>
                    <td width="50%">
                        <label class="parameterLabel">
                            <strong>Check out:</strong></label>
                        <span id="toDate"><%=itinerary.EndDate.ToString("dd/MM/yyyy") %></span>
                    </td>
                </tr>
            </table>
        </div>--%>
    </div>

    <%--Script for TBO & PKFare Modal Popup --%>
    <% if (onwardResult.ResultBookingSource == BookingSource.TBOAir || onwardResult.ResultBookingSource == BookingSource.PKFares )   {  %>
     <script>              
         //Inserting BOOK NOW button into Modal View
         $('#ctl00_cphTransaction_imgBtnPayment').insertBefore('#TBOConfirm [data-dismiss="modal"]');   
         if ($('#ctl00_cphTransaction_btnHold').length) {
             //Inserting HOLD button into Modal View
             $('#ctl00_cphTransaction_btnHold').insertBefore('#TBOConfirm [data-dismiss="modal"]');
             $('#ctl00_cphTransaction_btnHold').hide();
             $('#TBOHoldConfirmBtn').show();
         }
         $('#TBOConfirmBtn').show();

         //Showing Modal for TBO Confirm
          $('#TBOConfirmBtn').click(function () {                 
              if (document.getElementById('<%=chkRules.ClientID %>').checked == true) {   
                  $('#ctl00_cphTransaction_btnHold').hide();
                  $('#ctl00_cphTransaction_imgBtnPayment').show();
                  $('#ctl00_cphTransaction_imgBtnPayment').css('display', '');
                  $('#rules').hide();
                  $('#TBOConfirm').modal('show');
             } else {
                 validate()
             }
          })
         //Showing Modal for TBO Hold
         $('#TBOHoldConfirmBtn').click(function () {             
             if (document.getElementById('<%=chkRules.ClientID %>').checked == true) {                 
                 $('#ctl00_cphTransaction_imgBtnPayment').hide();
                 $('#ctl00_cphTransaction_btnHold').show();
                  $('#rules').hide();
                  $('#TBOConfirm').modal('show');
              } else {
                  validate()
              }
         })
    </script>
     <%  }  %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
