<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AirlineMaster"  Title="Airline Master" Codebehind="AirlineMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
    <%@ Register Src="DocumentManager.ascx" TagName="DocumentManager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<asp:HiddenField runat="server" ID="hdfAirlineImg" Value="0" />
<asp:HiddenField runat="server" ID="hdfCount" Value="0" />
<asp:HiddenField runat="server" ID="hdfImgPath" Value="0" />
<asp:HiddenField runat="server" ID="hdfAirlineCode" Value="0" />

    
    <div >
    <ul>
    <div class="">
        <div class="col-md-12">
            <div class="col-md-12">
                <h3>
                    Airline Master</h3>
            </div>
            
            <div class="col-md-6" style="text-align:right">
                <asp:Label ID="lblAirlineCode" runat="server" CssClass="label" Width="120px" Text="Airline Code:"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtAirlineCode" runat="server" Width="50px" MaxLength="2" CssClass="form-control capi"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <asp:CheckBox ID="chkIsLCC" runat="server" Text="IsLCC" />
            </div>
            <div class="col-md-6" style="text-align:right">
                <asp:Label ID="lblAirlineName" runat="server" CssClass="label" Width="120px" Text="Airline Name:"></asp:Label>
            </div>
            <div class="col-md-6">
                <asp:TextBox ID="txtAirlineName" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-6" style="text-align:right">
                <asp:Label ID="lblNumericCode" runat="server" Width="120px" CssClass="label" Text="Numeric Code:"></asp:Label>
            </div>
            <div class="col-md-6">
                <asp:TextBox ID="txtNumericCode" runat="server" Width="50px" MaxLength="3" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-6" style="text-align:right"> 
                <asp:Label ID="Label1" runat="server" Text="UAPI Allowed : "></asp:Label>
            </div>
            <div class="col-md-6">
                <asp:CheckBox ID="chkUAPIAllowed" runat="server" />
            </div>
            <div class="col-md-6" style="text-align:right">
                <asp:Label ID="Label2" runat="server" Text="TBO Air Allowed : "></asp:Label>
            </div>
            <div class="col-md-6">
                <asp:CheckBox ID="chkTBOAllowed" runat="server" />
            </div>
            <div class="col-md-6" style="text-align:right">
                <asp:Label ID="lblLogoFile" runat="server" CssClass="label" Width="80px" Text="Logo:"></asp:Label>
            </div>
            <div class="col-md-6" >
                <div class="col-md-3">
                    <uc1:DocumentManager ID="dmAirlineLogo" runat="server" DocumentImageUrl="~/images/common/Preview.png"
                        DefaultImageUrl="~/images/common/no_preview.png" ShowActualImage="false" SessionName="crenterlic"
                        DefaultToolTip="" />
                </div>
                <div class="col-md-3">
                    <asp:LinkButton ID="lnkView" runat="server" Text="View Image" Visible="true" OnClientClick="return View();"></asp:LinkButton>
                </div>
                <div class="col-md-3">
                    <asp:Image ID="imgPreview" Style="display: none;" runat="server" Height="80px" Width="80px" />
                </div>
                <div class="col-md-3">
                    <asp:Label ID="lblUploadMsg" runat="server"></asp:Label>
                    <asp:Label ID="lblUpload1" runat="server" Visible="true"></asp:Label>
                </div>
            </div>
            <div class="col-md-12">
            <div class="col-md-4">
                
                    </div>
                    <div class="col-md-4">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="but but_b" OnClick="btnSave_Click"
                    OnClientClick="return Save();" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="but but_b" OnClick="btnClear_Click" />
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="but but_b" OnClick="btnSearch_Click" Visible="false" />
                </div>
                <div class="col-md-4">
                
                </div>
            </div>
            <div class="col-md-12">
            </div>
            
            <div class="col-md-12">
                <asp:GridView ID="gvAirline" Width="100%" runat="server" DataKeyNames="airlineCode"
                    EmptyDataText="Airline Master List!" AutoGenerateColumns="false" GridLines="none"
                    CssClass="grdTable" OnSelectedIndexChanged="gvAirline_SelectedIndexChanged" CellPadding="4"
                    CellSpacing="0" OnPageIndexChanging="gvAirline_PageIndexChanging" OnRowDataBound="gvAirline_RowDataBound" AllowPaging="true" PageSize="50">
                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                    <Columns>
                        <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                            ControlStyle-CssClass="label" ShowSelectButton="True" />
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <cc1:Filter ID="HTtxtAirLineCode" Width="70px" HeaderText="AirLine Code" CssClass="inputEnabled"
                                    OnClick="FilterSearchAirline_Click" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="ITlblAirlineCode" runat="server" Text='<%# Eval("airlineCode") %>'
                                    CssClass="label grdof" ToolTip='<%# Eval("airlineCode") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <cc1:Filter ID="HTtxtAirLineName" Width="300px" CssClass="inputEnabled" HeaderText="AirLine Name"
                                    OnClick="FilterSearchAirline_Click" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="ITlblAirlineName" runat="server" Text='<%# Eval("airlineName") %>'
                                    CssClass="label grdof" ToolTip='<%# Eval("airlineName") %>' Width="300px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <cc1:Filter ID="HTtxtNumericCode" Width="150px" CssClass="inputEnabled" HeaderText="Numeric Code"
                                    OnClick="FilterSearchAirline_Click" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="ITlblNumericCode" runat="server" Text='<%# Eval("numericCode") %>'
                                    CssClass="label grdof" ToolTip='<%# Eval("numericCode") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <lable>Logo File</lable>
                                <%--<cc1:Filter ID="HTtxtLogoFile" Width="150px" CssClass="inputEnabled" HeaderText="Logo File"
                                        OnClick="FilterSearchAirline_Click" runat="server" />--%>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Image ID="ITlblLogoFile" runat="server" ImageUrl='<%# Eval("logofile") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <cc1:Filter ID="HTtxtIsLCC" Width="70px" HeaderText="IsLCC" CssClass="inputEnabled"
                                    OnClick="FilterSearchAirline_Click" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="ITlblIsLCC" runat="server" Text='<%# Eval("isLCC") %>' CssClass="label grdof"
                                    ToolTip='<%# Eval("isLCC") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <lable>UA</lable>
                                <%--<cc1:Filter ID="HTtxtUA" Width="70px" HeaderText="UA" CssClass="inputEnabled"
                                        OnClick="FilterSearchAirline_Click" runat="server" />--%>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="ITchkUA" runat="server" CssClass="label grdof"  />
                                <asp:HiddenField ID="IThdfAirlineCode" runat="server" Value='<%# Bind("airlineCode") %>'>
                                </asp:HiddenField>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <lable>TA</lable>
                                <%--<cc1:Filter ID="HTtxtTA" Width="70px" HeaderText="TA" CssClass="inputEnabled"
                                        OnClick="FilterSearchAirline_Click" runat="server" />--%>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="ITchkTA" runat="server" CssClass="label grdof" />
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            
            <div class="col-md-12">
                <asp:Button ID="btnUpdateAll" runat="server" Text="Update" CssClass="but but_b" OnClick="btnUpdateAll_Click" />
            </div>
          
          <div class="col-md-12">
                <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </div>
        </div>
        </div>
        </ul>
    </div>
       
    
    
   <script type="text/javascript">
    function Save() {
        if (getElement('txtAirlineCode').value == '') addMessage('Airline Code cannot be blank!', '');
        if (getElement('txtAirlineName').value == '') addMessage('Airline Name cannot be blank!', '');
        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
        }
    }
    
    function View() {
        var count = getElement('hdfCount').value;
        if (count == 1) {

            getElement('hdfCount').value = 0;
            document.getElementById('<%=imgPreview.ClientID %>').style.display = "none";
            getElement('lnkView').innerHTML = "View Image";
            return false;
        }
        else {
            document.getElementById('<%=imgPreview.ClientID %>').style.display = "block";
            document.getElementById('<%=imgPreview.ClientID %>').src = getElement('hdfImgPath').value;
            getElement('lnkView').innerHTML = "Hide";
            getElement('hdfCount').value = 1;
            return false;
        }
    }
    
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="airlineCode" 
    EmptyDataText="Airline Master List!" AutoGenerateColumns="false" PageSize="10" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
     
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate>
    <cc1:Filter ID="HTtxtAirlineCode" Width="70px" HeaderText="Airline Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblAirlineCode" runat="server" Text='<%# Eval("AirlineCode") %>' CssClass="label grdof" ToolTip='<%# Eval("AirlineCode") %>' Width="70px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>      
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtAirlineName"  Width="150px" CssClass="inputEnabled" HeaderText="Airline Name" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblAirlineName" runat="server" Text='<%# Eval("AirlineName") %>' CssClass="label grdof" ToolTip='<%# Eval("AirlineName") %>' Width="70px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>      
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate>
    <cc1:Filter ID="HTtxtIsLCC" Width="70px" HeaderText="IsLCC" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblIsLCC" runat="server" Text='<%# Eval("IsLCC") %>' CssClass="label grdof" ToolTip='<%# Eval("IsLCC") %>' Width="70px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>      
    </Columns>           
    </asp:GridView>

</asp:Content>
