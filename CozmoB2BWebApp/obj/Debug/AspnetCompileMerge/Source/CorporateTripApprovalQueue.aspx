﻿<%@ Page Language="C#"  MasterPageFile="~/TransactionVisaTitle.master"  AutoEventWireup="true" Title="Corporate Trip Approval Queue" Inherits="CorporateTripApprovalQueue" Codebehind="CorporateTripApprovalQueue.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script type="text/javascript">

    function Validate() {

        var valid = false;
        document.getElementById('errMess').style.display = "none";

        if (document.getElementById('ctl00_cphTransaction_dcReimFromDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select from date .";
        }
        else if (document.getElementById('ctl00_cphTransaction_dcReimToDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select to date.";
        }

        else {
            valid = true;
        }

        return valid;
    }
</script>

<div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <h2 style="color: #60c231;
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    text-decoration: underline;
    text-transform: uppercase;">
                Trip Approvers Queue</h2>
                 <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
                    </div>
                
            <div class="tab-content responsive">
                <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                      <%--<div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Booking Type</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlBookingType" runat="server">                                    
                                    <asp:ListItem Selected="True" Value="Normal" Text="Normal"></asp:ListItem>
                                    <asp:ListItem Value="Routing" Text="Routing"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                        
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn but_d btn_xs_block cursor_point mar-5"
                                Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="CorpTrvl-tabbed-panel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#Pending" aria-controls="Pending"
                                        role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                
                                    
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="Pending" style="overflow-x:scroll">
                                         
                                           <div class="table table-responsive mb-0">
                            <asp:GridView OnRowDataBound="gvPending_RowDataBound"  ID="gvPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvPending_PageIndexChanging">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMP_NAME" Width="100px" HeaderText="Employee Name" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTRIP_ID" Width="150px" HeaderText="Reference No" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("UNIQUE_TRIP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("UNIQUE_TRIP_ID") %>' Width="150px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtRETURN_DATE" Width="100px" HeaderText="Return Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblRETURN_DATE" runat="server" Text='<%# Eval("RETURN_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("RETURN_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="200px" HeaderText="Airline" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="200px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtROUTE" Width="300px" HeaderText="Route" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ROUTE") %>' Width="300px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTOTAL_FARE" Width="120px" HeaderText="Fare" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' Width="120px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                           
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" 
                                                Text='View Details'/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                          </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>



</asp:Content>

