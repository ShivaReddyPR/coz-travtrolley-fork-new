﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaggageInsuranceInvoice.aspx.cs" Inherits="CozmoB2BWebApp.BaggageInsuranceInvoiceGUI" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Insurance Invoice</title>
      <link rel="stylesheet" href="css/main-style.css" />
    <script type="text/javascript">
        function printPage() {
            document.getElementById('btnPrint').style.display = "none";
            window.print();
            setTimeout('showButtons()', 1000);
        }
        function showButtons() {
            document.getElementById('btnPrint').style.display = "block";
        }

</script>


          <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div id="">
    <form id="form1" runat="server">
    <div>

</div>
<div>

<input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 

<table id="tblInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table class="FlightInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" align="left">  <%--<img src="images/logo.jpg" alt="Agent Logo" height="50">--%>
    <%--<asp:Image ID="imgLogo" Width="80px" Height="40px" runat="server" AlternateText="AgentLogo" ImageUrl="" />--%>
    </td>
    <td width="33%" align="center"><h1 style=" font-size:26px">Invoice</h1></td>
    <td width="33%" align="right"><input style="width: 100px;" id="btnPrint" onclick="return printPage();" type="button"
                        value="Print Invoice" /></td>
  </tr>
</table>
    </td>
  </tr>
 
 
  <tr>
    <td colspan="2">
    
    
                         <div class="col-md-12 padding-0"> 

<div class="col-md-6">
 <table class="FlightInvoice" width="100%" style="font-size: 12px;font-family: 'Arial'">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <h3>Client Details</h3>                                            </td>
                                        </tr>
                                     <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Agency Name: </b></td>
                                            <td style="vertical-align: middle;"> <% = agency.Name %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Agency Code: </b></td>
                                            <td style="vertical-align: middle;"> <% = agency.Code %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>Agency Address: </b></td>
                                            <td style="vertical-align: middle;"><% = agencyAddress %></td>
                                        </tr>
                                            <tr>
                                            <td style="vertical-align: middle;width: 120px;"><b>TelePhone No: </b></td>
                                                <td style="vertical-align: middle;">
                                                    <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                        {%>
                                                    Phone:
                                                    <% = agency.Phone1%>
                                                    <%  }
                                                        else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                        { %>
                                                    Phone:
                                                    <% = agency.Phone2%>
                                                    <%  } %>
                                                    <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                        { %>
                                                    Fax:
                                                    <% = agency.Fax %>
                                                    <%  } %>                                                </td>
                                        </tr>
                                    </tbody>
                                </table>   
     
      <table class="FlightInvoice" width="100%" style="font-size: 12px;font-family: 'Arial'">
      <tbody>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <%--<h3 style="font-size: 16px; font-family: 'Arial';">Lead Passenger Details</h3>                                            --%>
                                                <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                                    <tbody>

                                    
                                    <tr> 
                                    
                                    
                                    
                                    <td colspan="2">
                                    
                                    
                                    <table border="1" width="100%"> 
                                    
                                    
                                     <tr>
                                                            <td>
                                                                <b>Plan Name: </b>
                                                            </td>
                                                            
                                                            <td>
                                                                <b>Policy No: </b>
                                                            </td>
                                                            </tr> 
                                                            <tr> 
                                                                <td height="30px">
                                                                <%=planName%>
                                                            </td>
                                                            <td height="30px">
                                                                <%=policy%>
                                                            </td>
                                                        </tr>
                                                        
                                                        </table>
                                    
                                    
                                    
                                     </td>
                                    
                                    
                                    </tr>
                                    
                                   
                               
                                                        </tbody>
                                                        </table>
                                                        </td>
                                                        
                              <%--<tbody>
                                <tr>
                                  <td colspan="4"><h3>Booking Details</h3></td>
                                </tr>
                                <tr height="30px">
                                  <td style="vertical-align: middle;width: 91px;"><b>Booking Date: </b></td>
                                  <td style="vertical-align: middle;"><%=header.CretedOn.ToString("dd MMM yy") %></td>
                                  
                                  <td style="vertical-align: middle;width: 91px;"><b>Travel Date: </b></td>
                                  <td style="vertical-align: middle;"><%=Convert.ToDateTime(header.DepartureDateTime).ToString("dd MMM yy")%></td>
                                  
                                </tr>
                                <tr>
                                 <%if (header != null && header.InsPlans != null && header.InsPlans.Count > 0)
                                   { %>
                                   <%for (int k = 0; k < header.InsPlans.Count; k++)
                                     { %>
                                  <td style="vertical-align: top;width: 91px;"><b>Plan Name:</b></td>
                                  <td colspan="3" style="vertical-align: top;"><%=header.InsPlans[k].PlanTitle%></td>
                                  <%} %>
                                  <%} %>
                                  <%--<td style="vertical-align: top;width: 91px;"><b>Check Out: </b></td>
                                  <td style="vertical-align: top;"></td>--%>
                                </tr>
                              </tbody>
                            </table>    
           
             <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <%--<h3 style="font-size: 16px; font-family: 'Arial';">Lead Passenger Details</h3>                                            --%>
                                                <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4">
                                                                <h3>
                                                                    Passenger Details</h3>
                                                            </td>
                                                        </tr>
                                                        <tr height="30px">
                                                            <td style="vertical-align: middle; width: 70%;">
                                                                <b>Pax Name: </b>
                                                            </td>
                                                                <td style="vertical-align: middle; width: 30%;">
                                                                <b><%--Pax Type:--%> </b>
                                                            </td>
                                                        </tr>
                                                        <%if (dsPaxDetails != null && dsPaxDetails.Tables[1].Rows.Count > 0)
                                                            { %>
                                                                   <%for (int k = 0; k < dsPaxDetails.Tables[1].Rows.Count; k++)
                                                                       { %>
                                                          <tr>

                                                              

                                                                  <td style="vertical-align: middle; width: 70%;"> <%=dsPaxDetails.Tables[1].Rows[k]["PAX_FIRST_NAME"] +" "+ dsPaxDetails.Tables[1].Rows[k]["PAX_LAST_NAME"]%> <br /> </td>  
                                                               
                                                               <td style="vertical-align: middle; width: 30%;">
                                                                    </td>
                                                          </tr>
                                                           
                                                                  <%} %>
                                                                  <%} %>
                                                       
                                                    </tbody>
                                                </table>
                                            </td>
                                          
                                        </tr>
                                       
                                    </tbody>
                                </table>     
 </div>

<div class="col-md-6"> 
 <table class="FlightInvoice" width="100%"  style="font-size: 12px;font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <h3>Invoice Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="vertical-align: middle;width: 150px;"><b>Invoice No: </b></td>
                                            <td style="vertical-align: middle;"><%=invoice.CompleteInvoiceNumber%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 150px;"><b>Invoice Date: </b></td>
                                            <td style="vertical-align: middle;"><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;width: 150px;"><b>PNR NO: </b></td>
                                            <td style="vertical-align: middle;"> <% = pnr %></td>
                                        </tr>
                                    </tbody>
                                </table>  
    
    <table class="FlightInvoice" border="0" width="100%" align="right" style="font-size: 12px;font-family: 'Arial'">
                            <tbody>
                             
                              <tr>
                                <td colspan="3"><h3>Rate Details</h3></td>
                              </tr>
                                <tr>
                                    <td style="vertical-align: middle; width: 150px;">
                                        <b>Premium Amount: </b>
                                    </td>
                                    
                                    <td style="vertical-align: middle;">
                                    <asp:Label ID="lblPremiumAmount" runat="server" Text="" ></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; ">
                                        <b><asp:Label ID="lblMarkup" Visible="false" runat="server" Text="MarkUp:"></asp:Label></b>
                                    </td>
                                    <td style="vertical-align: middle;">
                                    <asp:Label ID="lblMarkupVal" runat="server" Visible="false" Text=""></asp:Label>
                                    </td>
                                </tr>

                                 <tr>
                                    <td style="vertical-align: middle; ">
                                        <b><asp:Label ID="lblGST" Visible="false" runat="server" Text="GST:"></asp:Label></b>
                                    </td>
                                    <td style="vertical-align: middle;">
                                    <asp:Label ID="lblGSTValue" runat="server" Visible="false" Text=""></asp:Label>
                                    </td>
                                </tr>
                                   <tr>
                                    <td style="vertical-align: middle;">
                                        <b><asp:Label ID="lblDiscount" Visible="false"  runat="server" Text="Discount:"></asp:Label></b>
                                    </td>
                                    <td style="vertical-align: middle;" >
                                    <asp:Label ID="lblDiscountVal" Visible= "false" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                 
                              <tr>
                                <td style="vertical-align: middle;"><b>Total Amount: </b></td>
                                <td style="vertical-align: middle;">
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                </td>
                              </tr>
                            </tbody>
                          </table> 
                          
                          
                     
                           
</div>




<div class="clearfix"> </div>
</div>



 
    
    
    
    
     
     </td>
     </tr>
   
  
  
  
  
 
  <tr>

    <td colspan="2">
    
    <div class="col-md-12">
    
    <table border="0" class="FlightInvoice" width="100%" style="font-family: 'Arial'">
<tr>
                 <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Invoiced By: </b>
                 </td>
                 <td style="vertical-align: top;">
                     Cozmo Travel
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Created By: </b>
                 </td>
                 <td style="vertical-align: top;">
                     <%= member.FirstName + " " + member.LastName%>
                 </td>
             </tr>
               <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Location: </b>
                 </td>
                 <td style="vertical-align: top;">
                     <asp:Label ID="lblLocation" runat="server"></asp:Label>
                 </td>
             </tr>
    
             <tr>
             <td style="width:130px">
             
          
          </td>
          <td style="width:160px">
          <span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;"></span> 
              </td>
                    <td>
                        <div class="email-message-parent" style="display:none;" id="emailSent">
                    <span  style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                    </span>
                    
           </div>
             </td>
             </tr>
         </table>
    
    </div>
    
    
    
    </td>
  </tr>
</table>
</div>

    </form>
    </div>
</body>
</html>
