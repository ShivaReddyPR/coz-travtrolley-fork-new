﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TransactionVisaTitle.master" CodeBehind="V1AirlineFlightReport.aspx.cs" Title="V1 Airline Flight Report"  Inherits="CozmoB2BWebApp.V1AirlineFlightReport" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 100px" align="right"><a style="cursor: pointer; font-weight: bold; font-size: 8pt; color: Black" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a> </td>
            <td width="800px" align="right"></td>
        </tr>
    </table>


    <div class="grdScrlTrans" style="margin-top: -1px;">
        <div title="Param" id="divParam">
            <asp:Panel runat="server" ID="pnlParam" Visible="true">



                <div class="paramcon">

                    <div class="col-md-12 padding-0 marbot_10">


                        <div class="col-md-2">
                            <asp:Label ID="lblFromDate" Text="From Date:" runat="server"></asp:Label>
                        </div>

                        <div class="col-md-2">
                            <uc1:DateControl Id="dcFromDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                        </div>

                        <div class="col-md-2">
                            <asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label>
                        </div>

                        <div class="col-md-2">
                            <uc1:DateControl Id="dcToDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                        </div>

                        <div class="col-md-2">
                            <asp:Label ID="lblAgent" runat="server" Visible="true" Text="Agent:"></asp:Label>
                        </div>

                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlAgent" CssClass="inputDdlEnabled form-control" Visible="true" runat="server" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>

                        <div class="clearfix"></div>
                    </div>




                    <div class="col-md-12 padding-0 marbot_10">


                        <div class="col-md-2">
                            <asp:Label ID="lblB2BAgent" runat="server" Visible="true" Text="B2BAgent:"></asp:Label>
                        </div>

                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>

                        <div class="col-md-2">
                            <asp:Label ID="lblB2B2BAgent" runat="server" Visible="true" Text="B2B2BAgent:"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control"></asp:DropDownList>
                        </div>
                        <div class="col-md-2" style="display: none">
                            <asp:Label ID="lblSource" runat="server" Text="Source:"></asp:Label>
                        </div>

                        <div class="col-md-2" style="display: none">
                            <asp:DropDownList ID="ddlSource" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2" style="display:none">
                            <asp:Label ID="lblAcctStatus" runat="server" Text="Accounted Status:"></asp:Label>
                        </div>

                        <div class="col-md-2" style="display:none">
                            <asp:DropDownList CssClass="form-control" ID="ddlAcctStatus" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                <asp:ListItem Value="1" Text="YES"></asp:ListItem>
                                <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                            </asp:DropDownList>
                        </div>


                        <div class="clearfix"></div>
                    </div>



                    <div class="col-md-12 padding-0 marbot_10">




                        <div class="col-md-2" style="display:none">
                            <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label>
                        </div>

                        <div class="col-md-2" style="display:none">
                            <asp:DropDownList ID="ddlTransType" CssClass="form-control" runat="server" Visible="false" >
                                <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                                <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                                <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div>
                            <asp:Button runat="server" ID="btnSearch" Text="Search" OnClientClick="return ValidateParam();" CssClass="btn but_b pull-right" OnClick="btnSearch_Click" />
                        </div>

                        <div class="clearfix"></div>
                    </div>


                </div>


            </asp:Panel>
        </div>
        <table width="100%" id="tabSearch" border="0">
            <tr>
                <td>
                    <asp:GridView ID="gvFlightReport" Width="100%" runat="server" AllowPaging="true"
                        DataKeyNames="paxId" EmptyDataText="No Records Found" AutoGenerateColumns="false"
                        PageSize="50" GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0"
                        OnPageIndexChanging="gvFlightReport_PageIndexChanging">
                        <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
                        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                        <Columns>


                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Passenger Type
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblPaxType" runat="server" Text='<%# Eval("paxType") %>' CssClass="label grdof"
                                        ToolTip='<%# Eval("paxType") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Title
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblTitle" runat="server" Text='<%# Eval("title") %>' CssClass="label grdof"
                                        ToolTip='<%# Eval("title") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    First Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblFirstName" runat="server" Text='<%# Eval("firstName") %>' CssClass="label grdof"
                                        ToolTip='<%# Eval("firstName") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Last Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblLastName" runat="server" Text='<%# Eval("lastName") %>' CssClass="label grdof"
                                        ToolTip='<%# Eval("lastName") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Nationality
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("nationality") %>' CssClass="label grdof"
                                        ToolTip='<%# Eval("nationality") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Date of Birth
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblDOB" runat="server" Text='<%# Eval("dateOfBirth") %>' CssClass="label grdof"
                                        ToolTip='<%# Eval("dateOfBirth") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Passport No.
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblPSPNo" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Date of Expiry
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblPSPExp" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Issued Country Code
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblCntryCode" runat="server" CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Arrival Flight No
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblFlightNo" runat="server"  CssClass="label grdof"
                                        Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Flight Arrival Date
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblFlightArrivalDate" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Flight Arrival Time
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblFlightArrivalTime" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Departure Flight No
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblDepFlightNo" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Flight Departure Date
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblDepDate" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Flight Departure Time
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblDepTime" runat="server"  CssClass="label grdof"
                                        Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Group Id
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblGrpId" runat="server"  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Pnr
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblPnr" runat="server"  Text='<%# Eval("Pnr") %>' ToolTip='<%# Eval("Pnr") %>'  CssClass="label grdof"
                                         Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table>
            <tr>

                <td>
                    <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                        CssClass="button" />
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
    <asp:Label Style="color: #dd1f10" ID="lblError" runat="server"></asp:Label>
    <div>
        <asp:DataGrid ID="dgFlightReportList" runat="server" AutoGenerateColumns="false" >
            <Columns>
                <asp:BoundColumn HeaderText="Passenger Type" DataField="paxType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Title" DataField="title" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="First Name" DataField="firstName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Last Name" DataField="lastName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Nationality" DataField="nationality" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Date of Birth" DataField="dateOfBirth" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Passport No." DataField="passportNumber" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Date Of Expiry" DataField="dateOfExp" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
               <asp:BoundColumn HeaderText="Issued Country Code" DataField="IssuedCountryCode" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Arrival Flight No" DataField="ArrFlightNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Flight Arrival Date" DataField="ArrFlightDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Flight Arrival Time" DataField="ArrFlightTime" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Departure Flight No" DataField="DepFlightNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Flight Departure Date" DataField="depFlightDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Flight Departure Time" DataField="depFlightTime" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Group Id" DataField="GroupId" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Pnr" DataField="Pnr" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <script type="text/javascript">

        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function ValidateParam() {
            clearMessage();
            var fromDate = GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
            var fromTime = getElement('dcFromDate_Time');
            var toDate = GetDateTimeObject('ctl00_cphTransaction_dcToDate');
            var toTime = getElement('dcToDate_Time');
            if (fromDate == null) addMessage('Please select From Date !', '');
            //alert(fromTime);
            if (fromTime.value == '') addMessage('Please select From Time!', '');
            if (toDate == null) addMessage('Please select To Date !', '');
            if (toTime.value == '') addMessage('Please select To Time!', '');
            if ((fromDate != null && toDate != null) && fromDate > toDate) addMessage('From Date should not be later than To Date!', '');
            if (getMessage() != '') {
                alert(getMessage());
                clearMessage(); return false;
            }
        }


        function getElement(id) {
            return document.getElementById('ctl00_cphTransaction_' + id);
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
