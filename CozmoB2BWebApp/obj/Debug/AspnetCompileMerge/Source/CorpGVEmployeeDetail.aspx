﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorpGVEmployeeDetailGUI" Codebehind="CorpGVEmployeeDetail.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
       
    <div class="body_container">

<div>
    <div class="col-md-8 pad0">
    <%if (visaSession != null && visaSession.Adult > 0) //&& cropProfileDetails !=null 
        { %>
    <input type="hidden" id="hdnAdultCount" name="hdnAdultCount" value="<%=visaSession.Adult %>" />
    <input type="hidden" id="hdnChildCount" name="hdnAdultCount" value="<%=visaSession.Child %>" />
    <input type="hidden" id="hdnInfantCount" name="hdnAdultCount" value="<%=visaSession.Infant %>" />
           <% int totPax = visaSession.Adult + visaSession.Child + visaSession.Infant;
               int AdultCount = 0;
               int ChildCount = 0;
               int InfantCount = 0;
               for (int i = 0; i < totPax; i++)
               {
                %>


<div class="bggray col-md-12"> <strong> 
    <%if (visaSession.Adult > i)
        { %>
    Personal Details Adult <%=(AdultCount + 1)
                                  %>
    <input type="hidden" id="hdnPaxType-<%=i %>" name="hdnPaxType" value="ADULT" />
    <%
            AdultCount++;
        }
        else if ((visaSession.Adult + visaSession.Child) > i)
        { %>
    Personal Details Child <%=(ChildCount + 1) %>
     <input type="hidden" id="hdnPaxType-<%=i %>" name="hdnPaxType" value="CHILD" />
    <%
            ChildCount++;
        }
        else if ((visaSession.Adult + visaSession.Child + visaSession.Infant) > i)
        {%>
    Personal Details Infant <%=(InfantCount + 1) %>
     <input type="hidden" id="hdnPaxType-<%=i %>" name="hdnPaxType" value="INFANT" />
    <%InfantCount++;
        } %>

                               </strong></div>
<div class="bg_white pad_10 marbot_10"> 


<div class="row"> 




<div class="col-md-3"> 
<div class="form-group">
<label> Applicant Name: </label><sup style="color:Red">*</sup>
<input type="text" class="form-control"  id="txtApplicantName-<%=i %>" name="txtApplicantName-<%=i %>" maxlength="100" onkeypress="return isText(event);" />
      <span class="error_msg" id="errApplicantName-<%=i %>" ></span>
</div>
</div>

<div class="col-md-3"> 
<div class="form-group">
<label> Age Group: </label><sup style="color:Red">*</sup>
    <select class="form-control" id="ddlPaxType-<%=i %>" name="ddlPaxType-<%=i %>" disabled="disabled">
        <option selected="selected" value="Select PaxType">Select PaxType</option>
        <option value="ADULT">ADULT</option>
        <option value="CHILD">CHILD</option>
        <option value="INFANT">INFANT</option>
    </select>
    <span class="error_msg" id="errPaxType-<%=i %>" ></span>
</div>
</div>


<div class="col-md-3"> 
<div class="form-group">
<label> Gender: </label><sup style="color:Red">*</sup>
<select class="form-control" id="ddlGender-<%=i %>" name="ddlGender-<%=i %>">
<option selected="selected" value="Select Gender">Select Gender</option>
    <option value="M">Male</option>
    <option value="F">Female</option>
</select>
    <span class="error_msg" id="errGender-<%=i %>" ></span>
</div>
</div>
     <%if(i==0) { %>
    <div class="col-md-3"> 
<div class="form-group">
<label> Nationality: </label><sup style="color:Red">*</sup>
 <select class="form-control" id="ddlNationality-<%=i %>" name="ddlNationality-<%=i %>">
     <option selected="selected" value="Select Nationality">Select Nationality</option>
     <%if (nationalityList != null)
         {
             foreach (DictionaryEntry nationality in nationalityList)
             {%>

             <option value="<%=nationality.Value %>"><%=nationality.Key %></option>
     <%}
         } %>
</select>
    <span class="error_msg" id="errNationality-<%=i %>" ></span>
</div>
</div>
    <%} %>
</div>
   <%if (ChildCount == 0 && InfantCount==0)
       { %>
    <div class="row"> 


<div class="col-md-3"> 
<div class="form-group">
<label> Martial Status: </label><sup style="color:Red">*</sup>
<select class="form-control" id="ddlMaritalSt-<%=i %>" name="ddlMaritalSt-<%=i %>">
<option selected="selected" value="Select Martial Status">Select Martial Status</option>
    <option value="Married">Married</option>
    <option value="Single">Single</option>
</select>
    <span class="error_msg" id="errMaritalSt-<%=i %>" ></span>
</div>
</div>
<div class="col-md-3"> 
<div class="form-group">
<label> Profession: </label><sup style="color:Red">*</sup>
<input type="text" class="form-control" id="txtProfession-<%=i %>" name="txtProfession-<%=i %>" maxlength="75" onkeypress="return isText(event);" />
    <span class="error_msg" id="errProfession-<%=i %>" ></span>
</div>
</div>
<div class="col-md-3"> 
<div class="form-group">
<label> Email: </label><sup style="color:Red">*</sup>
<input type="text" class="form-control" id="txtEmail-<%=i %>" name="txtEmail-<%=i %>" maxlength="30"/> 
    <span class="error_msg" id="errEmail-<%=i %>" ></span>
</div>
</div>

<div class="col-md-3"> 
<div class="form-group">
<label> Phone: </label><sup style="color:Red">*</sup>
<input type="text" class="form-control" id="txtPhone-<%=i %>" name="txtPhone-<%=i %>" maxlength="15" /> 
    <span class="error_msg" id="errPhone-<%=i %>" ></span>
</div>
</div>
</div>
<%} %>
</div>

<div class="bggray col-md-12"> <strong> Passport Details </strong></div>
<div class="bg_white pad_10 marbot_10"> 

<div class="row"> 




<div class="col-md-3"> 
<div class="form-group">
<label> Passport Type: </label>
    <select class="form-control" id="txtPassportType-<%=i %>" name="txtPassportType-<%=i %>">
        <option value="REGULAR" selected="selected">REGULAR</option>
        <option value="OFFICIAL">OFFICIAL</option>
        <option value="DIPLOMATIC">DIPLOMATIC</option>
    </select>
    <span class="error_msg" id="errPassportType-<%=i %>" ></span>
</div>
</div>


<div class="col-md-3"> 
<div class="form-group">
<label> Passport No. </label><sup style="color:Red">*</sup>
<input type="text" class="form-control" id="txtPassportNo-<%=i %>" name="txtPassportNo-<%=i %>" maxlength="15" onkeypress="return isAlphaNumeric(event);"/>
    <span class="error_msg" id="errPassportNo-<%=i %>" ></span> 
</div>
</div>


<div class="col-md-6"> 
<div class="form-group">
<label> Issued Date:</label><sup style="color:Red">*</sup>
    <div class="row">
        <div class="col-md-4"> <select class="form-control pull-left" id="ddlPassIssueDay-<%=i %>" name="ddlPassIssueDay-<%=i %>">
         <option selected="selected" value="-1">Day</option>
            <%if (passDayList != null) {
                    foreach (string day in passDayList){ %>
             <option value="<%=day %>"><%=day %></option>
            <%}
                } %>
    </select></div>
        <div class="col-md-4"> <select class="form-control pull-left" id="ddlPassIssueMonth-<%=i %>" name="ddlPassIssueMonth-<%=i %>">
          <option selected="selected" value="-1">Month</option>
            <option value="01">Jan</option>
            <option value="02">Feb</option>
            <option value="03">Mar</option>
            <option value="04">Apr</option>
            <option value="05">May</option>
            <option value="06">Jun</option>
            <option value="07">Jul</option>
            <option value="08">Aug</option>
            <option value="09">Sep</option>
            <option value="10">Oct</option>
            <option value="11">Nov</option>
            <option value="12">Dec</option>
    </select></div>
         <div class="col-md-4">
             <select class="form-control pull-left" id="ddlPassIssueYear-<%=i %>" name="ddlPassIssueYear-<%=i %>">
         <option selected="selected" value="-1">Year</option>
                  <%if (passIssueYearList != null)
                     {
                         foreach (int year in passIssueYearList) {%>
                 <option value="<%=year %>"><%=year %></option>
                 <%}
                     }%>
    </select>
         </div>
    </div>
   
   
     
    <span class="error_msg" id="errPassIssueDate-<%=i %>"></span> 
</div>
</div>




</div>




<div class="row">


    <div class="col-md-3"> 
<div class="form-group">
<label> Country: </label><sup style="color:Red">*</sup>
<select class="form-control" id="ddlPassIssueCountry-<%=i %>" name="ddlPassIssueCountry-<%=i %>">
 <option selected="selected" value="Select Country">Select Country</option>
    <%if (countryList != null)
        {
            foreach (DictionaryEntry country in countryList)
            {%>

             <option value="<%=country.Value %>"><%=country.Key %></option>
     <%}
         } %>
</select>
    <span class="error_msg" id="errPassIssueCountry-<%=i %>" ></span> 
</div>
</div>

<div class="col-md-6"> 
<div class="form-group">
<label> Expiry Date:</label><sup style="color:Red">*</sup>
    <div class="row">
        <div class="col-md-4"> <select class="form-control pull-left" id="ddlPassExpiryDay-<%=i %>" name="ddlPassExpiryDay-<%=i %>" >
         <option selected="selected" value="-1">Day</option>
              <%if (passDayList != null) {
                    foreach (string day in passDayList){ %>
             <option value="<%=day %>"><%=day %></option>
            <%}
                } %>
    </select></div>
        <div class="col-md-4"> <select class="form-control pull-left" id="ddlPassExpiryMonth-<%=i %>" name="ddlPassExpiryMonth-<%=i %>">
            <option selected="selected" value="-1">Month</option>
            <option value="01">Jan</option>
            <option value="02">Feb</option>
            <option value="03">Mar</option>
            <option value="04">Apr</option>
            <option value="05">May</option>
            <option value="06">Jun</option>
            <option value="07">Jul</option>
            <option value="08">Aug</option>
            <option value="09">Sep</option>
            <option value="10">Oct</option>
            <option value="11">Nov</option>
            <option value="12">Dec</option>
    </select></div>
         <div class="col-md-4">
             <select class="form-control pull-left" id="ddlPassExpiryYear-<%=i %>" name="ddlPassExpiryYear-<%=i %>">
         <option selected="selected" value="-1">Year</option>
                 <%if (passExpiryYearList != null)
                     {
                         foreach (int year in passExpiryYearList) {%>
                 <option value="<%=year %>"><%=year %></option>
                 <%}
                     }%>
    </select>
         </div>
    </div>
    <span class="error_msg" id="errPassExpiryDate-<%=i %>"></span> 
</div>
</div>

</div>

</div>




<div class="bggray col-md-12"> <strong> Other Details </strong></div>
<div class="bg_white pad_10"> 

<div class="row"> 




<div class="col-md-3"> 
<div class="form-group">
<label> Fax: </label>
<input type="text" class="form-control" id="txtFax-<%=i %>" name="txtFax-<%=i %>" maxlength="15" onkeypress="return isAlphaNumeric(event);"/> 
    <span class="error_msg" id="errFax-<%=i %>" ></span> 
</div>
</div>

    
<div class="col-md-3"> 
<div class="form-group">
<label> City:</label><sup style="color:Red">*</sup>
<input type="text" class="form-control" id="txtCity-<%=i %>" name="txtCity-<%=i %>" maxlength="75" onkeypress="return isText(event);"/> 
    <span class="error_msg" id="errCity-<%=i %>" ></span> 
</div>
</div>



<div class="col-md-3"> 
<div class="form-group">
<label> Zip Postal/Code:</label><sup style="color:Red">*</sup>
<input type="text" class="form-control" id="txtZip-<%=i %>" name="txtZip-<%=i %>" maxlength="15" onkeypress="return isAlphaNumeric(event);"/>
     <span class="error_msg" id="errZip-<%=i %>" ></span>
</div>
</div>
    <%if (i == 0)
            { %>
    <div class="col-md-3"> 
<div class="form-group">
<label> Residence Country:</label><sup style="color:Red">*</sup>
    <select class="form-control" id="ddlResCountry-<%=i %>" name="ddlResCountry-<%=i %>">
        <option selected="selected" value="-1"> Residence Country</option>
         <%if (countryList != null)
            {
                foreach (DictionaryEntry country in countryList)
                {%>

             <option value="<%=country.Value %>"><%=country.Key %></option>
     <%}
            } %>
    </select>
 
    <span class="error_msg" id="errResCountry-<%=i %>" ></span> 
</div>
</div>
    <%} %>

<div class="col-md-9"> 
<div class="form-group">
<label> Office address: </label><sup style="color:Red">*</sup>
<textarea class="form-control" id="txtOffice-<%=i %>" name="txtOffice-<%=i %>" ></textarea> 
    <span class="error_msg" id="errOffice-<%=i %>" ></span>
</div>
</div>
    <div class="col-md-9"> 
<div class="form-group">
<label> Residence address: </label><sup style="color:Red">*</sup>
<textarea class="form-control" id="txtResidence-<%=i %>" name="txtResidence-<%=i %>" ></textarea> 
    <span class="error_msg" id="errResidence-<%=i %>" ></span>
</div>
</div>






</div>

</div>



    <%}
        } %>
        
        
        <div class="clearfix"></div>
    
 </div>
    
        <div class="col-md-4">


            <div class="bggray col-md-12"><strong>Summary</strong></div>
            <div class="bg_white">
                <table class="table table-striped custom-checkbox-table">
                    <tbody>
                        <tr>
                            <td align="left">

                                <label>Visa Country:</label>
                            </td>
                            <td align="right">
                                <label><%=visaSession.TravelToCountryName %></label>
                            </td>


                        </tr>
                        <tr>
                            <td align="left">

                                <label>Visa Type:</label>
                            </td>
                            <td align="right">
                                <label><%=visaSession.VisaTypeName %></label>
                            </td>


                        </tr>
                        <tr>
                            <td align="left">
                                <label>Visa Date:</label>
                            </td>
                            <td align="right">
                                <label><%=visaSession.VisaDate.ToString("dd MMM yyyy") %></label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">

                                <label>No of Applicants:</label>
                            </td>
                            <td align="right">
                                <label>
                                    Adult:<%= visaSession.Adult%>Child:<%=visaSession.Child %> Infant:<%=visaSession.Infant %>
                                </label>
                            </td>


                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
       
        <%if (!IsPostBack)
    {
        if (visaSession != null)
        {
            int totPax = visaSession.Adult + visaSession.Child + visaSession.Infant;
                    %>
        <script type="text/javascript">
            for (var k = 0; k < eval(<%=totPax%>) ; k++) {
                <% if (cropProfileDetails != null)
    {%>
                if (k == 0) {
                    if (document.getElementById('txtApplicantName-' + k) != null) {
                        document.getElementById('txtApplicantName-' + k).value = '<%=cropProfileDetails.SurName + ' ' + cropProfileDetails.Name%>';
                    }
                    if (document.getElementById('ddlNationality-' + k) != null) {
                        document.getElementById('ddlNationality-' + k).value = '<%=cropProfileDetails.NationalityCode%>';
                    }
                    if (document.getElementById('txtProfession-' + k) != null) {
                        document.getElementById('txtProfession-' + k).value = '<%=cropProfileDetails.DesignationName%>';
                    }
                    if (document.getElementById('ddlGender-' + k) != null) {
                        document.getElementById('ddlGender-' + k).value = '<%=cropProfileDetails.Gender%>';
                    }
                    if (document.getElementById('txtEmail-' + k) != null) {
                        document.getElementById('txtEmail-' + k).value = '<%=cropProfileDetails.Email%>';
                    }
                    if (document.getElementById('txtPhone-' + k) != null) {
                        document.getElementById('txtPhone-' + k).value = '<%=cropProfileDetails.Telephone%>';
                    }
                    if (document.getElementById('txtPassportNo-' + k) != null) {
                        document.getElementById('txtPassportNo-' + k).value = '<%=cropProfileDetails.PassportNo%>';
                    }
                    if (document.getElementById('ddlPassIssueDay-' + k) != null) {
                        document.getElementById('ddlPassIssueDay-' + k).value = '<%=cropProfileDetails.DateOfIssue.ToString("dd")%>';
                    }
                    if (document.getElementById('ddlPassIssueMonth-' + k) != null) {
                        document.getElementById('ddlPassIssueMonth-' + k).value = '<%=cropProfileDetails.DateOfIssue.ToString("MM")%>';
                    }
                    if (document.getElementById('ddlPassIssueYear-' + k) != null) {
                        <%if (cropProfileDetails.DateOfIssue != DateTime.MinValue)
    { %>
                        document.getElementById('ddlPassIssueYear-' + k).value = '<%=cropProfileDetails.DateOfIssue.ToString("yyyy")%>';
                        <%}%>
                    }
                    if (document.getElementById('ddlPassIssueCountry-' + k) != null) {
                        document.getElementById('ddlPassIssueCountry-' + k).value = '<%=cropProfileDetails.PlaceOfIssue%>';
                    }
                    if (document.getElementById('ddlPassExpiryDay-' + k) != null) {
                        document.getElementById('ddlPassExpiryDay-' + k).value = '<%=cropProfileDetails.DateOfExpiry.ToString("dd")%>';
                    }
                    if (document.getElementById('ddlPassExpiryMonth-' + k) != null) {
                        document.getElementById('ddlPassExpiryMonth-' + k).value = '<%=cropProfileDetails.DateOfExpiry.ToString("MM")%>';
                    }
                    if (document.getElementById('ddlPassExpiryYear-' + k) != null) {
                        <%if (cropProfileDetails.DateOfExpiry != DateTime.MinValue)
    { %>
                        document.getElementById('ddlPassExpiryYear-' + k).value = '<%=cropProfileDetails.DateOfExpiry.ToString("yyyy")%>';
                        <%}%>
                    }
                    if (document.getElementById('txtZip-' + k) != null) {
                        document.getElementById('txtZip-' + k).value = '<%=cropProfileDetails.Fax%>';
                    }
                    if (document.getElementById('ddlResCountry-' + k) != null) {
                        document.getElementById('ddlResCountry-' + k).value = 'AE'; //Hard Coded Vijay Explained
                    }
                    if (document.getElementById('txtOffice-' + k) != null) {
                        document.getElementById('txtOffice-' + k).value = '<%=cropProfileDetails.Address1%>';
                    }
                    if (document.getElementById('txtResidence-' + k) != null) {
                        document.getElementById('txtResidence-' + k).value = '<%=cropProfileDetails.Address2%>';
                    }
                    if (document.getElementById('ddlMaritalSt-' + k) != null) {
                        document.getElementById('ddlMaritalSt-' + k).value = '<%=cropProfileDetails.MartialStatus%>'
                    }
                }
           <% }%>
                if (document.getElementById('hdnPaxType-' + k) != null && document.getElementById('hdnPaxType-' + k).value == 'ADULT') {
                    if (document.getElementById('ddlPaxType-' + k) != null) {
                        document.getElementById('ddlPaxType-' + k).value = 'ADULT';
                    }
                }
                if (document.getElementById('hdnPaxType-' + k) != null && document.getElementById('hdnPaxType-' + k).value == 'CHILD') {
                    if (document.getElementById('ddlPaxType-' + k) != null) {
                        document.getElementById('ddlPaxType-' + k).value = 'CHILD';
                    }
                }
                if (document.getElementById('hdnPaxType-' + k) != null && document.getElementById('hdnPaxType-' + k).value == 'INFANT') {
                    if (document.getElementById('ddlPaxType-' + k) != null) {
                        document.getElementById('ddlPaxType-' + k).value = 'INFANT';
                    }
                }
             }
        </script>

        <%}
    }%>

 
        </div>


        <div> 
        
        <div class="col-md-12"><asp:Button ID="btnProceed"  Text="Proceed" runat="server" OnClick="btnProceed_Click" OnClientClick="return validate();" class="btn but_b pull-right" />
</div>
        </div> 
    <script>
        function validate()
        {
            var isValid = true;
            var sysdate = new Date();
            var adult=0;
            var child=0;
            var infant=0;
            if( document.getElementById('hdnAdultCount') !=null)
            {
                adult=document.getElementById('hdnAdultCount').value;
            }
            if(document.getElementById('hdnChildCount') !=null)
            {
                child=document.getElementById('hdnChildCount').value;
            }
            if(document.getElementById('hdnInfantCount') !=null)
            {
                infant=document.getElementById('hdnInfantCount').value;
            }
            var totPax=parseInt(adult)+parseInt(child)+parseInt(infant);
            for(var i=0;i<parseInt(totPax);i++)
            {
                if(document.getElementById('errApplicantName-'+i) !=null)
                {
                    document.getElementById('errApplicantName-'+i).innerHTML="";
                }
                if(document.getElementById('errddlPaxType-'+i) !=null)
                {
                    document.getElementById('errddlPaxType-'+i).innerHTML="";
                }
                if(document.getElementById('errNationality-'+i) !=null)
                {
                    document.getElementById('errNationality-'+i).innerHTML ="";
                }
                if(document.getElementById('errGender-'+i) !=null)
                {
                    document.getElementById('errGender-'+i).innerHTML="";
                }
                if(document.getElementById('errMaritalSt-'+i) !=null)
                {
                    document.getElementById('errMaritalSt-'+i).innerHTML="";
                }
                if(document.getElementById('errProfession-'+i) !=null)
                {
                    document.getElementById('errProfession-'+i).innerHTML="";
                }
                if(document.getElementById('errEmail-'+i) !=null)
                {
                    document.getElementById('errEmail-'+i).innerHTML="";
                }
                if(document.getElementById('errPhone-'+i) !=null)
                {
                    document.getElementById('errPhone-'+i).innerHTML="";
                }
                if(document.getElementById('errPassportNo-'+i) !=null)
                {
                    document.getElementById('errPassportNo-'+i).innerHTML="";
                }
                if(document.getElementById('errPlaceOfIssue-'+i) !=null)
                {
                    document.getElementById('errPlaceOfIssue-'+i).innerHTML="";
                }
                if(document.getElementById('errPassIssueCountry-'+i) !=null)
                {
                    document.getElementById('errPassIssueCountry-'+i).innerHTML="";
                }
                if (document.getElementById('errPassIssueDate-' + i) != null) {
                    document.getElementById('errPassIssueDate-' + i).innerHTML = "";
                }
                if (document.getElementById('errPassExpiryDate-' + i) != null) {
                    document.getElementById('errPassExpiryDate-' + i).innerHTML = "";
                }
                if(document.getElementById('errCity-'+i) !=null)
                {
                    document.getElementById('errCity-'+i).innerHTML="";
                }
                if (document.getElementById('errResCountry-' + i) != null) {
                    document.getElementById('errResCountry-' + i).innerHTML = "";
                }
                if(document.getElementById('errZip-'+i) !=null)
                {
                    document.getElementById('errZip-'+i).innerHTML="";
                }
                if (document.getElementById('errOffice-' + i) != null) {
                    document.getElementById('errOffice-' + i).innerHTML = "";
                }
                if (document.getElementById('errResidence-' + i) != null) {
                    document.getElementById('errResidence-' + i).innerHTML = "";
                }
                if(document.getElementById('txtApplicantName-'+i) !=null && document.getElementById('txtApplicantName-'+i).value=="")
                {
                    if(document.getElementById('errApplicantName-'+i) !=null)
                    {
                        document.getElementById('errApplicantName-'+i).innerHTML="Enter Applicant Name";
                        document.getElementById('errApplicantName-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('ddlPaxType-'+i) !=null && document.getElementById('ddlPaxType-'+i).selectedIndex <=0)
                {
                    if(document.getElementById('errPaxType-'+i) !=null)
                    {
                        document.getElementById('errPaxType-'+i).innerHTML="Select PaxType";
                        document.getElementById('errPaxType-'+i).focus();
                        isValid= false;
                    }
                }
                if (i == 0) {
                    if (document.getElementById('ddlNationality-' + i) != null && document.getElementById('ddlNationality-' + i).selectedIndex <= 0) {
                        if (document.getElementById('errNationality-' + i) != null) {
                            document.getElementById('errNationality-' + i).innerHTML = "Select Nationality";
                            document.getElementById('errNationality-' + i).focus();
                            isValid = false;
                        }
                    }
                }
                if(document.getElementById('ddlGender-'+i) !=null && document.getElementById('ddlGender-'+i).selectedIndex <=0)
                {
                    if(document.getElementById('errGender-'+i) !=null)
                    {
                        document.getElementById('errGender-'+i).innerHTML="Select Gender";
                        document.getElementById('errGender-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('ddlMaritalSt-'+i) !=null && document.getElementById('ddlMaritalSt-'+i).selectedIndex <=0)
                {
                    if(document.getElementById('errMaritalSt-'+i) !=null)
                    {
                        document.getElementById('errMaritalSt-'+i).innerHTML="Select Marital Status";
                        document.getElementById('errMaritalSt-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('txtProfession-'+i) !=null && document.getElementById('txtProfession-'+i).value=="")
                {
                    if(document.getElementById('errProfession-'+i) !=null)
                    {
                        document.getElementById('errProfession-'+i).innerHTML="Enter  Profession";
                        document.getElementById('errProfession-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('txtEmail-'+i) !=null && document.getElementById('txtEmail-'+i).value=="")
                {
                    if(document.getElementById('errEmail-'+i) !=null)
                    {
                        document.getElementById('errEmail-'+i).innerHTML="Enter  Email";
                        document.getElementById('errEmail-'+i).focus();
                        isValid= false;
                    }
                }
                else if(!checkEmail((document.getElementById('txtEmail-'+i).value)))
                {
                    if(document.getElementById('errEmail-'+i) !=null)
                    {
                        document.getElementById('errEmail-'+i).innerHTML="Email is not valid";
                        document.getElementById('errEmail-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('txtPhone-'+i) !=null && document.getElementById('txtPhone-'+i).value=="")
                {
                    if(document.getElementById('errPhone-'+i) !=null)
                    {
                        document.getElementById('errPhone-'+i).innerHTML="Enter Phone No";
                        document.getElementById('errPhone-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('txtPassportNo-'+i) !=null && document.getElementById('txtPassportNo-'+i).value=="")
                {
                    if(document.getElementById('errPassportNo-'+i) !=null)
                    {
                        document.getElementById('errPassportNo-'+i).innerHTML="Enter Passport No";
                        document.getElementById('errPassportNo-'+i).focus();
                        isValid= false;
                    }
                }
                
                if(document.getElementById('ddlPassIssueCountry-'+i) !=null && document.getElementById('ddlPassIssueCountry-'+i).selectedIndex <=0)
                {
                    if(document.getElementById('errPassIssueCountry-'+i) !=null)
                    {
                        document.getElementById('errPassIssueCountry-'+i).innerHTML="Select Issue Country";
                        document.getElementById('errPassIssueCountry-'+i).focus();
                        isValid= false;
                    }
                }
                var pspIssueDate = new Date(eval(document.getElementById('ddlPassIssueYear-' + i).value), eval(document.getElementById('ddlPassIssueMonth-' + i).value) - 1, eval(document.getElementById('ddlPassIssueDay-' + i).value));
                if (document.getElementById('ddlPassIssueDay-' + i).value == "-1") {
                    if (document.getElementById('errPassIssueDate-' + i) != null) {
                        document.getElementById('errPassIssueDate-' + i).innerHTML = 'Select Issue Day';
                        document.getElementById('errPassIssueDate-' + i).focus();
                        isValid = false;
                    }
                }
                else if (document.getElementById('ddlPassIssueMonth-' + i).value == "-1") {
                    if (document.getElementById('errPassIssueDate-' + i) != null) {
                        document.getElementById('errPassIssueDate-' + i).innerHTML = 'Select Issue Month';
                        document.getElementById('errPassIssueDate-' + i).focus();
                        isValid = false;
                    }
                }
                else if (document.getElementById('ddlPassIssueYear-' + i).value == "-1") {
                    if (document.getElementById('errPassIssueDate-' + i) != null) {
                        document.getElementById('errPassIssueDate-' + i).innerHTML = 'Select Issue Year';
                        document.getElementById('errPassIssueDate-' + i).focus();
                        isValid = false;
                    }
                }
                else if (pspIssueDate >= sysdate) {
                    if (document.getElementById('errPassIssueDate-' + i) != null) {
                        document.getElementById('errPassIssueDate-' + i).innerHTML = 'Passport Issue date should be less than current date';
                        document.getElementById('errPassIssueDate-' + i).focus();
                        isValid = false;
                    }
                }
                var pspExpDate = new Date(eval(document.getElementById('ddlPassExpiryYear-' + i).value), eval(document.getElementById('ddlPassExpiryMonth-' + i).value) - 1, eval(document.getElementById('ddlPassExpiryDay-' + i).value));
                if (document.getElementById('ddlPassExpiryDay-' + i).value == "-1") {
                    if (document.getElementById('errPassExpiryDate-' + i) != null) {
                        document.getElementById('errPassExpiryDate-' + i).innerHTML = 'Select Expiry Day';
                        document.getElementById('errPassExpiryDate-' + i).focus();
                        isValid = false;
                    }
                }
                else if (document.getElementById('ddlPassExpiryMonth-' + i).value == "-1") {
                    if (document.getElementById('errPassExpiryDate-' + i) != null) {
                        document.getElementById('errPassExpiryDate-' + i).innerHTML = 'Select Expiry Month';
                        document.getElementById('errPassExpiryDate-' + i).focus();
                        isValid = false;
                    }
                }
                else if (document.getElementById('ddlPassExpiryYear-' + i).value == "-1") {
                    if (document.getElementById('errPassExpiryDate-' + i) != null) {
                        document.getElementById('errPassExpiryDate-' + i).innerHTML = 'Select Expiry Year';
                        document.getElementById('errPassExpiryDate-' + i).focus();
                        isValid = false;
                    }
                }
                else if (pspExpDate <= sysdate) {
                    if (document.getElementById('errPassExpiryDate-' + i) != null) {
                        document.getElementById('errPassExpiryDate-' + i).innerHTML = 'Passport expiry date should be later than current date';
                        document.getElementById('errPassExpiryDate-' + i).focus();
                        isValid = false;
                    }
                }
                if (i == 0) {
                    if (document.getElementById('ddlResCountry-' + i) != null && document.getElementById('ddlResCountry-' + i).selectedIndex <= 0) {
                        if (document.getElementById('errResCountry-' + i) != null) {
                            document.getElementById('errResCountry-' + i).innerHTML = "Select Residence Country";
                            document.getElementById('errResCountry-' + i).focus();
                            isValid = false;
                        }
                    }
                }
                if(document.getElementById('txtCity-'+i) !=null && document.getElementById('txtCity-'+i).value =="")
                {
                    if(document.getElementById('errCity-'+i) !=null)
                    {
                        document.getElementById('errCity-'+i).innerHTML="Enter City Name";
                        document.getElementById('errCity-'+i).focus();
                        isValid= false;
                    }
                }
                if(document.getElementById('txtZip-'+i) !=null && document.getElementById('txtZip-'+i).value =="")
                {
                    if(document.getElementById('errZip-'+i) !=null)
                    {
                        document.getElementById('errZip-'+i).innerHTML="Enter Zip Name";
                        document.getElementById('errZip-'+i).focus();
                        isValid= false;
                    }
                }
                if (document.getElementById('txtOffice-' + i) != null && document.getElementById('txtOffice-' + i).value == "")
                {
                    if (document.getElementById('errOffice-' + i) != null) {
                        document.getElementById('errOffice-' + i).innerHTML = "Enter Office Address";
                        document.getElementById('errOffice-' + i).focus();
                        isValid = false;
                    }
                }
                if (document.getElementById('txtResidence-' + i) != null && document.getElementById('txtResidence-' + i).value == "") {
                    if (document.getElementById('errResidence-' + i) != null) {
                        document.getElementById('errResidence-' + i).innerHTML = "Enter Residence Address";
                        document.getElementById('errResidence-' + i).focus();
                        isValid = false;
                    }
                }
                
            }
            return isValid;
        }       
         
        
       var specialKeys = new Array();
       specialKeys.push(8); //Backspace
       specialKeys.push(9); //Tab
       specialKeys.push(46); //Delete
       specialKeys.push(36); //Home
       specialKeys.push(35); //End
       specialKeys.push(37); //Left
       specialKeys.push(39); //Right
       function isAlphaNumeric(e) {
           var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
           var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
           return ret;
        }
         function isText(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                return false;
            }
            return true;
        }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

