﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateFlexMasterUI1" Title="CorporateFlexMaster" EnableEventValidation="false" CodeBehind="CorporateFlexMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript" src="Scripts/responsive-tabs.js"></script>
    <asp:HiddenField runat="server" ID="hdnDTFlexMasterList" />
    <div class="container-fluid CorpTrvl-page">
        <div class="row">
            <div class="col-xs-12">
                <h2>Flex Master</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="CorpTrvl-tabbed-panel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs responsive" role="tablist">
                        <li role="presentation" class="active"><a href="#gen-info" aria-controls="gen-info"
                            role="tab" data-toggle="tab">FLEX MASTER</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content ">
                        <div role="tabpanel" class="tab-pane active" id="gen-info"  style="overflow:scroll">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-2">
                                    <div class="form-group">
                                       <label>Client :</label>
                                        <asp:DropDownList ID="ddlAgent" runat="server" class="form-control" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                        </asp:DropDownList>
                                    </div>
                                    
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-2">
                                     <div class="form-group">
                                        <label>Product :</label>
                                        <asp:DropDownList ID="ddlProduct" runat="server" class="form-control" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="1" Text="Flight"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Hotel"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                         
                                <asp:HiddenField ID="hdfFlexDetails" runat="server"></asp:HiddenField>
                               <div class="">
                                <asp:GridView ID="gvFlexDetails" CssClass="b2b-corp-table table table-bordered table-condensed" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                                    PageSize="20" DataKeyNames="flexId" CellPadding="0" ShowFooter="true" CellSpacing="0"
                                    GridLines="Both" OnRowCommand="gvFlexDetails_RowCommand" Width="100%" OnRowEditing="gvFlexDetails_RowEditing"
                                    OnRowDeleting="gvFlexDetails_RowDeleting" OnRowUpdating="gvFlexDetails_RowUpdating"
                                    OnRowCancelingEdit="gvFlexDetails_RowCancelingEdit" OnPageIndexChanging="gvFlexDetails_PageIndexChanging" Visible="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    GDS prefix</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITGDSprefix" runat="server" Text='<%#Eval("flexGDSprefix") %>' CssClass=" grdof"
                                                    ToolTip='<%#Eval("flexGDSprefix") %>' Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="EITtxtGDSprefix" runat="server" Enabled="true" Text='<%#Eval("flexGDSprefix") %>'
                                                    CssClass="inputDdlEnabled" onchange="ValidateDuplicate(this,'GDS prefix',0)"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:TextBox ID="FTtxtGDSprefix" runat="server" Enabled="true" CssClass="form-control" onchange="ValidateDuplicate(this,'GDS prefix',0)" style="height: 34px;"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    Label</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("flexLabel") %>' CssClass=" grdof"
                                                    ToolTip='<%#Eval("flexLabel") %>' Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="EITtxtLabel" runat="server" Enabled="true" Text='<%#Eval("flexLabel") %>'
                                                    CssClass="inputDdlEnabled" onchange="ValidateDuplicate(this,'Label',1)">></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:TextBox ID="FTtxtLabel" runat="server" Enabled="true" CssClass="form-control" onchange="ValidateDuplicate(this,'Label',1)" style="height: 34px;"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                                                                <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    <span class="mdt"></span>Dependency</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblValid" runat="server" Text='<%# Eval("flexValIdDESC") %>'
                                                    CssClass=" grdof" ToolTip='<%# Eval("flexValId") %>' Width="110px"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="EITddlValid" runat="server" Enabled="true">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="EIThdfvalid" runat="server" Value='<%# Eval("flexValId") %>'></asp:HiddenField>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="FTddlValid" runat="server" Enabled="true">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    <span class="mdt"></span>Control</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>'
                                                    CssClass=" grdof" ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="EITddlControl" runat="server" Enabled="true">
                                                    <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="EIThdfControl" runat="server" Value='<%# Eval("flexControl") %>'></asp:HiddenField>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="FTddlControl" runat="server" Enabled="true"
                                                    Maxlength="50">
                                                    <asp:ListItem Text="-SelectControl-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    <span class="mdt"></span>Place Holder</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblplaceHolder" runat="server" Text='<%# Eval("flexplaceholdDESC") %>'
                                                    CssClass=" grdof" ToolTip='<%# Eval("flexplacehold") %>' Width="110px"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="EITddlplaceHolder" runat="server" Enabled="true">
                                                    <asp:ListItem Text="Header" Value="HDR"></asp:ListItem>
                                                    <asp:ListItem Text="Footer" Value="FOT"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="EIThdfplaceHolder" runat="server" Value='<%# Eval("flexplacehold") %>'></asp:HiddenField>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="FTddlplaceHolder" runat="server" Enabled="true"
                                                    Maxlength="50">
                                                    <asp:ListItem Text="Header" Value="HDR"></asp:ListItem>
                                                    <asp:ListItem Text="Footer" Value="FOT"></asp:ListItem>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    Sql Query</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITSqlQuery" runat="server" Text='<%#Eval("flexSqlQuery") %>' style="width:100px;display: block;" CssClass=" grdof"
                                                    ToolTip='<%#Eval("flexSqlQuery") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="EITtxtSqlQuery" runat="server" Enabled="true" Text='<%#Eval("flexSqlQuery") %>'
                                                    CssClass="form-control"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:TextBox ID="FTtxtSqlQuery" runat="server" Enabled="true" CssClass="form-control" style="height: 34px;"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    <span class="mdt"></span>DataType</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>'
                                                    CssClass=" grdof" ToolTip='<%# Eval("flexDataTypeDESC") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="EITddlDataType" runat="server" Enabled="true">
                                                    <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                                    <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="EIThdfDataType" runat="server" Value='<%# Eval("flexDataType") %>'></asp:HiddenField>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="FTddlDatatype" runat="server" Enabled="true">
                                                    <asp:ListItem Text="-SelectDataType-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                                    <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    <span class="mdt"></span>Mandatory</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>'
                                                    CssClass=" grdof" ToolTip='<%# Eval("flexStatusDESC") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="EITddlMandatory" runat="server" Enabled="true">
                                                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="EIThdfStatus" runat="server" Value='<%# Eval("flexMandatoryStatus") %>'></asp:HiddenField>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:DropDownList CssClass="form-control" ID="FTddlMandatory" runat="server" Enabled="true">
                                                    <asp:ListItem Text="-SelectStatus-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    Order</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITOrder" runat="server" Text='<%#Eval("flexOrder") %>' CssClass=" grdof"
                                                    ToolTip='<%#Eval("flexOrder") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="EITtxtOrder" runat="server" Enabled="true" Text='<%#Eval("flexOrder") %>'
                                                    CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" onchange="ValidateDuplicate(this,'Order',1)"></asp:TextBox>
                                            </EditItemTemplate>
                                               
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:TextBox ID="FTtxtOrder" runat="server" Enabled="true" CssClass="form-control"
                                                    onkeypress="return restrictNumeric(this.id,'2');" onchange="ValidateDuplicate(this,'Order',1)" style="height: 34px;"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <HeaderTemplate>
                                                <label style="color: Black; font-weight: bold">
                                                    Disable</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                              <%-- <asp:CheckBox ID="ITchkDisable" runat="server" checked='<%#Bind("flexDisable") %>' Enabled="false" CssClass=" grdof" />--%>
                                                 <asp:CheckBox ID="ITchkDisable" runat="server" checked='<%# Eval("flexDisable") != null ? Eval("flexDisable").ToString() == "True" ?true:false : false%>' Enabled="false" CssClass=" grdof" />

                                            </ItemTemplate>
                                            <EditItemTemplate>
                                               <asp:CheckBox ID="EITchkDisable" runat="server" checked='<%# Eval("flexDisable") != null ? Eval("flexDisable").ToString() == "True" ?true:false : false%>' CssClass="" />
                                            </EditItemTemplate>
                                             <FooterTemplate>
                                                <asp:CheckBox ID="FTchkDisable" runat="server" Enabled="true" CssClass="" />
                                                   
                                            </FooterTemplate>
                                           
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label runat="server" ID="id" Width="100px"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                </asp:LinkButton>
                                                <asp:ImageButton ID="ibtnFDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif"
                                                    CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return Delete();"></asp:ImageButton>
                                            </ItemTemplate>

                                            <EditItemTemplate>
                                                <asp:LinkButton ID="lnkFUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                    OnClientClick="return validateDtlAddUpdate('U',this.id)"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkFCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                            <FooterTemplate>
                                                <asp:LinkButton ID="lnkFAdd" CommandName="Add" runat="server" Text="Add" Width="50px"
                                                    OnClientClick="return validateDtlAddUpdate('I',this.id)"></asp:LinkButton>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="grid011" />
                                    <RowStyle HorizontalAlign="left" />
                                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                </asp:GridView>
                                <div class="row top-buffer">
                                    <div class="col-xs-12">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary btn-blue" Visible="false" OnClick="btnSave_Click" OnClientClick="return Save();" />
                                        <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-default" Visible="false" OnClick="btnClear_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        (function ($) {
            fakewaffle.responsiveTabs(['xs', 'sm', 'md']);

        })(jQuery);
        function validateDtlAddUpdate(action, id) {
            try {
                debugger;
                var rowId = id.substring(0, id.lastIndexOf('_'));
                if (action == 'I') {
                    debugger;
                    if (document.getElementById(rowId + '_FTtxtLabel').value.trim() == '') addMessage('Label cannot be blank!', '');
                      if (document.getElementById(rowId + '_FTtxtGDSprefix').value.trim() == '') addMessage('Prefix cannot be blank!', '');
                    if (document.getElementById(rowId + '_FTddlControl').selectedIndex <= 0) addMessage('Please select a Control! ', '');
                    if ((document.getElementById(rowId + '_FTddlControl').value == 'L')|| (document.getElementById(rowId + '_FTddlControl').value == 'L' && document.getElementById(rowId + '_FTddlValid').selectedIndex > 0)) {
                        if (document.getElementById(rowId + '_FTtxtSqlQuery').value.trim() == '') addMessage('SqlQuery cannot be blank!', '');
                    }
                    if (document.getElementById(rowId + '_FTddlDatatype').selectedIndex <= 0) addMessage('Please select a DataType!', '');
                    if (document.getElementById(rowId + '_FTddlMandatory').selectedIndex <= 0) addMessage('Please select a MandatoryStatus!', '');
                    if (document.getElementById(rowId + '_FTtxtOrder').value.trim() == '') addMessage('Order cannot be blank!', '');
                }
                else if (action == 'U')//Update
                {
                    if (document.getElementById(rowId + '_EITtxtLabel').value.trim() == '') addMessage('Label cannot be blank!', '');
                     if (document.getElementById(rowId + '_EITtxtGDSprefix').value.trim() == '') addMessage('Prefix cannot be blank!', '');
                    if ((document.getElementById(rowId + '_EITddlControl').value == 'L')|| document.getElementById(rowId + '_EITddlControl').value == 'L' && document.getElementById(rowId + '_EITddlValid').selectedIndex > 0) {
                        if (document.getElementById(rowId + '_EITtxtSqlQuery').value.trim() == '') addMessage('SqlQuery cannot be blank!', '');
                    }
                    if (document.getElementById(rowId + '_EITtxtOrder').value.trim() == '') addMessage('Order cannot be blank!', '');
                }
                if (getMessage() != '') {
                    alert(getMessage()); clearMessage(); return false;
                }
            }
            catch (exception) {
                alert('Failed to validate data, Error: ' + exception.message);
                return false;
            }
        }
        function Save() {
            var gridv=document.getElementById("<%=gvFlexDetails.ClientID %>");
            var gridlength=gridv.rows.length;
            var grow = gridv.rows[gridlength - 1];
            var rowId = grow.cells[0].lastElementChild.id.substring(0, grow.cells[0].lastElementChild.id.lastIndexOf('_'));
            if (getElement('hdfFlexDetails').value == "2") addMessage('Flex Details in Edit Mode!', '');     
           else if (document.getElementById(rowId + '_FTtxtLabel').value.trim() != ''
                        || document.getElementById(rowId + '_FTtxtGDSprefix').value.trim() != ''
                        || document.getElementById(rowId + '_FTddlControl').selectedIndex != 0
                        || document.getElementById(rowId + '_FTtxtSqlQuery').value.trim() != ''
                        || document.getElementById(rowId + '_FTddlDatatype').selectedIndex != 0
                        || document.getElementById(rowId + '_FTddlMandatory').selectedIndex != 0
                        || document.getElementById(rowId + '_FTtxtOrder').value.trim() != ''
                        || document.getElementById(rowId + '_FTchkDisable').Checked) addMessage('please add all fields and click on Add', '');
           if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }
        }
       
        function ValidateDuplicate(Control, Text, id) {
            var List=JSON.parse(document.getElementById('<%=hdnDTFlexMasterList.ClientID %>').value);
      <%--      var grv = document.getElementById("<%=gvFlexDetails.ClientID %>");--%>
            var Value = document.getElementById(Control.id).value;
            if (List.length > 0) {
                 if (Value != '') {
                                
                for (var i = 0; i < List.length; i++) {
                    if (Text == "GDS prefix") {
                        if (List[i].flexGDSprefix.toUpperCase().trim() == Value.toUpperCase().trim()) {
                            alert(Text + " Exist");
                            document.getElementById(Control.id).value = "";
                        }
                    }
                    else if (Text == "Label") {
                        if (List[i].flexLabel.toUpperCase().trim() == Value.toUpperCase().trim()) {
                            alert(Text + " Exist");
                            document.getElementById(Control.id).value = "";
                        }
                    }   
                    else if (Text == "Order") {
                        if (List[i].flexOrder == Value) {
                            alert(Text + "Number Exist");
                            document.getElementById(Control.id).value = "";
                        }
                    }   
                }
            }
            }
           
        }
         function Delete() {
            debugger;
            if (getElement('hdfFlexDetails').value == "2") {
                addMessage('Flex Details in Edit Mode!', '');
                if (getMessage() != '') {
                    alert(getMessage()); clearMessage(); return false;
                }
            }
                 else {
               var r =  confirm('Do you want to delete the user?');
if (r == true) {
    return true;
} else {
    return false;
}
                 }
        }
    </script>
</asp:Content>

