﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="Flightqueue.aspx.cs" Inherits="CozmoB2BWebApp.Flightqueue" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <link href="css/TicketQuee.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <script src="scripts/paginathing.js" type="text/javascript"></script>


    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <style>
        .modal-dialog {
            width: 680px;
        }
        .modal-header {
            padding: 10px 10px;
            color: #FFF;
        }
 a:hover {
  cursor:pointer;
 }

    </style>
    <script type="text/javascript">
   <%--    var pageUrl = '<%=ResolveUrl("Flightqueue.aspx")%>'--%>
        var SearchList;
        var TotalRecords = 0;
        jQuery(document).ready(function ($) {
            Search();
            $('#NoRecords').hide();

        });

        function showselpage(event, pageno) {
            LoadData(pageno);
            $(this).addClass('active');

        }
        function BindAgents(AgentId, AgentType) {
            var Agent_id = "";
            var type = "";
            var B2BAgent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
            var B2B2BAgent_id = $('#<%=ddlB2B2BAgent.ClientID%>').val();
            if (AgentType == "B2B") {
                Agent_id = $('#<%=ddlAgency.ClientID%>').val();
                if ($('#<%=ddlB2BAgent.ClientID%>').val() < 0) {
                    $('#<%=ddlB2B2BAgent.ClientID %>').prop('disabled', true);
                }
                if (Agent_id == 0) {
                    type = "BASE";
                }
            }
            else if (AgentType == "B2B2B") {
                Agent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
                if (Agent_id >= "0") {
                    $('#<%=ddlB2B2BAgent.ClientID %>').prop('disabled', false);
                }
                else {
                    $('#<%=ddlB2B2BAgent.ClientID %>').prop('disabled', true);
                }
                if (Agent_id == " 0") {

                    if ($('#<%=ddlAgency.ClientID%>').val() > "1") {
                        type = "AGENT";/*AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations*/
                        Agent_id = $('#<%=ddlAgency.ClientID%>').val();
                    }
                    else {
                        type = "B2B"; /*B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations*/
                        Agent_id = $('#<%=ddlAgency.ClientID%>').val();
                    }
                }
                else {
                    if (Agent_id == "-1") {
                        Agent_id = $('#<%=ddlAgency.ClientID%>').val();
                        if ($('#<%=ddlAgency.ClientID%>').val() == " 0") {
                            type = "BASE";/* BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations*/
                        }
                    }
                }
                if ($('#<%=ddlAgency.ClientID%>').val() == " 0") {
                    if (Agent_id == " 0") {
                        type = "BASEB2B"; /*BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations*/
                    }
                }
            }
            if (AgentType == "") {
                Agent_id = $('#<%=ddlB2B2BAgent.ClientID%>').val();
                if (Agent_id == "0") {
                    type = "B2B2B"; /*B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations*/
                    Agent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
                    if (Agent_id == "0") {
                        type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    }
                }
                else if (Agent_id == "-1") {
                    Agent_id = $('#<%=ddlB2BAgent.ClientID%>').val();
                    if (Agent_id == "0") {
                        type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                        Agent_id = $('#<%=ddlAgency.ClientID%>').val();
                    }
                }
                if ($('#<%=ddlAgency.ClientID%>').val() == "0") {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == " 0") {
                        if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                            type = "";
                        }
                    }
                }
                if ($('#<%=ddlAgency.ClientID%>').val() != "0") {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {
                        if (('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                            type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                            Agent_id = $('#<%=ddlAgency.ClientID%>').val();
                        }
                    }
                }
            }
            if (AgentType != "") {
                var options = "";
                options += "<option value='-1'>--Select--</option><option value='0'>--All--</option>";
                $.ajax({
                    type: "POST",
                    url:"Flightqueue.aspx/BindAllAgentTypes",
                    dataType: "json",
                    contentType: "application/json",
                    data: "{'agentId':'" + AgentId + "','AgentType':'" + AgentType + "'}",
                    success: function (data) {
                        $.each(data.d, function (item) {
                            options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
                        });
                        if (AgentType == "B2B") {
                            $('#ctl00_cphTransaction_ddlB2BAgent').empty();
                            $('#ctl00_cphTransaction_ddlB2BAgent').append(options);
                            $('#' + getElement('ddlB2BAgent').id).select2('val', '-1');
                        }
                        else if (AgentType == "B2B2B") {
                            $('#ctl00_cphTransaction_ddlB2B2BAgent').empty();
                            $('#ctl00_cphTransaction_ddlB2B2BAgent').append(options);
                            $('#' + getElement('ddlB2B2BAgent').id).select2('val', '-1');
                        }

                    },
                    failure: function (error) { alert('error'); }

                });

            }
            BindLocation(Agent_id, type);
        }
        function BindLocation(Agent_id, Type) {
            var options = "";
            options += "<option value='-1'>--Select--</option><option value='0'>--All--</option>";
            $.ajax({
                type: "POST",
                url: "Flightqueue.aspx/BindLocation",
                dataType: "json",
                contentType: "application/json",
                data: "{'agentId':'" + Agent_id + "','type':'" + Type + "'}",
                success: function (data) {
                    $.each(data.d, function (item) {
                        options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
                    });
                    $('#ctl00_cphTransaction_ddlLocation').empty();
                    $('#ctl00_cphTransaction_ddlLocation').append(options);
                    $('#' + getElement('ddlLocation').id).select2('val', '-1');
                },
                failure: function (error) { alert('error'); }
            });

        }

        function Search() {
            //TransType  Start
            var transType = "";
                  <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.TransType == "B2B")
        {%>
            $(".TransType").hide();
            transType = "B2B";
       <% }
        else if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.TransType == "B2C")
        {%>
            $(".TransType").hide();
            transType = "B2C";
           <% }
        else
        {%>
            $(".TransType").show();
            transType = $('#<%=ddlTransType.ClientID%>').val();
           <% }%>
            //Transtype End
            //AgentFilter Starts
            var AgentFilter = $('#<%=ddlAgency.ClientID%>').val();
            var agentType = "";
            if (AgentFilter == "0") {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                    agentType = ""; // null Means binding in list all BOOKINGS
                }
            }
            if (AgentFilter > "0" && $('#<%=ddlB2BAgent.ClientID%>').val() != "-1") {
                if (AgentFilter > 1) {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {

                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else {
                        AgentFilter = $('#<%=ddlB2BAgent.ClientID%>').val();
                    }
                }
                else {
                    if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {

                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    AgentFilter = $('#<%=ddlB2BAgent.ClientID%>').val();
                }
            }
            if (AgentFilter > "0" && $('#<%=ddlB2B2BAgent.ClientID%>').val() != "-1") {
                if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else {
                    AgentFilter = $('#<%=ddlB2B2BAgent.ClientID%>').val();
                }
            }
            if ($('#<%=ddlAgency.ClientID%>').val() != "0") {
                if ($('#<%=ddlB2BAgent.ClientID%>').val() == "0") {
                    if ($('#<%=ddlB2B2BAgent.ClientID%>').val() == "0") {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        AgentFilter = $('#<%=ddlAgency.ClientID%>').val();
                    }
                }
            }
            //AgentFilter End
            //document.getElementById('ctl00_upProgress').style.display = 'block';
            var obj = {};
            obj.FromDate = $('#<%=txtFromDate.ClientID%>').val();
            obj.ToDate = $('#<%=txtToDate.ClientID%>').val();
            obj.Status = $('#<%=ddlBookingStatus.ClientID%>').val();
            obj.PaxName = $.trim($('#<%=txtPaxName.ClientID%>').val());
            obj.PnrNo = $.trim($('#<%=txtPNR.ClientID%>').val());
            obj.LocationId = $('#<%=ddlLocation.ClientID%>').val();
            obj.RoutingTripId = $.trim($('#<%=txtRoutingTrip.ClientID%>').val());
            obj.source = $('#<%=ddlsource.ClientID%>').val();
            obj.AgentFilter = AgentFilter;
            obj.agentType = agentType;
            obj.transType = transType;
            var jsons = JSON.stringify(obj);
            $.ajax({
                type: "POST",
                url: "Flightqueue.aspx/Search",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: jsons,
                success: function (data) {
                    SearchList = JSON.parse(data.d);
                    TotalRecords = SearchList.length;
                    $('.panel-footer').remove();
                    if (TotalRecords > 0) {
                        $('#NoRecords').hide();
                        $('.list-group').paginathing({
                            limitPagination: 9,
                            containerClass: 'panel-footer',
                            pageNumbers: true,
                            totalRecords: TotalRecords
                        });
                        LoadData(1);
                        ShowHide('divParam');
                       
                    }
                    else {
                          $('.list-group').children().remove();
                        $('#NoRecords').show();
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
            //document.getElementById('ctl00_upProgress').style.display = 'none';
        }
        function LoadData(page) {
            $('.list-group').children().remove();
            //var showFrom = ((Math.ceil(page) - 1) * 10) + 1;
            //var showTo = Math.ceil(showFrom) + 9;
               var showFrom = ((Math.ceil(page) - 1) * 10) ;
            var showTo = Math.ceil(showFrom) + 9;
            var orgqueueTemplate = $('#templateView').html();
            for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {
                var traveldate = new Date(SearchList[i].travelDate);
                var CreatedOn = new Date(SearchList[i].createdOn);
                traveldate.setDate(traveldate.getDate() - 2);
                var CancelDate = traveldate.toISOString();
                CancelDate = new Date(CancelDate);
                CurrentDate = new Date();
                traveldate = new Date(SearchList[i].travelDate);
                var BookingDate = new Date(SearchList[i].createdOn);
                var day = '';
                if (BookingDate == CurrentDate) {
                    day = 'Today'
                }
                else {
                    var days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
                    day = BookingDate.getDay();
                    day = days[day];
                }
                SearchList[i].createdOn = BookingDate.toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' }).replace(/ /g, ' ');
                SearchList[i].travelDate = traveldate.toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' }).replace(/ /g, ' ');
                var TotalFare =SearchList[i].TotalFare;
                if (SearchList[i].source == 'TBOAir') {
                    TotalFare = Math.ceil(TotalFare);
                }
                TotalFare = TotalFare.toFixed(SearchList[i].AgentDecimal).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                $('.list-group').append('<li id="List' + i + '">' + orgqueueTemplate + '</li>');
                $('#AgentName').attr('id', 'AgentName-' + i);
                $('#AgentName-' + i).append('<input id="AgentName-' + i + '" type="button" class="btn-link" value="' + SearchList[i].BookingAgentName + '" onclick="showAgent(' + i + ')" \>');
                $('#Status').attr('id', 'Status-' + i);
                $('#Status-' + i).append(' <label class="bold">' + SearchList[i].bookingstatus + '</label>');
                $('#PNR').attr('id', 'PNR-' + i);
                $('#PNR-' + i).append(' <label class="pull-left"><strong>PNR-<b><span id="lblPNR" style="font-weight: bold; color:Green">' + SearchList[i].pnr + '</span></b><span>&nbsp;</span><b></b></strong></label>');
                $('#FlightDetails').attr('id', 'FlightDetails-' + i);
                //$('#FlightDetails-' + i).append(' <label>' + SearchList[i].airlineCOde + '</label>|<label>' + SearchList[i].FlightNum + '</label>|<label>' + SearchList[i].AdultCount + ' </label> ADT');
                $('#FlightDetails-' + i).append(' <label>' + SearchList[i].airlineCOde + '</label>|<label>' + SearchList[i].FlightNum + '</label>');
                $('#RoutingTrip').attr('id', 'RoutingTrip-' + i);
                $('#RoutingTrip-' + i).append(' <label class="bold">' + SearchList[i].routing + '</label>');
                $('#TotalPrice').attr('id', 'TotalPrice-' + i);
                $('#TotalPrice-' + i).append('Total Price:<label class="bold">' + SearchList[i].AgentCurrency + ' ' + TotalFare + '</label>');
                $('#BookedOn').attr('id', 'BookedOn-' + i);
                $('#BookedOn-' + i).append(' Booked:<label class="bold">' + day + '</label>(<label class="bold">' + SearchList[i].createdOn + '</label>)');
                $('#PaxName').attr('id', 'PaxName-' + i);
                $('#PaxName-' + i).append('  Pax Name:<label class="bold">' + SearchList[i].leadpaxName + '</label>');
                $('#TravelDate').attr('id', 'TravelDate-' + i);
                $('#TravelDate-' + i).append('  Travel Date:<label class="bold">' + SearchList[i].travelDate + '</label>');
                $('#Location').attr('id', 'Location-' + i);
                $('#Location-' + i).append('  Location:<label class="bold">' + SearchList[i].locationName + '</label>');
                $('#BookedBy').attr('id', 'BookedBy-' + i);
                var Bookedon;
                if (SearchList[i].OnBehalf != null) {
                    Bookedon = SearchList[i].UserName + " (" + SearchList[i].OnBehalf + ")";
                }
                else {
                    Bookedon = SearchList[i].UserName;
                }
                $('#BookedBy-' + i).append('  Booked By:<label class="bold">' +Bookedon + '</label>');
              if ($('#<%=hdfLAgentType.ClientID%>').val() == "BaseAgent") {
                 $('#List' + i).find('#Source').attr('id', 'Source-' + i);
                $('#Source-' + i).append(' <label class="bold">Source :</label> <label class="bold">' + SearchList[i].source + '</label>');
       }          
                 $('#List' + i).find('#OpenButton').attr('id', 'OpenButton-' + i);
                $('#OpenButton-' + i).append('<a id="Open-' + i + '" class="button" href="ViewBookingForTicket.aspx?bookingId=' + SearchList[i].bookingId + '&fromAgent=true" target="_blank">Open</a>');
                 if (SearchList[i].bookingstatus == "Hold" || SearchList[i].bookingstatus == "Ready" || SearchList[i].bookingstatus == "InProgress") {
                    $('#OpenButton-' + i).append('<input id="ReleaseSeat-' + i + '" type="button" class="button" value="Release Seat" onclick="ReleaseSeat(\'' + SearchList[i].pnr + '\',' + i + ',' + SearchList[i].bookingId + ')"  \>');

                }
                else{
                $('#OpenButton-' + i).append('<input id="ViewInvoice-' + i + '" type="button" class="button" value="View Invoice" onclick="ViewInvoice(' + SearchList[i].flightId + ',' + SearchList[i].AgentId + ')"  \>');

                }
                 if (SearchList[i].RoutingTripID != null) {
                    $('#List' + i).find('#PNR').attr('id', 'PNR-' + i);
                    $('#PNR-' + i).append(' <label class="bold">Routing Trip Id-</label> <label class="bold" style="font-weight: bold; color:Green">' + SearchList[i].RoutingTripID + '</label>');
                }
                if (SearchList[i].TripId != null) {
                    $('#List' + i).find('#CorpBookingCode').attr('id', 'CorpBookingCode-' + i);
                    $('#CorpBookingCode-' + i).append(' <label class="bold">Corporate Booking Code :</label> <label class="bold">' + SearchList[i].TripId + '</label>');
                }
                if (SearchList[i].TransType == "B2C" || SearchList[i].paymentMode == "CreditCard") {
                    $('#List' + i).find('#divlnkPayment').attr('id', 'divlnkPayment-' + i);
                    $('#divlnkPayment-' + i).append('<input id="lnkPayment" type="button" class="btn-link" value="Payment Information" onclick="ViewPaymentInfo(' + SearchList[i].bookingId + ')" \>');
                }
                if ((CurrentDate <= CancelDate && SearchList[i].bookingstatus == "Ticketed") || (SearchList[i].AgentCountry == "3" && SearchList[i].bookingstatus == "Ticketed")) {
                    if (SearchList[i].source == "Air Arabia") {
                        $('#List' + i).find("#ddlRequestChange").attr('id', 'ddlRequestChange-' + i);
                        $('#ddlRequestChange-' + i).append('<div class="col-md-6 text-right"><label>Request Changes : </label></div><div class="col-md-6"><select id="ddlRequest' + i + '" class="form-control"><option value="0">Select</option><option value="2">Modification</option><option value="3">Refund</option></select></div>');
                    }
                    else {
                        if (CreatedOn == CurrentDate) {
                            $('#List' + i).find("#ddlRequestChange").attr('id', 'ddlRequestChange-' + i);
                            $('#ddlRequestChange-' + i).append('<div class="col-md-6 text-right"><label>Request Changes : </label></div><div class="col-md-6"><select id="ddlRequest' + i + '" class="form-control"><option value="0">Select</option><option value="1">Void</option><option value="2">Modification</option></select></div>');
                        }
                        else {
                            $('#List' + i).find("#ddlRequestChange").attr('id', 'ddlRequestChange-' + i);
                            $('#ddlRequestChange-' + i).append('<div class="col-md-6 text-right"><label>Request Changes : </label></div><div class="col-md-6"><select id="ddlRequest' + i + '" class="form-control"><option value="0">Select</option><option value="2">Modification</option><option value="3">Refund</option></select></div>');
                        }
                    }

                    $('#List' + i).find('#ReMarks').attr('id', 'ReMarks-' + i);
                    $('#ReMarks-' + i).append(' <textarea id="RequestRemarks-' + i + '" class="form-control"  cols="20" rows="2" style="height:auto" placeholder="Enter ReMarks Here"></textarea>');
                    $('#List' + i).find('#ChangeRequestButton').attr('id', 'ChangeRequestButton-' + i);
                    $('#ChangeRequestButton-' + i).append('<input id="btnRequest-' + i + '" type="button" class="button" value="Submit Request" onclick="SubmitRequest(' + SearchList[i].flightId + ',' + i + ')" \>');
                }
                else {
                    if (SearchList[i].bookingstatus == "RefundInProgress") {
                        $('#OpenButton-' + i).append('<div class="clearfix"></div><label style="background-color:#9AF1A2;font-style:italic;text-decoration-color:black;font-size:10pt;" class="float-left mt-2">Requested for Refund<label>');
                    }
                    else if (SearchList[i].bookingstatus == "ModificationInProgress") {
                        $('#OpenButton-' + i).append('<div class="clearfix"></div><label style="background-color:#9AF1A2;font-style:italic;text-decoration-color:black;font-size:10pt;" class="float-left mt-2">Requested for Modification<label>');
                    }
                    else if (SearchList[i].bookingstatus == "VoidInProgress") {
                        $('#OpenButton-' + i).append('<div class="clearfix"></div><label style="background-color:#9AF1A2;font-style:italic;text-decoration-color:black;font-size:10pt;" class="float-left mt-2">Requested for Void<label>');
                    }
                }
            }
            
        }
        function ViewPaymentInfo(bookingId) {
            $.ajax({
                type: "POST",
                url:"Flightqueue.aspx/GetPaymentInfo",
                dataType: "json",
                contentType: "application/json",
                data: "{'bookingId':'" + bookingId + "'}",
                success: function (data) {
                    if (data.d.length > 0) {
                        var PaymentList = data;
                        $('#PaymentInformation').show();
                        $('#Header').show();
                        $('.pay').remove();
                        $('#Detailspay').append('<span class="fleft width-90  margin-right-5 pay" style="width:180px;">' + PaymentList.d[0] + '</span>');
                        $('#Detailspay').append('<span class="fleft width-150  margin-right-5 pay" style="width:130px;">' + PaymentList.d[1] + '</span>');
                        $('#Detailspay').append('<span class="fleft width-90  margin-right-5 pay" style="width:100px;">' + PaymentList.d[2] + '</span>');
                        $('#Detailspay').append('<span class="fleft width-90  margin-right-5 pay" style="width:100px;">' + PaymentList.d[3] + '</span>');
                        $('#Detailspay').append('<span class="fleft width-60px  margin-right-5 pay" style="width:130px;">' + PaymentList.d[4] + '</span>');
                    }
                    else {
                        $('#PaymentInformation').show();
                        $('#Header').hide();
                        $('.pay').remove();
                        $('#Detailspay').append('<span style="padding-left:250px; font-weight:bold;" class="pay">Payment Information Not Available.</span>');
                    }
                }
            });
        }
        function SubmitRequest(FlightId, Id) {        
            if (CancelAirBooking(Id)) {               
                var RequestsId = $('#ddlRequest' + Id).val();
                 var RequestText =$('#ddlRequest' + Id).find('option:selected').text();
                var Obj = {};
                Obj.FlightId = FlightId;
                Obj.Remarks = $('#RequestRemarks-' + Id).val();
                Obj.RequestText = RequestText;
                Obj.RequestId = RequestsId;
                var jsons = JSON.stringify(Obj);
                $.ajax({
                    type: "POST",
                    url: "Flightqueue.aspx/SubmitRequest",
                    dataType: "json",
                    contentType: "application/json",
                    data: jsons,
                    success: function (data) {
            
                        Search();
                    }
                });
            }
            else {
                alert("Please Enter Remarks and Select Change request");
            }
        }
        function CancelAirBooking(Id) {   
            var val = $('#RequestRemarks-' + Id).val();
            var dropdown = $('#ddlRequest' + Id).val();
            if ($('#ddlRequest' + Id).val() == 0) {
                return false;
            }
            else if (val.length <= 0 || val == " ") {
                return false;
            }
            else {
                return true;
            }
        }
        function ViewBookingForTicket(BookingId) {
            var finalurl = "ViewBookingForTicket.aspx?bookingId=" + BookingId + "&fromAgent=true";
            window.location = finalurl;
        }
        function ViewInvoice(FlightId, AgentId) {
            window.open("CreateInvoice.aspx?flightId=" + FlightId + "&agencyId=" + AgentId + "&fromAgent=true", 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
        }
        function showAgent(id) {        
            $('#AgentDetails').show();
            $('#Agent_Name').append(' <label class="bold Agent text-primary">' + SearchList[id].BookingAgentName + '</label>');
            $('#CreditBalance').append('<label class="Agent">Credit Balance :</label> <label class="bold Agent">' + SearchList[id].AgentCurrency + ' ' + SearchList[id].AgentBalance + '</label>');
            $('#Mob').append('<label class="Agent">Mobile No :</label> <label class="bold Agent">' + SearchList[id].AgentphoneNO1 + '</label>');
            $('#Phone').append('<label class="Agent">Phone :</label> <label class="bold Agent">' + SearchList[id].AgentphoneNO2 + '</label>');
            $('#Email').append('<label class="Agent">Email :</label> <label class="bold Agent">' + SearchList[id].AgentEmail + '</label>');
        }
        function AgentModalClose() {
            $('.Agent').remove();
            $('#AgentDetails').hide();
        }
        function PaymentModalClose() {
            $('.pay').remove();
            $('#PaymentInformation').hide();
        }
        var releasingResult = -1;
        var releasingBookingId = -1;
        function ReleaseSeat(pnr, ressultNo, bookingId) {
            //$('.modal').modal('hide');
            if (releasingResult < 0) {
                if (confirm("Are you sure to cancel " + pnr)) {

                    if (window.XMLHttpRequest) {
                        Ajax = new XMLHttpRequest();
                    }
                    else {
                        Ajax = new ActiveXObject('Microsoft.XMLHTTP');
                    }
                    scroll(0, 0);
                    releasingResult = ressultNo;
                    releasingBookingId = bookingId;
                    document.getElementById('MessageBox').innerHTML = "Releasing seats : " + pnr;
                    var page = "ReleaseSeat.aspx";
                    var pars = "pnr=" + pnr;
                    pars += "&bookingId=" + bookingId;
                    //new Ajax.Request(page, { method: 'post', parameters: pars, onComplete: HandleCancel });
                    Ajax.onreadystatechange = HandleCancel;
                    Ajax.open('POST', page);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send(pars);
                }
            }
            else {
                alert("One cancellation already in progress");
            }
        }
        function HandleCancel() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var cancelRes = Ajax.responseText.split("|");
                        document.getElementById('MessageBox').innerHTML = cancelRes[2];
                        if (eval(cancelRes[0]) > 0) {
                            // document.getElementById('Result-' + releasingResult).style.display = "none";
                            $('#ReleaseSeat-' + releasingResult).hide();
                        }
                        releasingResult = -1;
                        releasingBookingId = -1;
                    }
                }
            }
        }
    </script>
    <script>
        var cal1;
        var cal2;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }
        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear(); cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }
            var date2 = cal2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>
    <script type="text/javascript">
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
    </script>
    <iframe id="IShimFrame" style="position: absolute; display: none;"></iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfLAgentType" Value=""></asp:HiddenField>
    <a style="cursor: default; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
        onclick="return ShowHide('divParam');">Hide Param</a>
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        From Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        To Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        Agent:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlAgency"
                            runat="server" class="form-control" onchange="javascript:BindAgents(this.value,'B2B');">
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="form-control" onchange="javascript:BindAgents(this.value,'B2B2B');">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" onchange="javascript:BindAgents(this.value,'');">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Location:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList CssClass="form-control" ID="ddlLocation"
                            runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        Booking Status:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList CssClass="form-control" ID="ddlBookingStatus"
                            runat="server">
                            <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Pax Name:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPaxName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        PNR No.
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPNR" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="TransType" style="display: none;">
                        <div class="col-md-2">
                            <asp:Label ID="lblTransType" runat="server" Text="TransType:"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlTransType" CssClass="form-control" runat="server">
                                <asp:ListItem Selected="True" Value="" Text="--All--"></asp:ListItem>
                                <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                                <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2">
                        Routing Trip Id.
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtRoutingTrip" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        Source:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList AppendDataBoundItems="true" CssClass="form-control" ID="ddlsource"
                            runat="server">
                            <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" runat="server" Text="Search" OnClientClick="javascript:Search();return false;" />
                </div>
            </div>
        </asp:Panel>

    </div>
    <div>
        <label id="NoRecords"><b>No Records Found!</b></label>
    </div>
    <div>
       <%-- AgentDetails PopUp--%>
        <div class="modal" id="AgentDetails">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">AgentDetails</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6" id="Agent_Name">
                                </div>
                                <div class="col-md-6" id="CreditBalance">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" id="Mob">
                                </div>
                                <div class="col-md-6" id="Phone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" id="Email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="AgentModalClose();">Close</button>
                    </div>
                </div>
            </div>
        </div>
     <%--   PaymentInformation PopUp--%>
        <div class="modal" id="PaymentInformation">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Payment Information</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div style='width: 680px;' class="fleft  padding-5 light-gray-bg margin-bottom-5" id="Header">
                                <span class="fleft width-90 margin-right-5" style='width: 180px;'><b>Order Id</b></span>
                                <span class="fleft width-150 margin-right-5" style='width: 130px;'><b>Payment Id</b></span>
                                <span class="fleft width-90 margin-right-5" style='width: 100px;'><b>Amount</b></span>
                                <span class="fleft width-60 margin-right-5" style='width: 100px;'><b>Charges</b></span>
                                <span class="fleft width-60px " style='width: 130px;'><b>Payment Sources</b></span>
                            </div>
                            <div style='width: 680px;' class="fleft  padding-5 light-gray-bg margin-bottom-5" id="Detailspay">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="PaymentModalClose();">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            
            <ul class="list-group">
            </ul>

        </div>
    </div>
    <%--Template To Display Queue--%>
    <div id="templateView" style="display: none;">
        <div class="tbl">
            <div class="col-md-12 padding-0 marbot_10">

                <div class="col-md-4" id="AgentName">
                </div>
                <div class="col-md-4" id="Status">
                </div>
                <div class="col-md-4" id="PNR">
                    <div style="display: none" id="Release-Seat">
                    </div>
                </div>



                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="FlightDetails">
                </div>
                <div class="col-md-4" id="RoutingTrip">
                </div>
                <div class="col-md-4" id="TotalPrice">
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="BookedOn">
                </div>
                <div class="col-md-4">
                    <b></b>

                </div>
                <div class="col-md-4" id="PaxName">
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="TravelDate">
                </div>
                <div class="col-md-4" id="Location">
                </div>
                <div class="col-md-4" id="BookedBy">
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-8" id="Source">
                </div>
                <div class="col-md-4" id="OpenButton">
                </div>
            </div>
            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-4" id="ddlRequestChange">
                </div>
                <div class="col-md-3" id="ReMarks">
                </div>
                <div class="col-md-3" id="ChangeRequestButton">
                </div>
                <div class="col-md-9">
                    <div>
                        <div style="position: relative" id="divlnkPayment">

                            <div id="PaymentInfo" class="visa_PaymentInfo_pop" style="position: absolute; display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                   
                </div>
            </div>
            <div class="col-md-12" id="CorpBookingCode">
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>

    <script type="text/javascript">

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
