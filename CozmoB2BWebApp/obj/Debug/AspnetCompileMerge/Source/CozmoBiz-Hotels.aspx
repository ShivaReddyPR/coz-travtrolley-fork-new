﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
<title>Hotels</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="images/favicon.ico">    	


 <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />

<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />

<link href="css/Default.css" rel="stylesheet" type="text/css" />

<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />

 <link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
        
    
          <!-- Bootstrap Core CSS -->
   <link href="css/bootstrap.min.css" rel="stylesheet" />

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />
    
    <link href="yui/build/fonts/fonts-min.css" rel="stylesheet" type="text/css" />
    
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="BookingPanelStyle.css" />
    
    <link href="<%=Request.Url.Scheme%>://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
    
    <link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
    
   <link href="css/toastr.css" rel="stylesheet" type="text/css" />
  


    
<link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />

<link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />


    
  
</head>
<body>






<div class="topHeader2"> 

<div class="container"> 
<div class="row"> 

<div class="col-md-12"> 
<img src="images/logoc.jpg">   

</div> 

          







</div>
</div>


</div>




<div class="heading_bizz"> 

<div class="container"> 
<h2> Hotels</h2>

</div>

</div>

          
<form runat="server" id="form1"> 


<div class="container"> 

<div class="innerPage2">

<div class="row"> 

<div class="col-12"> 

<p>



We make you feel at home, no matter where you stay in the world. CCTM has exclusive contracts with top business hotels and relationships with boutique properties, so you feel welcome and especially cared for. Whether it is negotiating on behalf of our clients to secure a favorable rate or easing claim management via expense cards, CCTM is the trusted partner you need to manage corporate bookings and accommodation. <br /> <br /> 

We take customer satisfaction very seriously and work with our network partners, in India, the Gulf region, and across the globe, to extend a superior quality of service. From remembering your dietary preferences, accommodating a special request, to organizing group stay during a conference, offsite or sales meet, CCTM goes the extra mile and includes thoughtful touches to make your visit memorable. 



</p>



</div>



</div>







</div>


</div>




</form>













<div class="footer_sml_bizz"> 
 
 
  Copyright 2019 Cozmo Travel World, India. All Rights Reserved. 


 
 
 </div>





    <script type="text/javascript" src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/Scripts/Menu/query2.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    
    
     <script type="text/javascript" src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/build/js/main.min.js"></script>
     
	 
	 
	 <script src="<%=Request.Url.Scheme%>://10.10.1.119/CozmoB2BWebAppTMP/scripts/select2.min.js" type="text/javascript"></script>
     
     
     
     
     <script type="text/javascript">
         $(document).ready(function () {

             $('select').select2();


         });
    
</script>




<script>


    //on scroll up/down show/hide top icon

    $('body').prepend('<a href="#" class="back-to-top"><i class="icon-expand_less"></i></a>');

    var amountScrolled = 300;

    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('slow');
        } else {
            $('a.back-to-top').fadeOut('slow');
        }
    });


    //on click scroll to top 

    $('a.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });


</script>




<script>



    $('#Products2,#Partners2').click(function () {
        var Linkid = $(this).attr('href'),
                            Linkid = Linkid.replace('#', '');
        $('html, body').animate({
            scrollTop: $('[name="' + Linkid + '"]').offset().top
        }, 500);


        //        alert("Hello! I am an alert box!");   



    })

       

</script>



 
    <link href="build/css/owl-carousel/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href="build/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
 

</body>
</html>





 


<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>


