﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="B2CVisaSettings" Title="B2CVisaSettings" Codebehind="B2CVisaSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">

     
    </script>
<asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label>
<a href="B2CSettings.aspx">Back To B2C Settings</a>
    <asp:MultiView ID="mvSettings" runat="server" ActiveViewIndex="0">
        <asp:View ID="VisaView" runat="server">
          
           
                <div class="ns-h3">Visa Settings</div>
                
                
               <div class="bg_white paramcon pad_10 bor_gray">
               
               
                 <div class="col-md-12 padding-0 marbot_10">   
               
                <div class="col-md-2"> <asp:Label ID="Label1" runat="server" Text="Email :"></asp:Label></div>
    <div class="col-md-2">
    
      <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                               <asp:RegularExpressionValidator ID="emailValid" runat="server" 
                                    ControlToValidate = "txtEmail" Display ="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
    ErrorMessage="*Please Enter Valid Email ID!"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="emailidRequired" runat="server" ControlToValidate="txtemail" ErrorMessage="*Email ID Is Required!"></asp:RequiredFieldValidator>
    
    
     </div>
     
     
    
    
    <div class="col-md-2"> <asp:Label ID="Label2" runat="server" Text="Waiting Text :"></asp:Label></div>
     <div class="col-md-2"> <asp:TextBox ID="txtWaitingText" CssClass="form-control" runat="server"></asp:TextBox></div>
     
     
     <div class="col-md-2"><asp:Label ID="Label3" runat="server" Text="Waiting Logo :"></asp:Label> </div>
         <div class="col-md-2"> <asp:FileUpload ID="fuWaitingLogo" runat="server" /></div>

<div class="clearfix"></div>
    </div>







  <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="Label4" runat="server" Text="Google Script Type :"></asp:Label></div>
    <div class="col-md-2"> <asp:RadioButton ID="rbtnGoogleAnalytics" runat="server" GroupName="Google" Text="Google Analytics" /></div>
    <div class="col-md-2"> <asp:RadioButton ID="rbtnGooglePPC" runat="server" GroupName="Google" Text="Google PPC" /></div>
    


<div class="clearfix"></div>
    </div>
               
                
  <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="Label5" runat="server" Text="Google Script :"></asp:Label> </div>
    <div class="col-md-6"> <asp:TextBox ID="txtGoogleScript" TextMode="MultiLine" Rows="5" Width="100%" runat="server"></asp:TextBox></div>


<div class="clearfix"></div>
    </div>
                
             
             
                  <div class="col-md-12 marbot_10">   
       
                 
                 
                  <asp:Button ID="btnSaveSettings" runat="server" CssClass="button pull-right" Text="Save" OnClick="btnSaveSettings_Click" />
                  
                  
                  <asp:Button ID="btnClearSettings" runat="server" CssClass="button pull-right" Text="Clear" CausesValidation="false" OnClick="btnClearSettings_Click" />
                  
                  
                  
                            <asp:Button ID="btnVisaViewNext" runat="server" Text="Next" CssClass="button pull-right" CausesValidation="false" OnClick="btnNext_Click" />
                 
                 
                 
                 
                  
                  </div>   
                  
                  
                
                    
                    
                    
                
                    
                    
                    <div class="clearfix"></div>
                </div>
            
        </asp:View>
       
        <asp:View ID="MarkupView" runat="server">
           
               
                <div class="ns-h3">
                   Payment Gateway</div>
                <div class="bg_white paramcon pad_10 bor_gray">
                  
                  <div id="errMess" runat="server" class="error_module" style="display: none;">
                            <div id="errorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5"></div>
                            </div>
                      
                       <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-3"><asp:CheckBox Width="110" ID="chkENBD" runat="server" Text="ENBD" Checked="true" />
       <asp:Label ID="Label9" runat="server" Text="Apply"></asp:Label>
                                <asp:TextBox ID="txtENBDCharges" runat="server" Width="50px"></asp:TextBox>
 
 
  </div>
    <div class="col-md-8"><asp:Label ID="Label10" runat="server" Text=" % of Credit Card charge will be charged extra per booking to Customer"></asp:Label> </div>


<div class="clearfix"></div>
    </div>




 <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-3"> <asp:CheckBox Width="100" ID="chkCCA" runat="server" Text="CC Avenue" />    <asp:Label ID="Label11" runat="server" Text="Apply"></asp:Label>
                                <asp:TextBox ID="txtCCACharges" runat="server" Width="50px"></asp:TextBox> </div>
   
    <div class="col-md-8"> <asp:Label ID="Label12" runat="server" Text=" % of Credit Card charge will be charged extra per booking to Customer"></asp:Label></div>


<div class="clearfix"></div>
    </div>




     <div class="col-md-12 marbot_10">    
     
     
     <asp:Button ID="btnSaveMarkup" runat="server" CssClass="button pull-right" Text="Save" OnClick="btnSaveMarkup_Click" />
     
     
     <asp:Button ID="btnClearMarkup" runat="server" CssClass="button pull-right" Text="Clear" OnClick="btnClearMarkup_Click" />
     
     <asp:Button ID="btnMarkupPrev" runat="server" Text="Previous" CssClass="button pull-right" OnClick="btnPrev_Click" />
     
     
     
     </div>

                    
                   
                   
             
                    
                    
               <div class="clearfix"></div>
            </div>
        </asp:View>
       <!--@@@@ FAQ and Fraud View -->
    </asp:MultiView>
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

