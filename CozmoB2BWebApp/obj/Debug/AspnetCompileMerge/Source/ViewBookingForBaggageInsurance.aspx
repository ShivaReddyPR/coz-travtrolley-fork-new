﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ViewBookingForBaggageInsurance.aspx.cs" Inherits="CozmoB2BWebApp.ViewBookingForBaggageInsuranceGUI" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script language="javascript">


        function ShowVoucher() {


            var url = 'BaggageInsuranceVoucher.aspx?BID=<%=HeaderId %>';
            //        var wnd = window.open(url, 'Voucher', 'width:600lpx;height:600px;');
            var wnd = window.open(url, 'Voucher', 'width:600,height:600,toolbar = yes, location = no, directories = no, status = yes, menubar = no, scrollbars = yes, resizable = yes, left = 50, top = 50');


        }

    </script>

    
    <div id="emailBlock" class="showmsg" style="position: absolute; display: none; left: 400px; top: 100px; width: 300px; z-index:1000">
                <div class="showMsgHeading">Enter Your Email Address</div>

                <a style="position: absolute; right: 5px; top: 3px;" onclick="return HidePopUp();" href="#" class="closex">X</a>
                <div class="padding-5">
                    <div style="background: #fff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td height="40" align="center">
                                    <b style="display: none; color: Red" id="err"></b>
                                    <asp:TextBox Style="border: solid 1px #ccc; width: 90%; padding: 2px;" ID="txtEmailId" runat="server" CausesValidation="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td height="40" align="center">
                                    <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate();" OnClick="btnEmailVoucher_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
   
    <%--<form id="form1" runat="server">--%>
    <div id="TestDiv" runat="server"> 
        <div class="" id="Div1" runat="server">

        <div class="ns-h3">
            Baggage Insurance Header 

            <label class=" pull-right marright_10"><a class="fcol_fff" href="#" onclick="return ShowPopUp(this.id);" style="display: block"> Email</a> </label>
            <%-- <asp:LinkButton ID="btnEmail" class="pull-right marright_10 fcol_fff"  runat="server" Text="Email" OnClientClick="return ShowPopUp(this.id);" />--%>

            <label class=" pull-right marright_10"><a class="fcol_fff" href="#" onclick="ShowVoucher()" style="display: block">Show Voucher</a> </label>

            <label class=" pull-right marright_10 hidden-xs"><a href="BaggageInsuranceQueue.aspx" class="fcol_fff" style="display: block">Insurance Queue</a> </label>

            <div class="clearfix"></div>
        </div>
        

        <div class=" bor_gray bg_white padding-5 marbot_10">

            <%if (header != null)
                { %>

            <div class="row">

                <div class="col-md-2">
                    <label>PNR: </label>
                </div>
                <div class="col-md-2"><b><%=header.PNR%> </b></div>

                <div class="col-md-2">
                    <label>Total Amount: </label>
                </div>
                <div class="col-md-2"><b><%=amount.ToString("N" + agent.DecimalValue)%> </b></div>

                <div class="col-md-2">
                    <label>Policy no:: </label>
                </div>
                <div class="col-md-2"><b><%=header.Policy%></b></div>

                <div class="clearfix"></div>
            </div>


        </div>


        <%} %>

        <div class="clearfix"></div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <% if (dsPaxDetails != null && dsPaxDetails.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < dsPaxDetails.Tables[1].Rows.Count; i++)
                    {%>
            <div id="accordion" class="panel-group">
                <div class="panel panel-default">


                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseOne<%=i %>" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle btn-block collapsed" aria-expanded="false">
                                <span class="glyphicon pull-right glyphicon-plus"></span>

                                <%=dsPaxDetails.Tables[1].Rows[i]["PAX_FIRST_NAME"] + " " + dsPaxDetails.Tables[1].Rows[i]["PAX_LAST_NAME"]%>
          
                            </a>
                        </h4>
                        
                  
                    </div>

                    <div class="panel-collapse collapse" id="collapseOne<%=i %>" aria-expanded="false" style="height: 0px;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-3 col-xs-6"><b>Title:</b><%=dsPaxDetails.Tables[1].Rows[i]["PAX_TTILE"]%></div>
                                <div class="col-md-3 col-xs-6">&nbsp;</div>
                                <div class="col-md-3 col-xs-6">&nbsp;</div>
                                <div class="col-md-3 col-xs-6"><b>Pax Email:</b><%=dsPaxDetails.Tables[1].Rows[i]["PAX_EMAIL"]%></div>
                                <div class="col-md-3 col-xs-6">&nbsp;</div>
                                <div class="col-md-3 col-xs-6"><b>Pax Phone:</b><%=dsPaxDetails.Tables[1].Rows[i]["PAX_PHONE_NO"]%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax PNR:</b><%=dsPaxDetails.Tables[1].Rows[i]["PAX_PNR"]%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax Ticket No:</b><%=dsPaxDetails.Tables[1].Rows[i]["PAX_TICKET_NO"]%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax Policy No:</b><%=dsPaxDetails.Tables[1].Rows[i]["PAX_POLICY_NO"]%></div>
                                <% if (!string.IsNullOrEmpty((dsPaxDetails.Tables[1].Rows[i]["pax_track_number"]).ToString()))
                                    {%>
                                <div class="col-md-3 col-xs-6"><b>Pax Track Number:</b><%=dsPaxDetails.Tables[1].Rows[i]["pax_track_number"]%></div>
                                <% }%>
                            </div>

                            <br />
                            <br />
                         

                        </div>



                    </div>
                </div>


            </div>
            <%}
                }%>

            <div id="divPlan" runat="server">
            </div>
        </div>
    </div>
    </div>
    
    <div id="divVoucher" runat="server" style="display:none">
 <style>@media only screen{html{min-height:100%;background:#f3f3f3}}@media only screen and (max-width:716px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}}@media only screen and (max-width:716px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .columns{padding-left:0!important;padding-right:0!important}th.small-2{display:inline-block!important;width:16.66667%!important}th.small-6{display:inline-block!important;width:50%!important}th.small-12{display:inline-block!important;width:100%!important}.columns th.small-12{display:block!important;width:100%!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}}</style></head><body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f3f3f3!important;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important"><span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span><table class="body" style="Margin:0;background:#f3f3f3!important;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><center data-parsed="" style="min-width:700px;width:100%"><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table align="center" class="container float-center" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:700px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:509px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><strong>Welcome to Cozmo Travel World .</strong></th></tr></table></th><th class="small-12 large-3 columns last" right="" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:8px;padding-right:16px;text-align:left;width:159px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><p class="text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:right"> <%=DateTime.Now.ToString("dd-MMM-yyyy") %></p></th></tr></table></th></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">COZMO is world’s premier assistance company, offering best, and superior assist services to people everywhere</th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><strong>Passenger Name:</strong>  <%=BaggageInsuranceVoucherList[0].PaxName%></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table><table align="center" class="container" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:700px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="b2b-baggage-table table-style" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><strong>COZMO PNR</strong> : <%=BaggageInsuranceVoucherList[0].PNR %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><strong>Plan Type</strong> : <%=BaggageInsuranceVoucherList[0].PlanDescription %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word">Policy NO:<%=BaggageInsuranceVoucherList[0].PolicyNo %></td></tr><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><strong>Plan Name</strong> :<%=BaggageInsuranceVoucherList[0].PlanName %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><strong>Tracking Commitment Period</strong> : 99Hrs</td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"></td></tr><tr style="padding:0;text-align:left;vertical-align:top"><td colspan="3" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left"><small style="color:#cacaca;font-size:80%">(COZMO is committed to trace the luggage with in specified hours mentioned under Tracking Commitment period. Baggage Tracking Request (BTR) reporting within 10 (ten) hours of aircraft's actual arrival time. Non reporting within above time-lines will disqualify the BTR service.)</small></p></td></tr><tr style="padding:0;text-align:left;vertical-align:top"><td colspan="3" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><strong>Tracking Lapse Commitment : INR 50,000/- </strong>per checked in undelivered baggage(Limited to Max 2 bags per member)</td></tr></tbody></table></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table align="center" class="container" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:700px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="b2b-baggage-table" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>Sno</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>Passenger Name</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>CRS/GDS or Airline PNR</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>Ticket No</strong></th></tr>
 <%for (int i = 0; i < BaggageInsuranceVoucherList.Count; i++)
                       {%>
<tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=(i+1) %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=BaggageInsuranceVoucherList[i].PaxName %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=BaggageInsuranceVoucherList[i].PNR %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=BaggageInsuranceVoucherList[i].TicketNo %></td></tr>
 <% } %>
</table></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table><table align="center" class="container" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:700px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">Membership fees charged is valid for specific travel duration only.</th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table><table align="center" class="container" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:700px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="b2b-baggage-table" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>Base Fees</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>CGST @ 9%	</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>SGST @ 9%</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>IGST @ 18%	</strong></th><th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top"><strong>Total Fees</strong></th></tr><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=baseFare %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=CGSTTaxValue %></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=SGSTTaxValue%></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=IGSTTaxValue%></td><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word"><%=TotalAmount%></td></tr></table></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><!-- prevent Gmail on iOS font size manipulation --><div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body></html>
  </div>
    

    <script>


        $('.collapse').on('shown.bs.collapse', function () {
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function () {
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });

        function ShowPopUp(id) {

           document.getElementById('<%=txtEmailId.ClientID %>').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            document.getElementById('emailBlock').style.left = (530) + 'px';
            document.getElementById('emailBlock').style.top = (400) + 'px';
            return false;
        }

        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }

        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>


    <style>
        .glyphicon-plus, .glyphicon-minus {
            font-size: 10px;
            margin-top: 4px;
            font-weight: normal;
        }
    </style>

    <%-- </form>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>



