﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CreateFleetInvoice" Codebehind="CreateFleetInvoice.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fleet Invoice</title>
 <link rel="stylesheet" href="css/main-style.css" />
      <script type="text/javascript">
          var Ajax;

          if (window.XMLHttpRequest) {
              Ajax = new XMLHttpRequest();
          }
          else {
              Ajax = new ActiveXObject('Microsoft.XMLHTTP');
          }
          function printPage() {
              document.getElementById('btnPrint').style.display = "none";
              window.print();
              setTimeout('showButtons()', 1000);
          }
          function showButtons() {
              document.getElementById('btnPrint').style.display = "block";
          }

    

    </script> 
         
   

</head>
<body>
    <form id="form1" runat="server">
   <div>
    <div style="position:absolute; right:700px; top:530px; display:none"  id="emailBlock">
                    
                    
                    <div style="border: solid 4px #ccc; width:200px" class="ShowMsgDiv"> 
                    <div class="showMsgHeading"> Enter Email Address <a style=" float:right; padding-right:10px" class="closex" href="javascript:HideEmailDiv()">X</a></div>
                    
                           <div style=" padding:10px">
                                    <input style=" width:100%; border: solid 1px #7F9DB9" id="addressBox" name="" type="text" /></div>
                               <div style=" padding-left:10px; padding-bottom:10px">
                                    <input onclick="SendMail()" type="button" value="Send mail" />
                                    
                                    </div>
                                    
                                    
                                    
                    </div>
                    

</div>
<div>

<input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 








<table id="tblInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
    <table class="hotlinvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" align="left"> 
    <asp:Image ID="imgLogo" Width="80px" Height="40px" runat="server" AlternateText="AgentLogo" ImageUrl="" />
    </td>
    <td width="33%" align="center"><h1 style=" font-size:26px">Invoice</h1></td>
    <td width="33%" align="right"><input style="width: 100px;" id="btnPrint" onclick="return printPage();" type="button"
                        value="Print Invoice" /></td>
  </tr>
</table>
    </td>
  </tr>
 
 
  <tr>
    <td colspan="2">
    <table class="hotlinvoice" width="100%" style="font-family: 'Arial'">
      <tbody>
                        <tr>
                            <td width="50%" style="vertical-align: top;">
                                <table width="100%" style="font-size: 12px;font-family: 'Arial'">
                                    <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Client Details</h3>                                            </td>
                                        </tr>
                                     <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Name: </b></td>
                                            <td style="vertical-align: top;"> <% = agency.Name %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Code: </b></td>
                                            <td style="vertical-align: top;"> <% = agency.Code %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Address: </b></td>
                                            <td style="vertical-align: top;"><% = agencyAddress %></td>
                                        </tr>
                                            <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>TelePhone No: </b></td>
                                                <td style="vertical-align: top;">
                                                    <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                        {%>
                                                    Phone:
                                                    <% = agency.Phone1%>
                                                    <%  }
                                                        else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                        { %>
                                                    Phone:
                                                    <% = agency.Phone2%>
                                                    <%  } %>
                                                    <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                        { %>
                                                    Fax:
                                                    <% = agency.Fax %>
                                                    <%  } %>                                                </td>
                                        </tr>
                                    </tbody>
                                </table>                            </td>
                          <td width="50%" style="vertical-align: top;">
                                <table width="90%" align="right" style="font-size: 12px;font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px; font-family: 'Arial';">Invoice Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Invoice No: </b></td>
                                            <td style="vertical-align: top;"><%=invoice.CompleteInvoiceNumber%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Invoice Date: </b></td>
                                            <td style="vertical-align: top;"><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Confirmation No: </b></td>
                                            <td style="vertical-align: top;"><% = confNo %></td>
                                        </tr>
                                            <tr>
                                             <%  
                                                 int bookingID = 0;
                                                 bookingID = BookingDetail.GetBookingIdByProductId(fleetId, ProductType.Car); 
                                            %>
                                            <td style="vertical-align: top;width: 120px;"><b>CozmoVocher No: </b></td>
                                            <td style="vertical-align: top;">HTL-CT-<%=bookingID%></td>
                                        </tr>
                                    </tbody>
                                </table>                          </td>
                        </tr>
                    </tbody>
                </table>
     
     </td>
     </tr>
   <tr>
   <td colspan="2">
     <table class="hotlinvoice" width="100%" style="font-family: 'Arial'">
      <tbody>
                        <tr>
                            <td width="50%" style="vertical-align: top;"><table width="100%" style="font-size: 12px;font-family: 'Arial'">
                              <tbody>
                                <tr>
                                  <td colspan="4" style="vertical-align: top;"><h3 style="font-size: 16px;font-family: 'Arial';">Booking Details</h3></td>
                                </tr>
                                <tr>
                                  <td style="vertical-align: top;width: 91px;"><b>Booking Date: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.CreatedOn.ToString("dd MMM yy HH:mm")%></td>
                                </tr>
                                <tr>
                                  <td style="vertical-align: top;width: 91px;"><b>Pickup Date: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.FromDate.ToString("dd MMM yy HH:mm") %></td>
                                  <td style="vertical-align: top;width: 91px;"><b>Drop Date: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.ToDate.ToString("dd MMM yy HH:mm")%></td>
                                </tr>
                                <tr>
                                  <%  
                                      System.TimeSpan diffResult = itinerary.ToDate.Subtract(itinerary.FromDate);
                                            %>
                                  <td style="vertical-align: top;width: 100px;"><b>No. of Day(s): </b></td>
                                  <td style="vertical-align: top;"><%= diffResult.Days%></td>
                                </tr>
                              </tbody>
                            </table></td>
                            <td width="50%" style="vertical-align: top;">
                                <table width="90%" align="right" style="font-size: 12px;font-family: 'Arial'">
      <tbody>
                                        <tr>
                                            <td style="vertical-align: top;" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Fleet Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Fleet Name: </b></td>
                                            <td style="vertical-align: top;"> <%=itinerary.FleetName%></td>
                                        </tr>
                                       
                                           <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pickup Location: </b></td>
                                            <td style="vertical-align: top;">  <%=itinerary.FromLocation %></td>
                                        </tr>
                                        <%if (!string.IsNullOrEmpty(itinerary.ToLocation))
                                          {%>
                                         <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Dropup Location: </b></td>
                                            <td style="vertical-align: top;"> <%=itinerary.ToLocation%></td>
                                        </tr>
                                        <%} %>
                                  </tbody>
                                </table>                          </td>
                        </tr>
                    </tbody>
                </table> 
       </td>
  </tr>
  
  
  
  <tr>
    <td valign="top">
    
    <table class="hotlinvoice" style="font-size: 12px;font-family: 'Arial'" width="100%">
                      <tbody>
                                        <tr>
                                            <td width="50%" style="vertical-align: top;">
                                                <%--<h3 style="font-size: 16px; font-family: 'Arial';">Lead Passenger Details</h3>                                            --%>
                                                <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4">
                                                                <h3 style="font-size: 16px; font-family: 'Arial';">
                                                                    Lead Passenger Details</h3>
                                                            </td>
                                                            </tr>
                                        
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pax Name: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.PassengerInfo[0].FirstName %> <%= itinerary.PassengerInfo[0].LastName %>
                                            </td>
                                             </tr>
                                               <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pax Email: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.PassengerInfo[0].Email %>
                                            </td>
                                             </tr>
                                              <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pax MobileNo: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.PassengerInfo[0].MobileNo %>
                                            </td>
                                             </tr>
                                             </tbody>
                                             </table></td>
                                             <td width="50%" style="vertical-align: top;">
                                             <table class="hotlinvoice" border="0" width="90%" align="right" style="font-size: 12px;font-family: 'Arial'">
                            <tbody>
                              <% decimal totalPrice = 0;
                                           PriceAccounts price = new PriceAccounts();
                                           price =itinerary.Price;
                                           decimal rateofExchange = price.RateOfExchange;
                                           decimal markup = 0, addlMarkup = 0, discount = 0, b2cMarkup = 0;

                                           string symbol = Util.GetCurrencySymbol(price.Currency);


                                           if (itinerary.Price.AccPriceType == PriceType.NetFare)
                                           {
                                               totalPrice = totalPrice + (itinerary.Price.NetFare+itinerary.Price.Tax);

                                               markup += itinerary.Price.Markup;
                                               addlMarkup = itinerary.Price.AsvAmount;
                                               b2cMarkup = itinerary.Price.B2CMarkup;
                                               discount = itinerary.Price.Discount;
                                           }
                                  %>
                              <tr>
                                <td style="vertical-align: top; width:50%;" colspan="2"><h3 style="font-size: 16px;font-family: 'Arial';">Rate Details</h3></td>
                              </tr>
                                <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Net Amount:</b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <% if (Settings.LoginInfo.AgentId <= 1)
                                           {%>
                                           <%=((totalPrice) ).ToString("N"+agency.DecimalValue)%>
                                           <%} 
                                           else{%>
                                           <%=((totalPrice+markup+b2cMarkup-discount) ).ToString("N"+agency.DecimalValue)%>
                                           <%} %>
                                        
                                    </td>
                                </tr>
                              
                                <%if (Settings.LoginInfo.AgentId == 1)
                                  { %>
                                    <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Markup: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                       
                                        <%=((markup+b2cMarkup) ).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                    </tr>
                                     <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Discount: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=((discount) ).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                     </tr>
                                    <%} %>
                               
                                <%if (addlMarkup > 0)
                                  {%>
                                <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Addl Markup: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=(addlMarkup).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                </tr>
                                <%} %>
                              <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Gross Amount:  </b></td>
                                <td style="vertical-align: top;"><%=Convert.ToDouble(Math.Ceiling(totalPrice + markup + b2cMarkup - discount) + addlMarkup).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                              <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Total Amount: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency%> <%=Math.Ceiling(Math.Ceiling(totalPrice + markup + b2cMarkup - discount) + addlMarkup).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                            </tbody>
                          </table>
                          </td>
                          </tr>
                                    </tbody>
                                </table>
                                
                                
                                </td>
                                </tr>
                                <tr>
   
    
    
    <td colspan="2">
    
    <table border="0" class="hotlinvoice" width="30%" style="font-family: 'Arial'">
<tr>
                 <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                 <td style="vertical-align: top; width: 100px;">
                     <b>invoiced By: </b>
                 </td>
                 <td style="vertical-align: top;"  align="left">
                     Cozmo Travel
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Created By: </b>
                 </td>
                 <td style="vertical-align: top;" align="left">
                     <%= member.FirstName + " " + member.LastName%>
                 </td>
             </tr>
             <tr>
             <td style="vertical-align: top; width: 100px;">
             <b>Location: </b>
             </td>
             <td style="vertical-align: top;" align="left">
             <%=location.Name %>
             </td>
             </tr>
             <tr>
             <td style="width:130px">
             <div class="width-100 fleft text-right" style="display:none"><img src="images/Email1.gif"  />
           <a href="javascript:ShowEmailDivD()">Email Invoice</a>
                                     
        
                       
          </div>
          </td>
          <td style="width:160px">
          <span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;"></span> 
              </td>
                    <td>
                        <div class="email-message-parent" style="display:none;" id="emailSent">
                    <span  style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                    </span>
                    
           </div>
             </td>
             </tr>
             </table>
     </td>
     <td></td>
     </tr>
         </table>
    
</div>

    </form>
</body>
</html>
