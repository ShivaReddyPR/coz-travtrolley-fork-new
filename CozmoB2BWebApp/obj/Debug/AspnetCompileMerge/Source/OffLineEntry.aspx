﻿<%@ Page
    Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="OffLineEntry.aspx.cs" Inherits="OffLineEntry" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>

<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <asp:UpdatePanel ID="uppanel1" runat="server">
        <ContentTemplate>
            <%--<script src="Scripts/OffLineEntryCities.js" type="text/javascript" defer="defer"></script>--%>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

            <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

            <script type="text/javascript" src="yui/build/event/event-min.js"></script>

            <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

            <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

            <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

            <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

            <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>


            <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

            <script src="yui/build/container/container-min.js" type="text/javascript"></script>

            <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
            <%--<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>--%>
            <style>
                .sales_container select,
                input {
                    padding-left: 4px;
                }

                .main_panel {
                    width: 100%;
                    /*			height: 768px;*/
                    display: block;
                    position: relative;
                    margin: 0 auto;
                    overflow: hidden;
                }

                .main_panel_wrap {
                    padding: 5px;
                }

                .show_cont {
                    overflow-y: auto;
                }

                .left_col {
                    background: #89a0c0;
                    padding: 0px;
                    border-right: solid 1px #ccc;
                    color: #fff;
                }

                    .left_col label {
                        margin-bottom: 0px;
                        color: #fff;
                        font-size: 90%;
                    }

                    .left_col a {
                        color: #fff;
                        font-size: 1.1em;
                    }

                    .left_col .form-group {
                        margin-bottom: 4px !important;
                    }

                    .left_col .form-group-title {
                        margin-top: 10px;
                        padding-left: 6px;
                        font-weight: bold;
                        font-size: 90%;
                        background: #1c498a;
                    }

                .right_col {
                    padding: 0px;
                    margin: 0px;
                }

                .main_panel .form-group {
                    margin: 0px;
                }

                .multi_inpt .input-group-addon {
                    padding: 0px 3px 0px 0px;
                    line-height: 1;
                    color: #555;
                    text-align: left !important;
                    background-color: transparent !important;
                    border: 0px solid #ccc;
                    width: 50%;
                }

                .multi_inpt.three-col .input-group-addon {
                    width: 33.33%;
                }

                .sales_container .btn-primary {
                    background: #1c498a;
                }

                .fixed_inpt_cal .pl-0 {
                    padding-left: 0px;
                    margin-bottom: 10px;
                }

                .fixed_inpt_cal .input-group-addon label {
                    float: left;
                }

                .fixed_inpt_cal .input-group-addon {
                    border-radius: 0px;
                    border: none;
                }

                .fixed_inpt_cal .btn_custom {
                    float: right;
                }

                .gridbg td,
                th {
                    font-size: 13px;
                }

                .gridbg {
                    overflow: auto;
                    overflow-y: hidden;
                    width: 100%;
                    background: #eeeeee;
                }

                .sector td,
                th {
                }

                .footer_big {
                    display: none !important;
                }

                .tab-content {
                    padding-top: 10px;
                }

                .col-container {
                    width: 100%;
                    margin-bottom: 70px;
                    position: relative;
                    display: flex;
                }

                .col {
                    -webkit-box-flex: 1;
                    -ms-flex: 1;
                    flex: 1;
                    max-width: 100%;
                    background: #e7e7e7;
                    vertical-align: top;
                    position: relative;
                    float: left;
                }

                .col2 {
                    -webkit-box-flex: 1;
                    -ms-flex: 1;
                    flex: 1;
                    max-width: 100%;
                    vertical-align: top;
                    float: left;
                }
                .col input, .CorpTrvl-page .form-control {
                    border-radius: 0px;
                    height: 30px;
                    text-transform:uppercase;
                }

                .col2 input {
                    border-radius: 0px;
                    height: 30px;
                }

                .col .form-group {
                    margin-bottom: 6px;
                }

                .col .form-group {
                    margin-bottom: 6px;
                }

                .col,
                .col2,
                label {
                    font-size: 12px !important;
                }

                #ticket_tab .flex-row {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-flex-wrap: wrap;
                    -ms-flex-wrap: wrap;
                    flex-wrap: wrap;
                }

                #ticket_tab div.row {
                    padding-top: 0;
                }

               
                .pnr-toggle-btn-wrap {
                    font-size: 13px;
                    position: absolute;
                    right: 15px;
                    z-index: 999;
                    background-color: #000;
                    color: #fff;
                    width: 8px;
                    height: 32px;
                    text-align: center;
                    line-height: 28px;
                    border-radius: 40px 0 0px 40px;
                    top: 10px;
                }

                @media(max-width:1199px) {
                    #RightPanel {
                        margin-top: 54px;
                        position: absolute;
                        top: 0;
                        right:10px;
                        max-width: 100%;
                    }

                        #RightPanel:before {
                            content: '';
                            position: fixed;
                            width: 100%;
                            height: 100%;
                            background-color: rgba(0,0,0,.4);
                            top: 0;
                            left: 0;
                            right: 0;
                            bottom: 0;
                        }

                    #hidePanel .fa:before {
                        content: "\f104";
                    }

                    .expanded-left-panel #hidePanel .fa:before {
                        content: '\f00d';
                    }

                    #RightPanel .main_panel {
                        background-color: #fff;
                    }

                    .pnr-toggle-btn-wrap {
                        right:18px;
                        width: auto;
                        height: auto;
                        line-height:20px;
                        border-radius: 50%;
                        top: 7px;
                    }

                        .pnr-toggle-btn-wrap a {
                            width: 20px;
                            height: 20px;
                            display: block;
                        }
                }

                 #RightPanel {
					display: none;
					 padding: 0;
				 }
				.expanded-left-panel #RightPanel {
					display: block;
				}

                .expanded-left-panel .pnr-toggle-btn-wrap .fa-angle-left:before {
                    content: "\f105";
                }

                .showParamToggle[aria-expanded="true"] .fa-plus-square:before {
                    content: "\f146";
                }


                @media(min-width:1199px) {

                    .pnr-toggle-btn-wrap {
                        top: 44%;
                    }

                    #ticket_tab .col-md-1, #ticket_tab .col-md-2, #ticket_tab .col-md-3 {
                        /*max-width: 130px;*/
                    }

                    .col {
                        max-width: 20%;
                    }

                    .col2 {
                        max-width:100%;
                    }

                    .expanded-left-panel .col2 {
                        max-width: 100%;
                        width: 100%;
                    }

                    .expa .col-container {
                        display: -webkit-box;
                        display: -ms-flexbox;
                        display: flex;
                    }
                }

                #RightPanel .form-control.select2-container, #LeftPanel .form-control.select2-container {
                    height: 30px !important;
                }


                #RightPanel .select2-container .select2-choice, #LeftPanel .select2-container .select2-choice {
                    height: 28px !important;
                    line-height: 30px !important;
                }

                .modal-header {
                    background: #636363;
                }
				[aria-expanded="true"] .fa.fa-plus-circle:before{
					content: "\f056";
				}
            </style>
            <div class="cz-container">
            <script type="text/javascript">
                function showpopup() {
                    $('#FareDiff').modal('show');
                    $('#showParam').collapse('hide');
                    document.getElementById('<%=hdnIsFromEdit.ClientID%>').value = "false";

                }



                function DisableSectorsList() {
                    document.getElementById('<%=ddldestination.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector1.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector2.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector3.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector4.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector5.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector6.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector7.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector8.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector9.ClientID%>').disabled = true;
                    document.getElementById('<%=ddlSector10.ClientID%>').disabled = true;
                }
                function ReloadSelectedTab() {
                    var selectedTab = document.getElementById('<%=hdnSelectedTabName.ClientID%>').value;
                    if (selectedTab == "TicketTab") {
                        $('#TicketTab').addClass('active');
                        $("#TicketTab").trigger('click');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        return;

                    }
                    if (selectedTab == "sectortab") {
                        $('#sectortab').addClass('active');
                        $("#sectortab").trigger('click');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        return;

                    }
                    if (selectedTab == "UDIDtab") {
                        $('#UDIDtab').addClass('active');
                        $("#UDIDtab").trigger('click');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                document.getElementById('<%=btnnext.ClientID%>').value = 'Save';
                        return;

                    }
                    else {
                        $('#TicketTab').addClass('active');
                        $("#TicketTab").trigger('click');
                        document.getElementById('ctl00_cphTransaction_btnprev').style.display = 'none';
                    }
                }

                function navigateTab(tabIndex) {
                    if (tabIndex == "1") {
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                        document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                        $('#TicketTab').addClass('active');
                        $('#sectortab').removeClass('active');
                        $('#UDIDtab').removeClass('active');
                    }
                    if (tabIndex == "2") {
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                        document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                        $('#sectortab').addClass('active');
                        $('#TicketTab').removeClass('active');
                        $('#UDIDtab').removeClass('active');
                    }

                    if (tabIndex == "3") {
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';

                document.getElementById('<%=btnnext.ClientID%>').value = 'Save';

                        $('#UDIDtab').addClass('active');
                        $('#sectortab').removeClass('active');
                        $('#TicketTab').removeClass('active');
                    }
                }
                function SetActiveTab() {
                    

                    if ($('#UDIDtab').hasClass('active')) {
                        $('#UDIDtab').removeClass('active');

                        $('#TicketTab').addClass('active');
                        $("#TicketTab").trigger('click');

                    }
                }

                function navigateNextTab() {
                    $('#showParam').collapse('hide');
                    
                    if (ValidateCtrls()) {
                        
                        if ($('#sectortab').hasClass('active')) {

                            if (ValidateSectorTab()) {
                                $('#sectortab').removeClass('active');
                                $("#TicketTab").removeClass('active');
                                $("#UDIDtab").addClass('active');
                                $("#UDIDtab").trigger('click');
                                document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'UDIDtab';
                                document.getElementById('<%=btnnext.ClientID%>').style.display = 'none';
                                document.getElementById('<%=btnSave.ClientID%>').style.display = 'block';
                            }
                        }

                        if ($('#TicketTab').hasClass('active')) {

                            $('#TicketTab').removeClass('active');
                            if (document.getElementById('sectortab').style.display != 'none') {
                                $('#sectortab').addClass('active');
                                $("#sectortab").trigger('click');
                                document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                                $('#UDIDtab').removeClass('active');

                            }                            
                        }
                    }
                    return false;
                }

                function ReinitiateValues() {
                    if (document.getElementById('<%=txtcashAmount.ClientID%>').value == "0.00") {
                        var sellingFare = eval(document.getElementById('<%=txtSellingFare.ClientID%>').value.replace(',', ''));
                        document.getElementById('<%=txtCommisionOn.ClientID%>').value = sellingFare;
                        document.getElementById('<%=txtDiscountOn.ClientID%>').value = sellingFare;

                        if (document.getElementById('<%=txtTax.ClientID%>').value.trim().length > 0)
                            tax = eval(document.getElementById('<%=txtTax.ClientID%>').value.replace(',', ''));

                        var totalFare = sellingFare + tax;
                        if (isNaN(totalFare)) {
                            totalFare = 0.00;
                        }
                        calculateCollectedAmt();
                        CalculateTax();
                        CalculateNetPay();
                        calculateProfit();
                        CalculateCashAmount();
                        CalculateVAT();
                        CheckSellingfareValid();
                        CalculateCommision();
                        CalculateDiscount();
                    }
                }

                function navigatePrevTab() {

                    if ($('#UDIDtab').hasClass('active')) {
                        $('#UDIDtab').removeClass('active');
                        $('#sectortab').addClass('active');
                        $("#sectortab").trigger('click');
                        $("#UDIDtab").removeClass('active');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                        document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        document.getElementById('<%=btnSave.ClientID%>').style.display = 'none';
                        document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'sectortab';
                        
                    }
                    else if ($('#sectortab').hasClass('active')) {
                        $('#sectortab').removeClass('active');
                        $('#TicketTab').addClass('active');
                        $("#UDIDtab").removeClass('active');
                        $("#TicketTab").trigger('click');
                        document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        //document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                        document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'TicketTab';
                    }
                    <%--else {
                        $('#TicketTab').addClass('active');
                        $("#TicketTab").trigger('click');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                        document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                        document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                        document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'TicketTab';
                    }--%>
                }

                function ChangePaymentmode1() {

                    var customerval = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                    if (customerval == "No Customer") {
                        $('#<%=ddlMode.ClientID%>').select2('val', 1);
                //$('#<%=ddlMode.ClientID%>').select2('customerval', "Cash");
                    }
                    else {
                        $('#ctl00_cphTransaction_ddlMode').select2('val', 3);
                    }
                }

                function makeEditFalse() {
                    document.getElementById('<%=hdnIsFromEdit.ClientID%>').value = "false";
                    if (document.getElementById('<%=hdnEditMode.ClientID%>').value == 'Update') {
                        document.getElementById('<%=btnnext.ClientID%>').style.display = 'none';
                    }
                    TaxCounterTozero();
                }

                var Ajax;
                if (window.XMLHttpRequest) {
                    Ajax = new XMLHttpRequest();
                }
                else {
                    Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                }

                function GetSectorsCount1(id) {
                    var sectorsCount = 0;
                    if (document.getElementById('<%=ddlSector1.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector2.ClientID%>').value != "-1") {
                        sectorsCount = 1;

                        if (document.getElementById('<%=ddlSector3.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector3.ClientID%>').value != "") {
                            sectorsCount = 2;

                            if (document.getElementById('<%=ddlSector4.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector4.ClientID%>').value != "") {
                        sectorsCount = 3;

                        if (document.getElementById('<%=ddlSector5.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector5.ClientID%>').value != "") {
                            sectorsCount = 4;

                            if (document.getElementById('<%=ddlSector6.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector6.ClientID%>').value != "") {
                                sectorsCount = 5;

                                if (document.getElementById('<%=ddlSector7.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector7.ClientID%>').value != "") {
                                    sectorsCount = 6;

                                    if (document.getElementById('<%=ddlSector8.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector8.ClientID%>').value != "") {
                                        sectorsCount = 7;

                                        if (document.getElementById('<%=ddlSector9.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector9.ClientID%>').value != "") {
                                            sectorsCount = 8;

                                            if (document.getElementById('<%=ddlSector10.ClientID%>').value != "-1" && document.getElementById('<%=ddlSector10.ClientID%>').value != "") {
                                                        sectorsCount = 9;

                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }

                        document.getElementById('<%=hndsegmentsCount.ClientID%>').value = sectorsCount;

                    }

                    if (eval(sectorsCount) > 0) {
                        $('#sectortab').show();
                        ShowSectorTable(eval(sectorsCount));
                        //CheckDuplicatesectors(id);
                    }
                    $('#showParam').collapse('hide');
                    disableSectors();

                }

                function bindRepAirline() {

                    var ddlairline = document.getElementById('<%=ddlairline.ClientID%>');

                    if (ddlairline.value != "-1") {
                        var val = document.getElementById('<%=ddlairline.ClientID%>').value;
                        $('#<%=ddlRepAirline.ClientID%>').select2('val', val);
                    }                    
                    
                    var txtNumber = document.getElementById('<%=txtNumber.ClientID%>');
                    if (ddlairline.value == '11') {
                        txtNumber.setAttribute('MaxLength', '10');
                        txtNumber.setAttribute('onkeypress', 'return allowNumerics(event);');
                    }
                    else { 
                        txtNumber.setAttribute('MaxLength', '50');
                        txtNumber.setAttribute('onkeypress', 'return isAlphaNumeric(event);');
                    }
                    CheckTicketNoValid();

                    if (ddlairline.value == '15' || ddlairline.value == '16') {
                        document.getElementById('<%=txtClassSector.ClientID%>').value = 'Y';
                        $('#<%=ddldestination.ClientID%>').select2('val', 'MCT');
                        BindSectors();
                        $('#<%=ddlSector1.ClientID%>').select2('val', 'SHJ');
                        $('#<%=ddlSector2.ClientID%>').select2('val', 'MCT');
                        $('#<%=ddlSector3.ClientID%>').select2('val', 'SHJ');
                        document.getElementById('<%=hndsegmentsCount.ClientID%>').value = '3';
                    }
                }

                function CalculateCommision() {
                    var id = document.getElementById('<%=txtCommper.ClientID%>').id;
                    Set(id);
                    var commisionPer = eval(parseFloat(document.getElementById('<%=txtCommper.ClientID%>').value));
                    var commissionOn = eval(document.getElementById('<%=txtCommisionOn.ClientID%>').value.replace(/[^\d\.]/g, ''));
                    var commisionAmt = commissionOn * (commisionPer / 100);
                    document.getElementById('<%=txtCommisionAmt.ClientID%>').value = parseFloat(commisionAmt).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                    document.getElementById('<%=txtCommision.ClientID%>').value = parseFloat(commisionAmt).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                    CalculateNetPay();
                }

                function CalculateDiscount() {
                    var id = document.getElementById('<%=txtDiscountper.ClientID%>').id;
                    Set(id);
                    var discountPer = eval(document.getElementById('<%=txtDiscountper.ClientID%>').value);
                    var discountOn = eval(document.getElementById('<%=txtDiscountOn.ClientID%>').value.replace(/[^\d\.]/g, ''));
                    var discountAmt = discountOn * (discountPer / 100);
                    document.getElementById('<%=txtDiscountAmt.ClientID%>').value = parseFloat(discountAmt).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                    document.getElementById('<%=txtDiscount.ClientID%>').value = parseFloat(discountAmt).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });;
                    calculateCollectedAmt();
                }
                function ClearSectorTable(number) {
                    if (eval(number) > 0) {
                        for (var i = 1; i <= number; i++) {

                            document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value = "";
                            document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).value = "";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = "";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').value = "";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value = "";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').value = "";
                            document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).value = "";
                            document.getElementById('ctl00_cphTransaction_txtClass' + i).value = "";
                            document.getElementById('ctl00_cphTransaction_txtDuration' + i).value = "";

                        }
                    }
                    //$("input:text").val("");

                }

                function ShowSectorTable(number) {
                    if (eval(number) > 0) {
                        document.getElementById('sectortab').style.display = "block";
                    }
                    else
                        document.getElementById('sectortab').style.display = "none";
                    for (var i = 1; i <= number; i++) {

                        document.getElementById('ctl00_cphTransaction_chkIsRefund' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ddlToSector' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').style.display = "block";
                        //if (i == 1) {
                        //    document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = document.getElementById('ctl00_cphTransaction_txtTravelDt').value;
                        //}
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtClass' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtDuration' + i).style.display = "block";
                        $('span').show();
                    }

                    for (var i = eval(number) + 1; i <= 20; i++) {
                        ////ctl00_cphTransaction_DepartureDt1_Date
                        //document.getElementById('ctl00_cphTransaction_row'+i).style.display = 'none';

                        document.getElementById('ctl00_cphTransaction_chkIsRefund' + i).style.display = 'none';
                        var DeptDt = document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date');
                        DeptDt.style.display = 'none';  //.style.display = 'none';
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = 'none';
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = 'none';
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = 'none';
                    }

                }

                function disableSectors() {
                    if (document.getElementById('<%=ddldestination.ClientID%>').value == "-1") {
                        document.getElementById('<%=ddlSector1.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector2.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector3.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector4.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector5.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector6.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector7.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector8.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector9.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector10.ClientID%>').disabled = true;

                    }
                    else {
                        document.getElementById('<%=ddlSector1.ClientID%>').disabled = false;
                        document.getElementById('<%=ddlSector2.ClientID%>').disabled = false;
                        document.getElementById('<%=ddlSector3.ClientID%>').disabled = false;
                        document.getElementById('<%=ddlSector4.ClientID%>').disabled = false;
                        document.getElementById('<%=ddlSector5.ClientID%>').disabled = false;
                    }
                    if (document.getElementById('<%=ddlSector5.ClientID%>').value != "-1") {
                        document.getElementById('<%=ddlSector6.ClientID%>').disabled = false;
                document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
            }
            else {
                document.getElementById('<%=ddlSector6.ClientID%>').disabled = true;
                document.getElementById('<%=ddlSector7.ClientID%>').disabled = true;
                document.getElementById('<%=ddlSector8.ClientID%>').disabled = true;
                document.getElementById('<%=ddlSector9.ClientID%>').disabled = true;
                document.getElementById('<%=ddlSector10.ClientID%>').disabled = true;
                    }
            <%--if(!document.getElementById('<%=btnSave.ClientID%>').disabled)
            {
                document.getElementById('select2-results-10').disabled = true;
            }--%>

                }


                function Check(id) {
                    var val = document.getElementById(id).value;
                    if (val == '0.00') {
                        document.getElementById(id).value = '';
                    }
                }
                function Set(id) {
                    var val = document.getElementById(id).value;
                    if (val == '' || val == '0.00' || val == '0') {
                        document.getElementById(id).value = '0.00';
                    }
                    else {
                        var index = val.indexOf('.'); // if val=10.00
                        if (index > 0) {
                            var arr = val.split('.');
                            if (arr[1] == "") {
                                arr[1] = "00";
                            }
                            val = parseInt(arr[0]) + "." + arr[1];
                        }
                        else if (index = 0)  //if val=.00
                        {
                            val = '0.00';
                        }
                        else             //if val=010
                        {
                            val = val.replace(/\b0+/g, "")
                            val = val + ".00";
                        }
                        document.getElementById(id).value = val;
                    }
                }

                function validateSector(event) {
                    var sectorName = $(event);
                    var passData = "sectorName=" + id;
                    Ajax.onreadystatechange = ShowSectorResult;
                    Ajax.open("POST", "OffLineEntry.aspx");
                    Ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    Ajax.send()

                }


                function hidePopUp(index) {
                    document.getElementById('<%=hdnSelectedIndex.ClientID%>').value = index;
                    document.getElementById('<%=hdnIsFromEdit.ClientID%>').value = "true";
                    // document.getElementById("array_disp").innerHTML = r;
                    // $('#FareDiff').modal('hide');
                    $('#showParam').collapse('hide');
                    getElement('EditPNRDiv').style.display = "block";
                    document.forms[0].submit();
                }

                function RemovePaxFromPopUp(index) {

                    if (confirm('Do you want to remove?')) {
                        document.getElementById('<%=hdnRemovePax.ClientID%>').value = index;
                        $('#showParam').collapse('hide');
                        document.forms[0].submit();
                    }
                }
                function hidepopupdata() {
                    $('#FareDiff').modal('hide');
                }

                function HideSearchOption() {
                    $('#showParam').collapse('hide');
                    $('#TicketTab').addClass('active');
                    document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';

                }
                function IsAlphaNumeric(e) {
                    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                    var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
                    return ret;
                }

                var cal1;
                var cal2;

                function init() {

                    //    showReturn();
                    var today = new Date();
                    // For making dual Calendar use CalendarGroup  for single Month use Calendar     
                    cal1 = new YAHOO.widget.Calendar("cal1", "container1");
                    //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                    cal1.cfg.setProperty("title", "Select sales date");
                    cal1.cfg.setProperty("close", true);
                    cal1.selectEvent.subscribe(setDates1);
                    cal1.render();

                    cal2 = new YAHOO.widget.Calendar("cal2", "container2");
                    cal2.cfg.setProperty("title", "Select travel date");
                    cal2.selectEvent.subscribe(setDates2);
                    cal2.cfg.setProperty("close", true);
                    cal2.render();
                }
                function showCal1() {

                    $('container2').context.styleSheets[0].display = "none";
                    $('container1').context.styleSheets[0].display = "block";
                    init();
                    cal1.show();
                    cal2.hide();
                }


                var departureDate = new Date();
                function showCal2() {
                    var date1 = document.getElementById('<%= txtSalesDate.ClientID%>').value;
                    if (date1 == '') {
                        toastr.error('Please select sales date');
                        return false;
                    }
                    $('container1').context.styleSheets[0].display = "none";
                    cal1.hide();
                    init();
                    // setting Calender2 min date acoording to calendar1 selected date
                    //var date1=new Date(tempDate.getDate()+1);

                    if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                        var depDateArray = date1.split('/');

                        var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                        cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                        cal2.cfg.setProperty("pageDate", depDateArray[0] + "/" + depDateArray[2]);
                        cal2.render();
                    }
                    document.getElementById('container2').style.display = "block";
                }
                function setDates1() {
                    var date1 = cal1.getSelectedDates()[0];

                    $('IShimFrame').context.styleSheets[0].display = "none";
                    this.today = new Date();
                    var thisMonth = this.today.getMonth();
                    var thisDay = this.today.getDate();
                    var thisYear = this.today.getFullYear();

                    var todaydate = new Date(thisYear, thisMonth, thisDay);
                    var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
                    var difference = (depdate.getTime() - todaydate.getTime());

                    departureDate = cal1.getSelectedDates()[0];
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";
                    //			
                    var month = date1.getMonth() + 1;
                    var day = date1.getDate();

                    if (month.toString().length == 1) {
                        month = "0" + month;
                    }

                    if (day.toString().length == 1) {
                        day = "0" + day;
                    }

            //document.getElementById('<%= txtSalesDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
                    document.getElementById('<%= txtSalesDate.ClientID %>').value = month + "/" + (day) + "/" + date1.getFullYear();

                    //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
                    //cal2.render();

                    cal1.hide();

                }
                function setDates2() {
                    var date1 = document.getElementById('<%=txtSalesDate.ClientID %>').value;
                    if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "First select sales date.";
                        return false;
                    }

                    var date2 = cal2.getSelectedDates()[0];

                    var depDateArray = date1.split('/');

                    // checking if date1 is valid		    
                    if (!CheckValidDate(depDateArray[1], depDateArray[0], depDateArray[2])) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                        return false;
                    }
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";

                    // Note: Date()	for javascript take months from 0 to 11
                    var depdate = new Date(depDateArray[2], depDateArray[0] - 1, depDateArray[1]);
                    var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
                    var difference = returndate.getTime() - depdate.getTime();

                    //         if (difference < 1) {
                    //             document.getElementById('errMess').style.display = "block";
                    //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                    //             return false;
                    //         }
                    //         if (difference == 0) {
                    //             document.getElementById('errMess').style.display = "block";
                    //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                    //             return false;
                    //         }
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";

                    var month = date2.getMonth() + 1;
                    var day = date2.getDate();

                    if (month.toString().length == 1) {
                        month = "0" + month;
                    }

                    if (day.toString().length == 1) {
                        day = "0" + day;
                    }

            // document.getElementById('<%=txtTravelDt.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
                    document.getElementById('<%= txtTravelDt.ClientID %>').value = month + "/" + (day) + "/" + date2.getFullYear();
                    cal2.hide();
                }
                YAHOO.util.Event.addListener(window, "load", init);

                function CheckPNRValid() {
                    if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length > 0)
                        document.getElementById('PNR').style.display = 'none';
                }
                function CheckTicketNoValid() {
                    var txtNumber = document.getElementById('<%=txtNumber.ClientID%>');
                    if (txtNumber.value.trim().length > 0) {
                        
                        document.getElementById('Number').style.display = 'none';
                        var ddlairline = document.getElementById('<%=ddlairline.ClientID%>');
                        if (ddlairline.value == '11' && txtNumber.value.trim().length != 10) {
                            document.getElementById('<%=txtNumber.ClientID%>').value = '';
                            txtNumber.value = '';
                            txtNumber.focus();
                            toastr.error('Ticket number should be 10 character length for airline 11');
                            }
                        }
                }

                function CheckSalesDtValid() {
                    var date1 = document.getElementById('<%=txtSalesDate.ClientID %>').value;
                    if (date1.length > 0) {
                        document.getElementById('errMess').style.display = "none";
                    }
                }
                function CheckTravelDtValid() {
                    var date1 = document.getElementById('<%=txtTravelDt.ClientID %>').value;
                    if (date1.length > 0) {
                        document.getElementById('traveldate').style.display = "none";
                    }
                }
                function CheckPaxNameValid() {
                    if (document.getElementById('<%=txtPaxName.ClientID %>').value.indexOf('/') != -1 || document.getElementById('<%=txtPaxName.ClientID %>').value.length > 0) {
                        document.getElementById('paxname').style.display = 'none';
                    }
                }

                function CheckClassValid() {
                    if (document.getElementById('<%=txtClassSector.ClientID %>').value.length > 0) {
                        document.getElementById('classerr').style.display = 'none';
                    }
                }

                function CheckSellingfareValid() {
                    if (document.getElementById('<%=txtSellingFare.ClientID %>').value.trim().length > 0) {
                        document.getElementById('sellingFare').style.display = 'none';
                    }
                }

                function CheckTaxValid() {
                    if (document.getElementById('ctl00_cphTransaction_txtCode0').value.trim().length > 0 || document.getElementById('ctl00_cphTransaction_txtValue0').value.trim().length > 0) {
                        document.getElementById('TaxErrorMsg').style.display = 'none';
                    }
                }

                function ValidateCtrls() {

                    var isValid = true;
                    $('#showParam').collapse('hide');
                    document.getElementById('paxname').style.display = 'none';
                    document.getElementById('sellingFare').style.display = 'none';
                    document.getElementById('PNR').style.display = 'none';
                    document.getElementById('AirLine').style.display = 'none';
                    document.getElementById('Number').style.display = 'none';
                    document.getElementById('salesdate').style.display = 'none';
                    document.getElementById('traveldate').style.display = 'none';
                    document.getElementById('locationErr').style.display = 'none';


                    document.getElementById('TaxErrorMsg').style.display = 'none';
                    document.getElementById('TrasactionErrormsg').style.display = 'none';
                    document.getElementById('MarkUPErrormsg').style.display = 'none';
                    document.getElementById('DiscountDetailsErr').style.display = 'none';
                    document.getElementById('CommissionErr').style.display = 'none';
                    document.getElementById('classerr').style.display = 'none';

                    document.getElementById('sectors').style.display = 'none';
                    document.getElementById('commision').style.display = 'none';

                    document.getElementById('SectorerrorMessage').style.display = 'none';
                    document.getElementById('destination').style.display = 'none';

                    if (document.getElementById('<%=txtNumber.ClientID%>').value.trim().length <= 0) {
                        //document.getElementById('Number').style.display = 'block';
                        //document.getElementById('Number').innerHTML = "Please Enter Ticket Number";

                        toastr.error('Please Enter Ticket Number');
                        $('#<%=txtNumber.ClientID%>').addClass('form-text-error');

                        isValid = false;
                    }
                    if (document.getElementById('<%=txtNumber.ClientID%>').value.trim().length > 0) {
                        if (document.getElementById('<%=txtNumber.ClientID%>').value.trim().length < 6) {
                    //document.getElementById('Number').style.display = 'block';
                    //document.getElementById('Number').innerHTML = "Please Enter Ticket Number";

                    toastr.error('Ticket Number minimum 6 charectors');
                    $('#<%=txtNumber.ClientID%>').addClass('form-text-error');

                            isValid = false;
                        }
                    }

                    if (document.getElementById('<%=ddldestination.ClientID%>').value == "-1") {

                        toastr.error('Please Select Destination');
                        $('#<%=ddldestination.ClientID%>').parent().addClass('form-text-error');

                        isValid = false;
                    }

                    if (document.getElementById('<%=txtClassSector.ClientID%>').value.trim().length <= 0) {

                        toastr.error('Please Enter class');
                        $('#<%=txtClassSector.ClientID%>').addClass('form-text-error');

                        isValid = false;
                    }

                    <%--if (document.getElementById('<%=ddlSector1.ClientID %>').value == "-1" || document.getElementById('<%=ddlSector2.ClientID %>').value == "-1") {

                        toastr.error('Please select minimum 2 sectors');
                        $('#<%=ddlSector1.ClientID%>').parent().addClass('form-text-error');

                        isValid = false;
                    }--%>

                    if (document.getElementById('<%=rbtnSelf.ClientID %>').checked == true
                    && document.getElementById('<%=ddlCustomer1.ClientID%>').value == "-1"
                    && document.getElementById('<%=ddlMode.ClientID%>').value == "3")
                    {
                                    toastr.error('Please Select Customer');
                                    $('#<%=ddlCustomer1.ClientID%>').parent().addClass('form-text-error');
                                    isValid = false;
                    }
                    else
                    {
                                    $('#<%=ddlCustomer1.ClientID%>').parent().removeClass('form-text-error');
                    }

                    if (document.getElementById('<%=txtMarkUp.ClientID %>').value.trim().length <= 0 || document.getElementById('<%=txtAmount2.ClientID %>').value.trim().length <= 0) {

                        toastr.error('Please Enter Mark Up Details');
                        $('#<%=txtMarkUp.ClientID%>').addClass('form-text-error');
                $('#<%=txtAmount2.ClientID%>').addClass('form-text-error');

                        isValid = false;
                    }

                    if (document.getElementById('<%=txtDiscountper.ClientID %>').value.trim().length <= 0) {

                        toastr.error('Please Enter Discount Details');
                        $('#<%=txtDiscountper.ClientID%>').addClass('form-text-error');

                        isValid = false;
                    }

                    if (document.getElementById('<%=txtCommper.ClientID %>').value.trim().length <= 0) {
                        toastr.error('Please Enter Commision Details');
                        $('#<%=txtCommper.ClientID%>').addClass('form-text-error');

                        isValid = false;
                    }

                    if (document.getElementById('<%=txtPaxName.ClientID %>').value.trim().length <= 0 || document.getElementById('<%=txtPaxName.ClientID %>').value.trim().length > 0) {
                        if (document.getElementById('<%=txtPaxName.ClientID %>').value.indexOf('/') != -1 || document.getElementById('<%=txtPaxName.ClientID %>').value.length <= 0 || document.getElementById('<%=txtPaxName.ClientID %>').value.indexOf('/') == -1) {
                    var pax = document.getElementById('<%=txtPaxName.ClientID %>').value.split('/');
                    if (pax[1] == "" || document.getElementById('<%=txtPaxName.ClientID %>').value.indexOf('/') == -1 || document.getElementById('<%=txtPaxName.ClientID %>').value.trim().length <= 0) {
                        isValid = false;
                        toastr.error('Please enter Passenger Name As FirstName/LastName');
                        $('#<%=txtPaxName.ClientID%>').addClass('form-text-error');


                            }
                            if (pax.length == 2 && (pax[0] != "" && pax[1] != "")) {

                                document.getElementById('paxname').style.display = 'none';

                            }

                        }
                    }
                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAgent.ClientID %>').value != "-1"
                && document.getElementById('<%=ddlConsultant.ClientID %>').value == "-1") {
                toastr.error('Please Select Location');
                $('#<%=ddlConsultant.ClientID%>').addClass('form-text-error');
                        isValid = false;

                    }
                    if (eval(document.getElementById('<%=txtSellingFare.ClientID %>').value) <= 0 || document.getElementById('<%=txtSellingFare.ClientID %>').value.trim().length <= 0) {

                        toastr.error('Please enter sellingFare');
                        $('#<%=txtSellingFare.ClientID%>').addClass('form-text-error');


                        isValid = false;
                    }
                    if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length <= 0) {

                        toastr.error('Please enter PNR');
                        $('#<%=txtPNRNum.ClientID%>').addClass('form-text-error');
                        isValid = false;
                    }
                    if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length > 0) {
                        if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length < 6) {

                    toastr.error('PNR Number minimum 6 charectors');
                    $('#<%=txtPNRNum.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }
                    }
                    if (document.getElementById('<%=ddlairline.ClientID %>').value == "-1") {

                        toastr.error('Please select AirLine');
                        $('#<%=ddlairline.ClientID%>').parent().addClass('form-text-error');


                        isValid = false;
                    }
                    if (document.getElementById('<%=ddlSupplier.ClientID %>').value == "-1") {

                        toastr.error('Please select Supplier');
                        $('#<%=ddlSupplier.ClientID%>').parent().addClass('form-text-error');


                        isValid = false;
                    }
                    if (document.getElementById('<%=txtSalesDate.ClientID %>').value.trim().length <= 0) {

                        toastr.error('Please enter salesdate');
                        $('#<%=txtSalesDate.ClientID%>').addClass('form-text-error');


                        isValid = false;
                    }
                    if (document.getElementById('<%=txtTravelDt.ClientID %>').value.trim().length <= 0) {
                        toastr.error('Please enter traveldate');
                        $('#<%=txtTravelDt.ClientID%>').addClass('form-text-error');


                        isValid = false;
                    }
                    // check destination duplication



                    //
                    if (document.getElementById('<%=txtCommision.ClientID %>').value.trim().length <= 0) {

                        toastr.error('Please enter commision');
                        $('#<%=txtCommision.ClientID%>').addClass('form-text-error');



                        isValid = false;
                    }

                    if (document.getElementById('<%=txtTransFee.ClientID%>').value.trim().length <= 0 || document.getElementById('<%=txtAddlTransFee.ClientID%>').value.trim().length <= 0) {

                        toastr.error('Please enter Trans Fee Details');
                        $('#<%=txtTransFee.ClientID%>').addClass('form-text-error');

                        isValid = false;
                    }

                    var txtValue0 = document.getElementById('ctl00_cphTransaction_txtValue0').value.trim();
                    if (txtValue0.length <= 0) {
                        toastr.error('Please Enter Tax Amount');
                        $('#ctl00_cphTransaction_txtValue0').addClass('form-text-error');
                        isValid = false;
                    }

                    if (txtValue0.length > 0 && Math.ceil(txtValue0) > 0 && document.getElementById('ctl00_cphTransaction_txtCode0').value.trim().length <= 0) {
                        toastr.error('Please Enter Tax Code');
                        $('#ctl00_cphTransaction_txtCode0').addClass('form-text-error');
                        isValid = false;
                    }
                    if (document.getElementById('<%=hndTaxCounter.ClientID%>').value.trim().length > 0) {
                        var TaxCounter = document.getElementById('<%=hndTaxCounter.ClientID%>').value;
                        for (var i = 1; i <= TaxCounter; i++) {
                            if (document.getElementById('txtCode' + i).value.trim().length <= 0 || document.getElementById('txtValue' + i).value.trim().length <= 0) {
                                document.getElementById('TaxErrorMsg').style.display = 'block';
                                document.getElementById('TaxErrorMsg').innerHTML = "Please Enter Tax Details";
                                isValid = false;
                            }
                        }
                    }




                    if (document.getElementById('<%=ddlSector6.ClientID%>').value != "" && document.getElementById('<%=ddlSector6.ClientID%>').value != "-1") {
                        if (document.getElementById('<%=txtConjPNR.ClientID %>').value.trim().length > 0) {
                    if (document.getElementById('<%=txtConjPNR.ClientID %>').value.trim().length < 6) {

                        toastr.error('Conjection PNR Number minimum 6 charectors');
                        $('#<%=txtConjPNR.ClientID%>').addClass('form-text-error');
                        isValid = false;
                    }
                }
                if (document.getElementById('<%=txtConjPNR.ClientID %>').value.trim().length <= 0) {
                    toastr.error('Please enter Conjection PNR Number');
                    $('#<%=txtConjPNR.ClientID%>').addClass('form-text-error');

                            isValid = false;
                        }

                    }
                    if (document.getElementById('<%=ddlSector6.ClientID%>').value != "" && document.getElementById('<%=ddlSector6.ClientID%>').value != "-1") {
                        if (document.getElementById('<%=txtCongTicketNo.ClientID %>').value.trim().length > 0) {
                    if (document.getElementById('<%=txtCongTicketNo.ClientID %>').value.trim().length < 6) {

                        toastr.error('Conjection Ticket Number minimum 6 charectors');
                        $('#<%=txtCongTicketNo.ClientID%>').addClass('form-text-error');
                        isValid = false;
                    }
                }
                if (document.getElementById('<%=txtCongTicketNo.ClientID %>').value.trim().length <= 0) {
                    toastr.error('Please enter Conjection Ticket Number');
                    $('#<%=txtCongTicketNo.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }
                                           
                    

                    }

                    var cashamt = eval(document.getElementById('<%=txtcashAmount.ClientID%>').value.replace(/,/g, ''));
                if (Math.ceil(cashamt) <= 0) {
                    toastr.error('Cash Amount should not be Zero/negative');
                    $('#<%=txtcashAmount.ClientID%>').addClass('form-text-error');
                    isValid = false;
                }

                    if (isValid) {
                        isValid = CheckDestinationExistInSectors();
                    }

                    if (isValid && !$('#sectortab').hasClass('active')) {
                        BindSectorTab();
                    }

                    return isValid;
                }
                function ValidateSectorTab1() {
                    var isValid = true;
                    if (document.getElementById('<%=hndsegmentsCount.ClientID%>').value.trim().length > 0 && isValid == true) {
                        var isSectorValid = true;
                        var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        if (segmentlength == "0") {

                            toastr.error('Please Enter sector Details');
                            $('#<%=hndsegmentsCount.ClientID%>').addClass('form-text-error');
                            document.getElementById('sectortab').click();
                            isValid = false;

                        }

                        for (var i = 1; i <= segmentlength; i++) {
                            if (document.getElementById('s2id_ctl00_cphTransaction_ddlFromSector' + i).innerText.trim() == "Select") {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('s2id_ctl00_cphTransaction_ddlFromSector' + i).focus();

                                toastr.error('Please select From Sector');
                                $('#s2id_ctl00_cphTransaction_ddlFromSector' + i).parent().addClass('form-text-error');


                                isSectorValid = false;
                                document.getElementById('sectortab').click();
                                isValid = false;
                            }
                            if (document.getElementById('s2id_ctl00_cphTransaction_ddlToSector' + i).innerText.trim() == "Select") {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('s2id_ctl00_cphTransaction_ddlToSector' + i).focus();

                                toastr.error('Please select To Sector');
                                $('#s2id_ctl00_cphTransaction_ddlToSector' + i).addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;

                            }
                            if (document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).focus();

                                toastr.error('Please enter Flying Carrier');
                                $('#ctl00_cphTransaction_txtFlyingCarrier' + i).addClass('form-text-error');
                                $('#ctl00_cphTransaction_txtFlyingCarrier' + i).focus();


                                document.getElementById('sectortab').click();

                                isValid = false;
                            }
                            if (document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).focus();

                                toastr.error('Please enter Flight No');
                                $('#ctl00_cphTransaction_txtFlightN0' + i).addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                            if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').focus();

                                toastr.error('Please enter DeptDate(DD/MM/YYYY)');
                                $('#ctl00_cphTransaction_DepartureDt' + i + '_Date').addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                            if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value != "" && document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value != "") {
                                var departureDt = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + i);
                                var arrivalDt = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + i);

                                if ((departureDt != null && arrivalDt != null) && departureDt > arrivalDt) {
                                    //addMessage('From Date should not be less than To Date!', '');
                                    //if (getMessage() != '') {
                                    alert('Departure Date should not be greater than Arrival Date!');
                                    return isValid = false;
                                    // }
                                }

                                //checking departure Date
                                if (i == 1) {
                                    var SectorMonth = "";
                                    var sectorDate = "";
                                    var date = new Date(departureDt);
                                    if ((date.getMonth() + 1) < 10)
                                        SectorMonth = "0" + (date.getMonth() + 1);
                                    else
                                        SectorMonth = date.getMonth() + 1;

                                    if (date.getDate() < 10) {
                                        sectorDate = "0" + (date.getDate());
                                    }
                                    else
                                        sectorDate = date.getDate();

                                    var sectordeptDate = SectorMonth + '/' + sectorDate + '/' + date.getFullYear();
                                    var traveldate = document.getElementById('<%=txtTravelDt.ClientID%>').value;
                                    alert(sectordeptDate + '-----------' + traveldate);
                                    if (sectordeptDate != traveldate) {
                                        alert('Departure Date should be equal to Travel Date');
                                        return isValid = false;
                                    }
                                }
                                else {
                                    var id = i - 1;
                                    var arrivalDatePreviousSector = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + id);
                                    if (arrivalDatePreviousSector != null && departureDt <= arrivalDatePreviousSector) {
                                        alert('Departure Date should be greater than the Previous Sector Arrival Date & Time');
                                        return isValid = false;
                                    }
                                }
                            }

                            if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').focus();



                                toastr.error('Please Enter Departure Time');
                                $('#ctl00_cphTransaction_DepartureDt' + i + '_Time').addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }


                            if (document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').focus();

                                toastr.error('Please enter Arrival Date(DD/MM/YYYY)');
                                $('#ctl00_cphTransaction_ArrivalDt' + i + '_Date').addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                            if (document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').focus();

                                //document.getElementById('SectorerrorMessage').innerHTML = "Please Enter Arrival Time";

                                toastr.error('Please Enter Arrival Time');
                                $('#ctl00_cphTransaction_ArrivalDt' + i + '_Time').addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                            if (document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).focus();

                                //document.getElementById('SectorerrorMessage').innerHTML = "Please Enter Fare Basis";

                                toastr.error('Please Enter Fare Basis');
                                $('#ctl00_cphTransaction_txtFareBasis' + i).addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                            if (document.getElementById('ctl00_cphTransaction_txtClass' + i).value.trim().length == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('ctl00_cphTransaction_txtClass' + i).focus();

                                //document.getElementById('SectorerrorMessage').innerHTML = "Please Enter class";

                                toastr.error('Please Enter class');
                                $('#ctl00_cphTransaction_txtClass' + i).addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                            if (document.getElementById('ctl00_cphTransaction_txtDuration' + i).value.trim() == 0) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';


                                //document.getElementById('SectorerrorMessage').innerHTML = "Please Enter Duration";
                                toastr.error('Please Enter Duration');
                                $('#ctl00_cphTransaction_txtDuration' + i).addClass('form-text-error');

                                document.getElementById('sectortab').click();
                                isValid = false;
                            }

                        }

                    }
                    return isValid;
                }
                function CheckDestinationExistInSectors() {
                    var sectorsCount = 0;
                    var destvalue = document.getElementById('<%=ddldestination.ClientID%>').value;
                    sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    var isValid = false;
                    for (var i = sectorsCount; i > 1; i--) {
                        if (isValid)
                            break;
                        if (document.getElementById('ctl00_cphTransaction_ddlSector' + i).value != "-1" && document.getElementById('ctl00_cphTransaction_ddlSector' + i).value != "") {
                            if (document.getElementById('ctl00_cphTransaction_ddlSector' + i).value != destvalue) 
                                isValid = false;
                            else
                                isValid = true;
                        }
                    }
                    if (!isValid)
                        toastr.error('Please make sure destination should be there in one of the sectors');
                    return isValid;
                }

                function disablefield() {
                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                        document.getElementById('wrapper').style.display = "block";
                    }
                    else {
                        document.getElementById('wrapper').style.display = "none";
                    }
                }

                function Validate() {

                    var msg = "";
            <%--if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == false && document.getElementById('<%=rbtnSelf.ClientID %>').checked == false) {
                msg += "Please select Agency Type!";
            }--%>
                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                        if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex <= 0) {
                            msg += "Please select an agent from the list!";
                        }
                    }
                    if (document.getElementById('<%=txtPnr.ClientID %>').value == '' && document.getElementById('<%=ddlSource.ClientID%>').selectedIndex <= 0) {
                        msg += "Please Select source";
                    }
            <%--if (document.getElementById('<%=txtPnr.ClientID %>').value == '' && document.getElementById('<%=ddlSource.ClientID%>').selectedIndex != 1) {
                msg += "Please Enter PNR number!";
            }--%>
                    if (msg.length != 0) {
                        alert(msg);
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                var taxCounter = 0;

                function EnableTaxAddctrl(isFromRebindCtrls) {

                    var validate = true;
                    var taxCode;
                    var taxValue;
                    var taxcount = eval(document.getElementById('ctl00_cphTransaction_hndTaxCounter').value);
                    if (isFromRebindCtrls)
                        return validate = true;
                    for (var i = 0; i <= eval(taxcount); i++) {
                        if (i == 0) {
                            taxCode = document.getElementById('ctl00_cphTransaction_txtCode' + i);
                            taxValue = document.getElementById('ctl00_cphTransaction_txtValue' + i);
                        }
                        else {
                            taxCode = document.getElementById('txtCode' + i);
                            taxValue = document.getElementById('txtValue' + i);
                        }

                        if (taxValue != null && taxValue.value.length == 0 ) {
                            toastr.error('Please Enter Tax Amount');
                            $('#ctl00_cphTransaction_txtValue' + i).addClass('form-text-error');
                            validate = false;
                        }

                        if (validate && taxCode.value > 0 && taxCode.length == 0) {
                            toastr.error('Please Enter Tax Code');
                            $('#ctl00_cphTransaction_txtCode' + i).addClass('form-text-error');
                            validate = false;
                        }
                    }


                    return validate;

                }

                function AddNewRow(code, value, isFromRebindCtrls) {
                    try {
                        if (EnableTaxAddctrl(isFromRebindCtrls)) {
                            //  debugger;
                            //taxCounter = eval(document.getElementById('ctl00_cphTransaction_hndTaxCounter').value);
                            taxCounter++;
                            var lblCode = document.createElement("label");
                            lblCode.innerText = "Tax Code:";

                            var txtCode = document.createElement("input");
                            txtCode.type = "text";
                            txtCode.className = "form-control";
                            txtCode.setAttribute('onkeypress', 'return isAlphaNumeric(event);');
                            txtCode.setAttribute('onkeyup', 'return changeToUpperCase(this);');
                            //txtCode.style = "width:76px";
                            txtCode.id = "txtCode" + taxCounter;
                            txtCode.name = "txtCode" + taxCounter;
                            txtCode.setAttribute('onblur', 'BindTaxCodeValues();')
                            txtCode.setAttribute('MaxLength', '2');
                            txtCode.value = code;

                            var spanTax = document.createElement("span");
                            spanTax.type = "span";
                            spanTax.id = "spanT" + taxCounter;
                            spanTax.className = "input-group-addon";
                            spanTax.appendChild(lblCode);
                            spanTax.appendChild(txtCode);
                            //spanTax.appendChild(hdnCode);

                            var lblAmt = document.createElement("label");
                            lblAmt.innerText = "Amount:";

                            var txtValue = document.createElement("input");
                            txtValue.type = "text";
                            //txtValue.style = "width:76px";
                            txtValue.className = "form-control";
                            txtValue.id = "txtValue" + taxCounter;
                            txtValue.name = "txtValue" + taxCounter;
                            txtValue.maxLength = 6;
                            txtValue.value = value;
                            txtValue.setAttribute('onkeypress', 'return allowNumerics(event);');
                            txtValue.setAttribute('onchange', ' AddTaxAmounts(event);');
                            txtValue.setAttribute('onblur', 'BindTaxCodeValues();')

                            var spanTaxVal = document.createElement("span");
                            spanTaxVal.type = "span";
                            spanTaxVal.id = "spanV" + taxCounter;
                            spanTaxVal.className = "input-group-addon";
                            spanTaxVal.appendChild(lblAmt);
                            spanTaxVal.appendChild(txtValue);
                            //spanTaxVal.appendChild(hdnValue);

                            var link = document.createElement("a");
                            link.id = "lnk" + taxCounter;
                            link.className = "text-danger";
                            link.href = "#"
                            link.setAttribute('onClick', 'RemoveRow(this)');

                            var lnkminus = document.createElement("i");
                            lnkminus.className = "fa fa-minus-circle";
                            lnkminus.id = "lnkminus" + taxCounter

                            link.appendChild(lnkminus);

                            var spanlink = document.createElement("span");
                            spanlink.type = "span";
                            spanlink.id = "spanlnk" + taxCounter;
                            spanlink.className = "input-group-addon";
                            spanlink.appendChild(link);

                            var divClear = document.createElement("div");
                            divClear.type = "div";
                            divClear.id = "clearDiv" + taxCounter;
                            divClear.className = "clearfix";

                            document.getElementById("taxDiv").appendChild(divClear);
                            document.getElementById("taxDiv").appendChild(spanTax);
                            document.getElementById("taxDiv").appendChild(spanTaxVal);
                            document.getElementById("taxDiv").appendChild(spanlink);


                            document.getElementById('<%=hndTaxCounter.ClientID%>').value = taxCounter;
                        }

                    }
                    catch (ex) {
                        taxCounter = 0;
                    }
                }

                function ClosePopUp(e) {
                    $(this).close();
                }

                function allowNumerics(evt) {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }

                function isAlpha(e) {
                    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                    var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
                    //autoCompInit1();
                    return ret;
                }

                function changeToUpperCase(obj)
                {
                    obj.value = obj.value.toUpperCase();
                }

                function checkAlphaNumeric(e) {
                    var regex = new RegExp("[a-zA-Z0-9]");
                    var key = e.keyCode || e.which;
                    key = String.fromCharCode(key);
                    if (!regex.test(key)) {
                        e.returnValue = false;
                        if (e.preventDefault) {
                            e.preventDefault();
                        }
                    }
                }

                function isAlphaNumeric(e) {

                    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                    var ret = ((keyCode >= 46 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11);
                    return ret;
                }

                var decimalpoint = eval('<%=agentDecimalPoints %>');

                function calculateCollectedAmt() {

                    var totalFare = 0;
                    var discount = 0;
                    var comm = 0;
                    var sellingFare = 0;
                    var tax = 0;
                    var markup = 0;
                    var addlmarkup = 0;
                    var collectedAmount = 0;

                    if (document.getElementById('<%=txtSellingFare.ClientID%>').value.trim().length > 0)
                        sellingFare = document.getElementById('<%=txtSellingFare.ClientID%>').value.replace(',', '');

                    if (document.getElementById('<%=txtMarkUp.ClientID%>').value.trim().length > 0)
                        markup = document.getElementById('<%=txtMarkUp.ClientID%>').value.replace(',', '');

                    if (document.getElementById('<%=txtAmount2.ClientID%>').value.trim().length > 0)
                        addlmarkup = document.getElementById('<%=txtAmount2.ClientID%>').value.replace(',', '');

                    tax = document.getElementById('<%=txtTax.ClientID%>').value.replace(',', '');
                    discount = document.getElementById('<%=txtDiscount.ClientID%>').value.replace(',', '');

                    collectedAmount = eval(sellingFare) + eval(tax) + eval(markup) + eval(addlmarkup) - eval(discount);
                    //CalculateVAT();
                    if (isNaN(collectedAmount)) {
                        collectedAmount = 0.00;
                    }
                    document.getElementById('<%=txtCollected.ClientID%>').value = parseFloat(collectedAmount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                    CalculateCashAmount();


                }


                function calculateTotalFare() {
                    var sellingFare = 0;
                    var markup = 0;
                    var addlMarkup = 0;
                    var tax = 0;
                    var netVAT = 0;
                    var totalFare = 0;

                    var id = document.getElementById('<%=txtSellingFare.ClientID%>').id;
                    Set(id);
                    document.getElementById('<%=txtCommper.ClientID%>').value = '0.00';
                    document.getElementById('<%=txtCommisionAmt.ClientID%>').value = '0.00';
                    document.getElementById('<%=txtDiscountper.ClientID%>').value = '0.00';
                    document.getElementById('<%=txtDiscountAmt.ClientID%>').value = '0.00';
                    document.getElementById('<%=txtCommision.ClientID%>').value = '0.00';
                    document.getElementById('<%=txtDiscount.ClientID%>').value = '0.00';

                    //total fare calc
                    if (document.getElementById('<%=txtSellingFare.ClientID%>').value.trim().length > 0) {
                        sellingFare = eval(document.getElementById('<%=txtSellingFare.ClientID%>').value.replace(/,/g, ''));
                document.getElementById('<%=txtCommisionOn.ClientID%>').value = sellingFare;
                document.getElementById('<%=txtDiscountOn.ClientID%>').value = sellingFare;
                    }
                    else {
                        if (document.getElementById('ctl00_cphTransaction_txtSellingFare').value.trim().length <= 0) {
                            document.getElementById('ctl00_cphTransaction_txtCommisionOn').value = sellingFare;
                            document.getElementById('ctl00_cphTransaction_txtDiscountOn').value = sellingFare;
                        }

                    }

                    if (document.getElementById('<%=txtTax.ClientID%>').value.trim().length > 0)
                        tax = eval(document.getElementById('<%=txtTax.ClientID%>').value.replace(/,/g, ''));

                    var totalFare = sellingFare + tax;
                    if (isNaN(totalFare)) {
                        totalFare = 0.00;
                    }
                    //VAT Calculation 

                    document.getElementById('<%=txtTotalFare.ClientID%>').value = parseFloat(totalFare).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });


                    calculateCollectedAmt();
                    CalculateTax();
                    CalculateNetPay();
                    calculateProfit();
                    CalculateCashAmount();
                    CalculateVAT();
                    CheckSellingfareValid();
                }

                function CalculateCollectedAmountForVAT() {
                    var totalFare = 0;
                    var discount = 0;
                    var comm = 0;
                    var sellingFare = 0;
                    var tax = 0;
                    var markup = 0;
                    var addlmarkup = 0;
                    var collectedAmount = 0;
                    var transFee = 0;
                    var addlTransFee = 0;

                    if (document.getElementById('<%=txtSellingFare.ClientID%>').value.trim().length > 0)
                        sellingFare = document.getElementById('<%=txtSellingFare.ClientID%>').value.replace(/,/g, '');

                    if (document.getElementById('<%=txtMarkUp.ClientID%>').value.trim().length > 0)
                        markup = document.getElementById('<%=txtMarkUp.ClientID%>').value.replace(/,/g, '');

                    if (document.getElementById('<%=txtAmount2.ClientID%>').value.trim().length > 0)
                        addlmarkup = document.getElementById('<%=txtAmount2.ClientID%>').value.replace(/,/g, '');

                    tax = document.getElementById('<%=txtTax.ClientID%>').value.replace(/,/g, '');
                    discount = document.getElementById('<%=txtDiscount.ClientID%>').value.replace(/,/g, '');

                    collectedAmount = eval(sellingFare) + eval(tax) + eval(markup) + eval(addlmarkup) - eval(discount);
                    if (isNaN(collectedAmount)) {
                        collectedAmount = 0.00;
                    }
                    document.getElementById('<%=txtCollected.ClientID%>').value = parseFloat(collectedAmount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });

                    if (document.getElementById('ctl00_cphTransaction_txtSellingFare').value.trim().length > 0) {
                        sellingFare = eval(document.getElementById('ctl00_cphTransaction_txtSellingFare').value.replace(/,/g, ''));
                    }

                    if (document.getElementById('ctl00_cphTransaction_txtTax').value.trim().length > 0)
                        tax = eval(document.getElementById('ctl00_cphTransaction_txtTax').value.replace(/,/g, ''));

                    if (document.getElementById('<%=txtMarkUp.ClientID%>').value.trim().length > 0)
                        markup = eval(document.getElementById('<%=txtMarkUp.ClientID%>').value.replace(/,/g, ''));

                    if (document.getElementById('<%=txtAmount2.ClientID%>').value.trim().length > 0)
                        addlmarkup = eval(document.getElementById('<%=txtAmount2.ClientID%>').value.replace(/,/g, ''));

                    if (document.getElementById('<%=txtTransFee.ClientID%>').value.trim().length > 0)
                        transFee = eval(document.getElementById('<%=txtTransFee.ClientID%>').value.replace(/,/g, ''));
                    if (document.getElementById('<%=txtAddlTransFee.ClientID%>').value.trim().length > 0)
                        addlTransFee = eval(document.getElementById('<%=txtAddlTransFee.ClientID%>').value.replace(/,/g, ''));

                    discount = eval(document.getElementById('<%=txtDiscount.ClientID%>').value.replace(/,/g, ''));

                    var cashAmount = sellingFare + tax + markup + addlmarkup - transFee - addlTransFee - discount;
                    //CalculateVAT();
                    document.getElementById('<%=txtcashAmount.ClientID%>').value = parseFloat(cashAmount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });;

                }


                var inputVATcharge = 0;
                var inputVATcostIncluded = "";
                var outputVATcharge = 0;
                var outputVATappliedOn = "";
                function CalculateVAT() {

                    var decimalpoint = eval('<%=agentDecimalPoints %>');
                    var origin = document.getElementById('<%=ddlSector1.ClientID%>').value;
                    var destination = document.getElementById('<%=ddldestination.ClientID%>').value;
                    var sellingFair = document.getElementById('<%=txtSellingFare.ClientID%>').value.replace(/,/g, '');
                    var markup = document.getElementById('<%=txtMarkUp.ClientID%>').value;
                    var addlmarkup = document.getElementById('<%=txtAmount2.ClientID%>').value;
                    var transFee = document.getElementById('<%=txtTransFee.ClientID%>').value;
                    var addltransFee = document.getElementById('<%=txtAddlTransFee.ClientID%>').value;
                    var totMarkup = eval(markup) + eval(addlmarkup) + eval(transFee) + eval(addltransFee);
                    if (eval(sellingFair) > 0 && origin != '' && origin != '-1' && destination != '' && destination != '-1') {
                        var paramList = 'sellingFair=' + sellingFair + '&decimalpoint=' + decimalpoint + '&origin=' + origin + '&destination=' + destination + '&totMarkup=' + totMarkup;
                        Ajax.onreadystatechange = CalculateVATComplete;
                        Ajax.open("POST", "OffLineEntryAjax.aspx");
                        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                        Ajax.send(paramList);
                    }
                }

                function CalculateVATComplete() {

                    if (Ajax.readyState == 4) {
                        if (Ajax.status == 200) {
                            if (Ajax.responseText.length > 0) {
                                var arrayType = "";
                                if (Ajax.responseText.indexOf('#') != "-1") {
                                    //alert(Ajax.responseText);
                                    arrayType = Ajax.responseText.split('#');
                                    if (arrayType[0].indexOf('I') == 0) {
                                        inputVATcharge = arrayType[0].split('|')[0].replace('I', '');
                                        inputVATcostIncluded = arrayType[0].split('|')[1];
                                    }
                                    if (arrayType[1].length > 1 && arrayType[1].indexOf('O') == 0) {
                                        outputVATcharge = arrayType[1].split('|')[0].replace('O', '');
                                        outputVATappliedOn = arrayType[1].split('|')[1];
                                    }
                                }
                                else {
                                    if (Ajax.responseText != "") {
                                        arrayType = Ajax.responseText.split('|');
                                        if (arrayType[0].indexOf('I') == 0) {
                                            inputVATcharge = arrayType[0];
                                            inputVATcostIncluded = arrayType[1];
                                        }
                                        if (arrayType[1].indexOf('O') == 0)
                                            outputVATcharge = arrayType[0];
                                        outputVATappliedOn = arrayType[1];
                                    }
                                }

                                if (inputVATcostIncluded == "false") {

                                    var totFair = eval(document.getElementById('<%=txtTotalFare.ClientID%>').value.replace(/,/g, ''));
                                    inputVATcharge = totFair * (inputVATcharge / 100);
                                    document.getElementById('<%=txtTotalFare.ClientID%>').value = parseFloat(eval(inputVATcharge) + eval(totFair)).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                                    document.getElementById('<%=TxtInputVAT.ClientID%>').value = parseFloat(inputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                                    document.getElementById('<%=txtTotalFare.ClientID%>').value = eval(document.getElementById('<%=txtTotalFare.ClientID%>').value) + eval(inputVATcharge);

                                }
                                if (outputVATcharge > 0 || inputVATcharge > 0) {
                                    CalculateCollectedAmountForVAT();
                                    var collAmount = document.getElementById('<%=txtCollected.ClientID%>').value.replace(/,/g, '');
                            outputVATcharge = eval(collAmount) * (outputVATcharge / 100);
                            var cashamt = eval(document.getElementById('<%=txtcashAmount.ClientID%>').value.replace(/,/g, ''));
                            var netVAT = eval(outputVATcharge) + eval(inputVATcharge);
                            document.getElementById('<%=txtcashAmount.ClientID%>').value = parseFloat(eval(cashamt) + eval(outputVATcharge)).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                            document.getElementById('<%=txtCollected.ClientID%>').value = parseFloat(eval(outputVATcharge) + eval(collAmount)).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                            document.getElementById('<%=TxtInputVAT.ClientID%>').value = parseFloat(inputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                            document.getElementById('<%=txtOutPutVAT.ClientID%>').value = parseFloat(outputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                            document.getElementById('<%=txtNetVAT.ClientID%>').value = parseFloat(netVAT).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });

                                }


                            }
                            else {
                                document.getElementById('<%=TxtInputVAT.ClientID%>').value = '0.00';
                                document.getElementById('<%=txtOutPutVAT.ClientID%>').value = '0.00';
                                document.getElementById('<%=txtNetVAT.ClientID%>').value = '0.00';
                                CalculateCollectedAmountForVAT();
                            }

                        }
                    }
                }

                function GetTaxValue() {
                    var taxval = eval(document.getElementById('<%=txtTax.ClientID%>').value.trim());
                    document.getElementById('hdnTaxAmt').value = taxval.toFixed(decimalpoint);
                }

                function CalculateTax() {
                    var taxValue = 0;
                    var sellingFare = 0;
                    var tax = 0;
                    //CalculateVAT();
                    var taxcount = eval(document.getElementById('ctl00_cphTransaction_hndTaxCounter').value);
                    for (var i = 0; i <= eval(taxcount); i++) {
                        if (i == 0) {
                            if (document.getElementById('ctl00_cphTransaction_txtValue' + i).value.length > 0) {
                                Set('ctl00_cphTransaction_txtValue' + i);
                                taxValue += eval(document.getElementById('ctl00_cphTransaction_txtValue' + i).value.replace(/,/g, ''));
                            }

                        }
                        else {
                            if (document.getElementById('txtValue' + i) != null && document.getElementById('txtValue' + i).value.length > 0) {
                                Set('txtValue' + i);
                                taxValue += eval(document.getElementById('txtValue' + i).value.replace(/,/g, ''));
                            }
                        }
                    }
                    if (isNaN(taxValue)) {
                        taxValue = 0.00;
                    }
                    document.getElementById('<%=txtTax.ClientID%>').value = parseFloat(taxValue).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });;

                    if (document.getElementById('ctl00_cphTransaction_txtSellingFare').value.trim().length > 0) {
                        sellingFare = eval(document.getElementById('ctl00_cphTransaction_txtSellingFare').value.replace(/,/g, ''));
                        document.getElementById('ctl00_cphTransaction_txtCommisionOn').value = parseFloat(sellingFare).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                document.getElementById('ctl00_cphTransaction_txtDiscountOn').value = parseFloat(sellingFare).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                    }

                    if (document.getElementById('ctl00_cphTransaction_txtTax').value.trim().length > 0)
                        tax = eval(document.getElementById('ctl00_cphTransaction_txtTax').value.replace(/,/g, ''));

                    var totalFare = sellingFare + tax;
                    if (isNaN(totalFare)) {
                        totalFare = 0.00;
                    }
                    document.getElementById('ctl00_cphTransaction_txtTotalFare').value = parseFloat(totalFare).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });

                    calculateCollectedAmt();
                    CalculateNetPay();
                    CalculateCashAmount();
                    CalculateVAT();
                }

                function CalculateCashAmount() {

                    var sellingFare = 0;
                    var tax = 0;
                    var discount = 0;
                    var markup = 0;
                    var addlmarkup = 0;
                    var transFee = 0;
                    var addlTransFee = 0;

                    if (document.getElementById('ctl00_cphTransaction_txtSellingFare').value.trim().length > 0) {
                        sellingFare = eval(document.getElementById('ctl00_cphTransaction_txtSellingFare').value.replace(/,/g, ''));
                    }

                    if (document.getElementById('ctl00_cphTransaction_txtTax').value.trim().length > 0)
                        tax = eval(document.getElementById('ctl00_cphTransaction_txtTax').value.replace(/,/g, ''));

                    if (document.getElementById('<%=txtMarkUp.ClientID%>').value.trim().length > 0)
                        markup = eval(document.getElementById('<%=txtMarkUp.ClientID%>').value.replace(/,/g, ''));

                    if (document.getElementById('<%=txtAmount2.ClientID%>').value.trim().length > 0)
                        addlmarkup = eval(document.getElementById('<%=txtAmount2.ClientID%>').value.replace(/,/g, ''));

                    if (document.getElementById('<%=txtTransFee.ClientID%>').value.trim().length > 0)
                        transFee = eval(document.getElementById('<%=txtTransFee.ClientID%>').value.replace(/,/g, ''));
                    if (document.getElementById('<%=txtAddlTransFee.ClientID%>').value.trim().length > 0)
                        addlTransFee = eval(document.getElementById('<%=txtAddlTransFee.ClientID%>').value.replace(/,/g, ''));

                    discount = eval(document.getElementById('<%=txtDiscount.ClientID%>').value.replace(/,/g, ''));

                    var cashAmount = sellingFare + tax + markup + addlmarkup - transFee - addlTransFee - discount;
                    //CalculateVAT();
                    document.getElementById('<%=txtcashAmount.ClientID%>').value = parseFloat(cashAmount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });;
                    CalculateVAT();
                }

                function CalculateNetPay() {
                    var sellingFare = 0;
                    var tax = 0;
                    var comm = 0;
                    var discount = 0;
                    var inputVAT = 0;
                    var outputVAT = 0;
                    var Netpay = 0;
                    var markup = 0;
                    var addlMarkup = 0;
                    var netVAT = 0;
                    //CalculateVAT();
                    if (document.getElementById('<%=txtSellingFare.ClientID%>').value.trim().length > 0)
                        var sellingFare = eval(document.getElementById('<%=txtSellingFare.ClientID%>').value.replace(/,/g, ''));
                    if (document.getElementById('<%=txtTax.ClientID%>').value.trim().length > 0)
                        tax = eval(document.getElementById('<%=txtTax.ClientID%>').value.replace(/,/g, ''));
                    if (document.getElementById('<%=txtCommision.ClientID%>').value.trim().length > 0)
                        comm = eval(document.getElementById('<%=txtCommision.ClientID%>').value.replace(/,/g, ''));

                    var Netpay = sellingFare + tax - comm;

                    if (isNaN(Netpay)) {
                        Netpay = 0.00;
                    }
                    document.getElementById('<%=txtNetpayable.ClientID%>').value = parseFloat(Netpay).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                    CalculateCashAmount();
                    CalculateVAT();
                }

                function calculateProfit() {

                    var markup = 0;
                    var amount2 = 0;
                    var profit = 0;
                    var transFee = 0;
                    var addlTransFee = 0;
            //CalculateVAT();
           <%-- if(document.getElementById('<%=txtCollected.ClientID%>').value.trim().length>0)
                collectedAmt = eval(document.getElementById('<%=txtCollected.ClientID%>').value);--%>
                    if (document.getElementById('<%=txtMarkUp.ClientID%>').value.trim() > 0) {
                        var id = document.getElementById('<%=txtMarkUp.ClientID%>').id;
                Set(id);
                markup = eval(document.getElementById('<%=txtMarkUp.ClientID%>').value.replace(/,/g, ''));
                    }
                    if (document.getElementById('<%=txtAmount2.ClientID%>').value.trim() > 0) {
                        var id = document.getElementById('<%=txtAmount2.ClientID%>').id;
                Set(id);
                amount2 = eval(document.getElementById('<%=txtAmount2.ClientID%>').value.replace(/,/g, ''));
                    }
                    if (document.getElementById('<%=txtTransFee.ClientID%>').value.trim() > 0) {
                        var id = document.getElementById('<%=txtTransFee.ClientID%>').id;
                Set(id);
                transFee = eval(document.getElementById('<%=txtTransFee.ClientID%>').value.replace(/,/g, ''));
                    }

                    if (document.getElementById('<%=txtAddlTransFee.ClientID%>').value.trim() > 0) {
                        var id = document.getElementById('<%=txtAddlTransFee.ClientID%>').id;
                Set(id);
                addlTransFee = eval(document.getElementById('<%=txtAddlTransFee.ClientID%>').value.replace(/,/g, ''));
                    }

                    var profit = (markup + amount2 + transFee + addlTransFee);

                    if (isNaN(profit)) {
                        profit = 0.00;
                    }
                    document.getElementById('<%=txtProfit.ClientID%>').value = parseFloat(profit).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });

                    calculateCollectedAmt();
                    CalculateCashAmount();
                    CalculateVAT();
                }

                function RemoveRow(e) {
                    try {

                        var val = e.id.substring(3);
                        RemoveTaxCodeValues(val);
                        var taxCount = document.getElementById('ctl00_cphTransaction_hndTaxCounter').value;

                        for (var i = 0; i < eval(taxCount); i++) {
                            if (document.getElementById("spanT" + (eval(i) + 1)) != null) {
                                var txtCode = document.getElementById("spanT" + (eval(i) + 1));
                                var txtValue = document.getElementById("spanV" + (eval(i) + 1));
                                var lnk = document.getElementById("spanlnk" + (eval(i) + 1));
                                var clearDiv = document.getElementById("clearDiv" + (eval(i) + 1));
                                document.getElementById("taxDiv").removeChild(clearDiv);
                                document.getElementById("taxDiv").removeChild(txtCode);
                                document.getElementById("taxDiv").removeChild(txtValue);
                                document.getElementById("taxDiv").removeChild(lnk);
                            }
                        }
                        var taxDetailsToBind = document.getElementById('ctl00_cphTransaction_hndTaxCodeValue').value;


                        //document.getElementById('ctl00_cphTransaction_hndTaxCodeValue').value = taxCodeValues;
                        document.getElementById('ctl00_cphTransaction_hndTaxCounter').value = eval(taxCount - 1);
                        document.getElementById('TaxErrorMsg').style.display = 'none';
                        ReLoadingTaxControlsFromEdit();
                        $('#showParam').collapse('hide');
                        CalculateTax();

                    }
                    catch (ex) {
                        taxCounter = 0;
                    }

                }

                function TaxCounterTozero() {
                    taxCounter = 0;
                    $('#showParam').collapse('hide');
                    document.getElementById('<%=hdnIsFromEdit.ClientID%>').value = "false";
                    var segcount = eval(document.getElementById('<%=hndsegmentsCount.ClientID%>').value);
                    if (segcount > 0)
                        ShowSectorTable(segcount);

                }
                //AddTaxAmounts
                var taxamount = 0;
                function AddTaxAmounts(e) {
                    // document.getElementById('hndTaxCodeValue').value=e.currentTarget.id +","+e.currentTarget.value;
                    //parseFloat(document.getElementById('hdnTaxAmounts').value.trim());
                    //taxamount += parseFloat(e.target.value);
                    //document.getElementById('hdnTaxAmounts').value = taxamount ;
                    CalculateTax();
                }

                function BindTaxCodeValues() {
                    var value = '';
                    var code0 = document.getElementById('ctl00_cphTransaction_txtCode0').value;
                    var value0 = document.getElementById('ctl00_cphTransaction_txtValue0').value;
                    value = 0 + "-" + code0 + "," + value0;
                    for (var i = 1; i <= taxCounter; i++) {
                        value += "|" + i + "-" + document.getElementById('txtCode' + i).value + "," + document.getElementById('txtValue' + i).value;
                    }
                    document.getElementById('<%=hndTaxCodeValue.ClientID%>').value = value;
                }

                function RemoveTaxCodeValues(val) {
                    var taxCount = document.getElementById('ctl00_cphTransaction_hndTaxCounter').value;
                    var id = val;
                    var value = '';
                    var code0 = document.getElementById('ctl00_cphTransaction_txtCode0').value;
                    var value0 = document.getElementById('ctl00_cphTransaction_txtValue0').value;
                    value = 0 + "-" + code0 + "," + value0;
                    for (var i = 1; i <= taxCount; i++) {
                        if (i >= id) {
                            if (i > id) {
                                var Newid = i - 1;
                                value += "|" + Newid + "-" + document.getElementById('txtCode' + i).value + "," + document.getElementById('txtValue' + i).value;
                            }
                        }
                        else {
                            value += "|" + i + "-" + document.getElementById('txtCode' + i).value + "," + document.getElementById('txtValue' + i).value;
                        }
                    }
                    document.getElementById('<%=hndTaxCodeValue.ClientID%>').value = value;
                }

                function AddTaxCodeToHnd(e) {

                    var id = e.currentTarget.id.replace('txtCode', '').replace('txtValue', '');
                    if (document.getElementById('txtCode' + id).value.trim().length > 0) {
                        var code = document.getElementById('txtCode' + id).value;
                        if (e.currentTarget.value != "") {
                            var value = document.getElementById('<%=hndTaxCodeValue.ClientID%>').value;
                            if (value.length == 0) {
                                var code0 = document.getElementById('ctl00_cphTransaction_txtCode0').value;
                                var value0 = document.getElementById('ctl00_cphTransaction_txtValue0').value;
                                value = 0 + "-" + code0 + "," + value0;
                            }
                            if (value.length > 0)
                                value = value + "|";
                            document.getElementById('<%=hndTaxCodeValue.ClientID%>').value = value + id + "-" + code + "," + e.currentTarget.value;
                        }
                    }
                }
                var isValid = true;
                function CheckDuplicateDynamicSectors(e) {
                    if (e.id.split('_')[2] == "ddlFromSector1") {
                        if (document.getElementById('<%=ddlSector1.ClientID%>').value.length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector1.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';

                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');



                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector1") {
                        if (document.getElementById('<%=ddlSector2.ClientID%>').value.length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector2.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector2") {
                        if (document.getElementById('<%=ddlSector2.ClientID %>').value.length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector2.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'


                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector2") {
                        if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');



                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }
                    }
                    if (e.id.split('_')[2] == "ddlFromSector3") {
                        if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector3") {
                        if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector4") {
                        if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector4") {
                        if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector5") {
                        if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                    if (e.id.split('_')[2] == "ddlToSector5") {
                        if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector6") {
                        if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector6") {
                        if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector7") {
                        if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector7") {
                        if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                    if (e.id.split('_')[2] == "ddlFromSector8") {
                        if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector8") {
                        if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector9") {
                        if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                    if (e.id.split('_')[2] == "ddlToSector9") {
                        if (document.getElementById('<%=ddlSector10.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                    if (document.getElementById('<%=ddlSector10.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                }

                function CheckDuplicatesectors(id) {
                    var isDuplicate = true;
                    var originVal = document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + id).innerText.trim();
                    //var controlID = event.controlID;
                    for (var i = 1; i < 10; i++) {
                        if (id == i)
                            continue;
                        if (originVal.length > 0 && document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + i).innerText.trim() != "Select") {
                            if (originVal == document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + i).innerText.trim()) {
                                //document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + id).value = "-1";
                                document.getElementById('sectors').style.display = 'block';
                                document.getElementById('sectors').innerHTML = "Sector Can't be Duplicated";
                                document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + i).value = "-1";
                                return isDuplicate = false;
                            }
                            else
                                document.getElementById('sectors').style.display = 'none';
                        }
                    }
                    //autoCompInit1();
                    //return isDuplicate;
                }

          <%--  function CheckDuplicateDestinationSector() {
                if (document.getElementById('<%=txtOrigin.ClientID%>').value.trim().length > 0 && document.getElementById('<%=txtDestination.ClientID%>').value.trim().length > 0) {
                    if (document.getElementById('<%=txtOrigin.ClientID%>').value.toLowerCase().trim() == document.getElementById('<%=txtDestination.ClientID%>').value.toLowerCase().trim()) {
                        document.getElementById('<%=txtDestination.ClientID%>').value = "";
                    document.getElementById('<%=txtDestination.ClientID%>').focus();
                    document.getElementById('origin').style.display = 'block';
                    document.getElementById('origin').innerHTML = "Origin And Destination Should not be same";

                }
                else
                    document.getElementById('origin').style.display = 'none';
            }
        }--%>



                function ReLoadingTaxControls() {
                    try {

                        var segcount = eval(document.getElementById('<%=hndsegmentsCount.ClientID%>').value);
                        if (segcount > 0)
                            ShowSectorTable(segcount);
                        var taxCounter = 0;
                        var taxCount = document.getElementById('<%=hndTaxCounter.ClientID%>').value;
                        var taxDetailsToBind = document.getElementById('<%=hndTaxCodeValue.ClientID%>').value;
                        for (var i = 0; i < eval(taxCount); i++) {
                            var res = taxDetailsToBind.split('|');
                            if (taxDetailsToBind == "") {
                                AddNewRow('', '', true);
                            }
                            if (taxDetailsToBind != "") {
                                var tax = res[i].split('-');
                                if (i == 0 && eval(tax[0]) == 0) {
                                    document.getElementById('<%=txtCode0.ClientID%>').value = tax[1].split(',')[0];
                            document.getElementById('<%=txtValue0.ClientID%>').value = tax[1].split(',')[1];
                                }
                                else {
                                    if ((i + 1) == eval(tax[0])) {
                                        AddNewRow(tax[1].split(',')[0], tax[1].split(',')[1], true);
                                    }
                                }
                            }
                        }
                    }
                    catch (ex) {
                        taxCounter = 0;
                    }
                }

                function ReLoadingTaxControlsFromEdit() {
                    taxCounter = 0;

                    var taxCount = document.getElementById('<%=hndTaxCounter.ClientID%>').value;
                    var taxDetailsToBind = document.getElementById('<%=hndTaxCodeValue.ClientID%>').value;

                    for (var i = 0; i <= eval(taxCount); i++) {
                        if (i == 0) {
                            var res = taxDetailsToBind.split('|');
                            var tax = res[i].split('-');
                            if (i == 0 && eval(tax[0]) == 0) {
                                document.getElementById('<%=txtCode0.ClientID%>').value = tax[1].split(',')[0];
                        document.getElementById('<%=txtValue0.ClientID%>').value = tax[1].split(',')[1];
                            }
                        }
                        else {
                            var res = taxDetailsToBind.split('|');
                            if (i != res.length) {
                                var tax = res[i].split('-');
                                if (i == eval(tax[0])) {
                                    AddNewRow(tax[1].split(',')[0], tax[1].split(',')[1], true);
                                }
                            }
                        }
                    }

                }

                function ChangeWidthForCalender() {
                    var segmentCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    //if (eval(segmentCount) > 0) {
                    // for (var i = 1; i <= eval(segmentCount) ; i++) {
                    var Deptdate = document.getElementById('ctl00_cphTransaction_dcFromDate_Date');// + i + '_Date');
                    var Depttime = document.getElementById('ctl00_cphTransaction_dcFromDate_Time');// + i + '_Time');
                    //var Arrdate = document.getElementById('ctl00_cphTransaction_txtArrivalDateTime' + i + '_Date');
                    //var Arrtime = document.getElementById('ctl00_cphTransaction_txtArrivalDateTime' + i + '_Time');
                    Deptdate.style['width'] = '100px';
                    Depttime.style['width'] = '50px';
                    //Arrdate.style['width'] = '100px';
                    //Arrtime.style['width'] = '50px';

                    //var depDateArray = Deptdate.value.split('-');
                    //var MonthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "sep", "Oct", "Nov", "Dec"];
                    ////MonthArray[]
                    //var depdate = new Date(depDateArray[2], depDateArray[1]-1, depDateArray[0]); 

                    ////var returndate = new Date(date2.getFullYear(),date2.getMonth(),date2.getDate());  
                    ////var difference = returndate.getTime() - 
                    //dt1 = new Date(2018,08, 05,11,10);
                    //dt1 = new Date(2018, 08, 05, 11, 13);

                    //var diff = (dt2.getTime() - dt1.getTime()) / 1000;
                    //diff /= 60;
                    //alert(Math.abs(Math.round(diff)));

                    //  }

                    //}
                }

            </script>

            <uc1:DateControl ID="dcLicExpDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" Visible="false" style="width: 100px;"></uc1:DateControl>

            <!-- SmartMenus core CSS (required) -->
            <%--  <link href="App_Themes/Pink/BookingStyle.css" rel="stylesheet" />
    <link href="App_Themes/Pink/cozmovisa-style.css" rel="stylesheet" />
    <link href="App_Themes/Pink/Default.css" rel="stylesheet" />
    <link href="App_Themes/Pink/main-style.css" rel="stylesheet" />
    <link href="App_Themes/Pink/style.css" rel="stylesheet" />--%>

            <asp:HiddenField ID="airlineCodeRet" runat="server" />
            <asp:HiddenField ID="airlineNameRet" runat="server" />
            <asp:HiddenField ID="hndTaxCodeValue" runat="server" />
            <asp:HiddenField ID="hdnEditMode" runat="server" Value="" />
            <asp:HiddenField ID="hdnFlightId" runat="server" Value="-1" />
            <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
            <div id="errMess" class="error_module" style="display: none;">
                <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                </div>
            </div>
            <div class="clear" style="margin-left: 25px">
                <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
                </div>
            </div>
            <div class="clear" style="margin-left: 30px">
                <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
                </div>
            </div>
            <div class="clear" style="margin-left: 30px">
                <div id="container3" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
                </div>
            </div>


            <input type="hidden" id="hdnTaxAmt" name="hdnTaxAmt" value="0" />
            <input type="hidden" id="hdnTaxAmounts" name="hdnTaxAmounts" value="0" />
            <input type="hidden" id="hdnSectorList" name="hdnSectorList" value="" runat="server" />
            <input type="hidden" id="hdnSelectedIndex" name="hdnSelectedIndex" runat="server" />
            <input type="hidden" id="hdnIsFromEdit" name="hdnIsFromEdit" runat="server" value="false" />
            <input type="hidden" id="hdnRemovePax" name="hdnRemovePax" runat="server" />
            <input type="hidden" id="hndTaxCounter" name="hdnTaxCounter" runat="server" value="0" />
            <input type="hidden" id="hdnSelectedTabName" name="hdnSelectedTabName" value="" runat="server" />
            <input type="hidden" id="hdnSaveItinerary" name="hdnSaveItinerary" value="" runat="server" />
            <input type="hidden" id="hdnPaxAdded" name="hdnPaxAdded" runat="server" value="false" />



            <asp:HiddenField ID="hndsegmentsCount" runat="server" Value="0" />
            <asp:HiddenField ID="hdnErrorMsg" runat="server" Value="" />
            <asp:HiddenField ID="hdnsegments" runat="server" Value="" />
            <asp:HiddenField ID="hdnAgentId" runat="server" Value="" />

            <div>
                <div class="body_container">

                    <%-- <span id="ctl00_upnllocation">--%>

                    <script type="text/javascript" lang="javascript">        
</script>

                    <span id="ctl00_cphTransaction_lblError" style="color: Red"></span>
                    <a class="showParamToggle" style="display:none" role="button" data-toggle="collapse" href="#showParam" aria-expanded="true" aria-controls="showParam">
                        <span class="fa fa-plus-square pr-2"></span>Import PNR
                    </a>

                    <div class="collapse in" id="showParam">
                        <div class="paramcon">

                            <div class="row px-3 px-md-0">

                                <div class="col-md-2 mt-5 pt-1">
                                    <asp:RadioButton ID="rbtnSelf" runat="server" GroupName="book" onchange="disablefield();"
                                        Font-Bold="True" Text="Self" Checked="true" />
                                    &nbsp;
                   
                    <asp:RadioButton ID="rbtnAgent" runat="server" GroupName="book" onchange="disablefield();"
                        Font-Bold="True" Text="Agency" />

                                </div>
                                <div class="col-md-4 mt-5 pt-1" id="wrapper" style="display: none">
                                    <div class="form-group">
                                        <asp:UpdatePanel ID="agentbalance" runat="server">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <asp:DropDownList runat="server" ID="ddlAgent" CssClass="form-control" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                    <asp:Label ID="lblAgentBalance" CssClass="col-md-6" runat="server" Text="" Style="display: none"></asp:Label>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>PNR No. </label>
                                        <asp:TextBox ID="txtPnr" runat="server" CssClass="form-control" MaxLength="10" onkeypress="return isAlphaNumeric(event);"></asp:TextBox>
                                        <%--
                                        <input name="ctl00$cphTransaction$txtPnr" type="text" id="ctl00_cphTransaction_txtPnr" class="form-control" />
                                        --%>
                                    </div>

                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Select Source </label>
                                        <asp:DropDownList runat="server" ID="ddlSource" CssClass="form-control"></asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group">
                                        <label>&nbsp; </label>

                                        <div>

                                            <%-- <a class="btn but_b" data-toggle="modal" data-target="#FareDiff">Search</a>
                                            --%>


                                            <asp:Button runat="server" CssClass="btn but_b" ID="btnRetrieve" Text="Retrieve Information" OnClientClick="return Validate();" OnClick="btnRetrieve_Click" />
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="">
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- Content Search -->

                    <!-- end search -->

                    <%--            </span>--%>

                    <asp:HiddenField ID="paymentmode" runat="server" />
                    <asp:HiddenField ID="hndId" runat="server" />
                    <div id="EditPNRDiv" style="display: none" runat="server">
                        <div class="col-container pt-4">
                            <!--left panel-->
                            <div id="LeftPanel" class="col2">
                                <a href="~/OffLineEntry.aspx" style="display:none" class="backToResults" runat="server"><< Back to Search</a>
                                <div class="right_col CorpTrvl-page">
                                    <div class="row">
                                        <div class="col-md-12 CorpTrvl-tabbed-panel mt-2 mb-0">
                                            <ul class="nav nav-tabs" id="Tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#ticket_tab" id="TicketTab" onclick="navigateTab(1);">Ticket </a>
                                                </li>

                                                <li>
                                                    <a data-toggle="tab" href="#sectors_tab" id="sectortab" onclick="navigateTab(2);">Sectors </a>
                                                </li>

                                                <li>
                                                    <a data-toggle="tab" href="#UDID" id="UDIDtab" onclick="navigateTab(3);">UDID </a>
                                                </li>

                                            </ul>

                                            <div class="tab-content p-2">
                                                <div id="ticket_tab" class="tab-pane fade in active">
                                                    <h4>
                                                        <strong>Ticket </strong>
                                                        <span class="float-right" id="spnagentBalance" style="display:none" >
                                                            <small class="text-capitalize">
                                                                Agent Balance:
                                                                <strong id="lblCusBalance"></strong></small>
                                                        </span>
                                                    </h4>

                                                    <div class="row no-gutter">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>PNR<span class="red_span">*</span></label>
                                                                <asp:TextBox ID="txtPNRNum" runat="server" class="form-control" ondrop="return false;" onpaste="return true;" MaxLength="10" onblur=" CheckPNRValid();"></asp:TextBox>
                                                                <b class="red_span" style="display: none" id="PNR"></b>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Airline<span class="red_span">*</span></label>
                                                                <asp:DropDownList ID="ddlairline" runat="server" CssClass="form-control" onchange="bindRepAirline()"></asp:DropDownList>
                                                                <%-- <asp:TextBox ID="txtPreferredAirlineRet" CssClass="form-control" Text="Type Preferred Airline"
                                            runat="server" onclick="IntDom('statescontainer5' , 'ctl00_cphTransaction_txtPreferredAirlineRet')"
                                            onblur="markout(this, 'Type Preferred Airline')" onfocus="markin(this, 'Type Preferred Airline')" onkeypress="autoCompInitPrefAirline1()"></asp:TextBox>
                                                            <div id="statescontainer5" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                                display: none; z-index: 103;">
                                                            </div>--%>
                                                                <b class="red_span" style="display: none" id="AirLine"></b>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Ticket Number<span class="red_span">*</span></label>
                                                                <asp:TextBox ID="txtNumber" runat="server" class="form-control" onkeypress="return isAlphaNumeric(event);" MaxLength="50" onchange=" CheckTicketNoValid();"></asp:TextBox>
                                                                <b class="red_span" style="display: none" id="Number"></b>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Rep AirLine</label>
                                                                <asp:DropDownList ID="ddlRepAirline" runat="server" CssClass="form-control"></asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Sales Date<span class="red_span">*</span></label>

                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtSalesDate" runat="server" data-calendar-contentid="#container1" class="inputEnabled form-control" Enabled="false" onchange="CheckSalesDtValid();" onblur=" CheckSalesDtValid();"></asp:TextBox>
                                                                    <div class="input-group-addon p-0">
                                                                        <a href="javascript:void(null)" onclick="showCal1()">
                                                                            <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <b class="red_span" style="display: none" id="salesdate"></b>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Travel Date<span class="red_span">*</span></label>
                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtTravelDt" runat="server" class="form-control" data-calendar-contentid="#container2" Enabled="false" onblur=" CheckTravelDtValid();"></asp:TextBox>
                                                                    <div class="input-group-addon p-0">
                                                                        <a href="javascript:void(null)" onclick="showCal2()">
                                                                            <img id="Img5" src="images/call-cozmo.png" alt="Pick Date" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <b class="red_span" style="display: none" id="traveldate"></b>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Supplier<span class="red_span">*</span></label>
                                                              
                                                                    <%--<asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control" onchange="bindRepAirline()"></asp:DropDownList>--%>
                                                                <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control" ></asp:DropDownList>

                                                                
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="row no-gutter">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Destination<span class="red_span">*</span></label>
                                                                        <asp:DropDownList runat="server" ID="ddldestination" CssClass="form-control" onchange="javascript:BindSectors();"></asp:DropDownList>
                                                                        <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer2"></div>
                                                                        <b class="red_span" style="display: none" id="destination"></b>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Customer 1<span id="spnCustomer" class="red_span">*</span></label><%--<label id="lblCusBalance"></label>--%>
                                                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlCustomer1" onchange="Customerchange();"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">

                                                                    <div class="form-group">

                                                                        <label>Class<span class="red_span">*</span></label>
                                                                        <asp:TextBox ID="txtClassSector" runat="server" class="form-control" onkeypress="return isAlpha(event);" MaxLength="2" onblur=" CheckClassValid();"></asp:TextBox>
                                                                        <b class="red_span" style="display: none" id="classerr"></b>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">

                                                            <%--                                                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                                        <ContentTemplate>--%>
                                                            <div class="row no-gutter">

                                                                <div class="col-md">

                                                                    <div class="form-group">

                                                                        <label>Sectors<span class="red_span">*</span></label>
                                                                        <asp:DropDownList runat="server" ID="ddlSector1" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                        <%-- <asp:TextBox ID="txtSect1" runat="server" class="form-control" onchange="GetSectorsCount()" onkeypress="return isAlpha(event);" onblur="CheckDuplicatesectors(1);" MaxLength="3"></asp:TextBox>
                                                                        --%>
                                                                        <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer3"></div>
                                                                        <b class="red_span" style="display: none" id="sectors"></b>
                                                                    </div>

                                                                </div>

                                                                <div class="col-md">

                                                                    <div class="form-group">

                                                                        <label>&nbsp; </label>
                                                                        <asp:DropDownList runat="server" ID="ddlSector2" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                        <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer4"></div>

                                                                    </div>

                                                                </div>

                                                                <div class="col-md">

                                                                    <div class="form-group">

                                                                        <label>&nbsp; </label>
                                                                        <asp:DropDownList runat="server" ID="ddlSector3" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                        <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer5"></div>
                                                                    </div>

                                                                </div>

                                                                <div class="col-md">

                                                                    <div class="form-group">

                                                                        <label>&nbsp; </label>
                                                                        <asp:DropDownList runat="server" ID="ddlSector4" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                        <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer6"></div>
                                                                    </div>

                                                                </div>

                                                                <div class="col-md">

                                                                    <div class="form-group">

                                                                        <label>&nbsp; </label>
                                                                        <asp:DropDownList runat="server" ID="ddlSector5" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                        <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer7"></div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <%-- </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlSector1" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>--%>
                                                        </div>

                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <label>Type</label>
                                                                <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" AutoPostBack="true">
                                                                    <asp:ListItem Value="AD" Text="Adult"></asp:ListItem>
                                                                    <asp:ListItem Value="CHD" Text="Child"></asp:ListItem>
                                                                    <asp:ListItem Value="INF" Text="Infant"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <label>Title</label>
                                                                <asp:DropDownList CssClass="form-control" ID="ddlPaxTitle" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Pax Name<span class="red_span">*</span></label>
                                                                <asp:TextBox ID="txtPaxName" runat="server" class="form-control"
                                                                     ondrop="return false;"
                                                                    onpaste="return true;" MaxLength="50" ToolTip="First Name & last name must be seperated with '/'" onblur=" CheckPaxNameValid();"></asp:TextBox>
                                                                <b class="red_span" style="display: none" id="paxname"></b>

                                                            </div>
                                                        </div>



                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Locations<span class="red_span">*</span></label>
                                                                <asp:DropDownList ID="ddlConsultant" runat="server" CssClass="form-control"></asp:DropDownList>
                                                            </div>
                                                            <b class="red_span" style="display: none" id="locationErr"></b>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">

                                                                <label>Type Code</label>
                                                                <asp:DropDownList ID="TypeCode" runat="server" CssClass="form-control">
                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                    <asp:ListItem>Deal Code</asp:ListItem>
                                                                    <asp:ListItem>Corp Code</asp:ListItem>
                                                                    <asp:ListItem>Tour Code</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Code</label>
                                                                <asp:TextBox ID="txtTypeCode" runat="server" onkeypress="return IsAlphaNumeric(event);" MaxLength="20" CssClass="form-control"></asp:TextBox>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">

                                                                <label>Remarks</label>
                                                                <asp:TextBox ID="txtRemarks" runat="server" class="form-control"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4>
                                                         
                                                        <a class="" data-toggle="collapse" href="#ConjDetailsCollapse" role="button" aria-expanded="false" aria-controls="ConjDetailsCollapse">
														 <strong>Conj Details</strong>
												 		   <span class="expand-details">
																<i class="fa fa-plus-circle"></i>
															</span>
													    </a>
                                                    </h4>
                                                    
                                                    <div class="collapse" id="ConjDetailsCollapse">
														<div class="row no-gutter">
															<div class="col-md-12">

                                                            <div class="table-responsive">

                                                                <table class="table gridbg">

                                                                    <tr>

                                                                        <th>Ticket No.</th>
                                                                        <th>PNR Number</th>
                                                                        <th>Sector-1</th>
                                                                        <th>Sector-2</th>
                                                                        <th>Sector-3</th>
                                                                        <th>Sector-4</th>
                                                                        <th>Sector-5</th>
                                                                        <th>&nbsp;</th>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>
                                                                            <asp:TextBox ID="txtCongTicketNo" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);"></asp:TextBox>

                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtConjPNR" runat="server" class="form-control" ondrop="return false;" onpaste="return true;" MaxLength="10" onkeypress="return isAlphaNumeric(event);"></asp:TextBox></td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlSector6" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer8"></div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlSector7" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer9"></div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlSector8" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer10"></div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlSector9" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer11"></div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlSector10" CssClass="form-control" onchange="GetSectorsCount(this)" Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer12"></div>
                                                                        </td>

                                                                        <td style="font-size: 18px" width="70"></td>
                                                                    </tr>
                                                                </table>
                                                            </div>

															</div>
														</div>
													</div> 
                                                   
                                                    <h4>
                                                        <strong>Settlement Mode</strong>
                                                    </h4>

                                                    <div class="row">

                                                        <div class="col-md-5" style="max-width: none; min-width: inherit;">
                                                            <div class="x_title pd-3">

                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="x_content">
                                                                <!--Start:xcontent-->
                                                                <table id="ctl00_cphTransaction_tblSettlement" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label>
                                                                                            <span id="ctl00_cphTransaction_lblSettlementMode">Mode:</span>
                                                                                        </label>
                                                                                        <div class="custom-select">
                                                                                            <asp:DropDownList ID="ddlMode" runat="server" onchange="Changemode();" CssClass="form-control">
                                                                                                <%--<asp:ListItem Value="1" Text="Cash"></asp:ListItem>
                                                                                                <asp:ListItem Value="2" Text="Card"></asp:ListItem>--%>
                                                                                                <asp:ListItem Value="3" Text="Credit"></asp:ListItem>
                                                                                            </asp:DropDownList>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div id="CashAmount" class="col-md-6">
                                                                                    <div class="form-group">
                                                                                         <label><asp:Label ID="lblCardType" runat="server" Text="Amount:"></asp:Label></label>
                                                                                        <asp:TextBox ID="txtcashAmount" runat="server" Text="0.00" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="fixed_inpt_cal">

                                                                <div class="row no-gutter">

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label>INPUT VAT  </label>
                                                                            <asp:TextBox ID="TxtInputVAT" runat="server" CssClass="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="InputVAT"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label>OUTPUT VAT  </label>
                                                                            <asp:TextBox ID="txtOutPutVAT" runat="server" Text="0.00" CssClass="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="outputVAT"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>NetVAT  </label>
                                                                            <asp:TextBox ID="txtNetVAT" runat="server" onchange="javascript:calculateTotalFare();" onkeypress="return allowNumerics(event);" Text="0.00" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="netVAT"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label>Total Fare  </label>
                                                                            <asp:TextBox ID="txtTotalFare" runat="server" onchange="javascript:calculateCollectedAmt()" onkeypress="return allowNumerics(event);" Enabled="false" Text="0.00" class="form-control"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="totalFair"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Discount  </label>
                                                                            <asp:TextBox ID="txtDiscount" runat="server" onchange="javascript:calculateCollectedAmt();CalculateNetPay();" onkeypress="return allowNumerics(event);" Text="0.00" CssClass="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Tax  </label>
                                                                            <asp:TextBox ID="txtTax" runat="server" onchange="javascript:calculateTotalFare();CalculateNetPay();" onkeypress="return allowNumerics(event);" Text="0.00" class="form-control" Enabled="false"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="tax"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Commision  </label>
                                                                            <asp:TextBox ID="txtCommision" runat="server" onchange="javascript:calculateCollectedAmt();CalculateNetPay();" onkeypress="return allowNumerics(event);" Text="0.00" class="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="commision"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Collected  </label>
                                                                            <asp:TextBox ID="txtCollected" runat="server" onkeypress="return allowNumerics(event);" Enabled="false" CssClass="form-control" Text="0.00"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="collected"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Net Payble  </label>
                                                                            <asp:TextBox ID="txtNetpayable" runat="server" onkeypress="return allowNumerics(event);" Text="0.00" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="netpayable"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Profit  </label>
                                                                            <asp:TextBox ID="txtProfit" runat="server" onkeypress="return allowNumerics(event);" Text="0.00" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="profit"></b>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div id="sectors_tab" class="tab-pane fade">

                                                    <h4>
                                                        <strong>Sectors </strong>
                                                    </h4>

                                                    <div><span id="SectorerrorMessage" style="color: Red"></span></div>
                                                    <div class=" sector">
                                                        <div id="divSectors" class="table-responsive" style="width: 100%; overflow: auto">
                                                            <asp:Table Class="table table-bordered b2b-corp-table" ID="tblSector" runat="server" Width="100%">

                                                                <asp:TableRow>
                                                                    <asp:TableCell>Refundable</asp:TableCell>
                                                                    <asp:TableCell>From Sector</asp:TableCell>
                                                                    <asp:TableCell>To Sector</asp:TableCell>
                                                                    <asp:TableCell>Flying Carrier</asp:TableCell>
                                                                    <asp:TableCell>Flight No</asp:TableCell>
                                                                    <asp:TableCell>DepartureDate - Time</asp:TableCell>
                                                                    <asp:TableCell>ArrivalDate - Time </asp:TableCell>
                                                                    <asp:TableCell>Fare Basis</asp:TableCell>
                                                                    <asp:TableCell>Class</asp:TableCell>
                                                                    <asp:TableCell>Duration</asp:TableCell>
                                                                </asp:TableRow>


                                                                <asp:TableRow ID="row1" Style="display: none">

                                                                    <asp:TableCell Width="50px">
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund1" Checked="true" Style="display: none" />
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector1" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector1" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier1" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN01" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt1" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>

                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt1" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis1" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass1" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration1" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);" ></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row2" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund2" Checked="true" Style="display: none" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector2" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector2" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier2" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN02" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt2" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt2" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis2" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass2" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration2" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row3" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund3" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector3" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector3" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier3" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="3"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN03" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt3" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt3" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis3" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass3" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="3"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration3" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row4" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund4" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector4" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector4" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier4" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="4"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN04" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt4" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt4" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis4" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass4" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="4"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration4" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row5" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund5" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector5" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector5" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier5" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN05" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt5" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt5" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis5" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass5" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration5" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row6" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund6" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector6" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector6" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier6" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="6"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN06" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="6"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt6" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt6" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis6" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass6" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="6"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration6" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row7" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund7" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector7" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>

                                                                        <asp:DropDownList runat="server" ID="ddlToSector7" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier7" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="7"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN07" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="7"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt7" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt7" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis7" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass7" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="7"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration7" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row8" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund8" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector8" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector8" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier8" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="8"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN08" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="8"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt8" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt8" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis8" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass8" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="8"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration8" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row9" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund9" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector9" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector9" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier9" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="9"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN09" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="9"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt9" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt9" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis9" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass9" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="9"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration9" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row10" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund10" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector10" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector10" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier10" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN010" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt10" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt10" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis10" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass10" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration10" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row11" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund11" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector11" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector11" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier11" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN011" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt11" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="11" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt11" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="11" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis11" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass11" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration11" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row12" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund12" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector12" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector12" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier12" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN012" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt12" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="12" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt12" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="12" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis12" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass12" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration12" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row13" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund13" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector13" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector13" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier13" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN013" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt13" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="13" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt13" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="13" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis13" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass13" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration13" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row14" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund14" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector14" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector14" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier14" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN014" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt14" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="14" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt14" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="14" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis14" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass14" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration14" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row15" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund15" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector15" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector15" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier15" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN015" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt15" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="15" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt15" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="15" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis15" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass15" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration15" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row16" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund16" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector16" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector16" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier16" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN016" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt16" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="16" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt16" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="16" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis16" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass16" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration16" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row17" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund17" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector17" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector17" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier17" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN017" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt17" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="17" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt17" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="17" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis17" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass17" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration17" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row18" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund18" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector18" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector18" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier18" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN018" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt18" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="18" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt18" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="18" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis18" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass18" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration18" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row19" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund19" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector19" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector19" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier19" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN019" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt19" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="19" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt19" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="19" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis19" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass19" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration19" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                                <asp:TableRow ID="row20" Style="display: none">

                                                                    <asp:TableCell>
                                                                        <asp:CheckBox runat="server" ID="chkIsRefund20" Checked="true" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlFromSector20" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:DropDownList runat="server" ID="ddlToSector20" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlyingCarrier20" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFlightN020" runat="server" CssClass="form-control"  ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="DepartureDt20" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="20" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <div>
                                                                            <uc1:DateControl ID="ArrivalDt20" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="20" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                        </div>

                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtFareBasis20" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtClass20" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                    <asp:TableCell>
                                                                        <asp:TextBox ID="txtDuration20" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                    </asp:TableCell>

                                                                </asp:TableRow>

                                                            </asp:Table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="UDID" class="tab-pane fade">
                                                    <h4>
                                                        <strong>UDID </strong>
                                                    </h4>

                                                    <div class="row">

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Cost Centre:</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Secretary:</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Approved By:</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Purpose:</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Deal Code:</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>Employee ID:</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>


                                            <div class="pnr-toggle-btn-wrap">

                                                <a id="hidePanel" href="javascript:void(0);" style="color: #fff">
                                                    <i class="fa fa-angle-left"></i>
                                                </a>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="row no-gutter">
                                        <div class="col-md-12" style="text-align: right;">


                                            <div class="pull-right mr-4 mt-2">

                                                <asp:Button ID="btnAdd" runat="server" CausesValidation="true" OnClientClick="return ValidateCtrls();" OnClick="btnAdd_Click" class="btn btn-primary btn_custom pull-left" Text="More Pax" Visible="false" />

                                                <asp:Button ID="btnprev" runat="server" class="btn btn-primary btn_custom pull-left" OnClientClick="navigatePrevTab(); return false;" Text="Prev" />
                                                <asp:Button ID="btnnext" runat="server" class="btn btn-primary btn_custom pull-left" OnClientClick="navigateNextTab(); return false;" Text="Next" />

                                                <asp:Button ID="btnUpdate" runat="server" CausesValidation="true" OnClick="btnUpdate_Click" OnClientClick="return ValidateCtrls();" class="btn btn-primary btn_custom pull-left" Text="Update" Visible="false" />

                                                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="return Savesegments();" class="btn btn-primary btn_custom pull-left" Text="Save" style="display:none" />

                                                <asp:Button ID="btnClear" runat="server" class="btn btn-secondary btn_custom pull-left" Text="Clear" OnClientClick="window.location = 'OffLineEntry.aspx'; return false;" />
                                                <div class="clearfix"></div>

                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>

                            <script>
                                function HidePanel() {

                                    document.getElementById("LeftPanel").style.width = "100%";

                                    document.getElementById("RightPanel").style.display = "none";

                                }
                            </script>

                            <!--right panel-->
                            <div id="RightPanel" class="col" style="margin-top:37px;">



                                <div>

                                    <div class="main_panel">
                                        <div class="main_panel_wrap">

                                            <div class="multi_inpt">
                                                <span class="input-group-addon">
                                                    <label style="display: block" >Currency:</label>
                                                    <asp:DropDownList ID="ddlcurrencycode" runat="server" Enabled="false">
                                                    </asp:DropDownList>
                                                    <%--<select disabled="" class="form-control">
                                                    <option>AED </option>
                                                </select>--%>
                                                </span>

                                                <span class="input-group-addon">
                                                    <label>Exc. Rate:</label>

                                                    <asp:TextBox ID="txtExcRate" runat="server" Text="1.00" class="form-control" Enabled="false"></asp:TextBox>
                                                    <%--  <input disabled="" class="form-control" value="1.000" type="text">--%>
                                                </span>

                                            </div>

                                            <script>
                                                $(document).ready(function () {

                                                    $("#addRow").click(function () {


                                                    });

                                                    $(".deleteRow").click(function () {
                                                        //$("#taxDiv2").hide().closest("#taxDiv2");
                                                    });

                                                    //  $('.no-select2').select2('destroy');

                                                });
                                            </script>
                                            <div class="form-group-title">
                                                <strong>Selling Fare : </strong>
                                            </div>
                                            <div class="form-group">
                                                <asp:TextBox ID="txtSellingFare" onkeypress="return allowNumerics(event);" runat="server" onchange="javascript:calculateTotalFare();" class="form-control" ondrop="return false;"
                                                    onpaste="return true;" onfocus="Check(this.id);" onBlur="Set(this.id);" MaxLength="7" Text="0.00" TabIndex="1"></asp:TextBox>
                                                <b class="red_span" style="display: none" id="sellingFare"></b>
                                            </div>
                                            <div class="form-group-title">
                                                <strong>Tax : </strong>

                                            </div>
                                            <div><b class="red_span" style="display: none" id="TaxErrorMsg"></b></div>
                                            <div id="taxDiv" class="multi_inpt">
                                                <span class="input-group-addon">
                                                    <label>Tax Code:</label>
                                                    <asp:TextBox ID="txtCode0" runat="server" onchange="javascript:GetTaxValue();" class="form-control" onblur=" CheckTaxValid();" onkeypress="return isAlphaNumeric(event);" MaxLength="2" TabIndex="1"></asp:TextBox>
                                                </span>

                                                <span class="input-group-addon">
                                                    <label>Amount:</label>
                                                    <asp:TextBox ID="txtValue0" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" onchange="CalculateTax();" TabIndex="1"></asp:TextBox>
                                                </span>

                                                <span class="input-group-addon">
                                                    <div>&nbsp;</div>

                                                    <a id="addRow" href="#">
                                                        <i class="fa fa-plus-circle" onclick="AddNewRow('','',false)" id="TaxDivElt"></i>
                                                    </a>
                                                </span>

                                            </div>

                                            <%--<div style="display: none" id="taxDiv2" class="multi_inpt">
                                        <span class="input-group-addon">
                                            <label>Tax Code:</label>
                                            <input class="form-control" type="text">
                                        </span>

                                        <span class="input-group-addon">
                                            <label>Amount:</label>
                                            <input class="form-control" type="text">
                                        </span>

                                        <span class="input-group-addon">
                                            <div>&nbsp;</div>

                                            <a href="#">
                                                <i class="fa fa-minus-circle deleteRow"></i>
                                            </a>
                                        </span>

                                    </div>--%>

                                            <div class="form-group-title">
                                                <strong>Commission:</strong>

                                            </div>

                                            <div class="multi_inpt three-col">
                                                <span class="input-group-addon">
                                                    <label>Comm %</label>
                                                    <asp:TextBox ID="txtCommper" runat="server" onkeypress="return allowNumerics(event);" MaxLength="3" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" onchange="CalculateCommision();" TabIndex="1"></asp:TextBox>
                                                </span>

                                                <span class="input-group-addon">
                                                    <label>Comm On</label>
                                                    <asp:TextBox ID="txtCommisionOn" runat="server" onkeypress="return allowNumerics(event);" MaxLength="7" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                </span>

                                                <span class="input-group-addon">
                                                    <label>Comm Amnt</label>
                                                    <asp:TextBox ID="txtCommisionAmt" runat="server" onkeypress="return allowNumerics(event);" MaxLength="7" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                </span>

                                            </div>
                                            <div><b class="red_span" style="display: none" id="CommissionErr"></b></div>
                                            <div class="form-group-title">
                                                <strong>Discount:</strong>
                                            </div>

                                            <div class="multi_inpt three-col">
                                                <span class="input-group-addon">
                                                    <label>Disc %</label>
                                                    <asp:TextBox ID="txtDiscountper" runat="server" onkeypress="return allowNumerics(event);" MaxLength="3" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" onchange="CalculateDiscount();" TabIndex="1"></asp:TextBox>
                                                </span>

                                                <span class="input-group-addon">
                                                    <label>Disc On</label>
                                                    <asp:TextBox ID="txtDiscountOn" runat="server" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false" class="form-control"></asp:TextBox>
                                                </span>

                                                <span class="input-group-addon">
                                                    <label>Disc Amnt</label>
                                                    <asp:TextBox ID="txtDiscountAmt" runat="server" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                </span>
                                            </div>
                                            <div><b class="red_span" style="display: none" id="DiscountDetailsErr"></b></div>

                                            <div class="form-group-title">
                                                <strong>Service charge:</strong>

                                            </div>

                                            <div class="multi_inpt">
                                                <span class="input-group-addon">
                                                    <label>Amount 1:</label>
                                                    <asp:TextBox ID="txtMarkUp" onchange="javascript:calculateProfit();" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                </span>
                                                <span class="input-group-addon">
                                                    <label>Amount 2:</label>
                                                    <asp:TextBox ID="txtAmount2" onchange="javascript:calculateProfit();" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" Text="0.00" class="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                </span>
                                            </div>
                                            <div><b class="red_span" style="display: none" id="MarkUPErrormsg"></b></div>
                                            <div class="multi_inpt" style="display: none" >
                                                <span class="input-group-addon">
                                                    <label>Transaction Fee</label>
                                                    <asp:TextBox ID="txtTransFee" runat="server" onkeypress="return allowNumerics(event);" onchange="javascript:calculateProfit();" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                </span>
                                                <span class="input-group-addon">
                                                    <label>Addl transaction Fee</label>
                                                    <asp:TextBox ID="txtAddlTransFee" runat="server" onkeypress="return allowNumerics(event);" onchange="javascript:calculateProfit();" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                </span>
                                            </div>
                                            <div><b class="red_span" style="display: none" id="TrasactionErrormsg"></b></div>

                                            <div class="multi_inpt">
                                                <span class="input-group-addon">
                                                    <%--  <label>Agent PLB</label>--%>
                                                    <asp:TextBox ID="txtAgentPLB" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                </span>
                                                <span class="input-group-addon">
                                                    <%-- <label>Agent Commision</label>--%>
                                                    <asp:TextBox ID="txtAgentCom" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                </span>
                                            </div>
                                            <div><b class="red_span" style="display: none" id="AgentComErr"></b></div>

                                            <div class="multi_inpt">
                                                <span class="input-group-addon">
                                                    <%--  <label>Our PLB</label>--%>
                                                    <asp:TextBox ID="txtOurPLB" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                </span>
                                                <span class="input-group-addon">
                                                    <%-- <label>Our Commision</label>--%>
                                                    <asp:TextBox ID="txtOurComm" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                </span>
                                            </div>
                                            <div><b class="red_span" style="display: none" id="OurCommErr"></b></div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!--footer starts here-->

                <!--Modal Popup-->
                <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" data-dismiss="modal" class="close mt-0" style="color: #fff; opacity: 1;">
                                    <span aria-hidden="true">×</span>
                                </button>

                                <h4 class="modal-title" id="rtgrt">PNR Search Results  </h4>
                            </div>
                            <span id="array_disp"></span>
                            <div class="modal-body p-0">
                                <div id="popupData" runat="server">
                                    <%if (itinerary != null && itinerary.Segments != null && itinerary.Passenger != null && itinerary.Segments.Length > 0 && itinerary.Passenger.Length > 0) %>
                                    <%{ %>
                                    <div class="table-responsive">
                                        <table class="table table-bordered b2b-corp-table mb-0" width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr>
                                                    <th>Invoice Date </th>
                                                    <th>PNR/Ticket</th>
                                                    <th>Sale Date</th>
                                                    <th>Airline</th>
                                                    <th>Pax Name</th>
                                                    <th>Fare</th>
                                                    <th>Tax</th>
                                                    <th>Service Charge</th>
                                                    <th>Markup </th>
                                                    <th>Mode</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>

                                            </thead>
                                            <tbody>
                                                <%for (int i = 0; i < itinerary.Passenger.Length; i++) %>
                                                <%{ %>

                                                <%if (itinerary.Passenger[i] != null) %>
                                                <%{ %>
                                                <tr>

                                                    <td><%=itinerary.CreatedOn.ToString("dd-MMM-yyyy") %></td>
                                                    <td><%=itinerary.PNR %></td>
                                                    <td><%=itinerary.CreatedOn.ToString("dd-MMM-yyyy") %></td>
                                                    <td><%=itinerary.Segments[0].Airline %></td>
                                                    <td><%=itinerary.Passenger[i].FirstName +"/"+ itinerary.Passenger[i].LastName %></td>
                                                    <td><%=itinerary.Passenger[i].Price.PublishedFare %></td>
                                                    <td><%=itinerary.Passenger[i].Price.Tax %></td>
                                                    <td><%=itinerary.Passenger[i].Price.SeviceTax %></td>
                                                    <td><%=itinerary.Passenger[i].Price.Markup+ itinerary.Passenger[i].Price.AsvAmount %></td>
                                                    <td><%=itinerary.PaymentMode %></td>

                                                    <td><a href="#" class="add-btn" id="Edit_<%=i %>" onclick="hidePopUp('<% =i %>')">Edit </a></td>

                                                    <td><a href="#" class="add-btn" id="Remove_<%=i %>" onclick="RemovePaxFromPopUp('<% =i %>')">Remove </a></td>
                                                    <%-- <asp:LinkButton class="getId" runat="server" ID="hlEdit" OnClick="hlEdit_Click" OnClientClick="hidePopUp(<% =i %>)" Text="Add/Edit"></asp:LinkButton></td>--%>
                                                </tr>
                                                <%} %>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="py-4 pr-4">
                                        <div class="float-right">
                                            <button type="button" data-dismiss="modal" value="Close" class="btn btn-primary btn_custom">
                                                <span aria-hidden="true">Close</span>
                                            </button>

                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <%} %>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>






            </div>
            <div class="clearfix"></div>



            <script>
                function StellrMode(obj) {
                    var selectBox = obj;
                    var selected = selectBox.options[selectBox.selectedIndex].value;
                    document.getElementById('<%=paymentmode.ClientID%>').value = selected;
                    /*  cash*/
                    if (selected === '1') {

                        CardType.style.display = "none";
                        CardAmount.style.display = "none";

                        CashAmount.style.display = "block";
                        CardPercentage.style.display = "none";
                        CreditAmount.style.display = "none";
                        Arcode.style.display = "none";

                    }

                    /*  card*/
                    else if (selected === '2') {
                        CardType.style.display = "block";

                        CardAmount.style.display = "block";

                        CashAmount.style.display = "none";

                        CardPercentage.style.display = "block";

                        CreditAmount.style.display = "none";
                        Arcode.style.display = "none";

                    }

                    /*  credit*/
                    else if (selected === '3') {
                        CardType.style.display = "none";

                        CardType.style.display = "none";

                        CardAmount.style.display = "none";

                        CashAmount.style.display = "none";

                        CardPercentage.style.display = "none";

                        CreditAmount.style.display = "block";

                        Arcode.style.display = "block";

                    } else {

                    }

                }
            </script>
            <script type="text/javascript">
                //<![CDATA[
                disablefield();
                $(document).ready(function () {
                    $('.add-btn').on('click', function () {
                        $('#EditPNRDiv').show();
                        $('#FareDiff').hide();
                        $('#showParam').collapse('hide');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                    })

                    $('body').on('click', '#hidePanel', function () {
                        $('body').toggleClass('expanded-left-panel');
                    })

                    $('.backToResults').on('click', function () {
                        $('#EditPNRDiv').hide();
                        $('#showParam').collapse('hide');
                    });
					$('body').addClass('expanded-left-panel');
					
                    disablefield();
                })
        <%-- <%if(Session["IsSaved"] != null){%>
        $('#FareDiff').modal('hide');
        document.getElementById('<%=lblResult.ClientID%>').value = "Successfully Saved";
        $('#lblResult').css("color", "green");
        Session["IsSaved"] = null;
        <%}%>--%>

                //]]>

                function BindSectors() {

                    var ddldestination = document.getElementById('<%=ddldestination.ClientID%>');
                    var ddlSector1 = document.getElementById('<%=ddlSector1.ClientID%>');

                    if (ddldestination.value == '-1' || ddldestination.value == '')
                        alert('Please select destination');

                    if (ddlSector1.disabled) {

                        for (var i = 1; i <= 5; i++) {

                            document.getElementById('ctl00_cphTransaction_ddlSector' + i).innerHTML = ddldestination.innerHTML;
                            $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                            document.getElementById('ctl00_cphTransaction_ddlSector' + i).disabled = false;

                        }

                    }
                    else {

                        for (var i = 1; i <= 5; i++) {
                            $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                        }

                        var ddlSector6 = document.getElementById('<%=ddlSector6.ClientID%>');

                if (!ddlSector1.disabled) {

                    for (var i = 6; i <= 10; i++) {

                        document.getElementById('ctl00_cphTransaction_ddlSector' + i).innerHTML = '';
                        $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                        document.getElementById('ctl00_cphTransaction_ddlSector' + i).disabled = true;

                    }

                }

                document.getElementById('<%=hndsegmentsCount.ClientID%>').value = '0';
                    }
                    TaxCounterTozero();

                }


                function GetSectorsCount(event) {

                    var sectorsCount = 0;
                    var id = event.id.substring(event.id.length - 1);
                    if (id == 0)
                        id = Math.ceil(id) + 10;
                    var selsectorval = event.value;
                    var ddldestination = document.getElementById('<%=ddldestination.ClientID%>');

                    if (selsectorval == '' || selsectorval == '-1') {
                        sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        if (sectorsCount != '' && sectorsCount != '0') {
                            if (Math.ceil(sectorsCount) == Math.ceil(id)) {
                                document.getElementById('<%=hndsegmentsCount.ClientID%>').value = Math.ceil(sectorsCount) - 1;
                                event.focus();
                                return;
                            }
                        }
                        toastr.error('Please select sector');
                        return;
                    }

                    if (ddldestination.value == '-1' || ddldestination.value == '') {
                        BindSectors();
                        document.getElementById('<%=hndsegmentsCount.ClientID%>').value = sectorsCount;
                        return;
                    }
                    else {

                        if (id > 1) {
                            var sectorid = id - 1;
                            var sectorvalue = document.getElementById('ctl00_cphTransaction_ddlSector' + sectorid).value;
                            if (sectorvalue == '' || sectorvalue == '-1') {
                                $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                toastr.error('Please select previous sector');
                                return;
                            }

                            if (sectorvalue == selsectorval) {
                                $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                toastr.error('Previous and current sectors cannot be same');
                                return;
                            }
                        }

                        if (id == 1 && ddldestination.value == selsectorval) {
                            $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                            toastr.error('Destination and sector-1 canot be same');
                            return;
                        }
                    }

                    var ddlSector6 = document.getElementById('<%=ddlSector6.ClientID%>');

                    if (id == 5 && ddlSector6.disabled) {
                        document.getElementById('<%=txtConjPNR.ClientID%>').value = document.getElementById('<%=txtPNRNum.ClientID%>').value;
                        for (var i = 6; i <= 10; i++) {

                            document.getElementById('ctl00_cphTransaction_ddlSector' + i).innerHTML = ddldestination.innerHTML;
                            $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                            document.getElementById('ctl00_cphTransaction_ddlSector' + i).disabled = false;

                        }
                    }

                    //if (id == 10) {
                    //    $('#s2id_ctl00_cphTransaction_ddlSector10').select2('val', ddldestination.value);
                    //    toastr.error('Last sector should be same as destination');
                    //    return;
                    //}

                    var isDuplicate = false;
                    var counter = 5;
                    if (!ddlSector6.disabled)
                        counter = 10;
                    sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    if (sectorsCount != '' && sectorsCount != 0) {
                        sectorsCount = 0;
                        for (var i = 1; i <= counter; i++) {
                            var currsectorval = document.getElementById('ctl00_cphTransaction_ddlSector' + i).value;

                            if (currsectorval != '' && currsectorval != '-1')
                                sectorsCount = sectorsCount + 1;

                            //if (i != id && currsectorval == selsectorval && currsectorval != '' && currsectorval != '-1') {
                            //    isDuplicate = true;
                            //}
                        }
                    }
                    else
                        sectorsCount = 1;
                    

                    //if (Math.ceil(sectorsCount) == Math.ceil(id) && Math.ceil(id) < 10 ) {

                    //    var nextsecid = Math.ceil(id) + 1;
                    //    if (isDuplicate)
                    //        nextsecid = Math.ceil(id);
                    //    if (selsectorval != ddldestination.value)
                    //        $('#s2id_ctl00_cphTransaction_ddlSector' + nextsecid).select2('val', ddldestination.value);
                    //    sectorsCount = nextsecid;
                    //}

                    document.getElementById('<%=hndsegmentsCount.ClientID%>').value = sectorsCount;

                    if (isDuplicate) {
                        if (Math.ceil(sectorsCount) != Math.ceil(id))
                            $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                        alert('Sector value cannot be duplicated');
                        return;
                    }

                    if (id == 1)
                        CalculateVAT();

                    $('#showParam').collapse('hide');

                }

                function BindSectorTab() {

                    var sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    var ddldestination = document.getElementById('<%=ddldestination.ClientID%>');
                    var ddlairline = document.getElementById('<%=ddlairline.ClientID%>');
                    var ddlRepAirline = document.getElementById('<%=ddlRepAirline.ClientID%>');
                    var txtTravelDt = document.getElementById('<%=txtTravelDt.ClientID%>');
                    var txtClassSector = document.getElementById('<%=txtClassSector.ClientID%>');

                    if (eval(sectorsCount) > 0) {
                        document.getElementById('sectortab').style.display = "block";
                    }
                    else
                        document.getElementById('sectortab').style.display = "none";

                    for (var i = 1; i <= sectorsCount - 1; i++) {

                        document.getElementById('ctl00_cphTransaction_row' + i).style.display = "table-row";

                        document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).innerHTML = ddldestination.innerHTML;
                        document.getElementById('ctl00_cphTransaction_ddlToSector' + i).innerHTML = ddldestination.innerHTML;

                        var frmsecvalue = document.getElementById('ctl00_cphTransaction_ddlSector' + i).value;
                        var Tosectorid = i + 1;
                        var Tosecvalue = document.getElementById('ctl00_cphTransaction_ddlSector' + Tosectorid).value;

                        $('#s2id_ctl00_cphTransaction_ddlFromSector' + i).select2('val', frmsecvalue);
                        $('#s2id_ctl00_cphTransaction_ddlToSector' + i).select2('val', Tosecvalue);

                        if (ddlairline.value != '11' && document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value == '')
                            document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value = ddlRepAirline.value;

                        if (i == 1) {
                            txtTravelDt = new Date(txtTravelDt.value);
                            txtTravelDt = txtTravelDt.format('dd-MMM-yyyy');
                        }

                        if (document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value == '')
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value = txtTravelDt;
                        if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value == '')
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = txtTravelDt;                        

                        $('#ctl00_cphTransaction_DepartureDt' + i + '_Time').attr("onchange", "return calculateduration(this,'dep', '" + i + "');");
                        $('#ctl00_cphTransaction_ArrivalDt' + i + '_Time').attr("onchange", "return calculateduration(this,'arr', '" + i + "');");

                        document.getElementById('ctl00_cphTransaction_txtClass' + i).value = txtClassSector.value;
                        document.getElementById('ctl00_cphTransaction_txtDuration' + i).value = '0';
                        $('span').show();
                    }

                    var txtcntrl = 'textbox', dropdown = 'dropdown';

                    for (var i = sectorsCount; i <= 20; i++) {

                        clearcntrls(dropdown, 'ddlFromSector', i, '');
                        clearcntrls(dropdown, 'ddlToSector', i, '');
                        clearcntrls(txtcntrl, 'txtFlyingCarrier', i, '');
                        clearcntrls(txtcntrl, 'txtFlightN0', i, '');
                        clearcntrls(txtcntrl, 'DepartureDt', i, '_Date');
                        clearcntrls(txtcntrl, 'DepartureDt', i, '_Time');
                        clearcntrls(txtcntrl, 'ArrivalDt', i, '_Date');
                        clearcntrls(txtcntrl, 'ArrivalDt', i, '_Time');
                        clearcntrls(txtcntrl, 'txtFareBasis', i, '');
                        clearcntrls(txtcntrl, 'txtClass', i, '');
                        clearcntrls(txtcntrl, 'txtDuration', i, '');
                        document.getElementById('ctl00_cphTransaction_row' + i).style.display = "none";

                    }
                }

                function ValidateSectorTab() {

                    var isValid = true;
                    var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    var emptyvar = '';

                    var valfrmsector = true, valtosector = true, valflycarr = true, valfltno = true, valDepDt = true, valArrDt = true, valdates = true,
                        valfarebasis = true, valclass = true, valduration = true, focusoncntrl = true, valDeptime = true, valArrtime = true, valtraveldt = true;

                    valtraveldt = validatetraveldt();
                    if (!valtraveldt) {
                        focusoncntrl = valtraveldt;
                        $('#ctl00_cphTransaction_DepartureDt1_Date').addClass('form-text-error');
                        document.getElementById('ctl00_cphTransaction_DepartureDt1_Date').focus();
                    }

                    for (var i = 1; i <= segmentlength - 1; i++) {

                        isValid = validatemandatory('ddlFromSector', i, 'dropdown', focusoncntrl, emptyvar, 'Select');
                        if (valfrmsector && !isValid) { valfrmsector = focusoncntrl = isValid; }

                        isValid = validatemandatory('ddlToSector', i, 'dropdown', focusoncntrl, emptyvar, 'Select');
                        if (valtosector && !isValid) { valtosector = focusoncntrl = isValid; }

                        isValid = validatemandatory('txtFlyingCarrier', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                        if (valflycarr && !isValid) { valflycarr = focusoncntrl = isValid; }

                        //isValid = validatemandatory('txtFlightN0', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                        //if (valfltno && !isValid) { valfltno = focusoncntrl = isValid; }

                        isValid = validatemandatory('DepartureDt', i, 'textbox', focusoncntrl, '_Date', emptyvar);
                        if (valDepDt && !isValid) { valDepDt = focusoncntrl = isValid; }

                        //isValid = validatemandatory('DepartureDt', i, 'textbox', focusoncntrl, '_Time', emptyvar);
                        //if (valDeptime && !isValid) { valDeptime = focusoncntrl = isValid; }

                        isValid = valArrDt = validatemandatory('ArrivalDt', i, 'textbox', focusoncntrl, '_Date', emptyvar);
                        if (valArrDt && !isValid) { valArrDt = focusoncntrl = isValid; }

                        //isValid = valArrDt = validatemandatory('ArrivalDt', i, 'textbox', focusoncntrl, '_Time', emptyvar);
                        //if (valArrtime && !isValid) { valArrtime = focusoncntrl = isValid; }

                        //isValid = validatemandatory('txtFareBasis', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                        //if (valfarebasis && !isValid) { valfarebasis = focusoncntrl = isValid; }

                        isValid = validatemandatory('txtClass', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                        if (valclass && !isValid) { valclass = focusoncntrl = isValid; }

                        //isValid = validatemandatory('txtDuration', i, 'textbox', focusoncntrl, emptyvar, '0');
                        //if (valduration && !isValid) { valduration = focusoncntrl = isValid; }

                        isValid = validatedate('DepartureDt', 'dep', i);
                        if (valdates && !isValid) { valdates = focusoncntrl = isValid; }

                        isValid = validatedate('ArrivalDt','arr', i);
                        if (valdates && !isValid) { valdates = focusoncntrl = isValid; }
                    }

                    if (!valdates) { toastr.error('Please make sure departure date and time is less than arrival date time in all segments'); }                    
                    if (!valtraveldt) { toastr.error('Please make sure sector-1 departure date is same as travel date'); }
                    if (!valduration) { toastr.error('Please enter duration'); }
                    if (!valclass) { toastr.error('Please enter class'); }
                    if (!valfarebasis) { toastr.error('Please enter fare basis'); }
                    if (!valArrtime) { toastr.error('Please enter arrival time'); }
                    if (!valArrDt) { toastr.error('Please enter arrival date'); }
                    if (!valDeptime) { toastr.error('Please enter departure time'); }
                    if (!valDepDt) { toastr.error('Please enter departure date'); }
                    if (!valfltno) { toastr.error('Please enter flight no'); }
                    if (!valflycarr) { toastr.error('Please enter flying carrier'); }
                    if (!valtosector) { toastr.error('Please select to Sector'); }
                    if (!valfrmsector) { toastr.error('Please select from Sector'); }

                    return focusoncntrl;
                }

                function validatemandatory(cntrlid, sectorno, cntrltype, focusoncntrl, datetime, defaultval) {

                    var valid = true;

                    if (cntrltype == 'dropdown') {

                        var ddlvalue = document.getElementById('s2id_ctl00_cphTransaction_' + cntrlid + sectorno).innerText.trim();
                        if (ddlvalue == defaultval || ddlvalue == '') {
                            $('#s2id_ctl00_cphTransaction_' + cntrlid + sectorno).parent().addClass('form-text-error');
                            if (focusoncntrl)
                                document.getElementById('s2id_ctl00_cphTransaction_' + cntrlid + sectorno).focus();
                            valid = false;
                        }

                    }

                    if (cntrltype == 'textbox') {
                        var txtvalue = document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorno + datetime).value.trim();
                        if (txtvalue == '' || txtvalue == defaultval) {
                            $('#ctl00_cphTransaction_' + cntrlid + sectorno + datetime).addClass('form-text-error');
                            if (focusoncntrl)
                                document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorno + datetime).focus();
                            valid = false;
                        }
                    }

                    return valid;
                }

                function clearcntrls(cntrltype, cntrlid, sectorid, datetime) {

                    if (cntrltype == 'textbox') {
                        document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorid + datetime).value = '';
                    }

                    if (cntrltype == 'dropdown') {

                        $('#s2id_ctl00_cphTransaction_' + cntrlid + sectorid).select2('val', '-1');
                        document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorid).innerHTML = '';
                    }
                }

                function Savesegments() {
                    var agentinfo = GetAgentInfo();
                    var agentdetails = agentinfo.split('|');
                    var cashamt = eval(document.getElementById('<%=txtcashAmount.ClientID%>').value.replace(/,/g, ''));

                    //if (Math.ceil(agentdetails[1]) >= Math.ceil(cashamt)) {

                        var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        var segmentvalues = '';

                        for (var i = 1; i <= segmentlength - 1; i++) {

                            segmentvalues += document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).value + '-' + document.getElementById('ctl00_cphTransaction_ddlToSector' + i).value + '|';

                        }
                        segmentvalues = segmentvalues.slice(0, -1);
                        document.getElementById('<%=hdnsegments.ClientID%>').value = segmentvalues;

                   // }
                   // else {
                   //     alert("Insufficient Funds.");
                   //     return false;
                    //}                        
                }

                function GetAgentInfo() {

                    var agentid = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                    var agentinfo = '';

                    $.ajax({                    
                        type: "POST",
                        url: "OfflineEntry.aspx/CheckAgentBalance",
                        contentType: "application/json; charset=utf-8",
                        data: "{'sAgentId':'"+ agentid +"'}",
                        dataType: "json",
                        async: false,
                        success: function (Et) {                    
                                if (Et.d != '') {
                                    agentinfo = Et.d;
                                }
                            },
                            failure: function (error) {
                                alert(error);        
                                return false;
                            }
                    }); 

                    return agentinfo;
                }

                function Changemode() {

                    var ddlMode = document.getElementById('<%=ddlMode.ClientID%>').value;
                    $('#<%=ddlCustomer1.ClientID%>').select2('val', '-1');
                    Customerchange();

                    if (ddlMode == '3')
                        $('#spnCustomer').show();
                    else
                        $('#spnCustomer').hide();
                }
                                

                function Customerchange() {
                    
                    var agentid = document.getElementById('<%=ddlCustomer1.ClientID%>').value;

                    if (agentid != '0' && agentid != '-1') {
                        var agentinfo = GetAgentInfo();
                        var agentdetails = agentinfo.split('|');
                        document.getElementById('spnagentBalance').style.display = 'block';
                        $('#lblCusBalance').text('(' + agentdetails[0] + ' ' + agentdetails[1] + ')');
                    }
                    else {
                        $('#lblCusBalance').text('');
                        document.getElementById('spnagentBalance').style.display = 'none';
                    }

                    <%--var ddlcurrencycode = document.getElementById('<%=ddlcurrencycode.ClientID%>');
                    ddlcurrencycode.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = agentdetails[0];
                    el.value = agentdetails[0];
                    ddlcurrencycode.add(el, 0);
                    LoadAgentLocations();--%>
                }

                var loc;
                function LoadAgentLocations() {

                    var location = 'ctl00_cphTransaction_ddlConsultant';
                    loc=location;
                    var sel = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                    if (sel == '-1' || sel == '')
                        sel = document.getElementById('<%=hdnAgentId.ClientID%>').value;
       
                    var paramList = 'requestSource=getAgentsLocationsByAgentId&AgentId=' + sel + '&id=' + location;
                    var url = "CityAjax.aspx?"+paramList;

                    if (window.XMLHttpRequest) {
                        Ajax = new XMLHttpRequest();
                    }
                    else {
                        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    Ajax.onreadystatechange = BindLocationsList;
                    Ajax.open('POST', url);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send();

                 }


                function BindLocationsList() {
        
                    if (Ajax.readyState == 4 && Ajax.status == 200)  
                    {
                        var ddl = document.getElementById(loc);
                        if(Ajax.responseText.length > 0) {                
                            if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "--Select Location--";
                            el.value = "-1";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');
                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }                    

                            $('#' + ddl.id).select2('val', '-1');
                            }                
                        }
                        else//Clear previous agent locations if no locations found for the current agent
                        {
                            if(ddl != null)
                            {
                                ddl.options.length=0;
                                var el = document.createElement("option");
                                el.textContent = "--Select Location--";
                                el.value = "-1";
                                ddl.add(el, 0);
                                $('#' + ddl.id).select2('val', '-1');//Select Location
                            }
                        }
                    }
                }

                function comparedate1(mindate, maxdate, message, event) {

                    if (mindate > maxdate && mindate != '' && maxdate != '' && mindate != null && maxdate != null) {
                        alert(message);
                        event.value = '';
                        event.focus();
                        return false;
                    }
                    else
                        return true;
                }

                function comparedate(mindate, maxdate, sectorno, cntrlid) {

                    if (mindate > maxdate && mindate != '' && maxdate != '' && mindate != null && maxdate != null) {
                        $('#ctl00_cphTransaction_' + cntrlid + sectorno + '_Date').addClass('form-text-error');
                        $('#ctl00_cphTransaction_' + cntrlid + sectorno + '_Time').addClass('form-text-error');
                        return false;
                    }
                    else
                        return true;
                }

                function validatedate(event, source, sectorno) {

                    var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    segmentlength = segmentlength - 1;

                    var currsecfrmdt = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + sectorno);
                    var currsectodt = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + sectorno);

                    if (segmentlength == 1 || (sectorno == 1 && source == 'dep') || (sectorno == segmentlength && source == 'arr')) {

                        if (comparedate(currsecfrmdt, currsectodt, sectorno, event))
                            return true;
                        else
                            return false;
                    }

                    var nextprevsectorno = sectorno;
                    var nextprevsecdate = currsecfrmdt;

                    if (source == 'dep') {
                        
                        nextprevsectorno = Math.ceil(sectorno) - 1;
                        nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + nextprevsectorno);
                        if (!comparedate(nextprevsecdate, currsecfrmdt, sectorno, event))
                            return false;
                    }

                    if (source == 'arr') {
                        
                        nextprevsectorno = Math.ceil(sectorno) + 1;
                        nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + nextprevsectorno);
                        if (!comparedate(currsectodt, nextprevsecdate, sectorno, event))
                            return false;
                    }

                    if (comparedate(currsecfrmdt, currsectodt, sectorno, event))
                        return true;
                    else
                        return false;

                }

                function validatetraveldt() {

                    var fromDate = document.getElementById('ctl00_cphTransaction_DepartureDt1_Date').value;

                    var travelDt = new Date(document.getElementById('<%=txtTravelDt.ClientID%>').value);
                    travelDt = travelDt.format('dd-MMM-yyyy');

                    if (fromDate != null && travelDt != null) {

                        if (fromDate != travelDt)
                            return false;
                    }
                    return true;
                }

                function calculateduration(event, source, sectorno) {

                    var fromDate = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + sectorno);
                    var toDate = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + sectorno);
                    var frmtime = document.getElementById('ctl00_cphTransaction_DepartureDt' + sectorno + '_Time').value;
                    var totime = document.getElementById('ctl00_cphTransaction_ArrivalDt' + sectorno + '_Time').value;

                    if ((frmtime!= '' && frmtime.length != 5) || (totime != '' && totime.length != 5)) {
                        toastr.error('Please enter valid time(00:01 - 23:59)');
                        event.value = '';
                        event.focus();
                        return false;
                     }

                    if (fromDate != null && toDate != null && frmtime!= '' && totime != '') {

                        if (fromDate >= toDate) {
                            document.getElementById('ctl00_cphTransaction_txtDuration' + sectorno).value = '';
                            toastr.error('Departure from date and time should be less than arrival to date and time');
                            return false;
                        }
                        else {
                            
                            //var frmtime = document.getElementById('ctl00_cphTransaction_DepartureDt' + sectorno + '_Time').value;
                            var frmarr = frmtime.split(':');
                            var frmhrs = frmarr[0];
                            var frmmins = frmarr[1];

                            //var totime = document.getElementById('ctl00_cphTransaction_ArrivalDt' + sectorno + '_Time').value;
                            var toarr = totime.split(':');
                            var tohrs = toarr[0];
                            var tomins = toarr[1];

                            if (Math.ceil(frmhrs) < 0 || Math.ceil(frmhrs) > 23 || Math.ceil(tohrs) < 0 || Math.ceil(tohrs) > 23
                                || Math.ceil(frmmins) < 0 || Math.ceil(frmmins) > 60 || Math.ceil(tomins) < 0 || Math.ceil(tomins) > 60) {

                                toastr.error('Please enter valid time(00:01 - 23:59)');
                                event.value = '';
                                event.focus();
                                return false;
                            }

                            if (frmmins > 0) {
                                frmmins = 60 - Math.ceil(frmmins);
                                frmhrs = Math.ceil(frmhrs) + 1;
                            }

                            var duration = ((Math.ceil(tohrs) - Math.ceil(frmhrs)) * 60) + Math.ceil(frmmins) + Math.ceil(tomins);

                            var startDay = new Date(fromDate);
                            var endDay = new Date(toDate);
                            var millisecondsPerDay = 1000 * 60 * 60 * 24;

                            var millisBetween = endDay.getTime() - startDay.getTime();
                            var days = Math.ceil(millisBetween / millisecondsPerDay) - 1;

                            if (duration <= 0)
                                days = Math.ceil(days) + 1;
                            duration = (Math.ceil(1440) * Math.ceil(days)) + Math.ceil(duration);

                            document.getElementById('ctl00_cphTransaction_txtDuration' + sectorno).value = duration;
                         }
                    }
                    else
                        document.getElementById('ctl00_cphTransaction_txtDuration' + sectorno).value = '0';
                }

                function SaveSuccess(status) {

                    if (status == 'S') { 
                        //alert('Itenary saved successfully');
                        //window.open('OfflineEntryPrintReceipt.aspx?FlightId=' + document.getElementById('ctl00_cphTransaction_hdnFlightId').value + '', '', 'width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                         window.open('OfflineEntryPrintReceipt.aspx?FlightId=' + document.getElementById('ctl00_cphTransaction_hdnFlightId').value+'','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                }
                    else
                        alert('Failed to save itinerary, please check the logs.');
                    window.location = 'OffLineEntry.aspx';

                }

                function Bindsectortabddls() {

                    for (var i = 1; i <= sectorsCount - 1; i++) {

                        document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).innerHTML = '';
                        document.getElementById('ctl00_cphTransaction_ddlToSector' + i).innerHTML = '';
                        document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value = '';
                        document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).value = '';
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = '';
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').value = '';
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value = '';
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').value = '';
                        document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).value = '';
                        document.getElementById('ctl00_cphTransaction_txtClass' + i).value = '';
                        document.getElementById('ctl00_cphTransaction_txtDuration' + i).value = '';

                        document.getElementById('ctl00_cphTransaction_chkIsRefund' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_ddlToSector' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').style.display = "none";
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = "none";
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = "none";
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = "none";
                        document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_txtClass' + i).style.display = "none";
                        document.getElementById('ctl00_cphTransaction_txtDuration' + i).style.display = "none";

                        document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ddlToSector' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtClass' + i).style.display = "block";
                        document.getElementById('ctl00_cphTransaction_txtDuration' + i).style.display = "block";

                    }
                }

                function validatedate_old(event, source, sectorno) {

                    var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                    segmentlength = segmentlength - 1;

                    var currsecfrmdt = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + sectorno);
                    var currsectodt = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + sectorno);

                    if (segmentlength == 1 || (sectorno == 1 && source == 'dep') || (sectorno == segmentlength && source == 'arr')) {

                        if (comparedate(currsecfrmdt, currsectodt, 'Departure date & time should be less than arrival date and time', event))
                            return true;
                        else
                            return false;
                    }

                    var nextprevsectorno = sectorno;
                    var message = '';
                    var nextprevsecdate = currsecfrmdt;

                    if (source == 'dep') {
                        
                        nextprevsectorno = Math.ceil(sectorno) - 1;
                        nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + nextprevsectorno);
                        message = 'Current sector departure date & time should be greater than previous sector arrival date & time';
                    }

                    if (source == 'arr') {
                        
                        nextprevsectorno = Math.ceil(sectorno) + 1;
                        nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + nextprevsectorno);
                        message = 'Current sector departure date & time should be greater than previous sector arrival date & time';
                    }
                    
                    if (comparedate(nextprevsecdate, currsecfrmdt, message, event)) {

                        if (comparedate(currsecfrmdt, currsectodt, 'Departure date & time should be less than arrival date and time', event))
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;

                }
                function GetFormattedDate() {
var todayTime = new Date();
var month = (todayTime .getMonth() + 1);
var day = (todayTime .getDate());
var year = (todayTime .getFullYear());
return month + "/" + day + "/" + year;
}
		$(function(){

		document.getElementById('<%= txtSalesDate.ClientID%>').value=GetFormattedDate();

});

            </script>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

