﻿<%@ Page  Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Title="Hala Service Queue" Inherits="HalaServiceQueueGUI" Codebehind="HalaServiceQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script>

        
    
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //         if (difference < 1) {
            //             document.getElementById('errMess').style.display = "block";
            //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //             return false;
            //         }
            //         if (difference == 0) {
            //             document.getElementById('errMess').style.display = "block";
            //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
            //             return false;
            //         }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>

   <script type="text/javascript">
function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
   
    </script>
   
   
    

    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none;
            z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none;
            z-index: 9999">
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 700px" align="left">
                <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                    onclick="return ShowHide('divParam');">Hide Parameter</a>
            </td>
        </tr>
    </table>
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        From Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        To Date:</div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                    Used Status:</div>
                    <div class="col-md-2">
                    <table>
                    <tr>
                    <td>
                    <asp:DropDownList CssClass="form-control" ID="ddlIsUsed" runat="server">
                           <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="1" Text="YES"></asp:ListItem>
                           <asp:ListItem  Value="0" Text="NO"></asp:ListItem>
                       </asp:DropDownList>
                    </td>
                    </tr>
                    </table>
                    </div>
                    
                   
                 
                    <div class="clearfix">
                    </div>
                </div>
                
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                       </div>
                    <div class="col-md-2">
                       
                    </div>
                    <div class="col-md-8">
                        <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" runat="server" Text="Search"
                            OnClick="btnSearch_Click" />
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
</div>
        </asp:Panel>
    </div>
    <div class="clearfix">
                    </div>
    <br />
    
    <div>
   
    <asp:GridView AllowPaging="true" PageSize="10" CssClass="table b2b-corp-table" Width="100%" ID="gvHalaList" DataKeyNames="add_id"
                                                    runat="server" AutoGenerateColumns="false" OnPageIndexChanging="gvHalaList_OnPageIndexChanging"  EmptyDataText = "No Records Found !" HeaderStyle-Font-Bold="true">
                                                   
                                                    <Columns>
                                                    
                                                    
    <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkstatus" runat="server" Checked='<%# Eval("isUsed").ToString()!= "N"%>' 
                                Enabled='<%# Eval("isUsed").ToString()!="U"%>' />
                                <asp:HiddenField ID="IThdfAddId" runat="server" Value='<%# Bind("add_id") %>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>                                             
                                                   
      <asp:BoundField  HeaderText="CREATED ON" DataField="created_on" HeaderStyle-Font-Bold="true"></asp:BoundField >                                            
    <asp:BoundField  HeaderText="DOCUMENT NUMBER" DataField="DocNo" HeaderStyle-Font-Bold="true"></asp:BoundField >
    <asp:BoundField  HeaderText="PASSENGER NAME" DataField="pax_name" HeaderStyle-Font-Bold="true"></asp:BoundField >
    <asp:BoundField  HeaderText="PASSENGER NATIONALITY" DataField="pax_nat" HeaderStyle-Font-Bold="true"></asp:BoundField >
     <asp:BoundField HeaderText="ROUTING" DataField="routing" HeaderStyle-Font-Bold="true"></asp:BoundField>
    <asp:BoundField  HeaderText="FLIGHT NUMBER" DataField="flightno" HeaderStyle-Font-Bold="true"></asp:BoundField >
    
     <asp:BoundField  HeaderText="PASSPORT NUMBER" DataField="pax_passport_no" HeaderStyle-Font-Bold="true"></asp:BoundField >
      <asp:BoundField  HeaderText="MOBILE NUMBER" DataField="pax_mobile_no" HeaderStyle-Font-Bold="true"></asp:BoundField >
    
    <asp:BoundField  HeaderText="SERVICE NAME" DataField="SER_NAME" HeaderStyle-Font-Bold="true"></asp:BoundField >    
    <asp:BoundField  HeaderText="AMOUNT" DataField="amount" HeaderStyle-Font-Bold="true"></asp:BoundField >
    <asp:BoundField  HeaderText="REMARKS" DataField="remarks" HeaderStyle-Font-Bold="true"></asp:BoundField >
                                                        
                                                    </Columns>
                                                </asp:GridView>
     
    </div>
    
    <div>
    <asp:DataGrid ID="dgHalaSerAcctReportList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="CREATED ON" DataField="created_on" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="DOCUMENT NUMBER" DataField="DocNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="PASSENGER NAME" DataField="pax_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="PASSENGER NATIONALITY" DataField="pax_nat" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="ROUTING" DataField="routing" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="FLIGHT NUMBER" DataField="flightno" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
     <asp:BoundColumn  HeaderText="PASSPORT NUMBER" DataField="pax_passport_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn >
     
      <asp:BoundColumn  HeaderText="MOBILE NUMBER" DataField="pax_mobile_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
      
    <asp:BoundColumn HeaderText="SERVICE NAME" DataField="SER_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="AMOUNT" DataField="amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="REMARKS" DataField="remarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    
    </Columns>
    </asp:DataGrid>
    </div>
    
    
    <table>
                      <tr>
                          
                          <td width="80px">
                          <asp:Button ID="btnUpdateStatus" runat="server" Text="Update Status" OnClick="btnUpdateStatus_Click" CssClass="button"/>
                              
                          </td>
                          <td>
                          <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                                  CssClass="button" />
                          </td>
                      </tr>
                      <tr>
                      <td>
                       <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
                      </td></tr>
                  </table>
    
</asp:Content>
