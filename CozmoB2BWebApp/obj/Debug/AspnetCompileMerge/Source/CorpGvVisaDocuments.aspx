﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CorpGvVisaDocumentsGUI" MasterPageFile="~/TransactionVisaTitle.master" Title="CorpGv Visa DOcuments" Codebehind="CorpGvVisaDocuments.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %><asp:content ID="content1" ContentPlaceHolderID="cphTransaction" runat="server">
   <div class="body_container" > 
       <table class="tblmarkup" id="tblMarkup" runat="server" style="width: 100%; height: 250px;">
                    </table> 
        
       
       </div>
  
<script>


    $(document).ready(function () {

        $('.image-popup-vertical-fit').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }

        });

        $('.image-popup-fit-width').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            image: {
                verticalFit: false
            }
        });

        $('.image-popup-no-margins').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });

    });
</script>
<script src="Scripts/jquery.magnific-popup.js"></script>
<link rel="stylesheet" href="css/magnific-popup.css">


<link rel="stylesheet" href="css/fontawesome/css/font-awesome.min.css">

    
</asp:content>
