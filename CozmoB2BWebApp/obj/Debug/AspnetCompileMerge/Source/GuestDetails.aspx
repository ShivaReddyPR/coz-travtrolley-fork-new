﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Codebehind="GuestDetails.aspx.cs" Inherits="GuestDetails" Title="Hotel Guest Details" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script src="Scripts/jsBE/Utils.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="Scripts/Common/Common.js"></script>
    <script type="text/javascript">
    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
function numbersonly()
{
   var data=document.getElementById('ctl00_cphTransaction_txtMarkup').value;
   var filter=/^\d+(?:\.\d{1,2})?$/;
   if(filter.test(data)==false)
   {   
       return false;
   }       
   else
       return true;
}
function validateName(oSrc, args) {
			    args.IsValid = (args.Value.length >= 2);
			}

function calculateMarkup()
{
    var dp = eval('<%=agency.DecimalValue.ToString()%>');
    var markup =!isNumber(document.getElementById('<%=txtMarkup.ClientID %>').value)?0:document.getElementById('<%=txtMarkup.ClientID %>').value;
    document.getElementById('<%=txtMarkup.ClientID %>').value=parseFloat(markup).toFixed(dp);
    var percent = document.getElementById('<%=rbtnPercent.ClientID %>').checked;
    var fixed = document.getElementById('<%=rbtnFixed.ClientID %>').checked;
    var total;
    
    if(window.navigator.appCodeName != "Mozilla")
    {
        total = document.getElementById('<%=lblCost.ClientID %>').innerText;
    }
    else
    {
        total = document.getElementById('<%=lblCost.ClientID %>').textContent;
    }
    total = total.replace(',','');
    var temp = 0;
    var disc = eval('<%=discount %>');
    total = parseFloat(total).toFixed(dp)- parseFloat(disc).toFixed(dp);
     total = Math.ceil(total);
    if(markup.length > 0)
    {
        if(percent == true)
        {
            temp =  eval(parseFloat(total)) +  eval(parseFloat((total * (markup / 100))).toFixed(dp));
                      
        }
        else if (fixed == true)
        {
            temp = eval(parseFloat(total)) + eval(parseFloat(markup).toFixed(dp));
        }
        temp = parseFloat(temp).toFixed(dp);
        if(temp == 0)
        {
            temp = total;
        }
        if(window.navigator.appCodeName != "Mozilla")
        {
            document.getElementById('<%=lblTotal.ClientID %>').innerText = temp;
        }
        else
        {
        document.getElementById('<%=lblTotal.ClientID %>').textContent = temp;
        }
    }
    else
    {
        if(window.navigator.appCodeName != "Mozilla")
        {
            document.getElementById('<%=lblTotal.ClientID %>').innerText = (total);
        }
        else
        {
            document.getElementById('<%=lblTotal.ClientID %>').textContent = (total);
        }
    }
}
function BookHotel()
{

//if(validations() )
     {
    document.getElementById('<%=action.ClientID %>').value ="bookHotel";

     }
//     else
//     {
//         return false; 
//     }   
 }
function validations()
{
    document.getElementById('firstError').style.display = "none";
    document.getElementById('lastError').style.display = "none";
    document.getElementById('EmailError').style.display = "none";
    document.getElementById('MobileError').style.display = "none";
    document.getElementById('AddressError').style.display = "none";
    document.getElementById('CityError').style.display = "none";
    document.getElementById('CountryError').style.display = "none"; 
    document.getElementById('NationalityError').style.display ="none"  ;
    document.getElementById('StateError').style.display = "none";
    document.getElementById('<%=txtFirstName.ClientID %>').value = Trim(document.getElementById('<%=txtFirstName.ClientID %>').value);
    var firstNameU = document.getElementById('<%=txtFirstName.ClientID %>').value.toUpperCase();
    if(document.getElementById('<%=txtFirstName.ClientID %>').value=="" )
    {
      document.getElementById('firstError').style.display = "block";
      document.getElementById('firstError').innerHTML="Please enter First Name";
      return false;
    }
    if (firstNameU=="MR" || firstNameU=="MRS" || firstNameU=="MISS" || firstNameU=="MS" || firstNameU=="DR" || firstNameU=="PHD" || firstNameU=="REV" || firstNameU=="INC" || firstNameU=="CO" || firstNameU=="COMPANY" || firstNameU=="LLC" || firstNameU=="LP" || firstNameU=="LTD")
    {
      document.getElementById('firstError').style.display = "block";
      document.getElementById('firstError').innerHTML="Please do not enter"+" "+ firstNameU +" "+"in First Name";
      return false;
    }
    if(document.getElementById('<%=txtFirstName.ClientID %>').value.length<2)
    {
      document.getElementById('firstError').style.display = "block";
      document.getElementById('firstError').innerHTML="Please enter minimum 2 letters for first name";
      return false;
    }
    var iChars2 = "0123456789";
    var nameRegex = /^[aA-zZ. ]+$/;
    for (var i = 0; i < document.getElementById('<%=txtFirstName.ClientID %>').value.length; i++) 
    {
	    if ((iChars2.indexOf(document.getElementById('<%=txtFirstName.ClientID %>').value.charAt(i)) != -1)) 
	    {
	        document.getElementById('firstError').style.display = "block";
          document.getElementById('firstError').innerHTML ="Please enter only Text for First Name!!";
	        return false;
	    }
	    else if(!nameRegex.test($('<%=txtFirstName.ClientID %>').value))
      {
         document.getElementById('firstError').style.display = "block";
         document.getElementById('firstError').innerHTML = "Please enter valid first name" ;
         return false;
      }
    }
    document.getElementById('<%=txtLastName.ClientID %>').value = Trim(document.getElementById('<%=txtLastName.ClientID %>').value);
    var lastNameU=document.getElementById('<%=txtLastName.ClientID %>').value.toUpperCase();
    if(document.getElementById('<%=txtLastName.ClientID %>').value=="" )
    {    
        document.getElementById('lastError').style.display = "block";
        document.getElementById('lastError').innerHTML="Please enter correct Last Name";
        return false;
    }
    else if (lastNameU=="MR" || lastNameU=="MRS" || lastNameU=="MISS" || lastNameU=="MS" || lastNameU=="DR" || lastNameU=="PHD" || lastNameU=="REV" || lastNameU=="INC" || lastNameU=="CO" || lastNameU=="COMPANY" || lastNameU=="LLC" || lastNameU=="LP" || lastNameU=="LTD")
    {
      document.getElementById('lastError').style.display = "block";
      document.getElementById('lastError').innerHTML="Please do not enter"+" "+ lastNameU +" "+"in Last Name";
      return false;
    }
    else if(document.getElementById('<%=txtLastName.ClientID %>').value.length<2)
    {
      document.getElementById('lastError').style.display = "block";
      document.getElementById('lastError').innerHTML="Please enter minimum 2 letters for last name";
      return false;
    }
    else if(!nameRegex.test($('<%=txtLastName.ClientID %>').value))
    {
       document.getElementById('lastError').style.display = "block";
       document.getElementById('lastError').innerHTML = "Please enter valid name" ;
       return false;
    }
    for (var i = 0; i < document.getElementById('<%=txtLastName.ClientID %>').value.length; i++) 
    {
  	    if ((iChars2.indexOf(document.getElementById('<%=txtLastName.ClientID %>').value.charAt(i)) != -1)) 
  	    {
  	    document.getElementById('lastError').style.display = "block";
        document.getElementById('lastError').innerHTML ="Please enter only Text for Last Name!!";
  	    return false;
  	    }
    }
    
    document.getElementById('<%=txtEmail.ClientID %>').value=Trim(document.getElementById('<%=txtEmail.ClientID %>').value);
    if(document.getElementById('<%=txtEmail.ClientID %>').value=="")
    {   
        document.getElementById('EmailError').style.display = "block";
        document.getElementById('EmailError').innerHTML="Please enter E-mail Address";
        return false;
    }
    if(!ValidEmail.test(Trim(document.getElementById('<%=txtEmail.ClientID %>').value)))
    {
        document.getElementById('EmailError').style.display = "block";    
        document.getElementById('EmailError').innerHTML='Please fill correct Email address';
        return false;
    }

        var nationality = document.getElementById('<%=ddlNationality.ClientID %>');
     if (nationality.options[nationality.selectedIndex].value == "Select Nationality") {
     document.getElementById('NationalityError').style.display = "block";
     document.getElementById('NationalityError').innerHTML = "Please Select your Nationality";
     return false;
     }

     
            
   
      

//    document.getElementById('areaCode').value=Trim(document.getElementById('areaCode').value);
//    if(document.getElementById('areaCode').value=="")
//    {
//        document.getElementById('phoneError').style.display = "block";
//        document.getElementById('phoneError').innerHTML ="Please Enter Area Code";
//        return false;
//    }
//    var iChars2 = "012345678 9";
//    for (var i = 0; i < document.getElementById('areaCode').value.length; i++) 
//    {
//  	    if (!(iChars2.indexOf(document.getElementById('areaCode').value.charAt(i)) != -1)) 
//  	    {
//  	        document.getElementById('phoneError').style.display = "block";
//            document.getElementById('phoneError').innerHTML ="Please enter only Numbers for AreaCode !!";
//  	        return false;
//  	    }
//    }
   
   
   
//   document.getElementById('state').value=Trim(document.getElementById('state').value);
//   if(document.getElementById('state').value=="")
//   {
//      document.getElementById('stateError').style.display = "block";  
//      document.getElementById('stateError').innerHTML ="Please Enter State Name";
//      return false;
//   } 
//  for (var i = 0; i < document.getElementById('zipcode').value.length; i++) 
//  {
//	    if (!(iChars2.indexOf(document.getElementById('zipcode').value.charAt(i)) != -1)) 
//	    {
//	      document.getElementById('zipError').style.display = "block";
//        document.getElementById('zipError').innerHTML ="Please enter only Numbers for ZipCode !!";
//	      return false;
//	    }
//  }
  return true;
}
function chooseHotel()
{
  document.getElementById('<%=action.ClientID %>').value ="changeHotel";
  document.forms[0].submit();
}




  function showFare(fareId)
  {
   if($(fareId).style.display != "block")
    {
      $(fareId).style.display = "block";
    }
    else
    {
      $(fareId).style.display = "none";
    }
  }
    function hide(id)
    {
      $(id).style.display="none";
    }
       
       function sendMail()
       {
        document.getElementById('<%=action.ClientID %>').value ="SendEmail";
         document.forms[0].submit();
       }       
       
       function paxNameValidation()
       {
            var pCount= 0;
            if($('<%=roomCount.ClientID %>')!=null)
            {
              pCount = $('<%=roomCount.ClientID %>').value;
            }
            var nameRegex =/^[aA-zZ. ]+$/;
            for(var i=1;i<=parseInt(pCount);i++)
            {
              var j=1;
              if(i==1)
              {
                j= 2;
              }
              for(;j<=parseInt($("Room" + i + "AdultCount").value);j++)
              {
                var title = Trim($("titleroom" + i + "R1adult" + j).value);
                var firstName = Trim($("firstroom" + i + "R1adult" + j).value);
                var firstNameU= firstName.toUpperCase();
                var lastName = Trim($("lastroom" + i + "R1adult" + j).value);
                var lastNameU= lastName.toUpperCase();
                
                if(title !="" && !nameRegex.test(title))
                {
                   $("TE" + i + "R1adult" + j).style.display = "block";
                   $("TE" + i + "R1adult" + j).innerHTML = "Please enter valid Title";
                   return false;
                }
                $("TE" + i + "R1adult" + j).style.display = "none";
               if(j==1 || eval('<%=(int)itinerary.Source %>') == 7)
                {                  
                  if(firstName =="")
                  {
                     $("FE" + i + "R1adult" + j).style.display = "block";
                     $("FE" + i + "R1adult" + j).innerHTML = "Please fill First Name";
                     return false;
                  }
                  else if(firstNameU=="MR" || firstNameU=="MRS" || firstNameU=="MISS" || firstNameU=="MS" || firstNameU=="DR" || firstNameU=="PHD" || firstNameU=="REV" || firstNameU=="INC" || firstNameU=="CO" || firstNameU=="COMPANY" || firstNameU=="LLC" || firstNameU=="LP" || firstNameU=="LTD")
                  {
                    $("FE" + i + "R1adult" + j).style.display = "block";
                    $("FE" + i + "R1adult" + j).innerHTML = "Please do not enter"+" "+ firstNameU +" "+"in Last Name";
                    return false;
                  }
                  else if(!nameRegex.test(firstName))
                  {
                     $("FE" + i + "R1adult" + j).style.display = "block";
                     $("FE" + i + "R1adult" + j).innerHTML = "Please enter valid First Name";
                     return false;
                  }
                  $("FE" + i + "R1adult" + j).style.display = "none";
                  if(lastName =="")
                  {
                    $("LE" + i + "R1adult" + j).style.display = "block";
                    $("LE" + i + "R1adult" + j).innerHTML = "Please fill Last Name ";
                    return false;
                  }
                  else if(lastNameU=="MR" || lastNameU=="MRS" || lastNameU=="MISS" || lastNameU=="MS" || lastNameU=="DR" || lastNameU=="PHD" || lastNameU=="REV" || lastNameU=="INC" || lastNameU=="CO" || lastNameU=="COMPANY" || lastNameU=="LLC" || lastNameU=="LP" || lastNameU=="LTD")
                  {
                    $("LE" + i + "R1adult" + j).style.display = "block";
                    $("LE" + i + "R1adult" + j).innerHTML = "Please do not enter"+" "+ lastNameU +" "+"in Last Name";
                    return false;
                  }
                  else if(!nameRegex.test(lastName))
                  {
                     $("LE" + i + "R1adult" + j).style.display = "block";
                     $("LE" + i + "R1adult" + j).innerHTML = "Please enter valid Last Name";
                     return false;
                  }
                  $("LE" + i + "R1adult" + j).style.display = "none";
                }
                else
                {
                  if(firstName !="" && !nameRegex.test(firstName))
                  {
                     $("FE" + i + "R1adult" + j).style.display = "block";
                     $("FE" + i + "R1adult" + j).innerHTML = "Please enter valid First Name";
                     return false;
                  }
                  $("FE" + i + "R1adult" + j).style.display = "none";
                  if(lastName !="" && !nameRegex.test(lastName))
                  {
                     $("LE" + i + "R1adult" + j).style.display = "block";
                     $("LE" + i + "R1adult" + j).innerHTML = "Please enter valid Last Name";
                     return false;
                  }
                  $("LE" + i + "R1adult" + j).style.display = "none";
                }
              }
             
              for(var j=1;j<=parseInt($("Room" + i + "ChildCount").value);j++)
              {
                var title = $("titleroom" + i + "R1child" + j).value;
                var firstName = Trim($("firstroom" + i + "R1child" + j).value);
                var firstNameU= firstName.toUpperCase();
                var lastName = Trim($("lastroom" + i + "R1child" + j).value);
                var lastNameU= lastName.toUpperCase();
                if(title !="" && !nameRegex.test(title))
                {
                   $("TE" + i + "R1child" + j).style.display = "block";
                   $("TE" + i + "R1child" + j).innerHTML = "Please enter valid Title";
                   return false;
                }
                $("TE" + i + "R1child" + j).style.display = "none";
                if(eval(<%=(int)itinerary.Source %>)==7)
                {                  
                  if(firstName =="")
                  {
                     $("FE" + i + "R1child" + j).style.display = "block";
                     $("FE" + i + "R1child" + j).innerHTML = "Please fill First Name ";
                     return false;
                  }
                  else if(firstNameU=="MR" || firstNameU=="MRS" || firstNameU=="MISS" || firstNameU=="MS" || firstNameU=="DR" || firstNameU=="PHD" || firstNameU=="REV" || firstNameU=="INC" || firstNameU=="CO" || firstNameU=="COMPANY" || firstNameU=="LLC" || firstNameU=="LP" || firstNameU=="LTD")
                  {
                    $("FE" + i + "R1child" + j).style.display = "block";
                    $("FE" + i + "R1child" + j).innerHTML = "Please do not enter"+" "+ firstNameU +" "+"in First Name";
                    return false;
                  }
                  else if(!nameRegex.test(firstName))
                  {
                     $("FE" + i + "R1child" + j).style.display = "block";
                     $("FE" + i + "R1child" + j).innerHTML = "Please enter valid First Name";
                     return false;
                  }
                  $("FE" + i + "R1child" + j).style.display = "none";
                  if(lastName =="")
                  {
                    $("LE" + i + "R1child" + j).style.display = "block";
                    $("LE" + i + "R1child" + j).innerHTML = "Please fill Last Name ";
                    return false;
                  }
                  else if(lastNameU=="MR" || lastNameU=="MRS" || lastNameU=="MISS" || lastNameU=="MS" || lastNameU=="DR" || firstNameU=="PHD" || lastNameU=="REV" || lastNameU=="INC" || lastNameU=="CO" || lastNameU=="COMPANY" || lastNameU=="LLC" || lastNameU=="LP" || lastNameU=="LTD")
                  {
                    $("LE" + i + "R1child" + j).style.display = "block";
                    $("LE" + i + "R1child" + j).innerHTML = "Please do not enter"+" "+ lastNameU +" "+"in Last Name";
                    return false;
                  }
                  else if(!nameRegex.test(lastName))
                  {
                     $("LE" + i + "R1child" + j).style.display = "block";
                     $("LE" + i + "R1child" + j).innerHTML = "Please enter valid Last Name";
                     return false;
                  }
                  $("LE" + i + "R1child" + j).style.display = "none";
                }
                if(firstName !="" && !nameRegex.test(firstName))
                {
                   $("FE" + i + "R1child" + j).style.display = "block";
                   $("FE" + i + "R1child" + j).innerHTML = "Please enter valid First Name";
                   return false;
                }
                $("FE" + i + "R1child" + j).style.display = "none";
                if(lastName !="" && !nameRegex.test(lastName))
                {
                   $("LE" + i + "R1child" + j).style.display = "block";
                   $("LE" + i + "R1child" + j).innerHTML = "Please enter valid Last Name";
                   return false;
                }
                $("LE" + i + "R1child" + j).style.display = "none";
              }
            } 
            return true;   
        }
    </script>
<script>
    function AssignValue(id,paxType) {
        var index = id.slice(-2);
        if (paxType == "C") {
            document.getElementById('ctl00_cphTransaction_hdfDept' + index).value = "";
            var val = document.getElementById(id).options[document.getElementById(id).selectedIndex].text;
            document.getElementById('ctl00_cphTransaction_hdfDept' + index).value = val;
        }
        else if (paxType == "A") {
            document.getElementById('ctl00_cphTransaction_hdfADept' + index).value = "";
            var val = document.getElementById(id).options[document.getElementById(id).selectedIndex].text;
            document.getElementById('ctl00_cphTransaction_hdfADept' + index).value = val;
        }
    }
</script>
 <script type="text/javascript">
     var specialKeys = new Array();
     specialKeys.push(8); //Backspace
     specialKeys.push(9); //Tab
     specialKeys.push(46); //Delete
     specialKeys.push(36); //Home
     specialKeys.push(35); //End
     specialKeys.push(37); //Left
     specialKeys.push(39); //Right
     function IsAlphaNumeric(e) {
         var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
         var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
         return ret;
     }
    </script>
    <asp:HiddenField ID="roomCount" runat="server" Value="<%=request.NoOfRooms %>" />
    <asp:HiddenField ID="action" runat="server" />
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    
   <div> 
   
   <div class=" col-md-3 padding-0 bg_white">
     <div class="ns-h3">Pricing Details </div>
   
   
  
   <div>  <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="gray-smlheading">
                                    <strong>Product</strong>
                                </td>
                                <td class="gray-smlheading">
                                    <strong>Price</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hotel Cost
                                </td>
                                <td>
                                    <%=(itinerary.Currency) %>
                                    <asp:Label ID="lblCost" runat="server" Text=""></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    (Inclusive of all Taxes &amp; Fees)
                                </td>
                            </tr>
                            <%if (discount > 0)
                              { %>
                              <tr>
                                <td>
                                    <strong>Discount</strong>
                                </td>
                                <td>
                                    <strong><%=(itinerary.Currency)%>
                                        <%=Math.Round(discount,2).ToString("N"+agency.DecimalValue.ToString()) %></strong>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td>
                                    <strong>Total</strong>
                                </td>
                                <td>
                                    <strong><%=(itinerary.Currency)%>
                                        <asp:Label ID="lblTotal" runat="server" ></asp:Label></strong>
                                </td>
                            </tr>
                        </table></div>
   
   
   <div class="ns-h3"> Markup</div>
   
   
   <div>  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="69%">
                                
                                <table> 
                                
                                <tr> 
                                
                                <td> <strong>Add Markup &nbsp; </strong></td>
                                <td> <asp:TextBox ID="txtMarkup" CssClass="form-control"  Width="70px" MaxLength="10" runat="server" onblur="calculateMarkup();" onkeypress="return isNumber(event);"  ondrop="return false;" Text="0"></asp:TextBox></td>
                                
                                </tr> 
                                
                                </table>
                                
                                
                                     
                                    
                                </td>
                                <td width="15%">
                                    P
                                    <label>
                                        <asp:RadioButton ID="rbtnPercent" runat="server" Checked="True" 
                                        GroupName="markup" onclick="calculateMarkup();"/>
                                    </label>
                                </td>
                                <td width="15%">
                                    F
                                     <label>
                                    <asp:RadioButton ID="rbtnFixed" runat="server" GroupName="markup" onclick="calculateMarkup();"/>
                                     </label>
                                </td>
                            </tr>
                        </table></div>
                 
<div class="ns-h3">Hotel Review</div>   
     <div>
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="42%">
                                    <strong>Hotel name :</strong>
                                </td>
                                <td width="58%" style="font-weight:bold">
                                    <%=itinerary.HotelName %>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>City Name : </strong>
                                    <br />
                                </td>
                                <td>
                                    <%=itinerary.CityRef %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Check in:
                                        <br />
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.StartDate.ToString("dd-MMM-yyyy") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Check out:
                                        <br />
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.EndDate.ToString("dd-MMM-yyyy") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Nights: </strong>
                                </td>
                                <td>
                                    <%=itinerary.EndDate.Subtract(itinerary.StartDate).Days%>
                                </td>
                            </tr>
                        </table>
                    </div>               
   
   
   <div class="ns-h3"> Room Details</div>
  
  <div> 
    <div>
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <%--<tr>
                                <td width="42%">
                                    <strong>Hotel name :</strong>
                                </td>
                                <td width="58%">
                                    <asp:Label ID="lblHotelName" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>--%>
                            <%int adults = 0, childs = 0;
                              foreach (HotelRoom room in itinerary.Roomtype)
                              {
                                  adults += room.AdultCount;
                                  childs += room.ChildCount;
                              }%>
                            <tr>
                                <td>
                                    <strong>Rooms: </strong>
                                        <%--<asp:Label ID="lblRoomName" Font-Bold="true" runat="server" Text=""></asp:Label>--%>
                                        <%=itinerary.Roomtype.Length %>
                                    
                                </td>
                                <td>
                                    Adults 
                                    <%--<asp:Label ID="lblAdult" runat="server" Text=""></asp:Label>--%>
                                    <%=adults %>
                                    , Child 
                                    <%--<asp:Label ID="lblChild" runat="server" Text=""></asp:Label>--%>
                                    <%=childs %>                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>Price :</strong>
                                </td>
                                <td>
                                    <strong><%=(itinerary.Currency)%>
                                        <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                        </strong>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
  
  </div>
   
   
   
    </div>
   
   
  <div class=" col-md-9">
  
   <div>
                
                <div class="">
                <div class="ns-h3">
                    Please enter the details of the guest.
                </div>
                    <div class="bg_white guest-details-form" style=" border:solid 1px #ccc; border-top:0px; margin-bottom:10px; padding-bottom:10px;">
                      
                      
                      <div> 
                    
                    
                      <div class="subgray-header marbot_10"> <b>
                                                    <%=(itinerary.Roomtype[0].RoomName)%> <asp:Label ID="Label1" runat="server" Text=" Adult 1 (Lead guest)" />
                                                </b></div>
                      
                      <div class="col-md-12 padding-0">                             
            <div class="col-md-3"> <strong>Title<br />
                                                    </strong></div>
            <div class="col-md-3"> 
                                                    <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control" CausesValidation="True"
                                                        ValidationGroup="pax">
                                                        <asp:ListItem Selected="True">Select</asp:ListItem>
                                                        <asp:ListItem>Mr.</asp:ListItem>
                                                        <asp:ListItem>Ms.</asp:ListItem>
                                                       <%-- <asp:ListItem>Dr.</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                               
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="ddlTitle"
                                                    ErrorMessage="Select Title" InitialValue="Select" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator></div>
            
            
            <div class="col-md-3"><strong>First Name*<br />
                                                    </strong> </div>
            <div class="col-md-3">   <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" CausesValidation="True"
                                                    ValidationGroup="pax" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"  MaxLength="20"></asp:TextBox>
                                                <b class="error" style="display: none" id="firstError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="txtFirstName"
                                                    ErrorMessage="Enter First Name" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" ControlToValidate="txtFirstName" ClientValidationFunction="validateName" SetFocusOnError="True" ValidationGroup="pax" ErrorMessage="Minimum 2 alphabets required"></asp:CustomValidator></div>
            
            <div class=" clearfix"> </div>
                </div>
                
                
                
                <div>                             
            <div class="col-md-3"><strong>Last Name*<br />
                                                    </strong> </div>
            <div class="col-md-3">  <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" CausesValidation="True"
                                                    ValidationGroup="pax" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"  MaxLength="20"></asp:TextBox>
                                                <b class="error" style="display: none" id="lastError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ErrorMessage="Enter Last Name"
                                                    ControlToValidate="txtLastName" SetFocusOnError="true" ValidationGroup="pax" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;" ></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="CustomValidator2" runat="server" Display="Dynamic" ControlToValidate="txtLastName" ClientValidationFunction="validateName" SetFocusOnError="True" ValidationGroup="pax" ErrorMessage="Minimum 2 alphabets required"></asp:CustomValidator></div>
            
            
            <div class="col-md-3"> <strong>E-mail ID*<br />
                                                </strong></div>
            
            <div class="col-md-3"> <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" MaxLength="50" ondrop="return false;" ></asp:TextBox>
                                                <b class="error" style="display: none" id="EmailError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic" ErrorMessage="Enter Email" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="pax"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ErrorMessage="Invalid Email" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="pax" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator> </div>
            
            <div class=" clearfix"> </div>
                </div>
                
                
                <div>                             
            <div class="col-md-3"><strong>Nationality*</strong> </div>
            
            
            <div class="col-md-3">             <asp:TextBox ID="txtNationality" runat="server" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" Enabled="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddlNationality" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" Visible="false">
                                                </asp:DropDownList>
                                                <b class="error" style="display: none" id="NationalityError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic" ErrorMessage="Choose Nationality" SetFocusOnError="true" ControlToValidate="ddlNationality" ValidationGroup="pax" InitialValue="Select Nationality"></asp:RequiredFieldValidator></div>
            
            
            
            
            
            
            <div class="col-md-3"> <strong>LPO:</strong></div>
            
            
            <div class="col-md-3"><asp:TextBox ID="txtPaymentNotes" runat="server"  CssClass="form-control" onkeypress='return (this.value.length < 100);' ondrop="return false;" ></asp:TextBox> </div>
            
            <div class=" clearfix"> </div>
                </div>
                
             
             
             <div>   
             
             
            
             <%if (cancellationInfo.ContainsKey("RequireCityCode") )
                                          { %>                  
            <div class="col-md-3"><strong>City*<br />
                                                </strong> </div>
           
            <div class="col-md-3"> 
            
               <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax"></asp:TextBox>
                                                <b class="error" style="display: none" id="CityError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"  Display="Dynamic" ErrorMessage="Enter City" SetFocusOnError="true" ControlToValidate="txtCity" ValidationGroup="pax"></asp:RequiredFieldValidator>
            
            </div>
            
             <%} %>
            
            
            
                <%if (cancellationInfo.ContainsKey("RequireCountryCode"))
                                          { %>
            
                        <div class="col-md-3"> <strong>Country*</strong></div>
            <div class="col-md-3"> 
            
             <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax">
                                                </asp:DropDownList>
                                                <b class="error" style="display: none" id="CountryError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ErrorMessage="Choose Country" SetFocusOnError="true" ControlToValidate="ddlCountry" ValidationGroup="pax" InitialValue="Select Country"></asp:RequiredFieldValidator>
            
            </div>
     
                                        
            <%} %>
                                        
                                        
                                        
                                        
            
       
            
            <div class=" clearfix"> </div>
                </div>
                
                           

                                        
           <div>        
                   
                    
                <%if (cancellationInfo.ContainsKey("RequireFullAddress"))
                                          { %>                
            <div class="col-md-3"> <strong>Address*<br />
                                                    </strong></div>
            <div class="col-md-3"> 
                        <asp:TextBox ID="txtAddress" runat="server" Rows="1" TextMode="MultiLine" CssClass="form-control" CausesValidation="True" ValidationGroup="pax"></asp:TextBox>
                                                <b class="error" style="display: none" id="AddressError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ErrorMessage="Enter Address" ControlToValidate="txtAddress" SetFocusOnError="true" ValidationGroup="pax"></asp:RequiredFieldValidator>
            
            </div>
            
            
              <%} %>
              
              <%if ( cancellationInfo.ContainsKey("RequireCountryAccessCode"))
                                          { %>                              
            <div class="col-md-3"> <strong>Country Access Code</strong></div>
               <div class="col-md-3">
                      <asp:TextBox ID="txtCountryAccessCode" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" MaxLength="5"></asp:TextBox>
                                                
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" runat="server" ErrorMessage="Enter Country Access Code" SetFocusOnError="true" ControlToValidate="txtCountryAccessCode" ValidationGroup="pax"></asp:RequiredFieldValidator>   
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="Dynamic" runat="server" ErrorMessage="Invalid Country Access Code" SetFocusOnError="true" ControlToValidate="txtCountryAccessCode" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>  
            
             </div>
             <%} %>               
            
            <div class=" clearfix"> </div>
                </div>
                
           

   
   
   <div> 
   
   
                               <%if ( cancellationInfo.ContainsKey("RequireState"))
                                          { %>                              
            <div class="col-md-3"> <strong>State*</strong></div>
              <div class="col-md-3"> 
                          <asp:TextBox ID="txtState" runat="server" MaxLength="25" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" runat="server" ErrorMessage="Enter State Name" SetFocusOnError="true" ControlToValidate="txtState" ValidationGroup="pax" InitialValue=""></asp:RequiredFieldValidator>
                           
                
            </div>

             <%} %> 
   
             
            
        <%if ( cancellationInfo.ContainsKey("RequireAreaCode"))
                                          { %>                              
            <div class="col-md-3"> <strong>Area Code</strong></div>
            <div class="col-md-3"> 
                          <asp:TextBox ID="txtAreaCode" runat="server" MaxLength="5" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                                                 
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="Dynamic" runat="server" ErrorMessage="Invalid Area Code" SetFocusOnError="true" ControlToValidate="txtAreaCode" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            
                
            </div>
            
            <%} %>    
            
            <div class=" clearfix"> </div>
                </div>
                
                
     <%--Added by somasekhar on 12/09/2018 -- Customer   Details  --%> 
   

          <div> 
   

           <%if (cancellationInfo.ContainsKey("RequirePostalCode"))
                                          { %>                              
            <div class="col-md-3"> <strong>Postal Code*</strong></div>
            <div class="col-md-3"> 
                          <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="10" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ErrorMessage="Enter Postal Code" SetFocusOnError="true" ControlToValidate="txtPostalCode" ValidationGroup="pax" InitialValue=""></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="Dynamic" runat="server" ErrorMessage="Invalid Postal Code" SetFocusOnError="true" ControlToValidate="txtPostalCode" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            
               
            </div>
            
            <%} %> 
        
            
                                         
                 <%if (cancellationInfo.ContainsKey("RequireTelephoneHolder") )
                                          { %>
                                          
            <div class="col-md-3"> <strong>Mobile No.*<br /> </strong></div> 
            <div class="col-md-3">
                      <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" MaxLength="15"></asp:TextBox>
                                                <b class="error" style="display: none" id="MobileError"></b>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ErrorMessage="Enter Phone Number" SetFocusOnError="true" ControlToValidate="txtMobile" ValidationGroup="pax"></asp:RequiredFieldValidator>   
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" runat="server" ErrorMessage="Invalid Phone Number" SetFocusOnError="true" ControlToValidate="txtMobile" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>  
            
             </div>
            
             <%} %>


           
            <div class=" clearfix"> </div>
                </div>                        
                                        
      <%--Added by somasekhar on 12/09/2018 --  GST Details  --%>                                               
                 
          <div>  
             <%if ( cancellationInfo.ContainsKey("RequireGSTNumber"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Number</strong></div>
              <div class="col-md-3"> 
                          <asp:TextBox ID="txtGSTNumber" runat="server" MaxLength="15" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="Dynamic" runat="server" ErrorMessage="Invalid GST Number" SetFocusOnError="true" ControlToValidate="txtGSTNumber" ValidationGroup="pax" ValidationExpression="\d{2}[a-zA-Z]{5}\d{4}[a-zA-Z]{1}\d[zZ]{1}[a-zA-Z\d]{1}"></asp:RegularExpressionValidator>
                        
            </div>

             <%} %> 
            
        <%if ( cancellationInfo.ContainsKey("RequireGSTCompanyName"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Company Name</strong></div>
            <div class="col-md-3"> 
                 <asp:TextBox ID="txtGSTCompanyName" runat="server" MaxLength="100" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                     
            </div>
            
            <%} %>    
            
            <div class=" clearfix"> </div>
                </div>                           
                                         
          <div>  
             <%if ( cancellationInfo.ContainsKey("RequireGSTCompanyEmailId"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Company EmailId</strong></div>
              <div class="col-md-3"> 
                          <asp:TextBox ID="txtGSTCompanyEmailId" runat="server" MaxLength="150" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator7" Display="Dynamic" runat="server" ErrorMessage="Invalid GST Company EmailId" SetFocusOnError="true" ControlToValidate="txtGSTCompanyEmailId" ValidationGroup="pax" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        
            </div>

             <%} %> 
            
        <%if ( cancellationInfo.ContainsKey("RequireGSTCompanyAddress"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Company Address</strong></div>
            <div class="col-md-3"> 
                 <asp:TextBox ID="txtGSTCompanyAddress" runat="server" Rows="1" TextMode="MultiLine" MaxLength="250" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                     
            </div>
            
            <%} %>    
            
            <div class=" clearfix"> </div>
                </div>      
           
          <div>  
             <%if ( cancellationInfo.ContainsKey("RequireGSTCity"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST City</strong></div>
              <div class="col-md-3"> 
                   <asp:TextBox ID="txtGSTCity" runat="server" MaxLength="50" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                       
            </div>

             <%} %> 
            
        <%if ( cancellationInfo.ContainsKey("RequireGSTPinCode"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST PinCode</strong></div>
            <div class="col-md-3"> 
                 <asp:TextBox ID="txtGSTPinCode" runat="server" MaxLength="10" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="Dynamic" runat="server" ErrorMessage="Invalid GST PinCode" SetFocusOnError="true" ControlToValidate="txtGSTPinCode" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                       
            </div>
            
            <%} %>    
            
            <div class=" clearfix"> </div>
                </div>                
                    
          <div>  
             <%if ( cancellationInfo.ContainsKey("RequireGSTState"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST State</strong></div>
              <div class="col-md-3"> 
                   <asp:TextBox ID="txtGSTState" runat="server" MaxLength="25" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                       
            </div>

             <%} %> 
            
        <%if ( cancellationInfo.ContainsKey("RequireGSTPhoneISD"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Phone ISD</strong></div>
            <div class="col-md-3"> 
                 <asp:TextBox ID="txtGSTPhoneISD" runat="server" MaxLength="5" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator9" Display="Dynamic" runat="server" ErrorMessage="Invalid GST Phone ISD Code" SetFocusOnError="true" ControlToValidate="txtGSTPhoneISD" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                       
            </div>
            
            <%} %>    
            
            <div class=" clearfix"> </div>
                </div>

          <div>  
             <%if ( cancellationInfo.ContainsKey("RequireGSTPhoneNumber"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Phone Number</strong></div>
              <div class="col-md-3"> 
                   <asp:TextBox ID="txtGSTPhoneNumber" runat="server" MaxLength="15" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" Display="Dynamic" runat="server" ErrorMessage="Invalid GST Phone Number" SetFocusOnError="true" ControlToValidate="txtGSTPhoneNumber" ValidationGroup="pax" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                     
            </div>

             <%} %> 
            
        <%if ( cancellationInfo.ContainsKey("RequireGSTCustomerName"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Customer Name</strong></div>
            <div class="col-md-3"> 
                 <asp:TextBox ID="txtGSTCustomerName" runat="server" MaxLength="100" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
                      
            </div>
            
            <%} %>    
            
            <div class=" clearfix"> </div>
                </div>

          <div>  
                        
        <%if ( cancellationInfo.ContainsKey("RequireGSTCustomerAddress"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Customer Address</strong></div>
            <div class="col-md-3">  
                 <asp:TextBox ID="txtGSTCustomerAddress" runat="server" Rows="1" TextMode="MultiLine" MaxLength="250" CssClass="form-control" CausesValidation="True" ValidationGroup="pax"></asp:TextBox>
            </div>
            
            <%} %>   
               <%if ( cancellationInfo.ContainsKey("RequireGSTCustomerState"))
                                          { %>                              
            <div class="col-md-3"> <strong>GST Customer State</strong></div>
              <div class="col-md-3"> 
                   <asp:TextBox ID="txtGSTCustomerState" runat="server" MaxLength="25" CssClass="form-control" CausesValidation="true" ValidationGroup="pax" ></asp:TextBox>
               </div>

             <%} %> 
            
            <div class=" clearfix"> </div>
                </div>
                       
                              <%if (agency.AddnlPaxDetails == "Y")
                          { %>
                      
                      
                      
                      
                      <div>                             
            <div class="col-md-3"><strong>Department*<br />
                                        </strong> </div>
            <div class="col-md-3">      
            
             <asp:DropDownList CssClass="form-control" ID="ddlDepartment" runat="server" CausesValidation= "true" ValidationGroup="pax"></asp:DropDownList>
                                    <%--<asp:TextBox ID="ddlD" runat="server" Width="200px" CausesValidation="True" ValidationGroup="pax"></asp:TextBox>--%>
                                    <b class="error" style="display: none" id="deptError"></b>
                                    <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" Display="Dynamic" ControlToValidate="ddlDepartment"
                                        ErrorMessage="Enter Department Name" InitialValue="Select Department" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator></div>
            
            
            <div class="col-md-3"> <strong>Division*<br />
                                        </strong></div>
            
            
            <div class="col-md-3">           <asp:TextBox ID="txtDivision" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" MaxLength="50" ondrop="return false;" ></asp:TextBox>
                                    <b class="error" style="display: none" id="divisionError"></b>
                                    <asp:RequiredFieldValidator ID="rfvDivision" runat="server" Display="Dynamic" ControlToValidate="txtDivision"
                                        ErrorMessage="Enter Division Name" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator></div>
            
            <div class=" clearfix"> </div>
                </div>
                
                
                
                
                <div>                             
            <div class="col-md-3"> <strong>Employee ID*<br />
                                        </strong></div>
            <div class="col-md-3">                      <asp:TextBox ID="txtEmpID" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" MaxLength="15" ondrop="return false;" ></asp:TextBox>
                                    <b class="error" style="display: none" id="empIDError"></b>
                                    <asp:RequiredFieldValidator ID="rfvEmpID" runat="server" Display="Dynamic" ControlToValidate="txtEmpID"
                                        ErrorMessage="Enter Employee ID" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator></div>
            
            
            
            
            <div class="col-md-3"> <strong>Employee*<br />
                                        </strong></div>
            
            
            <div class="col-md-3">
                     <asp:TextBox ID="txtEmployee" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" MaxLength="50" ondrop="return false;" ></asp:TextBox>
                                    <b class="error" style="display: none" id="empError"></b>
                                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" Display="Dynamic" ControlToValidate="txtEmployee"
                                        ErrorMessage="Enter Employee Name" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator>
             </div>
            
            <div class=" clearfix"> </div>
                </div>
                
                <div>                             
            <div class="col-md-3"> <strong>Purpose of Travel*<br />
                                        </strong></div>
            <div class="col-md-9"><asp:TextBox ID="txtPurpose" runat="server" CssClass="form-control" CausesValidation="True" ValidationGroup="pax" TextMode="MultiLine" onkeypress='return (this.value.length < 250);' ondrop="return false;" ></asp:TextBox>
                                    <b class="error" style="display: none" id="purposeError"></b>
                                    <asp:RequiredFieldValidator ID="rfvPurposs" runat="server" Display="Dynamic" ControlToValidate="txtPurpose"
                                        ErrorMessage="Enter Purpose of travel" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator> </div>


                                <div class=" clearfix"></div>
                            </div>












                            <%} %>

                            <div class="" id="tblFlexFields" runat="server">
                            </div>


                            <div class="clearfix"></div>

                        </div>

                        <asp:HiddenField ID="hdnFlexCount" runat="server" Value="0" />

                        <div style="padding-top: 20px; border-top: solid 1px #ccc;">






                            <table id="tblAdults" style="display: block;" width="100%" border="0" cellspacing="3" cellpadding="0" runat="server">
                            </table>
                        </div>
                    </div>
                    
                    
                     </div>
                     
                    
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     
                      <%--  <tr>
                            <td colspan="2">
                                   
                  <%--  <div style=" border:solid 1px #ccc; border-top:0px; margin-bottom:10px; padding-bottom:10px;">
                                <table id="tblChilds" width="100%" border="0" cellspacing="3" cellpadding="0" runat="server">
                                
                                    
                                </table><br />
                            </div>
                 
                    </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2">
                                <div style="padding: 10px;overflow:auto">
                                 <% 
                        
                     %>
                                   <div> <span style="color: red; text-decoration: underline">Cancellation Charges :</span> </div>
                                   <ul style="float: left; font-family:Calibri; font-size:14px;">
                                    <%string[] cdata = cancelData.Split('|');
                                      foreach (string data in cdata)
                                      { %>
                                       <li><%=data%></li>                                           
                                          <%}%>
                                          </ul>
                                          </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="color: Red; font-size: 14px">
                            <%=warningMsg %>
                        </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                
                                <label style="padding-right: 10px">                                    
                                    <asp:Button ID="imgContinue" runat="server" CssClass="but but_b" Text="Continue"  
                                    OnClientClick="BookHotel();" 
                                    OnClick="imgContinue_Click" CausesValidation="true" ValidationGroup="pax" />
                                    </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
  
  
  
  
  
   </div>
  
  <div class="clearfix"> </div>
   
   </div> 
    
    
    
    
    

</asp:Content>
