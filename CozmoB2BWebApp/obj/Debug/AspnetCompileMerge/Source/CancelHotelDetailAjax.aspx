<%@ Page Language="C#" AutoEventWireup="true" Inherits="CancelHotelDetailAjax" Codebehind="CancelHotelDetailAjax.aspx.cs" %>
<link rel="stylesheet" type="text/css" href="Styles/BookingStyle.css" />
<div class="cancellation_module">
<div class='showMsgHeading'>Cancellation and Charges</div>

<%
    if (result.BookingSource == CT.BookingEngine.HotelBookingSource.Miki)
    {%>
    <a style="position:absolute; top: 5px; right:10px" class="closex" href="#" onclick="javascript:hideCP('room<%=roomTypeCode %>')">X</a>
    <%}
    else if (result.BookingSource == CT.BookingEngine.HotelBookingSource.HotelBeds || result.BookingSource == CT.BookingEngine.HotelBookingSource.JAC) //Modified Brahmam 26.09.2014..........Modified on @@@ 02082016 by chandan ,Remove TBOHotel Source
    { %>
<a style="position:absolute; top: 5px; right:10px" class="closex" href="#" onclick="javascript:hideCP('room<%=Request["rCode"].Replace(" ", "+") %>')">X</a>
<%}
   
    else
    { %>
  <a style="position:absolute; top: 5px; right:10px" class="closex" href="#" onclick="javascript:hideCP('room<%=Request["rCode"] %>')">X</a>
  <%} %>
<%--<em class="close_button_cancellation">



<img src="Images/close.gif" alt="Close" onclick="javascript:hideCP('room<%=Request["rCode"] %>')"/>




</em>--%>
<%if (cancelInfo != null && cancelInfo.Length > 0)
  {%>

                      <ul style="width:100%; float:right; padding-left:0px;">
        <% if (result.BookingSource == CT.BookingEngine.HotelBookingSource.DOTW)
                         {
                             for (int j = 0; j < cancelInfo.Length - 1; j++)
                             { %>
                        <li style=" padding-top:3px;"><%=j + 1%>. <%=cancelInfo[j]%></li>
                          
                        <%}
                         }
                         else
                         {
                             for (int j = 0; j < cancelInfo.Length; j++)
                             {
                                 if (cancelInfo[j].Length > 0)
                                 {%>
                             <li><%=j + 1%>. <%=cancelInfo[j]%></li>
                             <%}
                             }
                         } %>
                      </ul> 
                      <%if (amendmentMessage != null && amendmentMessage.Length > 0)
                        { %>
                      <p><b>Amendment Charges</b></p>
                      <ul style="width:100%; float:left;">
                      <% for (int j = 0; j < amendmentMessage.Length - 1; j++)
                         { if(amendmentMessage[j].Length > 0){%>
                        <li><%=j + 1%>. <%=amendmentMessage[j]%></li>
                          
                        <%}} %>
                      </ul> 
                      <%}
                    }
                    else
                    {
           %>
           <p><b><%=errorMsg%></b></p>
           <%} %>
                       <%--<div class="close_window" ><span class="inactive" onmouseover="this.className='active'" onmouseout="this.className='inactive'" onclick="javascript:hideCP('room<%=Request["rCode"] %>')">Close Window</span></div>--%>
</div>
<div id='Meals' style='display:none'>
<%=amenities %>
</div>
