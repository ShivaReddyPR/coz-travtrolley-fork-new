﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ApiRoomDetails.aspx.cs" Inherits="CozmoB2BWebApp.ApiRoomDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">


    <%--   <script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
    <!-- New BE js -->



    <!-- end new Js -->

    <!-- end -->

    <!-- jQuery -->
    <!-- Bootstrap Core JavaScript -->
    <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />

    <!-- Required styles for Hotel Listing-->
    <link href="build/css/royalslider.css" rel="stylesheet" type="text/css" />
    <link href="build/css/rs-default.css" rel="stylesheet" type="text/css" />
    <style>
        .room-number-label {
            position: absolute;
            background-color: #000000;
            font-size: 11px;
            text-transform: uppercase;
            padding: 5px 4px 4px 5px;
            top: 0;
            left: 0;
            color: #fff;
            font-weight: bold;
            z-index: 21000;
            display: block;
        }

        .rate-breakup-div {
            position: absolute;
            right: 0px;
            width: 430px;
            margin-top: 10px;
            z-index: 10000;
            background-color: #fff;
        }

        .rate-breakup-close {
            top: 0px;
            right: 5px;
            color: #000;
            font-size: 16px;
        }
    </style>
    <input type="hidden" id="hdnObjRequest" runat="server" />
    <input type="hidden" id="hdnUserId" runat="server" />
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnBehalfLocation" runat="server" />
    <input type="hidden" id="hdnHotelCode" runat="server" />
    <input type="hidden" id="hdnSessionId" runat="server" />
    <input type="hidden" id="hdnRoomTypeCode" runat="server" />
    <input type="hidden" id="hdnRoomResult" runat="server" />
    <input type="hidden" id="hdnIsAgent" runat="server" /><!-- to restrict Supplire name 0=b2b|b2b2b , 1 =agent|base -->
    <div class="cz-container">


        <div class="body_container ui-listing-page loading-progress htldetail-page">

            <div class="results-list-wrapper pt-3">
                <div class="row custom-gutter">
                    <div class="col-12 col-lg-8">
                        <div class="ui-bg-wrapper">
                            <h3 id="h3HotelName"></h3>
                            <p class="text-address mb-4" id="pAddress"></p>

                            <!--Gallery-->
                            <div class="royalSlider rsUni hotel-detail-slider zoom-gallery" id="imgGallery">
                            </div>

                        </div>

                    </div>
                    <div class="col-12 col-lg-4">

                        <div class="ui-hotel-map">
                            <button type="button" class="btn" data-toggle="modal" data-target="#hotelMap">
                                <span class="icon icon-map-pin"></span>VIEW MAP
                            </button>
                        </div>

                        <div class="ui-hotel-review ui-bg-wrapper mb-2" style="display: none">
                            <div class="row custom-gutter review-rating">
                                <div class="col-4">
                                    <span class="rate primary-bgcolor">3.5</span>
                                </div>
                                <div class="col-8 text-right">
                                    <span class="review-word primary-color">Excellent</span>
                                    <p>Based on 1988 Reviews</p>
                                    <p>Reviews by<img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo"></p>
                                </div>
                            </div>
                        </div>

                        <div class="ui-bg-wrapper">
                            <div class="row">

                                <%--For room summary--%>
                                <ul class="ui-listing-wrapper list-unstyled" id="list-group-roomsummary">
                                </ul>
                                <%--End -  For room summary--%>
                                <div class="col-md-12 mt-2">
                                    <div class="row hotel-bookingdtl-dateinfo">
                                        <div class="col-md-12">
                                            <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-in</span>
                                            <span class="float-right text-gray-dark text-weight-400" id="startDate"></span>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-out</span>
                                            <span class="float-right text-gray-dark text-weight-400" id="endDate"></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <div class="row hotel-bookingdtl-price mb-3 py-3">
                                        <div class="col-sm-8 col-lg-12 col-xl-6">
                                            <span class="text pt-3" id="spanTotalPrice"></span>
                                        </div>
                                        <div class="col-sm-4 col-lg-12 col-xl-6">
                                            <div class="ui-list-price text-right">
                                                <span class="price d-inline-block d-sm-block strikeout" id="beforeTotalPrice"></span>
                                                <span class="price d-inline-block d-sm-block" id="TotalPrice"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-12 col-xl-6"><a href="javascript:void(0);" class="btn btn-link" onclick="window.location.href='ApiHotelResults.aspx'">Check other Hotel rooms</a></div>
                                        <div class="col-md-6 col-lg-12 col-xl-6 text-right book-btn-wrapper">
                                            <input type="button" class="btn btn-primary font-weight-bold" id="btnBookNow" onclick="BookRoom();" value="BOOK NOW" />
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row custom-gutter loadingHide" style="display: none">
                    <div class="col-12">
                        <h3 class="pt-5">Room Choices</h3>
                        <%--For room type selection--%>
                        <ul class="ui-listing-wrapper list-unstyled" id="list-group-rooms">
                        </ul>
                    </div>
                </div>
                <%--  Hotel Over View--%>
                <div class="row custom-gutter loadingHide" style="display: none">
                    <div class="col-12">
                        <h3 class="pt-5 pb-3">Hotel Overview</h3>
                        <div class="ui-bg-wrapper">
                            <h6 class="pl-3 mt-3 mb-0 font-weight-bold">Amenities</h6>
                            <%--For Hotel Amenities--%>

                            <ul class="amenities-list" id="Amenities">
                            </ul>
                            <div class="clearfix"></div>
                            <h6 class="mt-3 mb-2 px-3 font-weight-bold">General Description</h6>
                            <div class="hotel-description px-3" id="HotelDescription">
                            </div>
                            <div class="hotel-description px-3" style="display: none">

                                <p>
                                    Check-in: From 03:00 PM<br>
                                    Check-out: Until 12:00 PM
                                </p>
                            </div>
                            <h6 class="mt-3 mb-2 px-3 font-weight-bold">Location</h6>
                            <div class="hotel-description px-3">
                                <p id="pLocation"></p>
                            </div>
                            <h6 class="mt-3 mb-2 px-3 font-weight-bold" id="h6Rooms" style="display: none">Rooms </h6>
                            <div class="hotel-description px-3" id="roomLevelAmenities">
                            </div>


                        </div>
                    </div>
                </div>
                <%--  End - Hotel  Over View--%>
                <%--  Hotel Reviews--%>
                <div class="row custom-gutter" style="display: none">
                    <div class="col-12 mb-5">
                        <h3 class="pt-5 pb-3 mb-2 heading-h3">Reviews by TrustYou</h3>
                        <div class="ui-bg-wrapper">
                            <div class="row custom-gutter top-reviews mt-2">
                                <div class="col-md-4 col-xs-12">
                                    <span class="excellent rating text-weight-500">3.5</span>
                                    <span class="category text-weight-500 pl-2">Excellent</span>
                                </div>
                                <div class="col-md-8 text-right">
                                    <p>
                                        Verified reviews by
								<img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                                    </p>
                                </div>
                            </div>
                            <div class="row mt-4 top-reviews">
                                <div class="col-md-7 py-4">
                                    <h3 class="heading-h3 mb-3">Top Review </h3>
                                    <p>
                                        <span class="icon-quotes-left"></span>stay in mercure is a nice experiance for me. my room always well cleaned by staff muhammad fezan, vinogen and supervisor Jagdish. <span class="icon-quotes-right"></span>
                                    </p>
                                    <hr>
                                    <p>
                                        <span class="icon-quotes-left"></span>was in Mercure room 1031, Suresa from concierge make us feel at home and wonderful stay and im flattered with Suresa service and kindness. I enjoyed Dubai very much, nice people nice food very good service <span class="icon-quotes-right"></span>
                                    </p>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-4 badges  py-4">
                                    <h3 class="heading-h3 mb-3">Badges</h3>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <p class="text-weight-500"><span class="icon-medal badges-rating"></span>Excellent wellness hotel</p>
                                            <p>
                                                <span class="text-muted text-small">Top 7% in city </span>
                                            </p>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <p class="text-weight-500"><span class="icon-medal badges-rating"></span>Great Family Hotel</p>
                                            <p>
                                                <span class="text-muted text-small">Top 12% in city </span>
                                            </p>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <p class="text-weight-500"><span class="icon-medal badges-rating"></span>Excellent business hotel</p>
                                            <p>
                                                <span class="text-muted text-small">Top 9% in city </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--  End - Hotel Reviews--%>
            </div>

        </div>


        <!-- Hotel Map -->
        <div class="modal fade" id="hotelMap" tabindex="-1" role="dialog" aria-labelledby="hotelMap" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <span class="icon icon-close"></span>
                    </button>
                    <div class="modal-body">
                        <div id="map" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
        </div>

        <%-- Cancellation popup--%>
        <div class="modal fade" id="CancelPolicyModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="btnModelClose">&times;</button>
                        <h4 class="modal-title" id="headRoomTypeName"></h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <b id="txtBold">Cancellation Policy :
                                <br />
                            </b>
                        </p>
                        <div class="hotel-description px-3" id="h2CnlPolicy">
                        </div>
                        <%--<p id="h2CnlPolicy"></p>--%>
                    </div>
                    <div class="modal-footer" id="btnCancel" style="display: none;">
                        <input type="button" class="btn btn-success" value="Select another Hotel" onclick="window.location.href = 'ApiHotelResults.aspx'" />
                        <input type="button" class="btn btn-success" value="Select Another Room" data-dismiss="modal" id="btnAnotherRoom" />
                    </div>
                </div>
            </div>
        </div>


    </div>

    <%--  template room summary--%>
    <div id="templateRoomSummary" style="display: none;">
        <div class="col-md-12 mt-2">
            <h5 id="h5RoomName"></h5>
        </div>
        <div class="col-md-12">
            <ul class="ui-hotel-addons-list">
                <li class="with-icon" id="mealPlanDesc"></li>
                <%--<li class="with-icon"  >Room only</li>
								<li class="with-icon">FREE Breakfast</li>
								<li class="with-icon">FREE Cancellation</li>--%>
            </ul>

        </div>
        <div class="col-md-12 mt-2">
            <div class="row hotel-bookingdtl-dateinfo">
                <div class="col-md-12">
                    <span class="float-left text-gray-light" id="roomGuests"><i class="icon icon-group"></i>Room Guests</span>
                    <span class="float-right text-gray-dark text-weight-400" id="roomGuestsCount">1 Room & 2 Adults</span>
                </div>
            </div>
        </div>
    </div>
    <%-- template for Rooms selection--%>
    <div id="templateRooms" style="display: none;">

        <div class="ui-room-details-table position-relative mt-4">
            <h5 class="room-number-label" id="h5RoomNumber"><span class="text"></span></h5>
            <div class="row no-gutters room-head d-none d-lg-flex">
                <div class="col-lg-6 room-col">Room type</div>
                <div class="col-lg-3 room-col">Options</div>
                <div class="col-lg-3 room-col" id="nightsAvgRate">Room Price</div>
            </div>
            <ul class="ui-listing-wrapper list-unstyled" id="list-group-roomtypes">
            </ul>
        </div>

    </div>
    <%-- template for Room types selection--%>
    <div class="row no-gutters room-body" id="templateRoomTypes" style="display: none;">
        <div class="col-12 col-lg-6 room-col">
            <%-- <img src="build/img/hotel-thumb-demo.jpg" alt="" class="hotel-image pr-3">--%>
            <div class="">
                <h4 id="h4RoomTypeName"></h4>
                <%-- <p id="pRoomType"></p>--%>
                <input type="button" class="btn btn-link p-0" id="roomTypeCancelPolicy" value="Cancellation Policy" />
                <%-- <a id="roomTypeCancelPolicy" class="small" href="#"></a>--%>
                <h5 id="BookingSource"></h5>
            </div>
        </div>
        <div class="col-6 col-lg-3 room-col">
            <ul class="ui-hotel-addons-list">
                <li id="roomTypeRefund"></li>
                <%-- <li class="with-icon">Special Deal</li>--%>
                <li class="with-icon" id="roomTypeMealPlan"></li>
            </ul>
        </div>
        <div class="col-6 col-lg-3 room-col">
            <div class="ui-list-price text-center position-relative">
                <span class="price d-inline-block d-sm-block" id="roomTypeAvgRate"></span>
                <input type="button" class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0 select" id="btnSelect" value="SELECT" />

                <%-- <a tabindex="0"   class="price-breakup btn btn-link tooltip-btn position-absolute" role="button" 
                                       data-toggle="popover" data-trigger="focus" data-html="true" data-placement="bottom"  
                                       title="Price Breakup" data-content="<strong>And </strong>here's some amazing content. It's very engaging. Right?">
                                    <span class="icon icon-info"></span>
                                    </a>--%>

                <button class="price-breakup btn btn-link tooltip-btn position-absolute" type="button"
                    data-toggle="collapse" data-target="#collapseRateBreakup" aria-expanded="false"
                    aria-controls="collapseRateBreakup" id="btnRateBreakup">
                    <span class="icon icon-info"></span>
                </button>

                <div class="collapse rate-breakup-div" id="collapseRateBreakup">
                    <table class="table table-bordered table-condensed b2b-corp-table mb-0" id="tblRateBreakUp">
                        <tbody>
                            <tr>
                                <th colspan="8" class="showMsgHeading">Rate Breakup 
												<a class="rate-breakup-close position-absolute" href="javascript:void(0);"><span class="icon icon-close"></span></a>
                                </th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><strong>Sun</strong></td>
                                <td><strong>Mon</strong></td>
                                <td><strong>Tue</strong></td>
                                <td><strong>Wed</strong></td>
                                <td><strong>Thu</strong></td>
                                <td><strong>Fri</strong></td>
                                <td><strong>Sat</strong></td>
                            </tr>
                            <%--  <tr>
                                    <td>Week 2</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                </tr>
                                <tr>
                                    <td colspan="8">Total : <b>4,467.07</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">Extra Guest Charge : <b>0.00</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">Total Price : <b>4,467.07</b>
                                    </td>
                                </tr>--%>
                        </tbody>
                    </table>
                </div>

            </div>



        </div>
    </div>

    <script src="build/js/jquery.royalslider.min.js"></script>
    <script src="build/js/jquery.magnific-popup.min.js"></script>
    <script src="<%=Request.Url.Scheme%>://maps.googleapis.com/maps/api/js?key=AIzaSyB_RGvSBnxHX_IPvIrlEg1TGy3YaIwvwMk&sensor=true" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/Jquery/popup_window.js"></script>
    <%--<script src="<%=Request.Url.Scheme%>://maps.googleapis.com/maps/api/js?key=AIzaSyCtd7RZ0Mbe3DTvLjqvbQnsOGk37VhPjI8&libraries=places&callback=initMap" async defer></script>--%>

    <script type="text/javascript">            
        var request;
        var userId;
        var agentId;
        var behalfLocation;
        var decimalValue;
        //var index;
        //var totFare;
        var hotelCode;
        var sessionId;
        var DataObject;
        var resultObj;
        var lstImages;
        var TypeCode;//RoomTypeCode is for getting cancellation policy when we click on Book now
        var lstRoomTypes, lstRoomTypeCodes;
        function pageLoad() {
            GetRoomDetails();
        }
        function GetRoomDetails() {
            request = JSON.parse(document.getElementById('<%=hdnObjRequest.ClientID %>').value);
            userId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
            agentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
            behalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
               <%--  index = JSON.parse(document.getElementById('<%=hdnIndex.ClientID %>').value);
                 totFare = JSON.parse(document.getElementById('<%=hdnTotFare.ClientID %>').value);--%>
            hotelCode = JSON.parse(document.getElementById('<%=hdnHotelCode.ClientID %>').value);
            sessionId = JSON.parse(document.getElementById('<%=hdnSessionId.ClientID %>').value);
            var data = { request: request, userId: userId, agentId: agentId, behalfLocation: behalfLocation, hotelCode: hotelCode, sessionId: sessionId };
            var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
            if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }
            $.ajax({
                url: apiUrl + '/api/HotelRooms/GetRoomsResult',
                type: 'POST',
                ContentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: data,
                success: function (data) {

                    DataObject = JSON.parse(data);
                    resultObj = DataObject.ResultObj;
                    document.getElementById('<%=hdnRoomResult.ClientID %>').value = resultObj;
                    decimalValue = DataObject.decimalValue;
                    if (resultObj.RoomDetails !=null && resultObj.RoomDetails.length > 0)               
                    {
                        LoadRoomDetails(resultObj);
                    }
                    else {
                        var msg = "No Room(s) is/are avaliable for selected Hotel, please Select another Hotel !";
                        $("#txtBold").text("");
                        $("#btnModelClose").hide();
                        $("#btnAnotherRoom").hide();
                        $("#btnCancel").show();
                        $("#headRoomTypeName").text("");
                        $("#h2CnlPolicy").text(msg);
                        $("#CancelPolicyModal").modal("show");
                    }
                },
                complete: function () {
                    //Done with Loading
                    NProgress.done();
                },
                error: function (xhr, textStatus, errorThrown) {

                    // alert('Error in Operation');
                }
            });
        }
        // main Rooms load function
        function LoadRoomDetails(hotelResult) {

            $('#h3HotelName').text(hotelResult.HotelName);
            $('#pAddress').text(hotelResult.HotelAddress);
            initialize(hotelResult.HotelMap);
            LoadImages(hotelResult);
            LoadRoomSummary(hotelResult);
            LoadRoomTypes(hotelResult);
            LoadHotelOverview(hotelResult);

        }
        //for load image urls
        function LoadImages(hotelResult) {

            lstImages = new Array();
            var roomDetails = hotelResult.RoomDetails;
            var noofRooms = hotelResult.RoomGuest;
            if (roomDetails != null && roomDetails.length > 0 && noofRooms != null && noofRooms.length > 0) {
                for (var i = 0; i < roomDetails.length; i++) {
                    var lstImgs = roomDetails[i].Images;
                    if (lstImgs != null && lstImgs.length > 0) {
                        for (var j = 0; j < lstImgs.length; j++) {
                            if (imgUrlContains(lstImages, lstImgs[j]))
                                lstImages.push(lstImgs[j]);
                        }
                    }
                }
            }
            if (lstImages == null || lstImages.length < 1) {
                if (hotelResult.HotelImages != null && hotelResult.HotelImages.length > 0) {
                    for (var k = 0; k < hotelResult.HotelImages.length; k++) {
                        lstImages.push(hotelResult.HotelImages[k]);
                    }
                }
            }

            var images = "";
            for (var l = 0; l < lstImages.length; l++) {
                images += '<a class="rsImg" data-rsbigimg="build" href="' + lstImages[l] + '" data-rsw="700" data-rsh="300"><img width="96" height="72" class="rsTmb" src="' + lstImages[l] + '" /></a>';
            }
            $('#imgGallery').html(images);
            //CAROUSEL HOTEL GALLERY	 
            $('.royalSlider').each(function () {
                var sliderElm = $(this);
                var sliderOptions = {
                    controlNavigation: 'thumbnails',
                    thumbs: {
                        orientation: 'vertical',
                        paddingBottom: 4,
                        appendSpan: true
                    },
                    transitionType: 'fade',
                    autoScaleSlider: true,
                    autoScaleSliderWidth: 960,
                    autoScaleSliderHeight: 530,
                    loop: true,
                    arrowsNav: true,
                    keyboardNavEnabled: true,
                    imageScaleMode: 'fill'

                };
                sliderElm.royalSlider(sliderOptions);
            });
        }
        //Find to duplecate Room level image urls
        function imgUrlContains(arr, element) {
            for (var c = 0; c < arr.length; c++) {
                if (arr[c] == element) {
                    return false;
                }
            }
            return true;
        }
        //Loading for map
        function initialize(Map) {

            var data = Map.split('||');
            myCenter = new google.maps.LatLng(data[0], data[1]);
            var mapProp = {
                center: myCenter,
                zoom: 18,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                overviewMapControl: true,
                rotateControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"), mapProp);


            marker = new google.maps.Marker({
                position: myCenter,
                animation: google.maps.Animation.BOUNCE
            });

            marker.setMap(map);
        }
        //for load Room(s) Detail summary
        function LoadRoomSummary(hotelResult) {

            $('#list-group-roomsummary').children().remove();
            var templateRoomSummary = $('#templateRoomSummary').html();
            $('#startDate').text(new Date(hotelResult.StartDate).toDateString());
            $('#endDate').text(new Date(hotelResult.EndDate).toDateString());

            $('#beforeTotalPrice').append('<del id="beforeTotalPriceDel"><em id="beforeTotalPriceCurrency" class="currency">' + hotelResult.Currency + ' </em>' + ' ' + Math.ceil(hotelResult.TotalPrice).toFixed(decimalValue)) + ' </del>';
            $('#TotalPrice').append('<em id="totalPriceCurrency" class="currency">' + hotelResult.Currency + ' </em>' + ' ' + Math.ceil(hotelResult.TotalPrice - hotelResult.Price.Discount).toFixed(decimalValue));
             if ( hotelResult.TotalPrice.toFixed(decimalValue) == (hotelResult.TotalPrice - hotelResult.Price.Discount).toFixed(decimalValue)) {
                $('#beforeTotalPrice').children().hide();
             }
             else {
                $('#beforeTotalPrice').children().show();
            }

            var noofAdults = 0;
            var noofChilds = 0;
            var noofRooms = hotelResult.RoomGuest;
            if (noofRooms != null && noofRooms.length > 0) {

                for (var m = 0; m < noofRooms.length; m++) {
                    $('#list-group-roomsummary').append('<li id="List' + m + '" class="item">' + templateRoomSummary + '</li>');
                    $('#h5RoomName').attr('id', 'h5RoomName-' + m);
                    $('#h5RoomName-' + m).text(hotelResult.RoomDetails[m].RoomTypeName);
                    $('#mealPlanDesc').attr('id', 'mealPlanDesc-' + m);
                    $('#mealPlanDesc-' + m).text(hotelResult.RoomDetails[m].mealPlanDesc);
                    $('#roomGuestsCount').attr('id', 'roomGuestsCount-' + m);
                    $('#roomGuestsCount-' + m).text(hotelResult.RoomGuest[m].noOfAdults + " Adult(s) & " + hotelResult.RoomGuest[m].noOfChild + " Child(s)");
                    noofAdults += hotelResult.RoomGuest[m].noOfAdults;
                    noofChilds += hotelResult.RoomGuest[m].noOfChild;
                }
            }
            $('#spanTotalPrice').text("");
            $('#spanTotalPrice').text("Total price for " + noofAdults + " Adult(s), " + noofChilds + " Child(s)");
        }
        // for load Room types (Room Details)
        function LoadRoomTypes(hotelResult) {

            //var  noofNights= parseInt((new Date(hotelResult.EndDate) - new Date(hotelResult.StartDate)) / (1000 * 60 * 60 * 24));
            lstRoomTypes = [];
            lstRoomTypeCodes = [];
            //var roomDetails = hotelResult.RoomDetails;
            var noofRooms = hotelResult.RoomGuest;
            if (hotelResult.RoomDetails != null && hotelResult.RoomDetails.length > 0) {
                TypeCode = hotelResult.RoomDetails[0].RoomTypeCode;
                for (var n = 0; n < hotelResult.RoomDetails.length; n++) {
                    if (roomTypeContains(lstRoomTypeCodes, hotelResult.RoomDetails[n].RoomTypeCode.split("||")[1])) {
                        lstRoomTypeCodes.push(hotelResult.RoomDetails[n].RoomTypeCode.split("||")[1]);
                        lstRoomTypes.push(hotelResult.RoomDetails[n]);
                    }
                }
            }

            if (lstRoomTypes != null && lstRoomTypes.length > 0) {
                $('#list-group-roomtype').children().remove();
                var templateRooms = $('#templateRooms').html();

                for (var p = 0; p < noofRooms.length; p++) {
                    $('#list-group-rooms').append('<li id="List' + p + '" class="">' + templateRooms + '</li>');
                    $('#h5RoomNumber').attr('id', 'h5RoomNumber-' + p);
                    $('#h5RoomNumber-' + p).text("Room - " + (p + 1));

                    //$('#nightsAvgRate').attr('id', 'nightsAvgRate-' + p);
                    //$('#nightsAvgRate-' + p).text("PRICE FOR " + noofNights +" NIGHT(S)");

                    var templateRoomTypes = $('#templateRoomTypes').html();
                    $('#list-group-rooms').find('#list-group-roomtypes').attr('id', 'list-group-roomtypes-' + p);
                    for (var q = 0; q < lstRoomTypes.length; q++) {

                        $('#list-group-rooms').find('#list-group-roomtypes-' + p).append('<li id="ListTypes' + p + q + '" class="row no-gutters room-body">' + templateRoomTypes + '</li>');

                        //Room Type
                        $('#h4RoomTypeName').attr('id', 'h4RoomTypeName-' + p + q);
                        $('#h4RoomTypeName-' + p + q).text(lstRoomTypes[q].RoomTypeName);
                        //$('#pRoomType').attr('id', 'pRoomType-' + p + q);
                        //$('#pRoomType-' + p + q).text(lstRoomTypes[q].mealPlanDesc);
                        //cancel policy link -- To Do                             
                        $('#roomTypeCancelPolicy').attr('id', 'roomTypeCancelPolicy-' + p + q);
                        $('#roomTypeCancelPolicy-' + p + q).attr("onClick", "GetRoomCancelPolicy('" + lstRoomTypes[q].RoomTypeCode + "','" + lstRoomTypes[q].RoomTypeName + "','Cancel')");
                        $('#BookingSource').attr('id', 'BookingSource-' + p + q);  // for Booking source
                        if (document.getElementById('<%=hdnIsAgent.ClientID %>').value=="1") {
                            $('#BookingSource-' + p + q).text("Source : " + lstRoomTypes[q].RoomTypeCode.split('||')[3]);
                        }
                        else {
                             $('#BookingSource-' + p + q).text("");
                        }
                        //Options

                        $('#roomTypeRefund').attr('id', 'roomTypeRefund-' + p + q);
                        if (lstRoomTypes[q].RoomTypeName.split('-')[1] == "unknown") {
                             $('#roomTypeRefund-' + p + q).text('Nonrefundable');
                        }
                        else {
                             $('#roomTypeRefund-' + p + q).text(lstRoomTypes[q].RoomTypeName.split('-')[1]);
                        }
                       
                        $('#roomTypeMealPlan').attr('id', 'roomTypeMealPlan-' + p + q);
                        $('#roomTypeMealPlan-' + p + q).text(lstRoomTypes[q].mealPlanDesc);
                        // Special Deal  -- To Do

                        //Avg per night
                        $('#roomTypeAvgRate').attr('id', 'roomTypeAvgRate-' + p + q);
                        $('#roomTypeAvgRate-' + p + q).append('<em class="currency">' + hotelResult.Currency + ' </em>' + ' ' + parseFloat(lstRoomTypes[q].TotalPrice - lstRoomTypes[q].Discount).toFixed(decimalValue));
                        //var selectedRT = JSON.parse(lstRoomTypes[q]);

                        $('#btnSelect').attr('id', 'btnSelect-' + p + q);
                        $('#btnSelect-' + p + q).attr("onClick", "SelectRoomOption('" + hotelResult.Currency + "','" + lstRoomTypes[q].RoomTypeCode + "','" + q + "','" + noofRooms.length + "',this.id)");
                        if ("rt-" + p + q == "rt-00" || "rt-" + p + q == "rt-10" || "rt-" + p + q == "rt-20" || "rt-" + p + q == "rt-30") {
                            $('#btnSelect-' + p + q).prop('disabled', true);
                        }

                        // for  RateBreakup
                        $('#collapseRateBreakup').attr('id', 'collapseRateBreakup-' + p + q);
                        $('#btnRateBreakup').attr('data-target', '#collapseRateBreakup-' + p + q);
                        $('#btnRateBreakup').attr('id', 'btnRateBreakup-' + p + q);

                        $('#tblRateBreakUp').attr('id', 'tblRateBreakUp-' + p + q);
                        rows = tblContent(parseFloat(lstRoomTypes[q].TotalPrice - lstRoomTypes[q].Discount).toFixed(decimalValue), lstRoomTypes[q].RoomTypeCode);
                        $('#tblRateBreakUp-' + p + q).append(rows);
                        $(".body_container").removeClass("loading-progress");
                        $(".loadingHide").show();
                    }
                }

            }
        }


        //rate breake up table content

        // Create Dynamic Table rows of Rate Breakup
        function tblContent(totalPrice, roomtypecode) {

            var roomtypes = lstRoomTypes;
            var room = roomtypes.filter(function (item) {
                return (item.RoomTypeCode == roomtypecode);
            });
            var rows;
            var tr;
            var weekdays = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            var d1 = new Date(request.StartDate);
            var d2 = new Date(request.EndDate);
            d2.setDate(d2.getDate() - 1);
            // var Difftime = d2.getTime() - d1.getTime();
            var result = 0;
            // Get first full week
            var firstDayOfFirstFullWeek = d1;
            if (weekdays[d1.getDay()] != "Sunday") {
                result += 1;
                firstDayOfFirstFullWeek = d1.setDate(d1.getDate() + (7 - (d1.getDay())));
            }
            // Get last full week
            var lastDayOfLastFullWeek = d2;
            if (weekdays[d2.getDay()] != "Saturday") {
                result += 1;
                lastDayOfLastFullWeek = d2.setDate(d2.getDate() + (-(d2.getDay()) - 1));
            }
            var Difftime = lastDayOfLastFullWeek - firstDayOfFirstFullWeek;
            var diffResult = (Difftime / (1000 * 60 * 60 * 24));

            // Add number of full weeks
            result += parseInt((diffResult + 1) / 7);
            d1 = new Date(request.StartDate);
            d2 = new Date(request.EndDate);
            //      var days = parseInt((new Date(d2) - new Date(d1)) / (1000 * 60 * 60 * 24));

            var totalRoomSellRate = 0;
            var Count = result;
            for (var i = 0, j = 0; i < result; i++) {
                var week = i + 1;
                tr += "<tr>";
                tr += "<td >Week " + week + "</td>";
                var l = 0;
                if (weekdays[d1.getDay()] != "Sunday") {
                    var predeser = d1.getDay();
                    for (; l < predeser; l++) {
                        tr += "<td>&nbsp;</td>";
                    }
                }
                var rmInfo = room[0].Rates;
                if (i == Count--) {
                    d2.setDate(d2.getDate() - 1);
                }
                var k = 0;
                for (; l < 7 && d1 <= d2; l++) {



                    if (rmInfo.length > k) {
                        var rate = rmInfo[k].SellingFare;

                        tr += "<td>" + (parseFloat(totalPrice / rmInfo.length).toFixed(decimalValue)) + "</td>";
                        totalRoomSellRate += rate;
                    }
                    else {
                        tr += "<td>&nbsp;</td>";
                    }
                    d1.setDate(d1.getDate() + 1);
                    k++;
                }
                tr += "</tr>";
            }

            rows = tr + "<tr><td colspan='8'>" + "Total : " + "<b>" + totalPrice + "</b> </td> </tr>"
                + "<tr> <td colspan='8'>" + "Extra Guest Charge : " + "<b>" + "0.00" + "</b></td></tr>"
                + "<tr><td colspan='8'>" + "Total Price :" + "<b>" + totalPrice + "</b></td></tr>";

            return rows;

        }

        function roomTypeContains(arr, element) {
            for (var c = 0; c < arr.length; c++) {
                if (arr[c] === element) {
                    return false;
                }
            }
            return true;
        }
        //Room type option selection
        function SelectRoomOption(currency, RoomTypeCode, id, noofRooms, cid) {

            TypeCode = RoomTypeCode;
            var total = 0;
            var discount = 0;
            $('.select').attr('disabled', false);
            for (var i = 0; i < lstRoomTypes.length; i++) {
                var d = cid.replace('btnSelect-', '').replace(id, '');
                $('#h5RoomName-' + i).text($('#h4RoomTypeName-' + i + id).text());
                $('#mealPlanDesc-' + i).text($('#roomTypeMealPlan-' + i + id).text());

            }

            // For disable selected room type in each room and To show total amount in summary section for selcted room type
            for (var k = 0; k < noofRooms; k++) {
                $('#btnSelect-' + k + id).attr('disabled', true);
                total += parseFloat(lstRoomTypes[id].TotalPrice);
                discount += parseFloat(lstRoomTypes[id].Discount)
            }
            $('#beforeTotalPrice').children().remove();
            $('#TotalPrice').children().remove();
            $('#beforeTotalPrice').text("");
            $('#TotalPrice').text("");
            $('#beforeTotalPrice').append('<del><em class="currency">' + currency + ' </em>' + ' ' + Math.ceil(total).toFixed(decimalValue)) + ' </del>';
            $('#TotalPrice').append('<em class="currency">' + currency + ' </em>' + ' ' + Math.ceil(total - discount).toFixed(decimalValue));
             if ( total.toFixed(decimalValue) == (total - discount).toFixed(decimalValue)) {
                $('#beforeTotalPrice').children().hide();
            }
            else {
                $('#beforeTotalPrice').children().show();
            }
            //for Hotel Overview --  Rooms (Room level amenities) 
            var roomAmenities = lstRoomTypes[id].Amenities;;
            var amenities = "";
            document.getElementById("h6Rooms").style.display = "none";
            if (roomAmenities != null) {
                document.getElementById("h6Rooms").style.display = "block";

                for (var i = 0; i < roomAmenities.length; i++) {
                    amenities += roomAmenities[i] + ", ";
                }
            }
            $('#roomLevelAmenities').children().remove();
            $('#roomLevelAmenities').text("");
            $('#roomLevelAmenities').append('<p>' + amenities.replace(/,\s*$/, "") + '</p>');
            $("#btnBookNow").focus();
            topFunction();
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
        //Book Room
        function BookRoom() {
            $("#ctl00_upProgress").show();
            GetRoomCancelPolicy(TypeCode, "", "Book");

        }
        //update HotelDetails to Session

        function updateHotelDetails() {
            var data = JSON.stringify(resultObj).replace('&', '').replace('#', '').replace('<', '').replace('>', '').replace('%', '').replace(/'/g, '');
            $.ajax({
                type: "POST",
                url: "ApiRoomDetails.aspx/HotelResult",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: "{'data':'" + data + "' }",
                success: function (response) {
                    window.location.href = "ApiGuestDetails.aspx?RoomType=" + TypeCode.split("||")[1] + "&sessionId=" + sessionId;
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#ctl00_upProgress").hide();
                    // alert(xhr.responseText + " --- " + errorThrown);
                }
            });
        }
        //Get Room level Cancellation policy
        function GetRoomCancelPolicy(roomTypeCode, roomTypeName, type) {
            if (type == "Cancel") {
                $("#ctl00_upProgress").show();
            }
            var data = { roomTypeCode: roomTypeCode, resultObj: resultObj, sessionId: sessionId };

            var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
            
            if (apiUrl == null || apiUrl == '')
                apiUrl = DynamicAPIURL();

            $.ajax({
                url: apiUrl + '/api/HotelRooms/GetRoomsCancelPolicy',
                type: 'POST',
                ContentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: data,
                success: function (data) {
                    var rslt = JSON.parse(data);
                    resultObj = rslt.ResultObj;
                    var cnlPolicy = rslt.CancelPolicy.replace("|", ". <br>");
                    if (type == "Cancel") {
                        $("#btnCancel").hide();
                        $("#headRoomTypeName").text(roomTypeName);
                        $('#h2CnlPolicy').children().remove();
                        $('#h2CnlPolicy').text("");
                        $('#h2CnlPolicy').append('<p>' + cnlPolicy + '</p>');
                        $("#CancelPolicyModal").modal();
                        $("#ctl00_upProgress").hide();
                    }
                    else {
                        if (cnlPolicy == "") {
                            $("#btnCancel").show();
                            $("#headRoomTypeName").text("Cancellation Policy");
                            $("#h2CnlPolicy").text("No Cancellation Policy is avaliable for Selected Rooms So please Select another Room or Hotel");
                            $("#CancelPolicyModal").modal("show");
                               $("#ctl00_upProgress").hide();
                        }
                        else {
                            updateHotelDetails();

                        }
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    // alert('Error in cancellation policy Operation');
                    $("#ctl00_upProgress").hide();
                }
            });
        }
        //For load Hotel Overview
        function LoadHotelOverview(hotelResult) {
            // For Amenities        

            if (hotelResult.HotelFacilities != null && hotelResult.HotelFacilities.length > 0) {
                for (var i = 0; i < hotelResult.HotelFacilities.length; i++) {

                    var amenity = hotelResult.HotelFacilities[i].trim().toLowerCase();
                    amenity = amenity.replace(" ", "-");
                    $('#Amenities').append(' <li><span class="icon icon-' + amenity + '"></span><span class="text">' + hotelResult.HotelFacilities[i] + '</span></li>');

                }
            }
            // For Description 
            $('#HotelDescription').children().remove();
            $('#HotelDescription').text("");
            $('#HotelDescription').append('<p>' + hotelResult.HotelDescription + '</p>');
            // for Location
            $('#pLocation').text(hotelResult.HotelName + " , " + hotelResult.HotelAddress);

            //for Rooms (Room level amenities) 
            var roomAmenities = hotelResult.RoomDetails[0].Amenities;
            var amenities = "";
            document.getElementById("h6Rooms").style.display = "none";
            if (roomAmenities != null) {
                document.getElementById("h6Rooms").style.display = "block";
                for (var i = 0; i < roomAmenities.length; i++) {
                    amenities += roomAmenities[i] + ", ";
                }
                $('#roomLevelAmenities').children().remove();
                $('#roomLevelAmenities').text("");
                $('#roomLevelAmenities').append('<p>' + amenities.replace(/,\s*$/, "") + '</p>');
            }


        }
    </script>
    <script>
        //Loading Starts
        $(function () {
            NProgress.start();
        })
        // for royalSlider 
        $(window).load(function () {
            $('.rsSlide').each(function () {
                var hrefpath = $(this).find('img').attr('src');
                $(this).wrapAll('<a href="' + hrefpath + '"/>');
            })
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function (element) {
                        return element.find('img');
                    }
                }

            });
        })

        //For RateBreakup
        $('.ui-listing-wrapper').on('click', '.rate-breakup-close', function () {
            $('.rate-breakup-div').removeClass('in');
        })
        $(document).mouseup(function (e) {
            var MenuContainer = $('.rate-breakup-div');
            if (!MenuContainer.is(e.target) && MenuContainer.has(e.target).length === 0) {
                MenuContainer.removeClass('in');
            }
        });
        function DynamicAPIURL() {
                //debugger;
                var url;
                var Secure ='<%=Request.IsSecureConnection%>';
            if (Secure=='True') {
                url = '<%=Request.Url.Scheme%>'+"://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            else {
                  url = '<%=Request.Url.Scheme%>'+"://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            return url;
        }
    </script>
    <script src="build/js/jquery-ui.js"></script>
    <script src="build/js/nprogress.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
