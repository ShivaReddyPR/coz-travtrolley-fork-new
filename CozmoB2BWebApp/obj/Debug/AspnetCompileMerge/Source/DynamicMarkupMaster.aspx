﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="DynamicMarkupMaster.aspx.cs" Inherits="CozmoB2BWebApp.DynamicMarkupMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div class="col-md-12">
        <h3>Dynamic Markup Master</h3>
    </div>
    <div class="body_container" style="height:200px">
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                From Source :
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlFromSource" runat="server"  Width="150" OnSelectedIndexChanged="ddlFromSource_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="-1">Select From Source</asp:ListItem>
                </asp:DropDownList>
            </div>
             <div class="col-md-2">
               Compare Source :
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlCompareSource" AppendDataBoundItems="true" runat="server" Width="150">
                    <asp:ListItem Value="-1">Select Compare Source</asp:ListItem>
                </asp:DropDownList>
            </div>
              <div class="col-md-2">
               Product :
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlProduct" runat="server" Width="150" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select Product Type</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearfix"></div>
            </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                Agent :
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlAgent" runat="server" Width="150" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select Agent</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                Airline Code :
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="ChkAirline" runat="server" />
            </div>
            <div class="col-md-2">
                Route :
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="ChkRoute" runat="server" />
            </div>
            <div class="clearfix"></div>
            </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                Booking Class :
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="ChkBooking" runat="server" />
            </div>
             <div class="col-md-2">
                 Flight No :
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="ChkFlight" runat="server" />
            </div>
             <div class="col-md-2">
                 Transaction Type:
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlTransactionType" runat="server" Width="150" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select Transaction Type</asp:ListItem>
                    <asp:ListItem Value="B2B">B2B</asp:ListItem>
                    <asp:ListItem Value="B2C">B2C</asp:ListItem>
                    <asp:ListItem Value="MOBILE">MOBILE</asp:ListItem>
                </asp:DropDownList>
            </div>
           <%-- <div class="col-md-2">
               <asp:TextBox ID="TxtAirlineCode" runat="server"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
            </div>--%>
           
            <%-- <div class="col-md-6 preferred-airline">
                           <div class="form-control-holder">
                                   <div class="icon-holder">                                        
                                        <span class="icon-aircraft-take-off"></span>                     
                                   </div>  
                                     <asp:TextBox ID="txtAirline" CssClass="form-control" onchange="RecheckAirline(0)" 
                                                                runat="server" onclick="IntDom('statescontainer4' , 'ctl00_cphTransaction_txtAirline')" placeholder="Type Airline" Width="100px" ></asp:TextBox>
                               <div id="statescontainer4"  style="width:20px; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;    top: 38px;" ></div>
                                                                        
                                                            <asp:HiddenField ID="airlineCode" runat="server" />
                                                            <asp:HiddenField ID="airlineName" runat="server" />
                                       
                                
                              </div>
                             
                               <div id="divAirline0"  class="row no-gutters mb-2"> 
                                             
                                        <div class="col-md"> 
                                        <div id="divAirlines0" style="display: none">
                                                    <input type="text" id="txtAirline0"  name="txtAirline0"  Class="form-control" onclick="IntDom('statescontainer5' , 'txtAirline0')"
                                                        placeholder="Type Airline"  onchange="RecheckAirline(1)"   />
                                                    <div id="statescontainer5" style="width: 20px; line-height: 20px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                              <a class="remove-button" id="removeRowLink0" 
                                                    style="cursor: pointer; display: none" onclick="RemovePrefAirline(0)">
                                                   <span class="icon-close"></span>
                                                </a>
                                            </div>
                                    
                                </div>
                               <div  class="row no-gutters  mb-2" id="divAirline1">
                                            <div class="col-md">
                                                <div id="divAirlines1" style="display: none">
                                                    <input type="text" id="txtAirline1" name="txtAirline1" class="form-control"
                                                        onclick="IntDom('statescontainer6' , 'txtAirline1')" placeholder="Type Airline" style="width=100px" onchange="RecheckAirline(2)"/>
                                                    <div id="statescontainer6" style="width: 20px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink1" 
                                                    style="cursor: pointer; display: none" onclick="RemovePrefAirline(1)">
                                                     <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline2">
                                            <div class="col-md">
                                                <div id="divAirlines2" style="display: none">
                                                    <input type="text" id="txtAirline2" name="txtAirline2" class="form-control"
                                                        onclick="IntDom('statescontainer7' , 'txtAirline2')" placeholder="Type Airline" onchange="RecheckAirline(3)"/>
                                                    <div id="statescontainer7" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink2" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(2)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline3">
                                            <div class="col-md">
                                                <div id="divAirlines3" style="display: none">
                                                    <input type="text" id="txtAirline3" name="txtAirline3" class="form-control"
                                                        onclick="IntDom('statescontainer8' , 'txtAirline3')" placeholder="Type Airline" onchange="RecheckAirline(4)"/>
                                                    <div id="statescontainer8" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink3" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(3)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline4">
                                            <div class="col-md">
                                                <div id="divAirlines4" style="display: none">
                                                    <input type="text" id="txtAirline4" name="txtAirline4" class="form-control"
                                                        onclick="IntDom('statescontainer9' , 'txtAirline4')" placeholder="Type Airline" onchange="RecheckAirline(5)"/>
                                                    <div id="statescontainer9" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink4" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(4)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline5">
                                            <div class="col-md">
                                                <div id="divAirlines5" style="display: none">
                                                    <input type="text" id="txtAirline5" name="txtAirline5" class="form-control"
                                                        onclick="IntDom('statescontainer10' , 'txtAirline5')" placeholder="Type Airline" onchange="RecheckAirline(6)"/>
                                                    <div id="statescontainer10" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink5" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(5)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline6">
                                            <div class="col-md">
                                                <div id="divAirlines6" style="display: none">
                                                    <input type="text" id="txtAirline6" name="txtAirline6" class="form-control"
                                                        onclick="IntDom('statescontainer11' , 'txtAirline6')" placeholder="Type Airline" onchange="RecheckAirline(7)"/>
                                                    <div id="statescontainer11" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink6" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(6)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline7">
                                            <div class="col-md">
                                                <div id="divAirlines7" style="display: none">
                                                    <input type="text" id="txtAirline7" name="txtAirline7" class="form-control"
                                                        onclick="IntDom('statescontainer12' , 'txtAirline7')" placeholder="Type Airline" onchange="RecheckAirline(8)"/>
                                                    <div id="statescontainer12" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink7" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(7)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                               <div  class="row no-gutters  mb-2" id="divAirline8">
                                            <div class="col-md">
                                                <div id="divAirlines8" style="display: none">
                                                    <input type="text" id="txtAirline8" name="txtAirline8" class="form-control"
                                                        onclick="IntDom('statescontainer13' , 'txtAirline8')" placeholder="Type Airline" onchange="RecheckAirline(9)"/>
                                                    <div id="statescontainer13" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink8" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(8)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                                <div  class="row no-gutters  mb-2" id="divAirline9">
                                            <div class="col-md">
                                                <div id="divAirlines9" style="display: none">
                                                    <input type="text" id="txtAirline9" name="txtAirline9" class="form-control"
                                                        onclick="IntDom('statescontainer14' , 'txtAirline9')" placeholder="Type Airline" onchange="RecheckAirline(10)"/>
                                                    <div id="statescontainer14" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                        display: none; z-index: 103;">
                                                    </div>
                                                </div>
                                                <a class="remove-button" id="removeRowLink9" 
                                                    style="cursor: pointer; display: none"  onclick="RemovePrefAirline(9)">
                                                   <span class="icon-close"></span></a>
                                            </div>
                                        </div>
                                        <a class="fcol_fff cursor_point" id="addRowLink" style="color:Blue";  onclick="AddPrefAirline()">+ Add New Airline</a>

                                     
                        </div>--%>
               
       <%-- <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                Routes :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="TxtRoutes" runat="server"></asp:TextBox>
            </div>
             <div class="col-md-2">
                Booking Classes :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="TxtBooking" runat="server"></asp:TextBox>
            </div>
            <div class="clearfix"></div>--%>
            </div>

        <div class="col-md-12">
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn but_b" OnClientClick="return Save();" />
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b" OnClick="btnSearch_Click" />
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b" OnClick="btnClear_Click"/>
           <asp:LinkButton ID="lnkDynamicMarkupThreshold" runat="server" Text="View/Edit Dynamic Markup Threshold Page" Visible="false" style="color:Blue; width:200px;" OnClientClick="ChangeThreshold();" ></asp:LinkButton>
        </div>
        <asp:HiddenField ID="hdfid" Value="0" runat="server" />
        <asp:Label ID="lblSuccessMsg" runat="server" Visible="false"></asp:Label>
        </div>

   
      <script type="text/javascript">
        function Save() {
            if (getElement('ddlFromSource').value == -1) addMessage('FromSource cannot be blank!', '');
            if (getElement('ddlCompareSource').value == -1) addMessage('CompareSource cannot be blank!', '');
            if (getElement('TxtAirlineCode').value =='') addMessage('AirlineCode cannot be blank!', '');
            
            

        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
    function ChangeThreshold() {
        var DynId = document.getElementById('<%=hdfid.ClientID %>').value;
        window.open("DynamicMarkupThresholdMaster.aspx?&dynId=" + DynId + "&fromDynId=true", 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
          }
           //Adding  Airline Added by brahmam 14.10.2016
        function AddPrefAirline() {

            //show upto 3 preferred airline div's
            if (document.getElementById('divAirlines0').style.display == "none") {
                document.getElementById('divAirline0').style.display = 'block';
                document.getElementById('divAirlines0').style.display = 'block';
                document.getElementById('removeRowLink0').style.display = 'block';
                document.getElementById('txtAirline0').value = '';
            } else if (document.getElementById('divAirlines1').style.display == "none") {
                document.getElementById('divAirline1').style.display = 'block';
                document.getElementById('divAirlines1').style.display = 'block';
                document.getElementById('removeRowLink1').style.display = 'block';
                document.getElementById('txtAirline1').value = '';
            }
            else if (document.getElementById('divAirlines2').style.display == "none") {
                document.getElementById('divAirline2').style.display = 'block';
                document.getElementById('divAirlines2').style.display = 'block';
                document.getElementById('removeRowLink2').style.display = 'block';
                document.getElementById('txtAirline2').value = '';
            }
            else if (document.getElementById('divAirlines3').style.display == "none") {
                document.getElementById('divAirline3').style.display = 'block';
                document.getElementById('divAirlines3').style.display = 'block';
                document.getElementById('removeRowLink3').style.display = 'block';
                document.getElementById('txtAirline3').value = '';
            }
            else if (document.getElementById('divAirlines4').style.display == "none") {
                document.getElementById('divAirline4').style.display = 'block';
                document.getElementById('divAirlines4').style.display = 'block';
                document.getElementById('removeRowLink4').style.display = 'block';
                document.getElementById('txtAirline4').value = '';
            }
            else if (document.getElementById('divAirlines5').style.display == "none") {
                document.getElementById('divAirline5').style.display = 'block';
                document.getElementById('divAirlines5').style.display = 'block';
                document.getElementById('removeRowLink5').style.display = 'block';
                document.getElementById('txtAirline5').value = '';
            }

             else if (document.getElementById('divAirlines6').style.display == "none") {
                document.getElementById('divAirline6').style.display = 'block';
                document.getElementById('divAirlines6').style.display = 'block';
                document.getElementById('removeRowLink6').style.display = 'block';
                document.getElementById('txtAirline6').value = '';
            }
            else if (document.getElementById('divAirlines7').style.display == "none") {
                document.getElementById('divAirline7').style.display = 'block';
                document.getElementById('divAirlines7').style.display = 'block';
                document.getElementById('removeRowLink7').style.display = 'block';
                document.getElementById('txtAirline7').value = '';
            }
            else if (document.getElementById('divAirlines8').style.display == "none") {
                document.getElementById('divAirline8').style.display = 'block';
                document.getElementById('divAirlines8').style.display = 'block';
                document.getElementById('removeRowLink8').style.display = 'block';
                document.getElementById('txtAirline8').value = '';
            }
            else if (document.getElementById('divAirlines9').style.display == "none") {
                document.getElementById('divAirline9').style.display = 'block';
                document.getElementById('divAirlines9').style.display = 'block';
                document.getElementById('removeRowLink9').style.display = 'block';
                document.getElementById('txtAirline9').value = '';
            }

            if (document.getElementById('divAirlines0').style.display == "block" && document.getElementById('divAirlines1').style.display == "block" && document.getElementById('divAirlines2').style.display == "block" && document.getElementById("divAirlines3").style.display == "block"
                && document.getElementById("divAirlines4").style.display == "block" && document.getElementById("divAirlines5").style.display == "block" && document.getElementById("divAirlines6").style.display == "block" && document.getElementById("divAirlines7").style.display == "block"
             && document.getElementById("divAirlines8").style.display == "block"  && document.getElementById("divAirlines9").style.display == "block")
            {
                document.getElementById('addRowLink').style.display = 'none';
            }
          }
          //removing  Airline Added by brahmam 14.10.2016
        function RemovePrefAirline(index) {
            //Just clear the previously entered values.
            if (document.getElementById('txtAirline' + index) != null) {

                document.getElementById('txtAirline' + index).value = "";
            }
            document.getElementById('divAirline' + index).style.display = 'none';
            document.getElementById('divAirlines' + index).style.display = 'none';
            document.getElementById('removeRowLink' + index).style.display = 'none';
            document.getElementById('addRowLink').style.display = 'block';
            
            airlineCodes.splice((index + 1), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
           
          }
        //when  airline cleared from textbox
        function RecheckAirline(index) {
          
            airlineCodes.splice((index), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
           
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="DynID"
      emptydatalist="No DynamicMarkup List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="5"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">


     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField HeaderText="From Source" >
       
        <ItemTemplate >
    <asp:Label ID="ITlblFromSource" runat="server" Text='<%# Eval("FromSource") %>' CssClass="label grdof" ToolTip='<%# Eval("FromSource") %>' ></asp:Label>
   
    </ItemTemplate>  
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Compare Source">
        <ItemTemplate >
    <asp:Label ID="ITlblCompareSource" runat="server" Text='<%# Eval("CompareSource") %>' CssClass="label grdof" ToolTip='<%# Eval("CompareSource") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Airline Code Check">
        <ItemTemplate >
    <asp:Label ID="ITlblAirlineCodeCheck" runat="server" Text='<%# Eval("AirlineCodeCheck") %>' CssClass="label grdof" ToolTip='<%# Eval("AirlineCodeCheck") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Route Check">
        <ItemTemplate >
    <asp:Label ID="ITlblRouteCheck" runat="server" Text='<%# Eval("RouteCheck") %>' CssClass="label grdof" ToolTip='<%# Eval("RouteCheck") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Booking Class Check">
        <ItemTemplate >
    <asp:Label ID="ITlblBookingClassCheck" runat="server" Text='<%# Eval("BookingClassCheck") %>' CssClass="label grdof" ToolTip='<%# Eval("BookingClassCheck") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Flight No Check">
        <ItemTemplate >
    <asp:Label ID="ITlblFlightNoCheck" runat="server" Text='<%# Eval("FlightNoCheck") %>' CssClass="label grdof" ToolTip='<%# Eval("FlightNoCheck") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
         <%-- <asp:TemplateField HeaderText="Airline Codes">
        <ItemTemplate >
    <asp:Label ID="ITlblAirlineCodes" runat="server" Text='<%# Eval("AirlineCodes") %>' CssClass="label grdof" ToolTip='<%# Eval("AirlineCodes") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Routes">
        <ItemTemplate >
    <asp:Label ID="ITlblRoutes" runat="server" Text='<%# Eval("Routes") %>' CssClass="label grdof" ToolTip='<%# Eval("Routes") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Booking Classes">
        <ItemTemplate >
    <asp:Label ID="ITlblBookingClasses" runat="server" Text='<%# Eval("BookingClasses") %>' CssClass="label grdof" ToolTip='<%# Eval("BookingClasses") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>--%>
         <asp:TemplateField HeaderText="Transaction Type">
        <ItemTemplate >
    <asp:Label ID="ITlblTransaction" runat="server" Text='<%# Eval("TransactionType") %>' CssClass="label grdof" ToolTip='<%# Eval("TransactionType") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Product">
        <ItemTemplate >
    <asp:Label ID="ITlblProduct" runat="server" Text='<%# Eval("ProductId") %>' CssClass="label grdof" ToolTip='<%# Eval("ProductId") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Agent">
        <ItemTemplate >
    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("AgentId") %>' CssClass="label grdof" ToolTip='<%# Eval("AgentId") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>



     </Columns>

     </asp:GridView>
     
</asp:Content>
