﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ApiGuestDetails.aspx.cs" Inherits="CozmoB2BWebApp.ApiGuestDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">



<%--    <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <link href="css/Default.css" rel="stylesheet" type="text/css" />
    <link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
--%>
    <!-- Bootstrap Core CSS -->
<%--    <link href="css/bootstrap.min.css" rel="stylesheet" />

        <!-- manual css -->
        <link href="css/override.css" rel="stylesheet" />
        <link href="<%=Request.Url.Scheme%>://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
        <link href="build/css/main.min.css" rel="stylesheet" type="text/css" />


    <link rel="shortcut icon" href="images/FAV/favicon.ico" />
    <script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>

    <%-- <script src="scripts/select2.min.js" type="text/javascript"></script>--%>

    <!-- New BE js -->

    <!-- end new Js -->

    <!-- end -->

    <!-- jQuery -->
    <!-- Bootstrap Core JavaScript -->
<%--    <script src="Scripts/bootstrap.min.js"></script>--%>


    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="build/js/main.min.js"></script>
    <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-ui.js"></script>


    <style>
        .error {
            border: 1px solid #D8000C;
        }
        .custom-checkbox-style input[type=checkbox]+label:before{
                line-height: 1;
        }

        .form-control::-webkit-input-placeholder { color: lightgray; }
        .form-control::-moz-placeholder { color: lightgray; }
        .form-control:-ms-input-placeholder { color: lightgray; }

        .disabled { color: #ccc; pointer-events:none; }

    </style>

   <!--Search Passenger-->
   <script>

       /* Function to bind the search passenger filters html and to show the pop up */
       function ShowSearhPassenger(paxindx, pcid) {

           /* To disable bootstrap modal close on escape button and pop up outside click */

           $("#SearchPassenger").modal({
                backdrop: 'static',
                keyboard: false
           });

           /* If previously opened pop up and the current pop up button click belongs to the same pax, 
            * it will avoid binding the html once again and just open the existing pop up */
           if (cindex == paxindx && document.getElementById('txtSearchEmpId') != null)
               $('#SearchPassenger').modal('show');
           else {

               cindex = paxindx; cid = pcid;
               $('#SearchFilters').children().remove();
               $("#PassengerDetailsList").children().remove();
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmpId" class="form-control" placeholder="Please enter employee id" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchFirstName" class="form-control" placeholder="Please enter first name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchLastName" class="form-control" placeholder="Please enter last name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmail" class="form-control" placeholder="Please enter email" /></div>');

               /* If the current pop up was clicked earlier and has any filter options, same filters will assign to the filter controls and, 
                * will perform the search for the same filters */

               if (SearchFilter[cid] != null && SearchFilter[cid] != '') {

                   var Filters = SearchFilter[cid].split('|');
                   document.getElementById('txtSearchEmpId').value = Filters[0];
                   document.getElementById('txtSearchFirstName').value = Filters[1];
                   document.getElementById('txtSearchLastName').value = Filters[2];
                   document.getElementById('txtSearchEmail').value = Filters[3];
                   SearchPassenger();
               }

               $('#SearchPassenger').modal('show');
           }
       }


       var SearchFilter = new Array(20);
       var selctedprofiles = new Array(20);
       var cindex; cid = 0;
       var PaxProfilesData;
       var PaxProfilesFlexData;
       var AirlineCode = '';

       /* Added .1 sec timer to show processing div on search passengers results loading */
       function SearchPassenger() {
           document.getElementById('ctl00_upProgress').style.display = 'block';
           setTimeout(function(){ SearchPassengers(); document.getElementById('ctl00_upProgress').style.display ='none'; }, 100);            
       }

       /* Function to search passengers list with the given filters in the pop up */
       function SearchPassengers() {

           $("#PassengerDetailsList").children().remove();
           var EmployeeId = document.getElementById('txtSearchEmpId').value, FstName = document.getElementById('txtSearchFirstName').value,
               LstName = document.getElementById('txtSearchLastName').value; Email = document.getElementById('txtSearchEmail').value;
           AirlineCode = '';

           if (EmployeeId == '' && FstName == '' && LstName == '' && Email == '') {

               SearchFilter[cid] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter any filter option with 3 characters</div>");
               return false;
           }

           if ((EmployeeId != '' && EmployeeId.length < 3) || (FstName != '' && FstName.length < 3) || (LstName != '' && LstName.length < 3) || (Email != '' && Email.length < 3)) {
               SearchFilter[cid] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter minimum 3 characters in the filter</div>");
               return false;
           }

           SearchFilter[cid] = EmployeeId + '|' + FstName + '|' + LstName + '|' + Email + '|' + AirlineCode;

           ShowPassengerDetail(AjaxCall('PassengerDetails.aspx/GetPaxProfiles', "{'sFilters':'" + SearchFilter[cid] + "'}"));           
       }

       /* Function to bind passenger search results list to grid */
       function ShowPassengerDetail(results) {

           if (results == '' || results == null || results.length == 0 || results == 'undefined') {
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }
           else {
               PaxProfilesData = JSON.parse(results[0]);
               PaxProfilesFlexData = JSON.parse(results[1]);
           }

           $("#PassengerDetailsList").append('<table id="tblPaxDetails" class="table b2b-corp-table"><tr><th><span>Select</span></th><th><span>Employee Code</span></th><th><span>First Name</span></th><th><span>Last Name</span></th><th><span>Email</span></th><th><span>Passport No</span></th></tr></table>');
           var hasdata = false;

           $.each(PaxProfilesData, function (key, prfdata) {

               if (selctedprofiles.indexOf(prfdata.ProfileId) == -1) {
                   hasdata = true;
                   $("#tblPaxDetails").append('<tr>' +
                       
                       '<td> <span class="glyphicon glyphicon-plus-sign" id="prof_' + prfdata.ProfileId + '" onclick="SelectPaxData(this,' + prfdata.ProfileId + ')" /> </td>' +
                       '<td class="text-left"> <span>' + prfdata.EmployeeId + '</span></td> <td class="text-left"><span>' + prfdata.FName + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.LName + '</span></td> <td class="text-left"><span>' + prfdata.Email + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.PassportNo + '</span></td>' +
                       '</tr >');
               }
           });

           if (!hasdata) {
               $("#PassengerDetailsList").children().remove();
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }

       }

       /* Function to assign flex field controls data for additional pax */
       function AssignFlexData(cntrltype, cntrlid, paxid, PrfId, flexId) {

            var flxdata = '';
            $.each(PaxProfilesFlexData, function (sno, prfflxdata) {
                if (prfflxdata.ProfileId == PrfId && flexId == prfflxdata.FlexId)
                    flxdata = prfflxdata.FlexData;
            });                   

           if (flxdata != null && flxdata != 'undefined' && flxdata != '') {

                if ((cntrltype == "T" || cntrltype == "D") && document.getElementById('txt' + paxid + 'Flex' + cntrlid) != null)
                    document.getElementById('txt' + paxid + 'Flex' + cntrlid).value = flxdata;
                else if (cntrltype == "L" && document.getElementById('ddl' + paxid + 'Flex' + cntrlid) != null) {                
                    Setddlval('ddl' + paxid + 'Flex' + cntrlid, flxdata, '');
                }
           }
       }

       /* Function to assign the selected profile data to screen passenger details window */
       function SelectPaxData(event, PrfId) {

           try {
               
               var selpaxdtls = ''
               
               selctedprofiles[cid] = PrfId;

               $.each(PaxProfilesData, function (key, prfdata) {

                   if (prfdata.ProfileId == PrfId)
                       selpaxdtls = prfdata;
               });      

               document.getElementById('spnPaxAdd' + cindex).innerHTML = 'Update Passenger';

               if (cindex.substring(0, 1) == 'A') {

                   Setddlval('ddlAdultTitle-' + cindex.substring(1, cindex.length), selpaxdtls.Title, '');
                   Settextval('txtAdultFirstName-' + cindex.substring(1, cindex.length), selpaxdtls.FName);
                   Settextval('txtAdultLastName-' + cindex.substring(1, cindex.length), selpaxdtls.LName);
               }
               else {

                   Setddlval('ddlChildTitle-' + cindex.substring(1, cindex.length), selpaxdtls.Title, '')
                   Settextval('txtChildFirstName-' + cindex.substring(1, cindex.length), selpaxdtls.FName);
                   Settextval('txtChildLastName-' + cindex.substring(1, cindex.length), selpaxdtls.LName);
               }

               if (cindex == leadpax) {
                                      
                   document.getElementById('txtAdultEmail-01').value = selpaxdtls.Email;
                   document.getElementById('txtAdultPhone-01').value = selpaxdtls.Mobilephone;
                   document.getElementById('txtAdultAddress-01').value = selpaxdtls.Address1;
               }     

               var ancAPI = document.getElementById('ancdivFlex' + cindex);
               if (ancAPI != null && ancAPI.attributes["aria-expanded"].value == 'false')
                   ancAPI.click();

               if (FlexInfo != null && FlexInfo != 'undefined' && FlexInfo != '' && FlexInfo.length > 0) {

                   var flexids = []; 

                   $.each(FlexInfo, function (key, col) {
                                              
                       flexids.push('|' + col.flexId + col.flexControl + '|');

                       if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

                            var dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + cindex + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
                                'ddl' + cindex + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');
                            var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        
                            BindFlexdynamicddl(document.getElementById(dcntid).value, curflex + cindex + 'Flex' + key, col.flexSqlQuery, curflex); 
                       }
                       
                       AssignFlexData(col.flexControl, key, cindex, PrfId, col.flexId);
                   });    
               }    
           }
           catch (exception) {
               var exp = exception;
           }
           $('#SearchPassenger').modal('hide');
       }

   </script>

    <script type="text/javascript">

        /* To set lead pax traveller info for corporate profile */
        function AssignCorpData() {

            var corpinfo = JSON.parse(document.getElementById('<%=hdnCorpProfInfo.ClientID %>').value);

            Setddlval('ddlAdultTitle-01', corpinfo.Title, '');
            Settextval('txtAdultFirstName-01', corpinfo.SurName);
            Settextval('txtAdultLastName-01', corpinfo.Name);
            Settextval('txtAdultPhone-01', corpinfo.Mobilephone);
            Settextval('txtAdultEmail-01', corpinfo.Email);
            Settextval('txtAdultAddress-01', corpinfo.Address1);
            selctedprofiles[0] = corpinfo.ProfileId;

            document.getElementById('agentBalance').style.display = document.getElementById('divATB').style.display =
                document.getElementById('btnSearchPaxA01').style.display = document.getElementById('divPaxAddA01').style.display = 'none';

        }

        /* To bind flex field drop down list dynamically based on dependent flex field selected value */
        function BindFlexdynamicddl(selval, ddlid, ddlquery, cntrl) {

            try {

                ddlquery = (ddlquery == null || ddlquery == 'undefined' || ddlquery == '') ? '' : ddlquery.replace(/@value/g, selval);
                if (cntrl == 'ddl' && ddlquery != '')
                    document.getElementById(ddlid).innerHTML = GetFlexddlData(ddlquery, cntrl);
                else
                    document.getElementById(ddlid).value = ddlquery != '' ? GetFlexddlData(ddlquery, cntrl) : selval;
                document.getElementById(ddlid.replace(cntrl, 'div')).style.display = (selval != '' && selval != '-1') ? 'block' : 'none';
                Setddlval(ddlid, '-1', '');
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To get data for flex field drop down list based on flex query */
        function GetFlexddlData(ddlquery, cntrl) {

            try {

                ddlquery = ddlquery.replace(/'/g, '^');        
                var ddldata = (ddlquery == '' || ddlquery == 'undefined' || ddlquery == null) ? '' :
                    JSON.parse(AjaxCall('BookOffline.aspx/GetFlexddlData', "{'sddlQuery':'" + ddlquery + "'}"));
                if (ddldata != '') {

                    var col = [];

                    for (var key in ddldata[0]) {
                        col.push(key);
                    }

                    var ddlhtml = '';

                    if (cntrl == 'ddl') {
                        
                        for (var d = 0; d < ddldata.length; d++) {

                            ddlhtml += '<option value="' + ddldata[d][col[0]] + '">' + ddldata[d][col[1]] + '</option>';
                        }
                    }
                    return cntrl == 'txt' ? ddldata[0][col[0]] : '<option selected="selected" value="">--Select--</option>' + ddlhtml;
                }
                else
                    return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
            }
            catch (exception) {
                return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
            }
        }

        function BindFlexFields() {

            try {
                
                FlexInfo = JSON.parse(document.getElementById('<%=hdnFlexInfo.ClientID %>').value);
                Flexconfigs = document.getElementById('<%=hdnFlexConfig.ClientID %>').value == '' ? '' :
                    document.getElementById('<%=hdnFlexConfig.ClientID %>').value.split('|');

                for (var i = 0; i < (IsCorp ? 1 : Result.RoomGuest.length); i++) {

                    var NoofAdult = IsCorp ? 1 : Result.RoomGuest[i].noOfAdults;
                    var noofChlid = IsCorp ? 0 : Result.RoomGuest[i].noOfChild;

                    for (var j = 1; j <= NoofAdult; j++) {

                        GetFlexHTML('divFlexA' + i + j, 'A' + i + j);            
                        document.getElementById('divFlexA' + i + j).style.display = 'block';  
                    }

                    for (var k = 1; k <= noofChlid; k++) {

                        GetFlexHTML('divFlexC' + i + k, 'C' + i + k);               
                        document.getElementById('divFlexC' + i + k).style.display = 'block';  
                    }
                }
                    
                $('select').select2();
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To Bind Flex fields */
        function GetFlexHTML(divid, paxid) {
            
            if (FlexInfo != null) {

                var flexids = [];
                                
                $.each(FlexInfo, function (key, col) {

                    flexids.push('|' + col.flexId + col.flexControl + '|');

                    if (col.FlexProductId != '2')
                        return;

                    if (Flexconfigs.length > 0) {

                        var IsBind = true;
                        for (var r = 0; r < Flexconfigs.length; r++) {

                            if (!IsBind) { return; }
                            if (request.Corptravelreason != Flexconfigs[r].split(',')[0] && col.flexLabel.toUpperCase() == Flexconfigs[r].split(',')[1].toUpperCase())
                                IsBind = false;
                        }
                        if (!IsBind) { return; }
                    }
                    
                    var dataevnt = col.flexDataType == 'T' ? '' :
                        col.flexDataType == 'N' ? 'return IsNumeric(event)' : 'return IsAlphaNum(event)';
                    var dateval = col.flexControl == 'D' ? 'ValidateDate(this)' : '';
                    var cntrlhtml = '';
                    if (col.flexControl == 'T' || col.flexControl == 'D')
                        cntrlhtml = AddTextbox(col.flexLabel, paxid + 'Flex' + key, (paxid == leadpax ? col.Detail_FlexData : ''), (col.flexMandatoryStatus == 'Y'), (col.flexControl == 'T' ? dataevnt : ''), '50', (col.flexControl == 'D' ? dateval : ''), col.flexControl == 'D', col.flexDisable);
                    if (col.flexControl == 'L')
                        cntrlhtml = AddFlexddl(col.flexLabel, paxid + 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), (col.flexSqlQuery.indexOf('@value') != '-1' ? '' : col.flexSqlQuery), paxid, col.flexDisable);

                    var isDependent = false; var dcntid = '';

                    if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

                        dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + paxid + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
                            'ddl' + paxid + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');

                        if (document.getElementById(dcntid) == null)
                            return;

                        var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        var ocflex = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' : 'ddl';

                        isDependent = document.getElementById(dcntid).value == '' || document.getElementById(dcntid).value == '-1';             
                        
                        $('#' + dcntid).on('change', function (e) { BindFlexdynamicddl((ocflex == 'txt' ? e.target.value : e.val), curflex + paxid + 'Flex' + key, col.flexSqlQuery, curflex); });
                    }

                    isDependent = paxid == leadpax ? (isDependent && (col.Detail_FlexData == null || col.Detail_FlexData == 'undefined' || col.Detail_FlexData == '')) : isDependent;
                    
                    cntrlhtml = '<div ' + (isDependent ? 'style="display:none"' : '') + ' class="form-group col-md-4 ' + (col.flexDisable ? 'disabled' : '') + '" '
                        + 'id="div' + paxid + 'Flex' + key + '">' + cntrlhtml + '</div>';

                    if (key == 10) {                            

                        $('#' + divid).append('<div class="collapse" id="' + divid + 'MoreFlex"><div id="' + divid + 'More" class="row padding-0 marbot_10 ">' +
                            cntrlhtml + '</div>');
                        $('#' + divid).append(
                            '<a style="color: rgb(0, 0, 0); margin-left: 0px; padding: 0px;" id="anc' + divid + '" ' +
                            'aria-expanded="false" aria-controls="collapseExample" class="advPax-collapse-btn-new float-left"' +
                            'role="button" data-toggle="collapse" href="#' + divid + 'MoreFlex"></a>');
                    }
                    else if (key > 10)
                        $('#' + divid + 'More').append(cntrlhtml);
                    else
                        $('#' + divid).append(cntrlhtml);      

                    if (paxid == leadpax && col.flexControl != 'T' && col.Detail_FlexData != null && col.Detail_FlexData != 'undefined' && col.Detail_FlexData != '') {

                        if (dcntid != '') 
                            BindFlexdynamicddl(document.getElementById(dcntid).value, 'ddl' + paxid + 'Flex' + key, col.flexSqlQuery, 'ddl');
                        if (col.flexControl == 'L' && document.getElementById('ddl' + paxid + 'Flex' + key) != null)
                            Setdropval('ddl' + paxid + 'Flex' + key, col.Detail_FlexData, '');
                    }
                                    
                });                            
            }
        }

        /* To add mandatory aestrick span */
        function AddMandatory(isreq) {
            return isreq ? '<span class="red_span">*</span>' : '';
        }

        /* To add dynamic text box field */
        function AddTextbox(lable, id, val, Isreq, onkeypress, maxlen, onchange, IsDate, IsDisable) {        

            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <input id="txt' + id + '" type="text" onkeypress="' + onkeypress +
                '" placeholder= "Enter ' + lable + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control ' + (Isreq ? 'FlexReq ' : '') + (IsDate ? 'EnableCal' : '') +
                '" maxlength="' + maxlen + '" ' +
                (onchange != null && onchange != 'undefined' && onchange != '' ? 'onchange="' + onchange + '" ' : '') +
                (val != null && val != 'undefined' && val != '' ? 'value="' + val + '" ' : '') + '>';
        }

        /* To Bind Flex field drop down list based on flex query */
        function AddFlexddl(lable, id, val, Isreq, ddlquery, paxid, IsDisable) {
            
            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <select id="ddl' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control ' + (Isreq ? 'FlexReq' : '') + '">' +
                (ddlquery == '' ? '<option selected="selected" value="">--Select--</option>' : (paxid == leadpax ? GetFlexddlData(ddlquery, 'ddl')
                : document.getElementById('ddl' + leadpax + id.substring(3, id.length)).innerHTML)) +
                '</select>';
        }

        var FlexInfo = ''; leadpax = 'A01'; Flexconfigs = '';
        var IsCorp = false;

        /* To delete error class for all controls */
        function DelErrorClass() {

            var errmsgs = $('.error');
            if (errmsgs != null && errmsgs.length > 0) {

                $.each(errmsgs, function (key, cntrl) {

                    $('#' + cntrl.id).removeClass('error');
                });
            }
        }

        /* To save flex data */
        function SaveFlexInfo(p) {

            try {
                
                if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '')
                    return [];

                var flexcnt = [];                             

                $.each(FlexInfo, function (key, col) {      
                    
                    var flexdat = '';                    
                                                
                    if ((col.flexControl == 'T' || col.flexControl == 'D') && document.getElementById('txt' + p + 'Flex' + key) != null)
                        flexdat = document.getElementById('txt' + p + 'Flex' + key).value;
                    else if (col.flexControl == 'L' && document.getElementById('ddl' + p + 'Flex' + key) != null &&
                        document.getElementById('ddl' + p + 'Flex' + key).value != '' &&
                        document.getElementById('ddl' + p + 'Flex' + key).value != '-1') {
                        flexdat = document.getElementById('ddl' + p + 'Flex' + key).value;
                    } 

                    flexcnt.push({
                        FlexId: col.flexId, FlexLabel: col.flexLabel, FlexGDSprefix: col.flexGDSprefix,
                        FlexData: (flexdat != null && flexdat != '') ? flexdat : '', ProductID: '2'
                    });
                }); 
                                
                return flexcnt;
            }
            catch (exception) {
                return [];
            }
        }

        function ValidateFlex() {

            var fields = $('.FlexReq');
            var Count = 0;

            for (var i = 0; i < fields.length; i++) {
                
                if (fields[i].nodeName.toUpperCase() != "DIV" && $(fields[i]).val().trim() == '') {
                                        
                    $(fields[i]).addClass("error");
                    Count++;                 
                }
            }
            return Count == 0;
        }

        function ValidateDate(event) {
                        
            if (event.value == '')
                return;

            var date = event.value.split('/');

            if (!(date.length == 3 && date[0] < 32 && date[1] < 13 && date[2] < 2100 && date[2] > 1947)) {
                                
                ShowError('Please enter valid date');
                document.getElementById(event.id).focus();
                event.value = '';
            }                
        }

    </script>

    <script>
        var request;//request object from CS
        var sessionId;//Session Id from CS
        var Result;//Results from CS
        var SelectedRoom;
        var SelectedroomType;
        var RoomType = [];
        var product; var AgentId; var UserId; var bookingResult;
        var rows;

        $(document).ready(function () {//Page Load

            /* To set corporate flag*/
            IsCorp = document.getElementById('<%=hdnCorpProfInfo.ClientID %>').value != '';

            request = JSON.parse(document.getElementById('<%=hdnObjRequest.ClientID %>').value);
            sessionId = JSON.parse(document.getElementById('<%=hdnSessionId.ClientID %>').value);
            Result = JSON.parse(document.getElementById('<%=hdnHotelDetails.ClientID %>').value);
            SelectedroomType = JSON.parse(document.getElementById('<%=hdnSelectedRoomTypes.ClientID %>').value);
            AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
            UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
            // GetPaymentPreference();

            //Agent Balance for Pay By Label 
            $('#agentBalance').children().remove(); 
            $('#agentBalance').text("");  
            var bal = $("#ctl00_lblOBAgentBalance").text();
            bal= bal.match(/(\d+)/g); 
            if (bal != "" && bal!=null)  {                
                $('#agentBalance').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' +bal);
            }
            else {
                $('#agentBalance').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' + $('#ctl00_lblAgentBalance').text());
            }
        
            LoadRoomSummary(SelectedroomType);//Display Hotel Details
            rows = tblContent(SelectedroomType);
            $('#Price-roomsummary').append(rows);

            $(".PDrequired").blur(function () {

                tmpval = $(this).val().trim();
                if (tmpval == '') {
                    $(this).addClass('error'); $(this).val('');
                } else {
                    $(this).removeClass('error');
                    if (this.id == 'txtAdultEmail-01') {
                         $('#txtAdultEmail-01').next('span').remove();
                    }
   
                }
            });

            $('input[type=radio][id=rbnAccount]').change(function () {

                if ($("input:radio[id='rbnAccount']").is(":checked")) {
                    $("#divCard").hide();
                } 
            });

            $('input[type=radio][id=rbncard]').change(function () {

                if ($("input:radio[id='rbncard']").is(":checked")) {
                    $("#divCard").show();
                } 
            });

            /* To bind flex fields on page load */
            if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value != '') 
                BindFlexFields();

            /* To bind corporate info for lead pax */
            if (document.getElementById('<%=hdnCorpProfInfo.ClientID %>').value != '') 
                AssignCorpData();

            /* To enable date picker calender icon */
            $(".EnableCal").datepicker({ dateFormat: 'dd/mm/yy' });

            /* To enable error indicator for flex mandatory fields */
            $(".FlexReq").blur(function () {

                if ($(this).val() == '') 
                    $(this).addClass('error');
                else
                    $(this).removeClass('error');   
            });
        });

        function select(id) {
             tmpval = $('#'+id).val();
                if (tmpval == '') {
                    $('#'+id).addClass('error');
                } else {
                    $('#'+id).removeClass('error');
                }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }
        // Validating the characters.
        function isAlphaNumeric(evt) {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
            if (charCode == 32)
                return true;
            if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                return false;

            }
            return true;
        }
        //Get Payment Preferences
        function GetPaymentPreference() {
            
            var paymentPrefParams=JSON.parse(document.getElementById('<%=hdnPaymentPrefParams.ClientID %>').value);
            var Data = { SessionId: sessionId, PaymentPrefParams: paymentPrefParams };
             var apiUrl = DynamicAPIURL();
            $.ajax({
                url: apiUrl+'/api/HBooking/GetPaymentPreference',
                type: 'POST',
                ContentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: Data,

                //jData,
                success: function (data) {

                },
               error: function (xhr, textStatus, errorThrown) {
                      var msg = "Booking may or may not be happened please contact with Admin";
                      window.location.href = "ErrorPage.aspx?Err=" + msg;
                }
            });
           
        }

        //Display Hotel Details
        function LoadRoomSummary(SelectedroomType) {
            $('#list-group-roomsummary').children().remove();
            var templateRoomSummary = $('#templateRoomSummary').html();
            $('#startDate').text(new Date(Result.StartDate).toDateString());
            $('#endDate').text(new Date(Result.EndDate).toDateString());
            $('#h4HotelId').text(Result.HotelName);
            $('#HotelAddress').text(Result.HotelAddress);

            var rating;
            if (Result.Rating == 1) {
                rating = 'one-star-hotel';
            }
            else if (Result.Rating == 2) {
                rating = 'two-star-hotel';
            }
            else if (Result.Rating == 3) {
                rating = 'three-star-hotel';
            }
            else if (Result.Rating == 4) {
                rating = 'four-star-hotel';
            }
            else if (Result.Rating == 5) {
                rating = 'five-star-hotel';
            }
            $('#Rating').addClass(rating);
            if (SelectedroomType != null) {

                for (var m = 0; m < SelectedroomType.length; m++) {
                    $('#list-group-roomsummary').append('<li id="List' + m + '" class="item">' + templateRoomSummary + '</li>');
                    $('#h5RoomName').attr('id', 'h5RoomName-' + m);
                    $('#h5RoomName-' + m).text(SelectedroomType[m].RoomTypeName);
                    $('#mealPlanDesc').attr('id', 'mealPlanDesc-' + m);
                    $('#mealPlanDesc-' + m).text(SelectedroomType[m].mealPlanDesc);
                    $('#roomGuestsCount').attr('id', 'roomGuestsCount-' + m);
                    $('#roomGuestsCount-' + m).text(Result.RoomGuest[m].noOfAdults + " Adult(s) & " + Result.RoomGuest[m].noOfChild + " Child(s)");
                }
            }
            loadControls();
        }

        function tblContent(SelectedroomType) {
           
            var lblGSTVAT = request.LoginCountryCode == "IN" ? "Total GST " : "VAT ";
            var VAT  = 0;
            var objItinerary = JSON.parse(document.getElementById('<%=hdnItinerary.ClientID %>').value);
            for (var i = 0; i < objItinerary.Roomtype.length; i++) {
                VAT += objItinerary.Roomtype[i].Price.OutputVATAmount;
            }
             
            var decimalvalue = document.getElementById('<%=hdndecimal.ClientID %>').value;
            if (SelectedroomType != null) {
                var tr;
                var total = 0;
                tr += "<tr><th scope='row' class='font-weight-bold'>ROOM TYPE</th><td class='font-weight-bold'>TOTAL (in " + Result.Currency + ")</td></tr>"

                for (var m = 0; m < SelectedroomType.length; m++) {
                    tr = tr + "<tr><th scope='row'  class='RoomType' id=FareRoomType-" + m + ">" + SelectedroomType[m].RoomTypeName + "</th > <td id=roomPrice" + m + " >" + (SelectedroomType[m].TotalPrice - SelectedroomType[m].Discount).toFixed(decimalvalue) + "</td > </tr >"
                    total = (eval(total)+eval(SelectedroomType[m].TotalPrice - SelectedroomType[m].Discount)).toFixed(decimalvalue);
                }
                total = parseFloat(total) + parseFloat(VAT);
                tr = tr + "<tr><th scope='row'>"+lblGSTVAT +"</th><td id='VAT' >" + (VAT).toFixed(decimalvalue) + "</td></tr>"
                tr = tr + "<tr><th scope='row' >TOTAL</th><td id='totalRoomPrice'>" + (total).toFixed(decimalvalue) + "</td></tr>"
                //tr = tr + "<tr><th scope='row'>TOTAL INCL MARKUP</th><td id='totalwithMarkUp'></td></tr>"
                
                tr = tr + "<tr><th scope='row' class='font-weight-bold grand-total'>GRAND TOTAL</th><td class='font-weight-bold' id='grandTotal'>" + Math.ceil(total).toFixed(decimalvalue) + "</td></tr>"
                rows = tr;

                //to show for Amount to be booked label
                $('#amountToBeBook').children().remove(); 
                $('#amountToBeBook').text("");  
                $('#amountToBeBook').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' +  Math.ceil(total).toFixed(decimalvalue) );

                return rows;
            }
        }



        //Creating Dynamic Controls based on Noof Room ,Noof adults,Childs
        function loadControls() {

            var GuestAdult = $('#Adult').html();
            var GuestChild = $('#Child').html();
            var Count = 1; var ChildCount = 0; var TotalPax = 1;

            for (var i = 0; i < Result.RoomGuest.length; i++) {
                var NoofAdult = Result.RoomGuest[i].noOfAdults;
                var noofChlid = Result.RoomGuest[i].noOfChild;

                for (var j = 1; j <= NoofAdult; j++) {
                    if (i == 0 && j == 1) {
                    }
                    else {
                        $('#ListGuest').append('<li id="ListAdult' + i + j + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + GuestAdult + '</li>');
                        Count++;

                        var Title = ' <label>Title</label><select class="form-control custom-select PDrequired" id="ddlAdultTitle-' + i + j +
                            '" onchange="select(this.id);"><option value="">Select Title</option> <option value="Mr">Mr</option> <option value="Mrs">Mrs</option></select>';

                        $('#RoomCount').attr('id', 'RoomCount-' + i + j);
                        $('#RoomCount-' + i + j).text('Room-' + (i + 1));
                        $('#h4Adult').attr('id', 'h4Adult-' + i + j);
                        $('#h4Adult-' + i + j).text('Guest Adult-' + Count);
                        $('#divAdultTitle').attr('id', 'divAdultTitle-' + i + j);
                        $('#divAdultTitle-' + i + j).append(Title);

                        //$('#ddlAdultTitle').attr('id', 'ddlAdultTitle-' + i + j);
                        //$('#ddlAdultTitle-' + i + j).addClass("PDrequired");

                        $('#s2id_ddlAdultTitle').attr('id', 's2id_ddlAdultTitle-' + i + j);
                        $('#txtAdultFirstName').attr('id', 'txtAdultFirstName-' + i + j);
                        $('#txtAdultFirstName-' + i + j).addClass("PDrequired");
                        $('#txtAdultLastName').attr('id', 'txtAdultLastName-' + i + j);
                        $('#txtAdultLastName-' + i + j).addClass("PDrequired");

                        $('#divFlexA').attr('id', 'divFlexA' + i + j);                        
                        $('#btnSearchPaxA').attr('id', 'btnSearchPaxA' + i + j);                        
                        $('#btnSearchPaxA' + i + j).attr('onclick', 'ShowSearhPassenger("A' + i + j + '", ' + TotalPax + ')');
                        document.getElementById('btnSearchPaxA' + i + j).style.display = IsCorp ? 'none' : 'block';
                        $('#spnPaxAddA').attr('id', 'spnPaxAddA' + i + j);    
                        $('#chkPaxAddA').attr('id', 'chkPaxAddA' + i + j);    
                        $('#divPaxAddA').attr('id', 'divPaxAddA' + i + j);    
                        document.getElementById('divPaxAddA' + i + j).style.display = IsCorp ? 'none' : 'block';
                        TotalPax++;
                    }
                }
                for (var k = 1; k <= noofChlid; k++) {
                    $('#ListGuest').append('<li id="ListChild' + i + k + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + GuestChild + '</li>');
                    ChildCount++;
                                            var ChildTitle = ' <label>Title</label><select class="form-control custom-select PDrequired" id="ddlChildTitle-' + i + k + '" onchange="select(this.id);"><option value="">Select Title</option><option value="Master">Master</option> <option value="Mr">Mr</option> <option value="Mrs">Mrs</option></select>';

                    $('#h4Child').attr('id', 'h4Child-' + i + k);
                    $('#h4Child-' + i + k).text('Guest Child-' + ChildCount);
                     $('#divchild').attr('id', 'divchild-' + i + k);
                        $('#divchild-' + i + k).append(ChildTitle);
                    //$('#ddlChildTitle').attr('id', 'ddlChildTitle-' + i + k);
                    //$('#ddlChildTitle-' + i + k).addClass("PDrequired");
                    $('#txtChildFirstName').attr('id', 'txtChildFirstName-' + i + k);
                    $('#txtChildFirstName-' + i + k).addClass("PDrequired");
                    $('#txtChildLastName').attr('id', 'txtChildLastName-' + i + k);
                    $('#txtChildLastName-' + i + k).addClass("PDrequired");
                    $('#divFlexC').attr('id', 'divFlexC' + i + k);
                    $('#btnSearchPaxC').attr('id', 'btnSearchPaxC' + i + k);                        
                    $('#btnSearchPaxC' + i + k).attr('onclick', 'ShowSearhPassenger("C' + i + k + '", ' + TotalPax + ')');
                    document.getElementById('btnSearchPaxC' + i + k).style.display = IsCorp ? 'none' : 'block';
                    $('#spnPaxAddC').attr('id', 'spnPaxAddC' + i + k);    
                    $('#chkPaxAddC').attr('id', 'chkPaxAddC' + i + k);     
                    $('#divPaxAddC').attr('id', 'divPaxAddC' + i + k);    
                    document.getElementById('divPaxAddC' + i + k).style.display = IsCorp ? 'none' : 'block';
                    TotalPax++;
                }
            }
            $('select').select2();
        }

        //Booking Confirmation
        function ConfirmBooking() {

            DelErrorClass();

		  $("#ctl00_upProgress").show();
            var fields = $('.PDrequired');
            var Count = 0;
            for (var i = 0; i < fields.length; i++) {
                //console.log(fields[i].nodeName);
                if (fields[i].nodeName != "DIV") {
                    var textLength = $(fields[i]).val().trim();
                    if ($(fields[i]).val().trim() == '' || textLength.length < 2 ) {
                        $(fields[i]).addClass("error");
                        Count++;
                    }
                }


            }

            if ($('#txtAdultEmail-01').val() != "") {
                 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  
                if (!emailReg.test($('#txtAdultEmail-01').val())) {
                    $('#txtAdultEmail-01').addClass("error");
                    //$('#txtAdultEmail-01').after('<span style="color:red;">Enter valid Email</span>');
                    $("#ctl00_upProgress").hide();
                    return false;
                } else {
                    $('#txtAdultEmail-01').removeClass("error");

                }
            }

            if (!ValidateFlex() && Count == 0)
                Count++;

            if (Count != 0) {   
                 $("#ctl00_upProgress").hide();
                return false;
            }
            else {
                if ($("#ChkConfirm").is(":checked") && $("#ChkAddFee").is(":checked")) {
                    $('#ChkConfirmDiv').hide();
                    LoadPassengerDetails();
                    Createitinerary();
                }
                else {
                    $("#ctl00_upProgress").hide();
                    $('.chk_err_msg').hide();
                    $('#ChkConfirmDiv').show();
                    if (!$("#ChkConfirm").is(":checked")) {
                        $('.chk_confirm_msg').show();
                   }
                    if (!$("#ChkAddFee").is(":checked")) {
                        $('.chk_addfee_msg').show();
                   }
                    return false;
                }
                // $("#ctl00_upProgress").hide();
            }
        }
        //Assign all Passenger to Array of Object
        function LoadPassengerDetails() {
            var Hotel = []; var Passenger = {}; var totalpax = 0;
            for (var i = 0; i < Result.RoomGuest.length; i++) {
                Hotel = [];
                var NoofAdult = Result.RoomGuest[i].noOfAdults;
                var noofChlid = Result.RoomGuest[i].noOfChild;
                for (var j = 1; j <= NoofAdult; j++) {
                    var Leadpass;
                    if (i == 0 && j == 1) { Leadpass = true } else { Leadpass = false; }
                    Hotel.push({
                        PaxId: 0,
                        HotelId: 0,
                        Title: $('#ddlAdultTitle-' + i + j).val(),
                        Firstname: $('#txtAdultFirstName-' + i + j).val(),
                        Middlename: "",
                        Lastname: $('#txtAdultLastName-' + i + j).val(),
                        Phoneno: $('#txtAdultPhone-' + i + j).val(),
                        Countrycode: request.CountryCode,
                        NationalityCode: request.CountryName,
                        Areacode: "",
                        email: $('#txtAdultEmail-' + i + j).val(),
                        Addressline1: $('#txtAdultAddress-' + i + j).val(),
                        Addressline2: "",
                        city: $('#txtAdultCity-' + i + j).val(),
                        Zipcode: "",
                        State: "",
                        Country: request.PassengerCountryOfResidence,
                        Nationality: request.PassengerNationality,
                        PaxType: "Adult",
                        RoomId: 0,
                        LeadPassenger: Leadpass,
                        Age: 0,
                        Employee: "",
                        Division: "",
                        Department: "",
                        Purpose: "",
                        EmployeeID: "",
                        IsGST: false,
                        HotelPassengerGST: {},
                        FlexDetailsList: (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '' || (IsCorp && !Leadpass)) ? [] : SaveFlexInfo('A' + i + j),
                        CorpProfileId: document.getElementById('chkPaxAdd' + 'A' + i + j).checked ?
                            ((selctedprofiles[totalpax] != null && selctedprofiles[totalpax] != '') ? selctedprofiles[totalpax] + '-U' : '-1') : ''
                    });
                    totalpax++;
                }
                for (var k = 1; k <= noofChlid; k++) {
				 var AgeArr = Result.RoomGuest[i].childAge;
                    var Age = AgeArr[k - 1];
                    Hotel.push({
                        PaxId: 0,
                        HotelId: 0,
                        Title: $('#ddlChildTitle-' + i + k).val(),
                        Firstname: $('#txtChildFirstName-' + i + k).val(),
                        Middlename: "",
                        Lastname: $('#txtChildLastName-' + i + k).val(),
                        Phoneno: "",
                        Countrycode: request.CountryCode,
                        NationalityCode:  request.CountryName,
                        Areacode: "",
                        Email: "",
                        Addressline1: "",
                        Addressline2: "",
                        City: "",
                        Zipcode: "",
                        State: "",
                        Country: request.PassengerCountryOfResidence,
                        Nationality: request.PassengerNationality,
                        PaxType: "Child",
                        RoomId: 0,
                        LeadPassenger: false,
                        Age: Age,
                        Employee: "",
                        Division: "",
                        Department: "",
                        Purpose: "",
                        EmployeeID: "",
                        IsGST: false,
                        HotelPassengerGST: {},
                        FlexDetailsList: (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '' || (IsCorp && !Leadpass)) ? [] : SaveFlexInfo('C' + i + k),
                        CorpProfileId: document.getElementById('chkPaxAdd' + 'C' + i + k).checked ?
                            ((selctedprofiles[totalpax] != null && selctedprofiles[totalpax] != '') ? selctedprofiles[totalpax] + '-U' : '-1') : ''
                    });
                    totalpax++;
                }
                RoomType.push({ PassenegerInfo: Hotel });
            }
        }
        //Creating Itinerary in WebMethod
        function Createitinerary() {
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var roominfo = JSON.stringify(RoomType);
            var addlmarkup = document.getElementById('<%=hdnAsvAmount.ClientID %>').value;
            var remarks = $('#txtRemarks').val();
            var specialRequest = $('#txtSpecialRequest').val();
            $.ajax({
                type: "POST",
                url: "ApiGuestDetails.aspx/Createitinerary",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: "{'data':'" + roominfo + "','addlMarkup':'" + addlmarkup + "','remarks':'" + remarks + "','specialRequest':'" + specialRequest + "' }",
                success: function (data) {
                    if (data == null) {
                        $("#ctl00_upProgress").hide();
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Dear Agent something went wrong, Please try again.";
                    }
                    else {
                        product = JSON.parse(data.d);
                        if (product == "In Sufficient Balance") {
                            $("#ctl00_upProgress").hide();
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Dear Agent you do not have sufficient balance to book a room for this hotel.";
                        }
                        else {
                            document.getElementById('errMess').style.display = "none";
                            document.getElementById('errorMessage').innerHTML = "";
                            BookingHotel();
                        }
                    }

                },
                error: function (d) {
                    $("#ctl00_upProgress").hide();
                }
            });
        }
        function DynamicAPIURL() {
                debugger;
                var url;
                var Secure ='<%=Request.IsSecureConnection%>';
            if (Secure=='True') {
                url = "<%=Request.Url.Scheme%>://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            else {
                  url = "<%=Request.Url.Scheme%>://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            return url;
        }
        function BookingHotel() {
            var Data = { Product: product, AgentId: AgentId, UserId: UserId, SessionId: sessionId };
             var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
            if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }
            $.ajax({
                url: apiUrl+'/api/HBooking/BookHotel',
                type: 'POST',
                ContentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: Data,

                //jData,
                success: function (data) {
                    var response = JSON.parse(data);
                    bookingResult = response.bookingResponse;
                    product = response.itinerary;
                    BookingConfirm();
                },
                error: function (xhr, textStatus, errorThrown) {
                      var msg = "Booking may or may not be happened please contact with Admin";
                      window.location.href = "ErrorPage.aspx?Err=" + msg;
                }
            });
        }
        function BookingConfirm() {
            var bookingResponse = JSON.stringify(bookingResult);
            var itinerary = JSON.stringify(product);
            $.ajax({
                url: "ApiGuestDetails.aspx/CreateInvoice",
                type: "POST",
                async: false,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                data: "{'bookingResponse':'" + bookingResponse + "','hotelItinerary':'" + itinerary + "'}",
                success: function (data) {
                    console.log(data.d);
                    var error = data.d;
                    if (error.includes("error")) {
                        var Error = error.split("||");
                        window.location.href = "ErrorPage.aspx?Err=" + Error[1];
                    }
                    else {
                        var response = JSON.parse(data.d);
                        window.location.href = "APIHotelPaymentVoucher.aspx?ConfNo=" + response;
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                   // $("#ctl00_upProgress").hide();
                      var msg = "Booking may or may not be happened please contact with Admin";
                      window.location.href = "ErrorPage.aspx?Err=" + msg;
                }
            })
        }
        function CalculateAddlMarkUp() {
             var decimalvalue = document.getElementById('<%=hdndecimal.ClientID %>').value;
            if (SelectedroomType != null && $('#addlMarkUp').val() != "") {
                if ($('#addlMarkUp').hasClass("error")) {
                    $('#addlMarkUp').removeClass("error");
                }
                 var VAT  = 0;
                 var objItinerary = JSON.parse(document.getElementById('<%=hdnItinerary.ClientID %>').value);
                 for (var i = 0; i < objItinerary.Roomtype.length; i++) {
                    VAT += objItinerary.Roomtype[i].Price.OutputVATAmount;
                 }
                var total = 0;
                for (var m = 0; m < SelectedroomType.length; m++) {
                    total += SelectedroomType[m].TotalPrice - SelectedroomType[m].Discount;
                }
                var type = $('#ddlAddlType').val();
                var markupvalue = $('#addlMarkUp').val();
                var addlMarkup;
                if (type == "P") {
                    addlMarkup = eval(total) * eval(markupvalue) / 100;
                }
                else {
                    addlMarkup = markupvalue;
                }
                total = eval(total) +eval(VAT) + eval(addlMarkup)
                document.getElementById('<%=hdnAsvAmount.ClientID %>').value = addlMarkup;
                $('#totalRoomPrice').text(total.toFixed(decimalvalue));
                $('#grandTotal').text(Math.ceil(total).toFixed(decimalvalue));
                //to show for Amount to be booked label
                $('#amountToBeBook').children().remove(); 
                $('#amountToBeBook').text("");  
                $('#amountToBeBook').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' + Math.ceil(total).toFixed(decimalvalue) );

                $('#addlMarkUp').val("");
                 $('#ddlAddlType').select2('val', "P");
                
            }
            else {
                $('#addlMarkUp').addClass("error");
            }
        }
    </script>

     <input type="hidden" id="hdnItinerary" runat="server" />
    <input type="hidden" id="hdnObjRequest" runat="server" />
    <input type="hidden" id="hdnSessionId" runat="server" />
    <input type="hidden" id="hdnSelectedRoomTypes" runat="server" />
    <input type="hidden" id="hdnHotelDetails" runat="server" />
    <input type="hidden" id="hdnUserId" runat="server" />
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdndecimal" runat="server" />
    <input type="hidden" id="hdnAsvAmount" runat="server" value="0" />
    <input type="hidden" id="hdnPaymentPrefParams" runat="server" />
    <input type="hidden" id="hdnFlexInfo" runat="server" />
    <input type="hidden" id="hdnFlexConfig" runat="server" />
    <input type="hidden" id="hdnCorpProfInfo" runat="server" />
    <div class="cz-container">
        <div class="body_container ui-listing-page">

            <div class="ui-pax-details-page pt-3">
                <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" class="alert-messages alert-caution">
                    </div>
                </div>
                <div class="row custom-gutter">
                    <div class="col-md-8">
                        <h3 class="pt-0 pb-3">Guest Details</h3>
                        <div class="ui-form-pax-details mb-3" id="GuestDetails">
                            <!--Room 1 Starts-->
                            <div class="ui-bg-wrapper mb-1  ui-room-wrapper" id="room-1">
                                <div class="row custom-gutter">
                                    <div class="col-md-3">
                                        <span class="font-weight-bold label-title">ADULT 1  (LEAD GUEST) </span>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-link float-right" id="btnSearchPaxA01" onclick="ShowSearhPassenger('A01', 0)">Search Passenger
                                            <span class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="float-md-right room-type-title " id="Room1Name">Room 1  </span>
                                    </div>
                                </div>
                                <div class="row custom-gutter">
                                    <div id="divPaxAddA01" class="col-md-12">
                                        <span id="spnPaxAddA01" style="float:right">Add Passenger</span><input type="checkbox" id="chkPaxAddA01" style="float:right"/>
                                    </div>
                                </div>
                                <div class="row custom-gutter">
                                    <div class="form-group col-md-3">
                                        <label>Title</label>
                                        <select class="form-control custom-select PDrequired" id="ddlAdultTitle-01" onchange="select(this.id);">
                                            <option value="">Select Title</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>First Name</label>
                                        <input type="text" class="form-control PDrequired" placeholder="Enter First Name" id="txtAdultFirstName-01" onkeypress="return isAlphaNumeric(event);">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control PDrequired" placeholder="Enter Last Name" id="txtAdultLastName-01" onkeypress="return isAlphaNumeric(event);">
                                    </div>
                                    <%-- <div class="form-group col-md-3">
                                            <label>Nationality</label>
                                           <input type="text" class="form-control" disabled="disabled" id="txtAdultNationality-01"  />
                                        </div>--%>
                                    </div>
                                    <div class="row custom-gutter">
                                        <div class="form-group col-md-3">
                                            <label>City</label>
                                            <input type="text" class="form-control PDrequired" placeholder="Enter City" id="txtAdultCity-01">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Pin Code</label>
                                            <input type="text" class="form-control PDrequired" placeholder="Enter Pin Code" id="txtAdultPinCode-01" onkeypress="return isNumber(event);">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Address</label>
                                            <textarea class="form-control PDrequired" rows="2" placeholder="Enter Address" id="txtAdultAddress-01"></textarea>
                                        </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr class="dotted" />
                                    </div>
                                </div>
                            </div>
                            <!--END:Room 1 -->

                            <div class="ui-bg-wrapper">

                                    <div class="row custom-gutter">
                                        <div class="form-group col-md-6">
                                            <label>Email</label>
                                            <input type="text" class="form-control PDrequired" placeholder="Enter E-mail" id="txtAdultEmail-01">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone No</label>
                                            <input type="text" class="form-control PDrequired" placeholder="Enter Phone Number" id="txtAdultPhone-01" onkeypress="return isNumber(event);" maxlength="15">
                                        </div>
                                    </div>
                                    <div class="row custom-gutter baggage-wrapper" style="display:none" id="divFlexA01"></div>
                                </div>
                                <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListGuest"></ul>

                            <div class="ui-bg-wrapper mb-1">

                                    <div class="row custom-gutter">
                                        <div class="form-group col-md-6">
                                            <label>Internal Reference</label>
                                            <textarea class="form-control PDrequired"   placeholder="Enter Remarks" id="txtRemarks"></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Special Request</label>
                                            <textarea class="form-control PDrequired"   placeholder="Enter Special Request" id="txtSpecialRequest"></textarea>
                                          <label  style="text-transform: none;"><b>Please note additional requests are not guranteed you may contact the property/service provider directly to confirm/enquire</b></label><br />

                                        </div>
                                    </div>
                                </div>

                                <div class="ui-bg-wrapper mb-1" style="min-height: 267px;">
                                    <div class="row ui-htldtl-pmt-option">
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <span class="heading-normal text-gray-light mb-3 d-block">Pay by </span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="custom-radio-lg mb-3">
                                                        <input type="radio" name="customRadio" id="rbnAccount" checked="checked">
                                                        <label></label>
                                                        <div class="text">Account <span class="ac-balance primary-color" id="agentBalance"></span></div>
                                                    </div>
                                                    <div class="custom-radio-lg" style="display:none;">
                                                        <input type="radio" name="customRadio" id="rbncard">
                                                        <label></label>
                                                        <span class="text">Credit / Debit Card</span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-5">
                                                </div>
                                                <div class="col-md-7">
                                                    <div id="divATB" class="text ml-1"><b>Amount to be Booked  </b><span class="ac-balance primary-color" id="amountToBeBook"></span></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-5 pb-2" id="divCard" style="display:none;">
                                            <div class="row payment-icons-manual">
                                                <div class="col-md-12">
                                                    <div class="input-group py-2">
                                                        <span class="input-group-addon pr-5"><span class="icon-credit-card-alt"></span></span>
                                                        <input type="text" class="form-control" placeholder="Card Number">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="input-group py-2">
                                                        <span class="input-group-addon pr-5"><span class="icon-calendar"></span></span>
                                                        <input type="text" class="form-control" placeholder="MM/YY">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="input-group py-2">
                                                        <span class="input-group-addon pr-5">

                                                        <span class="icon icon-check-circle"></span>

                                                    </span>
                                                    <input type="text" class="form-control" placeholder="CVV">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group py-2">
                                                    <span class="input-group-addon pr-5"><span class="icon-user"></span></span>
                                                    <input type="text" class="form-control" placeholder="Cardholder Name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                   
                                        <div class="col-md-12 mt-4">
                                            <div class="alert alert-danger small p-3" role="alert" id="ChkConfirmDiv" style="display:none">                                                
                                                <div class="chk_confirm_msg chk_err_msg py-1 font-weight-bold">Please check the Terms checkbox to continue</div>
                                                <div class="chk_addfee_msg chk_err_msg py-1 font-weight-bold">Please check Additional Fee</div>
                                            </div>

                                            <span class="custom-checkbox-style dark">  
                                                <input type="checkbox" id="ChkConfirm" class="checkbox" />
                                                
                                                <label  id="lblChkConfirm" for="ChkConfirm" style="text-transform: none;">I accept Cancellation Policy, Hotel Policy & Terms & Conditions</label><br />
                                                 <input type="checkbox" id="ChkAddFee" class="checkbox" />
                                                 <label  id="lblChkAddFee" for="ChkAddFee" style="text-transform: none;">A fee of AED20 in addition to the hotel cancellation charges applies to all hotel cancellations!</label>
                                           <label  id="lblSupplierPerformance"  style="text-transform: none;"><b>Supplier Performance: </b> I hereby acknowledge and consent that due to the nature of the platform and its connectivity to 3rd party service providers, some processes offered by the 3rd party providers are not under Travtrolley's control.
Furthermore, I accept that it is my responsibility upon signing up with 3rd party service providers to ensure their flows and processes are in compliance with my company's operational requirements. </label>
                                                </span>

                                        </div>
                                    <div class="col-md-12 mt-3">
                                      
                                        <input type="button" class="btn btn-primary btn-lg float-right" value="Confirm" onclick="ConfirmBooking()" />
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">

                                <h5 class="pt-3 pb-3 small-heading">YOUR CHOICE</h5>
                                <div class="ui-bg-wrapper">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <h4 class="pb-0" id="h4HotelId"></h4>
                                        </div>
                                        <div class="col-md-12"><small class="text-address pb-3" id="HotelAddress"></small></div>

                                        <div class="col-md-12">
                                            <div id="Rating">
                                            </div>
                                        </div>

                                        <%--For room summary--%>
                                        <ul class="ui-listing-wrapper list-unstyled" id="list-group-roomsummary">
                                        </ul>
                                        <%--End -  For room summary--%>
                                        <div class="col-md-12 mt-2">
                                            <div class="row hotel-bookingdtl-dateinfo">
                                                <div class="col-md-12">
                                                    <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-in</span>
                                                    <span class="float-right text-gray-dark text-weight-400" id="startDate"></span>
                                                </div>
                                                <div class="col-md-12">
                                                    <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-out</span>
                                                    <span class="float-right text-gray-dark text-weight-400" id="endDate"></span>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <h5 class="mt-3 pt-3 pb-3 small-heading">FARE SUMMARY</h5>
                                <div class="ui-bg-wrapper">
                                    <div class="fare-summary">
                                        <div class="row custom-gutter">
                                            <div class="col-5">
                                                <div class="form-group">
                                                    <label>ADD MARKUP</label>
                                                    <input type="text" placeholder="0" maxlength="3" name="addlMarkUp" id="addlMarkUp" class="form-control" onkeypress="return isNumber(event);">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label></label>
                                                    <select class="form-control" id="ddlAddlType">
                                                        <option value="P">Percentage</option>
                                                        <option value="F">Fixed</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group mt-4 pt-2">
                                                    <input type="button" class="btn btn-primary" value="ADD" onclick="CalculateAddlMarkUp()" />
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-condensed ui-price-table" id="Price-roomsummary">
                                        </table>
                                    </div>
                                </div>
                                 &nbsp&nbsp <label   style="text-transform: none;"><b>Note: Rates may not include all fees(like Tourism fee etc.,)</b></label><br />


                            </div>
                        </div>
                    </div>
                </div>





            </div>

        </div>
    </div>
    <div class="ui-bg-wrapper mb-1  ui-room-wrapper" style="display: none;" id="Adult">
        <div class="row">
            <div class="col-md-12">
                <hr class="dotted bold" />
            </div>
        </div>
        <div class="row custom-gutter">
            <div class="col-md-3">
                <span class="font-weight-bold label-title" id="h4Adult"></span>
            </div>
            <div class="col-md-6">
                <button type="button" id="btnSearchPaxA" class="btn btn-link float-right">Search Passenger<span class="glyphicon glyphicon-search"></span></button>
            </div>
            <div class="col-md-3">
                <span class="float-md-right room-type-title" id="RoomCount"></span>
            </div>
        </div>
        <div class="row custom-gutter">
            <div id="divPaxAddA" class="col-md-12">
                <span id="spnPaxAddA" style="float:right">Add Passenger</span><input type="checkbox" id="chkPaxAddA" style="float:right"/>
            </div>
        </div>

        <div class="row custom-gutter">
            <div class="form-group col-md-4" id="divAdultTitle">
                <%-- <label>Title</label>
                <select class="form-control custom-select" id="ddlAdultTitle">
<option value="">select Title</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                </select>--%>
            </div>
            <div class="form-group col-md-4">
                <label>First Name</label>
                <input type="text" class="form-control" id="txtAdultFirstName" placeholder="Enter First Name" onkeypress="return isAlphaNumeric(event);">
            </div>
            <div class="form-group col-md-4">
                <label>Last Name</label>
                <input type="text" class="form-control" id="txtAdultLastName" placeholder="Enter Last Name" onkeypress="return isAlphaNumeric(event);">
            </div>
        </div>
        <div class="row custom-gutter baggage-wrapper" style="display:none" id="divFlexA"></div>
    </div>
    <div class="ui-bg-wrapper mb-1  ui-room-wrapper" style="display: none;" id="Child">
        <div class="row custom-gutter">
            <div class="col-md-4">
                <span class="font-weight-bold label-title" id="h4Child"></span>
            </div>
            <div class="col-md-6">
                <button type="button" id="btnSearchPaxC" class="btn btn-link float-right">Search Passenger<span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
        <div class="row custom-gutter">
            <div id="divPaxAddC" class="col-md-12">
                <span id="spnPaxAddC" style="float:right">Add Passenger</span><input type="checkbox" id="chkPaxAddC" style="float:right"/>
            </div>
        </div>
        <div class="row custom-gutter">
            <div class="form-group col-md-4" id="divchild">
                <%--<label>Title</label>
                <select class="form-control custom-select" id="ddlChildTitle">
                    <option value="">select Title</option>
                    <option value="Master">Master</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                </select>--%>
            </div>
            <div class="form-group col-md-4">
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="Enter First Name" id="txtChildFirstName" onkeypress="return isAlphaNumeric(event);">
            </div>
            <div class="form-group col-md-4">
                <label>Last Name</label>
                <input type="text" class="form-control" placeholder="Enter Last Name" id="txtChildLastName" onkeypress="return isAlphaNumeric(event);">
            </div>
        </div>
        <div class="row custom-gutter  baggage-wrapper" style="display:none" id="divFlexC"></div>
    </div>

    <%--  template room summary--%>
    <div id="templateRoomSummary" style="display: none;">
        <div class="col-md-12 mt-2">
            <h5 id="h5RoomName"></h5>
        </div>
        <div class="col-md-12">
            <ul class="ui-hotel-addons-list">
                <li class="with-icon" id="mealPlanDesc"></li>
                <%--<li class="with-icon"  >Room only</li>
								<li class="with-icon">FREE Breakfast</li>
								<li class="with-icon">FREE Cancellation</li>--%>
            </ul>

        </div>
        <div class="col-md-12 mt-2">
            <div class="row hotel-bookingdtl-dateinfo">
                <div class="col-md-12">
                    <span class="float-left text-gray-light" id="roomGuests"><i class="icon icon-group"></i>Room Guests</span>
                    <span class="float-right text-gray-dark text-weight-400" id="roomGuestsCount">1 Room & 2 Adults</span>
                </div>
            </div>
        </div>
    </div>


    <div id="templateRoomPriceSummary" style="display: none;">
    </div>

    <!-- Search Passenger Modal -->
    <div id="SearchPassenger" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search Passenger</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                    <div id="SearchFilters" class="row custom-gutter"></div>
                    </div>
                </div>
                <div class="modal-footer"> <button type="button" class="button" onclick="SearchPassenger(); return false">Search</button> </div>
                <div id="PassengerDetailsList" class="px-3 table table-responsive"></div>
            </div>
        </div>
    </div>
    <!--Search Passenger Modal End-->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
