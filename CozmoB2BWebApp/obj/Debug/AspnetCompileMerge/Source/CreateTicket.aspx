<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CreateTicket" Codebehind="CreateTicket.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

 <%--   <script src="Scripts/jsBE/ModalPop.js" type="text/javascript"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <script src="yui/build/animation/animation-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script type="text/javascript">
        toolTip = new YAHOO.widget.Tooltip("Tooltip", {context:"GetETicketLink", text:"Gets E-Ticket detail from source, if generated.", showDelay:500 } );  
        toolTip1 = new YAHOO.widget.Tooltip("Tooltip1", {context:"ETicketButton", text:"Make E-Ticket", showDelay:500 } );
        toolTip2 = new YAHOO.widget.Tooltip("Tooltip2", {context:"CreateManualTicketLink", text:"Creates Ticket Manually", showDelay:500 } );
    </script>
    <script type="text/javascript">
        function StartTicket()
        {        
             var isPassed=true;        
             if(document.getElementById('corporateCodeBox'))
             {
                 var corpCode = document.getElementById('corporateCodeBox').value;
                 var tourCode = document.getElementById('tourCodeBox').value;
                 var endorsement = document.getElementById('endorsementBox').value;
                 var remarks = document.getElementById('remarksBox').value;
                 corpCodePattern = /^[a-zA-Z0-9]*$/;
                 if(!corpCodePattern.test(corpCode))
                 {
                    document.getElementById('ETicketMessage').style.color="Red";
                    document.getElementById('ETicketMessage').innerHTML = "Invalid Corporate code"
                    isPassed=false;        
                 }
                 if(!corpCodePattern.test(tourCode))
                 {
                    document.getElementById('ETicketMessage').style.color="Red";
                    document.getElementById('ETicketMessage').innerHTML = "Invalid Tour code"
                    isPassed=false;        
                 }
                 if(!corpCodePattern.test(endorsement))
                 {
                    document.getElementById('ETicketMessage').style.color="Red";
                    document.getElementById('ETicketMessage').innerHTML = "Invalid Endorsement code"
                    isPassed=false;        
                 }
             }
             if(isPassed)
             {
                document.getElementById('ETicketMessage').style.color="black";      
//                ModalPop.AcceptButton.disabled = true;
//                ModalPop.DenyButton.disabled = true;
                document.getElementById('ETicketMessage').innerHTML = "Generating E-Ticket ... ";
//                //document.getElementById('corporateCode').value=document.getElementById('corpCode').value;
//                document.getElementById('corporateCode').value=document.getElementById('corporateCodeBox').value;
//                document.getElementById('tourCode').value=document.getElementById('tourCodeBox').value;
//                document.getElementById('endorsement').value=document.getElementById('endorsementBox').value;
//                document.getElementById('remarks').value=document.getElementById('remarksBox').value;
//                //$('ticket').submit();
                var url = 'AutoTicket.aspx?flightId=' + <%=itinerary.FlightId %>;
//                url = url + '&corporateCode=' + document.getElementById('corporateCode').value;
//                url = url + '&tourCode=' + document.getElementById('tourCode').value ;
//                url = url + '&endorsement=' + document.getElementById('endorsement').value;
//                url = url + '&remarks=' + document.getElementById('remarks').value;
                window.location.href = url;
             }  
        }
//        function GenerateTicket()
//        {
//        document.getElementById('popupbox').style.display='block';
//            ModalPop.AcceptButton.onclick = StartTicket;
//            var modalMessage="";

//            modalMessage += "<div id=\"ETicketMessage\" style=\"margin-top:15px\"></div>";
//            ModalPop.Show({x: 300, y: 300}, modalMessage)
//        }
                     
    </script>--%>
    <asp:HiddenField ID="hdfFlightId" runat="server" />
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="PrimaryView" runat="server">
  <table width="98%" align="right" border="0" cellspacing="3" cellpadding="0">
  <tr height="50px">
  <td width="50%">
  </td>
  <td width="50%">
  </td>
  </tr>
  
 <tr>
 <td>
  <div class="fleft font-size-16 fleft"> Agency Name: 
                <asp:Label ID="lblAgeny" Font-Bold="true" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblAgentName" Font-Bold="true" runat="server" Text=""></asp:Label>
                   
                </div>
 </td>
 <td>
  <div class="fright">
               <%-- <asp:Image ID="imgAlert" runat="server" ImageUrl="~/images/alert.gif" Visible="false" />--%>
                    PNR:
                            <asp:Label ID="lblPNRNo" runat="server" Font-Bold="true" Text=""></asp:Label>
                        </span>
                    <asp:Label ID="lblAlertmsg" CssClass="fleft width-100" runat="server"></asp:Label>
                </div>
 </td>
 </tr>
 <tr>
 <td>
  <div class="fleft">
                    Booking Date: <span class="bold">
                    <asp:Label ID="lblBkgDate" runat="server" Text=""></asp:Label> &nbsp;&nbsp;
&nbsp;                        
                    </span>
                </div>
                <div class="margin-left-50 fleft">
                    Travel Date: <span class="bold">
                    <asp:Label ID="lblTravelDate" runat="server" Text=""></asp:Label>
                        
                    </span>
                </div>
 </td>
 </tr>
 <tr>
<td> &nbsp;</td>
 </tr>
 <tr>
 <td>
 <table width="50%"  style="border:solid 1px #ccc;" >
 <tr>
 <td>
  <div class="ns-h3" > Passenger Details </div>         
 <div>
 <%  
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (IsTicketed(itinerary.Passenger[i].PaxId, ticketList))
                        {
                            continue;
                        }
                %>                
                <div class="clear">
                    <div class="width-250 fleft margin-top-10">
                    <%string title = string.Empty;
                      if (itinerary.Passenger[i].Title != null)
                      {
                          title = itinerary.Passenger[i].Title + " ";
                      } %>
                        <% = title + itinerary.Passenger[i].FirstName%>
                        <% = itinerary.Passenger[i].LastName %>
                    </div>
                    <div class="fleft">
                        <form method="post" name="paxForm<% = i %>" action="ManualTicket.aspx">
                            <input name="bookingId" type="hidden" value="<% = bookingId %>" />
                            <input name="paxId" type="hidden" value="<% = itinerary.Passenger[i].PaxId %>" />
                            <%--<a id="CreateManualTicketLink" href="javascript:document.paxForm<% = i %>.submit();">Create Manual Ticket</a>--%>
                        </form>
                    </div>
                    <% if (eTicketEligible == true) { %>
                    <div class="fleft" style="margin-left:10px;">
                        <form method="post" name="paxForm1<% = i %>" action="ManualTicket.aspx?eticketMode=true">
                            <input name="bookingId" type="hidden" value="<% = bookingId %>" />
                            <input name="pnrNo" type="hidden" value="<% = itinerary.PNR %>" />
                            <input name="paxId" type="hidden" value="<% = itinerary.Passenger[i].PaxId %>" />
                            <%  %>
                            <%--<a id="GetETicketLink" href="javascript:document.paxForm1<% = i %>.submit();" >Get E-Ticket detail</a>--%>
                        </form>
                    </div>
                    <% } %>
                </div>
                <%  }   %>
        
        </div>
 </td>
 </tr>
 </table>

 </td>
 </tr>
 <tr>
 <td> &nbsp;</td>
 </tr>
 <tr>
 <td align="center">
   <div class="fleft padding-3 creating-ticket-child margin-top-20 border-bottom-black padding-left-10">
               <% if (eTicketEligible && !ticketed && airlineEligible && ticketList.Count == 0) 
               {
                  // if ((isDomestic && Settings.LoginInfo.MemberType == MemberType.ADMIN) || (isDomestic == false && Settings.LoginInfo.MemberType == MemberType.ADMIN))
                  //{ %> 
                <div class="center">
                    <%--<form id="ticket" method="post" action="AutoTicket.aspx">--%>
                        <input name="flightId" type="hidden" value="<% = itinerary.FlightId %>" />
                        <input name="tourCode" type="hidden" id="tourCode"/>
                        <input name="endorsement" type="hidden" id="endorsement"/>
                        <input name="remarks" type="hidden" id="remarks"/>
                        <input name="corporateCode" type="hidden" id="corporateCode"/>                
                        <%--<input id="ETicketButton" onclick="StartTicket()" type="button" value=" Generate E-Ticket " />--%>
                        <asp:Button ID="btnGenerateTicket" runat="server" Text="Generate E-Ticket" 
                        onclick="btnGenerateTicket_Click" />
                    <%--</form>--%>
                <%--<a class="black-links" href="AutoTicket.aspx?pnr=<% = itinerary.PNR %>">Generate E-Ticket</a>--%>
                </div>
              <% //} 
                  } 
                  %>
               
            </div>
 </td>
 </tr>
 
 <tr>
 <td>
  <%--<div id="ETicketMessage" style="margin-top:15px"></div>--%>
  <asp:Label ID="lblETicketMsg" runat="server" Text=""></asp:Label>
  
 </td>
 </tr>
  </table>
  </asp:View>
  <asp:View ID="SecondaryView" runat="server">
    <%if (errorMessage.Length >0)
      { %>
    <div class="width-720 margin-left-30" style="text-align: center">
        <br />
        <br />
        <br />
        <span>
            <% = errorMessage%>
        </span>
        <br />
        <br />
       <%-- <%if (response.Status > 0 && response.Status != CT.BookingEngine.TicketingResponseStatus.NotSaved)
          { %>--%>
        <span>Go to <a href="AGENTBOOKINGQUEUE.ASPX">Booking Queue</a> or <a href="ViewBookingForTicket.aspx?bookingId=<% = booking.BookingId %>">
            View Booking</a>
            <% //TODO: Navigation links here. %>
        </span>
      <%--  <%}
          else if (response.Status > 0)
          { %>--%>
       <%-- <span>Go to <a href="FailedBookingQueue.aspx">Pending Queue</a>--%>
            <% //TODO: Navigation links here. %>
       <%-- </span>--%>
       <%-- <%} %>--%>
        <br />
        <br />
        <span>For further assistance contact
            <%= CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"]%>. </span>
        <br />
        <br />
    </div>
    <%} %>
    </asp:View>
        </asp:MultiView>
        <div class="fleft margin-top-10 form-parent-width" style="margin-left:200px; margin-top:150px;">
            <div class="rules-parent-width margin-left-40">
                <span class="font-red">
                <asp:Label ID="lblErrMsg" runat="server" Text=""></asp:Label>
                </span>
            </div>
        
            <div class="rules-parent-width margin-left-40">
                <span>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/alert.gif" Visible="false" AlternateText="Alert" /></span> 
                    <span class="margin-left-15">
                    <asp:Label ID="lblAltMsg" runat="server" Text= ""></asp:Label>
                    </span>
            </div>
       
          
        </div>
        <%  if (ticketList.Count > 0)
            {   %>
        <div class="fleft margin-top-10 locked-pnr-parent-width dotted-border padding" style="text-align:center">
            <%  //TODO: add Contract title %>
            
            <div class="fleft bold">
                <span class="fleft width-100px">Ticket No</span>
                <span class="fleft width-130 margin-left-10">Passenger</span>
                <span class="fleft width-120 margin-left-10">Routing</span>
                <span class="fleft width-70 margin-left-10">Fare Type</span>
                <span class="fleft width-70 margin-left-10">Airline</span>
                <span class="fleft width-70 margin-left-10">Status</span>
            </div>
            <%  int counter = 0;
                foreach (Ticket ticket in ticketList)
                {   %>
            <div class="fleft margin-top-15">
                <span class="fleft width-100px"><%=ticket.ValidatingAriline%><% = ticket.TicketNumber%></span>
                <%string title = string.Empty;
                  if (ticket.Title != null)
                  {
                      title = ticket.Title + " ";
                  } %>
                <span class="fleft width-130 margin-left-10"><% = title + ticket.PaxFirstName%> <% = ticket.PaxLastName%></span> 
                <span class="fleft width-120 margin-left-10"><% = routing%></span>
                <span class="fleft width-70 margin-left-10"><% = itinerary.FareType%></span>
                <span class="fleft width-70 margin-left-10"><% = ticket.ValidatingAriline%></span>
                <span class="fleft width-70 margin-left-10"><% = ticket.Status%></span>
                <div class="fleft width-70 margin-left-10">
                    <form id="Edit<% = ticket.TicketId %>" action="ManualTicket.aspx" method="post">                       
                        <input name="eticketMode" type="hidden" value="<%=ticket.ETicket.ToString().ToLower() %>" />                                                 
                        <input name="pnrNo" type="hidden" value="<% = itinerary.PNR %>" />                          
                        <input name="bookingId" type="hidden" value="<% = bookingId %>" />
                        <input name="paxId" type="hidden" value="<% = ticket.PaxId %>" />
                        <input name="ticketId" type="hidden" value="<% = ticket.TicketId %>" />
                        <input name="ticketStatus" type="hidden" value="<%=ticket.Status %>" />
                        <input name="editMode" type="hidden" value="true" />
                        <a href="javascript:document.getElementById('Edit<% = ticket.TicketId %>').submit();">Edit</a>
                        <a href="ETicket.aspx?ticketId=<%=ticket.TicketId%>" target="_blank">View</a>
                    </form>
                </div>
            </div>
            <%      counter++;
                } %>
        </div>        
        <%  }
            if (ticketed)
            { %>
        <div class="fleft margin-top-15 width-315">
        
            <form action="CreateInvoice.aspx" method="post">
                <div>
                    Remarks</div>
                <div class="margin-top-5 width-300 padding-left-10">
                    <textarea name="remarks" cols="30" rows="3" style="width: 300px; height: 50px;"></textarea>
                </div>
                
                <div class="fright margin-top-10">
                    <div class="margin-left-10">
                        <input name="flightId" type="hidden" value="<% = itinerary.FlightId %>" />
                        <input name="agencyId" type="hidden" value="<% = booking.AgencyId %>" />
                        <input name="pnr" type="hidden" value="<% = itinerary.PNR %>" />
                        <input name="routing" type="hidden" value="<% = routing %>" />
                        <%if (Invoice.isInvoiceGenerated(ticketList[0].TicketId) > 0)
                          { %>
                            <input type="submit" value=" View Invoice " />
                          <%}
                          else
                          { %>
                            <input type="submit" value=" Generate Invoice " />
                        <%} %>
                    </div>
                
                </div>
            </form>
        </div>
        <%  } %>
   
   







    <!-- Modified by Lokesh & Firoz on 18-June-2018-->
    <!-- New Design for E-ticket in Email-->

        <div id='EmailDiv' runat='server' style='width: 600px; display: none;'>
        <%if (isBookingSuccess)
          {
              List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

              if (ticketList != null && ticketList.Count > 0)
              {
                  ptcDetails = ticketList[0].PtcDetail;
              }
              else// For Hold Bookings
              {
                  ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
              }

              %>
             
             
             
            
      
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><h2>e-Ticket</h2></td>
                </tr>
              <%--<tr style="display:none;">
                <td>Trip ID -  <%if(!string.IsNullOrEmpty(flightItinerary.TripId)){ %>
                                                             
                              
                                        <%=flightItinerary.TripId %>
                                      
                                        <%} %>
                                        


                                        <%=booking.BookingId%>
                                        
                                        </td>
                </tr>--%>
              <tr>
                <td> 
                
                
                
                
                                          
                                                                            <!--Always show booking date only --> 
                    <%if (flightItinerary != null && flightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=flightItinerary.CreatedOn.ToString("ddd") + "," + flightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>
                                               
                
                </td>
                </tr>
              </table></td>
            <td align="right" valign="top">
            

            <asp:Image Width='159px' Height='51px' ID='imgLogo' runat='server'  />

           

      <%-- <img src="<%=Request.Url.Scheme%>://cozmotravel.com/assets/images/main-logo.svg" alt="#" height="70">--%>
            
            
            </td>
            </tr>

          <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>
          <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>

          <tr>
                                                      
                                                      <td colspan="2" align="right">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td rowspan="3" valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h2>Reservation Details </h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><strong>PNR No.  <%=flightItinerary.PNR%></strong></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <h2>&nbsp;</h2>
                                                                        </td>
                                                                        <td align="right">
                                                                            <table id="tblPrintEmailActions" width="200" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                        <td>
                                                                                                            </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Agent Name: <%=agency.Name %></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Phone: <%=agency.Phone1 %></strong></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                   
                                                    </tr>

          </table>

      
      
    

      
    

       
        <% 
              for (int count = 0; count < flightItinerary.Segments.Length; count++)
           {
               int paxIndex = 0;
               if (Request["paxId"] != null)
               {
                   paxIndex = Convert.ToInt32(Request["paxId"]);
               }

               List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
               ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
               %>
     
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            
            

            
 
          
         
<tr> 

<td height="20"> </td>


</tr>



          

<tr> 
<td>

    <%if(count == 0) %>
    <%{ %>


<table width="100%" border="1" cellspacing="0" cellpadding="0">

     <%--passanger header row  starts --%>
     <tr>
         
        <td align="center" valign="middle"><strong>Passenger Name</strong></td>
        <td align="center" valign="middle"><strong>E-Ticket Number</strong></td>
        <td align="center" valign="middle"><strong>Baggage</strong></td>
        <td align="center" valign="middle"><strong>Airline Ref.</strong></td>
          
        </tr> 

    <%--passanger row starts--%>
   
   
   
    <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
        { %> 
      <tr>
        <td align="center" valign="top"> <strong>
                                                    <%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%>
                                                    </strong> </td>
        <td align="center" valign="top"> 
        
             <%if (ticketList != null && ticketList.Count > 0) { %>
                                           <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                            <%} else { //for Corporate HOLD Booking%>
                                        <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%>
                                            <%} %>
        
        </td>
        <td align="center" valign="top">
        
       <%if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(flightItinerary.FlightId,flightItinerary.Passenger[j].Type)%>
    <%}              
    else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.PKFares ||flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp )
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && flightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (flightItinerary.FlightBookingSource == BookingSource.Amadeus && flightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = flightItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %>
         </td>
        <td align="center" valign="top">
        
        
       <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%>
            <%if (!string.IsNullOrEmpty(flightItinerary.TripId)) { %>
                                        <td style='width:50%;text-align:right'>                                        
                                       
                                       Corporate Booking Code- <%=flightItinerary.TripId %>
                                        </td>
                                        <%} %>
        
        </td>
        </tr>
    
    
    <%} %>

    
      </table>

<%} %>

 </td>

</tr>

 


           
<%--passanger row ends--%>


<tr> 
  
  <td height="20"> </td> 
  
</tr>


<tr> 


<td>

<table width="100%" border="1" style="border-collapse: collapse;" cellspacing="0" cellpadding="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="border-right: solid 1px #ccc;" valign="top"><table style="margin:auto" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding-top:5px" align="center">

                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(flightItinerary.Segments[count].Airline);%>
                                   <img src="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="#" />
                                    <%}
                                      catch { } %>
                  
                  </td>
                </tr>
              <tr>
                <td align="center"> <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(flightItinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label> </td>
                </tr>
              <tr>
                <td align="center"><strong> <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Airline + " " + flightItinerary.Segments[count].FlightNumber%>
                                     <%try
                                       {  //Loading Operating Airline and showing
                                           if ((!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)||(flightItinerary.FlightBookingSource == BookingSource.Indigo && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0 && flightItinerary.Segments[count].OperatingCarrier != "6E"))//Modified for Indigo CodeShare
                                           {
                                               string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                               CT.Core.Airline opAirline = new CT.Core.Airline();
                                               if (opCarrier.Split('|').Length > 1)
                                               {
                                                   opAirline.Load(opCarrier.Split('|')[0]);
                                               }
                                               else
                                               {
                                                   opAirline.Load(opCarrier.Substring(0, 2));
                                               } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                       }
                                       catch { } %>
                                </label></strong></td>
                </tr>
              </table></td>
            <td align="center" valign="top"><table style="margin:auto" width="94%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><i> Departure </i></td>
                    </tr>
                  <tr>
                    <td><strong><%=flightItinerary.Segments[count].Origin.CityName + ", " + flightItinerary.Segments[count].Origin.CountryName%></strong></td>
                    </tr>
                  <tr>
                    <td>
                    
                    <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%>
                    
                    
                     <%=flightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%> 
                     
                     
                     </td>
                    </tr>
                  <tr>
                    <td><%=flightItinerary.Segments[count].Origin.AirportName + ",Terminal-" + flightItinerary.Segments[count].DepTerminal%> </td>
                  </tr>
                  
                  </table>
                  </td>
                <td width="20"></td>
                <td>
                
               
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><i> Arrival </i></td>
                    </tr>
                  <tr>
                    <td><strong><%=flightItinerary.Segments[count].Destination.CityName + ", " + flightItinerary.Segments[count].Destination.CountryName%></strong></td>
                    </tr>
                  <tr>
                    <td> 

                

                    
                    <%=flightItinerary.Segments[count].ArrivalTime.ToString("MMM dd yyyy (ddd)")%>
                    <%=flightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%>
                    
                    
                     </td>

                    </tr>
                  <tr>
                    <td><%=flightItinerary.Segments[count].Destination.AirportName + ",Terminal-" + flightItinerary.Segments[count].ArrTerminal%> </td>
                  </tr>
                  
                  </table>
                  
                  
                  </td>
                </tr>
              </table></td>
            <td style="border-left: solid 1px #ccc;" valign="top">
            
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">
                    
                    <%if (flightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %>
                    
                   </td>
                </tr>
              <tr>
                <td align="center"> <%=flightItinerary.Segments[count].Duration%> </td>
                </tr>
              <tr>
                <td align="center">
                    
                 <%if (!string.IsNullOrEmpty(flightItinerary.Segments[count].CabinClass)) %>
                    <%{ %>
                    Class: <%=flightItinerary.Segments[count].CabinClass%>
                    <%} %>
                </td>
                </tr>
              <tr>
                <td style="color:#66ca1d" align="center"><%=booking.Status.ToString()%></td>
                </tr>

                
<%--                                    <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                                      {%>                                    
                                      <label style='padding-left: 20px'><label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                        width: 25%; '>Fare Type : </label> <%=flightItinerary.Segments[count].SegmentFareType%></label>                                                                             
                                      <%} %>--%>





              </table>
              
              
              
              
              
              
              </td>
            </tr>
          </table></td>
      </tr>
      </table>


 </td>

</tr>


           
            
            
            
            
            
            
            
            
            <%} %>
        </table>



       
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0;

                        if (ticketList != null && ticketList.Count > 0)
                        {
                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                                }
                                Baggage += ticketList[k].Price.BaggageCharge;
                                MarkUp += ticketList[k].Price.Markup;
                                Discount += ticketList[k].Price.Discount;
                                AsvAmount += ticketList[k].Price.AsvAmount;
                                outputVAT += ticketList[k].Price.OutputVATAmount;
                            }
                            if (ticketList[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticketList[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < flightItinerary.Passenger.Length; k++)
                            {
                                AirFare += flightItinerary.Passenger[k].Price.PublishedFare + flightItinerary.Passenger[k].Price.HandlingFeeAmount;
                                Taxes += flightItinerary.Passenger[k].Price.Tax + flightItinerary.Passenger[k].Price.Markup;
                                if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += flightItinerary.Passenger[k].Price.AdditionalTxnFee + flightItinerary.Passenger[k].Price.OtherCharges + flightItinerary.Passenger[k].Price.SServiceFee + flightItinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += flightItinerary.Passenger[k].Price.BaggageCharge;
                                MarkUp += flightItinerary.Passenger[k].Price.Markup;
                                Discount += flightItinerary.Passenger[k].Price.Discount;
                                AsvAmount += flightItinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += flightItinerary.Passenger[k].Price.OutputVATAmount;
                                if (flightItinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (flightItinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                    %>
                     <% if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId<=1)
                              {
                                  
                                  %>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            
                                            <td>
                                                <table style=" float:right" width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Taxes.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || flightItinerary.IsLCC ||flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp|| flightItinerary.FlightBookingSource == BookingSource.IndigoCorp )
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                  <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                            { %>
                                                                <b>Total GST</b>
                                                                <%}else
                                                                    { %>
                                                                <b>VAT</b>
                                                                <%} %>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=outputVAT.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td  style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                   <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                       { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
    else
    { %>
                                                                     <%=((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                     <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                      <%} %>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>

        <%} %>

       

       
       
        <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary and Unless ticket number is shown on the ticket copy.Ticket copy should not be handed over to customer.</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>
   
   
    </div>
  
  
    <!--Email sending code for Corporate Profile -->




</asp:Content>
