﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="FlightCommissionReportGUI" Title="Flight Commission Report" Codebehind="FlightCommissionReport.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:100px" align="right"><a style="cursor:Hand;font-weight:bold;font-size:8pt;color:Black" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a> </td>
                <td width="800px" align="right">
        </td>
          </tr>
         </table>
              <div class="grdScrlTrans" style="margin-top:-1px;">
    <div class="paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
       
       
       
             <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblFromDate" Text="From Date:" runat="server" ></asp:Label></div>
   
    <div class="col-md-2"> <uc1:DateControl Id="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>
    
    <div class="col-md-2"> <asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"><uc1:DateControl Id="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
   
    <div class="col-md-2"> <asp:Label ID="lblAgent" runat="server" Visible="true"  Text="Agent:"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlAgent"  CssClass="inputDdlEnabled form-control" Visible="true"  runat="server" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
     <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblB2BAgent" runat="server" Visible="true"  Text="B2BAgent:"></asp:Label></div>
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
    <div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" runat="server" Visible="true"  Text="B2B2BAgent:"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" ></asp:DropDownList> </div>
    <div class="col-md-2"> <asp:Label ID="lblSource" runat="server" Text="Source:"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlSource" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblAcctStatus" runat="server" Text="Accounted Status:"></asp:Label></div>
    
    <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlAcctStatus" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                         <asp:ListItem  Value="1" Text="YES"></asp:ListItem>
                                          <asp:ListItem  Value="0" Text="NO"></asp:ListItem>
                                    </asp:DropDownList></div>
    <div class="col-md-2"><asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label> </div>
    
     <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server" Visible="false">
                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="B2B" Text="B2B"></asp:ListItem>
                           <asp:ListItem  Value="B2C" Text="B2C"></asp:ListItem>
                    </asp:DropDownList></div>
                    
                    
   
     <div class="col-md-4"><asp:Button runat="server" ID="btnSearch" Text="Search"  OnClientClick="return ValidateParam();" 
     CssClass="btn but_b pull-right" OnClick="btnSearch_Click" /> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
    
    
       
        
        </asp:Panel>
    </div>
    <table width="100%"  id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td>
        <asp:GridView ID="gvFlightCommReport" Width="100%" runat="server" AllowPaging="true"
            DataKeyNames="ticketId" EmptyDataText="No Flight List!" AutoGenerateColumns="false"
            PageSize="19" GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0"
            OnPageIndexChanging="gvFlightCommReport_PageIndexChanging">
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
            
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label"
                            ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAgentCode" HeaderText="Agent Code" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentCode" runat="server" Text='<%# Eval("agent_code") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("agent_code") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAgentName" HeaderText="Agent Name" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentName" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("agent_name") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
               
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="HTtxtBookingDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="HTbtnBookingDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Booking.&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblBookingDate" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("createdOn")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("createdOn") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPnr" HeaderText="PNR" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPnr" runat="server" Text='<%#Eval("pnr")%>'
                            CssClass="label grdof" ToolTip='<%# Eval("pnr")%>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtTicketNo" HeaderText="Ticket NO" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblTicketNo" runat="server" Text='<%# Eval("ticketNumber") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("ticketNumber") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtSource" HeaderText="Source" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("Supplier") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("Supplier") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="HTtxtTravelDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="HTbtnTravelDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Travel&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblTravelDate" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("travelDate")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("travelDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAirline" HeaderText="Airline" CssClass="inputEnabled" Width="80px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAirline" runat="server" Text='<%# Eval("airlineCode") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("airlineCode") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtClass" HeaderText="Class" CssClass="inputEnabled" Width="70px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblClass" runat="server" Text='<%# Eval("class") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("class") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPassengerName" HeaderText="Passenger Name" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPassengerName" runat="server" Text='<%# Eval("paxfullName") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("paxfullName") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                   <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtRouting" HeaderText="Routing" Width="200px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblRouting" runat="server" Text='<%# Eval("routing") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("routing") %>' Width="200px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtStatus" HeaderText="Status" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("status") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("status") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtFare" HeaderText="Fare" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblFare" runat="server" Text='<%# CTCurrencyFormat(Eval("Fare"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Fare") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtTax" HeaderText="Tax" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblTax" runat="server" Text='<%# CTCurrencyFormat(Eval("tax"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("tax") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPayableAmount" HeaderText="Payable Amount" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPayableAmount" runat="server" Text='<%# CTCurrencyFormat(Eval("Payableamount"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PayableAmount") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                   <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtProfit" HeaderText="Profit" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblProfit" runat="server" Text='<%# CTCurrencyFormat(Eval("profit"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("profit") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                   <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtDiscount" HeaderText="Discount" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblDiscount" runat="server" Text='<%# CTCurrencyFormat(Eval("Discount"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Discount") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                 <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtInvoiceAmount" HeaderText="Invoice Amount" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblInvoiceAmount" runat="server" Text='<%# CTCurrencyFormat(Eval("TotalAmount"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("TotalAmount") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                   <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAgentSF" HeaderText="AgentSF" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentSF" runat="server" Text='<%# CTCurrencyFormat(Eval("AgentSF"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("AgentSF") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtMarkupType" HeaderText="Markup Type" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("Markup_Type") %>' CssClass="label grdof"
                            ToolTip='<%#Eval("Markup_Type")%>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtOurCommValue" HeaderText="Our Commission Value" Width="130px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblOurCommValue" runat="server" Text='<%# CTCurrencyFormat(Eval("Our_Commission_Value"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("Our_Commission_Value"),Eval("agent_decimal")) %>' Width="130px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                   <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtOurComm" HeaderText="Our Commission" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblOurComm" runat="server" Text='<%# CTCurrencyFormat(Eval("Our_Commision"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("Our_Commision"),Eval("agent_decimal")) %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                 <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAgentCommValue" HeaderText="Agent Commission value" Width="140px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" /> 
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentCommValue" runat="server" Text='<%# CTCurrencyFormat(Eval("Agent_Markup_Value"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("Agent_Markup_Value"),Eval("agent_decimal")) %>' Width="140px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAgentComm" HeaderText="Agent Commission" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentComm" runat="server" Text='<%# CTCurrencyFormat(Eval("Agent_Markup"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("Agent_Markup"),Eval("agent_decimal")) %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtInvoiceNo" HeaderText="Invoice No" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblInvoiceNo" runat="server" Text='<%# Eval("InvoiceNo") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("InvoiceNo") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtIsAccounted" HeaderText="IsAccounted" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblIsAccounted" runat="server" Text='<%# Eval("AccountedStatus") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("AccountedStatus") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtLocationName" HeaderText="Location Name" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblLocationName" runat="server" Text='<%# Eval("LocationName") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("LocationName") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtUserName" HeaderText="User Name" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblUserName" runat="server" Text='<%# Eval("UserName") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("UserName") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </td></tr>
    </table>
                  <table>
                      <tr>
                          <%--<td width="120px">
                              <asp:Button OnClick="btnUpdateStatus_Click" runat="server" ID="btnUpdateStatus" Text="Update Status"
                                  CssClass="button" />
                          </td>--%>
                          <td width="20px">
                          </td>
                          <td width="80px">
                              <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                                  CssClass="button" />
                          </td>
                          <td>
                          </td>
                      </tr>
                  </table>
  </div>
   <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<asp:Label style="COLOR: #dd1f10" id="lblError" runat="server"></asp:Label> 
<div>
    <asp:DataGrid ID="dgFlightCommReportList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="Agent Code" DataField="agent_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Agent Name" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Booking Date" DataField="createdOn" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="PNR" DataField="pnr" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Ticket NO" DataField="ticketNumber" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Source" DataField="Supplier" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Travel Date" DataField="travelDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Airline" DataField="airlineCode" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Class" DataField="class" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Passenger Name" DataField="paxfullName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Routing" DataField="routing" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Status" DataField="status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Fare" DataField="Fare" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Tax" DataField="tax" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Payable Amount" DataField="Payableamount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice Amount" DataField="TotalAmount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="profit" DataField="profit" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Discount" DataField="Discount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="AgentSF" DataField="AgentSF" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Markup Type" DataField="Markup_Type" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Our Commission Value" DataField="Our_Commission_Value" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Our Commission" DataField="Our_Commision" HeaderStyle-Font-Bold="true"></asp:BoundColumn> 
      <asp:BoundColumn HeaderText="Agent Commission Value" DataField="Agent_Markup_Value" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Agent Commission" DataField="Agent_Markup" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice No" DataField="InvoiceNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="AcctStatus" DataField="AccountedStatus" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Location Name" DataField="LocationName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="User Name" DataField="UserName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
    </asp:DataGrid>
    </div>
  <script  type="text/javascript">

function ShowHide(div)
{
    if(getElement('hdfParam').value=='1')
        {
            document.getElementById('ancParam').innerHTML='Show Param'
            document.getElementById(div).style.display='none';
            getElement('hdfParam').value='0';
        }
        else
        {
            document.getElementById('ancParam').innerHTML='Hide Param'
           document.getElementById('ancParam').value='Hide Param'
            document.getElementById(div).style.display='block';
            getElement('hdfParam').value='1';
        }
}
function ValidateParam()
{
     clearMessage();
    var fromDate=GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
    var fromTime=getElement('dcFromDate_Time');
    var toDate=GetDateTimeObject('ctl00_cphTransaction_dcToDate');
    var toTime=getElement('dcToDate_Time');
    if(fromDate==null) addMessage('Please select From Date !','');
    //alert(fromTime);
    if(fromTime.value=='') addMessage('Please select From Time!','');
    if(toDate==null) addMessage('Please select To Date !','');
    if(toTime.value=='') addMessage('Please select To Time!','');
    if((fromDate!=null && toDate!=null) && fromDate>toDate) addMessage('From Date should not be later than To Date!','');
    if(getMessage()!=''){ 
    alert(getMessage()); 
    clearMessage(); return false; }
}

 
 function getElement(id)
 {
    return document.getElementById('ctl00_cphTransaction_'+id);
 }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

