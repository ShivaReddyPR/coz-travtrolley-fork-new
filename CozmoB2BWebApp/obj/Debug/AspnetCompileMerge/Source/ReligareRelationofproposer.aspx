﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareRelationofproposer.aspx.cs" Inherits="CozmoB2BWebApp.ReligareRelationofproposer" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
 <%--<%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
   <div><h3>Religare Relation Of Proposer Master</h3></div>
         <div class="body_container" style="height:200px">
        <div class="col-md-12">

           
            <div class="col-md-2">
                <asp:Label ID="lblCode" runat="server" Text="Relation Code:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtRelationCode" runat="server"></asp:TextBox>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblRelation" runat="server" Text="Relation:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtRelation" runat="server"></asp:TextBox>
            </div>

            <br />

        <div class="col-md-12">
            <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
            </div>

        </div>
        <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <script type="text/javascript">
        function Save() {
            if (getElement('txtRelationCode').value == '') addMessage('Relation code cannot be blank!', '');
            if (getElement('txtRelation').value == '') addMessage('Relation cannot be blank!', '');
            

        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
    </script>
    <asp:HiddenField runat="server" ID="hdfMid" Value="0" />
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 
<div class=""> 
     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="RELATION_ID"
      emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="5"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">

     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("CODE") %>' CssClass="label grdof" ToolTip='<%# Eval("CODE") %>'></asp:Label>
   
    </ItemTemplate>  
              <HeaderTemplate>
                 Code
             </HeaderTemplate>
         </asp:TemplateField>

         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblRelation" runat="server" Text='<%# Eval("RELATION") %>' CssClass="label grdof" ToolTip='<%# Eval("RELATION") %>' ></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Relation
             </HeaderTemplate>
         </asp:TemplateField>

     </Columns>

     </asp:GridView>
     
</div>
    </asp:Content>



