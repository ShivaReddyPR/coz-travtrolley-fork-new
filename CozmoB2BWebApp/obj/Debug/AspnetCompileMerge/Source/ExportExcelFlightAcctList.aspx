﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ExportExcelFlightAcctListGUI" Codebehind="ExportExcelFlightAcctList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flight Account Report</title>
</head>
<body>
    <form id="form1" runat="server">
       <div>
    <asp:DataGrid ID="dgFlightAcctReportList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="Agent Code" DataField="agent_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Agent Name" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Booking Date" DataField="createdOn" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="PNR" DataField="pnr" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Ticket NO" DataField="ticketNumber" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Source" DataField="Supplier" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Travel Date" DataField="travelDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Airline" DataField="airlineCode" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Class" DataField="class" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Passenger Name" DataField="paxfullName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Routing" DataField="routing" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Status" DataField="status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Fare" DataField="Fare" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Tax" DataField="tax" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Payable Amount" DataField="Payableamount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice Amount" DataField="TotalAmount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="profit" DataField="profit" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Discount" DataField="Discount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="AgentSF" DataField="AgentSF" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice No" DataField="InvoiceNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="AcctStatus" DataField="AccountedStatus" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
    </asp:DataGrid>
    </div>
    </form>
</body>
</html>
