﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FleetBookingConfirm" Title="Fleet Booking Confirm" Codebehind="FleetBookingConfirm.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">
    if (history.length > 1) {
        history.forward(1);
    }
    function validateTerms() {
        document.getElementById('MainDiv').style.display = "block";
        document.getElementById('PreLoader').style.display = "none";
        if (document.getElementById('chkTerms').checked == true && document.getElementById('chkCancel').checked == true) {
            document.getElementById('validate').style.display = "none";
            document.getElementById('divCancelValidate').style.display = "none";
            if (document.getElementById('<%=hdnWarning.ClientID %>').value.length <= 0) {
                document.getElementById('MainDiv').style.display = "none";
                document.getElementById('PreLoader').style.display = "block";
            }
            history.forward(1);
            return true;
        }
        else if (document.getElementById('chkTerms').checked == false && document.getElementById('chkCancel').checked == false) {
            document.getElementById('validate').style.display = "block";
            document.getElementById('divCancelValidate').style.display = "block";
            return false;
        }
        else if (document.getElementById('chkTerms').checked == false) {
            document.getElementById('validate').style.display = "block";
            document.getElementById('divCancelValidate').style.display = "none";
            return false
        }
        else if (document.getElementById('chkCancel').checked == false) {
            document.getElementById('validate').style.display = "none";
            document.getElementById('divCancelValidate').style.display = "block";
            return false
        }
    }
</script>
<asp:HiddenField ID="hdnWarning" runat="server" />
<div id="MainDiv" class="mar-top-10">
        <div class="col-md-3 bg_white padding-0">
        <div class="">
                    <div class="ns-h3">
                        Pricing Details</div>
                 
                    <div style="padding-bottom: 5px;">
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    Car Rental Charges
                                </td>
                                <td>
                                   <%=(itinerary.Currency)%>
                                <%=total%>
                                    
                                </td>
                            </tr>
                                     <tr>
                            <td>
                                <strong class="spnred">Total Inc. Markup</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=(itinerary.Currency)%>
                                    <%=(markup > 0 ? (markup).ToString("N" + Settings.LoginInfo.DecimalValue) : (total).ToString("N" + Settings.LoginInfo.DecimalValue))%></strong>
                            </td>
                        </tr>
                        </table>
                    </div>
                    
                    <div class="ns-h3">
                        Fleet Details</div>
                    <div style="padding-bottom: 5px;">
                        <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                                <td width="42%">
                                    <strong>Fleet name :</strong>
                                </td>
                                <td width="58%" style="font-weight:bold">
                                    <%=itinerary.FleetName %>
                                </td>
                            </tr>
                          <tr>
                                <td width="42%">
                                    <strong>Pickup Details</strong>
                                </td>
                              <td></td>
                            </tr>
                           
                            <tr>
                                <td width="42%">
                                    <strong>Pickup City :</strong>
                                </td>
                                <td width="58%" style="font-weight:bold">
                                 <%=itinerary.FromCity%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Pickup Location : </strong>
                                    <br />
                                </td>
                                <td>
                                    <%=itinerary.FromLocation %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Pickup Date:
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.FromDate.ToString("dd-MMM-yyyy HH:mm") %>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>Return Details </strong>
                                </td>
                                 <td></td>
                            </tr>
                            <%if (itinerary.ReturnStatus == "Y")
                              { %>
                            <tr>
                                <td width="42%">
                                    <strong>Drop City :</strong>
                                </td>
                                <td width="58%" style="font-weight:bold">
                                  <%=itinerary.ToCity %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Drop Location : </strong>
                                    <br />
                                </td>
                                <td>
                                    <%=itinerary.ToLocation %>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td>
                                    <strong>Drop Date:
                                    </strong>
                                </td>
                                <td>
                                    <%=itinerary.ToDate.ToString("dd-MMM-yyyy HH:mm") %>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>Price :</strong>
                                </td>
                                <td>
                                    <strong>
                                       <%=(itinerary.Currency)%>
                                    <%=Math.Ceiling(totalCharge).ToString("N"+ Settings.LoginInfo.DecimalValue)%></strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                   
                    <div class="clear">
                    </div>
                </div>
          <div class="clear">
            </div>
            </div>
            <div class="col-md-9 pad_right0 pad_xs0">
            <div class="ns-h3">
                Payment Confirmation</div>
            <div>
             <div>
                    <asp:MultiView ID="PaymentMultiView" runat="server" ActiveViewIndex="0">
                    
                        <asp:View ID="NormalView" runat="server">
                           <div class="bg_white pad_10 bor_gray"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                           
                                           
                                           <tr> 
                                           <td colspan="2"> <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label></td>
                                           
                                           
                                           </tr>
                                           
                                           
                                            
                                            <% int count = 0;
                                               foreach (FleetPassenger paxDetails in itinerary.PassengerInfo)
                                               {
                                            %>
                                          
                                            <tr>
                                                <td  height="24" class="red_span" colspan="2">
                                                    <strong>(Lead Guest)</strong>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                              <tr>
                                                <td height="24">
                                                    <strong>Title:</strong>
                                                </td>
                                                <td>
                                                    <%=paxDetails.Title%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24" width="21%">
                                                    <strong>First Name :<br />
                                                    </strong>
                                                </td>
                                                <td width="79%">
                                                    <%=paxDetails.FirstName%><br />
                                                </td>
                                            </tr>
                                             <tr>
                                                <td height="24" width="21%">
                                                    <strong>Last Name :<br />
                                                    </strong>
                                                </td>
                                                <td width="79%">
                                                    <%=paxDetails.LastName%><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24">
                                                    <strong>E-mail ID:</strong>
                                                </td>
                                                <td>
                                                    <%=paxDetails.Email%>
                                                </td>
                                            </tr>
                                            <%if(!string.IsNullOrEmpty(paxDetails.PickupAddress)){ %>
                                             <tr>
                                                <td height="24">
                                                    <strong>Pickup Address:</strong>
                                                </td>
                                                <td>
                                                    <%=paxDetails.PickupAddress%>
                                                </td>
                                            </tr>
                                            <%} %>
                                              <%if(!string.IsNullOrEmpty(paxDetails.Landmark)){ %>
                                             <tr>
                                                <td height="24">
                                                    <strong>Landmark:</strong>
                                                </td>
                                                <td>
                                                    <%=paxDetails.Landmark%>
                                                </td>
                                            </tr>
                                            <%} %>
                                              <tr>
                                                <td height="24">
                                                    <strong>Mobile No1:</strong>
                                                </td>
                                                <td>
                                                    <%=paxDetails.MobileNo%>
                                                </td>
                                            </tr>
                                            <%if (!string.IsNullOrEmpty(paxDetails.MobileNo2))
                                              { %>
                                             <tr>
                                                <td height="24">
                                                    <strong>Mobile No2:</strong>
                                                </td>
                                                <td>
                                                    <%=paxDetails.MobileNo2%>
                                                </td>
                                            </tr>
                                            <%} %>
                                            <%
                                                }
                                               %>
                                        </table>
                                           </td> </tr>
                                 
                                            <tr>
                    <td height="21">
                       
                    </td>
                </tr>
              
                <tr>
                    
                </tr>
                </table> 
                </div>
                <div> 
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                <tr>
                    <td>
                        <div style="color: Red; font-size: 14px">
                            <%=warningMsg %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="21">
                        <hr class="b_bot_1 " />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        <table width="100%">
                            <tr class="nA-h4">
                                <td>
                                    Available Balance :
                                </td>
                                <td style="font-weight: bold">
                                    <%=Settings.LoginInfo.AgentBalance.ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                             <tr class="nA-h4">
                                <td>
                                   Car Rental Charges :
                                </td>
                                <td style="font-weight: bold">
                                    <%=total%>
                                </td>
                            </tr>
                             <tr class="nA-h4">
                                <td>
                                   VRF:
                                </td>
                                <td style="font-weight: bold">
                                    <b><%=Math.Round(Convert.ToDecimal(0)).ToString("N"+Settings.LoginInfo.DecimalValue) %>0</b>
                                </td>
                            </tr>
                            
                            <%if(itinerary.CDW > -1) {%>
                             <tr class="nA-h4">
                                <td>
                                    cdw:
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.CDW, Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if(itinerary.SCDW > -1) {%>
                             <tr class="nA-h4">
                                <td>
                                    SCDW:
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.SCDW,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                            <%if(itinerary.PAI > -1) {%>
                             <tr class="nA-h4">
                                <td>
                                    PAI:
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.PAI,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if(itinerary.CSEAT > -1) {%>
                             <tr class="nA-h4">
                                <td>
                                    CSEAT:
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.CSEAT,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if(itinerary.VMD > -1) {%>
                             <tr class="nA-h4">
                                <td>
                                    VMD:
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.VMD,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if(itinerary.Emirates_Link_Charge > 0) {%>
                             <tr class="nA-h4">
                                <td>
                                     Link Charges :
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.Emirates_Link_Charge,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.AirportCharge > 0)
                               {%>
                             <tr class="nA-h4">
                                <td>
                                     Airport Charges :
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.AirportCharge,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.GPS > 0)
                               {%>
                             <tr class="nA-h4">
                                <td>
                                     GPS :
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.GPS,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.CHAUFFER > 0)
                               {%>
                             <tr class="nA-h4">
                                <td>
                                     CHAUFFER :
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Round(itinerary.CHAUFFER,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                            <%} %>
                            <tr class="nA-h4">
                                <td>
                                    Amount to be Booked :
                                </td>
                                <td class="spnred" style="font-weight: bold">
                                <%=itinerary.Currency %>
                                    <%=Math.Ceiling(totalCharge).ToString("N"+Settings.LoginInfo.DecimalValue)%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="21">
                        <hr class="b_bot_1 ">
                    </td>
                </tr>
                <tr>
                    <td >
                    
                    
                    
                    <div> 
                       <div class="col-md-9">  
                       <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                       
                       <tr>
                        <td width="97%" align="left">
                                    <label>
                                        <input type="checkbox" name="chkCancel" id="chkCancel"  />
                                        I have reviewed and agreed on the below Cancellation Policy offered for this booking
                                    </label>
                                    
                                </td>
                                 </tr>
                                   <tr> 
                            
                            <td>  
                            <div id="divCancelValidate" style="display: none; color: Red">
                                        <span>Please check the Cancellation policy to continue</span>
                                    </div>
                            </td>
                            
                            </tr>
                           <tr>
                               <td style='padding: 5px;'>
                        <div style='padding: 0px 10px 0px 10px'>
                                   <li>No charges for cancellation of booking within 24
                                       hours</li>
                                   <li>10% of total bill will be charged for booking cancellation
                                       fee if the cancellation is done less than 4 hours from the pickup time/delivery
                                       of vehicle.</li>
                                   <li>2 hours grace time given to pick the vehicle after
                                       the booking time. Exceptions are acceptable on a case to case basis and upon approval
                                       of Management.</li><br />
                              </div> </td>
                           </tr>
                            <tr>
                                <td width="97%" align="left">
                                    <label>
                                        <input type="checkbox" name="chkTerms" id="chkTerms"  />
                                        I have reviewed and agreed on the <a target="_blank"  href="Retail Agreement.pdf">Terms and Conditions</a> offered for this booking
                                    </label>
                                    
                                </td>
                            </tr>
                            
                            
                            
                            
                            <tr> 
                            
                            <td> 
                            
                            <div id="validate" style="display: none; color: Red">
                                        <span>Please check the Terms checkbox to continue</span>
                                    </div>
                            </td>
                            
                            </tr>
                            
                     
                            
                            
                            
                            
                        </table>
                       
                       
                        </div>
                       
                       
                    
                  
                    
                    
                    <div class="clearfix"> </div>
                    </div>
                        
                    </td>
                </tr>
                <tr>
                <td colspan="2">
                 <div class="col-md-9"></div>
                  <div class="col-md-3"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr><td width="100%" align="center">
                    <asp:Button ID="imgMakePayment" runat="server" CssClass="btn but_b pull-right" Text="Confirm"
                                OnClick="imgMakePayment_Click" OnClientClick="return validateTerms();" />
                   </td>
                    </tr> </table>
                    </div>
                </td>
               </tr>
                </table>
                
                
                </div>
                
                        </asp:View>
                             <asp:View ID="ErrorView" runat="server">
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/FleetSearch.aspx">Go to Search Page</asp:HyperLink>
                    <br />
                </asp:View>
                        </asp:MultiView>
            </div>
              <div class="clear">
            </div>
             </div>
        <div class="clear">
        </div>
    </div>
   
    <div class="clear">
    </div>
    </div> 
    <div id="PreLoader" style="display: none">
        <div class="loadingDiv">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Awaiting Fleet Booking confirmation from</strong>
            </div>
            <div style="font-size: 21px; color: #3060a0">
                <strong><span id="searchCity">
                    <%=itinerary.FleetName %></span> </strong>
                <br />
                <strong style="font-size: 14px; color: black">do not click the refresh or back button
                    as the transaction may be interrupted or terminated.</strong>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

