﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/TransactionVisaTitle.master"
    AutoEventWireup="true" Inherits="CorporateProfileUI"
    Title="" Codebehind="CorporateProfile.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DocumentManager.ascx" TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <asp:HiddenField ID="hdfFFId" runat="server" Value="0" />
    <asp:HiddenField ID="hdfHMId" runat="server" Value="0" />
    <asp:HiddenField ID="hdfCCId" runat="server" Value="0" />
    <asp:HiddenField ID="hdfDetailsIndexFF" runat="server" Value="-1" />
    <asp:HiddenField ID="hdfDetailsIndexHM" runat="server" Value="-1" />
    <asp:HiddenField ID="hdfDetailsIndexCC" runat="server" Value="-1" />
    <%--<asp:HiddenField  id="HiddenField3" runat="server" Value="-1"/>--%>
    <asp:HiddenField ID="hdfFFDisplayText" runat="server" Value="" />
    <asp:HiddenField ID="hdfFFDisplayValue" runat="server" Value="" />
    <asp:HiddenField ID="hdfHMDisplayText" runat="server" Value="" />
    <asp:HiddenField ID="hdfHMDisplayValue" runat="server" Value="" />
    <asp:HiddenField ID="hdfCCDisplayText" runat="server" Value="" />
    <asp:HiddenField ID="hdfCCDisplayValue" runat="server" Value="" />
    <asp:HiddenField ID="hdfRTDetailId" runat="server" Value="0" />
    <asp:HiddenField ID="hdfFFDeleted" runat="server" Value="" />
    <asp:HiddenField ID="hdfHMDeleted" runat="server" Value="" />
    <asp:HiddenField ID="hdfCCDeleted" runat="server" Value="" />
    <asp:HiddenField ID="hdfProfileImage" runat="server" Value="" />
    <asp:HiddenField runat="server" ID="hdfImgPath" Value="0" />
    <asp:HiddenField runat="server" ID="hdfCount" Value="0" />
    <asp:HiddenField runat="server" ID="hdfMode" Value="0"></asp:HiddenField>
    <!-- Lokesh:June1,2017 Added for CorporateProfile.aspx -- Approvals Tab-->
    <asp:HiddenField runat="server" ID="hdnTicketApprovers" Value="" />
    <asp:HiddenField runat="server" ID="hdnExpenseApprovers" Value="" />
    <input type="hidden" id="hdnControlsCount" value="0" />
    <asp:HiddenField runat="server" ID="hdnSavedTA" Value="" />
    <asp:HiddenField runat="server" ID="hdnSavedEA" Value="" />
    <asp:HiddenField runat="server" ID="hdnDelETA" Value="" />
    <asp:HiddenField runat="server" ID="hdnVisaApprovers" Value="" />
    <asp:HiddenField runat="server" ID="hdnSavedVA" Value="" />
    <asp:HiddenField runat="server" ID="hdnVisaDetails" Value="" />
    <asp:HiddenField runat="server" ID="hdnDelVD" Value="" />
    <asp:HiddenField runat="server" ID="hdnProfileId" Value="" />
        <asp:HiddenField ID="hdfdependentddlvalues" runat="server" Value="" />
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <%--<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>--%>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />

    <script type="text/javascript" src="Scripts/jsBE/organictabs.jquery.js"></script>

    <script type="text/javascript" src="ash.js"></script>

    <link rel="stylesheet" href="css/style.css" />
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">

        function SetLoginName(event) {
            if (getElement('hdfMode').value == '0')
                document.getElementById('<%=txtLoginName.ClientID %>').value = event.value;
        }

        /***Lokesh:1June2017- Start : APPROVALS Tab Javascript Functions ******************/
        var Ajax; //New Ajax object.

        var editedTicketExpenseApprover = "";
        var editedTicketExpenseRowId = "";
        var editedTicketApprover = "";
        var editedExpenseApprover = "";
        var editedVisaApprover = "";
        var manualUploader = "";
        function BindDependencyddl(parentid, ChildId, Query) {
            debugger;
            var options = '';
            var value;
                value = $('#' + parentid).val();
            var SqlQuery = Query.replace("@value", value);
            $.ajax({
                type: "POST",
                url: "CorporateProfile.aspx/LoadDependencyddl", //It calls our web method  
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: "{'Query':'" + SqlQuery + "' }",
                success: function (data) {
                    debugger;
                    options += '<option value=' + -1 + '>-- Select --</option>'
                    $.each(data.d, function (index, item) {
                        debugger;
                        options += "<option value='" + index + "'>" + item + "</option>";
                    });
                    $('#' + ChildId).append(options);
                     $("#"+ ChildId).select2("val", "-1");
                },
                error: function (d) {
                }
            });
        }
  function ValueDependencyddl(id,index) {
            debugger;
            var value = $('#' + id).val();
           var hdn= $('#<%=hdfdependentddlvalues.ClientID %>').val();
            if ($('#<%=hdfdependentddlvalues.ClientID %>').val() == "") {
                if (value != '-1')
                    document.getElementById('<%=hdfdependentddlvalues.ClientID %>').value = index + "-" + value;
            }
            else {
                 if (value != '-1')
                    document.getElementById('<%=hdfdependentddlvalues.ClientID %>').value +=","+ index + "-" + value;
            }
             
        }


        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function UploadFiles() {
            manualUploader.uploadStoredFiles();
            createFileUploader();
        }

        function ClearAll() {
          createFileUploader();
        }

        function appendAllApproversList() {

            bindExpenseApproversHtml();
            bindTicektApproversHtml();
            bindVisaApproversHtml();
            bindVisaDetailsHtml();
            clearTicketApprovers();
            clearEditedVariables();
            clearExpenseApprovers();
            clearVisaApprovers();
            clearVisaDetails();
           createFileUploader();
            getProfileDocuments(document.getElementById("<%=hdnProfileId.ClientID %>").value);
        }

        function bindVisaApproversHtml() {

            if (document.getElementById("<%=hdnVisaApprovers.ClientID %>").value.length > 0) {
                var savedTicketApprovers = document.getElementById('<%=hdnVisaApprovers.ClientID %>').value.split('|');

                for (var ea = 0; ea < savedTicketApprovers.length; ea++) {


                    var rowInfo = "";
                    rowInfo = savedTicketApprovers[ea];
                    document.getElementById("<%=ddlVisaApprovers.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];

                    document.getElementById("<%=ddlVisaOrder.ClientID%>").value = rowInfo.split('^')[1].split('-')[1];


                    var ticketApprover = document.getElementById("<%=ddlVisaApprovers.ClientID %>");
                    var ticketApproverName = ticketApprover.selectedIndex > -1 ? ticketApprover.options[ticketApprover.selectedIndex].text : '';


                    var ticketAppOrder = document.getElementById("<%=ddlVisaOrder.ClientID %>");
                    var ticketAppOrderText = ticketAppOrder.selectedIndex > -1 ? ticketAppOrder.options[ticketAppOrder.selectedIndex].text : '';

                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    AddTicketExpenseApproversDiv(

                    ticketApproverName,
                    ticketAppOrderText,
                    "V",
                    rowInfo)

                } //EOF : For Loop
            } //EOF if block

        } //EOF function




        function bindVisaDetailsHtml() {
           
            //$("#visaDetailsChildDiv").empty();
            //alert("i am here");
            if (document.getElementById("<%=hdnVisaDetails.ClientID %>").value.length > 0) {
                var savedTicketApprovers = document.getElementById('<%=hdnVisaDetails.ClientID %>').value.split('|');

                for (var ea = 0; ea < savedTicketApprovers.length; ea++) {


                    var rowInfo = "";
                    rowInfo = savedTicketApprovers[ea];
                    document.getElementById("<%=ddlVisaIssueCntry.ClientID %>").value = rowInfo.split('^')[1].split('#')[0];

                    var ticketApprover = document.getElementById("<%=ddlVisaIssueCntry.ClientID %>");
                    var ticketApproverName = ticketApprover.selectedIndex > -1 ? ticketApprover.options[ticketApprover.selectedIndex].text : '';
                    AddVisaDetailsDiv(

                    ticketApproverName,
                    rowInfo.split('^')[1].split('#')[1],
                    rowInfo.split('^')[1].split('#')[2],
                        rowInfo.split('^')[1].split('#')[3],
                    rowInfo.split('^')[1].split('#')[4],
                    "VD",
                    rowInfo)

                } //EOF : For Loop
            } //EOF if block

        } //EOF function



        function bindExpenseApproversHtml() {

            if (document.getElementById("<%=hdnExpenseApprovers.ClientID %>").value.length > 0) {
                var savedTicketApprovers = document.getElementById('<%=hdnExpenseApprovers.ClientID %>').value.split('|');

                for (var ea = 0; ea < savedTicketApprovers.length; ea++) {


                    var rowInfo = "";
                    rowInfo = savedTicketApprovers[ea];
                    document.getElementById("<%=ddlExpenseApprover.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];

                    document.getElementById("<%=ddlExpenseOrder.ClientID%>").value = rowInfo.split('^')[1].split('-')[1];


                    var ticketApprover = document.getElementById("<%=ddlExpenseApprover.ClientID %>");
                    var ticketApproverName = ticketApprover.selectedIndex > -1 ? ticketApprover.options[ticketApprover.selectedIndex].text : '';


                    var ticketAppOrder = document.getElementById("<%=ddlExpenseOrder.ClientID %>");
                    var ticketAppOrderText = ticketAppOrder.selectedIndex > -1 ? ticketAppOrder.options[ticketAppOrder.selectedIndex].text : '';

                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    AddTicketExpenseApproversDiv(

                    ticketApproverName,
                    ticketAppOrderText,
                    "E",
                    rowInfo)

                } //EOF : For Loop
            } //EOF if block

        } //EOF function


        function bindTicektApproversHtml() {

            //Main :Row Information Capturing.
            //Record Id (^)
            //ApproverId -
            //OrderId-
            //Type-

            if (document.getElementById("<%=hdnTicketApprovers.ClientID %>").value.length > 0) {
                var savedTicketApprovers = document.getElementById('<%=hdnTicketApprovers.ClientID %>').value.split('|');

                for (var ea = 0; ea < savedTicketApprovers.length; ea++) {


                    var rowInfo = "";
                    rowInfo = savedTicketApprovers[ea];
                    document.getElementById("<%=ddlTicketApprover.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];

                    document.getElementById("<%=ddlTicketOrder.ClientID%>").value = rowInfo.split('^')[1].split('-')[1];


                    var ticketApprover = document.getElementById("<%=ddlTicketApprover.ClientID %>");
                    var ticketApproverName = ticketApprover.options[ticketApprover.selectedIndex].text;


                    var ticketAppOrder = document.getElementById("<%=ddlTicketOrder.ClientID %>");
                    var ticketAppOrderText = ticketAppOrder.options[ticketAppOrder.selectedIndex].text;
                    AddTicketExpenseApproversDiv(

                    ticketApproverName,
                    ticketAppOrderText,
                    "T",
                    rowInfo)

                } //EOF : For Loop
            } //EOF if block

        } //EOF function

        function RemoveApproversDiv(ctrlID, type) {

            var ctrlID = parseInt(ctrlID);
            var removeApprover = document.getElementById('rowInfo' + ctrlID).innerHTML;
            $('#capturedApprover' + ctrlID).remove();
            //document.getElementById('capturedApprover' + ctrlID).remove();
            var hdnDelETA = document.getElementById('<%=hdnDelETA.ClientID %>').value;
            if (hdnDelETA.length == 0) {
                document.getElementById('<%=hdnDelETA.ClientID %>').value = removeApprover;
            }
            else {
                document.getElementById('<%=hdnDelETA.ClientID %>').value = hdnDelETA + "|" + removeApprover;
            }

            //Ticket
            if (type == "T") {

                var selectedApps = document.getElementById('<%=hdnTicketApprovers.ClientID %>').value.split('|');
                var selApp = '';
                for (var i = 0; i < selectedApps.length; i++) {

                    if (selectedApps[i] != removeApprover) {

                        if (selApp == '') {
                            selApp = selectedApps[i];
                        }
                        else {
                            selApp = selApp + '|' + selectedApps[i];
                        }
                    }
                }

                document.getElementById('<%=hdnTicketApprovers.ClientID %>').value = selApp;
                var deletedA = removeApprover.split('^')[1].split('-')[0];

                var selTA = document.getElementById('<%=hdnSavedTA.ClientID %>').value.split('|');
                var selA = '';
                for (var i = 0; i < selTA.length; i++) {

                    if (selTA[i] != deletedA) {

                        if (selA == '') {
                            selA = selTA[i];
                        }
                        else {
                            selA = selA + '|' + selTA[i];
                        }
                    }
                }

                document.getElementById('<%=hdnSavedTA.ClientID %>').value = selA;


            }
            else if (type == "E")//Expense
            {

                var selectedApps = document.getElementById('<%=hdnExpenseApprovers.ClientID %>').value.split('|');
                var selApp = '';
                for (var i = 0; i < selectedApps.length; i++) {

                    if (selectedApps[i] != removeApprover) {

                        if (selApp == '') {
                            selApp = selectedApps[i];
                        }
                        else {
                            selApp = selApp + '|' + selectedApps[i];
                        }
                    }
                }

                document.getElementById('<%=hdnExpenseApprovers.ClientID %>').value = selApp;


                var deletedA = removeCapExpenseAllowance.split('^')[1].split('-')[0];
                var selEA = document.getElementById('<%=hdnSavedEA.ClientID %>').value.split('|');
                var selA = '';
                for (var i = 0; i < selEA.length; i++) {

                    if (selEA[i] != deletedA) {

                        if (selA == '') {
                            selA = selEA[i];
                        }
                        else {
                            selA = selA + '|' + selEA[i];
                        }
                    }
                }
                document.getElementById('<%=hdnSavedEA.ClientID %>').value = selA;

            } //EOF EXPENSE APPROVERS

            else if (type == "V")//VISA APPROVERS
            {

                var selectedApps = document.getElementById('<%=hdnVisaApprovers.ClientID %>').value.split('|');
                var selApp = '';
                for (var i = 0; i < selectedApps.length; i++) {

                    if (selectedApps[i] != removeApprover) {

                        if (selApp == '') {
                            selApp = selectedApps[i];
                        }
                        else {
                            selApp = selApp + '|' + selectedApps[i];
                        }
                    }
                }

                document.getElementById('<%=hdnVisaApprovers.ClientID %>').value = selApp;


                var deletedA = removeCapExpenseAllowance.split('^')[1].split('-')[0];
                var selEA = document.getElementById('<%=hdnSavedVA.ClientID %>').value.split('|');
                var selA = '';
                for (var i = 0; i < selEA.length; i++) {

                    if (selEA[i] != deletedA) {

                        if (selA == '') {
                            selA = selEA[i];
                        }
                        else {
                            selA = selA + '|' + selEA[i];
                        }
                    }
                }
                document.getElementById('<%=hdnSavedVA.ClientID %>').value = selA;

            } //EOF VISA APPROVERS
        }



        function updateVisaApproverHtml(ctrlId, rowInfo) {
            var ctrlId = parseInt(ctrlId);
            var approver = document.getElementById("<%=ddlVisaApprovers.ClientID %>");
            var approverText = approver.options[approver.selectedIndex].text;

            var approverOrder = document.getElementById("<%=ddlVisaOrder.ClientID %>");
            var approverOrderText = approverOrder.options[approverOrder.selectedIndex].text;

            document.getElementById('an' + ctrlId).innerHTML = approverText;
            document.getElementById('ho' + ctrlId).innerHTML = approverOrderText;
            document.getElementById('rowInfo' + ctrlId).innerHTML = rowInfo;


        }

        function updateVisaApprovals() {
            if (Trim(document.getElementById('<%=ddlVisaApprovers.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Approver from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlVisaOrder.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select order.";
            }
            else {
                var selTicketAppName = document.getElementById("<%=ddlVisaApprovers.ClientID %>").value;
                var savedTicketApprovers = document.getElementById("<%=hdnSavedVA.ClientID %>").value.split('|');
                var duplicateCount = 0;

                var duplicateTicketAppName = [];
                for (var u = 0; u < savedTicketApprovers.length; u++) {
                    duplicateTicketAppName[u] = savedTicketApprovers[u];
                }
                for (var ft = 0; ft < duplicateTicketAppName.length; ft++) {

                    if (duplicateTicketAppName[ft] == editedVisaApprover) {
                        duplicateTicketAppName[ft] = selTicketAppName;
                    }
                }
                for (var d = 0; d < duplicateTicketAppName.length; d++) {

                    if (selTicketAppName == duplicateTicketAppName[d]) {
                        duplicateCount++;
                    }
                }

                if (duplicateCount > 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already added this combination";
                }
                else {

                    document.getElementById('addVisaApprovals').style.display = "block";
                    document.getElementById('cancelVisaApprovals').style.display = "none";
                    document.getElementById('updateVisaApprovals').style.display = "none";

                    valid = true;
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    var rowInfo = editedTicketExpenseApprover.split('^')[0];  //recordId;
                    //ApproverId
                    rowInfo += "^" + document.getElementById('<%=ddlVisaApprovers.ClientID %>').value;
                    //OrderId
                    rowInfo += "-" + document.getElementById('<%=ddlVisaOrder.ClientID %>').value;
                    //Type
                    rowInfo += "-" + "V"; //VISA APPROVERS FLAG



                    if (document.getElementById("<%=hdnSavedVA.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnSavedVA.ClientID %>").value = selTicketAppName;
                    }
                    else {
                        var savedTicketApprovers = document.getElementById('<%=hdnSavedVA.ClientID %>').value.split('|');
                        var selTicketApprovers = '';
                        for (var i = 0; i < savedTicketApprovers.length; i++) {


                            if (selTicketApprovers == '') {
                                if (savedTicketApprovers[i] == editedVisaApprover) {
                                    selTicketApprovers = selTicketAppName;
                                }
                                else {
                                    selTicketApprovers = savedTicketApprovers[i];
                                }
                            }
                            else {
                                if (savedTicketApprovers[i] == editedVisaApprover) {
                                    selTicketApprovers = selTicketAppName + '|' + savedTicketApprovers[i];
                                }
                                else {
                                    selTicketApprovers = selTicketApprovers + '|' + savedTicketApprovers[i];
                                }
                            }
                        }

                        document.getElementById('<%=hdnSavedVA.ClientID %>').value = selTicketApprovers;

                        var selectedApprovers = document.getElementById('<%=hdnVisaApprovers.ClientID %>').value.split('|');
                        var selApprover = '';
                        for (var i = 0; i < selectedApprovers.length; i++) {


                            if (selApprover == '') {
                                if (selectedApprovers[i] == editedTicketExpenseApprover) {
                                    selApprover = rowInfo;
                                }
                                else {
                                    selApprover = selectedApprovers[i];
                                }
                            }
                            else {
                                if (selectedApprovers[i] == editedTicketExpenseApprover) {
                                    selApprover = selApprover + '|' + rowInfo;
                                }
                                else {
                                    selApprover = selApprover + '|' + selectedApprovers[i];
                                }
                            }
                        }


                        document.getElementById("<%=hdnVisaApprovers.ClientID %>").value = selApprover;
                        updateVisaApproverHtml(editedTicketExpenseRowId, rowInfo)
                        clearVisaApprovers();

                    }




                }


            } //Eof else block
        } //Eof function


        function updateExpenseApproverHtml(ctrlId, rowInfo) {
            var ctrlId = parseInt(ctrlId);
            var approver = document.getElementById("<%=ddlExpenseApprover.ClientID %>");
            var approverText = approver.options[approver.selectedIndex].text;

            var approverOrder = document.getElementById("<%=ddlExpenseOrder.ClientID %>");
            var approverOrderText = approverOrder.options[approverOrder.selectedIndex].text;

            document.getElementById('an' + ctrlId).innerHTML = approverText;
            document.getElementById('ho' + ctrlId).innerHTML = approverOrderText;
            document.getElementById('rowInfo' + ctrlId).innerHTML = rowInfo;


        }



        function updateExpenseApprovals() {
            if (Trim(document.getElementById('<%=ddlExpenseApprover.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Approver from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlExpenseOrder.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select order.";
            }
            else {
                var selTicketAppName = document.getElementById("<%=ddlExpenseApprover.ClientID %>").value;
                var savedTicketApprovers = document.getElementById("<%=hdnSavedEA.ClientID %>").value.split('|');
                var duplicateCount = 0;

                var duplicateTicketAppName = [];
                for (var u = 0; u < savedTicketApprovers.length; u++) {
                    duplicateTicketAppName[u] = savedTicketApprovers[u];
                }
                for (var ft = 0; ft < duplicateTicketAppName.length; ft++) {

                    if (duplicateTicketAppName[ft] == editedExpenseApprover) {
                        duplicateTicketAppName[ft] = selTicketAppName;
                    }
                }
                for (var d = 0; d < duplicateTicketAppName.length; d++) {

                    if (selTicketAppName == duplicateTicketAppName[d]) {
                        duplicateCount++;
                    }
                }

                if (duplicateCount > 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already added this combination";
                }
                else {

                    document.getElementById('addExpenseApprovals').style.display = "block";
                    document.getElementById('cancelExpenseApprovals').style.display = "none";
                    document.getElementById('updateExpenseApprovals').style.display = "none";

                    valid = true;
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    var rowInfo = editedTicketExpenseApprover.split('^')[0];  //recordId;
                    //ApproverId
                    rowInfo += "^" + document.getElementById('<%=ddlExpenseApprover.ClientID %>').value;
                    //OrderId
                    rowInfo += "-" + document.getElementById('<%=ddlExpenseOrder.ClientID %>').value;
                    //Type
                    rowInfo += "-" + "E"; //Expense Approver Flag



                    if (document.getElementById("<%=hdnSavedEA.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnSavedEA.ClientID %>").value = selTicketAppName;
                    }
                    else {
                        var savedTicketApprovers = document.getElementById('<%=hdnSavedEA.ClientID %>').value.split('|');
                        var selTicketApprovers = '';
                        for (var i = 0; i < savedTicketApprovers.length; i++) {


                            if (selTicketApprovers == '') {
                                if (savedTicketApprovers[i] == editedExpenseApprover) {
                                    selTicketApprovers = selTicketAppName;
                                }
                                else {
                                    selTicketApprovers = savedTicketApprovers[i];
                                }
                            }
                            else {
                                if (savedTicketApprovers[i] == editedExpenseApprover) {
                                    selTicketApprovers = selTicketAppName + '|' + savedTicketApprovers[i];
                                }
                                else {
                                    selTicketApprovers = selTicketApprovers + '|' + savedTicketApprovers[i];
                                }
                            }
                        }

                        document.getElementById('<%=hdnSavedEA.ClientID %>').value = selTicketApprovers;

                        var selectedApprovers = document.getElementById('<%=hdnExpenseApprovers.ClientID %>').value.split('|');
                        var selApprover = '';
                        for (var i = 0; i < selectedApprovers.length; i++) {


                            if (selApprover == '') {
                                if (selectedApprovers[i] == editedTicketExpenseApprover) {
                                    selApprover = rowInfo;
                                }
                                else {
                                    selApprover = selectedApprovers[i];
                                }
                            }
                            else {
                                if (selectedApprovers[i] == editedTicketExpenseApprover) {
                                    selApprover = selApprover + '|' + rowInfo;
                                }
                                else {
                                    selApprover = selApprover + '|' + selectedApprovers[i];
                                }
                            }
                        }


                        document.getElementById("<%=hdnExpenseApprovers.ClientID %>").value = selApprover;
                        updateExpenseApproverHtml(editedTicketExpenseRowId, rowInfo)
                        clearExpenseApprovers();

                    }




                }


            } //Eof else block
        } //Eof function


        function updateTicketApproverHtml(ctrlId, rowInfo) {
            var ctrlId = parseInt(ctrlId);
            var approver = document.getElementById("<%=ddlTicketApprover.ClientID %>");
            var approverText = approver.options[approver.selectedIndex].text;

            var approverOrder = document.getElementById("<%=ddlTicketOrder.ClientID %>");
            var approverOrderText = approverOrder.options[approverOrder.selectedIndex].text;

            document.getElementById('an' + ctrlId).innerHTML = approverText;
            document.getElementById('ho' + ctrlId).innerHTML = approverOrderText;
            document.getElementById('rowInfo' + ctrlId).innerHTML = rowInfo;


        }

        function updateTicketApprovals() {
            if (Trim(document.getElementById('<%=ddlTicketApprover.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Approver from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlTicketOrder.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select order.";
            }
            else {
                var selTicketAppName = document.getElementById("<%=ddlTicketApprover.ClientID %>").value;
                var savedTicketApprovers = document.getElementById("<%=hdnSavedTA.ClientID %>").value.split('|');
                var duplicateCount = 0;

                var duplicateTicketAppName = [];
                for (var u = 0; u < savedTicketApprovers.length; u++) {
                    duplicateTicketAppName[u] = savedTicketApprovers[u];
                }
                for (var ft = 0; ft < duplicateTicketAppName.length; ft++) {

                    if (duplicateTicketAppName[ft] == editedTicketApprover) {
                        duplicateTicketAppName[ft] = selTicketAppName;
                    }
                }
                for (var d = 0; d < duplicateTicketAppName.length; d++) {

                    if (selTicketAppName == duplicateTicketAppName[d]) {
                        duplicateCount++;
                    }
                }

                if (duplicateCount > 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already added this combination";
                }
                else {

                    document.getElementById('addTicketApprovals').style.display = "block";
                    document.getElementById('updateTicketApprovals').style.display = "none";
                    document.getElementById('cancelTicketApprovals').style.display = "none";

                    valid = true;
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    var rowInfo = editedTicketExpenseApprover.split('^')[0];  //recordId;
                    //ApproverId
                    rowInfo += "^" + document.getElementById('<%=ddlTicketApprover.ClientID %>').value;
                    //OrderId
                    rowInfo += "-" + document.getElementById('<%=ddlTicketOrder.ClientID %>').value;
                    //Type
                    rowInfo += "-" + "T"; //ticket



                    if (document.getElementById("<%=hdnSavedTA.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnSavedTA.ClientID %>").value = selTicketAppName;
                    }
                    else {
                        var savedTicketApprovers = document.getElementById('<%=hdnSavedTA.ClientID %>').value.split('|');
                        var selTicketApprovers = '';
                        for (var i = 0; i < savedTicketApprovers.length; i++) {


                            if (selTicketApprovers == '') {
                                if (savedTicketApprovers[i] == editedTicketApprover) {
                                    selTicketApprovers = selTicketAppName;
                                }
                                else {
                                    selTicketApprovers = savedTicketApprovers[i];
                                }
                            }
                            else {
                                if (savedTicketApprovers[i] == editedTicketApprover) {
                                    selTicketApprovers = selTicketAppName + '|' + savedTicketApprovers[i];
                                }
                                else {
                                    selTicketApprovers = selTicketApprovers + '|' + savedTicketApprovers[i];
                                }
                            }
                        }

                        document.getElementById('<%=hdnSavedTA.ClientID %>').value = selTicketApprovers;

                        var selectedApprovers = document.getElementById('<%=hdnTicketApprovers.ClientID %>').value.split('|');
                        var selApprover = '';
                        for (var i = 0; i < selectedApprovers.length; i++) {


                            if (selApprover == '') {
                                if (selectedApprovers[i] == editedTicketExpenseApprover) {
                                    selApprover = rowInfo;
                                }
                                else {
                                    selApprover = selectedApprovers[i];
                                }
                            }
                            else {
                                if (selectedApprovers[i] == editedTicketExpenseApprover) {
                                    selApprover = selApprover + '|' + rowInfo;
                                }
                                else {
                                    selApprover = selApprover + '|' + selectedApprovers[i];
                                }
                            }
                        }


                        document.getElementById("<%=hdnTicketApprovers.ClientID %>").value = selApprover;
                        updateTicketApproverHtml(editedTicketExpenseRowId, rowInfo)
                        clearTicketApprovers();

                    }




                }


            } //Eof else block
        } //Eof function


        function EditTicketExpenseApprover(ctrlID, type) {

            var ctrlID = parseInt(ctrlID);
            var rowInfo = document.getElementById("rowInfo" + ctrlID).innerHTML;
            editedTicketExpenseApprover = rowInfo;
            editedTicketExpenseRowId = ctrlID;

            if (type == "T") { //Ticket
                editedTicketApprover = rowInfo.split('^')[1].split('-')[0]; //Approver Id
                editedExpenseApprover = "";
                editedVisaApprover = "";
                editedVisaDetails = "";
            }
            if (type == "E") {//Expense
                editedExpenseApprover = rowInfo.split('^')[1].split('-')[0]; //Approver Id
                editedTicketApprover = "";
                editedVisaApprover = "";
                editedVisaDetails = "";

            }

            if (type == "V") {//VISA
                editedVisaApprover = rowInfo.split('^')[1].split('-')[0]; //Approver Id
                editedTicketApprover = "";
                editedExpenseApprover = "";
                editedVisaDetails = "";

            }
            if (type == "VD") {//VISA DETAILS
                editedVisaDetails = rowInfo.split('^')[1].split('#')[0]; //Approver Id
                editedTicketApprover = "";
                editedExpenseApprover = "";
                editedVisaApprover = "";

            }


            if (type == "T") { //Ticket
                var recordId = rowInfo.split('^')[0];
                document.getElementById("<%=ddlTicketApprover.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];
                document.getElementById("<%=ddlTicketOrder.ClientID %>").value = rowInfo.split('^')[1].split('-')[1];

                document.getElementById('addTicketApprovals').style.display = "none";
                document.getElementById('updateTicketApprovals').style.display = "block";
                document.getElementById('cancelTicketApprovals').style.display = "block";
            }

            //ExpenseType
            if (type == "E") {
                var recordId = rowInfo.split('^')[0];
                document.getElementById("<%=ddlExpenseApprover.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];
                document.getElementById("<%=ddlExpenseOrder.ClientID %>").value = rowInfo.split('^')[1].split('-')[1];

                document.getElementById('addExpenseApprovals').style.display = "none";
                document.getElementById('updateExpenseApprovals').style.display = "block";
                document.getElementById('cancelExpenseApprovals').style.display = "block";

            }



            //VISA APPROVERS
            if (type == "V") {
                var recordId = rowInfo.split('^')[0];
                document.getElementById("<%=ddlVisaApprovers.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];
                document.getElementById("<%=ddlVisaOrder.ClientID %>").value = rowInfo.split('^')[1].split('-')[1];

                document.getElementById('addVisaApprovals').style.display = "none";
                document.getElementById('updateVisaApprovals').style.display = "block";
                document.getElementById('cancelVisaApprovals').style.display = "block";

            }

            //VISA DETAILS
            if (type == "VD") {
                var recordId = rowInfo.split('^')[0];
                document.getElementById("<%=ddlVisaIssueCntry.ClientID %>").value = rowInfo.split('^')[1].split('#')[0];
                document.getElementById("<%=txtVisaNo.ClientID %>").value = rowInfo.split('^')[1].split('#')[1];
                document.getElementById('ctl00_cphTransaction_dcVisaIssueDate_Date').value = rowInfo.split('^')[1].split('#')[2];
                document.getElementById('ctl00_cphTransaction_dcVisaExpDate_Date').value = rowInfo.split('^')[1].split('#')[3];
                document.getElementById("<%=ddlvisaplaceofissue.ClientID %>").value = rowInfo.split('^')[1].split('#')[4];

                document.getElementById('addVisaDetails').style.display = "none";
                document.getElementById('updateVisaDetails').style.display = "block";
                document.getElementById('cancelVisaDetails').style.display = "block";

            }

        } //EOF

        function RemoveTicketExpenseApproverDiv(ctrlID, type) {


        }
        function clearEditedVariables() {
            editedTicketExpenseApprover = "";
            editedTicketExpenseRowId = "";
            editedTicketApprover = "";
            editedExpenseApprover = "";
            editedVisaApprover = "";
            editedVisaDetails = "";

        }
        function cancelTicketApprover() {
            clearTicketApprovers();
            clearEditedVariables();
        }

        function cancelExpenseApprovals() {
            clearExpenseApprovers();
            clearEditedVariables();

        }
        function cancelVisaApprovals() {
            clearVisaApprovers();
            clearEditedVariables();

        }

        function cancelVisaDetails() {
            clearVisaDetails();
            clearEditedVariables();

        }


        function clearTicketApprovers() {

            document.getElementById('errMess').style.display = "none";
            document.getElementById('<%=ddlTicketApprover.ClientID %>').value = "0";
            document.getElementById('<%=ddlTicketOrder.ClientID %>').value = "0";

            document.getElementById('addTicketApprovals').style.display = "block";
            document.getElementById('updateTicketApprovals').style.display = "none";
            document.getElementById('cancelTicketApprovals').style.display = "none";

        }
        function clearExpenseApprovers() {
            document.getElementById('errMess').style.display = "none";
            document.getElementById('<%=ddlExpenseApprover.ClientID %>').value = "0";
            document.getElementById('<%=ddlExpenseOrder.ClientID %>').value = "0";

            document.getElementById('addExpenseApprovals').style.display = "block";
            document.getElementById('updateExpenseApprovals').style.display = "none";
            document.getElementById('cancelExpenseApprovals').style.display = "none";


        }
        function clearVisaApprovers() {
            document.getElementById('errMess').style.display = "none";
            document.getElementById('<%=ddlVisaApprovers.ClientID %>').value = "0";
            document.getElementById('<%=ddlVisaOrder.ClientID %>').value = "0";

            document.getElementById('addVisaApprovals').style.display = "block";
            document.getElementById('updateVisaApprovals').style.display = "none";
            document.getElementById('cancelVisaApprovals').style.display = "none";


        }
        function clearVisaDetails() {

            document.getElementById('errMess1').style.display = "none";
            document.getElementById('<%=ddlVisaIssueCntry.ClientID %>').value = "0";
            document.getElementById('<%=txtVisaNo.ClientID %>').value = "";
            document.getElementById('ctl00_cphTransaction_dcVisaIssueDate_Date').value = "";
            document.getElementById('ctl00_cphTransaction_dcVisaExpDate_Date').value = "";
             document.getElementById('<%=ddlvisaplaceofissue.ClientID %>').value = "0";
            document.getElementById('addVisaDetails').style.display = "block";
            document.getElementById('updateVisaDetails').style.display = "none";
            document.getElementById('cancelVisaDetails').style.display = "none";
        }

        function AddTicketExpenseApproversDiv(appName, appOrder, type, rowInfo) {

            var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
            ctrlID = ctrlID + 1;
            document.getElementById('hdnControlsCount').value = ctrlID;
            var paramList = 'requestSource=getApproversHtml' + '&id=' + ctrlID + '&approverName=' + appName + '&hierarchyOrder=' + appOrder + '&type=' + type + '&rowInfo=' + rowInfo;
            var url = "CorportatePoliciesExpenseAjax.aspx";
            if (type == "T") {
                Ajax.onreadystatechange = GetTicketApproversHtml;
            }
            else if (type == "E") {
                Ajax.onreadystatechange = GetExpenseApproversHtml;

            }
            else if (type == "V") {
                Ajax.onreadystatechange = GetVisaApproversHtml;

            }
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function GetTicketApproversHtml(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        $("#ticketApproversChildDiv:last").append(Ajax.responseText);


                        $("#aExpenseApprovers").trigger('click');
                        $("#aTicketApprovers").trigger('click');
                        clearTicketApprovers();
                    }
                }
            }
        }



        function GetExpenseApproversHtml(response) {

            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        $("#expenseApproversChildDiv:last").append(Ajax.responseText);
                        $("#aTicketApprovers").trigger('click');
                        $("#aExpenseApprovers").trigger('click');
                        clearExpenseApprovers();

                    }
                }
            }
        }

        function GetVisaApproversHtml(response) {

            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        $("#visaApproversChildDiv:last").append(Ajax.responseText);
                        $("#aTicketApprovers").trigger('click');
                        $("#aVisaApprovers").trigger('click');
                        clearVisaApprovers();

                    }
                }
            }
        }





        function Trim(stringToTrim) {
            return stringToTrim.replace(/^\s+|\s+$/g, "");
        }


        function addTicketApprovals() {

            //rowInfo -- recordId ^ ApproverId-OrderId-Type |recordId ^ ApproverId-OrderId-Type

            console.log("==========Exceuting addTicketApprovals function============");
            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlTicketApprover.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Approver from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlTicketOrder.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select order.";
            }
            else {

                //The below is used to restrict one approver one hierarchy order.the combination should not repeat.
                var selTicketAppName = document.getElementById("<%=ddlTicketApprover.ClientID %>").value;
                var savedTicketApprovers = document.getElementById("<%=hdnSavedTA.ClientID %>").value.split('|');
                var duplicateCount = 0;
                for (var p = 0; p < savedTicketApprovers.length; p++) {
                    if (selTicketAppName == savedTicketApprovers[p]) {
                        duplicateCount++;
                    }

                }
                if (duplicateCount >= 1) {
                    //$('select').select2('val','0');
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already addded this combination";
                }
                else {

                    valid = true;
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    var rowInfo = "-1" //recordId;
                    //ApproverId
                    rowInfo += "^" + document.getElementById('<%=ddlTicketApprover.ClientID %>').value;
                    //OrderId
                    rowInfo += "-" + document.getElementById('<%=ddlTicketOrder.ClientID %>').value;
                    //Type
                    rowInfo += "-" + "T"; //ticket

                    if (document.getElementById("<%=hdnTicketApprovers.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnTicketApprovers.ClientID %>").value = rowInfo;
                    }
                    else {
                        document.getElementById("<%=hdnTicketApprovers.ClientID %>").value += "|" + rowInfo;
                    }


                    if (document.getElementById("<%=hdnSavedTA.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnSavedTA.ClientID %>").value = selTicketAppName;
                    }
                    else {
                        document.getElementById("<%=hdnSavedTA.ClientID %>").value += "|" + selTicketAppName;

                    }




                    var appNameT = document.getElementById("<%=ddlTicketApprover.ClientID %>");
                    var selectedAppNameText = appNameT.options[appNameT.selectedIndex].text;

                    var appOrderT = document.getElementById("<%=ddlTicketOrder.ClientID %>");
                    var selectedAppOrderText = appOrderT.options[appOrderT.selectedIndex].text;
                    AddTicketExpenseApproversDiv(

                    selectedAppNameText,
                    selectedAppOrderText,
                    "T",
                    rowInfo)

                }
                $('select').select2();

            }
            return valid;

        } //EOF

        function addExpenseApprovals() {
            //rowInfo -- recordId ^ ApproverId-OrderId-Type |recordId ^ ApproverId-OrderId-Type
            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlExpenseApprover.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Approver from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlExpenseOrder.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select order.";
            }
            else {

                //The below is used to restrict one approver one hierarchy order.the combination should not repeat.
                var selTicketAppName = document.getElementById("<%=ddlExpenseApprover.ClientID %>").value;
                var savedTicketApprovers = document.getElementById("<%=hdnSavedEA.ClientID %>").value.split('|');
                var duplicateCount = 0;
                for (var p = 0; p < savedTicketApprovers.length; p++) {
                    if (selTicketAppName == savedTicketApprovers[p]) {
                        duplicateCount++;
                    }

                }
                if (duplicateCount >= 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already addded this combination";
                    //$('select').select2('val', '0');
                }
                else {

                    valid = true;
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    var rowInfo = "-1" //recordId;
                    //ApproverId
                    rowInfo += "^" + document.getElementById('<%=ddlExpenseApprover.ClientID %>').value;
                    //OrderId
                    rowInfo += "-" + document.getElementById('<%=ddlExpenseOrder.ClientID %>').value;
                    //Type
                    rowInfo += "-" + "E"; //ticket

                    if (document.getElementById("<%=hdnExpenseApprovers.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnExpenseApprovers.ClientID %>").value = rowInfo;
                    }
                    else {
                        document.getElementById("<%=hdnExpenseApprovers.ClientID %>").value += "|" + rowInfo;
                    }


                    if (document.getElementById("<%=hdnSavedEA.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnSavedEA.ClientID %>").value = selTicketAppName;
                    }
                    else {
                        document.getElementById("<%=hdnSavedEA.ClientID %>").value += "|" + selTicketAppName;

                    }

                    var appNameT = document.getElementById("<%=ddlExpenseApprover.ClientID %>");
                    var selectedAppNameText = appNameT.options[appNameT.selectedIndex].text;

                    var appOrderT = document.getElementById("<%=ddlExpenseOrder.ClientID %>");
                    var selectedAppOrderText = appOrderT.options[appOrderT.selectedIndex].text;
                    AddTicketExpenseApproversDiv(

                    selectedAppNameText,
                    selectedAppOrderText,
                    "E",
                    rowInfo)
                }
                $('select').select2();
            }
            return valid;
        } //EOF


        function addVisaApprovals() {
            //rowInfo -- recordId ^ ApproverId-OrderId-Type |recordId ^ ApproverId-OrderId-Type
            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlVisaApprovers.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Approver from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlVisaOrder.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select order.";
            }
            else {

                //The below is used to restrict one approver one hierarchy order.the combination should not repeat.
                var selTicketAppName = document.getElementById("<%=ddlVisaApprovers.ClientID %>").value;
                var savedTicketApprovers = document.getElementById("<%=hdnSavedVA.ClientID %>").value.split('|');
                var duplicateCount = 0;
                for (var p = 0; p < savedTicketApprovers.length; p++) {
                    if (selTicketAppName == savedTicketApprovers[p]) {
                        duplicateCount++;
                    }

                }
                if (duplicateCount >= 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already addded this combination";
                    //$('select').select2('val', '0');
                }
                else {

                    valid = true;
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //ApproverId -
                    //OrderId-
                    //Type-

                    var rowInfo = "-1" //recordId;
                    //ApproverId
                    rowInfo += "^" + document.getElementById('<%=ddlVisaApprovers.ClientID %>').value;
                    //OrderId
                    rowInfo += "-" + document.getElementById('<%=ddlVisaOrder.ClientID %>').value;
                    //Type
                    rowInfo += "-" + "V"; //VISA APPROVERS

                    if (document.getElementById("<%=hdnVisaApprovers.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnVisaApprovers.ClientID %>").value = rowInfo;
                    }
                    else {
                        document.getElementById("<%=hdnVisaApprovers.ClientID %>").value += "|" + rowInfo;
                    }


                    if (document.getElementById("<%=hdnSavedVA.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnSavedVA.ClientID %>").value = selTicketAppName;
                    }
                    else {
                        document.getElementById("<%=hdnSavedVA.ClientID %>").value += "|" + selTicketAppName;

                    }

                    var appNameT = document.getElementById("<%=ddlVisaApprovers.ClientID %>");
                    var selectedAppNameText = appNameT.options[appNameT.selectedIndex].text;

                    var appOrderT = document.getElementById("<%=ddlVisaOrder.ClientID %>");
                    var selectedAppOrderText = appOrderT.options[appOrderT.selectedIndex].text;
                    AddTicketExpenseApproversDiv(

                    selectedAppNameText,
                    selectedAppOrderText,
                    "V",
                    rowInfo)
                }
                $('select').select2();
                //$('select').select2('val', '0');
            }
            return valid;
        } //EOF




        function addVisaDetails() {
            var dcVisaIssueDate_Date = getElement('dcVisaIssueDate_Date').value.split('-');
            var dcVisaExpDate_Date = getElement('dcVisaExpDate_Date').value.split('-');

            //rowInfo -- recordId ^ Visa Country#Visa No#Issue Date#Expiry Date |recordId ^ Visa Country#Visa No#Issue Date#Expiry Date
            var valid = false;
            document.getElementById('errMess1').style.display = "none";

            if (getElement('ddlVisaIssueCntry').selectedIndex == 0) {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select visa country from the list .";
            }
            else if (getElement('txtVisaNo').value == '') {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please enter visa number.";
            }
            else if (getElement('dcVisaIssueDate_Date').value == '') {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select Visa Issue Date.";
            }
            else if (getElement('dcVisaIssueDate_Date').value != '' && (new Date(dcVisaIssueDate_Date[2], dcVisaIssueDate_Date[1] - 1, dcVisaIssueDate_Date[0]) > new Date(yyyy, mm - 1, dd))) {
              
                document.getElementById('errMess1').style.display = "block";              
                document.getElementById('errMess1').innerHTML = "Visa Issue Date cannot be greater then Current Date.";
                            
            }
            else if (getElement('dcVisaExpDate_Date').value == '') {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select Visa Expiry Date.";
            }
            else if (getElement('dcVisaExpDate_Date').value != '' && (new Date(dcVisaExpDate_Date[2], dcVisaExpDate_Date[1] - 1, dcVisaExpDate_Date[0]) < new Date(yyyy, mm - 1, dd))) {
              
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Visa Exp Date cannot be less then Current Date.";

            } 
            else if (getElement('dcVisaIssueDate_Date').value != '' && getElement('dcVisaExpDate_Date').value != '' && (new Date(dcVisaIssueDate_Date[2], dcVisaIssueDate_Date[1] - 1, dcVisaIssueDate_Date[0]) > new Date(dcVisaExpDate_Date[2], dcVisaExpDate_Date[1] - 1, dcVisaExpDate_Date[0]))) {
             
                document.getElementById('errMess1').style.display = "block";
                    document.getElementById('errMess1').innerHTML = "Visa Issue Date cannot be greater then Visa Exp Date.";
         
            }
             else if (getElement('ddlvisaplaceofissue').selectedIndex == 0) {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select place of visa Issue from the list .";
            }
            else {

                valid = true;
                //Main :Row Information Capturing.
                //Record Id (^)
                //Visa Country#
                //Visa No#
                //Issue Date#
                //Expiry Date#
                //Type


                var rowInfo = "-1" //recordId;
                //Visa Country
                rowInfo += "^" + document.getElementById('<%=ddlVisaIssueCntry.ClientID %>').value;
                //Visa No
                rowInfo += "#" + document.getElementById('<%=txtVisaNo.ClientID %>').value;
                //Issue Date
                rowInfo += "#" + getElement('dcVisaIssueDate_Date').value;
                //Expiry Date
                rowInfo += "#" + getElement('dcVisaExpDate_Date').value;
                 //place of issue
                rowInfo += "#" + document.getElementById('<%=ddlvisaplaceofissue.ClientID %>').value;

                //Type
                rowInfo += "#" + "VD"; //VISA DETAILS

                if (document.getElementById("<%=hdnVisaDetails.ClientID %>").value.length == 0) {
                    document.getElementById("<%=hdnVisaDetails.ClientID %>").value = rowInfo;
                }
                else {
                    document.getElementById("<%=hdnVisaDetails.ClientID %>").value += "|" + rowInfo;
                }

                var visaCountry = document.getElementById("<%=ddlVisaIssueCntry.ClientID %>");
                var visaplaceofissue= document.getElementById('<%=ddlvisaplaceofissue.ClientID %>');
                var selectedvisaCountryText = visaCountry.options[visaCountry.selectedIndex].text;
                var PlaceofIssueText=visaplaceofissue.options[visaplaceofissue.selectedIndex].text;

                AddVisaDetailsDiv(

                    selectedvisaCountryText,
                    document.getElementById('<%=txtVisaNo.ClientID %>').value,
                    getElement('dcVisaIssueDate_Date').value,
                    getElement('dcVisaExpDate_Date').value,
                   PlaceofIssueText,
                    "VD",
                    rowInfo)

                $('select').select2();

              }
            return valid;
        } //EOF



        function AddVisaDetailsDiv(visaCountry, visaNo, issueDate, ExpDate,placeofissue,type, rowInfo) {

            var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
            ctrlID = ctrlID + 1;
            document.getElementById('hdnControlsCount').value = ctrlID;
            var paramList = 'requestSource=getVisaDetailsHtml' + '&id=' + ctrlID + '&visaCountry=' + visaCountry + '&visaNo=' + visaNo + '&issueDate=' + issueDate + '&ExpDate=' + ExpDate + '&rowInfo=' + rowInfo + '&type=' + type+ '&Placeofissue=' + placeofissue;
            var url = "CorportatePoliciesExpenseAjax.aspx";
            if (type == "VD") {
                Ajax.onreadystatechange = GetVisaDetailsHtml;
            }

            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }


        function GetVisaDetailsHtml(response) {

            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        // alert(Ajax.responseText);
                        $("#visaDetailsChildDiv:last").append(Ajax.responseText);
                        clearVisaDetails();

                    }
                }
            }
        }

        function clearVisaDetails() {

            document.getElementById('errMess1').style.display = "none";
            getElement('ddlVisaIssueCntry').selectedIndex = 0;
            document.getElementById('<%=txtVisaNo.ClientID %>').value = "";
             getElement('ddlvisaplaceofissue').selectedIndex = 0;
            getElement('dcVisaIssueDate_Date').value = "";
            getElement('dcVisaExpDate_Date').value = "";
            document.getElementById('addVisaDetails').style.display = "block";
            document.getElementById('updateVisaDetails').style.display = "none";
            document.getElementById('cancelVisaDetails').style.display = "none";
        }


        function RemoveVisaDetailsDiv(ctrlID, type) {
          
            var ctrlID = parseInt(ctrlID);
            var removeApprover = document.getElementById('rowInfo' + ctrlID).innerHTML;
            $('#capturedApprover' + ctrlID).remove();
            //document.getElementById('capturedApprover' + ctrlID).remove();
            var hdnDelETA = document.getElementById('<%=hdnDelVD.ClientID %>').value;
            if (hdnDelETA.length == 0) {
                document.getElementById('<%=hdnDelVD.ClientID %>').value = removeApprover;
            }
            else {
                document.getElementById('<%=hdnDelVD.ClientID %>').value = hdnDelETA + "|" + removeApprover;
            }

            //VISA DETAILS
            if (type == "VD") {

                var selectedApps = document.getElementById('<%=hdnVisaDetails.ClientID %>').value.split('|');
                var selApp = '';
                for (var i = 0; i < selectedApps.length; i++) {

                    if (selectedApps[i] != removeApprover) {

                        if (selApp == '') {
                            selApp = selectedApps[i];
                        }
                        else {
                            selApp = selApp + '|' + selectedApps[i];
                        }
                    }
                }

                document.getElementById('<%=hdnVisaDetails.ClientID %>').value = selApp;


            }
            //VISA DETAILS
        }



        function updateVisaDetails() {
            //rowInfo -- recordId ^ Visa Country#Visa No#Issue Date#Expiry Date |recordId ^ Visa Country#Visa No#Issue Date#Expiry Date
            var valid = false;
            var dcVisaIssueDate_Date = getElement('dcVisaIssueDate_Date').value.split('-');
            var dcVisaExpDate_Date = getElement('dcVisaExpDate_Date').value.split('-');
           
            document.getElementById('errMess1').style.display = "none";

            if (getElement('ddlVisaIssueCntry').selectedIndex == 0) {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select visa country from the list .";
            }
            else if (getElement('txtVisaNo').value == '') {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please enter visa number.";
            }
            else if (getElement('dcVisaIssueDate_Date').value == '') {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select Visa Issue Date.";
            }
            else if (getElement('dcVisaIssueDate_Date').value != '' && (new Date(dcVisaIssueDate_Date[2], dcVisaIssueDate_Date[1] - 1, dcVisaIssueDate_Date[0]) > new Date(yyyy, mm - 1, dd))) {

                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Visa Issue Date cannot be greater then Current Date.";

            }
            else if (getElement('dcVisaExpDate_Date').value == '') {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select Visa Expiry Date.";
            }
            else if (getElement('dcVisaExpDate_Date').value != '' && (new Date(dcVisaExpDate_Date[2], dcVisaExpDate_Date[1] - 1, dcVisaExpDate_Date[0]) < new Date(yyyy, mm - 1, dd))) {

                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Visa Exp Date cannot be less then Current Date.";

            }
            else if (getElement('dcVisaIssueDate_Date').value != '' && getElement('dcVisaExpDate_Date').value != '' && (new Date(dcVisaIssueDate_Date[2], dcVisaIssueDate_Date[1] - 1, dcVisaIssueDate_Date[0]) > new Date(dcVisaExpDate_Date[2], dcVisaExpDate_Date[1] - 1, dcVisaExpDate_Date[0]))) {

                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Visa Issue Date cannot be greater then Visa Exp Date.";

            }
else if (getElement('ddlvisaplaceofissue').selectedIndex == 0) {
                document.getElementById('errMess1').style.display = "block";
                document.getElementById('errMess1').innerHTML = "Please select visa place of issue from the list .";
            }
            else {

                document.getElementById('addVisaDetails').style.display = "block";
                document.getElementById('cancelVisaDetails').style.display = "none";
                document.getElementById('updateVisaDetails').style.display = "none";

                valid = true;
                //Main :Row Information Capturing.
                //Record Id (^)
                //Visa Country#
                //Visa No#
                //Issue Date#
                //Expiry Date#
                //Type


                var rowInfo = "-1" //recordId;
                //Visa Country
                rowInfo += "^" + document.getElementById('<%=ddlVisaIssueCntry.ClientID %>').value;
                //Visa No
                rowInfo += "#" + document.getElementById('<%=txtVisaNo.ClientID %>').value;
                //Issue Date
                rowInfo += "#" + getElement('dcVisaIssueDate_Date').value;
                //Expiry Date
                rowInfo += "#" + getElement('dcVisaExpDate_Date').value;
                //place of issue
                rowInfo += "#" + document.getElementById('<%=ddlvisaplaceofissue.ClientID %>').value;

                var selectedApprovers = document.getElementById('<%=hdnVisaDetails.ClientID %>').value.split('|');
                var selApprover = '';
                for (var i = 0; i < selectedApprovers.length; i++) {


                    if (selApprover == '') {
                        if (selectedApprovers[i] == editedTicketExpenseApprover) {
                            selApprover = rowInfo;
                        }
                        else {
                            selApprover = selectedApprovers[i];
                        }
                    }
                    else {
                        if (selectedApprovers[i] == editedTicketExpenseApprover) {
                            selApprover = selApprover + '|' + rowInfo;
                        }
                        else {
                            selApprover = selApprover + '|' + selectedApprovers[i];
                        }
                    }
                }


                document.getElementById("<%=hdnVisaDetails.ClientID %>").value = selApprover;
                updateVisaDetailsHtml(editedTicketExpenseRowId, rowInfo)
                clearVisaDetails();


            } //Eof else block
        } //Eof function

        function updateVisaDetailsHtml(ctrlId, rowInfo) {
            var ctrlId = parseInt(ctrlId);
            var cntry = document.getElementById("<%=ddlVisaIssueCntry.ClientID %>");
            var cntryText = cntry.options[cntry.selectedIndex].text;

            var visaNo = document.getElementById("<%=txtVisaNo.ClientID %>").value;
            var placeofissue=document.getElementById("<%=ddlvisaplaceofissue.ClientID %>").value;

            document.getElementById('vc' + ctrlId).innerHTML = cntryText;
            document.getElementById('vn' + ctrlId).innerHTML = visaNo;
            document.getElementById('vid' + ctrlId).innerHTML = getElement('dcVisaIssueDate_Date').value;
            document.getElementById('ved' + ctrlId).innerHTML = getElement('dcVisaExpDate_Date').value;
             document.getElementById('vpi' + ctrlId).innerHTML = placeofissue;
            document.getElementById('rowInfo' + ctrlId).innerHTML = rowInfo;


        }


        function getProfileDocuments(profileId) {
            var paramList = 'requestSource=getProfileDocDetails&profileId=' + profileId;
            var url = "CorportatePoliciesExpenseAjax.aspx";
            Ajax.onreadystatechange = bindDocDetails;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function bindDocDetails() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        if (Ajax.responseText != "Error") {
                            $("#bookingRecords:last").append(Ajax.responseText);
                        }
                    }
                }
            }
        }

        function Download(path, docName) {
            //alert(path);
            //alert(docName);
            var open = window.open("DownloadeDoc.aspx?path=" + path + "&docName=" + docName);
            return false;
        }





        /****************** End : APPROVALS Tab Javascript Functions **********************/



        var cal1;
        var cal2;
        var cal3;
        var cal4;

        function init1() {

            cal1 = new YAHOO.widget.Calendar("cal1", "callContainer1");
            cal1.selectEvent.subscribe(setDate1);
            //alert('hi1');
            cal1.cfg.setProperty("close", true);
            cal1.render();
        }
        function init2() {

            cal2 = new YAHOO.widget.Calendar("cal2", "callContainer2");
            //alert('hi2');
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function init3() {

            cal3 = new YAHOO.widget.Calendar("cal3", "callContainer3");
            //alert('hi3');
            cal3.selectEvent.subscribe(setDate3);
            cal3.cfg.setProperty("close", true);
            cal3.render();
        }

        function init4() {

            cal4 = new YAHOO.widget.Calendar("cal4", "callContainer4");
            //alert('hi4');
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }

        YAHOO.util.Event.addListener(window, "load", init1);
        YAHOO.util.Event.addListener(window, "load", init2);
        YAHOO.util.Event.addListener(window, "load", init3);
        YAHOO.util.Event.addListener(window, "load", init4);


        function showCalendar(container) {
            init1();
            // alert(document.getElementById(container));
            var containerId = container;
            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer2').style.display = "none";
                document.getElementById('callContainer3').style.display == "none";
                document.getElementById('callContainer4').style.display == "none";
            }
        }

        function showCalendar2(container) {
            init2();
            //  alert(document.getElementById(container));
            var containerId = container;

            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer1').style.display = "none";
                document.getElementById('callContainer3').style.display == "none";
                document.getElementById('callContainer4').style.display == "none";

            }
        }

        function showCalendar3(container) {
            init3();
            //  alert(document.getElementById(container));
            var containerId = container;

            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer1').style.display = "none";
                document.getElementById('callContainer2').style.display == "none";
                document.getElementById('callContainer4').style.display == "none";

            }
        }
        function showCalendar4(container) {
            init4();
            //  alert(document.getElementById(container));
            var containerId = container;

            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer1').style.display = "none";
                document.getElementById('callContainer2').style.display == "none";
                document.getElementById('callContainer3').style.display == "none";

            }
        }
        function setDate1() {

            var date1 = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtDOI.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer1').style.display = "none";
        }

        function setDate2() {
            var date1 = cal2.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtDOE.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer2').style.display = "none";
        }

        function setDate3() {
            var date1 = cal3.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtDOB.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer3').style.display = "none";
        }

        function setDate4() {
            var date1 = cal4.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtCardDOE.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer4').style.display = "none";
        }
    </script>
  <%-- Style Added for Drag&Drop Dev Express Control - somasekhar on 30/03/2018--%>
    <style>

        .dropZoneExternal > div,
.dropZoneExternal > img
{
    position: absolute;
}
.dropZoneExternal
{
    position: relative;
    border: 1px dashed #f17f21!important;
    cursor: pointer;
}
.dropZoneExternal,
.dragZoneText
{
    width: 100%;
    height: 250px;
}
.dropZoneText
{
    width: 200px;
    height: 100px;
    color: #fff;
    background-color: #888;
}
#dropZone
{
    top: 0;
    padding: 100px 25px; 
}
.uploadControlDropZone,
.hidden
{
    display: none;
}
.dropZoneText,
.dragZoneText
{
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    font-size: 20pt;
}
.dragZoneText
{
    color: #808080;
}
.dxucInlineDropZoneSys span
{
    color: #fff!important;
    font-size: 10pt;
    font-weight: normal!important;
}
.uploadControlProgressBar
{
    width: 150px!important;
}
.validationMessage
{
    padding: 0 20px;
    text-align: center;
}
.uploadControl
{
    margin-top: 10px;
}
.Note
{
    max-width: 300px;
}
    </style>

     <%-- Script Added for Drag&Drop Dev Express Control - somasekhar on 30/03/2018--%>
    <script type="text/javascript">
        function onUploadControlFileUploadComplete(s, e) {
         
            if (e.isValid)
            {
                document.getElementById("uploadedImage").src = e.callbackData;
                document.getElementById('<%=imgProfilePic.ClientID %>').src = e.callbackData;              
            }
            setElementVisible("uploadedImage", e.isValid);
        }
        function onImageLoad() {
            var externalDropZone = document.getElementById("externalDropZone");
            var uploadedImage = document.getElementById("uploadedImage");
            uploadedImage.style.left = (externalDropZone.clientWidth - uploadedImage.width) / 2 + "px";
            uploadedImage.style.top = (externalDropZone.clientHeight - uploadedImage.height) / 2 + "px";
            setElementVisible("dragZone", false);
        }
        function setElementVisible(elementId, visible) {
          document.getElementById(elementId).className = visible ? "" : "hidden";
        }
    </script>


    <div class="container-fluid CorpTrvl-page">

        <div class="row">
            <div class="col-xs-12">
                <h2>
                    Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-2">
                <div class="profile-photo-wrapper">
                    <div class="photo">
                        <%--src="images/silhouettee.png"--%>
                        <asp:Image runat="server" ID="imgProfilePic" src="images/silhouettee.png" alt="" />
                    </div>
                    <a style="display:none" href="#" class="add-remove-btn">ADD / REMOVE PHOTO</a>
                    <div class="clearfix">
                    </div>
                </div>
             
                   <div>
                        
                    <div id="externalDropZone" class="dropZoneExternal">
                            <div id="dragZone">
                                <span class="dragZoneText">Drag an image here</span>
                            </div>
                            <img id="uploadedImage" src="" class="hidden" alt="" onload="onImageLoad()" />
                            <div id="dropZone" class="hidden">
                                <span class="dropZoneText">Drop an image here</span>
                            </div> 
                        </div>
                       <%--<dx:ASPxUploadControl ID="UploadControl" ClientInstanceName="UploadControl" runat="server" UploadMode="Auto" AutoStartUpload="True" Width="100%"
                            ShowProgressPanel="True" CssClass="uploadControl" DialogTriggerID="externalDropZone" OnFileUploadComplete="UploadControl_FileUploadComplete">
                            <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" ExternalDropZoneID="externalDropZone" DropZoneText="" />
                            <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage" DisableHttpHandlerValidation="true" />
                            <BrowseButton Text="Select an image for upload..." />
                            <DropZoneStyle CssClass="uploadControlDropZone" />
                            <ProgressBarStyle CssClass="uploadControlProgressBar" />
                            <ClientSideEvents
                                DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }"
                                DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }"
                                FileUploadComplete="onUploadControlFileUploadComplete"></ClientSideEvents>
                        </dx:ASPxUploadControl>--%>
                  </div>

            </div>
            <div class="col-md-9 col-lg-10" >

                <div class="row" style="padding-bottom :10px;" runat="server" id="divEmpQuerySearch">
                                <div class="col-xs-8 col-sm-8" >
                                    <div class="form-group">
                                        <div class="col-xs-8 col-sm-4" >
                                         <asp:TextBox CssClass="form-control"  runat="server" ID="txtEmpQuerySearch" placeholder="Employee Code"  MaxLength="10"></asp:TextBox>
                                       </div>
                                            <div class="col-xs-8 col-sm-2" >
                                         <asp:Button CssClass="btn btn-default" runat="server" Text="SEARCH" ID="btnEmpQuerySearch"  OnClick="btnEmpQuerySearch_Click" />
                                   </div>
                                                 </div>
                                </div>
                            </div>


                <div class="CorpTrvl-tabbed-panel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs responsive" role="tablist">
                        <li role="presentation" class="active"><a href="#gen-info" aria-controls="gen-info"
                            role="tab" data-toggle="tab">GENERAL INFO</a></li>
                        <li role="presentation"><a href="#login-info" aria-controls="login-info" role="tab"
                            data-toggle="tab">LOGIN INFO</a></li>
                        <li role="presentation"><a href="#personal-info" aria-controls="personal-info" role="tab"
                            data-toggle="tab">PERSONAL INFO</a></li>
                        <li role="presentation"><a href="#travel-info" aria-controls="travel-info" role="tab"
                            data-toggle="tab">TRAVEL INFO</a></li>
                        <li role="presentation"><a href="#hotel-info" aria-controls="card-info" role="tab"
                            data-toggle="tab">HOTEL INFO</a></li>
                        <li role="presentation"><a href="#card-info" aria-controls="card-info" role="tab"
                            data-toggle="tab">CARD INFO</a></li>
                        <li role="presentation"><a href="#other-remarks" aria-controls="other-remarks" role="tab"
                            data-toggle="tab">OTHER REMARKS</a></li>
                        <li role="presentation"><a href="#additional-details" aria-controls="additional-details"
                            role="tab" data-toggle="tab">ADD DETAILS</a></li>
                        <li role="presentation"><a href="#policy-info" aria-controls="policy-info" role="tab"
                            data-toggle="tab">POLICY</a></li>
                        <!-- Lokesh : 31May2017 Added Approvals Tab-->
                        <!-- Approvals Tab-->
                        <li role="presentation"><a href="#approvals" aria-controls="approvals" role="tab"
                            data-toggle="tab" aria-expanded="true">APPROVALS</a></li>
                        <li role="presentation"><a href="#documents" aria-controls="approvals" role="tab" data-toggle="tab"
                            aria-expanded="true">DOCUMENTS</a></li>
                         <li role="presentation"><a href="#contact-info" aria-controls="contact-info" role="tab"
                            data-toggle="tab">CONTACT INFO</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content responsive">
                        <div role="tabpanel" class="tab-pane active" id="gen-info">
                            <div class="row">
                                <div class="col-xs-8 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Client</label>
                                        <%-- <select class="form-control">
                            <option>COZMO TRAVEL</option>
                        </select>--%>
                                        <asp:DropDownList ID="ddlAgent" runat="server" class="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-4">
                                    <asp:CheckBox runat="server" ID="chkIsActive" Text="Is Active" Checked="true"/>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-1 col-sm-1 col-md-1">
                                    <div class="form-group">
                                        <label for="">
                                            Title</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Surname">--%>
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlTitle" >
                                            <asp:ListItem Text="Mr." Value="Mr"></asp:ListItem>
                                            <asp:ListItem Text="Mrs." Value="Mrs"></asp:ListItem>
                                            <asp:ListItem Text="Mstr." Value="Mstr"></asp:ListItem>
                                            <asp:ListItem Text="Miss." Value="Miss"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Surname/Last Name</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Surname">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtSurname" placeholder="Surname"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Given Name/First Name</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Given Name">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtname" placeholder="Given Name"></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="col-xs-5 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Middle Name</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Given Name">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtMiddleName" placeholder="Middle Name"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Batch #</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Given Name">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtbatch" placeholder="Batch"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Designation</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Designation">--%>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                        <label>
                                            Date of Joining
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                            <uc1:DateControl ID="dcJoiningDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True"  />
                                        </div>
                                    </div> 
                                    </div>
                                </div>
                                  <div style="display:none" class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                        <label>
                                            Date Of Termination
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                            <uc1:DateControl ID="dcTerminationdate" runat="server" DateFormat="DDMMYYYY" DateOnly="True"  />
                                        </div>
                                    </div> 
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Employee ID</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Employee ID">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtEmpId" placeholder="Employee ID"></asp:TextBox>
                                    </div>
                                </div>
                                
                                
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                          <label for="">
                                            Division</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Division">--%>
                                        <asp:DropDownList ID="ddlDivision" runat="server" class="form-control" placeholder="Division">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group"> 
                                         <label for="">
                                            Cost Centre</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Cost Centre">--%>
                                        <asp:DropDownList ID="ddlCostCentre" runat="server" class="form-control" placeholder="Cost Centre">
                                        </asp:DropDownList>
                                       </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="center-block">
                                            Telephone</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Telephone">--%>
                                        <asp:TextBox Width="20%" class="form-control pull-left" runat="server" ID="txtPhoneCountryCode"
                                            placeholder="Code"></asp:TextBox>
                                        <asp:TextBox Width="80%" class="form-control pull-left" runat="server" ID="txtTelPhone"
                                            placeholder="Telephone"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="center-block">
                                            Mobile</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Mobile">--%>
                                        <asp:TextBox Width="20%" class="form-control pull-left" runat="server" ID="txtMobileCoutryCode"
                                            placeholder="Code"></asp:TextBox>
                                        <asp:TextBox Width="80%" class="form-control pull-left" runat="server" ID="txtMobileNo"
                                            placeholder="Mobile"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            ZIP CODE</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Fax">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtFax" placeholder="Zip Code"></asp:TextBox>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Email</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Email">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtEmail" onchange="SetLoginName(this);" placeholder="Email"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Grade</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Fax">--%>
                                        <asp:DropDownList ID="ddlGrade" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Profile Type</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Email">--%>
                                        <asp:DropDownList ID="ddlProfileType" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            Address</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Address Line 1">--%>
                                       <asp:TextBox class="form-control" runat="server" ID="txtAddress2" placeholder="Office Address"></asp:TextBox>                                         
                                    </div>
                                    <div class="form-group">
                                        <%--<input type="text" class="form-control" id="" placeholder="Address Line 2">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtAddress1" placeholder="Residential Address"></asp:TextBox>
                                    </div>
                                    <div style="display:none"  class="form-group">
                                        <label for="exampleInputEmail1">
                                            Upload Profile Pic</label>
                                    </div>
                                    <%--<input type="text" class="form-control" id="" placeholder="Address Line 1">--%>
                                    <div style="display:none"  class="col-md-1">
                                        <CT:DocumentManager ID="profileImage" runat="server" DefaultImageUrl="~/images/common/no_preview.png"
                                            DocumentImageUrl="~/images/common/Preview.png" ShowActualImage="false" SessionName="crenterlic"
                                            DefaultToolTip="" />
                                    </div>
                                    <br />
                                    <%--  <div class="col-md-1"><asp:LinkButton ID="lnkView" runat="server" Text="View&nbsp;Image" Visible="true" OnClientClick="return View();"></asp:LinkButton></div>--%>
                                    <%-- <div class="col-md-2">
            <asp:Image ID="imgPreview" Style="display: none;" runat="server" Height="70px" Width="80px" /> 
             <asp:Label ID="lblUploadMsg" runat="server"></asp:Label>
             <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
         </div>--%>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                 
                                </div>
                            </div>
                        </div>
                        <!--Commented By Lokesh As there is no need to save the users in the user table-->
                        
                        <div role="tabpanel" class="tab-pane" id="login-info">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Login Name</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Login Name">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtLoginName" placeholder="Login Name"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Password</label>
                                        <%--<input type="password" class="form-control" id="" placeholder="Password">--%>
                                        <asp:TextBox class="form-control" runat="server" TextMode="Password" ID="txtPassword"
                                            placeholder="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Confirm Password</label>
                                        <%--<input type="password" class="form-control" id="" placeholder="Confirm Password">--%>
                                        <asp:TextBox class="form-control" runat="server" TextMode="Password" ID="txtConfirmPassword"
                                            Onchange="return CheckPassword();" placeholder="Confirm Password"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <%--<div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Member type</label>
                                        <%--<select class="form-control">
                              <option>Administrator</option>
                          </select>--%>
                                        <%--<asp:DropDownList ID="ddlMemberType" runat="server" class="form-control" placeholder="Division">
                                        </asp:DropDownList>
                                    </div>
                                </div>--%>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Location</label>
                                        <%--<select class="form-control">
                              <option>Location</option>
                          </select>--%>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control" placeholder="Location">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="height: 250px; width: 300px; margin-top: -0.5px; border: solid 1px" class="grdScrlTrans">
                                      <%--  <asp:GridView ID="gvUserRoleDetails" Width="100%" runat="server" AllowPaging="true"
                                            DataKeyNames="Role_id" EmptyDataText="No Role Details!" AutoGenerateColumns="false"
                                            PageSize="50" GridLines="none" CssClass="grdTable" CellPadding="1" CellSpacing="0">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="center" />
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text="Select" Style="color: Black"></asp:Label>
                                                        <asp:CheckBox runat="server" ID="HTchkSelectAll" onClick="return SelectAllRoles(this.id);">
                                                        </asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemStyle />
                                                    <ItemTemplate> 
                                                        <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" Checked='<%# Eval("urd_status").ToString() == "A"%>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <HeaderTemplate>
                                                        <label style="float: left">
                                                            Role Name
                                                        </label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblRolName" Style="text-align: left" runat="server" Text='<%# Eval("Role_name") %>'
                                                            CssClass="label" ToolTip='<%# Eval("Role_name") %>' Width="160px"></asp:Label>
                                                        <asp:HiddenField ID="hdfRoleId" runat="server" Value='<%# Bind("Role_id") %>'></asp:HiddenField>
                                                        <asp:HiddenField ID="hdfExistStatus" runat="server" Value='<%# Bind("urd_status") %>'>
                                                        </asp:HiddenField>
                                                        <asp:HiddenField ID="IThdfURDId" runat="server" Value='<%# Bind("urd_id") %>'></asp:HiddenField>
                                                        <asp:HiddenField ID="IThdfUrdRoleId" runat="server" Value='<%# Bind("urd_role_id") %>'>
                                                        </asp:HiddenField>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div role="tabpanel" class="tab-pane " id="personal-info">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Nationality</label>
                                        <%--<select class="form-control">
                            <option>Select</option>
                            <option>Indian</option>
                            <option>Indian</option>
                            <option>Indian</option>
                          </select>--%>
                                        <asp:DropDownList ID="ddlNationality" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Passport #</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Passport #">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtPassportNo" placeholder="Passport #"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Date of Issue</label>
                                        <div class="input-group">
                                            <%--<input type="text" class="form-control" id="" placeholder="DD / MM / YY">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>--%>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDOI" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer1" style="position: absolute; top: 200x; left: 5%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <%--  <td> <asp:TextBox ID="txtDOITime" runat="server" CssClass="form-control pull-left" Width="60"></asp:TextBox> </td>--%>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar('callContainer1')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Valid Till</label>
                                        <div class="input-group">
                                            <%--<input type="text" class="form-control" id="" placeholder="DD / MM / YY">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>--%>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDOE" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer2" style="position: absolute; top: 200x; left: -30%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <%--  <td> <asp:TextBox ID="txtDOITime" runat="server" CssClass="form-control pull-left" Width="60"></asp:TextBox> </td>--%>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar2('callContainer2')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Country of Issue</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Place of Issue">--%>
                                        <%--<asp:TextBox class="form-control" runat="server" ID="txtPlaceOfIssue" placeholder="Place of Issue"></asp:TextBox>--%>
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlcountryOfIssue">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Residency</label>
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlResidency">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            ExecAssistance</label>
                                       <asp:TextBox class="form-control" runat="server" ID="txtExecAssistance" placeholder="ExecAssistance"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                           DeligateSupervisor</label>
                                       <asp:TextBox class="form-control" runat="server" ID="txtDeligateSupervisor" placeholder="DeligateSupervisor"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Date of Birth</label>
                                        <div class="input-group">
                                            <%--<input type="text" class="form-control" id="" placeholder="DD / MM / YY">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>--%>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer3" style="position: absolute; top: 200x; left: 5%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                    </td>
                                                    <%--  <td> <asp:TextBox ID="txtDOITime" runat="server" CssClass="form-control pull-left" Width="60"></asp:TextBox> </td>--%>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar3('callContainer3')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Place of Birth</label>
                                        <%--<div class="input-group">--%>
                                        <%--<input type="text" class="form-control" id="" placeholder="DD / MM / YY">--%>
                                        <%-- <asp:TextBox class="form-control" runat="server" ID="txtPlaceofBirth" placeholder="Place of Birth"></asp:TextBox>--%>
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlPlaceOfBirth">
                                        </asp:DropDownList>
                                        <%--<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>--%>
                                        <%-- </div>--%>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label class="center-block">
                                            Gender</label>
                                        <div class="radio">
                                            <%--<label class="radio-inline">
                                  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Male
                                </label>--%>
                                            <asp:RadioButton class="radio-inline" ID="rdbMale" runat="server" Text="Male" GroupName="Gender" />
                                            <%--<label class="radio-inline">
                                  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Female
                                </label>--%>
                                            <asp:RadioButton class="radio-inline" ID="rdbFeMale" runat="server" Text="FeMale"
                                                GroupName="Gender" />
                                        </div>
                                    </div>
                                </div>
                                  <div class="col-xs-12 col-sm-4 col-lg-3"> 
                                    <div class="form-group">
                                        <label> Martial Status: </label>
                                         <asp:DropDownList ID="ddlMartialStatus" runat="server" class="form-control">
                                             <asp:ListItem Text="--Select Martial Status--" Value="" />
                                              <asp:ListItem Text="Married" Value="Married" />
                                             <asp:ListItem Text="Single" Value="Single" />
                                        </asp:DropDownList>
                                        
                                        
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        VISA DETAILS</h4>
                                </div>
                            </div>
                            <div id="errMess1" style="display: none; color: Red; font-weight: bold; text-align: center;">
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Visa Country. <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" runat="server" ID="ddlVisaIssueCntry">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Visa No. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtVisaNo"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Issue Date. <span class="fcol_red">*</span></label>
                                        <uc1:DateControl ID="dcVisaIssueDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Expiry Date. <span class="fcol_red">*</span></label>
                                        <uc1:DateControl ID="dcVisaExpDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           Place Of Issue. <span class="fcol_red">*</span></label>
                                       <asp:DropDownList runat="server" ID="ddlvisaplaceofissue" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addVisaDetails();" id="addVisaDetails"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelVisaDetails" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript: cancelVisaDetails();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: Green" id="updateVisaDetails" href="javascript:void(0);"
                                                class="add-more" onclick="return updateVisaDetails();"><span class=" glyphicon glyphicon-ok">
                                                </span></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="visaDetailsChildDiv">
                                    </div>
                                </div>
                            </div>
                              <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        NATIONAL ID DETAILS</h4>
                                </div>
                            </div>
                          <%--  <div id="errMess2" style="display: none; color: Red; font-weight: bold; text-align: center;">
                            </div>--%>
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                       <div class="form-group">
                                        <label for="">
                                            National ID No.(Ex:Emirates ID) <span class="fcol_red"></span></label>
                                        <asp:TextBox CssClass="form-control"  runat="server" ID="txtNationalIDNo" placeholder="National ID No"  MaxLength="20"></asp:TextBox>
                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Expiry Date. <span class="fcol_red"></span></label>
                                        <uc1:DateControl ID="dcNationalIDExpDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                                
                                
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        TAX DETAILS</h4>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                           Number. <span class="fcol_red">*</span></label>
                                       <asp:TextBox runat="server" ID="txtGSTNumber" class="form-control" placeholder="Number"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Email. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTEmail" placeholder="Email"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Name. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTName" placeholder="Name"></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Phone. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTPhone" placeholder="Phone"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-xs-6 col-sm-6 col-md-4">
                                    <div class="form-group">
                                       <div class="form-group">
                                        <label for="">
                                               Address.  <span class="fcol_red"></span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTAddress" placeholder="Address" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
                                    </div>
                                    </div>
                                </div>
                                </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="travel-info">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Domestic Eligibility</label>
                                        <asp:DropDownList runat="server" ID="ddlDomestic" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            International Eligibility</label>
                                        <asp:DropDownList runat="server" ID="ddlinternational" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Seat Preference</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Seat Preference">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtSeatPref" placeholder="Seat Preference"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Meal Request</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Meal Request">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtMealRequest" placeholder="Meal Request"></asp:TextBox>
                                    </div>
                                </div>
                                   <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                           GDSProfilePNR</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtGDSProfilePNR" placeholder="GDS Profile PNR"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                                                        <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                       GDS SETTINGS</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            GDS. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDS"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           Description. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSDescription"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            OwnerPCC. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSOwnerPCC"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Queue No. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSQueueNo"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           extraCommand. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSextraCommand"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           corporateSSR. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDScorporateSSR"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           OSI. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSOSI"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           Remraks. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSRemraks" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addVisaDetails();" id="addVisaDetails"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelVisaDetails" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript: cancelVisaDetails();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: Green" id="updateVisaDetails" href="javascript:void(0);"
                                                class="add-more" onclick="return updateVisaDetails();"><span class=" glyphicon glyphicon-ok">
                                                </span></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="visaDetailsChildDiv">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="hotel-info">
                            <div class="row">
                                <div class="col-xs-12 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Room Type</label>
                                        <%--<select class="form-control">
                            <option>Select</option>
                          </select>--%>
                                        <asp:DropDownList ID="ddlRoomType" runat="server" class="form-control">
                                            <asp:ListItem Text="Select Roomtype" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Standard Single" Value="STDS"></asp:ListItem>
                                            <asp:ListItem Text="Delux Single" Value="DLXS"></asp:ListItem>
                                            <asp:ListItem Text="Standard Double" Value="STDD"></asp:ListItem>
                                            <asp:ListItem Text="Delux Double" Value="DLXD"></asp:ListItem>
                                            <asp:ListItem Text="Suit" Value="SUIT"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <label for="">
                                            Remarks/Comments</label>
                                        <%--<textarea class="form-control"></textarea>--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtRemarks" placeholder="Remarks/Comments"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="card-info">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        FREQUENT FLYER</h4>
                                </div>
                            </div>
                            <div class="row freqflyer-wrapper">
                                <div class="col-xs-12 col-sm-4  col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Airline</label>
                                        <%--<select class="form-control">
                            <option>Select</option>
                          </select>--%>
                                        <asp:DropDownList ID="ddlAirLine" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-10 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Card #</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Card Number">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtAirCardNo" placeholder="Card Number"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <%--<a href="javascript:void(0);" id="Add-More" class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a>--%>
                                    <asp:Button CssClass="btn-link" runat="server" Text="Add" ID="btnAddFF" OnClientClick="return AddDetails('FF');" />
                                    <asp:Button CssClass="btn-link" ID="btnEditFF" Text="Update" OnClientClick="return UpdateItems('FF')"
                                        runat="server" />
                                    <asp:ListBox runat="server" ID="lstFF" Width="300px" EnableViewState="true" onchange="return EditItems('FF')">
                                    </asp:ListBox>
                                    <asp:Button CssClass="btn-link" ID="btnRemoveFF" Text="Remove" OnClientClick="return removeItem('FF')"
                                        runat="server" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        HOTEL MEMBERSHIP</h4>
                                </div>
                            </div>
                            <div class="row freqflyer-wrapper">
                                <div class="col-xs-12 col-sm-4  col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Hotel</label>
                                        <%--<select class="form-control">
                            <option>Select</option>
                          </select>--%>
                                        <asp:DropDownList ID="ddlHotel" runat="server" class="form-control">
                                            <asp:ListItem Text="Select Hotel" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Hotel California" Value="HTLC"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-10 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Card #</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Card Number">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtHotelCardNo" placeholder="Card Number"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <%--<a href="javascript:void(0);" id="Add-More" class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a>--%>
                                    <asp:Button CssClass="btn-link" runat="server" Text="Add" ID="btnAddHM" OnClientClick="return AddDetails('HM');" />
                                    <asp:Button CssClass="btn-link" ID="btnUpdateHM" Text="Update" OnClientClick="return UpdateItems('HM')"
                                        runat="server" />
                                    <asp:ListBox runat="server" ID="lstHM" Width="300px" EnableViewState="true" onchange="return EditItems('HM')">
                                    </asp:ListBox>
                                    <asp:Button CssClass="btn-link" ID="btnRemoveHM" Text="Remove" OnClientClick="return removeItem('HM')"
                                        runat="server" />
                                </div>
                            </div>
                            <h4>
                                CREDIT CARD</h4>
                            <div class="row freqflyer-wrapper">
                                <div class="col-xs-12 col-sm-4  col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Card #</label>
                                        <%-- <input type="text" class="form-control" id="" placeholder="Card Number">--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtCCcardNo" placeholder="Card Number"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-10 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Card Expiry</label>
                                        <div class="input-group">
                                            <%--<input type="text" class="form-control" id="" placeholder="DD / MM / YY">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>--%>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtCardDOE" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer4" style="position: absolute; top: 200x; left: 5%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                    </td>
                                                    <%--  <td> <asp:TextBox ID="txtDOITime" runat="server" CssClass="form-control pull-left" Width="60"></asp:TextBox> </td>--%>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar4('callContainer4')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <%-- <a href="javascript:void(0);" id="Add-More" class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a>--%>
                                    <asp:Button CssClass="btn-link" runat="server" Text="Add" ID="btnAddCC" OnClientClick="return AddDetails('CC');" />
                                    <asp:Button CssClass="btn-link" ID="btnUpdateCC" Text="Update" OnClientClick="return UpdateItems('CC')"
                                        runat="server" />
                                    <asp:ListBox runat="server" ID="lstCC" Width="300px" EnableViewState="true" onchange="return EditItems('CC')">
                                    </asp:ListBox>
                                    <asp:Button CssClass="btn-link" ID="btnRemoveCC" Text="Remove" OnClientClick="return removeItem('CC')"
                                        runat="server" />
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="other-remarks">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <label for="">
                                            Travel History</label>
                                        <%--<textarea class="form-control"></textarea>--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtOtherRemarks" TextMode="MultiLine"
                                            placeholder="Travel History"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="additional-details">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hdnFlexCount" runat="server" Value="0" />
                                        <%--  <div class="col-md-12  padding-0 marbot_10" id="tblFlexFields" runat="server" >
                          <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                        <%--</div>--%>
                                        <table id="tblFlexFields" runat="server" width="100%" cellpadding="0" cellspacing="0"
                                            border="0">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvFlexDetails" Width="100%" runat="server" AllowPaging="true" DataKeyNames="FLEXID"
                                                        EmptyDataText="No FLEX DETAILS!" AutoGenerateColumns="false" PageSize="50" GridLines="none"
                                                        CssClass="grdTable" CellPadding="4" CellSpacing="0" ShowFooter="true" OnRowDataBound="gvFlexDetails_RowDataBound">
                                                        <HeaderStyle BackColor="Gray" HorizontalAlign="Left"></HeaderStyle>
                                                        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                                        <AlternatingRowStyle />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderStyle HorizontalAlign="left" />
                                                                <HeaderTemplate>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="IThdfFlexId" runat="server" Value='<%# Eval("flexId") %>' />
                                                                    <asp:HiddenField ID="IThdfFlexMandatoryYN" runat="server" Value='<%# Eval("flexMandatoryStatus") %>' />
                                                                    <asp:HiddenField ID="IThdfFlexControl" runat="server" Value='<%# Eval("flexControl") %>' />
                                                                    <asp:HiddenField ID="IThdfFlexQry" runat="server" Value='<%# Eval("flexSqlQuery") %>' />
                                                                    <asp:HiddenField ID="IThdfDetailID" runat="server" Value='<%# Eval("DetailId") %>' />
                                                                    <asp:HiddenField ID="IThdfFlexDataType" runat="server" Value='<%# Eval("flexDataType") %>' />
                                                                    <asp:HiddenField ID="IThdfFlexLabel" runat="server" Value='<%# Eval("flexLabel") %>' />
                                                                      <asp:HiddenField ID="IThdfFlexvalid" runat="server" Value='<%# Eval("flexValId") %>' />
                                                                    <asp:HiddenField ID="IThdfFlexGDSprefix" runat="server" Value='<%# Eval("flexGDSprefix") %>' />
                                                                    <asp:Label ID="ITlblLabel" runat="server" Width="110px" Enabled="true" CssClass="label"
                                                                        Text='<%# Eval("flexLabel") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle HorizontalAlign="left" />
                                                                <HeaderTemplate>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="left" />
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="IThdfFlexData" runat="server" Value='<%# Eval("Detail_FlexData") %>' />
                                                                    <asp:TextBox ID="ITtxtFlexData" runat="server" Width="220px" Enabled="true" CssClass="form-control"
                                                                        MaxLength="20"></asp:TextBox>
                                                                    <asp:DropDownList ID="ITddlFlexData" runat="server" Width="220px" Enabled="true"
                                                                        CssClass="form-control" MaxLength="20">
                                                                    </asp:DropDownList>
                                                                    <table runat="server" id="ITtblFlexDate">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DropDownList ID="ITddlDay" runat="server" Width="70px" Enabled="true" CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ITddlMonth" runat="server" Width="70px" Enabled="true" CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ITddlYear" runat="server" Width="80px" Enabled="true" CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--<label for="">Other Remarks</label>
                          <%--<textarea class="form-control"></textarea>--%>
                                        <%--<asp:TextBox class="form-control" runat="server" ID="TextBox1" TextMode="MultiLine"  placeholder="Other Remarks" ></asp:TextBox>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="policy-info">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hdnPolicyCount" runat="server" Value="0" />
                                        <%--  <div class="col-md-12  padding-0 marbot_10" id="tblFlexFields" runat="server" >
                          <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                        <%--</div>--%>
                                        <table id="tblPolicyFields" runat="server" width="100%" cellpadding="0" cellspacing="0"
                                            border="0">
                                            <tr>
                                                <td>
                                                    <label>
                                                        Policies</label>
                                                </td>
                                                <td>
                                                    <asp:CheckBoxList ID="chkPolicies" runat="server">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Approvals Tab-->
                        <div role="tabpanel" class="tab-pane" id="approvals">
                            <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        TICKET</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Approver Name <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlTicketApprover" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Order <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlTicketOrder" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addTicketApprovals()" id="addTicketApprovals"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelTicketApprovals" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript:cancelTicketApprover();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: green;" id="updateTicketApprovals" href="javascript:void(0);"
                                                class="add-more" onclick="return updateTicketApprovals();"><span class=" glyphicon glyphicon-ok">
                                                </span></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        EXPENSE</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Approver Name <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlExpenseApprover" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Order <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlExpenseOrder" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addExpenseApprovals()" id="addExpenseApprovals"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelExpenseApprovals" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript: cancelExpenseApprovals();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: none; color: Green" id="updateExpenseApprovals"
                                                href="javascript:void(0);" class="add-more" onclick="return updateExpenseApprovals();">
                                                <span class=" glyphicon glyphicon-ok"></span></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        VISA</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Approver Name <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlVisaApprovers" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Order <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlVisaOrder" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addVisaApprovals()" id="addVisaApprovals"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelVisaApprovals" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript: cancelVisaApprovals();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: none; color: Green" id="updateVisaApprovals"
                                                href="javascript:void(0);" class="add-more" onclick="return updateVisaApprovals();">
                                                <span class=" glyphicon glyphicon-ok"></span></a>
                                </div>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs responsive" role="tablist">
                                        <li role="presentation"><a id="aTicketApprovers" href="#ticketApprovers" aria-controls="tab-7"
                                            role="tab" data-toggle="tab">Ticket Approvers</a> </li>
                                        <li role="presentation"><a id="aExpenseApprovers" href="#expenseApprovers" aria-controls="tab-8"
                                            role="tab" data-toggle="tab">Expense Approvers</a> </li>
                                        <li role="presentation"><a id="aVisaApprovers" href="#visaApprovers" aria-controls="tab-9"
                                            role="tab" data-toggle="tab">Visa Approvers</a> </li>
                                    </ul>
                                    <div class="tab-content responsive">
                                        <div role="tabpanel" class="tab-pane" id="ticketApprovers">
                                            <div id="ticketApproversChildDiv">
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="expenseApprovers">
                                            <div id="expenseApproversChildDiv">
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="visaApprovers">
                                            <div id="visaApproversChildDiv">
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: block" class="tab-pane">
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- EOf approvals tab-->
                        <div role="tabpanel" class="tab-pane" id="documents">
                            <div class="row">
                                <!-- Fine Uploader New/Modern CSS file
    ====================================================================== -->
                                <link href="fileuploader/fine-uploader-new.css" rel="stylesheet">
                                <!-- Fine Uploader JS file
    ====================================================================== -->

                                <script src="fileuploader/fine-uploader.js"></script>

                                <!-- Fine Uploader Thumbnails template w/ customization
    ====================================================================== -->

                                <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Select files</div>
                </div>
                
                
                
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
                                </script>

                                <style>
                                    #trigger-upload {
                                        color: white;
                                        background-color: #00ABC7;
                                        font-size: 14px;
                                        padding: 7px 20px;
                                        background-image: none;
                                    }

                                    #fine-uploader-manual-trigger .qq-upload-button
                                    {
                                        margin-right: 15px;
                                    }
                                    #fine-uploader-manual-trigger .buttons
                                    {
                                        width: 36%;
                                    }
                                    #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container
                                    {
                                        width: 60%;
                                    }
                                    .custom-checkbox-table input[type="checkbox"] + label::before
                                    {
                                        border: solid 1px #ccc;
                                    }
                                </style>
                                <div class="col-md-12">
                                    <div id="fine-uploader-manual-trigger">
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    $(document).ready(function() {
                                       createFileUploader();
                                    });
                                    function createFileUploader() {
                                        manualUploader = new qq.FineUploader({
                                            element: document.getElementById('fine-uploader-manual-trigger'),
                                            template: 'qq-template-manual-trigger',
                                            request: {
                                                endpoint: 'CorpFileUploadHandler.ashx'
                                            },
                                            thumbnails: {
                                                placeholders: {
                                                    waitingPath: 'fileuploader/waiting-generic.png',
                                                    notAvailablePath: 'fileuploader/not_available-generic.png'
                                                }
                                            },
                                            autoUpload: false,
                                            debug: true
                                        });
                                    }
                                </script>

                                <div class="col-md-12">
                                    <h4>
                                        Uploaded Documents
                                    </h4>
                                    <table class="table table-bordered b2b-corp-table" id="tblDocRecords">
                                        <tbody id="bookingRecords">
                                            <tr>
                                                <th>
                                                    DOC NAME
                                                </th>
                                                <th>
                                                    ACTION
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <%--  <link rel="stylesheet" href="css/fontawesome/css/font-awesome.min.css">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped custom-checkbox-table">
                                                <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="custom-checkbox-table">
                                                                <input id="chk1" name="chk1" type="checkbox"><label for="chk1">Passport copy</label></span>
                                                        </td>
                                                        <td>
                                                            <a href="#"><i class="fa fa-eye"></i>View </a>
                                                        </td>
                                                        <td align="right">
                                                            <a href="#"><i class="fa fa-download "></i>Download </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="custom-checkbox-table">
                                                                <input id="chk2" name="chk2" type="checkbox"><label for="chk2">Passport copy</label></span>
                                                        </td>
                                                        <td>
                                                            <a href="#"><i class="fa fa-eye"></i>View </a>
                                                        </td>
                                                        <td align="right">
                                                            <a href="#"><i class="fa fa-download "></i>Download </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="custom-checkbox-table">
                                                                <input id="chk3" name="chk3" type="checkbox"><label for="chk3">Passport copy</label></span>
                                                        </td>
                                                        <td>
                                                            <a href="#"><i class="fa fa-eye"></i>View </a>
                                                        </td>
                                                        <td align="right">
                                                            <a href="#"><i class="fa fa-download "></i>Download </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="custom-checkbox-table">
                                                                <input id="chk4" name="chk4" type="checkbox"><label for="chk4">Passport copy</label></span>
                                                        </td>
                                                        <td>
                                                            <a href="#"><i class="fa fa-eye"></i>View </a>
                                                        </td>
                                                        <td align="right">
                                                            <a href="#"><i class="fa fa-download "></i>Download </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <!--Contant Info-->
                          <div role="tabpanel" class="tab-pane" id="contact-info">
                                              <div class="row">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Country</label>
                                          <asp:DropDownList CssClass="form-control" runat="server" ID="ddlcontactcountry">
                                        </asp:DropDownList>
                                    </div>
                            </div>
                                                         <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            State</label>
                                          <asp:TextBox runat="server" ID="txtcontactState" placeholder="Contact State" CssClass="form-control"></asp:TextBox>
                                    </div>
                            </div>
                                                      <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            City</label>
                                          <asp:TextBox runat="server" ID="txtcontactCity" placeholder="Contact City" CssClass="form-control"></asp:TextBox>
                                    </div>
                            </div>
                                                    <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Street</label>
                                          <asp:TextBox runat="server" ID="txtcontactstreet" placeholder="Contact street" CssClass="form-control"></asp:TextBox>
                                    </div>
                            </div>
                              </div>
                                  <div class="row">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Pincode</label>
                                        <asp:TextBox runat="server" ID="txtcontactpincode" placeholder="Contact Pincode" CssClass="form-control"></asp:TextBox>
                                    </div>
                            </div>
                                                         <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Phone</label>
                                          <asp:TextBox runat="server" ID="txtContactPhone" placeholder="Contact Phone" CssClass="form-control"></asp:TextBox>
                                    </div>
                            </div>
                                                      <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            CCEmail</label>
                                          <asp:TextBox runat="server" ID="txtContactccemail" placeholder="CCEmail" CssClass="form-control"></asp:TextBox>
                                    </div>
                            </div>
                                                    <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Email Notitfication</label>
                                         <asp:RadioButton runat="server" ID="rbncontactEmailNotification" ssClass="form-control"  />
                                    </div>
                            </div>
                              </div>
                              </div>
                    </div>
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <%--<button type="button" class="btn btn-primary">SAVE</button>
                   <button type="button" class="btn btn-default ">CLEAR</button>--%>
                            <!--OnClientClick="return Save();" -->
                            <asp:Button CssClass="btn btn-primary" runat="server" Text="SAVE" ID="btnSave" OnClick="btnSave_Click"
                                OnClientClick="return Save();" />
                            <asp:Button CssClass="btn btn-default" runat="server" Text="CLEAR" ID="btnClear"
                                OnClick="btnClear_Click" />
                            <asp:Button CssClass="btn btn-default" runat="server" Text="SEARCH" ID="btnSearch"
                                OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //  (function($) {
        //      fakewaffle.responsiveTabs(['xs', 'sm', 'md']);



        //  })(jQuery);

     
        function validateDuplicate(type, index) {

            var displayText = "";
            var displayValue = "";
            var cardValue = "";
            var detailId = 0;
            var listBox = "";
            var listBoxText = "";
            var opt = document.createElement("option");
            if (type == "FF") {
                if (getElement("ddlAirLine").selectedIndex > 0 || getElement('txtAirCardNo').value != '') {
                    var ddlAir = getElement("ddlAirLine");
                    displayText = ddlAir.options[ddlAir.selectedIndex].innerHTML;
                    displayValue = getElement('ddlAirLine').value;
                    cardValue = getElement('txtAirCardNo').value;
                    detailId = getElement('hdfFFId').value;
                    opt.text = displayText + '--' + cardValue; ;
                    listBox = getElement("lstFF");
                    for (var i = 0; i < listBox.options.length; i++) {

                        listBoxText = listBox.options[i].innerHTML;
                        if (i != index) {
                            if (opt.text == listBoxText) {
                                alert('Airline and Card No combinations already exists');
                                return false;
                            }
                        }
                    }
                }
            }
            else if (type == "HM") {
                if (getElement("ddlHotel").selectedIndex > 0 && getElement('txtHotelCardNo').value != '') {
                    var ddlHtl = getElement("ddlHotel");
                    displayText = ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                    displayValue = getElement('ddlHotel').value;
                    cardValue = getElement('txtHotelCardNo').value;
                    detailId = getElement('hdfHMId').value;
                    opt.text = displayText + '--' + cardValue; ;
                    listBox = getElement("lstHM");
                    for (var i = 0; i < listBox.options.length; i++) {

                        listBoxText = listBox.options[i].innerHTML;
                        if (i != index) {
                            if (opt.text == listBoxText) {
                                alert('Hotel and Card No combinations already exists');
                                return false;
                            }
                        }
                    }
                }
            }
            else if (type == "CC") {
                if (getElement('txtCCcardNo').value != '' && getElement('txtCardDOE').value != '') {
                    //var ddlHtl = getElement("ddlHotel");

                    displayText = getElement("txtCCcardNo").value;              //ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                    displayValue = getElement("txtCCcardNo").value;
                    cardExpDate = getElement('txtCardDOE').value;
                    detailId = getElement('hdfCCId').value;
                    opt.text = displayText + '--' + cardExpDate; ;
                    listBox = getElement("lstCC");
                    for (var i = 0; i < listBox.options.length; i++) {

                        listBoxText = listBox.options[i].innerHTML;

                        if (opt.text == listBoxText) {
                            alert('Credit Card and Card DOE combinations already exists');
                            return false;
                        }
                    }

                }

            }
            return true;
        }

        function AddDetails(type) {
            //alert('hi');
            if (validateDuplicate(type, -5)) {
                var displayText = "";
                var displayValue = "";
                var cardValue = "";
                var detailId = 0;
                var opt = document.createElement("option");
                if (type == "FF") {
                    if (getElement("ddlAirLine").selectedIndex > 0 && getElement('txtAirCardNo').value != '') {
                        var ddlAir = getElement("ddlAirLine");
                        displayText = ddlAir.options[ddlAir.selectedIndex].innerHTML;
                        displayValue = getElement('ddlAirLine').value;
                        cardValue = getElement('txtAirCardNo').value;
                        detailId = getElement('hdfFFId').value;
                        opt.text = displayText + '--' + cardValue; ;
                        opt.value = detailId + '||' + displayValue + '||' + cardValue;
                        //alert(opt.value);
                        getElement("lstFF").options.add(opt);
                        $('#' + getElement('lstFF').id).select2('val', "0");
                        $('#' + getElement('ddlAirLine').id).select2('val', "0");
                        //ddlAir.selectedIndex = 0;
                        getElement('txtAirCardNo').value = '';
                    }
                    else {
                        alert('Airline/Credit Card no cannot be blank');
                        // return false;      
                    }
                }
                else if (type == "HM") {
                    if (getElement("ddlHotel").selectedIndex > 0 && getElement('txtHotelCardNo').value != '') {
                        var ddlHtl = getElement("ddlHotel");
                        displayText = ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                        displayValue = getElement('ddlHotel').value;
                        cardValue = getElement('txtHotelCardNo').value;
                        detailId = getElement('hdfHMId').value;
                        opt.text = displayText + '--' + cardValue; ;
                        opt.value = detailId + '||' + displayValue + '||' + cardValue;

                        getElement("lstHM").options.add(opt);
                        // alert(opt.value);
                        $('#' + getElement('lstHM').id).select2('val', "0");
                        $('#' + getElement('ddlHotel').id).select2('val', "0");
                        //ddlHtl.selectedIndex = 0;
                        getElement('txtHotelCardNo').value = '';
                    }
                    else {
                        alert('Hotel/Credit Card no cannot be blank');
                        // return false;      
                    }
                }
                else if (type == "CC") {
                    var txtCardDOE = getElement('txtCardDOE').value.split('/');
                    if (getElement('txtCardDOE').value != '' && (new Date(txtCardDOE[2], txtCardDOE[1] - 1, txtCardDOE[0]) < new Date(yyyy, mm - 1, dd)) ) {
                        alert('Card Expiry Date cannot be less then Current Date !');
                    
                    }
                   else if (getElement('txtCCcardNo').value != '' && getElement('txtCardDOE').value != '') {
                        //var ddlHtl = getElement("ddlHotel");
                       
                        displayText = getElement("txtCCcardNo").value;              //ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                        displayValue = getElement("txtCCcardNo").value;
                        cardExpDate = getElement('txtCardDOE').value;
                        detailId = getElement('hdfCCId').value;
                        opt.text = displayText + '--' + cardExpDate; ;
                        opt.value = detailId + '||' + displayValue + '||' + cardExpDate;

                        getElement("lstCC").options.add(opt);
                        $('#' + getElement('lstCC').id).select2('val', "0");
                        getElement("txtCCcardNo").value = '';
                        getElement('txtCardDOE').value = '';
                    }
                    else {
                        alert('Credit Card No/ Exp Date cannot be blank');
                    }
                }
                BindDetails(type);
                return false;
            }
            return false;
        }


        function removeItem(type) {
            var listControl = "";
            var deletedItems = ""
            if (type == "FF") {
                listControl = getElement("lstFF");
                var optionsList = '';
                deletedItems = getElement('hdfFFDeleted').value
                if (listControl.value.length > 0) {
                    var itemIndex = listControl.selectedIndex;
                    if (itemIndex >= 0) {
                        deletedItems += listControl.options[itemIndex].value + "&&";
                        listControl.remove(itemIndex);
                        $('#' + getElement('lstFF').id).select2('val', "0");
                        $('#' + getElement('ddlAirLine').id).select2('val', "0");
                        getElement('txtAirCardNo').value = "";
                    }
                    getElement('hdfFFDeleted').value = deletedItems;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                    //listFF.select();
                }

            }
            else if (type == "HM") {
                listControl = getElement("lstHM");
                var optionsList = '';
                deletedItems = getElement('hdfHMDeleted').value
                if (listControl.value.length > 0) {
                    var itemIndex = listControl.selectedIndex;
                    if (itemIndex >= 0) {
                        deletedItems += listControl.options[itemIndex].value + "&&";
                        listControl.remove(itemIndex);
                        $('#' + getElement('lstHM').id).select2('val', "0");
                        $('#' + getElement('ddlHotel').id).select2('val', "0");
                        getElement('txtHotelCardNo').value = "";
                    }
                    getElement('hdfHMDeleted').value = deletedItems;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                    //listFF.select();
                }

            }
            else if (type == "CC") {
                listControl = getElement("lstCC");
                var optionsList = '';

                if (listControl.value.length > 0) {
                    var itemIndex = listControl.selectedIndex;
                    if (itemIndex >= 0) {
                        deletedItems += listControl.options[itemIndex].value + "&&";
                        listControl.remove(itemIndex);
                        $('#' + getElement('lstCC').id).select2('val', "0");
                    }
                    getElement('hdfCCDeleted').value = deletedItems;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                    //listFF.select();
                }
            }

            BindDetails(type);
            return false;
        }




        function EditItems(type) {
            var listControl = "";
            var values = "";
            var itemIndex = "";
            if (type == "FF") {
                listControl = getElement("lstFF");
                itemIndex = listControl.selectedIndex;
                if (itemIndex >= 0) {
                    values = listControl.options[itemIndex].value;
                    var detailsVal = values.split('||');
                    getElement('hdfFFId').value = detailsVal[0];
                    //getElement('ddlAirLine').value = detailsVal[1];
                    $('#' + getElement('ddlAirLine').id).select2('val', detailsVal[1]);
                    getElement('txtAirCardNo').value = detailsVal[2];
                    getElement('hdfDetailsIndexFF').value = itemIndex;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                }

            }
            else if (type == "HM") {
                listControl = getElement("lstHM");
                itemIndex = listControl.selectedIndex;
                if (itemIndex >= 0) {
                    values = listControl.options[itemIndex].value;
                    var detailsVal = values.split('||');
                    getElement('hdfHMId').value = detailsVal[0];
                    //getElement('ddlHotel').value = detailsVal[1];
                    $('#' + getElement('ddlHotel').id).select2('val', detailsVal[1]);
                    getElement('txtHotelCardNo').value = detailsVal[2];
                    getElement('hdfDetailsIndexHM').value = itemIndex;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                }

            } if (type == "CC") {
                listControl = getElement("lstCC");
                itemIndex = listControl.selectedIndex;
                if (itemIndex >= 0) {
                    values = listControl.options[itemIndex].value;
                    var detailsVal = values.split('||');
                    getElement('hdfCCId').value = detailsVal[0];
                    getElement('txtCCcardNo').value = detailsVal[1];
                    getElement('txtCardDOE').value = detailsVal[2];
                    getElement('hdfDetailsIndexCC').value = itemIndex;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                }

            }


            return false;


        }

        function UpdateItems(type) {
            var displayText = "";
            var displayValue = "";
            var cardValue = "";
            var detailId = 0;

            var opt = document.createElement("option");
            var updateIndex = "-1";
            if (type == "FF") updateIndex = getElement('hdfDetailsIndexFF').value;
            else if (type == "HM") updateIndex = getElement('hdfDetailsIndexHM').value;
            else if (type == "CC") updateIndex = getElement('hdfDetailsIndexCC').value;

            if (updateIndex >= 0) {
                if (validateDuplicate(type, updateIndex)) {
                    if (type == "FF") {
                        //            var opt = document.createElement("option");            
                        var ddlAir = getElement("ddlAirLine");
                        displayText = ddlAir.options[ddlAir.selectedIndex].innerHTML;
                        displayValue = getElement('ddlAirLine').value;
                        cardValue = getElement('txtAirCardNo').value;
                        detailId = getElement('hdfFFId').value;
                        opt.text = displayText + '--' + cardValue; ;
                        opt.value = detailId + '||' + displayValue + '||' + cardValue;
                        //alert(opt.value); 
                        getElement("lstFF").options[updateIndex] = new Option(opt.text, opt.value, false, false);
                        $('#' + getElement('lstFF').id).select2('val', "0");
                        $('#' + getElement('ddlAirLine').id).select2('val', "0");
                        //ddlAir.selectedIndex = 0;
                        getElement('txtAirCardNo').value = '';
                        getElement('hdfDetailsIndexFF').value = "-1";
                    } else if (type == "HM") {
                        var ddlHtl = getElement("ddlHotel");
                        displayText = ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                        displayValue = getElement('ddlHotel').value;
                        cardValue = getElement('txtHotelCardNo').value;
                        detailId = getElement('hdfHMId').value;
                        opt.text = displayText + '--' + cardValue; ;
                        opt.value = detailId + '||' + displayValue + '||' + cardValue;
                        //alert(opt.value); 
                        getElement("lstHM").options[updateIndex] = new Option(opt.text, opt.value, false, false);
                        $('#' + getElement('lstHM').id).select2('val', "0");
                        $('#' + getElement('ddlHotel').id).select2('val', "0");
                        //ddlHtl.selectedIndex = 0;
                        getElement('txtHotelCardNo').value = '';
                        getElement('hdfDetailsIndexHM').value = "-1";
                    }
                    else if (type == "CC") {
                        var txtCardDOE = getElement('txtCardDOE').value.split('/');
                        if (getElement('txtCardDOE').value != '' && (new Date(txtCardDOE[2], txtCardDOE[1] - 1, txtCardDOE[0]) < new Date(yyyy, mm - 1, dd))) {                          
                                alert('Card Expiry Date cannot be less then Current Date !');
                        }
                        else {
                            // var opt = document.createElement("option");
                            displayText = getElement("txtCCcardNo").value;
                            displayValue = getElement("txtCCcardNo").value;
                            cardExpDate = getElement('txtCardDOE').value;
                            detailId = getElement('hdfCCId').value;
                            opt.text = displayText + '--' + cardExpDate;;
                            opt.value = detailId + '||' + displayValue + '||' + cardExpDate;
                            //alert(opt.value); 

                            getElement("lstCC").options[updateIndex] = new Option(opt.text, opt.value, false, false);
                            //ddlHtl.selectedIndex = 0;
                            $('#' + getElement('lstCC').id).select2('val', "0");
                            getElement('txtCCcardNo').value = '';
                            getElement('txtCardDOE').value = '';
                            getElement('hdfDetailsIndexCC').value = "-1";
                        }
                       
                    }
                }
                return false;
            }
            else {
                alert('No item selected to update');
                //listControl.focus();
                return false;
            }

            BindDetails(type);
            return false;


        }

        function BindDetails(type) {
            var listBox = ""
            var texts = "";
            var values = "";
            if (type == "FF") {
                var listBox = getElement("lstFF");
                for (var i = 0; i < listBox.options.length; i++) {
                    texts += listBox.options[i].innerHTML + "&&";
                    values += listBox.options[i].value + "&&";
                }

                getElement('hdfFFDisplayText').value = texts;
                getElement('hdfFFDisplayValue').value = values;
            }
            else if (type == "HM") {
                var listBox = getElement("lstHM");
                for (var i = 0; i < listBox.options.length; i++) {
                    texts += listBox.options[i].innerHTML + "&&";
                    values += listBox.options[i].value + "&&";
                }

                getElement('hdfHMDisplayText').value = texts;
                getElement('hdfHMDisplayValue').value = values;
                //alert(getElement('hdfHMDisplayText').value);
            }
            else if (type == "CC") {
                var listBox = getElement("lstCC");
                for (var i = 0; i < listBox.options.length; i++) {
                    texts += listBox.options[i].innerHTML + "&&";
                    values += listBox.options[i].value + "&&";
                }

                getElement('hdfCCDisplayText').value = texts;
                getElement('hdfCCDisplayValue').value = values;
            }
            // alert(getElement('hdfHMDisplayText').value);
            //alert(getElement('hdfHMDisplayValue').value);

        }

        function removeDuplicates(arr) {
            var tmp = [];
            for (var i = 0; i < arr.length; i++) {
                if (tmp.indexOf(arr[i]) == -1) {
                    tmp.push(arr[i]);
                }
            }
            return tmp;
        }


        /*
        Added by Somasekhar on 09/04/2018
        get Current Date in dd-mm-yyyy(06-10-2018) format

        */
//============================ 
        var currentDate = new Date();
        var dd = currentDate.getDate();
        var mm = currentDate.getMonth() + 1;
        var yyyy = currentDate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        currentDate = dd + '-' + mm + '-' + yyyy;
//====================================


        function Save() {
            var msg = '';


            var approversTicketOrder = [];
            var approversExpenseOrder = [];

            //Case-1: verify Ticket Approvers hierarchy order.

            if (document.getElementById('<% =hdnTicketApprovers.ClientID %>').value.length > 0) {
                var selTicketApprovers = document.getElementById('<% =hdnTicketApprovers.ClientID %>').value.split('|');
                for (var i = 0; i < selTicketApprovers.length; i++) {
                    approversTicketOrder[i] = parseInt(selTicketApprovers[i].split('^')[1].split('-')[1]);
                }
                var largest = 0;

                for (i = 0; i <= largest; i++) {
                    if (approversTicketOrder[i] > largest) {
                        largest = approversTicketOrder[i];
                    }
                }
                approversTicketOrder = removeDuplicates(approversTicketOrder);
                console.log(approversTicketOrder);
                console.log(approversTicketOrder.length);

                if (approversTicketOrder.length > 1) {

                    for (var i = 0; i <= largest - 1; i++) {

                        if (i == 0) {
                            if (approversTicketOrder[i + 1] - approversTicketOrder[i] != 1) {
                                msg = 'Ticket Approvers Hierarchy order mismatch';
                                break;
                            }
                        }
                        else {
                            if (approversTicketOrder[i] - approversTicketOrder[i - 1] != 1) {
                                msg = 'Ticket Approvers Hierarchy order mismatch';
                                break;
                            }
                        }
                    }
                }
                else {
                    console.log("inside else block");
                    if (largest - 1 != 0) {
                        msg = 'Ticket Approvers Hierarchy order mismatch';
                    }

                }
            }
            //Case-2: verify Expense Approvers hierarchy order.
            if (document.getElementById('<% =hdnExpenseApprovers.ClientID %>').value.length > 0) {
                var selTicketApprovers = document.getElementById('<% =hdnExpenseApprovers.ClientID %>').value.split('|');
                for (var i = 0; i < selTicketApprovers.length; i++) {
                    approversExpenseOrder[i] = parseInt(selTicketApprovers[i].split('^')[1].split('-')[1]);
                }
                var largest = 0;

                for (i = 0; i <= largest; i++) {
                    if (approversExpenseOrder[i] > largest) {
                        largest = approversTicketOrder[i];
                    }
                }
                approversExpenseOrder = removeDuplicates(approversExpenseOrder);
                console.log(approversExpenseOrder);
                console.log(approversExpenseOrder.length);

                if (approversExpenseOrder.length > 1) {

                    for (var i = 0; i <= largest - 1; i++) {

                        if (i == 0) {
                            if (approversExpenseOrder[i + 1] - approversExpenseOrder[i] != 1) {
                                msg = 'Expense Approvers Hierarchy order mismatch';
                                break;
                            }
                        }
                        else {
                            if (approversExpenseOrder[i] - approversExpenseOrder[i - 1] != 1) {
                                msg = 'Expense Approvers Hierarchy order mismatch';
                                break;
                            }
                        }
                    }
                }
                else {
                    console.log("inside else block");
                    if (largest - 1 != 0) {
                        msg = 'Expense Approvers Hierarchy order mismatch';
                    }
                }
            }
            //alert('hi');
            if (getElement('ddlAgent').selectedIndex == 0) msg += '\n Please Select Agent !';
            if (getElement('txtSurname').value == '') msg += '\n Surname can nnot be empty !';
            if (getElement('txtname').value == '') msg += '\n Name cannot be empty !';
            if (getElement('txtMiddleName').value == '') msg += '\n Middle Name cannot be empty !';
             //if (getElement('txtbatch').value == '') msg += '\n Batch cannot be empty !';
            if (getElement('ddlDesignation').selectedIndex == 0) msg += '\n Please select Designation !';
            if (getElement('txtEmpId').value == '') msg += '\n Employee ID cannot be empty  !';
            if (getElement('ddlDivision').selectedIndex == 0) msg += '\n Please select Division !';
            if (getElement('ddlCostCentre').selectedIndex == 0) msg += '\n Please select CostCentre !';

            if (getElement('txtTelPhone').value != '')
                if (getElement('txtPhoneCountryCode').value == '') msg += '\n Phone Country code cannot be empty  !';

            if (getElement('txtPhoneCountryCode').value != '')
                if (getElement('txtTelPhone').value == '') msg += '\n Phone code cannot be empty  !';

            if (getElement('txtMobileCoutryCode').value == '') msg += '\n Mobile Phone Country code cannot be empty  !';
            if (getElement('txtMobileNo').value == '') msg += '\n MobileNo cannot be empty  !';
            if (getElement('txtEmail').value == '') msg += '\n Email cannot be empty  !';

            if (getElement('ddlGrade').selectedIndex == 0) msg += '\n Please select Grade !';
            if (getElement('ddlProfileType').selectedIndex == 0) msg += '\n Please select Profile Type !';

            if (getElement('hdfMode').value == '0') {// Add mode
                
                if (getElement('txtLoginName').value == '') msg += '\n Login Name cannot be empty  !';
                if (getElement('txtPassword').value == '') msg += '\n Password cannot be empty  !';
                if (getElement('txtConfirmPassword').value == '') msg += '\n Confirm password cannot be empty  !';
                if (getElement('txtConfirmPassword').value != getElement('txtPassword').value) msg += '\n Password and Confirm password should be same !';
            }
            // // For Login Panel Validation
            //if (getElement('txtLoginName').value == '') msg += '\n Login Name cannot be empty  !';
            //if (getElement('hdfMode').value == '0') {
            //    if (getElement('txtPassword').value == '') msg += '\n Password Name cannot be blank !';
            //    if (getElement('txtConfirmPassword').value == '') msg += '\n Confirm Password Name cannot be blank !';
            //    else if (getElement('txtPassword').value != getElement('txtConfirmPassword').value) msg += '\n Password mismatch !';
            //}
            //if (getElement('ddlMemberType').selectedIndex <= 0) msg += '\n Please select Member Type !';
            //if (getElement('ddlLocation').selectedIndex <= 0) msg += '\n Please select Location !';
             // End Login Panel Validation
            if (getElement('ddlNationality').selectedIndex == 0) msg += '\n  Please select Nationality !';
            if (getElement('txtPassportNo').value == '') msg += '\n  Passport No cannot be blank !';
            if (getElement('txtDOI').value == '') msg += '\n  Date Of Issue cannot be blank !';
            //======== Added by Somasekhar on 09/04/2018 =========
            if (getElement('txtDOI').value != '') {
                var txtDOI = getElement('txtDOI').value.split('/');
               
                if (new Date(txtDOI[2], txtDOI[1] - 1, txtDOI[0]) > new Date(yyyy, mm - 1, dd)) {
                    msg += '\n Date of Issue cannot be greater then Current Date !';
                }
            }

            //==================================
            if (getElement('txtDOE').value == '') msg += '\n  Date Of Expiry cannot be blank !';
            //======== Added by Somasekhar on 09/04/2018 =========
            if (getElement('txtDOE').value != '') {
                var txtDOE = getElement('txtDOE').value.split('/');
               
                if (new Date(txtDOE[2], txtDOE[1] - 1, txtDOE[0]) < new Date(yyyy, mm - 1, dd)) {
                    msg += '\n Date Of Expiry cannot be less then Current Date !';
                }
            }
            
            if (getElement('txtDOI').value != '' && getElement('txtDOE').value != '')
            {
                var   txtDOI=getElement('txtDOI').value.split('/');
                var txtDOE = getElement('txtDOE').value.split('/');

                if(new Date(txtDOI[2], txtDOI[1] - 1, txtDOI[0])>new Date(txtDOE[2], txtDOE[1] - 1, txtDOE[0]))   
                {
                    msg += '\n Date of Issue cannot be greater then Valid Till  !';
                }
              
            }
            //===================================
            if (getElement('ddlcountryOfIssue').selectedIndex == 0) msg += '\n  country Of Issue cannot be blank !';
            if (getElement('txtDOB').value == '') msg += '\n  Date Of Birth cannot be blank !';
            //======== Added by Somasekhar on 09/04/2018 =========
            if (getElement('txtDOB').value != '') {
                var txtDOB = getElement('txtDOB').value.split('/');

                if (new Date(txtDOB[2], txtDOB[1] - 1, txtDOB[0]) > new Date(yyyy, mm - 1, dd)) {
                    msg += '\n Date of Birth cannot be greater then Current Date !';
                }
            }
            //===========================================================
            if (getElement('ddlPlaceOfBirth').selectedIndex == 0) msg += '\n  Place Of Birth cannot be blank !';
          //  if (getElement('ddlRoomType').selectedIndex <= 0) msg += '\n Please select Room Type !';
            if (getElement('dcJoiningDate_Date').value == '') msg += '\n  Date Of Joining cannot be blank !';
            //======== Added by Somasekhar on 09/04/2018 =========
         
            if (getElement('dcJoiningDate_Date').value != '')
            {
                var txtDOI = getElement('dcJoiningDate_Date').value.split('-');

                if (new Date(txtDOI[2], txtDOI[1] - 1, txtDOI[0]) > new Date(yyyy, mm - 1, dd)) {
                    msg += '\n  Date Of Joining cannot be greater then Current Date !';
                }
            }
            if (document.getElementById('<%=dcNationalIDExpDate.FindControl("Date").ClientID %>').value!= '') {
              
                var dcNationalIDExpDate = document.getElementById('<%=dcNationalIDExpDate.FindControl("Date").ClientID %>').value.split('-');

                if (new Date(dcNationalIDExpDate[2], dcNationalIDExpDate[1] - 1, dcNationalIDExpDate[0]) < new Date(yyyy, mm - 1, dd)) {
                    msg += '\n National ID Exp. Date cannot be less then Current Date !';
                }
            }
            //================================================
// removing flex vlaidation
            //if (msg == '') {
            //    if (ValidateFlexDetails())
            //        return true;
            //    else return false;
            //}

            if (msg != '') {
                alert(msg);
                return false;
            }
            return true;
        }
        //function CheckPassword() {
        //    var msg = '';
        //    if (getElement('txtPassword').value != getElement('txtConfirmPassword').value) {
        //        msg += 'Password Mismatch!';
        //        getElement('txtConfirmPassword').value = '';
        //        getElement('txtConfirmPassword').focus();
        //    }

        //    if (msg != '') {
        //        alert(msg);
        //        return false;
        //    }
        //    return true;
        //}


        //Validating TaxGrid Details
        function ValidateFlexDetails() {
            try {

                var msg = '';
                var labelName = '';
                var counts = getElement('gvFlexDetails').rows.length - 1;
                for (var i = 0; i < counts; i++) {

                    if (i + 2 < 10) {

                        rows = "0" + (i + 2);
                    }
                    else {

                        rows = i + 2;
                    }

                    if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexLabel') != null) {
                        labelName = document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexLabel').value;
                        if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexMandatoryYN').value == 'Y') {
                            // alert(document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexControl').value);
                            if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexControl').value == 'T') {
                                if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_ITtxtFlexData').value == '')
                                    msg += labelName + ' cannot be blank!\n';
                            }
                            else if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexControl').value == 'L') {
                                if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_ITddlFlexData').selectedIndex <= 0)
                                    msg += labelName + ' should be selected !\n';
                            }
                            else if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_IThdfFlexControl').value == 'L') {
                                if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_ITddlDay').selectedIndex <= 0)
                                    msg += labelName + ' is invalid !\n';
                                else if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_ITddlMonth').selectedIndex <= 0)
                                    msg += labelName + ' is invalid !\n';
                                else if (document.getElementById("ctl00_cphTransaction_gvFlexDetails_ctl" + rows + '_ITddlYear').selectedIndex <= 0)
                                    msg += labelName + ' is invalid !\n';
                            }
                        }
                    }

                }
                if (msg != '') {
                    alert('Additional Details :- \n' + msg);
                    return false;
                }
                return true;
            }
            catch (e) {
                return true;
            }
        }


        function ShowProfilePic() {
         
            document.getElementById('<%=imgProfilePic.ClientID %>').src = getElement('hdfImgPath').value;
        }


        //function SelectAllRoles() {
        //    var chkId = document.getElementById("ctl00_cphTransaction_gvUserRoleDetails_ctl01_HTchkSelectAll").id;
        //    var counts = getElement('gvUserRoleDetails').rows.length - 1;
        //    var chkAll = "";
        //    for (var i = 0; i < counts; i++) {

        //        if (i + 2 < 10) {

        //            rows = "0" + (i + 2);
        //        }
        //        else {

        //            rows = i + 2;
        //        }

        //        if (document.getElementById("ctl00_cphTransaction_gvUserRoleDetails_ctl" + rows + '_ITchkSelect') != null) {
        //            document.getElementById("ctl00_cphTransaction_gvUserRoleDetails_ctl" + rows + '_ITchkSelect').checked = document.getElementById(chkId).checked
        //        }
        //    }
        //    return true;
        //}
    </script>

</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ProfileId"
        EmptyDataText="No Corporate Profile List!" AutoGenerateColumns="false" PageSize="17"
        GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
        CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <%--<cc1:Filter   ID="HTtxtSurName" Width="70px" HeaderText="Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 --%>
                    <label>
                        Sur Name</label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblSurName" runat="server" Text='<%# Eval("SurName") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("SurName") %>' Width="70px"></asp:Label>
                    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <%--<cc1:Filter ID="HTtxtName"  Width="100px" CssClass="inputEnabled" HeaderText="Name" OnClick="FilterSearch_Click" runat="server" />  --%>
                    <label>
                        Name</label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("Name") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <%--<cc1:Filter ID="HTtxtEmpId"  Width="150px" CssClass="inputEnabled" HeaderText="Address" OnClick="FilterSearch_Click" runat="server" />                 --%>
                    <label>
                        Employee Id</label>
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblEmpId" runat="server" Text='<%# Eval("EmployeeId") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("EmployeeId") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <%--<cc1:Filter ID="HTtxtPhone"  Width="150px" CssClass="inputEnabled" HeaderText="Phone" OnClick="FilterSearch_Click" runat="server" />                 --%>
                    <label>
                        Phone</label>
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblPhone" runat="server" Text='<%# Eval("Telephone") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Telephone") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <%--<cc1:Filter ID="HTtxtMobPhone"  Width="150px" CssClass="inputEnabled" HeaderText="Phone" OnClick="FilterSearch_Click" runat="server" />                 --%>
                    <label>
                        Mobile Phone</label>
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblMobPhone" runat="server" Text='<%# Eval("Mobilephone") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Mobilephone") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>--%>
</asp:Content>
