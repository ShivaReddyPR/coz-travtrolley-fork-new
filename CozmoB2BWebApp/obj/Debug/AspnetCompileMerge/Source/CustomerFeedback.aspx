﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CustomerFeedback" Title="CustomerFeedback" Codebehind="CustomerFeedback.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script language="JavaScript" type="text/javascript" src="JSLib/Utils.js"></script>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script> 

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="ash.js"></script>

    <script type="text/javascript" src="js2/Common.js"></script>
     <script type="text/javascript" src="JSLib/DefaultPage.js"></script>

    <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">
    function init() {

        //    showReturn();
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal1.cfg.setProperty("title", "Select From Date");
        cal1.cfg.setProperty("close", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();

        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
        cal2.cfg.setProperty("title", "Select To Date");
        cal2.selectEvent.subscribe(setDate2);
        cal2.cfg.setProperty("close", true);
        cal2.render();
    }

    function showCalendar1() {
        document.getElementById('container2').style.display = "none";
        document.getElementById('container1').style.display = "block";
        init();

    }

    function showCalendar2() {

        document.getElementById('container1').style.display = "none";
        cal1.hide();
        init();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById("ctl00_cphTransaction_txtFrom").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');

            //  var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

            //cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            //cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal2.render();
        }
        document.getElementById('container2').style.display = "block";
    }


    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];

        document.getElementById('IShimFrame').style.display = "none";
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());

        departureDate = cal1.getSelectedDates()[0];
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        //			
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        var time = "";
        var timePM = "";

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");

        document.getElementById("ctl00_cphTransaction_txtFrom").value = day + "/" + (month) + "/" + date1.getFullYear();


        //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
        //cal2.render();

        cal1.hide();

    }
    function setDate2() {

        var date1 = document.getElementById("ctl00_cphTransaction_txtFrom").value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }

        var date2 = cal2.getSelectedDates()[0];
        var depDateArray = date1.split('/');

        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        //var thisTime = this.today.getHours();
        //var thisMinutes = this.today.getMinutes();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }

        document.getElementById("ctl00_cphTransaction_txtTo").value = day + "/" + month + "/" + date2.getFullYear();


        cal2.hide();
    }
    YAHOO.util.Event.addListener(window, "load", init);

    function Validate() {
        var date1 = document.getElementById("ctl00_cphTransaction_txtFrom").value;
        var date2 = document.getElementById("ctl00_cphTransaction_txtTo").value;

        var date3 = cal1.getSelectedDates()[0];
        var depDateArray1 = date1.split('/');
        var year1 = parseInt(depDateArray1[2]);

        var date4 = cal2.getSelectedDates()[0];
        var depDateArray2 = date2.split('/');
        var year2 = parseInt(depDateArray2[2]);

        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select From date.";
            return false;
        } else if (date2.length == 0 || date2 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select To date.";
            return false;
        } else if (!CheckValidDate(depDateArray1[0], depDateArray1[1], depDateArray1[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid From Date";
            return false;
        } else if (!CheckValidDate(depDateArray2[0], depDateArray2[1], depDateArray2[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        } else if (year1 < 1900 || year1 > 9999) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid From Date";
            return false;
        } else if (year2 < 1900 || year2 > 9999) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }
        else return true;


    }


    function viewDesc(id) {
        var rowId = id.substring(0, id.lastIndexOf('_'));
        document.getElementById('divDesc').style.display = "block";
        var positions = getRelativePositions(document.getElementById(id));
        document.getElementById('divDesc').style.left = (positions[0] - 300) + 'px';
        document.getElementById('divDesc').style.top = (positions[1] - 50) + 'px';
        var remarks = document.getElementById(rowId + '_ITIhdnDesc').value
        document.getElementById('ctl00_cphTransaction_lblDesc').innerHTML = remarks;
        return false;
    }
    function HideDesc() {
        document.getElementById('divDesc').style.display = "none";
        document.getElementById('ctl00_cphTransaction_lblDesc').value = "";
        return false;
    }


    function getRelativePositions(obj) {
        var curLeft = 0;
        var curTop = 0;
        if (obj.offsetParent) {
            do {
                curLeft += obj.offsetLeft;
                curTop += obj.offsetTop;
            } while (obj = obj.offsetParent);

        }
        return [curLeft, curTop];
    }
    </script>

<%--<div  style="margin-top:5px; width:1090px;border-left:1px solid #000; border-right:1px solid #000; border-bottom:1px solid #000" class="ns-h3">Customer FeedBAck  </div>       
--%>    
    <div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>

         <div  style="margin-top:10px;">
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
       
       
       <div class="marbot_20"> 
       
      <table cellpadding="0" cellspacing="0">
            <tr>
                <td>  <asp:Label  ID="lblFromDate" Text="From Date" runat="server"></asp:Label></td>
                
                <td> 
                
                <table> 
                
                <tr> 
                <td> <asp:TextBox ID="txtFrom" runat="server" Width="100" CssClass=" form-control"></asp:TextBox></td>
                
                <td> <img class="cursor_point" src="images/call-cozmo.png" alt="cal" onclick="showCalendar1('container1')" /></td>
                </tr>
                
                </table>
                
                
                 </td>
                 
                 
                <td> <asp:Label  ID="lblToDate" Text="To Date" runat="server" ></asp:Label> </td>
                
                 <td> 
                 
                 <table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>  <asp:TextBox ID="txtTo" runat="server" Width="100" CssClass=" form-control"></asp:TextBox>  </td>
    
    <td>  <img  class="cursor_point" src="images/call-cozmo.png" alt="cal" onclick="showCalendar2('container2')" />  </td>
  </tr>
</table>

                 
                 
                 
                  </td>
                  <td width="10"> </td>
                  
                  
                <td> 
                <asp:Button  runat="server" ID="btnSearch" Text="Search" CssClass="button"  OnClientClick="return Validate();" onclick="btnSearch_Click"  />
                         
                         
                         <asp:Button runat="server"  ID="btnClear" Text="Clear" 
                         CssClass="button" OnClick="btnClear_Click"  />
                
                
                </td>
               
            </tr> 
      
<tr>
<td>
 
</td>
    <td >         
              <div  id="DateBox" class="fleft padding-top-bottom" >
                           
                <div class="clear" style="position: absolute; top:110px; "><div id="container1" style="display:none;"></div></div>
              </div>
             
           </td>
           
         <td>
            
        </td>
        
        
         <td >         
              <div  id="Div1" class="fleft padding-top-bottom" >
                           
                <div class="clear" style="position: absolute; top:110px;"><div id="container2" style="display:none;"></div></div>
              </div>
             
           </td>
 
         
                <td></td>
                 <td  align="left">
                         </td>
            </tr>
        </table>
       
       
       </div>
       
       
       
      
        
        </asp:Panel>
    </div>
    
    
    
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                               <div title="Desc" id="divDesc" style="position:absolute;display:none; right:355px;top:250px;">
        <table style="border:solid 1px skyblue;background-color:White" border="0" width="200px" height="50px" >
        <tr >
            <td valign="top" align="left"><asp:Label ID="lblDesc" runat="server" ></asp:Label></td>
        </tr>
        <tr>
        <td colspan="2" align="left" valign="top">
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" Width="60px" Height="30px"  OnClientClick="return HideDesc();" />
          </td>
         </tr>
        </table>
    </div>
            <div id="Div2"  runat="server"   style="width:100%;  overflow:hidden; color: #000000; background-color: #FFFFFF;">
                <%--<div class="one" style="width: 776px; overflow: auto; height: 400px;">--%>
                   
                   
                    <asp:GridView  ID="gvFeedback" Width="100%" runat="server" AllowPaging="true" DataKeyNames="fedId"
                        EmptyDataText="No Feedback List!" AutoGenerateColumns="false" 
                        PageSize="2" GridLines="horizontal"
                        onpageindexchanging="gvFeedback_PageIndexChanging" 
                        onrowdatabound="gvFeedback_RowDataBound">
                      
                        <HeaderStyle CssClass="showMsgHeading" HorizontalAlign="Left"  Width="100%"></HeaderStyle>
                      
                        <RowStyle CssClass="" HorizontalAlign="left" />
                        <AlternatingRowStyle CssClass="repRow" />
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                <HeaderTemplate>
                                 <b>   <label style="color: white;">
                                        S.No</label></b>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' 
                                        ToolTip='<%# Container.DataItemIndex+1 %>' Width="40px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderStyle-HorizontalAlign="left">
                                <HeaderTemplate >
                                  <b>  <label style="color: white;">
                                        Date</label></b>
                                </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="ITlblDate" runat="server" Text='<%# Bind("createdOn", "{0:dd-MMM-yyyy}") %>' 
                                        ToolTip='<%# Eval("createdOn") %>' Width="90px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                             <HeaderStyle HorizontalAlign="Center" />
                                <HeaderTemplate>
                                  <b>  <label style="color: white;">
                                        Type</label></b>
                                </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="ITlblType" runat="server" Text='<%# Eval("fedType") %>' 
                                        ToolTip='<%# Eval("fedType") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                             <HeaderStyle HorizontalAlign="Center" />
                                <HeaderTemplate>
                                  <b>  <label style="color: white;">
                                        Pax Name</label></b>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITlblPaxName" runat="server" Text='<%# (Eval("fedName")) %>' 
                                        ToolTip='<%# Eval("fedName") %>' Width="190px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                             <HeaderStyle HorizontalAlign="Center" />
                                <HeaderTemplate>
                                 <b>  <label style="color: white;">
                                        Email</label></b>
                                </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="ITlblEmail" runat="server" Text='<%# Eval("fedEmailId") %>'
                                        ToolTip='<%# Eval("fedEmailId") %>' Width="230px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                               <HeaderStyle HorizontalAlign="Center" />
                                <HeaderTemplate>
                                  <b>  <label style="color: white;">
                                        Phone</label></b>
                                </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="ITlblPhone" runat="server" Text='<%# Eval("fedPhoneNumber") %>' 
                                        ToolTip='<%# Eval("fedPhoneNumber") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                              <HeaderStyle HorizontalAlign="Center" />
                                <HeaderTemplate>
                                  <b>  <label style="color: white;">
                                        Desription</label></b>
                                </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="ITlblDescription" style="" runat="server"
                                     Text='<%# Eval("fedDescription") %>' Width="210px"></asp:Label>
                                     
                                      <asp:HiddenField ID="ITIhdnDesc" runat="server" Value='<%# Eval("fedDescription") %>' />
    <asp:LinkButton ID="ITlnkdesc" OnClientClick="return viewDesc(this.id)" runat="server" Width="150px" CommandName="Select" CausesValidation="True" Text="View" Visible="false"></asp:LinkButton>
                                      
                                         
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                              <HeaderStyle HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    <b><label style="color: white;">
                                        Hear About</label></b>
                                </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="ITlblAbout" runat="server" Text='<%# Eval("fedhearabout") %>' 
                                        ToolTip='<%# Eval("fedhearabout") %>' Width="150px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    
                    </div>
                   <%-- </div>--%>
                </td>
            </tr>
        </table>
         <table>
                      <tr>
                          <td width="80px">
                              <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                                  CssClass="button" />
                          </td>
                          <td>
                          </td>
                      </tr>
                  </table>
    </div>
      <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

