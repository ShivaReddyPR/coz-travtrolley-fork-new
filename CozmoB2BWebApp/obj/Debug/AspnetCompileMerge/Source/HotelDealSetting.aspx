<%@ Page Language="C#" AutoEventWireup="true" Inherits="HotelDealSettingGUI" MasterPageFile="~/TransactionBE.master" Codebehind="HotelDealSetting.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%--<%@ Import Namespace="CT.HolidayDeals" %>--%>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">
<script type="text/javascript">
    function Search() {
        if (document.getElementById('active').checked || document.getElementById('inactive').checked) {
            document.getElementById('pageSubmit').value = "search";
            document.forms[0].submit();
        }
        else {
            document.getElementById('errorMessage').innerHTML = "";
            document.getElementById('errorMessage').innerHTML = "Please choose a filter";
            document.getElementById('errorMessage').style.display = "block";
        }
    }
    function Save() {
        if (isValid()) {
            document.getElementById('pageSubmit').value = "save";
            document.forms[0].submit();
        }
    }

    function isValid(id) {
        var re2digit = /^[0-9]+/
        var count = eval(document.getElementById('counter').value);
        var activeCount = eval(document.getElementById('activeCount').value);
        var pagingString = "";
        var dealArray = new Array(count);
        var pagingDealArray = new Array(activeCount);
        var pagingStringArray;
        var pagingSortedArray = new Array(activeCount + 1);
        for (var i = 0; i < activeCount + 1; i++) {
            pagingSortedArray[i] = 0;
        }  
        if (document.getElementById('colorRow1').value != "") {
            document.getElementById(document.getElementById('colorRow1').value).style.backgroundColor = "#d7dfec";
            document.getElementById('colorRow1').value = "";
        }
        if (document.getElementById('colorRow2').value != "") {
            document.getElementById(document.getElementById('colorRow2').value).style.backgroundColor = "#d7dfec";
            document.getElementById('colorRow2').value = "";
        }
        var activeDealCount = 0;
        for (var i = 0; i < count; i++) {
            if (document.getElementById('isActive-' + i).value == "true") {
                if (!re2digit.test(document.getElementById('paging-' + i).value)) {
                    document.getElementById('row-' + i).style.backgroundColor = "#E8EB57";
                    document.getElementById('colorRow1').value = "row-" + i;
                    alert("For Paging Order:" + document.getElementById('paging-' + i).value + ", Please enter proper paging order from 1-" + activeCount + ". The row has been marked yellow");
                    return false;
                }
                else {
                    //if (eval(document.getElementById('paging-' + i).value) > activeCount) { Temp commented
                    if (eval(document.getElementById('paging-' + i).value) > 500) {
                        document.getElementById('row-' + i).style.backgroundColor = "#E8EB57";
                        document.getElementById('colorRow1').value = "row-" + i;
                        alert("For Paging Order" + document.getElementById('paging-' + i).value + " the paging limit has exeeded. Please enter paging from 1-" + activeCount + ". The row has been marked yellow");
                        return false;
                    }
                    else {
                        if (!id && id != 0) {
                            pagingString += document.getElementById('paging-' + i).value + ',';
                            pagingDealArray[activeDealCount] = document.getElementById('paging-' + i).value;
                            activeDealCount++;
                        }
                        else if (i != eval(id)) {
                        pagingString += document.getElementById('paging-' + i).value + ',';
                        pagingDealArray[activeDealCount] = document.getElementById('paging-' + i).value;
                            activeDealCount++;
                        }
                    }
                }
            }
        }
        pagingString = pagingString.substring(0, (pagingString.length - 1));
        pagingStringArray = pagingString.split(',');

        for (var i = 0; i < pagingStringArray.length; i++) {
            if (eval(pagingStringArray[i]) == 0) {
                for (var k = 0; k < count; k++) {
                    if (document.getElementById('paging-' + k).value == pagingDealArray[i]) {
                        break;
                    }
                }
                document.getElementById('row-' + k).style.backgroundColor = "#E8EB57";
                document.getElementById('colorRow1').value = "row-" + k;
                alert("Paging number for active package cannot be 0. Please set paging number : " + pagingDealArray[i] + ". The row has been marked yellow.");
                return false;
            }
            else {
                pagingSortedArray[eval(pagingStringArray[i])] = eval(pagingStringArray[i]);
                for (var j = i + 1; j < pagingStringArray.length; j++) {
                    if (eval(pagingStringArray[j]) == 0) {
                        for (var k = 0; k < count; k++) {
                            if (document.getElementById('paging-' + k).value == pagingDealArray[j]) {
                                break;
                            }
                        }
                        document.getElementById('row-' + k).style.backgroundColor = "#E8EB57";
                        document.getElementById('colorRow1').value = "row-" + k;
                        alert("Paging number for active package cannot be 0. Please set paging number : " + pagingDealArray[j] + ". The row has been marked yellow.");
                        return false;
                    }
                    else {
                        if (eval(pagingStringArray[i]) == eval(pagingStringArray[j])) {
                            for (var k = 0; k < count; k++) {
                                if (document.getElementById('paging-' + k).value == pagingDealArray[i]) {
                                    break;
                                }
                            }
                            for (var m = 0; m < count; m++) {
                                if (document.getElementById('paging-' + m).value == pagingDealArray[j]) {
                                    break;
                                }
                            }
                            document.getElementById('row-' + k).style.backgroundColor = "#E8EB57";
                            document.getElementById('colorRow1').value = "row-" + k;
                            document.getElementById('row-' + m).style.backgroundColor = "#E8EB57";
                            document.getElementById('colorRow2').value = "row-" + m;
                            alert("Paging number clash at paging number:" + pagingDealArray[i] + " and " + pagingDealArray[j] + ". Value set is:" + pagingStringArray[j] + ". The affected rows are colored yellow. Plz scroll to find them.");
                            return false;
                        }
                    }
                }
            }
        }
        if (id || id == 0) {
            activeCount = activeCount - 1; // To reduce paging number by 1 when a package is set to inactive.
        }
        for (var i = 1; i < activeCount + 1; i++) {
            if (pagingSortedArray[i] == 0) {
                alert("Paging value:" + i + " is not set. Please set pagingvalues from 1-" + activeCount);
                return false;
            }
        }
        return true;
    }

    function CheckVal(i) {
        if (eval(document.getElementById('paging-' + i).value) == 0) {
            document.getElementById('stat2-' + i).innerHTML = "<a href=\"javascript:Inactivate(" + i + ")\">inActivate</a>";
        }
        else {
            document.getElementById('stat2-' + i).innerHTML = "inActivate";
        }
    }

    function Activate(id) {
        document.getElementById('submitPackageId').value = document.getElementById('Package-' + id).value;
        document.getElementById('pageSubmit').value = "activate";
        document.forms[0].submit();
    }

    function Inactivate(id) {
        if (eval(document.getElementById('paging-' + id).value) == 0) {
            if (isValid(id)) {
                document.getElementById('submitPackageId').value = document.getElementById('Package-' + id).value;
            document.getElementById('pageSubmit').value = "inactivate";
                document.forms[0].submit();
            }
        }
        else {
            alert("Please set paging order to 0 for package which you want to inactivate.");
            return false;
        }
    }

    function Delete(id) {
        document.getElementById('submitPackageId').value = document.getElementById('Package-' + id).value;
        document.getElementById('pageSubmit').value = "delete";
        document.forms[0].submit();
    }
</script>
<%--<form id="form1" action="hoteldealsetting.aspx?memberId=<%=member.MemberId%>&agencyId=<%=agency.AgencyId%>" method="post">--%>
<div class="ns-h3">Package Order</div>
<div>
             <div class="col-md-12 padding-0 marbot_10 bg_white bor_gray pad_10 paramcon">    
             
            <div class="col-md-12">
          
                <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.MemberType == CT.TicketReceipt.BusinessLayer.MemberType.ADMIN)
                  { %>
                
                <%--<a href="HolidayWhiteLabelSettings.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Setting</a>--%>
                <%--<a href="HolidayWhiteLabelSiteLayout.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Layout</a>--%>
                <a href="HolidayDealPackage.aspx" style="color:Blue;">Create Package</a>
                <%--<a href="HotelDealSetting.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Deal Layout</a>--%>
                <a href="EnquiryQueue.aspx" style="color:Blue;">Order Tracking</a>
                <%--<a href="PackageQueues.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Package Queues</a>--%>
                
               
                <%}
                    else
                  { %>
                  
                  
                  <a  href="HolidayDealPackage.aspx" style="color:Blue;">Create Package</a>
                  <a   href="HotelDealSetting.aspx" style="color:Blue;">Package List</a>
                  <%--<a href="PackageQueriesPanel.aspx">Order Tracking</a>
                  <a href="PackageQueues.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Package Queues</a>--%>
                  
                   <%} %>
        
                  
         </div>                                     
    <div class="col-md-2"><label><b>  Active </b><input type="checkbox" name="active" id="active" <%=activeCheck%> /></label> 
    
    <label> <b>Inactive </b><input type="checkbox" name="inactive" id="inactive" <%=inactiveCheck%>/> </label>
    
    <label> </label>   
    </div>
    <div class="col-md-1">Agent: <sup>*</sup>  </div>
    
     <div class="col-md-2"><asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" ></asp:DropDownList> </div>
                 <div class="col-md-1">Theme:  </div>
    
     <div class="col-md-2"><asp:DropDownList CssClass="form-control" ID="ddlTheme" runat="server" ></asp:DropDownList> </div>

                 


<div class="col-md-2 martop_xs10 marbot_10"><input class="button" type="button" value="Search" onclick="javascript:Search()" /> </div>

    <div class="clearfix"></div>
                  <div class="col-md-2">Culture Code :  </div>
    
     <div class="col-md-2"><asp:DropDownList CssClass="form-control" ID="ddlCultureCode" runat="server" >
         <asp:ListItem Text="English" Value="EN" Selected="True"></asp:ListItem>
          <asp:ListItem Text="Arabic" Value="AR" ></asp:ListItem>
                           </asp:DropDownList> </div>
    </div>

<input type="hidden" id="activeValue" name="activeValue" value="<%=activeValue %>" />
<input type="hidden" id="inActiveValue" name="inActiveValue" value="<%=inactiveValue %>" />

<%if(errorString.Length>0)
  { %>
<div style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5" >
<%=errorString %>
</div>
<%}
    else
  { %>
  <div style="float:left; color:Red;display:none;" class="padding-5 yellow-back width-100 center margin-top-5" id="errorMessage">
</div>
  <%} %>

<div> <input type="button" class="button" value="Save" onclick="javascript:Save()" /></div>
<input type="hidden" id="counter" name="counter" value="<%=counter %>" />
<input type="hidden" id="pageSubmit" name="pageSubmit" value="" />
<input type="hidden" id="colorRow1" name="colorRow1" value="" />
<input type="hidden" id="colorRow2" name="colorRow2" value="" />
<input type="hidden" id="LeftDeal1" name="LeftDeal1" value="" />
<input type="hidden" id="LeftDeal2" name="LeftDeal2" value="" />
<input type="hidden" id="submitPackageId" name="submitPackageId" value="" />
<input type="hidden" id="whereString" name="whereString" value="<%=whereString %>" />
<div class="table-responsive bg_white">
<table class="table">
  <tr>
    <th><b>PackageId</b></th>
    
    <th><b>City Name</b></th>
    <th><b>PackageName</b></th>
       <th><b>themeNames</b></th>
    <th><b>Paging Order</b></th>
    <th><b>Status</b></th>

  </tr>
  
  <%for (int i = 0; i < counter; i++)
    {
        if (packagesList.Rows[i]["packageStatus"].ToString() == "A")
        {
            activeCount++;
        }
      %>
      
    <tr style="width: 930px; padding:3px 0; border-bottom: 1px solid #fff;background-color:#fff;" id="row-<%=i %>">
   
    <td><input type="hidden" id="Package-<%=i%>" name="Package-<%=i %>" value="<%=packagesList.Rows[i]["packageId"]%>" /><span>
        <%=packagesList.Rows[i]["packageId"]%>
        </span> </td>
    <td> <span>
        <%=packagesList.Rows[i]["cityname"]%>
        </span></td>
   
    <td> <span>
        <%=packagesList.Rows[i]["packageName"]%> 
        </span></td>
        <%if (packagesList.Rows[i]["themeNames"] != DBNull.Value)
            { %>
        <td> <span>
        <%=packagesList.Rows[i]["themeNames"]%> 
        </span></td>
        <%} %>
     <td> <span>
        <%if (packagesList.Rows[i]["packageStatus"].ToString() == "A")
          { %>
        <input type="text" id="paging-<%=i%>" name="paging-<%=i%>" class="form-control"  maxlength="3" value="<%=packagesList.Rows[i]["paging"] %>" onchange="javascript:CheckVal(<%=i%>)"/>
        <%}
          else
          { %>
          <input type="text" id="paging-<%=i%>" name="paging-<%=i%>" class="form-control"  value="<%=packagesList.Rows[i]["paging"] %>" readonly="readonly" />
          <%} %>
        </span></td>

    <td> 
      <span id="stat-<%=i%>">
        <%if (packagesList.Rows[i]["packageStatus"].ToString() == "A")
          {%>
        <label style="color:Blue;"> Active</label>
        <input type="hidden" id="isActive-<%=i %>" name="isActive-<%=i %>" value="true" />
        <%}
          else
          { %>
          <label style="color:Blue;"> InActive</label>
        <input type="hidden" id="isActive-<%=i %>" name="isActive-<%=i %>" value="false"/>
          <%} %>
        </span>       
        <span>
        <a href="PackageMaster.aspx?packageId=<%=packagesList.Rows[i]["packageId"] %>" style="color:Blue;">Edit</a>
        </span>       
        <span>
        <%if (packagesList.Rows[i]["packageStatus"].ToString() != "A")
          {%>
        <a href="javascript:Delete(<%=i %>)" style="color:Blue;">Delete</a>
        <%}
          else
          { %>
          &nbsp;
          <%} %>
        </span>    
       <span id="stat2-<%=i%>">
        <%if (packagesList.Rows[i]["packageStatus"].ToString() == "A")
          {
              if (((packagesList.Rows[i]["paging"] == DBNull.Value) ? 0 : Convert.ToInt32(packagesList.Rows[i]["paging"])) == 0)
              {%>
        <a href="javascript:Inactivate(<%=i %>)">
        <%} %>
        inActivate
        </a>
        <%if (((packagesList.Rows[i]["paging"] == DBNull.Value) ? 0 : Convert.ToInt32(packagesList.Rows[i]["paging"])) == 0)
          {%>
        
        <%}
          }
          else
          { %>
        <a href="javascript:Activate(<%=i %>)" style="color:Blue;">Activate</a>
          <%} %>
        </span>      
        </td>

  </tr>
  
   <%} %>
</table>

 </div>
<div><input type="button" class="button"  value="Save" onclick="javascript:Save()" /></div>


<input type="hidden" id="activeCount" name="activeCount" value="<%=activeCount %>" />           
          <div class="clear"></div>      
</div>
</asp:Content>
