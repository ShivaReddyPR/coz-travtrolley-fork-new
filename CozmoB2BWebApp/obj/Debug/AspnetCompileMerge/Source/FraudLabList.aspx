﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="FraudLabListGUI" Title="FraudLabList" Codebehind="FraudLabList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
     <style>
         .fraud-lab-page table td{
            padding: 2px 2px 2px 3px;
         }
         .fraud-lab-page table td input[type=checkbox]{
            margin-right: 12px;
            margin-top: 8px;
            float: left;
         }
     </style>
    <script>
        var cal1;
        var cal2;
        function init() {
            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "fcontainer1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "fcontainer2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('fcontainer2').context.styleSheets[0].display = "none";
            $('fcontainer1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2() {
            $('fcontainer1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());
                //cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
               // cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('fcontainer2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //         if (difference < 1) {
            //             document.getElementById('errMess').style.display = "block";
            //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //             return false;
            //         }
            //         if (difference == 0) {
            //             document.getElementById('errMess').style.display = "block";
            //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
            //             return false;
            //         }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
        function CheckValidDate(Day, Mn, Yr) {
            var DateVal = Mn + "/" + Day + "/" + Yr;
            var dt = new Date(DateVal);

            if (dt.getDate() != Day) {
                return false;
            }
            else if (dt.getMonth() != Mn - 1) {
                //this is for the purpose JavaScript starts the month from 0
                return false;
            }
            else if (dt.getFullYear() != Yr) {
                return false;
            }
            return (true);
        }
        
    </script>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <div class="clear" style="margin-left: 25px">
        <div id="fcontainer1" style="position: absolute; top:170px; left: 200px; display: none;">
            <a onclick="document.getElementById('fcontainer1').style.display = 'none'"></a>
        </div>
    </div>

    <div class="clear" style="margin-left: 25px">
        <div id="fcontainer2" style="position: absolute; top: 170px; left: 450px; display: none;">
            <a onclick="document.getElementById('fcontainer2').style.display = 'none'"></a>
        </div>
    </div>   
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
        onclick="return ShowHide('divParam');">Hide Parameter</a>

    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon fraud-lab-page" style="height: 350px;">

                    <div class="col-md-12 pb-3">
                        <div class="col-md-2">
                            <label class="lblbold12">
                                From Date</label>
                        </div>
                        <div class="col-md-2">

                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                            Width="100px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showCal1()">
                                            <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                        </a>
                                    </td>
                                </tr>
                            </table>

                        </div>


                        <div class="col-md-2">
                            <label class="lblbold12">
                                To Date</label>
                        </div>


                        <div class="col-md-2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showCal2()">
                                            <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                        </a>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>

                <div class="col-md-12">

                    <div class="col-md-3" id="AgentssList">
                        
                        <span style="font-weight: bolder; font-size: 18px;">Agents:</span>
                        <input type="checkbox" id="selectAllAgents" onchange="AllAgents();" />


                        <span style="font-weight: bolder; font-size: 13px;">Select All</span>
                        <div class="col-md-12" style="height: 150px; overflow-y: scroll;">
                            <asp:CheckBoxList ID="chkAgent" runat="server" RepeatDirection="Vertical" RepeatColumns="1">
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="col-md-3" id="ProductsList">
                        <span style="font-weight: bolder; font-size: 18px;">Products::</span>
                        <input type="checkbox" id="selectAllProducts" onchange="AllProducts();" />
                        <span style="font-weight: bolder; font-size: 13px;">Select All</span>
                        <div class="col-md-12" style="height: 150px; overflow-y: scroll;">
                            <asp:CheckBoxList ID="chkProduct" runat="server" RepeatDirection="Vertical" RepeatColumns="2">
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="col-md-3" id="StatusList">
                        <span style="font-weight: bolder; font-size: 18px;">Status:</span>
                        <input type="checkbox" id="selectAllStatus" onchange="AllStatus();"/>
                        <span style="font-weight: bolder; font-size: 13px;">Select All</span>
                        <div class="col-md-12" style="height: 150px; overflow-y: scroll;">
                            <asp:CheckBoxList ID="chkStatus" runat="server" RepeatDirection="Vertical" RepeatColumns="2">
                                <asp:ListItem Text="APPROVED" Value="APPROVE"></asp:ListItem>
                                <asp:ListItem Text="REJECTED" Value="REJECT"></asp:ListItem>
                                <asp:ListItem Text="REVIEW" Value="REVIEW"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="col-md-3" id="Pgsourcelist">
                        <span style="font-weight: bolder; font-size: 18px;">Sources:</span>
                        <input type="checkbox" id="selectAllSources" onchange="Allsources();" />
                        <span style="font-weight: bolder; font-size: 13px;">Select All</span> 
                        <div class="col-md-12" style="height: 150px; overflow-y: scroll;">
                        <asp:CheckBoxList ID="ChkPgSources" runat="server" RepeatDirection="Vertical" RepeatColumns="2">

                            </asp:CheckBoxList>

                            </div>



                    </div>
                       <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="btn but_b pull-right  mr-4 mt-4" OnClick="btnSearch_Click" />
                </div>
            

             



            </div>
            
                <div class="clearfix">
                </div>
        </asp:Panel>        
    </div>
    <style> 
    .costum_modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.costum_modal_content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}
</style>
     <div id="divHistory" class="costum_modal">
             <div class="costum_modal_content">              
                 <h4 class="modal-title">Status History</h4>
                 <asp:GridView ID="gvfraudHistory" runat="server" AutoGenerateColumns="False" Width="100%"
                     AllowPaging="false" PageSize="10" DataKeyNames="histId" CellPadding="4" CellSpacing="0"
                     GridLines="none" >
                     <Columns>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">
                                      Status</label>
                             </HeaderTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="ITlblDocName" runat="server" Text='<%# Eval("status") %>' CssClass=""
                                      Width="100px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField>
                             <HeaderStyle Width="100px" />
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">Remarks</label>
                             </HeaderTemplate>
                             <ItemStyle />
                             <ItemTemplate>
                                 <asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("Remarks") %>' CssClass=""
                                      Width="300px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                          <asp:TemplateField>
                             <HeaderStyle Width="200px" />
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">Modified By</label>
                             </HeaderTemplate>
                             <ItemStyle />
                             <ItemTemplate>
                                 <asp:Label ID="ITlblModifiedBy" runat="server" Text='<%# Eval("username") %>' CssClass=""
                                      Width="200px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">
                                     Modified Date</label>
                             </HeaderTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="ITlblModifiedDate" runat="server" Text='<%# Eval("ModifiedOn") %>' CssClass=""
                                     ToolTip='<%# Eval("ModifiedOn") %>' Width="300px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>

                     </Columns>
                     <HeaderStyle CssClass="normal-heading"></HeaderStyle>
                     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                 </asp:GridView>
                   <input type="button" value="Close" name="Close" class="button" onclick="hideHistory()" />
             </div>
             </div>
    <div class="table-responsive">
    <table width="100%" id="tabSearch" class="table" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:GridView  ID="gvFraudLabList" Width="100%" runat="server" AllowPaging="true"
                    DataKeyNames="respId" EmptyDataText="No data Found!" AutoGenerateColumns="false"
                    PageSize="25" GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0" 
                    OnRowCommand="gvFraudLabList_RowCommand" OnRowUpdating="gvFraudLabList_RowUpdating" OnRowDataBound="gvFraudLabList_RowDataBound" OnPageIndexChanging="gvFraudLabList_PageIndexChanging">
                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow"  />
                    <Columns>
                        <asp:TemplateField>                                      
                    <ItemTemplate>
                        <asp:CheckBox ID="flchkSelect" runat="server" Width="20px" CssClass="label" Checked="false"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="left" />
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrAgent" runat="server" Text="Agent Name"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField>
                            <ItemStyle HorizontalAlign="left" />
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrproduct" runat="server" Text="Product Type"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblProduct" runat="server" Text='<%# Eval("productType") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="left" />
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrreuqest" runat="server" Text="Request ID"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblrequestid" runat="server" Text='<%# Eval("reqId") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdricountry_match" runat="server" Text="countrymatch"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlbliscountrymatch" runat="server" Text='<%# Eval("is_country_match") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrishighriskcountry" runat="server" Text="Risk country"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblishighriskcountry" runat="server" Text='<%# Eval("is_high_risk_country") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrinkm" runat="server" Text="Distance in km"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblkm" runat="server" Text='<%# Eval("distance_in_km") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrdistance_in_mile" runat="server" Text="Distance in mile"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlbldistanceinmile" runat="server" Text='<%# Eval("distance_in_mile") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrip_country" runat="server" Text="Ip country"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblipcountry" runat="server" Text='<%# Eval("ip_country") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrip_region" runat="server" Text="Ip region"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="ITlblip_region" runat="server" Text='<%# Eval("ip_region") %>' CssClass="label grdof" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrfraudlabsstatus" runat="server" Text="Status"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>   
                                 <asp:HiddenField id="IThdffraudlabsstatus" runat="server" value='<%# Eval("status") %>'></asp:HiddenField>
                                <asp:DropDownList ID="gdddlStatus" runat="server" Width="120px" Enabled="false">                                
                                    <asp:ListItem Text="PENDING" Value="P"></asp:ListItem>
                                    <asp:ListItem Text="INPROGRESS" Value="I"></asp:ListItem>
                                     <asp:ListItem Text="COMPLETE" Value="C"></asp:ListItem>
                                </asp:DropDownList>
                           </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrfraudlabsprostatus" runat="server" Text="fraudlabsprostatus"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate> 
                                <asp:Label ID="ITtxtprostatus" runat="server" Text='<%# Eval("fraudlabspro_status") %>' CssClass="label grdof" Width="100px"></asp:Label>                              
                           </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrRemarks" runat="server" Text="Remarks"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:TextBox ID="ITtxtRemarks" Enabled="false" runat="server" MaxLength="50"></asp:TextBox>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                       
                          <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrHistory" runat="server" Text="History"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                               <asp:LinkButton ID="lbtnHistory"  runat="server" Text="View History" CommandArgument='<%# Container.DataItemIndex%>'  CommandName="ShowHistory"></asp:LinkButton>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdrpgsource" runat="server" Text="Source"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                               <asp:Label ID="lblsource" runat="server" Text='<%#Eval("Pg_source")%>'></asp:Label>
                                </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblhdPath" runat="server" Text="File"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle />
                            <ItemTemplate>
                                
                                <asp:HiddenField ID="hdnPath" runat="server" Value='<%#Eval("xmlPath")%>' />                                
                                <input type="button"  id="lbpath"
                                    value="Download" onclick="download('<%#Eval("path").ToString().Replace("\\", "/")%>')" ></input>                             
                       </ItemTemplate>
                           
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actions">
                            <ItemTemplate>
                                <asp:Button ID="btnUpdate" runat="server" Enabled="false" CommandArgument='<%# Container.DataItemIndex%>'  CommandName="Update" Text="Update" />                               
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    </div>
    <script type="text/javascript">
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function AllAgents() {
            var chkFields = document.getElementById("<%=chkAgent.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAllAgents').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
        }
        function AllProducts() {
            var chkFields = document.getElementById("<%=chkProduct.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAllProducts').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
        }
        function Allsources() {
            var chkFields = document.getElementById("<%=ChkPgSources.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAllSources').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
        }
        function AllStatus() {
            var chkFields = document.getElementById("<%=chkStatus.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAllStatus').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
        }        
        function hideHistory() {            
            document.getElementById('divHistory').style.display = "none";
            $('#divHistory').modal('hide');
            return false;
        }
         function setDocumentPosition(id,divId) {
            document.getElementById(divId.id).style.display = "block";
           // var positions = getRelativePositions(document.getElementById(id));            
        }
         //function getRelativePositions(obj) {
         //   var curLeft = 0;
         //   var curTop = 0;
         //   if (obj.offsetParent) {
         //       do {
         //           curLeft += obj.offsetLeft;
         //           curTop += obj.offsetTop;
         //       } while (obj = obj.offsetParent);

         //   }
         //   return [curLeft, curTop];
         //}
        function download(path) {
            var xmlpaths = path.split('$$');
            for (var i = 0; i < xmlpaths.length; i++)
            {
             var path = xmlpaths[i];
             var type = xmlpaths[i];
             var docName = xmlpaths[i];
             var open = window.open('DownloadeDoc.aspx?path=' + path + '&type=' + type + '&docName=' + docName);
                
            }
            return false;
        }    
        function EnableControls(ddl, txt,btnupdate, enable) {   
            document.getElementById(ddl).disabled = !enable;
            document.getElementById(txt).disabled = !enable;            
            document.getElementById(btnupdate).disabled = !enable;
            
        }
    </script>
</asp:Content>

