﻿<%@ WebHandler Language="C#" Class="hn_SimpeFileUploader" %>

using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Web.SessionState;
using System.Collections.Generic;
using CT.Core;

public class hn_SimpeFileUploader : IHttpHandler, IRequiresSessionState
{
    //int numFiles = 1;
    public void ProcessRequest(HttpContext context)
    {
        try
        {

            //uploaded files saving session
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (context.Session["Files"] != null)
            {
                files = context.Session["Files"] as List<HttpPostedFile>;
            }
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                files.Add(file);
            }
            context.Session["Files"] = files;
        }
        catch(Exception ex) {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    

}
