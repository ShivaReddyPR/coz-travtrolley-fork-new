﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true"
    EnableEventValidation="false" Inherits="CorporatePoliciesUI"
    Title="CorporatePolicies" Codebehind="CorporatePolicies.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
 
    <script type="text/javascript">


        /********************Start : Approval Tab Javascript Functions*************************/

        function validatePreTripApproval() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlPreTripCurrency.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select currency from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPreTripAmount.ClientID %>').value.length == 0)) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }
            else {
                valid = true;
            }
            return valid;

        } //EOF


        function validatePreLaunchApproval() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlRelaunchPriceChange.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select currency from the list .";
            }
            else if (Trim(document.getElementById('<%=txtRelaunchPriceAmount.ClientID %>').value.length == 0)) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }
            else {
                valid = true;
            }
            return valid;

        }



        /********************End : Approval Tab Javascript Functions**************************/





        /********************Start : Expense Tab Javascript Functions**************************/


        //Please do not remove the following comments 
        // *********************Very Important ***************
        //The following symbols are used for identification
        // ^(Upper Triangle) -- Used to maintain the record identity (id of the record)
        // -(Minus) -- Used to combine both the values eg:Country -City
        // | (Pipe) -- Used to separate each poilcy detail.

        var editedExpenseAllowance = "";
        var editedExpenseAllowanceRowId = "";
        var editedCapCityExpense = "";
        var editedCapCityAllowance = "";

        function clearEditedVariables() {
            editedExpenseAllowance = "";
            editedExpenseAllowanceRowId = "";
            editedCapCityExpense = "";
            editedCapCityAllowance = "";
        }
        function cancelCapExpense() {
            clearExpense();
            clearEditedVariables();
        }

        function cancelCapAllowance() {
            clearAllowance();
            clearEditedVariables();

        }


        function bindCapPerAllowanceHtml() {


            ////RowInfo -- AllowanceType
            //{recordId^Country-City-Currency-Amount-TravelDays-Type}}


            if (document.getElementById("<%=hdnCapAllowance.ClientID %>").value.length > 0) {
                //console.log("===============Executing bindCapPerAllowanceHtml inside if block================ ");
                //console.log(document.getElementById("<%=hdnCapAllowance.ClientID %>").value);

                var savedCapCityAllowance = document.getElementById('<%=hdnCapAllowance.ClientID %>').value.split('|');
                //console.log("====selectedCapCityAllowance" + savedCapCityAllowance);


                for (var ea = 0; ea < savedCapCityAllowance.length; ea++) {
                    //console.log("i value" + ea);

                    var rowInfo = "";
                    rowInfo = savedCapCityAllowance[ea];

                    //console.log("====rowInfo" + rowInfo);



                    document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];

                    document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>").value = rowInfo.split('^')[1].split('-')[2];


                    //City
                    LoadCitiesCapAllowance("<%=ddlCountryCapAllowance.ClientID %>");

                    //Get all the options of the dropdown
                    var options = document.getElementById('<%=ddlCityCapAllowance.ClientID %>').options;

                    var p = 0;
                    for (var i = 0; i < options.length; i++) {

                        if (options[i].value == rowInfo.split('^')[1].split('-')[1]) {

                            options[i].selected = true;
                            p = i;
                            break;
                        }
                    }

                    document.getElementById('<%=ddlCityCapAllowance.ClientID %>').options[p].selected = true;

                    var countriesE = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>");
                    var selectedCountryText = countriesE.options[countriesE.selectedIndex].text;
                    //console.log(selectedCountryText);

                    var citiesE = document.getElementById("<%=ddlCityCapAllowance.ClientID %>");
                    var selectedCityText = citiesE.options[citiesE.selectedIndex].text;
                    //console.log(selectedCityText);
                    ////RowInfo -- AllowanceType
                    //{recordId^Country-City-Currency-Amount-TravelDays-Type}}

                    var expenseCurrency = document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>");
                    var selectedExpCurrencyText = expenseCurrency.options[expenseCurrency.selectedIndex].text;
                    //console.log(selectedExpCurrencyText);



                    var travelDays = rowInfo.split('^')[1].split('-')[4];
                    if (travelDays == "True") {
                        travelDays = "True";
                    }
                    else {
                        travelDays = "False";
                    }

                    AddExpenseAllowanceDiv(
                    selectedCountryText,
                    selectedCityText,
                    "Allowance",
                    selectedExpCurrencyText,
                    rowInfo.split('^')[1].split('-')[3],
                    "No",
                    travelDays,
                    rowInfo.split('^')[1].split('-')[5],
                    rowInfo
                    )
                    //EOF : AddExpenseAllowanceDiv


                } //EOF for loop

                //clearExpense();
            } //EOF if block


        } //EOF function






        function bindCapPerExpenseHtml() {
            //console.log("===============Executing bindCapPerExpenseHtml================ ");

            //RowInfo -- ExpenseType
            //{recordId^Country-City-ExpenseType-Currency-Amount-DailyActuals-TravelDays-Type}


            if (document.getElementById("<%=hdnCapExpense.ClientID %>").value.length > 0) {
                //console.log("===============Executing bindCapPerExpenseHtml inside if block================ ");
                //console.log(document.getElementById("<%=hdnCapExpense.ClientID %>").value);

                var savedCapCityExpense = document.getElementById('<%=hdnCapExpense.ClientID %>').value.split('|');
                //console.log("====selectedCapCityExpense" + savedCapCityExpense);


                for (var et = 0; et < savedCapCityExpense.length; et++) {
                    //console.log("i value" + et);

                    var rowInfo = "";
                    rowInfo = savedCapCityExpense[et];

                    //console.log("====rowInfo" + rowInfo);

                    //console.log(rowInfo.split('^')[1].split('-')[0]);
                    //console.log(rowInfo.split('^')[1].split('-')[2]);
                    //console.log(rowInfo.split('^')[1].split('-')[3]);

                    document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];
                    document.getElementById("<%=ddlCapExpenseType.ClientID %>").value = rowInfo.split('^')[1].split('-')[2];
                    document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>").value = rowInfo.split('^')[1].split('-')[3];


                    //City
                    LoadCitiesCapExpense("<%=ddlCountryCapExpense.ClientID %>");

                    //Get all the options of the dropdown
                    var options = document.getElementById('<%=ddlCityCapExpense.ClientID %>').options;

                    var p = 0;
                    for (var i = 0; i < options.length; i++) {

                        if (options[i].value == rowInfo.split('^')[1].split('-')[1]) {

                            options[i].selected = true;
                            p = i;
                            break;
                        }
                    }

                    document.getElementById('<%=ddlCityCapExpense.ClientID %>').options[p].selected = true;

                    var countriesE = document.getElementById("<%=ddlCountryCapExpense.ClientID %>");
                    var selectedCountryText = countriesE.options[countriesE.selectedIndex].text;
                    //console.log(selectedCountryText);

                    var citiesE = document.getElementById("<%=ddlCityCapExpense.ClientID %>");
                    var selectedCityText = citiesE.options[citiesE.selectedIndex].text;
                    //console.log(selectedCityText);

                    var expenseTypes = document.getElementById("<%=ddlCapExpenseType.ClientID %>");
                    var selectedExpTypeText = expenseTypes.options[expenseTypes.selectedIndex].text;
                    ////console.log(selectedExpTypeText);


                    var expenseCurrency = document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>");
                    var selectedExpCurrencyText = expenseCurrency.options[expenseCurrency.selectedIndex].text;
                    //console.log(selectedExpCurrencyText);

                    //console.log(rowInfo.split('^')[1].split('-')[4]);
                    //console.log(rowInfo.split('^')[1].split('-')[6]);
                    //console.log(rowInfo.split('^')[1].split('-')[7]);

                    var travelDays = rowInfo.split('^')[1].split('-')[6];
                    if (travelDays == "True") {
                        travelDays = "True";
                    }
                    else {
                        travelDays = "False";
                    }

                    AddExpenseAllowanceDiv(
                    selectedCountryText,
                    selectedCityText,
                    selectedExpTypeText,
                    selectedExpCurrencyText,
                    rowInfo.split('^')[1].split('-')[4],
                    "Yes",
                    travelDays,
                    rowInfo.split('^')[1].split('-')[7],
                    rowInfo
                    )//EOF : AddExpenseAllowanceDiv


                } //EOF for loop

                //clearExpense();
            } //EOF if block


        } //EOF function


        function updateCapExpenseHtml(ctrlId, rowInfo) {
            var ctrlId = parseInt(ctrlId);

            var countriesE = document.getElementById("<%=ddlCountryCapExpense.ClientID %>");
            var selectedCountryText = countriesE.options[countriesE.selectedIndex].text;

            var citiesE = document.getElementById("<%=ddlCityCapExpense.ClientID %>");
            var selectedCityText = citiesE.options[citiesE.selectedIndex].text;

            var expType = document.getElementById("<%=ddlCapExpenseType.ClientID %>");
            var selectedExpTypeText = expType.options[expType.selectedIndex].text;

            var expCurrency = document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>");
            var selectedExpCurrencyText = expCurrency.options[expCurrency.selectedIndex].text;
            //Counry,city code
            document.getElementById('cc' + ctrlId).innerHTML = selectedCountryText + "," + selectedCityText;
            //Expense type
            document.getElementById('et' + ctrlId).innerHTML = selectedExpTypeText;
            //Currency

            var amount = document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value;
            document.getElementById('cur' + ctrlId).innerHTML = selectedExpCurrencyText + " " + amount;

            //document.getElementById('amount' + ctrlId).innerHTML = amount;
            var traveldays = "False";
            if (document.getElementById('chkCapExpenseTravelDays').checked) {
                traveldays = "True";
            }

            document.getElementById('td' + ctrlId).innerHTML = traveldays;
            document.getElementById('rowInfo' + ctrlId).innerHTML = rowInfo;

        }


        function updateCapAllowanceHtml(ctrlId, rowInfo) {
            var ctrlId = parseInt(ctrlId);

            var countriesE = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>");
            var selectedCountryText = countriesE.options[countriesE.selectedIndex].text;

            var citiesE = document.getElementById("<%=ddlCityCapAllowance.ClientID %>");
            var selectedCityText = citiesE.options[citiesE.selectedIndex].text;



            var expCurrency = document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>");
            var selectedExpCurrencyText = expCurrency.options[expCurrency.selectedIndex].text;

            //Counry,city code
            document.getElementById('cc' + ctrlId).innerHTML = selectedCountryText + "," + selectedCityText;

            //Currency

            var amount = document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value;
            document.getElementById('cur' + ctrlId).innerHTML = selectedExpCurrencyText + " " + amount;

            //document.getElementById('amount' + ctrlId).innerHTML = amount;
            var traveldays = "False";
            if (document.getElementById('chkCapAllowanceTravelDays').checked) {
                traveldays = "True";
            }

            document.getElementById('td' + ctrlId).innerHTML = traveldays;
            document.getElementById('rowInfo' + ctrlId).innerHTML = rowInfo;
            //console.log(rowInfo);


        }





        function updateCapExpense() {


            //Country
            if (Trim(document.getElementById('<%=ddlCountryCapExpense.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select country from the list .";
            }

            //City

            else if (Trim(document.getElementById('<%=ddlCityCapExpense.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select city from the list .";
            }
            //Expense Type
            else if (Trim(document.getElementById('<%=ddlCapExpenseType.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select expense type from the list .";
            }

            //Currency
            else if (Trim(document.getElementById('<%=ddlCapExpenseCurrency.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select currency from the list .";
            }

            //Amount
            else if (Trim(document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value) == "" || document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }


            else {


                // verify the Country  City Expense Type duplicates
                //Country
                var selCapCityExpense = document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value;
                selCapCityExpense += "-";

                //City
                selCapCityExpense += document.getElementById("<%=ddlCityCapExpense.ClientID %>").value;
                selCapCityExpense += "-";

                //ExpenseType
                selCapCityExpense += document.getElementById("<%=ddlCapExpenseType.ClientID %>").value;

                var duplicateCount = 0;



                var capCityExpenseSaved = document.getElementById("<%=hdnCapCityExpense.ClientID %>").value.split('|');

                //Insert all the cap city expenses into the array.
                var duplicateCityExpense = [];
                for (var u = 0; u < capCityExpenseSaved.length; u++) {

                    duplicateCityExpense[u] = capCityExpenseSaved[u];
                }
                //console.log(duplicateCityExpense);

                //Insert the new selected cap city expense at the index;
                for (var ft = 0; ft < duplicateCityExpense.length; ft++) {

                    if (duplicateCityExpense[ft] == editedCapCityExpense) {
                        duplicateCityExpense[ft] = selCapCityExpense;
                    }
                }

                //console.log(duplicateCityExpense);

                for (var d = 0; d < duplicateCityExpense.length; d++) {

                    if (selCapCityExpense == duplicateCityExpense[d]) {
                        duplicateCount++;
                    }

                }


                //alert(duplicateCount);


                if (duplicateCount > 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already added this combination";

                }
                else {


                    document.getElementById('addCapExpense').style.display = "block";
                    document.getElementById('updateCapExpense').style.display = "none";
                    document.getElementById('cancelCapExpense').style.display = "none";

                    //Main :Row Information Capturing.

                    //Record Id (^)
                    //Country -
                    //City-
                    //ExpenseType-
                    //Currency-
                    //Amount-
                    //DailyActuals-
                    //Traveldays
                    var traveldays = "False";
                    if (document.getElementById('chkCapExpenseTravelDays').checked) {
                        traveldays = "True";
                    }

                    var rowInfo = editedExpenseAllowance.split('^')[0];
                    rowInfo += "^";

                    //Country
                    rowInfo += document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value;
                    rowInfo += "-";

                    //City
                    rowInfo += document.getElementById("<%=ddlCityCapExpense.ClientID %>").value;
                    rowInfo += "-";

                    //ExpenseType
                    rowInfo += document.getElementById("<%=ddlCapExpenseType.ClientID %>").value;
                    rowInfo += "-";

                    //Currency
                    rowInfo += document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>").value;
                    rowInfo += "-";

                    //Amount
                    rowInfo += document.getElementById("<%=txtCapExpenseAmount.ClientID %>").value;
                    rowInfo += "-";

                    //Daily Actuals
                    rowInfo += "True";
                    rowInfo += "-";

                    //Traveldays
                    rowInfo += traveldays;
                    rowInfo += "-";

                    //Type -- Expense(E) or Allowance(A)
                    rowInfo += "E";

                    var capCityExpense = document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value;
                    capCityExpense += "-";

                    //City
                    capCityExpense += document.getElementById("<%=ddlCityCapExpense.ClientID %>").value;
                    capCityExpense += "-";

                    //ExpenseType
                    capCityExpense += document.getElementById("<%=ddlCapExpenseType.ClientID %>").value;





                    if (document.getElementById("<%=hdnCapCityExpense.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnCapCityExpense.ClientID %>").value = capCityExpense;
                    }
                    else {
                    
                        var selectedCapCityExpense = document.getElementById('<%=hdnCapCityExpense.ClientID %>').value.split('|');
                        var selCapCityExpense = '';
                        for (var i = 0; i < selectedCapCityExpense.length; i++) {


                            if (selCapCityExpense == '') {
                                if (selectedCapCityExpense[i] == editedCapCityExpense) {
                                    selCapCityExpense = capCityExpense;
                                }
                                else {
                                    selCapCityExpense = selectedCapCityExpense[i];
                                }
                            }
                            else {
                                if (selectedCapCityExpense[i] == editedCapCityExpense) {
                                    selCapCityExpense = capCityExpense + '|' + selCapCityExpense;
                                }
                                else {
                                    selCapCityExpense = selCapCityExpense + '|' + selectedCapCityExpense[i];
                                }
                            }
                        }

                        document.getElementById('<%=hdnCapCityExpense.ClientID %>').value = selCapCityExpense;

                    }

                    var selectedExpenses = document.getElementById('<%=hdnCapExpense.ClientID %>').value.split('|');
                    var selExpense = '';
                    for (var i = 0; i < selectedExpenses.length; i++) {


                        if (selExpense == '') {
                            if (selectedExpenses[i] == editedExpenseAllowance) {
                                selExpense = rowInfo;
                            }
                            else {
                                selExpense = selectedExpenses[i];
                            }
                        }
                        else {
                            if (selectedExpenses[i] == editedExpenseAllowance) {
                                selExpense = selExpense + '|' + rowInfo;
                            }
                            else {
                                selExpense = selExpense + '|' + selectedExpenses[i];
                            }
                        }
                    }
                    //console.log("============Selected Expenses============");
                    //console.log(selExpense);

                    document.getElementById("<%=hdnCapExpense.ClientID %>").value = selExpense;

                    updateCapExpenseHtml(editedExpenseAllowanceRowId, rowInfo)

                    clearExpense();
                }
            }

        }

        function updateCapAllowance() {

            document.getElementById('errMess').style.display = "none";

            //Country
            if (Trim(document.getElementById('<%=ddlCountryCapAllowance.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select country from the list .";
            }

            //City

            else if (Trim(document.getElementById('<%=ddlCityCapAllowance.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select city from the list .";
            }


            //Currency
            else if (Trim(document.getElementById('<%=ddlCurrencyCapAllowance.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select currency from the list .";
            }

            //Amount
            else if (Trim(document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value) == "" || document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }



            else {

                // verify the City country duplicates
                //Country
                var selCapCityAllowance = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value;
                selCapCityAllowance += "-";

                //City
                selCapCityAllowance += document.getElementById("<%=ddlCityCapAllowance.ClientID %>").value;



                var duplicateCount = 0;
                var capCityAllowanceSaved = document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value.split('|');
                //console.log(capCityAllowanceSaved);


                //Insert all the cap city allowances into the array.
                var duplicateCityAllowance = [];
                for (var u = 0; u < capCityAllowanceSaved.length; u++) {

                    duplicateCityAllowance[u] = capCityAllowanceSaved[u];
                }
                //console.log(duplicateCityAllowance);

                //Insert the new selected cap city allowance at the index;
                for (var ft = 0; ft < duplicateCityAllowance.length; ft++) {

                    if (duplicateCityAllowance[ft] == editedCapCityAllowance) {
                        duplicateCityAllowance[ft] = selCapCityAllowance;
                    }
                }

                //console.log(duplicateCityAllowance);

                for (var d = 0; d < duplicateCityAllowance.length; d++) {

                    if (selCapCityAllowance == duplicateCityAllowance[d]) {
                        duplicateCount++;
                    }

                }


                //alert(duplicateCount);


                if (duplicateCount > 1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already addded this combination";
                    $('select').select2();
                }
                else {

                    //Country
                    var capCityAllowance = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value;
                    capCityAllowance += "-";

                    //City
                    capCityAllowance += document.getElementById("<%=ddlCityCapAllowance.ClientID %>").value;


                    if (document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value.length == 0) {
                        document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value = capCityAllowance;
                    }
                    else {

                        //This variable will hold all the allowances that are saved
                        var selectedCapCityAllowance = document.getElementById('<%=hdnCapCityAllowance.ClientID %>').value.split('|');
                        var selCapCityAllowance = '';
                        for (var i = 0; i < selectedCapCityAllowance.length; i++) {


                            if (selCapCityAllowance == '') {
                                if (selectedCapCityAllowance[i] == editedCapCityAllowance) {
                                    selCapCityAllowance = capCityAllowance;
                                }
                                else {
                                    selCapCityAllowance = selectedCapCityAllowance[i];
                                }
                            }
                            else {
                                if (selectedCapCityAllowance[i] == editedCapCityAllowance) {
                                    selCapCityAllowance = capCityAllowance + '|' + selCapCityAllowance;
                                }
                                else {
                                    selCapCityAllowance = selCapCityAllowance + '|' + selectedCapCityAllowance[i];
                                }
                            }
                        }

                        document.getElementById('<%=hdnCapCityAllowance.ClientID %>').value = selCapCityAllowance;
                        document.getElementById('addCapAllowance').style.display = "block";
                        document.getElementById('updateCapAllowance').style.display = "none";
                        document.getElementById('cancelCapAllowance').style.display = "none";
                        var traveldays = "False";
                        if (document.getElementById('chkCapAllowanceTravelDays').checked) {
                            traveldays = "True";
                        }
                        //Main :Row Information Capturing.

                        //Record Id (^)
                        //Country -
                        //City-
                        //Currency-
                        //Amount-
                        //Traveldays
                        //Type

                        var rowInfo = editedExpenseAllowance.split('^')[0];
                        rowInfo += "^";

                        //Country
                        rowInfo += document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value;
                        rowInfo += "-";

                        //City
                        rowInfo += document.getElementById("<%=ddlCityCapAllowance.ClientID %>").value;
                        rowInfo += "-";

                        //Currency
                        rowInfo += document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>").value;
                        rowInfo += "-";

                        //Amount
                        rowInfo += document.getElementById("<%=txtCapAllowanceAmount.ClientID %>").value;
                        rowInfo += "-";

                        //Traveldays
                        rowInfo += traveldays;
                        rowInfo += "-";

                        //Type -- Expense or Allowance
                        rowInfo += "A";

                        if (document.getElementById("<%=hdnCapAllowance.ClientID %>").value.length == 0) {
                            document.getElementById("<%=hdnCapAllowance.ClientID %>").value = rowInfo;
                        }
                        else {
                            document.getElementById("<%=hdnCapAllowance.ClientID %>").value += "|" + rowInfo;
                        }


                        var selectedAllowances = document.getElementById('<%=hdnCapAllowance.ClientID %>').value.split('|');
                        var selAllowance = '';
                        for (var i = 0; i < selectedAllowances.length; i++) {

                            if (selAllowance == '') {
                                if (selectedAllowances[i] == editedExpenseAllowance) {
                                    selAllowance = rowInfo;
                                }
                                else {
                                    selAllowance = selectedAllowances[i];
                                }
                            }
                            else {
                                if (selectedAllowances[i] == editedExpenseAllowance) {
                                    selAllowance = selAllowance + '|' + rowInfo;
                                }
                                else {
                                    selAllowance = selAllowance + '|' + selectedAllowances[i];
                                }
                            }
                        }
                        document.getElementById("<%=hdnCapAllowance.ClientID %>").value = selAllowance;
                        //console.log("============Selected Allowances============");
                        //console.log(selAllowance);
                        updateCapAllowanceHtml(editedExpenseAllowanceRowId, rowInfo);
                        clearAllowance();
                    }
                }
            }
        } //Eof for loop


        function clearExpense() {
            document.getElementById('<%=ddlCountryCapExpense.ClientID %>').value = "0";
            document.getElementById('<%=ddlCityCapExpense.ClientID %>').value = "0";
            document.getElementById('<%=ddlCapExpenseType.ClientID %>').value = "0";
            document.getElementById('<%=ddlCapExpenseCurrency.ClientID %>').value = "0";
            document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value = "";
            document.getElementById('chkCapExpenseTravelDays').checked = false;
            document.getElementById('errMess').style.display = "none";

            document.getElementById('updateCapExpense').style.display = "none";
            document.getElementById('cancelCapExpense').style.display = "none";
            document.getElementById('addCapExpense').style.display = "block";


        }

        function clearAllowance() {

            document.getElementById('<%=ddlCountryCapAllowance.ClientID %>').value = "0";
            document.getElementById('<%=ddlCityCapAllowance.ClientID %>').value = "0";
            document.getElementById('<%=ddlCurrencyCapAllowance.ClientID %>').value = "0";
            document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value = "0";
            document.getElementById('chkCapAllowanceTravelDays').checked = false;
            document.getElementById('errMess').style.display = "none";

            document.getElementById('updateCapAllowance').style.display = "none";
            document.getElementById('cancelCapAllowance').style.display = "none";
            document.getElementById('addCapAllowance').style.display = "block";
        }




        function EditExpenseAllowance(ctrlID, type) {

            var ctrlID = parseInt(ctrlID);
            var rowInfo = document.getElementById("rowInfo" + ctrlID).innerHTML;
            editedExpenseAllowance = rowInfo;
            editedExpenseAllowanceRowId = ctrlID;

            if (type == "E") { //Expense
                editedCapCityExpense = rowInfo.split('^')[1].split('-')[0]; //country
                editedCapCityExpense += "-" + rowInfo.split('^')[1].split('-')[1]; //City
                editedCapCityExpense += "-" + rowInfo.split('^')[1].split('-')[2]; //Expense type
                editedCapCityAllowance = "";
            }
            if (type == "A") {//Allowance
                editedCapCityAllowance = rowInfo.split('^')[1].split('-')[0]; //country
                editedCapCityAllowance += "-" + rowInfo.split('^')[1].split('-')[1]; //City
                editedCapCityExpense = "";

            }



            //ExpenseType
            if (type == "E") {
                //RowInfo
                //{recordId^Country-City-ExpenseType-Currency-Amount-DailyActuals-TravelDays-Type}

                var recordId = rowInfo.split('^')[0];
                //Counrty
                 $('#ctl00_cphTransaction_ddlCountryCapExpense').select2("val", rowInfo.split('^')[1].split('-')[0]);
               // document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];


                //City

                LoadCitiesCapExpense("<%=ddlCountryCapExpense.ClientID %>");

                //Get all the options of the dropdown
                var options = document.getElementById('<%=ddlCityCapExpense.ClientID %>').options;

                var p = 0;
                for (var i = 0; i < options.length; i++) {

                    if (options[i].value == rowInfo.split('^')[1].split('-')[1]) {

                        options[i].selected = true;
                        p = i;
                        break;
                    }
                }
                 $('#ctl00_cphTransaction_ddlCityCapExpense').select2("val", options[p].value);
                //document.getElementById('<%=ddlCityCapExpense.ClientID %>').options[p].selected = true;







                //ExpenseType
                $('#ctl00_cphTransaction_ddlCapExpenseType').select2("val", rowInfo.split('^')[1].split('-')[2]);
                //document.getElementById("<%=ddlCapExpenseType.ClientID %>").value = rowInfo.split('^')[1].split('-')[2];

                //currency
                $('#ctl00_cphTransaction_ddlCapExpenseCurrency').select2("val", rowInfo.split('^')[1].split('-')[3]);
                //document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>").value = rowInfo.split('^')[1].split('-')[3];

                //Amount
                document.getElementById("<%=txtCapExpenseAmount.ClientID %>").value = rowInfo.split('^')[1].split('-')[4];

                //Traveldays
                //console.log(rowInfo.split('^')[1].split('-')[6]);

                if (rowInfo.split('^')[1].split('-')[6] == "True") {

                    document.getElementById('chkCapExpenseTravelDays').checked = true;
                }
                else {
                    document.getElementById('chkCapExpenseTravelDays').checked = false;
                }

                document.getElementById('addCapExpense').style.display = "none";
                document.getElementById('updateCapExpense').style.display = "block";
                document.getElementById('cancelCapExpense').style.display = "block";




            } //EOF first if block

            //AllowanceType
            if (type == "A") {
                //RowInfo
                //{recordId^Country-City-Currency-Amount-TravelDays-Type}

                var recordId = rowInfo.split('^')[0];
                //Counrty
                 $('#ctl00_cphTransaction_ddlCountryCapAllowance').select2("val", rowInfo.split('^')[1].split('-')[0]);
              //  document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value = rowInfo.split('^')[1].split('-')[0];



                //City
                LoadCitiesCapAllowance("<%=ddlCountryCapAllowance.ClientID %>");

                //Get all the options of the dropdown
                var options = document.getElementById('<%=ddlCityCapAllowance.ClientID %>').options;

                var p = 0;
                for (var i = 0; i < options.length; i++) {

                    if (options[i].value == rowInfo.split('^')[1].split('-')[1]) {

                        options[i].selected = true;
                        p = i;
                        break;
                    }
                }
                 $('#ctl00_cphTransaction_ddlCityCapAllowance').select2("val", options[p].value);
               // document.getElementById('<%=ddlCityCapAllowance.ClientID %>').options[p].selected = true;



                //currency
                 $('#ctl00_cphTransaction_ddlCurrencyCapAllowance').select2("val", rowInfo.split('^')[1].split('-')[2]);
               // document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>").value = rowInfo.split('^')[1].split('-')[2];

                //Amount
                document.getElementById("<%=txtCapAllowanceAmount.ClientID %>").value = rowInfo.split('^')[1].split('-')[3];

                //Traveldays

                if (rowInfo.split('^')[1].split('-')[4] == "True") {

                    document.getElementById('chkCapAllowanceTravelDays').checked = true;
                }
                else {
                    document.getElementById('chkCapAllowanceTravelDays').checked = false;
                }

                document.getElementById('addCapAllowance').style.display = "none";
                document.getElementById('updateCapAllowance').style.display = "block";
                document.getElementById('cancelCapAllowance').style.display = "block";



            } //EOF first if block





        } //EOf of function

        function RemoveExpensesAllowancesDiv(ctrlID, type) {

            var ctrlID = parseInt(ctrlID);
            var removeCapExpenseAllowance = document.getElementById('rowInfo' + ctrlID).innerHTML;
            document.getElementById('capturedExpense' + ctrlID).remove();
            var hdnDelCapExpenseAllowance = document.getElementById('<%=hdnDelCapExpenseAllowance.ClientID %>').value;
            if (hdnDelCapExpenseAllowance.length == 0) {
                document.getElementById('<%=hdnDelCapExpenseAllowance.ClientID %>').value = removeCapExpenseAllowance;
            }
            else {
                document.getElementById('<%=hdnDelCapExpenseAllowance.ClientID %>').value = hdnDelCapExpenseAllowance + "|" + removeCapExpenseAllowance;
            }

            // Expense   : recordId ^ Country - City - ExpenseType - Currency - Amount - DailyActuals - TravelDays - Type 
            //Allowance : recordId^Country-City-Currency-Amount-TravelDays-Type




            //Expense
            if (type == "E") {

                var selectedExpenses = document.getElementById('<%=hdnCapExpense.ClientID %>').value.split('|');
                var selExpense = '';
                for (var i = 0; i < selectedExpenses.length; i++) {

                    if (selectedExpenses[i] != removeCapExpenseAllowance) {

                        if (selExpense == '') {
                            selExpense = selectedExpenses[i];
                        }
                        else {
                            selExpense = selExpense + '|' + selectedExpenses[i];
                        }
                    }
                }

                document.getElementById('<%=hdnCapExpense.ClientID %>').value = selExpense;


                var deletedE = removeCapExpenseAllowance.split('^')[1].split('-')[0] + '-' + removeCapExpenseAllowance.split('^')[1].split('-')[1] + '-' + removeCapExpenseAllowance.split('^')[1].split('-')[2];
                //console.log(deletedE);
                var selCapCityE = document.getElementById('<%=hdnCapCityExpense.ClientID %>').value.split('|');
                var selE = '';
                for (var i = 0; i < selCapCityE.length; i++) {

                    if (selCapCityE[i] != deletedE) {

                        if (selE == '') {
                            selE = selCapCityE[i];
                        }
                        else {
                            selE = selE + '|' + selCapCityE[i];
                        }
                    }
                }

                document.getElementById('<%=hdnCapCityExpense.ClientID %>').value = selE;
                //console.log(document.getElementById('<%=hdnCapCityExpense.ClientID %>').value);

            }
            else if (type == "A")//Allowance
            {

                var selectedAllowances = document.getElementById('<%=hdnCapAllowance.ClientID %>').value.split('|');
                var selAllowance = '';
                for (var i = 0; i < selectedAllowances.length; i++) {

                    if (selectedAllowances[i] != removeCapExpenseAllowance) {

                        if (selAllowance == '') {
                            selAllowance = selectedAllowances[i];
                        }
                        else {
                            selAllowance = selAllowance + '|' + selectedAllowances[i];
                        }
                    }
                }

                document.getElementById('<%=hdnCapAllowance.ClientID %>').value = selAllowance;


                var deletedA = removeCapExpenseAllowance.split('^')[1].split('-')[0] + '-' + removeCapExpenseAllowance.split('^')[1].split('-')[1];
                var selCapCityA = document.getElementById('<%=hdnCapCityAllowance.ClientID %>').value.split('|');
                var selA = '';
                for (var i = 0; i < selCapCityA.length; i++) {

                    if (selCapCityA[i] != deletedA) {

                        if (selA == '') {
                            selA = selCapCityA[i];
                        }
                        else {
                            selA = selA + '|' + selCapCityA[i];
                        }
                    }
                }

                document.getElementById('<%=hdnCapCityAllowance.ClientID %>').value = selA;
                //console.log(document.getElementById('<%=hdnCapCityAllowance.ClientID %>').value);

            }







        }

        //This function will Load the cities for the selected country.
        function LoadCitiesCapAllowance(id) {

            city = 'ctl00_cphTransaction_ddlCityCapAllowance';
            var paramList = 'requestSource=getAirPortList' + '&Country=' + document.getElementById(id).value + '&id=' + city; ;
            var url = "CityAjax.aspx";
            Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);

        }



        function AddExpenseAllowanceDiv(country, city, expenseType, currency, amount, dailyActuals, travelDays, Type, rowInfo) {
            //console.log("============Inside AddExpenseAllowanceDiv=========");

            var ctrlID = parseInt(document.getElementById('hdnExpenseTabControlsCount').value);
            ctrlID = ctrlID + 1;
            document.getElementById('hdnExpenseTabControlsCount').value = ctrlID;
            //console.log("ctrlID" + ctrlID);
            //console.log(country);
            //console.log(city);
            //console.log(expenseType);
            //console.log(currency);
            //console.log(amount);
            //console.log(dailyActuals);
            //console.log(travelDays);
            //console.log(Type);
            //console.log(rowInfo);

            var paramList = 'requestSource=getExpenseAllowanceHtml' + '&id=' + ctrlID + '&countryCode=' + country + '&cityCode=' + city + '&expenseType=' + expenseType + '&currency=' + currency + '&amount=' + amount + '&dailyActuals=' + dailyActuals + '&includeTravelDays=' + travelDays + '&type=' + Type + '&rowInfo=' + rowInfo;
            var url = "CorportatePoliciesExpenseAjax.aspx";
            if (Type == "E") {
                Ajax.onreadystatechange = GetExpenseHtml;
            }
            else {
                Ajax.onreadystatechange = GetAllowanceHtml;
            }
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function GetExpenseHtml(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        $("#onlyExpensesChildDiv:last").append(Ajax.responseText);
                        clearExpense();

                        $("#aonlyAllowance").trigger('click');
                        $("#aonlyExpenses").trigger('click');

                    }
                }
            }
        }


        function GetAllowanceHtml(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        $("#onlyAllowancesChildDiv:last").append(Ajax.responseText);
                        clearAllowance();
                        $("#aonlyExpenses").trigger('click');
                        $("#aonlyAllowance").trigger('click');


                    }
                }
            }
        }










        //Expense Types : Meal , Transportation ,Invitation

        function addCapExpenseAllowance(type) {



            var valid = false;

            //Expense Type 
            if (type == "E") {

                document.getElementById('errMess').style.display = "none";

                //Country
                if (Trim(document.getElementById('<%=ddlCountryCapExpense.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select country from the list .";
                }

                //City

                else if (Trim(document.getElementById('<%=ddlCityCapExpense.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select city from the list .";
                }
                //Expense Type
                else if (Trim(document.getElementById('<%=ddlCapExpenseType.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select expense type from the list .";
                }

                //Currency
                else if (Trim(document.getElementById('<%=ddlCapExpenseCurrency.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select currency from the list .";
                }

                //Amount
                else if (Trim(document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value) == "" || document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please add amount.";
                }




                else {

                    //Need to verify the City Expense Type duplicates
                    //Country
                    var selCapCityExpense = document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value;
                    selCapCityExpense += "-";

                    //City
                    selCapCityExpense += document.getElementById("<%=ddlCityCapExpense.ClientID %>").value;
                    selCapCityExpense += "-";

                    //ExpenseType
                    selCapCityExpense += document.getElementById("<%=ddlCapExpenseType.ClientID %>").value;

                    var capCityExpenseSaved = document.getElementById("<%=hdnCapCityExpense.ClientID %>").value.split('|');

                    var duplicateCount = 0;

                    for (var p = 0; p < capCityExpenseSaved.length; p++) {

                        if (selCapCityExpense == capCityExpenseSaved[p]) {
                            duplicateCount++;
                        }
                    }

                    //alert(duplicateCount);

                    if (duplicateCount >= 1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already addded this combination";
                        $('select').select2();
                    }



                    else {

                        valid = true;
                        var traveldays = "False";
                        if (document.getElementById('chkCapExpenseTravelDays').checked) {
                            traveldays = "True";
                        }

                        //Main :Row Information Capturing.

                        //Record Id (^)
                        //Country -
                        //City-
                        //ExpenseType-
                        //Currency-
                        //Amount-
                        //DailyActuals-
                        //Traveldays

                        var rowInfo = "-1";
                        rowInfo += "^";

                        //Country
                        rowInfo += document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value;
                        rowInfo += "-";

                        //City
                        rowInfo += document.getElementById("<%=ddlCityCapExpense.ClientID %>").value;
                        rowInfo += "-";

                        //ExpenseType
                        rowInfo += document.getElementById("<%=ddlCapExpenseType.ClientID %>").value;
                        rowInfo += "-";

                        //Currency
                        rowInfo += document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>").value;
                        rowInfo += "-";

                        //Amount
                        rowInfo += document.getElementById("<%=txtCapExpenseAmount.ClientID %>").value;
                        rowInfo += "-";

                        //Daily Actuals
                        rowInfo += "True";
                        rowInfo += "-";

                        //Traveldays
                        rowInfo += traveldays;
                        rowInfo += "-";

                        //Type -- Expense or Allowance
                        rowInfo += "E";



                        var capCityExpense = document.getElementById("<%=ddlCountryCapExpense.ClientID %>").value;
                        capCityExpense += "-";

                        //City
                        capCityExpense += document.getElementById("<%=ddlCityCapExpense.ClientID %>").value;
                        capCityExpense += "-";

                        //ExpenseType
                        capCityExpense += document.getElementById("<%=ddlCapExpenseType.ClientID %>").value;





                        if (document.getElementById("<%=hdnCapCityExpense.ClientID %>").value.length == 0) {
                            document.getElementById("<%=hdnCapCityExpense.ClientID %>").value = capCityExpense;
                        }
                        else {
                            document.getElementById("<%=hdnCapCityExpense.ClientID %>").value += "|" + capCityExpense;

                        }



                        if (document.getElementById("<%=hdnCapExpense.ClientID %>").value.length == 0) {
                            document.getElementById("<%=hdnCapExpense.ClientID %>").value = rowInfo;
                        }
                        else {
                            document.getElementById("<%=hdnCapExpense.ClientID %>").value += "|" + rowInfo;

                        }
                        var countriesE = document.getElementById("<%=ddlCountryCapExpense.ClientID %>");
                        var selectedCountryText = countriesE.options[countriesE.selectedIndex].text;

                        var citiesE = document.getElementById("<%=ddlCityCapExpense.ClientID %>");
                        var selectedCityText = citiesE.options[citiesE.selectedIndex].text;

                        var expType = document.getElementById("<%=ddlCapExpenseType.ClientID %>");
                        var selectedExpTypeText = expType.options[expType.selectedIndex].text;

                        var expCurrency = document.getElementById("<%=ddlCapExpenseCurrency.ClientID %>");
                        var selectedExpCurrencyText = expCurrency.options[expCurrency.selectedIndex].text;


                        AddExpenseAllowanceDiv(
                    selectedCountryText,
                    selectedCityText,
                    selectedExpTypeText,
                    selectedExpCurrencyText,
                    document.getElementById('<%=txtCapExpenseAmount.ClientID %>').value,
                    "Yes",
                    traveldays,
                    "E",
                    rowInfo

                    )
                    }

                }

            }

            //Allowance Type 
            if (type == "A") {

                document.getElementById('errMess').style.display = "none";

                //Country
                if (Trim(document.getElementById('<%=ddlCountryCapAllowance.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select country from the list .";
                }

                //City

                else if (Trim(document.getElementById('<%=ddlCityCapAllowance.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select city from the list .";
                }


                //Currency
                else if (Trim(document.getElementById('<%=ddlCurrencyCapAllowance.ClientID %>').value) == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select currency from the list .";
                }

                //Amount
                else if (Trim(document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value) == "" || document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please add amount.";
                }


                else {


                    //Need to verify the City country duplicates
                    //Country
                    var selCapCityAllowance = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value;
                    selCapCityAllowance += "-";

                    //City
                    selCapCityAllowance += document.getElementById("<%=ddlCityCapAllowance.ClientID %>").value;
                    var capCityAllowanceSaved = document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value.split('|');

                    //console.log(selCapCityAllowance);
                    //console.log(capCityAllowanceSaved);


                    var duplicateCount = 0;
                    for (var y = 0; y < capCityAllowanceSaved.length; y++) {
                        if (selCapCityAllowance == capCityAllowanceSaved[y]) {
                            duplicateCount++;
                        }
                    }

                    //alert(duplicateCount);

                    if (duplicateCount >= 1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already addded this combination";
                        $('select').select2();
                    }
                    else {





                        valid = true;
                        var traveldays = "False";
                        if (document.getElementById('chkCapAllowanceTravelDays').checked) {
                            traveldays = "True";
                        }


                        //Main :Row Information Capturing.

                        //Record Id (^)
                        //Country -
                        //City-
                        //Currency-
                        //Amount-
                        //Traveldays
                        //Type

                        var rowInfo = "-1";
                        rowInfo += "^";

                        //Country
                        rowInfo += document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value;
                        rowInfo += "-";

                        //City
                        rowInfo += document.getElementById("<%=ddlCityCapAllowance.ClientID %>").value;
                        rowInfo += "-";

                        //Currency
                        rowInfo += document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>").value;
                        rowInfo += "-";

                        //Amount
                        rowInfo += document.getElementById("<%=txtCapAllowanceAmount.ClientID %>").value;
                        rowInfo += "-";

                        //Traveldays
                        rowInfo += traveldays;
                        rowInfo += "-";

                        //Type -- Expense or Allowance
                        rowInfo += "A";


                        var capCityAllowance = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>").value;
                        capCityAllowance += "-";

                        //City
                        capCityAllowance += document.getElementById("<%=ddlCityCapAllowance.ClientID %>").value;

                        if (document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value.length == 0) {
                            document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value = capCityAllowance;
                        }
                        else {
                            document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value += "|" + capCityAllowance;

                        }

                        //console.log(document.getElementById("<%=hdnCapCityAllowance.ClientID %>").value);




                        if (document.getElementById("<%=hdnCapAllowance.ClientID %>").value.length == 0) {
                            document.getElementById("<%=hdnCapAllowance.ClientID %>").value = rowInfo;
                        }
                        else {
                            document.getElementById("<%=hdnCapAllowance.ClientID %>").value += "|" + rowInfo;
                        }

                        var countriesE = document.getElementById("<%=ddlCountryCapAllowance.ClientID %>");
                        var selectedCountryText = countriesE.options[countriesE.selectedIndex].text;

                        var citiesE = document.getElementById("<%=ddlCityCapAllowance.ClientID %>");
                        var selectedCityText = citiesE.options[citiesE.selectedIndex].text;



                        var expCurrency = document.getElementById("<%=ddlCurrencyCapAllowance.ClientID %>");
                        var selectedExpCurrencyText = expCurrency.options[expCurrency.selectedIndex].text;


                        AddExpenseAllowanceDiv(
                    selectedCountryText,
                    selectedCityText,
                    "Allowance",
                    selectedExpCurrencyText,
                    document.getElementById('<%=txtCapAllowanceAmount.ClientID %>').value,
                    "No",
                    traveldays,
                    "A",
                    rowInfo
                    )


                    }


                }
            }
            $('select').select2();
            return valid;


        } //EOF : addCapExpenseAllowance function







        //This function will Load the cities for the selected country.
        function LoadCitiesCapExpense(id) {

            city = 'ctl00_cphTransaction_ddlCityCapExpense';
            var paramList = 'requestSource=getAirPortList' + '&Country=' + document.getElementById(id).value + '&id=' + city; ;
            var url = "CityAjax.aspx";
            Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);

        }

        //This function validates the user input which accepts only +ve integers with or without decimals.
        //This function will be invoked when the user enters the amount.
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }


        /***************************Start : Expense Tab Javascript Functions****************/





        //Please do not remove the following comments 
        // *********************Very Important ***************
        //The following symbols are used for identification
        // ^(Upper Triangle) -- Used to maintain the record identity (id of the record)
        // -(Minus) -- Used to combine both the values eg:Country -City
        // | (Pipe) -- Used to separate each poilcy detail.


        var Ajax; //New Ajax object.
        var city;
        var cities = [];
        var cabinType;
        var cabinTypes = [];

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        //This function removes any unwanted spaces at both starting and ending position of a string
        function Trim(stringToTrim) {
            return stringToTrim.replace(/^\s+|\s+$/g, "");
        }


        //When the user clicks on the save button without filling the agent id and policy code this method returns a message.
        function Validate() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (document.getElementById('<%=preTripCostExceeds.ClientID %>').checked == true && document.getElementById('<%=ddlPreTripCurrency.ClientID %>').value == "0") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select currency from the list .";
            }

            else if (document.getElementById('<%=preTripCostExceeds.ClientID %>').checked == true && document.getElementById('<%=txtPreTripAmount.ClientID %>').value.length ==0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }

            else if (document.getElementById('<%=chkRelaunchPriceChange.ClientID %>').checked == true && document.getElementById('<%=ddlRelaunchPriceChange.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please  select currency from the list";
            }


            else if (document.getElementById('<%=chkRelaunchPriceChange.ClientID %>').checked == true && document.getElementById('<%=txtRelaunchPriceAmount.ClientID %>').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }          
            else {
                valid = true;
            }

            return valid;
        }


        //Clears all the values that are previously filled or prefetched .
        function Clear() {
            if (document.getElementById('<%=ddlAgents.ClientID %>')) {

                document.getElementById('<%=ddlAgents.ClientID %>').value = "0";
            }
            if (document.getElementById('<%=txtPolicyName.ClientID %>')) {
                document.getElementById('<%=txtPolicyName.ClientID %>').value = "";
            }
            if (document.getElementById('ctl00_cphTransaction_txtFromDate_Date')) {
                document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value = "";
            }
            if (document.getElementById('<%=ddlCountry.ClientID %>')) {
                document.getElementById('<%=ddlCountry.ClientID %>').value = "0";
            }
            if (document.getElementById('<%=ddlCity.ClientID %>')) {
                document.getElementById('<%=ddlCity.ClientID %>').value = "0";
            }
            if (document.getElementById('addCon')) {
                document.getElementById('addCon').checked = false;
            }
            if (document.getElementById('errMess')) {

                document.getElementById('errMess').style.display = "none";
                document.getElementById('errMess').innerHTML = "";
            }
            if (document.getElementById('<%=ddlAirline.ClientID %>')) {
                document.getElementById('<%=ddlAirline.ClientID %>').value = "0";
            }
            if (document.getElementById('ctl00_cphTransaction_txtRateCode')) {
                document.getElementById('ctl00_cphTransaction_txtRateCode').value = "";
            }
            if (document.getElementById('securityrules')) {
                document.getElementById('securityrules').checked = false;
            }

            if (document.getElementById('<%=ddlPrefAirline.ClientID %>')) {
                document.getElementById('<%=ddlPrefAirline.ClientID %>').value = "0";
            }

            if (document.getElementById('<%=ddlAvoidAirline.ClientID %>')) {
                document.getElementById('<%=ddlAvoidAirline.ClientID %>').value = "0";
            }

            if (document.getElementById('allowAnyFare')) {
                document.getElementById('allowAnyFare').checked = false;
            }

            if (document.getElementById('allowLowestFare %>')) {
                document.getElementById('allowLowestFare').checked = false;
            }

            if (document.getElementById('<%=ddlCabinType.ClientID %>')) {
                document.getElementById('<%=ddlCabinType.ClientID %>').value = "0";
            }

            if (document.getElementById('<%=ddlExceptionCabinType.ClientID %>')) {
                document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "0";
            }

            if (document.getElementById('exception1')) {
                document.getElementById('exception1').checked = true;
            }

            if (document.getElementById('<%=ddlExceptionCity.ClientID %>')) {
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = false;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
            }



            if (document.getElementById('<%=ddlCabinTypeRule2.ClientID %>')) {
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
            }



            if (document.getElementById('txtDuration')) {
                document.getElementById('txtDuration').value = "";
                document.getElementById('txtDuration').disabled = true;
            }



            if (document.getElementById('exception2')) {
                document.getElementById('exception2').checked = false;
            }

            if (document.getElementById('exception3')) {
                document.getElementById('exception3').checked = false;
            }

            if (document.getElementById('<%=hdnDelDestinationsToAvoid.ClientID %>')) {
                document.getElementById('<%=hdnDelDestinationsToAvoid.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnDelAirlinesNego.ClientID %>')) {
                document.getElementById('<%=hdnDelAirlinesNego.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnDelPreferAirline.ClientID %>')) {
                document.getElementById('<%=hdnDelPreferAirline.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnDelAvoidAirlines.ClientID %>')) {
                document.getElementById('<%=hdnDelAvoidAirlines.ClientID %>').value = "";
            }

            if (document.getElementById('<%=hdnDelDefaultCabin.ClientID %>')) {
                document.getElementById('<%=hdnDelDefaultCabin.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnDelCabinTypeException.ClientID %>')) {
                document.getElementById('<%=hdnDelCabinTypeException.ClientID %>').value = "";
            }

            if (document.getElementById('<%=hdnFromDate.ClientID %>')) {
                document.getElementById('<%=hdnFromDate.ClientID %>').value = "";
            }

            if (document.getElementById('<%=hdnDestinations.ClientID %>')) {
                document.getElementById('<%=hdnDestinations.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnAirlineNegotiated.ClientID %>')) {
                document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value = "";
            }

            if (document.getElementById('<%=hdnAirlinePrefered.ClientID %>')) {
                document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnAirlineAvoid.ClientID %>')) {
                document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnAirFareShopping.ClientID %>')) {
                document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = "";
            }


            if (document.getElementById('<%=hdnSecurityRules.ClientID %>')) {
                document.getElementById('<%=hdnSecurityRules.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnDefaultCabin.ClientID %>')) {
                document.getElementById('<%=hdnDefaultCabin.ClientID %>').value = "";
            }
            if (document.getElementById('<%=hdnCabinTypeException.ClientID %>')) {
                document.getElementById('<%=hdnCabinTypeException.ClientID %>').value = "";
            }
            if (document.getElementById('childFromDates')) {
                document.getElementById('childFromDates').value = "0";
            }
            if (document.getElementById('<%=hdfEMId.ClientID %>')) {
                document.getElementById('<%=hdfEMId.ClientID %>').value = "0";
            }
            if (document.getElementById('<%=hdfMode.ClientID %>')) {
                document.getElementById('<%=hdfMode.ClientID %>').value = "0";
            }
            editedTravelDatesToAvoid = "";
            editedDestination = "";
            editedNegFares = "";
            editedAirlinePrefered = "";
            editedAirlineAvoid = "";
            editedCabinTypeException = "";

            //Expense tab clear fields
            document.getElementById('<%=hdnCapExpense.ClientID %>').value = "";
            document.getElementById('<%=hdnCapAllowance.ClientID %>').value = "";
            document.getElementById('<%=hdnDelCapExpenseAllowance.ClientID %>').value = "";
            document.getElementById('<%=hdnCapCityExpense.ClientID %>').value = "";
            document.getElementById('<%=hdnCapCityAllowance.ClientID %>').value = "";
            document.getElementById('hdnExpenseTabControlsCount').value = "0";
            editedExpenseAllowance = "";
            editedExpenseAllowanceRowId = "";
            editedCapCityExpense = "";
            editedCapCityAllowance = "";

            //Approval Tab Clear Fields

            //PRE-TRIP APPROVAL
            document.getElementById('<%=preTripNoApproval.ClientID %>').checked = true;
            document.getElementById('<%=preTripAlways.ClientID %>').checked = false;
            document.getElementById('<%=preTripCostExceeds.ClientID %>').checked = false;
            document.getElementById('<%=ddlPreTripCurrency.ClientID %>').value = "0";
            document.getElementById('<%=txtPreTripAmount.ClientID %>').value = "";



            //RELAUNCH APPROVAL PROCESS
            document.getElementById('<%=chkRelaunchPriceChange.ClientID %>').checked = false;
            document.getElementById('<%=chkRelaunchItineraryChange.ClientID %>').checked = false;
            document.getElementById('<%=chkRelaunchItineraryChange.ClientID %>').value = "0";
            document.getElementById('<%=txtRelaunchPriceAmount.ClientID %>').value = "";

            //PRE-VISA APPROVAL
            document.getElementById('<%=preVisaNoApproval.ClientID %>').checked = true;
            document.getElementById('<%=preVisaAlways.ClientID %>').checked = false;
            
        }


        //Gets the cabin types for the supplied default cabin type 
        function LoadCabinTypes(id) {
            //alert(id);
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlCabinType.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select default cabin type from the list .";
            }
            else {

                var ddlCabinType = document.getElementById("<%=ddlCabinType.ClientID%>");

                var ddlCabinTypeText = ddlCabinType.options[ddlCabinType.selectedIndex].text;

                if (document.getElementById('<%=hdnDefaultCabin.ClientID %>').value.length == 0) {
                    document.getElementById('<%=hdnDefaultCabin.ClientID %>').value = ddlCabinTypeText;
                }
                else {
                    document.getElementById('<%=hdnDefaultCabin.ClientID %>').value = document.getElementById('<%=hdnDefaultCabin.ClientID %>').value.split('^')[0] + '^' + ddlCabinTypeText;
                    //alert(document.getElementById('<%=hdnDefaultCabin.ClientID %>').value);
                }

                appendDefaultCabin(ddlCabinTypeText);
                document.getElementById('<%=ddlCabinType.ClientID %>').disabled = true;

                cabinType = 'ctl00_cphTransaction_ddlExceptionCabinType';
                //alert(document.getElementById(id).value);
                var paramList = 'requestSource=getCabinTypeList' + '&removeCabinType=' + document.getElementById(id).value + '&id=' + cabinType; ;
                var url = "CityAjax.aspx";
                Ajax.onreadystatechange = GetCabinTypesList;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }

        }
        //Gets the cabin types
        function GetCabinTypesList(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        cabinType = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(cabinType);
                        if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Cabin Type";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }

                            if (cabinTypes.length > 0 && cabinTypes[0].trim().length > 0) {
                                var id = eval(cabinType.split('-')[1]);
                                if (id == undefined) {
                                    id = 0;
                                }
                                else {
                                    id = eval(id - 1);
                                }

                                ddl.value = cabinTypes[id];
                            }
                        }
                    }
                }
            }
        }

        //Gets the cities for the supplied country id

        function LoadCities(id) {
            //alert(document.getElementById(id).value);
            city = 'ctl00_cphTransaction_ddlCity';
            var paramList = 'requestSource=getAirPortList' + '&Country=' + document.getElementById(id).value + '&id=' + city; ;
            var url = "CityAjax.aspx";
            Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);

        }
        //Ajax call which brings the cities list for the supplied country id
        function ShowCitiesList(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        city = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(city);
                        if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select City";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }

                            if (cities.length > 0 && cities[0].trim().length > 0) {
                                var id = eval(city.split('-')[1]);
                                if (id == undefined) {
                                    id = 0;
                                }
                                else {
                                    id = eval(id - 1);
                                }

                                ddl.value = cities[id];
                            }
                        }
                    }
                }
            }
        }

        //This method is used to show the default cabin selected by the user
        function appendDefaultCabin(defaultCabin) {
            document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divDefaultCabin = '<div id = "' + dynamicDivId + '" class="updated-row-content">';
            var dynamicSpanId = 'span-row' + children;
            divDefaultCabin += '<span class="text" id ="' + dynamicSpanId + '">' + defaultCabin + '</span>'; //Value which is displayed to the end user
            divDefaultCabin += '<a href="#" class="add-more remove-row"  onclick="javascript:removeDefaultCabinDiv(' + children + ')">';
            divDefaultCabin += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divDefaultCabin += '</a>';
            divDefaultCabin += '<a href="#" class="add-more"  onclick="javascript:editDefaultCabinDiv(' + children + ')">';
            divDefaultCabin += '<img src="Images/grid/wg_edit.gif" />';
            divDefaultCabin += '</a>';
            divDefaultCabin += '</div>';
            document.getElementById('defaultCabinCollection').style.display = 'block';
            document.getElementById('defaultCabinCollection').innerHTML += divDefaultCabin;
        }

        //This function is triggerd when the user clicks on the default cabin type that is previously selected.
        function editDefaultCabinDiv(index) {
            document.getElementById('<%=ddlCabinType.ClientID %>').disabled = false;
            document.getElementById('defaultCabinCollection').innerHTML = '';

        }

        //This function is triggered when the user removes the default cabin type that is previously selected  by the user
        function removeDefaultCabinDiv(index) {

            document.getElementById('div-row' + index).remove();
            document.getElementById('<%=hdnDefaultCabin.ClientID %>').value = '';
            document.getElementById('<%=ddlCabinType.ClientID %>').disabled = false;

        }

        //This function adds the TRAVEL DATES TO AVOID 
        function addFromDate() {

            var valid = false;
            var fromDate = document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value;
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }

            else if (fromDate == null || fromDate == " " || fromDate.length == 0 || fromDate == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "First select from date.";
            }
            else {

                var hdnFromDate = document.getElementById('<%=hdnFromDate.ClientID %>').value;
                
                if (hdnFromDate.length == 0 && document.getElementById('fromDatesCollection').children.length == 0) {
                    document.getElementById('<%=hdnFromDate.ClientID %>').value = fromDate;
                    document.getElementById('childFromDates').value = "1";
                    appendChildDates(fromDate);
                    document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value = "";
                }
                else {
                    if (hdnFromDate.indexOf(fromDate) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this date !";
                    }
                    else {
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
                        appendChildDates(fromDate);
                        document.getElementById('<%=hdnFromDate.ClientID %>').value = hdnFromDate + "|" + fromDate;
                        alert(document.getElementById('<%=hdnFromDate.ClientID %>').value);
                        valid = true;
                        document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value = "";
                    }
                }
            }
            return valid;
        }

        //This function deletes the selected TRAVEL DATES TO AVOID
        function removeChildDateDiv(index) {
            var removeDate = document.getElementById('span-row' + index).innerHTML;
            document.getElementById('div-row' + index).remove();
            var selectedFromDates = document.getElementById('<%=hdnFromDate.ClientID %>').value.split('|');
            var selDates = '';
            for (var i = 0; i < selectedFromDates.length; i++) {
                if (selectedFromDates[i] != removeDate) {

                    if (selDates == '') {
                        selDates = selectedFromDates[i];
                    }
                    else {
                        selDates = selDates + '|' + selectedFromDates[i];
                    }
                }
            }
            document.getElementById('<%=hdnFromDate.ClientID %>').value = selDates;
        }

        var editedTravelDatesToAvoid = "";

        //This function edits the selected TRAVEL DATES TO AVOID
        function editChildDateDiv(index) {
            //alert(index);
            var editDate = document.getElementById('span-row' + index).innerHTML;
            //alert(editDate);
            document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value = editDate;
            editedTravelDatesToAvoid = editDate + "~" + index;
            //alert(editedTravelDatesToAvoid);
            document.getElementById('updateFromDate').style.display = "block";
            document.getElementById('addFromDate').style.display = "none";
        }

        //This function updates the selected TRAVEL DATES TO AVOID
        function updateFromDate() {
            var valid = false;
            document.getElementById('errMess').style.display = "none";
            var hdnFromDate = document.getElementById('<%=hdnFromDate.ClientID %>').value;
            if (hdnFromDate.indexOf(document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value) != -1) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "You have already selected this date !";
            }
            else {
                document.getElementById('updateFromDate').style.display = "none";
                document.getElementById('addFromDate').style.display = "block";
                document.getElementById("span-row" + editedTravelDatesToAvoid.split('~')[1]).innerHTML = document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value;
                var selectedFromDates = document.getElementById('<%=hdnFromDate.ClientID %>').value.split('|');
                var selDates = '';
                for (var i = 0; i < selectedFromDates.length; i++) {


                    if (selDates == '') {

                        if (selectedFromDates[i] == editedTravelDatesToAvoid.split('~')[0]) {
                            selDates = document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value;
                        }
                        else {

                            selDates = selectedFromDates[i];
                        }
                    }
                    else {
                        if (selectedFromDates[i] == editedTravelDatesToAvoid.split('~')[0]) {
                            selDates = selDates + '|' + document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value;
                        }
                        else {
                            selDates = selDates + '|' + selectedFromDates[i];
                        }
                    }

                }
                editedTravelDatesToAvoid = "";
                document.getElementById('ctl00_cphTransaction_txtFromDate_Date').value = "";
                document.getElementById('<%=hdnFromDate.ClientID %>').value = selDates;
                // alert(document.getElementById('<%=hdnFromDate.ClientID %>').value);
                valid = true;
            }
            return valid;

        }



        //This function appends all the selected TRAVEL DATES TO AVOID
        function appendChildDates(fromDate) {

            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divFromDates = '<div id = "' + dynamicDivId + '" class="updated-row-content">';
            var dynamicSpanId = 'span-row' + children;
            divFromDates += '<span class="text" id ="' + dynamicSpanId + '">' + fromDate + '</span>';
            divFromDates += '<a href="#" class="add-more remove-row"  onclick="javascript:removeChildDateDiv(' + children + ')">';
            divFromDates += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divFromDates += '</a>';
            divFromDates += '<a href="#" class="add-more"  onclick="javascript:editChildDateDiv(' + children + ')">';
            divFromDates += '<img src="Images/grid/wg_edit.gif" />';
            divFromDates += '</a>';
            divFromDates += '</div>';

            document.getElementById('fromDatesCollection').style.display = 'block';
            document.getElementById('fromDatesCollection').innerHTML += divFromDates;
        }

        //This function appends the selected DESTINATIONS TO AVOID
        function appendDestinations(cityName) {
            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divDestinations = '<div id = "' + dynamicDivId + '" class="updated-row-content">';
            var dynamicSpanId = 'span-row' + children;
            divDestinations += '<span style="display:none" id ="rowId' + dynamicSpanId + '">' + cityName.split('^')[0] + '</span>';
            divDestinations += '<span class="text" id ="' + dynamicSpanId + '">' + cityName.split('^')[1] + '</span>';
            divDestinations += '<a href="#" class="add-more remove-row"  onclick="javascript:removeChildDestinationsDiv(' + children + ')">';
            divDestinations += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divDestinations += '</a>';
            divDestinations += '<a href="#" class="add-more"  onclick="javascript:editChildChildDestinationsDiv(' + children + ')">';
            divDestinations += '<img src="Images/grid/wg_edit.gif" />';
            divDestinations += '</a>';
            divDestinations += '</div>';
            document.getElementById('destinationsCollection').style.display = 'block';
            document.getElementById('destinationsCollection').innerHTML += divDestinations;
        }

        //This function adds the DESTINATIONS TO AVOID
        function addDestinations() {

            var valid = false;
            var selDestination = '';
            var avoidConnection = 'No';
            document.getElementById('errMess').style.display = "none";
            if (document.getElementById('addCon').checked) {
                avoidConnection = 'Yes';
            }
            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlCountry.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Country from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlCity.ClientID %>').value) == "0") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select City from the list .";
            }
            else {

                document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
                var recordId = "-1";
                selDestination = recordId + "^" + document.getElementById('<%=ddlCountry.ClientID %>').value + "-" + Trim(document.getElementById('<%=ddlCity.ClientID %>').value) + "- Avoid Connection:" + avoidConnection;
                var hdnDestinations = document.getElementById('<%=hdnDestinations.ClientID %>').value;
                if (hdnDestinations.length == 0 && document.getElementById('destinationsCollection').children.length == 0) {
                    document.getElementById('<%=hdnDestinations.ClientID %>').value = selDestination;
                    appendDestinations(selDestination);
                    valid = true;
                }
                else {

                    var selectedDestinationsWithoutId = document.getElementById('<%=hdnDestinations.ClientID %>').value.split('|');
                    var selDestinationWithoutId = '';
                    for (var i = 0; i < selectedDestinationsWithoutId.length; i++) {


                        if (selDestinationWithoutId == '') {

                            selDestinationWithoutId = selectedDestinationsWithoutId[i].split('^')[1].split('-')[0] + '-' + selectedDestinationsWithoutId[i].split('^')[1].split('-')[1];
                        }
                        else {
                            selDestinationWithoutId = selDestinationWithoutId + '|' + selectedDestinationsWithoutId[i].split('^')[1].split('-')[0] + '-' + selectedDestinationsWithoutId[i].split('^')[1].split('-')[1];
                        }

                    }
                    if (selDestinationWithoutId.indexOf(selDestination.split('^')[1].split('-')[0] + '-' + selDestination.split('^')[1].split('-')[1]) != -1) {

                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this destination !";
                    }
                    else {
                        appendDestinations(selDestination);
                        document.getElementById('<%=hdnDestinations.ClientID %>').value = hdnDestinations + "|" + selDestination;
                        valid = true;
                    }

                }
                document.getElementById('<%=ddlCountry.ClientID %>').value = "0";
                document.getElementById('<%=ddlCity.ClientID %>').value = "0";
                document.getElementById('addCon').checked = false;

                $('select').select2();
            }


            return valid;

        }




        var editedDestination = "";
        //This function edits the selected DESTINATIONS TO AVOID
        function editChildChildDestinationsDiv(index) {
            debugger;
            document.getElementById('addDesToAvoid').style.display = "none";
            document.getElementById('updateDesToAvoid').style.display = "block";

            var editDestination = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            //alert(editDestination);

            editedDestination = editDestination + "~" + index;
            //alert(editedDestination);
            var selCountry = editDestination.split('^')[1].split('-')[0];
            //alert(selCountry);
            var selCity = editDestination.split('^')[1].split('-')[1];
            //alert(selCity);
            var avoidConnection = editDestination.split('^')[1].split('-')[2].split(':')[1];
            //alert(avoidConnection);
            $('#ctl00_cphTransaction_ddlCountry').select2("val", selCountry);
           // document.getElementById('<%=ddlCountry.ClientID %>').select2 = selCountry;
            LoadCities('<%=ddlCountry.ClientID %>');

            //console.log(selCity);

            //Get all the options of the dropdown
            var options = document.getElementById('<%=ddlCity.ClientID %>').options;
            // console.log(options.length);
            var p = 0;
            for (var i = 0; i < options.length; i++) {
                // console.log(options[i].value);
                if (options[i].value == selCity) {
                    //  console.log("Inside the if loop");
                    options[i].selected = true;
                    p = i;
                    break;
                }
            }
            //alert(p);
            $('#ctl00_cphTransaction_ddlCity').select2("val", options[p].value);
          //  document.getElementById('<%=ddlCity.ClientID %>').options[p].selected = true;
            //console.log("Came out of the loop");


            //  document.getElementById('<%=ddlCity.ClientID %>').value = selCity;


            if (avoidConnection == 'Yes') {
                document.getElementById('addCon').checked = true;
            }
            else {
                document.getElementById('addCon').checked = false;
            }
        }

        //This function updates the selected DESTINATIONS TO AVOID
        function updateDestinations() {

            var valid = false;
            var selDestination = '';
            var avoidConnection = 'No';
            document.getElementById('errMess').style.display = "none";
            if (document.getElementById('addCon').checked) {
                avoidConnection = 'Yes';
            }
            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlCountry.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Country from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlCity.ClientID %>').value) == "0") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select City from the list .";
            }

            else {
                //var recordId = editedDestination.split('~')[0].split('^')[0];
                selDestnation = document.getElementById('<%=ddlCountry.ClientID %>').value + "-" + Trim(document.getElementById('<%=ddlCity.ClientID %>').value) + "- Avoid Connection:" + avoidConnection;
                //alert(selDestnation);
                var hdnDestinations = document.getElementById('<%=hdnDestinations.ClientID %>').value;
                //alert(hdnDestinations);

                var selectedDestinationsWithoutId = document.getElementById('<%=hdnDestinations.ClientID %>').value.split('|');
                var selDestinationWithoutId = '';
                for (var i = 0; i < selectedDestinationsWithoutId.length; i++) {


                    if (selDestinationWithoutId == '') {

                        selDestinationWithoutId = selectedDestinationsWithoutId[i].split('^')[1].split('-')[0] + '-' + selectedDestinationsWithoutId[i].split('^')[1].split('-')[1];
                    }
                    else {
                        selDestinationWithoutId = selDestinationWithoutId + '|' + selectedDestinationsWithoutId[i].split('^')[1].split('-')[0] + '-' + selectedDestinationsWithoutId[i].split('^')[1].split('-')[1];
                    }

                }
                if (selDestinationWithoutId.indexOf(selDestnation.split('-')[0] + '-' + selDestnation.split('-')[1]) != -1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already selected this destination !";
                }
                else {

                    document.getElementById("span-row" + editedDestination.split('~')[1]).innerHTML = selDestnation;
                    var selectedDestinations = document.getElementById('<%=hdnDestinations.ClientID %>').value.split('|');
                    var selDestination = '';
                    for (var i = 0; i < selectedDestinations.length; i++) {

                        //// alert(selectedDestinations[i]);
                        if (selDestination == '') {
                            if (selectedDestinations[i] == editedDestination.split('~')[0]) {
                                selDestination = editedDestination.split('~')[0].split('^')[0] + '^' + selDestnation;
                            }
                            else {
                                selDestination = selectedDestinations[i];
                            }
                        }
                        else {
                            if (selectedDestinations[i] == editedDestination.split('~')[0]) {
                                selDestination = selDestination + '|' + editedDestination.split('~')[0].split('^')[0] + '^' + selDestnation;
                            }
                            else {
                                selDestination = selDestination + '|' + selectedDestinations[i];
                            }
                        }
                    }

                    document.getElementById('<%=hdnDestinations.ClientID %>').value = selDestination;
                    //// alert(document.getElementById('<%=hdnDestinations.ClientID %>').value);
                    document.getElementById('updateDesToAvoid').style.display = 'none';
                    document.getElementById('addDesToAvoid').style.display = 'block';
                    editedDestination = "";
                    document.getElementById('<%=ddlCountry.ClientID %>').value = "0";
                    document.getElementById('<%=ddlCity.ClientID %>').value = "0";
                    document.getElementById('addCon').checked = false;
                    valid = true;

                }

            }
        }

        //This function removes the selected destination combination in DESTINATIONS TO AVOID section
        function removeChildDestinationsDiv(index) {

            var removeDestination = document.getElementById('rowIdspan-row' + index).innerHTML + '^' + document.getElementById('span-row' + index).innerHTML;
            //// alert(removeDestination);

            var hdnDelDestinations = document.getElementById('<%=hdnDelDestinationsToAvoid.ClientID %>').value;
            if (hdnDelDestinations.length == 0) {
                document.getElementById('<%=hdnDelDestinationsToAvoid.ClientID %>').value = removeDestination;
            }
            else {
                document.getElementById('<%=hdnDelDestinationsToAvoid.ClientID %>').value = hdnDelDestinations + "|" + removeDestination;
            }

            //// alert(document.getElementById('<%=hdnDelDestinationsToAvoid.ClientID %>').value);

            document.getElementById('div-row' + index).remove();
            var selectedDestinations = document.getElementById('<%=hdnDestinations.ClientID %>').value.split('|');
            var selDestination = '';
            for (var i = 0; i < selectedDestinations.length; i++) {
                //// alert(selectedDestinations[i].split('^'));
                if (selectedDestinations[i] != removeDestination) {

                    if (selDestination == '') {
                        selDestination = selectedDestinations[i];
                    }
                    else {
                        selDestination = selDestination + '|' + selectedDestinations[i];
                    }
                }
            }
            //// alert(selDestination);
            document.getElementById('<%=hdnDestinations.ClientID %>').value = selDestination;
            //// alert(document.getElementById('<%=hdnDestinations.ClientID %>').value);
        }


        //------------------ Start:AIRLINES WITH NEGOTIATED FARES----------------------
        function addAirLinesWithNegotiatedFares() {

            var valid = false;
            var selAirline = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlAirline.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Airline from the list .";
            }
            else if (Trim(document.getElementById('<%=txtRateCode.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add special rate code.";
            }
            else {

                document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
                var recordId = "-1";
                selAirline = recordId + "^" + Trim(document.getElementById('<%=ddlAirline.ClientID %>').value) + "-" + Trim(document.getElementById('<%=txtRateCode.ClientID %>').value);

                //// alert(selAirline);
                var hdnAirlineNegotiated = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value;
                //// alert(hdnAirlineNegotiated);

                if (hdnAirlineNegotiated.length == 0 && document.getElementById('airlineNegotiatedCollection').children.length == 0) {
                    document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value = selAirline;
                    // alert(selAirline);
                    // alert(document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value);
                    appendAirlinenegotiated(selAirline);
                    valid = true;
                }
                else {

                    var selectedAirlinesNegotiatedWithoutId = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value.split('|');
                    var selAirlineWithoutId = '';
                    for (var i = 0; i < selectedAirlinesNegotiatedWithoutId.length; i++) {
                        if (selAirlineWithoutId == '') {
                            selAirlineWithoutId = selectedAirlinesNegotiatedWithoutId[i].split('^')[1].split('-')[0] + '-' + selectedAirlinesNegotiatedWithoutId[i].split('^')[1].split('-')[1];
                        }
                        else {
                            selAirlineWithoutId = selAirlineWithoutId + '|' + selectedAirlinesNegotiatedWithoutId[i].split('^')[1].split('-')[0] + '-' + selectedAirlinesNegotiatedWithoutId[i].split('^')[1].split('-')[1];
                        }
                    }
                    if (selAirlineWithoutId.indexOf(selAirline.split('^')[1].split('-')[0] + '-' + selAirline.split('^')[1].split('-')[1]) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this airline !";
                    }
                    else {
                        appendAirlinenegotiated(selAirline);
                        document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value = hdnAirlineNegotiated + "|" + selAirline;
                        valid = true;
                    }

                }
                //// alert('out of the block');
                document.getElementById('<%=ddlAirline.ClientID %>').value = "0";
                document.getElementById('<%=txtRateCode.ClientID %>').value = "";
                $('select').select2();
            }
            return valid;
        }


        function appendAirlinenegotiated(selAirline) {
            //// alert("inside the append section");
            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divAirlinenegotiated = '<div id = "' + dynamicDivId + '" class="updated-row-content">';
            var dynamicSpanId = 'span-row' + children;
            divAirlinenegotiated += '<span style="display:none" id ="rowId' + dynamicSpanId + '">' + selAirline.split('^')[0] + '</span>';
            divAirlinenegotiated += '<span class="text" id ="' + dynamicSpanId + '">' + selAirline.split('^')[1] + '</span>';
            divAirlinenegotiated += '<a href="#" class="add-more remove-row"  onclick="javascript:removeAirlineNegotiatedDiv(' + children + ')">';
            divAirlinenegotiated += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divAirlinenegotiated += '</a>';
            divAirlinenegotiated += '<a href="#" class="add-more remove-row"  onclick="javascript:editAirlineNegotiatedDiv(' + children + ')">';
            divAirlinenegotiated += '<img src="Images/grid/wg_edit.gif">';
            divAirlinenegotiated += '</a>';
            divAirlinenegotiated += '</div>';
            document.getElementById('airlineNegotiatedCollection').style.display = 'block';
            document.getElementById('airlineNegotiatedCollection').innerHTML += divAirlinenegotiated;

        }
        var editedNegFares = "";
        function editAirlineNegotiatedDiv(index) {

            document.getElementById('addAirLinesWithNegotiatedFares').style.display = "none";
            document.getElementById('updateAirLinesWithNegotiatedFares').style.display = "block";
            var editNegFares = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            // alert(editNegFares);
            editedNegFares = editNegFares + "~" + index;
            // alert(editedNegFares);
            var selAirline = editNegFares.split('^')[1].split('-')[0];
            // alert(selAirline);
            var selSpecialRateCode = editNegFares.split('^')[1].split('-')[1];
            // alert(selSpecialRateCode);
             $('#ctl00_cphTransaction_ddlAirline').select2("val", selAirline);
           // document.getElementById('<%=ddlAirline.ClientID %>').value = selAirline;
            document.getElementById('<%=txtRateCode.ClientID %>').value = selSpecialRateCode;
        }


        function updateAirLinesWithNegotiatedFares() {
            var valid = false;
            var selAirline = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlAirline.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Airline from the list .";
            }
            else if (Trim(document.getElementById('<%=txtRateCode.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add special rate code.";
            }
            else {
                selAirline = Trim(document.getElementById('<%=ddlAirline.ClientID %>').value) + "-" + Trim(document.getElementById('<%=txtRateCode.ClientID %>').value);

                // alert(selAirline);
                //var hdnAirlineNegotiated = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value;


                var selectedAirlinesNegotiatedWithoutId = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value.split('|');
                // alert(selectedAirlinesNegotiatedWithoutId);
                var selAirlineNegotiatedWithoutId = '';
                for (var i = 0; i < selectedAirlinesNegotiatedWithoutId.length; i++) {
                    // alert(selectedAirlinesNegotiatedWithoutId[i].split('^')[1]);
                    if (selAirlineNegotiatedWithoutId == '') {
                        selAirlineNegotiatedWithoutId = selectedAirlinesNegotiatedWithoutId[i].split('^')[1];
                    }
                    else {
                        selAirlineNegotiatedWithoutId = selAirlineNegotiatedWithoutId + '|' + selectedAirlinesNegotiatedWithoutId[i].split('^')[1];
                    }
                }

                //// alert(hdnAirlineNegotiated);
                if (selAirlineNegotiatedWithoutId.indexOf(selAirline) != -1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already selected this airline !";
                }

                else {
                    document.getElementById("span-row" + editedNegFares.split('~')[1]).innerHTML = selAirline;
                    document.getElementById('<%=ddlAirline.ClientID %>').value = "0";
                    document.getElementById('<%=txtRateCode.ClientID %>').value = "";
                    var selAirLineNeg = '';
                    var selectedAirlinesNegotiated = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value.split('|');
                    for (var i = 0; i < selectedAirlinesNegotiated.length; i++) {
                        if (selAirLineNeg == '') {
                            if (selectedAirlinesNegotiated[i] == editedNegFares.split('~')[0]) {
                                selAirLineNeg = editedNegFares.split('~')[0].split('^')[0] + '^' + selAirline;
                            }
                            else {
                                selAirLineNeg = selectedAirlinesNegotiated[i];
                            }
                        }
                        else {
                            if (selectedAirlinesNegotiated[i] == editedNegFares.split('~')[0]) {
                                selAirLineNeg = selAirLineNeg + '|' + editedNegFares.split('~')[0].split('^')[0] + '^' + selAirline;
                            }
                            else {
                                selAirLineNeg = selAirLineNeg + '|' + selectedAirlinesNegotiated[i];
                            }
                        }

                    }
                    document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value = selAirLineNeg;
                    editedNegFares = "";
                    document.getElementById("addAirLinesWithNegotiatedFares").style.display = "block";
                    document.getElementById("updateAirLinesWithNegotiatedFares").style.display = "none";
                    valid = true;


                }



            }

            return valid;
        }



        function removeAirlineNegotiatedDiv(index) {
            var removeAirlineNegotiated = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            var hdnDelAirlinesNego = document.getElementById('<%=hdnDelAirlinesNego.ClientID %>').value;
            if (hdnDelAirlinesNego.value == "") {
                document.getElementById('<%=hdnDelAirlinesNego.ClientID %>').value = removeAirlineNegotiated;
            }
            else {
                document.getElementById('<%=hdnDelAirlinesNego.ClientID %>').value = hdnDelAirlinesNego + "|" + removeAirlineNegotiated;
            }
            document.getElementById('div-row' + index).remove();
            var selectedAirlinesNegotiated = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value.split('|');
            var selAirline = '';
            for (var i = 0; i < selectedAirlinesNegotiated.length; i++) {
                if (selectedAirlinesNegotiated[i] != removeAirlineNegotiated) {

                    if (selAirline == '') {
                        selAirline = selectedAirlinesNegotiated[i];
                    }
                    else {
                        selAirline = selAirline + '|' + selectedAirlinesNegotiated[i];
                    }
                }
            }
            document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value = selAirline;
        }
        //------------------ End:AIRLINES WITH NEGOTIATED FARES------------------------

        //------------------ Start:PREFERRED  AIRLINES----------------------
        function addPreferedAirLines() {

            var valid = false;
            var selAirline = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlPrefAirline.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Airline from the list .";
            }
            else {
                var recordId = "-1";

                document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
                selAirline = recordId + "^" + Trim(document.getElementById('<%=ddlPrefAirline.ClientID %>').value);
                var hdnAirlinePrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value;
                var hdnAirlineAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value;

                //Selected Prefered Airlines Without Id
                var selectedAirlinesPreferedWithoutId = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                var selAirlineWithoutId = '';
                for (var i = 0; i < selectedAirlinesPreferedWithoutId.length; i++) {


                    if (selAirline == '') {
                        selAirlineWithoutId = selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }
                    else {
                        selAirlineWithoutId = selAirlineWithoutId + '|' + selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }

                }

                //Selected Airlines to Avoid Without Id
                var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                var selAirlineAvoidWithoutId = '';
                for (var i = 0; i < selectedAirlinesAvoid.length; i++) {
                    if (selAirlineAvoidWithoutId == '') {
                        selAirlineAvoidWithoutId = selectedAirlinesAvoid[i].split('^')[1];
                    }
                    else {
                        selAirlineAvoidWithoutId = selAirlineAvoidWithoutId + '|' + selectedAirlinesAvoid[i].split('^')[1];
                    }
                }
                if (hdnAirlinePrefered.length == 0 && document.getElementById('airlinePreferedCollection').children.length == 0) {

                    if (document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.length > 0) {
                        if (selAirlineAvoidWithoutId.indexOf(selAirline.split('^')[1]) != -1) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "You have already selected this airline in Airlines to avoid !";
                        }
                        else {
                            document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = selAirline;
                            appendAirlinePrefered(selAirline);
                            valid = true;
                        }
                    }

                    else {
                        document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = selAirline;
                        appendAirlinePrefered(selAirline);
                        valid = true;
                    }
                }
                else {

                    if (selAirlineWithoutId.indexOf(selAirline.split('^')[1]) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this airline !";
                    }
                    else if (document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.length > 0) {

                        if (selAirlineAvoidWithoutId.indexOf(selAirline.split('^')[1]) != -1) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "You have already selected this airline in Airlines to avoid !";
                        }
                        else {
                            appendAirlinePrefered(selAirline);
                            document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = hdnAirlinePrefered + "|" + selAirline;
                            valid = true;
                        }
                    }
                    else {
                        appendAirlinePrefered(selAirline);
                        document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = hdnAirlinePrefered + "|" + selAirline;
                        valid = true;
                    }

                }
                document.getElementById('<%=ddlPrefAirline.ClientID %>').value = "0";
                $('select').select2();
            }
            return valid;
        }


        function appendAirlinePrefered(selAirline) {

            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divAirlinePrefered = '<div id = "' + dynamicDivId + '" class="updated-row-content">';
            var dynamicSpanId = 'span-row' + children;
            divAirlinePrefered += '<span style="display:none" id ="rowId' + dynamicSpanId + '">' + selAirline.split('^')[0] + '</span>';
            divAirlinePrefered += '<span class="text" id ="' + dynamicSpanId + '">' + selAirline.split('^')[1] + '</span>';
            divAirlinePrefered += '<a href="#" class="add-more remove-row"  onclick="javascript:removeAirlinePreferedDiv(' + children + ')">';
            divAirlinePrefered += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divAirlinePrefered += '</a>';
            divAirlinePrefered += '<a href="#" class="add-more remove-row"  onclick="javascript:editAirlinePreferedDiv(' + children + ')">';
            divAirlinePrefered += '<img src="Images/grid/wg_edit.gif">';
            divAirlinePrefered += '</a>';
            divAirlinePrefered += '</div>';
            document.getElementById('airlinePreferedCollection').style.display = 'block';
            document.getElementById('airlinePreferedCollection').innerHTML += divAirlinePrefered;

        }


        var editedAirlinePrefered = '';
        function editAirlinePreferedDiv(index) {

            document.getElementById('addPreferedAirLines').style.display = "none";
            document.getElementById('updatePreferedAirLines').style.display = "block";
            var editAirlinePrefer = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            editedAirlinePrefered = editAirlinePrefer + "~" + index;
            var selAirline = editAirlinePrefer.split('^')[1];
             $('#ctl00_cphTransaction_ddlPrefAirline').select2("val", selAirline);
           // document.getElementById('<%=ddlPrefAirline.ClientID %>').value = selAirline;
        }

        function updatePreferedAirLines() {
            var valid = false;
            var selAirline = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlPrefAirline.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Airline from the list .";
            }
            else {

                selAirline = Trim(document.getElementById('<%=ddlPrefAirline.ClientID %>').value);
                var hdnAirlinePrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value;
                var hdnAirlineAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value;


                var selectedAirlinesPreferedWithoutId = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                var selAirlineWithoutId = '';
                for (var i = 0; i < selectedAirlinesPreferedWithoutId.length; i++) {


                    if (selAirline == '') {
                        selAirlineWithoutId = selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }
                    else {
                        selAirlineWithoutId = selAirlineWithoutId + '|' + selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }

                }


                var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                var selAirlineAvoidWithoutId = '';
                for (var i = 0; i < selectedAirlinesAvoid.length; i++) {
                    if (selAirlineAvoidWithoutId == '') {
                        selAirlineAvoidWithoutId = selectedAirlinesAvoid[i].split('^')[1];
                    }
                    else {
                        selAirlineAvoidWithoutId = selAirlineAvoidWithoutId + '|' + selectedAirlinesAvoid[i].split('^')[1];
                    }
                }




                if (selAirlineWithoutId.indexOf(selAirline) != -1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already selected this airline !";
                }
                else if (document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.length > 0) {

                    if (selAirlineAvoidWithoutId.indexOf(selAirline) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this airline in Airlines to avoid !";
                    }
                    else {

                        document.getElementById("span-row" + editedAirlinePrefered.split('~')[1]).innerHTML = selAirline;
                        var selectedAirlinesPrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                        var selAirlinePrefered = '';
                        for (var i = 0; i < selectedAirlinesPrefered.length; i++) {


                            if (selAirlinePrefered == '') {
                                if (selectedAirlinesPrefered[i] == editedAirlinePrefered.split('~')[0]) {
                                    selAirlinePrefered = editedAirlinePrefered.split('~')[0].split('^')[0] + '^' + selAirline;
                                }
                                else {
                                    selAirlinePrefered = selectedAirlinesPrefered[i];
                                }
                            }
                            else {
                                if (selectedAirlinesPrefered[i] == editedAirlinePrefered.split('~')[0]) {
                                    selAirlinePrefered = selAirlinePrefered + '|' + editedAirlinePrefered.split('~')[0].split('^')[0] + '^' + selAirline;
                                }
                                else {
                                    selAirlinePrefered = selAirlinePrefered + '|' + selectedAirlinesPrefered[i];
                                }
                            }

                        }
                        //// alert(document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value);
                        document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = selAirlinePrefered;
                        valid = true;
                        document.getElementById('addPreferedAirLines').style.display = "block";
                        document.getElementById('updatePreferedAirLines').style.display = "none";
                        editedAirlinePrefered = '';
                        document.getElementById('<%=ddlPrefAirline.ClientID %>').value = "0";

                    }

                }

                else {

                    document.getElementById("span-row" + editedAirlinePrefered.split('~')[1]).innerHTML = selAirline;
                    var selectedAirlinesPrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                    var selAirlinePrefered = '';
                    for (var i = 0; i < selectedAirlinesPrefered.length; i++) {


                        if (selAirlinePrefered == '') {
                            if (selectedAirlinesPrefered[i] == editedAirlinePrefered.split('~')[0]) {
                                selAirlinePrefered = editedAirlinePrefered.split('~')[0].split('^')[0] + '^' + selAirline;
                            }
                            else {
                                selAirlinePrefered = selectedAirlinesPrefered[i];
                            }
                        }
                        else {
                            if (selectedAirlinesPrefered[i] == editedAirlinePrefered.split('~')[0]) {
                                selAirlinePrefered = selAirlinePrefered + '|' + editedAirlinePrefered.split('~')[0].split('^')[0] + '^' + selAirline;
                            }
                            else {
                                selAirlinePrefered = selAirlinePrefered + '|' + selectedAirlinesPrefered[i];
                            }
                        }

                    }
                    //// alert(document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value);
                    document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = selAirlinePrefered;
                    valid = true;
                    document.getElementById('addPreferedAirLines').style.display = "block";
                    document.getElementById('updatePreferedAirLines').style.display = "none";
                    editedAirlinePrefered = '';
                    document.getElementById('<%=ddlPrefAirline.ClientID %>').value = "0";

                }

            }



        }




        function removeAirlinePreferedDiv(index) {
            var removeAirlinePrefered = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            var hdnDelPreferAirline = document.getElementById('<%=hdnDelPreferAirline.ClientID %>').value;
            if (hdnDelPreferAirline.value == "") {
                document.getElementById('<%=hdnDelPreferAirline.ClientID %>').value = removeAirlinePrefered;
            }
            else {
                document.getElementById('<%=hdnDelPreferAirline.ClientID %>').value = hdnDelPreferAirline + "|" + removeAirlinePrefered;
            }

            document.getElementById('div-row' + index).remove();
            var selectedAirlinesPrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
            var selAirline = '';
            for (var i = 0; i < selectedAirlinesPrefered.length; i++) {
                if (selectedAirlinesPrefered[i] != removeAirlinePrefered) {

                    if (selAirline == '') {
                        selAirline = selectedAirlinesPrefered[i];
                    }
                    else {
                        selAirline = selAirline + '|' + selectedAirlinesPrefered[i];
                    }
                }
            }
            document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value = selAirline;
        }
        //------------------ End:PREFERED AIRLINES------------------------


        //------------------ Start:AVOID AIRLINES----------------------
        function addAvoidAirLines() {

            var valid = false;
            var selAirline = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlAvoidAirline.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Airline from the list .";
            }
            else {

                document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
                var recordId = "-1";
                selAirline = recordId + "^" + Trim(document.getElementById('<%=ddlAvoidAirline.ClientID %>').value);


                var hdnAirlineAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value;
                var hdnAirlinePrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value;

                var selectedAirlinesPreferedWithoutId = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                var selAirlineWithoutId = '';
                for (var i = 0; i < selectedAirlinesPreferedWithoutId.length; i++) {
                    if (selAirline == '') {
                        selAirlineWithoutId = selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }
                    else {
                        selAirlineWithoutId = selAirlineWithoutId + '|' + selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }

                }


                var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                var selAirlineAvoidWithoutId = '';
                for (var i = 0; i < selectedAirlinesAvoid.length; i++) {
                    if (selAirlineAvoidWithoutId == '') {
                        selAirlineAvoidWithoutId = selectedAirlinesAvoid[i].split('^')[1];
                    }
                    else {
                        selAirlineAvoidWithoutId = selAirlineAvoidWithoutId + '|' + selectedAirlinesAvoid[i].split('^')[1];
                    }
                }
                if (hdnAirlineAvoid.length == 0 && document.getElementById('airlineAvoidCollection').children.length == 0) {
                    if (document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.length > 0) {
                        if (selAirlineWithoutId.indexOf(selAirline.split('^')[1]) != -1) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "You have already selected this airline in Preferred Airline !";
                        }
                        else {
                            document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = selAirline;
                            appendAirlineAvoid(selAirline);
                            valid = true;
                        }
                    }
                    else {
                        document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = selAirline;
                        appendAirlineAvoid(selAirline);
                        valid = true;
                    }
                }
                else {

                    if (selAirlineAvoidWithoutId.indexOf(selAirline.split('^')[1]) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this airline !";
                    }
                    else if (document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.length > 0) {
                        if (selAirlineWithoutId.indexOf(selAirline.split('^')[1]) != -1) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "You have already selected this airline in Preferred Airline !";
                        }
                        else {
                            appendAirlineAvoid(selAirline);
                            document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = hdnAirlineAvoid + "|" + selAirline;
                            valid = true;
                        }

                    }


                    else {
                        appendAirlineAvoid(selAirline);
                        document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = hdnAirlineAvoid + "|" + selAirline;
                        valid = true;
                    }

                }
                document.getElementById('<%=ddlAvoidAirline.ClientID %>').value = "0";
                $('select').select2();
            }
            return valid;
        }


        function appendAirlineAvoid(selAirline) {

            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divAirlineAvoid = '<div id = "' + dynamicDivId + '" class="updated-row-content">';

            var dynamicSpanId = 'span-row' + children;
            divAirlineAvoid += '<span style="display:none" id ="rowId' + dynamicSpanId + '">' + selAirline.split('^')[0] + '</span>';
            divAirlineAvoid += '<span class="text" id ="' + dynamicSpanId + '">' + selAirline.split('^')[1] + '</span>';
            divAirlineAvoid += '<a href="#" class="add-more remove-row"  onclick="javascript:removeAirlineAvoidDiv(' + children + ')">';
            divAirlineAvoid += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divAirlineAvoid += '</a>';
            divAirlineAvoid += '<a href="#" class="add-more remove-row"  onclick="javascript:editAirlineAvoidDiv(' + children + ')">';
            divAirlineAvoid += '<img src="Images/grid/wg_edit.gif">';
            divAirlineAvoid += '</a>';
            divAirlineAvoid += '</div>';
            document.getElementById('airlineAvoidCollection').style.display = 'block';
            document.getElementById('airlineAvoidCollection').innerHTML += divAirlineAvoid;

        }
        var editedAirlineAvoid = '';
        function editAirlineAvoidDiv(index) {
            document.getElementById('addAvoidAirLines').style.display = "none";
            document.getElementById('updateAvoidAirLines').style.display = "block";
            var editAirlineAvoid = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            editedAirlineAvoid = editAirlineAvoid + "~" + index;
            var selAirline = editAirlineAvoid.split('^')[1];
             $('#ctl00_cphTransaction_ddlAvoidAirline').select2("val", selAirline);
           // document.getElementById('<%=ddlAvoidAirline.ClientID %>').value = selAirline;
        }
        function updateAvoidAirLines() {
            var valid = false;
            var selAirline = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }
            else if (Trim(document.getElementById('<%=ddlAvoidAirline.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Airline from the list .";
            }
            else {

                selAirline = Trim(document.getElementById('<%=ddlAvoidAirline.ClientID %>').value);
                var hdnAirlinePrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value;
                var hdnAirlineAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value;

                var selectedAirlinesPreferedWithoutId = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                var selAirlineWithoutId = '';
                for (var i = 0; i < selectedAirlinesPreferedWithoutId.length; i++) {


                    if (selAirline == '') {
                        selAirlineWithoutId = selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }
                    else {
                        selAirlineWithoutId = selAirlineWithoutId + '|' + selectedAirlinesPreferedWithoutId[i].split('^')[1];
                    }

                }
                var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                var selAirlineAvoidWithoutId = '';
                for (var i = 0; i < selectedAirlinesAvoid.length; i++) {
                    if (selAirlineAvoidWithoutId == '') {
                        selAirlineAvoidWithoutId = selectedAirlinesAvoid[i].split('^')[1];
                    }
                    else {
                        selAirlineAvoidWithoutId = selAirlineAvoidWithoutId + '|' + selectedAirlinesAvoid[i].split('^')[1];
                    }
                }
                if (selAirlineAvoidWithoutId.indexOf(selAirline) != -1) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "You have already selected this airline in Airlines to avoid !";
                }
                else if (document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.length > 0) {
                    if (selAirlineWithoutId.indexOf(selAirline) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already selected this airline in Prefered Airlines !";
                    }
                    else {
                        document.getElementById("span-row" + editedAirlineAvoid.split('~')[1]).innerHTML = selAirline;
                        var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                        var selAirlineAvoid = '';
                        for (var i = 0; i < selectedAirlinesAvoid.length; i++) {


                            if (selAirlineAvoid == '') {
                                if (selectedAirlinesAvoid[i] == editedAirlineAvoid.split('~')[0]) {
                                    selAirlineAvoid = editedAirlineAvoid.split('~')[0].split('^')[0] + '^' + selAirline;
                                }
                                else {
                                    selAirlineAvoid = selectedAirlinesAvoid[i];
                                }
                            }
                            else {
                                if (selectedAirlinesAvoid[i] == editedAirlineAvoid.split('~')[0]) {
                                    selAirlineAvoid = selAirlineAvoid + +'|' + editedAirlineAvoid.split('~')[0].split('^')[0] + '^' + selAirline;
                                }
                                else {
                                    selAirlineAvoid = selAirlineAvoid + '|' + selectedAirlinesAvoid[i];
                                }
                            }

                        }
                        document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = selAirlineAvoid;
                        valid = true;
                        document.getElementById('addAvoidAirLines').style.display = "block";
                        document.getElementById('updateAvoidAirLines').style.display = "none";
                        editedAirlineAvoid = '';
                        document.getElementById('<%=ddlAvoidAirline.ClientID %>').value = "0";

                    }
                }
                else {
                    document.getElementById("span-row" + editedAirlineAvoid.split('~')[1]).innerHTML = selAirline;
                    var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                    var selAirlineAvoid = '';
                    for (var i = 0; i < selectedAirlinesAvoid.length; i++) {


                        if (selAirlineAvoid == '') {
                            if (selectedAirlinesAvoid[i] == editedAirlineAvoid.split('~')[0]) {
                                selAirlineAvoid = editedAirlineAvoid.split('~')[0].split('^')[0] + '^' + selAirline;
                            }
                            else {
                                selAirlineAvoid = selectedAirlinesAvoid[i];
                            }
                        }
                        else {
                            if (selectedAirlinesAvoid[i] == editedAirlineAvoid.split('~')[0]) {
                                selAirlineAvoid = selAirlineAvoid + +'|' + editedAirlineAvoid.split('~')[0].split('^')[0] + '^' + selAirline;
                            }
                            else {
                                selAirlineAvoid = selAirlineAvoid + '|' + selectedAirlinesAvoid[i];
                            }
                        }

                    }
                    document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = selAirlineAvoid;
                    valid = true;
                    document.getElementById('addAvoidAirLines').style.display = "block";
                    document.getElementById('updateAvoidAirLines').style.display = "none";
                    editedAirlineAvoid = '';
                    document.getElementById('<%=ddlAvoidAirline.ClientID %>').value = "0";

                }

            }



        }


        function removeAirlineAvoidDiv(index) {

            var removeAirlineAvoid = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            var hdnDelAvoidAirlines = document.getElementById('<%=hdnDelAvoidAirlines.ClientID %>').value;
            if (hdnDelAvoidAirlines.value == "") {
                document.getElementById('<%=hdnDelAvoidAirlines.ClientID %>').value = removeAirlineAvoid;
            }
            else {
                document.getElementById('<%=hdnDelAvoidAirlines.ClientID %>').value = hdnDelAvoidAirlines + "|" + removeAirlineAvoid;
            }
            document.getElementById('div-row' + index).remove();
            var selectedAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
            var selAirline = '';
            for (var i = 0; i < selectedAirlinesAvoid.length; i++) {
                if (selectedAirlinesAvoid[i] != removeAirlineAvoid) {

                    if (selAirline == '') {
                        selAirline = selectedAirlinesAvoid[i];
                    }
                    else {
                        selAirline = selAirline + '|' + selectedAirlinesAvoid[i];
                    }
                }
            }
            document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value = selAirline;
        }
        //------------------ End:AVOID AIRLINES------------------------


        //------------------Start :AIR FARE SHOPPING------------------------
        function airFareShopping() {
            if (document.getElementById('allowAnyFare').checked) {
                if (document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.length == 0) {

                    document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = "ANY";
                }
                else {
                    if (document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.indexOf('^') > -1) {
                        document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.split('^')[0] + '^' + "ANY";
                    }
                    else {
                        document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = "ANY";
                    }
                }
            }
            if (document.getElementById('allowLowestFare').checked) {
                if (document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.length == 0) {
                    document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = "LOWEST";
                }
                else {

                    if (document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.indexOf('^') > -1) {
                        document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.split('^')[0] + '^' + "LOWEST";
                    }
                    else {
                        document.getElementById('<%=hdnAirFareShopping.ClientID %>').value = "LOWEST";
                    }
                }
            }

        }
        //-------------------End :AIR FARE SHOPPING-------------------------


        //------------------Start :SECURITY RULES------------------------

        function securityRules() {

            //alert(document.getElementById('<%=hdnSecurityRules.ClientID %>').value);

            if (document.getElementById('securityrules').checked) {
                if (document.getElementById('<%=hdnSecurityRules.ClientID %>').value.length == 0) {
                    document.getElementById('<%=hdnSecurityRules.ClientID %>').value = "SAMEFLIGHT|YES";
                }
                else {
                    if (document.getElementById('<%=hdnSecurityRules.ClientID %>').value.indexOf('^') > -1) {
                        document.getElementById('<%=hdnSecurityRules.ClientID %>').value = document.getElementById('<%=hdnSecurityRules.ClientID %>').value.split('^')[0] + '^' + "SAMEFLIGHT|YES";
                    }
                    else {
                        document.getElementById('<%=hdnSecurityRules.ClientID %>').value = "SAMEFLIGHT|YES";
                    }
                }
            }
            else {
                if (document.getElementById('<%=hdnSecurityRules.ClientID %>').value.length == 0) {
                    document.getElementById('<%=hdnSecurityRules.ClientID %>').value = "SAMEFLIGHT|NO";
                }
                else {
                    if (document.getElementById('<%=hdnSecurityRules.ClientID %>').value.indexOf('^') > -1) {

                        document.getElementById('<%=hdnSecurityRules.ClientID %>').value = document.getElementById('<%=hdnSecurityRules.ClientID %>').value.split('^')[0] + '^' + "SAMEFLIGHT|NO";
                    }
                    else {
                        document.getElementById('<%=hdnSecurityRules.ClientID %>').value = "SAMEFLIGHT|NO";
                    }
                }

            }

        }
        //-------------------End :SECURITY RULES-------------------------


        //------------------Start :CABIN TYPE RULES------------------------
        function cabinexceptionTypes() {

            if (document.getElementById('exception1').checked) {
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = false;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                document.getElementById('txtDuration').value = "";
                document.getElementById('txtDuration').disabled = true;
                document.getElementById('exception2').checked = false;
                document.getElementById('exception3').checked = false;
            }
            if (document.getElementById('exception2').checked) {
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = false;
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                document.getElementById('txtDuration').value = "";
                document.getElementById('txtDuration').disabled = false;
                document.getElementById('exception1').checked = false;
                document.getElementById('exception3').checked = false;
            }
            if (document.getElementById('exception3').checked) {
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                document.getElementById('txtDuration').value = "";
                document.getElementById('txtDuration').disabled = true;
                document.getElementById('exception1').checked = false;
                document.getElementById('exception2').checked = false;
            }
        }


        function addCabinTypeException() {
            var valid = false;
            var selCabinTypeException = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }

            else if (Trim(document.getElementById('<%=ddlCabinType.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Default Cabin Type from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Allow Cabin Type from the list .";
            }
            //            else if (document.getElementById('exception1').checked == false && document.getElementById('exception2').checked == false && document.getElementById('exception3').checked == false) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errMess').innerHTML = "Please select any exception rule .";
            //            }

            else {


                if (document.getElementById('exception1').checked == true && document.getElementById('<%=ddlExceptionCity.ClientID %>').value == "0") {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select any destination.";
                }
                else if (document.getElementById('exception2').checked == true && document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value == "0") {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select any reason.";
                }
                else if (document.getElementById('exception2').checked == true && document.getElementById('txtDuration').value == "") {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please enter hours.";
                }
                else {

                    document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);

                    var ddlCabinType = document.getElementById("<%=ddlCabinType.ClientID%>");
                    var ddlCabinTypeText = ddlCabinType.options[ddlCabinType.selectedIndex].text;

                    var ddlExceptionCabinType = document.getElementById("<%=ddlExceptionCabinType.ClientID%>");
                    var ddlExceptionCabinTypeText = ddlExceptionCabinType.options[ddlExceptionCabinType.selectedIndex].text;

                    var recordId = "-1";
                    selCabinTypeException = recordId + "^" + Trim(ddlExceptionCabinTypeText);


                    if (document.getElementById('exception1').checked == true) {

                        var ddlExceptionCity = document.getElementById("<%=ddlExceptionCity.ClientID%>").value;
                        //var ddlExceptionCityText = ddlExceptionCity.options[ddlExceptionCity.selectedIndex].text;
                        selCabinTypeException = selCabinTypeException + "- DESTINATION -" + ddlExceptionCity;

                    }
                    if (document.getElementById('exception2').checked == true) {

                        var ddlCabinTypeRule2 = document.getElementById("<%=ddlCabinTypeRule2.ClientID%>");
                        var ddlCabinTypeRule2Text = ddlCabinTypeRule2.options[ddlCabinTypeRule2.selectedIndex].text;

                        if (ddlCabinTypeRule2Text == 'Flight Duration exceeds') {
                            ddlCabinTypeRule2Text = 'FLIGHTDURATION';
                        }
                        if (ddlCabinTypeRule2Text == 'Journey Time exceeds') {
                            ddlCabinTypeRule2Text = 'JOURNYTIME';
                        }

                        selCabinTypeException = selCabinTypeException + "-" + ddlCabinTypeRule2Text + "-" + Trim(document.getElementById('txtDuration').value);
                    }
                    if (document.getElementById('exception3').checked == true) {
                        selCabinTypeException = selCabinTypeException + "-" + "NOSEATS";
                    }
                    var hdnCabinTypeException = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value;


                    if (hdnCabinTypeException.length == 0 && document.getElementById('cabinTypeExceptionCollection').children.length == 0) {
                        document.getElementById('<%=hdnCabinTypeException.ClientID %>').value = selCabinTypeException;
                        appendCabinTypeException(selCabinTypeException);
                        valid = true;
                    }
                    else {

                        var selectedCabinTypeExceptionsWithoutId = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value.split('|');
                        var selCabinTypeExceptionWithoutId = '';
                        for (var i = 0; i < selectedCabinTypeExceptionsWithoutId.length; i++) {


                            if (selCabinTypeExceptionWithoutId == '') {
                                selCabinTypeExceptionWithoutId = selectedCabinTypeExceptionsWithoutId[i].split('^')[1];
                            }
                            else {
                                selCabinTypeExceptionWithoutId = selCabinTypeExceptionWithoutId + '|' + selectedCabinTypeExceptionsWithoutId[i].split('^')[1];
                            }
                        }



                        if (selCabinTypeExceptionWithoutId.indexOf(selCabinTypeException.split('^')[1]) != -1) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "You have already added this exception !";
                        }
                        else {
                            appendCabinTypeException(selCabinTypeException);
                            document.getElementById('<%=hdnCabinTypeException.ClientID %>').value = hdnCabinTypeException + "|" + selCabinTypeException;
                            document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "0";

                            if (document.getElementById('exception1').checked) {
                                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = false;
                                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                                document.getElementById('txtDuration').value = "";
                                document.getElementById('txtDuration').disabled = true;
                                document.getElementById('exception2').checked = false;
                                document.getElementById('exception3').checked = false;

                            }
                            if (document.getElementById('exception2').checked) {
                                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = false;
                                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                                document.getElementById('txtDuration').value = "";
                                document.getElementById('txtDuration').disabled = false;
                                document.getElementById('exception1').checked = false;
                                document.getElementById('exception3').checked = false;

                            }
                            if (document.getElementById('exception3').checked) {
                                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                                document.getElementById('txtDuration').value = "";
                                document.getElementById('txtDuration').disabled = true;
                                document.getElementById('exception1').checked = false;
                                document.getElementById('exception2').checked = false;
                            }
                            valid = true;
                        }

                    }

                    document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "0";
                    $('select').select2();
                }
            }

            return valid;
        }


        function appendCabinTypeException(selCabinTypeException) {

            var children = parseInt(document.getElementById('childFromDates').value);
            var dynamicDivId = 'div-row' + children;
            var divCabinTypeException = '<div id = "' + dynamicDivId + '" class="updated-row-content">';
            var dynamicSpanId = 'span-row' + children;
            divCabinTypeException += '<span style="display:none" id ="rowId' + dynamicSpanId + '">' + selCabinTypeException.split('^')[0] + '</span>';
            divCabinTypeException += '<span class="text" id ="' + dynamicSpanId + '">' + selCabinTypeException.split('^')[1] + '</span>';
            divCabinTypeException += '<a href="#" class="add-more remove-row"  onclick="javascript:removeCabinTypeExceptionDiv(' + children + ')">';
            divCabinTypeException += '<span class=" glyphicon glyphicon-remove-sign"></span>';
            divCabinTypeException += '</a>';
            divCabinTypeException += '<a href="#" class="add-more remove-row"  onclick="javascript:editCabinTypeExceptionDiv(' + children + ')">';
            divCabinTypeException += '<img src="Images/grid/wg_edit.gif">';
            divCabinTypeException += '</a>';
            divCabinTypeException += '</div>';
            document.getElementById('cabinTypeExceptionCollection').style.display = 'block';
            document.getElementById('cabinTypeExceptionCollection').innerHTML += divCabinTypeException;

        }
        var editedCabinTypeException = '';
        function editCabinTypeExceptionDiv(index) {
            debugger;
            document.getElementById('addCabinTypeException').style.display = "none";
            document.getElementById('updateCabinTypeException').style.display = "block";

            var editCabinTypeException = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;

            editedCabinTypeException = editCabinTypeException + "~" + index;
            var selExceptionCabinType = editCabinTypeException.split('^')[1].split('-')[0];
            // alert(selExceptionCabinType);
            if (selExceptionCabinType == 'Business') {
                 $('#ctl00_cphTransaction_ddlExceptionCabinType').select2("val", "1");
                document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "1";
            }
            else if (selExceptionCabinType == 'Economy') {
                 $('#ctl00_cphTransaction_ddlExceptionCabinType').select2("val", "2");
               // document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "2";
            }
            else if (selExceptionCabinType == 'First') {
                 $('#ctl00_cphTransaction_ddlExceptionCabinType').select2("val", "3");
                //document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "3";
            }
            else if (selExceptionCabinType == 'PremiumEconomy') {
                 $('#ctl00_cphTransaction_ddlExceptionCabinType').select2("val", "4");
               // document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "4";
            }
            else {
                 $('#ctl00_cphTransaction_ddlExceptionCabinType').select2("val", "0");
               // document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "0";
            }
            //// alert(document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value);

            //  alert(editCabinTypeException.split('^')[1].split('-')[1]);
            // alert(editCabinTypeException);
            // alert(editCabinTypeException.split('^')[1].split('-')[2]);

            if (editCabinTypeException.split('^')[1].split('-')[1].indexOf('DESTINATION') > -1) {
                // alert('Inside the first if bloak');
                document.getElementById('exception1').checked = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = false;
                 $('#ctl00_cphTransaction_ddlExceptionCity').select2("val", editCabinTypeException.split('^')[1].split('-')[2]);
               // document.getElementById('<%=ddlExceptionCity.ClientID %>').value = editCabinTypeException.split('^')[1].split('-')[2];
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                document.getElementById('txtDuration').value = "";
                document.getElementById('txtDuration').disabled = true;
                document.getElementById('exception2').checked = false;
                document.getElementById('exception3').checked = false;

            }
            else if (editCabinTypeException.split('^')[1].split('-')[1].indexOf('FLIGHTDURATION') > -1 || editCabinTypeException.split('^')[1].split('-')[1].indexOf('JOURNYTIME') > -1) {
                document.getElementById('exception2').checked = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = false;

                if (editCabinTypeException.split('^')[1].split('-')[1] == "FLIGHTDURATION") {
                     $('#ctl00_cphTransaction_ddlCabinTypeRule2').select2("val","1");
                   // document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "1";
                }
                else if (editCabinTypeException.split('^')[1].split('-')[1] == "JOURNYTIME") {
                     $('#ctl00_cphTransaction_ddlCabinTypeRule2').select2("val", "2");
                    //document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "2";
                }
                else {
                     $('#ctl00_cphTransaction_ddlCabinTypeRule2').select2("val", "0");
                   // document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                }


                document.getElementById('txtDuration').value = editCabinTypeException.split('^')[1].split('-')[2];
                document.getElementById('txtDuration').disabled = false;
                document.getElementById('exception1').checked = false;
                document.getElementById('exception3').checked = false;

            }
            else if (editCabinTypeException.split('^')[1].split('-')[1].indexOf('NOSEATS') > -1) {
                document.getElementById('exception3').checked = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                document.getElementById('txtDuration').value = "";
                document.getElementById('txtDuration').disabled = true;
                document.getElementById('exception1').checked = false;
                document.getElementById('exception2').checked = false;

            }
        }


        function updateCabinTypeException() {

            var valid = false;
            var selCabinTypeException = '';
            document.getElementById('errMess').style.display = "none";

            if (Trim(document.getElementById('<%=ddlAgents.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Client from the list .";
            }
            else if (Trim(document.getElementById('<%=txtPolicyName.ClientID %>').value) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add policy name.";
            }

            else if (Trim(document.getElementById('<%=ddlCabinType.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Default Cabin Type from the list .";
            }
            else if (Trim(document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value) == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Allow Cabin Type from the list .";
            }
            else {


                if (document.getElementById('exception1').checked == true && document.getElementById('<%=ddlExceptionCity.ClientID %>').value == "0") {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select any destination.";
                }
                else if (document.getElementById('exception2').checked == true && document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value == "0") {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select any reason.";
                }
                else if (document.getElementById('exception2').checked == true && document.getElementById('txtDuration').value == "") {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please enter hours.";
                }
                else {
                    var ddlCabinType = document.getElementById("<%=ddlCabinType.ClientID%>");
                    var ddlCabinTypeText = ddlCabinType.options[ddlCabinType.selectedIndex].text;

                    var ddlExceptionCabinType = document.getElementById("<%=ddlExceptionCabinType.ClientID%>");
                    var ddlExceptionCabinTypeText = ddlExceptionCabinType.options[ddlExceptionCabinType.selectedIndex].text;

                    selCabinTypeException = Trim(ddlExceptionCabinTypeText);


                    if (document.getElementById('exception1').checked == true) {
                        var ddlExceptionCity = document.getElementById("<%=ddlExceptionCity.ClientID%>").value;
                        selCabinTypeException = selCabinTypeException + "- DESTINATION -" + ddlExceptionCity;
                    }
                    if (document.getElementById('exception2').checked == true) {

                        var ddlCabinTypeRule2 = document.getElementById("<%=ddlCabinTypeRule2.ClientID%>");
                        var ddlCabinTypeRule2Text = ddlCabinTypeRule2.options[ddlCabinTypeRule2.selectedIndex].text;

                        if (ddlCabinTypeRule2Text == 'Flight Duration exceeds') {
                            ddlCabinTypeRule2Text = 'FLIGHTDURATION';
                        }
                        if (ddlCabinTypeRule2Text == 'Journey Time exceeds') {
                            ddlCabinTypeRule2Text = 'JOURNYTIME';
                        }

                        selCabinTypeException = selCabinTypeException + "-" + ddlCabinTypeRule2Text + "-" + Trim(document.getElementById('txtDuration').value);
                    }
                    if (document.getElementById('exception3').checked == true) {
                        selCabinTypeException = selCabinTypeException + "-" + "NOSEATS";
                    }
                    var hdnCabinTypeException = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value;

                    var selectedCabinTypeExceptionsWithoutId = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value.split('|');
                    var selCabinTypeExceptionWithoutId = '';
                    for (var i = 0; i < selectedCabinTypeExceptionsWithoutId.length; i++) {


                        if (selCabinTypeExceptionWithoutId == '') {
                            selCabinTypeExceptionWithoutId = selectedCabinTypeExceptionsWithoutId[i].split('^')[1];
                        }
                        else {
                            selCabinTypeExceptionWithoutId = selCabinTypeExceptionWithoutId + '|' + selectedCabinTypeExceptionsWithoutId[i].split('^')[1];
                        }
                    }




                    if (selCabinTypeExceptionWithoutId.indexOf(selCabinTypeException) != -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "You have already added this exception !";
                    }
                    else {
                        document.getElementById("span-row" + editedCabinTypeException.split('~')[1]).innerHTML = selCabinTypeException;
                        var selectedCabinTypeExceptions = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value.split('|');
                        var CabinTypeException = '';
                        for (var i = 0; i < selectedCabinTypeExceptions.length; i++) {


                            if (CabinTypeException == '') {
                                if (selectedCabinTypeExceptions[i] == editedCabinTypeException.split('~')[0]) {
                                    CabinTypeException = editedCabinTypeException.split('~')[0].split('^')[0] + '^' + selCabinTypeException;
                                }
                                else {
                                    CabinTypeException = selectedCabinTypeExceptions[i];
                                }
                            }
                            else {
                                if (selectedCabinTypeExceptions[i] == editedCabinTypeException.split('~')[0]) {

                                    CabinTypeException = CabinTypeException + '|' + editedCabinTypeException.split('~')[0].split('^')[0] + '^' + selCabinTypeException;
                                }
                                else {
                                    CabinTypeException = CabinTypeException + '|' + selectedCabinTypeExceptions[i];
                                }
                            }

                        }
                        // alert(document.getElementById('<%=hdnCabinTypeException.ClientID %>').value);
                        document.getElementById('<%=hdnCabinTypeException.ClientID %>').value = CabinTypeException;
                        //alert(CabinTypeException);
                        document.getElementById('addCabinTypeException').style.display = "block";
                        document.getElementById('updateCabinTypeException').style.display = "none";
                        document.getElementById('<%=ddlExceptionCabinType.ClientID %>').value = "0";
                        editedCabinTypeException = '';

                        if (document.getElementById('exception1').checked) {
                            document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = false;
                            document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                            document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                            document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                            document.getElementById('txtDuration').value = "";
                            document.getElementById('txtDuration').disabled = true;
                            document.getElementById('exception2').checked = false;
                            document.getElementById('exception3').checked = false;

                        }
                        if (document.getElementById('exception2').checked) {
                            document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                            document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                            document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = false;
                            document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                            document.getElementById('txtDuration').value = "";
                            document.getElementById('txtDuration').disabled = false;
                            document.getElementById('exception1').checked = false;
                            document.getElementById('exception3').checked = false;

                        }
                        if (document.getElementById('exception3').checked) {
                            document.getElementById('<%=ddlExceptionCity.ClientID %>').disabled = true;
                            document.getElementById('<%=ddlExceptionCity.ClientID %>').value = "0";
                            document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').disabled = true;
                            document.getElementById('<%=ddlCabinTypeRule2.ClientID %>').value = "0";
                            document.getElementById('txtDuration').value = "";
                            document.getElementById('txtDuration').disabled = true;
                            document.getElementById('exception1').checked = false;
                            document.getElementById('exception2').checked = false;
                        }

                        valid = true;
                    }

                }

            } // End of else block
        }



        function removeCabinTypeExceptionDiv(index) {

            var removeCabinTypeException = document.getElementById('rowIdspan-row' + index).innerHTML + "^" + document.getElementById('span-row' + index).innerHTML;
            var hdnDelCabinTypeException = document.getElementById('<%=hdnDelCabinTypeException.ClientID %>').value;
            if (hdnDelCabinTypeException.value == "") {
                document.getElementById('<%=hdnDelCabinTypeException.ClientID %>').value = removeCabinTypeException;
            }
            else {
                document.getElementById('<%=hdnDelCabinTypeException.ClientID %>').value = hdnDelCabinTypeException + "|" + removeCabinTypeException;
            }

            document.getElementById('div-row' + index).remove();
            var selectedCabinTypeExceptions = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value.split('|');
            var selCabinTypeException = '';
            for (var i = 0; i < selectedCabinTypeExceptions.length; i++) {
                if (selectedCabinTypeExceptions[i] != removeCabinTypeException) {

                    if (selCabinTypeException == '') {
                        selCabinTypeException = selectedCabinTypeExceptions[i];
                    }
                    else {
                        selCabinTypeException = selCabinTypeException + '|' + selectedCabinTypeExceptions[i];
                    }
                }
            }
            document.getElementById('<%=hdnCabinTypeException.ClientID %>').value = selCabinTypeException;
        }
        //-------------------End :CABIN TYPE RULES-------------------------



        //--------------------Start:Append all policy details in edit mode

        function appendAllPolicyDetails() {

            //1.TRAVEL DATES TO AVOID
            document.getElementById('childFromDates').value = "1";

            var hdnFromDate = document.getElementById('<%=hdnFromDate.ClientID %>').value;
            if (hdnFromDate != null && hdnFromDate != "" && document.getElementById('fromDatesCollection') != null) {
                var selectedTravelDatesToAvoid = document.getElementById('<%=hdnFromDate.ClientID %>').value.split('|');
                var selTravelDate = '';
                if (selectedTravelDatesToAvoid.length > 0) {
                    for (var i = 0; i < selectedTravelDatesToAvoid.length; i++) {
                        selTravelDate = selectedTravelDatesToAvoid[i];
                        appendChildDates(selTravelDate);
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);
                    }

                }
            }

            //2.DESTINATIONS TO AVOID
            var hdnDestinations = document.getElementById('<%=hdnDestinations.ClientID %>').value;
            if (hdnDestinations != null && hdnDestinations != "" && document.getElementById('destinationsCollection') != null) {

                var selDestinationsToAvoid = document.getElementById('<%=hdnDestinations.ClientID %>').value.split('|');
                var selDestination = '';
                if (selDestinationsToAvoid.length > 0) {
                    for (var i = 0; i < selDestinationsToAvoid.length; i++) {
                        selDestination = selDestinationsToAvoid[i];
                        appendDestinations(selDestination);
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);

                    }
                }
            }

            //3.AIRLINES WITH NEGOTIATED FARES
            var hdnAirlineNegotiated = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value;
            if (hdnAirlineNegotiated != null && hdnAirlineNegotiated != "" && document.getElementById('airlineNegotiatedCollection') != null) {
                var selAirlinesNegotiated = document.getElementById('<%=hdnAirlineNegotiated.ClientID %>').value.split('|');
                var selAirline = '';
                if (selAirlinesNegotiated.length > 0) {
                    for (var i = 0; i < selAirlinesNegotiated.length; i++) {
                        selAirline = selAirlinesNegotiated[i];
                        appendAirlinenegotiated(selAirline);
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);

                    }
                }
            }

            //4.SECURITY RULES
            var hdnSecurityRules = document.getElementById('<%=hdnSecurityRules.ClientID %>').value;
            if (document.getElementById('<%=hdnSecurityRules.ClientID %>').value.length > 0) {

                var sameFlightValue = document.getElementById('<%=hdnSecurityRules.ClientID %>').value.split('|')[1];
                if (sameFlightValue == 'YES') {
                    document.getElementById('securityrules').checked = true;
                }
                else {
                    document.getElementById('securityrules').checked = false;
                }
            }

            //5.PREFERRED AIRLINES
            var hdnAirlinePrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value;
            if (hdnAirlinePrefered != null && hdnAirlinePrefered != "" && document.getElementById('airlinePreferedCollection') != null) {
                var selAirline = '';
                var selAirlinesPrefered = document.getElementById('<%=hdnAirlinePrefered.ClientID %>').value.split('|');
                if (selAirlinesPrefered.length > 0) {
                    for (var i = 0; i < selAirlinesPrefered.length; i++) {
                        selAirline = selAirlinesPrefered[i];
                        appendAirlinePrefered(selAirline);
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);

                    }
                }
            }

            //6.AIRLINES TO AVOID
            var hdnAirlineAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value;
            if (hdnAirlineAvoid != null && hdnAirlineAvoid != "" && document.getElementById('airlineAvoidCollection') != null) {

                var selAirlinesAvoid = document.getElementById('<%=hdnAirlineAvoid.ClientID %>').value.split('|');
                var selAirline = '';
                if (selAirlinesAvoid.length > 0) {
                    for (var i = 0; i < selAirlinesAvoid.length; i++) {
                        selAirline = selAirlinesAvoid[i];
                        appendAirlineAvoid(selAirline);
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);

                    }
                }
            }

            //7.AIR FARE SHOPPING
            var hdnAirFareShopping = document.getElementById('<%=hdnAirFareShopping.ClientID %>').value;
            if (document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.length > 0) {

                var fareshoppingValue = document.getElementById('<%=hdnAirFareShopping.ClientID %>').value.split('^')[1];
                if (fareshoppingValue == 'ANY') {
                    document.getElementById('allowAnyFare').checked = true;
                }
                else if (fareshoppingValue == 'LOWEST') {
                    document.getElementById('allowLowestFare').checked = true;
                }
                else {
                    document.getElementById('allowAnyFare').checked = false;
                    document.getElementById('allowLowestFare').checked = false;
                }
            }


            //8.CABIN TYPE EXCEPTIONS
            var hdnCabinTypeException = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value
            if (hdnCabinTypeException != null && hdnCabinTypeException != "" && document.getElementById('cabinTypeExceptionCollection') != null) {
                var selCabinTypeExceptions = document.getElementById('<%=hdnCabinTypeException.ClientID %>').value.split('|');
                var selCabinType = '';
                if (selCabinTypeExceptions.length > 0) {
                    for (var i = 0; i < selCabinTypeExceptions.length; i++) {
                        selCabinType = selCabinTypeExceptions[i];
                        appendCabinTypeException(selCabinType);
                        document.getElementById('childFromDates').value = parseInt(parseInt(document.getElementById('childFromDates').value) + 1);

                    }
                }
            }

            //9.DEFAULT CABIN
            var hdnDefaultCabin = document.getElementById('<%=hdnDefaultCabin.ClientID %>').value;
            if (hdnDefaultCabin != null && hdnDefaultCabin != "") {
                document.getElementById('<%=ddlCabinType.ClientID %>').disabled = false;
                LoadCabinTypes('<%=ddlCabinType.ClientID %>');
            }

            //Bind all the policy Expense Details here.

            bindCapPerExpenseHtml(); //ExpenseType     policyExpenseDetails
            bindCapPerAllowanceHtml(); //AllowanceType policyExpenseDetails
        }
        //---------------------End:Append all policy details in edit mode     
    </script>
    <!--Expense tab hidden fields The below hidden fields are used for all the the CRUD{Create,Read,Update,Delete} operations on the page -->
    <asp:HiddenField runat="server" ID="hdnCapExpense" />
    <asp:HiddenField runat="server" ID="hdnCapAllowance" />
    <asp:HiddenField runat="server" ID="hdnDelCapExpenseAllowance" />
    <asp:HiddenField runat="server" ID="hdnCapCityExpense" />
    <asp:HiddenField runat="server" ID="hdnCapCityAllowance" />
    <input type="hidden" id="hdnExpenseTabControlsCount" value="0" />
    <!--The below hidden fields are used for all the CRUD{Create,Read,Update,Delete} operations on the page -->
    <asp:HiddenField runat="server" ID="hdnDelDestinationsToAvoid" />
    <asp:HiddenField runat="server" ID="hdnDelAirlinesNego" />
    <asp:HiddenField runat="server" ID="hdnDelPreferAirline" />
    <asp:HiddenField runat="server" ID="hdnDelAvoidAirlines" />
    <asp:HiddenField runat="server" ID="hdnDelDefaultCabin" />
    <asp:HiddenField runat="server" ID="hdnDelCabinTypeException" />
    <asp:HiddenField runat="server" ID="hdnFromDate" />
    <asp:HiddenField runat="server" ID="hdnDestinations" />
    <asp:HiddenField runat="server" ID="hdnAirlineNegotiated" />
    <asp:HiddenField runat="server" ID="hdnAirlinePrefered" />
    <asp:HiddenField runat="server" ID="hdnAirlineAvoid" />
    <asp:HiddenField runat="server" ID="hdnAirFareShopping" />
    <asp:HiddenField runat="server" ID="hdnSecurityRules" />
    <asp:HiddenField runat="server" ID="hdnDefaultCabin" />
    <asp:HiddenField runat="server" ID="hdnCabinTypeException" />
    <input type="hidden" id="childFromDates" value="0" />
    <asp:HiddenField runat="server" ID="hdfEMId" Value="0"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfMode" Value="0"></asp:HiddenField>
    <div class="container-fluid CorpTrvl-page">
        <div class="row">
            <div class="col-xs-12">
                <h2> New Policy</h2>
            </div>
        </div>
        <!--  <div class="row">
            <div class="col-md-12 hidden-xs hidden-sm">
                <div class="policy-details pull-right">
                    <ul>
                        <li>Last Edited by <em>cozmo travel</em></li>
                        <li>Created By <em>cozmo travel</em></li>
                        <li>Creation Date <em>08-January-2017</em></li>
                        <li>Creation Date <em>08-January-2017</em></li>
                        <li>Creation Date <em>08-January-2017</em></li>
                    </ul>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-3 col-lg-2">
                <div class="profile-photo-wrapper">
                    <div class="photo">
                        <span class="glyphicon glyphicon-list-alt icon"></span>
                    </div>
                    <button class="btn btn-default view-profiles">
                        2 Profiles</button>
                </div>
            </div>
            <div class="col-md-9 col-lg-10">
                <div class="CorpTrvl-tabbed-panel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs responsive" role="tablist">
                        <li role="presentation" class="active"><a href="#destinations-policy" aria-controls="destinations-policy"
                            role="tab" data-toggle="tab">Dates & Destinations </a></li>
                        <li role="presentation"><a onclick="return Validate();" href="#air-policy" aria-controls="air-policy"
                            role="tab" data-toggle="tab">Air</a></li>
                        <li role="presentation"><a href="#hotel-policy" aria-controls="hotel-policy" role="tab"
                            data-toggle="tab">Hotels</a></li>
                        <li role="presentation"><a href="#expenses-policy" aria-controls="expenses-policy"
                            role="tab" data-toggle="tab">Expenses</a></li>
                        <li role="presentation"><a href="#approval" aria-controls="approval" role="tab" data-toggle="tab"
                            aria-expanded="true">Approval</a></li>
                    </ul>
                    <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
                    <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content responsive">
                        <div role="tabpanel" class="tab-pane active" id="destinations-policy">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Client Information</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <asp:DropDownList CssClass="form-control" ID="ddlAgents" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select Client"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Policy Name</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Policy Name</label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtPolicyName"></asp:TextBox>
                                        <%--<input type="text" class="form-control" id="txtPolicyName" placeholder="Policy Name">--%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Travel Dates to Avoid</h4>
                                </div>
                            </div>
                            <div id="fromDatesCollection">
                            </div>
                            <!-- <div class="row updated-row">
                                <div class="col-xs-12">
                                    <div class="updated-row-content">
                                        <span class="text">02/August/2017 - 10/August/2017</span> <a href="javascript:void(0);"
                                            class="add-more remove-row" id="removeThis1" onclick="removeDivRow('#removeThis1');">
                                            <span class=" glyphicon glyphicon-remove-sign"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row updated-row">
                                <div class="col-xs-12">
                                    <div class="updated-row-content">
                                        <span class="text">02/August/2017 - 10/August/2017</span> <a href="javascript:void(0);"
                                            class="add-more remove-row" id="removeThis3" onclick="removeDivRow('#removeThis3');">
                                            <span class=" glyphicon glyphicon-remove-sign"></span></a>
                                    </div>
                                </div>
                            </div> -->
                            <!--  <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td colspan="2">
                                                    <label>
                                                        Pickup Date</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                  
                                                </td>
                                                <td>
                                                    <a href="javascript:void(null)" onclick="showCalendar1()">
                                                        <img id="dateLink1" src="images/call-cozmo.png" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            From Date</label>
                                        <div class="input-group">
                                            <uc1:DateControl ID="txtFromDate" runat="server" BaseYearLimit="0" DateFormat="DDMMYYYY"
                                                DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left"
                                                OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down"
                                                WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday">
                                            </uc1:DateControl>
                                            <%--<div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <!--  <div class="col-xs-8 col-sm-2">
                                            <div class="form-group">
                                                <label for="">To Date</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="" placeholder="DD / MM / YY">
                                                    <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                                </div>
                                            </div>
                                        </div> -->
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" id="addFromDate" onclick="return addFromDate();" class="add-more">
                                        <span class=" glyphicon glyphicon-plus-sign"></span></a><a href="javascript:void(0);"
                                            style="display: none;" id="updateFromDate" onclick="return updateFromDate();"
                                            class="add-more"><span class=" glyphicon glyphicon-ok"></span></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Destinations to Avoid</h4>
                                </div>
                            </div>
                            <div id="destinationsCollection">
                            </div>
                            <!--  <div class="row updated-row">
                                <div class="col-xs-12">
                                    <div class="updated-row-content">
                                        <span class="text">India, Mumbai, Avoid Connections: Yes</span> <a href="javascript:void(0);"
                                            class="add-more remove-row" id="removeThis2" onclick="removeDivRow('#removeThis2');">
                                            <span class=" glyphicon glyphicon-remove-sign"></span></a>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Country</label>
                                        <%--<select class="form-control">
                                            <option>Select</option>
                                        </select>--%>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCountry" onchange="LoadCities(this.id)"
                                            runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select Country"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            City</label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCity" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select City"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <div class="form-group">
                                        <div class="checkbox CorpTrvl-checkbox margin-t">
                                            <label>
                                                <input type="checkbox" id="addCon">
                                                Avoid Connections
                                            </label>
                                            <a id="addDesToAvoid" href="javascript:void(0);" class="add-more" onclick="return addDestinations();">
                                                <span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;"
                                                    id="updateDesToAvoid" href="javascript:void(0);" class="add-more" onclick="return updateDestinations();">
                                                    <span class=" glyphicon glyphicon-ok"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="air-policy">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Airlines with Negotiated Fares</h4>
                                </div>
                            </div>
                            <div id="airlineNegotiatedCollection">
                            </div>
                            <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Airline</label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlAirline" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select Airline"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<input type="text" class="form-control" id="" placeholder="Airline">--%>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Special Rate Code *</label>
                                        <asp:TextBox runat="server" ID="txtRateCode" CssClass="form-control"></asp:TextBox>
                                        <%-- <input type="text" class="form-control" id="" placeholder="Rate Code">--%>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-2">
                                    <a id="addAirLinesWithNegotiatedFares" href="javascript:void(0);" class="add-more"
                                        onclick="return addAirLinesWithNegotiatedFares();"><span class=" glyphicon glyphicon-plus-sign">
                                        </span></a><a style="display: none;" id="updateAirLinesWithNegotiatedFares" href="javascript:void(0);"
                                            class="add-more" onclick="return updateAirLinesWithNegotiatedFares();"><span class=" glyphicon glyphicon-ok">
                                            </span></a>
                                </div>
                            </div>
                            <!--  <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="alert alert-warning text-center" role="alert">
                                        No airlines with negotiated rates are available</div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Security Rules</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="checkbox CorpTrvl-checkbox">
                                        <label>
                                            <input id="securityrules" onclick="securityRules();" type="checkbox">If another
                                            Cozmo Travel Demo traveler has a reservation on the same flight, prompt to Book
                                            a different flight
                                        </label>
                                    </div>
                                </div>
                                <!-- <div class="col-xs-12">
                                    <div class="checkbox CorpTrvl-checkbox">
                                        <label>
                                            <input type="checkbox">If another Cozmo Travel Demo traveler has a reservation on
                                            the same flight, prompt to Book a different flight
                                        </label>
                                    </div>
                                </div>  -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Preferred Airlines</h4>
                                        </div>
                                    </div>
                                    <div id="airlinePreferedCollection">
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                <label for="">
                                                    Airline</label>
                                                <asp:DropDownList CssClass="form-control" ID="ddlPrefAirline" runat="server">
                                                    <asp:ListItem Selected="True" Value="0" Text="Select Airline"></asp:ListItem>
                                                </asp:DropDownList>
                                                <!--   <input type="text" class="form-control" id="" placeholder="Airline"> -->
                                            </div>
                                        </div>
                                        <div class="col-xs-1">
                                            <a id="addPreferedAirLines" href="javascript:void(0);" onclick="return addPreferedAirLines();"
                                                class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style='display: none;'
                                                    id="updatePreferedAirLines" href="javascript:void(0);" onclick="return updatePreferedAirLines();"
                                                    class="add-more"><span class=" glyphicon glyphicon-ok"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Airlines to Avoid</h4>
                                        </div>
                                    </div>
                                    <div id="airlineAvoidCollection">
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                <label for="">
                                                    Airline</label>
                                                <asp:DropDownList CssClass="form-control" ID="ddlAvoidAirline" runat="server">
                                                    <asp:ListItem Selected="True" Value="0" Text="Select Airline"></asp:ListItem>
                                                </asp:DropDownList>
                                                <%-- <input type="text" class="form-control" id="" placeholder="Airline">--%>
                                            </div>
                                        </div>
                                        <div class="col-xs-1">
                                            <a id="addAvoidAirLines" href="javascript:void(0);" onclick="return addAvoidAirLines();"
                                                class="add-more"><span class="glyphicon glyphicon-plus-sign"></span></a><a id="updateAvoidAirLines"
                                                    style="display: none;" href="javascript:void(0);" onclick="return updateAvoidAirLines();"
                                                    class="add-more"><span class="glyphicon glyphicon-ok"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%-- <div class="row">
                                <div class="col-xs-12 col-md-6 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Preferred Alliances</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                <label for="">
                                                    Alliance</label>
                                                <select class="form-control">
                                                    <option>Oneworld Alliance</option>
                                                    <option>Oneworld Alliance</option>
                                                    <option>Oneworld Alliance</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-1">
                                            <a href="javascript:void(0);" class="add-more"><span class=" glyphicon glyphicon-plus-sign">
                                            </span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Alliances to avoid</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                <label for="">
                                                    Alliance</label>
                                                <select class="form-control">
                                                    <option>Oneworld Alliance</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-1">
                                            <a href="javascript:void(0);" class="add-more"><span class=" glyphicon glyphicon-plus-sign">
                                            </span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Air Fare Shopping</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="radio CorpTrvl-checkbox">
                                        <label>
                                            <input id="allowAnyFare" onclick="airFareShopping();" type="radio" name="fare">Allow
                                            any Fare Type
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="radio CorpTrvl-checkbox">
                                        <label>
                                            <input id="allowLowestFare" onclick="airFareShopping();" type="radio" name="fare">Mandate
                                            Lowest Fares
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Cabin Type Rules</h4>
                                </div>
                            </div>
                            <div id="defaultCabinCollection">
                            </div>
                            <div class="row">
                                <div class="col-xs-8 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Default Cabin Type</label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCabinType" runat="server" onchange="LoadCabinTypes(this.id)">
                                            <asp:ListItem Selected="True" Value="0" Text="Select Cabin Type"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Business"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Economy"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="First"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="PremiumEconomy"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%-- <select class="form-control">
                                            <option>Economy / Coach</option>
                                        </select>--%>
                                    </div>
                                </div>
                            </div>
                            <div id="cabinTypeExceptionCollection">
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <%--<a href="#" class="btn btn-default add-exception">Add Exception</a>--%>
                                    <a id="addCabinTypeException" href="javascript:void(0);" onclick="return addCabinTypeException();"
                                        class="add-more">Add Exception<span class=" glyphicon glyphicon-plus-sign"> </span>
                                    </a><a style="display: none;" id="updateCabinTypeException" href="javascript:void(0);"
                                        onclick="return updateCabinTypeException();" class="add-more">Update Exception<span
                                            class=" glyphicon glyphicon-ok"> </span></a>
                                </div>
                            </div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-2">
                                        <div class="form-group">
                                            <label for="">
                                                Allow
                                            </label>
                                            <asp:DropDownList CssClass="form-control" ID="ddlExceptionCabinType" runat="server">
                                                <asp:ListItem Selected="True" Value="0" Text="Select Cabin Type"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%--<select class="form-control">
                                                <option>Economy / Coach</option>
                                            </select>--%>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-2">
                                        <div class="radio CorpTrvl-radio-withinput">
                                            <label>
                                                <input id="exception1" checked onclick="cabinexceptionTypes()" type="radio" name="fare">If
                                                going to
                                            </label>
                                            <div class="form-group">
                                                <asp:DropDownList CssClass="form-control" ID="ddlExceptionCity" runat="server">
                                                    <asp:ListItem Selected="True" Value="0" Text="Select City"></asp:ListItem>
                                                </asp:DropDownList>
                                                <%-- <input type="text" class="form-control" id="" placeholder="">--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-2">
                                        <div class="radio CorpTrvl-radio-withinput">
                                            <label>
                                                <input id="exception2" onclick="cabinexceptionTypes()" type="radio" name="fare">Or
                                                if
                                            </label>
                                            <div class="form-group">
                                                <%--<select class="form-control">
                                                    <option>Flight Duration exceeds</option>
                                                    <option>Journey Time exceeds</option>
                                                </select>--%>
                                                <asp:DropDownList disabled CssClass="form-control" ID="ddlCabinTypeRule2" runat="server">
                                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Flight Duration exceeds"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Journey Time exceeds"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group top-buffer-2x">
                                            <div class="input-group">
                                                <input type="number" min="0" class="form-control" onkeydown="if(this.value.length==2 && event.keyCode!=8) return false;"
                                                    disabled id="txtDuration" placeholder="">
                                                <div class="input-group-addon">
                                                    hours</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-2">
                                        <div class="radio CorpTrvl-radio-withinput">
                                            <label>
                                                <input id="exception3" onclick="cabinexceptionTypes()" type="radio" name="fare">Or
                                                if No Seats
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="hotel-policy">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        Minimum Stay Time</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3">
                                    <div class="form-group">
                                        <label>
                                            Avoid hotel booking for stays less than
                                        </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="" placeholder="">
                                            <div class="input-group-addon">
                                                hours</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Preferred Hotel Chains</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                <label for="">
                                                    Hotel Chain</label>
                                                <input type="text" class="form-control" id="" placeholder="Hotel Chain">
                                            </div>
                                        </div>
                                        <div class="col-xs-1">
                                            <a href="javascript:void(0);" class="add-more"><span class="glyphicon glyphicon-plus-sign">
                                            </span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Hotel Chains to Avoid</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                <label for="">
                                                    Hotel Chain</label>
                                                <input type="text" class="form-control" id="" placeholder="Hotel Chain">
                                            </div>
                                        </div>
                                        <div class="col-xs-1">
                                            <a href="javascript:void(0);" class="add-more"><span class=" glyphicon glyphicon-plus-sign">
                                            </span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Star rating</label>
                                        <select class="form-control">
                                            <option>1 Rating</option>
                                            <option>2 Rating</option>
                                            <option>3 Rating</option>
                                            <option>4 Rating</option>
                                            <option>5 Rating</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Expenses Tab:Lokesh 26May2017 -->
                        <div role="tabpanel" class="tab-pane" id="expenses-policy">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="dotted-heading">
                                        Cap Per Expense
                                    </h4>
                                </div>
                            </div>
                            <!-- CAP PER EXPENSE -->
                            <div class="row" data-clone-content="CapePerExpense" id="CapePerExpense">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Country<span class="fcol_red">*</span>
                                        </label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCountryCapExpense" onchange="LoadCitiesCapExpense(this.id)"
                                            runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            City <span class="fcol_red">*</span>
                                        </label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCityCapExpense" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Expense Type <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCapExpenseType" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Currency <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCapExpenseCurrency" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Amount <span class="fcol_red">*</span></label>
                                        <asp:TextBox onkeypress="return isNumber(event);" CssClass="form-control" runat="server"
                                            ID="txtCapExpenseAmount"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Daily Actuals <span class="fcol_red">*</span></label>
                                        <asp:DropDownList Enabled="false" CssClass="form-control" ID="ddlExpDailyActuals"
                                            runat="server">
                                            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="Y" Text="Yes"></asp:ListItem>
                                            <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<input type="radio" disabled="disabled" checked="checked" id="rbtnExpDailyActuals" />--%>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="custom_check">
                                        <input id="chkCapExpenseTravelDays" type="checkbox">
                                        <label for="chkCapExpenseTravelDays">
                                            Include Travel Days</label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label>
                                            &nbsp;
                                        </label>
                                        <div class="font_med">
                                            <a id="addCapExpense" onclick="return addCapExpenseAllowance('E');" href="javascript:void(0);">
                                                <span class="glyphicon glyphicon-plus-sign"></span></a><a style="display: none; color: Red"
                                                    id="cancelCapExpense" href="javascript:void(0);" class="add-more" onclick=" javascript:cancelCapExpense();">
                                                    <span class=" glyphicon glyphicon-remove"></span></a><a style="display: none; color: Green"
                                                        id="updateCapExpense" href="javascript:void(0);" class="add-more" onclick="return updateCapExpense();">
                                                        <span class=" glyphicon glyphicon-ok"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!--
                                
                                <div class="col-xs-12">
                                    <table>
                                        <tr>
                                            <td>
                                                Daily Actuals
                                                <input type="radio" disabled="disabled" checked="checked" id="rbtnExpDailyActuals" />
                                            </td>
                                            <td>
                                                Include Travel Days
                                                <input id="chkCapExpenseTravelDays" type="checkbox">
                                            </td>
                                            <td>
                                                <a id="addCapExpense" onclick="return addCapExpenseAllowance('E');" href="javascript:void(0);"
                                                    data-clone-button="CapeAllowance"><span class="glyphicon glyphicon-plus-sign"></span>
                                                </a><a style="display: none;" id="updateCapExpense" href="javascript:void(0);" class="add-more"
                                                    onclick="return updateCapExpense();"><span class=" glyphicon glyphicon-ok"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                               
                               
                                -->
                            </div>
                            <!-- CAP ON ALLOWANCE -->
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="dotted-heading">
                                        Cap on Allowance
                                    </h4>
                                </div>
                            </div>
                            <div class="row" data-clone-content="CapeAllowance" id="CapeAllowance">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Country <span class="fcol_red">*</span>
                                        </label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCountryCapAllowance" onchange="LoadCitiesCapAllowance(this.id)"
                                            runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            City <span class="fcol_red">*</span>
                                        </label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCityCapAllowance" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Currency <span class="fcol_red">*</span>
                                        </label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlCurrencyCapAllowance" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class=" col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Amount <span class="fcol_red">*</span>
                                        </label>
                                        <asp:TextBox onkeypress="return isNumber(event);" CssClass="form-control" runat="server"
                                            ID="txtCapAllowanceAmount"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Daily Actuals <span class="fcol_red">*</span></label>
                                        <asp:DropDownList Enabled="false" CssClass="form-control" ID="ddlAllowanceDailyActuals"
                                            runat="server">
                                            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="Y" Text="Yes"></asp:ListItem>
                                            <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<input type="radio" disabled="disabled" checked="checked" id="rbtnExpDailyActuals" />--%>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="custom_check">
                                        <input id="chkCapAllowanceTravelDays" type="checkbox">
                                        <label for="chkCapAllowanceTravelDays">
                                            Include Travel Days</label>
                                    </div>
                                </div>
                                <!-- <div class="col-md-4">
                                    <table>
                                        <tr>
                                            <td>
                                                Include Travel Days<input id="chkCapAllowanceTravelDays" type="checkbox" />
                                            </td>
                                        </tr>
                                    </table>
                                </div> -->
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label>
                                            &nbsp;
                                        </label>
                                        <div class="font_med">
                                            <a id="addCapAllowance" href="javascript:void(0)" onclick="return addCapExpenseAllowance('A');"
                                                data-clone-button="CapeAllowance"><span class="glyphicon glyphicon-plus-sign"></span>
                                            </a><a style="display: none; color: Red" id="cancelCapAllowance" href="javascript:void(0);"
                                                class="add-more" onclick="javascript:cancelCapAllowance();"><span class=" glyphicon glyphicon-remove">
                                                </span></a><a style="display: none; color: Green" id="updateCapAllowance" href="javascript:void(0);"
                                                    class="add-more" onclick="return updateCapAllowance();"><span class=" glyphicon glyphicon-ok">
                                                    </span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs responsive" role="tablist">
                                        <li role="presentation" class="active"><a id="aonlyExpenses" href="#onlyExpenses"
                                            aria-controls="onlyExpenses" role="tab" data-toggle="tab" aria-expanded="true">Cap
                                            Per Expense</a> </li>
                                        <li role="presentation" class=""><a id="aonlyAllowance" href="#onlyAllowance" aria-controls="onlyAllowance"
                                            role="tab" data-toggle="tab" aria-expanded="true">Cap on Allowance</a> </li>
                                    </ul>
                                    <div class="tab-content responsive">
                                        <div role="tabpanel" class="tab-pane" id="onlyExpenses">
                                            <div id="onlyExpensesChildDiv">
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="onlyAllowance">
                                            <div id="onlyAllowancesChildDiv">
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: block" class="tab-pane">
                                            <!-- <a class="btn btn-primary pull-right" href="#">DELETE ALL </a>-->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- approval Tab:Lokesh 31May2017 -->
                        <div role="tabpanel" class="tab-pane" id="approval">
                            <!-- PRE-TRIP APPROVAL -->
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="dotted-heading">
                                        Pre-Trip Approval
                                    </h4>
                                </div>
                                <div class="col-md-2">
                                    <div>
                                        <asp:RadioButton Checked="true" ID="preTripNoApproval" GroupName="radio" runat="server" />
                                        <label>
                                            No Approval</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div>
                                        <asp:RadioButton ID="preTripAlways" GroupName="radio" runat="server" />
                                        <label>
                                            Always Required</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="custom_check">
                                        <asp:RadioButton ID="preTripCostExceeds" GroupName="radio" runat="server" />
                                        <label>
                                            Required if trip cost exceeds</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Currency <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlPreTripCurrency" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Amount <span class="fcol_red">*</span></label>
                                        <asp:TextBox onkeypress="return isNumber(event);" CssClass="form-control" runat="server"
                                            ID="txtPreTripAmount"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <!-- RELAUNCH APPROVAL PROCESS -->
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="dotted-heading">
                                        Relaunch Approval Process
                                    </h4>
                                </div>
                                <div class="col-md-5">
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkRelaunchPriceChange" />
                                        <label>
                                            Required if price change exceeds booking price by</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Currency <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlRelaunchPriceChange" runat="server">
                                            <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>
                                            Amount <span class="fcol_red">*</span></label>
                                        <asp:TextBox onkeypress="return isNumber(event);" CssClass="form-control" runat="server"
                                            ID="txtRelaunchPriceAmount"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkRelaunchItineraryChange" />
                                        <label>
                                            Required if there is an itinerary change</label>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <!-- VISA APPROVAL -->
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="dotted-heading">
                                        VISA Approval
                                    </h4>
                                </div>
                                <div class="col-md-2">
                                    <div>
                                        <asp:RadioButton Checked="true" ID="preVisaNoApproval" GroupName="radioV" runat="server" />
                                        <label>
                                            No Approval</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div>
                                        <asp:RadioButton ID="preVisaAlways" GroupName="radioV" runat="server" />
                                        <label>
                                            Always Required</label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                CssClass="btn btn-default " />
                            <asp:Button ID="btnSave" runat="server" OnClientClick="return Validate();" Text="Save"
                                OnClick="btnSave_Click" CssClass="btn btn-default " />
                            <asp:Button ID="btnClear" OnClientClick="javascript:Clear();" runat="server" Text="Clear"
                                OnClick="btnClear_Click" CssClass="btn btn-default " />
                            <%--<button type="button" class="btn btn-primary">"
                                SAVE</button>
                            <button type="button" class="btn btn-default ">
                                CLEAR</button>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<script type="text/javascript">
        //To hide added div
        function removeDivRow(elem) {
            $(elem).closest('.updated-row').remove();
        }

        (function($) {
            //fakewaffle.responsiveTabs(['xs', 'sm', 'md']);

        })(jQuery);
    </script>  --%>
</asp:Content>
<%--All Policy Details Search Grid --%>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="PolicyId"
        EmptyDataText="No Policies List!" AutoGenerateColumns="false" PageSize="17" GridLines="none"
        CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
        CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtName" Width="100px" CssClass="inputEnabled" HeaderText="Policy Name"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("PolicyName") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("PolicyName") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtCode" Width="70px" HeaderText="Policy Code" CssClass="inputEnabled"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("PolicyCode") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("PolicyCode") %>' Width="70px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
