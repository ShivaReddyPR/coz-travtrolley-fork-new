﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FlightChangeRequestQueueGUI" Title="Flight Change Request Queue" Codebehind="FlightChangeRequestQueue.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>


    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />

    <script type="text/javascript">
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //    if (difference < 1) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //        return false;
            //    }
            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
         </script>

    <script type="text/javascript">

        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        function showAgent(id) {

            document.getElementById('DisplayAgent' + id).style.display = 'block';
        }

                function ShowPage(pageNo) {

                    document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
                    document.getElementById('<%=btnSearch.ClientID %>').click();
                    //document.forms[0].submit();
                }

         function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function Validate(index) 
        {
            var adminFee = eval(document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_txtAdminFee').value);
            var supplierFee = eval(document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_txtSupplierFee').value);
            var charges = adminFee + supplierFee;
            var total = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_lblTotal').innerHTML;
            var totalAmount = total.split(" ");
            var totalamt = eval(totalAmount[1].replace(',', ''));
            if (charges > totalamt) {
                alert("Cancellation charges should not be greater then Booking Amount");
                return false;
            }
        }
        
    </script>

    <asp:HiddenField ID="hdnBooking" runat="server" />
    <asp:HiddenField ID="PageNoString" runat="server" Value="1"/>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 130px; left: 97px; display: none; z-index:9999;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 130px; left: 337px; display: none; z-index:9999;">
        </div>
    </div>
     <asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
          <tr>
              <td style="width: 700px" align="left">
                  <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                      onclick="return ShowHide('divParam');">Hide Parameter</a>
              </td>
          </tr>
      </table>
       <div class="paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
         
         
         
         
                 <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-2">  From Date:</div>
    <div class="col-md-2">
    
    
   
    
      <table> 
    <td> <asp:TextBox ID="txtFromDate" runat="server" Width="100px" CssClass="inputEnabled form-control"></asp:TextBox></td>
     <td><a href="javascript:void(null)" onclick="showCal1()">
                                       <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a> </td>
    
    </table>
    
    
    
                             
                                                                    
                                                                    
                                                                    
                                                                    </div>
     <div class="col-md-2">  To Date:</div>
       <div class="col-md-2">     
       
        <table> 
    <td><asp:TextBox ID="txtToDate" runat="server" Width="100px" CssClass="inputEnabled form-control"></asp:TextBox> </td>
     <td> <a href="javascript:void(null)" onclick="showCal2()">
                                                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a></td>
    
    </table>
    
       
       
           
                            </div>
                                                                    
         <div class="col-md-2">Agent: </div>
         <div class="col-md-2"><asp:DropDownList AppendDataBoundItems="true"  CssClass="inputEnabled form-control" ID="ddlAgency" runat="server" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="-1">All</asp:ListItem>
                                
                            </asp:DropDownList> </div>

    <div class="clearfix"></div>
    </div>
    
    
    
            <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-2"> Location:</div>
    <div class="col-md-2">     <asp:DropDownList  AppendDataBoundItems="true" CssClass="inputEnabled form-control" Enabled="false" ID="ddlLocation" runat="server">
                                <asp:ListItem Selected="True" Value="-1">All</asp:ListItem>                                
                            </asp:DropDownList></div>
     <div class="col-md-2"> Booking Status:</div>
       <div class="col-md-2">   <asp:DropDownList  AppendDataBoundItems="true" CssClass="inputEnabled form-control" ID="ddlBookingStatus" runat="server">
                                <asp:ListItem Selected="True" Value="-1">All</asp:ListItem>                               
                            </asp:DropDownList></div>
         <div class="col-md-2"> Pax Name: </div>
         <div class="col-md-2"><asp:TextBox ID="txtPaxName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox> </div>

    <div class="clearfix"></div>
    </div>
    
    
    
            <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-2">  PNR No. </div>
    <div class="col-md-2"> <asp:TextBox ID="txtPNR" runat="server" CssClass="inputEnabled form-control"></asp:TextBox></div>
     <div class="col-md-8"> <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" runat="server" Text="Search" OnClick="btnSearch_Click" /> </div>



    <div class="clearfix"></div>
    </div>
    
    
         
         
         
          
        </asp:Panel>
        </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="right">
                <div style="text-align:right"><%=show %></div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%;">
                    <asp:DataList ID="dlChangeRequests" runat="server" Width="100%" OnItemDataBound="dlChangeRequests_ItemDataBound" OnItemCommand = "dlChangeRequests_ItemCommand">
    <ItemTemplate>
    <div style="display: none" id="DisplayAgent<%#Container.ItemIndex %>" class="div-TravelAgent">
                <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href="#"
                    onclick="hidestuff('DisplayAgent<%#Container.ItemIndex %>');">
                    <img src="images/close1.png" /></a></span>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="25">
                            <b>
                                <asp:Label ID="lblAgencyName" runat="server" Text=""></asp:Label></b>
                        </td>
                        <td>
                            Credit Balance: <strong>
                                <asp:Label ID="lblAgencyBalance" runat="server" Text=""></asp:Label></strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="25">
                            Mob : <strong>
                                <asp:Label ID="lblAgencyPhone1" runat="server" Text=""></asp:Label></strong>
                        </td>
                        <td>
                            Phone : <strong>
                                <asp:Label ID="lblAgencyPhone2" runat="server" Text=""></asp:Label></strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="25">
                            Email : <strong>
                                <asp:Label ID="lblAgencyEmail" runat="server" Text=""></asp:Label></strong>
                        </td>
                        <td>
                            <a style="display:none;" href="#">View Full Profile</a>
                        </td>
                    </tr>
                </table>
                
                
            </div>
    <div>
  
        <asp:HiddenField ID="hdnIndex" runat="server" />        
                
                
                
                
                
                
       
                
          <div class="tbl"> 
    <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-4"> <a href="#" onclick="showAgent(<%#Container.ItemIndex %>);"><strong>
                                <asp:Label ID="lblAgentName" runat="server" Text=""></asp:Label></strong></a></div>
    <div class="col-md-4"><asp:Label ID="lblBookingStatus" runat="server" Text=""></asp:Label> </div>
    <div class="col-md-4"><strong>PNR-<b><asp:Label ID="lblPNR" runat="server" Text=""></asp:Label></b></strong> </div>



    <div class="clearfix"></div>
    </div>
    
    
    
    
        <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-4">  <asp:Label ID="lblAirline" runat="server" Text=""></asp:Label>|
                                        <asp:Label ID="lblFlightNumber" runat="server" Text=""></asp:Label>| 
                            <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label> ADT</div>
    <div class="col-md-4"><strong>
                                <asp:Label ID="lblTrip" runat="server" Text=""></asp:Label> </strong> </div>
    <div class="col-md-4">Total Price:<strong><b> 
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></b> </strong> </div>



    <div class="clearfix"></div>
    </div>


         
         
             <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-4"> Booked:<b> <asp:Label ID="lblDayString" runat="server" Text=""></asp:Label>(<asp:Label ID="lblBookingDate"
                                                runat="server" Text=""></asp:Label>)</b> </div>
    <div class="col-md-4">Flight:<b><asp:Label ID="lblTimeString" runat="server" Text=""></asp:Label></b> </div>
    <div class="col-md-4">  Pax Name:<b><asp:Label ID="lblLeadPaxName" runat="server" Text=""></asp:Label></b></div>



    <div class="clearfix"></div>
    </div>
       
                                    <div class="col-md-12 padding-0 marbot_10">                          

             
    <div class="col-md-4"> Travel Date:<b><asp:Label ID="lblDepartureTime" runat="server" Text=""></asp:Label></b> </div>
    <div class="col-md-4"> Location:<b><asp:Label ID="lblLocation" runat="server" Text=""></asp:Label></b></div>
    <div class="col-md-4"><asp:HyperLink ID="lblReleaseSeat" Visible="false" runat="server">Release Seat</asp:HyperLink> </div>



    <div class="clearfix"></div>
    </div>
    
    
<div class="col-md-12 padding-0 marbot_10">   
    
    <asp:Label ID="lblRemarks" runat="server" BackColor="#CCCCCC" Font-Italic="True"
                                Font-Names="Arial" Font-Size="10pt" Text='<%#Eval("SpecialRequest") %>' Width="100%"></asp:Label>
    
    </div>



   <div class="col-md-12 padding-0 marbot_10">                          

    
    <div> 
     <table> 
             


<tr>
                        
                                    <td>&nbsp;</td>
                                    <td align="center">
                                        <asp:Label ID="Label1" runat="server" Text="Admin Fee" Width="130px"></asp:Label>
                                        &nbsp;
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="Label2" runat="server" Text="Supplier Fee"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                   
                                  
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAdminCurrency" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtAdminFee" runat="server" Width="80px" Text="0" onfocus="if( this.value=='0' ) { this.value=''; }" onblur="if( this.value=='' ) { this.value='0'; }" ></asp:TextBox>&nbsp;
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSupplierCurrency" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtSupplierFee" runat="server" Width="80px" Text="0" onfocus="if( this.value=='0' ) { this.value=''; }" onblur="if( this.value=='' ) { this.value='0'; }"></asp:TextBox>
                                    </td>
                                    <td>
                                        
                                        <asp:Button ID="btnRefund" runat="server" Font-Bold="True" Text="Refund" CommandName="Refund" CssClass="button"
                                            CommandArgument='<%#Eval("FlightId") %>' />
                                    </td>
                                
                                </tr>
                                
                                
                                </table>

</div> 

    <div class="clearfix"></div>
    </div>
          
          <div class="clearfix"></div>
                
              </div>      
              
              
           </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <table align="right">
                                
                            </table>
                        </td>
                    </tr>
                </table>



                
                    
                    
                    
                    
                    
                    
                          
                    
              
            </div>
    </ItemTemplate>
    </asp:DataList>
                </div>
            </td>
        </tr>
        <tr>
             <td align="right">
                <div style="text-align:right"><%=show %></div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

