<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="corporateloginUI" Title="Corporate Login" Codebehind="corporatelogin.aspx.cs" %>

<%@ MasterType VirtualPath="~/Transaction.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">


<style type="text/css">
.footer_big ,.bggray {
display:none;
}


.minfoo {  text-align:center;  border-top:solid 1px #ccc; padding-top:10px; padding-bottom:10px; color:#666  }
.footer_mini footer_domain-login { display:none }
.carousel .carousel-inner .bg4 {background-image:url(images/slider5.jpg); background-position: center top; }

.login_container {   background: rgba(43, 60, 88, 0.7);  }

 .login_lock  {background:url(images/sprite6.png) no-repeat center #2b3c58; 
	  background-position:10px -65px; padding-left:32px; color:#fff; font-weight:bold     }
	  
.boxp { background:#2b3c58; text-align:center; padding: 10px 14px 20px 14px; color:#fff; min-height:260px; margin-bottom:24px; }

.boxp h4 {  margin:0px; font-size:21px; padding: 0px 0px 10px 0px;    }

.corporatepar { border-bottom: solid 1px #ccc;  margin-bottom:24px; padding-bottom:14px;  }

.learmorecor { margin:10px; text-align: center; }
.learmorecor .btn { background:#2b3c58; color:#fff; }


.corpico1,.corpico2,.corpico3 { margin-top:20px; margin-bottom:20px; height:54px; width:100%;  }

.corpico1 { background:url(images/corporate_icons.png) no-repeat top center;    }

.corpico2 { background:url(images/corporate_icons.png) no-repeat; background-position: center -144px;      }
.corpico3 { background:url(images/corporate_icons.png) no-repeat; background-position: center bottom;      }


.navbar-toggle { background:#2b3c58 }	  


	  

</style>




    <script language="javascript" type="text/javascript">

        function setFocus() {
            document.getElementById('ctl00_cphTransaction_txtLoginName').focus();
        }
        setTimeout('setFocus()', 1000);

        function ShowPopUp(id) {
            document.getElementById('<%=txtEmailId.ClientID %>').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('forgotPwd').style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('forgotPwd').style.left = (positions[0] + 130) + 'px';
            //            document.getElementById('forgotPwd').style.top = (positions[1] - 200) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('forgotPwd').style.display = "none";
        }
        function getRelativePositions(obj) {
            var curLeft = 0;
            var curTop = 0;
            if (obj.offsetParent) {
                do {
                    curLeft += obj.offsetLeft;
                    curTop += obj.offsetTop;
                } while (obj = obj.offsetParent);

            }
            return [curLeft, curTop];
        }
        function Validate() {

            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
            
       
     </script>


 <script>

     $(function() {
         $('#carouselSlider,#carouselSlider2').carousel({
             interval: 5000 //changes the speed
         });

     });

</script>


<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

      
        <asp:UpdatePanel ID="upnlLogin" runat="server" UpdateMode="conditional">
        <ContentTemplate>
        




<div class="login_container col-md-3 col-xs-12"> 
<%--  <img src="images/agent-icon.png" alt="" class="agent-icon" />--%>
  <h3>Corporate Login  </h3> 
  
<div class="martop_xs10">

<div class="login_div"> 


 <asp:TextBox ID="txtLoginName" class="login_text" autocomplete="off" placeholder="User Name" runat="server"></asp:TextBox>
</div> 


 
  </div> 
  

<div>

<div class="password_div"> 

<asp:TextBox ID="txtPassword" class="password_text"  autocomplete="off" placeholder="Password"  runat="server" TextMode="password"></asp:TextBox>



</div> 
 
  </div> 


<div class="martop_10">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
   
   
    
    
    
    
    <a class="fcol_fff" id="forgotpassword" href="#"  onclick="return ShowPopUp(this.id);"> Forgot password?</a></td>
    
    <td><label class="pull-right"> 
    
    
    <asp:Button ID="btnLogin" Width="100" class="login_lock btn" runat="server" Text="Login" OnClick="btnLogin_Click" OnClientClick=" Validate()" />
    
    

 
 </label></td>
  </tr>
  
  <tr><td colspan="2"> <asp:Label ID="lblForgotPwd" runat="server" Text="" style="color:Red; font-size:14px;"></asp:Label>
  
  <asp:Label style="color:Red; font-size:14px;" runat="server" ID="lblError" CssClass="lblError"></asp:Label></span>
  
  </td> </tr>
</table>


 </div> 
 
  
  
  
  
  
  </div>
                 



<div  class="modal" id="forgotPwd">
           
			
			
			<div class="modal-content forgot_div">
      <em class="close close_popup" style=" position:absolute;  right:10px; top:10px; cursor:pointer">
					  
						<i><img align="right" alt="Close" onclick="HidePopUp()" src="images/close-itimes.gif"></i>
					</em>

					
					
           
    <div><center> <h4>Forgot password? </h4></center> </div>
                                        
      <div class="martop_14">
      
      <asp:TextBox ID="txtEmailId" CssClass="form-control margin_top10" placeholder="Enter Your Email Address" runat="server" CausesValidation="True" ></asp:TextBox>
      
      
      
       </div>
            
        <div class="learmorecor">   
                
             
                
                
                
                <asp:Button ID="btnGetPassword" CssClass="btn" runat="server" Text="Get Password" OnClientClick="return Validate()"
                            OnClick="btnGetPassword_Click" />
                          
                          
                           <b style="display: none; color:Red" id="err"></b>
                
               
                
                </div>   
           
           
            
           
    </div>


		
					
					         

              
                   
               
           </div>
       
      
    

        </ContentTemplate>
        </asp:UpdatePanel>


                            
          
   
      

      <link rel="stylesheet" href="css/mosaic.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="Scripts/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="Scripts/mosaic.1.0.1.js"></script>
		
		<script type="text/javascript">

		    jQuery(function($) {
		        $('.cover').mosaic({
		            animation: 'slide', //fade or slide
		            hover_x: '400px'		//Horizontal position on hover
		        });
		    });
		    
		</script>
	
	
	
	<div> 
 
 

<!-- Half Page Image Background Carousel Header -->

<header id="carouselSlider" class="carousel slide hidden-xs"  data-ride="carousel">
    
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carouselSlider" data-slide-to="0" class="active"></li>
            <li data-target="#carouselSlider" data-slide-to="1"></li> 
            <li data-target="#carouselSlider" data-slide-to="2"></li>  
           
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
           
            
           

 <div class="item bg bg4 active"> <div class="carousel-caption"></div> </div>
  
  <div class="item bg bg4"><div class="carousel-caption"></div></div>

<div class="item bg bg4"> <div class="carousel-caption"></div></div>
            
                     
        </div>
             
        <!-- Controls -->
    <!--    <a class="left carousel-control" href="#carouselSlider" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#carouselSlider" data-slide="next">
            <span class="icon-next"></span>
        </a>
-->
    </header>
    
<!-- Half Page Image Background Carousel Header ends -->
           
  
  
  
  


<div class="clearfix"> </div>
</div>	

	
     








<!--body container ends-->


<div class="loginbody"> 
      
      <div class="col-md-12 hidden-xs">
<div class="col-md-12">

<h3><center> Cozmo OBT </center></h3>


<div class="corporatepar"> <center> Online Travel Booking System for Business allows employees to plan and book their trip, with automated validation of the travel policy every step of the way, saving time and effort in organizing travel.

</center>






</div>

</div>

</div>


      <div class="col-md-12 hidden-xs">


  <div class="col-md-4">
  <div class="boxp"> 
  

  <div class="corpico1"> </div>

  <h4> Traveller Profile</h4>
  <div> Enforce policies with a customizable pre-trip planning process that enables managers to approve, reject and request changes.</div>
  
  
  </div>
  
  
  </div>

 
 <div class="col-md-4">
  <div class="boxp"> 
  

  <div class="corpico2"> </div>

  <h4> Trip Planning</h4>
  <div> Enforce policies with a customizable pre-trip planning process that enables managers to approve, reject and request changes.</div>
  
  
  </div>
  
  
  </div>
  
  
  <div class="col-md-4">
  <div class="boxp"> 
  

  <div class="corpico3"> </div>

  <h4> Travel Policy</h4>
  <div> Enforce policies with a customizable pre-trip planning process that enables managers to approve, reject and request changes.</div>
  
  
  </div>
  
  
  </div>  



<div class="clearfix"> </div>

  </div>





  <div class="clearfix"> </div>

</div>


<div class="minfoo"> 

<center>

 <span id="ctl00_lblCopyRight">Copyright � <script>                                               var dteNow = new Date(); var intYear = dteNow.getFullYear(); document.write(intYear);</script> Abraaj , UAE. All Rights Reserved.</span>

  </center>
</div>
 
 <div  style="display: none"> 

      <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 150px">
            </td>
            <td style="width: 150px">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label Visible="false" ID="lblCompanyName" runat="server" Text="Company:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" Visible="false" ID="ddlCompany" CssClass="inputDdlEnabled"
                    Width="154px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="height: 15px">
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
            </td>
        </tr>
    </table>
  
  
  </div>
  
  

           
</asp:Content>
