﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorpGVSalesDispatchQueue" Codebehind="CorpGVSalesDispatchQueue.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
  
  
    <script>
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function ValidateParam() {
            clearMessage();
            var fromDate = GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
            var fromTime = getElement('dcFromDate_Time');
            var toDate = GetDateTimeObject('ctl00_cphTransaction_dcToDate');
            var toTime = getElement('dcToDate_Time');
            if (fromDate == null) addMessage('Please select From Date !', '');
            if (fromTime.value == '') addMessage('Please select From Time!', '');
            if (toDate == null) addMessage('Please select To Date !', '');
            if (toTime.value == '') addMessage('Please select To Time!', '');
            if ((fromDate != null && toDate != null) && fromDate > toDate) addMessage('From Date should not be later than To Date!', '');
            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }
        }
        function setDocumentPosition(id,divId) {
            document.getElementById(divId.id).style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            document.getElementById('<%=hdfHistory.ClientID%>').value = "0";
        }
        function getRelativePositions(obj) {
            var curLeft = 0;
            var curTop = 0;
            if (obj.offsetParent) {
                do {
                    curLeft += obj.offsetLeft;
                    curTop += obj.offsetTop;
                } while (obj = obj.offsetParent);

            }
            return [curLeft, curTop];
        }
        function DownloadDocument(path, filename) {
            var path = path + '/' + filename;
            var docName = filename;
            var open = window.open('DownloadeDoc.aspx?path=' + path + '&docName=' + docName);
            return false;
        }
        function GridShowRemarks() {
            var gridView = document.getElementById("<%=gvDocuments.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            var checked = true;
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && !checkBoxes[i].checked) {
                    checked = false;
                }
            }
            if (!checked) {
                if (document.getElementById("<%=chkDocumentVerified.ClientID %>") != null) {
                    document.getElementById("<%=chkDocumentVerified.ClientID %>").checked = false;
                }
            }
            else {
                if (document.getElementById("<%=chkDocumentVerified.ClientID %>") != null) {
                    document.getElementById("<%=chkDocumentVerified.ClientID %>").checked = true;
                }
            }
            if (document.getElementById("<%=txtRemarks.ClientID %>") != null) {
                if (document.getElementById("<%=chkDocumentVerified.ClientID %>") != null && document.getElementById("<%=chkDocumentVerified.ClientID %>").checked) {
                    document.getElementById("<%=txtRemarks.ClientID %>").style.display = "none";
                }
                else {
                    document.getElementById("<%=txtRemarks.ClientID %>").style.display = "block";
                }
            }
        }
        function ShowRemarks() {
             if (document.getElementById("<%=txtRemarks.ClientID %>") != null) {
                if (document.getElementById("<%=chkDocumentVerified.ClientID %>") != null && document.getElementById("<%=chkDocumentVerified.ClientID %>").checked) {
                    document.getElementById("<%=txtRemarks.ClientID %>").style.display = "none";
                }
                else {
                    document.getElementById("<%=txtRemarks.ClientID %>").style.display = "block";
                }
            }
        }
        function ShowHistory() {
           
            document.getElementById('<%=hdfHistory.ClientID%>').value = "1";
        }
        function hideHistory() {
            document.getElementById('<%=hdfHistory.ClientID%>').value = "0";
            document.getElementById('divHistory').style.display = "none";
            $('#divHistory').modal('hide');
            return false;
        }
    </script>


    <asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
    <asp:HiddenField ID="hdnDispatchStatus" runat="server" Value="" />
    <asp:HiddenField ID="hdnDispatchValue" runat="server" Value="" />
    <asp:HiddenField ID="hdfPaxId" runat="server" ></asp:HiddenField>
     <asp:HiddenField ID="hdfHistory" runat="server" Value="0" ></asp:HiddenField>


   <style> .but_b1 { background: #666; } </style>  


   <div> 

      <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:100px" align="right"><a style="cursor:Hand;font-weight:bold;color:Black" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a> </td>
                <td width="800px" align="right">
        </td>
          </tr>
         </table>
   
   </div>

     <div class="grdScrlTrans" style="margin-top:-1px;">
          <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
         <div class="paramcon"> 
       
      
   <div class="col-md-12 padding-0 marbot_10"> 
       
       <div class="col-md-2">  <asp:Label ID="lblFromDate" Text="From Date:" runat="server" ></asp:Label></div>
       
       <div class="col-md-2"> <uc1:DateControl class="form-control" Id="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
       
       
       <div class="col-md-2"> <asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label> </div>
       
       <div class="col-md-2"> <uc1:DateControl class="form-control" Id="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
       
              <div class="col-md-2"> <asp:Label ID="lblAgent" runat="server" Visible="true"  Text="Agent:"></asp:Label> </div>
       
       <div class="col-md-2"> <asp:DropDownList ID="ddlAgent"  CssClass="inputDdlEnabled form-control" Visible="true"  runat="server" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> </div>
        
       <div class="clearfix"> </div>
       </div>
       
     <div class="col-md-12 padding-0 marbot_10"> 
       
       
       <div class="col-md-2"> <asp:Label ID="lblB2BAgent" runat="server" Visible="true"  Text="B2BAgent:"></asp:Label> </div>
       
       <div class="col-md-2"> <asp:DropDownList  ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> </div>
       
              
       <div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" runat="server" Visible="true"  Text="B2B2BAgent:"></asp:Label> </div>
       <div class="col-md-2"> <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged1" AutoPostBack="true"></asp:DropDownList> </div>
       
       
       <div class="col-md-2"> <asp:Label ID="lblLocation" runat="server" Text="Location:"></asp:Label> </div>
       <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlLocation" runat="server">
                       </asp:DropDownList> </div>
        
      <div class="clearfix"> </div>
       </div>
       
             <div class="col-md-12 padding-0 marbot_10"> 
       
       <div class="col-md-2"> <asp:Label ID="lblConsultant"  runat="server" Text="Consultant:"></asp:Label> </div>
       <div class="col-md-2">  <asp:DropDownList  CssClass="form-control" ID="ddlConsultant" runat="server">
                   </asp:DropDownList></div>
                   
       <div class="col-md-2"> <asp:Label ID="lblCountry" runat="server" Text="Country To Visit:"></asp:Label> </div>
       <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true">
                       </asp:DropDownList> </div>
                       
                   <div class="col-md-2"> <asp:Label ID="lblVisaType" runat="server" Text="Visa Type:"></asp:Label> </div>
       
       <div class="col-md-2">  <asp:DropDownList CssClass="form-control" ID="ddlVisaType" runat="server">
                    </asp:DropDownList></div>        
          
       <div class="clearfix"> </div>
       </div>
            <div class="col-md-12 padding-0 marbot_10">
                 <div class="col-md-2"> <asp:Label ID="lblNationality"  runat="server" Text="Nationality:"></asp:Label> </div>
                  <div class="col-md-2">  <asp:DropDownList  CssClass="form-control" ID="ddlNationality" runat="server">
                   </asp:DropDownList></div>
                <%-- <div class="col-md-8">
                     <label class="pull-right">
                         <asp:Button runat="server" ID="btnSearch" Text="Search"
                             CssClass="btn but_b" OnClick="btnSearch_Click"/>
                     </label>
                 </div>--%>
             </div>
       </div>
         
        </asp:Panel>
    </div>
          <div style=" padding-top:14px"> 
     
     
     <table> 
     
     <tr> 
     
       <td>            
             <asp:Button ID="btnSearch" Text="All" runat="server" OnClientClick="return ValidateParam();" CssClass="btn but_b1"  OnClick ="btnSearch_Click" ></asp:Button>
              <asp:Button ID="btnReceived" Text="Received" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1" OnClick="btnReceived_Click"></asp:Button>
              
              <asp:Button  ID="btnDocReceived" Text="Doc Received" runat="server" OnClientClick="return ValidateParam();" 
              CssClass="btn but_b1" OnClick="btnDocReceived_Click" ></asp:Button>
              
              <asp:Button  ID="btnDocVerified"  Text="Doc Verified" runat="server" OnClientClick="return ValidateParam();" CssClass="btn but_b1" OnClick="btnDocVerified_Click" ></asp:Button>
              
              <asp:Button  ID="btnDocMisMatch"  Text="Doc MisMatch" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1" OnClick="btnDocMisMatch_Click"></asp:Button>
              
               <asp:Button  ID="btnReadyForSub" Text="Ready For Submission" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1"  OnClick="btnReadyForSub_Click" ></asp:Button>
               
               <asp:Button  ID="btnSubmitted"  Text="Submitted" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1" OnClick="btnSubmitted_Click"></asp:Button>
               
               <asp:Button  ID="btnCollection"  Text="Collection" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1" OnClick="btnCollection_Click"></asp:Button>
               
               <asp:Button  ID="btnDispatched"  Text="Dispatched" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1" OnClick="btnDispatched_Click"></asp:Button>
               
               <asp:Button  ID="btnAcknowledge"  Text="Acknowledge" runat="server" OnClientClick="return ValidateParam();" CssClass=" btn but_b1" OnClick="btnAcknowledge_Click"></asp:Button> 
            </td>
     
     </tr>
     
     
     </table>
     
     
     
     
     </div> 
         <table>
     <tr>
  
           
            
            
            </tr>
            <tr>
          <td align="right"><asp:Label ID="lblStatus" runat="server" Text="Status:" Visible="false"></asp:Label></td>
          <td width="120"><asp:DropDownList ID="ddlStatus" runat="server" CssClass="inputEnabled form-control" Width="153px" Visible="false" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="-1" Selected="True" Text="All"></asp:ListItem>
                    <asp:ListItem Value="A" Text="APPROVED"></asp:ListItem>
                    <asp:ListItem Value="R" Text="REJECTED"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
    
    </table>



<style> 


    .costum_modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.costum_modal_content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}


</style>


<div id="divDocuments" class="costum_modal">

  <!-- Modal content -->
  <div class="costum_modal_content">
    <%--<span class="close">&times;</span>--%>
    <p>
    
    <div> 
    <asp:HiddenField ID="hdnVisaId" runat="server" Value="" />
          <asp:HiddenField ID="hdnProfileId" runat="server" Value="" />
     <td><asp:Label  id="lblDocNo" runat="server" Font-Bold="true" Text="File No:"></asp:Label>
     <asp:Label  id="lblDocNoValue" runat="server" Text="" CssClass="lblel"></asp:Label>
      <asp:Label  id="lblDPaxName" runat="server" Font-Bold="true" Text="Pax Name:" CssClass="lblel"></asp:Label>
     <asp:Label  id="lblDPaxNameValue" runat="server" Text="" CssClass="lblel"></asp:Label>
    
    </div>

   <div class="table-responsive">
    
    <asp:GridView CssClass="table table-striped" ID="gvDocuments" runat="server" AutoGenerateColumns="False"
                 AllowPaging="false" PageSize="10" DataKeyNames="DocId" 
                 GridLines="none" OnRowDataBound="gvDocuments_RowDataBound">
                 <Columns>
                     <asp:TemplateField>
                         <HeaderTemplate>
                         </HeaderTemplate>
                         <ItemTemplate>
                             <asp:HiddenField runat="server" ID="IThdfDocKey" Value='<%# Eval("DocId") %>'></asp:HiddenField>
                             <label>
                                 <asp:CheckBox runat="server" ID="ITchkSelect" onclick="javascript:GridShowRemarks();" ></asp:CheckBox></label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField>
                         <HeaderTemplate>
                             <label style=" font-weight:bold; color:#000">
                                 Doc.Name</label>
                         </HeaderTemplate>
                         <ItemTemplate>
                             <asp:Label ID="ITlblDocName" runat="server" Text='<%# Eval("DocTypeName") %>' CssClass=""
                                 ToolTip='<%# Eval("DocTypeName") %>' Width="300px"></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                      <asp:TemplateField>
                         <HeaderStyle />
                         <HeaderTemplate>
                             <label style=" font-weight:bold; color:#000">Document</label>
                         </HeaderTemplate>
                         <ItemStyle />
                         <ItemTemplate>
                             <asp:HyperLink ID="ITHylnkDocument" runat="Server" Text='<%# Eval("DocTypeName") %>' Width="70px" ></asp:HyperLink>
                             <%--<asp:LinkButton ID="ITlnkDocument" runat="Server" Text='<%# Eval("DocTypeName") %>' Width="70px"  OnClientClick='<%# "DownloadDocument(\"" + Eval("DocTypeName" ) + "\");" %>' ></asp:LinkButton>--%>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField>
                         <HeaderTemplate>
                             <label style=" font-weight:bold; color:#000">
                                 status</label>
                         </HeaderTemplate>
                         <ItemTemplate>
                             <asp:Label ID="ITlblDocStatus" runat="server" Text='<%# Eval("Status") %>' CssClass=""
                                 ToolTip='<%# Eval("Status") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                    
                 </Columns>
                 <HeaderStyle CssClass="normal-heading"></HeaderStyle>
                 <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                 <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
             </asp:GridView>
    </div>


    <div> 
    
     <asp:CheckBox ID="chkDocumentVerified" runat="server" Text="DocumentVerified" Visible="false" onclick="javascript:ShowRemarks();"/>
                 
                
    </div>

    <div>  <asp:TextBox CssClass="form-control" ID="txtRemarks" runat="server" placeholder="Remarks" TextMode="MultiLine" Height="50px"></asp:TextBox> </div>
        <div>
            <asp:LinkButton ID="lnlDownloadAll" runat="server" Text="DownloadAll" OnClick="lnlDownloadAll_Click"></asp:LinkButton>
        </div>

<div> 

 <asp:Button ID="btnDocUpdate" runat="server" Text="DocUpdate" CssClass="button" OnClick="btnDocUpdate_Click" /> 
     <asp:Button ID="btnDocClose" runat="server" Text="Close" CssClass="button" OnClick="btnDocClose_Click" />
</div>


    </p>
  </div>

</div>

          <div id="divDocNo" style="position:absolute;display:none;">
    
    <div class="bg_white bor_gray pad_10"> 
    
     <table class="trpad" width="300">
    
        <tr>
        <td>
            <asp:Label ID="lblDocCountry" Text="Country:" runat="server" ></asp:Label>
             
             </td>
       
       <td> <asp:Label ID="lblDocCountryValue" runat="server" ></asp:Label></td>
         
        </tr>
        
        <tr> 
        
        <td> <asp:Label ID="lblDocNationality" Text="Nationality:" runat="server" ></asp:Label></td>
        
         <td>
             
             <asp:Label ID="lblDocNationalityValue" runat="server" ></asp:Label>
             </td>
        
        </tr>
        
        
        
       
         
        
        <tr>  <td> <asp:Label ID="lblDocPassportType" Text="PassportType:" runat="server" ></asp:Label></td>  <td><asp:Label ID="lblDocPassportTypeValue" runat="server" ></asp:Label> </td> </tr>
        
        
        
          <tr>  <td><asp:Label ID="lblDocVisaType" Text="VisaType:" runat="server" ></asp:Label> </td>  <td><asp:Label ID="lblDocVisaValue" runat="server" ></asp:Label> </td> </tr>
          
            <tr>  <td><asp:Label ID="lblDocResidenceName" Text="Residence:" runat="server" ></asp:Label> </td>  <td><asp:Label ID="lblDocResidenceNameValue" runat="server" ></asp:Label> </td> </tr>
        
       
         
          <tr>  <td> <asp:Label ID="lblDocVisitorName" Text="Visitor:" runat="server" ></asp:Label></td>  <td><asp:Label ID="lblDocVisitorNameValue" runat="server" ></asp:Label> </td> </tr>
         
         
        
        
         <tr>  <td> <asp:Label ID="lblDocPasportNo" Text="Passport #:" runat="server" ></asp:Label></td>  <td><asp:Label ID="lblDocPasportNoValue" runat="server" ></asp:Label> </td> </tr>
         
         
         
         
        
          
          
        
         
        <tr>
              
        
        
        <td colspan="2" align="right">
        <asp:Button ID="btnDClose" runat="server" Text="Close" CssClass="btn but_b" OnClick ="btnDClose_Click" /></td>
        </tr>
        </table>
    
    
    </div>
    
    
            
      
    </div>



         


    
        
         <div id="divHistory" class="costum_modal">


             <div class="costum_modal_content">
               
                 <h4 class="modal-title">Remarks History</h4>
                 <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" Width="100%"
                     AllowPaging="false" PageSize="10" DataKeyNames="his_id" CellPadding="4" CellSpacing="0"
                     GridLines="none">
                     <Columns>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">
                                     Dispatch Status</label>
                             </HeaderTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="ITlblDocName" runat="server" Text='<%# Eval("fpax_dispatch_status_name") %>' CssClass=""
                                     ToolTip='<%# Eval("fpax_dispatch_status_name") %>' Width="300px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField>
                             <HeaderStyle Width="100px" />
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">Dispatch_remarks</label>
                             </HeaderTemplate>
                             <ItemStyle />
                             <ItemTemplate>
                                 <asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("fpax_dispatch_remarks") %>' CssClass=""
                                     ToolTip='<%# Eval("fpax_dispatch_remarks") %>' Width="300px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                          <asp:TemplateField>
                             <HeaderStyle Width="100px" />
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">Modified By</label>
                             </HeaderTemplate>
                             <ItemStyle />
                             <ItemTemplate>
                                 <asp:Label ID="ITlblModifiedBy" runat="server" Text='<%# Eval("modifiedBy") %>' CssClass=""
                                     ToolTip='<%# Eval("modifiedBy") %>' Width="300px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <label style=" font-weight:bold; color:#000">
                                     Modified Date</label>
                             </HeaderTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="ITlblModifiedDate" runat="server" Text='<%# Eval("fpax_modified_on") %>' CssClass=""
                                     ToolTip='<%# Eval("fpax_modified_on") %>' Width="300px"></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>

                     </Columns>
                     <HeaderStyle CssClass="normal-heading"></HeaderStyle>
                     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                 </asp:GridView>

                   <input type="button" value="Close" name="Close" class="button" onclick="hideHistory()" />
             </div>
             </div>

         <asp:GridView ID="gvVisaSales" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="FPAX_ID" 
    EmptyDataText="No Visa Sales List!" AutoGenerateColumns="false" PageSize="16" GridLines="none"  CssClass="grdTable"
     CellPadding="4" CellSpacing="0" OnRowDataBound="gvVisaSales_RowDataBound" 
    OnPageIndexChanging="gvVisaSales_PageIndexChanging" OnSelectedIndexChanged="gvVisaSales_SelectedIndexChanged">
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    
    <HeaderTemplate>
    <label CssClass="filterHeaderText">Select</label>
    <asp:CheckBox runat="server" id="HTchkSelectAll" AutoPostbACK="true" OnCheckedChanged="ITchkSelect_CheckedChanged" ></asp:CheckBox>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" OnClick="enableGridControls(this.id);" CssClass="" Checked='<%# Eval("checked_status").ToString()=="true"%>'></asp:CheckBox>
     </ItemTemplate>    
    </asp:TemplateField>   
   <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'  ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  

    <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <label style="color:Black; font-weight:bold" CssClass="filterHeaderText">Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptDate" Width="130px" runat="server" Text='<%# IDDateTimeFormat(Eval("fvs_doc_date")) %>' CssClass="grdof"  ToolTip='<%# Eval("fvs_doc_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>
    
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <label style="color:Black; font-weight:bold" CssClass="filterHeaderText">File #</label>
     </HeaderTemplate>
    <ItemTemplate >
    <asp:LinkButton ID="ITlblDocNo" runat="server" Text='<%# Eval("fvs_doc_no") %>' ToolTip='<%# Eval("fvs_doc_no") %>' Width="80px" OnClick="ITlblDocNo_Click" style="Color:Red"></asp:LinkButton>
    <asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("fvs_id") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfPaxId" runat="server" Value='<%# Bind("fpax_id") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfProfileId" runat="server" Value='<%# Bind("fvs_corpprofile_id") %>'></asp:HiddenField>
    </ItemTemplate>    
    
    </asp:TemplateField> 
    
     <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
        <label style="color:Black; font-weight:bold" CssClass="filterHeaderText">Docket #</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblDocketNo" runat="server" Text='<%# Eval("FVS_DOCKET_NO") %>' CssClass="label grdof width100"  ToolTip='<%# Eval("FVS_DOCKET_NO") %>' Width="100px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> 
    
     <asp:TemplateField>
    <HeaderStyle  Width="70px"/>
    <HeaderTemplate>
    <label CssClass="filterHeaderText">Status</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:HiddenField id="IThdfApproveStatus" runat="server" value='<%# Eval("FPAX_DISPATCH_STATUS") %>'></asp:HiddenField>
    
    <asp:DropDownList ID="ITddlApprove" CssClass="inputDdlDisabled" Enabled="false" Width="100px" runat="server" onchange="statusChanged(this.id)">
    <asp:ListItem Value="A" >Approved</asp:ListItem>
    <asp:ListItem Value="R" >Rejected</asp:ListItem>
    </asp:DropDownList>


    </ItemTemplate>    
    </asp:TemplateField> 
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label CssClass="filterHeaderText">Visa #</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:TextBox ID="ITtxtVisaNo" runat="server" Width="110px" CssClass="form-control" Text='<%# Eval("fpax_visa_no") %>' MaxLength="20" ></asp:TextBox>
    </ItemTemplate>    
    </asp:TemplateField> 
    
   <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label CssClass="filterHeaderText">Visa Issue Date</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <uc1:DateControl Id="ITdcVisaIssueDate" Width="110px"  runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" Value='<%# Eval("FPAX_VISA_ISSUE_DATE").Equals(DBNull.Value)?DateTime.MinValue:(DateTime)Eval("FPAX_VISA_ISSUE_DATE")%>' DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
    </ItemTemplate>    
    </asp:TemplateField>  
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label CssClass="filterHeaderText">Visa Exp.Date</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <uc1:DateControl Id="ITdcVisaExpDate" Enabled="false" Width="110px" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" Value='<%# Eval("fpax_visa_exp_date").Equals(DBNull.Value)?DateTime.MinValue:(DateTime)Eval("fpax_visa_exp_date")%>' DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label CssClass="filterHeaderText">Visa Coll.Date</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <uc1:DateControl Id="ITdcVisaCollDate" Width="110px" Enabled="false"  runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" Value='<%# Eval("fpax_visa_exp_date").Equals(DBNull.Value)?DateTime.MinValue:(DateTime)Eval("fpax_visa_exp_date")%>' DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
    </ItemTemplate>    
    </asp:TemplateField> 
    
      <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
     <label CssClass="filterHeaderText">Remarks</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:TextBox ID="ITtxtRemarks" runat="server" Width="150px" MaxLength="250" Text='<%# Eval("Fpax_dispatch_remarks") %>'   CssClass="form-control "></asp:TextBox>
     </ItemTemplate>    
    </asp:TemplateField>    
     <%--<asp:TemplateField>
    <HeaderTemplate>
        <label  style="color:Black">Add&nbsp;Charges</label>        
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Button runat="server" id="ITlnkAddCharges" CssClass="button" style="Color:Red" Font-Size="8pt" CommandName="Select" Text="Add Charges" ></asp:Button>
        <asp:HiddenField id="IThdfChargesId" runat="server" Value='<%# Bind("FPAX_ADD_CHARGES_ID") %>'></asp:HiddenField>
    </ItemTemplate>
    </asp:TemplateField>--%>
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate> 
     <label style="color:Black;font-weight:bold;">Application #</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
        <asp:TextBox ID="ITtxtTrackingNo" runat="server" CssClass="inputEnabled form-control" Text='<%# Eval("fpax_tracking_no") %>' Width="80px" MaxLength="20"  ></asp:TextBox>
     </ItemTemplate>    
    </asp:TemplateField>   
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate> 
     <label style="color:Black;font-weight:bold;">Documents #</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
     <asp:Button runat="server" id="ITlnkDocuments" CssClass="btn but_b"  CommandName="Select"  Text="Documents" ></asp:Button>
      <asp:HiddenField id="IThdfCountryId" runat="server" Value='<%# Bind("fvs_country_id") %>'></asp:HiddenField>
      <asp:HiddenField id="IThdfNationality" runat="server" Value='<%# Bind("fvs_nationality_id") %>'></asp:HiddenField>
      <asp:HiddenField id="IThdfVisaTypeId" runat="server" Value='<%# Bind("FVS_VISA_TYPE_ID") %>'></asp:HiddenField>
      <asp:HiddenField id="IThdfResidenceId" runat="server" Value='<%# Bind("FVS_RESIDENCE_ID") %>'></asp:HiddenField>
      
      </ItemTemplate>    
    </asp:TemplateField> 

         <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate> 
     <label style="color:Black;font-weight:bold;" CssClass="filterHeaderText">Assign To</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
        <asp:DropDownList ID="ITddlAssignTo" runat="server" Width="150px"></asp:DropDownList>
     </ItemTemplate>    
    </asp:TemplateField>     
       <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate> 
     <label style="color:Black;font-weight:bold;" >Assign To</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
        <asp:Label ID="ITlblAssignTo" runat="server" Text='<%# Eval("assignTouser") %>' CssClass="grdof"  ToolTip='<%# Eval("assignTouser") %>' Width="100px"></asp:Label>
     </ItemTemplate>    
    </asp:TemplateField>   
     <asp:TemplateField>
    <HeaderStyle  />
    <ItemStyle />
    <HeaderTemplate>
     <label style="color:Black;font-weight:bold;">Dis.Status</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblDispatchStatus" runat="server" Text='<%# Eval("fpax_dispatch_status_name") %>' CssClass="grdof"  ToolTip='<%# Eval("fpax_dispatch_status_name") %>' Width="100px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  
     
          <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Disp.Ref #</label>
     </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblDispRefNo" runat="server" Text='<%# Eval("FPAX_DOC_NO") %>' CssClass="grdof" ToolTip='<%# Eval("FPAX_DOC_NO") %>' Width="90px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField> 
      <asp:TemplateField>
    <HeaderStyle  width="100px"/>
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Rcpt Print</label>    
    </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <asp:LinkButton ID="ITlnkPrint" runat="Server" onClick="ITlnkPrint_Click" Text="Rcpt Print" width="70px"  ></asp:LinkButton>
    </ItemTemplate>    
    </asp:TemplateField>
        <asp:TemplateField>

            <HeaderTemplate>
                <label style="color: Black"><strong>Remarks History</strong></label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="ITlnkHistory" CommandName="Select" Text="Remarks History" runat="server" OnClientClick="ShowHistory();"></asp:LinkButton>

            </ItemTemplate>
        </asp:TemplateField>
   <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Country</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("countryName") %>' CssClass="grdof"  ToolTip='<%# Eval("countryName") %>' Width="100px"></asp:Label>
    <asp:HiddenField ID="IThdnPSPType" runat="server"  Value='<%# Eval("fvs_passport_type") %>' />
    <asp:HiddenField ID="IThdnVisaType" runat="server"  Value='<%# Eval("fvs_visa_type_name") %>' />
    <asp:HiddenField ID="IThdnResidenceName" runat="server"  Value='<%# Eval("FVS_RESIDENCE_NAME") %>' />
    
    </ItemTemplate>    
    </asp:TemplateField> 
    
     <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Nationality</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("nationality") %>' CssClass="grdof"  ToolTip='<%# Eval("nationality") %>' Width="100px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Visitor Name</label>
     </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <asp:Label ID="ITlblVisitorName" runat="server" Text='<%# Eval("FPAX_NAME") %>' CssClass="grdof"  ToolTip='<%# Eval("FPAX_NAME") %>' Width="125px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Passport #</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblPassport" runat="server" Text='<%# Eval("fpax_passport_no") %>' CssClass="grdof"  ToolTip='<%# Eval("fpax_passport_no") %>' Width="100px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
    
     
    
    <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">User</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblUSer" runat="server" Text='<%# Eval("fVS_CREATED_NAME") %>' CssClass="grdof"  ToolTip='<%# Eval("fVS_CREATED_NAME") %>' Width="120px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> 
    
    <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Location</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("fvs_location_name") %>' CssClass="grdof"  ToolTip='<%# Eval("fvs_location_name") %>' Width="120px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    
   
    <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Agent</label>
     </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("fvs_agent_name") %>' CssClass="label grdof width120"  ToolTip='<%# Eval("fvs_agent_name") %>' Width="120px"></asp:Label>
    <asp:HiddenField id="IThdfAgentId" runat="server" Value='<%# Bind("FVS_AGENT_ID") %>'></asp:HiddenField>
    </ItemTemplate>    
    </asp:TemplateField>  
   
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Total Cost</label>
     </HeaderTemplate>
    <ItemStyle HorizontalAlign="right" />
    <ItemTemplate>
    <asp:Label ID="ITlblTotalVisaFee" style="margin-right:5px" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("fvs_total_visa_fee")) %>' CssClass="grdof"  ToolTip='<%# Eval("fvs_total_visa_fee") %>' Width="80px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
     <asp:TemplateField>
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Currency</label>
     </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblCurrenecy" runat="server" Text='<%# Eval("fvs_currency_code") %>' CssClass="grdof"  ToolTip='<%# Eval("fvs_currency_code") %>' Width="50px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>   
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Coll.Mode</label>
     </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblHTtxtModeDesc" style="margin-right:5px" runat="server" Text='<%# Eval("fvs_settlement_mode") %>' CssClass="grdof"  ToolTip='<%# Eval("fvs_settlement_mode") %>' Width="120px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField> 
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black;font-weight:bold;">Mode Remarks</label>
     </HeaderTemplate>
    <ItemStyle HorizontalAlign="right" />
    <ItemTemplate>
    <asp:Label ID="ITlblModeRemraks" style="margin-right:5px" runat="server" Text='<%# Eval("fvs_mode_remarks") %>' CssClass="grdof"  ToolTip='<%# Eval("fvs_mode_remarks") %>' Width="120px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  
    </Columns>           
    </asp:GridView>
     </div>
    
    
    <div class="col-md-12"> 
    <br /> 
    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn but_b" OnClick="btnUpdate_Click" OnClientClick="return disableUpdate();"/>
     
    <asp:Label ID="lblSuccessMsg" runat="server" Text="" CssClass="lblSuccess"></asp:Label>
     <br />  <br />  <br /> 
    </div>


    <script>

        function disableUpdate() {
            try{
                document.getElementById('<%=btnUpdate.ClientID%>').style.display = "none";
                return true;
            }
            catch (ex) {
                return true;
            }
        }

        function enableGridControls(chkId) {

            var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
            //alert(Id);
            if (document.getElementById(chkId) != null) {

                //if (!document.getElementById(chkId).checked) //document.getElementById(Id + 'ITtxtRemarks').value = '';
                document.getElementById(Id + 'ITtxtRemarks').disabled = !document.getElementById(chkId).checked;
                document.getElementById(Id + 'ITtxtRemarks').className = document.getElementById(chkId).checked ? 'form-control' : 'form-control'
                if (getElement('hdnDispatchStatus').value == "G" || getElement('hdnDispatchStatus').value == "R" || (getElement('hdnDispatchStatus').value == "A" && getElement('hdnDispatchValue').value != "A")) {
                    if (!document.getElementById(chkId).checked) document.getElementById(Id + 'ITtxtVisaNo').value = '';
                    document.getElementById(Id + 'ITtxtVisaNo').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ITtxtVisaNo').className = document.getElementById(chkId).checked ? 'form-control' : 'form-control'

                    if (!document.getElementById(chkId).checked) document.getElementById(Id + 'ITddlApprove').selectedIndex = 0;
                    document.getElementById(Id + 'ITddlApprove').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ITddlApprove').className = document.getElementById(chkId).checked ? 'inputDdlEnabled' : 'inputDdlDisabled'

                    if (!document.getElementById(chkId).checked) document.getElementById(Id + 'ITdcVisaIssueDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').className = document.getElementById(chkId).checked ? 'inputEnabled form-control' : 'form-control'

                    if (!document.getElementById(chkId).checked) document.getElementById(Id + 'ITdcVisaExpDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaExpDate_Date').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ITdcVisaExpDate_Date').className = document.getElementById(chkId).checked ? 'inputEnabled form-control' : 'form-control'

                    if (!document.getElementById(chkId).checked) document.getElementById(Id + 'ITdcVisaCollDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaCollDate_Date').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ITdcVisaCollDate_Date').className = document.getElementById(chkId).checked ? 'inputEnabled form-control' : 'form-control'
                    statusChanged(document.getElementById(Id + 'ITddlApprove').id);
                }
                else if (getElement('hdnDispatchStatus').value == "A" && getElement('hdnDispatchValue').value == "A") {
                    if (!document.getElementById(chkId).checked) document.getElementById(Id + 'ITtxtTrackingNo').value = '';
                    document.getElementById(Id + 'ITtxtTrackingNo').disabled = !document.getElementById(chkId).checked;
                    document.getElementById(Id + 'ITtxtTrackingNo').className = document.getElementById(chkId).checked ? 'form-control' : 'form-control'
                }
                else if (getElement('hdnDispatchStatus').value == "D" || getElement('hdnDispatchStatus').value == "E" || getElement('hdnDispatchStatus').value == "B") {
                    document.getElementById(Id + 'ITlnkDocuments').disabled = !document.getElementById(chkId).checked;
                }
                else if (getElement('hdnDispatchStatus').value == "C") {
                    document.getElementById(Id + 'ITddlAssignTo').disabled = !document.getElementById(chkId).checked;
                }
            }
            if (getElement('hdnDispatchStatus').value == "G" || getElement('hdnDispatchStatus').value == "R" || (getElement('hdnDispatchStatus').value == "A" && getElement('hdnDispatchValue').value != "A")) {
                if (document.getElementById(Id + 'IThdfApproveStatus').value != "S") {
                    document.getElementById(Id + 'ITddlApprove').value = document.getElementById(Id + 'IThdfApproveStatus').value;
                }
            }
        }
        function statusChanged(changedId) {

            var Id = changedId.substring(0, changedId.lastIndexOf('_') + 1)
            if (document.getElementById(Id + 'ITchkSelect').checked) {
                var changed = document.getElementById(Id + 'ITddlApprove').value;
                if (changed == "A") {
                    document.getElementById(Id + 'ITtxtVisaNo').value = '';
                    document.getElementById(Id + 'ITtxtVisaNo').disabled = false;
                    document.getElementById(Id + 'ITtxtVisaNo').className = 'inputEnabled form-control';

                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').disabled = false;
                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').className = 'inputEnabled form-control';

                    document.getElementById(Id + 'ITdcVisaExpDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaExpDate_Date').disabled = false;
                    document.getElementById(Id + 'ITdcVisaExpDate_Date').className = 'inputEnabled form-control';

                    document.getElementById(Id + 'ITdcVisaCollDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaCollDate_Date').disabled = false
                    document.getElementById(Id + 'ITdcVisaCollDate_Date').className = 'inputEnabled form-control';

                }
                else if (changed == "R") {
                    document.getElementById(Id + 'ITtxtVisaNo').value = '';
                    document.getElementById(Id + 'ITtxtVisaNo').disabled = true;
                    document.getElementById(Id + 'ITtxtVisaNo').className = 'inputDisabled form-control';

                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').disabled = true;
                    document.getElementById(Id + 'ITdcVisaIssueDate_Date').className = 'inputDisabled form-control';

                    document.getElementById(Id + 'ITdcVisaExpDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaExpDate_Date').disabled = true;
                    document.getElementById(Id + 'ITdcVisaExpDate_Date').className = 'inputDisabled form-control';

                    document.getElementById(Id + 'ITdcVisaCollDate_Date').value = '';
                    document.getElementById(Id + 'ITdcVisaCollDate_Date').disabled = true
                    document.getElementById(Id + 'ITdcVisaCollDate_Date').className = 'inputDisabled form-control';

                }
            }
        }
        function setDocumentNoPosition(id) {
            document.getElementById('divDocNo').style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            document.getElementById('divDocNo').style.left = positions[0] + 'px';
            document.getElementById('divDocNo').style.top = (positions[1] + 20) + 'px';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>
