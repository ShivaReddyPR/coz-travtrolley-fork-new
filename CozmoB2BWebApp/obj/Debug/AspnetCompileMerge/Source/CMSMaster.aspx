﻿<%@ Page Title="CMSMaster" EnableEventValidation="false" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CMSMaster" Codebehind="CMSMaster.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        function saveAlert() {
            alert('Saved Successfully');
            navigateToTab();
        }
        function ErrorAlert(msg) {
            alert(msg);
        }
        function updateAlert() {
            alert('Updated Successfully');
            navigateToTab();
        }
        function deleteAlert() {
            alert('Changed Successfully');
                
        }
        function showBannersDiv() {
            alert('Please Add Corresponding Banner Images and Links');
            location.reload();
        }
        function editModeReloadPage() {
            location.reload();
        }


        function navigateToTab() {
           
            var tabId = document.getElementById('<%=hdntabId.ClientID %>').value;
            
            //1-Header Tab
            //2-Home Settings Tab
            //2-F Flight Home Tab
            //2-H Hotel Home Tab
            //2-P Holidays Home Tab
            //2-V Visa Home Tab
            //3-Footer Tab
            //4-Social Media Tab
            //5-Tags Manager Tab
            
            $("li").removeClass('active');
            //alert(tabId);
            switch (tabId) {
                case "1": //1-Header Tab
                    $('a[href="#HeaderTab"]').click();
                    break;
                case "2-F": //Flight
                    $('a[href="#HomeSettings"]').click();
                    $('a[href="#FlightHome"]').click();
                          
                            break;
                        case "2-H": //Hotel
                            
                            $('a[href="#HomeSettings"]').click();
                            $('a[href="#HotelHome"]').click();
                            break;
                        case "2-P": //Packages
                            $('a[href="#HomeSettings"]').click();
                            $('a[href="#HolidaysHome"]').click();
                            break;
                        case "2-V": //Visa
                            $('a[href="#HomeSettings"]').click();
                            $('a[href="#VisaHome"]').click();
                            break;
                        case "3"://Footer Tab
                            $('a[href="#FooterTab"]').click();
                            break;
                        case "4"://Social Media
                            $('a[href="#SocialMedia"]').click();
                            break;
                        case "5"://Tags Manager
                            $('a[href="#TagsManager"]').click();
                            break;
                            
                        default:
                            $('a[href="#HeaderTab"]').click();
                            }

                        }
                        function saveNotification() {
                            alert('Saved Successfully');
                           
                        }
                        function updateNotification() {
                            alert('Updated Successfully');
                            
                        }

        function Validate() {
            var valid = false;
            if (document.getElementById('<%=ddlCountry.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please Select Country!";
            }
            else if (document.getElementById('<%=ddlCulture.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please Select Culture!";
            }
            else {
                valid = true;
            }
            return valid;
        }

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));

            return ret;
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function HeaderValidate() {
            var valid = false;
            document.getElementById('divHDError').style.display = "none";
            if (document.getElementById('<%=txtContactNumber.ClientID %>').value == "") {
                document.getElementById('divHDError').style.display = "block";
                document.getElementById('divHDError').innerHTML = "Please Enter Contact Number";
            }
            else if (document.getElementById('<%=txtCopyRight.ClientID %>').value == "") {
                document.getElementById('divHDError').style.display = "block";
                document.getElementById('divHDError').innerHTML = "Please Enter CopyRight";
            }
            else if (document.getElementById('<%=hdfHeaderImage.ClientID %>').value == "0" && document.getElementById('<%=fuLogo.ClientID %>').files.length === 0) {
                document.getElementById('divHDError').style.display = "block";
                document.getElementById('divHDError').innerHTML = "Logo should be uploaded !";

            }
            else if (document.getElementById('<%=txtLogoDesc.ClientID %>').value == "") {
                document.getElementById('divHDError').style.display = "block";
                document.getElementById('divHDError').innerHTML = "Please Enter Tag Desc";
            }
            
            else {
                valid = true;
            }
            return valid;
        }

      
    </script>
<asp:HiddenField runat="server" ID="hdntabId" />
<asp:HiddenField runat="server" ID="hdfHeaderImage" Value="0" />
    <div class="body_container">
        <h4>
            Page Settings</h4>
        <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>
                        Country <span class="fcol_red">*</span></label>
                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlCountry">
                        <asp:ListItem Selected="True" Value="0" Text="--Select Country--"></asp:ListItem>
                        <asp:ListItem Value="AE" Text="United Arab Emirates"></asp:ListItem>
                        <asp:ListItem Value="SA" Text="Saudi Arabia"></asp:ListItem>
                        <asp:ListItem Value="QA" Text="Qatar"></asp:ListItem>
                        <asp:ListItem Value="KW" Text="Kuwait"></asp:ListItem>
                        <asp:ListItem Value="BH" Text="Bahrain"></asp:ListItem>
                        <asp:ListItem Value="IN" Text="India"></asp:ListItem>
                        <asp:ListItem Value="EZ" Text="EMTAZ"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <%--For Culture Addedby by Harish on 20-Feb-2018 --%>
        <div class="row">
            <div class="col-md-3">
                <label>
                    Culture <span class="fcol_red">*</span>
                </label>
                 <asp:DropDownList CssClass="form-control" runat="server" ID="ddlCulture">
                        <asp:ListItem Selected="True" Value="0" Text="--Select Culture--"></asp:ListItem>
                        <asp:ListItem Value="EN" Text="English"></asp:ListItem>
                        <asp:ListItem Value="AR" Text="Arabic"></asp:ListItem>                       
                    </asp:DropDownList>
            </div>
        </div>
        <div class="CorpTrvl-tabbed-panel" id="BannersDiv" visible="false" runat="server">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li  class="active" role="presentation"><a href="#HeaderTab" aria-controls="HeaderTab"
                    role="tab" data-toggle="tab">Header</a></li>
                <li  role="presentation"><a href="#HomeSettings" aria-controls="HomeSettings" role="tab"
                    data-toggle="tab">Home Settings</a></li>
                <li   role="presentation"><a href="#FooterTab" aria-controls="FooterTab" role="tab"
                    data-toggle="tab">Footer</a></li>
                <li id="liSM" role="presentation"><a href="#SocialMedia" aria-controls="SocialMedia"
                    role="tab" data-toggle="tab">Social Media</a></li>
                <li  role="presentation" style=""><a href="#TagsManager" aria-controls="TagsManager" role="tab"
                    data-toggle="tab">Tags Manager</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content cmsmaster-form">
                <div role="tabpanel" class="tab-pane active" id="HeaderTab">
                
                 <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>
                                <div id="divHDError" style="display: none; color: Red; font-weight: bold; text-align: center;">
                               
                            </div>
                        </div>
                    </div>
                        </div>
                    <div class="row">
                    
                    <asp:Label Visible="false" runat="server" ID="lblHeaderVal"></asp:Label>
                    
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>
                                    Contact Number <span class="fcol_red">*</span></label>
                                <asp:TextBox MaxLength="20" CssClass="form-control" runat="server" ID="txtContactNumber"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ErrorMessage="Required" runat="server" ID="rfvContactNumber"
                                    ControlToValidate="txtContactNumber" ValidationGroup="VGHeaderTab"></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>
                                    Copyright Text <span class="fcol_red">*</span></label>
                                <asp:TextBox MaxLength="250" CssClass="form-control" runat="server" ID="txtCopyRight"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ErrorMessage="Required" runat="server" ID="rfvCopyRight"
                                    ControlToValidate="txtCopyRight" ValidationGroup="VGHeaderTab"></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>
                                            Logo<span style="color: red;">(Recommended size 160px X 42px; max size: 100 kb) </span><span class="fcol_red">*</span></label>
                                        <asp:UpdatePanel ID="UpdatePanelHeaderTab" runat="server" UpdateMode="conditional">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveHeaderTab" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:FileUpload runat="server" ID="fuLogo" AllowMultiple="false" accept="image/*" />
                                                <%--<asp:RequiredFieldValidator ID="rfvLogo" runat="server" ControlToValidate="fuLogo"
                                                    ValidationGroup="VGHeaderTab" ErrorMessage="File Required"></asp:RequiredFieldValidator>--%>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                 <div class="col-md-1">
                            <div class="form-group">
                                <asp:Image ID="ImageHeader" runat="server" Visible="false" Width="100px" Height="60px" />
                            </div>
                        </div>
                                <div class="col-md-6" style="display:none">
                                    <div class="form-group">
                                        <label>
                                            Alt Tag <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtLogoDesc" MaxLength="100" Text="Cozmo Travel"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator runat="server" ID="rfvLogoDesc" ControlToValidate="txtLogoDesc"
                                            ErrorMessage="Required" ValidationGroup="VGHeaderTab"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Menu Settings: a)Show Top Menu
                                    <asp:CheckBox runat="server" ID="chkTopMenu" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    b) Show Footer Menu<asp:CheckBox runat="server" ID="chkFooterMenu" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    c) Show Social Icons<asp:CheckBox runat="server" ID="chkSocialIcons" /></label>
                            </div>
                        </div>
                    </div>
                    <asp:Button CssClass="but_b" runat="server" ID="btnSaveHeaderTab" OnClick="btnSaveHeaderTab_Click"
                        Text="Save Header Details" OnClientClick="return HeaderValidate();" />
                </div>
                <div role="tabpanel" class="tab-pane" id="HomeSettings">
                    <div class="tabbed-panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li  role="presentation" class="active"><a href="#FlightHome" aria-controls="FlightHome"
                                role="tab" data-toggle="tab">Flight Home</a></li>
                            <li  role="presentation"><a href="#HotelHome" aria-controls="HotelHome" role="tab"
                                data-toggle="tab">Hotel Home</a></li>
                            <li  role="presentation"><a href="#HolidaysHome" aria-controls="HolidaysHome" role="tab"
                                data-toggle="tab">Holidays Home</a></li>
                            <li  role="presentation"><a href="#VisaHome" aria-controls="VisaHome" role="tab" data-toggle="tab">
                                Visa Home</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="FlightHome">
                                <div class="row">
                                    <asp:Label runat="server" ID="custValHeroF" ForeColor="Red"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Hero Banner</h3>
                                        <span style="color: red;">(Recommended size 1920px X 600px; max size: 1024 kb (1 mb)) </span>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- Hero Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                               <div class="table-responsive">
                                                      <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvHeroF" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvHeroF_RowCancelingEdit" OnRowDeleting="gvHeroF_RowDeleting"
                                                    OnRowEditing="gvHeroF_RowEditing" OnRowUpdating="gvHeroF_RowUpdating" OnRowCommand="gvHeroF_RowCommand"
                                                    OnRowDataBound="gvHeroF_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnHeroStatusF" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgHeroFPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditHeroF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditHeroF" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateHeroF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlHeroF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuHeroF" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileHeroF" runat="server" ControlToValidate="fuHeroF"
                                                                            ValidationGroup="ValidationHeroF" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddHeroF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroTitleF" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroTitleF" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroTitleF" runat="server" ControlToValidate="txtEditHeroTitleF"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroF" />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILEPATHF" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILENAMEF" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILETYPEF" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroOrderNoF" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroTitleF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroTitleF" runat="server" ControlToValidate="txtHeroTitleF"
                                                                    Text="Required" ValidationGroup="ValidationHeroF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgEffDateF" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditHeroImgEffeF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgEffeFlight" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgExpDateF" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditHeroImgExpF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgExpF" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerURlF" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroBannerURlF" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroBannerURLF" runat="server" ControlToValidate="txtEditHeroBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroF" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroBannerURlF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroBannerURLF" runat="server" ControlToValidate="txtHeroBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationHeroF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerOrderF" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlHeroBannerOrderF">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLHeroBannerF" runat="server" ControlToValidate="ddlHeroBannerOrderF"
                                                                    ValidationGroup="ValidationHeroF" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusHeroF"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateHeroF" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditHeroF" />
                                                                <asp:Button ID="btnCancelHeroF" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditHeroF" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelHeroF" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddHeroF" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationHeroF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvHeroF" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Hero Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>
                                            Promo Slider</h3>
                                        <asp:Label runat="server" ID="custValMidF" Visible="false"></asp:Label>
                                        <span style="color: red;">(Recommended size 250px X 250px; max size: 200 kb) </span>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- Mid Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                     <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvMidF" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvMidF_RowCancelingEdit" OnRowDeleting="gvMidF_RowDeleting"
                                                    OnRowDataBound="gvMidF_RowDataBound" OnRowEditing="gvMidF_RowEditing" OnRowUpdating="gvMidF_RowUpdating"
                                                    OnRowCommand="gvMidF_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnMidStatusF" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgMidFPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditMidF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditMidF" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateMidF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlMidF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuMidF" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileMidF" runat="server" ControlToValidate="fuMidF"
                                                                            ValidationGroup="ValidationMidF" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddMidF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidTitleF" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidTitleF" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidTitleF" runat="server" ControlToValidate="txtEditMidTitleF"
                                                                    Text="Required" ValidationGroup="ValidationEditMidHeroF" />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILEPATHF" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILENAMEF" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILETYPEF" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidOrderNoF" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidTitleF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidTitleF" runat="server" ControlToValidate="txtMidTitleF"
                                                                    Text="Required" ValidationGroup="ValidationMidF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidImgEffDateF" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditMidImgEffeF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcMidImgEffeFlight" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidImgExpDateF" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditMidImgExpF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcMidImgExpF" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidBannerURlF" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidBannerURlF" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidBannerURLF" runat="server" ControlToValidate="txtEditMidBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationEditMidHeroF" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidBannerURlF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidBannerURLF" runat="server" ControlToValidate="txtMidBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationMidF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Content Description">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidContentDescF" runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="500" CssClass="form-control" ID="txtEditMidContentDescF"
                                                                    runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidContentDescF" runat="server" ControlToValidate="txtEditMidContentDescF"
                                                                    Text="Required" ValidationGroup="ValidationEditMidHeroF" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="500" CssClass="form-control" ID="txtMidContentDescF"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidContentDescF" runat="server" ControlToValidate="txtMidContentDescF"
                                                                    Text="Required" ValidationGroup="ValidationMidF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidBannerOrderF" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlMidBannerOrderF">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLMidBannerF" runat="server" ControlToValidate="ddlMidBannerOrderF"
                                                                    ValidationGroup="ValidationMidF" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusMidF"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- For Button Caption(Flight FlexDetails) Addedby by Harish on 20-Feb-2018 --%>
                                                        <asp:TemplateField HeaderText="Button Caption">
                                                            <EditItemTemplate>
                                                                 <asp:TextBox TextMode="MultiLine" MaxLength="100" CssClass="form-control" ID="txtEditMidButtonCaptionF" runat="server"
                                                                    Text='<%# Eval("FLEX_1")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidButtonCaptionF" runat="server" ControlToValidate="txtEditMidButtonCaptionF"
                                                                    Text="Required" ValidationGroup="ValidationEditMidHeroF" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                 <asp:Label Width="80px" ID="lblMidButtonCaptionF" runat="server" Text='<%# Eval("FLEX_1")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="100" CssClass="form-control" ID="txtMidButtonCaptionF"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidButtonCaptionF" runat="server" ControlToValidate="txtMidButtonCaptionF"
                                                                    Text="Required" ValidationGroup="ValidationMidF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateMidF" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditMidHeroF" />
                                                                <asp:Button ID="btnCancelMidF" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditMidF" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelMidF" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddMidF" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationMidF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvMidF" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Mid Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <%-- DEAL OF THE MONTH --%>
                                <div class="row">
                                    <asp:Label runat="server" ID="custValDealF"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Deal of the Month</h3>
                                        <span style="color: red;">(Recommended size 1550px X 550px; max size: 1024 kb (1 mb))</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- Deal Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                     <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvDealF" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvDealF_RowCancelingEdit" OnRowDeleting="gvDealF_RowDeleting"
                                                    OnRowEditing="gvDealF_RowEditing" OnRowUpdating="gvDealF_RowUpdating" OnRowDataBound="gvDealF_RowDataBound"
                                                    OnRowCommand="gvDealF_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnDealStatusF" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgDealFPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditDealF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditDealF" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateDealF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlDealF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuDealF" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileDealF" runat="server" ControlToValidate="fuDealF"
                                                                            ValidationGroup="ValidationDealF" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddDealF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealTitleF" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditDealTitleF" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditDealTitleF" runat="server" ControlToValidate="txtEditDealTitleF"
                                                                    Text="Required" ValidationGroup="ValidationEditDealF" />
                                                                <asp:HiddenField runat="server" ID="hdfDealCONTENTFILEPATHF" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfDealCONTENTFILENAMEF" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfDealCONTENTFILETYPEF" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfDealOrderNoF" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtDealTitleF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvDealTitleF" runat="server" ControlToValidate="txtDealTitleF"
                                                                    Text="Required" ValidationGroup="ValidationDealF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealImgEffDateF" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditDealImgEffeF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcDealImgEffeFlight" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealImgExpDateF" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditDealImgExpF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcDealImgExpF" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealBannerOrderF" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlDealBannerOrderF">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLDealBannerF" runat="server" ControlToValidate="ddlDealBannerOrderF"
                                                                    ValidationGroup="ValidationDealF" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusDealF"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateDealF" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditDealF" />
                                                                <asp:Button ID="btnCancelDealF" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditDealF" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelDealF" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddDealF" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationDealF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvDealF" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Deal Banner Grid-->
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Label runat="server" ID="custValDealsF"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Deals of the Month - Slider</h3>
                                        <span style="color: red;">(Recommended size 150px X 150px; max size: 200 kb)</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- Deals Slider Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvDealsF" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvDealsF_RowCancelingEdit" OnRowDataBound="gvDealsF_RowDataBound"
                                                    OnRowDeleting="gvDealsF_RowDeleting" OnRowEditing="gvDealsF_RowEditing" OnRowUpdating="gvDealsF_RowUpdating"
                                                    OnRowCommand="gvDealsF_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnDealsStatusF" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgDealsFPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditDealsF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditDealsF" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateDealsF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlDealsF">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuDealsF" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileDealsF" runat="server" ControlToValidate="fuDealsF"
                                                                            ValidationGroup="ValidationDealsF" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddDealsF" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealsTitleF" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditDealsTitleF" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditDealsTitleF" runat="server" ControlToValidate="txtEditDealsTitleF"
                                                                    Text="Required" ValidationGroup="ValidationEditDealsF" />
                                                                <asp:HiddenField runat="server" ID="hdfDealsCONTENTFILEPATHF" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfDealsCONTENTFILENAMEF" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfDealsCONTENTFILETYPEF" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfDealsOrderNoF" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtDealsTitleF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvDealsTitleF" runat="server" ControlToValidate="txtDealsTitleF"
                                                                    Text="Required" ValidationGroup="ValidationDealsF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealsImgEffDateF" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditDealsImgEffeF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcDealsImgEffeFlight" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealsImgExpDateF" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditDealsImgExpF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcDealsImgExpF" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealsBannerURlF" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditDealsBannerURlF"
                                                                    runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditDealsBannerURLF" runat="server" ControlToValidate="txtEditDealsBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationEditDealsF" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtDealsBannerURlF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvDealsBannerURLF" runat="server" ControlToValidate="txtDealsBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationDealsF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDealsBannerOrderF" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlDealsBannerOrderF">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLDealsBannerF" runat="server" ControlToValidate="ddlDealsBannerOrderF"
                                                                    ValidationGroup="ValidationDealsF" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusDealsF"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateDealsF" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditDealsF" />
                                                                <asp:Button ID="btnCancelDealsF" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditDealsF" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelDealsF" OnClientClick="return alert('Do you want to delete?')"
                                                                    runat="server" CommandName="Delete" Text="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddDealsF" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationDealsF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvDealF" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Deal Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="row">
                                    <asp:Label runat="server" ID="custValVideoF"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Video
                                        </h3>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- Video Banner Grid -->
                                        <asp:UpdatePanel ID="pnlFVideo" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                  <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvFVideo" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvFVideo_RowCancelingEdit" OnRowDataBound="gvFVideo_RowDataBound"
                                                    OnRowDeleting="gvFVideo_RowDeleting" OnRowEditing="gvFVideo_RowEditing" OnRowUpdating="gvFVideo_RowUpdating"
                                                    OnRowCommand="gvFVideo_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVideoBannerURlF" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditVideoBannerURlF"
                                                                    runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditVideoBannerURLF" runat="server" ControlToValidate="txtEditVideoBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationEditVideoF" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtVideoBannerURlF" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvVideoBannerURLF" runat="server" ControlToValidate="txtVideoBannerURlF"
                                                                    Text="Required" ValidationGroup="ValidationVideoF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnVideoStatusF" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Label ID="lblVideoImgEffDateF" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditVideoImgEffeF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcVideoImgEffeFlight" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVideoImgExpDateF" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdfVideoOrderNoF" Value='<%# Eval("ORDERNO")%>' />
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditVideoImgExpF" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcVideoImgExpF" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVideoBannerOrderF" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlVideoBannerOrderF">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLVideoBannerF" runat="server" ControlToValidate="ddlVideoBannerOrderF"
                                                                    ValidationGroup="ValidationVideoF" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusVideoF"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateVideoF" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditVideoF" />
                                                                <asp:Button ID="btnCancelVideoF" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditVideoF" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelVideoF" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddVideoF" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationVideoF" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvFVideo" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Video Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="HotelHome">
                                <div class="row">
                                    <asp:Label runat="server" ID="custValHeroH"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Hero Banner</h3>
                                        <span style="color: red;">(Recommended size 1920px X 600px; max size: 1024 kb (1 mb))</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!--Hotel: Hero Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvHeroH" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvHeroH_RowCancelingEdit" OnRowDataBound="gvHeroH_RowDataBound"
                                                    OnRowDeleting="gvHeroH_RowDeleting" OnRowEditing="gvHeroH_RowEditing" OnRowUpdating="gvHeroH_RowUpdating"
                                                    OnRowCommand="gvHeroH_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnHeroStatusH" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgHeroHPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditHeroH">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditHeroH" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateHeroH" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlHeroH">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuHeroH" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileHeroH" runat="server" ControlToValidate="fuHeroH"
                                                                            ValidationGroup="ValidationHeroH" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddHeroH" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroTitleH" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroTitleH" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroTitleH" runat="server" ControlToValidate="txtEditHeroTitleH"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroH" />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILEPATHH" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILENAMEH" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILETYPEH" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroOrderNoH" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroTitleH" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroTitleH" runat="server" ControlToValidate="txtHeroTitleH"
                                                                    Text="Required" ValidationGroup="ValidationHeroH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgEffDateH" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditHeroImgEffeH" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgEffeHotel" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgExpDateH" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditHeroImgExpH" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgExpH" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerURlH" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroBannerURlH" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroBannerURLH" runat="server" ControlToValidate="txtEditHeroBannerURlH"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroH" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroBannerURlH" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroBannerURLH" runat="server" ControlToValidate="txtHeroBannerURlH"
                                                                    Text="Required" ValidationGroup="ValidationHeroH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerOrderH" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlHeroBannerOrderH">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLHeroBannerH" runat="server" ControlToValidate="ddlHeroBannerOrderH"
                                                                    ValidationGroup="ValidationHeroH" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusHeroH"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateHeroH" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditHeroH" />
                                                                <asp:Button ID="btnCancelHeroH" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditHeroH" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelHeroH" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddHeroH" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationHeroH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvHeroH" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Hero Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="row">
                                    <asp:Label runat="server" ID="custValMidH"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Hotel Promo Boxes</h3>
                                        <span style="color: red;">(Recommended size 250px x 250px; max size: 200 kb)</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!--Hotel: Hero Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvMidH" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvMidH_RowCancelingEdit" OnRowDeleting="gvMidH_RowDeleting"
                                                    OnRowEditing="gvMidH_RowEditing" OnRowUpdating="gvMidH_RowUpdating" OnRowDataBound="gvMidH_RowDataBound"
                                                    OnRowCommand="gvMidH_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnMidStatusH" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgMidHPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditMidH">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditMidH" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateMidH" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlMidH">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuMidH" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileMidH" runat="server" ControlToValidate="fuMidH"
                                                                            ValidationGroup="ValidationMidH" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddMidH" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidTitleH" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidTitleH" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidTitleH" runat="server" ControlToValidate="txtEditMidTitleH"
                                                                    Text="Required" ValidationGroup="ValidationEditMidH" />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILEPATHH" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILENAMEH" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILETYPEH" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidOrderNoH" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidTitleH" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidTitleH" runat="server" ControlToValidate="txtMidTitleH"
                                                                    Text="Required" ValidationGroup="ValidationMidH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidContentNameH" runat="server" Text='<%# Eval("CONTENTNAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidCONTENTNAMEH"
                                                                    runat="server" Text='<%# Eval("CONTENTNAME")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidCONTENTNAMEH" runat="server" ControlToValidate="txtEditMidCONTENTNAMEH"
                                                                    Text="Required" ValidationGroup="ValidationEditMidH" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidCONTENTNAMEH" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidCONTENTNAMEH" runat="server" ControlToValidate="txtMidCONTENTNAMEH"
                                                                    Text="Required" ValidationGroup="ValidationMidH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidContentDescH" runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="250" CssClass="form-control" ID="txtEditMidCONTENTDESCH"
                                                                    runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidCONTENTDESCH" runat="server" ControlToValidate="txtEditMidCONTENTDESCH"
                                                                    Text="Required" ValidationGroup="ValidationEditMidH" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="250" CssClass="form-control" ID="txtMidCONTENTDESCH"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidCONTENTDESCH" runat="server" ControlToValidate="txtMidCONTENTDESCH"
                                                                    Text="Required" ValidationGroup="ValidationMidH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Value">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidContentValueH" runat="server" Text='<%# Eval("CONTENTVALUE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="10" CssClass="form-control" ID="txtEditMidCONTENTVALUEH"
                                                                    runat="server" Text='<%# Eval("CONTENTVALUE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidCONTENTVALUEH" runat="server" ControlToValidate="txtEditMidCONTENTVALUEH"
                                                                    Text="Required" ValidationGroup="ValidationEditMidH" />
                                                                <asp:CompareValidator ID="cv1" runat="server" ControlToValidate="txtEditMidCONTENTVALUEH"
                                                                    Type="Integer" Operator="DataTypeCheck" ErrorMessage="Value must be an integer!" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="10" CssClass="form-control" ID="txtMidCONTENTVALUEH" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidCONTENTVALUEH" runat="server" ControlToValidate="txtMidCONTENTVALUEH"
                                                                    Text="Required" ValidationGroup="ValidationMidH" />
                                                                <asp:CompareValidator ID="cv" runat="server" ControlToValidate="txtMidCONTENTVALUEH"
                                                                    Type="Integer" Operator="DataTypeCheck" ErrorMessage="Value must be an integer!" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidImgEffDateH" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditMidImgEffeH" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcMidImgEffeHotel" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidImgExpDateH" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditMidImgExpH" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcMidImgExpH" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidBannerURlH" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidBannerURlH" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidBannerURLH" runat="server" ControlToValidate="txtEditMidBannerURlH"
                                                                    Text="Required" ValidationGroup="ValidationEditMidH" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidBannerURlH" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidBannerURLH" runat="server" ControlToValidate="txtMidBannerURlH"
                                                                    Text="Required" ValidationGroup="ValidationMidH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidBannerOrderH" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlMidBannerOrderH">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLMidBannerH" runat="server" ControlToValidate="ddlMidBannerOrderH"
                                                                    ValidationGroup="ValidationMidH" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusMidH"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateMidH" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditMidH" />
                                                                <asp:Button ID="btnCancelMidH" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditMidH" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelMidH" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddMidH" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationMidH" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvMidH" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Hero Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="HolidaysHome">
                                <div class="row">
                                    <asp:Label runat="server" ID="custValHeroP"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Hero Banner</h3>
                                        <span style="color: Red;">Upload Image (Recommended size 1920px x 600px; max size: 1024 kb (1 mb))</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!--Hotel: Hero Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvHeroP" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvHeroH_RowCancelingEdit" OnRowDeleting="gvHeroP_RowDeleting"
                                                    OnRowEditing="gvHeroP_RowEditing" OnRowUpdating="gvHeroP_RowUpdating" OnRowDataBound="gvHeroP_RowDataBound"
                                                    OnRowCommand="gvHeroP_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnHeroStatusP" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgHeroPPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditHeroP">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditHeroP" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateHeroP" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlHeroP">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuHeroP" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileHeroP" runat="server" ControlToValidate="fuHeroP"
                                                                            ValidationGroup="ValidationHeroP" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddHeroP" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroTitleP" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroTitleP" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroTitleP" runat="server" ControlToValidate="txtEditHeroTitleP"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroP" />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILEPATHP" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILENAMEP" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILETYPEP" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroOrderNoP" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroTitleP" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroTitleP" runat="server" ControlToValidate="txtHeroTitleP"
                                                                    Text="Required" ValidationGroup="ValidationHeroP" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgEffDateP" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditHeroImgEffeP" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgEffePkg" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgExpDateP" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditHeroImgExpP" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgExpP" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerURlP" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroBannerURlP" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroBannerURLP" runat="server" ControlToValidate="txtEditHeroBannerURlP"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroP" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroBannerURlP" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroBannerURLP" runat="server" ControlToValidate="txtHeroBannerURlP"
                                                                    Text="Required" ValidationGroup="ValidationHeroP" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerOrderP" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlHeroBannerOrderP">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLHeroBannerP" runat="server" ControlToValidate="ddlHeroBannerOrderP"
                                                                    ValidationGroup="ValidationHeroP" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusHeroP"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateHeroP" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditHeroP" />
                                                                <asp:Button ID="btnCancelHeroP" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditHeroP" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelHeroP" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddHeroP" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationHeroP" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvHeroP" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Hero Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="row">
                                    <asp:Label runat="server" ID="custValVideoP"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Video
                                        </h3>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- Video Banner Grid -->
                                        <asp:UpdatePanel ID="pnlVideoPKG" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvPVideo" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvPVideo_RowCancelingEdit" OnRowDataBound="gvPVideo_RowDataBound"
                                                    OnRowDeleting="gvPVideo_RowDeleting" OnRowEditing="gvPVideo_RowEditing" OnRowUpdating="gvPVideo_RowUpdating"
                                                    OnRowCommand="gvPVideo_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVideoBannerURlP" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditVideoBannerURlP"
                                                                    runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditVideoBannerURLP" runat="server" ControlToValidate="txtEditVideoBannerURlP"
                                                                    Text="Required" ValidationGroup="ValidationEditVideoP" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtVideoBannerURlP" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvVideoBannerURLP" runat="server" ControlToValidate="txtVideoBannerURlP"
                                                                    Text="Required" ValidationGroup="ValidationVideoP" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnVideoStatusP" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Label ID="lblVideoImgEffDateP" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdfVideoOrderNoP" Value='<%# Eval("ORDERNO")%>' />
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditVideoImgEffeP" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcVideoImgEffePKG" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVideoImgExpDateP" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditVideoImgExpP" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcVideoImgExpP" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVideoBannerOrderP" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlVideoBannerOrderP">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLVideoBannerP" runat="server" ControlToValidate="ddlVideoBannerOrderP"
                                                                    ValidationGroup="ValidationVideoP" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusVideoP"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateVideoP" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditVideoP" />
                                                                <asp:Button ID="btnCancelVideoP" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditVideoP" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelVideoP" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddVideoP" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationVideoP" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvPVideo" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Video Grid-->
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="VisaHome">
                                <div class="row">
                                    <asp:Label runat="server" ID="custValHeroV"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Hero Banner</h3>
                                        <span style="color: Red;">(Recommended size 1920px X 600px; max size: 1024 kb (1 MB))</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!--Visa: Hero Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvHeroV" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvHeroV_RowCancelingEdit" OnRowDeleting="gvHeroV_RowDeleting"
                                                    OnRowEditing="gvHeroV_RowEditing" OnRowUpdating="gvHeroV_RowUpdating" OnRowCommand="gvHeroV_RowCommand"
                                                    OnRowDataBound="gvHeroV_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnHeroStatusV" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgHeroVPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditHeroV">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditHeroV" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateHeroV" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlHeroV">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuHeroV" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileHeroV" runat="server" ControlToValidate="fuHeroV"
                                                                            ValidationGroup="ValidationHeroV" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddHeroV" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroTitleV" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroTitleV" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroTitleV" runat="server" ControlToValidate="txtEditHeroTitleV"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroV" />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILEPATHV" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILENAMEV" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroCONTENTFILETYPEV" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfHeroOrderNoV" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroTitleV" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroTitleV" runat="server" ControlToValidate="txtHeroTitleV"
                                                                    Text="Required" ValidationGroup="ValidationHeroV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgEffDateV" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditHeroImgEffeV" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgEffeVisa" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroImgExpDateV" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditHeroImgExpV" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcHeroImgExpV" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerURlV" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditHeroBannerURlV" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditHeroBannerURLV" runat="server" ControlToValidate="txtEditHeroBannerURlV"
                                                                    Text="Required" ValidationGroup="ValidationEditHeroV" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtHeroBannerURlV" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvHeroBannerURLV" runat="server" ControlToValidate="txtHeroBannerURlV"
                                                                    Text="Required" ValidationGroup="ValidationHeroV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHeroBannerOrderV" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlHeroBannerOrderV">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLHeroBannerF" runat="server" ControlToValidate="ddlHeroBannerOrderV"
                                                                    ValidationGroup="ValidationHeroV" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusHeroV"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateHeroV" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditHeroV" />
                                                                <asp:Button ID="btnCancelHeroV" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditHeroV" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelHeroV" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddHeroV" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationHeroV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvHeroV" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Hero Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="row">
                                    <asp:Label runat="server" ID="custValMidV"></asp:Label>
                                    <div class="col-md-12">
                                        <h3>
                                            Visa Promo Boxes</h3>
                                        <span style="color: Red;">(Recommended size 250px X 250px; max size: 200 kb)</span>
                                    </div>
                                    <div class="col-md-12">
                                        <!--Visa: Hero Banner Grid -->
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                <asp:GridView CssClass="table b2b-corp-table" Width="100%" ID="gvMidV" DataKeyNames="DETAILID"
                                                    runat="server" AutoGenerateColumns="false" ShowFooter="true" HeaderStyle-Font-Bold="true"
                                                    OnRowCancelingEdit="gvMidV_RowCancelingEdit" OnRowDeleting="gvMidV_RowDeleting"
                                                    OnRowEditing="gvMidV_RowEditing" OnRowUpdating="gvMidV_RowUpdating" OnRowDataBound="gvMidV_RowDataBound"
                                                    OnRowCommand="gvMidV_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Upload Image">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnMidStatusV" Value='<%# Eval("STATUS")%>' />
                                                                <asp:Image Width="100px" Height="100px" runat="server" ID="imgMidVPath" ImageUrl='<%# Eval("CONTENTFILEPATH")%>' />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlEditMidV">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuEditMidV" AllowMultiple="false" accept="image/*" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnUpdateMidV" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlMidV">
                                                                    <ContentTemplate>
                                                                        <asp:FileUpload runat="server" ID="fuMidV" AllowMultiple="false" accept="image/*" />
                                                                        <asp:RequiredFieldValidator ID="rfvFileMidV" runat="server" ControlToValidate="fuMidV"
                                                                            ValidationGroup="ValidationMidV" ErrorMessage="File Required">
                                                                        </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnAddMidV" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidTitleV" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidTitleV" runat="server"
                                                                    Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidTitleV" runat="server" ControlToValidate="txtEditMidTitleV"
                                                                    Text="Required" ValidationGroup="ValidationEditMidV" />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILEPATHV" Value='<%# Eval("CONTENTFILEPATH")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILENAMEV" Value='<%# Eval("CONTENTFILENAME")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidCONTENTFILETYPEV" Value='<%# Eval("CONTENTFILETYPE")%>' />
                                                                <asp:HiddenField runat="server" ID="hdfMidOrderNoV" Value='<%# Eval("ORDERNO")%>' />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidTitleV" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidTitleV" runat="server" ControlToValidate="txtMidTitleV"
                                                                    Text="Required" ValidationGroup="ValidationMidV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidNameV" runat="server" Text='<%# Eval("CONTENTNAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidNameV" runat="server"
                                                                    Text='<%# Eval("CONTENTNAME")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidNameV" runat="server" ControlToValidate="txtEditMidNameV"
                                                                    Text="Required" ValidationGroup="ValidationEditMidV" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidNameV" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidNameV" runat="server" ControlToValidate="txtMidNameV"
                                                                    Text="Required" ValidationGroup="ValidationMidV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidDescV" runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="500" CssClass="form-control" ID="txtEditMidDescV"
                                                                    runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidDescV" runat="server" ControlToValidate="txtEditMidDescV"
                                                                    Text="Required" ValidationGroup="ValidationEditMidV" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="500" CssClass="form-control" ID="txtMidDescV"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidDescV" runat="server" ControlToValidate="txtMidDescV"
                                                                    Text="Required" ValidationGroup="ValidationMidV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Effective Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidImgEffDateV" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditMidImgEffeV" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcMidImgEffeVisa" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidImgExpDateV" runat="server" Text='<%# CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditMidImgExpV" runat="server"
                                                                    DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <uc1:DateControl ID="dcMidImgExpV" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                                </uc1:DateControl>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidBannerURlV" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMidBannerURlV" runat="server"
                                                                    Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidBannerURLV" runat="server" ControlToValidate="txtEditMidBannerURlV"
                                                                    Text="Required" ValidationGroup="ValidationEditMidV" />
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMidBannerURlV" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidBannerURLV" runat="server" ControlToValidate="txtMidBannerURlV"
                                                                    Text="Required" ValidationGroup="ValidationMidV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMidBannerOrderV" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlMidBannerOrderV">
                                                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvDDLMidBannerF" runat="server" ControlToValidate="ddlMidBannerOrderV"
                                                                    ValidationGroup="ValidationMidV" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblStatusMidV"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                           <%-- For Button Caption(Visa FlexDetails) Addedby by Harish on 20-Feb-2018 --%> 
                                                        <asp:TemplateField HeaderText="Button Caption">
                                                            <EditItemTemplate>
                                                                 <asp:TextBox TextMode="MultiLine" MaxLength="100" CssClass="form-control" ID="txtEditMidButtonCaptionV" runat="server"
                                                                    Text='<%# Eval("FLEX_1")%>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEditMidButtonCaptionV" runat="server" ControlToValidate="txtEditMidButtonCaptionV"
                                                                    Text="Required" ValidationGroup="ValidationEditMidV" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                 <asp:Label Width="80px" ID="lblMidButtonCaptionV" runat="server" Text='<%# Eval("FLEX_1")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox TextMode="MultiLine" MaxLength="100" CssClass="form-control" ID="txtMidButtonCaptionV"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMidButtonCaptionV" runat="server" ControlToValidate="txtMidButtonCaptionV"
                                                                    Text="Required" ValidationGroup="ValidationMidV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Actions">
                                                            <EditItemTemplate>
                                                                <asp:Button ID="btnUpdateMidV" runat="server" CommandName="Update" Text="Update"
                                                                    ValidationGroup="ValidationEditMidV" />
                                                                <asp:Button ID="btnCancelMidV" runat="server" CommandName="Cancel" Text="Cancel" />
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEditMidV" runat="server" CommandName="Edit" Text="Edit" />
                                                                <asp:Button ID="btnDelMidV" runat="server" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddMidV" runat="server" CssClass="btn btn-primary rbpackage-btn"
                                                                    CommandName="AddNew" Text="Add" ValidationGroup="ValidationMidV" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvMidV" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End of Hero Banner Grid-->
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="FooterTab">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                Footer Menu</h3>
                        </div>
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <div class="table-responsive">
                                    <asp:GridView CssClass="table b2b-corp-table" ID="gridViewL" ShowFooter="true" DataKeyNames="LINKID"
                                        runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" OnRowCancelingEdit="gridViewL_RowCancelingEdit"
                                        OnRowDeleting="gridViewL_RowDeleting" OnRowEditing="gridViewL_RowEditing" OnRowUpdating="gridViewL_RowUpdating"
                                        OnRowCommand="gridViewL_RowCommand" OnRowDataBound="gridViewL_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Link Type">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hdnFooterLinksStatusL" Value='<%# Eval("STATUS")%>' />
                                                    <asp:Label ID="lblLinkType" runat="server" Text='<%# Eval("LINKTYPE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlFooterLinkType">
                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Information" Value="Information"></asp:ListItem>
                                                        <asp:ListItem Text="Our Services" Value="OurServices"></asp:ListItem>
                                                        <asp:ListItem Text="Customer Care" Value="CustomerCare"></asp:ListItem>
                                                        <asp:ListItem Text="Resources" Value="resources"></asp:ListItem>
                                                        <asp:ListItem Text="Become an Affiliate" Value="BecomeAnAffiliate"></asp:ListItem>
                                                        <asp:ListItem Text="Affliations" Value="Affliations"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvLinkType" runat="server" ControlToValidate="ddlFooterLinkType"
                                                        ValidationGroup="ValidationL" ErrorMessage="Required" InitialValue="0" Display="Dynamic">

                                                    </asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("ORDERNO")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlFooterCopyRightOrder">
                                                        <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvDDLFooterCopyRightOrder" runat="server" ControlToValidate="ddlFooterCopyRightOrder"
                                                        ValidationGroup="ValidationL" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Menu Item">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMenuItem" runat="server" Text='<%# Eval("LINKNAME")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox MaxLength="50" CssClass="form-control" ID="txtEditMenuItem" runat="server"
                                                        Text='<%# Eval("LINKNAME")%>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvEditMenuItem" runat="server" ControlToValidate="txtEditMenuItem"
                                                        Text="Required" ValidationGroup="ValidationEditL" />
                                                    <asp:HiddenField runat="server" ID="hdfFooterLinksType" Value='<%# Eval("LINKTYPE")%>' />
                                                    <asp:HiddenField runat="server" ID="hdfFooterLinksOrderNo" Value='<%# Eval("ORDERNO")%>' />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox MaxLength="50" CssClass="form-control" ID="txtMenuItem" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvMenuItem" runat="server" ControlToValidate="txtMenuItem"
                                                        Text="Required" ValidationGroup="ValidationL" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMenuURL" runat="server" Text='<%# Eval("LINKURL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtEditMenuURL" runat="server"
                                                        Text='<%# Eval("LINKURL")%>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvEditURL" runat="server" ControlToValidate="txtEditMenuURL"
                                                        Text="Required" ValidationGroup="ValidationEditL" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox MaxLength="250" CssClass="form-control" ID="txtMenuURL" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvMenuURL" runat="server" ControlToValidate="txtMenuURL"
                                                        Text="Required" ValidationGroup="ValidationL" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Effective Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLinkImgEffDate" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="dcEditLinkImageEffective"
                                                        runat="server" DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <uc1:DateControl ID="dcLinkImageEffective" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                    </uc1:DateControl>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLinkImgExpDate" runat="server" Text='<%#CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="dcEditLImageExpiry" runat="server"
                                                        DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <uc1:DateControl ID="dcLImageExpiry" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                    </uc1:DateControl>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblStatusFooterL"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions">
                                                <EditItemTemplate>
                                                    <asp:Button ID="ButtonUpdateL" runat="server" CommandName="Update" Text="Update"
                                                        ValidationGroup="ValidationEditL" />
                                                    <asp:Button ID="ButtonCancelL" runat="server" CommandName="Cancel" Text="Cancel" />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="ButtonEditL" runat="server" CommandName="Edit" Text="Edit" />
                                                    <asp:Button ID="ButtonDeleteL" runat="server" CommandName="Delete" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="ButtonAddL" runat="server"
                                                        CommandName="AddNew" Text="Add" ValidationGroup="ValidationL" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gridViewL" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="SocialMedia">
                    <div class="row">
                        <div class="Col-md-3">
                            <h3>
                                Social Menu
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="smpnlSM" runat="server">
                                <ContentTemplate>
                                    <div class="table-responsive">
                                    <asp:GridView CssClass="table b2b-corp-table" ID="gridViewSM" ShowFooter="true" DataKeyNames="DETAILID"
                                        runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" OnRowCancelingEdit="gridViewSM_RowCancelingEdit"
                                        OnRowDeleting="gridViewSM_RowDeleting" OnRowEditing="gridViewSM_RowEditing" OnRowUpdating="gridViewSM_RowUpdating"
                                        OnRowCommand="gridViewSM_RowCommand" OnRowDataBound="gridViewSM_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="SOCIAL MEDIA">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="smhdnFooterLinksStatusL" Value='<%# Eval("STATUS")%>' />
                                                    <asp:Label ID="smlblLinkType" runat="server" Text='<%# Eval("CONTENTTYPE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="smddlFooterLinkType">
                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="FACEBOOK" Value="SOCIALFB"></asp:ListItem>
                                                        <asp:ListItem Text="TWITTER" Value="SOCIALTW"></asp:ListItem>
                                                        <asp:ListItem Text="INSTAGRAM" Value="SOCIALINST"></asp:ListItem>
                                                        <asp:ListItem Text="LINKED IN" Value="SOCIALLIIN"></asp:ListItem>
                                                        <asp:ListItem Text="YOUTUBE" Value="SOCIALYOU"></asp:ListItem>
                                                        <asp:ListItem Text="PINTEREST" Value="SOCIALPI"></asp:ListItem>
                                                        <asp:ListItem Text="SNAPCHAT" Value="SOCIALSC"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="smrfvLinkType" runat="server" ControlToValidate="smddlFooterLinkType"
                                                        ValidationGroup="ValidationSM" ErrorMessage="Required" InitialValue="0" Display="Dynamic">

                                                    </asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="URL (Eg:<%=Request.Url.Scheme%>://www.cozmotravel.com)">
                                                <ItemTemplate>
                                                    <asp:Label ID="smlblMenuURL" runat="server" Text='<%# Eval("CONTENTURL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox MaxLength="250" CssClass="form-control" ID="smtxtEditMenuURL" runat="server"
                                                        Text='<%# Eval("CONTENTURL")%>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="smrfvEditURL" runat="server" ControlToValidate="smtxtEditMenuURL"
                                                        Text="Required" ValidationGroup="ValidationEditSM" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox MaxLength="250" CssClass="form-control" ID="smtxtMenuURL" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="smrfvMenuURL" runat="server" ControlToValidate="smtxtMenuURL"
                                                        Text="Required" ValidationGroup="ValidationSM" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Effective Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="smlblLinkImgEffDate" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField runat="server" ID="smhdfFooterLinksType" Value='<%# Eval("CONTENTTYPE")%>' />
                                                    <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="smdcEditLinkImageEffective"
                                                        runat="server" DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <uc1:DateControl ID="smdcLinkImageEffective" runat="server" DateFormat="DDMMYYYY"
                                                        DateOnly="True"></uc1:DateControl>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="smlblLinkImgExpDate" runat="server" Text='<%#CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="smdcEditLImageExpiry" runat="server"
                                                        DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <uc1:DateControl ID="smdcLImageExpiry" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                    </uc1:DateControl>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="smlblStatusFooterL"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions">
                                                <EditItemTemplate>
                                                    <asp:Button ID="smButtonUpdateL" runat="server" CommandName="Update" Text="Update"
                                                        ValidationGroup="ValidationEditSM" />
                                                    <asp:Button ID="smButtonCancelL" runat="server" CommandName="Cancel" Text="Cancel" />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="smButtonEditL" runat="server" CommandName="Edit" Text="Edit" />
                                                    <asp:Button ID="smButtonDeleteL" runat="server" CommandName="Delete" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="smButtonAddL" runat="server"
                                                        CommandName="AddNew" Text="Add" ValidationGroup="ValidationSM" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gridViewSM" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="TagsManager">
                    <div class="row">
                        <div class="Col-md-3">
                            <h3>
                                Tag Manager
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="pnlTM" runat="server">
                                <ContentTemplate>
                                    <div class="table-responsive">
                                    <asp:GridView CssClass="table b2b-corp-table" ID="gridViewTM" ShowFooter="true" DataKeyNames="DETAILID"
                                        runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" OnRowCancelingEdit="gridViewTM_RowCancelingEdit"
                                        OnRowDeleting="gridViewTM_RowDeleting" OnRowEditing="gridViewTM_RowEditing" OnRowUpdating="gridViewTM_RowUpdating"
                                        OnRowCommand="gridViewTM_RowCommand" OnRowDataBound="gridViewTM_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="SOCIAL MEDIA">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="tmhdnFooterLinksStatusL" Value='<%# Eval("STATUS")%>' />
                                                    <asp:Label ID="tmlblLinkType" runat="server" Text='<%# Eval("CONTENTTYPE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="tmddlFooterLinkType">
                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="GOOGLETAG" Value="GOOGLETAG"></asp:ListItem>
                                                        <asp:ListItem Text="FACEBOOKTAG" Value="FACEBOOKTAG"></asp:ListItem>
                                                        <asp:ListItem Text="TWITTERTAG" Value="TWITTERTAG"></asp:ListItem>
                                                        <asp:ListItem Text="OTHERTAG" Value="OTHERTAG"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="tmrfvLinkType" runat="server" ControlToValidate="tmddlFooterLinkType"
                                                        ValidationGroup="ValidationTM" ErrorMessage="Required" InitialValue="0" Display="Dynamic">

                                                    </asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            
                                           
                                            <asp:TemplateField HeaderText="Header Tags Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblHeaderTag" runat="server" Text='<%# Eval("CONTENTTITLE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    
                                                    <asp:TextBox MaxLength="250" TextMode="MultiLine" CssClass="form-control" ID="txtEditHeaderTag" runat="server"
                                                        Text='<%# Eval("CONTENTTITLE")%>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="tmrfvEditHeaderTag" runat="server" ControlToValidate="txtEditHeaderTag"
                                                        Text="Required" ValidationGroup="ValidationEditTM" />
                                                </EditItemTemplate>
                                                
                                                <FooterTemplate>
                                                    <asp:TextBox TextMode="MultiLine" CssClass="form-control" ID="txtHeaderTag" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="tmrfvHeaderTag" runat="server" ControlToValidate="txtHeaderTag"
                                                        Text="Required" ValidationGroup="ValidationTM" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                            
                                            <asp:TemplateField HeaderText="Body Tags Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="tmlblMenuURL" runat="server" Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField runat="server" ID="tmhdfFooterLinksType" Value='<%# Eval("CONTENTTYPE")%>' />
                                                    <asp:TextBox TextMode="MultiLine" CssClass="form-control" ID="tmtxtEditMenuURL" runat="server"
                                                        Text='<%# Eval("CONTENTDESCRIPTION")%>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="tmrfvEditURL" runat="server" ControlToValidate="tmtxtEditMenuURL"
                                                        Text="Required" ValidationGroup="ValidationEditTM" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox TextMode="MultiLine" CssClass="form-control" ID="tmtxtMenuURL" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="tmrfvMenuURL" runat="server" ControlToValidate="tmtxtMenuURL"
                                                        Text="Required" ValidationGroup="ValidationTM" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                            
                                            <asp:TemplateField HeaderText="Effective Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="tmlblLinkImgEffDate" runat="server" Text='<%#CZDateFormat(Eval("EFFECTIVEDATE"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <uc1:DateControl Value='<%# Eval("EFFECTIVEDATE")%>' ID="tmdcEditLinkImageEffective"
                                                        runat="server" DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <uc1:DateControl ID="tmdcLinkImageEffective" runat="server" DateFormat="DDMMYYYY"
                                                        DateOnly="True"></uc1:DateControl>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="tmlblLinkImgExpDate" runat="server" Text='<%#CZDateFormat(Eval("EXPIRYDATE"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <uc1:DateControl Value='<%# Eval("EXPIRYDATE")%>' ID="tmdcEditLImageExpiry" runat="server"
                                                        DateFormat="DDMMYYYY" DateOnly="True"></uc1:DateControl>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <uc1:DateControl ID="tmdcLImageExpiry" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                                    </uc1:DateControl>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="tmlblStatusFooterL"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions">
                                                <EditItemTemplate>
                                                    <asp:Button ID="tmButtonUpdateL" runat="server" CommandName="Update" Text="Update"
                                                        ValidationGroup="ValidationEditTM" />
                                                    <asp:Button ID="tmButtonCancelL" runat="server" CommandName="Cancel" Text="Cancel" />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="tmButtonEditL" runat="server" CommandName="Edit" Text="Edit" />
                                                    <asp:Button ID="tmButtonDeleteL" runat="server" CommandName="Delete" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="tmButtonAddL" runat="server"
                                                        CommandName="AddNew" Text="Add" ValidationGroup="ValidationTM" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gridViewTM" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table style="float: right">
            <tr>
                <td>
                    <asp:Button runat="server" ID="btnSave" CssClass="but_b" Text="Load" OnClick="btnSave_Click"
                        OnClientClick="return Validate();" />
                </td>
                <td>
                    <asp:Button Visible="false" runat="server" ID="btnFinalSave" CssClass="but_b" Text="Cancel"
                        OnClick="btnCancel_Click" />
                </td>
                <td>
                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click" Text="Search" runat="server"
                        CssClass="but_b" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:UpdatePanel ID="pnlSearchGrid" runat="server">
        <ContentTemplate>
            <div class="table-responsive">
            <asp:GridView CssClass="table b2b-corp-table  mb-0" Width="100%" ID="gvSearch" DataKeyNames="CMSID"
                runat="server" AutoGenerateColumns="false"  HeaderStyle-Font-Bold="true"
                OnRowDeleting="gvSearch_RowDeleting" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" OnRowDataBound="gvSearch_RowDataBound">
                <Columns>
                <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True" />
                    <asp:TemplateField HeaderText="Country">
                        <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hdnSearchStatus" Value='<%# Eval("STATUS")%>' />
                            <asp:Label ID="lblSearchCountryCode" runat="server" Text='<%# Eval("COUNTRYNAME")%>'></asp:Label>
                        </ItemTemplate>                       
                        
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Culture">
                        <ItemTemplate>
                            <asp:Label ID="lblSearchCultureCode" runat="server" Text='<%# Eval("CULTURE")%>'></asp:Label>
                        </ItemTemplate>                      
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                        
                            <asp:Label ID="lblStatusSearchCMS" runat="server"></asp:Label>
                        </ItemTemplate>
                        
                        
                        
                    </asp:TemplateField>
                    
                    
                    
                    <asp:TemplateField HeaderText="Actions">
                        <ItemTemplate>
                            
                            <asp:Button ID="btnDelSearchCMS" runat="server" CommandName="Delete" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
