﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="viewbookingforReligareInsurance.aspx.cs" Inherits="CozmoB2BWebApp.viewbookingforReligareInsurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <script language="javascript">


        function ShowVoucher() {


            var url = 'PrintReligareInsuranceVoucher.aspx?InsId=<%=insId %>';
            //        var wnd = window.open(url, 'Voucher', 'width:600lpx;height:600px;');
            var wnd = window.open(url, 'Voucher', 'width:600,height:600,toolbar = yes, location = no, directories = no, status = yes, menubar = no, scrollbars = yes, resizable = yes, left = 50, top = 50');


        }

    </script>

    <%--<form id="form1" runat="server">--%>
    <div class="body_container" id="Div1">

        <div class="ns-h3">
            Insurance Header 


            <label class=" pull-right marright_10"><a class="fcol_fff" href="#" onclick="ShowVoucher()">Show Voucher</a> </label>

            <label class=" pull-right marright_10 hidden-xs"><a href="InsuranceQueue.aspx" class="fcol_fff">Insurance Queue</a> </label>

            <div class="clearfix"></div>
        </div>
        <div class=" bor_gray bg_white padding-5 marbot_10">
             <asp:HiddenField ID="hdnpolicyNo" runat="server" />
 




            <%if (religareHeader != null)
                { %>

            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2 col-xs-6">
                    <label>City: </label>
                </div> 
         <div class="col-md-2 col-xs-6"><b><%= religareHeader.ReligarePassengers[0].City%> </b></div> 

                <%--<div class="col-md-2 col-xs-12">
                    <label>ArrivalCity: </label>
                </div>--%>
                <%--                <div class="col-md-2 col-xs-12"><b><%= ArrivalCity%> </b></div>--%>

                <div class="col-md-2 col-xs-12">
                    <label>Booking Status: </label>
                </div>

                <div class="col-md-2 col-xs-12"><b>
                        <%if (religareHeader.Status.ToString() == "1")
                        { %> Confirmed <%}
                        else if (religareHeader.Status.ToString() == "2")
                        { %> Cancelled <%} 
                        else if (religareHeader.Status.ToString() == "3")
                        { %> Error <%} 
                        else if (religareHeader.Status.ToString() == "4")
                        { %> Error <%} 
                        else if (religareHeader.Status.ToString() == "6")
                        { %> PartialCancelled <%} 
                         else if (religareHeader.Status.ToString() == "7")
                        { %> Cancelled <%}
                        else  
                        { %> NA <%}                                             
                                    %>                    
                    </b></div>
                <div class="col-md-2">
                    <label>Product: </label>
                </div>
                <div class="col-md-2">
                                    <b> <%= religareHeader.ProductName %> </b>
                 
                </div>

                <div class="clearfix"></div>
            </div>


            <div class="col-md-12 padding-0 marbot_10">

                <div class="col-md-2">
                    <label>Start Date: </label>
                </div>

                <div class="col-md-2"><b><%=  Convert.ToDateTime(religareHeader.Startdate).ToString("dd-MMM-yyyy") %></b></div>
                <div class="col-md-2">
                    <label>End Date: </label>
                </div>
                <div class="col-md-2"><b><%= Convert.ToDateTime(religareHeader.EndDate).ToString("dd-MMM-yyyy") %> </b></div>

                <div class="col-md-2">
                    <label>Trip Id: </label>
                </div>
                <div class="col-md-2"><b><%= religareHeader.Agency_ref %> </b></div>


                <div class="clearfix"></div>
            </div>



            <div class="col-md-12 padding-0 marbot_10">


                <div class="col-md-2">
                    <label>Policy No: </label>
                </div>
                <div class="col-md-2"><b><%= religareHeader.Policy_no%> </b></div>

                <div class="col-md-2">
                    <label>Total Amount: </label>
                </div>
                <div class="col-md-2"><b><%= religareHeader.Currency%>&nbsp;&nbsp; <%= (Convert.ToDecimal(religareHeader.Premium_Amount+religareHeader.HandlingFee+religareHeader.Markup+religareHeader.GstValue)).ToString("N" + agent.DecimalValue)%> </b></div>
                <div class="col-md-2">
                    <label>Booking Date: </label>
                </div>

                <div class="col-md-2"><b><%= Convert.ToDateTime(religareHeader.Created_on).ToString("dd-MMM-yyyy")%> </b></div>

                <div class="clearfix"></div>
            </div>



            <%--            <div class="col-md-12 padding-0 marbot_10">

                <div class="col-md-2">
                    <label>Adults: </label>
                </div>

                <div class="col-md-2"><b><%= InsuranceHdr.Adults%> </b></div>
                <div class="col-md-2">
                    <label>Childs: </label>
                </div>
                <div class="col-md-2"><b><%= InsuranceHdr.Childs%> </b></div>

                <div class="col-md-2">
                    <label>Infants: </label>
                </div>
                <div class="col-md-2"><b><%= InsuranceHdr.Infants%> </b></div>


                <div class="clearfix"></div>
            </div>--%>


            <div class="col-md-12 padding-0">

                <%--<div class="col-md-2"><label>Premium Charge type: </label> </div>

<div class="col-md-2"> <b><%= InsuranceHdr.Rows[0]["PremiumChargeType"]%> </b></div>
                --%>
                




                <div class="clearfix"></div>
            </div>


            <%} %>

            <div class="clearfix"></div>
        </div>


        <div class="table-responsive">
            <div class="col-md-12 padding-0 marbot_10">
                <% if (religareHeader != null && religareHeader.ReligarePassengers != null && religareHeader.ReligarePassengers.Count > 0)
                    {
                        for (int i = 0; i < religareHeader.ReligarePassengers.Count; i++)
                        {%>
                <div id="accordion" class="panel-group">
                    <div class="panel panel-default">


                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseOne<%=i %>" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle btn-block collapsed" aria-expanded="false">
                                    <span class="glyphicon pull-right font_bold glyphicon-plus"></span>

                                    <%=religareHeader.ReligarePassengers[i].FirstName + " " + religareHeader.ReligarePassengers[i].LastName%>
          
                                </a>
                            </h4>
                        </div>


                        <div class="panel-collapse collapse" id="collapseOne<%=i %>" aria-expanded="false" style="height: 0px;">
                            <div>
                                <div class="col-md-3 col-xs-6">
                                    <b>Pax Type:</b>
                                    <%= (religareHeader.ReligarePassengers[i].Relation =="Proposer"   ? "Proposer":"Insured Passenger" ) %>
                                </div>
                                <div class="col-md-3 col-xs-6"><b>Pax Gender:</b><%=religareHeader.ReligarePassengers[i].Gender.ToString()%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax DOB:</b><%= Convert.ToDateTime(religareHeader.ReligarePassengers[i].Dob).ToString("dd-MMM-yyyy") %></div>
                                <div class="col-md-3 col-xs-6"><b>Pax Email:</b><%=religareHeader.ReligarePassengers[i].Email%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax City:</b><%=religareHeader.ReligarePassengers[i].City%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax State:</b><%=religareHeader.ReligarePassengers[i].State%></div>
                                <div class="col-md-3 col-xs-6"><b>Pax Country:</b>India</div>
                                <div class="col-md-3 col-xs-6"><b>Pax Phone:</b><%=religareHeader.ReligarePassengers[i].Mobileno%></div>
                                <div class="col-md-3 col-xs-6">
                                    <b>Pax Doc Type:</b>                                     
                                    <% if (religareHeader.ReligarePassengers[i].ResidenceProof == "1")
                                        { %>  Govt issued ID  <% } %>
                                    <% else if (religareHeader.ReligarePassengers[i].ResidenceProof == "2")
                                    { %> OCI card   <% } %>
                                    <% else if (religareHeader.ReligarePassengers[i].ResidenceProof == "3")
                                    {%>  Company ID   <% } %> 
                                    <% else 
                                    {   %>  NA  <% } %>
<%--                                    <%=religareHeader.ReligarePassengers[i].ResidenceProof%>--%>
                                </div>
                                <div class="col-md-3 col-xs-6"><b>Pax Doc No:</b>
                                    <%= (religareHeader.ReligarePassengers[i].ProofDetails == "" ? "NA" : religareHeader.ReligarePassengers[i].ProofDetails)  %>
                                    
                                </div>

                            </div>


                            <div class="table table-bordered">
                                <input id="hdnPaxCount" type="hidden" value='<%= religareHeader.ReligarePassengers.Count %>' />
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="display: none; width: 100%" href="#collapseOne<%=i %>" data-parent="#accordion" data-toggle="collapse">
                                                    <span class="glyphicon pull-right font_bold glyphicon-minus"></span>
                                                    <%=religareHeader.ReligarePassengers[i].FirstName + " " + religareHeader.ReligarePassengers[i].LastName%>
                                                </a> 
                                            </h4>
                                        </div>
                                        <div>
                                            <%-- id="collapseOne<%=i %>" class="collapse in"--%>
                                            <table width="100%" border="1" class="table" cellpadding="0" cellspacing="0">
                                                <tr class="font_bold">
                                                    <td>Plan Title</td>
                                                    <td>Plan Code</td>
                                                    <td>Policy No</td>
                                                    <td id="tdPolicy<%=i %>" style="display: none;"></td>
                                                    <td>Policy Link</td>
                                                </tr>
                                                <tr>
                                                    <td><%=religareHeader.ProductName %></td>
                                                    <td><%=religareHeader.Produ_id %></td>
                                                    <td><%=religareHeader.Policy_no %></td>
                                                    <td id="tdViewLink<%=i %>" style="display: block;"><a   class="certificate" href='#'>View Certificate</a></td>

                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <%}
                    }%>

                <div id="divPlan" runat="server">
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">

        $('.collapse').on('shown.bs.collapse', function () {
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function () {
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
        $(document).ready(function () {
            $(".certificate").click(function () {
                var policyNo = $("#ctl00_cphTransaction_hdnpolicyNo").val();
                 $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "viewbookingforReligareInsurance.aspx/GetPolicyPDFData",
                    data: "{'policyNo':'" + policyNo + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "")
                            alert('Statement Not Generated For Given Pnumber');
                        else {
                            var pdfWindow = window.open("");
                            pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + data.d + "'></iframe>");
                        }
                    },
                    error: function (result) { }
                });
            });
        });
    </script>
   
</asp:Content>
