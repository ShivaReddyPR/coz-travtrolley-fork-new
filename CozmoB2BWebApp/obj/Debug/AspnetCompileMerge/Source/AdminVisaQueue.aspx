﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AdminVisaQueue" EnableEventValidation="false"
    ValidateRequest="false" Codebehind="AdminVisaQueue.aspx.cs" %>

<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.BookingEngine.GDS" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.AccountingEngine" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Visa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
 <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <style type="text/css">
        <!
        -- .box_visaquee
        {
            line-height: 0px;
            display: block;
            font-size: 11px;
            -webkit-border-radius: 4px;
            background: #f5f5f5;
            padding: 8px 8px 8px 8px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            width: 942px;
            margin-top: 7px;
            margin-bottom: 7px;
            border: solid 1px #ccc;
            height: 35px;
        }
        -- ></style>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/Queue.js" type="text/javascript"></script>
    
      <script>
          var cal1;
          var cal2;

          function init() {

              //    showReturn();
              var today = new Date();
              // For making dual Calendar use CalendarGroup  for single Month use Calendar     
              cal1 = new YAHOO.widget.Calendar("cal1", "container1");
              //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
              cal1.cfg.setProperty("title", "Select CheckIn date");
              cal1.cfg.setProperty("close", true);
              cal1.selectEvent.subscribe(setDates1);
              cal1.render();

              cal2 = new YAHOO.widget.Calendar("cal2", "container2");
              cal2.cfg.setProperty("title", "Select CheckOut date");
              cal2.selectEvent.subscribe(setDates2);
              cal2.cfg.setProperty("close", true);
              cal2.render();
          }
          function showCal1() {
              $('container2').context.styleSheets[0].display = "none";
              $('container1').context.styleSheets[0].display = "block";
              init();
              cal1.show();
              cal2.hide();
          }


          var departureDate = new Date();
          function showCal2() {
              $('container1').context.styleSheets[0].display = "none";
              cal1.hide();
              init();
              // setting Calender2 min date acoording to calendar1 selected date
              var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
              //var date1=new Date(tempDate.getDate()+1);

              if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                  var depDateArray = date1.split('/');

                  var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                  cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                  cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                  cal2.render();
              }
              document.getElementById('container2').style.display = "block";
          }
          function setDates1() {
              var date1 = cal1.getSelectedDates()[0];

              $('IShimFrame').context.styleSheets[0].display = "none";
              this.today = new Date();
              var thisMonth = this.today.getMonth();
              var thisDay = this.today.getDate();
              var thisYear = this.today.getFullYear();

              var todaydate = new Date(thisYear, thisMonth, thisDay);
              var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
              var difference = (depdate.getTime() - todaydate.getTime());


              departureDate = cal1.getSelectedDates()[0];
              document.getElementById('errMess').style.display = "none";
              document.getElementById('errorMessage').innerHTML = "";
              //			
              var month = date1.getMonth() + 1;
              var day = date1.getDate();

              if (month.toString().length == 1) {
                  month = "0" + month;
              }

              if (day.toString().length == 1) {
                  day = "0" + day;
              }

              document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

              //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
              //cal2.render();

              cal1.hide();

          }
          function setDates2() {
              var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
              if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                  document.getElementById('errMess').style.display = "block";
                  document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                  return false;
              }

              var date2 = cal2.getSelectedDates()[0];

              var depDateArray = date1.split('/');

              // checking if date1 is valid		    
              if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                  document.getElementById('errMess').style.display = "block";
                  document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                  return false;
              }
              document.getElementById('errMess').style.display = "none";
              document.getElementById('errorMessage').innerHTML = "";

              // Note: Date()	for javascript take months from 0 to 11
              var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
              var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
              var difference = returndate.getTime() - depdate.getTime();

              //         if (difference < 1) {
              //             document.getElementById('errMess').style.display = "block";
              //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
              //             return false;
              //         }
              //         if (difference == 0) {
              //             document.getElementById('errMess').style.display = "block";
              //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
              //             return false;
              //         }
              document.getElementById('errMess').style.display = "none";
              document.getElementById('errorMessage').innerHTML = "";

              var month = date2.getMonth() + 1;
              var day = date2.getDate();

              if (month.toString().length == 1) {
                  month = "0" + month;
              }

              if (day.toString().length == 1) {
                  day = "0" + day;
              }

              document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
              cal2.hide();
          }
          YAHOO.util.Event.addListener(window, "load", init);
    </script>

    <script type="text/javascript">
        var Ajax;

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function GetVisaType() {
            var countryCode = document.getElementById('ctl00_cphTransaction_ddlCountry').value;
            if (countryCode == "Select Country") {
                var select = document.getElementById('visaType');
                select.options.length = 0; // clear out existing items 
                select.options.add(new Option("Select", "-1"))
                document.getElementById('visaTypeList').value = "";
            }
            else {
                var paramList = 'searchKey=' + countryCode + '&requestFrom=VisaType';
                var url = "VisaAjax.aspx";
                
                Ajax.onreadystatechange = GetVisaTypeComplete;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }
        }
        
        function GetVisaTypeComplete() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var arrayType = "";
                        document.getElementById('visaTypeList').value = Ajax.responseText;
                        arrayType = Ajax.responseText.split('/');
                        var select = document.getElementById('visaType');
                        select.options.length = 0; // clear out existing items 
                        select.options.add(new Option("Select", "-1"))
                        if (arrayType[0] != "") {
                            for (var i = 0; i < arrayType.length; i++) {
                                var visaType = arrayType[i].split(',');
                                select.options.add(new Option(visaType[0], visaType[1]))
                            }
                        }
                    }
                }
            }
        }
        
        function FillDropDown() {
            var arrayType = "";
            var visaTypeList = document.getElementById('visaTypeList').value;
            if (visaTypeList != "") {
                arrayType = visaTypeList.split('/');
                var select = document.getElementById('visaType');
                select.options.length = 0; // clear out existing items 
                select.options.add(new Option("Select", "-1"))
                if (arrayType[0] != "") {
                    for (var i = 0; i < arrayType.length; i++) {
                        var visaType = arrayType[i].split(',');
                        if (document.getElementById('SelectedVisaType').value != "-1" && visaType[1] == document.getElementById('SelectedVisaType').value) {
                            select.options.add(new Option(visaType[0], visaType[1], true, true))
                        }
                        else {
                            select.options.add(new Option(visaType[0], visaType[1]))
                        }
                    }
                }
            }
        }
        function VisaTypeChange() {
            document.getElementById('SelectedVisaType').value = document.getElementById('visaType').value;

        }
        function ShowPop(ab, div) {
            var i = ab;

        }

        function ShowAgentPopUp(id, parentId, agencyId) {
            blockId = id;
            var url = "QueueAjax.aspx";
            var paramList = 'isReadOnlyProfile=true';
            paramList += '&agencyId=' + agencyId;
            paramList += '&blockId=' + id;
            document.getElementById(blockId).style.display = "block";
            document.getElementById(blockId).innerHTML = "Loading...";
            
            Ajax.onreadystatechange = DisplayAgentProfile;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }


        function ShowPaxPopUp(id, parentId, paxId) {
            blockId = id;
            var url = "QueueAjax.aspx";
            var paramList = 'isReadOnlyPaxProfile=true';
            paramList += '&paxId=' + paxId;
            paramList += '&blockId=' + id;
            document.getElementById(blockId).style.display = "block";
            document.getElementById(blockId).innerHTML = "Loading...";

            
            Ajax.onreadystatechange = DisplayPaxProfile;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }
        var pblockIdp;
        function ViewPaymentInfo(id, visaId) {
            pblockId = id;
            var url = "VisaAjax.aspx";
            var paramList = 'isVisapaymentInfo=true';
            paramList += '&visaId=' + visaId;
            document.getElementById(pblockId).style.display = "block";
            document.getElementById(pblockId).innerHTML = "Loading...";
            
            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById(pblockId).innerHTML = Ajax.responseText;
                    }
                }
            }
        }

        var processingResult = -1;
        var processingBookingId = -1;
        var processingAction = "";
        var processingPNR = "";
        var blockId;
        function UpdateVisaStatus(visaId, ressultNo) {

            var statusId = document.getElementById('updateVisaStatus-' + ressultNo).value;
            if (processingResult < 0) {
                scroll(0, 0);
                processingResult = ressultNo;
                document.getElementById('MessageBox').innerHTML = "Update Visa Status : " + visaId;
                var page = "UpdateVisaStatus.aspx";
                var pars = "visaId=" + visaId;
                pars += "&statusId=" + statusId;
                
                Ajax.onreadystatechange = HandleCancel;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
                document.getElementById('form1').submit();
            }
            else {
                alert("One " + proccessingAction + " already in progress");
            }
        }

        function HandleCancel() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var cancelRes = Ajax.responseText.split("|");
                        document.getElementById('MessageBox').innerHTML = cancelRes[0];
                        processingResult = -1;
                        processingAction = "";
                        processingBookingId = -1;
                    }
                }
            }
        }


        //        function UpdateVisaStatus1(visaId, ressultNo) {

        //            document.getElementById('visaId').value = visaId;
        //            document.getElementById('statusId').value = $('updateVisaStatus-' + ressultNo).value;
        //            document.getElementById('form1').action = "AdminVisaQueue.aspx";
        //            document.getElementById('form1').submit();
        //        }



        function DisplayAgentProfile() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById(blockId).innerHTML = response.responseText;
                    }
                } 
            }
        }

        function DisplayPaxProfile() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById(blockId).innerHTML = response.responseText;
                    }
                }
            }
        }


        function ShowPage(pageNo) {
            document.getElementById('PageNoString').value = pageNo;
            document.forms[0].submit();
        }

        function GotoCreateInvoice(flightId, pnr, agencyId) {
            document.getElementById('flightId').value = flightId;
            document.getElementById('pnr').value = pnr;
            document.getElementById('agencyId').value = agencyId;
            document.getElementById('form2').submit();

        }
        function hideDiv(divId) {
            document.getElementById(divId).style.display = "none";
        }
        function Trim(str) {
            return str.replace(/^\s+|\s+$/g, '');
        }
        function ShowSearchPop1() {
            document.getElementById('SearchPop').style.display = "block";
        }
        function SearchAgent(val) {
            var url = "AgencyListAjax.aspx";
            var paramList;
            var boxvalue = document.getElementById('<% = SearchBox.ClientID %>').value;
            if (val == null) {
                paramList = "boxtext=" + boxvalue + "&forLedger=" + true;
            }
            else {
                paramList = "boxtext=" + boxvalue + "&forLedger=" + true + "&loginAgent=" + val;
            }
            var id = "AgencyList";
            document.getElementById(id).innerHTML = '<div class="orange-font">Loading data...</div>';
            
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            return true;
        }
        function AddInException() {
            var csvstr = '';
            var csvName
            for (j = 0; j < document.forms[0].elements.length; j++) {
                // for all elements in form
                f = document.forms[0].elements[j];
                var idstr = f.id.toString().substr(0, 17);
                if (f.type == 'checkbox' && idstr == 'checkAddException') {
                    if (f.checked) {
                        var arr = f.id.split('-');
                        if (csvstr == '') {
                            csvstr = arr[1];
                            csvName = document.getElementById('agencyName-' + arr[1]).innerHTML;
                        }
                        else {
                            csvstr = csvstr + ',' + arr[1];
                            csvName = csvName + ',' + document.getElementById('agencyName-' + arr[1]).innerHTML;
                        }
                    }
                }
            }
            document.getElementById('AgentName').value = csvstr;
            document.getElementById('AgencyName').value = csvName;
            document.getElementById('AgencyName1').innerHTML = csvName;
            document.getElementById('UnselectAgents').style.display = "block";
            document.getElementById('SearchPop').style.display = "none";
        }
        function hideDiv(divId) {
            document.getElementById(divId).style.display = "none";
        }
        function UnselectAgents() {
            document.getElementById('AgentName').value = "";
            document.getElementById('AgencyName').value = "";
            document.getElementById('AgencyName1').innerHTML = "";
            document.getElementById('UnselectAgents').style.display = "none";
        }
    </script>

    <%--<div id="header">
            <ul>
                <li id="current"><a href="#">Visa Queue</a></li>
            </ul>
        </div>--%>

<iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
<div class="bg_white bor_gray pad_10 paramcon"> 


     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><label class="lblbold12">
                            Transaction Id (Visa)</label> </div>
    <div class="col-md-2"> <input id="VisaId" placeholder="Separate with commas" class="form-control" name="VisaId" type="text" /></div>
   
   
    <div class="col-md-2"><label class="lblbold12">
                            Search By Status</label> </div>
    <div class="col-md-2"><asp:DropDownList class="form-control" name="ddlVisaStatus" ID="ddlVisaStatus"
                                runat="server">
                            </asp:DropDownList> </div>
   

                            
                            
                            
     <div class="col-md-2"> <label class="lblbold12">
                            Restrict By PaxName</label></div>
     
     
     <div class="col-md-2"><input id="Pax" class="form-control" name="Pax" type="text" /> </div>
     
     


<div class="clearfix"></div>
    </div>
    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><label class="lblbold12">
                            Search By Country</label> </div>
    <div class="col-md-2"> <asp:DropDownList class="form-control" ID="ddlCountry" name="ddlCountry"
                                runat="server" onchange="Javascript:GetVisaType()">
                            </asp:DropDownList></div>
                            
                            
    <div class="col-md-2"><label class="lblbold12">
                            Search By Visa Type</label> </div>
                            
                            
     <div class="col-md-2"> 
          <div>
                            <input type="hidden" id="visaTypeList" name="visaTypeList" value="" />
                            <input type="hidden" id="SelectedVisaType" name="SelectedVisaType" value="" />
                            <select class="form-control" name="visaType" id="visaType" onchange="VisaTypeChange()">
                                <option value="-1">Select</option>
                            </select></div>
     
     </div>
     <div class="col-md-2"><label class="lblbold12">
                            Search By Agent</label> </div>
    <div class="col-md-2"> <asp:DropDownList class="form-control" ID="ddlAgent" name="ddlAgent" 
                                runat="server">                               
                            </asp:DropDownList></div>
     
     <div class="col-md-2"> </div>
         <div class="col-md-2"> </div>

<div class="clearfix"></div>
    </div>
    
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; right: 400px; display: none;
            z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; right:100px; display: none;
            z-index: 9999">
        </div>
    </div>
    
    <div class="col-md-12 padding-0 marbot_10">                                      
   
         <div class="col-md-2"><label class="lblbold12">
                            Search By User</label> </div>
    <div class="col-md-2"> <asp:DropDownList class="form-control" ID="ddlUser" 
                                runat="server">                               
                            </asp:DropDownList></div>
 <div class="col-md-2"><label class="lblbold12">
                            From Date</label> </div>
    <div class="col-md-2"> 
    
    <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                            
                            </div>
                            
                            
    <div class="col-md-2"><label class="lblbold12">
                            To Date</label> </div>
                            
                            
     <div class="col-md-2"> 
      <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
     
     </div>
     
     
     <div class="col-md-2"> </div>
         <div class="col-md-2"> </div>

<div class="clearfix"></div>
    </div>
    
    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
    <%--<div class="col-md-4"> 
                            
                            
                            <table> 
                            
                            <tr> 
                            
                            
                            <td> <label class="lblbold12">
                            Restrict to Agent(s)</label></td>
                            <td>    <% if (true)
                       {%>
                        <div>
                            <span><a class="normallink" href="javascript:ShowSearchPop1()">Select Agent(s)</a>
                            
                            
                            </span>
                            <input id="AgentName" name="AgentName" type="hidden" />
                            <div id="AgencyName1" = "AgencyName1">
                            </div>
                            <input type="hidden" id="AgencyName" name="AgencyName" />
                        </div>
                        <div id="UnselectAgents" style="display: none;">
                            <a href="javascript:UnselectAgents()">UnSelect Agent(s)</a>
                        </div>
                        <%} %>
                    </td>
                </tr>
            </table>
        </div>--%>
       <%-- <div class="col-md-4">
            <a class="normallink" href="AdminVisaQueue.aspx">Clear All Filter</a></div>--%>
        <div class="col-md-12">
            <input class="btn but_b pull-right" type="submit" value="Submit" /></div>
        <div class="clearfix">
        </div>
    </div>
    
    <div class="clearfix"></div>
    
    </div>
    
    
    
    
    <div>
      
        <div>
            <input id="PageNoString" name="PageNoString" type="hidden" value="1" />
        </div>
        <% if (listOfVisaBookings.Count == 0)
           {%>
        <div class="fleft italic margin-top-20 font-14" style="margin-left: 90px; margin-top: 50px">
            There is no Pending Booking in the Queue
        </div>
        <%}
           else
           {%>
        <div>
            <span id="MessageBox" class="font-red">
                <% if (message.Length > 0)
                   { %>
                <% = message%>
                <% } %>
            </span>
        </div>
        <%} %>
        <!-- Paging show -->
        <%if (listOfVisaBookings.Count > 0)
      { %>
        <div style="margin-bottom: 10px;">
            <%= show%>
        </div>
        <% } %>
        <!-- paging show END -->
        <div>
            <!--Middle-Left-Parent-starts-->
            <div>
                <%         
                for (int i = 0; i < listOfVisaBookings.Count; i++)
                {
                    DataRow[] dataRow = listOfVisaBookings[i];
                    string dateString = string.Empty;
                    string daystring = string.Empty;
                    string timestring = string.Empty;
                    string visaStatus = string.Empty;
                    //dateString = Util.GetDateString(Util.UTCToIST(Convert.ToDateTime(dataRow[0]["visaBookingDate"])));
                    //daystring = Util.GetDayString(Util.UTCToIST(Convert.ToDateTime(dataRow[0]["visaBookingDate"])));
                    dateString = Util.GetDateString(Convert.ToDateTime(dataRow[0]["visaBookingDate"]));
                    daystring = Util.GetDayString(Convert.ToDateTime(dataRow[0]["visaBookingDate"]));
                    if (daystring == "Today")
                    {
                        TimeSpan tspan = DateTime.UtcNow - Convert.ToDateTime(dataRow[0]["visaBookingDate"].ToString());
                        string time = tspan.Hours.ToString("00") + ":" + tspan.Minutes.ToString("00");
                        //timestring = time + " hours Ago " + Util.GetTimeString(Util.UTCToIST(Convert.ToDateTime(dataRow[0]["visaBookingDate"])));
                        timestring = time + " hours Ago " + Util.GetTimeString(Convert.ToDateTime(dataRow[0]["visaBookingDate"]));
                    }
                    else
                    {
                        //timestring = Util.GetTimeString(Util.UTCToIST(Convert.ToDateTime(dataRow[0]["visaBookingDate"])));
                        timestring = Util.GetTimeString(Convert.ToDateTime(dataRow[0]["visaBookingDate"]));
                    }
                %>
                <div id="Result-<%=i %>" class="" onclick="ShowPop('<%=i %>',this)">
                    <div class="bg_white bor_gray pad_10 marbot_10">
                       
                       
                       
                         
                         
                         
                              <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-5">
 
 
 
 
           <% 
                                string linkClass = string.Empty;
                                string linkOnclick = "void(0)";
                                linkClass = " hand underline";
                                //Add option for memberid
                                linkOnclick = "Javascript:ShowAgentPopUp('AgentProfile-" + i + "','Result-" + i + "'," + dataRow[0]["agencyId"].ToString() + ")";                                
                                    %>
                                    <div>
                                        <%--<span class="<% = linkClass %> heading-suigi" title="Click to see agent profile"
                                    onclick="<% = linkOnclick %>">
                                    <%=dataRow[0]["AgencyName"]%></span>--%>
                                        <span class="heading-suigi">
                                            <%=dataRow[0]["AgencyName"]%></span>
                                    </div>
                                 
                                    <div class="agency_module" id="AgentProfile-<%=i %>" style="height: auto !important;
                                        display: none;">
                                        
                                    </div>
                                 
                                    <div>
                                        <span style="margin-right: 21px;"><b>Booked:</b> </span><span>
                                            <span class="font-14">
                                                <% = daystring%>
                                            </span>(<% = dateString%>) <span>
                                                <% = timestring%>
                                            </span></span>
                                        <br />
                                        <span style="margin-right: 0px;"><b>Type of Visa:</b> </span><span>
                                            <% = dataRow[0]["visaType"]%>
                                        </span>
                                        <br />
                                        <% int reundableFeefor = 1;
                                   for (int count = 1; count < dataRow.Length; count++)
                                   {
                                       //if (dataRow[count]["paxLastName"].ToString().ToLower() != dataRow[0]["paxLastName"].ToString().ToLower())Commented by shiva to calculate refund fee for all pax
                                       {
                                           reundableFeefor++;
                                       }

                                   }
                  
                                        %>
                                        <div>
                                           
                                         <b>Price:</b><b>
                                                <% = String.Format("{0:0.00}", ((decimal)dataRow[0]["visaFee"] * dataRow.Length) + ((decimal)dataRow[0]["visaRefundableFee"] * reundableFeefor))%>
                                                <%=dataRow[0]["currencyCode"]%> </b>
                                        </div>
                                       <%-- <%if (Convert.ToInt32(dataRow[0]["visaStatus"]) != (int)VisaStatus.Submitted && Convert.ToInt32(dataRow[0]["visaStatus"]) != (int)VisaStatus.NotEligible && Convert.ToInt32(dataRow[0]["visaStatus"]) != (int)VisaStatus.Eligible && Convert.ToInt32(dataRow[0]["visaStatus"]) != (int)VisaStatus.PartiallyEligible && Convert.ToInt32(dataRow[0]["visaStatus"]) != (int)VisaStatus.RequestChange && Convert.ToInt32(dataRow[0]["visaStatus"]) != (int)VisaStatus.Cancelled)--%>
                                        <%if (Convert.ToInt32(dataRow[0]["visaStatus"]) == (int)VisaStatus.InProcess ||  Convert.ToInt32(dataRow[0]["visaStatus"]) == (int)VisaStatus.Approved || Convert.ToInt32(dataRow[0]["visaStatus"]) == (int)VisaStatus.InProcess_M)
                                      { %>
                                       
                                       
                                        <div>
                                           
                                            <a class="suigi-link" href="javascript:ViewPaymentInfo('PaymentInfo-<%=dataRow[0]["VisaId"]%>','<%=dataRow[0]["VisaId"]%>')">
                                                Payment Information</a>
                                        </div>
                                        
                                        
                                       <div class="visa_PaymentInfo_pop" id="PaymentInfo-<%=dataRow[0]["VisaId"]%>" 
                                        style="height: auto !important; left:40px; position:absolute; z-index:9999; 
                                            display: none;">  </div>
                                        
                                        
                                        <%} %>
                                    </div>
                                      <%--------------------- Start Queue History  ----------------------%>
     <% string visaId = dataRow[0]["VisaId"].ToString(); %>
      <div class="text-success" id="BHContainer" onclick="DisplayBookingHistory(<% = visaId %>)" style="cursor:pointer;">
                                    Queue History
                                </div>
                                <div class="fleft margin-top-20" id="QueueHistory-<%=visaId%>" style="width: 100%; background: #fff; display: none; position: static; left: 300px; top: 450px; border: 0px outset #ccc;">
                                </div>
                                <%--------------------- End Queue History ----------------------%>
                                    
 
 
  </div>
 
 
    <div class="col-md-3"> 
    
    
    
    <div> 
    
     <label class="heading-suigi-normal">
                                        <% = (Visa.VisaStatus)Enum.Parse(typeof(Visa.VisaStatus), dataRow[0]["visaStatus"].ToString())%>
                                    </label>
                                    <br />
                                    <br />
                                    <div class="fleft right-block font-11">
                                        Visa Country :<label style="font-weight: bold"><%=dataRow[0]["visaCountry"]%></label>
                                    </div>
    
    </div>
    
    
    </div>
    
    
    
    
    <div class="col-md-3"> 
    
    <div>
                                        <label class="heading-suigi-normal">
                                            VISA<%= Convert.ToDateTime(dataRow[0][4]).ToString("yy") %><%= Convert.ToDateTime(dataRow[0][4]).ToString("MM") %><% = dataRow[0]["VisaId"]%>
                                        </label>
                                    </div>
                                    <%
                      
                                for (int k = 0; k < dataRow.Length; k++)
                                {

                                    linkOnclick = "Javascript:ShowPaxPopUp('PaxProfile-" + i + "','Result-" + i + "'," + dataRow[k]["paxId"].ToString() + ")";                                
                                    %>
                                    <div>
                                        <%--<label class="suigi-link">    
                                <span class="<% = linkClass %> suigi-link" title="Click to see pax profile"
                                    onclick="<% = linkOnclick %>">
                                    <%=dataRow[k]["paxFirstName"]%>
                                    <%=dataRow[k]["paxLastName"]%></span>
                                    
                                     </label>--%>
                                        <label class="suigi-link">
                                            <span class="suigi-link">
                                                <%=dataRow[k]["paxFirstName"]%>
                                                <%=dataRow[k]["paxLastName"]%></span>
                                        </label>
                                    </div>
                                    <div id="PaxProfile-<%=i %>" style="height: auto !important; display: none;">
                                    </div>
                                    <br />
                                    <%} %>
    
    
    
    </div>
    
    
    
     <div class="col-md-1"> 
      <div>
                                        <span class="fleft" style="display: none">Status:
                                            <select name="updateVisaStatus-<% = i %>" id="updateVisaStatus-<% = i %>">
                                                <option value="-1">Select</option>
                                                <% if (dataRow[0]["visaStatus"].ToString() == "1")
                                   {%>
                                                <option value="5">Eligible</option>
                                                <option value="6">Not Eligible</option>
                                                <% }
                                   else if (dataRow[0]["visaStatus"].ToString() == "4")
                                   {%>
                                                <option value="2">Approved</option>
                                                <option value="3">Rejected</option>
                                                <%}%>
                                            </select>
                                        </span><span class="fleft" style="display: none"><a href="Javascript:UpdateVisaStatus('<% = dataRow[0]["VisaId"] %>', <% = i %>)">
                                            Update</a> </span><span class="fleft">
                                                <input class="btn but_b pull-right" id="Open-<% = i %>" onclick="location.href='ViewBookingForVisa.aspx?visaId=<% =dataRow[0]["VisaId"] %>'"
                                                    type="button" value="Open" style="width:70px;" />
                                            </span>
                                        <input type="hidden" id="hdVisaId" name="hdVisaId" value="" />
                                        <input type="hidden" id="statusId" name="statusId" value="" />
                                    </div>
     
     
     </div>


<div class="clearfix"></div>
    </div>
    
    
                         
                         
                     
                        
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <%  }%>
                <input id="ResultCount" type="Hidden" value="<%= listOfVisaBookings.Count%>" />
            </div>
            <!--Middle-Left-Parent-Ends-->
            <!--Middle-Right-Parent-Starts-->
        </div>
        <div class="clear">
        </div>
    </div>
    <div id="SearchPop" class="light-grey-bg border-y padding-bottom-10 search-popup-parent"
        style="display: none; width: 340px; position: absolute; left: 695px; top: 150px;">
        <div class="fleft border-bottom-black padding-5 search-popup-child" style="width: 310px;">
            <span class="hand fright margin-left-5" onclick="hideDiv('SearchPop')">
                <img alt="close" src="Images/close.gif" />
            </span>
            <div class="margin-top-5">
                <span class="font-16 fleft margin-top-2 width-70">Search</span> <span class="margin-left-5 fleft margin-top-2 width-200">
                    <span class="fleft">
                        <asp:TextBox ID="SearchBox" runat="server" CssClass="text" Text="" Width="115px"></asp:TextBox></span>
                    <span class="fleft margin-left-5">
                        <input type="button" class="fleft" name="AgentSearch" id="AgentSearch" value="Search"
                            onclick="javascript:SearchAgent();" /></span> </span>
            </div>
        </div>
        <div id="AgencyList" class="fleft width-300 margin-top-5 padding-5" style="height: 130px;
            overflow: auto;">
            <!-- on ajax page -->
        </div>
        <div class="fleft width-100" style="border-top: solid 2px black;">
            <span class="fleft center width-100 margin-top-5">
                <input id="AddSelected" onclick="AddInException()" type="button" value="AddSelected" /></span>
        </div>
    </div>
    <div style="color: Red;">
        Note : Showing recent
        <%=queueCount%>
        bookings only.To view the detail of a specific booking, please enter VisaId or Visa
        Number in the filtration criteria.
    </div>
    <%if (Request["VisaId"] != null && Request["VisaId"] != string.Empty)
      {
    %>

    <script type="text/javascript">
             document.getElementById('VisaId').value ='<%=Request["VisaId"].Replace("'","\\'")%>'            
    </script>

    <%} %>
    <% if (Request["AgentName"] != null && Request["AgentName"].Length > 0)
       { %>

    <script type="text/javascript">
             document.getElementById('AgentName').value = '<%=Request["AgentName"].Replace("'","\\'")%>';
             document.getElementById('AgencyName1').innerHTML = '<%=Request["AgencyName"].Replace("'","\\'")%>';
             document.getElementById('AgencyName').value = '<%=Request["AgencyName"].Replace("'","\\'")%>'; 
             document.getElementById('UnselectAgents').style.display = "block";            
    </script>

    <% } %>
    <%if (Request["visaType"] != null && Request["visaType"] != string.Empty)
      {
    %>

    <script type="text/javascript">
        document.getElementById('visaType').value = '<%=Request["visaType"]%>'            
    </script>

    <%} %>
    <% if (Request["Pax"] != null && Request["Pax"].Length > 0)
       { %>

    <script type="text/javascript">
             document.getElementById('Pax').value = '<%=Request["Pax"].Replace("'","\\'")%>';
    </script>

    <% } %>
    <%if (Request["visaStatus"] != null && Request["visaStatus"] != string.Empty)
      {
    %>

    <script type="text/javascript">
        document.getElementById('visaStatus').value = '<%=Request["visaStatus"]%>'            
    </script>

    <%} %>
    <form id="form2" action="createinvoice.aspx" method="post">
    <input type="hidden" id="agencyId" name="agencyId" value="" />
    <input type="hidden" id="pnr" name="pnr" value="" />
    <input type="hidden" id="fromAgent" name="fromAgent" value="true" />
    <input type="hidden" id="bookingId" name="bookingId" value="" />
    </form>
    <%if (Request["visaTypeList"] != null)
     {%>

    <script type="text/javascript">
        document.getElementById('SelectedVisaType').value = '<%=Request["SelectedVisaType"] %>'
        document.getElementById('visaTypeList').value = '<%=Request["visaTypeList"] %>'
        FillDropDown();
    
    </script>

    <%} %>
</asp:Content>
