﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OfflineEntryPrintReceipt.aspx.cs" Inherits="OfflineEntryPrintReceiptUI"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html ><head runat="server" >
    <meta http-equiv="Content-Type"  content="text/html; charset=UTF-8" />
    
    <title>Print Visa Sales Report</title>
    <style type="text/css">
.receipt-wrapper {
	font-family: Arial, Helvetica, sans-serif;
	color: #373737;
	font-size: 12px;
	line-height: normal;
	border: 1px solid #cacbcb;
	padding: 10px;
	width: 650px;
	margin: 20px auto 0 auto;
	position: relative;
	-webkit-print-color-adjust: exact;
}
.receipt-wrapper .receipt-outer-text {
	position: absolute;
	top: -15px;
	font-size: 10px;
	left: 0;
	color: #cacbcb;
}
.receipt-wrapper .bottom-text {
	color: #cacbcb;
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 11px;
}
.receipt-wrapper .bottom-text .link {
	color: #6b6b6b;
}
.receipt-header {
/*		border-bottom: 1px solid #efefef;*/
}
.receipt-header .receipt {
	text-align: center;
	font-weight: bold;
	line-height: 20px;
}
.receipt-header .date {
	text-align: right;
	color: #6b6b6b;
	font-style: italic;
	font-size: 11px;
	padding: 0 5px;
	line-height: 18px;
}
.receipt-general-info {
	padding-top: 10px;
	padding-bottom: 10px;
}
.receipt-general-info td {
	padding: 3px 10px 1px 10px;
	width: 50%;
	border-bottom: 1px solid #e6e6e6;
	border-right: 1px solid #e6e6e6;
	background-color: #f5f5f5;
	border: 1px solid #fff;
	box-shadow: inset 0 0 0 1000px #f5f5f5;
}
.receipt-general-info td[colspan="2"] {
	width: 100%;
}
.receipt-general-info tr:first-child td {
	border-top: 0;
}
.receipt-general-info .text-left, .receipt-general-info .bold-text {
	/*
		width: 50%;
    	float: left;
		display: block;
*/
	display: inline-block;
}
.receipt-general-info .text-left {
	color: #6b6b6b;
	width: 40%;
	float: left;
}
.receipt-general-info td[colspan="2"] .text-left {
	width: 125px;
}
.receipt-general-info .bold-text {
	font-weight: bold;
	vertical-align: top;
	float: left;
	width: 60%;
}
.receipt-general-info td[colspan="2"] .bold-text {
	width: auto;
}
.receipt-visitor-info {
	border: 1px solid #757575;
	margin-bottom: 0;
}
.receipt-visitor-info th {
	background-color: #757575;
	padding: 7px 10px;
	/*		border-right: 1px solid #757575;*/
	text-align: left;
	color: #fff;
	box-shadow: inset 0 0 0 1000px #757575;
}
.receipt-visitor-info td {
	padding: 4px 10px;
	border-top: 1px solid #cecece;
}
.receipt-fee-info {
}
.receipt-fee-info td {
	padding: 4px 8px;
	border-bottom: 1px solid #e6e6e6;
	border-right: 1px solid #e6e6e6;
	/*		background-color:#f3f3f3;*/
/*		box-shadow: inset 0 0 0 1000px #f3f3f3;	*/
	border: 1px solid #fff;
}
.receipt-fee-info td:first-child {
}
.receipt-fee-info td:last-child {
	background-color: #e6e6e6;
	box-shadow: inset 0 0 0 1000px #e6e6e6;
	text-align: right;
}
.receipt-fee-info .last td {
	/*border-bottom:0;*/
	font-weight: bold;
	background-color: #d0d0d0;
	box-shadow: inset 0 0 0 1000px #d0d0d0;
}
.receipt-fee-info tr:last-child td:last-child {
/*		border-bottom:1px solid #757575;*/
/*		border-bottom:2px solid #757575;*/
}

@media print {
	.receipt-wrapper {
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
	}
	.receipt-general-info td[colspan="2"] .text-left {
		width: 128px;
	}
	.receipt-visitor-info th {
		color: #fff;
		text-shadow: 0 0 0 #cfffcc;
	}
	 @media print and (-webkit-min-device-pixel-ratio:0) {
		 .receipt-visitor-info th {
		 color: #fff;
		 -webkit-print-color-adjust: exact;
		}
	}
}
</style>
    <script type="text/javascript" language="javascript">
    //    function print()
    //    {
    //        document.getElementById('Print').style.display="none";
    //	    window.print();	 
    //	    setTimeout('showButtons()',100000);
    //    }
    //    function showButtons()
    //    {
    //        document.getElementById('Print').style.display="block";	    
    //    }
    function printPage() {
        document.getElementById('btnPrint').style.display = "none";
        window.print();
        //alert('2');
        setTimeout('showButtons()', 1000);
        //alert('3');
    }
    function showButtons() {
        document.getElementById('btnPrint').style.display = "block";
    }
    </script>

</head>

<body data-gr-c-s-loaded="true">
    <form  id="form1" runat="server">

        <table id="tblCustomerCopy" width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-wrapper">
	<tbody><tr>
		<td>
            
        </td>
	</tr>
	<tr>
		<td>
            <div id="divPrintButton" style="text-align: left;">
                <input style="width: 90px;" id="btnPrint" onclick="return printPage();" class="button" type="button" value="Print / طباعة">
            </div>
        </td>
	</tr>
	<tr>
		<td><!--Header-->        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-header">
          <tbody>
            <tr>
              <td>
                  <asp:label runat="server" id="Label1" class="receipt-outer-text">Customer Copy / نسخة العميل</asp:label>
                  <img src="./Print Visa Sales Report_files/logo.gif" alt="" width="130px"></td>
              <td class="receipt">
                  <div style="font-size:16px">
                      <asp:label runat="server" id="lblReceiptHeader">Tax Invoice / فاتورة ضريبية </asp:label>
                  </div>
                <div id="divVATReg" style="font-size: 11px;color: #6b6b6b;font-weight: normal;"><span id="lblVatReg">TRN :</span><span id="lblVATRegValue">TRN 100019554300003</span></div></td>
              <td class="date">  <asp:label runat="server" id="lblDateValue"></asp:label> <br>
                </td>
            </tr>
          </tbody>
        </table>
      </td>
	</tr>
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-general-info">
          <tbody>
            <tr>
              <td>
                    <asp:label runat="server" id="lblPNRNo" class="text-left">PNR Number <br> رقم PNR</asp:label>
                     <asp:label runat="server" id="lblPNRNoValue" class="bold-text"></asp:label>
              </td>
              <td>
                  <asp:label runat="server" id="lblTicketNo" class="text-left">Ticket Number<br> رقم التذكرة : </asp:label>
                  <asp:label runat="server" id="lblTicketNoValue" class="bold-text"></asp:label>
              </td>
            </tr>
            
            <tr>
              <td>
                  <asp:label runat="server" id="lblTravelDate" class="text-left">Travel Date <br> موعد السفر :</asp:label>
                  <asp:label runat="server" id="lblTravelDateValue" class="bold-text"></asp:label>
              </td>
              
              <td>
                 <asp:label runat="server" id="lblSectors" class="text-left">Sector(s) <br> القطاعات :</asp:label>
                  <asp:label runat="server" id="lblSectorsValue" class="bold-text"></asp:label>
              </td>
                
            </tr>
            
     
            
            
            <tr>
              <td colspan="2">              
                <asp:label runat="server" id="lblRemarks" class="text-left">Remarks <br> ملاحظات :</asp:label>
                <asp:label runat="server" id="lblRemarksValue" class="bold-text">efefefeffe.</asp:label>    
              </td>
            </tr>
        
          </tbody>
        </table></td>
	</tr>
	<tr>
		<td>
    <div>
			<table class="receipt-visitor-info" cellspacing="0" cellpadding="0" border="0" id="tblPaxDetails" style="width:100%;border-collapse:collapse;">
				<tbody><tr>
					<th scope="col">
                        <label> Title / اسم                                            
                        </label>
                    </th>
                    <th scope="col">
                        <label>Name / عنوان
                        </label>
                    </th>
				</tr>
                
                <tr>
					<td>
                        <asp:Label runat="server" id="lblPaxTitleValue" ></asp:Label>                        
                    </td>
                    <td>
                        <asp:Label runat="server" id="lblPaxNameValue" ></asp:Label>
                    </td>
				</tr>
               
			</tbody></table>
		</div>
        </td>
	</tr>
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="" class="receipt-fee-info">
          <tbody>
            <tr>
              <td rowspan="6" style="text-align:left; vertical-align: bottom">
               
               
               
                <div id="" class="text-left" style="color: #6b6b6b;"><asp:label runat="server" id="lblCollectionMode" class="text-left" style="color: #6b6b6b;">Settlement Mode / وضع التسوية :</asp:label></div>
                <div id="" class="bold-text"><strong><asp:label runat="server" id="lblCollectionModeValue" class="bold-text"> </asp:label></strong></div>
               
               
               
                </td>
                <td></td>
              <td><asp:label runat="server" id="lblFare" class="text-left">Fare / أجرة  :</asp:label></td>
              <td><asp:label runat="server" id="lblFareValue" class="bold-text"></asp:label></td> 
            </tr>

            <tr>
                <td></td>
              <td><asp:label runat="server" id="lblHandlingFee" class="text-left">Handling Fee / رسوم المعالجة  :</asp:label></td>
              <td><asp:label runat="server" id="lblHandlingFeeValue" class="bold-text">0.00</asp:label></td>     
            </tr>
         
            <tr>
                <td></td>
              <td style="border-bottom: 1px solid #e6e6e6;"><strong><asp:label runat="server" id="lblTotCost" class="text-left">Total  / مجموع :</asp:label></strong></td>
              <td style="border-bottom: 1px solid #e6e6e6;"><strong><asp:label runat="server" id="lblTotCostValue" class="bold-text"></asp:label></strong></td>
            </tr>
            
            
            
            
            <tr>
              <td></td>
              <td> VAT on Service Charge at 5.00 / ضريبة القيمة المضافة على رسوم الخدمة الساعة 5.00 : </td>
              <td bgcolor="#D0D0D0"><asp:label runat="server" id="lblVATValue" class="bold-text"></asp:label></td>
            </tr>
            <tr class="last">
              <td style="border-right-color:#d0d0d0"></td>
              <td style="border-left-color:#d0d0d0"> <span id="lblToCollect" class="text-left">Grand Total / المجموع الكلي  :</span><br>
                <asp:label runat="server"  style="font-size: 11px;font-weight: normal;color: #797777;display:none">
                  (Rounding Adjustment. : [<span id="lblRoundAdjust" class="bold-text">0.00</span>])
                 </asp:label>                
                </td>
              <td><asp:label runat="server"  id="lblToCollectValue" class="bold-text"></asp:label></td>
            </tr>

        

         


          </tbody>
        </table></td>
	</tr>

	<tr>
		<td class="bottom-text">
                    
                </td>
	</tr>
	<tr>
		<td class="bottom-text"> 
                    <asp:label runat="server"  id="lblTicketedBy" class="text-left">Issued by:</asp:label>
                    <asp:label runat="server"  id="lblTicketedByValue" class="bold-text"></asp:label> 
                    
                </td>
	</tr>
</tbody></table>
      
    </form>


</body>

</html>
