﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ForGotPassword" Codebehind="ForGotPassword.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
   <div class="center" style="width: 400px; margin-top: 70px; padding-top: 20px; padding-bottom: 20px;
            background: #f6fff6; border: solid 1px #888888;margin-left:150px">
            <div style="width: 390px; margin-bottom: 10px; font-size: large;">
                <asp:Label ID="ResetPasswordMsg" runat="server" Text="Enter your new password here"></asp:Label>
            </div>
            <asp:Panel ID="ResetPasswordPanel" runat="server" Height="109px" Width="397px">
                <asp:CompareValidator ID="ComparePassword" runat="server" ControlToCompare="NewPassword"
                    ControlToValidate="ConfirmPassword" Display="Dynamic" EnableClientScript="True">Password entries did not match</asp:CompareValidator>
                <br />
                <asp:RegularExpressionValidator ID="PasswordLength" runat="server" ControlToValidate="NewPassword"
                    Display="Dynamic" EnableClientScript="true" ErrorMessage="Password should be between 5 to 15 characters"
                    ValidationExpression=".{5,15}"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="PasswordRequiredValidator" runat="server" ControlToValidate="NewPassword"
                    Display="Dynamic" ErrorMessage="Password can not be left blank"></asp:RequiredFieldValidator>
                <div style="width: 390px; margin-top: 10px;">
                    <div style="width: 180px; height: 30px; line-height: 28px; float: left; text-align: right;
                        margin-right: 10px;">
                        <asp:Label ID="LabelNewPassword" runat="server" Text="New Password :   "></asp:Label>
                    </div>
                    <div style="width: 190px; height: 30px; float: left; text-align: left;">
                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div style="width: 390px; margin-top: 10px">
                    <div style="width: 180px; height: 30px; line-height: 28px; float: left; text-align: right;
                        margin-right: 10px;">
                        <asp:Label ID="LabelConfirmPassword" runat="server" Text="Confirm Password : "></asp:Label>
                    </div>
                    <div style="width: 190px; height: 30px; float: left; text-align: left;">
                        <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                    <div style="margin-left:240px;">
                    <asp:Button ID="ResetPasswordButton" runat="server" OnClick="ResetPasswordButton_Click"
                        Text=" Reset" />
                        </div>
                    <div class="fleft">
                    <asp:HiddenField ID="requestId" runat="server" />
                    
                </div>
                </div>
                
            </asp:Panel>
        </div>
    </form>
</body>
</html>
