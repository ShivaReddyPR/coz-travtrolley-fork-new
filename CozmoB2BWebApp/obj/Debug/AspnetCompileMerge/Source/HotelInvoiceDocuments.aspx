﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelInvoiceDocuments.aspx.cs" Inherits="CozmoB2BWebApp.HotelInvoiceDocuments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Invoice Documents</title>
    <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <link href="css/Default.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Core CSS -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    
    <script src="Scripts/bootstrap.min.js"></script>
<style> h3 { padding: 20px 0px 15px 0px!important; }</style>


    <!-- manual css -->
      <link href="css/override.css" rel="stylesheet" />
    <%----------------------------------the files for multiple uploads-------------------------------------------------%>
    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <script src="DropzoneJs_scripts/dropzone.js"></script>
    <%----------------------------------------------------------------------------------------------------------------%>
    <script>
        var dropZoneObj;
        var dropZone;

        function InitDropZone() {

            Dropzone.autoDiscover = false;

            dropZoneObj = {
                url: "hn_GSTFileUploader.ashx",
                maxFiles: 10,
                addRemoveLinks: true,
                success: function (file, response) {
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                    console.log("Successfully uploaded :" + imgName);
                    $("#btnUploadFiles").show();
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }
            dropZone = new Dropzone('#dZUpload', dropZoneObj);
        }

         $(document).ready(function() {
            InitDropZone();
        });
       
    </script>
</head>
<body>
    <form id="form1" runat="server">
        
        <div class="col-md-12">
                    <div style="text-align:right;padding-right:20px;">
                    <input type="button" onclick="window.close()" class="button-normal" value="Close" />
                        </div>
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    

                <asp:Panel class="pnldrop" ID="pnlDragandDrop" runat="server" Style="padding-top:20px; width: 100%; height: 100px; background-color: White; border-color: blue">
                    <div class="ns-h3">
                                <span>Upload Hotel Invoice Documents</span>
                            </div>
                    <div class="col-md-8">

                        <div id="dZUpload" class="dropzone" runat="server">
                            <div class="dz-default dz-message">
                                Upload or Drag Drop documents here.
                            </div>
                        </div>
                        <asp:Button ID="btnUploadFiles" runat="server" Text="Upload Files" OnClick="btnUploadFiles_Click" CssClass="button-normal" style="display:none;" />
                    </div>
                    <div class="col-md-4">
                        <asp:Panel ID="pnlUploadedFiles" runat="server" Width="500px" Height="200px" class="pnldrop" BorderStyle="Solid" BorderWidth="1px" BorderColor="Gray" Style="overflow: auto;">
                            <div class="ns-h3">
                                <span>View Files</span>
                            </div>
                        </asp:Panel>
                    </div>

                </asp:Panel>
                
                    </div>
        
    </form>
</body>
</html>
