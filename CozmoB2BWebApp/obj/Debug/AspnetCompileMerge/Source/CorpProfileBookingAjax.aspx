﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="CorpProfileBookingAjax" Codebehind="CorpProfileBookingAjax.aspx.cs" %>



    
        <%if (ShowPopup)
          { %>
        <div id="divDiffFlight<%=resultObj.ResultId %>" class="row">
            <div class="col-xs-12">
                <div class="form-group">                
                    <label>
                        Please Select Travel Reason </label>
                        <% %>
                    <select id='ddlTravelReason<%=resultObj.ResultId %>'>
                    <option value="-1">Select Reason</option>
                    <% foreach (System.Data.DataRow row in dtTravelReason.Rows)
                       {%>
                    <option value='<%=row["ReasonId"].ToString() %>'><%=row["Description"].ToString()%></option>
                    <%} %>
                    </select>                    
                    <span id="errorReason" class="errorMessageBlock" style="display: none">Invalid Reason
                        </span>
                </div>
            </div>
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary" onclick='BookNow(<%=resultObj.ResultId %>)'>
                    Continue</button>
                <button type="button" class="btn btn-deafult cancel-btn" onclick='HideTravelReason(<%=resultObj.ResultId %>)'>
                    Cancel</button>
            </div>
        </div>
        <%} if (ShowError)
          { %>
        <div id="divSameFlight<%=resultObj.ResultId %>" class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label id="lblSameFlight">
                        <%=errorMessage%>
                    </label>
                </div>
            </div>
            <div class="col-xs-12">
                <button type="button" class="btn btn-deafult cancel-btn" onclick="HideTravelReason(<%=resultObj.ResultId %>)">
                    Close</button>
            </div>
        </div>
        <%}%>
    

