﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="FixedDepartureInvoice" Codebehind="FixedDepartureInvoice.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>FixedDeparture Invoice</title>
      <link rel="stylesheet" href="css/main-style.css" />
    <script type="text/javascript">
    function printPage() {
        document.getElementById('btnPrint').style.display = "none";
        window.print();
        setTimeout('showButtons()', 1000);
    }
    function showButtons() {
        document.getElementById('btnPrint').style.display = "block";
    }

</script>
    <style type="text/css">
        .style2
        {
            height: 30px;
        }
    </style>
</head>
<body>
<div id="middle-container">
    <form id="form1" runat="server">
    <div>

</div>
<div>

<input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 

<table id="tblInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table class="FlightInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" align="left">  <%--<img src="images/logo.jpg" alt="Agent Logo" height="50">--%>
    </td>
    <td width="33%" align="center"><h1 style=" font-size:26px">Invoice</h1></td>
    <td width="33%" align="right"><input style="width: 100px;" id="btnPrint" onclick="return printPage();" type="button"
                        value="Print Invoice" /></td>
  </tr>
</table>
    </td>
  </tr>
 
 
    <tr>
        <td colspan="2">
            <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
                <tbody>
                    <tr>
                        <td width="50%" style="vertical-align: top;">
                            <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                <tbody>
                                    <tr>
                                        <td height="30px" colspan="2">
                                            <h3 style="font-size: 16px; font-family: 'Arial';">
                                                Client Details</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Agency Name: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <% = agency.Name %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Agency Code: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <% = agency.Code %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Agency Address: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <% = agencyAddress %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>TelePhone No: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                {%>
                                            Phone:
                                            <% = agency.Phone1%>
                                            <%  }
                                                else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                { %>
                                            Phone:
                                            <% = agency.Phone2%>
                                            <%  } %>
                                            <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                { %>
                                            Fax:
                                            <% = agency.Fax %>
                                            <%  } %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="50%" style="vertical-align: top;">
                            <table width="90%" align="right" style="font-size: 12px; font-family: 'Arial'">
                                <tbody>
                                    <tr>
                                        <td colspan="2" class="style2">
                                            <h3 style="font-size: 16px; font-family: 'Arial';">
                                                Invoice Details</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Invoice No: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=invoice.CompleteInvoiceNumber%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Invoice Date: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <% = invoice.CreatedOn.ToString("dd MMMM yyyy") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Booking Ref: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <% = pnr %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
                <tbody>
                    <tr>
                        <td width="50%" style="vertical-align: top;">
                            <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                <tbody>
                                    <tr>
                                        <td colspan="4" style="vertical-align: top;">
                                            <h3 style="font-size: 16px; font-family: 'Arial';">
                                                Booking Details</h3>
                                        </td>
                                    </tr>
                                    <tr height="30px">
                                        <td style="vertical-align: middle; width: 95px;">
                                            <b>Tour Name: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.Name%>
                                        </td>
                                        <td style="vertical-align: middle; width: 95px;">
                                            <b>Date Of Issue: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=Convert.ToDateTime(activity.TransactionHeader.Rows[0]["CreatedDate"]).ToString("dd MMMM yyyy")%>
                                        </td>
                                    </tr>
                                    <tr height="30px">
                                        <td style="vertical-align: middle; width: 95px;">
                                            <b>Tour Date: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd MMMM yyyy")%>
                                        </td>
                                       <%-- <td style="vertical-align: middle; width: 95px;">
                                            <b>Duration: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.DurationHours.ToString()%>
                                        </td>--%>
                                    </tr>
                                    <tr height="30px">
                                        <td style="vertical-align: middle; width: 95px;">
                                            <b>Pick Up Point: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.PickupLocation %>
                                        </td>
                                        <td style="vertical-align: middle; width: 95px;">
                                            <b>Pick Up Time: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.PickupDate.ToString("hh:mm tt")%>
                                        </td>
                                    </tr>
                                    <tr height="30px">
                                        <td style="vertical-align: middle; width: 95px;">
                                            <b>Drop Up Point: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.DropoffLocation%>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="50%" style="vertical-align: top;">
                            <table class="FlightInvoice" border="0" width="90%" align="right" style="font-size: 12px;
                                font-family: 'Arial'">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: top;" colspan="3">
                                            <h3 style="font-size: 16px; font-family: 'Arial';">
                                                Passenger Details</h3>
                                        </td>
                                    </tr>
                                    <tr height="30px">
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Lead Pax Name: </b>
                                        </td>
                                        <%--<%for (int i = 0; i < activity.TransactionDetail.Rows.Count; i++)
                                          { %>--%>
                                        <td style="vertical-align: middle;">
                                            <%=activity.TransactionDetail.Rows[0]["FirstName"].ToString() + " " + activity.TransactionDetail.Rows[0]["LastName"].ToString()%>
                                        </td>
                                      <%--  <%} %>--%>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>No Of Adults: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.TransactionHeader.Rows[0]["Adult"].ToString()%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>No Of Childs: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.TransactionHeader.Rows[0]["Child"].ToString()%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>No Of Infants: </b>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <%=activity.TransactionHeader.Rows[0]["Infant"].ToString()%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" valign="top">
            <table class="FlightInvoice" width="100%" style="font-family: 'Arial'">
                <tbody>
                    <tr>
                        <td width="50%" style="vertical-align: top;">
                            <table width="100%" style="font-size: 12px; font-family: 'Arial'">
                                <tbody>
                                    <tr>
                                        <td colspan="4" style="vertical-align: top;">
                                            <h3 style="font-size: 16px; font-family: 'Arial';">
                                                Rate Details</h3>
                                        </td>
                                    </tr>
                              </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Net Amount: </b>
                                        </td>
                                        <td style="vertical-align: middle;" align="left">
                                         <%=agency.AgentCurrency%>
                                            <asp:Label ID="lblNetValue" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                               <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b><asp:Label ID="lblMarkup" runat="server" Text="Markup:" Visible="false"></asp:Label></b>
                                        </td>
                                        <td style="vertical-align: middle;" align="left">
                                            <asp:Label ID="lblMarkupValue" runat="server" Text="" Visible></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b><asp:Label ID="lblDiscount" runat="server" Text="Discount:" ></asp:Label></b>
                                        </td>
                                        <td style="vertical-align: middle;" align="left">
                                            <asp:Label ID="lblDiscountValue" runat="server" Text="" Visible></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; width: 120px;">
                                            <b>Total Amount: </b>
                                        </td>
                                        <td style="vertical-align: middle;" align="left">
                                         <%=agency.AgentCurrency%>
                                            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="50%" style="vertical-align: top;">
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
 
  <tr>

    <td colspan="2">
    
    <table border="0" class="FlightInvoice" width="100%" style="font-family: 'Arial'">
<tr>
                 <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Invoiced By: </b>
                 </td>
                 <td style="vertical-align: top;">
                     Cozmo Travel
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Created By: </b>
                 </td>
                 <td style="vertical-align: top;">
                     <%= member.FirstName + " " + member.LastName%>
                 </td>
             </tr>
               <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Location: </b>
                 </td>
                 <td style="vertical-align: top;">
                     <asp:Label ID="lblLocation" runat="server"></asp:Label>
                 </td>
             </tr>
    
             <tr>
                 <td style="width: 130px">
                 </td>
                 <td style="width: 160px">
                     <span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;"></span>
                 </td>
                    <td>
                        <div class="email-message-parent" style="display:none;" id="emailSent">
                    <span  style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                    </span>
                    
           </div>
             </td>
             </tr>
         </table>
    </td>
  </tr>
</table>
</div>

    </form>
    </div>
</body>
</html>
