<%@ Page AutoEventWireup="true" Inherits="TrainBookingQueue" Title="Train Booking Queue"
    Language="C#" MasterPageFile="~/TransactionBE.master" CodeBehind="TrainBookingQueue.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.BookingEngine.GDS" %>
<%@ Import Namespace="CT.AccountingEngine" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <style>
        .pagination > .active > a {
            background-color: #60c231;
            border-color: #60c231;
            color:#fff;
        }

        .pagination > .active > a {
            background-color: #60c231;
            border-color: #60c231;
        }

            .pagination > .active > a:hover {
                background-color: #60c231;
                border-color: #60c231;
            }
    </style>
    <script>
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select Create From Date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select Created To Date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();

            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("title", "Select Boarding Date");
            cal3.selectEvent.subscribe(setDates3);
            cal3.cfg.setProperty("close", true);
            cal3.render();
        }
        function showCal1() {
            init();
            $('container3').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            cal3.hide();
            document.getElementById('container1').style.display = "block";

        }
        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            cal1.hide();
            cal3.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtCreatedFrom.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function showCal3() {
            init();
            $('container1').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "block";
            cal3.show();
            cal1.hide();
            cal2.hide();
            document.getElementById('container3').style.display = "block";

        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference > 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Created From Date should be less than or equal to today's date ";
                return false;
            }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtCreatedFrom.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtCreatedFrom.ClientID %>').value;
            //if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            //    document.getElementById('errMess').style.display = "block";
            //    document.getElementById('errorMessage').innerHTML = "First select From Date.";
            //    return false;
            //}

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid From Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to From Date (" + date1 + ") ";
                return false;
            }

            //document.getElementById('errMess').style.display = "none";
            //document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtCreatedTo.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        function setDates3() {
            var date1 = cal3.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%= txtBoardingDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal3.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>

    <script>
        function showProgress() {
            $("#ctl00_upProgress").show();
        }
        function hideProgress() {
            $("#ctl00_upProgress").hide();
        }
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }
        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }
        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }
        function showAgent(id) {

            document.getElementById('DisplayAgent' + id).style.display = 'block';
        }
        function ShowRemarks(id, index) {
            var ddl = id;
            var txtid = 'ctl00_cphTransaction_dlBookingQueue_ctl0' + index + '_txtCorpRemarks';
            if ($('#' + ddl).val() == 'R') {
                $('#' + txtid).show();
            }
            else {
                $('#' + txtid).hide();
            }
        }
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
    </script>

    <script type="text/javascript">
        function ShowOrHidePNR(index) {
            $('#divPendingList' + index + ' input[type=hidden]').val($('#ddlPendingList' + index).val());
            $('#divInProgressList' + index + ' input[type=hidden]').val($('#ddlInProgressList' + index).val());
            $('#divConfirmedList' + index + ' input[type=hidden]').val($('#ddlConfirmedList' + index).val());
            if ($('#ddlInProgressList' + index).val() == 'Confirmed') {
                $('#divPNR' + index).show();
                return false;
            }
            else {
                $('#divPNR' + index).hide();
            }
        }
        function ValidateRemarksSubmit(index) {
            if ($('#divRemarks' + index + ' textarea').val().trim() == '') {
                showErr(index, 'Please Enter Remarks', 'error');
                return false;
            }
            if ($('#ddlInProgressList' + index).val() == 'Confirmed' && $('#divPNR' + index + ' input[type=text]').val().trim() == '') {
                showErr(index, 'Please Enter PNR', 'error');
                return false;
            }
        }
        function showErr(index, msg, status) {
            $('#lblErrMsg' + index).show();
            $('#lblErrMsg' + index).text(msg);
            if (status == 'success') {
                $('#lblErrMsg' + index).css("color", "green");
            }
            else {
                $('#lblErrMsg' + index).css("color", "red");
            }
        }
        function showModal() {
            $("#modalRemarks").modal('show');
        }

    </script>

    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>

    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 180px; left: 227px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 180px; left: 610px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container3" style="position: absolute; top: 100px; left: 227px; display: none; z-index: 9999">
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label" style="padding:0;">
        <tr>
            <td style="width: 700px" align="left">
                <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                    onclick="return ShowHide('divParam');">Hide Parameter</a>
            </td>
        </tr>
    </table>
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        Boarding Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtBoardingDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal3()">
                                        <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        Agent:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList AppendDataBoundItems="true" CssClass="form-control" ID="ddlAgency"
                            runat="server">
                            <%--<asp:ListItem Value="-1">Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Train Number:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox CssClass="form-control" ID="txtTrainNumber" runat="server">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        Reference ID:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox CssClass="form-control" ID="txtReferenceID" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        PNR:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox CssClass="form-control" ID="txtPNR" runat="server">
                        </asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        Train Name:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox CssClass="form-control" ID="txtTrainName" runat="server">
                        </asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>

                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        Status:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList AppendDataBoundItems="true" CssClass="form-control" ID="ddlStatus"
                            runat="server">
                            <asp:ListItem Value="All">All</asp:ListItem>
                            <asp:ListItem Value="Pending">Pending</asp:ListItem>
                            <asp:ListItem Value="In Progress">In Progress</asp:ListItem>
                            <asp:ListItem Value="Confirmed">Confirmed</asp:ListItem>
                            <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Email:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox CssClass="form-control" ID="txtEmail" runat="server">
                        </asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        Passenger Name:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox CssClass="form-control" ID="txtPaxName" runat="server"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        Created From:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCreatedFrom" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        Created To:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCreatedTo" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div id="errMess" class="error_module col-md-12" style="display: none;">
                        <div id="errorMessage" style="float: left; color: Red;" class="yellow-back width-100 margin-top-5">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-12">
                        <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" runat="server" Text="Search"
                            OnClick="btnSearch_Click" />
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <br />
    <br />
    <div class="col-md-12 padding-0 marbot_10">
        <div class="col-md-8">
            <asp:Label ID="lblErrorMessage" runat="server" Style="color: red;"></asp:Label>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <div class="col-md-12 padding-0 marbot_10" id="divPagination" runat="server">
        <nav aria-label="...">
            <ul class="pagination">
                <li class="page-item" id="liFirst" runat="server">
                    <asp:LinkButton ID="btnFirst" runat="server" CssClass="page-link"
                        OnClick="btnFirst_Click" Text="First"></asp:LinkButton></li>
                <li class="page-item" id="liPreviousSet" runat="server">
                    <asp:LinkButton ID="btnPreviousSet" runat="server" CssClass="page-link"
                        OnClick="btnPreviousSet_Click" Text="<<"></asp:LinkButton></li>
                <li class="page-item" id="liPrevious" runat="server">
                    <asp:LinkButton ID="btnPrevious" runat="server" CssClass="page-link"
                        OnClick="btnPrevious_Click" Text="<"></asp:LinkButton></li>

                <asp:Repeater ID="rptPagination" runat="server" OnItemCommand="rptPagination_ItemCommand">
                    <ItemTemplate>
                        <li class='page-item <%#Eval("Class")%>'>
                            <asp:LinkButton ID="btnPagination" CssClass="page-link"
                                runat="server" CommandName="pagination" Enabled='<%#Eval("Enabled")%>'
                                CommandArgument='<%#Eval("PageNumber")%>' Text='<%#Eval("PageNumber")%>'></asp:LinkButton></li>
                    </ItemTemplate>
                </asp:Repeater>

                <li class="page-item" id="liNext" runat="server">
                    <asp:LinkButton ID="btnNext" runat="server" CssClass="page-link"
                        OnClick="btnNext_Click" Text=">"></asp:LinkButton></li>
                <li class="page-item" id="liNextSet" runat="server">
                    <asp:LinkButton ID="btnNextSet" runat="server" CssClass="page-link"
                        OnClick="btnNextSet_Click" Text=">>"></asp:LinkButton></li>
                <li class="page-item" id="liLast" runat="server">
                    <asp:LinkButton ID="btnLast" runat="server" CssClass="page-link"
                        OnClick="btnLast_Click" Text="Last"></asp:LinkButton></li>
            </ul>
        </nav>
    </div>
    <div>
        <asp:DataList ID="dlBookingQueue" runat="server" AutoGenerateColumns="false"
            CellPadding="0" CellSpacing="0" OnItemCommand="dlBookingQueue_ItemCommand"
            Width="100%" ShowHeader="true" DataKeyField="RBID">
            <ItemTemplate>
                <%--<div style="display: none; background-color: #e6f1d9; width: 50%;" id="DisplayAgent<%#Container.ItemIndex %>" class="div-TravelAgent">
                    <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href='#DisplayAgent<%#Container.ItemIndex %>'
                        onclick="hidestuff('DisplayAgent<%#Container.ItemIndex %>');">
                        <img src="images/close1.png" /></a></span>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="25">
                                <label><strong><%#Eval("agent_name")%></strong></label>
                            </td>
                            <td>Credit Balance:
                                <label><strong><%#Eval("agent_current_balance")%></strong></label>
                            </td>
                        </tr>
                        <tr>
                            <td height="25">Mob :
                                <label><strong><%#Eval("agent_phone1")%></strong></label>
                            </td>
                            <td>Phone :
                                <label><strong><%#Eval("agent_phone2")%></strong></label>
                            </td>
                        </tr>
                        <tr>
                            <td height="25">Email :
                                <label><strong><%#Eval("agent_email1")%></strong></label>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>--%>
                <div class="tbl">
                    <asp:HiddenField ID="hdnIndex" runat="server" />
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            <%--<a href='#DisplayAgent<%#Container.ItemIndex %>' onclick="showAgent(<%#Container.ItemIndex %>);">
                                <label><strong><%#Eval("agent_name")%></strong></label>
                            </a>--%>
                            <label style="color:#60c231;"><strong><%#Eval("agent_name")%></strong>
                        </div>
                        <div class="col-md-4">
                            Status:
                            <label><strong><%#Eval("Status")%></strong></label>
                        </div>
                        <div class="col-md-4" style='display: <%# (Eval("Status").ToString() == "Confirmed" || Eval("Status").ToString() == "Cancelled") && Eval("PNR").ToString() != "" ? "block" : "none" %>'>
                            <label class="pull-left">
                                PNR:
                                <label style="color: green;"><strong><%#Eval("PNR")%></strong></label>
                                </strong></label>
                            <div style="display: none" id="Release-Seat">
                                <textarea class="" name="textarea" id="textarea" cols="20" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            <asp:Label ID="lblTrainNumber" runat="server" Text='<%#Eval("TrainNumber")%>'></asp:Label>
                            |
                            <asp:Label ID="lblTrainName" runat="server" Text='<%#Eval("TrainName")%>'></asp:Label>
                            |
                            <asp:Label ID="lblAdults" runat="server" Text='<%#Eval("Adults")%>'></asp:Label>
                            ADT |
                            <asp:Label ID="lblChildren" runat="server" Text='<%#Eval("Children")%>'></asp:Label>
                            CHD
                        </div>
                        <div class="col-md-4">
                            <strong>
                                <asp:Label runat="server" ID="lblFromStation" Text='<%#Eval("FromStation")%>'></asp:Label>
                                -
                                <asp:Label runat="server" ID="lblToStation" Text='<%#Eval("ToStation")%>'></asp:Label></strong>
                        </div>
                        <div class="col-md-4">
                            Reference ID: <strong>
                                <asp:Label runat="server" ID="lblReferenceId" Text='<%#Eval("ReferenceId")%>'></asp:Label></strong>

                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            Requested On:
                            <label><strong><%#Eval("CreatedWeekDay")%>(</strong></label>
                            <strong>
                                <asp:Label runat="server" ID="lblCreatedOn" Text='<%#Eval("CreatedOn")%>'></asp:Label>)</strong>

                        </div>
                        <div class="col-md-4">
                            Boarding From: <strong>
                                <asp:Label runat="server" ID="lblBoardingFrom" Text='<%#Eval("BoardingFrom")%>'></asp:Label></strong>
                        </div>
                        <div class="col-md-4">
                            Passenger Name: <strong>
                                <asp:Label runat="server" ID="lblPaxName" Text='<%#Eval("PaxName")%>'></asp:Label></strong>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            Boarding Date:
                            <label><strong><%#Eval("BoardingWeekDay")%>(</strong></label>
                            <strong>
                                <asp:Label runat="server" ID="lblBoardingDate" Text='<%#Eval("BoardingDate")%>'></asp:Label>)</strong>
                        </div>
                        <div class="col-md-4">
                            Class Preference: <strong>
                                <asp:Label runat="server" ID="lblClass" Text='<%#Eval("Class")%>'></asp:Label></strong>
                        </div>
                        <div class="col-md-4">
                            Passenger Email: <strong>
                                <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("BookingEmail")%>'></asp:Label></strong>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>

                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">Created By:  <label><strong><%#Eval("CreatedBy")%></strong></div>
                    </div>

                    <div class="col-md-12 padding-0 marbot_10" style='display: <%# Eval("Status").ToString() == "Cancelled" ? "none" : "block" %>'>
                        <div class="col-md-1">
                            Status :
                        </div>
                        <div class="col-md-3" style='display: <%# Eval("Status").ToString() == "Pending" ? "block" : "none" %>'>
                            <select id='ddlPendingList<%#Container.ItemIndex %>' class="form-control" onchange="return ShowOrHidePNR('<%#Container.ItemIndex %>');">
                                <option value="Pending">Pending</option>
                                <option value="In Progress">In Progress</option>
                            </select>
                        </div>
                        <div class="col-md-3" style='display: <%# Eval("Status").ToString() == "In Progress" ? "block" : "none" %>'>
                            <select id='ddlInProgressList<%#Container.ItemIndex %>' class="form-control" onchange="return ShowOrHidePNR('<%#Container.ItemIndex %>');">
                                <option value="In Progress">In Progress</option>
                                <option value="Confirmed">Confirmed</option>
                            </select>
                        </div>
                        <div class="col-md-3" style='display: <%# Eval("Status").ToString() == "Confirmed" ? "block" : "none" %>'>
                            <select id='ddlConfirmedList<%#Container.ItemIndex %>' class="form-control" onchange="return ShowOrHidePNR('<%#Container.ItemIndex %>');">
                                <option value="Confirmed">Confirmed</option>
                                <option value="Cancelled">Cancelled</option>
                            </select>
                        </div>
                        <div id='divPendingList<%#Container.ItemIndex %>' style="display: none;">
                            <asp:HiddenField ID="hdnPendingList" Value='<%# Eval("Status").ToString() == "Pending" ? "Pending" : "" %>' runat="server" />
                        </div>
                        <div id='divInProgressList<%#Container.ItemIndex %>' style="display: none;">
                            <asp:HiddenField ID="hdnInProgressList" Value='<%# Eval("Status").ToString() == "In Progress" ? "In Progress" : "" %>' runat="server" />
                        </div>
                        <div id='divConfirmedList<%#Container.ItemIndex %>' style="display: none;">
                            <asp:HiddenField ID="hdnConfirmedList" Value='<%# Eval("Status").ToString() == "Confirmed" ? "Confirmed" : "" %>' runat="server" />
                        </div>

                        <div class="col-md-4" id='divRemarks<%#Container.ItemIndex %>'>
                            <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Rows="1" CssClass="form-control" placeholder="Enter Remarks here"
                                runat="server"></asp:TextBox>
                        </div>
                        <div id='divPNR<%#Container.ItemIndex %>' class="col-md-3" style="display: none;">
                            <asp:TextBox ID="txtPNR" CssClass="inputEnabled form-control" runat="server" placeholder="Enter PNR"></asp:TextBox>
                        </div>


                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-6">
                            <label id="lblErrMsg<%#Container.ItemIndex %>" style="display: none;"></label>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <asp:Button ID="btnRequest" runat="server" CommandName="SubmitRemarks" CommandArgument='<%#Eval("RBId") %>' Visible='<%# Eval("Status").ToString() == "Cancelled" ? false : true %>'
                                Text="Submit" CssClass="button" OnClientClick='<%# String.Format("return ValidateRemarksSubmit({0});", Container.ItemIndex) %>' />
                            <asp:Button ID="btnRemarksHistory" runat="server" CommandName="ShowHistory" CommandArgument='<%#Eval("RBId") %>'
                                Text="Show Remarks" CssClass="button" />

                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnAgentID" runat="server" Value='<%#Eval("agent_id")%>' />
                    <asp:HiddenField ID="hdnReservationUpto" runat="server" Value='<%#Eval("ReservationUpto")%>' />
                    <asp:HiddenField ID="hdnCurrentPNR" runat="server" Value='<%#Eval("PNR")%>' />
                    <asp:HiddenField ID="hdnCurrentStatus" runat="server" Value='<%#Eval("Status") %>' />
                    <asp:HiddenField ID="hdnQuota" runat="server" Value='<%#Eval("Quota")%>' />
                    <asp:HiddenField ID="hdnItemIndex" runat="server" Value='<%#Container.ItemIndex %>' />
                    <div class="clearfix">
                    </div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:DataList>
    </div>

    <div class="modal fade" id="modalRemarks" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Train Booking Remarks</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px !important;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" style="background-color: #d9ecdf; padding: 10px;">
                        <div class="col-md-1">#</div>
                        <div class="col-md-2">Status</div>
                        <div class="col-md-3">Updated on</div>
                        <div class="col-md-6">Remarks</div>
                    </div>
                    <asp:DataList ID="dlRemarksHistory" runat="server" AutoGenerateColumns="false"
                        CellPadding="0" CellSpacing="0"
                        Width="100%" ShowHeader="true" DataKeyField="RBRID">
                        <ItemTemplate>
                            <div class="col-md-12" style="padding: 5px;">
                                <div class="col-md-1"><%#Eval("RowNumber") %></div>
                                <div class="col-md-2"><%#Eval("RemarksStatus") %></div>
                                <div class="col-md-3"><%#Eval("CreatedOn") %></div>
                                <div class="col-md-6"><%#Eval("Remarks") %></div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
