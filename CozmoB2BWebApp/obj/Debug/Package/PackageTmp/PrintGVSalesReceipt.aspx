<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintGVSalesReceiptUI" Codebehind="PrintGVSalesReceipt.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>Print GV Sales Report</title>
    <link type="text/css" href="Styles/CTStyle.css" rel="stylesheet" />
    <link type="text/css" href="Styles/default.css" rel="stylesheet" />
</head>
<script type="text/javascript" language="javascript">

 function printPage()
    {
        document.getElementById('btnPrint').style.display="none";
	    window.print();	 
	    //alert('2');
	    setTimeout('showButtons()',1000);
	    //alert('3');
    }
    function showButtons()
    {
        document.getElementById('btnPrint').style.display="block";	    
    }
    </script>

<body>

    <form id="form1" runat="server">
    <table width="100%" style="text-align:center;margin-left:0px" >
    <tr>
    <td  align="center">
    
    <div  style="width:630px">
    <div id="divPrintButton" style="text-align:left;">
    <input style="width:100px;" id="btnPrint" onclick="return printPage();" class="button" type="button" value="Print Receipt" />
    </div>
    <%--<div style="border:1px solid #000; padding:5px; width:100%; overflow:hidden;">--%>
    <asp:Panel runat="server" ID="pnlPNR" Visible="false">
    <div style="border:1px solid #000; padding:5px; width:100%; overflow:hidden;">
    
  
   </div>
   <p style="text-align:center; font-size:14px; font-weight:bold; border-top:1px solid #000;"></p>
   </asp:Panel>
    <div style="border:1px solid #000; padding:5px; width:100%; overflow:hidden;">
<table cellpadding="0" cellspacing="0" class="label" width="100%" >
        <tr>
            <td  align="right">
                <table width="100%">
                <tr>
                <td align="left">
                    <label style="font-style:italic;font-size:10px;color:Gray" class="rptLabel">Customer Copy</label>
                </td>
                
                <td><label class="rptLabel">Receipt</label></td><td style="width:300px;"><div class="logo"><img height="40px" src="images/logo.gif" alt="" /></div>
                </td></tr></table>
            </td>
        </tr>    
        <tr>
            <td style="text-align:left">
            <br />
      <table cellpadding="0" cellspacing="0" class="rptTable"  >
       <%-- <tr>
            <td style="width:150px;"></td>
            <td></td>
        </tr>--%>
        <tr>
        <td  style="width:20%" ></td>
        <td  style="width:80%" ></td>
        </tr>
        
        <tr>
            <td  align="left"><asp:Label ID="lblReceiptNo" CssClass="rptLabel" runat="server" Text="Receipt No.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblReceiptNoValue" CssClass="rptLabelValue" runat="server" ></asp:Label>
            
            
            <table align="right">
            
            <tr>
            <td align="left"><asp:Label ID="lblDate" CssClass="rptLabel" runat="server" Text="Date:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblDateValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
            
            </table>
            
            
            
            </td>
        </tr>
        <tr>
            <td  align="left"><asp:Label ID="lblDocketNo" CssClass="rptLabel" runat="server" Text="Docket No.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblDocketNoValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr id="trAddFee" runat="server" visible="false">
            <td  align="left"><asp:Label ID="lblAddFee" CssClass="rptLabel" runat="server" Text="Add. Fees:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblAddFeeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
         <tr id="trAddSvc" runat="server" visible="false">
            <td  align="left"><asp:Label ID="lblAddServices" CssClass="rptLabel" runat="server" Text="Add. Services:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblAddServicesValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
        <tr>
            <td align="left"><asp:Label ID="lblVisaType" CssClass="rptLabel" runat="server" Text="Type Of Visa:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblVisaTypeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblSubmittedFor" CssClass="rptLabel" runat="server" Text="Submitted For:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblSubmittedForValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td  valign="top" align="left"><asp:Label ID="lblNumberOfApplicant" CssClass="rptLabel" runat="server" Text="No.Of Applicant:" ></asp:Label></td>
           <td>
                <table cellpadding="0" cellspacing="0" style="margin-top:-5px;margin-bottom:-5px" >
                <tr>
                <td  align="left"><asp:Label ID="lblAdults" CssClass="rptLabel" runat="server" Text="Adults:"></asp:Label></td>
                <td align="left"><asp:Label ID="lblAdultsValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
                <td  align="left"><asp:Label ID="lblChild" CssClass="rptLabel" runat="server" Text="Child:"></asp:Label></td>
                <td align="left"><asp:Label ID="lblChildValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
                <td  align="left"><asp:Label ID="lblInfant" CssClass="rptLabel" runat="server" Text="Infants:"></asp:Label></td>
                <td align="left"><asp:Label ID="lblInfantValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
                </tr>
                </table>           
           </td>
        </tr>
   <%--     <tr>
            <td align="left" valign="top"><asp:Label ID="lblPrincipalApplicant" CssClass="rptLabel" runat="server" Text="Principal Applicant:"></asp:Label></td>
            <td align="left" valign="top"><asp:Label ID="lblPrincipalApplicantValue" CssClass="rptLabelValue grdof" Width="450px"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left" valign="top"><asp:Label ID="lblPassportNo" CssClass="rptLabel" runat="server" Text="Passport #:"></asp:Label></td>
            <td align="left" valign="top"><asp:Label ID="lblPassportNoValue" CssClass="rptLabelValue grdof" Width="450px"  runat="server" ></asp:Label></td>
        </tr>--%>
        <tr>
            <td align="left" valign="top"><asp:Label ID="lblNationality" CssClass="rptLabel" runat="server" Text="Nationality:"></asp:Label></td>
            <td align="left" valign="top"><asp:Label ID="lblNationalityValue" CssClass="rptLabelValue" runat="server" Width="450px"  ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblVisaFee" CssClass="rptLabel" runat="server" Text="Visa Fee:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblVisaFeeValue" CssClass="rptLabelValue" runat="server" ></asp:Label>
            
            <table align="right">
            
            <tr style="display:none;">
            <td align="left"><asp:Label ID="lblSvcTax" CssClass="rptLabel" runat="server" Text="Svc Tax:" Visible="false"></asp:Label></td>
            <td align="left"><asp:Label ID="lblSvcTaxValue" CssClass="rptLabelValue" runat="server" Visible="false"></asp:Label></td>
        </tr>
            
            </table>
            
            
            
            </td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCollectionMode" CssClass="rptLabel" runat="server" Text="Collection Mode:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCollectionModeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
         <tr style="display:none;">
            <td align="left"><asp:Label ID="lblConsultant" CssClass="rptLabel" runat="server" Text="Consultant:" Visible="false"></asp:Label></td>
            <td align="left"><asp:Label ID="lblConsultantValue" CssClass="rptLabelValue" runat="server" Visible="false"></asp:Label></td>
        </tr>
        
        
        
        <tr> 
        
        <td colspan="2">
        
        <%--<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td><strong>IVR No </strong></td>
    <td><strong>Visitor</strong></td>
    <td><strong>Passport#</strong></td>
    <td><strong>Nationality</strong></td>
  </tr>
  <tr>
    <td>11699</td>
    <td>ziyad</td>
    <td>g8tyd</td>
    <td>Indian</td>
  </tr>
</table>--%>

        <table border="0" >
        <tr>
        <td valign="top">
         <fieldset style="border-width:2px">
             <legend> Visitor Details </legend>
               <%-- <div style=" min-height:50px; width:500px; margin-top:0px; border:solid 2px #ccc; border-top:none" class="" >--%>
                    <asp:DataList  ID="dlPaxDetails" BorderStyle="None" GridLines="None"  runat="server" Width="100%">
                        <HeaderTemplate>            
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">IVR No</th>
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">Visitor</th>                                                      
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">Age Group</th>    
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">Passport #</th>    
                           <%--<th style="width:70px;height:2%;border-bottom: 1px solid black;text-align:right;font-size:11px">Amount</th>--%>
                         </HeaderTemplate>
                        <ItemTemplate>
                        
                            <%--<td valign="top" style="height:19px"><asp:Label runat="server" CssClass="rptLabelNewValue" ID="lblSlNo"  Text='<%# Container.ItemIndex + 1 %>' Width="10px"></asp:Label></td>--%>
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITlblIVRNo"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_ID")%>' Width="60px"></asp:Label></td>                           
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITlblVisitor"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_NAME")%>' Width="200px"></asp:Label></td>                           
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITlblAgeGrp"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_PAX_TYPE")%>' Width="150px"></asp:Label></td>                           
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITlblPassportNo"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_PASSPORT_NO")%>' Width="150px"></asp:Label></td>                           
                            
                        </ItemTemplate>
        
            </asp:DataList>
                
                              
                            <%--</div>--%>
                            </fieldset>
        </td>
   </tr>
    </table>
        
        </td>
        </tr>
         
    </table> 
    
    </td>
    </tr>
    <tr>
    <td>
   
  
    <br /><br />
    <asp:Label ID="lblLocationTerms" runat="server" style="font-style:italic;"></asp:Label><br />
   <asp:Label ID="lblLocationAds" runat="server" style="font-size:12px;  padding-bottom:5px;"></asp:Label>
    <%--<Div style="font-style:italic;">Dear Customer: Prior to Payment, Please Reconfirm Your Booking Details. Once Payment is made all Air Arabia Terms And condition will be Applicable</div>
   
    G.S.A Cozmo Travel L.L.C, P.O.Box: 3393, Tower 400, Sharjah- UAE--%>
    </td>
    
    </tr>
     <tr>
        <td>
            <label style="font-style:italic;font-weight:bold;font-size:13px">** This receipt is not valid without company stamp **</label>
        </td>
    </tr>
 </table>
    
     </div>
 <%--<br /><br /> --%>
 <asp:Panel runat="server" id="pnlAccountsCopy" Visible="true">
  <p style="text-align:center; font-size:14px; font-weight:bold; border-top:1px solid #000;"></p>
   <div style="border:1px solid #000; padding:5px; width:100%; overflow:hidden;">
    

    <table cellpadding="0" cellspacing="0" class="label" width="100%" >
         <tr>
            <td  align="right">
                <table width="100%">
                <tr>
                <td align="left">
                    <label style="font-style:italic;font-size:10px;color:Gray" class="rptLabel">Accounts Copy</label>
                </td>
                
                <td><label class="rptLabel">Receipt</label></td><td style="width:300px;"><div class="logo"><img height="40px" src="images/logo.gif" alt="" /></div>
                </td></tr></table>
            </td>
        </tr>    
         <tr>
            <td style="text-align:left">
            <br />
      <table cellpadding="0" cellspacing="0" class="rptTable"  >
       <%-- <tr>
            <td style="width:150px;"></td>
            <td></td>
        </tr>--%>
        <tr>
        <td  style="width:20%" ></td>
        <td  style="width:80%" ></td>
        </tr>
        
        <tr>
            <td  align="left"><asp:Label ID="lblCopyReceiptNo" CssClass="rptLabel" runat="server" Text="Receipt No.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyReceiptNoValue" CssClass="rptLabelValue" runat="server" ></asp:Label>
            
            
            <table align="right">
            <tr>
            <td align="left"><asp:Label ID="lblCopyDate" CssClass="rptLabel" runat="server" Text="Date:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyDateValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
            
            </table>
            
            </td>
        </tr>
         <tr>
            <td  align="left"><asp:Label ID="lblCopyDocketNo" CssClass="rptLabel" runat="server" Text="Docket No.:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyDocketNoValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
         <tr id="trCopyAddFee" runat="server" visible="false">
            <td  align="left"><asp:Label ID="lblCopyAddFee" CssClass="rptLabel" runat="server" Text="Add. Fees:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyAddFeeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
        <tr id="trCopyAddSvc" runat="server" visible="false">
            <td  align="left"><asp:Label ID="lblCopyAddServices" CssClass="rptLabel" runat="server" Text="Add. Services:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyAddServicesValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
        <tr>
            <td align="left"><asp:Label ID="lblCopyVisaType" CssClass="rptLabel" runat="server" Text="Type Of Visa:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyVisaTypeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopySubmittedFor" CssClass="rptLabel" runat="server" Text="Submitted For:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopySubmittedForValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td  valign="top" align="left"><asp:Label ID="lblCopyNumberOfApplicant" CssClass="rptLabel" runat="server" Text="No.Of Applicant:" ></asp:Label></td>
           <td>
                <table cellpadding="0" cellspacing="0" style="margin-top:-5px;margin-bottom:-5px" >
                <tr>
                <td  align="left"><asp:Label ID="lblCopyAdults" CssClass="rptLabel" runat="server" Text="Adults:"></asp:Label></td>
                <td align="left"><asp:Label ID="lblCopyAdultsValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
                <td  align="left"><asp:Label ID="lblCopyChild" CssClass="rptLabel" runat="server" Text="Child:"></asp:Label></td>
                <td align="left"><asp:Label ID="lblCopyChildValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
                <td  align="left"><asp:Label ID="lblCopyInfant" CssClass="rptLabel" runat="server" Text="Infants:"></asp:Label></td>
                <td align="left"><asp:Label ID="lblCopyInfantValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
                </tr>
                </table>           
           </td>
        </tr>
       <%-- <tr>
            <td align="left" valign="top"><asp:Label ID="lblCopyPrincipalApplicant" CssClass="rptLabel" runat="server" Text="Principal Applicant:"></asp:Label></td>
            <td align="left" valign="top"><asp:Label ID="lblCopyPrincipalApplicantValue" CssClass="rptLabelValue grdof" Width="450px"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td align="left" valign="top"><asp:Label ID="lblCopyPassportNo" CssClass="rptLabel" runat="server" Text="Passport #:"></asp:Label></td>
            <td align="left" valign="top"><asp:Label ID="lblCopyPassportNoValue" CssClass="rptLabelValue grdof" Width="450px"  runat="server" ></asp:Label></td>
        </tr>--%>
        <tr>
            <td align="left" valign="top"><asp:Label ID="lblCopyNationality" CssClass="rptLabel" runat="server" Text="Nationality:"></asp:Label></td>
            <td align="left" valign="top"><asp:Label ID="lblCopyNationalityValue" CssClass="rptLabelValue" runat="server" Width="450px"  ></asp:Label></td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopyVisaFee" CssClass="rptLabel" runat="server" Text="Visa Fee:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyVisaFeeValue" CssClass="rptLabelValue" runat="server" ></asp:Label>
            
              <table align="right">
            
            <tr>
            <td align="left"><asp:Label ID="lblCopySvcTax" CssClass="rptLabel" runat="server" Text="Svc Tax:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopySvcTaxValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
            
            </table>
            
            
            
            </td>
        </tr>
        <tr>
            <td align="left"><asp:Label ID="lblCopyCollectionMode" CssClass="rptLabel" runat="server" Text="Collection Mode:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyCollectionModeValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
        
         <tr>
            <td align="left"><asp:Label ID="lblCopyConsultant" CssClass="rptLabel" runat="server" Text="Consultant:"></asp:Label></td>
            <td align="left"><asp:Label ID="lblCopyConsultantValue" CssClass="rptLabelValue" runat="server" ></asp:Label></td>
        </tr>
         
         
         <tr> 
        
        <td colspan="2">
        
  
  <table border="0" >
        <tr>
        <td valign="top">
         <fieldset style="border-width:2px">
             <legend> Visitor Details </legend>
               <%-- <div style=" min-height:50px; width:500px; margin-top:0px; border:solid 2px #ccc; border-top:none" class="" >--%>
                    <asp:DataList  ID="dlCopyPaxDetails" BorderStyle="None" GridLines="None"  runat="server" Width="100%">
                        <HeaderTemplate>            
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">IVR No</th>
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">Visitor</th>                                                      
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">Age Group</th>    
                           <th style="height:2%;border-bottom: 1px solid black;font-size:11px">Passport #</th>    
                           <%--<th style="width:70px;height:2%;border-bottom: 1px solid black;text-align:right;font-size:11px">Amount</th>--%>
                         </HeaderTemplate>
                        <ItemTemplate>
                        
                            <%--<td valign="top" style="height:19px"><asp:Label runat="server" CssClass="rptLabelNewValue" ID="lblSlNo"  Text='<%# Container.ItemIndex + 1 %>' Width="10px"></asp:Label></td>--%>
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITCopylblIVRNo"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_ID")%>' Width="60px"></asp:Label></td>                           
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITCopylblVisitor"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_NAME")%>' Width="200px"></asp:Label></td>                           
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITCopylblAgeGrp"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_PAX_TYPE")%>' Width="150px"></asp:Label></td>                           
                            <td valign="top" align="left" > <asp:Label runat="server" ID="ITCopylblPassportNo"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "FPAX_PaSSPORT_NO")%>' Width="150px"></asp:Label></td>                           
                            
                        </ItemTemplate>
        
            </asp:DataList>
                
                              
                            <%--</div>--%>
                            </fieldset>
        </td>
   </tr>
    </table>
        
        </td>
        </tr>
         
         
    </table> 
    
    </td>
    </tr>
    <tr>
<%--    <td>
    <p><img src="images/URDU_LA.gif" width="600px" height="40px" alt="" /></p>
    <Div  style="font-style:italic;">Dear Customer: Prior to Payment, Please Reconfirm Your Booking Details. Once Payment is made all Air Arabia Terms And condition will be Applicable</Div>
   
    G.S.A Cozmo Travel L.L.C, P.O.Box: 3393, Tower 400, Sharjah- UAE, TEL: 06 507 4444
    </td>--%>
    
    </tr>
 </table>
 </div>
</asp:Panel>    
    
     </div>
<%--</div>--%>
 </td></tr>
    </table>
<%--    <br /><br /><br /><br /><br /><br />--%>

<!-- Doc Details Starting -->
 <%--<table width="100%" style="text-align:center;margin-left:0px" >
 <tr>
    <td align="center" >
        <label style="font-weight:bold">COZMO TRAVEL <br /></label>
        <label> Visa Application Submission Form</LabeL>
    </td>     
 </tr>
 <tr>
    <td>
    <table width="100%">
                <tr>
                <td style="width:400px"></td>
                <td><label>Date:</label><asp:Label ID="lblDocumentsDate" runat="server" ></asp:Label>
                </td>
          </tr>           
          </table>
     </td>
  </tr>
    <tr>
    <td  align="left">
       
       
            <table>
        <tr>
            <td>
                <label style="font-weight:bold"> Principal Applicant:</label>
                 <asp:Label ID="lblDocApplicant" CssClass="rptLabelValue" runat="server" ></asp:Label>
                <br /><label>(name of the applicant/company)</label> 
            </td>
        </tr>
         <tr>            
           <td>
                <label style="font-weight:bold">Visa Submission for:</label>
                <asp:Label ID="lblSubmitted" CssClass="rptLabelValue" runat="server" ></asp:Label>
                <br /><label>(country name)</label> 
            </td>
        </tr>
         <tr>
         <td>
                <label style="font-weight:bold">Total Number of Applications:</label>  
                <asp:Label ID="lblTotalNo" CssClass="rptLabelValue" runat="server" ></asp:Label>
                <br />
         </td>
            
        </tr>
        </table>
       </td></tr> 
       <tr><td align="left">
        <!--  Document Details  -->
        <div id="divPaxDocumentDetails" title="Document Details" style="border:solid 1px black;width:99%">
               <%-- <asp:DataList id="dlPaxDocuments" runat="server" RepeatColumns="9" Width="100%" 
                    GridLines="both" CellPadding="2" RepeatDirection="Horizontal" RepeatLayout="table" OnItemDataBound="dlPaxDocuments_ItemDataBound">
          <HeaderTemplate><label style="font-weight:bold">Documents Information</label> </HeaderTemplate>
               <ItemTemplate>
               <table cellpadding="0" cellspacing="0" border="0">
               <tr>
                <td></td>
                <td style="Width:100%"></td>
               </tr>
              <tr>
                   <td  align="left" valign="top"  >
                      <asp:Label  style="color:Navy;font-weight:bold;font-size:9pt;" ID="ITlblPaxName" Text='<%# Eval("FPAX_NAME") %>'  runat="server"  ></asp:Label>
                      <asp:HiddenField ID="IThdfPaxId" Value='<%# Eval("DD_FPAX_ID") %>'  runat="server" ></asp:HiddenField>
                   </td>
                   <td align="left" valign="top">
                      <asp:Label ID="ITlblDocName" Text='<%# Eval("DOC_NAME") %>' style="color:black;font-size:6pt"   runat="server" Width="100%" ></asp:Label>
                   </td>
               </tr>
               </table>
               </ItemTemplate>
                </asp:DataList>--%>
                
                
                               
                
                
                <%--<asp:DataList id="dlPaxDocuments" runat="server"  Width="100%" 
                    GridLines="both" CellPadding="2" RepeatDirection="Vertical"  RepeatLayout="table" OnItemDataBound="dlPaxDocuments_ItemDataBound">
          <HeaderTemplate><label style="font-weight:bold;font-size:15px">Documents Information</label> </HeaderTemplate>
               <ItemTemplate>
               <table cellpadding="0" cellspacing="0" border="0">
               <%--<tr>
                <td></td>
                <td style="Width:100%"></td>
               </tr>--%>
              <%--<tr>
                <td valign="top" align="left" > <asp:Label runat="server" ID="ITlblService"  CssClass="rptLabelNewValue" Text='<%# DataBinder.Eval(Container.DataItem, "doc_name")%>' Width="250px"></asp:Label></td>                           
              
               </tr>
              
               </table>
               </ItemTemplate>
                </asp:DataList>
              </div> 
    </td>    
    </tr>
   
    <tr>
        <td>
            <br />
            <table width="100%">
                <tr>
                     <td align="left">
                        <label> _________________________________________<br />Name & Signature of Applicant / Authorised Person </label>
                     </td>    
                     <td align="right" >
                        <label style="text-align:left">Name & Signature<br /><br /> _____________________________________ <br /> 
                            Received Passport & Other Original Documents</label>
                     </td>     
                </tr>
            </table>
        </td>
          
    </tr>
    <tr>
        <td align="left">
        <br /><br /><br />
            <label style="font-weight:bold">Declaration:</label><br />
            <label style="font-size:smaller"> I Hereby Confirms as under:</label><br />
            <table cellpadding="0" cellspacing="0"  style="Width:100%;font-size:smaller">
            <tr>
                <td style="width:20px"></td>
                <td></td>
            </tr>
            
                <tr>
                <td valign="top">i)</td>
                <td valign="top">The Above Document have been submitted to M/S Cozmo Travel for the express purpose of application of visas for the Country as mentioned
                                    above.I/We authorize M/S Cozmo Travel to submit the documents and to collect the documnets from the consulate on my/our bahalf.
                </td>
                </tr>
                <tr>
               <td valign="top">ii)</td>
               <td valign="top">I/We confirm that i am authorised by the other applications (other than me) to submit documents under the clause (I).
               </td>  
               </tr>
               <tr>
               <td valign="top">iii)</td>
               <td valign="top">I/We indemnify M/S Cozmo travel for any and all declarations made under the above application.I confirm that i am responsible for all declarations made under the application.
               </td> 
               </tr>
               <tr>   
                 <td valign="top">iv)</td>
                 <td valign="top">I/We confirm that i indemnify M/S Cozmo Travel for rejection of the application on any or all grounds by the Consulate.
                 </td> 
                 </tr>
                 <tr>
                  <td valign="top">v)</td>
                  <td valign="top">I/We indemnify M/S Cozmo travel towards any express or implied financial or other liabilities arising out of the above application.M/S Cozmo Travel shall not responsible for any liabilty arising out of non receipt of visas.M/S Cozmo Travel
                        will not be responsible for any cancellation or other charges including but not restricted to airlines,hotels,insurence etc.
                  </td> 
                  </tr>   
                  <tr>
                 <td valign="top">vi)</td>
                 <td valign="top">On submission of the application all charges mentioned above are payable to M/S Cozmo Travel including the Consulate fees and M/S Cozmo Travel service charges irrespective of receipt of visas.
                 </td> 
                 </tr>
                 <tr>
                <td valign="top">vii)</td>
                <td valign="top">I/We agree that indemnify M/S Cozmo travel will not take the responsibilities in case of loss of the documents including the passport by the Consulate. M/S Cozmo Travel will be indemnified from all claims including but not restricted to legel,financial or 
                        implied losses out of such an event.
                  </td>    
                
                </tr>   
                
                <tr>
                <td valign="top">viii)</td>
                 <td valign="top">Approval of your visa application is at the sole discretion of the Immigration officer at the Consulate / Embassy and we at Cozmo Travel do not influence their decision making for approval / rejection of your visa application
                  </td>          
                
                </tr>
                
                
                  <tr>
                <td valign="top">ix)</td>
                 <td valign="top">The rules of Immigration are subjected to change from time to time and your application could be subjected to the same while under process.
                  </td>          
                
                </tr>
                
                
                  <tr>
                <td valign="top">x)</td>
                 <td valign="top">In case of an application being rejected / put on hold Cozmo Travel would be in no position to refund the visa application or processing fees    
                  </td>          
                
                </tr>
                
                </table>        
                  
                             
                
        </td>
    </tr>
    </table>--%>
    </form>
</body>
</html>
