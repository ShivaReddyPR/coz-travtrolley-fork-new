﻿<%@ Page Title="Corporate Profile Master" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="CorpProfile.aspx.cs" Inherits="CozmoB2BWebApp.CorpProfile" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
	 <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />

    <script type="text/javascript" src="Scripts/jsBE/organictabs.jquery.js"></script>

    <script type="text/javascript" src="ash.js"></script>

    <link rel="stylesheet" href="css/style.css" />
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
	 <asp:HiddenField runat="server" ID="hdnApprovers" Value="" />
	 <asp:HiddenField runat="server" ID="hdnVisaDetails" Value="" />
	<asp:HiddenField runat="server" ID="hdnApproversList" Value="" />
	 <asp:HiddenField ID="hdfRTDetailId" runat="server" Value="0" />
	<asp:HiddenField runat="server" ID="hdfMode" Value="0"></asp:HiddenField>
	<asp:HiddenField runat="server" ID="hdnDelVD" Value="" />
	<script type="text/javascript">
		function pageLoad() {
			showCostCenters();
		}
		function showCostCenters() {
            var pType = $('#<%=ddlProfileType.ClientID%> option:selected').text().toUpperCase();
            if (pType == "BOOKER") {
                $('#divCostCenters').show();
            } else {
                $('#ctl00_cphTransaction_chkCostCenters').each(function () {
                    if ($(this).is(":checked")) {
                        $("[id*=ctl00_cphTransaction_chkCostCenters] input").attr("checked", "checked");
                    } else {
                        $("[id*=ctl00_cphTransaction_chkCostCenters] input").removeAttr("checked");
                    }                   
                });
                 $('#divCostCenters').hide();
            }
		}
		function isNumbers(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
		}
		var cal1;
        var cal2;
        var cal3;
        //var cal4;

        function init1() {

            cal1 = new YAHOO.widget.Calendar("cal1", "callContainer1");
            cal1.selectEvent.subscribe(setDate1);
            //alert('hi1');
            cal1.cfg.setProperty("close", true);
            cal1.render();
        }
        function init2() {

            cal2 = new YAHOO.widget.Calendar("cal2", "callContainer2");
            //alert('hi2');
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function init3() {

            cal3 = new YAHOO.widget.Calendar("cal3", "callContainer3");
            //alert('hi3');
            cal3.selectEvent.subscribe(setDate3);
            cal3.cfg.setProperty("close", true);
            cal3.render();
        }


        YAHOO.util.Event.addListener(window, "load", init1);
        YAHOO.util.Event.addListener(window, "load", init2);
        YAHOO.util.Event.addListener(window, "load", init3);
        //YAHOO.util.Event.addListener(window, "load", init4);
		 function showCalendar(container) {
            init1();
            // alert(document.getElementById(container));
            var containerId = container;
            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer2').style.display = "none";
                document.getElementById('callContainer3').style.display == "none";
                document.getElementById('callContainer4').style.display == "none";
            }
		}
		function showCalendar2(container) {
            init2();
            //  alert(document.getElementById(container));
            var containerId = container;

            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer1').style.display = "none";
                document.getElementById('callContainer3').style.display == "none";
                document.getElementById('callContainer4').style.display == "none";

            }
		}
		function showCalendar3(container) {
            init3();
            //  alert(document.getElementById(container));
            var containerId = container;

            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block";
                document.getElementById('callContainer1').style.display = "none";
                document.getElementById('callContainer2').style.display == "none";
                document.getElementById('callContainer4').style.display == "none";

            }
        }
  
		 function setDate1() {

            var date1 = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtDOI.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer1').style.display = "none";
        }

        function setDate2() {
            var date1 = cal2.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtDOE.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer2').style.display = "none";
        }

        function setDate3() {
            var date1 = cal3.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<% = txtDOB.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('callContainer3').style.display = "none";
        }

        

	</script>
	<div class="container">
        <div class="col-md-12">
            <div class="ns-h3">
                General Info
            </div>
			<% %>
			<%--<div class="row">--%>
				<div id="divClient" runat="server" style="display:none">
                                <div class="col-xs-8 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Client</label>
                                        <asp:DropDownList ID="ddlAgent" runat="server" class="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
					</div>
				<div id="divActive" runat="server" style="display:none">
                                <div class="col-xs-8 col-sm-4">
                                    <asp:CheckBox runat="server" ID="chkIsActive" Text="Is Active" Checked="true"/>
                                    </div>
					</div>
                           <%-- </div>--%>
			 <%--<div class="row">--%>
				 <div id="divTitle" runat="server" style="display:none">
                                <div class="col-xs-1 col-sm-1 col-md-1">
                                    <div class="form-group">
                                        <label for="">
                                            Title</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Surname">--%>
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlTitle" >
                                            <asp:ListItem Text="Mr." Value="Mr"></asp:ListItem>
                                            <asp:ListItem Text="Mrs." Value="Mrs"></asp:ListItem>
                                            <asp:ListItem Text="Mstr." Value="Mstr"></asp:ListItem>
                                            <asp:ListItem Text="Miss." Value="Miss"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
				 <div id="DivSurname" runat="server" style="display:none">
                                <div class="col-xs-5 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Surname/Last Name</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtSurname" placeholder="Surname"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divGivenName" runat="server" style="display:none">
                                <div class="col-xs-5 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Given Name/First Name</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtname" placeholder="Given Name"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
				  <div id="divMiddleName" runat="server" style="display:none">
                                  <div class="col-xs-5 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Middle Name</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtMiddleName" placeholder="Middle Name"></asp:TextBox>
                                    </div>
                                </div>
					  </div>
				  <div id="DivBatch" runat="server" style="display:none">
                                <div class="col-xs-5 col-sm-2">
                                    <div class="form-group">
                                        <label for="">
                                            Batch #</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtbatch" placeholder="Batch" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
					  </div>
                           <%-- </div>--%>
			 <div id="divDestignation" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Designation</label>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
			<div id="divDateofJoining" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                        <label>
                                            Date of Joining
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                            <uc1:DateControl ID="dcJoiningDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True"  />
                                        </div>
                                    </div> 
                                    </div>
                                </div>
				</div>
			<div id="DivEmploid" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Employee ID</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtEmpId" placeholder="Employee ID"></asp:TextBox>
                                    </div>
                                </div>
				</div>
			 <div id="divDivision" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                          <label for="">
                                            Division</label>
                                        <asp:DropDownList ID="ddlDivision" runat="server" class="form-control" placeholder="Division">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
			<div id="divCostCenter" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group"> 
                                         <label for="">
                                            Cost Centre</label>
                                        <asp:DropDownList ID="ddlCostCentre" runat="server" class="form-control" placeholder="Cost Centre">
                                        </asp:DropDownList>
                                       </div>
                                </div>
				</div>
            <div id="divTelephone" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="center-block">
                                            Telephone</label>
                                        <asp:TextBox Width="20%" class="form-control pull-left" runat="server" ID="txtPhoneCountryCode"
                                            placeholder="Code"></asp:TextBox>
                                        <asp:TextBox Width="80%" class="form-control pull-left" runat="server" ID="txtTelPhone"
                                            placeholder="Telephone" onKeyPress="return isNumbers(event)"></asp:TextBox>
                                    </div>
                                </div>
				</div>
			<div id="divMobile" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="center-block">
                                            Mobile</label>
                                        <asp:TextBox Width="20%" class="form-control pull-left" runat="server" ID="txtMobileCoutryCode"
                                            placeholder="Code"></asp:TextBox>
                                        <asp:TextBox Width="80%" class="form-control pull-left" runat="server" ID="txtMobileNo"
                                            placeholder="Mobile" onKeyPress="return isNumbers(event)"></asp:TextBox>
                                    </div>
                                </div>
				</div>
			<div id="divZipCode" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            ZIP CODE</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtFax" placeholder="Zip Code"></asp:TextBox>
                                    </div>
                                </div>
				</div>
             <div id="divEmail" runat="server" style="display:none">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Email</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtEmail"  placeholder="Email"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
			<div id="divGrade" runat="server" style="display:none">

                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Grade</label>
                                        <asp:DropDownList ID="ddlGrade" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
			<div id="divProfileType" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Profile Type</label>
                                       
                                        <asp:DropDownList ID="ddlProfileType" runat="server" class="form-control" onchange="showCostCenters()">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
			<div id="divApproveType" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Approver Type</label>
                                        <asp:DropDownList ID="ddlApproverType" runat="server" class="form-control">                                           
                                            <asp:ListItem Value="E"  >Expense Approver</asp:ListItem>
                                            <asp:ListItem Value="T"  >Travel Approver</asp:ListItem>
                                            <asp:ListItem Value="O"  >Offline Approver</asp:ListItem>
                                            <asp:ListItem Value="V"  >Visa Approver</asp:ListItem>
                                             <asp:ListItem Value="N" selected >No Approver</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
			<div id="divAffidavit" runat="server" style="display:none">
                                 <div class="col-md-4 col-lg-3">
                                     <div class="form-group" style="margin-top:30px">
                                         <asp:CheckBox ID="chkAffidavit"  runat="server" Text="Generate Affidavit" />
                                     </div>
                                     </div>
				</div>
			 <div class="col-md-4 col-lg-3" id="divCostCenters" style="display:none">
                                     <div class="form-group" style="margin-top:30px">
                                          <label for="">Booker Cost Centers</label>
                                        <asp:CheckBoxList ID="chkCostCenters" runat="server">
                                           
                                        </asp:CheckBoxList>
                                     </div>
                                     </div>
             <div id="divAddress" runat="server" style="display:none">                  
                      <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            Address</label>
                                        <%--<input type="text" class="form-control" id="" placeholder="Address Line 1">--%>
                                       <asp:TextBox class="form-control" runat="server" ID="txtAddress2" placeholder="Office Address"></asp:TextBox>                                         
                                    </div>
						  </div>
				 </div>
                                
            <br />
        <br />
        </div>
        
        <div class="col-md-12">
            <div class="ns-h3">
                Personal Info
            </div>
            <div id="divNationality" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Nationality</label>
                                        <asp:DropDownList ID="ddlNationality" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
			 <div id="divPassport" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Passport #</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtPassportNo" placeholder="Passport #"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
			 <div id="divDateOfIssue" runat="server" style="display:none">
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Date of Issue</label>
                                        <div class="input-group">
                                           
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDOI" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer1" style="position: absolute; top: 200x; left: 5%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar('callContainer1')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
				 </div>
			 <div id="divValidTill" runat="server" style="display:none">
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Valid Till</label>
                                        <div class="input-group">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDOE" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer2" style="position: absolute; top: 200x; left: -30%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <%--  <td> <asp:TextBox ID="txtDOITime" runat="server" CssClass="form-control pull-left" Width="60"></asp:TextBox> </td>--%>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar2('callContainer2')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
				 </div>
			 <div id="divCountryOfIssue" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Country of Issue</label>
                                     
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlcountryOfIssue">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
			 <div id="divResidency" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Residency</label>
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlResidency">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
			 <div id="divExcesAssistance" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            ExecAssistance</label>
                                       <asp:TextBox class="form-control" runat="server" ID="txtExecAssistance" placeholder="ExecAssistance"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
			 <div id="divDeligateSP" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                           Deligate Supervisor</label>
                                        <asp:DropDownList ID="ddlDeligateSupervisor" class="form-control" runat="server" AppendDataBoundItems="true">
                                          <asp:ListItem Value="0" Selected="true">Select Delegate </asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
				 </div>
			 <div id="divDateofBirth" runat="server" style="display:none">
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Date of Birth</label>
                                        <div class="input-group">
                                           
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control pull-left" Width="100px"></asp:TextBox>
                                                        <div class="clear" style="margin-left: 30px">
                                                            <div id="callContainer3" style="position: absolute; top: 200x; left: 5%; z-index: 9999;
                                                                display: none;">
                                                            </div>
                                                    </td>
                                                    <%--  <td> <asp:TextBox ID="txtDOITime" runat="server" CssClass="form-control pull-left" Width="60"></asp:TextBox> </td>--%>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="showCalendar3('callContainer3')">
                                                            <img src="images/call-cozmo.png" alt="Pick Date" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
			</div>
			<div id="divPlaceofBirth" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Place of Birth</label>
                                      
                                        <asp:DropDownList class="form-control" runat="server" ID="ddlPlaceOfBirth">
                                        </asp:DropDownList>
                                       
                                    </div>
                                </div>
				</div>
			<div id="divGender" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <div class="form-group">
                                        <label class="center-block">
                                            Gender</label>
                                        <div class="radio">
                                           
                                            <asp:DropDownList class="form-control" runat="server" ID="ddlGender" >
                                            <asp:ListItem Selected="True" Value="-1">Select Gender</asp:ListItem>
                                                    <asp:ListItem Value="1">Male</asp:ListItem>
                                                    <asp:ListItem Value="2">Female</asp:ListItem>
                                        </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
				</div>
			<div id="divMartial" runat="server" style="display:none">
                                  <div class="col-xs-12 col-sm-4 col-lg-3"> 
                                    <div class="form-group">
                                        <label> Martial Status: </label>
                                         <asp:DropDownList ID="ddlMartialStatus" runat="server" class="form-control">
                                             <asp:ListItem Text="--Select Martial Status--" Value="" />
                                              <asp:ListItem Text="Married" Value="Married" />
                                             <asp:ListItem Text="Single" Value="Single" />
                                        </asp:DropDownList>
                                        
                                        
                                    </div>
                                </div>
				</div>
			<div id="divVisadetailsDisplay" runat="server" style="display:none">
                     <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        VISA DETAILS</h4>
                                </div>
                            </div>     
			<div id="errMess1" style="display: none; color: Red; font-weight: bold; text-align: center;">
                            </div>
			 <div id="divVisaCountry" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Visa Country. <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" runat="server" ID="ddlVisaIssueCntry">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
			 <div id="divVisano" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Visa No. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtVisaNo"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
			 <div id="divIssueDate" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Issue Date. <span class="fcol_red">*</span></label>
                                        <uc1:DateControl ID="dcVisaIssueDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
				 </div>
			 <div id="divExpiryDate" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Expiry Date. <span class="fcol_red">*</span></label>
                                        <uc1:DateControl ID="dcVisaExpDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
				 </div>
			 <div id="divPlaceOfissue" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           Place Of Issue. <span class="fcol_red">*</span></label>
                                       <asp:DropDownList runat="server" ID="ddlvisaplaceofissue" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
				 </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addVisaDetails();" id="addVisaDetails"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelVisaDetails" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript: cancelVisaDetails();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: Green" id="updateVisaDetails" href="javascript:void(0);"
                                                class="add-more" onclick="return updateVisaDetails();"><span class=" glyphicon glyphicon-ok">
                                                </span></a>
                                </div>
				</div>
			<div id="divvisaDetailsChildDiv" style="display:none">
                        
                                <div class="col-md-12">
                                    <div id="visaDetailsChildDiv">
                                    </div>
                                </div>
                             
				</div>
			<div id="divNationalDeta" runat="server" style="display:none">
                      <div class="row">
                                <div class="col-xs-12">
                                    <h4>
                                        NATIONAL ID DETAILS</h4>
                                </div>
                            </div> 
			
			 <div id="divNationalityId" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                       <div class="form-group">
                                        <label for="">
                                            National ID No.(Ex:Emirates ID) <span class="fcol_red"></span></label>
                                        <asp:TextBox CssClass="form-control"  runat="server" ID="txtNationalIDNo" placeholder="National ID No"  MaxLength="20"></asp:TextBox>
                                    </div>
                                    </div>
                                </div>
				 </div>
                         <div id="divExpiryDateN" runat="server" style="display:none"> 
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Expiry Date. <span class="fcol_red"></span></label>
                                        <uc1:DateControl ID="dcNationalIDExpDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                                </div>
                   </div>   
			 <div id="divTaxDetailDisplay" runat="server" style="display:none">
                                <div class="col-xs-12">
                                    <h4>
                                        TAX DETAILS</h4>
                                </div>
				  <div id="divNumber" runat="server" style="display:none">
                                 <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                           Number. <span class="fcol_red">*</span></label>
                                       <asp:TextBox runat="server" ID="txtGSTNumber" class="form-control" placeholder="Number"></asp:TextBox>
                                    </div>
                                </div>
					  </div>
				 <div id="divEmailTax" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Email. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTEmail" placeholder="Email"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divName" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Name. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTName" placeholder="Name"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divPhone" runat="server" style="display:none">
                                  <div class="col-xs-4 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            Phone. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTPhone" placeholder="Phone"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
				 <div id="divAddressTax" runat="server" style="display:none">
				 <div class="col-xs-6 col-sm-6 col-md-4">
                                    <div class="form-group">
                                       <div class="form-group">
                                        <label for="">
                                               Address.  <span class="fcol_red"></span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGSTAddress" placeholder="Address" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
                                    </div>
                                    </div>
                                </div>
					 </div>
                            </div>
			
                           
             <br />
        <br />
        </div>

        <div class="col-md-12">
            <div class="ns-h3">
                Travel Info
            </div>
            <div id="divTravelDomestic" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Domestic Eligibility</label>
                                        <asp:DropDownList runat="server" ID="ddlDomestic" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
			 <div id="divTravelInternal" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            International Eligibility</label>
                                        <asp:DropDownList runat="server" ID="ddlinternational" CssClass="form-control">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
				 </div>
			 <div id="divTravelSeat" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Seat Preference</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtSeatPref" placeholder="Seat Preference"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
			 <div id="divTravelMeal" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Meal Request</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtMealRequest" placeholder="Meal Request"></asp:TextBox>
                                    </div>
                                </div>
				 </div>
			 <div id="divTravelGds" runat="server" style="display:none">
                                   <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                           GDSProfilePNR</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtGDSProfilePNR" placeholder="GDS Profile PNR"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
			 <div id="DivGdsSettindsDisplay" runat="server" style="display:none">
                                <div class="col-xs-12">
                                    <h4>
                                       GDS SETTINGS</h4>
                                </div>
				  <div id="DivTravelSettingsGds" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            GDS. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDS"></asp:TextBox>
                                    </div>
                                </div>
					  </div>
				 <div id="divTravelDescription" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           Description. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSDescription"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divTravelOwnerPcc" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            OwnerPCC. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSOwnerPCC"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divTravelQueNO" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Queue No. <span class="fcol_red">*</span></label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSQueueNo"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divTravelExtra" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           extraCommand. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSextraCommand"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divTravelCorporateSsr" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           corporateSSR. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDScorporateSSR"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divTravelOsi" runat="server" style="display:none">
                                 <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           OSI. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSOSI"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
				 <div id="divTravelRemarks" runat="server" style="display:none">
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                           Remraks. <span class="fcol_red">*</span></label>
                                       <asp:TextBox CssClass="form-control" runat="server" ID="txtGDSRemraks" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
                                    </div>
                                </div>
					 </div>
                                <div class="col-xs-1">
                                    <a href="javascript:void(0);" onclick="return addVisaDetails();" id="addVisaDetails"
                                        class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a><a style="display: none;
                                            color: red;" id="cancelVisaDetails" href="javascript:void(0);" class="add-more"
                                            onclick=" javascript: cancelVisaDetails();"><span class=" glyphicon glyphicon-remove">
                                            </span></a><a style="display: none; color: Green" id="updateVisaDetails" href="javascript:void(0);"
                                                class="add-more" onclick="return updateVisaDetails();"><span class=" glyphicon glyphicon-ok">
                                                </span></a>
                                </div>
                            
                            </div>
        </div>
		 <div class="col-md-12">
            <div class="ns-h3">
                HOTEL INFO
            </div>
             <div id="divHotelRoomtype" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-3">
                                    <div class="form-group">
                                        <label for="">
                                            Room Type</label>
                                        <asp:DropDownList ID="ddlRoomType" runat="server" class="form-control">
                                            <asp:ListItem Text="Select Roomtype" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Standard Single" Value="STDS"></asp:ListItem>
                                            <asp:ListItem Text="Delux Single" Value="DLXS"></asp:ListItem>
                                            <asp:ListItem Text="Standard Double" Value="STDD"></asp:ListItem>
                                            <asp:ListItem Text="Delux Double" Value="DLXD"></asp:ListItem>
                                            <asp:ListItem Text="Suit" Value="SUIT"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
			  <div id="divhotelRemarks" runat="server" style="display:none">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <label for="">
                                            Remarks/Comments</label>
                                        <%--<textarea class="form-control"></textarea>--%>
                                        <asp:TextBox class="form-control" runat="server" ID="txtRemarks" placeholder="Remarks/Comments"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
        </div>
		<div class="col-md-12">
            <div class="ns-h3">
               ADD DETAILS
            </div>
           <div id="divAddCostCenter" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group"> 
                                         <label for="">
                                            Cost Centre</label>
                                        <asp:DropDownList ID="ddlAddCostCenter" runat="server" class="form-control" placeholder="Cost Centre">
                                        </asp:DropDownList>
                                       </div>
                                </div>
				</div>
			<div id="divAddEmpId" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            Employee ID</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtAddEmpId" placeholder="Employee ID"></asp:TextBox>
                                    </div>
                                </div>
				</div>
			<div id="divAddErpCompany" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            ERP COMPANY</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtAddErpCompany" placeholder=" ERP COMPANY"></asp:TextBox>
                                    </div>
                                </div>
				</div>
			<div id="divAddTrip" runat="server" style="display:none">
                                <div class="col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label for="">
                                            TRIP DESCRIPTION</label>
                                        <asp:TextBox class="form-control" runat="server" ID="txtAddTrip" placeholder="TRIP DESCRIPTION"></asp:TextBox>
                                    </div>
                                </div>
				</div>
			<div id="divAddLocation" runat="server" style="display:none">
			 <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label for="">
                                            Location</label>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control" placeholder="Location" AppendDataBoundItems="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
			<div id="divAddAproveName" runat="server" style="display:none">
			<div class="col-xs-4 col-sm-2 col-md-2">
                                    <div class="form-group">
                                        <label for="">
                                            Approver Name <span class="fcol_red">*</span></label>
                                        <asp:DropDownList CssClass="form-control" ID="ddlAppName" runat="server">
                                             <asp:ListItem Value="-1" >Select Approver Name</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
				</div>
        </div>
		<div class="row top-buffer">
                        <div class="col-md-12">
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b button pull-right" OnClick="btnClear_Click"/>
			 <div id="divbtnSearch" runat="server" style="display:none">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b button pull-right"  OnClick="btnSearch_Click" />
				 </div>
            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" CssClass="btn but_b button  pull-right" OnClientClick="return Save();"/>
           </div>
                    </div>
		
    </div>
	<script type="text/javascript">
		 function GridSearchValidation() {
            var msg = '';
            if (document.getElementById('ctl00_cphSearch_txtgSurname').value == '' && document.getElementById('ctl00_cphSearch_txtGname').value == '' &&
               document.getElementById('ctl00_cphSearch_txtGEmpid').value == '' && document.getElementById('ctl00_cphSearch_txtGEmail').value == '') {
                msg = 'Please enter any value for SurName or Name or Empid or Email';
            }
            if (msg != '') {
                alert(msg);
                return false;
            }
            return true;

        }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
	 <div>
        <div class="col-md-12">
            <div class="col-md-1">
                surname:
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtgSurname" runat="server" Width="90px"></asp:TextBox>
            </div>
            <div class="col-md-1">
                Name:
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtGname" runat="server" Width="90px"></asp:TextBox>
            </div>
            <div class="col-md-1">
                EmployeeId:
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtGEmpid" runat="server" Width="90px"></asp:TextBox>
            </div>
            <div class="col-md-1">
                Email:
            </div>
            <div class="col-md-2">
                 <asp:TextBox ID="txtGEmail" runat="server" Width="90px"></asp:TextBox>
            </div>
            <div class="col-md-1">
            <asp:Button ID="btnGSearch" runat="server" Text="Search" OnClick="btnGSearch_Click"  OnClientClick="return GridSearchValidation()"/>
                </div>
        </div>
    </div>
     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ProfileId"
        EmptyDataText="No Corporate Profile List!" AutoGenerateColumns="false" PageSize="17"
        GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
        CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
          
        </HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True" />
            
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                   

                                   
                    <label>
                        Sur Name</label>

                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblSurName" runat="server" Text='<%# Eval("SurName") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("SurName") %>' Width="70px"></asp:Label>
                  
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                   
                    <label>
                        Name</label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("Name") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                                   
                    <label>
                        Employee Id</label>
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblEmpId" runat="server" Text='<%# Eval("EmployeeId") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("EmployeeId") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                                
                    <label>
                        Phone</label>
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblPhone" runat="server" Text='<%# Eval("Telephone") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Telephone") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                   
                    <label>
                        Mobile Phone</label>
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblMobPhone" runat="server" Text='<%# Eval("Mobilephone") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Mobilephone") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                   <label> Email </label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblEmail" runat="server" Text='<%# Eval("Email") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("Email") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
