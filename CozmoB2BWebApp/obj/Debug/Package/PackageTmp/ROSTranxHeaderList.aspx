 <%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSTranxHeaderListGUI" Title="ROS Tranx Header"  ValidateRequest="false" EnableEventValidation="false" Codebehind="ROSTranxHeaderList.aspx.cs" %>
  <%@ MasterType VirtualPath="~/TransactionBE.master" %>
 <%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphtransaction" Runat="Server">
<asp:HiddenField runat="server" id="hdfStatusVal" ></asp:HiddenField> <%--which status value need to be bound--%>
<asp:HiddenField runat="server" id="hdfAssignedCount" ></asp:HiddenField><%--to get newly assigned count--%>
<asp:HiddenField runat="server" id="hdfDriverStatus" ></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfDirection" ></asp:HiddenField>
<table cellpadding="0" cellspacing="0" class="label" >
            <tr>
                <td style="width:100px" align="right">
                <%--<a style="cursor:Hand;font-weight:bold;font-size:8pt;color:Black;" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a>--%>
                </td>
          </tr>
         </table>
         
         
         
         
         <div class="paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
        
        
        <div> 
        <div class="col-md-12"> 
        
        <table> 
        <td><asp:Label ID="lblStaff" runat="server" Text="Staff:" ></asp:Label> </td>
        <td><asp:DropDownList CssClass="form-control" ID="ddlStaff" runat="server" Enabled="true" Width="160px">  </asp:DropDownList> </td>


        <td align="right"> <asp:Label ID="lblFromDate" runat="server" Text="From Date:"  Width="100px"></asp:Label></td>
        <td ><uc1:DateControl ID="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></td>
        
        
      <td align="right"><asp:Label ID="lblToDate" runat="server" Text="To Date:"  Width="100px"></asp:Label></td>
        <td><uc1:DateControl ID="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></td>
        

        

        </table>
        
        
        </div>
        
       
        
        <div class="clearfix"> </div>
        
        </div>
        
        
         <div class="margin-top-10"> 
        
        
          <div class="col-md-12 marbot_10">
         
         
         <asp:Button ID="btnSearch" Text="All" runat="server" OnClientClick="return ValidateParam();" CssClass="button"  OnClick ="btnSearch_Click" ></asp:Button>
         
         <asp:Button  Visible="false" ID="btnPending" Text="Pending" runat="server" OnClientClick="return ValidateParam();" CssClass="button" OnClick ="btnPending_Click" ></asp:Button>
         
         <asp:Button ID="btnConfirmed" Text="Scheduled" runat="server" OnClientClick="return ValidateParam();" CssClass="button"   OnClick ="btnConfirmed_Click" ></asp:Button>
         
         <asp:Button ID="btnAssigned" Text="Assigned-Home" runat="server" OnClientClick="return ValidateParam();"   CssClass="button" OnClick ="btnAssigned_Click" ></asp:Button>
         
         <asp:Button  ID="btnAssignedA" Text="Assigned-Airport" runat="server" OnClientClick="return ValidateParam();"   CssClass="button" OnClick ="btnAssignedA_Click" ></asp:Button>
         
         <asp:Button ID="btnPickedUp" Text="Picked Up" runat="server" OnClientClick="return ValidateParam();"  CssClass="button" OnClick ="btnPickedUp_Click" ></asp:Button>
         
         
         <asp:Button  ID="btnNotResp" Text="Not Responding" runat="server" OnClientClick="return ValidateParam();"  CssClass="button" OnClick ="btnNotResp_Click" ></asp:Button>
         
        
         
         <asp:Button ID="btnCompleted" Text="Completed" runat="server" OnClientClick="return ValidateParam();"  CssClass="button" OnClick ="btnCompleted_Click" ></asp:Button>
         
         
         
            
         
         
          </div>
        
        
         <div class="clearfix"> </div>
        
        </div>
        
    </asp:Panel>
    </div>
<div class="grdScrlTransko" style="margin-top:-2px; position:relative!important; width:100%;" >
    
    <asp:Updatepanel id="uplGrid" runat="server" updatemode="conditional">
  <ContentTemplate> 
 
<asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>  
    <table id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td>
    
    <asp:HiddenField ID="hdfVisaId" runat="server" ></asp:HiddenField>
     <asp:HiddenField runat="server" id="hdfVisaDetailsMode"  Value="0"></asp:HiddenField>
    <asp:GridView ID="gvRosterList" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="RM_ID" 
    EmptyDataText="No Roster List!" AutoGenerateColumns="false" PageSize="25" 
            GridLines="Horizontal"  CssClass="grdTable"  CellPadding="4" CellSpacing="0" PagerSettings-Mode="NumericFirstLast"    
            OnRowDatabound="gvRosterList_RowDataBound" OnPageIndexChanging="gvRosterList_PageIndexChanging"
            OnRowEditing="gvRosterList_RowEditing"  OnRowCancelingEdit="gvRosterList_RowCancelingEdit" 
            OnRowUpdating="gvRosterList_RowUpdating"  OnRowDeleting="gvRosterList_RowDeleting">
        
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvRow01" />    
    <Columns> 
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label" ToolTip='<%# Container.DataItemIndex+1 %>' Width="40px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  
    
       <asp:TemplateField>
    <HeaderStyle VerticalAlign="middle" />
    <HeaderTemplate>
      <cc2:Filter ID="HTtxtLocation" Width="70px" HeaderText="Location" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblLocation" Width="130px" runat="server" Text='<%# Eval("FROM_LOC_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("FROM_LOC_NAME") %>' ></asp:Label>                
    </ItemTemplate>  
    <EditItemTemplate>
       <asp:Label ID="EITlblLocation" Width="130px" runat="server" Text='<%# Eval("FROM_LOC_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("FROM_LOC_NAME") %>' ></asp:Label>                
    </EditItemTemplate>              
    </asp:TemplateField>
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtStaffId" Width="70px" HeaderText="Staff Id" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblStaffId" runat="server" Text='<%# Eval("[STAFF_ID]") %>' CssClass="label grdof" ToolTip='<%# Eval("[STAFF_ID]") %>' Width="100px"></asp:Label>
    <asp:HiddenField id="IThdfRMId" runat="server" Value='<%# Bind("RM_ID") %>'></asp:HiddenField>
    
    </ItemTemplate>    
    <EditItemTemplate>
       <asp:Label ID="EITlblStaffid" Width="130px" runat="server" Text='<%# Eval("STAFF_ID") %>' CssClass="label grdof"  ToolTip='<%# Eval("STAFF_ID") %>' ></asp:Label>                
    </EditItemTemplate>     
    </asp:TemplateField>  
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate> 
    <cc2:Filter ID="HTtxtStaffName" HeaderText="Staff Name" CssClass="inputEnabled" Width="80px" OnClick="Filter_Click" runat="server"  />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <%--<asp:TextBox ID="ITtxtTrackingNo" runat="server" Width="150px" MaxLength="250" CssClass="InputEnabled" ></asp:TextBox>--%>
    <asp:Label ID="ITlblStaffName" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("EMP_NAME") %>' Width="80px"></asp:Label>
    </ItemTemplate>    
    
    <EditItemTemplate>
       <asp:Label ID="EITlblStaffName" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("EMP_NAME") %>' Width="80px"></asp:Label>
    </EditItemTemplate> 
    </asp:TemplateField>  
    
   <%--  <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate> 
    <cc2:Filter ID="HTtxtSigIn" HeaderText="SigIn" CssClass="inputEnabled" Width="80px" OnClick="Filter_Click" runat="server"  />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
   
    <asp:Label ID="ITlblSigIn" runat="server" Text='<%# Eval("RM_SIGN_IN") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_SIGN_IN") %>' Width="80px"></asp:Label>
    </ItemTemplate>    
    
    <EditItemTemplate>
       <asp:Label ID="EITlblSigIn" runat="server" Text='<%# Eval("RM_SIGN_IN") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_SIGN_IN") %>' Width="80px"></asp:Label>
    </EditItemTemplate> 
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate> 
    <cc2:Filter ID="HTtxtSigOut" HeaderText="Sign Out" CssClass="inputEnabled" Width="80px" OnClick="Filter_Click" runat="server"  />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
   
    <asp:Label ID="ITlblSigOut" runat="server" Text='<%# Eval("RM_SIGN_OUT") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_SIGN_OUT") %>' Width="80px"></asp:Label>
    </ItemTemplate>    
    
    <EditItemTemplate>
       <asp:Label ID="EITlblSigOut" runat="server" Text='<%# Eval("RM_SIGN_OUT") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_SIGN_OUT") %>' Width="80px"></asp:Label>
    </EditItemTemplate> 
    </asp:TemplateField>--%>     
    
     <asp:TemplateField>
    <HeaderStyle  />
    <ItemStyle />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtFromLoc" CssClass="inputEnabled" HeaderText="From" Width="80px" OnClick="Filter_Click" runat="server"  />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <%--<asp:Label ID="ITlblFromLoc" runat="server" Text='<%# Eval("FROM_LOC") %>' CssClass="label grdof"   ToolTip='<%# Eval("FROM_LOC_DETAILS") %>' Width="100px"></asp:Label>--%>
    <asp:LinkButton ID="ITlblFromLoc" runat="server" Text='<%# Eval("FROM_LOC") %>' CssClass="label grdof" ForeColor="Blue" OnClientClick="return ShowLocDetails(this.id,'F');"  ToolTip='<%# Eval("FROM_LOC_DETAILS") %>' Width="100px"></asp:LinkButton>
     <asp:HiddenField id="IThdfFromLocName" runat="server" value='<%# Eval("FROM_LOC_NAME") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfFromLocDetails" runat="server" value='<%# Eval("ROS_FROM_LOC_DET") %>'></asp:HiddenField> 
    <%--<asp:HiddenField id="IThdfFromLocDetails" runat="server" value='<%# Eval("FROM_LOC_DETAILS") %>'></asp:HiddenField>    --%>
    <asp:HiddenField id="IThdfFromLocMap" runat="server" value='<%# Eval("ROS_FROM_LOC_MAP") %>'></asp:HiddenField>
    <div id="ITdivFLocDetails"  runat="server" class="divLocation2"   align="left">    
    
    <div class="showMsgHeading"> Location  <asp:LinkButton ID="ITFbtnOk" CssClass="closex pull-right" runat="server" Text="X"> </asp:LinkButton> </div>                    
                        
                        <div class="pad_10"> <asp:Label  ID="ITlblFLocName" style="font-size:12px;" runat="server" Text=""></asp:Label>  <br />                    
                        <asp:Label ID="ITlblFLocDtls" style="font-size:12px;" runat="server" Text=""></asp:Label>  <br />                    
                        <asp:Label ID="ITlblFLocMap" style="font-size:12px;" runat="server" Text=""></asp:Label>  <br /></div> 
                      
                     
                       
                       
                       
                      
                      </div>
    </ItemTemplate>  
     <EditItemTemplate>
        <asp:DropDownList ID="EITddlFromLocation" runat="server" style="text-transform:capitalize;" onChange="SetLocDetails(this.id,this.value,'F');" CssClass="inputDdlEnabled"  
        Width="120px"></asp:DropDownList>
        <asp:TextBox ID="EITtxtFromLocDetails" runat="server" Text='<%# Eval("ROS_FROM_LOC_DET") %>' CssClass="inputEnabled"  TextMode="MultiLine" Height="40px" > </asp:TextBox>
         <asp:HiddenField runat="server" ID="EIThdfFromLocType" Value='<%# Eval("RM_FROM_LOC_TYPE") %>' />
         <asp:HiddenField runat="server" ID="EIThdfFromLocId" Value='<%# Eval("RM_FROM_LOC_ID") %>' />
         <asp:HiddenField runat="server" ID="EIThdfFromIDWithName" Value='<%# Eval("FROM_LOCID_WITH_NAME") %>' />
    </EditItemTemplate>     
    </asp:TemplateField>   
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc2:Filter ID="HTDate" Width="80px" CssClass="inputEnabled" HeaderText="Date" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblDate" runat="server" Text='<%# CZDateFormat(Eval("RM_DATE")) %>' CssClass="label grdof" ToolTip='<%# (Eval("RM_DATE")) %>' Width="100px"></asp:Label>
    </ItemTemplate>    
     <EditItemTemplate>
        <uc1:DateControl Id="EITdcDate" Enabled="true"    runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
       </EditItemTemplate>     
    </asp:TemplateField>  
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc2:Filter ID="HTTime" Width="80px" CssClass="inputEnabled" HeaderText="Time" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblTime" runat="server" Text='<%# Eval("RM_TIME") %>' CssClass="label grdof" ToolTip='<%# Eval("RM_TIME") %>' Width="100px"></asp:Label>
    </ItemTemplate>    
    <EditItemTemplate>
        <uc1:DateControl Id="EITdcTime" Enabled="true"   runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" TimeOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
       </EditItemTemplate>  
    </asp:TemplateField> 
    <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtDriverName" HeaderText="Assigned Driver" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
     <asp:Label ID="ITlblDriver" runat="server" Text='<%# Eval("DRIVER_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("DRIVER_NAME") %>' Width="125px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
   <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtVehicleName" HeaderText="Vehicle No" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
         <asp:Label ID="ITlblVehicleName" runat="server" Text='<%# Eval("ROS_VEHICLE_NO") %>' CssClass="label grdof"  ToolTip='<%# Eval("ROS_VEHICLE_NO") %>' Width="125px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtToLoc" Width="80px" CssClass="inputEnabled" HeaderText="To" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:LinkButton  ID="ITlblToLoc" runat="server" Text='<%# Eval("TO_LOC") %>' CssClass="label grdof" ForeColor="Blue"  ToolTip='<%# Eval("TO_LOC_DETAILS") %>' OnClientClick="return ShowLocDetails(this.id,'T');" Width="100px"></asp:LinkButton>
     <asp:HiddenField id="IThdfToLocName" runat="server" value='<%# Eval("TO_LOC_NAME") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfToLocDetails" runat="server" value='<%# Eval("ROS_TO_LOC_DET") %>'></asp:HiddenField>    
    <asp:HiddenField id="IThdfToLocMap" runat="server" value='<%# Eval("ROS_TO_LOC_MAP") %>'></asp:HiddenField>
     <div id="ITdivTLocDetails"  runat="server" class="divLocation2"   align="left">   
     
     <div class="showMsgHeading"> Location  <asp:LinkButton ID="ITTbtnOk" CssClass="closex pull-right" runat="server" Text="X"> </asp:LinkButton> </div>    
     
                       <div class="pad_10">       <asp:Label  ID="ITlblTLocName" style="font-size:12px;" runat="server" Text=""></asp:Label>  <br />                    
                        <asp:Label ID="ITlblTLocDtls" style="font-size:12px;" runat="server" Text=""></asp:Label>  <br />                    
                        <asp:Label ID="ITlblTLocMap" style="font-size:12px;" runat="server" Text=""></asp:Label>  <br /> </div>
                      
                          
                          
                          
                        
                          
                          
                 
                       
                       
                      
                      </div>
    </ItemTemplate>   
    
    <EditItemTemplate>
        <asp:DropDownList ID="EITddlToLocation" runat="server" style="text-transform:capitalize;" onChange="SetLocDetails(this.id,this.value,'T');" CssClass="inputDdlEnabled"  
        Width="120px"></asp:DropDownList>
          <asp:TextBox ID="EITtxtToLocDetails" runat="server" Text='<%# Eval("ROS_TO_LOC_DET") %>'  CssClass="inputEnabled"  TextMode="MultiLine" Height="40px" > </asp:TextBox>
         <asp:HiddenField runat="server" ID="EIThdfToLocType" Value='<%# Eval("RM_TO_LOC_TYPE") %>' />
         <asp:HiddenField runat="server" ID="EIThdfToLocId" Value='<%# Eval("RM_TO_LOC_ID") %>' />
         <asp:HiddenField runat="server" ID="EIThdfToIDWithName" Value='<%# Eval("TO_LOCID_WITH_NAME") %>' />
    </EditItemTemplate>     
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtFlightNo" HeaderText="Flight No" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server"  />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblFlightNo" runat="server" Text='<%# Eval("RM_FLIGHTNO") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_FLIGHTNO") %>' Width="120px"></asp:Label>
    </ItemTemplate> 
     <EditItemTemplate>
        <asp:TextBox ID="EITtxtFlightNo" runat="server" style="text-transform:capitalize;" CssClass="inputEnabled"  Text='<%# Eval("RM_FLIGHTNO") %>' 
        Width="120px"></asp:TextBox>
         <asp:HiddenField id="EIThdfRMId" runat="server" Value='<%# Bind("RM_ID") %>'></asp:HiddenField>
    </EditItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc2:Filter ID="HTtxtFromSector"  Width="100px" CssClass="inputEnabled" HeaderText="From Sector" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblFromSector" runat="server" Text='<%# Eval("RM_FROM_SECTOR") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_FROM_SECTOR") %>' Width="100px"></asp:Label>                
    </ItemTemplate>  
    <EditItemTemplate>
        <asp:TextBox ID="EITtxtFromSector" runat="server" style="text-transform:capitalize;"  CssClass="inputEnabled"  Text='<%# Eval("RM_FROM_SECTOR") %>' 
        Width="120px"></asp:TextBox>
    </EditItemTemplate>      
    </asp:TemplateField>
     <asp:TemplateField>
    <HeaderTemplate >
    <cc2:Filter ID="HTtxtToSector"  Width="100px" CssClass="inputEnabled" HeaderText="To Sector" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblToSector" runat="server" Text='<%# Eval("RM_TO_SECTOR") %>' CssClass="label grdof"  ToolTip='<%# Eval("RM_TO_SECTOR") %>' Width="125px"></asp:Label>                
    </ItemTemplate>  
     <EditItemTemplate>
        <asp:TextBox ID="EITtxtToSector" runat="server" style="text-transform:capitalize;" CssClass="inputEnabled"  Text='<%# Eval("RM_TO_SECTOR") %>' 
        Width="120px"></asp:TextBox>
    </EditItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtTripStatus" HeaderText="Trip Status" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
     <asp:Label ID="ITlblTripStatus" runat="server" Text='<%# Eval("ROS_TRIP_STATUS_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("ROS_TRIP_STATUS_NAME") %>' Width="125px"></asp:Label>                
    </ItemTemplate>   
   
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtDriverStatus" HeaderText="Driver Status" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
     <asp:Label ID="ITlblDriverStatus" runat="server" Text='<%# Eval("ROS_DRIVER_STATUS_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("ROS_DRIVER_STATUS_NAME") %>' Width="125px"></asp:Label>                
    </ItemTemplate>   
     
    </asp:TemplateField>
    
      <asp:TemplateField>
    <HeaderStyle VerticalAlign="middle" />
    <HeaderTemplate>
      <cc2:Filter ID="HTtxtTripId" Width="70px" HeaderText="Trip Id" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblRMid" Width="130px" runat="server" Text='<%# Eval("ROS_TRIPID") %>' CssClass="label grdof"  ToolTip='<%# Eval("ROS_TRIPID") %>' ></asp:Label>                
    </ItemTemplate>  
    <EditItemTemplate>
       <asp:Label ID="EITlblRMid" Width="130px" runat="server" Text='<%# Eval("ROS_TRIPID") %>' CssClass="label grdof"  ToolTip='<%# Eval("ROS_TRIPID") %>' ></asp:Label>                
    </EditItemTemplate>              
    </asp:TemplateField>
    
    
     <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtDriver" HeaderText="Assigned Driver" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:HiddenField id="IThdfDriver" runat="server" value='<%# Eval("ROS_DRIVER_ID") %>'></asp:HiddenField>
    <asp:CheckBox ID="ITchkSelect" OnClick="enableGridControls(this.id);" runat="server" Width="20px" Visible="false" Checked='<%# Eval("checked_status").ToString()=="true"%>' ToolTip="Tick To Edit Driver and Vehicle" ></asp:CheckBox>
    <asp:DropDownList ID="ITddlDriver" CssClass="inputDdlEnabled" Enabled="true" Width="85px" runat="server"></asp:DropDownList>
    </ItemTemplate>    
    </asp:TemplateField>
   <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtVehicle" HeaderText="Vehicle No" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:HiddenField id="IThdfVehicle" runat="server" value='<%# Eval("VEHICLE_ID") %>'></asp:HiddenField>
    <asp:DropDownList ID="ITddlVehicle" CssClass="inputDdlEnabled" Enabled="true" Width="85px" runat="server"></asp:DropDownList>
    </ItemTemplate>    
    </asp:TemplateField>
    
     
    
      <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtRemarks" HeaderText="Remarks" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
     <asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("ROS_REMARKS") %>' CssClass="label grdof"  ToolTip='<%# Eval("ROS_REMARKS") %>' Width="125px"></asp:Label>                
    </ItemTemplate>   
    <EditItemTemplate>
        <asp:TextBox ID="EITtxtRemarks" runat="server" style="text-transform:capitalize;" CssClass="inputEnabled"  Text='<%# Eval("ROS_REMARKS") %>' 
        Width="150px"></asp:TextBox>
         
    </EditItemTemplate>   
    </asp:TemplateField>
    
     <asp:TemplateField>
    <HeaderStyle Width="30px" />
    <HeaderTemplate></HeaderTemplate>
    <ItemTemplate>
    <asp:LinkButton ID="lnkEdit" Visible='<%# (bool)(Eval("ROS_TRIP_STATUS").ToString()=="P"?true:false) %>' runat="server" CssClass="linup" Text="Edit" CommandName="Edit"  Width="20px" >
    </asp:LinkButton>
    <asp:ImageButton ID = "ibtnDelete" runat="server"  Visible='<%# (bool)(Eval("ROS_DRIVER_STATUS").ToString()=="N"?true:false) %>'  ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="ret ValidateDelete()"  ></asp:ImageButton>
    </ItemTemplate>
    <EditItemTemplate>
    <asp:LinkButton ID="lnkUpdate" CssClass="linup" runat="server" Width="52px" Text="Update" CommandName="Update" ></asp:LinkButton><br />
    <asp:LinkButton ID="lnkCancel" CssClass="linup" runat="server" Text="Cancel" Width="52px" CommandName="Cancel"></asp:LinkButton>
    </EditItemTemplate>                                       
    </asp:TemplateField>
      
   
    </Columns>           
    </asp:GridView>
    </td></tr>
    <tr>
        <td align="left">
        <table>
        <tr>
        <td width="80px"><asp:Button OnClick="btnExport_Click"  runat="server" id="btnExport" Text="Export To Excel" CssClass="button" /> </td>
        <td width="80px">
        <asp:Button OnClick="btnUpdate_Click"  runat="server" id="btnUpdate" Text="Update Roster" CssClass="button" /> 
        <td>
        </td>
        </tr>
        </table>
         </td>
    </tr>
    </table>
    </ContentTemplate>
    </asp:Updatepanel>
    </div>
  

<asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<asp:Label style="COLOR: #dd1f10" id="lblError" runat="server"></asp:Label> 



<div>
    <asp:DataGrid ID="dgRosterList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <%--<asp:BoundColumn HeaderText="S.No." DataField="receipt_number" HeaderStyle-Font-Bold="true" ></asp:BoundColumn>--%>
    <asp:BoundColumn HeaderText="Location" DataField="FROM_LOC_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Staff Id" DataField="STAFF_ID" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Staff Name" DataField="EMP_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
     <asp:BoundColumn HeaderText="From" DataField="FROM_LOC" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Date" DataField="RM_DATE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Time" DataField="RM_TIME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="Assigned Driver" DataField="DRIVER_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Vehicle No" DataField="ROS_VEHICLE_NO" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="To" DataField="TO_LOC" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Flight No" DataField="RM_FLIGHTNO" HeaderStyle-Font-Bold="true"></asp:BoundColumn>   
    <asp:BoundColumn HeaderText="From Sector" DataField="RM_FROM_SECTOR" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="To Sector" DataField="RM_TO_SECTOR" ItemStyle-Font-Bold="true" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Trip Status" DataField="ROS_TRIP_STATUS_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Driver Status" DataField="ROS_DRIVER_STATUS_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Trip Id" DataField="RM_ID" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Remarks" DataField="ROS_REMARKS" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
   
    </Columns>
    </asp:DataGrid>
    
    
    
    </div>
<script  type="text/javascript">

//    function ShowHide(div) {
//        if (getElement('hdfParam').value == '1') {
//            //alert(document.getElementById(div));
//            //alert(document.getElementById(div).innerHtml);
//            //getElement(div).value;
//            document.getElementById('ancParam').innerHTML = 'Show Param'
//            document.getElementById('divButton').style.display = document.getElementById(div).style.display = 'none';
//            document.getElementById(div).style.display = 'none';
//            getElement('hdfParam').value = '0';
//        }
//        else {
//            document.getElementById('ancParam').innerHTML = 'Hide Param'
//            document.getElementById('ancParam').value = 'Hide Param'
//            document.getElementById(div).style.display = 'block';
//            document.getElementById('divButton').style.display = document.getElementById(div).style.display = 'block';
//            getElement('hdfParam').value = '1';
//        }
//    }
    function ValidateParam() {
        
        var fromDate = GetDateObject('ctl00_cphTransaction_dcFromDate');
//        var fromTime = getElement('dcFromDate_Time');
        var toDate = GetDateObject('ctl00_cphTransaction_dcToDate');
//        var toTime = getElement('dcToDate_Time');
        if (fromDate == null) addMessage('Please select From Date !', '');
        //alert(fromTime);
        //if (fromTime.value == '') addMessage('Please select From Time!', '');
        if (toDate == null) addMessage('Please select To Date !', '');
       // if (toTime.value == '') addMessage('Please select To Time!', '');
        if ((fromDate != null && toDate != null) && fromDate > toDate) addMessage('From Date should not be later than To Date!', '');
        if (getElement('hdfVisaDetailsMode').value == "2") addMessage('Details are in Edit mode !', '');
        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); return false;
        }
    }

    function validateAdvanceFilter(id, mode) {

        if (mode == 'A') {

            getElement('pnlAdvanceFilterDocDate').style.display = "block";
            getElement('divAdvanceFilterDocDate').style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            getElement('divAdvanceFilterDocDate').style.left = positions[0] + 'px';
            getElement('divAdvanceFilterDocDate').style.top = (positions[1] + 20) + 'px';

        }
        else {
            getElement('pnlAdvanceFilterDocDate').style.display = "none";
            getElement('divAdvanceFilterDocDate').style.display = "none";
            getElement('dcDocDateTo_Date').value = '';
            getElement('dcDocDateFrom_Date').value = '';

        }
        return false;

    }
    function getRelativePositions(obj) {
        var curLeft = 0;
        var curTop = 0;
        if (obj.offsetParent) {
            do {
                curLeft += obj.offsetLeft;
                curTop += obj.offsetTop;
            } while (obj = obj.offsetParent);

        }
        return [curLeft, curTop];
    }
    function getElement(id) {
        return document.getElementById('ctl00_cphTransaction_' + id);
    }

  
//    function setDelete(id) {
//        var rowId = id.substring(0, id.lastIndexOf('_'));
//        var Visaid = document.getElementById(rowId + '_IThdfVSId').value
//        document.getElementById('<%= hdfVisaId.ClientID %>').value = Visaid;
//        document.getElementById('divDelete').style.display = "block";
//        //var positions = getRelativePositions(document.getElementById(id));
//        //document.getElementById('divDelete').style.left = (positions[0]-5550) + 'px';
//        //document.getElementById('divDelete').style.top = (positions[1] + 20) + 'px';
//        return false;
//    }

//    function validateRemarks() {
//        if (getElement('txtRemarks').value == '') addMessage('Remarks cannot be blank!', '');
//        if (getMessage() != '') {
//            alert(getMessage()); clearMessage(); return false;
//        }
//    }
    function enableGridControls(chkId) {

        var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
        //alert(Id);
        if (document.getElementById(chkId) != null) {
            //alert(document.getElementById(Id+'ITtxtRemarks'))
            //alert(document.getElementById(Id+'ITtxtRemarks').value);
            // if(!document.getElementById(chkId).checked) document.getElementById(Id+'ITtxtRemarks').value='';
            document.getElementById(Id + 'ITddlDriver').disabled = !document.getElementById(chkId).checked;
            document.getElementById(Id + 'ITddlDriver').className = document.getElementById(chkId).checked ? 'inputEnabled' : 'inputDisabled'
            
            document.getElementById(Id + 'ITddlVehicle').disabled = !document.getElementById(chkId).checked;
            document.getElementById(Id + 'ITddlVehicle').className = document.getElementById(chkId).checked ? 'inputEnabled' : 'inputDisabled'
          
        }
    }

   
    function SetLocDetails(rowId,val,source) {
        
        if (val != "0") {
            var Id = rowId.substring(0, rowId.lastIndexOf('_') + 1)
            //document.getElementById(Id + 'ITddlVehicle')
            var locDetails = val.split("~");
            if (source == "F") {
                document.getElementById(Id + 'EITtxtFromLocDetails').value = locDetails[1];
                document.getElementById(Id + 'EIThdfFromLocId').value = locDetails[0];

            }
            else {
        
                document.getElementById(Id + 'EITtxtToLocDetails').value = locDetails[1];
                document.getElementById(Id + 'EIThdfToLocId').value = locDetails[0];
            }
        }

    }
    function ValidateDelete() {
        if (getElement('hdfVisaDetailsMode').value == "2") addMessage('Details are in Edit mode !', '');
        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); return false;
        }

    }

    function ShowLocDetails(rowId, source) {
       
        var Id = rowId.substring(0, rowId.lastIndexOf('_') + 1);
        var locName = '<b>' + "Location Name : " + '</b>';
        var locAdddress = '<b>' + "Location Address : " + '</b>';
        var locMap = '<b>' + "Location Map : " + '</b>';
        if (source == "F") {
            document.getElementById(Id + 'ITdivFLocDetails').style.display = "block";
            document.getElementById(Id + 'ITlblFLocName').innerHTML = locName + document.getElementById(Id + 'IThdfFromLocName').value;
            document.getElementById(Id + 'ITlblFLocDtls').innerHTML = locAdddress + document.getElementById(Id + 'IThdfFromLocDetails').value;
            document.getElementById(Id + 'ITlblFLocMap').innerHTML = locMap + document.getElementById(Id + 'IThdfFromLocMap').value; 
        
        }
        else {
            document.getElementById(Id + 'ITdivTLocDetails').style.display = "block";
            document.getElementById(Id + 'ITlblTLocName').innerHTML = locName + document.getElementById(Id + 'IThdfToLocName').value;
            document.getElementById(Id + 'ITlblTLocDtls').innerHTML = locAdddress  + document.getElementById(Id + 'IThdfToLocDetails').value;
            document.getElementById(Id + 'ITlblTLocMap').innerHTML = locMap + document.getElementById(Id + 'IThdfToLocMap').value; 
            
        }
        return false;
    }
    
</script>
</asp:Content>

