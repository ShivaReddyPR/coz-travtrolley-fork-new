﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="VisaFaqList" Title="Cozmo Travels" MaintainScrollPositionOnPostback="true" Codebehind="VisaFaqList.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

   
        <div style="clear: both; padding:10px 5px;">
            <p style="font-size: medium; margin-bottom:7px;">
                <b>Visa Faq List</b>
            </p>
             <label style="padding-right: 10px;">
               Agent
            </label>
            <span style="padding-right: 5px;">
                <asp:DropDownList ID="ddlAgent" CssClass="inp_border18" Width="160" runat="server">
                </asp:DropDownList>
            </span>
           
            <span>
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
            </span>
             <p>
            <span style="float:right;">
                <asp:HyperLink ID="hyplinkAddVisa" runat="server" NavigateUrl="~/AddVisaFaq.aspx">Add New Faq</asp:HyperLink >
            </span>
            </p>
            </div>
            <br />
                <div>
                    <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
                </div>
                <asp:GridView ID="gvVisaFaq" CssClass="grid_view" runat="server" AutoGenerateColumns="False" CellPadding="2"
                    Width="100%" DataKeyNames="faqId" ForeColor="#333333" GridLines="None" 
                    OnRowDataBound="gvVisaFaq_RowDataBound"
                    OnRowCommand="gvVisaFaq_RowCommand" AllowPaging="True" OnPageIndexChanging="gvVisaFaq_PageIndexChanging">
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="faqId" HeaderText="ID" ReadOnly="True" >
                            <HeaderStyle HorizontalAlign="Left"  />
                        </asp:BoundField>
                         <asp:BoundField DataField="agency" HeaderText="Agent" ReadOnly="True">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:BoundField>
                        <asp:BoundField DataField="faqQuestion" HeaderText="Question">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="isActive" HeaderText="Is Active" ReadOnly="True">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="faqId" HeaderText="Edit"
                            DataNavigateUrlFormatString="AddVisaFaq.aspx?id={0}" >
                            <ControlStyle  />
                        </asp:HyperLinkField>
                        <asp:TemplateField HeaderText="Activate/Deactivate">
                            <HeaderStyle HorizontalAlign="left" Width="100px" />
                            <ItemStyle HorizontalAlign="left" Font-Underline="True" />
                            <ItemTemplate>
                                <asp:LinkButton ID="linkButtonStatus" runat="server" Text="Activate"
                                    CommandName="ChangeStatus">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="#E3EAEB" HorizontalAlign="Left" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Left" Font-Bold="True" />
                    <HeaderStyle BackColor="#761002" Font-Bold="True" ForeColor="White" HorizontalAlign="Left"  />
                    <AlternatingRowStyle BackColor="white" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                        NextPageText="Next" PreviousPageText="Previous" />
                </asp:GridView>
            
            <br />

</asp:Content>

