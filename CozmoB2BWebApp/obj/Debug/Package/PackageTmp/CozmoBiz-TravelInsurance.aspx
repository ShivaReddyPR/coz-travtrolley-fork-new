﻿<%@ Page Language="C#" %>
	<!DOCTYPE html>
	<html>

	<head id="Head1" runat="server">
		<title>Travel Insurance</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">

        
		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="bizzlogin.aspx"><img src="images/logoc.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="heading_bizz">
			<div class="container">
				<h2> Travel Insurance </h2>
			</div>
		</div>
		<form runat="server" id="form1">
			<div class="container">
				<div class="innerPage2">
					<div class="row">
						<div class="col-12">
							<p>Losing your bags enroute to your destination, or suffering other travel-related mishaps is an unfortunate reality for customers who live out of a suitcase. Travel insurance is a must for peace of mind and hassle-free travel. CCTM is well-placed to improve customer safety with timely alerts, and we are one of the few distributors of Blue Ribbon bags, in addition to providing standard travel insurance services.
								<br />
								<br />Blue Ribbon bags help track and expedite the return of misplaced baggage for 96 hours after your flight lands. This service covers every flight, on every airline, everywhere in the world. Get continuous status updates via email and get a guaranteed payment for bags lost, without having to furnish proof of baggage contents.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="footer_sml_bizz">Copyright 2019 Cozmo Travel World, India. All Rights Reserved.</div>


	</body>

	</html>
