﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="ReligareInsuranceInvoice.aspx.cs" Inherits="CozmoB2BWebApp.ReligareInsuranceInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />


    <script type="text/javascript">      
        $(document).ready(function () {  //divInvoiceDetails
            $('#btnPrint').click(function () {
                $('#btnPrint').hide();
                var grid = document.getElementById("divInvoiceDetails");
                var printWindow = window.open('', '', 'height=900,width=1600');
                printWindow.document.write('<html><head>');
                printWindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">');
                printWindow.document.write('<link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />');
                printWindow.document.write('<style type="text/css"> @media print {table, th, td {border-collapse: collapse;} #agentDetails{width:100%;} #invoiceDetails{width:100%;} .ns-h3{ text-align:center;} .table{border:hidden;font-size:15px;} </style>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(grid.outerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
              
              document.getElementById('btnPrint').style.display = "block";
                  return false;
            });
           
        });


    </script>
<%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            background-color: #f1f1f1;
        }
         .table{
             border:hidden; 
             width:100%;
         }
         .tCell{             
             width:250%;
         }
          
         
        
    </style>
     </head> 
 
<body>
    <form id="form1" runat="server">
    <div class="container" id="divInvoiceDetails">
        <div style="border-radius: 5px;">
            <h3 style="text-align: center">Invoice<span id="btnPrint" style="float: right; border-radius: 5px;" class="btn btn-primary active">Print</span></h3>
        </div>
        <div class="row">
            <div class="form-group" style="text-align: center;">
                <table class="table">
                    <tr>
                        <td class="ns-h3">
                            <h5><b>Client Details</b></h5>
                        </td>
                        <td class="ns-h3">
                            <h5><b>Invoice Details</b></h5>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table" style="width:100%">
                <tr>
                    <td >
                        <div class="form-group">
                            <div class="col-md-6">
                                <table class="table" id="agentDetails">
                                    <tr>
                                        <td class="tCell">Agency Name:</td>
                                       <td class="tCell">
                                            <asp:Label ID="lblagencyName" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td class="tCell">Agency Code:</td>
                                        <td class="tCell">
                                            <asp:Label ID="lblAgencyCode" runat="server" /></td>
                                    </tr>
                                    <tr>
                                       <td class="tCell">Agency Address:</td>
                                        <td class="tCell">
                                            <asp:Label ID="lblAgencyAddress" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td class="tCell">TelePhone No:</td>
                                        <td class="tCell">
                                            <asp:Label ID="lblAgencyMobile" runat="server" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                    <td >
                        <div class="form-group">
                            <div class="col-md-6">
                                <table class="table" id="invoiceDetails">
                                    <tr>
                                        <td class="tCell">Invoice No:</td>
                                        <td class="tCell"> 
                                            <asp:Label ID="lblInvoiceNo" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td class="tCell">Invoice Date:</td>
                                       <td class="tCell">
                                            <asp:Label ID="lblInvoiceDate" runat="server" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="row">
            <div class="form-group">
                <table class="table" style="text-align: center;" id="hhh">
                    <tr>
                        <td class="ns-h3">
                            <h5><b>Booking Details</b></h5>
                        </td>
                        <td class="ns-h3">
                            <h5><b>Rate  Details</b></h5>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table" >
                <tr>
                    <td  >
                        <div class="form-group">
                            <div class="col-md-6">
                                <table class="table">
                                    <tr>
                                        <td class="tCell">Booking Date:</td>
                                        <td >
                                            <asp:Label ID="lblBookingDate" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td class="tCell">Travel Date:</td>
                                        <td >
                                            <asp:Label ID="lblTravelDate" runat="server" /></td>
                                    </tr>
                                    <tr>
                                       <td class="tCell">Plan Name:</td>
                                        <td >
                                            <asp:Label ID="lblPlanName" runat="server" /></td>
                                    </tr>
                                    <tr>
                                       <td class="tCell">Policy No:</td>
                                        <td class="tCell">
                                            <asp:Label ID="lblPolicyNo" runat="server" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                    <td  >
                        <div class="form-group">
                            <div class="col-md-6">
                                <table class="table">
                                    <tr>
                                         <td class="tCell">Premium Amount:</td>
                                         <td class="tCell">
                                            <asp:Label ID="lblPremium" runat="server" /></td>
                                    </tr>
                                    <tr style="display:none">
                                         <td class="tCell" >Discount:</td>
                                         <td class="tCell">
                                            <asp:Label ID="lblDiscount" runat="server" /></td>
                                    </tr>
                                     <tr>
                                       <td class="tCell"><asp:Label id="Label1" Runat="server" Text="Markup:" /></td>
                                         <td class="tCell">
                                            <asp:Label ID="lblMarkup" runat="server" /></td>
                                    </tr>
                                    <tr>
                                       <td class="tCell"><asp:Label id="lblSgst" Runat="server" Text="GST:" /></td>
                                         <td class="tCell">
                                            <asp:Label ID="lblsgstValue" runat="server" /></td>
                                    </tr>
                                     <tr id="tdcgst" runat="server">
                                       <td class="tCell"><asp:Label id="lblCgst" Runat="server" Text="GST:" /></td>
                                         <td class="tCell">
                                            <asp:Label ID="lblcgstValue" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td class="tCell">Total Amount:</td>
                                         <td class="tCell">
                                            <asp:Label ID="lblTotalAmount" runat="server" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="row">
            <div class="form-group">
                <table class="table" style="text-align: center;">
                    <tr>
                        <td class="ns-h3">
                            <h5><b>Passenger Details</b></h5>
                        </td>

                    </tr>
                </table>
            </div>
            <table class="table" >
                <tr>
                    <td >
                        <div class="form-group">
                            <div class="col-md-6">
                                <asp:Table CssClass="table" runat="server" ID="tblpaxDetails">
                                    <asp:TableHeaderRow Style="height: 10px">
                                        <asp:TableHeaderCell>Passenger Name</asp:TableHeaderCell>
                                        <asp:TableHeaderCell>Type</asp:TableHeaderCell>
                                    </asp:TableHeaderRow>
                                    <asp:TableRow runat="server"></asp:TableRow>
                                    <asp:TableRow runat="server"></asp:TableRow>
                                </asp:Table>



                            </div>
                        </div>
                    </td>
                    <td  >
                        <div class="form-group"   >
                            <div class="col-md-6">
                                <table class="table"  >
                                    <tr>
                                        <td class="tCell">Invoiced By:</td>
                                        <td class="tCell">
                                            <asp:Label ID="lblInvoicedBy" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td class="tCell">Created By:</td>
                                     <td class="tCell">
                                            <asp:Label ID="lblCreatedBy" runat="server" /></td>
                                    </tr>
                                    <tr>
                                         <td class="tCell">Location:</td>
                                    <td class="tCell">
                                            <asp:Label ID="lblLocation" runat="server"   /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
        </form>
 </body>
 
</html>
