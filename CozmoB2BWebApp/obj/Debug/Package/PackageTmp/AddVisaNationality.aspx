﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"
    Inherits="AddVisaNationality" Title="Cozmo Travels" ValidateRequest="false" Codebehind="AddVisaNationality.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    
    
    <div> 
    <div class="col-md-6"> <h4> 
   <asp:Label runat="server" ID="lblStatus" 
                                Text="Add Visa Nationality"></asp:Label>
            
    
    </h4> </div>
    
    <div class="col-md-6">
    
    <asp:HyperLink ID="HyperLink1" class="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaNationalityList.aspx">Go to Visa Nationality List</asp:HyperLink>
    
    


 </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
    
            <div class=" paramcon"> 
    <div class="col-md-2"><label>
                                    Nationality Name <sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"><p>
                                
                                <span>
                                    <asp:TextBox CssClass="form-control"  ID="txtNationalityName" runat="server"></asp:TextBox>&nbsp;</span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="Please fill nationality name "
                                    ControlToValidate="txtNationalityName"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgcityName" runat="server" ErrorMessage="Please enter only character"
                         ValidationExpression="[a-zA-Z\s]+" ControlToValidate="txtNationalityName" Display="Dynamic"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter maximum 50 character"
                         ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtNationalityName" Display="Dynamic"></asp:RegularExpressionValidator>
                                      </p> </div>
                <div class="col-md-2">
                    <label>
                        Arabic Nationality Name<sup style="color:Red">*</sup>
                    </label> 
                </div>
                <div class="col-md-2">
                    <span>
                        <asp:TextBox CssClass="form-control"  ID="txtARNationalityName" onkeypress="return CheckArabicCharactersOnly(event);" runat="server"></asp:TextBox>&nbsp;
                    </span>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  Display="Dynamic" ErrorMessage="Please fill Arabic nationality name "
                                    ControlToValidate="txtARNationalityName"></asp:RequiredFieldValidator>
                </div>
                 
    <div class="col-md-2"> <label>
                                    Nationality Code<sup style="color:Red">*</sup></label></div>
    
    <div class="col-md-2"> <p>
                                
                                <span>
                                    <asp:TextBox CssClass="form-control" ID="txtNationalityCode" runat="server" MaxLength="4"></asp:TextBox>&nbsp;</span>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"  Display="Dynamic" ErrorMessage="Please enter maxmimum 4 character code"
                                    ValidationExpression="^[a-z,A-Z]{2,4}$" ControlToValidate="txtNationalityCode"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please fill nationality code "
                                    ControlToValidate="txtNationalityCode" Display="Dynamic"></asp:RequiredFieldValidator>
                            </p></div>   
                            
                            
                            
<div style="padding:10px"> 
   <asp:Button ID="btnSave" runat="server" Text=" Save " CssClass="but but_b pull-right"  
                                        OnClick="btnSave_Click" />


</div>
                             

    
    
 <div class="clearfix"> </div> 
    </div>
    
  <script  type="text/javascript">
// Allow Arabic Characters only
function CheckArabicCharactersOnly(e) {
var unicode = e.charCode ? e.charCode : e.keyCode
if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
if (unicode == 32)
return true;
else {
if ( (unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabic
return false; //disable key press
}
}
}
</script>  
       

</asp:Content>
