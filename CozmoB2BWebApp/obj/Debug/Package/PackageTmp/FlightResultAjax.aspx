﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlightResultAjax.aspx.cs" Inherits="CozmoB2BWebApp.FlightResultAjax" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<% if (Session["sessionId"] != null && Settings.LoginInfo != null)
    {
        if (onwardFilteredResults != null && Request["type"].ToLower() == "on")
        {%>
<div class="sorting-wrapper d-none d-md-flex">
    <div class="flex-fill" style="max-width:120px;">
        <a href="javascript:void(0);" id="onDepSort" onclick="DepartureTimeSort('On')" class="flight-sort-btn">Departure <span class='glyphicon glyphicon-arrow-down <%=dtimeActive %>'></span></a>
    </div>
    <div class="flex-fill">
        <a href="javascript:void(0);" id="onStopSort" onclick="StopsSort('On')" class="flight-sort-btn">Stops <span class='glyphicon glyphicon-arrow-down <%=stopsActive %>'></span></a>
    </div>
    <div class="flex-fill">
        <a href="javascript:void(0);" id="onArrSort" onclick="ArrivalTimeSort('On')" class="flight-sort-btn">Arrival <span class='glyphicon glyphicon-arrow-down <%=atimeActive %>'></span></a>

    </div>
    <div class="flex-fill">
        <a href="javascript:void(0);" id="onPriceSort" onclick="PriceSort('On')" class="flight-sort-btn">Price <span class='glyphicon glyphicon-arrow-down <%=priceActive %>'></span></a>

        <input type="checkbox" id="chkOnDownAll" title="Select All for Download" onchange="SelectAll('On')" class="position-absolute" style="right: 10px;top: 5px;" />
      <%--  Select All  --%>
    </div>
</div>
<%--<input type="hidden" id="onwardFiltered" value="<%=onwardFilteredResults.Count %>" />--%>

<ul class="list-unstyled flight-list-wrapper">
<%int counter = 0,counterFD = 0;//Flag which determines whether to display the flight information or not.
                    //Step-1:Grouping the results based on the resultKey
    var groupedOnwardList = onwardFilteredResults
       .GroupBy(u => u.ResultKey)
       .Select(grp => grp.ToList())
       .ToList();
    //Step-2:Now Each Group Has set of results.//Iterate through the groups
    foreach (var group in groupedOnwardList)
    {
        SearchResult res = group.FirstOrDefault();
        FlightInfo onwardSegment = res.Flights[0][0];
        FlightInfo returnSegment = res.Flights[0][res.Flights[0].Length - 1];
        TimeSpan totalDuration = new TimeSpan();
        List<FlightInfo> flightInfos = new List<FlightInfo>(res.Flights[0]);
        //flightInfos.ForEach(f => f.Duration = f.ArrivalTime.Subtract(f.DepartureTime));
        foreach (FlightInfo seg in flightInfos)
        {
            totalDuration = totalDuration.Add(seg.Duration);
        }
        CT.Core.Airline airline = new CT.Core.Airline();
        int specialReturnCount = 0;
                                            foreach(SearchResult mres in group)
                                            {
                                                if (!string.IsNullOrEmpty(mres.FareType) && (mres.FareType =="SPECIAL ROUND TRIP FARE" || mres.FareType =="Special Return Trip"))
                                                {
                                                    specialReturnCount++;
                                                }
                                            }

%>

                                        <!--ITEMSR:Item Special Round Trip -->
                                        <!--ITEMN:Item Normal -->
                                   <%if (specialReturnCount > 0) %>
                                   <%{ %>
                                    <li id="OnResult<%=counter %>" class="OITEMSR item <%=(counter == 0 ? "checked" : "") %>">
                                        <%} %>
                                        <%else %>
                                        <%{ %>
                                        <li id="OnResult<%=counter %>" class="OITEMN item <%=(counter == 0 ? "checked" : "") %>">
                                        <%} %>

        <div class="row">

            <div class="col-12">
                <img src="images/AirlineLogo/<%=onwardSegment.Airline %>.gif" alt="SG-Airline" class="airline-logo float-left pr-2" />
                <%
                    airline.Load(onwardSegment.Airline);%>
                <span class="airline-name pb-2">
                    <%=airline.AirlineName %> <span><%=airline.AirlineCode %> <%=onwardSegment.FlightNumber %></span>
                </span>
            </div>

            <div class="col-md-5 col-lg-3">
                <div class="time"><%=onwardSegment.DepartureTime.ToString("HH:mm") %><em><%=onwardSegment.DepartureTime.ToString("tt") %></em></div>
                <div class="airport-code"><%=onwardSegment.Origin.AirportCode %><span class="city"><%=onwardSegment.Origin.CityName %></span></div>
            </div>

            <div class="col-md-1 col-lg-1  text-align-center">
                <i class="font-icon icon-flight-right"></i>
                
            </div>


            <div class="col-md-5 col-lg-3 separator-right text-right">
                <div class="time"><%=returnSegment.ArrivalTime.ToString("HH:mm") %><em><%=returnSegment.ArrivalTime.ToString("tt") %></em></div>
                <div class="airport-code"><%=returnSegment.Destination.AirportCode %><span class="city"><%=returnSegment.Destination.CityName %></span></div>
            </div>


            <div class="col-md-12 col-lg-5 book-now-section  d-flex d-lg-block">
                <div class="row no-gutters">
                    <div class="col-12 mb-2">
                        <span class="currency"><%=res.Currency %></span>
                    </div>
                    <%foreach (SearchResult res1 in group) %>
                    <%{ %>

                    <input type="hidden" id="hdnOnFareType<%=counter %>" value="<%=res1.FareType %>" />
                    <input type="hidden" id="hdnOnAirline<%=counter %>" value="<%=onwardSegment.Airline %>" />
                    <input type="hidden" id="hdnOnStops<%=counter %>" value="<%=onwardSegment.Stops %>" />
                    <input type="hidden" id="hdnOnRefundable<%=counter %>" value="<%=res1.NonRefundable %>" />
                    <input type="hidden" id="hdnOnDeparture<%=counter %>" value="<%=onwardSegment.DepartureTime %>" />
                    <input type="hidden" id="hdnOnArrival<%=counter %>" value="<%=onwardSegment.ArrivalTime %>" />
                    <input type="hidden" id="hdnOnPrice<%=counter %>" value="<%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare) : res1.TotalFare) %>" />
                    <input type="hidden" id="hdnOnResultId<%=counter %>" value="<%=res1.ResultId %>" />
                    <input type="hidden" id="hdnOnSource<%=counter %>" value="<%=res1.ResultBookingSource %>" />

                                                    <!---ONWARD------>
                                                    <!--BASED ON THE BELOW DIV WE WILL FILTER THE SPECIAL ROUND TRIP FARE RESULTS OR Special Return Trip FARE Results  -->
                                                    <!--6E/6ECORP SPECIAL ROUND TRIP -->
                                                    <!--SG/SGCORP Special Return Trip -->
                                                    <!--SRT :  SPECIAL ROUND TRIP FARE OR SPECIAL RETURN TRIP-->
                                                    <!--NSRT : NOT SPECIAL ROUND TRIP FARE OR NOT SPECIAL RETURN TRIP-->
                                                       <%if (!string.IsNullOrEmpty(res1.FareType) && (res1.FareType =="SPECIAL ROUND TRIP FARE" || res1.FareType =="Special Return Trip")) %>
                                                    <%{ %>
                                                     <div class="col-12 custom-radio-flight SRTO"> <!--SRT : SPECIAL ROUND TRIP FARE OR SPECIAL RETURN TRIP-->
                                                       <%} %>
                                                       <%else %>
                                                       <%{ %>
                                                       <div class="col-12 custom-radio-flight NSRTO"> <!--NSRT : NOT SPECIAL ROUND TRIP FARE OR NOT SPECIAL RETURN TRIP-->
                                                       <%} %>
                        <label class="control-label">
                            <input type="radio" name="OnwardflightresultItem" id="rbOnResult<%=counter %>" class="flight-radio-button" onchange="SelectResult(<%=counter %>,'ONWARD')">
                            <label></label>
                        </label>
                        <%decimal discount = res1.FareBreakdown.Sum(x => x.AgentDiscount);%>
                        <label class="control-label float-right">
                            <input type="checkbox" id="chkOnRes-<%=counter %>" onchange="selectBox(this.id,'On')" />
                        </label>
                        <label class="control-label price">
                            <span class="stops"><%=res1.FareType %></span>
                            <%--<span  class="stops">Pub Fare</span><%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare + (double)discount).ToString("N" + agency.DecimalValue) : (res1.TotalFare + (double)discount).ToString("N" + agency.DecimalValue)) %>--%>
                            <%--<span  class="stops">Offer Fare</span>--%> <%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare).ToString("N" + res1.Price.DecimalPoint) : res1.TotalFare.ToString("N" + res1.Price.DecimalPoint)) %>
                        </label>


                    </div>

                    <%counter++; %>
                    <%} %>
                </div>
            </div>

             <%
                                                bool displayDetails = true;
                                                foreach (SearchResult res2 in group) %>
                                                    <%{ %>
                                            <%if (displayDetails) %>
                                            <%{ %>
                                            <div class="col-12 onwardFD" id="divFD<%=counterFD %>">
                                                 <%}%>
                                                <%else %>
                                                <%{ %>
                                                   <div style="display:none" class="col-12 onwardFD" id="divFD<%=counterFD %>">
                                                <%}%>
                                                <button class="btn btn-primary btn-sm btn-flight-details" type="button" data-toggle="collapse" data-target="#flightdetails-<%=res2.ResultId %>" aria-expanded="false" aria-controls="flightdetails-<%=res2.ResultId %>">
                                                    FLIGHT DETAILS <span class="font-icon icon-arrow_drop_down"></span>
                                                </button>
<span class="stops float-left pt-3 px-3"><%=(res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0 ? "NONSTOP" : (res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1 ? "ONESTOP" : "TWOSTOPS")) %>    </span>
                                                <span class="total-duration">Total Duration:<em><%=(totalDuration.Days >= 1) ? Math.Floor(totalDuration.TotalHours) : totalDuration.Hours %>.<%=totalDuration.Minutes %></em></span>
                                                <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                                    {%>
                                                <span class="booking-source small"><%=res2.ResultBookingSource.ToString() %></span>
                                                <%} %>
                                                
                                                <div class="clearfix"></div>
                                                <div class="collapse flighdtl-collapse-content" id="flightdetails-<%=res2.ResultId %>">
                                                    <div class="card card-body">
                                                        <%--Flight Details Tabbed Panel--%>

                                                        <ul class="nav nav-pills mb-3" id="flightdetails-tab" role="tablist">
                                                            <li class="nav-item active">
                                                                <a class="nav-link active" data-toggle="pill" href="#pills-flightInfo-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-flightInfo-tab-<%=res2.ResultId %>" aria-selected="true">Flight Info</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-fareRules-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-fareRules-tab-<%=res2.ResultId %>" onclick="GetFareRule(<%=res2.ResultId %>,'<%=Session["SessionId"].ToString() %>','ONWARD',<%=counterFD %>)" aria-selected="false">Fare Rules</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-baggage-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-baggage-tab-<%=res2.ResultId %>" aria-selected="false">Baggage</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="flightdetails-tabContent">
                                                            <div class="tab-pane fade show active in" id="pills-flightInfo-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                <table id="ctl00_cphTransaction_dlFlightResults_ctl03_flightDetails" class="flight_details_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <%foreach (FlightInfo seg in res2.Flights[0])
    {
        airline.Load(seg.Airline);
                                                                        %>
                                                                        <tr>
                                                                            <td width="40px" align="Center" class="f1"><span class="p20">
                                                                                <img src="images/AirlineLogo/<%=seg.Airline %>.GIF" width="50px" height="35px"></span><br>
                                                                                <%=airline.AirlineName %><br>
                                                                                (<%=seg.Airline %>) <%=seg.FlightNumber %><br>
                                                                            </td>
                                                                            <td class="f2"><%=seg.Origin.CityName %><br>
                                                                                <%=seg.DepartureTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br>
                                                                                Terminal: <%=seg.DepTerminal %></td>
                                                                            <td class="f3"><%=seg.Destination.CityName %><br>
                                                                                <%=seg.ArrivalTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br />
                                                                                Terminal: <%=seg.ArrTerminal %></td>
                                                                            <td class="f4"><%=(seg.Duration.Days>=1)? Math.Floor(seg.Duration.TotalHours) : seg.Duration.Hours %>Hrs <%=seg.Duration.Minutes %>Mins<br>
                                                                                <%=(string.IsNullOrEmpty(seg.CabinClass) ? seg.BookingClass : seg.CabinClass + "-" + seg.BookingClass) %><br>
                                                                                <br>
                                                                            </td>
                                                                            <td rowspan="1" class="f5">Aircraft:  <%=seg.Craft %><span class="cccline">|</span> <span class="cccline">|</span><%=(res2.NonRefundable ? "Non Refundable" : "Refundable") %></td>
                                                                        </tr>
                                                                        <%if (res2.Flights[0].Length > 0)
    { %>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <div class="flight_amid_info"></div>
                                                                            </td>
                                                                        </tr>
                                                                        <%}
    } %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-fareRules-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-fareRules-tab-<%=res2.ResultId %>">
                                                                <div class="height-200 overflow-hidden" style="overflow-y: auto">
                                                                    <div class="small" id="onFareRules<%=counterFD %>">
                                                                        <div class="padding-10 refine-result-bg-color">
                                                                            Please wait ...
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-baggage-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-baggage-tab-<%=res2.ResultId %>">
                                                                <table class="table table-bordered small" width="100%" border="1" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th style="width: 50%"><strong>Sector / Flight</strong></th>
                                                                            <th style="width: 50%"><strong>Baggage</strong></th>
                                                                        </tr>
                                                                        <%for (int i = 0; i < res2.Flights[0].Length; i++)
    {
        FlightInfo seg = res2.Flights[0][i];
                                                                        %>
                                                                        <tr>
                                                                            <td><%=seg.Origin.AirportCode %> - <%=seg.Destination.AirportCode %> <%=seg.Airline %> <%=seg.FlightNumber %></td>
                                                                            <%if (!string.IsNullOrEmpty(seg.DefaultBaggage)){%>
                                                                                        <td><%=(seg.DefaultBaggage)%> </td><%}%>
                                                                                        <%else
                                                                                          {%>
                                                                                          <td>As Per Airline Policy</td>
                                                                                          <%} %>
                                                                        </tr>
                                                                        <%} %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <%counterFD++;displayDetails = false; %> 
                                            <%} %>



        </div>
    </li>






<%}  %>

    </ul>

<%}
    if (returnFilteredResults != null && Request["type"].ToLower() == "ret")
    {%>
<div class="sorting-wrapper d-none d-md-flex">
    <div class="flex-fill">
        <a href="javascript:void(0);" id="retDepSort" onclick="DepartureTimeSort('Ret')" class="flight-sort-btn">Departure <span class='glyphicon glyphicon-arrow-down <%=dtimeActive %>'></span></a>
    </div>
    <div class="flex-fill">
        <a href="javascript:void(0);" id="retStopSort" onclick="StopsSort('Ret')" class="flight-sort-btn">Stops <span class='glyphicon glyphicon-arrow-down <%=stopsActive %>'></span></a>
    </div>
    <div class="flex-fill">
        <a href="javascript:void(0);" id="retArrSort" onclick="ArrivalTimeSort('Ret')" class="flight-sort-btn">Arrival <span class='glyphicon glyphicon-arrow-down <%=atimeActive %>'></span></a>
    </div>
    <div class="flex-fill">
        <a href="javascript:void(0);" id="retPriceSort" onclick="PriceSort('Ret')" class="flight-sort-btn">Price <span class='glyphicon glyphicon-arrow-down <%=priceActive %>'></span></a>

        <input type="checkbox" id="chkRetDownAll" title="Select All for Download" onchange="SelectAll('Ret')" class="position-absolute" style="right: 10px;top: 5px;" />
   <%--     Select All  --%>
    </div>
</div>

<%--<input type="hidden" id="returnFiltered" value="<%=returnFilteredResults.Count %>" />--%>

<ul class="list-unstyled flight-list-wrapper">
<%int counterR = 0,counterRFD = 0;//Flag which determines whether to display the flight information or not.
                     //Step-1:Grouping the results based on the resultKey;
    var groupedReturnList = returnFilteredResults
           .GroupBy(u => u.ResultKey)
           .Select(grp => grp.ToList())
           .ToList();
    //Step-2:Now Each Group Has set of results.//Iterate through the groups
    foreach (var group in groupedReturnList)
    {
        SearchResult res = group.FirstOrDefault();
        FlightInfo onwardSegment = res.Flights[0][0];
        FlightInfo returnSegment = res.Flights[0][res.Flights[0].Length - 1];
        TimeSpan totalDuration = new TimeSpan();
        List<FlightInfo> flightInfos = new List<FlightInfo>(res.Flights[0]);
        //flightInfos.ForEach(f => f.Duration = f.ArrivalTime.Subtract(f.DepartureTime));
        foreach (FlightInfo seg in flightInfos)
        {
            totalDuration = totalDuration.Add(seg.Duration);
        }
        CT.Core.Airline airline = new CT.Core.Airline();
        int specialReturnCount = 0;
                                            foreach(SearchResult mres in group)
                                            {
                                                if (!string.IsNullOrEmpty(mres.FareType) && (mres.FareType =="SPECIAL ROUND TRIP FARE" || mres.FareType =="Special Return Trip"))
                                                {
                                                    specialReturnCount++;
                                                }
                                            }
%>

                                 <%if (specialReturnCount > 0) %>
                                    <%{ %>
                                        <!--ITEMSR:Item Special Round Trip -->
                                        <!--ITEMN:Item Normal -->
                                    <li id="RetResult<%=counterR %>" class="RITEMSR item <%=(counterR == 0 ? "checked" : "") %>">
                                        <%} %>
                                        <%else %>
                                        <%{ %>
                                        <li id="RetResult<%=counterR %>" class="RITEMN item <%=(counterR == 0 ? "checked" : "") %>">
                                        <%} %>
        <div class="row">
            <div class="col-12">
                <img src="images/AirlineLogo/<%=onwardSegment.Airline %>.gif" alt="SG-Airline" class="airline-logo float-left pr-2" />
                <%
                    airline.Load(onwardSegment.Airline);%>
                <span class="airline-name pb-2">
                    <%=airline.AirlineName %> <span><%=airline.AirlineCode %> <%=onwardSegment.FlightNumber %></span>
                </span>
            </div>

            <div class="col-md-5 col-lg-3">
                <div class="time"><%=onwardSegment.DepartureTime.ToString("HH:mm") %><em><%=onwardSegment.DepartureTime.ToString("tt") %></em></div>
                <div class="airport-code"><%=onwardSegment.Origin.AirportCode %><span class="city"><%=onwardSegment.Origin.CityName %></span></div>
            </div>

            <div class="col-md-1 col-lg-1  text-align-center">
                <i class="font-icon icon-flight-right"></i>
              
            </div>

            <div class="col-md-5 col-lg-3 separator-right text-right">
                <div class="time"><%=returnSegment.ArrivalTime.ToString("HH:mm") %><em><%=returnSegment.ArrivalTime.ToString("tt") %></em></div>
                <div class="airport-code"><%=returnSegment.Destination.AirportCode %><span class="city"><%=returnSegment.Destination.CityName %></span></div>
            </div>

            <div class="col-md-12 col-lg-5 book-now-section  d-flex d-lg-block">
                <div class="row no-gutters">
                    <div class="col-12 mb-2">
                        <span class="currency"><%=res.Currency %></span>
                    </div>
                    <%foreach (SearchResult res1 in group) %>
                    <%{ %>

                    <input type="hidden" id="hdnRetFareType<%=counterR %>" value="<%=res1.FareType %>" />
                    <input type="hidden" id="hdnRetAirline<%=counterR %>" value="<%=onwardSegment.Airline %>" />
                    <input type="hidden" id="hdnRetStops<%=counterR %>" value="<%=onwardSegment.Stops %>" />
                    <input type="hidden" id="hdnRetRefundable<%=counterR %>" value="<%=res1.NonRefundable %>" />
                    <input type="hidden" id="hdnRetDeparture<%=counterR %>" value="<%=onwardSegment.DepartureTime %>" />
                    <input type="hidden" id="hdnRetArrival<%=counterR %>" value="<%=onwardSegment.ArrivalTime %>" />
                    <input type="hidden" id="hdnRetPrice<%=counterR %>" value="<%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare) : res1.TotalFare) %>" />
                    <input type="hidden" id="hdnRetResultId<%=counterR %>" value="<%=res1.ResultId %>" />
                    <input type="hidden" id="hdnRetSource<%=counterR %>" value="<%=res1.ResultBookingSource %>" />

                                                    <!---RETURN------>
                                                    <!--BASED ON THE BELOW DIV WE WILL FILTER THE SPECIAL ROUND TRIP FARE RESULTS OR Special Return Trip FARE Results  -->
                                                    <!--6E/6ECORP SPECIAL ROUND TRIP -->
                                                    <!--SG/SGCORP Special Return Trip -->
                                                    <!--SRT :  SPECIAL ROUND TRIP FARE OR SPECIAL RETURN TRIP-->
                                                    <!--NSRT : NOT SPECIAL ROUND TRIP FARE OR NOT SPECIAL RETURN TRIP-->
                                                       <%if (!string.IsNullOrEmpty(res1.FareType) && (res1.FareType =="SPECIAL ROUND TRIP FARE" || res1.FareType =="Special Return Trip")) %>
                                                    <%{ %>
                                                     <div class="col-12 custom-radio-flight SRTR"> <!--SRT : SPECIAL ROUND TRIP FARE OR SPECIAL RETURN TRIP-->
                                                       <%} %>
                                                       <%else %>
                                                       <%{ %>
                                                       <div class="col-12 custom-radio-flight NSRTR"> <!--NSRT : NOT SPECIAL ROUND TRIP FARE OR NOT SPECIAL RETURN TRIP-->
                                                       <%} %>
                        <label class="control-label">
                            <input type="radio" name="ReturnflightresultItem" id="rbRetResult<%=counterR %>" class="flight-radio-button" onchange="SelectResult(<%=counterR %>,'RETURN')" />
                            <label></label>
                        </label>
                        <%decimal discount = res1.FareBreakdown.Sum(x => x.AgentDiscount);%>
                        <label class="control-label float-right">
                            <input type="checkbox" id="chkRetRes-<%=counterR %>" onchange="selectBox(this.id,'Ret')" /></label>
                        <label class="control-label price">
                            <span class="stops"><%=res1.FareType %></span>
                            <%--<span  class="stops">Pub Fare</span><%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare + (double)discount).ToString("N" + agency.DecimalValue) : (res1.TotalFare + (double)discount).ToString("N" + agency.DecimalValue)) %>--%>
                            <%=(res1.ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling(res1.TotalFare).ToString("N" + res1.Price.DecimalPoint) : res1.TotalFare.ToString("N" + res1.Price.DecimalPoint)) %>
                        </label>
                    </div>

                    <%counterR++; %>
                    <%} %>
                </div>
            </div>

           
                                              <% bool displayDetails = true;
                                                  foreach (SearchResult res2 in group) %>
                                                    <%{ %>
                                         <%if (displayDetails) %>
                                            <%{ %>
                                            <div class="col-12 returnFD" id="divRFD<%=counterRFD %>">
                                                 <%}%>
                                                <%else %>
                                                <%{ %>
                                                   <div style="display:none" class="col-12 returnFD" id="divRFD<%=counterRFD %>">
                                                <%}%>
                                                <button class="btn btn-primary btn-sm btn-flight-details" type="button" data-toggle="collapse" data-target="#flightdetails-<%=res2.ResultId %>" aria-expanded="false" aria-controls="flightdetails-1">
                                                    FLIGHT DETAILS <span class="font-icon icon-arrow_drop_down"></span>
                                                </button>
                                                <span class="stops float-left pt-3 px-3"><%=(res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 0 ? "NONSTOP" : (res2.Flights[0].Select(p => p.FlightNumber).Distinct().Count() - 1 == 1 ? "ONESTOP" : "TWOSTOPS")) %>     </span>
                                                <span class="total-duration">Total Duration:<em><%=(totalDuration.Days >= 1) ? Math.Floor(totalDuration.TotalHours) : totalDuration.Hours %>.<%=totalDuration.Minutes %></em></span>
                                                <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                                    {%>
                                                <span class="booking-source small"><%=res2.ResultBookingSource.ToString() %></span>
                                                <%} %>
                                                <div class="clearfix"></div>
                                                <div class="collapse flighdtl-collapse-content" id="flightdetails-<%=res2.ResultId %>">
                                                    <div class="card card-body">
                                                        <%--Flight Details Tabbed Panel--%>

                                                        <ul class="nav nav-pills mb-3" id="flightdetails-tab" role="tablist">
                                                            <li class="nav-item active">
                                                                <a class="nav-link active" data-toggle="pill" href="#pills-flightInfo-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-flightInfo-tab-<%=res2.ResultId %>" aria-selected="true">Flight Info</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-fareRules-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-fareRules-tab-<%=res2.ResultId %>" onclick="GetFareRule(<%=res2.ResultId %>,'<%=Session["SessionId"].ToString() %>','RETURN',<%=counterRFD %>)" aria-selected="false">Fare Rules</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="pill" href="#pills-baggage-tab-<%=res2.ResultId %>" role="tab" aria-controls="pills-baggage-tab-<%=res2.ResultId %>" aria-selected="false">Baggage</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="flightdetails-tabContent">
                                                            <div class="tab-pane fade show active in" id="pills-flightInfo-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                <table id="ctl00_cphTransaction_dlFlightResults_ctl03_flightDetails" class="flight_details_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <%foreach (FlightInfo seg in res2.Flights[0])
    {
        airline.Load(seg.Airline);
                                                                        %>
                                                                        <tr>
                                                                            <td width="40px" align="Center" class="f1"><span class="p20">
                                                                                <img src="images/AirlineLogo/<%=seg.Airline %>.GIF" width="50px" height="35px"></span><br>
                                                                                <%=airline.AirlineName %><br>
                                                                                (<%=seg.Airline %>) <%=seg.FlightNumber %><br>
                                                                            </td>
                                                                            <td class="f2"><%=seg.Origin.CityName %><br>
                                                                                <%=seg.DepartureTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br>
                                                                                Terminal: <%=seg.DepTerminal %></td>
                                                                            <td class="f3"><%=seg.Destination.CityName %><br>
                                                                                <%=seg.ArrivalTime.ToString("ddd, dd MMM yyyy, HH:mm tt") %><br />
                                                                                Terminal: <%=seg.ArrTerminal %></td>
                                                                            <td class="f4"><%=(seg.Duration.Days>=1)? Math.Floor(seg.Duration.TotalHours) : seg.Duration.Hours %>Hrs <%=seg.Duration.Minutes %>Mins<br>
                                                                                <%=(string.IsNullOrEmpty(seg.CabinClass) ? seg.BookingClass : seg.CabinClass + "-" + seg.BookingClass) %><br>
                                                                                <br>
                                                                            </td>
                                                                            <td rowspan="1" class="f5">Aircraft:  <%=seg.Craft %><span class="cccline">|</span> <span class="cccline">|</span><%=(res2.NonRefundable ? "Non Refundable" : "Refundable") %></td>
                                                                        </tr>
                                                                        <%if (res2.Flights[0].Length > 0)
    { %>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <div class="flight_amid_info"></div>
                                                                            </td>
                                                                        </tr>
                                                                        <%}
    } %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-fareRules-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-fareRules-tab-<%=res2.ResultId %>">
                                                                <div class="height-200 overflow-hidden" style="overflow-y: auto">
                                                                    <div class="small" id="retFareRules<%=counterRFD %>">
                                                                        <div class="padding-10 refine-result-bg-color">
                                                                            Please wait ...
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="pills-baggage-tab-<%=res2.ResultId %>" role="tabpanel" aria-labelledby="pills-baggage-tab-<%=res2.ResultId %>">
                                                                <table id="" class="table table-bordered small" width="100%" border="1" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th style="width: 50%"><strong>Sector / Flight</strong></th>
                                                                            <th style="width: 50%"><strong>Baggage</strong></th>
                                                                        </tr>
                                                                        <%for (int i = 0; i < res2.Flights[0].Length; i++)
    {
        FlightInfo seg = res2.Flights[0][i];
                                                                        %>
                                                                        <tr>
                                                                            <td><%=seg.Origin.AirportCode %> - <%=seg.Destination.AirportCode %> <%=seg.Airline %> <%=seg.FlightNumber %></td>
                                                                             <%if (!string.IsNullOrEmpty(seg.DefaultBaggage)){%>
                                                                                        <td><%=(seg.DefaultBaggage)%> </td><%}%>
                                                                                        <%else
                                                                                          {%>
                                                                                          <td>As Per Airline Policy</td>
                                                                                          <%} %>
                                                                        </tr>
                                                                        <%} %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%counterRFD++; displayDetails = false; %>
                                            <%} %>

        </div>
    </li>






<%}  %>

    </ul>

<%}
    }
    else
    {%>
Your Login Session Expired. Please Search again.
                                            <%}%>
