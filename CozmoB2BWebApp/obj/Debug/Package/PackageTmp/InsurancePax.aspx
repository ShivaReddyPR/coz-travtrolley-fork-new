﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" Inherits="InsurancePax"
    Title="Insurance Passengers" Codebehind="InsurancePax.aspx.cs" %>
    <%@ Import Namespace="CT.BookingEngine.Insurance" %>
    <%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
      <link href="css/steps.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var Ajax;
        var cid;
        var index;
        var check = '<%=check %>';

        function getCities(id, seq) {
            
            cid = id + '' + seq;
            index = seq;
            var paramList = "ccode=";
            var code = "";
            if (document.getElementById('ctl00_cphTransaction_ddlCountry' + id + seq) != null) {
                code = document.getElementById('ctl00_cphTransaction_ddlCountry' + id + seq).value;
            }
            paramList += code;
            paramList += '&requestFrom=cityList';
            var url = "CityAjax";
            if (window.XMLHttpRequest) {
                Ajax = new window.XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }
            var async = false;
            Ajax.onreadystatechange = fillCities;
            Ajax.open('POST', url, async);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function fillCities() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var cityString = Ajax.responseText;
                        arrayCity = cityString.split('/');
                        var SelectId = document.getElementById('ctl00_cphTransaction_ddlCity' + cid);
                        SelectId.options.length = 0; // clear out existing items
                        SelectId.options.add(new Option("Select", "-1"))
                        if (arrayCity[0] != "") {
                            for (var i = 0; i < arrayCity.length; i++) {
                                var city = arrayCity[i].split(',');

                                SelectId.options.add(new Option(city[0], city[1]));
                            }
                            if (check == "True") {
                                setCity2(1, index);
                            }
                        }
                    }
                }
            }
        }

        function setCity(id, seq) {
            if (document.getElementById('ctl00_cphTransaction_ddlCity' + id + seq) != null) {
                var city = document.getElementById('ctl00_cphTransaction_ddlCity' + id + seq);

                document.getElementById('ctl00_cphTransaction_hdnCity' + id + seq).value = city.options[city.selectedIndex].text;
            }
        }
        function setCity2(id, seq) {
            var city = document.getElementById('ctl00_cphTransaction_ddlCity' + id + seq);
            //city.options[city.selectedIndex].text = document.getElementById('ctl00_cphTransaction_hdnCity' + id + seq).value;
            var val = document.getElementById('ctl00_cphTransaction_hdnCity' + id + seq).value;
            for (var x = 1; x < city.options.length; x++) {
                if (city.options[x].innerHTML == val) {
                    city.selectedIndex = x;
                }
            }
        }

        function fillCityOnSubmit() {
            var adultCount = eval('<%=adultCount %>');

            for (i = 0; i < adultCount; i++) {
                getCities(1, i);
            }
        }

        function validatePaxOnSubmit() {
            
            for (j = 1; j <= 3; j++) {
                ValidatePaxDOB(j);    
            }
        }

        function setGender(id, seq) {
            if (document.getElementById('ctl00_cphTransaction_Prefix' + id + seq) != null) {
                var title = document.getElementById('ctl00_cphTransaction_Prefix' + id + seq);

                if (title.value == "Mr") {
                    document.getElementById('ctl00_cphTransaction_ddlGender' + id + seq).value = "Male";
                }
                else if (title.value == "Mrs" || title.value == "Ms") {
                    document.getElementById('ctl00_cphTransaction_ddlGender' + id + seq).value = "Female";
                }
            }   
        }
        
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
                return false;
            }
            return true;
        }
        var isValid = true;
        function ValidatePaxDOB(id) {
            var isSeniorCitizen = '<%=isSeniorCitizen%>';
            var paxCount = eval('<%=paxCount %>');
            var currency = '<%=currencyCode%>';
            var isDomestic = '<%=InsRequest.IsDomesticSearch%>';
            for (i = 0; i < paxCount; i++) {
                var paxDate = eval(0);
                var paxMonth = eval(0);
                var paxYear = eval(0);

                if (document.getElementById('ctl00_cphTransaction_ddlDay' + id + i) != null) {
                    paxDate = document.getElementById('ctl00_cphTransaction_ddlDay' + id + i).value;
                }

                if (document.getElementById('ctl00_cphTransaction_ddlMonth' + id + i) != null) {
                    paxMonth = document.getElementById('ctl00_cphTransaction_ddlMonth' + id + i).value;
                }

                if (document.getElementById('ctl00_cphTransaction_ddlYear' + id + i) != null) {
                    paxYear = document.getElementById('ctl00_cphTransaction_ddlYear' + id + i).value;
                }
                if (paxDate != "-1" && paxMonth != "-1" && paxYear != "-1") {
                    var paxDOB = new Date(eval(paxYear), eval(paxMonth) - 1, eval(paxDate));

                    var age = Math.round((new Date().getTime() - paxDOB.getTime()) / ((24 * 60 * 60 * 1000)));

                    if (age > 0) {

                        if (id == 1) {
                            if (age < 731) {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Adult Age is less than 2 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Adult Age is less than 2 years";
                                        isValid = false;
                                    }

                                }
                            }
                            //if Domestic Travel is True & currency=INR getting adult age should not be greater than 70. 
                            else if (age > 25568 && isDomestic=='True' && currency=="INR")
                            {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Adult Age is greater than 70 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Adult Age is greater than 70 years";
                                        isValid = false;
                                    }
                                }
                            }
                                //27395 old
                            else if (age > 27756&& isSeniorCitizen == 'False' && currency!="INR") { //Adult
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Adult Age is greater than 75 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Adult Age is greater than 75 years";
                                        isValid = false;
                                    }
                                }
                            }
                            //If Domestic is False & currency=INR then Adult age should not be greater than 60 years
                            else if (age > 21915 && isSeniorCitizen == 'False' && currency == "INR" && isDomestic=='False') { //For INR currency adult age should be below 60 years, if exceeds showing validation message
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Adult Age is greater than 60 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Adult Age is greater than 60 years";
                                        isValid = false;
                                    }
                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "";
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "";
                                }
                            }

                        }
                        if (id == 2) {
                            if (age < 731) {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Child Age is less than 2 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Child Age is less than 2 years";
                                        isValid = false;
                                    }

                                }
                            }
                            //If Currency=INR then child age should be 2-12 years
                            else if (age > 4383 && currency == "INR"){
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Child Age is greater than 12 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Child Age is greater than 12 years";
                                        isValid = false;
                                    }
                                }

                            }
                            else if (age > 8037) {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Child Age is greater than 22 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Child Age is greater than 22 years";
                                        isValid = false;
                                    }
                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "";
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "";
                                }
                            }

                        }
                        if (id == 3) {
                            //If domestic Travel is true and Infant agen limit is 6 months -2 years in India
                            if (age < 180 && isDomestic=='True') {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Infant Age should be greater than 6 months";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Infant Age should be greater than 6 months";
                                        isValid = false;
                                    }
                                }
                            }
                            else if (age < 90 && isDomestic == 'False' && currency=="INR") { //For International Travel and Infant age limit is 3 months -2 years in India
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Infant Age should be greater than 3 months";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Infant Age should be greater than 3 months";
                                        isValid = false;
                                    }
                                }
                            }

                            else if (age < 30) {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Infant Age should be greater than 30 days";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Infant Age should be greater than 30 days";
                                        isValid = false;
                                    }
                                }
                            }
                            else if (age > 730) {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                                    if (window.navigator.appName == "Mozilla") {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Infant Age should not be greater than 2 years";
                                        isValid = false;
                                    }
                                    else {
                                        document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Infant Age should not be greater than 2 years";
                                        isValid = false;
                                    }

                                }
                            }
                            else {
                                if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "";
                                    document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "";
                                }
                            }

                        }
                    } else {
                        if (document.getElementById('ctl00_cphTransaction_lblError' + id + i) != null) {
                            document.getElementById('ctl00_cphTransaction_lblError' + id + i).style.color = "#FF0000";
                            if (window.navigator.appName == "Mozilla") {
                                document.getElementById('ctl00_cphTransaction_lblError' + id + i).textContent = "Invalid Date";
                                isValid = false;
                            }
                            else {
                                document.getElementById('ctl00_cphTransaction_lblError' + id + i).innerHTML = "Invalid Date";
                                isValid = false;
                            }
                        }
                    }
                }
            }
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function Validate() {
            isValid = true;
            var adultCount = eval('<%= InsRequest.Adults %>');
            if (adultCount > 0) {
                ValidatePaxDOB(1);
            }
            var childCount = eval('<%= InsRequest.Childs %>');
            if (childCount > 0) {
                ValidatePaxDOB(2);
            }
            var infantCount = eval('<%= InsRequest.Infants %>');
            if (infantCount > 0) {
                ValidatePaxDOB(3);
            }
            if (isValid == true) {
                if (Page_ClientValidate()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    </script>

    
    <div id="PaxDiv">
        
        <div class="wizard-steps">
 
  
  <div  class="completed-step"><a href="#step-one"><span>1</span>Select Policy</a></div>
  <div  class="active-step" ><a href="#step-two"><span>2</span> PAX Details</a></div>
  <div><a href="#"><span>3</span> Secure Payments</a></div>
  <div><a href="#"><span>4</span> Confirmation</a></div>
    <div class="clear"></div>
</div>


<h3> Plan Details</h3>
<div style=" background:#fff">  
   
<table class="table table-bordered">
<%if (selectedPlans != null)
  { %>
  <tr>
   
   
    <td height="24"><b><asp:Label ID="lblinsPlan" runat="server" Text="Insurance Plan"></asp:Label></b></td>

    <td height="24"><b><asp:Label ID="Label3" runat="server" Text="Description"></asp:Label></b></td>

    <td> <b>Amount</b> </td>


   
  </tr>
  <%if (selectedPlans.OTAPlans != null && selectedPlans.OTAPlans.Count > 0)
    {
        %>
    <%for (int m = 0; m < selectedPlans.OTAPlans.Count; m++)
      {
          totalAmount += Math.Round(selectedPlans.OTAPlans[m].NetFare + selectedPlans.OTAPlans[m].InputVATAmount + selectedPlans.OTAPlans[m].Markup - selectedPlans.OTAPlans[m].Discount, Settings.LoginInfo.DecimalValue);
          %>
  

  
  
  
  <tr>
     <td><label><%=selectedPlans.OTAPlans[m].Title%></label></td>
    <td><label><%=selectedPlans.OTAPlans[m].Description.Replace("<>","")%></label></td>
  
  <td> <%=Math.Round(selectedPlans.OTAPlans[m].NetFare + selectedPlans.OTAPlans[m].InputVATAmount  + selectedPlans.OTAPlans[m].Markup - selectedPlans.OTAPlans[m].Discount, Settings.LoginInfo.DecimalValue)%> </td>
  
  </tr>
  <%} %>
  
  <%} %>
  
  
   <%if (selectedPlans.UpsellPlans != null && selectedPlans.UpsellPlans.Count > 0)
     { %>
    <%for (int n = 0; n < selectedPlans.UpsellPlans.Count; n++)
      {
          totalAmount += Math.Round(selectedPlans.UpsellPlans[n].NetFare + selectedPlans.UpsellPlans[n].InputVATAmount + selectedPlans.UpsellPlans[n].Markup - selectedPlans.UpsellPlans[n].Discount, Settings.LoginInfo.DecimalValue);
          %>
 
 
  <tr>
   
   
   
    <td><label><%=selectedPlans.UpsellPlans[n].Title%></label></td>

   
    <td><label><%=selectedPlans.UpsellPlans[n].Description.Replace("<>","")%></label></td>
<td><%=Math.Round(selectedPlans.UpsellPlans[n].NetFare + selectedPlans.UpsellPlans[n].InputVATAmount + selectedPlans.UpsellPlans[n].Markup - selectedPlans.UpsellPlans[n].Discount, Settings.LoginInfo.DecimalValue)%> </td>
    
  </tr>



  <%} %>
  <%} %>




  <tr> 
  
  
  <td>
      <%if ((!string.IsNullOrEmpty(InsRequest.PseudoCode) && InsRequest.CurrencyCode=="INR") || (string.IsNullOrEmpty(InsRequest.PseudoCode) && Settings.LoginInfo.Currency == "INR"))
          { %>
           <b>Grand Total (inclusive of 18% GST): </b> </td><td> </td><td><strong> <%=Settings.LoginInfo.Currency%>  <%=totalAmount%></strong>
      <%}
     else
     {%>
      <b>Grand Total:</b> </td><td> </td><td><strong> <%=Settings.LoginInfo.Currency%> <%=totalAmount%></strong> 
    <%}%>
     </td>
  
  </tr>

<%} %>

</table>
   </div>
        <br />
        <div id="error" style="display: none; color: green">
        </div>
        <div style=" height:auto" class="ns-h3">
            Insurance Passengers
        </div>
        
        <div style="border: solid 1px #ccc;">
        
        
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
            
            
            
            
          <div class="col-md-12">
            
             <div class="alert alert-warning py-2 mt-4 text-center" role="alert">
                 Enter passenger information as per passport, amendment restricted        
             </div>
                
            <center> 
           
            
            <table> 
            
                  <tr>
                    <td height="50" valign="middle" align="right">
                        PNR <span style='color: red;'>*</span>:
                    </td>
                    
                    <td align="left">
                        <asp:TextBox ID="txtPNR" CssClass=" form-control" Width="100" runat="server" CausesValidation="true"
                            ValidationGroup="pax"></asp:TextBox>
                   
                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ErrorMessage="PNR is Mandatory"
                            ControlToValidate="txtPNR" ValidationGroup="pax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                </table>
            
            </center>

           </div>
           
            
            
            <div class="col-md-12">
            
            <table id="tblPassengers"  cellspacing="0" cellpadding="0" runat="server" >
          
            </table>
            
            
            
             </div>
            
            
            
            
            
            <div class="clearfix"> </div>
            
        </div>
        <div style="text-align: right">
            <br />
            <asp:Button CssClass="button-normal" ID="btnSubmit" runat="server" Text="Continue" CausesValidation="true"
                ValidationGroup="pax" OnClick="btnContinue_Click" OnClientClick="return Validate();" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
