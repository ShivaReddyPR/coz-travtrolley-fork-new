﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CozmoB2BWebApp.ViewBookingForHotel" CodeBehind="ViewBookingForHotel.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

     <%--<script>
        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }
    </script>--%>

    <script>
        $(document).ready(function () {

            //if (document.location.search.length) {
                //var HotelId = GetParameterValues('HotelId');
                //var pStatus = GetParameterValues('pStatus');
                var pageParams = JSON.parse(AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'ViewBookingForHotel', 'sessionData':'', 'action':'get'}"));
                var HotelId = "";
                var pStatus = "";
                if (!IsEmpty(pageParams)) {
                    HotelId = !IsEmpty(pageParams.HotelId) ? pageParams.HotelId : "";
                    pStatus = !IsEmpty(pageParams.pStatus) ? pageParams.pStatus : "";
                }
                $('#hdnIsstatus').val(pStatus);
                //if (HotelId != undefined) {
                if (HotelId != "") {
                    $("#HotelId").val(HotelId);
                } 
                GetHotelItineraryInfo(HotelId);    
                GetHotelItineraryPaxInfo(HotelId);
                GetHotelItineraryRoomInfo(HotelId);
                GetHotelItineraryRoomNameSummaryInfo(HotelId);
            //}            
        })
    </script>

    <script>
        function GetHotelItineraryInfo(HotelId) {
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/GetHotelItineraryInfo",
                dataType: "json",
                contentType: "application/json",
                data: "{'HotelId':'" + HotelId + "'}",
                success: function (data) {
                    var d = JSON.parse(data.d); 
                    $('#HotelName').text(d[0].hotelName);                       
                    $('#Address').text(d[0].addressLine1);                        
                    $('#NoOfRooms').text(d[0].noOfRooms);   
                    $('#NoOfRoomsSummary').text(d[0].noOfRooms);                       
                    $('#hdnConfNo').val(d[0].confNo)                    
                    $('#hdnagencyId').val(d[0].agencyId);
                    ///$('#CancellationPolicy').text(d[0].hotelCancelPolicy);
                    var CancellationPolicy = $('#CancellationPolicy').text(d[0].hotelCancelPolicy);
                    CancellationPolicy.html(CancellationPolicy.html().replace('|', '<br/>').replace('^', '<br/>'));
                     //var hotelPolicyDetails = $('#HotelPolicyDetails').text(d[0].hotelPolicyDetails);
                     //hotelPolicyDetails.html(hotelPolicyDetails.html().replace('br /','<br/>').replace('<br />', '<br/>').replace("</p>", "<br/>").replace("<p>", "<br/>").replace("<li>", "<br/>").replace("</ul>", "<br/>").replace("<br />", "<br/>").replace("\n", "").replace("\r", "").replace("</li>", "<br/>"));
                   // $('#HotelPolicyDetails').text(d[0].hotelPolicyDetails);    
                    $('#CheckIn').text(d[0].checkInDate);
                    $('#CheckOut').text(d[0].checkOutDate);                    
                    $('#nights').text(d[0].nights);  
                    $('#Disclaimer').text(d[0].Disclaimer);

                     if ((d[0].hotelPolicyDetails) != "") {
                        $('#HotelPolicyDetails').text(d[0].hotelPolicyDetails);                     
                         $('#HotelNorms').show();
                        $('#divdisclaimer').show();                      
                    }
                    else {
                         $('#HotelNorms').hide();                                               
                    }
                    if (d[0].source == "22") {
                        $('#divdisclaimer').show();
                    }
                    else {
                        $('#divdisclaimer').hide();
                    }
                  
                },
                failure: function (error) { alert('error'); }
            });
        }        
    
        function GetHotelItineraryPaxInfo(HotelId) {          
              var HotelId = $('#HotelId').val();
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/GetHotelItineraryPaxInfo",
                 dataType: "json",
                contentType: "application/json",  
                data: "{'HotelId':'" + HotelId + "'}",                              
                success: function (data) {
                    var d = JSON.parse(data.d);                                         
                    $.each(d, function (i, val) {  
                        if (i == 0) {
                            $('#LGuestName').text(d[0].Name);
                            $('#Email').text(d[0].email);
                            $('#PhoneNo').text(d[0].phone);
                            $('#Nationality').text(d[0].nationality);
                        }                                                  
                    });                         
                }
            })
        }
     
        function GetHotelItineraryRoomInfo(HotelId) {
              var HotelId = $('#HotelId').val();
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/GetHotelItineraryRoomInfo",
                 dataType: "json",
                contentType: "application/json",  
                data: "{'HotelId':'" + HotelId + "'}",             
                success: function (data) {
                    var d = JSON.parse(data.d);                    
                    $("#RoomDetails").html('');
                    var items = '';                    
                    items = '<table class="table table-condensed table-bordered bg_white" cellspacing="0" width="100%" border="0" style="border:0px solid !important;">'
                    var GuestCount = "0";
                    $.each(d, function (i, val) {
                        GuestCount = eval(GuestCount) + (eval(val.adultCount + val.childCount));
                        items += '<thead><tr><th scope="col" style="width: 40.7%;"><b>Room Type</b></th><td>' + val.roomName + '<br> <small class="text-danger">Incl:' + val.MealPlanDesc + '</label></small></td></tr>'                       
                        items += '<tr><th scope="col" style="width: 40.7%;"><b>Room' + eval(i + 1)+ '</b></th><td>' + val.adultCount + '<span>Adult(s),</span>' + val.childCount + '<span>Child(s)</span></td></tr></thead><tbody>'                     
                    $('#hdnpriceId').val(d[0].priceId); 
                    });
                    $('#noOfGuests').text(GuestCount); 
                    items += '</tbody></table>';
                    $("#RoomDetails").html(items);
                  // GetHotelItineraryPriceInfo(d[0].priceId); 
                }
            })
        }

        function GetHotelItineraryRoomNameSummaryInfo(HotelId) {            
              var HotelId = $('#HotelId').val();
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/GetHotelItineraryRoomInfo",
                 dataType: "json",
                contentType: "application/json",  
                data: "{'HotelId':'" + HotelId + "'}",             
                success: function (data) {
                    var lblGSTVAT = "VAT";
                    var VAT = 0;
                    var MARKUP  = 0;
                    var SelectedroomType = JSON.parse(data.d);
                    for (var i = 0; i < SelectedroomType.length; i++) {
                        VAT += SelectedroomType[i].OUTVAT_AMOUNT;
                        MARKUP += SelectedroomType[i].markup;
                    }                    
                    $("#Priceroomsummary").html('');
                     var items = '';                    
                    items = '<table class="table table-condensed">'                   
                var total = 0;               
                    items += "<tr><th scope='row' class='font-weight-bold'>ROOM TYPE</th><td class='font-weight-bold'>TOTAL (in AED)</td></tr>"
                for (var m = 0; m < SelectedroomType.length; m++) {
                    items += "<tr><th scope='row'  class='RoomType' id=FareRoomType-" + m + ">" + SelectedroomType[m].roomName + "</th > <td id=roomPrice" + m + " >" + (SelectedroomType[m].netFare).toFixed(2) + "</td > </tr >"
                    total = (eval(total) + eval(SelectedroomType[m].netFare)).toFixed(2);
                    }
                    total = parseFloat(total) + parseFloat(VAT) + parseFloat(MARKUP);  
                     items += "<tr><th scope='row' >MARKUP</th><td id='Markup'>" + (MARKUP).toFixed(2) + "</td></tr>"
                    items += "<tr><th scope='row'>" + lblGSTVAT + "</th><td id='VAT' >" + (VAT).toFixed(2) + "</td></tr>"                   
                items += "<tr><th scope='row' >TOTAL</th><td id='totalRoomPrice'>" + (total).toFixed(2) + "</td></tr>"
                items += "<tr><th scope='row' class='font-weight-bold grand-total'>GRAND TOTAL</th><td class='font-weight-bold' id='grandTotal'>" + Math.ceil(total).toFixed(2) + "</td></tr>"
                items += '</table>';                    
                     $("#Priceroomsummary").html(items);
                }
            })
        }
        
        function ViewInvoice() { 
            var hotelId = $('#HotelId').val();
            var confNo = $('#hdnConfNo').val();
            var agentId = $('#hdnagencyId').val();
            //window.open("CreateHotelInvoice.aspx?hotelId=" + hotelId + "&agencyId=" + agentId + "&ConfNo=" + confNo + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CreateHotelInvoice', 'sessionData':'" + (hotelId + '|' + agentId + '|' + confNo) + "', 'action':'set'}");
            window.open("CreateHotelInvoice.aspx", "HotelInvoice", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
            return false;
        }

        function ViewVoucher() {
            var hotelId = $('#HotelId').val();
            var confNo = $('#hdnConfNo').val();
            //var approvalStatus = "";
            var approvalStatus = $('#hdnIsstatus').val();
            if (approvalStatus!=null &&  approvalStatus!='') {
                //window.open("CorpHotelSummary.aspx?ConfNo=" + confNo +"&HotelId=" + hotelId +"", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CorpHotelSummary', 'sessionData':'" + (confNo + '|' + hotelId) + "', 'action':'set'}");
                window.open("CorpHotelSummary.aspx", "Summary", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
            } else {
                //window.open("printHotelVoucher.aspx?ConfNo=" + confNo + "&HotelId=" + hotelId + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PrintHotelVoucher', 'sessionData':'" + (confNo + '|' + hotelId) + "', 'action':'set'}");
            window.open("printHotelVoucher.aspx", "Voucher", "_blank", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                }
                return false;
        }

        function ViewDocs() {
            var hotelId = $('#HotelId').val();
            //var wnd = window.open('HotelInvoiceDocuments.aspx?id=' + hotelId, '_blank', 'width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no');
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'HotelInvoiceDocuments', 'sessionData':'" + (hotelId) + "', 'action':'set'}");
            window.open("HotelInvoiceDocuments.aspx", "HotelInvoiceDocuments", "_blank", "width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no");
        }      

        function GetBookingHistory() {
              var HotelId = $('#HotelId').val();
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/GetBookingHistory",
                 dataType: "json",
                contentType: "application/json",  
                data: "{'HotelId':'" + HotelId + "'}",               
                success: function (data) {
                    var d = JSON.parse(data.d);
                    $("#BookHistory").html('');
                    var items = '';
                    items = '<table class="table-standard table table-condensed table-bordered mb-0" cellspacing="0" width="100%" border="0" style="border:0px solid !important;">'
                    items += '<thead><tr><th scope="col">Date</th><th scope="col">Time (Local)</th><th scope="col">Narration</th><th scope="col">Consultant</th></tr></thead><tbody>'                   
                    $.each(d, function (i, val) {                                            
                        items += '<tr><td>' + val.date + '</td><td>' + val.Time + '</td><td>' + val.narration + '</td><td>' + val.LastModifiedName + '</td></tr>'
                    });
                    items += '</tbody></table>';
                    $("#BookHistory").html(items);
                }
            })
        }

        function AddComments() {
            var HotelId = $('#HotelId').val();            
            var RequestRemarks = $.trim($('#<%=txtComments.ClientID%>').val());
            if (RequestRemarks == "") {
                alert("Please enter Remarks");
                return false;
            }
             var obj = {};
            obj.HotelId = HotelId;
            obj.Comments = RequestRemarks;
            var jsons = JSON.stringify(obj);
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/AddComment",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: jsons,
                success: function (data) {
                    $('#<%=txtComments.ClientID%>').val("");
                    GetBookingHistory();
                }
            })
        }

        function ShowPaxDetails() {
            var HotelId = $('#HotelId').val();
            $.ajax({
                type: 'POST',
                url: "ViewBookingForHotel.aspx/GetHotelAllPaxInfo",
                 dataType: "json",
                contentType: "application/json",  
                data: "{'HotelId':'" + HotelId + "'}",                              
                success: function (data) {
                    var d = JSON.parse(data.d);                                      
                    $("#PaxHistory").html('');
                    var items = '';
                    var paxType='';
                    var leadPassenger='';
                    items = '<table class="table-standard table table-condensed table-bordered mb-0" cellspacing="0" width="100%" border="0">'                   
                    items += '<thead><tr><th scope="col">Name</th><th scope="col">Pax Type</th><th scope="col">Age</th><th scope="col">Lead Passenger</th><th scope="col">Phone No.</th><th scope="col">Nationality</th><th scope="col">Email</th><th scope="col">RoomId</th></tr></thead><tbody>'
                    $.each(d, function (i, val) {
                        if (val.paxType == 1)
                            paxType = "Adult";
                        else
                            paxType = "Child";
                        if (val.leadPassenger == true)
                            leadPassenger = "Lead Passenger";
                        else
                            leadPassenger = "";
                        items += '<tr><td>' + val.Name + '</td><td>' + paxType + '</td><td>' + val.Age + '</td><td>' + leadPassenger + '</td><td>' + val.phone + '</td><td>' + val.nationality + '</td><td>' + val.email + '</td><td>' + val.roomId + '</td></tr>'                            
                    });
                    items += '</tbody></table>';                 
                    $("#PaxHistory").html(items);           
                }
            })
            $("#PaxDetailsPop").modal("show");    
        }
    </script>

           
<div class="row">

    <div class="col-md-9">
        <input type="hidden" id="HotelId" />
        <input type="hidden" id="hdnConfNo" />
        <input type="hidden" id="hdnagencyId" />
        <input type="hidden" id="hdnpriceId"/>
        <input type="hidden" id="hdnLoginInfo" runat="server" />
        <input type="hidden" id="hdnIsstatus" />
        <%--<asp:HiddenField runat="server" ID="hdfIsCorporate" Value=""></asp:HiddenField>--%>
       
        <div class="row">           
            <div class="col-12 mb-3 text-right">  
                <%--<a href="HotelBookingQueue.aspx" style="float:left;">&lt;&lt; Back To Booking Queue</a>--%>
                <a href="javascript: history.go(-1)" style="float:left;">&lt;&lt; Back To Booking Queue</a>
                
                <%--<input type="button" class="btn btn-danger" value="Cancel Booking">--%>
                <%--<input type="button" class="btn btn-primary" value="View Voucher">--%>
                <input type="button" class="btn btn-primary" value="View Documents" onclick="ViewDocs();">
            </div>
            <div class="col-12">
                <div class="ns-h3">Booking Details </div>
                <div class="bg_white mb-4 p-3 bor_gray">
                    <h4><label id="HotelName"></label></h4>
                    <p class="text-muted" id="Address"></p>
                    <p class="mt-2">
                        <label class="mr-3">                            
                            <%--<span class="icon-calendar"></span>--%> Check In: <strong id="CheckIn"></strong></label>
                        <label class="mr-3">
                            <%--<span class="icon-calendar"></span>--%> Check Out: <strong id="CheckOut"></strong></label>
                        <label class="mr-3" id="nights"></label><label>Nights</label> 
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">      
                <div id=RoomDetails style="margin-bottom:-20px;"></div>   
                <table class="table table-condensed table-bordered bg_white">                 
                    <tr>
                        <td style="width: 40.7%;"><b> No. of Rooms </b></td>
                        <td id="NoOfRooms"></td>
                    </tr>
                    <tr>
                        <td><b>No. of Guests -</b></td>
                        <td id="noOfGuests"></td>
                    </tr>
                    <tr id="RoomCountDetails">                     
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="ns-h3"> Lead Guest </div>
                <div class="bg_white mb-2 p-3 bor_gray">
                    <div class="row">
                        <div class="col-8 col-lg-8">
                            <div class="mb-1 font-weight-bold"> Name : <label id="LGuestName"></label></div>
                            <div class="mb-1"> Email : <label id="Email"></label></div>
                            <div class="mb-1"> PhoneNo : <label id="PhoneNo"></label></div>
                            <div class="mt-2"> <strong> Guest Nationality </strong> : <label id="Nationality"></label></div>
                        </div>
                        <div class="col-4 col-lg-4 text-right">
                            <a href="#" onclick="ShowPaxDetails();"> <span class="icon-plus"></span> Show Pax Details </a>
                        </div>
                       
                           <div class="modal fade in farerule-modal-style" data-backdrop="static" id="PaxDetailsPop" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display:none;">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" id="btnModelC">&times;</button>
                                        <h4 class="modal-title">Pax Details</h4>
                                    </div>
                                    <div class="modal-body"> 
                                        <p class="Ruleshdr" id="PaxHistory"></p>                                       
                                    </div>              
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>

            <div class="col-12"> <small class="text-danger">Last minute booking – Please allow 2 to 3 hours to update booking status.</small> </div>
        </div>

        <div class="row mt-4" id="HotelNorms">
            <div class="col-12">
                <div class="ns-h3"> Hotel Norms </div>
                <div class="bg_white mb-2 p-3 bor_gray">
                    <p>
                        <label id="HotelPolicyDetails"></label>                     
                    </p>
                </div>
            </div>
        </div>

          <div class="row mt-4" id="divdisclaimer">
            <div class="col-12">
                <div class="ns-h3"> Disclaimer </div>
                <div class="bg_white mb-2 p-3 bor_gray">
                    <p>
                        <label id="Disclaimer"></label>  <br />
                        <label class="bold"> Bed type availabilities depends @ Check-In time </label>
                    </p>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-12">
                <div class="ns-h3"> Cancellation and Charges </div>
                <div class="bg_white mb-2 p-3 bor_gray">
                    <label id="CancellationPolicy"></label>                 
                   <%-- <div class="text-info"> <small> Early check out will attract full cancellation charge unless otherwise specified. </small> </div>--%>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-3">
         <div class="ns-h3">Sales Summary </div>
        <div class="bg_white mb-2" id="Priceroomsummary"></div>

         <%--<a class="btn btn-primary showMoreCollapse" data-toggle="collapse" href="#showMoreCollapse" data-text="Show More" data-active-text="Hide" role="button" aria-expanded="false" aria-controls="showMoreCollapse">
            Show More
         </a>--%>

        <div class="collapse mt-3" id="showMoreCollapse">
          <div class="bg_white">
             <table class="table table-condensed">
                <tr class="bg-light">
                    <td> <strong >Agent Markup</strong> </td>
                    <td class="text-right"><strong> AED <%--0.00 --%> </strong><strong id="Markup"></strong></td>
                </tr>
                <tr class="border-bottom">
                    <td> Net Fare </td>
                    <td class="text-right"> AED <label id="netFare"></label> <%--14,564--%>   </td>
                </tr>         
             </table>
          </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-12 col-lg-8">
        <div class="row">
            <div class="col-12 d-flex">
               <%-- <a class="mr-3 p-2 bg-white bor_gray font-weight-bold" href="javascript:void(0)"> <span class="icon-mail"></span> Email Booking </a>
                <a class="mr-3 p-2 bg-white bor_gray font-weight-bold" href="javascript:void(0)"> <span class="icon-print"></span> Print Booking </a>--%>
                <a class="mr-3 p-2 bg-white bor_gray font-weight-bold" href="javascript:void(0)" onclick="showStuff('bookingHistory')"> <span class="icon-clock"></span> Booking History </a>                 
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-4">
        <div class="input-group">
            <div class="input-group-prepend col-10">
                <asp:TextBox ID="txtComments" placeholder="Enter Comments" TextMode="MultiLine" Rows="0" Height="36px" CssClass="form-control" runat="server"> </asp:TextBox>
            </div>
            <input type="button" class="btn btn-primary" value="Submit" onclick="AddComments()">
        </div>
    </div>
</div>

<div class="row">
    <div id="bookingHistory" style="display:none" class="col-12 mt-2 mb-3">         
        <div class="ns-h3"> Booking History
            <a class="text-white" href="javascript:void(0)" onclick="hidestuff('bookingHistory')"> <span class="icon-cancel float-right"></span></a>
        </div>
        <div id="BookHistory"></div>
    </div>

    <div class="col-12 text-right mt-5">
        <% if (Settings.LoginInfo.IsCorporate != "Y")
            { %>
        <input type="button" class="btn btn-primary" onclick="ViewInvoice();" value="View Invoice">
        <%} %>
        <input type="button" class="btn btn-primary" onclick="ViewVoucher();" value="View Voucher">
    </div>
</div>

<script type="text/javascript">
    function showStuff(id) {
        GetBookingHistory();
        document.getElementById(id).style.display = 'block';
    }
    function hidestuff(boxid) {   
        document.getElementById(boxid).style.display = "none";
    }
    $(document).ready(function () { 
        $('#showMoreCollapse').on('show.bs.collapse', function (e) {
            $('.showMoreCollapse').text($('.showMoreCollapse').data('active-text'))
        })
        $('#showMoreCollapse').on('hide.bs.collapse', function (e) {
            $('.showMoreCollapse').text($('.showMoreCollapse').data('text'))
        })   
    })
</script>
   
</asp:Content>
