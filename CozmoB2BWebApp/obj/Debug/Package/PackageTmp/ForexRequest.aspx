﻿<%@ Page Title="Forex Request" Language="C#" MasterPageFile="~/TransactionBE.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ForexRequest.aspx.cs" Inherits="CozmoB2BWebApp.ForexRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div class="body_container" style="margin-top: 20px;">
        <div class="row" id="divBaseAgent">
            <div class="col-md-2">
                <div class="form-group">
                    <span>Select Agent Type</span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <asp:HiddenField ID="hdnFRID" runat="server" />
                    <asp:HiddenField ID="hdnFRRID" runat="server" />
                    <asp:HiddenField ID="hdnFRRefNo" runat="server" />
                    <asp:HiddenField ID="hdnAgentType" runat="server" />
                    <asp:RadioButton ID="rbself" runat="server" Text="Self" GroupName="agents" checked="true" onchange="ShowAgentDetails()" />
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <asp:RadioButton ID="rbb2b" runat="server" Text="Agent" GroupName="agents"  onchange="ShowAgentDetails()" /> 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group"  id="divagent" style="display:none">
                    <span>Select Agent</span>
                    <asp:DropDownList ID="ddlagents" runat="server" CssClass="form-control"   style="display:none" onchange="BindLocations();">
                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                    </asp:DropDownList>
                    <span id="errorddlagents" style="color: red;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group"  id="divAgentLoc" style="display:none">
                    <span>Select Agent Location</span>
                    <asp:DropDownList ID="ddlAgentLoc" runat="server" CssClass="form-control"  style="display:none">
                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                    </asp:DropDownList>
                    <span id="errorddllocation" style="color: red;"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <span>Select Product</span>
                    <asp:DropDownList ID="ddlProduct" CssClass="form-control" runat="server">
                        <asp:ListItem Value="-1">-- Select Product--</asp:ListItem>
                        <asp:ListItem Value="Multicurrency Card">Multicurrency Card</asp:ListItem>
                        <asp:ListItem Value="Cash">Cash</asp:ListItem>
                    </asp:DropDownList>
                    <span id="errorProduct" style="color: red;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Select Currency</span>
                    <asp:DropDownList ID="ddlCurrency" CssClass="form-control" runat="server">
                        <asp:ListItem Value="-1">-- Select Currency--</asp:ListItem>
                        <asp:ListItem Value="US Dollar">US Dollar</asp:ListItem>
                        <asp:ListItem Value="Euro">Euro</asp:ListItem>
                        <asp:ListItem Value="Sterling Pound">Sterling Pound</asp:ListItem>
                        <asp:ListItem Value="ThaiBhat">Thai Bhat</asp:ListItem>
                        <asp:ListItem Value="Singapore Dollar">Singapore Dollar</asp:ListItem>
                        <asp:ListItem Value="Canadian Dollar">Canadian Dollar</asp:ListItem>
                        <asp:ListItem Value="Malaysian Ringgit">Malaysian Ringgit</asp:ListItem>
                        <asp:ListItem Value="UAE Dirham">UAE Dirham</asp:ListItem>
                        <asp:ListItem Value="Hongkong Dollar">Hongkong Dollar</asp:ListItem>
                        <asp:ListItem Value="AustralianDollar">Australian Dollar</asp:ListItem>
                    </asp:DropDownList>
                    <span id="errorCurrency" style="color: red;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Forex</span>
                    <asp:TextBox ID="txtForex" runat="server" CssClass="form-control" placeholder="Forex"   />
                </div>
                  <span id="errorForex" style="color: red;"></span>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <span>Amount</span>
                    <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" placeholder="Amount"  />
                </div>
                 <span id="errorAmount" style="color: red;"></span>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Name</span>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Name" />
                </div>
                 <span id="errorName" style="color: red;"></span>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Mobile No</span>
                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" placeholder="Mobile No" onKeyPress="return isNumber(event)" />
                </div>
                 <span id="errorMobileNo" style="color: red;"></span>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Email</span>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email" />
                </div>
                 <span id="errorEmail" style="color: red;"></span>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Travelling To</span>
                    <asp:TextBox ID="txtTravelling" runat="server" CssClass="form-control" placeholder="Travelling To" />
                </div>
                 <span id="errorTravelling" style="color: red;"></span>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <asp:Button ID="btnsave" CssClass="btn btn-primary" Style="float: right;margin-left:3px;" OnClick="btnsave_Click" runat="server" Text="Get Quote" OnClientClick="javascript:return ValidaeRequest();" />
            </div>
            <div class="form-group">
                <asp:Button ID="btnClear" CssClass="btn btn-primary" Style="float: right;margin-left:3px;" OnClick="btnClear_Click" runat="server" Text="Clear" OnClientClick="javascript:return Clear();" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function ValidaeRequest() {
            var isvalid = true;
            var isChecked = $("#ctl00_cphTransaction_rbb2b").is(":checked");
            if (isChecked) {
                var ddlagents = $("#<%=ddlagents.ClientID%>").val();
                if (ddlagents == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlagents").focus();
                    $("#s2id_ctl00_cphTransaction_ddlagents").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddlagents").innerText = "Please select Agent.";
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlagents").css("border-color", "");
                    document.getElementById("errorddlagents").innerText = "";
                }
                 var ddlAgentLoc = $("#<%=ddlAgentLoc.ClientID%>").val();
                if (ddlAgentLoc == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlAgentLoc").focus();
                    $("#s2id_ctl00_cphTransaction_ddlAgentLoc").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorddllocation").innerText = "Please select Agent Location.";
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlAgentLoc").css("border-color", "");
                    document.getElementById("errorddllocation").innerText = "";
                }
            }
            var ddlProduct = $("#<%=ddlProduct.ClientID%>").val();
            if (ddlProduct == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlProduct").focus();
                $("#s2id_ctl00_cphTransaction_ddlProduct").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorProduct").innerText = "Please select Agent.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlProduct").css("border-color", "");
                document.getElementById("errorProduct").innerText = "";
            }

            var ddlCurrency = $("#<%=ddlCurrency.ClientID%>").val();
            if (ddlCurrency == "-1") {
                $("#s2id_ctl00_cphTransaction_ddlCurrency").focus();
                $("#s2id_ctl00_cphTransaction_ddlCurrency").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorCurrency").innerText = "Please select Currency.";
            }
            else {
                $("#s2id_ctl00_cphTransaction_ddlCurrency").css("border-color", "");
                document.getElementById("errorCurrency").innerText = "";
            }
            var txtForex = $("#<%=txtForex.ClientID%>").val();
            if (txtForex == "") {
                $("#<%=txtForex.ClientID%>").focus();
                $("#<%=txtForex.ClientID%>").css("border-color", "Red");
                document.getElementById("errorForex").innerText = "Please Enter Forex.";
                isvalid = false;
            } else {
                $("#<%=txtForex.ClientID%>").css("border-color", "");
                document.getElementById("errorForex").innerText = "";
            }

            var txtAmount = $("#<%=txtAmount.ClientID%>").val();
            if (txtAmount == "") {
                $("#<%=txtAmount.ClientID%>").focus();
                $("#<%=txtAmount.ClientID%>").css("border-color", "Red");
                document.getElementById("errorAmount").innerText = "Please Enter Amount.";
                isvalid = false;
            } else {
                $("#<%=txtAmount.ClientID%>").css("border-color", "");
                document.getElementById("errorAmount").innerText = "";
            }
            var txtName = $("#<%=txtName.ClientID%>").val();
            if (txtName == "") {
                $("#<%=txtName.ClientID%>").focus();
                $("#<%=txtName.ClientID%>").css("border-color", "Red");
                document.getElementById("errorName").innerText = "Please Enter Name.";
                isvalid = false;
            } else {
                $("#<%=txtName.ClientID%>").css("border-color", "");
                 document.getElementById("errorName").innerText = "";
            }
             var txtMobileNo = $("#<%=txtMobileNo.ClientID%>").val();
            if (txtMobileNo == ""  || parseInt(txtMobileNo[0]) < 6) {
                $("#<%=txtMobileNo.ClientID%>").focus();
                $("#<%=txtMobileNo.ClientID%>").css("border-color", "Red");
                document.getElementById("errorMobileNo").innerText = "Please Enter Mobile No.";
                if (txtMobileNo.length < 10 || parseInt(txtMobileNo[0]) < 5)
                    document.getElementById("errorMobileNo").innerText = "Please Enter valid Mobile No.";
                isvalid = false;
            } else {
                $("#<%=txtMobileNo.ClientID%>").css("border-color", "");
                document.getElementById("errorMobileNo").innerText = "";
            }
            var txtEmail = $("#<%=txtEmail.ClientID%>").val();
            if (txtEmail == "") {
                $("#<%=txtEmail.ClientID%>").focus();
                $("#<%=txtEmail.ClientID%>").css("border-color", "Red");
                isvalid = false;
                document.getElementById("errorEmail").innerText = "Please Enter Email.";
            } else {
                var mailformat = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                if (!(txtEmail.match(mailformat))) {
                    $("#<%=txtEmail.ClientID%>").focus();
                    $("#<%=txtEmail.ClientID%>").css("border-color", "Red");
                    isvalid = false;
                    document.getElementById("errorEmail").innerText = "Please Enter Valid Email.";
                } else {
                    $("#<%=txtEmail.ClientID%>").css("border-color", "");
                    document.getElementById("errorEmail").innerText = "";
                }
            }
           
            var txtTravelling = $("#<%=txtTravelling.ClientID%>").val();
            if (txtTravelling == "") {
                $("#<%=txtTravelling.ClientID%>").focus();
                $("#<%=txtTravelling.ClientID%>").css("border-color", "Red");
                document.getElementById("errorTravelling").innerText = "Please Enter Travelling.";
                isvalid = false;
            } else {
                $("#<%=txtTravelling.ClientID%>").css("border-color", "");
                document.getElementById("errorTravelling").innerText = "";
            }
            return isvalid;
        }
        function ShowAgentDetails() {
            var isChecked = $("#ctl00_cphTransaction_rbb2b").is(":checked");
            isChecked ? $("#divagent").show() : $("#divagent").hide();
            isChecked ? $("#divAgentLoc").show() : $("#divAgentLoc").hide();
            isChecked ? $("#<%=ddlAgentLoc.ClientID%>").show() : $("#<%=ddlAgentLoc.ClientID%>").hide();
            isChecked ? $("#<%=ddlagents.ClientID%>").show() : $("#<%=ddlagents.ClientID%>").hide();
        }
         function message(msg) {
            alert(msg); 
        }
        function BindLocations() {
            var agentId = parseInt($("#<%=ddlagents.ClientID%>").val());
            var options = '';
            if (agentId > 0) {
                $.ajax({
                    type: "POST",
                    url: "ForexRequest.aspx/BindLocations",
                    contentType: "application/json; charset=utf-8",
                    data: "{'agentid':'" + $("#<%=ddlagents.ClientID%>").val() + "'}",
                    dataType: "json",
                    async: true,
                    success: function (data) {
                        options = '<option value="-1">--Select Location--</option>';
                        $.each(data.d, function (index, item) {
                            options += '<option value="' + index + '">' + item + '</option>'
                        });
                        $('#ctl00_cphTransaction_ddlAgentLoc').html(options); 
                        $('#' + getElement('ddlAgentLoc').id).select2('val', '-1');
                    },
                    error: (error) => {
                        console.log(JSON.stringify(error));
                    }
                });
            }
            else {
                options = '<option value="-1">--Select Location--</option>';
                $('#ctl00_cphTransaction_ddlAgentLoc').html(options); 
                 $('#' + getElement('ddlAgentLoc').id).select2('val', '-1');
            }
        }
        function pageLoad() {
            var agentType = $("#<%=hdnAgentType.ClientID%>").val();
            agentType == "BaseAgent" ? $('#divBaseAgent').show():$('#divBaseAgent').hide();
       }
    </script>
</asp:Content> 
