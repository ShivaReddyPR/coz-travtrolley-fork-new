﻿<%@ Page Language="C#" %>
	<!DOCTYPE html>
	<html>

	<head id="Head1" runat="server">
		<title>CozmoMICE</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">

		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="bizzlogin.aspx"><img src="images/logoc.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="heading_bizz">
			<div class="container">
				<h2> CozmoMICE </h2>
			</div>
		</div>
		<form runat="server" id="form1">
			<div class="container">
				<div class="innerPage2">
					<div class="row">
						<div class="col-12">
							<p>CozmoMICE is the only partner you need to pull off events, of any size, across the globe. Be it in Bali or Baku, Dubai or Delhi, our experienced team of organizers are familiar with what it takes to organize successful, large-scale productions to complement your brand and showcase your company in the best possible light, to the outside world.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="footer_sml_bizz">Copyright 2019 Cozmo Travel World, India. All Rights Reserved.</div>

	</body>

	</html>
