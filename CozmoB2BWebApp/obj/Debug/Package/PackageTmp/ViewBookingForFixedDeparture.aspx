﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ViewBookingForFixedDepartureGUI"
    Title="View Booking For FixedDeparture" Codebehind="ViewBookingForFixedDeparture.aspx.cs" %>
<%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<!-- ----------------------For Calender Control--------------------------- -->
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<!-- ----------------------End Calender Control--------------------------- -->
    <style type="text/css">
        .heading
        {
            background: #0000FF;
            font-size: 13px;
            margin-left: 10px;
            font-weight: bold;
        }
        
        a.nounderline
        {
            text-decoration: none;
        }
        
        .font-nor { font-weight:normal!important; color:#fff; }
        
        

        
    </style>
    <script type="text/javascript">

        var cal1;
        var cal2;
        function init() {
            var dt = new Date();
            cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
            cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
            cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
            //        cal1.cfg.setProperty("title", "Select your desired checkin date:");
            cal1.cfg.setProperty("close", true);
            cal1.cfg.setProperty("iframe", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();


            cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
            cal2.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
            cal2.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
            cal2.cfg.setProperty("close", true);
            cal2.cfg.setProperty("iframe", true);
            cal2.selectEvent.subscribe(setDate2);
            cal2.render();
            
        }

        function showCalendar1() {

            document.getElementById('container1').style.display = "block";
            document.getElementById('Outcontainer1').style.display = "block";
        }
        function showCalendar2() {

            document.getElementById('container2').style.display = "block";
            document.getElementById('Outcontainer2').style.display = "block";
        }

        var departureDate = new Date();

        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];
            var dt = new Date();
            this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('errMessHotel').style.visibility = "visible";
                document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
                return false;
            }
            departureDate = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtDepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal1.hide();
            document.getElementById('Outcontainer1').style.display = "none";
        }

        function setDate2() {
            var date1 = cal2.getSelectedDates()[0];
            var dt = new Date();
            this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('errMessHotel').style.visibility = "visible";
                document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
                return false;
            }
            departureDate = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtVisaDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal2.hide();
            document.getElementById('Outcontainer2').style.display = "none";
        }
        YAHOO.util.Event.addListener(window, "load", init);
        YAHOO.util.Event.addListener(window, "click", init);
    
    

    
    </script>
    <script type="text/javascript">
        function CalculateBalance() {
            var currency = '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>';
            var cash = eval(document.getElementById('<%=txtCash.ClientID %>').value);
            var credit = eval(document.getElementById('<%=txtCredit.ClientID %>').value);
            var card = eval(document.getElementById('<%=txtCard.ClientID %>').value);
            var paidAmount = eval(document.getElementById('<%=hdnPaidAmount.ClientID %>').value);
            var total = eval('<%=(total)%>') - paidAmount;
            var amountPaid = eval(0);
            var charge = eval(document.getElementById('<%=ddlCreditCard.ClientID %>').value);
            if (document.getElementById('<%=txtCash.ClientID %>').value == "") {
                cash = eval(0);
            }
            if (document.getElementById('<%=txtCredit.ClientID %>').value == "") {
                credit = eval(0);
            }
            if (document.getElementById('<%=txtCard.ClientID %>').value == "") {
                card = eval(0);
            }
            cash = Math.ceil(cash);
            card = Math.ceil(card);
            credit = Math.ceil(credit);
            total = Math.ceil(total);

            //card += (card * charge / 100);
            card = Math.ceil(card);
            document.getElementById('CardAmount').innerHTML = Math.ceil(eval(card * charge / 100)).toFixed(2);

            if (document.getElementById('tblCash').style.display == "block" && eval(cash) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                return false;
            }
            else if (document.getElementById('tblCredit').style.display == "block" && eval(cash + credit) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total - cash - credit)).toFixed(2);
                return false;
            }
            else if (document.getElementById('tblCard').style.display == "block" && eval(cash + card) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total - cash - card)).toFixed(2);
                return false;
            }
            else if (document.getElementById('tblCredit').style.display == "block" && document.getElementById('tblCard').style.display == "block" && eval(cash + credit + card) > eval(total)) {
                document.getElementById('errorMsg').style.display = "block";
                document.getElementById('errorMsg').innerHTML = "Payment amount should be less than or equal to Total";
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total - cash - credit - card)).toFixed(2);
                return false;
            }
            
            else {

                document.getElementById('errorMsg').style.display = "none";
                document.getElementById('errorMsg').innerHTML = "";
                amountPaid = cash;
                if (document.getElementById('tblCredit').style.display == "block") {
                    total -= credit;
                    amountPaid += credit;
                }
                if (document.getElementById('tblCard').style.display == "block") {
                    total -= Math.ceil(card);
                    amountPaid += Math.ceil(card);

                }
                if (document.getElementById('tblNextPayment').style.display = "block") {
                    document.getElementById('tblNextPayment').style.display = "none"

                }

                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total - cash)).toFixed(2);
                if (eval(cash) < eval(total) || eval(cash + credit) < eval(total) || eval(cash + card) < eval(total) || eval(cash + credit + card) < eval(total)) {
                    document.getElementById('tblNextPayment').style.display = "block";
                }
                else {
                    document.getElementById('<%=txtDepDate.ClientID %>').value = "";
                }
                return true;
            }
        }

        function ShowHideCreditCard() {
            var ddl = document.getElementById('<%=ddlSettlementMode.ClientID %>');
            var currency = '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>';
            var total = eval('<%=(total-paidAmount)%>');
            var charge = eval(document.getElementById('<%=ddlCreditCard.ClientID %>').value);

            if (ddl.options[ddl.selectedIndex].text.indexOf("Card") >= 0) {
                if (ddl.options[ddl.selectedIndex].text == "Card") {
                    document.getElementById('tblCash').style.display = "none";
                    document.getElementById('tblCredit').style.display = "none";
                    document.getElementById('<%=txtCash.ClientID %>').value = "0";
                    document.getElementById('<%=txtCredit.ClientID %>').value = "0";
                }
                else {
                    document.getElementById('tblCash').style.display = "block";
                    document.getElementById('tblCredit').style.display = "none";
                }
                document.getElementById('tblCreditCard').style.display = "block";
                document.getElementById('tblCard').style.display = "block";
                
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total )).toFixed(2);
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = currency + " " + Math.ceil(eval(total)).toFixed(2);
            }
            else {
                document.getElementById('tblCreditCard').style.display = "none"
                document.getElementById('tblCard').style.display = "none";
                document.getElementById('tblCredit').style.display = "none";
            }

            if (ddl.options[ddl.selectedIndex].text.indexOf("Credit") >= 0) {
                if (ddl.options[ddl.selectedIndex].text == "Credit") {
                    document.getElementById('tblCredit').style.display = "block";
                    document.getElementById('tblCreditCard').style.display = "none"
                    document.getElementById('tblCard').style.display = "none";
                    document.getElementById('tblCash').style.display = "none";
                }
                else {
                    document.getElementById('tblCredit').style.display = "none";
                }
                document.getElementById('tblCredit').style.display = "block";
            }
            if (ddl.options[ddl.selectedIndex].text.indexOf("Cash") >= 0) {
                if (ddl.options[ddl.selectedIndex].text == "Cash") {
                    document.getElementById('tblCash').style.display = "block";
                    document.getElementById('tblCredit').style.display = "none";
                    document.getElementById('tblCreditCard').style.display = "none"
                    document.getElementById('tblCard').style.display = "none";
                    
                }
                else {
                    document.getElementById('tblCash').style.display = "none";
                }
                document.getElementById('tblCash').style.display = "block";
            }
//            else {
//                document.getElementById('tblCredit').style.display = "none"
//                document.getElementById('tblCard').style.display = "none"
//            }
            CalculateBalance();
        }

        function ApplyCreditCardCharge() {
            var ddl = document.getElementById('<%=ddlCreditCard.ClientID %>');
            var total = eval('<%=(total-paidAmount) %>');
            var card = eval(document.getElementById('<%=txtCard.ClientID %>').value);
            
            if (ddl.selectedIndex > 0) {
                var charge = eval(ddl.value);
                //total += card * charge / 100;
                document.getElementById('<%=hdnCardType.ClientID %>').value = ddl.options[ddl.selectedIndex].text;
                document.getElementById('<%=lblCharge.ClientID %>').innerHTML = parseFloat(charge).toFixed(2);
                document.getElementById('CardAmount').innerHTML = Math.ceil((card * charge / 100)).toFixed(2);
                document.getElementById('<%=lblTotalAmount.ClientID %>').innerHTML = Math.ceil(total).toFixed(2);
                document.getElementById('<%=lblBalance.ClientID %>').innerHTML = Math.ceil(total).toFixed(2);
            }
            CalculateBalance();
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 46) {
                return true;
            }
            if ((charCode > 31) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function Validate() {
            if (document.getElementById('<%=txtCash.ClientID %>').value == "") {
                document.getElementById('<%=txtCash.ClientID %>').value = "0";
            }
            if (document.getElementById('<%=txtCredit.ClientID %>').value == "") {
                document.getElementById('<%=txtCredit.ClientID %>').value = "0";
            }
            if (document.getElementById('<%=txtCard.ClientID %>').value == "") {
                document.getElementById('<%=txtCard.ClientID %>').value = "0";
            }
            //            if (('<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId %>') == '1') {
            var cash = eval(document.getElementById('<%=txtCash.ClientID %>').value);
            var credit = eval(document.getElementById('<%=txtCredit.ClientID %>').value);
            var card = eval(document.getElementById('<%=txtCard.ClientID %>').value);
            cash = Math.ceil(cash);
            card = Math.ceil(card);
            credit = Math.ceil(credit);
            if (document.getElementById('tblCash').style.display == "block" && document.getElementById('tblCredit').style.display == "block" && document.getElementById('tblCard').style.display == "block") {
                if (cash == Math.ceil(0) && card == Math.ceil(0) && credit == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            } else if (document.getElementById('tblCash').style.display == "block" && document.getElementById('tblCredit').style.display == "block") {
                if (cash == Math.ceil(0) && credit == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            } else if (document.getElementById('tblCash').style.display == "block" && document.getElementById('tblCard').style.display == "block") {
                if (cash == Math.ceil(0) && card == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            else if (document.getElementById('tblCash').style.display == "block") {
                if (cash == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            else if (document.getElementById('tblCredit').style.display == "block") {
                if (credit == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }
            else if (document.getElementById('tblCard').style.display == "block") {
                if (card == Math.ceil(0)) {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Payment Amount";
                    return false;
                }
            }

            if (document.getElementById('tblNextPayment').style.display == "block") {
                if (document.getElementById('ctl00_cphTransaction_txtDepDate').value == "") {
                    document.getElementById('errorMsg').style.display = "block";
                    document.getElementById('errorMsg').innerHTML = "please enter Next Payment Date";
                    return false;
                }
            }
            if (!CalculateBalance()) {
                return false;
            }
            else
                return true;
            //            }
        }

        function ViewFallowUp() {
            document.getElementById('divFollowUp').style.display = 'block';
        }

        function FallowUpHide() {
            document.getElementById('divFollowUp').style.display = 'none';
        }

        function Download() {
            var path = document.getElementById('hdnDocPath').value;
            var docName = document.getElementById('hdnDocName').value;
            var open = window.open("DownloadeDoc.aspx?path=" + path + "&docName=" + docName);
            return false;
        }

        function ValidateDate() {
            var msg = true;
            var date = document.getElementById('<%=txtVisaDate.ClientID %>').value;
            if (date.length <= 0 || date == 'DD/MM/YYYY') {
                document.getElementById('errMessFed').style.display = 'block';
                document.getElementById('errMessFed').innerHTML = 'Enter Valid Visa Date';
                msg = false;
            }
            else {
                document.getElementById('errMessFed').style.display = "none";
                msg = true;
            }
            return msg;
        }
    </script>
    
    <div title="Visa Date FollowUp" id="divFollowUp" style="position:absolute; display:none; padding: 7px 14px 10px 14px; 
    
    left:200px;top:210px;  border:solid 2px #18407B; width:500px; background-color: white;"  >
        
        <table style="background-color:White" border="0">
        
        <div>
        
        
        <h2 style="color: #0099ff; font-family: Calibri; font-size: 22px">
                                        Visa Date History</h2>
        
                                   
                                   <a style=" position: absolute; right:10px; top:7px; color:#ccc;  font-weight:normal; 
                                       
                                       font-size:18px; text-decoration:none" href="#" onclick="FallowUpHide()">X</a>
        
        </div>
        
        
         <asp:DataList ID="dlFollowUp" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0" Width="500px" ForeColor="" ShowHeader="true"
                DataKeyField="Visa_id">
                <HeaderStyle  Font-Bold="true" ForeColor="White" HorizontalAlign="Left" />
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td width="150px" class="ns-h3">
                                Visa Date
                            </td>
                            <td width="140px" class="ns-h3">
                                Mail Sent
                            </td>
                             <td width="150px" class="ns-h3">
                                Visa Created On
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td class="bor-1 pad_10L" width="150px">
                                <%#Convert.ToDateTime(Eval("Visa_date")).ToString("dd-MMM-yyyy")%>
                            </td>
                            <td class="bor-1 pad_10L" width="140px" align="left">
                                <%#Eval("MailRequired")%>
                            </td>
                             <td  class="bor-1 pad_10L" width="150px" align="left">
                                <%#Convert.ToDateTime(Eval("Visa_created_on")).ToString("dd-MMM-yyyy")%>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
        </table>
    </div>
    
    <asp:HiddenField ID="hdnCardType" runat="server" />
  <asp:HiddenField ID="hdnPaidAmount" runat="server" Value="0" />
  <div class="ns-h3 mar-top-10"> 
        
        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                            
                           
                            <td width="60%"> Fixed Departure Header</td>
                            
                                <td width="20%" align="left">
                                     <asp:LinkButton  ID="lnkShowVoucher" runat="server" Text="» Fixed Departure Voucher" class="nounderline font-nor"
                                        OnClick="lnkShowVoucher_Click"></asp:LinkButton>
                                </td>
                                <td width="20%" align="center">
                                    <a  href="FixedDepartureQueue.aspx" class="nounderline font-nor">» Fixed Departure Queue</a>
                                </td>
                            </tr>
                        </table>
        
        </div>
       <%-- <div id="divImg" runat="server" style="display:none;">
        <asp:Image ID="imgLogo" runat="server" />
        </div>
  --%>
    <div id="Div1" runat="server">
        
       
       
        
        
        <div style=" height: 150px; padding:10px; color: #000000; background-color: #FFFFFF;
            border: solid 1px #ccc;">
            <asp:HiddenField ID="hdnBookingId" runat="server" />
            
            
            <table width="100%">
            
                <%if (ActHdr != null && ActHdr.Rows.Count > 0)
                  { %>
                <tr height="30px">
                    <td width="100px">
                        <label>
                            Fixed Departure Name:
                        </label>
                    </td>
                    <td>
                        <b>
                            <%= ActHdr.Rows[0]["activityName"]%>
                        </b>
                    </td>
                    <td width="50px">
                        <label>
                            TripId:
                        </label>
                    </td>
                    <td>
                        <b>
                            <%= ActHdr.Rows[0]["TripId"]%>
                        </b>
                    </td>
                    <td width="130px">
                        <label>
                            Transaction Date:
                        </label>
                    </td>
                    <td>
                        <b>
                            <%= ActHdr.Rows[0]["Booking"]%>
                        </b>
                    </td>
                </tr>
                <tr height="30px">
                    <td width="100px">
                        <label>
                            Booking Date:
                        </label>
                    </td>
                    <td>
                        <b>
                            <%= ActHdr.Rows[0]["TransactionDate"]%>
                        </b>
                    </td>
                    <td width="90px">
                        <label>
                            Agency Name:
                        </label>
                    </td>
                    <td>
                        <b>
                            <%= ActHdr.Rows[0]["agent_name"]%>
                        </b>
                    </td>
                    <td width="130px">
                        <label>
                            Total Price:
                        </label>
                    </td>
                    <td>
                        <b>
                            <%=agency.AgentCurrency%>
                            &nbsp;<%= Convert.ToDecimal(ActHdr.Rows[0]["TotalPrice"]).ToString("N"+agency.DecimalValue)%>
                        </b>
                    </td>
                </tr>
                <%--<tr height="30px">
    <td  width="100px"><label>Order Id: </label></td>
    <td><b><%= ActHdr.Rows[0]["OrderId"]%> </b> </td>
    
    <td width="130px"><label>TransactionT ype: </label></td>
    <td><b><%= ActHdr.Rows[0]["TransactionType"]%> </b> </td>
    </tr>--%>
                <tr height="30px">
                    <td width="120px">
                        <label>
                            No Of Passenger(s):
                        </label>
                    </td>
                    <td>
                        <b>
                            <%= (Convert.ToInt32(ActHdr.Rows[0]["Adult"]) + Convert.ToInt32(ActHdr.Rows[0]["Child"]) + Convert.ToInt32(ActHdr.Rows[0]["Infant"]))%>
                        </b>
                    </td>
                     
                </tr>
                <tr height="50px">
                  <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.MemberType == CT.TicketReceipt.BusinessLayer.MemberType.ADMIN && ActHdr.Rows[0]["PaymentStatus"].ToString() == "0")
                    { %>
                    <td width="120px">
                        <label>
                            Document upload:
                        </label>
                    </td>
                    
                         
    <td>
    <CT:DocumentManager ID="DocumentImage" runat="server" DefaultImageUrl="~/images/common/no_preview.png"
                    DocumentImageUrl="~/images/common/Preview.png"   ShowActualImage="false"  SessionName="crenterlic"
                    DefaultToolTip=""  />
        </td>
         <td>
         <asp:Label ID="lblMessage" runat="server"></asp:Label>
         </td>
           
         <td>
          <asp:Button ID="btnAdd" Text="Update" runat="server"  CssClass="button"  Height="22px" OnClick ="btnAdd_Click" ></asp:Button>
         </td>
        
                     <%} %>
                </tr>
                <tr>
                 <%if (Convert.ToString(ActHdr.Rows[0]["Img_Filename"]) != string.Empty && ActHdr.Rows[0]["PaymentStatus"].ToString() == "0")
           {%>
         <td>
            <input type="hidden" id="hdnDocName" name="DocName" value="<%=ActHdr.Rows[0]["Img_Filename"].ToString() %>" />
            <input type="hidden" id="hdnDocPath" name="DocPath" value="<%=ActHdr.Rows[0]["Img_FilePath"].ToString() %>" />
        <asp:LinkButton ID="lnkDownLoad" runat="Server" OnClientClick="return Download();" Text="View/Download"></asp:LinkButton>           
         </td>
         <%} %>
                </tr>
                
                
                <%} %>
            </table>
            
            
        </div>
       
       
        <div class="mar-top-10">
            <asp:DataList ID="dlPaxDetails" runat="server" Width="100%">
                <HeaderTemplate>
                    <table class="grid-tble" width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr height="30px">
                            <td width="05%" class="ns-h3">
                                S#
                            </td>
                            <td width="20%" class="ns-h3">
                                Full Name
                            </td>
                            <td width="25%" class="ns-h3">
                                Email
                            </td>
                            <td width="20%" class="ns-h3">
                                Phone No
                            </td>
                            <td width="20%" class="ns-h3">
                                Nationality
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table class="tblpax" width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr height="30px">
                            <td width="5%">
                                <%#Eval("PaxSerial")%>
                            </td>
                            <td width="20%">
                                <%#Eval("FirstName")%>&nbsp;<%# Eval("LastName")%>
                            </td>
                            <td width="25%">
                                <%#Eval("Email")%>
                            </td>
                            <td width="20%">
                                <%#Eval("PhoneCountryCode")%>&nbsp;<%# Eval("Phone")%>
                            </td>
                            <td width="20%">
                                <%#Eval("Nationality")%>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
                    
        </div>
        
        
        <br />
        <asp:GridView ID="gvFDPayments" runat="server" Width="100%" BorderWidth="0" 
                AutoGenerateColumns="False" CssClass="tblpax" Caption="Fixed Departure Payment Details" >
                <Columns>
                
                    <asp:BoundField DataField="PaymentMode" HeaderText="Mode">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px" />
                        <ItemStyle Width="150px" BorderWidth="0" />
                    </asp:BoundField>
                   
                    <%--<asp:BoundField DataField="TotalPrice" HeaderText="Total Amount">
                        <HeaderStyle Font-Bold="True" CssClass="heading" Height="30px"/>
                        <ItemStyle Width="100px" />
                    </asp:BoundField>--%>
                    <asp:BoundField DataField="Amount" HeaderText="Amount">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0" HorizontalAlign="Right"/>
                    </asp:BoundField>
                     <asp:BoundField DataField="PromotionAmount" HeaderText="Discount">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0" HorizontalAlign="Right"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="BalanceAmount" HeaderText="Bal.Amount">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0"  HorizontalAlign="Right"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="CCCharge" HeaderText="CC Charge">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0"   HorizontalAlign="Right"/>
                    </asp:BoundField>
                     <asp:BoundField DataField="PaymentType" HeaderText="Pay.Type">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0" />
                    </asp:BoundField>
                    <asp:BoundField DataField="NextPaymentDate" HeaderText="Next Pay.Date" DataFormatString="{0:dd/MM/yyyy}">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="150px" BorderWidth="0" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Currency" HeaderText="Currency">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ExchangeRate" HeaderText="Ex.Rate ">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="100px" BorderWidth="0" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CreditCardType" HeaderText="CC Type">
                        <HeaderStyle Font-Bold="True" CssClass="ns-h3" Height="30px"/>
                        <ItemStyle Width="200px" BorderWidth="0" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <table style=" border:none" class="tblpax" >
            <tr>
            <td style="width:100px; font-weight:bold" align="right" class="">
                <asp:Label ID="Label1" runat="server" Text="Total :"></asp:Label>
            </td>
           
            <td style="width:50px" align="right" >
                <asp:Label ID="lblTotalPayable" runat="server" Text=""></asp:Label>
            </td>
             <td style="width:50px" align="right" >
                <asp:Label ID="lblDiscount" runat="server" Text=""></asp:Label>
            </td>
            <td style="width:80px" colspan="2" align="right">
                <asp:Label ID="lblBalancePayable" runat="server" Text=""></asp:Label>
            </td>
            <td style="width:75px"  align="right">
                <asp:Label ID="lblCCCharge" runat="server" Text=""></asp:Label>
            </td>
            </tr>
            </table>    
        
        <%if (total - paidAmount > 0)  //&& CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.MemberType == CT.TicketReceipt.BusinessLayer.MemberType.ADMIN
          { %>
     
     
     <div style="margin-top: 10px;" class="ns-h3">  Payment</div>
     
        <div id="PartialPayment" style="border: solid 1px #ccc; padding:10px; height:250px">
            <div id="errorMsg" style="display:none;color:Red">
                </div>
            <table style=" line-height:23px">
                
                <tr>
                    <td>
                        Settlement Mode :
                    </td>
                    <td width="70%">
                        <asp:DropDownList Width="200px" CssClass="inputDdlEnabled" ID="ddlSettlementMode" runat="server" onchange="ShowHideCreditCard()">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="tblCreditCard" style="display: none">
                            <tr>
                                <td>
                                    Credit Card Type :
                                </td>
                                <td style="width: 70%">
                                    <asp:DropDownList Width="200px" CssClass="inputDdlEnabled" ID="ddlCreditCard" runat="server" onchange="ApplyCreditCardCharge()">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Card Charge :
                                </td>
                                <td style="width: 70%">
                                    <asp:Label CssClass="inputDdlEnabled" ID="lblCharge" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        Total Amount :
                    </td>
                    <td>
                        <asp:Label CssClass="inputDdlEnabled" ID="lblTotalAmount" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                
                <tr>
                <td colspan="2">
                    <table id="tblCash" style="display: block; width: 100%">
                        <tr>
                            <td>
                                Cash :
                            </td>
                            <td style="width: 55%">
                                <asp:TextBox  CssClass="inputDdlEnabled" ID="txtCash" runat="server" onkeypress="return isNumber(event)" onkeyup="CalculateBalance()"
                                    onblur="CalculateBalance()" Text="0"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="tblCredit" style="display: none; width: 100%">
                            <tr>
                                <td>
                                    Credit :
                                </td>
                                <td style="width: 55%">
                                    <asp:TextBox CssClass="inputDdlEnabled" ID="txtCredit" runat="server" Text="0" onkeypress="return isNumber(event)" onkeyup="CalculateBalance()" onblur="CalculateBalance()"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="tblCard" style="display: none; width: 100%">
                            <tr>
                                <td>
                                    Card :
                                </td>
                                <td style="width: 55%">
                                    <asp:TextBox  CssClass="inputDdlEnabled" ID="txtCard" runat="server" Text="0" onkeypress="return isNumber(event)" onkeyup="CalculateBalance()" onblur="CalculateBalance()"></asp:TextBox>
                                    <br />Card Charge: <span id="CardAmount"></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        Balance Amount :
                    </td>
                    <td>
                        <asp:Label ID="lblBalance" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="tblNextPayment" style="display: none; ">
                            <tr>
                                <td>
                                   NextPayment Date :
                                </td>
                                <td align="left">
                                    <div style=" float:left; text-align:left">  <asp:TextBox ID="txtDepDate" runat="server" CssClass="inp_00" Text=""></asp:TextBox></div>
                                </td>
                                <td align="left" valign="bottom">
                                    <a href="javascript:void(null)" onclick="showCalendar1()" />
                                    <img src="images/call-cozmo.png"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="30" colspan="2">
                        <asp:Button CssClass="button" ID="btnPayment" runat="server" Text="Pay Now" OnClick="btnPayment_Click" OnClientClick="return Validate();" />
                    </td>
                </tr>
            </table>
        </div>
        <%} %>
        <div>
         
         <div style="margin-top: 10px;" class="ns-h3">Visa Date FallowUp</div>
         <div style="border: solid 1px #ccc; padding:10px; height:50px">
         <table>
         <tr>
         <td>
          <div class="error_msg" style="display:none;" id="errMessFed"> </div>
         </td>
         </tr>
         <tr>
         
                                <td>
                                   Visa Date :
                                </td>
                                <td align="left">
                                    <div style=" float:left; text-align:left">  <asp:TextBox ID="txtVisaDate" runat="server" CssClass="inp_00" Text=""></asp:TextBox></div>
                                </td>
                                <td align="left" valign="bottom">
                                    <a href="javascript:void(null)" onclick="showCalendar2()" />
                                    <img src="images/call-cozmo.png"></a>
                                </td>
                                <td>
                                <asp:CheckBox ID="chkmail" runat="server" Text="(Please check the box to send mail)" Font-Bold="true" />
                                </td>
                            </tr>
                            <tr>
                             <td height="30" colspan="2">
                             <asp:Button CssClass="button" ID="btnUpdate" runat="server" Text="Update" OnClick="Update_Click" OnClientClick="return ValidateDate();" />
                    </td>
                    <td>
                     <a href="#" onclick="javascript:ViewFallowUp(this.id);" style="color:Blue;">View Visa Date History </a>
                    </td>
                            </tr>
         </table>
         </div>
    </div>
    </div>
     <div id="Outcontainer1" style="position:absolute;display: none; left:220px; top:354px;  border:solid 0px #ccc;  z-index:200;  " >

    <a onclick="document.getElementById('Outcontainer1').style.display = 'none'">
        <img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />

</a>

<div id="container1" style="border:1px solid #ccc;">
</div>

</div>
     <div id="Outcontainer2" style="position:absolute;display: none; left:220px; top:354px;  border:solid 0px #ccc;  z-index:200;  " >

    <a onclick="document.getElementById('Outcontainer2').style.display = 'none'">
        <img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />

</a>

<div id="container2" style="border:1px solid #ccc;">
</div>

</div>
<div id="printableArea" runat="server" style="display:none;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                    <tbody>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="21%">
                                                <%--<img width="200" height="71" src="<%=Request.Url.Scheme %>://cozmotravel.com/images/packageVoucherlogo.gif">--%>
                                                <asp:Image ID="imgHeaderLogo" runat="server" Width="200px" Height="60px" AlternateText="AgentLogo" ImageUrl="images/header_cozmovisa.jpg" />
                                            </td>
                                            <td width="79%" align="right">
                                                <div style="float: right;">
                                                    <a href="#" onclick="printDiv('<%=printableArea.ClientID %>')">Print &nbsp;
                                                        <img align="absmiddle" style="padding-left: 2;" src="<%=Request.Url.Scheme %>://cozmotravel.com/Images/print_blue.gif" title="Print"
                                                            alt="print"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="25" bgcolor="#18407B">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Voucher</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10">
                                    <tbody>
                                        <tr>
                                            <td height="25">
                                                <strong>Date of Issue:</strong>
                                                <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Passenger(s): </strong>
                                                <asp:Label ID="lblPassengers" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Booking Ref:</strong>
                                                <asp:Label ID="lblBookingRef" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Lead Passenger: </strong>
                                                <asp:Label ID="lblLeadPassenger" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="25" bgcolor="#18407B">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Guest Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Guest Name :</strong> 
                                                <asp:Label ID="lblGuestName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td width="50%">
                                                <strong>Tour Name :</strong> 
                                                <asp:Label ID="lblActivityName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="top">
                                                <strong>Nationality : </strong>
                                                <asp:Label ID="lblNationality" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Date :</strong> 
                                                <asp:Label ID="lblTransactionDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td height="20" valign="top">
                                                <strong>No of Adults </strong>: 
                                                <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <%--<strong>Duration :</strong> 
                                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label> &nbsp;hours
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <%--<td height="20" valign="top">
                                                <strong>No of Children</strong> : 
                                                <asp:Label ID="lblChildCount" runat="server" Text=""></asp:Label>
                                            </td>--%>
                                           <%-- <td style="display:none;">
                                                <strong>Car/Bus Type :</strong> eded
                                                 <strong>Entrance Fees :</strong> AED 
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>--%>
                                              
                                        </tr>
                                        <tr style="display:none;">
                                            <td height="20" valign="top">
                                                <strong>Hotel :</strong> Al Wahrami
                                            </td>
                                            <td>
                                                <strong>Guide Language :</strong> Arabic
                                            </td>
                                        </tr>
                                        <tr>
                                             <%-- <td height="20" valign="top">
                                                <strong>No of Infants</strong> : 
                                                <asp:Label ID="lblInfantCount" runat="server" Text=""></asp:Label>
                                            </td>--%>
                                            <td>
                                                <strong>Pick up point :</strong> 
                                                <asp:Label ID="lblPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td style="height: 20px">
                                                <strong>Entrance Fees :</strong> 
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                        <td height="20" valign="top">
                                                <strong>Country :</strong> 
                                                <asp:Label ID="lblCountry" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Pick up Time :</strong> 
                                                <asp:Label ID="lblPickupTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                         <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Drop Off point :</strong> 
                                                <asp:Label ID="lblDropPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td height="20" valign="top">
                                                <strong>Address :</strong> 
                                                <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td valign="top" style="height: 20px">
                                                <strong>Tel number :</strong> 
                                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                                            </td>
                                          
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div style="line-height: 25px; display:none;" >
                                                    <strong><span style="background: #FFFF00">Emergency Contact Details:</span></strong>
                                                    999999999</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="97%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td height="25" bgcolor="#03a7e6">
                                                        <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                                            Booked and Payable by
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="padd10" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="height: 20px">
                                                                    <strong>Cozmo Travel LLC </strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="97%" border="0" align="right" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td height="25" bgcolor="#03a7e6">
                                                        <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                                            Supplier / Driver contact if
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="padd10" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20">
                                                                    <strong>Contact no :</strong> 9312678087
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="25" bgcolor="#18407B">
                                <label style="padding-left: 20px; color: #FFFFFF; font-size: 16px;">
                                    Terms &amp; Conditions</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="40" bgcolor="" align="center">
                                <div style="border: solid 1px #ccc; padding: 10px; text-align: center">
                                    Cozmo Holidays Tel. No. : <br>
                                    Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:ziyad@cozmotravel.com">
                                        info@cozmotravel.com</a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
