﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintFleetVoucher" Codebehind="PrintFleetVoucher.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <title>Voucher</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />


 

</head>
<body>
    <form id="form1" runat="server">
    <script>
        function printPage() {
            document.getElementById('btnPrint').style.display = "none";
            document.getElementById('btnEmail').style.display = "none";
            window.print();
            setTimeout('showButtons()', 1000);
            return false;
        }
        function showButtons() {
            document.getElementById('btnPrint').style.display = "block";
            document.getElementById('btnEmail').style.display = "block";
        }
        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function ShowPopUp(id) {
            document.getElementById('txtEmailId').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
            return false;
        }
        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }
        
    </script>
   <div class="col-md-12">
   <div id="emailBlock" class="showmsg" style="position:absolute;display:none; left:400px;top:200px;width:300px;">
          <div class="showMsgHeading"> Enter Your Email Address</div>
          
          <a style=" position:absolute; right:5px; top:3px;" onclick="return HidePopUp();" href="#" class="closex"> X</a>
           <div class="padding-5"> 
           <div style=" background:#fff">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
                           <tr>
                           <td height="40" align="center">
                            <b style="display: none; color:Red" id="err"></b>
                           <asp:TextBox style=" border: solid 1px #ccc; width:90%; padding:2px;"  ID="txtEmailId" runat="server" CausesValidation="True" ></asp:TextBox>
                           </td>
                           </tr>
                           <tr>
                           
                           <td height="40" align="center">
                           <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                           </td>
                       </tr>
                   </table>
            </div>
           
           
           
             </div>
           
           
               
           </div>
    <div id="printableArea" runat="server" style="border-style: solid;
        border-width: 1px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <tr>
                            <td>
                            
                            
                            
    <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"> <asp:Image ID="imgHeaderLogo" runat="server" Width="162"  Height="51px" AlternateText="AgentLogo" ImageUrl="" /></div>
    <div class="col-md-8"> 
    
    
   <label class="pull-right mar10">  <asp:Button ID="btnPrint" runat="server" Text="Print Voucher" OnClientClick="return printPage();" /></label>
    
      <label class="pull-right mar10">  <asp:Button ID="btnEmail" runat="server" Text="Email Voucher" OnClientClick="return ShowPopUp(this.id);"/></label>
    
    
    </div>




    <div class="clearfix"></div>
    </div>
    
    
    
    
                            
                            
                                
                            </td>
                          
                        </tr>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                            border: solid 1px; border-collapse: collapse">
                            <tbody>
                                <tr>
                                    <td style="background: #1c498a; height: 24px; line-height: 22px; font-size: 13px;
                                        color: #fff; padding-left: 10px; font-weight: bold;" colspan="2">
                                        <label>
                                            Voucher Details</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Fleet Name: </strong>
                                        <asp:Label ID="lblFleetName" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <strong>Date of Issue:</strong>
                                        <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="25">
                                        <strong>Booking Ref:</strong>
                                        <asp:Label ID="lblBookingRef" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="background: #1c498a; height: 24px; line-height: 22px; font-size: 13px;
                        color: #fff; padding-left: 10px; font-weight: bold;">
                        <label>
                            Guest Details</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                            border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                            <tbody>
                                <tr>
                                    <td width="50%" height="20" valign="top">
                                        <strong>Title :</strong>
                                        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td width="50%">
                                        <strong>First Name :</strong>
                                        <asp:Label ID="lblFirstName" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20" valign="top">
                                        <strong>Last Name :</strong>
                                        <asp:Label ID="lblLastName" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <strong>Email :</strong>
                                        <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                 
                              
                                <tr>
                                    <td height="20" valign="top">
                                        <strong>Pickup Date </strong>:
                                        <asp:Label ID="lblFromDate" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <strong>Drop Date :</strong>
                                        <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20" valign="top">
                                        <strong>Pickup Location </strong>:
                                        <asp:Label ID="lblPickupLocation" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <strong>
                                            <asp:Label ID="lblDropupLocation" runat="server" Text="Drop Location" Visible="false"></asp:Label></strong>
                                        <asp:Label ID="lblDropupLocationValue" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                  <tr>
                                    <td height="20" valign="top">
                                        <strong>Mobile No1 :</strong>
                                        <asp:Label ID="lblMobileNo" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                                <strong>   <asp:Label ID="lblMobileNo2" runat="server" Text="Mobile No2:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblMobileNo2Value" runat="server" Text=""></asp:Label>
                                                </td>
                                </tr>
                                <tr>
                                                <td height="20" valign="top">
                                                  <strong><asp:Label ID="lblPickupAddress" runat="server" Text="Pickup Address:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblPickupAddressValue" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                   <strong><asp:Label ID="lblLandmark" runat="server" Text="Landmark:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblLandmarkValue" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                            </tbody>
        </table>
        </td> </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="background: #1c498a; height: 24px; line-height: 22px; font-size: 13px;
                        color: #fff; padding-left: 10px; font-weight: bold;">
                        <label>
                            Payment Details</label>
                    </td>
                </tr>
                <tr>
                <td>
                <%if(itinerary != null && itinerary.Price != null)
                  { %>
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                           <%-- <tr>
                               
                            <td>
                                <b>Name</b>
                            </td>
                               
                                <td>
                                    <b>Total</b>
                                </td>
                            </tr>--%>
                             <% decimal gtotal = 0, pageMarkup = 0;%>
                            <tr>
                            
                                <td>
                                   Car Rental Charges
                                </td>
                                <td>
                               
                                <%pageMarkup = itinerary.Price.AsvAmount; %>
                                    
                                    <%=Math.Round(itinerary.Price.NetFare + itinerary.Price.AsvAmount - itinerary.Price.Discount, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                                
                               
                            </tr>
                            <tr>
                               
                            <td>
                                VRF
                            </td>
                               
                                <td>
                                    <%=Math.Round(Convert.ToDecimal(0)).ToString("N"+Settings.LoginInfo.DecimalValue) %>
                                </td>
                            </tr>
                            <%if (itinerary.CDW > -1)
                              { %>
                            <tr>
                            
                                <td>
                                  CDW
                                </td>
                                <td>
                               
                                    <%=Math.Round(itinerary.CDW, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                                
                               
                            </tr>
                            
                            <%} %>
                             <%if (itinerary.SCDW > -1)
                               { %>
                            <tr>
                                <td>
                                   SCDW
                                </td>
                                <td>
                                    
                                    <%=Math.Round(itinerary.SCDW, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                            <%if (itinerary.PAI > -1)
                              { %>
                            <tr>
                                <td>
                                   PAI
                                </td>
                                <td>
                               
                                    <%=Math.Round(itinerary.PAI, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.CSEAT > -1)
                               { %>
                            <tr>
                                <td>
                                   CSEAT
                                </td>
                                <td>
                               
                                    <%=Math.Round(itinerary.CSEAT, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.VMD > -1)
                               { %>
                            <tr>
                                <td>
                                   VMD
                                </td>
                                <td>
                               
                                    
                                    
                                    <%=Math.Round(itinerary.VMD, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.GPS > -1)
                               { %>
                            <tr>
                                <td>
                                   GPS
                                </td>
                                <td>
                               
                                    
                                    
                                    <%=Math.Round(itinerary.GPS, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.CHAUFFER > -1)
                               { %>
                            <tr>
                                <td>
                                   CHAUFFER
                                </td>
                                <td>
                               
                                    
                                    
                                    <%=Math.Round(itinerary.CHAUFFER, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                           <%if (itinerary.Emirates_Link_Charge > -1)
                             { %>
                            <tr>
                                <td>
                                   Link Charges :
                                </td>
                                <td>
                               
                                    
                                    
                                    <%=Math.Round(itinerary.Emirates_Link_Charge, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                             <%if (itinerary.AirportCharge > -1)
                               { %>
                            <tr>
                                <td>
                                  Airport Charge
                                </td>
                                <td>
                               
                                    
                                    
                                    <%=Math.Round(itinerary.AirportCharge, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>
                                    
                                </td>
                            </tr>
                            <%} %>
                           <% gtotal += Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.AsvAmount - itinerary.Price.Discount + itinerary.Price.Tax); %>
                            
                            <%if (pageMarkup > 0)
                              { %>
                            
                         <!--   <tr>
                                <td>
                                    <b>Addl Markup </b>
                                </td>
                                <td>
                                    <b class="spnred"> 
                                        <%=Math.Round(pageMarkup, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%></b>
                                </td>
                            </tr> -->
                            <%} %>
                              <tr>
                                <td>
                                    <b>Grand Total </b>
                                </td>
                                <td>
                                    <b class="spnred"><%=itinerary.Price.Currency%>
                                        <%=(Math.Ceiling(gtotal)).ToString("N" + itinerary.Price.DecimalPoint)%></b>
                                </td>
                            </tr>
                        </table>
                 <%} %>
                </td>
                </tr>
                 <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                   <tr>
                                <td>
                                  <strong>Cancellation Policy:</strong> 
                                </td>
                            </tr>
                             <tr>
                               <td style='padding: 5px;'>
                        <div style='padding: 0px 10px 0px 10px'>
                                   <li>No charges for cancellation of booking within 24
                                       hours</li>
                                   <li>10% of total bill will be charged for booking cancellation
                                       fee if the cancellation is done less than 4 hours from the pickup time/delivery
                                       of vehicle.</li>
                                   <li>2 hours grace time given to pick the vehicle after
                                       the booking time. Exceptions are acceptable on a case to case basis and upon approval
                                       of Management.</li><br />
                              </div> </td>
                           </tr>
                <tr>
                <tr>
                    <td height="40" bgcolor="" align="center">
                        <div style="border: solid 1px; padding: 10px; text-align: center">
                            Tel. No. :+971 600522200
                            <br />
                            Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:info@sayararental.com">
                                info@sayararental.com</a>
                        </div>
                    </td>
                </tr>
                </tr>
                <tr>
                    <td>
                        <center>
                            <div>
                                Booked and payable by Sayara services.</div>
                        </center>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </form>
</body>
</html>
