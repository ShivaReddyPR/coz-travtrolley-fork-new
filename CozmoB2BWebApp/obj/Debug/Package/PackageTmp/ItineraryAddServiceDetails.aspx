﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ItineraryAddServiceDetailsGUI" Codebehind="ItineraryAddServiceDetails.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Itinerary Add Service Details</title>
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <link href="css/override.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/Default.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/Jquery/jquery-1.4.2.min.js"></script>
    <script src="Scripts/Common/Common.js" type="text/javascript"></script>    

    <%--Yahoo Calendar Start--%>
    <link href="yui/build/fonts/fonts-min.css" rel="stylesheet" type="text/css" />
    <link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />  
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <%--Yahoo Calendar End--%>
    <style>
		.but_b, .a.but_b {
			background: #1c498a;
			color: #fff;
			font-weight: bolder;
			line-height: 36px;
			display: inline-block;
			padding: 0px 10px 0px 10px;
		}
	</style>
</head>
<body style="background-color: #f7f7f7;">
    <form id="form1" runat="server">

        <script type="text/javascript">

            //$(document).ready(function () {
            //    controlsEnabling();
            //});

            //function controlsEnabling() {
            //    debugger;
            //    var currentLocation = document.referrer;
            //    if (currentLocation.includes("ItineraryAdditionalServiceDetailsIframe.aspx")) {
            //        $("#txtPaxName").removeAttr('disabled');
            //        $("#txtFlightNumbers").removeAttr('disabled');
            //        $("#txtTravelDate").removeAttr('disabled');
            //        $("#txtRouting").removeAttr('disabled');
            //        $("#txtAmount").removeAttr('disabled');
            //        $("#txtPNR").removeAttr('disabled');
            //        return false;
            //    }
            //    return true;
            //}

            function BalanceNotification() {
                alert("Insufficient Balance");
            }
            function Notification() {
                // alert("Saved successfully");
                window.parent.$('#ISerDetDiv').modal('hide');
                //window.parent.document.getElementById('ISerDetDiv').style.display = "none";

            }

            function DuplicateNotification() {
                alert("An item already exists");
                //  window.parent.$('#ISerDetDiv').modal('hide');

            }

            //function reloadPage() {
            //    document.getElementById('ddlService').value = '0';
            //    document.getElementById('txtRemarks').value = '';   

            //    var PNRDisable_Check = document.getElementById('txtPNR');
            //    if (PNRDisable_Check.disabled) {
            //        //alert("disabled");
            //    } else {
            //        document.getElementById('ddlNationality').value = '0';
            //        document.getElementById('txtMobileNo').value = '';
            //        document.getElementById('txtTravelDate').value = '';
            //        document.getElementById('txtRouting').value = '';
            //        document.getElementById('txtPaxName').value = '';
            //        document.getElementById('txtPassportNumber').value = '';
            //        document.getElementById('txtFlightNumbers').value = '';
            //        document.getElementById('txtAmount').value = '';
            //        document.getElementById('txtPNR').value = '';
            //    }
            //}

            //This function validates the user input which accepts only +ve integers with or without decimals.
            //This function will be invoked when the user enters the Adult ,Child,Infant Fare Details.
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            //This function will validate the user input and allows the user to enter only alpha numeric             characters.
            //This function will be invoked when the user enters the PNR's.
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right
            function IsAlphaNumeric(e) {
                var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));

                return ret;
            }



            function Save() {

                if (document.getElementById('ddlService').selectedIndex <= 0) addMessage('Please select Service from the list!', '');


                if (document.getElementById('txtPaxName').value == '') addMessage('Passenger Name cannot be blank!', '');


                if (document.getElementById('ddlNationality').selectedIndex <= 0) addMessage('Please select Nationality from the list!', '');


                if (document.getElementById('txtPassportNumber').value == '') addMessage('Passport number cannot be blank!', '');

                if (document.getElementById('txtMobileNo').value == '') addMessage('Mobile number cannot be blank!', '');

                if (document.getElementById('txtFlightNumbers').value == '') addMessage('Flight numbers cannot be blank!', '');
                if (document.getElementById('txtTravelDate').value == '') addMessage('Travel Date cannot be blank!', '');

                if (document.getElementById('txtRouting').value == '') addMessage('Routing cannot be blank!', '');
                // if (document.getElementById('txtRemarks').value == '') addMessage('Remarks cannot be blank!', '');
                if (document.getElementById('txtAmount').value == '') addMessage('Amount cannot be blank!', '');
                if (document.getElementById('txtPNR').value == '') addMessage('PNR cannot be blank!', '');

                var nationality = document.getElementById("<%=hdnNat.ClientID%>").value;
                //console.log(nationality);
                if (nationality.length > 0 &&
                document.getElementById('ddlNationality').selectedIndex > 0
                && document.getElementById('ddlNationality').selectedIndex > 0) {
                    var selNat = document.getElementById("<%=ddlNationality.ClientID%>").value;
                    console.log(selNat);
                    console.log(nationality.indexOf(selNat));

                    if (nationality.indexOf(selNat) == -1) {
                        addMessage('Selected Service is not avaialble for the selected nationality!', '');
                    }
                }


                if (getMessage() != '') {

                    alert(getMessage()); clearMessage();
                    return false;
                }
            }
            var cal1;

            function init() {               
                //    showReturn();
                var today = new Date();
                // For making dual Calendar use CalendarGroup  for single Month use Calendar     
                cal1 = new YAHOO.widget.Calendar("cal1", "container1");
                //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                cal1.cfg.setProperty("title", "Select Travel date");
                cal1.cfg.setProperty("close", true);
                cal1.selectEvent.subscribe(setDates1);
                cal1.render();
            }
            function showCal1() {
                $('container1').context.styleSheets[0].display = "block";
                init();
                cal1.show();
            }

            function setDates1() {
                var date1 = cal1.getSelectedDates()[0];
                var month = date1.getMonth() + 1;
                var day = date1.getDate();
                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                document.getElementById('<%= txtTravelDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
                cal1.hide();

            }
            function isText(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                    return false;
                }
                return true;
            }
        </script>

        <%--<div style="text-align: center">
                <h4>Service Details</h4>
            </div>--%>

        <div class="clear" style="margin-left: 25px">
            <div id="container1" style="position: absolute; top: 90px; left: 260px; display: none; z-index: 9999">
            </div>
        </div>
        <div>
            <asp:HiddenField runat="server" ID="hdnNat" />
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="body_container">
                        <div class=" paramcon row" title="header" style="margin-top: 15px">
                            <div class="col-xs-12 col-sm-6 col-md-12 padding-0  marbot_10" style="margin-bottom: 10px">
                                <div class="col-md-2 padding-0 marbot_10">
                                    Service Name<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:DropDownList AutoPostBack="true" CssClass="form-control" runat="server" ID="ddlService"
                                        OnSelectedIndexChanged="ddlService_Change">
                                    </asp:DropDownList><%--onchange="controlsEnabling()"--%>
                                </div>                              
                                <div class="col-md-2 col-md-2 padding-0 marbot_10">
                                    Passenger Name<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox Enabled="false" CssClass="form-control" ID="txtPaxName" runat="server"
                                        MaxLength="50" onkeypress="return isText(event);"></asp:TextBox>
                                </div>
								<div class="col-md-2 padding-0 marbot_10">
                                    Nationality<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlNationality">
                                    </asp:DropDownList>
                                </div>
                            </div>                           
                            <%--<div class="clearfix"></div>--%>
                            <div class="col-xs-12 col-sm-6 col-md-12 padding-0  marbot_10" style="margin-bottom: 10px">
                                <div class="col-md-2 padding-0 marbot_10">
                                    Passport Number<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox CssClass="form-control" ID="txtPassportNumber" runat="server" MaxLength="50" onkeypress="return IsAlphaNumeric(event);"></asp:TextBox>
                                </div>
                                 <div class="col-md-2 padding-0 marbot_10">
                                    Mobile Number<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox CssClass="form-control" ID="txtMobileNo" runat="server" MaxLength="50" onkeypress="return isNumber(event);" ></asp:TextBox>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    Flight Number<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox Enabled="false" CssClass="form-control" ID="txtFlightNumbers" runat="server"
                                        MaxLength="50" onkeypress="return IsAlphaNumeric(event);"></asp:TextBox>
                                </div>
                            </div>
                       
                            <div class="col-xs-12 col-sm-6 col-md-12 padding-0  marbot_10">
                                <div class="col-md-2 padding-0 marbot_10">
                                    Travel Date<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox Enabled="false" CssClass="form-control" ID="txtTravelDate" runat="server"></asp:TextBox>
                                            </td>
                                            <%if (txtTravelDate.Enabled == true)
                                                {%>
                                            <td>
                                                <a id="anc_dateLink1" runat="server" href="javascript:void(0)" onclick="showCal1()">
                                                    <img id="dateLink1" runat="server" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>

                                            </td>
                                            <%} %>
                                            
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    Routing<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox Enabled="false" CssClass="form-control" ID="txtRouting" runat="server"
                                        MaxLength="50"></asp:TextBox>
                                </div>
                          
                                    <div class="col-md-2 padding-0 marbot_10">
                                    Amount<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox Enabled="false" onkeypress="return isNumber(event);" CssClass="form-control"
                                        ID="txtAmount" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                          

                            <div class="col-xs-12 col-sm-6 col-md-12 padding-0  marbot_10">
                                 <div class="col-md-2 padding-0 marbot_10">
                                    PNR<span class="fcol_red">*</span>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox Enabled="false" CssClass="form-control"
                                        ID="txtPNR" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    Remarks<%--<span class="fcol_red">*</span>--%></div>
                                <div class="col-md-2 padding-0 marbot_10">
                                    <asp:TextBox TextMode="MultiLine" CssClass="form-control" ID="txtRemarks" runat="server"
                                        MaxLength="50"></asp:TextBox>
                                </div>
                              
                            </div>
                              <div class="clearfix"></div>
                            <center>
                            	<asp:Button ID="btnSave" CssClass="but but_b" Text="Save" runat="server" OnClientClick="return Save();"
                                        OnClick="btnSave_Click" Style="display: inline;margin-top:30px" />&nbsp;&nbsp;
                                <asp:Button ID="BtnClear" CssClass="but but_b" Text="Clear" runat="server" OnClick="BtnClear_Click" />
                   <%-- <input type="button" class="but but_b" value="Clear" onclick="reloadPage();" />--%>
                            </center>

                        
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlService" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>

