﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewSwitcher.aspx.cs" Inherits="CozmoB2BWebApp.ViewSwitcher" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Awaiting Booking Confirmation</title>
    
</head>
<body>
    <form id="form1" runat="server">
        
    </form>
    <div id="PreLoader">
            <div style="top: 230px;border-radius: 16px; text-align:center; width:60%;padding:10px; margin:auto; background:#FFFFFF; line-height:30px; border: solid 1px #ccc;">
                <div>
                    <img src="images/preloader11.gif" alt="spinner" />
                </div>
                <div style="font-size: 16px; color: #999999">

                    <strong>Awaiting confirmation</strong>

                </div>
                <div style="font-size: 21px; color: #3060a0">

                    <strong style="font-size: 14px; color: black">do not click the refresh or back button as the transaction may be interrupted or terminated.</strong>

                </div>
            </div>
        </div>
    <script>
        function Redirect() {          
        
            window.location.href = '<%=url%>';
        }
        window.onload = Redirect();
    </script>
</body>
</html>
