<%@ Page Language="C#" AutoEventWireup="true" Inherits="PaymentProcessingForHotel" Codebehind="PaymentProcessingForHotel.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>"://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Untitled Page</title>
</head>
<body>

<%if (paymentGateway == "CCAvenue")
{%>
    <form id="nonseamless" action="<%=paymentGatewaySite %>" method="post" name="redirect" >   
    <%
        string ccaRequest;
        string strEncRequest;
        CCA.Util.CCACrypto ccaCrypto = new CCA.Util.CCACrypto();
        string workingKey;
        workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
       string rootFolder = ConfigurationManager.AppSettings["RootFolder"].ToString();
       string redirectUrl = string.Empty;
       if (string.IsNullOrEmpty(rootFolder))
       {
           redirectUrl =Request.Url.Scheme+ "://" + siteName + "/HTLBookingConfirm";
       }
       else
       {
           redirectUrl = Request.Url.Scheme+"://" + siteName + "/" + ConfigurationManager.AppSettings["RootFolder"].ToString() + "/HTLBookingConfirm";
       }
        
        string strAccessCode = ConfigurationManager.AppSettings["CCA_Access_Code"];
        string preCurrency = string.Empty;
        if (ConfigurationManager.AppSettings["TestMode"].ToString() == "True")
        {
            preCurrency = "USD";
        }
        else if (agency != null && !string.IsNullOrEmpty(agency.AgentCurrency))
        {
            preCurrency = agency.AgentCurrency;
        }
        else
        {
            preCurrency = "AED";
        }
        ccaRequest = "merchant_id=" + ConfigurationManager.AppSettings["CCAvenueMerchantId"] + "&order_id=" + orderId + "&amount=" + totalAmount + "&currency=" + preCurrency + "&redirect_url=" + redirectUrl + "&cancel_url=" + redirectUrl + "&language=EN";
      
        strEncRequest = ccaCrypto.Encrypt(ccaRequest, workingKey); 
     %>
        <input  type="hidden" name="Merchant_Id" value="<%=ConfigurationManager.AppSettings["CCAvenueMerchantId"]%>" />
	    <input  type="hidden" name="Amount" value="<%=totalAmount%>" />
	    <input  type="hidden" name="Order_Id" value="<%=orderId%>" />
	    <input type="hidden" name="Redirect_Url" value="<%=redirectUrl %>"/>
	    <%--<input  type="hidden" name="Checksum" value="<%=checksum%>" />	--%> 
	    <input type="hidden" id="encRequest" name="encRequest" value="<%=strEncRequest%>"/>
        <input type="hidden" name="access_code" id="access_code" value="<%=strAccessCode%>"/>    
    </form>
    <script type="text/javascript">
        document.getElementById('nonseamless').submit();
    </script>
<%}%>
</body>
</html>
