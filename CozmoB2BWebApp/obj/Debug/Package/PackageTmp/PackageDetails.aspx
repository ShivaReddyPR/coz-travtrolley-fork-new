﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="PackageDetailsGUI" Title="Package Details" Codebehind="PackageDetails.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
   
   <script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<script type="text/javascript" src="Scripts/jsBE/jquery-1.8.2.min.js"></script>

<script type="text/javascript" src="Scripts/jsBE/WindowPop.js"></script>
<%--<script type="text/javascript" src="Scripts/prototype.js"></script>--%>
<!-- ----------------------For Calender Control--------------------------- -->
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<!-- ----------------------End Calender Control--------------------------- -->
 <link rel="stylesheet" type="text/css" href="Theme/Common.css" />


<!-- Add jQuery library -->
<script type="text/javascript" src="Scripts/JS2/jquery-1.8.2.min.js"></script>
	
<script type="text/javascript" src="Scripts/jsBE/prototype.js"></script>

	<!-- Add fancyBox main JS and CSS files -->

	<link rel="Stylesheet" type="text/css" href="Theme/style.css" />


<link href="css/ModalPop.css"  rel="stylesheet" type="text/css" />
 


<script type="text/javascript">

    var cal1;
    var cal2;
    function init() {
        var dt = new Date();
        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("title", "");
        cal1.cfg.setProperty("close", true);
        cal1.cfg.setProperty("iframe", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();
        //        cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
        //        cal2.cfg.setProperty("title", "Select your desired checkout date:");
        //        cal2.selectEvent.subscribe(setDate2);
        //        cal2.cfg.setProperty("close", true);
        //        cal2.render();
        //load();
    }

    function showCalendar1() {
        //cal2.hide();
        document.getElementById('container1').style.display = "block";


        document.getElementById('Outcontainer1').style.display = "block";
        //        document.getElementById('Outcontainer2').style.display = "none";
    }

    var departureDate = new Date();
    //    function showCalendar2() {
    //        cal1.hide();
    //        var date1 = document.getElementById('checkInDate').value;

    //        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
    //            var depDateArray = date1.split('/');
    //            var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);
    //            cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
    //            cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
    //            cal2.render();
    //        }
    //        document.getElementById('container2').style.display = "block";


    //        document.getElementById('Outcontainer2').style.display = "block";
    //        document.getElementById('Outcontainer1').style.display = "none";
    //    }
    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMessHotel').style.visibility = "visible";
            document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=checkInDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        cal1.hide();
        document.getElementById('Outcontainer1').style.display = "none";
    }
    YAHOO.util.Event.addListener(window, "load", init);




    function selectedNationality(value) {
        //alert(value);
        document.getElementById('paxNationality').value = value;
    }
    function selectedResidence(value) {
        //alert(value);
        document.getElementById('paxResidence').value = value;
    }
    </script>



   <script type="text/javascript" src="Scripts/jsBE/ModalPop.js"></script>


  
  
  <script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<link rel="stylesheet" href="css/ajax_tab_css.css">
   
  
    
     <script type="text/javascript">


         $(function() {
             $('.tabs').tabs()
         });

         $('.tabs').bind('change', function(e) {
             var nowtab = e.target // activated tab
             var divid = $(nowtab).attr('href').substr(1);
             if (divid == "ajax") {
                 $.getJSON('<%=Request.Url.Scheme %>://1.upstatic.sinaapp.com/api.php').success(function(data) {
                     $("#" + divid).text(data.msg);
                 });
             }


         });
  
</script>

  
  
   
   
<link rel="stylesheet" type="text/css" href="css/fancybox2.css" media="screen" />


    <script type="text/javascript">

        function ShowDiv(name) {
            if (name == "home") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Inclusions") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Itinerary") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "RoomRates") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Terms") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                return false;
            }
        }

        //        $(function() {
        //            $('.tabs').tabs()
        //        });

        //        $('.tabs').bind('change', function(e) {
        //            var nowtab = e.target // activated tab
        //            var divid = $(nowtab).attr('href').substr(1);
        //            if (divid == "ajax") {
        //                $.getJSON('<%=Request.Url.Scheme %>://1.upstatic.sinaapp.com/api.php').success(function(data) {
        //                    $("#" + divid).text(data.msg);
        //                });
        //            }


        //        });
        //  
    </script>

   
    
<%--<script type="text/javascript" language="javascript">

    // Regular expression for Not a number;
    var IsNaN = /\D/;
    function SendMailTemp() {
        var phoneNo = document.getElementById('phoneNo').value;
        var packageName = document.getElementById('dealName').value;
        var paramList = "";
        paramList += 'dealId=' + $('dealId').value;
        paramList += '&agencyId=' + $('agencyId').value;
        paramList += '&phoneNo=' + phoneNo;
        paramList += '&packageName=' + packageName;
        //paramList += '&nights=' + $('nights').value;
        //paramList += '&isInternational=' + $('isInternational').value;
        if (Trim(phoneNo) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = 'Please enter phone no.';
            return false;
        }
        if (isNaN(phoneNo)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = 'Please enter only number';
            return false;
        }
        paramList += '&submitEnquiry1=true';
        var url = "EmailAjax";
        new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EmailResponse });
    }
    function EmailResponse(response) {
        if (response.responseText == "true") {
            document.getElementById('message').style.display = "block";
            document.getElementById('message').style.top = document.getElementById('message').style.top - 85 + "px";
            document.getElementById('message').style.left = document.getElementById('message').style.left + 5 + "px";
            document.getElementById('errMess').style.display = "none";
            document.getElementById('phoneNo').value = "enter phone number";
            document.getElementById('phoneField').style.display = "none";
        }
    }
    function ResetForm() {
        document.getElementById('phone').value = "";
        document.getElementById('email').value = "";
        document.getElementById('city').value = "";
        document.getElementById('country').value = "";
        document.getElementById('description').value = ""
        document.getElementById('paxName').value = "";
    }


    function SubmitEnquiry() {
        var phoneNo = document.getElementById('phone').value;
        var activityName = document.getElementById('dealName').value;
        var email = document.getElementById('email').value;
        var productType = document.getElementById('productType').value;
        var city = document.getElementById('city').value;
        var country = document.getElementById('country').value;
        var paxName = document.getElementById('paxName').value;
        var description = document.getElementById('description').value;
        var dealId = document.getElementById('dealId').value;
        var agencyId = document.getElementById('agencyId').value;
        var noofpax = document.getElementById('noofpax').value;
        //var paxName = document.getElementById('paxName').value;

        var paramList = "";
        paramList += '&dealId=' + dealId;
        paramList += '&agencyId=' + agencyId;
        paramList += '&description=' + description;
        paramList += '&productType=' + "A";
        paramList += '&phoneNo=' + phoneNo;
        paramList += '&paxName=' + paxName;
        paramList += '&EmailId=' + email;
        paramList += '&City=' + city;
        paramList += '&Country=' + country;
        // paramList += '&nights=' + $('nights').value;
        paramList += '&activityName=' + activityName;
        paramList += '&Noofpax=' + noofpax;
        if (Trim(paxName) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your name';
            return false;
        }
        if (Trim(email) == "") {
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your email';
            return false;
        }
        if (Trim(city) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your city';
            return false;
        }
        if (Trim(country) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your country';
            return false;
        }
        if (!ValidEmail.test(email)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter valid email';
            return false;
        }
        if (Trim(phoneNo) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter phone number';
            return false;
        }
        if (isNaN(phoneNo)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter correct phone number';
            return false;
        }
        if (Trim(description) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter some description / comment  of your enquiry';
            return false;
        }
        paramList += '&submitActivityEnquiry=true';
        var url = "EmailAjax";
        new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EnquiryRes });
    }
    function EnquiryRes(response) {
        if (response.responseText == "true") {
            document.getElementById('inline1').style.display = "none";
            document.getElementById('err').style.display = "none";
            document.getElementById('message').style.display = "block";
            HideEmailPopup();
            ResetForm();
        }
    }
    function ShowEmailPopup() {

        //        ModalPop.Container = $$$('emailEnquiry');
        ModalPop.Container = document.getElementById('emailEnquiry');
        ModalPop.Show({ x: 735, y: 539 }, "<div>ytytyy</div>");

    }

    function HideEmailPopup() {
        ModalPop.Hide();
    }

    function ShowPopUP() {
        document.getElementById('popup').style.display = "block";
        document.getElementById('message').style.display = "none";
    }
    function HidePopUP() {
        document.getElementById('popup').style.display = "none";
    }
    function HidePopUp() {

        ModalPop.Hide();
    }
    function markout(textBox, txt) {
        if (textBox.value == "") {
            textBox.value = txt;
        }
    }
    function markin(textBox, txt) {
        if (textBox.value == txt) {
            textBox.value = "";
        }
    }
    </script>
   
    <script type="text/javascript" >

        //        function LoadCities() {
        //           var country =  document.getElementById('ddlCountries').options[document.getElementById('ddlCountries').selectedIndex].value;
        //            var paramList = "";
        //            paramList += 'countryCode=' + country;
        //            paramList += '&LoadCities=true';
        //            var url = "EmailAjax.aspx";
        //            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: LoadCitiesResponse });
        //        }

        function LoadCitiesResponse(response) {
            if (response.responseText.length > 0) {
                var cities = response.responseText.split(',');
                var ddl = document.getElementById('ddlCities');
                ddl.options.clear();
                if (cities[0] != "") {
                    for (var i = 0; i < cities.length; i++) {
                        var city = cities[i].split('|');
                        var opt = document.createElement("option");
                        opt.text = city[0];
                        opt.value = city[1];
                        ddl.options.add(opt);
                    }
                }
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        // Regular expression for Not a number;
        var IsNaN = /\D/;
        function SendMail() {
            var phoneNo = document.getElementById('phoneNo').value;
            var packageName = document.getElementById('dealName').value;
            var paramList = "";
            paramList += 'packageId=' + $('packageId').value;
            paramList += '&agencyId=' + $('agencyId').value;
            paramList += '&phoneNo=' + phoneNo;
            paramList += '&packageName=' + packageName;
            paramList += '&nights=' + $('nights').value;
            paramList += '&isInternational=' + $('isInternational').value;
            if (Trim(phoneNo) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = 'Please enter phone no.';
                return false;
            }
            if (isNaN(phoneNo)) {
                document.getElementById('message').style.display = "none";
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = 'Please enter only number';
                return false;
            }
            paramList += '&sendPackageDetails=true';
            var url = "EmailAjax";
            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EmailResponse });
        }
        function EmailResponse(response) {
            if (response.responseText == "true") {
                document.getElementById('message').style.display = "block";
                document.getElementById('message').style.top = document.getElementById('message').style.top - 85 + "px";
                document.getElementById('message').style.left = document.getElementById('message').style.left + 5 + "px";
                document.getElementById('errMess').style.display = "none";
                document.getElementById('phoneNo').value = "enter phone number";
                document.getElementById('phoneField').style.display = "none";
            }
        }
        function ResetForm() {
            document.getElementById('phone').value = "";
            document.getElementById('email').value = "";
            document.getElementById('remarks').value = ""
            document.getElementById('paxName').value = "";
            document.getElementById('<%=txtCity.ClientID %>').value = "";
            document.getElementById('<%=txtCountry.ClientID %>').value = "";
            document.getElementById('<%=ddlAdults.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlChilds.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlInfants.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=checkInDate.ClientID %>').value = "";
        }


        function SubmitEnquiry() {


            var phoneNo = document.getElementById('phone').value;
            var packageName = document.getElementById('dealName').value;
            var email = document.getElementById('email').value;
            var paxName = document.getElementById('paxName').value;
            var remarks = document.getElementById('remarks').value;
            var day = document.getElementById('<%=checkInDate.ClientID %>').value;

            var country = document.getElementById('<%=txtCountry.ClientID %>').value;
            var city = document.getElementById('<%=txtCity.ClientID %>').value;
            var adults = document.getElementById('<%=ddlAdults.ClientID %>').options[document.getElementById('<%=ddlAdults.ClientID %>').selectedIndex].value;
            var childs = document.getElementById('<%=ddlChilds.ClientID %>').options[document.getElementById('<%=ddlChilds.ClientID %>').selectedIndex].value;
            var infants = document.getElementById('<%=ddlInfants.ClientID %>').options[document.getElementById('<%=ddlInfants.ClientID %>').selectedIndex].value;

            var paramList = "";
            paramList += 'packageId=' + document.getElementById('packageId').value;
            paramList += '&agencyId=' + document.getElementById('agencyId').value;
            paramList += '&productType=' + "P";
            paramList += '&remarks=' + remarks;
            paramList += '&country=' + country;
            paramList += '&city=' + city;
            paramList += '&depDate=' + day
            paramList += '&adults=' + adults;
            paramList += '&childs=' + childs;
            paramList += '&infants=' + infants;
            paramList += '&phoneNo=' + phoneNo;
            paramList += '&paxName=' + paxName;
            paramList += '&EmailId=' + email;
            paramList += '&packageName=' + packageName;
            if (Trim(paxName) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter your Name';
                return false;
            }
            if (Trim(email) == "") {
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter your Email';
                return false;
            }
            if (!ValidEmail.test(email)) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter valid Email';
                return false;
            }

            if (Trim(country) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter Country of Residence';
                return false;
            }
            if (Trim(city) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter City of Residence';
                return false;
            }
            if (Trim(day) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter Departure Date';
                return false;
            }

            if (Trim(phoneNo).length == 0) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter Phone number';
                return false;
            }
            if (isNaN(phoneNo)) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter correct Phone number';
                return false;
            }
            if (Trim(remarks).length == 0) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter a brief description of your need';
                return false;
            }
            paramList += '&submitEnquiry=true';
            var url = "EmailAjax";
            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EnquiryRes });
        }
        function EnquiryRes(response) {
            if (response.responseText == "true") {
                alert('Enquery Submitted Successful');
                HideEmailPopup();
                //jQuery(document).fancybox().close();

                //document.getElementById('err').style.display = "none";                
                //document.getElementById('message').style.display = "none";
                ResetForm();
            }
        }
        function ShowEmailPopup() {
            ModalPop.Container = $$$('emailEnquiry');
            ModalPop.Show({ x: 735, y: 530 }, "<div>ytytyy</div>");

        }

        function HideEmailPopup() {
            ModalPop.Hide();
        }
        function foucusOut(textBox, txt) {
            if (textBox.value == "") {
                textBox.value = txt;
            }
        }
        function focusIn(textBox, txt) {
            if (textBox.value == txt) {
                textBox.value = "";
            }
        }
    </script>--%>
      
    <script type="text/javascript" >

//        function LoadCities() {
//           var country =  document.getElementById('ddlCountries').options[document.getElementById('ddlCountries').selectedIndex].value;
//            var paramList = "";
//            paramList += 'countryCode=' + country;
//            paramList += '&LoadCities=true';
//            var url = "EmailAjax.aspx";
//            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: LoadCitiesResponse });
//        }

        function LoadCitiesResponse(response) {
            if (response.responseText.length > 0) {
                var cities = response.responseText.split(',');
                var ddl = document.getElementById('ddlCities');
                ddl.options.clear();
                if (cities[0] != "") {
                    for (var i = 0; i < cities.length; i++) {
                        var city = cities[i].split('|');
                        var opt = document.createElement("option");
                        opt.text = city[0];
                        opt.value = city[1];
                        ddl.options.add(opt);
                    }
                }
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        // Regular expression for Not a number;
        var IsNaN = /\D/;
        function SendMail() {
            var phoneNo = document.getElementById('phoneNo').value;
            var packageName = document.getElementById('dealName').value;
            var paramList = "";
            paramList += 'packageId=' + $('packageId').value;
            paramList += '&agencyId=' + $('agencyId').value;
            paramList += '&phoneNo=' + phoneNo;
            paramList += '&packageName=' + packageName;
            paramList += '&nights=' + $('nights').value;
            paramList += '&isInternational=' + $('isInternational').value;
            if (Trim(phoneNo) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = 'Please enter phone no.';
                return false;
            }
            if (isNaN(phoneNo)) {
                document.getElementById('message').style.display = "none";
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = 'Please enter only number';
                return false;
            }
            paramList += '&sendPackageDetails=true';
            var url = "EmailAjax";
            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EmailResponse });
        }
        function EmailResponse(response) {
            if (response.responseText == "true") {
                document.getElementById('message').style.display = "block";
                document.getElementById('message').style.top = document.getElementById('message').style.top - 85 + "px";
                document.getElementById('message').style.left = document.getElementById('message').style.left + 5 + "px";
                document.getElementById('errMess').style.display = "none";
                document.getElementById('phoneNo').value = "enter phone number";
                document.getElementById('phoneField').style.display = "none";
            }
        }
        function ResetForm() {
            document.getElementById('phone').value = "";
            document.getElementById('email').value = "";
            document.getElementById('remarks').value = ""
            document.getElementById('paxName').value = "";
            document.getElementById('<%=txtCity.ClientID %>').value = "";
            document.getElementById('<%=txtCountry.ClientID %>').value = "";
            document.getElementById('<%=ddlAdults.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlChilds.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlInfants.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=checkInDate.ClientID %>').value = "";
        }


        function SubmitEnquiry() {

            
            var phoneNo = document.getElementById('phone').value;
            var packageName = document.getElementById('dealName').value;
            var email = document.getElementById('email').value;
            var paxName = document.getElementById('paxName').value;
            var remarks = document.getElementById('remarks').value;
            var day = document.getElementById('<%=checkInDate.ClientID %>').value;

            var country = document.getElementById('<%=txtCountry.ClientID %>').value;
            var city = document.getElementById('<%=txtCity.ClientID %>').value;
            var adults = document.getElementById('<%=ddlAdults.ClientID %>').options[document.getElementById('<%=ddlAdults.ClientID %>').selectedIndex].value;
            var childs = document.getElementById('<%=ddlChilds.ClientID %>').options[document.getElementById('<%=ddlChilds.ClientID %>').selectedIndex].value;
            var infants = document.getElementById('<%=ddlInfants.ClientID %>').options[document.getElementById('<%=ddlInfants.ClientID %>').selectedIndex].value;
            
            var paramList = "";
            paramList += 'packageId=' + document.getElementById('packageId').value;
            paramList += '&agencyId=' + document.getElementById('agencyId').value;
            paramList += '&productType=' + "P";
            paramList += '&remarks=' + remarks;
            paramList += '&country=' + country;
            paramList += '&city=' + city;
            paramList += '&depDate=' + day 
            paramList += '&adults=' + adults;
            paramList += '&childs=' + childs;
            paramList += '&infants=' + infants;
            paramList += '&phoneNo=' + phoneNo;
            paramList += '&paxName=' + paxName;
            paramList += '&EmailId=' + email;            
            paramList += '&packageName=' + packageName;
            if (Trim(paxName) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter your Name';
                return false;
            }
            if (Trim(email) == "") {
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter your Email';
                return false;
            }
            if (!ValidEmail.test(email)) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter valid Email';
                return false;
            }

            if (Trim(country) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter Country of Residence';
                return false;
            }
            if (Trim(city) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter City of Residence';
                return false;
            }
            if (Trim(day) == "") {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter Departure Date';
                return false;
            }
           
            if (Trim(phoneNo).length == 0) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter Phone number';
                return false;
            }
            if (isNaN(phoneNo)) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter correct Phone number';
                return false;
            }
            if (Trim(remarks).length == 0) {
                document.getElementById('message').style.display = "none";
                document.getElementById('err').style.display = "block";
                document.getElementById('err').innerHTML = 'Enter a brief description of your need';
                return false;
            }
            paramList += '&submitEnquiry=true';
            var url = "EmailAjax";
            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EnquiryRes });
        }
        function EnquiryRes(response) {
            if (response.responseText == "true") {
                document.getElementById('err').style.display = "none";
                document.getElementById('message').style.display = "block";
                alert('Enquiry Submitted Successful');
                HideEmailPopup();
            //jQuery(document).fancybox().close();
            
                //document.getElementById('err').style.display = "none";                
                //document.getElementById('message').style.display = "none";
                ResetForm();
            }
        }
        function ShowEmailPopup() {
            ModalPop.Container = $$$('emailEnquiry');
            ModalPop.Show("<div>ytytyy</div>");

        }

        function HideEmailPopup() {
            ModalPop.Hide();
        }
        function foucusOut(textBox, txt) {
            if (textBox.value == "") {
                textBox.value = txt;
            }
        }
        function focusIn(textBox, txt) {
            if (textBox.value == txt) {
                textBox.value = "";
            }
        }
    </script>

    <%--<form id="form1" action="PackageDetails.aspx">--%>
    
    <div class="error_msg" style="display:none;" id="errMessHotel"> </div>
   
    
<div>
     
    
    <div id="login_fzebra" style="display: none;">
	




<div class="Email_Package_fancy">


 
<div style=" padding:40px 15px 0px 15px">


<table width="220" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="84%"><label>
      <%--<input style="width:100%" type="text"  name="textfield" id="textfield" />--%>
      
      
      
      <input  style="width:100%" type="text" onBlur="if(this.value=='') this.value='Enter your e-mail'" class="inptfff" onFocus="if(this.value =='Enter your e-mail' ) this.value=''" value="Enter your e-mail" />
      
      
    </label></td> 
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><a style=" float:right" class="blue_button"><span>Submit</span></a></td>
  </tr>
</table>


</div>



</div>







	</div>
	
	<div id="emailEnquiry" class="signin_box_class" style="z-index:9999;display: none; ">
	
	<div id="Outcontainer1" style="position:absolute; display: none; left:23px;  top:100px; border:solid 0px #ccc;  z-index:200; " >

<%--<a onclick="document.getElementById('Outcontainer1').style.display = 'none'" >


<img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />

</a>--%>

<div id="container1"></div>


</div>
	
	
	

<div class="summer-promotion-window">
<em class="close" style=" position:absolute; z-index:9999;  right:10px; cursor:pointer">
					  
						<i><img alt="Close" onclick="HideEmailPopup()" src="images/close-itimes.gif"></i>
					</em>
					
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" style="">
            
            
            <div class="col-md-12"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><h2 style="font-family:Calibri; font-size:22px">Why Wait &amp; Watch</h2></td>
              </tr>
              
            </table></div>
            
            
            
            </td>
          </tr>
          
          <tr>
            <td valign="top" style="border-bottom: solid 1px #CCCCCC"></td>
            </tr>
            
            
          
          
          <tr> 
          <td> 
          
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td>
                
                
                 <div class="col-md-12"><strong style="font-size:13px; color:#cc0000"><strong style=" font-size:14px">
            <%=deal.DealName %>
            (<%=deal.Nights %>
            Nights &
            <%=deal.Nights+1 %>
            Days)</strong></strong>  </div>
                
                </td>
              </tr>
             
              <tr>
                <td>
                
                
                
            
            <div class="col-md-12"> 
            
                <label style="font-size:11px">
                
                
                
                Please provide your enquiry below, one of our agent will get back to you with details.
               
                </label>
            
            
            </div> 
                
                
                
                
                
      
      <div class="col-md-12 padding-0 marbot_10">   
                                         
    <div class="col-md-6">    <div><strong>Name</strong><span style=" color:Red"> *</span> </div>
                
                   <div class="marbot_10"> <input class="form-control"  class="send-enq" type="text" id="paxName" name="paxName" /></div> </div>
    <div class="col-md-6">      <div> <strong>Email ID</strong> <span style=" color:Red"> *</span></div>
                 <div class="marbot_10"><input  class="form-control" type="text" id="email" name="email" /></div></div>


    <div class="clearfix"></div>
    </div>
    
    
               
      <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-6"> <div> <strong>Country of Residence</strong> <span style=" color:Red"> *</span></div>
                
                  <div class="marbot_10"> <asp:TextBox class="form-control" ID="txtCountry" runat="server"></asp:TextBox></div> </div>
    <div class="col-md-6">  <div> <strong>City of Residence</strong> <span style=" color:Red"> *</span></div>
                
                
                  <div class="marbot_10"> <asp:TextBox  ID="txtCity" class="form-control" runat="server"></asp:TextBox></div></div>


    <div class="clearfix"></div>
    </div>       
            
                
               
      <div class="col-md-12 padding-0 marbot_10">                                      
    
    <div class="col-md-6">
    
    
     <div> 
    
    <div>  <strong>Departure Date</strong></div>
     <div class="">  
    
    <table> 
    <tr> 
    <td> <asp:TextBox class="form-control" ID="checkInDate" runat="server" Width="110px" MaxLength="2" onkeypress="return isNumberKey(event)" >
                      
                      </asp:TextBox></td>
    
    <td>     <a href="javascript:void(null)" onclick="showCalendar1()"/><img src="images/call-cozmo.png"></a></td>
    
    
  
      
    
    
    
    
    </tr>
    
    
    </table>
    
    </div>
    </div>
    
    </div>

<div class="col-md-6">
    
    <div> 
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    
    
    
    
        <div class="marbot_10">                                      

 
   
    
    
    <div class="martop_xs10">
    
       <div>    <strong>No of Passengers</strong></div>
   <div>  <table>
    
    <td> <asp:Label ID="Label1" runat="server" Text="Adults:"></asp:Label></td>
    
    <td> <asp:DropDownList CssClass="form-control" ID="ddlAdults"   runat="server">
              
              <asp:ListItem  Selected="True" Text="1" Value="1"></asp:ListItem>
              
              
              <asp:ListItem Text="2" Value="2"></asp:ListItem>
              <asp:ListItem Text="3" Value="3"></asp:ListItem>
              <asp:ListItem Text="4" Value="4"></asp:ListItem>
              <asp:ListItem Text="5" Value="5"></asp:ListItem>
              <asp:ListItem Text="6" Value="6"></asp:ListItem>
              <asp:ListItem Text="7" Value="7"></asp:ListItem>
              <asp:ListItem Text="8" Value="8"></asp:ListItem>
              <asp:ListItem Text="9" Value="9"></asp:ListItem>
              </asp:DropDownList></td>
    
    <td> <asp:Label ID="Label2" runat="server" Text="Childs:"></asp:Label></td>
    
    <td> <asp:DropDownList CssClass="form-control" ID="ddlChilds" runat="server">
              
              
              <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
              
              <asp:ListItem  Text="1" Value="1"></asp:ListItem>
              <asp:ListItem Text="2" Value="2"></asp:ListItem>
              <asp:ListItem Text="3" Value="3"></asp:ListItem>
              <asp:ListItem Text="4" Value="4"></asp:ListItem>
              <asp:ListItem Text="5" Value="5"></asp:ListItem>
              
              </asp:DropDownList></td>
    
    <td><asp:Label ID="Label3" runat="server" Text="Infants:"></asp:Label> </td>
    
      <td> <asp:DropDownList  CssClass="form-control" ID="ddlInfants" runat="server">
              <asp:ListItem style="padding:1px;" Selected="True" Text="0" Value="0"></asp:ListItem>
              <asp:ListItem Text="1" Value="1"></asp:ListItem>
              <asp:ListItem Text="2" Value="2"></asp:ListItem>
              <asp:ListItem Text="3" Value="3"></asp:ListItem>
              <asp:ListItem Text="4" Value="4"></asp:ListItem>
              <asp:ListItem Text="5" Value="5"></asp:ListItem>
              
              </asp:DropDownList></td>
     </table></div>
    
    
     </div>



    <div class="clearfix"></div>
    </div>
    
    
  
                    
    
    
    </td>
  </tr>
</table>
                
                </div> </div>
    <div class="clearfix"></div>
    </div>  
                
               
           
      <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-6">
    
                 <div> <strong>Phone Number</strong> <span style=" color:Red"> *</span></div>
                
                
                
                   <div class="marbot_10"><input  class="form-control" type="text" id="phone" name="phone"  onkeypress="return restrictNumeric(this.id,'1');"/></div>
                
     </div>
    <div class="col-md-6"> </div>


    <div class="clearfix"></div>
    </div>           
                
       
       
             
      <div class="col-md-12 marbot_10">                                      
       
      <div><strong>Please enter a brief description of your need.<span style=" color:Red"> *</span></strong> </div>          
                
           <div class="marbot_10"><textarea rows="4" class="form-control" id="remarks" name="remarks"></textarea></div> 


    <div class="clearfix"></div>
    </div>        
                
                
                
                
                
                
   

        
        
      
        
        
        <div> <%--<a  class="but but_b" onclick="SubmitEnquiry(); ">Submit </a>--%>
        
        
        
        <input type="submit" class="but but_b pull-right btn-xs-block" onclick="return SubmitEnquiry();" value="Submit">
        
        
        </div>    
              
              
                <div><label> <span style="color:Red;font-size:medium;" id="err" style="display:none;"></span></label> </div>   
          
                </td>
              </tr>
              
             
                  
                  
                </table>
          
          </td>
          
          
          </tr>
              
              
            </table>



</div>





<div class="clear"></div>


	</div>
	
	
    
    
    <div>
<div class="ns-h3">
                    <%=deal.DealName %>
                    (<%=deal.Nights %>
                        Nights &amp;
                        <%=deal.Nights + 1 %>
                        Days) </div>
<div class="clear"></div>
 


<div class="wraps0ppp">


<div> 

<div class="col-md-8 gap_pck pad_left0"> 
      <img width="624px" height="270px" src="<%=imageServerPath + deal.MainImagePath%>" alt="<%=deal.DealName %>" title="<%=deal.DealName %>" />
        
        
        
        
        <%--<img width="100%" height="250px"  src="<%=Request.Url.Scheme %>://cozmotravel.com/Images/PackageImages/168/140620161752245224_M.jpg" />--%>
        


</div> 

<div class="col-md-4"> 


            
            
            <div> 

            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
             
             
             
             <tr>
             <td colspan="2"> 
             
<center>              
                         <p class="lowest_price">Starting From</p>
<p class="best_price"><%=Settings.LoginInfo.Currency %> <%=minPrice.ToString("N"+Settings.LoginInfo.DecimalValue) %></p>

<span class="spccc">per person</span></center>
             
             </td>
             
             
             </tr>
             
             
             <tr> 
             
             <td style=" border-bottom: solid 1px #ccc; height:10px;" colspan="2"> </td>
             </tr>
             
              <tr>
                <td align="right"><img src="images/submit_enquiry_icon.jpg" width="26" height="29"></td>
                <td align="left">
               <%--<a class="nor_lnk fancybox" href="#EmailPackage">Submit Enquiry </a>--%>
                
          <a class="nor_lnk" href="#" onclick="ShowEmailPopup()">Submit Enquiry </a>
              
               <%--  <a class="fancybox" href="#login_fzebra">View Photos</a>--%>
                
                </td>
              </tr>
              
               <tr>
              
              
                <td style=" border-bottom: solid 1px #ccc;" colspan="2"> </td>
             </tr>
             
             
              <tr>
                <td width="40%" align="right"><img src="images/print_icon.jpg" width="26" height="29"></td>
                <td width="60%" align="left">
                
              <%--  <a  href="#">Print this Package </a>--%>
                
             <a style=" cursor:pointer" class="nor_lnk" onClick="javascript:window.open('PrintPackage?packageId=<%=Request["packageId"] %>','','location=0,status=0,scrollbars=1,width=1000,height=600,top=0,left=0');">Print this Package </a></td>
                
               
              </tr>
              
                 <tr> 
             
             <td style=" border-bottom: solid 1px #ccc;" colspan="2"> </td>
             </tr>
            </table>
            
              </div>
     
            
     
        

</div> 


<div class="clearfix"> </div> 

</div>



</div>






</div>


 <div class="col-md-12 padding-0 margin_top20"> 

 

    
    <!--easy reponsive tabs-->
    <script src="Scripts/easyResponsiveTabs.js"></script>
    
    <link rel="stylesheet" type="text/css" href="css/easy-responsive-tabs.css" />
  
    
     	<!--Plug-in Initialisation-->
	<script type="text/javascript">
	    $(document).ready(function() {
	        //Horizontal Tab
	        $('#parentHorizontalTab').easyResponsiveTabs({
	            type: 'default', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            fit: true, // 100% fit in a container
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });

	        // Child Tab
	        $('#ChildVerticalTab_1').easyResponsiveTabs({
	            type: 'vertical',
	            width: 'auto',
	            fit: true,
	            tabidentify: 'ver_1', // The tab groups identifier
	            activetab_bg: '#fff', // background color for active tabs in this group
	            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
	            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
	            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
	        });

	        //Vertical Tab
	        $('#parentVerticalTab').easyResponsiveTabs({
	            type: 'vertical', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            fit: true, // 100% fit in a container
	            closed: 'accordion', // Start closed if in accordion view
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo2');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });
	    });
</script>

<div id="parentVerticalTab">
            <ul class="resp-tabs-list hor_1">
                <li>Overview</li>
                <li>Inclusions & Exclusions</li>
                
                <li>Itinerary</li>
                
                <li> Package Rates</li> 
             
                         
                <li>Terms & Conditions</li> 
                                
            </ul>
           
           
            <div class="resp-tabs-container hor_1">
                
              

<div  id="Overview">
 
 
 
 <%=deal.Overview %>
</div>


 <div id="Inclusions">
 


                            
                            <ul style="padding:0px 20px 0px 15px;">
                            <strong>Inclusions:</strong><br />
                            <%for (int i = 0; i < inclusionsLimit; i++)
                              { %>
                         
                                <li style="list-style-type:circle; padding-bottom:10px "><%=inclusions[i]%></li>
                          
                            <%} %>
                            </ul>
                            <ul style="padding:0px 20px 0px 15px;">
                            <strong>Exclusions:</strong><br />
                                <%for (int i = 0; i < notes.Length; i++)
                                  { %>
                                <li style="list-style-type:circle; padding-bottom:10px">
                                     
                                            <%=notes[i]%>
                                        
                                </li>
                                <%} %>
                            </ul>
                       
 
 
 </div>
 
  <div id="Itinerary">
  
  
<span>
 <%for (int i = 0; i < (itinaryList.Count / 2 + itinaryList.Count % 2); i++)
                                  {
                                      if (i <= (deal.Nights ))
                                      {
                                          dayItinerary = itinaryList[i].Split(splitter, StringSplitOptions.RemoveEmptyEntries);%>
                               
                               
                               
                             
                                    
                                    
                                    
                                    <div style=" padding-bottom:7px"><b>Day
                                            <%=i + 1%>
                                            :
                                            <%=dayItinerary[0]%>
                                        </b></div>
                                           
                                            <%=dayItinerary[1]%>
                                            <br /> 
                                           
                                           
                                             <div style=" padding-top:7px; padding-bottom:7px"> <b>Meals: </b><%=dayItinerary[2]%></div>
                                            
                                           
                                           
                                            
                                            <%if (dayItinerary[3] != "N.A" && dayItinerary[3] != "N.A.")
                                              { %>
                                            <b>Optional: </b>
                                            <%=dayItinerary[3]%>
                                            <br />
                                            <%} %>
                                       
                                       <br />
                                       
                               
                                
                                
                                
                                <%}
                                  } %>
                            </span>
  
  
  
  <span class="package_subpart">
                                <%for (int i = (itinaryList.Count / 2 + itinaryList.Count % 2); i < itinaryList.Count; i++)
                                  {
                                      if (i <= (deal.Nights ))
                                      {
                                          dayItinerary = itinaryList[i].Split(splitter, StringSplitOptions.RemoveEmptyEntries);%>
                              
                                    <%--<i><img src="Images/blue_bullet.gif" alt="Bullet" /></i> --%>
                                
                                 <div style=" padding-bottom:7px">  <b>Day
                                            <%=i + 1%>
                                            :
                                            <%=dayItinerary[0]%>
                                        </b>
                                        
                                          </div>
                                            
                                            
                                            <%=dayItinerary[1]%>
                                            <br />
                                            
                                            <div style=" padding-top:7px; padding-bottom:7px"> 
                                            <b>Meals: </b>
                                            
                                            <%=dayItinerary[2]%>
                                            </div>
                                            
                                            <%if (dayItinerary[3] != "N.A")
                                              { %>
                                            <b>Optional: </b>
                                            <%=dayItinerary[3]%>
                                            <br />
                                            <%} %>
                                       
                                        <br />
                               
                                
                                
                                <%}
                                  } %>
                            </span>
  
  </div>
  
   
    <div id="Term-condition">


                            <%if (price.Length > 0)
                              { %>
                              <strong>Prices does not inlcude:</strong>
                            <ul style="padding:0px 20px 0px 15px;">              
                                
                              
                                <%for (int i = 0; i < price.Length; i++)
                                  { %>
                                <li style="list-style-type:circle; padding-bottom:10px ">
                                     
                                            <%=price[i] %>
                                        
                                </li>
                                <%} %>
                            </ul>
                            <%} %>
                           
                            <%if (terms.Length > 0)
                              { %>
                            <div>
                               <%--<br /> <strong>Terms and Conditions:</strong>--%>
                                <%for (int i = 0; i < terms.Length; i++)
                                  { %>
                                  
                                  
                               <ul style="padding:0px 20px 0px 15px;">              
                                <li style="list-style-type:circle; padding-bottom:10px ">
                                     
                                            <%=terms[i]%>
                                        
                               </li>
                                
                                </ul>
                              
                                <%} %>
                            </div>
                            <%} %>
                       
                        
    
    
    
    </div>
    
    
    <div id="RoomRates">
<div>
                       
                        <%=deal.RoomRates %>
                    </div>






                   
                
 </div>
            
            
        </div>

</div> 
    
    <%--</form>--%>
    
    </div>



<div class="clear"></div>
</div>
    
    <input type="hidden" id="agencyId" name="agencyId" value="<%=Settings.LoginInfo.AgentId %>" />
    <input type="hidden" id="packageId" name="packageId" value="<%=deal.DealId %>" />
    <input type="hidden" id="dealName" name="dealName" value="<%=deal.DealName %>" />
    <input type="hidden" id="nights" name="nights" value="<%=deal.Nights %>" />
    <input type="hidden" id="isInternational" name="isInternational" value="<%=deal.IsInternational %>" />
    
    
    
    <div style="display:none" class="content_container">
        <div class="content_maincontainer">
         
            <div style=" display:none" class="box_maincontainer padding_bottom">
                <div class="packages_container">
                    <div style="border: solid 2px red" class="packages_fulldescription_module">
                        <label>
                            <tt>
                                </tt>
                            
                        </label>
                        <span class="packages_fulldescription_content width_100"><tt>Overview : </tt>
                           
                        </span>
                    </div>
                    
                    <div class="packageenquiry_content">
                      <i id="message" style="display: none;"><del>
                                    <img src="Images/greenmark.gif" alt="mark" /></del> <dfn>Our Travel Expert will call
                                        you shortly</dfn> </i>
                                        
                                        <tt style="width:181px">
                                            
                                            
                                            <span id="bookNow" style="display:none"><a class="clik" href="<%=redirectToPage %><%=deal.DealId %>" >Book Now</a></span>
                                            </tt>
                    </div>
                    <div class="packages_fulldescription_content width_100">
                        <tt>Inclusions : </tt>
                        
                    </div>
                    <%if (itinaryList.Count > 0)
                      { %>
                    <div class="packages_fulldescription_content width_100">
                        <tt>Itinerary : </tt>
                        <div class="package_subcontent">
                            
                        </div>
                    </div>
                    <%} %>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
   
    
    
    
   
    <script>
        //document.getElementById("middle-container").style.display = 'none';
    </script>
</asp:Content>

