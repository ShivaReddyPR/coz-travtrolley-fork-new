﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="ExcelUploadFile" Codebehind="ExcelUploadFile.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <script type="text/javascript">
        function Validate() {
            if (document.getElementById('<%= fileuploadExcel1.ClientID %>').value == '' || document.getElementById('<%=fileuploadExcel1.ClientID %>').value == null) {
                alert('Attach Excel File cannot be Blank ! ');
                return false;
            }
            return true;
        }
    </script>
    
    
              <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <form id="form1" runat="server">
    <asp:Label ID="lblErrorMessage" runat="server" Visible="false"></asp:Label>
    
     <label class="btn btn-default btn-file">
  Attach Excel file:
    <asp:FileUpload ID="fileuploadExcel1" runat="server" />
    
    
    </label>
    <asp:Button CssClass="btn btn-primary" ID="btnSend" OnClientClick="return Validate();" runat="server" Text="Export" OnClick="btnSend_Click" />
   
   
    </form>
</body>
</html>
