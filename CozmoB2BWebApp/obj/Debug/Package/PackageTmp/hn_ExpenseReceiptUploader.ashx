﻿<%@ WebHandler Language="C#" Class="hn_ExpenseReceiptUploader" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Collections.Generic;
using CT.Core;
public class hn_ExpenseReceiptUploader : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (context.Session["ExpenseReceiptFiles"] != null)
            {
                files = context.Session["ExpenseReceiptFiles"] as List<HttpPostedFile>;
            }
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                files.Add(file);
            }
            context.Session["ExpenseReceiptFiles"] = files;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}
