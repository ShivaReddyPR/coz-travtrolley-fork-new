﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareProductTypeMaster.aspx.cs" Inherits="CozmoB2BWebApp.ReligareProductTypeMaster" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
 <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
      <div><h3>Religare Product Type Master</h3></div>
         <div class="body_container" style="height:200px">
        <div class="col-md-12">
            <div class="col-md-2">
                <asp:Label ID="lblProductName" runat="server" Text="Product Name:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-12">
            <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
            </div>
            </div>
        <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <script type="text/javascript">
        function Save() {
        if (getElement('txtProductName').value == '') addMessage('Product Name cannot be blank!', '');
        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
    </script>
    <asp:HiddenField runat="server" ID="hdfmeid" Value="0" />
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 

 <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="PRODUCTID"
      emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="20" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">


     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField>
           
        <ItemTemplate >
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("PRODUCTNAME") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCTNAME") %>'></asp:Label>
   
    </ItemTemplate>   
          <HeaderTemplate>
              Product Name
          </HeaderTemplate>
         </asp:TemplateField>
     </Columns>

     </asp:GridView>
    </asp:Content>

    

