﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Title="Offline Train Booking" CodeBehind="OfflineTrainBooking.aspx.cs" Inherits="CozmoB2BWebApp.OfflineTrainBooking" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style>
        .disabled-button {
            background-color: #4c5f5930;
        }

        .enabled-button {
            background-color: #60c231;
        }

        .search-matrix-wrap {
            background-color: #f7f7f7;
            min-height: auto
        }

            .search-matrix-wrap .form-control-holder {
                margin-bottom: 0px;
                border: solid 1px #ccc;
            }

            .search-matrix-wrap .btn-group {
                border: solid 1px #ccc;
            }

            .search-matrix-wrap .form-control, .search-matrix-wrap .form-control-element, .search-matrix-wrap .form-control-holder .form-control-text, .CorpTrvl-page .form-control {
                height: 30px;
                line-height: 30px;
            }

        .form-control-holder .form-control.select2-container .select2-choice, .form-control-holder .select2-container.form-control-element .select2-choice {
            height: inherit;
            line-height: inherit;
        }

        .search-matrix-wrap .form-control-holder .icon-holder span, .search-matrix-wrap .form-control-holder .icon-holder {
            padding-top: 0px;
        }

        .search-matrix-wrap .with-custom-dropdown::after {
            padding-top: 6px;
        }

        .rulddlhotel {
            color: Black
        }

        .CorpTrvl-page .policy-textarea {
            height: 80px;
        }

        .bg_supplier {
            background-color: #60c231;
            color: #fff;
        }

        .bg_agent {
            background-color: #6d6d6d;
            color: #fff;
        }

        .rptdiv {
            margin-bottom: 10px
        }

            .rptdiv .roomNumber {
                background: #ccc;
                display: block;
                padding: 0px 10px 0px 10px;
                line-height: 24px
            }

            .rptdiv .borsap {
                border: solid 1px #ccc;
                padding: 0px 10px 0px 10px
            }

        [aria-expanded="true"] .fa.fa-plus-circle:before {
            content: "\f056";
        }

        .error {
            border: 1px solid #D8000C;
        }
    </style>
    <%--<script src="/scripts/Jquery/Jquery-ui.min.js"></script>--%>
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-ui.js"></script>
    <script src="Scripts/Common/common.js"></script>
    <script src="Scripts/Common/FlexFields.js"></script>
    <script type="text/javascript">

        var LoginInfo;
        var RailwayItinerary = [];
        var PassenegerInfo = [];
        var BookingRemarks = [];

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
            return ret;
        }
        function IsAlpha(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
            return ret;
        }
        function IsAlphaNumericString(id) {
            if (/^[a-zA-Z ]*$/.test($('#' + id).val()) == false) {
                $('#' + id).val($('#' + id).val().replace(/[^a-zA-Z]/gi, ''));
            }
        }
        function IsAlphaString(id) {
            if (/^[a-zA-Z0-9 ]*$/.test($('#' + id).val()) == false) {
                $('#' + id).val($('#' + id).val().replace(/[^a-zA-Z]/gi, ''));
            }
        }
        try {
            $(document).ready(function () {
                LoginInfo = JSON.parse(document.getElementById('<%=hdnLoginInfo.ClientID %>').value);
                LoginInfo.IsOnBehalfOfAgent = false;
                BindDataonpageload();
                //loadDummyData();
                resetDate();
            });
        } catch (e) {
            var msg = JSON.stringify(e.stack); e
            exceptionSaving(msg);
        }
        function resetDate() {
            FromDate = new Date();
            $("#txtDate").datepicker(
                {
                    minDate: 0,
                    numberOfMonths: [1, 2],
                    dateFormat: 'dd/mm/yy',
                    onSelect: function (dateText, inst) {
                        var selectedDate = new Date(ConvertDate(dateText));
                        selectedDate.setDate(selectedDate.getDate() + 1);
                    },
                    onchange: function (dateText, inst) {
                    }
                }
            ).datepicker("setDate", FromDate);
            FromDate.setDate(FromDate.getDate() + 1);
        }
        function showMessage(msg, status) {
            if (status == 'success') {
                $("#SubmitMessage div").removeClass("alert-danger");
                $("#SubmitMessage div").addClass("alert-success");
            }
            if (status == 'error') {
                $("#SubmitMessage div").removeClass("alert-success");
                $("#SubmitMessage div").addClass("alert-danger");
            }
            $("#SubmitMessage div").text(msg);
            $("#SubmitMessage").show();
        }
        function hideMessage() {
            $("#SubmitMessage div").text('');
            $("#SubmitMessage").hide();
        }
        function showValidationMessage(msg) {
            $("#errMess").show();
            $("#errorMessage").text(msg);
        }
        function hideValidationMessage() {
            $("#errorMessage").text = "";
            $("#errMess").hide();
        }
        function loadDummyData() {
            $('#txtFromStation').val('New Delhi');
            $('#txtToStation').val('Jammu');
            $('#txtBoardingFrom').val('New Delhi');
            $('#ddlClass').val('Sleeper');
            $('#s2id_ddlClass .select2-chosen').text('Sleeper');
            $('#txtReservationUpto').val('Test');
            $('#txtTrainNumber').val('TRN001');
            $('#txtTrainName').val('Netravati');
            $('#ddlIdProofType').val('PAN CARD');
            $('#s2id_ddlIdProofType .select2-chosen').text('PAN CARD');
            $('#txtIdNumber').val('CCBPR0001');
            $('#ddlQuota').val('TATKAL');
            $('#s2id_ddlQuota .select2-chosen').text('TATKAL');
            $('#txtEmail').val('rathlal@gmail.com');
            $('#ddlChild').val('1');
        }
        function loadDummyPassenger() {
            let adultCount = $("#ddlAdult").val();
            let childCount = $("#ddlChild").val();
            for (var j = 1; j <= adultCount; j++) {
                $('#txtFirstName-' + j).val('AdultName' + j);
                $('#ddlGender-' + j).val('Male');
                $('#txtAge-' + j).val('34');
                $('#ddlBerth-' + j).val('Upper');
                $('#ddlSeniorCitizen-' + j).val('Yes');
                $('#ddlMeal-' + j).val('Veg');
            }
            for (var k = 1; k <= childCount; k++) {
                $('#txtFirstNameC-' + k).val('ChildName' + k);
                $('#ddlGenderC-' + k).val('Female');
                $('#ddlAgeC-' + k).val('10');
                $('#ddlBerthC-' + k).val('Lower');
                $('#ddlMealC-' + k).val('Non Veg');
            }
        }
        function exceptionSaving(msg) {
            var data = JSON.parse(AjaxCalls("OfflineTrainBooking.aspx/auditSaving", "{'exception':'" + msg + "'}"));
        }
        function showHideAgent() {
            if ($("#tglAgentCtr").prop("disabled") != true) {
                var x = document.getElementById("clientDiv");
                if (x.style.display === "none") {
                    x.style.display = "block";
                    LoginInfo.IsOnBehalfOfAgent = true;
                    LoginInfo.AgentId = $("#ddlAgent").val();
                    LoginInfo.LocationID = $("#ddlLocation").val();

                } else {
                    x.style.display = "none";
                    LoginInfo = JSON.parse(document.getElementById('<%=hdnLoginInfo.ClientID %>').value);
                    LoginInfo.IsOnBehalfOfAgent = false;
                }
            }
        }
        $(function () {
            $('#ddlAgent').change(function () {
                $('.agnetBalance').hide();
                $('#' + $(this).val()).show();
                LoginInfo.AgentId = $('#ddlAgent').val();
            });
        });
        $(function () {
            $('#ddlLocation').change(function () {
                LoginInfo.LocationID = $('#ddlLocation').val();
            });
        });
        function loadControls() {
            try {
                let adultCount = $("#ddlAdult").val();
                let childrenCount = $("#ddlChild").val();
                var GuestAdult = $('#Adult').html();
                var Count = 0; var ChildCount = 0; var TotalPax = 1;
                $('#ListGuest').empty();
                for (var j = 1; j <= adultCount; j++) {
                    $('#ListGuest').append('<li id="ListAdult' + j + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + GuestAdult + '</li>');
                    Count++;

                    $('#h4PaxType').attr('id', 'h4PaxType-' + j);
                    if (j == 1) {
                        $('#h4PaxType-' + j).text('ADULT-' + Count + '(LEAD PASSENGER)');
                    }
                    else {
                        $('#h4PaxType-' + j).text('ADULT-' + Count);
                    }

                    var firstname = '<label>First Name<font color="red">*</font></label><input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" placeholder="First Name" maxlength="50"  id="txtFirstName-' + j + '">';
                    $('#divpaxFirstName').attr('id', 'divpaxFirstName-' + j);
                    $('#divpaxFirstName-' + j).append(firstname);

                    var lastname = '<label>Last Name</label><input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" placeholder="Last Name" maxlength="50" id="txtLastName-' + j + '">';
                    $('#divpaxLastName').attr('id', 'divpaxLastName-' + j);
                    $('#divpaxLastName-' + j).append(lastname);

                    var gender = '<label>Gender<font color="red">*</font></label><select class="form-control custom-select PDrequired" id="ddlGender-' + j +
                        '" onchange="select(this.id);"><option value="0">Select Gender</option> <option value="Male">Male</option> <option value="Female">Female</option></select>';
                    $('#divpaxGender').attr('id', 'divpaxGender-' + j);
                    $('#divpaxGender-' + j).append(gender);

                    var age = '<label>Age<font color="red">*</font></label><input type="text" class="form-control" placeholder="Age" maxlength="2" id="txtAge-' + j + '">';
                    $('#divpaxAge').attr('id', 'divpaxAge-' + j);
                    $('#divpaxAge-' + j).append(age);

                    $('#txtAge-' + j).attr('onkeypress', 'return isNumber(event);');

                    var berth = '<label>Berth Preference</label><select class="form-control custom-select PDrequired" id="ddlBerth-' + j +
                        '" onchange="select(this.id);"><option value="0">Select Berth Preference</option> <option value="Lower">Lower</option> <option value="Middle">Middle</option>' +
                        '<option value = "Upper" >Upper</option> <option value = "Side Lower" >Side Lower</option> <option value = "Side Upper">Side Upper</option>' +
                        '<option value = "Window">Window</option> <option value = "Aisle">Aisle</option></select > ';
                    $('#divpaxBerth').attr('id', 'divpaxBerth-' + j);
                    $('#divpaxBerth-' + j).append(berth);

                    var meal = '<label>Meal</label><select class="form-control custom-select PDrequired" id="ddlMeal-' + j +
                        '" onchange="select(this.id);"><option value="0">Select Meal</option> <option value="Veg">Veg</option> <option value="Non Veg">Non Veg</option></select>';
                    $('#divpaxMeal').attr('id', 'divpaxMeal-' + j);
                    $('#divpaxMeal-' + j).append(meal);

                    var seniorCitizen = '<label>Senior Citizen<font color="red">*</font></label><select class="form-control custom-select PDrequired" id="ddlSeniorCitizen-' + j +
                        '" onchange="select(this.id);"><option value="0">Select</option> <option value="No">No</option> <option value="Yes">Yes</option> </select>';
                    $('#divpaxSeniorCitizen').attr('id', 'divpaxSeniorCitizen-' + j);
                    $('#divpaxSeniorCitizen-' + j).append(seniorCitizen);
                }
                for (var k = 1; k <= childrenCount; k++) {
                    $('#ListGuest').append('<li id="ListAdultC' + k + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + GuestAdult + '</li>');
                    ChildCount++;
                    $('#h4PaxType').attr('id', 'h4PaxTypeC-' + k);
                    $('#h4PaxTypeC-' + k).text('CHILD-' + ChildCount);

                    var firstname = '<label>First Name<font color="red">*</font></label><input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" placeholder="First Name" id="txtFirstNameC-' + k + '">'
                    $('#divpaxFirstName').attr('id', 'divpaxFirstNameC-' + k);
                    $('#divpaxFirstNameC-' + k).append(firstname);

                    var lastname = '<label>Last Name</label><input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" placeholder="Last Name" id="txtLastNameC-' + k + '">'
                    $('#divpaxLastName').attr('id', 'divpaxLastNameC-' + k);
                    $('#divpaxLastNameC-' + k).append(lastname);

                    var gender = '<label>Gender<font color="red">*</font></label><select class="form-control custom-select PDrequired" id="ddlGenderC-' + k +
                        '" onchange="select(this.id);"><option value="0">Select Gender</option> <option value="Male">Male</option> <option value="Female">Female</option></select>';
                    $('#divpaxGender').attr('id', 'divpaxGenderC-' + k);
                    $('#divpaxGenderC-' + k).append(gender);

                    var age = '<label>Age<font color="red">*</font></label><select class="form-control custom-select PDrequired" id="ddlAgeC-' + k +
                        '" onchange="select(this.id);"><option value="0">Select Age</option> <option value="5">5</option> <option value="6">6</option>' +
                        '<option value = "7" >7</option> <option value = "8" >8</option> <option value = "9">9</option>' +
                        '<option value = "10">10</option> <option value = "11">11</option><option value = "12">12</option></select >';
                    $('#divpaxAge').attr('id', 'divpaxAgeC-' + k);
                    $('#divpaxAgeC-' + k).append(age);

                    var berth = '<label>Berth Preference</label><select class="form-control custom-select PDrequired" id="ddlBerthC-' + k +
                        '" onchange="select(this.id);"><option value="0">Select Berth Preference</option> <option value="Lower">Lower</option> <option value="Middle">Middle</option>' +
                        '<option value = "Upper" >Upper</option> <option value = "Side Lower" >Side Lower</option> <option value = "Side Upper">Side Upper</option>' +
                        '<option value = "Window">Window</option> <option value = "Aisle">Aisle</option></select > ';
                    $('#divpaxBerth').attr('id', 'divpaxBerthC-' + k);
                    $('#divpaxBerthC-' + k).append(berth);

                    $('#divpaxSeniorCitizen').attr('id', 'divpaxSeniorCitizenC-' + k);
                    $('#divpaxSeniorCitizenC-' + k).parent().attr("style", "display:none");

                    var meal = '<label>Meal</label><select class="form-control custom-select PDrequired" id="ddlMealC-' + k +
                        '" onchange="select(this.id);"><option value="0">Select Meal</option> <option value="Veg">Veg</option> <option value="Non Veg">Non Veg</option></select>';
                    $('#divpaxMeal').attr('id', 'divpaxMealC-' + k);
                    $('#divpaxMealC-' + k).append(meal);
                }
                $("#divPassengerDetailsBlock").show();
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function clearTrainDetails() {
            try {
                hideMessage();
                disableControls(false);
                LoginInfo = JSON.parse(document.getElementById('<%=hdnLoginInfo.ClientID %>').value);
                LoginInfo.IsOnBehalfOfAgent = false;
                $('#ddlAgent').val('-1');
                $('#ddlAgent').select2();
                $("#ddlLocation option").remove();
                var $chkbox = $('#s2id_ddlLocation').find('span');
                $chkbox[0].innerText = "Select Location";
                $("#s2id_ddlLocation option").remove();
                var options = "<option value='" + -1 + "'>Select Location</option>";
                $('#ddlLocation').append(options);

                $('#txtFromStation').val('');
                $('#txtToStation').val('');
                $('#txtBoardingFrom').val('');
                $('#ddlClass').val('0');
                $('#s2id_ddlClass .select2-chosen').text('Class');
                $('#txtReservationUpto').val('');
                $('#txtTrainNumber').val('');
                $('#txtTrainName').val('');
                $('#ddlIdProofType').val('0');
                $('#s2id_ddlIdProofType .select2-chosen').text('ID Proof Type');
                $('#txtIdNumber').val('');
                $('#ddlQuota').val('');
                $('#s2id_ddlQuota .select2-chosen').text('Quota');
                $('#txtEmail').val('');
                $('#ddlAdult').val('1');
                $('#ddlChild').val('0');

                trainPaxCount();
                resetDate();
                $("#tglAgentCtr").prop("disabled", false);
                $("#tglAgentCtr").prop("checked", false);
                $("#clientDiv").css("display", "none");
                $('#s2id_ddlAgent .select2-chosen').text('Select Client');
                $('#s2id_ddlLocation .select2-chosen').text('Select Location');
                removePassengers();

                BookingRemarks.length = 0;
                PassenegerInfo.length = 0;
                RailwayItinerary.length = 0;
            }
            catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function clearPassengerDetails() {
            try {
                hideMessage();
                let adultCount = $("#ddlAdult").val();
                let childCount = $("#ddlChild").val();

                for (var j = 1; j <= adultCount; j++) {
                    $('#txtFirstName-' + j).val('');
                    $('#txtLastName-' + j).val('');
                    $('#txtAge-' + j).val('');
                    $('#ddlBerth-' + j).val('0');
                    $('#ddlSeniorCitizen-' + j).val('0');
                    $('#ddlMeal-' + j).val('0');
                }
                for (var k = 1; k <= childCount; k++) {
                    $('#txtFirstNameC-' + k).val('');
                    $('#txtLastNameC-' + k).val('');
                    $('#ddlAgeC-' + k).val('0');
                    $('#ddlBerthC-' + k).val('0');
                    $('#ddlMealC-' + k).val('0');
                }
            }
            catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function removePassengers() {
            $("#ListGuest").empty();
            $("#divPassengerDetailsBlock").hide();
            $('#btnClearTrainDetails').prop("disabled", false);
            $('#btnAddTrainDetails').prop("disabled", false);
            $('#btnClearTrainDetails').removeClass("disabled-button").addClass("enabled-button");
            $('#btnAddTrainDetails').removeClass("disabled-button").addClass("enabled-button");
            $("#SubmitBlock").hide();
            disableControls(false);
        }
        function disableControls(val) {
            try {
                $("#ddlAdult").prop("disabled", (val == true) ? true : false);
                $("#ddlChild").prop("disabled", (val == true) ? true : false);
                $("#tglAgentCtr").prop("disabled", (val == true) ? true : false);
                $("#ddlAgent").prop("disabled", (val == true) ? true : false);
                $("#ddlLocation").prop("disabled", (val == true) ? true : false);
                //document.getElementById("tglAgentCtr").disabled = (val == true) ? true : false;
                $('.tgl tgl-light').prop("disabled", false);
                //$(".tgl tgl-light").prop("disabled", (val == true) ? true : false);

                $('#txtFromStation').prop("disabled", (val == true) ? true : false);
                $('#txtToStation').prop("disabled", (val == true) ? true : false);
                $('#txtBoardingFrom').prop("disabled", (val == true) ? true : false);
                $('#ddlClass').prop("disabled", (val == true) ? true : false);
                $('#s2id_ddlClass .select2-chosen').prop("disabled", (val == true) ? true : false);
                $('#txtReservationUpto').prop("disabled", (val == true) ? true : false);
                $('#txtTrainNumber').prop("disabled", (val == true) ? true : false);
                $('#txtTrainName').prop("disabled", (val == true) ? true : false);
                $('#ddlIdProofType').prop("disabled", (val == true) ? true : false);
                $('#s2id_ddlIdProofType .select2-chosen').prop("disabled", (val == true) ? true : false);
                $('#txtIdNumber').prop("disabled", (val == true) ? true : false);
                $('#ddlQuota').prop("disabled", (val == true) ? true : false);
                $('#s2id_ddlQuota .select2-chosen').prop("disabled", (val == true) ? true : false);
                $('#txtEmail').prop("disabled", (val == true) ? true : false);
                $('#ddlChild').prop("disabled", (val == true) ? true : false);

                $('#btnClearTrainDetails').prop("disabled", (val == true) ? true : false);
                $('#btnAddTrainDetails').prop("disabled", (val == true) ? true : false);

                $('#btnClearTrainDetails').removeClass((val == true) ? "enabled-button" : "disabled-button").addClass((val == true) ? "disabled-button" : "enabled-button");
                $('#btnAddTrainDetails').removeClass((val == true) ? "enabled-button" : "disabled-button").addClass((val == true) ? "disabled-button" : "enabled-button");

            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function select(id) {
            tmpval = $('#' + id).val();
            if (tmpval == '') {
                $('#' + id).addClass('error');
            } else {
                $('#' + id).removeClass('error');
            }
        }
        function ConvertDate(selector) { // To matach with Main search Date Format
            try {
                var from = selector.split("/");
                var Date = from[1] + "/" + from[0] + "/" + from[2];
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
            return Date;
        }
        function BindDataonpageload() {
            try {
                var options = "";
                var data = JSON.parse(AjaxCalls("OfflineTrainBooking.aspx/LoadRequiredData", ""));
                options = "";
                $.each(data.Agents, function (index, item) {
                    //Excluding logged in adgent
                    if (item.AGENT_ID != LoginInfo.AgentId) {
                        options += "<option value='" + item.AGENT_ID + "'>" + item.AGENT_NAME + "</option>";
                    }
                });
                agents = data.Agents;
                $('#ddlAgent').append(options);
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function AjaxCalls(url, data) {
            try {
                var obj = "";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
                    },
                    error: function (d) {
                        console.log(JSON.stringify(error));
                    }
                });
                return obj;
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function getLocationsByAgentId() {
            try {
                var options = "<option value='" + -1 + "'>Select Location  </option>";
                var Agentid = $('#ddlAgent').val();
                var selectedAgent = agents.find(x => x.AGENT_ID == Agentid);
                //$('#divBehalfAgentBalance').show();
                $('#spnbalance').text(selectedAgent.current_balance);
                var data = JSON.parse(AjaxCalls("OfflineTrainBooking.aspx/locationsByAgentId", "{'agentId':'" + Agentid + "'}"));
                $.each(data.Locations, function (index, item) {
                    options += "<option value='" + item.LOCATION_ID + "'>" + item.LOCATION_NAME + "</option>";
                });

                $("#ddlLocation option").remove();
                $('#ddlLocation').append(options);
                options = "";
                LoginInfo.AgentId = Agentid;
                LoginInfo.IsOnBehalfOfAgent = true;
                LoginInfo.Decimal = data.Agency["DecimalValue"];
                LoginInfo.Currency = data.Agency["AgentCurrency"];

            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }

        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function trainPaxCount() {
            var htlAdultCount = parseInt($('#ddlAdult').val());
            var htlChildCount = parseInt($('#ddlChild').val());
            var TrainTotalPax = '<strong>';
            TrainTotalPax += htlAdultCount;
            TrainTotalPax += '</strong> Adult ';
            if (htlChildCount > 0) {
                TrainTotalPax += ', <strong>' + htlChildCount + '</strong> Child';
            }
            $('#trainTotalPax').html(TrainTotalPax);
        }
        $('#passengerSelectDropDown select').change(function () {
            trainPaxCount();
        });
        $(function () {
            $('.no-select2').select2('destroy');
        });
        function addTrainDetails() {
            hideMessage();
            try {
                if (('<%=Settings.LoginInfo.MemberType %>') == '<%=MemberType.ADMIN%>') {
                    if (document.getElementById("clientDiv").style.display == "block") {
                        if ($('#ddlAgent').val() <= 0) {
                            showValidationMessage("Please select any agent");
                            return;
                        }
                        else {
                            hideValidationMessage();
                        }
                        if ($('#ddlAgent').val() > 0 && $('#ddlLocation').val() <= 0) {
                            showValidationMessage("Please select location for agent");
                            return;
                        }
                        else {
                            hideValidationMessage();
                        }
                    }
                }
                if ($('#txtFromStation').val().trim() == '') {
                    showValidationMessage("Please enter From Station");
                    return;
                }
                else if ($('#txtFromStation').val().trim().length < 3) {
                    showValidationMessage("Please enter atleast 3 characters for From Station");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtToStation').val().trim() == '') {
                    showValidationMessage("Please enter To Station");
                    return;
                }
                else if ($('#txtToStation').val().trim().length < 3) {
                    showValidationMessage("Please enter atleast 3 characters for To Station");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtDate').val().trim() == '') {
                    showValidationMessage("Please select Boarding Date");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtBoardingFrom').val().trim() == '') {
                    showValidationMessage("Please select Boarding From");
                    return;
                }
                else if ($('#txtBoardingFrom').val().trim().length < 3) {
                    showValidationMessage("Please enter atleast 3 characters for Boarding From");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#ddlClass').val() == '0') {
                    showValidationMessage("Please select Class Preference");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtReservationUpto').val().trim() == '') {
                    showValidationMessage("Please enter Reservation Upto");
                    return;
                }
                else if ($('#txtReservationUpto').val().trim().length < 3) {
                    showValidationMessage("Please enter atleast 3 characters for Reservation Upto");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtTrainNumber').val().trim() == '') {
                    showValidationMessage("Please enter Train Number");
                    return;
                }
                else if ($('#txtTrainNumber').val().trim().length < 3) {
                    showValidationMessage("Please enter atleast 3 characters for Train Number");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtTrainName').val().trim() == '') {
                    showValidationMessage("Please enter Train Name");
                    return;
                }
                else if ($('#txtTrainName').val().trim().length < 3) {
                    showValidationMessage("Please enter atleast 3 characters for Train Name");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($("#ddlIdProofType").val() != '0' && $('#txtIdNumber').val().trim() == '') {
                    showValidationMessage("Please enter ID Proof Number");
                    return;
                }
                if ($("#ddlIdProofType").val() == '0' && $('#txtIdNumber').val().trim() != '') {
                    showValidationMessage("Please select ID Proof Type");
                    return;
                }
                if ($('#txtIdNumber').val().trim() != '') {
                    if ($('#txtIdNumber').val().trim().length < 3) {
                        showValidationMessage("Please enter atleast 3 characters for ID Proof Number");
                        return;
                    }
                }
                if ($('#ddlQuota').val() == '0') {
                    showValidationMessage("Please select Quota");
                    return;
                }
                else {
                    hideValidationMessage();
                }
                if ($('#txtEmail').val() == '') {
                    showValidationMessage("Please enter Email");
                    return;
                } else {
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if (!emailReg.test($('#txtEmail').val())) {
                        showValidationMessage("Please enter valid Email ");
                        return;
                    }
                    else {
                        hideValidationMessage();
                    }
                }

                disableControls(true);
                loadControls();
                $("#SubmitBlock").show();
                //loadDummyPassenger();

            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function validatePassangers() {
            $("#ctl00_upProgress").show();
            var submit = true;
            let adultCount = $("#ddlAdult").val();
            let childCount = $("#ddlChild").val();
            document.getElementById('errMess').style.display = "none";
            for (var j = 1; j <= adultCount; j++) {
                if ($('#txtFirstName-' + j).val().trim() == '') {
                    showValidationMessage("Please Select Adult" + j + " First Name");
                    submit = false;
                    break;
                }
                else if ($('#txtFirstName-' + j).val().trim().length < 2) {
                    showValidationMessage("Please enter atleast 2 characters for Adult" + j + " First Name");
                    submit = false;
                    break;
                }
                if ($('#txtLastName-' + j).val().trim() != '') {
                    if ($('#txtLastName-' + j).val().trim().length < 2) {
                        showValidationMessage("Please enter atleast 2 characters for Adult" + j + " Last Name");
                        submit = false;
                        break;
                    }
                }
                if ($('#ddlGender-' + j).val() == '0') {
                    showValidationMessage("Please enter Adult" + j + " Gender");
                    submit = false;
                    break;
                }
                if ($('#txtAge-' + j).val().trim() == '') {
                    showValidationMessage("Please enter Adult" + j + " Age");
                    submit = false;
                    break;
                }
                else if ($('#txtAge-' + j).val().trim() <= 12) {
                    showValidationMessage("Adult" + j + " age should be greater than 12");
                    submit = false;
                    break;
                }
                if ($('#ddlBerth-' + j).val() == '0') {
                    showValidationMessage("Please enter Adult" + j + " Berth Preference");
                    submit = false;
                    break;
                }
                if ($('#ddlSeniorCitizen-' + j).val() == '0') {
                    showValidationMessage("Please enter Adult" + j + " Senior Citizen Value");
                    submit = false;
                    break;
                }
            }
            for (var k = 1; k <= childCount; k++) {
                if (submit == true) {
                    if ($('#txtFirstNameC-' + k).val().trim() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Child" + k + " First Name";
                        submit = false;
                        break;
                    }
                    else if ($('#txtFirstNameC-' + k).val().trim().length < 2) {
                        showValidationMessage("Please enter atleast 2 characters for Child" + k + " First Name");
                        submit = false;
                        break;
                    }
                    if ($('#txtLastNameC-' + k).val().trim() != '') {
                        if ($('#txtLastNameC-' + k).val().trim().length < 2) {
                            showValidationMessage("Please enter atleast 2 characters for Child" + k + " Last Name");
                            submit = false;
                            break;
                        }
                    }
                    if ($('#ddlGenderC-' + k).val() == '0') {
                        showValidationMessage("Please enter Child" + k + " Gender");
                        submit = false;
                        break;
                    }
                    if ($('#ddlAgeC-' + k).val() == '0') {
                        showValidationMessage("Please enter Child" + k + " Age");
                        submit = false;
                        break;
                    }
                    if ($('#ddlBerthC-' + k).val() == '0') {
                        showValidationMessage("Please enter Child" + k + " Berth Preference");
                        submit = false;
                        break;
                    }
                }
            }
            if (submit == false) {
                $("#ctl00_upProgress").hide();
            }
            return submit;
        }
        function createItinerary() {
            try {
                let adultCount = $("#ddlAdult").val();
                let childCount = $("#ddlChild").val();
                for (var j = 1; j <= adultCount; j++) {
                    var Leadpass;
                    if (j == 1) { Leadpass = true } else { Leadpass = false; }
                    PassenegerInfo.push({
                        PaxId: 0,
                        BookingID: 0,
                        PaxName: $('#txtFirstName-' + j).val().trim() + ' ' + $('#txtLastName-' + j).val().trim(),
                        Age: $('#txtAge-' + j).val().trim(),
                        Gender: $('#ddlGender-' + j).val(),
                        BerthPreference: $('#ddlBerth-' + j).val(),
                        IsSeniorCitizen: $('#ddlSeniorCitizen-' + j).val() == 'Yes' ? true : false,
                        MealPreference: $('#ddlMeal-' + j).val(),
                        IsChild: false,
                        CreatedBy: LoginInfo.AgentId,
                        IsLeadPassenger: Leadpass
                    })
                }
                for (var k = 1; k <= childCount; k++) {
                    PassenegerInfo.push({
                        PaxId: 0,
                        BookingID: 0,
                        PaxName: $('#txtFirstNameC-' + k).val().trim() + ' ' + $('#txtLastNameC-' + k).val().trim(),
                        Age: $('#ddlAgeC-' + k).val(),
                        Gender: $('#ddlGenderC-' + k).val(),
                        BerthPreference: $('#ddlBerthC-' + k).val(),
                        IsSeniorCitizen: false,
                        MealPreference: $('#ddlMealC-' + k).val(),
                        IsChild: true,
                        CreatedBy: LoginInfo.AgentId,
                        IsLeadPassenger: false
                    });
                }
                BookingRemarks.push({
                    BookingRemarksID: 0,
                    BookingID: 0,
                    RemarksStatus: 'Pending',
                    Remarks: 'Request created',
                    CreatedBy: LoginInfo.AgentId
                });
                RailwayItinerary.push({
                    BookingID: 0,
                    FromStation: $('#txtFromStation').val().trim(),
                    ToStation: $('#txtToStation').val().trim(),
                    BoardingDate: ConvertDate($('#txtDate').val()),
                    BoardingFrom: $('#txtBoardingFrom').val().trim(),
                    Class: $('#ddlClass').val(),
                    ReservationUpto: $('#txtReservationUpto').val().trim(),
                    TrainNumber: $('#txtTrainNumber').val().trim(),
                    TrainName: $('#txtTrainName').val().trim(),
                    IDProofType: $('#ddlIdProofType').val() != '0' ? $('#ddlIdProofType').val() : null,
                    IDProofNumber: $('#txtIdNumber').val().trim() != '' ? $('#txtIdNumber').val().trim() : null,
                    Adults: $('#ddlAdult').val(),
                    Children: $('#ddlChild').val(),
                    Quota: $('#ddlQuota').val(),
                    AgentId: LoginInfo.AgentId,
                    LocationId: LoginInfo.LocationID,
                    IsOnbehalf: LoginInfo.IsOnBehalfOfAgent,
                    Email: $('#txtEmail').val().trim(),
                    Status: 'Pending',
                    CreatedBy: LoginInfo.CreatedBy,
                    PassengerDetails: PassenegerInfo,
                    BookingRemarks: BookingRemarks[0]
                })
            } catch (e) {
                $("#ctl00_upProgress").hide();
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function submit() {
            $("#ctl00_upProgress").show();
            try {
                if (validatePassangers()) {
                    createItinerary();
                    var itinerary = JSON.stringify(RailwayItinerary[0]).replace('&', '').replace('#', '').replace('<', '').replace('>', '').replace('%', '').replace(/'/g, '').replace(/\\"/g, '');
                    var data = JSON.parse(AjaxCalls("OfflineTrainBooking.aspx/createBooking", "{'railwayItinerary':'" + itinerary + "'}"));
                    if (data != null) {
                        if (data["Status"] == "success") {
                            clearTrainDetails();
                            $("#ctl00_upProgress").hide();
                            //showMessage('Request has been created with Reference ID : ' + data["ReferenceID"], 'success');
                            showModal('Request has been created with Reference ID : ' + data["ReferenceID"]);
                        }
                    }
                    else {
                        clearTrainDetails();
                        $("#ctl00_upProgress").hide();
                        //showMessage('Error occured. Please contact support team.', 'error');
                        showModal('Error occured. Please contact support team.');
                    }
                }
                else {
                    $("#ctl00_upProgress").hide();
                }
            } catch (e) {
                clearTrainDetails();
                $("#ctl00_upProgress").hide();
                //showMessage('Error occured. Please contact support team.', 'error');
                showModal('Error occured. Please contact support team.');
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function showModal(msg) {
            $("#divModalMessage").text(msg);
            $("#modalSubmit").modal('show');
        }
    </script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div class="body_container">
        <input type="hidden" id="hdnLoginInfo" runat="server" />
        <div id="errMess" class="error_module" style="display: none; padding-bottom: 10px;">
            <div id="errorMessage" class="alert-messages alert-caution"></div>
        </div>
        <div class="search-matrix-wrap hotels">
            <div id="divAgents" runat="server" class="custom-radio-table row mb-3">
                <div class="col-12">
                    <div class="row custom-gutter">
                        <div class="col-md-12">
                            <div class="agent-checkbox-wrap float-left">
                                <input id="tglAgentCtr" type="checkbox" name="tglAgentCtr" class="tgl tgl-light" />
                                <label onclick="showHideAgent()" class="tgl-btn" for="tglAgentCtr"><em></em></label>
                            </div>
                        </div>
                        <div id="clientDiv" class="col-md-12" style="display: none">
                            <div class="row">
                                <div class="col-md-3 mt-2">
                                    <div class="form-control-holder">
                                        <select id="ddlAgent" class="form-control" onchange="getLocationsByAgentId();">
                                            <option value="-1">Select Client</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mt-2">
                                    <div class="form-control-holder">
                                        <select id="ddlLocation" class="form-control">
                                            <option value="-1">Select Location</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2 text-right">
                                    <div class="agnetBalance text-primary" id="divBehalfAgentBalance" style="display: none">
                                        <span class="icon-money icon"></span>
                                        <%-- <span class="currency" id="spncurrency">AED</span> --%>
                                        <span class="agent-balance" id="spnbalance"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row custom-gutter mb-3">
                <div class="col-md-4">
                    <label style="color: #333;">From Station<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-location" aria-label="Origin Station"></span>
                        </div>
                        <input type="text" id="txtFromStation" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control origin-airport" placeholder="From Station" />
                    </div>
                </div>
                <div class="col swap-airports-wrap">
                    <label></label>
                    <a href="javascript:void(0);" class="swap-airports" tabindex="-1" title="Switch Airports"><span style="color: #60c231;" class="icon-loop-alt4"></span></a>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">To Station<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-location" aria-label="Destination Station"></span>
                        </div>
                        <input type="text" id="txtToStation" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control dest-airport" placeholder="To Station" />
                    </div>

                </div>
                <div class="col-md-3">
                    <label style="color: #333;">Boarding Date<span style="color: red;">*</span></label>
                    <a class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-calendar" aria-label=""></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Boarding Date" id="txtDate" />
                    </a>
                </div>
            </div>
            <div class="row custom-gutter mb-3">
                <div class="col-md-4">
                    <label style="color: #333;">Boarding From<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" id="txtBoardingFrom" placeholder="Boarding From">
                    </div>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">Class Preferencee<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-star" aria-label=""></span>
                        </div>
                        <select class="form-control" id="ddlClass">
                            <option selected="selected" value="0">Class</option>
                            <option value="Sleeper">Sleeper</option>
                            <option value="03 A/C">03 A/C</option>
                            <option value="02 A/C">02 A/C</option>
                            <option value="01 A/C">01 A/C</option>
                            <option value="CHAIR CAR">CHAIR CAR</option>
                            <option value="EXE CHAIR CAR">EXE CHAIR CAR</option>
                            <option value="EA EXE CHAIR CAR">EA EXE CHAIR CAR</option>

                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">Reservation Upto<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" id="txtReservationUpto" placeholder="Reservation Upto">
                    </div>
                </div>
            </div>
            <div class="row custom-gutter mb-3">
                <div class="col-md-4">
                    <label style="color: #333;">Train Number<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <input type="text" onkeyup="return IsAlphaString(this.id);" onkeypress="return IsAlpha(event);" class="form-control" id="txtTrainNumber" placeholder="Train Number">
                    </div>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">Train Name<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <input type="text" onkeyup="return IsAlphaNumericString(this.id);" onkeypress="return IsAlphaNumeric(event);" class="form-control" id="txtTrainName" placeholder="Train Name">
                    </div>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">Number of Passengers<span style="color: red;">*</span></label>
                    <div class="custom-dropdown-wrapper">
                        <a href="jaavscript:void(0);" class="form-control-element with-custom-dropdown" data-dropdown="#passengerSelectDropDown">
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-group"></span>
                                </div>
                                <span class="form-control-text" id="trainTotalPax">1 Adult</span>
                            </div>
                        </a>
                        <div class="dropdown-content d-none p-4 hotel-pax-dropdown" id="passengerSelectDropDown" style="max-height: 270px;">
                            <div class="row custom-gutter" id="room-1">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select onchange="trainPaxCount();" class="form-control no-select2 adult-pax" name="ddlAdult" id="ddlAdult">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Child(5-12 yrs)</label>
                                        <select onchange="trainPaxCount();" name="ddlChild" id="ddlChild" class="form-control no-select2 child-pax">
                                            <option selected="selected" value="0">None</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row custom-gutter mb-3">
                <div class="col-md-4">
                    <label style="color: #333;">ID Proof</label>
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-star" aria-label=""></span>
                        </div>
                        <select class="form-control" id="ddlIdProofType">
                            <option selected="selected" value="0">ID Proof Type</option>
                            <option value="AADHAAR CARD">AADHAAR CARD</option>
                            <option value="PAN CARD">PAN CARD</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">ID Proof Number</label>
                    <div class="form-control-holder">
                        <input type="text" onkeyup="return IsAlphaString(this.id);" onkeypress="return IsAlpha(event);" class="form-control" id="txtIdNumber" placeholder="ID Number">
                    </div>
                </div>
                <div class="col-md-4">
                    <label style="color: #333;">Quota<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-star" aria-label=""></span>
                        </div>
                        <select class="form-control" id="ddlQuota">
                            <option selected="selected" value="0">Quota</option>
                            <option value="GENERAL">GENERAL</option>
                            <option value="TATKAL">TATKAL</option>
                            <option value="PREMIUM TATKAL">PREMIUM TATKAL</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row custom-gutter">
                <div class="col-md-4">
                    <label style="color: #333;">Email<span style="color: red;">*</span></label>
                    <div class="form-control-holder">
                        <input type="text" class="form-control" id="txtEmail" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="row custom-gutter">
                <div id="SubmitMessage" class="col-md-12" style="display: none; padding: 10px 5px">
                    <div class="alert" role="alert"></div>
                </div>
            </div>
            <div class="col-md-12" style="text-align: right;">
                <div class="pull-right mr-4 mt-2">
                    <input id="btnClearTrainDetails" class="but but_b block_xs" type="button" value="Clear" onclick="clearTrainDetails()" />
                    <input id="btnAddTrainDetails" class="but but_b block_xs" type="button" value="Add Details" onclick="addTrainDetails()" />
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="paramcon bg_white pad_10" title="Param" id="lstError" style="display: none">
            <div>
                <center><label id="lblError" class="lblSuccess" style="color:red;FONT-SIZE: 14px;"></label></center>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="divPassengerDetailsBlock" style="display: none;">
            <div class="right_col CorpTrvl-page">
                <div class="row">
                    <div class="col-md-12 CorpTrvl-tabbed-panel mt-2 mb-0">
                        <ul class="nav nav-tabs" id="Tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#paxTabDiv" id="paxTab" onclick="navigateTab(1);">Passenger Details  </a>
                            </li>
                        </ul>
                        <div class="tab-content p-2">
                            <div id="paxTabDiv" class="tab-pane fade in active">
                                <h4><a class="" data-toggle="collapse" href="#PaxDetailsCollapse" role="button" aria-expanded="true" aria-controls="PaxDetailsCollapse">
                                    <strong>Passenger Details</strong> <span class="expand-details"><i class="fa fa-plus-circle"></i></span></a>
                                </h4>
                                <div class="collapse in" id="PaxDetailsCollapse">
                                    <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListGuest"></ul>
                                    <div class="row no-gutter border-bottom pb-3 pt-3" style="display: none;" id="Adult">
                                        <div class="col-12 mb-4">
                                            <div class="row">
                                                <div class="col-6">
                                                    <strong>
                                                        <label id="h4PaxType"></label>
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxFirstName">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxLastName">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxGender">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxAge">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxBerth">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxMeal">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxSeniorCitizen">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row no-gutter pb-3 pt-3">
                                        <div class="col-12 mb-4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <a id="btnRemovePassengers" class="btn btn-primary btn_custom pull-left mr-4" href="#" onclick="removePassengers();">Remove Passengers</a>
                                                    <a class="btn btn-primary btn_custom pull-left mr-4" href="#" onclick="clearPassengerDetails();">Clear</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="SubmitBlock" class="row no-gutter" style="display: none;">
        <div class="col-md-12" style="text-align: right;">
            <div class="pull-right mr-4 mt-2">
                <a id="btnClear" class="btn btn-primary btn_custom pull-left mr-4" href="#" onclick="clearTrainDetails();">Reset</a>
                <a id="btnSubmit" onclick="submit();" class="btn btn-primary btn_custom pull-left" href="#">Submit</a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSubmit" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle">Train Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px !important;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divModalMessage"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

