﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="B2CActivitySettings" Title="B2C Activity Settings" Codebehind="B2CActivitySettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script type="text/javascript">
    function validation() {

        var chklist = document.getElementById('<%= chkProduct.ClientID %>');
        var chkListinputs = chklist.getElementsByTagName("input");
        var count = 0;
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                if (i < 2 || i == 4) {
                    if (document.getElementById('ctl00_cphTransaction_ddlSource_' + i).selectedIndex <= 0) {
                        document.getElementById('<%= errMess.ClientID %>').style.display = "block";
                        document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Source!";
                        return false;
                    }
                }
                count = count + 1;
            }
        }
        if (count == 0) {
            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Atleast one Product should be selected !";
            return false;
        }
        return true;
    }
    function Check(id) {
        var val = document.getElementById(id).value;
        if (val == '0.00') {
            document.getElementById(id).value = '';
        }
    }

    function Set(id) {
        var val = document.getElementById(id).value;
        if (val == '' || val == '0.00') {
            document.getElementById(id).value = '0.00';
        }
    }
    function SetValue() {
        var chklist = document.getElementById('<%= chkProduct.ClientID %>');
        var chkListinputs = chklist.getElementsByTagName("input");
        var B2CMarkup = eval(0);
        var totalFee = eval(0);
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                B2CMarkup = parseInt(document.getElementById('ctl00_cphTransaction_txtB2CMarkup_' + i).value);
                if (isNaN(B2CMarkup)) {
                    B2CMarkup = 0;
                }

                totalFee = B2CMarkup;
                document.getElementById('ctl00_cphTransaction_txtMarkUp_' + i).value = totalFee.toFixed(2);
                document.getElementById('ctl00_cphTransaction_txtB2CMarkup_' + i).value = B2CMarkup.toFixed(2);
                //                document.getElementById('ctl00_cphTransaction_txtOurComm_' + i).value = OurComm.toFixed(2);
            }
        }
    }

    function ValidateFAQ() {
        if (document.getElementById('<%=txtQuestion.ClientID %>').value == "") {
            document.getElementById('<%= divErrMess.ClientID %>').style.display = "block";
            document.getElementById('<%= divErrorMessage.ClientID %>').innerHTML = "Please enter a valid question !";
            return false;
        }
        if (document.getElementById('<%=txtAnswer.ClientID %>').value == "") {
            document.getElementById('<%= divErrMess.ClientID %>').style.display = "block";
            document.getElementById('<%= divErrorMessage.ClientID %>').innerHTML = "Please enter valid text for Answer !";
            return false;
        }
        return true;
    }
     
    </script>
<asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label>
<a href="B2CSettings.aspx">Back To B2C Settings</a>
    <asp:MultiView ID="mvSettings" runat="server" ActiveViewIndex="0">
        <asp:View ID="ActivityView" runat="server">
        
            
                <div class="ns-h3">Activity Settings</div>
              <div class="bg_white paramcon pad_10 bor_gray">
                
                
                
                
                     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="Label1" runat="server" Text="Email :"></asp:Label></div>
 
 
 
    <div class="col-md-2">
    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
    
                   
                                <asp:RegularExpressionValidator ID="emailValid" runat="server" 
                                    ControlToValidate = "txtEmail" Display ="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
    ErrorMessage="Please Enter Valid Email."></asp:RegularExpressionValidator>
    
     </div>
    
    <div class="col-md-2"> <asp:Label ID="Label2" runat="server" Text="Waiting Text :"></asp:Label></div>
    
     <div class="col-md-2"> <asp:TextBox ID="txtWaitingText" runat="server" CssClass="form-control"></asp:TextBox></div>
     
     
     <div class="col-md-2"><asp:Label ID="Label3" runat="server" Text="Waiting Logo :"></asp:Label> </div>
     
     
     
         <div class="col-md-2"> <asp:FileUpload ID="fuWaitingLogo" runat="server"/></div>

<div class="clearfix"></div>
    </div>




     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="Label4" runat="server" Text="Google Script Type :"></asp:Label></div>
 
 
    <div class="col-md-2"> <asp:RadioButton ID="rbtnGoogleAnalytics" runat="server" GroupName="Google" Text="Google Analytics" /></div>
    
    
    <div class="col-md-2"> <asp:RadioButton ID="rbtnGooglePPC" runat="server" GroupName="Google" Text="Google PPC" /></div>
    

<div class="clearfix"></div>
    </div>






     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="Label5" runat="server" Text="Google Script :"></asp:Label></div>
 
 
    <div class="col-md-6"><asp:TextBox ID="txtGoogleScript" TextMode="MultiLine" Rows="5" Width="100%" runat="server"></asp:TextBox> </div>


<div class="clearfix"></div>
    </div>





     <div class="col-md-12 marbot_10">                                      
   
<asp:Button ID="btnSaveSettings" runat="server" CssClass="button pull-right" Text="Save" OnClick="btnSaveSettings_Click" />
<asp:Button ID="btnClearSettings" runat="server" CssClass="button pull-right" Text="Clear" OnClick="btnClearSettings_Click" />


<asp:Button ID="btnActivityViewNext" runat="server" Text="Next" CssClass="button pull-right" OnClick="btnNext_Click" />

    </div>


                    
                    
                    <div class="clearfix"></div>
                </div>
          
        </asp:View>
      
      
        <asp:View ID="MarkupView" runat="server">
           
              
                <div class="ns-h3">
                    Markup and Payment Gateway</div>
               
               
                <div class="bg_white paramcon pad_10 bor_gray">
                 
                 
                 <h4> Payment Gateway</h4>
                 
                      <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-3">
 <asp:CheckBox Width="110" ID="chkENBD" runat="server" Text="ENBD" Checked="true" />
 <asp:Label ID="Label9" runat="server" Text="Apply"></asp:Label>
 <asp:TextBox ID="txtENBDCharges" runat="server" Width="50px"></asp:TextBox>
 
 
  </div>
 
 
    <div class="col-md-8"> <asp:Label ID="Label10" runat="server" Text=" % of Credit Card charge will be charged extra per booking to Customer"></asp:Label></div>


<div class="clearfix"></div>
    </div>




     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-3"> 
 
 <asp:CheckBox Width="110" ID="chkCCA" runat="server" Text="CC Avenue" />
 

 
 <asp:Label ID="Label6" runat="server" Text="Apply"></asp:Label><asp:TextBox ID="txtCCACharges" runat="server" Width="50px"></asp:TextBox>
 
 
 
 </div>
 
    <div class="col-md-8"><asp:Label ID="Label12" runat="server" Text=" % of Credit Card charge will be charged extra per booking to Customer"></asp:Label> </div>


<div class="clearfix"></div>
    </div>



<h4>  Update B2C Markup <asp:HiddenField ID="hdnCount" runat="server" /></h4>



     <div class="col-md-12 padding-0 marbot_10">                                      
   <div id="errMess" runat="server" class="error_module" style="display: none;">
                                                                    <div id="errorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                                                    </div>
                                                                </div>
 <div class="col-md-2"> <asp:Label ID="Label8" runat="server" Text="Select Agent :"></asp:Label></div>
 
    <div class="col-md-2"> 
    <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server" AutoPostBack="true" 
    OnSelectedIndexChanged="ddlAgent_OnSelectedIndexChanged" />
                                                                </asp:DropDownList>
    
    
    </div>



<div class="clearfix"></div>
    </div>
    
    
    
         <div class="col-md-12 marbot_10">                                      
   <asp:CheckBoxList ID="chkProduct" CssClass="chkBoxList" runat="server" RepeatLayout="Table"
                                                        RepeatDirection="Horizontal" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="chkProduct_OnSelectedIndexChanged">
                                                    </asp:CheckBoxList>

    </div>



     <div class="col-md-12 marbot_10">   
     <table id="tblMarkup" runat="server" style="width: 100%">
                                                    </table>
     
     
     </div>
    
    
    
    
    <div class="col-md-12 marbot_10"> 
    <asp:Button ID="btnSaveMarkup" runat="server" CssClass="button pull-right" Text="Save" OnClick="btnSaveMarkup_Click" />
    
    
    <asp:Button ID="btnClearMarkup" runat="server" CssClass="button pull-right" Text="Clear" OnClick="btnClearMarkup_Click" />
    
 
    
    
    <asp:Button ID="btnMarkupNext" runat="server" Text="Next" CssClass="button pull-right" OnClick="btnNext_Click" />
    
       <asp:Button ID="btnMarkupPrev" runat="server" Text="Previous" CssClass="button pull-right" OnClick="btnPrev_Click" />
     </div>
                                                    
                                                    


               
                   
        <div class="clearfix"></div>
                    
                    
                    
                </div>
          
           
          
        </asp:View>
        
        
        
        
        
        
        <asp:View ID="FaqView" runat="server">
           
            
                <div class="ns-h3">
                    Frequently Asked Questions</div>
               <div class="bg_white paramcon pad_10 bor_gray">
                 
                 
                 
                      <div class="col-md-12 padding-0 marbot_10">                                      
   <div id="divErrorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                </div>
                       <div id="divErrMess" runat="server" class="error_module" style="display: none;">
                                
                            </div>         
                                
 <div class="col-md-2"> <asp:Label ID="Label13" runat="server" Text="Question :"></asp:Label></div>
    <div class="col-md-6"> <asp:TextBox CssClass="form-control" ID="txtQuestion" runat="server" Width="100%"></asp:TextBox></div>


<div class="clearfix"></div>
    </div>




     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="Label14" runat="server" Text="Answer :"></asp:Label></div>
 
 
    <div class="col-md-6"> <asp:TextBox ID="txtAnswer" runat="server" TextMode="MultiLine" Rows="6" Width="100%"></asp:TextBox></div>


<div class="clearfix"></div>
    </div>

<div class="col-md-12 marbot_10">     

<asp:Button ID="btnSaveFAQ" runat="server" Text="Save" CssClass="button pull-right" OnClientClick="return ValidateFAQ();" OnClick="btnSaveFAQ_Click"/>

<asp:Button ID="btnClearFAQ" runat="server" Text="Clear" CssClass="button pull-right" OnClick="btnClearFAQ_Click"/>





<asp:Button ID="btnFAQNext" runat="server" Text="Next" CssClass="button pull-right" OnClick="btnNext_Click" />

<asp:Button ID="btnFAQPrev" runat="server" Text="Previous" CssClass="button pull-right" OnClick="btnPrev_Click" />

</div>




<div class="col-md-12 marbot_10">   


<asp:GridView ID="gvFAQ" runat="server" DataKeyNames="QuestionId" AutoGenerateColumns="false" OnSelectedIndexChanging="gvFAQ_SelectedIndexChanging">
                            <Columns>
                            <asp:CommandField ButtonType="Link" ShowSelectButton="true" SelectText="Edit" />
                            <asp:BoundField HeaderText="Question" DataField="Question" />
                            <asp:BoundField HeaderText="Answer" DataField="Answer" />                            
                            <asp:BoundField HeaderText="CreatedOn" DataField="CreatedOn"  />                            
                            <asp:BoundField HeaderText="ModifiedOn" DataField="LastModifiedOn"  />
                            
                            </Columns>
                            </asp:GridView>

</div>
                 
                 
                 
                 
                   
                    
                    
                    
                    <div class="clearfix"></div>
                </div>
          
        </asp:View>
        
        
        
        
        <asp:View ID="FraudPanelView" runat="server">
         
           
                <div class="ns-h3">
                    Fraud Panel Settings</div>
              <div class="bg_white paramcon pad_10 bor_gray">
                    <table style="width: 100%;" border="1">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                   
                   
                   
                   <div> <asp:Button ID="btnFraudPrev" runat="server" Text="Previous" CssClass="button pull-right" OnClick="btnPrev_Click" /></div>
                   
                 
               <div class="clearfix"></div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
