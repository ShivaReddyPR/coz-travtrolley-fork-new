﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="WLCustomerDetails" Title="WLCustomerDetails" Codebehind="WLCustomerDetails.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<div>


<div>
<form id="Searchform" action="WLCustomerDetails.aspx" method="post">
<input class="noofsets" id="noofsets" type="hidden" value="<%=noofsets%>" />
<input class="noofpages" id="noofpages" type="hidden" value="<%=noofpages %>" />
<input type="hidden" id="requestedpageno" name="pageNo" value="<%=pageno%>"/>
<input type="hidden" id="requestedset" name="setno" value="<%=setno %>" />
<input type="hidden" name="Searchby" value="<%=Searchby %>" id="Searchby" />


<div> 



<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Search</td>
     <td width="10"> </td>
    <td><select style=" width:200px" class="form-control" id="Searchthrough">
     <option  value="Name" selected="selected" >Name</option>
     <option  value="email" >Email ID</option>
     <option  value="MobileNo">Mobile No</option>    
</select></td>
  <td width="10"> </td>
    <td> <input class="form-control" type="text" id="SearchBox" name="SearchBox" onkeydown="testForEnter(event)" value="<%=SearchBox%>" /></td>
      <td width="10"> </td>
    <td><input type="button" id="search" Class="btn but_b" style=" width:100px"   value="Submit" onclick="submitsearch()" /></td>
  </tr>
</table>



</div>









 
 </form>
 
 

 <div id="Errormsg" style="margin:8px 20px 0px 100px;color:Red;font-size:large"><%=errormessage%></div>
 
 </div>
 <%if(errormessage == "") { %>
 <div class="" style="">
   <div class="paging" style="width: 100%">
   
   <label class="pull-right"> 
   
   <%if(setno!=1){ %>
   <b><a href="javascript:firstset()">First</a></b> |<%} %>
  
   <%if (pageno != 1)
      { %>
        <b><a href="javascript:prevpage()">Prev</a></b> |<% } %>
        
   <%for (int i = ((setno - 1) * 5)+1; i <= ((setno - 1)*5)+5 && i <= noofpages; i++)
     {%>
        <%if (i == pageno)
          { %>
        <b><a href="javascript:paging(<%=i%>)"><%=i%></a></b> |
        <%}
          else
          { %>
        <a href="javascript:paging(<%=i%>)"><%=i%></a> |
        <%} %>
    <% } %>
    
    <%if(pageno != noofpages){ %>
    <b><a href="javascript:nextpage()">Next</a></b> |<% }  %>
    
    <%if (setno != noofsets)
      { %>
    <b><a href="javascript:lastset()">Last</a></b><%} %>  
    </label>
    
    </div>
   
   
    <table border="1"  class="datagrid" style="width:100%;" >
    <tr>
    <th ><b>Customer ID</b></th>
    <th ><b>First Name</b></th>
    <th ><b>Last Name</b></th>
    <th ><b>E-mail id</b></th>
    <th ><b>Mobile No</b></th>
    <th ><b>IPAddress</b></th>
    <th ><b>SiteName</b></th>
    </tr>
    <%for (int i = 0; i < customerlist.Count; i++)
      { %>
     <tr>
     <td  ><% =customerlist[i].CustomerId %></td>
     <td  ><% =customerlist[i].FirstName %></td>
     <td  ><% =customerlist[i].LastName %></td> 
     <td  ><% =customerlist[i].UserName %></td>
     <td  ><% =customerlist[i].MobileNo %></td> 
     <td  ><% =customerlist[i].IPAddress %></td> 
     <td  ><% =customerlist[i].SiteName%></td> 
     </tr>
    <%} %>
    
    </table>    
 </div>
 <%} %>
 </div>
 <script type="text/javascript">

     for (var i = 0; i < 3; i++) {
         if (document.getElementById("Searchthrough").options[i].value == document.getElementById("Searchby").value) {
             document.getElementById("Searchthrough").selectedIndex = i;
         }
     }
  
   </script>
   
   <script type="text/javascript">
       function paging(i) {
           document.getElementById("requestedpageno").value = i + " ";
           //           document.getElementById("Searchform").submit();
           document.forms[0].submit();
       }

       function prevpage() {
           document.getElementById("requestedpageno").value = parseInt(document.getElementById("requestedpageno").value, 10) - 1 + "";
           if (parseInt(document.getElementById("requestedpageno").value, 10) < ((parseInt(document.getElementById("requestedset").value, 10) - 1) * 5))
               document.getElementById("requestedset").value = parseInt(document.getElementById("requestedset").value, 10) - 1 + "";
           //           document.getElementById("Searchform").submit();
           document.forms[0].submit();
       }
       function nextpage() {
           document.getElementById("requestedpageno").value = parseInt(document.getElementById("requestedpageno").value, 10) + 1 + "";
           if (parseInt(document.getElementById("requestedpageno").value, 10) > ((parseInt(document.getElementById("requestedset").value, 10) - 1) * 5) + 5)
               document.getElementById("requestedset").value = parseInt(document.getElementById("requestedset").value, 10) + 1 + "";
           //           document.getElementById("Searchform").submit();
           document.forms[0].submit();
       }

       function submitsearch() {

           document.getElementById("Searchby").value = document.getElementById("Searchthrough").options[document.getElementById("Searchthrough").selectedIndex].value;
           document.getElementById("requestedpageno").value = "1";
           document.getElementById("requestedset").value = "1";
           //           document.getElementById("Searchform").submit();
           document.forms[0].submit();
       }
       function firstset() {
           document.getElementById("requestedset").value = "1";
           document.getElementById("requestedpageno").value = ((parseInt(document.getElementById("requestedset").value, 10) - 1) * 5) + 1 + "";
           //           document.getElementById("Searchform").submit();
           document.forms[0].submit();
       }
       function lastset() {
           document.getElementById("requestedset").value = document.getElementById("noofsets").value;
           document.getElementById("requestedpageno").value = document.getElementById("noofpages").value;
           //           document.getElementById("Searchform").submit();
           document.forms[0].submit();
       }
       function testForEnter(event) {
           if (event.keyCode == 13) {
               document.getElementById("Searchby").value = document.getElementById("Searchthrough").options[document.getElementById("Searchthrough").selectedIndex].value;
               document.getElementById("requestedpageno").value = "1";
               document.getElementById("requestedset").value = "1";
           }
       } 


    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

